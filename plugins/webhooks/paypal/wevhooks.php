<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
include('../../../inc/global.php');

$Data=file_get_contents('php://input');
$JSON=str::json_data($Data, 'decode');

ob_start();
echo "JSON:\r\n"; str::dump($JSON); echo "\r\n";
$log=ob_get_contents();
ob_end_clean();
file::write_file('/logs/paypal/webhooks/'.date('Y_m/d/', $c['time']), date('H.i.s', $c['time']).'.txt', $log);//把返回数据写入文件

if($JSON){
	//webhooks记录
	$dataWeb=array(
		'EventId'		=>	$JSON['id'],			//事件通知ID
		'ResourceType'	=>	$JSON['resource_type'],	//与通知事件相关的资源名称
		'EventType'		=>	$JSON['event_type'],	//触发事件通知的事件
		'Summary'		=>	$JSON['summary'],		//事件通知的摘要说明
		'Resource'		=>	addslashes(str::json_data($JSON['resource'])),//触发事件通知的资源
		'CreateTime'	=>	strtotime($JSON['create_time'])//创建事件通知的日期和时间
	);
	db::insert('orders_paypal_webhooks_log', $dataWeb);
	$LogId=db::get_insert_id();
	
	//事件处理
	switch($JSON['event_type']){
		//case 'PAYMENT.SALE.COMPLETED':
		//	//----------------------------------------------- 销售完成 Start -----------------------------------------------
		//	
		//	//----------------------------------------------- 销售完成 End -----------------------------------------------
		//	break;
		case 'PAYMENT.SALE.REFUNDED':
			//----------------------------------------------- 商家销售退款 Start -----------------------------------------------
			$Status=strtolower($JSON['resource']['state']);//退款状态
			if($Status=='completed'){
				//退款状态已完成
				$Status=1;
			}elseif($Status=='pending'){
				//退款状态正在等待处理
				$Status=2;
			}else{
				//退款状态失败
				$Status=0;
			}
			$SaleId=$JSON['resource']['sale_id'];//销售ID
			$payment_info_row=db::get_one('orders_payment_info', "MTCNNumber='{$SaleId}'");
			if($payment_info_row){
				//退款记录
				$OrderId=$payment_info_row['OrderId'];
				if(!db::get_row_count('orders_refund_info', "OrderId='{$OrderId}' and Status=1")){
					//已经在do_action记录退款成功，就不必再执行
					$order_row=db::get_one('orders', "OrderId='{$OrderId}'");
					$dataRefund=array(
						'Number'	=>	$SaleId,
						'Amount'	=>	$JSON['resource']['amount']['total'],
						'Currency'	=>	$JSON['resource']['amount']['currency'],
						'Status'	=>	$Status,
						'UpdateTime'=>	strtotime($JSON['resource']['update_time'])
					);
					if(db::get_row_count('orders_refund_info', "OrderId='{$OrderId}'")){
						db::update('orders_refund_info', "OrderId='{$OrderId}'", $dataRefund);
					}else{
						$dataRefund['OrderId']=$OrderId;
						$dataRefund['AccTime']=strtotime($JSON['resource']['create_time']);
						db::insert('orders_refund_info', $dataRefund);
					}
					//货币符号
					$Symbol=db::get_value('currency', "Currency='{$JSON['resource']['amount']['currency']}'", 'Symbol');
					//订单取消
					if($Status==1){
						$PassOrderStatus=$order_row['OrderStatus'];
						$dataCancel=array(
							'OrderStatus'	=>	7,
							'UpdateTime'	=>	$c['time']
						);
						db::update('orders', "OrderId='{$OrderId}'", $dataCancel);
						$order_row=db::get_one('orders', "OrderId='{$OrderId}'");//更新订单信息
						$Log='Update order status from '.$c['orders']['status'][$PassOrderStatus].' to '.$c['orders']['status'][7].'. This order has been refunded successfully, refund amount: '.$Symbol.$refund_data['Amount'];
						orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $OrderId, 7, $Log, 1);
						//处理订单信息
						orders::orders_products_update(7, $order_row, 1);
						$OId=$order_row['OId'];
						//发邮件
						$ToAry=array($order_row['Email']);
						if((int)db::get_value('system_email_tpl', "Template='order_cancel'", 'IsUsed')){//取消订单
							include($c['root_path'].'/static/static/inc/mail/order_cancel.php');
							ly200::sendmail($ToAry, $mail_title, $mail_contents);
						}
						manage::operation_log('更新订单状态');
					}
				}
			}
			//----------------------------------------------- 商家销售退款 End -----------------------------------------------
			break;
		case 'CUSTOMER.DISPUTE.CREATED':
			//----------------------------------------------- 顾客创建争议 Start -----------------------------------------------
			if($JSON['resource']['disputed_transactions']){//商家看到的交易ID
				$OrderId=db::get_value('orders_payment_info', "MTCNNumber='{$JSON['resource']['disputed_transactions'][0]['seller_transaction_id']}'", 'OrderId');
			}
			$data=array(
				'DisputeID'			=>	$JSON['resource']['dispute_id'],//争议ID
				'OrderId'			=>	$OrderId,
				'CreateTime'		=>	strtotime($JSON['resource']['create_time']),	//争议创建的日期和时间
				'UpdateTime'		=>	strtotime($JSON['resource']['update_time']),	//争议最后更新的日期和时间
				'TransactionsInfo'	=>	addslashes(str::json_data($JSON['resource']['disputed_transactions'])),//争议相应的交易详情
				'Reason'			=>	$JSON['resource']['reason'],					//争议原因
				'Status'			=>	$JSON['resource']['status'],					//争议状态
				'Currency'			=>	$JSON['resource']['dispute_amount']['currency_code'],//争议金额的货币
				'Amount'			=>	$JSON['resource']['dispute_amount']['value'],	//争议金额
				'Offer'				=>	addslashes(str::json_data($JSON['resource']['offer'])),//顾客要求退还的争议
				'Lifecycle'			=>	$JSON['resource']['dispute_life_cycle_stage'],	//争议生命周期的阶段
				'Channel'			=>	$JSON['resource']['dispute_channel']			//顾客创建争议的渠道
			);
			db::insert('orders_paypal_dispute', $data);
			$DId=db::get_insert_id();
			db::update('orders_paypal_webhooks_log', "LId='{$LogId}'", array('RelatedId'=>$DId));//webhooks记录争议ID
			$dataTo=array(
				'DId'		=>	$DId,//争议ID
				'PostedBy'	=>	($JSON['resource']['messages'][0]['posted_by']=='BUYER'?1:0),	//发布者 BUYER:顾客 SELLER:商家
				'Content'	=>	addslashes($JSON['resource']['messages'][0]['content']),		//与通知事件相关的资源名称
				'AccTime'	=>	strtotime($JSON['resource']['messages'][0]['time_posted'])		//创建事件通知的日期和时间
			);
			db::insert('orders_paypal_dispute_message', $dataTo);
			//----------------------------------------------- 顾客创建争议 End -----------------------------------------------
			break;
		case 'CUSTOMER.DISPUTE.UPDATED':
			//----------------------------------------------- 顾客争议更新 Start -----------------------------------------------
			$dispute_row=str::str_code(db::get_one('orders_paypal_dispute', "DisputeID='{$JSON['resource']['dispute_id']}'"));
			$DId=$dispute_row['DId'];
			$OfferData=str::json_data(htmlspecialchars_decode($dispute_row['Offer']), 'decode');
			$Status=$JSON['resource']['status'];//争议状态
			$Lifecycle=$JSON['resource']['dispute_life_cycle_stage'];//争议生命周期的阶段
			$UpdateTime=strtotime($JSON['resource']['update_time']);//争议最后更新的日期和时间
			$Offer=$JSON['resource']['offer'];//争议互相要求条件
			$Links=$JSON['resource']['links'];//事件地址
			$IsRefund=$RefundStatus=0;
			$data=array(
				'UpdateTime'=>$UpdateTime//争议最后更新的日期和时间
			);
			if($Status!=$dispute_row['Status']){//争议状态有改动
				$data['Status']=$Status;
				if($dispute_row['Status']=='OPEN' && $Status=='RESOLVED'){//解决争议
					$data['ResolvedTime']=$UpdateTime;
				}else{//升级到仲裁
					$data['ClaimTime']=$UpdateTime;
				}
			}
			if($Lifecycle!=$dispute_row['Lifecycle']){//争议阶段有改动
				$data['Lifecycle']=$Lifecycle;
			}
			if($Offer && $Offer['seller_offered_amount'] && $OfferData && !$OfferData['seller_offered_amount']){//顾客有返回建议结果
				$IsRefund=1;
				$RefundStatus=2;
				$data['Offer']=addslashes(str::json_data($Offer));
				foreach((array)$Links as $k=>$v){
					$v['rel']=='make_offer' && $RefundStatus=0;//有提议事件，说明顾客反对了提议
				}
			}
			db::update('orders_paypal_dispute', "DId='{$DId}'", $data);
			db::update('orders_paypal_webhooks_log', "LId='{$LogId}'", array('RelatedId'=>$DId));//webhooks记录争议ID
			if($IsRefund==1){//退款程序处理
				$refund_row=str::str_code(db::get_one('orders_paypal_dispute_refund', "DId='{$DId}' and Status=1"));
				if($refund_row){
					db::update('orders_paypal_dispute_refund', "RId='{$refund_row['RId']}'", array('Status'=>$RefundStatus));
				}
			}
			$Count=count($JSON['resource']['messages']);
			if($Count>1){//获取最新的对话
				$Count-=1;
				$dataTo=array(
					'DId'		=>	$DId,//争议ID
					'PostedBy'	=>	($JSON['resource']['messages'][$Count]['posted_by']=='BUYER'?1:0),	//发布者 BUYER:顾客 SELLER:商家
					'Content'	=>	addslashes($JSON['resource']['messages'][$Count]['content']),		//与通知事件相关的资源名称
					'Status'	=>	$RefundStatus,
					'AccTime'	=>	strtotime($JSON['resource']['messages'][$Count]['time_posted'])		//创建事件通知的日期和时间
				);
				$refund_row && $dataTh['RId']=$refund_row['RId'];
				db::insert('orders_paypal_dispute_message', $dataTo);
			}
			//----------------------------------------------- 顾客争议更新 End -----------------------------------------------
			break;
		case 'CUSTOMER.DISPUTE.RESOLVED':
			//----------------------------------------------- 顾客争议完成 Start -----------------------------------------------
			$dispute_row=db::get_one('orders_paypal_dispute', "DisputeID='{$JSON['resource']['dispute_id']}'");
			$DId=$dispute_row['DId'];
			$Status=$JSON['resource']['status'];//争议状态
			$Lifecycle=$JSON['resource']['dispute_life_cycle_stage'];//争议生命周期的阶段
			$UpdateTime=strtotime($JSON['resource']['update_time']);//争议最后更新的日期和时间
			$data=array(
				'UpdateTime'	=>	$UpdateTime,
				'ResolvedTime'	=>	$UpdateTime,
				'Status'		=>	$Status,
				'Lifecycle'		=>	$Lifecycle
			);
			db::update('orders_paypal_dispute', "DId='{$DId}'", $data);
			db::update('orders_paypal_webhooks_log', "LId='{$LogId}'", array('RelatedId'=>$DId));//webhooks记录争议ID
			//----------------------------------------------- 顾客争议完成 End -----------------------------------------------
			break;
	}
}

header("HTTP/1.1 200 OK");
exit;
?>