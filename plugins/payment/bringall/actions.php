<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  bringall(铂赢奥)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class bringall_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('bringall', $this, '__config'); 
        $pluginManager->register('bringall', $this, 'do_payment');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		$is_mobile=ly200::is_mobile_client(1);
		if($_POST){
			function array2String($arr){
			 if(is_null($arr) or !is_array($arr)) return false;
				 $str = '';
				 $arr_length = count($arr)-1;
				 foreach( $arr as $key => $value ){
					$str.=$key.'='.$value.'&';				
				}
				return urldecode($str);
				//必须使用urldecode()方法处理明文字符串
			}
			function get_type_card($data, $CardNo){	//多通道型,如果开启多通道,不同卡种使用不同通道,多通道直接数据库增加通道,默认单通道
				$result=array(	//默认单通道(国外通道)
					'merNo'	=>	$data['merNo'],
					'terNo'	=>	$data['terNo'],
					'hash'	=>	$data['hash'],
				);
				if($data['terNo1']){	//通道2(国内通道)
					$regV="/^4\d{15}$/";			//visa卡(维萨):4开头,共16位
					$regM1 = "/^5\d{15}$/";			//master卡(万事达):5或者222100-272099开头,共16位
					$regM2 = "/^2[2-7]\d{14}$/";
					$regJ = "/^35\d{14}$/";			//jcb卡:35开头,共16位
					$regAE = "/^3[47]\d{13}$/";		//american express(美国运通)卡34、37开头,共15位
					if(preg_match($regM1, $CardNo) || preg_match($regM2, $CardNo)){	//master卡走国内通道
						$result['terNo']=$data['terNo1'];
						$result['hash']=$data['hash1'];
					}
				}
				return $result;
			}
			$account=get_type_card($data['account'], $_POST['CardNo']);
			$parameter=array(
				'apiType'		=>	1,	//接口类型	1:普通接口 2:app sdk 3:快捷支付 4:虚拟
				'merremark'		=>	$data['order_row']['OId'],	//商户预留字段 哎没办法,非空
				'returnURL'		=>	"{$data['domain']}/cart/success/{$OId}.html",	//返回地址
				'merMgrURL'		=>	$data['domain'],	//来源网址
				'webInfo'		=>	$_SERVER['HTTP_USER_AGENT'],	//客户端浏览器信息
				'language'		=>	'en_US',	//语言
				'cardCountry'	=>	db::get_value('country', "CId='{$data['order_row']['BillCId']}'", 'Acronym'),	//账单国家
				'cardState'		=>	$data['order_row']['BillState'],	//账单州(省)
				'cardCity'		=>	$data['order_row']['BillCity'],	//账单城市
				'cardAddress'	=>	$data['order_row']['BillAddressLine1'],	//账单详细地址
				'cardZipCode'	=>	$data['order_row']['BillZipCode'],	//账单邮编				
				'cardFullName'	=>	$data['order_row']['BillFirstName'].'.'.$data['order_row']['BillLastName'],	//持卡人姓名
				'cardFullPhone'	=>	$data['order_row']['BillPhoneNumber'],	//持卡人电话
				'grCountry'		=>	db::get_value('country', "CId='{$data['order_row']['ShippingCId']}'", 'Acronym'),	//收货国家
				'grState'		=>	$data['order_row']['ShippingState'],	//收货州(省)
				'grCity'		=>	$data['order_row']['ShippingCity'],	//收货城市
				'grAddress'		=>	$data['order_row']['ShippingAddressLine1'],	//收货地址
				'grZipCode'		=>	$data['order_row']['ShippingZipCode'],	//收货地址邮编
				'grEmail'		=>	$data['order_row']['Email'],	//收货邮箱地址
				'grphoneNumber'	=>	$data['order_row']['ShippingPhoneNumber'],	//收货人电话
				'grPerName'		=>	$data['order_row']['ShippingFirstName'].'.'.$data['order_row']['ShippingLastName'],	//收货人姓名
				/********************************** 卡信息 *************************************/
				'cardNO'		=>	$_POST['CardNo'],	//卡号
				'expYear'		=>	$_POST['CardExpireYear'],	//有效年
				'expMonth'		=>	$_POST['CardExpireMonth'],	//信用卡有效月
				'cvv'			=>	$_POST['CardSecurityCode'],	//cvv
			);
			$goodsString=array();
			$order_products_list_row=db::get_all('orders_products_list', "OrderId='{$data['order_row']['OrderId']}'", '*', 'LId asc');
			foreach($order_products_list_row as $k=>$v){
				$goodsString['goodsInfo'][$k]=array('goodsName'=>$v['Name'], 'quantity'=>$v['Qty'], 'goodsPrice'=>sprintf('%01.2f', cart::iconv_price($v['Price'], 2, $orders_row['Currency'], 0)));
			}
			//货物信息
			$parameter['goodsString']=str::json_data($goodsString);
			$arrHashCode=array(
				'EncryptionMode'	=>	'SHA256',
				'CharacterSet'		=>	'UTF8',	//编码
				'merNo'				=>	$account['merNo'],	//商户号
				'terNo'				=>	$account['terNo'],	//终端号
				'orderNo'			=>	$data['order_row']['OId'],	//订单号
				'currencyCode'		=>	$data['order_row']['Currency'],	//交易币种
				'amount'			=>	$data['total_price'],	//支付金额,保留两位小数
				'payIP'				=>	ly200::get_ip(),	//客户的IP
				'transType'			=>	'sales',	//交易类型
				'transModel'		=>	'M',	//交易模式
			);
			
			$strHashCode=array2String($arrHashCode).$account['hash'];
			$arrHashCode['hashcode']=hash("sha256",$strHashCode);
			$strHashInfo=array2String($arrHashCode);
			$strBaseInfo=array2String($parameter);
			$post_data=$strBaseInfo.$strHashInfo;
			
			$curl = curl_init(); 
			curl_setopt($curl, CURLOPT_URL, 'https://payment.bringallpay.com/payment/api/payment');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($curl, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($curl, CURLOPT_TIMEOUT, 300);
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($curl);
			curl_close($curl);
			$output=str::json_data($output,'decode');
			
			$txt='';
			if($output['tradeNo']){
				$returnData=array(
					'transType' 	=> $output['transType'],
					'orderNo'		=> $output['orderNo'],
					'merNo'			=> $output['merNo'],
					'terNo'			=> $output['terNo'],
					'currencyCode'	=> $output['currencyCode'],
					'amount'		=> $output['amount'],
					'tradeNo' 		=> $output['tradeNo'],
					'respCode'		=> $output['respCode'],
					'respMsg'		=>$output['respMsg']
				);
				ksort($returnData);
				$hashcode=array2String($returnData).$account['hash'];
				$hashcode=hash("sha256",$hashcode);
				if($output['hashcode']==$hashcode){
					if($output['respCode']=='00'){
						$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
						orders::orders_payment_result(1, $UserName, $data['order_row'], '');
					}else{
						$txt=$output['respMsg'];
					}
				}else{
					$txt='hashcode error';
				}
			}else{
				$txt=$output['respMsg'];
			}
			
			ob_start();
			print_r($_GET);
			print_r($_POST);
			print_r($output);
			echo "\r\n\r\n$txt";
			$log=ob_get_contents();
			ob_end_clean();
			file::write_file('/_pay_log_/bringall/'.date('Y_m/', $c['time']), "{$data['order_row']['OId']}.txt", $log);	//把返回数据写入文件
			js::location("/cart/success/{$data['order_row']['OId']}.html", $txt, '.top');
		}else{
			$title='Bringall Payment';
			$year=@date('Y', $c['time']);
			include($c['root_path'].'/static/js/plugin/payment/CreditCard.php');
		}
    }
}
?>