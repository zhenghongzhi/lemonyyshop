<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  globebill - yandex (钱宝支付 globebill)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class globebill_yandex_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('globebill_yandex', $this, '__config'); 
        $pluginManager->register('globebill_yandex', $this, 'do_payment'); 
        $pluginManager->register('globebill_yandex', $this, 'returnUrl'); 
    }
	
	function __config($data){
		return @in_array($data, array('do_payment', 'returnUrl'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$is_mobile=ly200::is_mobile_client(1);
		$_CurrencyAry=array('CNY', 'USD', 'GBP', 'EUR', 'AUD', 'CAD', 'JPY', 'SGD', 'HKD', 'MYR', 'TWD', 'RUB', 'NZD', 'NOK', 'DKK', 'SEK', 'CHF', 'BRL', 'THB', 'KRW', 'TRY', 'ZAR', 'AED', 'MXN', 'CLP', 'BYR', 'MKD', 'CSD', 'PKR', 'UAH', 'IRR', 'TZS', 'AMD', 'ISK', 'AZN', 'ALL', 'BDT', 'SAR', 'GEL', 'PLN', 'BAM', 'BGN', 'HRK', 'HUF', 'LTL', 'LVL', 'RON', 'RSD', 'ILS', 'CZK', 'INR', 'PHP');
		@!in_array($data['order_row']['Currency'], $_CurrencyAry) && js::localtion('/', $c['lang_pack']['cart']['not_accept'], '.top');//不支持的货币
		!(int)$data['order_row']['BillCId'] && $data['order_row']['BillCId']=$data['order_row']['ShippingCId'];
		
		$Remark=$Method=ucfirst(@str_replace('globebill_', '', $data['method_path']));
		$Method=='Sofort' && $Method='Directpay';
		$Method=='Boleto' && $Method='Ebanx';
		$Method=='Credit_card' && $Method='Credit Card';
		
		if($Method=='Qiwi' && !$p_qiwiUsername && !$p_qiwiCountryCode){
			$title='Credit Card Payment';
			$country_code_ary=array('7', '91', '994', '82', '372', '375', '374', '44', '998', '972', '66', '90', '81', '1', '507', '77', '380', '371', '370', '996', '9955', '992', '373', '84');
			include($c['root_path'].'/static/js/plugin/payment/CreditCard_Qiwi.php');
		}else{
			$form_data=array( 
				'merNo'			=>	$data['account']['merNo'],
				'gatewayNo'		=>	$data['account']['gatewayNo'],
				'orderNo'		=>	$data['order_row']['OId'],
				'orderCurrency'	=>	$data['order_row']['Currency'],
				'orderAmount'	=>	$data['total_price'],
				'returnUrl'		=>	"{$data['domain']}/payment/globebill_yandex/returnUrl/{$data['order_row']['OId']}.html",
				
				'email'			=>	$data['order_row']['Email'],
				'firstName'		=>	$data['order_row']['BillFirstName']?$data['order_row']['BillFirstName']:$data['order_row']['ShippingFirstName'],
				'lastName'		=>	$data['order_row']['BillLastName']?$data['order_row']['BillLastName']:$data['order_row']['ShippingLastName'],
				'phone'			=>	$data['order_row']['BillPhoneNumber']?$data['order_row']['BillCountryCode'].$data['order_row']['BillPhoneNumber']:$data['order_row']['ShippingCountryCode'].$data['order_row']['ShippingPhoneNumber'],
				'country'		=>	db::get_value('country', "CId='{$data['order_row']['BillCId']}'", 'Acronym'),
				'state'			=>	$data['order_row']['BillState']?$data['order_row']['BillState']:$data['order_row']['ShippingState'],
				'city'			=>	$data['order_row']['BillCity']?$data['order_row']['BillCity']:$data['order_row']['ShippingCity'],
				'address'		=>	$data['order_row']['BillAddressLine1']?$data['order_row']['BillAddressLine1'].$data['order_row']['BillAddressLine2']:$data['order_row']['ShippingAddressLine1'].$data['order_row']['ShippingAddressLine2'],
				'zip'			=>	$data['order_row']['BillZipCode']?$data['order_row']['BillZipCode']:$data['order_row']['ShippingZipCode'],
				
				'paymentMethod'	=>	$Method,
				'interfaceInfo'	=>	'Ueeshop',
				'remark'		=>	$Remark.'||'
			);
			$form_data['state']=='' && $form_data['state']=$form_data['city'];
			$is_mobile && $form_data['isMobile']=1;
			
			if($form_data['paymentMethod']=='Qiwi'){//加上Qiwi需要的资料
				$form_data['qiwiUsername']=@trim($p_qiwiUsername);
				$form_data['qiwiCountryCode']=@trim($p_qiwiCountryCode);
			}elseif($form_data['paymentMethod']=='Ebanx'){//巴西本地支付 Boleto 加上巴西的身份证/营业执照号等信息
				//$form_data['ebanxcpf']=$order_row['BillTaxCode']?$order_row['BillTaxCode']:$order_row['ShippingTaxCode'];
				$form_data['ebanxName']=$form_data['firstName'].' '.$form_data['lastName'];
				$form_data['ebanxEmail']=$form_data['email'];
				$form_data['ebanxTypeCode']='boleto';	// boleto ,  _tef (网银转账),   directboleto (直连模式)
			}
			
			$signString=$form_data['merNo'].$form_data['gatewayNo'].$form_data['orderNo'].$form_data['orderCurrency'].$form_data['orderAmount'].$form_data['returnUrl'].$data['account']['signkey'];
			$form_data['signInfo']=@hash("sha256", $signString);
			
			echo '<form id="globebill_form" action="'.$data['form_action'].'" method="post">';
				foreach((array)$form_data as $key=>$value){
					echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
				}
				echo '<input type="submit" value="Submit" style="width:1px; height:1px; display:none;" />';
			echo '</form>';
			echo '<script language="javascript">document.getElementById("globebill_form").submit();</script>';
		}
    }
	
	function returnUrl($data){
		global $c;
		
		$data=$_POST?$_POST:$_GET;

		$OId=$data['orderNo']?$data['orderNo']:$_GET['OId'];//订单号
		if(!$data['signInfo'] && !(int)$data['orderStatus']){//支付出错
			$payment_result=$data['orderInfo'];
		}else{
			$PaymentMethod=@reset(@explode('||', $data['remark']));
			$method='Globebill'.$PaymentMethod;
			$account=str::json_data(db::get_value('payment', "Method='{$method}'", 'Attribute'), 'decode');
			$signString=$account['merNo'].$account['gatewayNo'].$data['tradeNo'].$data['orderNo'].$data['orderCurrency'].$data['orderAmount'].$data['orderStatus'].$data['orderInfo'].$account['signkey'];
			$signInfo=@hash("sha256", $signString);
		
			if(strtolower($data['signInfo'])==strtolower($signInfo)){ //验证数字签名
				$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
				!$order_row && exit();
				$total_price=cart::iconv_price(orders::orders_price($order_row), 2);
				$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
				
				if($data['orderStatus']==1){ //支付完成
					$payment_result=orders::orders_payment_result(1, $UserName, $order_row, $data['orderInfo']);
				}elseif($data['orderStatus']==-1 || $data['orderStatus']==-2){ //待处理、待确认
					$payment_result=orders::orders_payment_result(2, $UserName, $order_row, $data['orderInfo']);	
				}else{ //支付错误
					if($order_row['OrderStatus']<3){
						$payment_result=orders::orders_payment_result(0, $UserName, $order_row, $data['orderInfo']);
					}
					$payment_result='Payment wrong! '.$data['orderInfo'];
				}
			}else{ //验证失败
				$payment_result='Validation failure! '.$data['orderInfo'];
			}
		}
		
		ob_start();
		print_r($_GET);
		print_r($_POST);
		echo "\r\n\r\n$signInfo";
		echo "\r\n\r\n$payment_result";
		$pay_log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/globebill/'.$PaymentMethod.'/'.date('Y_m/', $c['time']), "{$OId}-".((int)$data['isPush']?'push-':'').rand(1,1000).".txt", $pay_log);	//把返回数据写入文件
		
		$jumpUrl="/cart/success/{$OId}.html";
		js::location($jumpUrl, $payment_result, '.top');
	}
}
?>