<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  glbpay 九盈支付(Glbpay)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class stripealipay_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('stripealipay', $this, '__config');
        $pluginManager->register('stripealipay', $this, 'do_payment');
		$pluginManager->register('stripealipay', $this, 'notify');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment','notify'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		$is_mobile=ly200::is_mobile_client(1);
		include('stripe_alipay.php');
    } 
	
	function notify(){
		$OId=$_GET['OId'];
		$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1,2,3)");
		$payment_row=db::get_one('payment',"PId='{$order_row['PId']}'");
		$account=str::json_data($payment_row['Attribute'], 'decode');
		$total_price=sprintf('%01.2f', orders::orders_price($order_row, 1));
		
		$json_data=array(
			'source'		=>	$_GET['source'],
			'currency'		=>	strtolower($order_row['Currency']),
			'amount'		=>	$total_price*100,
			'description'	=>	$order_row['OId'],
		);
		
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$account['Secret_key']}"));
		curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/charges');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($json_data));
		$output = curl_exec($ch);
		curl_close($ch);
		
		$output=str::json_data($output,'decode');
		$log='Payment failed';
		if($output['paid'] && $output['status']=='succeeded'){
			$log='payment successful';
			$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
			$order_row=db::get_one('orders', "OId='{$order_row['OId']}' and OrderStatus in(1,2,3)");
			orders::orders_payment_result(1, $UserName, $order_row, '');
		}else{
			$log.=':'.$output['error']['message'];
		}
		file::write_file('/_pay_log_/stripe_alipay/'.date('Y_m/', $c['time']), $order_row['OId'].".txt", print_r($json_data,true).print_r($output,true));	//把返回数据写入文件
		js::location("/cart/success/{$order_row['OId']}.html", $log, '.top');
	}
}
?>