<?php $pay_data=$data; //转换一下，防止其他地方已经调用这一变量?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Stripe</title>
<script src="https://js.stripe.com/v3/"></script>
<script>
var stripe = Stripe('<?=$pay_data['account']['Publishable_key']?>');
stripe.createSource({
  type: 'alipay',
  amount: '<?=$data['total_price']*100?>',
  currency: '<?=strtolower($data['order_row']['Currency'])?>',
  redirect: {
    return_url: '<?=$pay_data['domain'].'/payment/stripealipay/notify/'.$pay_data['order_row']['OId'].'.html'?>',
  },
}).then(function(result) {
	//console.log(result);
	if(result.source){	//正常返回
		window.location.href=result.source.redirect.url;
	}else if(result.error){  //错误信息
		alert(result.error.message);
	}else{	//出错
		alert('Failure to pay, an unknown error, Please contact the administrator!');
	}
});
</script>
</head>
<body>
</body>
</html>
