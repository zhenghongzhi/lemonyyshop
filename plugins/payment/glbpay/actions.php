<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  glbpay 九盈支付(Glbpay)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class glbpay_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('glbpay', $this, '__config');
        $pluginManager->register('glbpay', $this, 'do_payment');
        $pluginManager->register('glbpay', $this, 'notify');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment', 'notify'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		$is_mobile=ly200::is_mobile_client(1);
		
		//支持货币：CNY(人民币)、USD(美元)、GBP(英镑)、EUR(欧元)、HKD(港币)、AUD(澳元)、CAD(加元)、CHF(瑞士法郎)、DKK(丹麦克朗)、SEK(瑞典克郞)、NOK(挪威克朗)
		//$glb_currency=array('CNY', 'USD', 'GBP', 'EUR', 'HKD', 'AUD', 'CAD', 'CHF', 'DKK', 'SEK', 'NOK');
		//支持货币：CNY(人民币)、USD(美元)、GBP(英镑)、EUR(欧元)、HKD(港币)、AUD(澳元)、CAD(加元)、CHF(瑞士法郎)、DKK(丹麦克朗)、SEK(瑞典克郞)、NOK(挪威克朗)、SAR(里亚尔)、AED(迪拉姆)
		$glb_currency=array('CNY', 'USD', 'GBP', 'EUR', 'HKD', 'AUD', 'CAD', 'CHF', 'DKK', 'SEK', 'NOK', 'SAR', 'AED');
		@!in_array($data['order_row']['Currency'], $glb_currency) && js::location('/', $c['lang_pack']['cart']['not_accept'], '.top');//不支持的货币
		!in_array($data['order_row']['OrderStatus'], array(1,3)) && js::location("/account/orders/view{$data['order_row']['OId']}.html");
		
		$year=@date('Y', $c['time']);
		
		if($_POST){
			@extract($_POST, EXTR_PREFIX_ALL, 'p');
			$CountryArray = array(
				'AD'	=>	'AND',
				'AE'	=>	'ARE',
				'AF'	=>	'AFG',
				'AG'	=>	'ATG',
				'AI'	=>	'AIA',
				'AL'	=>	'ALB',
				'AM'	=>	'ARM',
				'AO'	=>	'AGO',
				'AQ'	=>	'ATA',
				'AR'	=>	'ARG',
				'AS'	=>	'ASM',
				'AT'	=>	'AUT',
				'AU'	=>	'AUS',
				'AW'	=>	'ABW',
				'AX'	=>	'ALA',
				'AZ'	=>	'AZE',
				'BA'	=>	'BIH',
				'BB'	=>	'BRB',
				'BD'	=>	'BGD',
				'BE'	=>	'BEL',
				'BF'	=>	'BFA',
				'BG'	=>	'BGR',
				'BH'	=>	'BHR',
				'BI'	=>	'BDI',
				'BJ'	=>	'BEN',
				'BL'	=>	'BLM',
				'BM'	=>	'BMU',
				'BN'	=>	'BRN',
				'BO'	=>	'BOL',
				'BQ'	=>	'BES',
				'BR'	=>	'BRA',
				'BS'	=>	'BHS',
				'BT'	=>	'BTN',
				'BV'	=>	'BVT',
				'BW'	=>	'BWA',
				'BY'	=>	'BLR',
				'BZ'	=>	'BLZ',
				'CA'	=>	'CAN',
				'CC'	=>	'CCK',
				'CF'	=>	'CAF',
				'CH'	=>	'CHE',
				'CL'	=>	'CHL',
				'CM'	=>	'CMR',
				'CO'	=>	'COL',
				'CR'	=>	'CRI',
				'CU'	=>	'CUB',
				'CV'	=>	'CPV',
				'CX'	=>	'CXR',
				'CY'	=>	'CYP',
				'CZ'	=>	'CZE',
				'DE'	=>	'DEU',
				'DJ'	=>	'DJI',
				'DK'	=>	'DNK',
				'DM'	=>	'DMA',
				'DO'	=>	'DOM',
				'DZ'	=>	'DZA',
				'EC'	=>	'ECU',
				'EE'	=>	'EST',
				'EG'	=>	'EGY',
				'EH'	=>	'ESH',
				'ER'	=>	'ERI',
				'ES'	=>	'ESP',
				'FI'	=>	'FIN',
				'FJ'	=>	'FJI',
				'FK'	=>	'FLK',
				'FM'	=>	'FSM',
				'FO'	=>	'FRO',
				'FR'	=>	'FRA',
				'GA'	=>	'GAB',
				'GD'	=>	'GRD',
				'GE'	=>	'GEO',
				'GF'	=>	'GUF',
				'GH'	=>	'GHA',
				'GI'	=>	'GIB',
				'GL'	=>	'GRL',
				'GN'	=>	'GIN',
				'GP'	=>	'GLP',
				'GQ'	=>	'GNQ',
				'GR'	=>	'GRC',
				'GS'	=>	'SGS',
				'GT'	=>	'GTM',
				'GU'	=>	'GUM',
				'GW'	=>	'GNB',
				'GY'	=>	'GUY',
				'HK'	=>	'HKG',
				'HM'	=>	'HMD',
				'HN'	=>	'HND',
				'HR'	=>	'HRV',
				'HT'	=>	'HTI',
				'HU'	=>	'HUN',
				'ID'	=>	'IDN',
				'IE'	=>	'IRL',
				'IL'	=>	'ISR',
				'IM'	=>	'IMN',
				'IN'	=>	'IND',
				'IO'	=>	'IOT',
				'IQ'	=>	'IRQ',
				'IR'	=>	'IRN',
				'IS'	=>	'ISL',
				'IT'	=>	'ITA',
				'JE'	=>	'JEY',
				'JM'	=>	'JAM',
				'JO'	=>	'JOR',
				'JP'	=>	'JPN',
				'KH'	=>	'KHM',
				'KI'	=>	'KIR',
				'KM'	=>	'COM',
				'KW'	=>	'KWT',
				'KY'	=>	'CYM',
				'LB'	=>	'LBN',
				'LI'	=>	'LIE',
				'LK'	=>	'LKA',
				'LR'	=>	'LBR',
				'LS'	=>	'LSO',
				'LT'	=>	'LTU',
				'LU'	=>	'LUX',
				'LV'	=>	'LVA',
				'LY'	=>	'LBY',
				'MA'	=>	'MAR',
				'MC'	=>	'MCO',
				'MD'	=>	'MDA',
				'ME'	=>	'MNE',
				'MF'	=>	'MAF',
				'MG'	=>	'MDG',
				'MH'	=>	'MHL',
				'MK'	=>	'MKD',
				'ML'	=>	'MLI',
				'MM'	=>	'MMR',
				'MO'	=>	'MAC',
				'MQ'	=>	'MTQ',
				'MR'	=>	'MRT',
				'MS'	=>	'MSR',
				'MT'	=>	'MLT',
				'MV'	=>	'MDV',
				'MW'	=>	'MWI',
				'MX'	=>	'MEX',
				'MY'	=>	'MYS',
				'NA'	=>	'NAM',
				'NE'	=>	'NER',
				'NF'	=>	'NFK',
				'NG'	=>	'NGA',
				'NI'	=>	'NIC',
				'NL'	=>	'NLD',
				'NO'	=>	'NOR',
				'NP'	=>	'NPL',
				'NR'	=>	'NRU',
				'OM'	=>	'OMN',
				'PA'	=>	'PAN',
				'PE'	=>	'PER',
				'PF'	=>	'PYF',
				'PG'	=>	'PNG',
				'PH'	=>	'PHL',
				'PK'	=>	'PAK',
				'PL'	=>	'POL',
				'PN'	=>	'PCN',
				'PR'	=>	'PRI',
				'PS'	=>	'PSE',
				'PW'	=>	'PLW',
				'PY'	=>	'PRY',
				'QA'	=>	'QAT',
				'RE'	=>	'REU',
				'RO'	=>	'ROU',
				'RS'	=>	'SRB',
				'RU'	=>	'RUS',
				'RW'	=>	'RWA',
				'SB'	=>	'SLB',
				'SC'	=>	'SYC',
				'SD'	=>	'SDN',
				'SE'	=>	'SWE',
				'SG'	=>	'SGP',
				'SI'	=>	'SVN',
				'SJ'	=>	'SJM',
				'SK'	=>	'SVK',
				'SL'	=>	'SLE',
				'SM'	=>	'SMR',
				'SN'	=>	'SEN',
				'SO'	=>	'SOM',
				'SR'	=>	'SUR',
				'SS'	=>	'SSD',
				'ST'	=>	'STP',
				'SV'	=>	'SLV',
				'SY'	=>	'SYR',
				'SZ'	=>	'SWZ',
				'TC'	=>	'TCA',
				'TD'	=>	'TCD',
				'TG'	=>	'TGO',
				'TH'	=>	'THA',
				'TK'	=>	'TKL',
				'TL'	=>	'TLS',
				'TN'	=>	'TUN',
				'TO'	=>	'TON',
				'TR'	=>	'TUR',
				'TV'	=>	'TUV',
				'TZ'	=>	'TZA',
				'UA'	=>	'UKR',
				'UG'	=>	'UGA',
				'US'	=>	'USA',
				'UY'	=>	'URY',
				'VA'	=>	'VAT',
				'VE'	=>	'VEN',
				'VG'	=>	'VGB',
				'VI'	=>	'VIR',
				'VN'	=>	'VNM',
				'WF'	=>	'WLF',
				'WS'	=>	'WSM',
				'YE'	=>	'YEM',
				'YT'	=>	'MYT',
				'ZA'	=>	'ZAF',
				'ZM'	=>	'ZMB',
				'ZW'	=>	'ZWE',
				'CN'	=>	'CHN',
				'CG'	=>	'COG',
				'CD'	=>	'COD',
				'MZ'	=>	'MOZ',
				'GG'	=>	'GGY',
				'GM'	=>	'GMB',
				'MP'	=>	'MNP',
				'ET'	=>	'ETH',
				'NC'	=>	'NCL',
				'VU'	=>	'VUT',
				'TF'	=>	'ATF',
				'NU'	=>	'NIU',
				'UM'	=>	'UMI',
				'CK'	=>	'COK',
				'GB'	=>	'GBR',
				'TT'	=>	'TTO',
				'VC'	=>	'VCT',
				'TW'	=>	'TWN',
				'NZ'	=>	'NZL',
				'SA'	=>	'SAU',
				'LA'	=>	'LAO',
				'KP'	=>	'PRK',
				'KR'	=>	'KOR',
				'PT'	=>	'PRT',
				'KG'	=>	'KGZ',
				'KZ'	=>	'KAZ',
				'TJ'	=>	'TJK',
				'TM'	=>	'TKM',
				'UZ'	=>	'UZB',
				'KN'	=>	'KNA',
				'PM'	=>	'SPM',
				'SH'	=>	'SHN',
				'LC'	=>	'LCA',
				'MU'	=>	'MUS',
				'CI'	=>	'CIV',
				'KE'	=>	'KEN',
				'MN'	=>	'MNG'
			);
			
			$monthArr=array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
			$yearArr=array();
			for($i=0;$i<10;$i++){
				$yearArr[]=$year+$i;
			}
			
			(!$p_CardNo || !is_numeric($p_CardNo) || @strlen($p_CardNo)!=16 || !@in_array($p_CardExpireMonth, $monthArr) || !@in_array($p_CardExpireYear, $yearArr) || !$p_CardSecurityCode || !is_numeric($p_CardSecurityCode) || @strlen($p_CardSecurityCode)!=3 ) && js::back();
		
			$CId=(int)$data['order_row']['BillCId']?(int)$data['order_row']['BillCId']:(int)$data['order_row']['ShippingCId'];
			$Acronym=db::get_value('country', "CId='{$CId}'", 'Acronym');
			$ShippingAcronym=$Acronym;
			$CId!=(int)$data['order_row']['ShippingCId'] && $ShippingAcronym=db::get_value('country', "CId='{$data['order_row']['ShippingCId']}'", 'Acronym');
			
			$form_data=array( 
				//***************************支付基本信息部分***************************
				'version'			=>	'1.1.2',	//接口版本号
				'txntype'			=>	'sale',
				'merchantid'		=>	$data['account']['mechantid'],	//商户号		
				'orderid'			=>	$data['order_row']['OId'],	//订单号
				'orderdate'			=>	@date('YmdHis', $data['order_row']['OrderTime']),	//订单时间
				'ordercurrency'		=>	strtoupper($data['order_row']['Currency']),//$currency_id,,	//交易币种
				'orderamount'		=>	$data['total_price'],	//支付金额
				//信用卡信息
				'cardnumber'		=>	$p_CardNo,	//卡号	41111111111111111
				'expyear'			=>	$p_CardExpireYear,	//卡有效期年	2020
				'expmonth'			=>	$p_CardExpireMonth,	//卡有效期月	11
				'cvv'				=>	$p_CardSecurityCode,	//CVV2	123
				'issuebank'			=>	$p_IssuingBank,	//发卡行
				
				//***************************付款人信息部分***************************
				'deliveryfirstname'	=>	$data['order_row']['ShippingFirstName'],	//收货人姓
				'deliverylastname'	=>	$data['order_row']['ShippingLastName'],	//收货人名
				'deliveryaddress'	=>	$data['order_row']['ShippingAddressLine1'],	//收货人地址
				'deliverycountry'	=>	$CountryArray[$ShippingAcronym],//收货人国家
				'deliverystate'		=>	$data['order_row']['ShippingState'],	//收货人省份
				'deliverycity'		=>	$data['order_row']['ShippingCity'],	//收货人城市
				'deliveryemail'		=>	$data['order_row']['Email'],	//收货人电邮
				'deliveryphone'		=>	$data['order_row']['ShippingPhoneNumber'],	//收货人电话
				'deliverypost'		=>	$data['order_row']['ShippingZipCode'],	//收货人邮编
		
				'billingfirstname'	=>	$data['order_row']['BillFirstName']?$data['order_row']['BillFirstName']:$data['order_row']['ShippingFirstName'],	//收货人姓
				'billinglastname'	=>	$data['order_row']['BillLastName']?$data['order_row']['BillLastName']:$data['order_row']['ShippingLastName'],	//收货人名
				'billingaddress'	=>	$data['order_row']['BillAddressLine1']?$data['order_row']['BillAddressLine1']:$data['order_row']['ShippingAddressLine1'],	//收货人地址
				'billingcountry'	=>	$CountryArray[$Acronym],//收货人国家
				'billingstate'		=>	$data['order_row']['BillState']?$data['order_row']['BillState']:$data['order_row']['ShippingState'],	//收货人省份
				'billingcity'		=>	$data['order_row']['BillCity']?$data['order_row']['BillCity']:$data['order_row']['ShippingCity'],	//收货人城市
				'billingemail'		=>	$data['order_row']['Email'],	//收货人电邮
				'billingphone'		=>	$data['order_row']['BillPhoneNumber']?$data['order_row']['BillPhoneNumber']:$data['order_row']['ShippingPhoneNumber'],	//收货人电话
				'billingpost'		=>	$data['order_row']['BillZipCode']?$data['order_row']['BillZipCode']:$data['order_row']['ShippingZipCode'],	//收货人邮编
				//***************************付款人信息部分***************************
				
				'clientip'			=>	ly200::get_ip(),
				'storetype'			=>	'UEESHOP',
				'accessurl'			=>	"{$data['domain']}?utm_nooverride=1",	//回调地址		/payment/glbpay/notify/
			);
			
			//签名的表单字段名
			$signature=$data['account']['hashkey'];
			$signature_ary=array('accessurl', 'billingaddress', 'billingcity', 'billingcountry', 'billingemail', 'billingfirstname', 'billinglastname', 'billingphone', 'billingpost', 'billingstate', 'cardnumber', 'clientip', 'cvv', 'dcctxnid', 'deliveryaddress', 'deliverycity', 'deliverycountry', 'deliveryemail', 'deliveryfirstname', 'deliverylastname', 'deliveryphone', 'deliverypost', 'deliverystate', 'expmonth', 'expyear', 'issuebank', 'merchantid', 'orderamount', 'ordercurrency', 'orderdate', 'orderid', 'storetype', 'txntype', 'version');
			
			sort($signature_ary);
			foreach($signature_ary as $v){ $signature.=$form_data[$v]; }
			$form_data['signature']=@md5($signature);
			$json_data=str::json_data($form_data);
			
			$payUrl='https://pgw.glbpay.com/api/bgpay';
			$ch = curl_init($payUrl);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($json_data))
			);
			$result=str::json_data(@curl_exec($ch), 'decode');
			
			// 校验源字符串
			$signreturn_ary=array('version', 'merchantid', 'orderid', 'orderdate', 'ordercurrency', 'orderamount', 'cardnumber', 'remark1', 'remark2', 'remark3', 'txnid', 'txndate', 'status', 'respcode', 'respmsg');
			$md5src=$data['account']['hashkey'];
			sort($signreturn_ary);
			foreach($signreturn_ary as $v){ $md5src.=$result[$v]; }
			$md5sign=md5($md5src);
			
			$OId=$data['order_row']['OId'];
			//$responseLog="Your payment operation failed! Reasons for failure: {$result['respmsg']}";		
			$jumpUrl="/cart/success/{$OId}.html";
			
			if($result['signature']!='' && strtoupper($result['signature'])==strtoupper($md5sign)){	//检验成功
				$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
				if((int)$result['status']==1){ //更新订单状态为支付成功
					$error=orders::orders_payment_result(1, $UserName, $data['order_row'], $result['respmsg']);
				}else{ //更新订单状态为其他状态
					$error=orders::orders_payment_result(0, $UserName, $data['order_row'], $result['respmsg']);
				}
			}
			
			ob_start();
			echo "\r\n\r\n POST Data";
			print_r($form_data);
			echo "\r\n\r\n Return Data";
			print_r($result);
			echo "\r\n\r\n md5sign: $md5sign";
			echo "\r\n\r\n signature: ".$result['signature'];
			echo "\r\n\r\n response log: ".$result['respmsg'];
			echo "\r\n\r\n $error";
			$log=ob_get_contents();
			ob_end_clean();
			
			file::write_file('/_pay_log_/glbpay/'.date('Y_m/', $c['time']), $OId.'-'.mt_rand(10, 99).".txt", $log);	//把返回数据写入文件
			js::location($jumpUrl, $Log);	
		}else{
			$title='Credit Card Payment';
			$IssuingBank=1;
			include($c['root_path'].'/static/js/plugin/payment/CreditCard.php');
		}
    } 
	
	function notify($data){
		global $c;
		
		$account=str::json_data(db::get_value('payment', "Method='Glbpay'", 'Attribute'), 'decode');
		
		$data_ary=array(
			//***************************支付基本信息***************************
			'version'			=>	$_POST['version'],	//接口版本号
			'encoding'			=>	$_POST['encoding'],	//字符集
			'language'			=>	$_POST['language'],	//界面语言
			'merchantid'		=>	$_POST['merchantid'],	//商户号
			'transtype'			=>	$_POST['transtype'],	//交易类型
			'orderid'			=>	$_POST['orderid'],	//订单号
			'orderdate'			=>	$_POST['orderdate'],	//订单时间
			'currency'			=>	$_POST['currency'],	//交易币种
			'orderamount'		=>	$_POST['orderamount'],	//支付金额
			'callbackurl'		=>	$_POST['callbackurl'],
			'remark1'			=>	$_POST['remark1'],
			'remark2'			=>	$_POST['remark2'],
			'remark3'			=>	$_POST['remark3'],
			
			//***************************交易结果***************************
			'paycurrency'		=>	$_POST['paycurrency'],	//实际支付币种
			'payamount'			=>	$_POST['payamount'],	//实际支付金额
			'transid'			=>	$_POST['transid'],	//系统流水号
			'transdate'			=>	$_POST['transdate'],	//系统时间
			'status'			=>	$_POST['status'],	//支付结果(Y:支付成功 ,N:支付失败)
			'message'			=>	$_POST['message'],	//支付信息
			'signature'			=>	$_POST['signature']	//数字签名
		);
		
		$hashkey=$account['hashkey'];//商户的密钥
		
		//签名的表单字段名
		$signature='';
		$signature_ary=array('version','encoding','language','merchantid','transtype','orderid','orderdate','currency','orderamount','paycurrency','payamount','remark1','remark2','remark3','transid','transdate','status');
		foreach($signature_ary as $key){
			$signature.=$form_data[$key];
		}
		$signature=$hashkey.$signature;//密钥放最前面
		$signature_string=@md5($signature);//md5算法
		
		if(strtolower($signature_string)==strtolower($data_ary['signature'])){//验证数字签名
			$OId=$data_ary['orderid'];
			$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
			!$order_row && js::location('/');
			$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
			
			if($data_ary['status']=='Y'){//支付成功
				$payment_result=orders::orders_payment_result(1, $UserName, $order_row, $_POST['respmsg']);
			}else{//支付失败
				$payment_result=orders::orders_payment_result(0, $UserName, $order_row, $_POST['respmsg']);
			}
		}else{//验证失败
			$payment_result='Validation failure!';
		}
		
		ob_start();
		print_r($_GET);
		print_r($_POST);
		echo "\r\n\r\n$signature_string";
		echo "\r\n\r\n$payment_result";
		$log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/glbpay/'.date('Y_m/', $c['time']), "{$OId}-".rand(0, 1000).".txt", $log);	//把返回数据写入文件
	}
}
?>