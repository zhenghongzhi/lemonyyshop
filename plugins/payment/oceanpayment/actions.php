<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  oceanpayment
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class oceanpayment_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('oceanpayment', $this, '__config'); 
        $pluginManager->register('oceanpayment', $this, 'do_payment'); 
        $pluginManager->register('oceanpayment', $this, 'notify'); 
        $pluginManager->register('oceanpayment', $this, 'returnUrl'); 
    }
	
	function __config($data){
		return @in_array($data, array('do_payment', 'notify', 'returnUrl'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		
		$billPhone=$data['order_row']['BillPhoneNumber']?$data['order_row']['BillCountryCode'].$data['order_row']['BillPhoneNumber']:'N/A';
		$billAcronym=db::get_value('country', "CId='{$data['order_row']['BillCId']}'", 'Acronym');
		$billAddress=$data['order_row']['BillAddressLine1'].' '.$data['order_row']['BillAddressLine2'];
		$shipAcronym=db::get_value('country', "CId='{$data['order_row']['ShippingCId']}'", 'Acronym');
		
		// $data['account']['account']='150260'; //用正式域名测试
		// $data['account']['terminal']='15026007';
		// $data['account']['secureCode']='N08Zp6L4l8224B40rZ42jDBt0fTl2Z4N6hz4j884Vp286t46J20j44v000448200';
		
		// $data['account']['account']='191292'; //用测试域名测试
		// $data['account']['terminal']='19129202';
		// $data['account']['secureCode']='48r08n642htfL6r4X466NNJB2v86J0d648z60VF602fbj84P4682b42v64ptpt84';
		
		$form_data = array(
			'account'		=>	$data['account']['account'],
			'terminal'		=>	$data['account']['terminal'],
			
			'backUrl'		=>	"{$data['domain']}/payment/oceanpayment/returnUrl/{$data['order_row']['OId']}.html",
			'noticeUrl'		=>	"{$data['domain']}/payment/oceanpayment/notify/{$data['order_row']['OId']}.html",//收到服务器回调后需响应 receive-ok
			
			'methods'		=>	'Credit Card',//	Credit Card,	Yandex
			//'pages'			=>	0,//0: PC 端页面 (默认);		1: 手机端页面
			
			'order_number'	=>	$data['order_row']['OId'],
			'order_currency'=>	$data['order_row']['Currency'],
			'order_amount'	=>	$data['total_price'],
			'order_notes'	=>	'',//订单备注
			
			//持卡人信息/账单信息
			'billing_firstName'	=>	trim($data['order_row']['BillFirstName']),	//名
			'billing_lastName'	=>	trim($data['order_row']['BillLastName']),	//姓
			'billing_email'		=>	trim($data['order_row']['Email']),	//邮箱
			'billing_phone'		=>	trim($billPhone),	//电话
			'billing_country'	=>	trim($billAcronym)?trim($billAcronym):'N/A',//$data['order_row']['ShippingCountry'],	//国家
			'billing_state'		=>	trim($data['order_row']['BillState'])?trim($data['order_row']['BillState']):'N/A',	//省份/州
			'billing_city'		=>	trim($data['order_row']['BillCity'])?trim($data['order_row']['BillCity']):'N/A',	//城市
			'billing_address'	=>	trim($billAddress)?trim($billAddress):'N/A',	//联系地址
			'billing_zip'		=>	trim($data['order_row']['BillZipCode'])?trim($data['order_row']['BillZipCode']):'N/A',	//邮政编码
			//收货信息
			'ship_firstName'	=>	trim($data['order_row']['ShippingFirstName']),	//收货人名
			'ship_lastName'		=>	trim($data['order_row']['ShippingLastName']),	//收货人姓
			'ship_phone'		=>	trim($data['order_row']['ShippingCountryCode'].$data['order_row']['ShippingPhoneNumber']),	//收货人电话
			'ship_country'		=>	$shipAcronym?$shipAcronym:'N/A',//$data['order_row']['ShippingCountry'],	//收货国家
			'ship_state'		=>	trim($data['order_row']['ShippingState']?$data['order_row']['ShippingState']:'N/A'),	//收货省份/州
			'ship_city'			=>	trim($data['order_row']['ShippingCity']),	//收货城市
			'ship_addr'			=>	trim($data['order_row']['ShippingAddressLine1']),	//详细地址
			'ship_zip'			=>	trim($data['order_row']['ShippingZipCode']),	//收货邮编
			
			'logoUrl'		=>	$data['domain'].$c['config']['global']['LogoPath']
		);
		//使用 SHA256 加密，明文加密结构：account+terminal+backUrl+order_number+order_currency+order_amount+billing_firstName+billing_lastName+billing_email+secureCode
		$form_data['signValue']=hash('sha256', $form_data['account'].$form_data['terminal'].$form_data['backUrl'].$form_data['order_number'].$form_data['order_currency'].$form_data['order_amount'].$form_data['billing_firstName'].$form_data['billing_lastName'].$form_data['billing_email'].$data['account']['secureCode']);
		
		// $PayUrl='https://secure.oceanpayment.com/gateway/service/test';//测试环境
		$PayUrl='https://secure.oceanpayment.com/gateway/service/pay';//正式环境
		echo '<form id="oceanpayment_form" action="'.$PayUrl.'" method="post">';
		
		foreach((array)$form_data as $key=>$value){
			echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
		}
		
		echo '<input type="submit" value="Submit" style="width:1px; height:1px; display:none;" /></form><script language="javascript">document.getElementById("oceanpayment_form").submit();</script>';/**/
		exit;
    } 
     
    function returnUrl($data){
		global $c;
		
		$account=str::json_data(db::get_value('payment', "Method='OceanPayment'", 'Attribute'), 'decode');
		//account+terminal+order_number+order_currency+order_amount+order_notes+card_number+payment_id+payment_authType+payment_status+payment_details+payment_risk+secureCode
		$sha256String=strtoupper(hash('sha256', $_REQUEST['account'].$_REQUEST['terminal'].$_REQUEST['order_number'].$_REQUEST['order_currency'].$_REQUEST['order_amount'].$_REQUEST['order_notes'].$_REQUEST['card_number'].$_REQUEST['payment_id'].$_REQUEST['payment_authType'].$_REQUEST['payment_status'].$_REQUEST['payment_details'].$_REQUEST['payment_risk'].$account['secureCode']));
		
		$OId=$_GET['OId'];
		$jumpUrl="/account/orders/view{$OId}.html";
		$tips=@explode(':', $_REQUEST['payment_details']);
		if(strtoupper($_REQUEST['signValue'])==$sha256String){//检验通过
			$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1, 3)");
			
			if($order_row){
				if($_REQUEST['payment_status']==1){	//检查状态
					$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][4];
					db::update('orders', "OId='$OId'", array('OrderStatus'=>4));
					
					orders::orders_log((int)$_SESSION['User']['UserId'], ((int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist'), $order_row['OrderId'], 4, $Log);
					orders::orders_products_update(4, $order_row);
					
					$ToAry=array($order_row['Email']);
					$c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
					include($c['static_path'].'/inc/mail/order_payment.php');
					ly200::sendmail($ToAry, "We have received from your payment for order#".$OId, $mail_contents);
					orders::orders_sms($OId);
					
					$jumpUrl="/cart/success/{$OId}.html";
					$payment_result='Payment success!';
				}elseif($_REQUEST['payment_status']==-1){
					if($_REQUEST['payment_authType']==1){
						db::update('orders', "OId='$OId'", array('OrderStatus'=>2));
				
						$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][2].'<br />'.$tips[1];
						orders::orders_log((int)$_SESSION['User']['UserId'], $UserName, $order_row['OrderId'], 2, $Log);
						$jumpUrl="/cart/success/{$OId}.html";
						$payment_result="Payment processed!";
					}else{
						db::update('orders', "OId='$OId'", array('OrderStatus'=>3));
						$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][3].'<br />'.$tips[1];
						orders::orders_log((int)$_SESSION['User']['UserId'], $UserName, $order_row['OrderId'], 3, $Log);
						$payment_result="Payment Wrong!";
					}
				}else{
					db::update('orders', "OId='$OId'", array('OrderStatus'=>3));
					$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][3].'<br />'.$tips[1];
					orders::orders_log((int)$_SESSION['User']['UserId'], $UserName, $order_row['OrderId'], 3, $Log);
					$payment_result="Payment Wrong!";
				}
			}else{
				$payment_result='Illegal request!';
			}
		}else{
			$payment_result='Verification failed!';
		}
				
		ob_start();
		print_r($_GET);
		print_r($_REQUEST);
		echo "\r\n\r\nPayment Details: {$_REQUEST['payment_details']}";
		echo "\r\n\r\nsignValue: {$_REQUEST['signValue']}";
		echo "\r\n\r\nsha256String: $sha256String";
		echo "\r\n\r\n$payment_result";
		$log=ob_get_contents();
		ob_end_clean();
		$ext=rand(0,9).rand(0,9);
		file::write_file('/_pay_log_/oceanpayment/'.date('Y_m/', $c['time']), "return-{$OId}-{$ext}.txt", $log);	//把返回数据写入文件
		
		js::location($jumpUrl, $tips[1]);
    }
	
	function notify($data){
		global $c;
		$account = str::json_data(db::get_value('payment', "Method='OceanPayment'", 'Attribute'), 'decode');
		$OId=$_GET['OId'];

		/**
		*  判断是否为xml
		*
		*/
		function xml_parser($str){
			$xml_parser = xml_parser_create();
			if(!xml_parse($xml_parser,$str,true)){
				xml_parser_free($xml_parser);
				return false;
			}else {
				return true;
			}
		}
		//获取推送输入流XML
		$xml_str = file_get_contents("php://input");

		//判断返回的输入流是否为xml
		if(xml_parser($xml_str)){
			$xml = simplexml_load_string($xml_str);
			//把推送参数赋值到$_REQUEST
			$_REQUEST['response_type']	  = (string)$xml->response_type;
			$_REQUEST['account']		  = (string)$xml->account;
			$_REQUEST['terminal'] 	      = (string)$xml->terminal;
			$_REQUEST['payment_id'] 	  = (string)$xml->payment_id;
			$_REQUEST['order_number']     = (string)$xml->order_number;
			$_REQUEST['order_currency']   = (string)$xml->order_currency;
			$_REQUEST['order_amount']     = (string)$xml->order_amount;
			$_REQUEST['payment_status']   = (string)$xml->payment_status;
			$_REQUEST['payment_details']  = (string)$xml->payment_details;
			$_REQUEST['signValue'] 	      = (string)$xml->signValue;
			$_REQUEST['order_notes']	  = (string)$xml->order_notes;
			$_REQUEST['card_number']	  = (string)$xml->card_number;
			$_REQUEST['payment_authType'] = (string)$xml->payment_authType;
			$_REQUEST['payment_risk'] 	  = (string)$xml->payment_risk;
			$_REQUEST['methods'] 	  	  = (string)$xml->methods;
			$_REQUEST['payment_country']  = (string)$xml->payment_country;
			$_REQUEST['payment_solutions']= (string)$xml->payment_solutions;
			
			//获取本地的code值
			$secureCode = $account['secureCode'];
			$local_signValue  = hash("sha256",$_REQUEST['account'].$_REQUEST['terminal'].$_REQUEST['order_number'].$_REQUEST['order_currency'].$_REQUEST['order_amount'].$_REQUEST['order_notes'].$_REQUEST['card_number'].$_REQUEST['payment_id'].$_REQUEST['payment_authType'].$_REQUEST['payment_status'].$_REQUEST['payment_details'].$_REQUEST['payment_risk'].$secureCode);
			//加密串校验
			if (strtolower($local_signValue) == strtolower($_REQUEST['signValue'])) {
				$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1,2,3)");
				if($order_row){
					if ($_REQUEST['payment_status'] == 1) {
					//支付成功
						$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][4];
						db::update('orders', "OId='$OId'", array('OrderStatus'=>4));
						
						orders::orders_log((int)$_SESSION['User']['UserId'], ((int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist'), $order_row['OrderId'], 4, $Log);
						orders::orders_products_update(4, $order_row);
						
						$ToAry=array($order_row['Email']);
						$c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
						include($c['static_path'].'/inc/mail/order_payment.php');
						ly200::sendmail($ToAry, "We have received from your payment for order#".$OId, $mail_contents);
						orders::orders_sms($OId);
						
						$jumpUrl="/cart/success/{$OId}.html";
						$payment_result='Payment success!';
					}elseif($_REQUEST['payment_status'] == 0){
					//支付失败
						db::update('orders', "OId='$OId'", array('OrderStatus'=>3));
						$Log='Update order status from '.$c['orders']['status'][$order_row['OrderStatus']].' to '.$c['orders']['status'][3].'<br />'.$tips[1];
						orders::orders_log((int)$_SESSION['User']['UserId'], $UserName, $order_row['OrderId'], 3, $Log);
						$payment_result="Payment Wrong!";
					}
					echo "receive-ok";
				}
			}else{
				//校验失败
			}
		}
		ob_start();
		print_r($_GET);
		print_r($_REQUEST);
		var_dump($xml_str);
		echo "\r\n\r\n$contents";
		echo "\r\n\r\n$payment_result";
		$log=ob_get_contents();
		ob_end_clean();
		$ext=rand(0,9).rand(0,9);
		file::write_file('/_pay_log_/oceanpayment/'.date('Y_m/', $c['time']), "notify-{$OId}-{$ext}.txt", $log);	//把返回数据写入文件
	}
}
?>