<?php
/*
 * Powered by ueeshop.com		http://www.ueeshop.com
 * 广州联雅网络科技有限公司		020-83226791
 */

//订单数据 初始化
$OId = $_GET['OId'];
$order_row = db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
if (!$order_row) {
	file::write_file('/_pay_log_/paypal/REST_log/' . date('Y_m/d/', $c['time']), "{$OId}-" . mt_rand(10, 99) . '-error.txt', "订单数据为空\r\n\r\n" . date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n" . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC') . "\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true));
	exit;
}
$UserName = (int)$_SESSION['User']['UserId'] ? $_SESSION['User']['FirstName'] . ' ' . $_SESSION['User']['LastName'] : 'Tourist';
//支付信息 初始化
$Attribute = db::get_value('payment', 'Method="Paypal"', 'NewAttribute');
$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id = $Account['ClientId'];
$client_secret = $Account['ClientSecret'];
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
$params = array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token = PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token = $token->getAccessToken();

$PaymentId = $_POST['paymentID'];
$PayerId = $_POST['payerID'];

$params = array('payer_id'=>$PayerId);
$PayReturn = PPOpenIdPayment::executePayment($PaymentId, $params, $access_token, $apicontext);
$Json = str::json_data($PayReturn, 'decode');

$state = strtolower($Json['state']);
$payState = strtolower($Json['transactions'][0]['related_resources'][0]['sale']['state']);
if ($state == 'approved') {
	//买方批准交易
    if ($payState == 'completed') {
        //已完成
        $orderStatus = 1;
    } elseif ($payState == 'pending') {
        //待处理
        $orderStatus = 2;
    } else {
        //partially_refunded, refunded, denied
        $orderStatus = 0;
    }
	orders::orders_payment_result($orderStatus, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal/REST_log/' . date('Y_m/d/', $c['time']), "{$OId}-" . mt_rand(10, 99) . '-Execute.txt', date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n" . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC') . "\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true) . "\r\n\r\n Token: " . $access_token . "\r\n\r\n" . print_r($Json, true));
	//记录PayPal买家账号的收货地址信息
	$shipto_ary = array(
		'Account'		=>	addslashes($Json['payer']['payer_info']['email']),
		'FirstName'		=>	addslashes($Json['payer']['payer_info']['first_name']),
		'LastName'		=>	addslashes($Json['payer']['payer_info']['last_name']),
		'AddressLine1'	=>	addslashes($Json['payer']['payer_info']['shipping_address']['line1']),
		'CountryCode'	=>	'+' . $Json['payer']['payer_info']['shipping_address']['country_code'],
		'City'			=>	addslashes($Json['payer']['payer_info']['shipping_address']['city']),
		'State'			=>	addslashes($Json['payer']['payer_info']['shipping_address']['state']),
		'Country'		=>	addslashes($Json['payer']['payer_info']['shipping_address']['country_code']),
		'ZipCode'		=>	addslashes($Json['payer']['payer_info']['shipping_address']['postal_code'])
	);
	if (db::get_row_count('orders_paypal_address_book', "OrderId='{$order_row['OrderId']}'")) {
		db::update('orders_paypal_address_book', "OrderId='{$order_row['OrderId']}'", $shipto_ary);
	} else {
		$shipto_ary['OrderId'] = $order_row['OrderId'];
		db::insert('orders_paypal_address_book', $shipto_ary);
	}
	orders::orders_payment_info($order_row['OrderId'], $Json['payer']['payer_info']['email'], $Json['transactions'][0]['related_resources'][0]['sale']['id'], $order_row['Currency']);
} else {
	//交易请求失败
	orders::orders_payment_result(0, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal/REST_log/' . date('Y_m/d/', $c['time']), "{$OId}-" . mt_rand(10, 99) . '-error.txt', date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n" . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC') . "\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true) . "\r\n\r\n Token: " . $access_token . "\r\n\r\n" . print_r($Json, true));
}

echo '{"OId":"' . $OId . '"}';
