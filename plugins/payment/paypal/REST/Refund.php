<?php
//初始化
$Attribute = db::get_value('payment', 'Method="Paypal"', 'NewAttribute');
$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id = $Account['ClientId'];
$client_secret = $Account['ClientSecret'];
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
$params = array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token = PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token = $token->getAccessToken();
$PaymentId = $_POST['paymentID'];
$PayerId = $_POST['payerID'];
$params = array(
	'amount'		=>	array(
							'total'		=>	$total_price,
							'currency'	=>	$order_row['Currency'],
						),
	'invoice_number'=>	''
);
$RefundReturn = PPOpenIdPayment::refundSale($SaleId, $params, $access_token, $apicontext);
$JSON = str::json_data($RefundReturn, 'decode');
if (strtolower($JSON['state']) == 'completed') {
	//退款状态已完成
	$Status = 1;
} elseif (strtolower($JSON['state']) == 'pending') {
	//退款状态正在等待处理
	$Status = 2;
} else {
	//退款状态失败
	$Status = 0;
}

$refund_data = array(
	'Number'	=>	$SaleId,
	'Amount'	=>	$total_price,
	'Currency'	=>	$order_row['Currency'],
	'Status'	=>	(int)$Status,
	'UpdateTime'=>	$c['time']
);

if (db::get_row_count('orders_refund_info', "OrderId='{$order_row['OrderId']}'")) {
	db::update('orders_refund_info', "OrderId='{$order_row['OrderId']}'", $refund_data);
} else {
	$refund_data['OrderId'] = $order_row['OrderId'];
	$refund_data['AccTime'] = $c['time'];
	db::insert('orders_refund_info', $refund_data);
}

$Symbol = db::get_value('currency', "Currency='{$refund_data['Currency']}'", 'Symbol');

//订单取消
if ($Status==1) {
	$PassOrderStatus = $order_row['OrderStatus'];
	$cancel_data = array(
		'OrderStatus'	=>	7,
		'UpdateTime'	=>	$c['time']
	);
	db::update('orders', "OrderId='{$order_row['OrderId']}'", $cancel_data);
	$order_row = db::get_one('orders', "OrderId='{$order_row['OrderId']}'");//更新订单信息
	$Log = 'Update order status from ' . $c['orders']['status'][$PassOrderStatus] . ' to ' . $c['orders']['status'][7] . '. This order has been refunded successfully, refund amount: ' . $Symbol . $refund_data['Amount'];
	orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $order_row['OrderId'], 7, $Log, 1);
	//处理订单信息
	orders::orders_products_update(7, $order_row, 1);
	$OId = $order_row['OId'];
	/******************** 发邮件 ********************/
	$ToAry = array($order_row['Email']);
	if ((int)db::get_value('system_email_tpl', "Template='order_cancel'", 'IsUsed')) {
        //取消订单
		include($c['root_path'] . '/static/static/inc/mail/order_cancel.php');
		ly200::sendmail($ToAry, $mail_title, $mail_contents);
	}
	/******************** 发邮件结束 ********************/
	manage::operation_log('更新订单状态');
}

file::write_file('/_pay_log_/paypal/refund_log/' . date('Y_m/d/', $c['time']), "{$order_row['OId']}-{$Status}.txt", date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n Token: " . $access_token . "\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true) . "\r\n\r\n" . print_r($JSON, true));

$result = array(
	'OId'		=>	$order_row['OId'],
	'Number'	=>	$refund_data['Number'],
	'Amount'	=>	$Symbol.$refund_data['Amount'],
	'Status'	=>	$refund_data['Status'],
	'Time'		=>	date('Y-m-d H:i:s', $refund_data['UpdateTime'])
);
echo str::json_data($result);
exit;
