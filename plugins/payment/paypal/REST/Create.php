<?php
/*
 * Powered by ueeshop.com		http://www.ueeshop.com
 * 广州联雅网络科技有限公司		020-83226791
 */

//订单数据 初始化
$OId = $_GET['OId'];
if ($OId) {
    //来自订单详细页
	$order_row = db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
	if (!$order_row) {
		file::write_file('/_pay_log_/paypal/REST_log/' . date('Y_m/d/', $c['time']), "{$OId}-" . mt_rand(10, 99) . '-error.txt', "订单数据为空\r\n\r\n" . date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n" . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC') . "\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true));
		exit;
	}
	$payment_row = db::get_one('payment', 'Method="Paypal"');
	if ($order_row && $payment_row) {
		db::update('orders', "OId='{$OId}'", array(
            'PId'               =>  $payment_row['PId'],
            'PaymentMethod'     =>  $payment_row['Name' . $c['lang']],
            'PayAdditionalFee'  =>  $payment_row['AdditionalFee'],
            'PayAdditionalAffix'=>  $payment_row['AffixPrice'],
            'UpdateTime'        =>  $c['time']
        ));
		$order_row = db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
	}
} else {
    //来自下单页
	$Data = $_POST['data'];
	$Data = explode('&', urldecode($Data));
	foreach ((array)$Data as $k => $v) {
		$Ary = explode('=', $v);
		$key = strstr($Ary[0], '['); 
		if ($key) {
			$_POST[str_replace($key, '', $Ary[0])][substr($key, 1, -1)] = $Ary[1];
		} else {
			$_POST[$Ary[0]] = $Ary[1];
		}
	}
	$_POST['ReturnType'] = 1;
	$return = self::placeorder();
	$OId = $return['OId'];
	$order_row = db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
}
//支付信息 初始化
$Attribute = db::get_value('payment', 'Method="Paypal"', 'NewAttribute');
$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id = $Account['ClientId'];
$client_secret = $Account['ClientSecret'];
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
$params = array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token = PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token = $token->getAccessToken();
//订单信息
if($order_row['Currency']!=$order_row['ManageCurrency']){
    $total_price	= orders::paypal_orders_total_price($order_row, 1, 1);
    $handling_fee	= $total_price-orders::paypal_orders_total_price($order_row, 0, 1);
    $ProductPrice	= (float)orders::orders_product_price($order_row, 1);
    $DisAfterPrice	= orders::paypal_orders_total_price($order_row, 0, 1);
}else{
    $total_price	= orders::paypal_orders_total_price($order_row, 1);
    $handling_fee	= $total_price-orders::paypal_orders_total_price($order_row, 0);
    $ProductPrice	= (float)orders::orders_products_total_price($order_row);
    $ProductPrice	= cart::iconv_price_rate($ProductPrice, $order_row['Currency']);
    $DisAfterPrice	= orders::paypal_orders_total_price($order_row, 0);
}
$ShippingPrice = cart::iconv_price_rate($order_row['ShippingPrice'], $order_row['Currency']);
$InsurancePrice = cart::iconv_price_rate($order_row['ShippingInsurancePrice'], $order_row['Currency']);
//$DisBeforePrice = cart::iconv_price_format($ProductPrice+$ShippingPrice+$InsurancePrice);
$DisBeforePrice = $ProductPrice + $ShippingPrice + $InsurancePrice;
$DisAfterPrice = (float)substr(sprintf('%01.4f', $DisAfterPrice), 0, -2);
$Discount_ary = array((string)$DisAfterPrice, (string)$DisBeforePrice);
$discount_price = ($Discount_ary[0] == $Discount_ary[1] ? 0 : $Discount_ary[1] - $Discount_ary[0]);
$discount_price = (float)substr(sprintf('%01.4f', $discount_price), 0, -2);
$domain = ly200::get_domain();
$zero = cart::currency_float_price('0.00', $order_row['Currency']);
//重新保留成两位小数
$total_price = (float)substr(sprintf('%01.4f', $total_price), 0, -2);
$_ProductPrice = (float)substr(sprintf('%01.4f', $ProductPrice), 0, -2);
$_ShippingPrice = (float)substr(sprintf('%01.4f', $ShippingPrice), 0, -2);
$_InsurancePrice = (float)substr(sprintf('%01.4f', $InsurancePrice), 0, -2);
$handling_fee = (float)substr(sprintf('%01.4f', $handling_fee), 0, -2);
$prod_price = substr(sprintf('%01.4f', $_ProductPrice - $discount_price), 0, -2);
//保留两位小数的前后差值
$sProductPrice = $ProductPrice - $_ProductPrice;
$sShippingPrice = $ShippingPrice - $_ShippingPrice;
$sInsurancePrice = $InsurancePrice - $_InsurancePrice;
$Blance = $sProductPrice + $sShippingPrice + $sInsurancePrice;
if ($Blance > 0) {
	$handling_fee += $Blance;
	$handling_fee = (float)substr(sprintf('%01.4f', $handling_fee), 0, -2);
}
$params = array(
	'intent'=>'sale',
	'payer'=>array('payment_method'=>'paypal'),
	'transactions'=>array(
		array(
			'amount'=>array(
				'total'=>cart::currency_float_price($total_price, $order_row['Currency']),
				'currency'=>$order_row['Currency'],
				'details'=>array(
					'subtotal'=>cart::currency_float_price($prod_price, $order_row['Currency']),
					'shipping'=>cart::currency_float_price($_ShippingPrice, $order_row['Currency']),
					'handling_fee'=>cart::currency_float_price($handling_fee, $order_row['Currency']),
					'shipping_discount'=>$zero,
					'insurance'=>cart::currency_float_price($_InsurancePrice, $order_row['Currency'])
				)
			),
            'invoice_number'=>$OId,
			'item_list'=>array(
				'items'=>array(),
				/********* 不要删，可以测试调用
				'shipping_address'=>array(
					'recipient_name'=>'Brian Robinson',
					'line1'=>'4th Floor',
					'line2'=>'Unit #34',
					'city'=>'San Jose',
					'country_code'=>'US',
					'postal_code'=>'95131',
					'phone'=>'011862212345678',
					'state'=>'CA'
				)*/
			)
		)
	),
	'redirect_urls'=>array(
		'return_url'=>"{$domain}/cart/success/{$OId}.html",
		'cancel_url'=>"{$domain}/cart/success/{$OId}.html"
	)
);

$Pro = 0;
$_total_price = 0;
$order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, o.SKU as OrderSKU, p.Prefix, p.Number', 'o.OvId asc, o.LId asc');
foreach ($order_list_row as $v) {
	if ($v['BuyType'] == 4) {
        //组合促销
		$package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
		$Name = $package_row['Name'];
		$SKU = $package_row['Name'];
	} else {
        //普通产品
		$Name = $v['Name'];
		$SKU = $v['OrderSKU'] ? $v['OrderSKU'] : $v['Prefix'] . $v['Number'];
	}
	$price = $v['Price'] + $v['PropertyPrice'];
	$v['Discount'] < 100 && $price *= $v['Discount'] / 100;
    $cur_price = cart::iconv_price($price, 2, $order_row['Currency'], 0);
	$params['transactions'][0]['item_list']['items'][$Pro] = array(
		'sku'		=>	$SKU,
		'name'		=>	$Name,
		'quantity'	=>	$v['Qty'],
		'price'		=>	cart::currency_float_price($cur_price, $order_row['Currency']),
		'currency'	=>	$order_row['Currency']
	);
    $_total_price += cart::currency_float_price($cur_price, $order_row['Currency']) * $v['Qty'];
	++$Pro;
}

//计算折扣的误差值
$cur_discount_price = $_total_price - $prod_price;
if ($cur_discount_price != $discount_price) {
    $discount_price = $cur_discount_price;
}

if ($discount_price > 0) {
	$params['transactions'][0]['item_list']['items'][$Pro] = array(
		'sku'		=>	'Discount',
		'name'		=>	$c['lang_pack']['user']['discount'],
		'quantity'	=>	1,
		'price'		=>	cart::currency_float_price($discount_price * -1, $order_row['Currency']),
		'currency'	=>	$order_row['Currency']
	);
}
$PayReturn = PPOpenIdPayment::createPayment($params, $access_token, $apicontext);
$PayReturn = str::json_data($PayReturn, 'decode');
$PayReturn['OId'] = $OId;
$PayReturn = str::json_data($PayReturn);
echo $PayReturn;

ob_start();
print_r($_GET);
print_r($_POST);
echo "\r\n\r\n OId: $OId";
echo "\r\n\r\n Source: " . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC');
echo "\r\n\r\n Token: $access_token";
print_r($params);
print_r($PayReturn);
$log=ob_get_contents();
ob_end_clean();
file::write_file('/_pay_log_/paypal/REST_log/' . date('Y_m/d/', $c['time']), "{$OId}-" . mt_rand(10, 99) . '-Create.txt', $log); //把返回数据写入文件
