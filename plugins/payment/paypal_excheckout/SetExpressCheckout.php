<?php
/***********************************************************
SetExpressCheckout.php

This is the main web page for the Express Checkout sample.
The page allows the user to enter amount and currency type.
It also accept input variable paymentType which becomes the
value of the PAYMENTACTION parameter.

When the user clicks the Submit button, ReviewOrder.php is
called.

Called by index.html.

Calls ReviewOrder.php.

***********************************************************/
// clearing the session before starting new API Call
$shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0;
$where='c.'.$c['where']['cart'];
(int)$_POST['ShoppingCId'] && $where.=" and c.CId='{$_POST['ShoppingCId']}'";
if($_POST['CartCId']){
	$CIdStr=$_POST['CartCId'];
	$where.=" and c.CId in({$CIdStr})";
}
if($_POST['SourceType']=='shipping_cost'){//来自产品详细页
	$ProInfo=explode('&', $_POST['ProInfo']);
	$ProId=(int)str_replace('ProId=', '', $ProInfo[2]);
	$Qty=(int)str_replace('Qty=', '', $ProInfo[3]);
	$where.=" and c.ProId='{$ProId}' and c.Qty='{$Qty}'";
	$cart_row[0]=db::get_one('shopping_cart c left join products p on c.ProId=p.ProId', $where, 'c.*, p.Name_en, p.PicPath_0, p.IsFreeShipping, p.Price_1, p.IsCombination, p.Wholesale, p.IsPromotion, p.PromotionType, p.StartTime, p.EndTime, p.PromotionPrice', 'c.CId desc');
	$CIdStr=$cart_row[0]['CId'];
	if($shipping_template){ 
		foreach((array)$_POST['_shipping_method'] as $k=>$v){
			$_POST['_shipping_method'][$CIdStr]=$v;
			unset($_POST['_shipping_method'][$k]);
		}
		$price_ary=str::json_data(htmlspecialchars_decode(stripslashes($_POST['ShippingPrice'])), 'decode');
		$express_ary=str::json_data(htmlspecialchars_decode(stripslashes($_POST['ShippingExpress'])), 'decode');
		foreach((array)$price_ary as $k=>$v){
			$price_ary[$CIdStr]=$v;
			$express_ary[$CIdStr]=$express_ary[$k];
			unset($price_ary[$k]);
			unset($express_ary[$k]);
		}
		$_POST['ShippingPrice']=addslashes(str::json_data($price_ary));
		$_POST['ShippingExpress']=addslashes(str::json_data($express_ary));
	}
}else{
	$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', $where, 'c.*, p.Name_en, p.PicPath_0, p.IsFreeShipping, p.Price_1, p.IsCombination, p.Wholesale, p.IsPromotion, p.PromotionType, p.StartTime, p.EndTime, p.PromotionPrice', 'c.CId desc');
}
!count($cart_row) && js::location("/cart/");
$UserId=(int)$_SESSION['User']['UserId'];

//-------------------------------------------生成订单号-------------------------------------------
while(1){
	$OId=date('ymdHis', $c['time']).mt_rand(10,99);
	if(!db::get_row_count('orders', "OId='$OId'")){
		break;
	}
}
//-------------------------------------------生成订单号-------------------------------------------

$i=1;
$TotalQty=$ProductPrice=$IsFreeShipping=$totalWeight=$totalVolume=$s_totalWeight=$s_totalVolume=0;
$insert_sql='';
$products_ary=array();
foreach($cart_row as $v){
	$insert_sql.=($i%100==1)?"insert into `shopping_excheckout` (OId, CId, UserId, SessionId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Volume, Price, Qty, Property, PropertyPrice, OvId, Discount, Remark, Language, AccTime) VALUES":',';
	$Name=str::str_code($v['Name'.$c['lang']], 'addslashes');
	$SKU=str::str_code($v['SKU'], 'addslashes');
	$Property=str::str_code($v['Property'], 'addslashes');
	$Remark=str::str_code($v['Remark'], 'addslashes');
	$insert_sql.="('{$OId}', '{$v['CId']}', '{$v['UserId']}', '{$v['SessionId']}', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$v['PicPath']}', '{$v['StartFrom']}', '{$v['Weight']}', '{$v['Volume']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}', '{$v['Discount']}', '{$Remark}', '{$v['Language']}', '{$c['time']}')";
	
	$products_ary[]=array(
		'ProId'			=>	$v['ProId'],
		'CId'			=>	$v['CId'],
		'Name'			=>	$v['Name'.$c['lang']],
		'Price'			=>	($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1),
		'Qty'			=>	$v['Qty'],
	);
	
	$ProductPrice+=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1)*$v['Qty'];
	$TotalQty+=$v['Qty'];
	
	if($i++%100==0){
		db::query($insert_sql);
		$insert_sql='';
	}
	// 产品重量体积重新计算
	$Data=cart::product_type(array(
		'Type'	=> $v['BuyType'],
		'KeyId'	=> $v['KeyId'],
		'Qty'	=> $v['Qty'],
		'Attr'	=> $v['Attr']
	));
	$BuyType	= $Data['BuyType'];	//产品类型
	$ProId		= $Data['ProId'];	//产品ID
	$Price		= $Data['Price'];	//产品目前的单价
	$Qty		= $Data['Qty'];		//购买的数量
	$Discount	= $Data['Discount'];//折扣
	$Attr		= $Data['Attr'];	//产品属性
	$Weight		= $Data['Weight'];	//产品目前的重量
	$Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
	$OvId		= 1;//发货地ID
	$Qty<1 && $Qty=1;
	//产品属性的处理
	$AttrData=cart::get_product_attribute(array(
		'Type'			=> 0,//不用获取产品属性名称
		'BuyType'		=> $BuyType,
		'ProId'			=> $ProId,
		'Price'			=> $Price,
		'Attr'			=> $Attr,
		'IsCombination'	=> $Data['ProdRow']['IsCombination'],
		'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
		'SKU'			=> $Data['ProdRow']['SKU'],
		'Weight'		=> $Weight
	));
	$Price			= $AttrData['Price'];		//产品单价
	$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
	$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
	$OvId			= $AttrData['OvId'];		//发货地ID
	$Weight			= $AttrData['Weight'];		//产品重量
	$Attr			= $AttrData['Attr'];		//产品属性数据
	//普通产品，重新计算价格，开始判断批发价和促销价
	if($BuyType==0){
		if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
			$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
		}else{ //没有属性
			$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
		}
	}
	//更新产品信息
	$update_data=array(
		'Weight'		=>	(float)($Weight?$Weight:$v['Weight']),
		'Volume'		=>	(float)sprintf('%01.3f', ($Volume?$Volume:$v['Volume'])),
		'Price'			=>	(float)sprintf('%01.2f', ($Price?$Price:$v['Price'])),
		'Qty'			=>	$Qty,
		'PropertyPrice'	=>	(float)sprintf('%01.2f', ($PropertyPrice?$PropertyPrice:$v['PropertyPrice'])),
		'Discount'		=>	(int)($Discount?$Discount:$v['Discount'])
	);
	if($BuyType==0){//检查产品混批功能
		$result=(float)cart::update_cart_wholesale_price($v['ProId'], $v, $v['CId'], 1);//检查产品混批功能
		$result && $update_data['Price']=$result;
	}
	//整理产品的信息数据
	$item_price=($update_data['Price']+$update_data['PropertyPrice'])*($update_data['Discount']<100?$update_data['Discount']/100:1)*$Qty;
	$totalVolume+=($update_data['Volume']*$Qty);
	if($shipping_template){
		$CartId=(int)$v['CId'];
		if(!$pro_info_ary[$CartId]){
			$pro_info_ary[$CartId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$OvId);
		}
		$pro_info_ary[$CartId]['tWeight']=($update_data['Weight']*$Qty);
		$pro_info_ary[$CartId]['tVolume']=($update_data['Volume']*$Qty);
		$pro_info_ary[$CartId]['tQty']=$Qty;
		$pro_info_ary[$CartId]['Price']=$item_price;
		if((int)$v['IsFreeShipping']==1){//免运费
			$pro_info_ary[$CartId]['IsFreeShipping']=1; //其中有免运费
		}else{
			$pro_info_ary[$CartId]['Weight']=($update_data['Weight']*$Qty);
			$pro_info_ary[$CartId]['Volume']=($update_data['Volume']*$Qty);
			$pro_info_ary[$CartId]['Qty']=$Qty;
		}
	}else{
		if(!$pro_info_ary[$OvId]){
			$pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
		}
		$pro_info_ary[$OvId]['tWeight']+=($update_data['Weight']*$Qty);
		$pro_info_ary[$OvId]['tVolume']+=($update_data['Volume']*$Qty);
		$pro_info_ary[$OvId]['tQty']+=$Qty;
		$pro_info_ary[$OvId]['Price']+=$item_price;
		if((int)$v['IsFreeShipping']==1){//免运费
			$pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
		}else{
			$pro_info_ary[$OvId]['Weight']+=($update_data['Weight']*$Qty);
			$pro_info_ary[$OvId]['Volume']+=($update_data['Volume']*$Qty);
			$pro_info_ary[$OvId]['Qty']+=$Qty;
		}
	}
}
$insert_sql!='' && db::query($insert_sql);

//使用优惠券
$CouponPrice=$CouponDiscount=0;
$coupon=@trim($_POST['order_coupon_code']);
if($coupon!=''){
	include($c['root_path'].'static/static/do_action/cart.php');
	$coupon_row=cart_module::get_coupon_info($coupon, $ProductPrice, (int)$_SESSION['User']['UserId'], $CIdStr);
	if($coupon_row['status']==1){
		$CouponCode=addslashes($coupon_row['coupon']);
		if($coupon_row['type']==1){	//CouponType: [0, 打折] [1, 减价格]
			$CouponPrice=$coupon_row['cutprice'];
		}else{
			if($coupon_row['pro_price']){
				$CouponPrice=($coupon_row['pro_price']*(100-$coupon_row['discount'])/100);
			}else{
				$CouponDiscount=(100-$coupon_row['discount'])/100;
			}
		}
	}else{
		unset($_SESSION['Cart']['Coupon']);
	}
}
//自定义参数： 产品数量、国家ID、快递ID、快递名、快递类型、是否购买快递保险、快递运费、快递保险费、优惠券代码
if(is_array($_POST['SId']) || is_array($_POST['ShippingInsurance']) || $shipping_template){ //PC端是数组格式，移动端是JSON格式
	$_SId=$_ShippingInsurance=array();
	if($shipping_template){
		$CId_Ary=@explode(',', $CIdStr);
		$_shipping_method=array();
		foreach((array)$_POST['_shipping_method'] as $k=>$v){
			$_shipping_method[]=(int)$k;
		}
		foreach((array)$CId_Ary as $v){
			if($v && !in_array($v, (array)$_shipping_method)){
				js::location('/cart/', $c['lang_pack']['mobile']['choose_ship']);
			}
		}
		$_SId=$_POST['_shipping_method'];
	}else{
		foreach((array)$_POST['SId'] as $k=>$v){
			if((int)$v<1){ js::location('/cart/'); exit; }
			$_SId['OvId_'.$k]=$v;
		}
		$OverseaAry=explode(',', $_POST['order_shipping_oversea']);
		foreach($Oversea as $v){//丢失海外仓
			if(!$_SId['OvId_'.$v]){ js::location('/cart/'); exit; }
		}
	}
	foreach((array)$_POST['ShippingInsurance'] as $k=>$v){ $_ShippingInsurance['OvId_'.$k]=$v; }
	$_SId=str::json_data($_SId);
	$_ShippingInsurance=str::json_data($_ShippingInsurance);
	$SIdAry=$_POST['SId'];
	$ShippingInsurance=$_POST['ShippingInsurance'];
}else{
	$error = 0;
	$_SId=$_POST['SId'];
	$_ShippingInsurance=$_POST['ShippingInsurance'];
	$SIdAry=@str::json_data(str_replace(array('OvId_', '\\'), '', $_SId), 'decode');//发货方式
	$ShippingInsurance=@str::json_data(str_replace(array('OvId_', '\\'), '', $_ShippingInsurance), 'decode');//发货方式
	count($SIdAry) || $error=1;
	foreach($SIdAry as $k=>$v){
		if((int)$v<1) $error=1;
	}
	if($error){js::location("/cart/"); exit;}
}

$sType=@str::json_data(str_replace(array('OvId_', '\\'), '', $_POST['ShippingMethodType']), 'decode');//海运或空运
$shipping_ary=orders::orders_shipping_method($SIdAry, (int)$_POST['CId'], $sType, $ShippingInsurance, $pro_info_ary, $_POST['order_shipping_api']);
if($shipping_ary['ShippingOvPrice']){
    $_POST['ShippingPrice']=$shipping_ary['ShippingOvPrice'];
    $_POST['ShippingInsurancePrice']=$shipping_ary['ShippingOvInsurancePrice'];
}else{
    $_POST['ShippingPrice']=$shipping_ary['ShippingPrice'];
    $_POST['ShippingInsurancePrice']=$shipping_ary['ShippingInsurancePrice'];
}

$CUSTOM=(int)count($products_ary).'|'.(int)$_POST['CId'].'|'.trim($_SId).'|'.trim($_POST['ShippingExpress']).'|'.trim($_POST['ShippingMethodType']).'|'.trim($_ShippingInsurance).'|'.trim($_POST['ShippingPrice']).'|'.trim($_POST['ShippingInsurancePrice']).'|'.trim($coupon_row['coupon']);
$CUSTOM=stripslashes($CUSTOM);
$CUSTOM=str_replace('OvId_', '', $CUSTOM);
//运费、保险费
$_POST['ShippingPrice']=str::json_data(stripslashes($_POST['ShippingPrice']), 'decode');
$_POST['ShippingInsurancePrice']=str::json_data(stripslashes($_POST['ShippingInsurancePrice']), 'decode');
$ShippingPrice=$ShippingInsurancePrice=0;
foreach((array)$_POST['ShippingPrice'] as $k=>$v){
	$ShippingPrice+=(float)$v;
}
foreach((array)$_POST['ShippingInsurancePrice'] as $k=>$v){
	$ShippingInsurancePrice+=(float)$v;
}

$ShippingPrice = cart::iconv_price($ShippingPrice, 2, '', 0);
$ShippingInsurancePrice = cart::iconv_price($ShippingInsurancePrice, 2, '', 0);

//购物满减促销
$Discount=$DiscountPrice=$AfterPrice_1=0;
$cutArr=str::json_data(db::get_value('config', "GroupId='cart' and Variable='discount'", 'Value'), 'decode');
if($cutArr['IsUsed']==1 && $c['time']>=$cutArr['StartTime'] && $c['time']<=($cutArr['EndTime']+30)){//下单时间差30(s)
	foreach((array)$cutArr['Data'] as $k=>$v){
		if(cart::iconv_price($ProductPrice, 2, '', 0)<cart::iconv_price($k, 2, '', 0)) break;
		if($cutArr['Type']==1){
			$DiscountPrice=$v[1];
		}else{
			$Discount=(100-$v[0]);
		}
		$AfterPrice_1=$ProductPrice-($cutArr['Type']==1?$v[1]:($ProductPrice*(100-$v[0])/100));
	}
}
//会员折扣优惠
$AfterPrice_0=$UserDiscount=0;
if((int)$_SESSION['User']['UserId']){//实时查询当前会员等级 && (int)$_SESSION['User']['Level']
	(int)$_SESSION['User']['Level']=db::get_value('user', "UserId='{$_SESSION['User']['UserId']}'", 'Level');
	$UserDiscount=(float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
	$UserDiscount=($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100;
	$AfterPrice_0=$ProductPrice*($UserDiscount/100);
}
if($AfterPrice_0 && $AfterPrice_1){
	if($AfterPrice_0<$AfterPrice_1){//会员优惠价 < 全场满减价
		$Discount=$DiscountPrice=0;
	}else{
		$UserDiscount=0;
	}
}

//最低消费设置
$_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)*(1-$CouponDiscount)-$CouponPrice-$DiscountPrice;//订单折扣后的总价
if((int)$c['config']['global']['LowConsumption'] && $_total_price<(float)cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0)){
	js::location("/cart/", $c['lang_pack']['cart']['error']['low_error']); exit;
}

//优惠券减去的金额
$_total=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100);
$cutprice=0;
if($CouponPrice){
	$cutprice=$CouponPrice;
}elseif($CouponDiscount){
	$cutprice=$_total-($_total*(1-$CouponDiscount));
}
?>
<div id="excheckout_loading">
    <form action="/payment/paypal_excheckout/ReviewOrder/" method="POST" id="paypal_excheckout_form" style="visibility:hidden;">
        <?php
		$len=count($products_ary);
		foreach($products_ary as $v){
			if($DiscountPrice){
				$price=$v['Price']-($DiscountPrice/($len*$v['Qty']));
			}elseif($Discount || $UserDiscount){
				$price=$v['Price']*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100);
			}else{
				$price=$v['Price'];
			}
		?>
            <input type="hidden" name="L_NAME[]" value="<?=$v['Name'];?>" />
            <input type="hidden" name="L_AMT[]" value="<?=sprintf('%01.2f', $price);?>" />
            <input type="hidden" name="L_QTY[]" value="<?=$v['Qty'];?>" />
        <?php }?>
		<input type="submit" name="" value="submit" />
        <input type="hidden" name="OId" value="<?=$OId;?>" />
        <input type="hidden" name="CUSTOM" value="<?=htmlspecialchars($CUSTOM);?>" />
        <input type="hidden" name="ShippingInsurance" value="<?=(int)$_POST['ShippingInsurance'];?>" />
        <input type="hidden" name="ShippingPrice" value="<?=sprintf('%01.2f', $ShippingPrice);?>" />
        <input type="hidden" name="ShippingInsurancePrice" value="<?=sprintf('%01.2f', $ShippingInsurancePrice);?>" />
        <input type="hidden" name="CouponCutPrice" value="<?=sprintf('%01.2f', cart::iconv_price($cutprice, 2, '', 0));?>" />
    </form>
    <script>
    	document.getElementById('paypal_excheckout_form').submit();
    </script>
</div>
