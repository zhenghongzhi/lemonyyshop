<?php
/**********************************************************
DoExpressCheckoutPayment.php

This functionality is called to complete the payment with
PayPal and display the result to the buyer.

The code constructs and sends the DoExpressCheckoutPayment
request string to the PayPal server.

Called by GetExpressCheckoutDetails.php.

Calls CallerService.php and APIError.php.

**********************************************************/

include_once('CallerService.php');
$domain=ly200::get_domain();
ini_set('session.bug_compat_42',0);
ini_set('session.bug_compat_warn',0);

$reshashAry=$_SESSION['Gateway']['PaypalExcheckout']['reshash'];

$token=urlencode($reshashAry['TOKEN']);
$payerID=urlencode($reshashAry['PAYERID']);
$OId=$reshashAry['PAYMENTREQUEST_0_INVNUM'];	//订单号

/******************************************************************
$nvpstr='&TOKEN='.$token.'&PAYERID='.$payerID.'&PAYMENTREQUEST_0_PAYMENTACTION='.Paypal_PaymentType;

$itemamt=0.00;
$nvpstr_product=$name=$amt=$qty='';
for($i=0; $i<10; $i++){
	$L_Name=$reshashAry["L_PAYMENTREQUEST_0_NAME{$i}"];
	$L_AMT=$reshashAry["L_PAYMENTREQUEST_0_AMT{$i}"];
	$L_QTY=$reshashAry["L_PAYMENTREQUEST_0_QTY{$i}"];
	if(!$L_Name || $L_AMT<=0 || !$L_QTY) break;
	
	$itemamt+=cart::iconv_price($L_AMT*$L_QTY, 2, '', 0);
	$name.="&L_PAYMENTREQUEST_0_NAME{$i}=".urlencode($L_NAME);
	$amt.="&L_PAYMENTREQUEST_0_AMT{$i}=".$L_AMT;
	$qty.="&L_PAYMENTREQUEST_0_QTY{$i}=".$L_QTY;
}
$nvpstr_product.=$name.$amt.$qty;//产品内容.$L_num
$nvpstr.="&PAYMENTREQUEST_0_CURRENCYCODE=".$reshashAry['PAYMENTREQUEST_0_CURRENCYCODE'];
$nvpstr.="&PAYMENTREQUEST_0_AMT=".$reshashAry['PAYMENTREQUEST_0_AMT'];
$nvpstr.="&PAYMENTREQUEST_0_ITEMAMT=".$reshashAry['PAYMENTREQUEST_0_ITEMAMT'];
$nvpstr.='&PAYMENTREQUEST_0_INVNUM='.$reshashAry['PAYMENTREQUEST_0_INVNUM'];//订单号
$nvpstr.='&PAYMENTREQUEST_0_SHIPPINGAMT='.$reshashAry['PAYMENTREQUEST_0_SHIPPINGAMT'];//订单的邮费总额
$nvpstr.='&PAYMENTREQUEST_0_INSURANCEAMT='.$reshashAry['PAYMENTREQUEST_0_INSURANCEAMT'];//订单的邮寄保费总额
$nvpstr.='&PAYMENTREQUEST_0_SHIPDISCAMT='.-$reshashAry['PAYMENTREQUEST_0_SHIPDISCAMT'];//优惠券抵扣金额
$nvpstr.='&PAYMENTREQUEST_0_HANDLINGAMT='.$reshashAry['PAYMENTREQUEST_0_HANDLINGAMT'];//订单处理费用的总额
$nvpstr.=$nvpstr_product;
******************************************************************/


/********************************************************************/
$currCodeType=$reshashAry['PAYMENTREQUEST_0_CURRENCYCODE'];
$paymentAmount=$reshashAry['PAYMENTREQUEST_0_AMT'];

$nvpstr='&TOKEN='.$token.'&PAYERID='.$payerID.'&PAYMENTREQUEST_0_PAYMENTACTION='.Paypal_PaymentType.'&PAYMENTREQUEST_0_AMT='.$paymentAmount.'&PAYMENTREQUEST_0_CURRENCYCODE='.$currCodeType;
/********************************************************************/

$resArray=hash_call("DoExpressCheckoutPayment", $nvpstr);

!$OId && $OId=$_GET['OId'];

file::write_file('/_pay_log_/paypal_excheckout/log/'.date('Y_m/d/', $c['time']), "{$OId}-DoExpress.txt", date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n".$nvpstr."\r\n\r\n".print_r($resArray, true));

//付款正在审核的原因
$response_ary=array(
	'None'			=> '没有审核的原因。',
	'Address'		=> '付款审核的原因是因为买家没有包括确认送货地址和您的收款习惯设定设置要手动接受或拒绝这些款项。修改您的设定，请参看账户信息中习惯设定部分。',
	'Authorization'	=> '付款被审核的原因是已经被授权但还没有解决。您必须先捕获这笔款项。',
	'Echeck'		=> '付款被审核的原因是由于买家使用 eCheck,并且还没有被清算。',
	'Intl'			=> '付款被审核的原因是您拥有非美国账户，但是不拥有取款的机制。您必须从您的账户浏览中接受或者拒绝这笔付款。',
	'multi-currency'=> '账户中没有设置发送货币的余额，并且您没有在收款习惯设定里设置自动转换并接收这样的付款。您必须手动接受或者拒绝这笔付款。',
	'Order'			=> '付款被审核的原因是订单部分被授权，但还没有被清算。',
	'Paymentreview'	=> '付款被审核的原因是因为付款因为风险而正在接受 PayPal 的审查。',
	'Unilateral'	=> '付款被审核的原因是因为这笔付款是从一个邮寄地址而来并且该邮寄地址并没有被注册或者确认。',
	'Verify'		=> '付款被审核的原因是因为您的账户还没有被验证。您必须先验证您的账户，之后才能接受付款。',
	'Other'			=> '付款因为其他的原因而被审核。想要了解更多信息请联系 PayPal 客服。'
);

$ack=strtoupper($resArray["ACK"]);
if($ack!='SUCCESS' && $ack!='SUCCESSWITHWARNING'){
	$_SESSION['Gateway']['PaypalExcheckout']['reshash']=$resArray;
	if($resArray['L_ERRORCODE0']=='10486'){//需要重定向回Paypal
		
		$ShippingPrice=(float)$reshashAry['PAYMENTREQUEST_0_SHIPPINGAMT']; //运费
		$ShippingInsurancePrice=(float)$reshashAry['PAYMENTREQUEST_0_INSURANCEAMT']; //快递保险费
		$CouponCutPrice=(float)abs($reshashAry['PAYMENTREQUEST_0_SHIPDISCAMT']); //优惠券抵扣金额 取正
		$CUSTOM=urlencode($reshashAry['PAYMENTREQUEST_0_CUSTOM']); //自定义值
		$LOGOIMG=urlencode($reshashAry['LOGOIMG']); //自定义LOGO
		$CARTBORDERCOLOR=trim($reshashAry['CARTBORDERCOLOR']); //自定义边框颜色
		
		$AdditionalFee=db::get_value('payment', 'IsUsed=1 and Method="Excheckout"', 'AdditionalFee');
		$order_row=db::get_one('orders', "OId='$OId'");
		$orders_pro_row=db::get_all('orders_products_list', "OrderId='{$order_row['OrderId']}'");
		
		$itemamt=$_itemPrice=0.00;
		$nvpstr_product=$L_name=$L_amt=$L_qty=$L_num='';
		foreach($orders_pro_row as $k=>$v){
			$L_AMT=$reshashAry['L_PAYMENTREQUEST_0_AMT'.$k];
			$L_QTY=$reshashAry['L_PAYMENTREQUEST_0_QTY'.$k];
			$itemamt+=(float)cart::iconv_price($L_AMT, 2, '', 0)*$L_QTY; 
			$L_name.='&L_PAYMENTREQUEST_0_NAME'.$k.'='.urlencode($reshashAry['L_PAYMENTREQUEST_0_NAME'.$k]);
			$L_amt.='&L_PAYMENTREQUEST_0_AMT'.$k.'='.cart::iconv_price($L_AMT, 2, '', 0);
			$L_qty.='&L_PAYMENTREQUEST_0_QTY'.$k.'='.$L_QTY;
		}
		$nvpstr_product.=$L_name.$L_amt.$L_qty;//产品内容.$L_num
		//$HANDLINGAMT=sprintf('%01.2f',($itemamt+$ShippingPrice+$ShippingInsurancePrice)*($AdditionalFee/100));
        $HANDLINGAMT=substr(sprintf('%01.4f', ($itemamt+$ShippingPrice+$ShippingInsurancePrice-$CouponCutPrice)*($AdditionalFee/100)), 0, -2);
		$amt=$itemamt + $ShippingPrice + $ShippingInsurancePrice + $HANDLINGAMT - $CouponCutPrice;//订单总价
		
		$domain=ly200::get_domain();
		$returnURL=urlencode("{$domain}/payment/paypal_excheckout/ReviewOrder/?utm_nooverride=1");
		$cancelURL=urlencode("{$domain}/payment/paypal_excheckout/cancel/{$OId}.html?utm_nooverride=1" );//&paymentType=$paymentType
	
		$nvpstr ='';
		$nvpstr.='&RETURNURL='.$returnURL;//客户选择通过 PayPal 付款后其浏览器将返回到的 URL
		$nvpstr.='&CANCELURL='.$cancelURL;//客户不批准使用 PayPal 向您付款时将返回到的 URL
		$nvpstr.='&PAYMENTREQUEST_0_INVNUM='.$OId;//订单号
		$nvpstr.='&PAYMENTREQUEST_0_AMT='.$amt;//支付总金额
		$nvpstr.='&PAYMENTREQUEST_0_ITEMAMT='.$itemamt;//订单所有物品的价格
		$nvpstr.='&PAYMENTREQUEST_0_SHIPPINGAMT='.$ShippingPrice;//订单的邮费总额
		$nvpstr.='&PAYMENTREQUEST_0_INSURANCEAMT='.$ShippingInsurancePrice;//订单的邮寄保费总额
		$nvpstr.='&PAYMENTREQUEST_0_SHIPDISCAMT='.-$CouponCutPrice;//优惠券抵扣金额
		$nvpstr.='&PAYMENTREQUEST_0_HANDLINGAMT='.$HANDLINGAMT;//订单处理费用的总额
		$nvpstr.='&PAYMENTREQUEST_0_CURRENCYCODE='.$_SESSION['Currency']['Currency'];//交易币种
		$nvpstr.='&PAYMENTREQUEST_0_PAYMENTACTION='.Paypal_PaymentType;//希望获取付款的方式
		$nvpstr.='&PAYMENTREQUEST_0_CUSTOM='.$CUSTOM;//用户可以根据自己的需求自定义的域
		$nvpstr.='&LOGOIMG='.$LOGOIMG;//自定义付款页面LOGO
		$nvpstr.='&CARTBORDERCOLOR='.$CARTBORDERCOLOR;//自定义付款页面边框颜色
		$nvpstr.='&ADDRESSOVERRIDE=0'.$nvpstr_product.'&BRANDNAME='.$c['config']['global']['SiteName'];
		$nvpstr.='&ButtonSource=ueeshop_Cart';	//BN Code(来源标志)
		(int)db::get_value('payment', 'IsUsed=1 and Method="Excheckout"', 'IsCreditCard')==1 && $nvpstr.='&LANDINGPAGE=billing'; //信用卡支付
		$nvpstr =$nvpHeader.$nvpstr;
		
		$resArray=hash_call('SetExpressCheckout', $nvpstr);
		$_SESSION['Gateway']['PaypalExcheckout']['reshash']=$resArray;
		file::write_file('/_pay_log_/paypal_excheckout/log/'.date('Y_m/d/', $c['time']), "{$OId}-SetAgainExpress.txt", date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n".$nvpstr."\r\n\r\n".print_r($_SESSION['Gateway'], true));	//把返回数据写入文件
		
		$ack=strtoupper($resArray['ACK']);
		if($ack=='SUCCESS'){ //Redirect to paypal.com here
			$token=urldecode($resArray['TOKEN']);
			$payPalURL=PAYPAL_URL.$token;
			js::location($payPalURL, 'This transaction cannot be completed. Redirecting to PayPal.');
		}else{ //Redirecting to APIError.php to display errors.
			$location = '/payment/paypal_excheckout/APIError/';
			js::location($location);
		}
	}else{
		header('Location: /payment/paypal_excheckout/APIError/');
	}
}else{
	unset($_SESSION['Gateway']['PaypalExcheckout']);
	$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1,2,3)");
	!$order_row && js::location('/');
	$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
	
	if($resArray['PAYMENTINFO_0_PAYMENTSTATUS']=='Completed' || $resArray['PAYMENTINFO_0_PAYMENTSTATUS']=='Completed-Funds-Held'){
		//1. 付款已经完成，并且交易金额已经成功添加到您的账户。
		//2. 付款已经完成，并且款项已经添加到您审核的余额。
		$Log=orders::orders_payment_result(1, $UserName, $order_row, '');
	}elseif($resArray['PAYMENTINFO_0_PAYMENTSTATUS']=='Pending' || $resArray['PAYMENTINFO_0_PAYMENTSTATUS']=='Processed'){
		//1. 付款正在审核中。
		//2. 付款已经被接受。
		$Log=orders::orders_payment_result(2, $UserName, $order_row, $response_ary[$resArray['PAYMENTINFO_0_PENDINGREASON']]);
	}else{
		//更新订单状态为支付失败
		$Log=orders::orders_payment_result(0, $UserName, $order_row, '');
	}
	
	//订单付款信息记录（更新交易ID数据）
	if(db::get_row_count('orders_payment_info', "OrderId='{$order_row['OrderId']}'")){
		db::update('orders_payment_info', "OrderId='{$order_row['OrderId']}'", array('MTCNNumber'=>$resArray['PAYMENTINFO_0_TRANSACTIONID']));
	}
	
	$url="{$domain}/cart/success/{$order_row['OId']}.html";
	//!(int)$_SESSION['User']['UserId'] && $url="{$domain}/cart/complete/{$order_row['OId']}.html";
	js::location($url);
}
?>
