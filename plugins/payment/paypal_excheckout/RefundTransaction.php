<?php
include_once('CallerService.php');
$domain=ly200::get_domain();
ini_set('session.bug_compat_42',0);
ini_set('session.bug_compat_warn',0);

$nvpstr='&TRANSACTIONID='.$SaleId;
$resArray=hash_call('RefundTransaction', $nvpstr);

//退款状态
$refund_status_ary=array(
	'none'		=> '退款付款状态失败。',
	'instant'	=> '退款付款状态成功。',
	'delayed'	=> '退款付款状态延迟。',
);
//退款付款状态延迟的原因
$refund_status_ary=array(
	'none'				=> '退款状态是即时的。',
	'echeck'			=> '退款状态延迟。',
	'regulatoryreview'	=> '付款尚待审核，同时我们确保其符合监管要求。 我们将在24-72小时内再次与您联系，并提供审核结果。',
);

$ack=strtoupper($resArray['ACK']);
if($ack=='SUCCESS'){
	$REFUNDSTATUS=strtolower($resArray['REFUNDSTATUS']);
	$PENDINGREASON=strtolower($resArray['PENDINGREASON']);
	if($REFUNDSTATUS=='instant'){
		//退款成功
		$Status=1;
	}elseif($REFUNDSTATUS=='delayed'){
		//退款延迟
		$Status=2;
	}else{
		//退款失败
		$Status=0;
	}
}else{//ACK为Failure
	$Status=0;
}

$refund_data=array(
	'Number'	=>	$SaleId,
	'Amount'	=>	$total_price,
	'Currency'	=>	$order_row['Currency'],
	'Status'	=>	(int)$Status,
	'UpdateTime'=>	$c['time'],
);
if(db::get_row_count('orders_refund_info', "OrderId='{$order_row['OrderId']}'")){
	db::update('orders_refund_info', "OrderId='{$order_row['OrderId']}'", $refund_data);
}else{
	$refund_data['OrderId']=$order_row['OrderId'];
	$refund_data['AccTime']=$c['time'];
	db::insert('orders_refund_info', $refund_data);
}

$Symbol=db::get_value('currency', "Currency='{$refund_data['Currency']}'", 'Symbol');

//订单取消
if($Status==1){
	$PassOrderStatus=$order_row['OrderStatus'];
	$cancel_data=array(
		'OrderStatus'	=>	7,
		'UpdateTime'	=>	$c['time']
	);
	db::update('orders', "OrderId='{$order_row['OrderId']}'", $cancel_data);
	$order_row=db::get_one('orders', "OrderId='{$order_row['OrderId']}'");//更新订单信息
	$Log='Update order status from '.$c['orders']['status'][$PassOrderStatus].' to '.$c['orders']['status'][7].'. This order has been refunded successfully, refund amount: '.$Symbol.$refund_data['Amount'];
	orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $order_row['OrderId'], 7, $Log, 1);
	//处理订单信息
	orders::orders_products_update(7, $order_row, 1);
	$OId=$order_row['OId'];
	/******************** 发邮件 ********************/
	$ToAry=array($order_row['Email']);
	if((int)db::get_value('system_email_tpl', "Template='order_cancel'", 'IsUsed')){//取消订单
		include($c['root_path'].'/static/static/inc/mail/order_cancel.php');
		ly200::sendmail($ToAry, $mail_title, $mail_contents);
	}
	/******************** 发邮件结束 ********************/
	manage::operation_log('更新订单状态');
}

file::write_file('/_pay_log_/paypal_excheckout/refund_log/'.date('Y_m/d/', $c['time']), "{$order_row['OId']}-OLD-{$Status}.txt", date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".$nvpstr."\r\n\r\n".print_r($resArray, true)."\r\n\r\nREFUND STATUS:".$refund_status_ary[$REFUNDSTATUS]."\r\n\r\nPENDING REASON:".$refund_status_ary[$PENDINGREASON]);

$result=array(
	'OId'		=>	$order_row['OId'],
	'Number'	=>	$refund_data['Number'],
	'Amount'	=>	$Symbol.$refund_data['Amount'],
	'Status'	=>	$refund_data['Status'],
	'Time'		=>	date('Y-m-d H:i:s', $refund_data['UpdateTime'])
);
echo str::json_data($result);
exit;
?>