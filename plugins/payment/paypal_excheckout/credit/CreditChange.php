<?php
//初始化
$shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used'])?1:0;
$payment_row=db::get_one('payment', 'Method="Excheckout"');
$Attribute=$payment_row['NewAttribute'];
$Account=str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id=$Account['ClientId'];
$client_secret=$Account['ClientSecret'];
$OId=$_POST['OId'];
$order_row=db::get_one('orders', "OId='{$OId}' and OrderStatus in(1, 2, 3)");
$paypal_address_row=db::get_one('orders_paypal_address_book', "OrderId='{$order_row['OrderId']}'");
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext=new PPApiContext(array('mode'=>$c['paypal']));//sandbox | live
$params=array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token=PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token=$token->getAccessToken();
$PaymentId=$paypal_address_row['PaymentId'];
$PayerId=$paypal_address_row['PayerId'];

/***************************************************** 按ID 部分更新付款 Start *****************************************************/
//Partially updates a payment, by ID. You can update the amount, shipping address, invoice ID, and custom data. You cannot update a payment after the payment executes.
$ProductPrice=$_POST['ProductPrice'];
$DiscountPrice=$_POST['DiscountPrice'];
$DiscountPrice>0 && $DiscountPrice*=-1;
$ShippingPrice=0;
$ShippingInsurancePrice=0;
//快递信息
$shipping_ary=array(
	'ShippingOvSId'			=>	str::json_data(stripslashes($_POST['order_shipping_method_sid']), 'decode'),
	'ShippingOvExpress'		=>	'',
	'ShippingOvType'		=>	str::json_data(stripslashes($_POST['order_shipping_method_type']), 'decode'),
	'ShippingOvInsurance'	=>	str::json_data(stripslashes($_POST['order_shipping_insurance']), 'decode'),
	'ShippingOvPrice'		=>	str::json_data(stripslashes($_POST['order_shipping_price']), 'decode'),
	'ShippingOvInsurancePrice'=>str::json_data(stripslashes($_POST['InsurancePrice']), 'decode'),
	'CouponCode'			=>	$_POST['order_coupon_code'],
	'ShippingPrice'			=>	0,//快递运费
	'ShippingInsurancePrice'=>	0,//快递保险费
);
$ExpressAry=array();
foreach((array)$shipping_ary['ShippingOvSId'] as $v){
	$ExpressAry[$k]=db::get_value('shipping', "SId={$v}", 'Express');
}
$shipping_ary['ShippingOvExpress']=$ExpressAry;
if(count($shipping_ary['ShippingOvSId'])>1 || $shipping_template){//多个发货地
	$shipping_ary['ShippingExpress']=$shipping_ary['ShippingMethodSId']=$shipping_ary['ShippingMethodType']=$shipping_ary['ShippingInsurance']='';
	$shipping_ary['ShippingOvExpress']=str::json_data($shipping_ary['ShippingOvExpress']);
	$shipping_ary['ShippingOvSId']=str::json_data($shipping_ary['ShippingOvSId']);
	$shipping_ary['ShippingOvType']=str::json_data($shipping_ary['ShippingOvType']);
	$shipping_ary['ShippingOvInsurance']=str::json_data($shipping_ary['ShippingOvInsurance']);
	$shipping_ary['ShippingOvPrice']=str::json_data($shipping_ary['ShippingOvPrice']);
	$shipping_ary['ShippingOvInsurancePrice']=str::json_data($shipping_ary['ShippingOvInsurancePrice']);
	foreach((array)$shipping_ary['ShippingOvPrice'] as $k=>$v){
		$ShippingPrice+=$v;
		$ShippingInsurancePrice+=$shipping_ary['ShippingOvInsurance'][$k];
	}
}else{//单个发货地
	$shipping_ary['ShippingExpress']=implode('', $shipping_ary['ShippingOvExpress']);
	$shipping_ary['ShippingMethodSId']=implode('', $shipping_ary['ShippingOvSId']);
	$shipping_ary['ShippingMethodType']=implode('', $shipping_ary['ShippingOvType']);
	$shipping_ary['ShippingInsurance']=implode('', $shipping_ary['ShippingOvInsurance']);
	$shipping_ary['ShippingPrice']=implode('', $shipping_ary['ShippingOvPrice']);
	$shipping_ary['ShippingInsurancePrice']=implode('', $shipping_ary['ShippingOvInsurancePrice']);
	$shipping_ary['ShippingOvExpress']=$shipping_ary['ShippingOvSId']=$shipping_ary['ShippingOvType']=$shipping_ary['ShippingOvInsurance']=$shipping_ary['ShippingOvPrice']=$shipping_ary['ShippingOvInsurancePrice']='';
	$ShippingPrice=$shipping_ary['ShippingPrice'];
	$ShippingInsurancePrice=$shipping_ary['ShippingInsurancePrice'];
}
$ShippingPrice=sprintf('%01.2f', $ShippingPrice);
$ShippingInsurancePrice=sprintf('%01.2f', $ShippingInsurancePrice);
//优惠券处理
$CouponPrice=$CouponDiscount=0;
if($shipping_ary['CouponCode']){
	$coupon_row=db::get_one('sales_coupon', "CouponNumber='{$shipping_ary['CouponCode']}'");
	$CId=(int)$coupon_row['CId'];
	$UserId=(int)$order_row['UserId'];
	$BeUseTimes=db::get_row_count('sales_coupon_log', "UserId={$UserId} and CId={$CId}");
	if(!$coupon_row || $c['time']<$coupon_row['StartTime'] || $c['time']>$coupon_row['EndTime'] || ($coupon_row['UseNum'] && $BeUseTimes>=$coupon_row['UseNum']) || $ProductPrice<$coupon_row['UseCondition']){
		$coupon_ary=array();
	}else{
		$CouponCode=addslashes($coupon_row['CouponNumber']);
		if($coupon_row['CouponType']==1){//CouponType: [0, 打折] [1, 减价格]
			$CouponPrice=$coupon_row['Money'];
		}else{
			$CouponPrice=($ProductPrice*(100-$coupon_row['Discount'])/100);
		}
	}
}
$CouponPrice>0 && $DiscountPrice-=$CouponPrice;
$DiscountPrice=sprintf('%01.2f', $DiscountPrice);
//手续费
$TotalPrice=$ProductPrice+$ShippingPrice+$ShippingInsurancePrice+$DiscountPrice;//订单原始总价
$TotalPrice=(float)substr(sprintf('%01.4f', $TotalPrice), 0, -2);//舍弃四舍五入
$TotalPrice=cart::currency_float_price($TotalPrice, $order_row['Currency']);
$AdditionalFee=$payment_row['AdditionalFee'];//手续费
$FeePrice=$TotalPrice*($AdditionalFee/100)+$payment_row['AffixPrice'];
$FeePrice=(float)substr(sprintf('%01.4f', $FeePrice), 0, -2);//舍弃四舍五入
$FeePrice>0 && $TotalPrice+=$FeePrice;
//价格整理
$ProductPrice=(float)substr(sprintf('%01.4f', $ProductPrice+$DiscountPrice), 0, -2);//舍弃四舍五入
$ProductPrice=cart::currency_float_price($ProductPrice, $order_row['Currency']);
$ShippingPrice=cart::currency_float_price($ShippingPrice, $order_row['Currency']);
$ShippingInsurancePrice=cart::currency_float_price($ShippingInsurancePrice, $order_row['Currency']);
$FeePrice=cart::currency_float_price($FeePrice, $order_row['Currency']);
$DiscountPrice=cart::currency_float_price($DiscountPrice, $order_row['Currency']);
$TotalPrice=cart::currency_float_price($TotalPrice, $order_row['Currency']);
$zero=cart::currency_float_price('0.00', $order_row['Currency']);
//折扣编号
$DiscountNo=(int)$paypal_address_row['DiscountNo'];
//更新数据
$params=array(array(
	'op'	=>	'replace',
	'path'	=>	'/transactions/0/amount',
	'value'	=>	array(
					'total'		=>	$TotalPrice,
					'currency'	=>	$order_row['Currency'],
					'details'	=>	array(
										'subtotal'			=>	$ProductPrice,
										'shipping'			=>	$ShippingPrice,
										'handling_fee'		=>	$FeePrice,
										'shipping_discount'	=>	$zero,
										'insurance'			=>	$ShippingInsurancePrice
									)
				)
));
if($DiscountPrice){
	$ShowReturn=PPOpenIdPayment::showPaymentDetails($PaymentId, '', $access_token, $apicontext);
	$JSON=str::json_data($ShowReturn, 'decode');
	$proAry=$JSON['transactions'][0]['item_list']['items'];
    $_total_price = 0;
    foreach ((array)$proAry as $v) { //合计产品总价
        if ($v['sku'] != 'Discount') {
            $_total_price += $v['price'] * $v['quantity'];
        }
    }
    //计算折扣的误差值
    $subtotal_price = $ProductPrice - $DiscountPrice;
    if ($subtotal_price > $_total_price) {
        $CurDiscountPrice = $subtotal_price - $_total_price;
        if ($CurDiscountPrice > 0) {
            $DiscountPrice += $CurDiscountPrice;
        }
    } elseif ($_total_price > $subtotal_price) {
        $CurDiscountPrice = $_total_price - $subtotal_price;
        if ($CurDiscountPrice > 0) {
            $DiscountPrice -= $CurDiscountPrice;
        }
    }
	if($DiscountNo>0){//替换
		$proAry[$DiscountNo]['price']=$DiscountPrice;
	}else{//添加
		$proAry[]=array('name'=>$c['lang_pack']['user']['discount'], 'sku'=>'Discount', 'price'=>$DiscountPrice, 'currency'=>$order_row['Currency'], 'quantity'=>1);
	}
	$params[1]=array(
		'op'	=>	'replace',
		'path'	=>	'/transactions/0/item_list/items',
		'value'	=>	$proAry
	);
}
$ChangeReturn=PPOpenIdPayment::changePaymentDetails($PaymentId, $params, $access_token, $apicontext);
$JSON=str::json_data($ChangeReturn, 'decode');
if(strtolower($JSON['state'])=='created'){
	//更新数据成功
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-ChangeExpress.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n"."Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($params, true)."\r\n\r\n".print_r($JSON, true));
}else{
	//更新数据失败
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-error.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n"."Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($params, true)."\r\n\r\n".print_r($JSON, true));
	echo '{"msg":"Submission Failed!", "ret":0}';
	exit;
}
//保存数据库，检查订单货币汇率需要转换不
if($order_row['Currency']!=$order_row['ManageCurrency']){
    $ExchangeRate=db::get_value('currency', "Currency='{$order_row['Currency']}'", 'ExchangeRate');
    $ManageExchangeRate=db::get_value('currency', 'ManageDefault=1', 'ExchangeRate');
    $CouponPrice=cart::currency_price($CouponPrice, $ExchangeRate, $ManageExchangeRate);
    
    $shipping_ary['ShippingPrice']=cart::currency_price_rate($shipping_ary['ShippingPrice'], $ExchangeRate, $ManageExchangeRate);
    $shipping_ary['ShippingPrice']=(float)round($shipping_ary['ShippingPrice'], 2);
    $shipping_ary['ShippingInsurancePrice']=cart::currency_price_rate($shipping_ary['ShippingInsurancePrice'], $ExchangeRate, $ManageExchangeRate);
    $shipping_ary['ShippingInsurancePrice']=(float)round($shipping_ary['ShippingInsurancePrice'], 2);
    
    $ShippingOvPrice=str::json_data(stripslashes($shipping_ary['ShippingOvPrice']), 'decode');
    foreach((array)$ShippingOvPrice as $k=>$v){
        $v=cart::currency_price_rate($v, $ExchangeRate, $ManageExchangeRate);
        $ShippingOvPrice[$k]=(float)round($v, 2);
    }
    $shipping_ary['ShippingOvPrice']=str::json_data($shipping_ary['ShippingOvPrice']);
    $ShippingOvInsurancePrice=str::json_data(stripslashes($shipping_ary['ShippingOvInsurancePrice']), 'decode');
    foreach((array)$ShippingOvInsurancePrice as $k=>$v){
        $v=cart::currency_price_rate($v, $ExchangeRate, $ManageExchangeRate);
        $ShippingOvInsurancePrice[$k]=(float)round($v, 2);
    }
	$shipping_ary['ShippingOvInsurancePrice']=str::json_data($shipping_ary['ShippingOvInsurancePrice']);
}
$update_data=array(
	/*******************优惠券信息*******************/
	'CouponCode'			=>	$CouponCode,
	'CouponPrice'			=>	$CouponPrice,
	'CouponDiscount'		=>	$CouponDiscount,
	/*******************发货方式*******************/
	'ShippingExpress'		=>	addslashes($shipping_ary['ShippingExpress']?$shipping_ary['ShippingExpress']:''),
	'ShippingMethodSId'		=>	addslashes($shipping_ary['ShippingMethodSId']?$shipping_ary['ShippingMethodSId']:''),
	'ShippingMethodType'	=>	addslashes($shipping_ary['ShippingMethodType']?$shipping_ary['ShippingMethodType']:''),
	'ShippingInsurance'		=>	addslashes($shipping_ary['ShippingInsurance']?$shipping_ary['ShippingInsurance']:''),
	'ShippingPrice'			=>	$shipping_ary['ShippingPrice'],
	'ShippingInsurancePrice'=>	$shipping_ary['ShippingInsurancePrice'],
	'ShippingOvExpress'		=>	addslashes($shipping_ary['ShippingOvExpress']?$shipping_ary['ShippingOvExpress']:''),
	'ShippingOvSId'			=>	addslashes($shipping_ary['ShippingOvSId']?$shipping_ary['ShippingOvSId']:''),
	'ShippingOvType'		=>	addslashes($shipping_ary['ShippingOvType']?$shipping_ary['ShippingOvType']:''),
	'ShippingOvInsurance'	=>	addslashes($shipping_ary['ShippingOvInsurance']?$shipping_ary['ShippingOvInsurance']:''),
	'ShippingOvPrice'		=>	addslashes($shipping_ary['ShippingOvPrice']?$shipping_ary['ShippingOvPrice']:''),
	'ShippingOvInsurancePrice'=>addslashes($shipping_ary['ShippingOvInsurancePrice']?$shipping_ary['ShippingOvInsurancePrice']:''),
	'shipping_template'		=>	(int)$shipping_template,
);
if($_POST['PhoneCode']){
	$update_data['ShippingPhoneNumber']=$_POST['PhoneCode'];
	$update_data['BillPhoneNumber']=$_POST['PhoneCode'];
}
db::update('orders', "OId='{$OId}'", $update_data);
//确认这订单能够正常下单，才进行更新操作的优惠券信息
$coupon_row && user::sales_coupon_log($coupon_row, $OrderId);
$_SESSION['Cart']['Coupon']='';
unset($_SESSION['Cart']['Coupon']);
/***************************************************** 按ID 部分更新付款 End *****************************************************/

/***************************************************** 执行已批准的PayPal付款 Start *****************************************************/
//Executes a PayPal payment that the customer has approved. You can optionally update one or more transactions when you execute the payment.
$params=array(
	'payer_id'	=>	$PayerId
);
$PayReturn=PPOpenIdPayment::executePayment($PaymentId, $params, $access_token, $apicontext);
$JSONTO=str::json_data($PayReturn, 'decode');
$ErrorCode='';
//判断付款状态
$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
if(strtolower($JSONTO['state'])=='approved'){
	//客户批准交易
	$payState = strtolower($JSONTO['transactions'][0]['related_resources'][0]['sale']['state']);
    if ($payState == 'completed') {
        //已完成
        $orderStatus = 1;
    } elseif ($payState == 'pending') {
        //待处理
        $orderStatus = 2;
    } else {
        //partially_refunded, refunded, denied
        $orderStatus = 0;
    }
	orders::orders_payment_result($orderStatus, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-DoExpress.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n"."Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSONTO, true));
}else{
	//交易请求失败
	$ErrorCode=$JSONTO['failure_reason'];
	orders::orders_payment_result(0, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-error.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSONTO, true));
}
//交易失败缘由
if($ErrorCode){
	db::update('orders_paypal_address_book', "OrderId='$OrderId'", array('ErrorCode'=>$ErrorCode));
}
//记录
orders::orders_payment_info($order_row['OrderId'], $JSONTO['payer']['payer_info']['email'], $JSONTO['transactions'][0]['related_resources'][0]['sale']['id'], $JSONTO['transactions'][0]['amount']['currency']);
/***************************************************** 执行已批准的PayPal付款 End *****************************************************/

echo '{"OId":"'.$OId.'", "ret":1}';
exit;