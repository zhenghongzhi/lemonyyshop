<?php
//判断是否允许非会员下单
$TouristsShopping=(int)db::get_value('config', "GroupId='global' and Variable='TouristsShopping'", 'Value');
if(!$TouristsShopping && !$_SESSION['User']['UserId']){
	echo str::json_data(array('message'=>$c['lang_pack']['login_tips']));
	exit;
}

//生成订单号
while(1){
	$OId=date('ymdHis', $c['time']).mt_rand(10,99);
	if(!db::get_row_count('orders', "OId='$OId'")){
		break;
	}
}
//接收数据
if($_POST){
	$ProInfo=array();
	foreach($_POST as $v){
		$ary=explode('=', urldecode($v));
		if($count=stripos($ary[0], '[')){
			$ProInfo[substr($ary[0], 0, $count)][substr($ary[0], $count+1, -1)]=$ary[1];
		}else{
			$ProInfo[$ary[0]]=$ary[1];
		}
	}
}
//整理产品资料
$where='c.'.$c['where']['cart'];
$_GET['CartCId'] && $where.=" and c.CId in({$_GET['CartCId']})";
if($ProInfo['SourceType']=='shipping_cost'){
	//来自产品详细页
	$_POST=$ProInfo;
	$_POST['ReturnType']=1;
	$_POST['excheckout']=1;
	$return=self::additem();
	$CId=$return['CId'];
	if($CId==0){
		echo str::json_data(array('message'=>$c['lang_pack']['products']['attributes_tips']));
		exit;
	}
	$where.=" and c.CId='{$CId}' and c.Qty='{$ProInfo['Qty']}'";
	$cart_row[0]=db::get_one('shopping_cart c left join products p on c.ProId=p.ProId', $where, 'c.*, p.Name_en, p.PicPath_0, p.Prefix, p.Number', 'c.CId desc');
}else{
	//来自购物车
	$CId=str_replace('.', ',', $ProInfo['CId']);
	$where.=" and c.CId in({$CId})";
	$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', $where, 'c.*, p.Name_en, p.PicPath_0, p.Prefix, p.Number', 'c.CId desc');
}
$i=1;
$insert_sql='';
foreach($cart_row as $v){
	$insert_sql.=($i%100==1)?"insert into `shopping_excheckout` (OId, CId, UserId, SessionId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Volume, Price, Qty, Property, PropertyPrice, OvId, Discount, Remark, Language, AccTime) VALUES":',';
	if($v['BuyType']==4){//组合促销
		$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
		$Name=str::str_code($package_row['Name'], 'addslashes');
		$SKU=str::str_code($package_row['Name'], 'addslashes');
	}else{
		$Name=str::str_code($v['Name'.$c['lang']], 'addslashes');
		$SKU=$v['SKU']?$v['SKU']:$v['Prefix'].$v['Number'];
		$SKU=str::str_code($SKU, 'addslashes');
	}
	$Property=str::str_code($v['Property'], 'addslashes');
	$Remark=str::str_code($v['Remark'], 'addslashes');
	$insert_sql.="('{$OId}', '{$v['CId']}', '{$v['UserId']}', '{$v['SessionId']}', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$v['PicPath']}', '{$v['StartFrom']}', '{$v['Weight']}', '{$v['Volume']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}', '{$v['Discount']}', '{$Remark}', '{$v['Language']}', '{$c['time']}')";
	if($i++%100==0){
		db::query($insert_sql);
		$insert_sql='';
	}
}
$insert_sql!='' && db::query($insert_sql);
//记录日志
ob_start();
print_r($_GET);
print_r($_POST);
echo "\r\n\r\n OId: $OId";
echo "\r\n\r\n Source: ".(ly200::is_mobile_client(1)==1?'Mobile':'PC');
$log=ob_get_contents();
ob_end_clean();
file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-SetExpress.txt', $log);//把返回数据写入文件
//返回数据
echo str::json_data(array('OId'=>$OId));
exit;