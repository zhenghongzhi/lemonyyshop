<?php
//初始化
$OId = $_POST['OId'];
$Attribute = db::get_value('payment', 'Method="Excheckout"', 'NewAttribute');
$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id = $Account['ClientId'];
$client_secret = $Account['ClientSecret'];
//数据整理
$Currency = $_SESSION['Currency']['Currency'];
$total_price = $subtotal_price = $original_price = $ProductPrice = 0;
$prod_ary = array();
$prod_list_row = db::get_all('shopping_excheckout', "OId='{$OId}'");
foreach ((array)$prod_list_row as $v) {
	if (!trim($v['Name']) && !$v['Qty']) break;
	$prod_ary[] = $v;
	$price = $v['Price'] + $v['PropertyPrice'];
	$v['Discount'] < 100 && $price *= $v['Discount'] / 100;
	$subtotal_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
    $original_price += $price * $v['Qty'];
	if ($v['Discount'] < 100 && $Currency == $_SESSION['ManageCurrency']['Currency']) {
        //有折扣计算，导致出现三位小数以上，同时汇率为1
        $ProductPrice += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
    } elseif ($Currency!=$_SESSION['ManageCurrency']['Currency']) {
    	//当两者货币不一致
    	$ProductPrice += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
    } else {
	    $ProductPrice += $price * $v['Qty'];
    }
}
$subtotal_price = sprintf('%01.2f', $subtotal_price);
$zero = cart::currency_float_price('0.00', $Currency);
//“会员优惠”和“全场满减”之间的优惠对比
$DisData=cart::discount_contrast($original_price); //传入的价格不算汇率 
$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
$Discount		= $DisData['Discount'];			//全场满减的折扣
$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
//最低消费设置
$total_price = $ProductPrice;
if ($Discount > 0) {
    //折扣金额
	$discount_normal_price = cart::iconv_price_format($total_price * ($Discount / 100));
	$total_price -= $discount_normal_price;
}
if ($UserDiscount > 0) {
    //会员折扣金额
	$discount_user_price = cart::iconv_price_format($total_price * ((100 - $UserDiscount) / 100));
	$total_price -= $discount_user_price;
}
if ($DiscountPrice > 0) {
    //全场满减的抵现金
	$total_price -= $DiscountPrice;
}
if ((int)$c['config']['global']['LowConsumption'] && cart::iconv_price($total_price, 2, '', 0) < cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0)) {
	$difference = cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0) - cart::iconv_price($total_price, 2, '', 0);
	echo str::json_data(array('name'=>'low_error', 'message'=>str_replace(array('%low_price%', '%difference%'), array(cart::iconv_price($c['config']['global']['LowPrice']), cart::iconv_price($difference)), $c['lang_pack']['cart']['error']['consumption'])));
	exit;
}
//计算汇率
if ($_SESSION['Currency']['Currency'] == $_SESSION['ManageCurrency']['Currency']) {
	//当两者货币一致时候，才直接转换总价格的汇率
	$total_price = cart::iconv_price($total_price, 2, '', 0);
}
$discount_price = ($subtotal_price - $total_price);
//保留两位小数
$discount_price = sprintf('%01.2f', $discount_price);
$total_price = sprintf('%01.2f', $total_price);
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
$params = array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token = PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token = $token->getAccessToken();
$domain = ly200::get_domain();
$params = array(
	'intent'=>'sale',
	'payer'=>array('payment_method'=>'paypal'),
	'transactions'=>array(
		array(
			'amount'=>array(
				'total'=>cart::currency_float_price($total_price, $Currency),
				'currency'=>$Currency,
				'details'=>array(
					'subtotal'=>cart::currency_float_price($total_price, $Currency),
					'shipping'=>$zero,
					'handling_fee'=>$zero,
					'shipping_discount'=>$zero,
					'insurance'=>$zero
				)
			),
            'invoice_number'=>$OId,
			'item_list'=>array(
				'items'=>array(),
				/********* 不要删，可以测试调用
				'shipping_address'=>array(
					'recipient_name'=>'Brian Robinson',
					'line1'=>'4th Floor',
					'line2'=>'Unit #34',
					'city'=>'San Jose',
					'country_code'=>'US',
					'postal_code'=>'95131',
					'phone'=>'011862212345678',
					'state'=>'CA'
				)*/
			)
		)
	),
	'redirect_urls'=>array(
		'return_url'=>"{$domain}/payment/paypal_excheckout/ReviewOrder/?utm_nooverride=1",
		'cancel_url'=>"{$domain}/?do_action=cart.paypal_checkout_cancel_log&OId={$OId}&NoCreate=1"
	)
);

if ($OId) {
	$Pro = 0;
    $_total_price = 0;
	foreach ((array)$prod_ary as $v) {
		$price = $v['Price'] + $v['PropertyPrice'];
		$v['Discount'] < 100 && $price *= $v['Discount'] / 100;
		$params['transactions'][0]['item_list']['items'][$Pro] = array(
			'sku'		=>	$v['SKU'],
			'name'		=>	$v['Name'],
			'quantity'	=>	$v['Qty'],
			'price'		=>	cart::currency_float_price(cart::iconv_price($price, 2, '', 0), $Currency),
			'currency'	=>	$Currency
		);
        $_total_price += cart::currency_float_price(cart::iconv_price($price, 2, '', 0), $Currency) * $v['Qty'];
		++$Pro;
	}
    //计算折扣的误差值
    if ($subtotal_price > $_total_price) {
        $cur_discount_price = $subtotal_price - $_total_price;
        if ($cur_discount_price > 0) {
            $discount_price -= $cur_discount_price;
        }
    } elseif ($_total_price > $subtotal_price) {
        $cur_discount_price = $_total_price - $subtotal_price;
        if ($cur_discount_price > 0) {
            $discount_price += $cur_discount_price;
        }
    }
	if ($discount_price > 0) {
		$params['transactions'][0]['item_list']['items'][$Pro] = array(
			'sku'		=>	'Discount',
			'name'		=>	$c['lang_pack']['user']['discount'],
			'quantity'	=>	1,
			'price'		=>	cart::currency_float_price($discount_price * -1, $Currency),
			'currency'	=>	$Currency
		);
	}
	$PayReturn = PPOpenIdPayment::createPayment($params, $access_token, $apicontext);
	if ($PayReturn) {
		echo $PayReturn;
		$PayData = str::json_data($PayReturn, 'decode');
		db::insert('shopping_pay_log', array(
			'OId'		=>	$OId,
			'PayID'		=>	$PayData['id'],
			'AccTime'	=>	$c['time']
		));
	}else{
		echo 'ERROR';
	}
} else {
	echo 'ERROR';
	$OId = 's_'.$c['time']; //如果没有订单号，生成临时订单号，方便检查
}

ob_start();
print_r($_GET);
print_r($_POST);
echo "\r\n\r\n OId: $OId";
echo "\r\n\r\n Source: ".(ly200::is_mobile_client(1)==1?'Mobile':'PC');
echo "\r\n\r\n Token: $access_token";
echo "\r\n\r\n";
print_r($params);
echo "\r\n\r\n";
print_r($PayReturn);
$log=ob_get_contents();
ob_end_clean();
file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-CreateExpress.txt', $log); //把返回数据写入文件

exit;
