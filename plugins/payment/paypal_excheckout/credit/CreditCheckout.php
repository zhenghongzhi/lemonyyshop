<?php
//初始化
$shipping_template = @in_array('shipping_template', (array)$c['plugins']['Used'])?1:0;
$Attribute = db::get_value('payment', 'Method="Excheckout"', 'NewAttribute');
$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
$client_id = $Account['ClientId'];
$client_secret = $Account['ClientSecret'];
//向Paypal请求
$paypal_sdk=$c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
include($paypal_sdk . '/common/PPApiContext.php');
include($paypal_sdk . '/common/PPModel.php');
include($paypal_sdk . '/common/PPUserAgent.php');
include($paypal_sdk . '/common/PPReflectionUtil.php');
include($paypal_sdk . '/common/PPArrayUtil.php');
include($paypal_sdk . '/PPConfigManager.php');
include($paypal_sdk . '/PPLoggingManager.php');
include($paypal_sdk . '/PPHttpConfig.php');
include($paypal_sdk . '/PPHttpConnection.php');
include($paypal_sdk . '/PPLoggingLevel.php');
include($paypal_sdk . '/PPConstants.php');
include($paypal_sdk . '/transport/PPRestCall.php');
include($paypal_sdk . '/exceptions/PPConnectionException.php');
include($paypal_sdk . '/handlers/IPPHandler.php');
include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
include($paypal_sdk . '/auth/openid/PPOpenIdPayment.php');
include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
$params = array(
	'grant_type'	=>	'client_credentials',
	'client_id'		=>	$client_id,
	'client_secret' =>	$client_secret
);
$token = PPOpenIdPayment::createFromAuthorizationCode($params, $apicontext);
$access_token = $token->getAccessToken();
$PaymentId = $_POST['paymentID'];
$PayerId = $_POST['payerID'];
$params = array(
	'payer_id'	=>	$PayerId
);
/***************************************************** 显示付款明细 Start *****************************************************/
//Shows details for a payment, by ID, that has yet to complete. For example, shows details for a payment that was created, approved, or failed.
$ShowReturn = PPOpenIdPayment::showPaymentDetails($PaymentId, '', $access_token, $apicontext);
$JSON = str::json_data($ShowReturn, 'decode');
//创建订单 初始化
$OId = db::get_value('shopping_pay_log', "PayID='{$PaymentId}'", 'OId'); //订单号
//记录数据
if (strtolower($JSON['state']) == 'created') {
	//交易已成功创建
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-GetExpress.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSON, true));
} else {
	//交易创建失败
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-error.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSON, true));
	echo '{"OId":"'.$OId.'", "ret":1}';
	exit;
}
//订单数据
$payment_row = db::get_one('payment', "PId=2");
$cart_row = db::get_all('shopping_excheckout c left join products p on c.ProId=p.ProId', "c.{$c['where']['cart']} and c.OId='{$OId}'", 'c.*, p.Name'.$c['lang'].', p.PicPath_0', 'c.Id asc');
$cartInfo = db::get_one('shopping_excheckout', "{$c['where']['cart']} and OId='{$OId}'", "sum((Price+PropertyPrice)*Discount/100*Qty) as ProductPrice, sum(Weight*Qty) as totalWeight, sum(Volume*Qty) as totalVolume");
$ProductPrice = $cartInfo['ProductPrice']; //产品总价
$totalWeight = $cartInfo['totalWeight']; //产品总重量
$totalVolume = $cartInfo['totalVolume']; //产品总体积
$CouponCutPrice = (float)$JSON['transactions'][0]['amount']['details']['shipping_discount']; //折扣金额
$Currency = $JSON['transactions'][0]['amount']['currency']; //币种
$CountryAcronym = $JSON['payer']['payer_info']['shipping_address']['country_code']; //国家缩写编码
//转换后台默认汇率
if ($Currency == $_SESSION['Currency']['Currency']) {
	$ExchangeRate = $_SESSION['Currency']['ExchangeRate'];
} else {
	$ExchangeRate = db::get_value('currency', "Currency='{$Currency}'", 'ExchangeRate');
}
//设置Paypal买家账号的收货地址信息
$CountryAcronym == 'C2' && $CountryAcronym = 'CN'; //Paypal那边的中国编号是C2
$country_row = db::get_one('country', "Acronym='{$CountryAcronym}'");
$shipto_ary = array(
	'Account'		=>	$JSON['payer']['payer_info']['email'],
	'FirstName'		=>	$JSON['payer']['payer_info']['shipping_address']['recipient_name'],
	'LastName'		=>	'',
	'AddressLine1'	=>	$JSON['payer']['payer_info']['shipping_address']['line1'],
	'AddressLine2'	=>	$JSON['payer']['payer_info']['shipping_address']['line2'],
	'CountryCode'	=>	$country_row['Code'],
	'PhoneNumber'	=>	$JSON['payer']['payer_info']['phone'],
	'City'			=>	$JSON['payer']['payer_info']['shipping_address']['city'],
	'State'			=>	$JSON['payer']['payer_info']['shipping_address']['state'],
	'Country'		=>	$country_row['Country'],
	'CId'			=>	$country_row['CId'],
	'ZipCode'		=>	$JSON['payer']['payer_info']['shipping_address']['postal_code'],
);
//会员信息
$user_data = user::check_login('', 1);
!$user_data && $user_data = array('UserId'=>0, 'Email'=>$JSON['payer']['payer_info']['email']);
//“会员优惠”和“全场满减”之间的优惠对比
$DisData = cart::discount_contrast($ProductPrice);
$DiscountPrice = $DisData['DiscountPrice']; //全场满减的抵现金
$Discount = $DisData['Discount']; //全场满减的折扣
$UserDiscount = $DisData['UserDiscount']; //会员优惠的折扣
//分销订单
if ($_SESSION['DIST']) {
	$source_row = db::get_one('user', "UserId='{$_SESSION['DIST']['UserId']}'"); //分销上级的会员数据
	$DISTInfo = $source_row['DISTUId'] . $source_row['UserId'] . ',';
}
$States = addslashes($shipto_ary['State']);
$ShippingSId = (int)db::get_value('country_states', "AcronymCode='{$States}'", 'SId');
//生成订单
$order_data = array(
    /*******************订单基本信息*******************/
    'OId'					=>	$OId, //订单号
    'UserId'				=>	$user_data['UserId'],
    'Source'				=>	ly200::is_mobile_client(0)?1:0,
    'RefererId'				=>	(int)$_COOKIE['REFERER'],
    'Email'					=>	$user_data['Email'],
    'Discount'				=>	$Discount,
    'DiscountPrice'			=>	$DiscountPrice,
    'UserDiscount'			=>	$UserDiscount,
    'ProductPrice'			=>	$ProductPrice,
    'Currency'				=>	$Currency,
    'ManageCurrency'		=>	$_SESSION['ManageCurrency']['Currency'], //当前后台货币
    'Rate'					=>	$_SESSION['Currency']['Rate'], //当前货币的汇率
    'TotalWeight'			=>	$totalWeight,
    'TotalVolume'			=>	$totalVolume,
    'OrderTime'				=>	$c['time'],
    'UpdateTime'			=>	$c['time'],
    'DISTInfo'				=>	$DISTInfo,
    'IP'					=>	ly200::get_ip(),
    /*******************收货地址*******************/
    'ShippingFirstName'		=>	addslashes($shipto_ary['FirstName']),
    'ShippingLastName'		=>	'',
    'ShippingAddressLine1'	=>	addslashes($shipto_ary['AddressLine1']),
    'ShippingAddressLine2'	=>	addslashes($shipto_ary['AddressLine2']),
    'ShippingCountryCode'	=>	'+' . $shipto_ary['CountryCode'],
    'ShippingPhoneNumber'	=>	$shipto_ary['PhoneNumber'],
    'ShippingCity'			=>	addslashes($shipto_ary['City']),
    'ShippingState'			=>	$States,
    'ShippingSId'			=>	$ShippingSId,
    'ShippingCountry'		=>	addslashes($shipto_ary['Country']),
    'ShippingCId'			=>	$shipto_ary['CId'],
    'ShippingZipCode'		=>	$shipto_ary['ZipCode'],
    /*******************账单地址*******************/
    'BillFirstName'			=>	addslashes($shipto_ary['FirstName']),
    'BillLastName'			=>	addslashes($shipto_ary['LastName']),
    'BillAddressLine1'		=>	addslashes($shipto_ary['AddressLine1']),
    'BillAddressLine2'		=>	addslashes($shipto_ary['AddressLine2']),
    'BillCountryCode'		=>	'+'.$shipto_ary['CountryCode'],
    'BillPhoneNumber'		=>	$shipto_ary['PhoneNumber'],
    'BillCity'				=>	addslashes($shipto_ary['City']),
    'BillState'				=>	addslashes($shipto_ary['State']),
    'BillSId'				=>	$shipto_ary['SId'],
    'BillCountry'			=>	addslashes($shipto_ary['Country']),
    'BillCId'				=>	$shipto_ary['CId'],
    'BillZipCode'			=>	$shipto_ary['ZipCode'],
    /*******************付款方式*******************/
    'PId'					=>	$payment_row['PId'],
    'PaymentMethod'			=>	$payment_row['Name'.$c['lang']],
    'PayAdditionalFee'		=>	$payment_row['AdditionalFee'],
    'PayAdditionalAffix'	=>	$payment_row['AffixPrice']
);
db::insert('orders', $order_data);
$OrderId = db::get_insert_id();
//订单产品信息
$i = 1;
$CId_ary = array();
$insert_sql = '';
$order_pic_dir = $c['orders']['path'] . date('ym', $c['time']) . "/{$OId}/";
!is_dir($c['root_path'] . $order_pic_dir) && file::mk_dir($order_pic_dir);
foreach ($cart_row as $v) {
    $CId_ary[] = $v['CId'];
    $ext_name = file::get_ext_name($v['PicPath']);
    $ImgPath = $order_pic_dir . str::rand_code() . '.' . $ext_name;
    @copy($c['root_path'] . $v['PicPath'] . '.240x240.' . $ext_name, $c['root_path'] . $ImgPath);
    $Name = str::str_code($v['Name' . $c['lang']], 'addslashes');
    $SKU = str::str_code($v['SKU'], 'addslashes');
    $Property = str::str_code($v['Property'], 'addslashes');
    $Remark = str::str_code($v['Remark'], 'addslashes');
    $insert_sql .= ($i % 10 == 1) ? "insert into `orders_products_list` (OrderId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Price, Qty, Property, PropertyPrice, OvId, CId, Discount, Remark, Language, AccTime) VALUES" : ',';
    $insert_sql .= "('$OrderId', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$ImgPath}', '{$v['StartFrom']}', '{$v['Weight']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}', '{$v['CId']}', '{$v['Discount']}', '{$Remark}', '{$v['Language']}', '{$c['time']}')";
    if ($i++ % 10 == 0) {
        db::query($insert_sql);
        $insert_sql = '';
    }
}
$insert_sql != '' && db::query($insert_sql);
//循环产品数据，获取折扣编号
$DiscountNo = '';
foreach ((array)$JSON['transactions'][0]['item_list']['items'] as $k => $v) {
	if ($v['sku'] == 'Discount') {
        //折扣编号
		$DiscountNo = $k;
	}
}
//记录PayPal买家账号的收货地址信息
$shipto_data = array(
	'Account'		=>	addslashes($shipto_ary['Account']),
	'FirstName'		=>	addslashes($shipto_ary['FirstName']),
	'LastName'		=>	'',
	'AddressLine1'	=>	addslashes($shipto_ary['AddressLine1']) . ', ' . addslashes($shipto_ary['AddressLine2']),
	'CountryCode'	=>	'+' . $shipto_ary['CountryCode'],
	'PhoneNumber'	=>	$shipto_ary['PhoneNumber'],
	'City'			=>	addslashes($shipto_ary['City']),
	'State'			=>	addslashes($shipto_ary['State']),
	'Country'		=>	addslashes($shipto_ary['Country']),
	'ZipCode'		=>	addslashes($shipto_ary['ZipCode']),
	'PaymentId'		=>	addslashes($PaymentId),
	'PayerId'		=>	addslashes($PayerId),
	'WebCountry'	=>	addslashes($WebCountryName),
	'DiscountNo'	=>	$DiscountNo
);
if ($ErrorCode) {
	$shipto_data['ErrorCode'] = $ErrorCode;
}
if (db::get_row_count('orders_paypal_address_book', "OrderId='$OrderId'")) {
	db::update('orders_paypal_address_book', "OrderId='$OrderId'", $shipto_data);
} else {
	$shipto_data['OrderId'] = $OrderId;
	db::insert('orders_paypal_address_book', $shipto_data);
}
//没登录会员的情况下单，利用邮箱判断是否为会员，是则分配到该会员，不是则创建临时会员，类似下单后自动注册会员
orders::assign_member($order_data);
//清除原有数据
db::delete('shopping_excheckout', "{$c['where']['cart']} and OId='{$OId}'");
if (count($CId_ary)) {
    $CId_List = @implode(',', $CId_ary);
    db::delete('shopping_cart', "CId in($CId_List)");
}
unset($cart_row, $order_data, $cart_attr, $cart_attr_value, $cart_attr_data, $json_id_ary, $json_value_ary, $order_list);
//如果订单没有产品
$row_count = (int)db::get_row_count('orders_products_list', "OrderId='{$OrderId}'");
if (!$row_count) {
    db::delete('orders', "OrderId='{$OrderId}'");
    file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId . '-' . mt_rand(10,99) . '-error.txt', date('Y-m-d H:i:s', $c['time']) . "\r\n\r\n" . (ly200::is_mobile_client(1) == 1 ? 'Mobile' : 'PC') . "\r\n\r\n" . "Error Information: No Products Data\r\n\r\n" . print_r($_GET, true) . "\r\n\r\n" . print_r($_POST, true) . "\r\n\r\n" . print_r($order_data, true));
    exit;
}
//下单记录
orders::orders_log((int)$user_data['UserId'], $user_data['UserId'] ? ($_SESSION['User']['FirstName'] . ' ' . $_SESSION['User']['LastName']) : 'System', $OrderId, 1, "Place an Order: " . $OId);
//下单减库存
if ((int)$c['config']['global']['LessStock'] == 0) {
    $orders_row = db::get_one('orders', "OrderId='$OrderId'");
    orders::orders_products_update(1, $orders_row);
}
//下单后邮件通知
if((int)$c['config']['global']['CheckoutEmail']){
    $ToAry=array($user_data['Email']);
    include($c['static_path'].'/inc/mail/order_create.php');
    $c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
    ly200::sendmail($ToAry, "Place an Order: ".$OId, $mail_contents);;
    orders::orders_sms($OId);	
}
//创建订单成功
file::write_file('/_pay_log_/paypal_excheckout/'.date('Y_m/', $c['time']), "{$OId}.txt", date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSON, true));	//把返回数据写入文件
//失败返回
if($ErrorCode){
	echo '{"OId":"'.$OId.'", "ret":1}';
	exit;
}
/***************************************************** 显示付款明细 End *****************************************************/

/***************************************************** 执行已批准的PayPal付款 Start *****************************************************/
//Executes a PayPal payment that the customer has approved. You can optionally update one or more transactions when you execute the payment.
/*
$PayReturn=PPOpenIdPayment::executePayment($PaymentId, $params, $access_token, $apicontext);
$JSONTO=str::json_data($PayReturn, 'decode');
$ErrorCode='';
//判断付款状态
$order_row=db::get_one('orders', "OrderId='$OrderId' and OrderStatus in(1, 2, 3)");
$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
if(strtolower($JSONTO['state'])=='approved'){
	//客户批准交易
	orders::orders_payment_result(1, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-DoExpress.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n"."Token: ".$access_token."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSONTO, true));
}else{
	//交易请求失败
	$ErrorCode=$JSONTO['failure_reason'];
	orders::orders_payment_result(0, $UserName, $order_row, '');
	file::write_file('/_pay_log_/paypal_excheckout/credit_log/'.date('Y_m/d/', $c['time']), $OId.'-'.mt_rand(10,99).'-error.txt', date('Y-m-d H:i:s', $c['time'])."\r\n\r\n".(ly200::is_mobile_client(1)==1?'Mobile':'PC')."\r\n\r\n".print_r($_GET, true)."\r\n\r\n".print_r($_POST, true)."\r\n\r\n".print_r($JSONTO, true));
}
//交易失败缘由
if($ErrorCode){
	$row=db::get_one('orders_paypal_address_book', "OrderId='$OrderId'");
	$row['ErrorCode'] && $ErrorCode=$row['ErrorCode'].'|'.$ErrorCode;
	db::update('orders_paypal_address_book', "OrderId='$OrderId'", array('ErrorCode'=>$ErrorCode));
}
//记录
orders::orders_payment_info($OrderId, $shipto_data['Account'], $JSONTO['transactions'][0]['related_resources'][0]['sale']['id'], $Currency);
*/
/***************************************************** 执行已批准的PayPal付款 End *****************************************************/

echo '{"OId":"'.$OId.'", "ret":1}';
exit;