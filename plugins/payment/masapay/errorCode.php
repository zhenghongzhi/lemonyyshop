<?php 
$array = array(
	'1000'		=>	'Invalid Parameters',
	'1001'		=>	'Error Occurred While Converting Message',
	'1002'		=>	'Invalid Request parameters',
	'1003'		=>	'Error Occurred While verifying Signature',
	'1004'		=>	'Failed to Verify Signature',
	'1005'		=>	'Error occurred While Signing',
	'1006'		=>	'Failed to Sign Message',
	'1007'		=>	'Parameter Type Not Matches',
	'1008'		=>	'Invalid Payment Method',
	'1009'		=>	'The Merchant doesn\'t have privilege of connect to institute directly',
	'1010'		=>	'Configure error of direct connection',
	'1011'		=>	'The institute of direct connection has not been activated',
	'1012'		=>	'Invalid Parameter Length',
	'1015'		=>	'Invalid password of the current institute',
	'1016'		=>	'Charset Not Supported',
	'1017'		=>	'Charset Can Not Be Empty',
	'1017-1'	=>	'Language Not Supported',
	'1018'		=>	'Language Can Not Be Empty',
	'1018-1'	=>	'Sign Method Not Supported',
	'1019'		=>	'Sign Method Can Not Be Empty',
	'1020'		=>	'orgcode is Empty or Not Supported',
	'1021'		=>	'The orgcode of local payment is not supported',
	'1022'		=>	'The orgcode of international credit card payment is not supported',
	'1023'		=>	'Invalid order amount format',
	'1024'		=>	'Invalid Email Format',
	'1025'		=>	'Invalid IP address',
	'1026'		=>	'Invalid Date Format',
	'1026-1'	=>	'The length of date is larger than 14',
	'1027'		=>	'Date of the order cannot be empty',
	'1028'		=>	'Gateway order id cannot be empty',
	'1029'		=>	'Rate service call exception',
	'1030'		=>	'Real exchange rate is empty',
	'1031'		=>	'Session has expired',
	'1031-1'	=>	'Browser cookie is blocked',
	'1032'		=>	'Black list filter',
	'1032-1'	=>	'Hit card-number black list',
	'1032-2'	=>	'Hit email black list',
	'1032-3'	=>	'Hit IP black list',
	'1032-4'	=>	'Hit address black list',
	'1035'		=>	'merchant order number incorrect\uff01',
	'2000'		=>	'Transaction is abnormal',
	'2001'		=>	'Unsupported Gateway Version',
	'2002'		=>	'Duplicate Order Number Detected',
	'2002-0'	=>	'Duplicate Order Number with Different Payment URL',
	'2002-1'	=>	'Duplicate Order Number with Different Amount and Currency.',
	'2002-2'	=>	'Order has already been paid and cannot re-submit',
	'2002-3'	=>	'Your order has been processed successfully and currently under review status. Please do not re-submit this order to avoid repeated deduction.',
	'2002-4'	=>	'The order has being processed2003 Merchant Not Exists',
	'2003'		=>	'Merchant Not Exists',
	'2004'		=>	'Error Occurred While Notifying the Merchant',
	'2005'		=>	'Invalid Order Amount',
	'2006'		=>	'Currency Not Matches',
	'2007'		=>	'Target institute error',
	'2008'		=>	'Initiator of the refund must be specified',
	'2009'		=>	'Acquiring refund order id cannot be empty',
	'2010'		=>	'Merchant is not available',
	'2011'		=>	'Refund amount must be specified',
	'2012'		=>	'Refund id must be specified',
	'2013'		=>	'Gateway order id must be specified',
	'2014'		=>	'Currency wallet of the account has not been activated',
	'2015'		=>	'deposit to this account has been disabled',
	'2016'		=>	'withdraw from this account has been disabled',
	'2017'		=>	'This merchant acquiring channel no available',
	'2023'		=>	'Not enough balance',
	'2025'		=>	'The order status is error',
	'2040'		=>	'Refund is not allowed for an order which already has a chargeback',
	'2043'		=>	'Duplicate refund order number',
	'2044'		=>	'This institute doesn\'t support refund',
	'2045'		=>	'This institution prohibited to do refund',
	'2046'		=>	'The current transaction is out of the range of refund',
	'2047'		=>	'The transaction is currently not refundable',
	'2100'		=>	'Status code error',
	'2104'		=>	'Connection refused',
	'2105'		=>	'The request timeout',
	'3000'		=>	'Payment abnormal',
	'3001'		=>	'Available refund amount is not enough',
	'3002'		=>	'The order does not exist.',
	'3003'		=>	'The order has already been paid',
	'3003'		=>	'1 The order has been closed',
	'3004'		=>	'Invalid order amount',
	'3005'		=>	'withdraw from this account has been disabled',
	'3006'		=>	'deposit to this account has been disabled',
	'3007'		=>	'The Original Payment Order Number Does not Exist',
	'3008'		=>	'Duplicated Payment Request',
	'3009'		=>	'Acquiring Institute Error',
	'3010'		=>	'Payment Amount Not Matches',
	'3011'		=>	'Unknown Payment Status',
	'3012'		=>	'Information of the payment instruction does not exist',
	'3013'		=>	'Refund amount is larger than payment amount',
	'3014'		=>	'Payment at institute side is invalid',
	'3015'		=>	'The currency is not supported',
	'3016'		=>	'Payment URL and merchant URL are not matched',
	'3017'		=>	'Some order number you supplied are not matched',
	'3018'		=>	'The order has been done',
	'3019'		=>	'Information of the remittance is not unique',
	'3020'		=>	'Service type is temporary not support',
	'3021'		=>	'The remittance\'s amount you typed is less than the order\'s amount',
	'3022'		=>	'Currency of remittance is different from order\'s',
	'3023'		=>	'The order is auditing and could not be changed',
	'3024'		=>	'Institute order does not exist',
	'3025'		=>	'Institutions do not exist refund orders',
	'3026'		=>	'Channels resulting state is empty',
	'3027'		=>	'Exception notification Merchant',
	'3028'		=>	'Please check the refund amount',
	'3029'		=>	'Please contact administrator',
	'3030'		=>	'Do not duplicate payments.',
	'3084'		=>	'Insufficient funds in the account.',
	'3085'		=>	'Unable to Verify PIN',
	'3086'		=>	'Security Policy Violation',
	'3088'		=>	'Exceed Times Limitation',
	'3089'		=>	'High risk transaction',
	'3091'		=>	'Invalid Institute Parameter',
	'3092'		=>	'Invalid Transaction',
	'3093'		=>	'Exceed amount limit',
	'3094'		=>	'Restricted Card',
	'3095'		=>	'Transaction Timeout',
	'3096'		=>	'Invalid Expiration',
	'3097'		=>	'The card type is not accepted by the payment processor',
	'3098'		=>	'System Busy',
	'3099'		=>	'System Busy',
	'3100'		=>	'Do not honor',
	'3101'		=>	'Invalid Card Information',
	'3102'		=>	'Expired card',
	'3103'		=>	'Invalid account',
	'3104'		=>	'Invalid Card Verification Value (CVV)',
	'3105'		=>	'The transaction has already been settled or reversed.',
	'3106'		=>	'Exceeds PIN Retry',
	'3107'		=>	'The merchant reference code for this authorization request matches the merchant reference code of another authorization request that you sent within the past 15minutes',
	'3108'		=>	'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification System (AVS)check',
	'3109'		=>	'The issuing bank has questions about the request. You do not receive an authorization code programmatically',
	'3110'		=>	'Refer to card issuer',
	'3111'		=>	'Inactive card or card not authorized for card-not-present transactions',
	'3117'		=>	'The request is missing one or more required fields',
	'3118'		=>	'Only a partial amount was approved.',
	'3119'		=>	'Stolen or lost card.',
	'3120'		=>	'The customer matched an entry on the processor\u2019s negative file.',
	'3121'		=>	'The authorization has already been reversed',
	'3122'		=>	'The authorization has already been captured',
	'3123'		=>	'stand-alone credits are not allowed',
	'3125'		=>	'Redirect URL issued',
	'3126'		=>	'The transaction has been cancelled by the customer',
	'3127'		=>	'Phone Authorize',
	'3128'		=>	'Batch Send',
	'3129'		=>	'Invalid card number',
	'3112'		=>	'High Risk-Transnational transaction',
	'3113'		=>	'High Risk-Linked to high risk country',
	'3114'		=>	'High Risk-Same identity with multiple cards',
	'3115'		=>	'High Risk-Frequent transactions',
	'3116'		=>	'High Risk-Other aggregate risk',
	'3130'		=>	'Institute Transaction Timeout',
	'4000'		=>	'Settled error',
	'4001'		=>	'Failed to create settlement stream',
	'4002'		=>	'Institute settlement is failed',
	'5000'		=>	'Institute is abnormal',
	'5001'		=>	'Service of the institute is temporarily stopped',
	'5002'		=>	'Status of the institute is abnormal',
	'5003'		=>	'Merchant number does not exist in institution',
	'5004'		=>	'Invalid signature at institute side',
	'5005'		=>	'Failed to receive notification from institute',
	'5006'		=>	'Failed to connect to institute',
	'5007'		=>	'Unknown error from institute',
	'5008'		=>	'Institute is abnormal',
	'5009'		=>	'The notification\'s amount from institute does not match',
	'5010'		=>	'Unsupported service of the institute',
	'5011'		=>	'Real paid amount is less than order\'s',
	'5012'		=>	'Real refund amount is more than order\'s',
	'6000'		=>	'Database error',
	'6001'		=>	'Database error',
	'6002'		=>	'Database error',
	'7000'		=>	'Offline remittance is abnormal',
	'7001'		=>	'The order is auditing',
	'7002'		=>	'Real paid currency is different from order\'s',
	'7003'		=>	'Real paid amount is less than order\'s',
	'8000'		=>	'Gateway order does not exist',
	'8001'		=>	'Gateway order has already been held on',
	'8002'		=>	'Gateway order has been wholly refunded',
	'8003'		=>	'Transaction has been settled',
	'8004'		=>	'Unfinished transaction',
	'8005'		=>	'Gateway order has not been held on',
	'8006'		=>	'Settle data does not exist',
	'8007'		=>	'PE losses are fail',
	'8008'		=>	'PE release fails',
	'8009'		=>	'This channel does not support reconciliation losses are linked',
	'9000'		=>	'System is maintaining',
	'9001'		=>	'HTTP request timeout',
	'9002'		=>	'Unknown exception',
	'9003'		=>	'MQ is abnormal',
	'9004'		=>	'MQ Error',
	'9005'		=>	'System error',
	'9006'		=>	'Message format is not available',
	'9007'		=>	'Service is offline',
	'CHN9002'	=>	'System runtime exception',
	'CHN2017'	=>	'This merchant acquiring channel no available',
	'CHN2001'	=>	'Configure error of institution weight.The sum is zero',
	'CHN2002'	=>	'Configure error of merchant channel',
	'CHN2015'	=>	'deposit to this account has been disabled',
	'CHN2016'	=>	'Card is not supported',
);
