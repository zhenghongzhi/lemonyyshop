<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  masa
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class masapay_actions 
{ 
	//解析函数的参数是pluginManager的引用 
	function __construct(&$pluginManager){
		//注册这个插件 
		//第一个参数是钩子的名称 
		//第二个参数是pluginManager的引用 
		//第三个是插件所执行的方法 
		$pluginManager->register('masapay', $this, '__config'); 
		$pluginManager->register('masapay', $this, 'do_payment'); 
		$pluginManager->register('masapay', $this, 'notify');
	}
	
	function __config($data){
		return @in_array($data, array('do_payment', 'notify', 'returnUrl'))?'enable':'';
	}

	function array2String($arr,$form_data){
		if(is_null($arr) or !is_array($arr)) return false;
		$str = '';
		$arr_length = count($arr)-1;
		foreach( $arr as $k => $v ){
			$val = $v;
			$key = $k;
			if($form_data){
				$key = $v;
				$val = $form_data[$v];
			}
			$str.=$key.'='.$val.'&';	
		}
		return $str;
	}

	 
	function do_payment($data){
		global $c;
		$sandbox = 0;
		$order_row = $data['order_row'];
		$OId = $order_row['OId'];
		$deviceFingerprintID = "m{$account['merchantId']}_{$OId}{$c['time']}";
		if($sandbox){
			$deviceFingerprint_js = 'https://h.online-metrix.net/fp/check.js?org_id=1snn5n9w&session_id=masapay2'.$deviceFingerprintID;
		}else{
			$deviceFingerprint_js = 'https://h.online-metrix.net/fp/check.js?org_id=k8vif92e&session_id=masapay2'.$deviceFingerprintID;
		}

		$year=@date('Y', $c['time']);
		$monthArr=array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
		$yearArr=array();
		for($i=0; $i<10; $i++){
			$yearArr[]=$year+$i;
		}
		
		$orgCodeArr = array('VISA','MASTER','JCB','AME','DINERSCLUB','DISCOVER');
		if($_POST){
			@extract($_POST, EXTR_PREFIX_ALL, 'p');
			(!$p_CardNo || !is_numeric($p_CardNo) || @strlen($p_CardNo)!=16 || !@in_array($p_CardExpireMonth, $monthArr) || !@in_array($p_CardExpireYear, $yearArr) || !$p_CardSecurityCode || !is_numeric($p_CardSecurityCode) || @strlen($p_CardSecurityCode)!=3 || !in_array($p_orgCode,$orgCodeArr)) && js::back();
			
			$total_price = $data['total_price'];
			$Currency = $order_row['Currency'];
			$currency_arr = array('CNY','USD','EUR','RUB','JPY','MOP','MYR','NZD','CHF','THB','TWD','KRW','INR','CAD','AUD','HKD','GBP');

			if(!in_array($Currency,$currency_arr)){
				js::localtion('/', $c['lang_pack']['cart']['not_accept'], '.top');//不支持的货币
			}

			if(!in_array($Currency,array('JPY','KRW'))){//日元、韩元为元 不处理
				if(in_array($Currency,array('KWD','OMR','BHD','JOD'))){ //KWD、OMR、BHD、JOD为厘
					$total_price = $total_price * 1000;
				}else{ //其他币种为分
					$total_price = $total_price * 100;
				}
			}
			$prod_list_row = db::get_all('orders_products_list',"OrderId='{$order_row['OrderId']}'",'Name','LId asc');
			$goodsName = $prod_list_row[0]['Name'];
			$goodsDesc = '';
			foreach ($prod_list_row as $k => $v) {
				$goodsDesc .= ($k+1).'^'.$v['Name'];
			}
			
			$form_data = array(
				'version'						=>	1.7,
				'merchantId'					=>	$data['account']['merchantId'],
				'charset'						=>	'utf-8',
				'language'						=>	strtoupper(trim($c['lang'],'_')),
				'signType'						=>	'MD5',
				'merchantOrderNo'				=>	$OId,
				'goodsName'						=>	$goodsName,
				'goodsDesc'						=>	$goodsDesc,
				'orderExchange'					=>	2,//订单通讯方式 1异步订单 2同步订单 3同步风控订单
				'currencyCode'					=>	$Currency,
				'orderAmount'					=>	$total_price,//除了日元、韩元为元，KWD、OMR、BHD、JOD为厘（1元 =1000厘）,其他币种为分
				// 'flag3D'						=>	'N',//3D标志
				// 'validationOrderNo'				=>	'',//3D验证订单号
				'submitTime'					=>	date('YmdHis',$c['time']),
				'expiryTime'					=>	date('YmdHis',$c['time']+86400*30),//30天到期
				'bgUrl'							=>	"{$data['domain']}/payment/masapay/notify/{$order_row['OId']}.html",
				// 'transType'						=>	'EDC',	//汇率查询参数---使用了汇率查询时填‘DCC’，否则为‘EDC’。值为空时默认为‘EDC’
				// 'dccRateOrderId'					=>	'',		//汇率查询流水号 --- DCC汇率查询时返回的流水号,如果transType是DCC的话，必填；
				// 'installementOrderId'			=>	'',		//分期查询流水号 --- 分期查询时返回的流水号，分期交易时必填此参数，其余交易为空
				// 'ext1'							=>	'',		//扩展字段 -- 通知商户订单支付处理结果时回传该参数。不能包含=，&等特殊字符
				// 'ext2'							=>	'',		//扩展字段 -- 通知商户订单支付处理结果时回传该参数。不能包含=，&等特殊字符
				// 'remark'							=>	'',		//用于商户的特定业务信息的传递，只有商户不masapay约定传递此参数才有效。 参数名1^参数值1|参数名2^参数值2|
				'payMode'						=>	10,		//支付方式 -- 10国际信用卡
				'orgCode'						=>	$p_orgCode,	//付款机构代码 
				'cardNumber'					=>	$p_CardNo,
				// 'cardHolderFirstName'			=>	$p_FirstName, 			//持卡人名
				// 'cardHolderLastName'				=>	$p_LastName, 			//持卡人姓
				'cardExpirationMonth'			=>	$p_CardExpireMonth,		//卡有效月份
				'cardExpirationYear'			=>	$p_CardExpireYear,		//卡有效年份
				'securityCode'					=>	$p_CardSecurityCode,	//安全码
				'cardHolderEmail'				=>	$order_row['Email'],	//持卡人邮箱
				// 'cardHolderPhoneNumber'			=>	$p_cardHolderPhoneNumber, //持卡人手机 
				// 'payExt1'						=>	'',		//支付扩展 备用
				// 'payExt2'						=>	'',		//支付扩展 备用
				'billName'						=>	"{$order_row['BillFirstName']} {$order_row['BillLastName']}",
				'billAddress'					=>	"{$order_row['BillAddressLine1']}{$order_row['BillAddressLine2']},{$order_row['BillCity']},{$order_row['BillState']}",
				'billPostalCode'				=>	$order_row['BillZipCode'],
				// 'billCompany'					=>	'',		//公司名
				'billCountry'					=>	db::get_value('country',"CId='{$order_row['BillCId']}'",'Acronym'),
				'billState'						=>	db::get_value('country_states',"SId={$order_row['BillSId']}",'AcronymCode'),
				'billCity'						=>	$order_row['BillCity'],
				'billEmail'						=>	$order_row['Email'],
				'billPhoneNumber'				=>	$order_row['BillPhoneNumber'],
				'shippingName'					=>	"{$order_row['ShippingFirstName']} {$order_row['ShippingLastName']}",
				'shippingAddress'				=>	"{$order_row['ShippingAddressLine1']}{$order_row['ShippingAddressLine2']}, {$order_row['ShippingCity']} , {$order_row['ShippingState']}",
				'shippingPostalCode'			=>	$order_row['ShippingZipCode'],
				// 'shippingCompany'				=>	'',		//收货人公司名称
				'shippingCountry'				=>	db::get_value('country',"CId='{$order_row['ShippingCId']}'",'Acronym'),
				'shippingState'					=>	db::get_value('country_states',"SId={$order_row['ShippingSId']}",'AcronymCode'),
				'shippingCity'					=>	$order_row['ShippingCity'],
				'shippingEmail'					=>	$order_row['Email'],
				'shippingPhoneNumber'			=>	$order_row['ShippingPhoneNumber'],
				'deviceFingerprintID'			=>	$deviceFingerprintID,
				// 'payerName'						=>	'', //付款人姓名  
				// 'payerMobile'					=>	'', //付款人手机
				// 'payerEmail'						=>	'', //付款人邮箱
				// 'registerUserId'					=>	'', //注册用户 ID
				'registerUserEmail'				=>	$order_row['Email'],
				'registerTime'					=>	$_SESSION['User']?date('YmdHis',$_SESSION['User']['RegTime']):date('YmdHis',$order_row['AccTime']),
				'registerIp'					=>	$_SESSION['User']?$_SESSION['User']['RegIp']:$order_row['IP'],
				'registerTerminal'				=>	'0'.$order_row['Source'],
				'orderIp'						=>	$order_row['IP'],
				'orderTerminal'					=>	'0'.$order_row['Source'],
				// 'referer'						=>	'', //客户下单网址
				// 'ext3'							=>	'',	//扩展字段
				// 'ext4'							=>	'',	//扩展字段
			);
			// str::dump($form_data);exit;
			$signMsgAry = array(
				'version','merchantId','signType','merchantOrderNo','currencyCode','orderAmount','submitTime','cardNumber','cardExpirationMonth','cardExpirationYear','securityCode'
			);
			$AccountKey = $data['account']['key'];
			$signMsg = urldecode(self::array2String($signMsgAry,$form_data)."key={$AccountKey}");
			$form_data['signMsg'] = md5($signMsg);

			if($sandbox){
				$PayUrl='https://open-sandbox.masapay.com/masapi/receiveMerchantOrder.htm';//测试环境
			}else{
				// $PayUrl='https://open.masapay.com/masapi/receiveMerchantOrder.htm';//国内正式环境
				$PayUrl='https://open1.masapay.com/masapi/receiveMerchantOrder.htm';//国外正式环境
			}
			
			$curl=@curl_init(); 
			@curl_setopt($curl, CURLOPT_URL, $PayUrl);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			@curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			@curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
			@curl_setopt($curl, CURLOPT_REFERER, $data['domain']);
			@curl_setopt($curl, CURLOPT_POST, 1);
			@curl_setopt($curl, CURLOPT_PORT, 443);
			@curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
			@curl_setopt($curl, CURLOPT_TIMEOUT, 300);
			@curl_setopt($curl, CURLOPT_HEADER, 0);
			@curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$result=@curl_exec($curl);
			@curl_close($curl);

			// notify 处理程序 所以注释
			$result_data = str::json_data($result,'decode');
			$return_sign_data = array('signType','merchantOrderNo','currencyCode','orderAmount','paidAmount','resultCode');
			$return_signMsg = urldecode(self::array2String($return_sign_data,$result_data)."key={$AccountKey}");
			$return_sign = strtoupper(md5($return_signMsg));
			$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';

			$Currency = $result_data['currencyCode'];

			$payPrice = $result_data['orderAmount'];

			$total_price=sprintf('%01.2f', orders::orders_price($order_row, 1));

			if(!in_array($Currency,array('JPY','KRW'))){//日元、韩元为元 不处理
				if(in_array($Currency,array('KWD','OMR','BHD','JOD'))){ //KWD、OMR、BHD、JOD为厘
					$payPrice = $payPrice / 1000;
				}else{ //其他币种为分
					$payPrice = $payPrice / 100;
				}
			}

			if($return_sign == $result_data['signMsg']){
				if(abs($payPrice-$total_price)>1 && abs($payPrice-$total_price)>($total_price/100)){	//检查金额(相差小于1 / 相差小于总金额的1%)
					$payment_result='grand total error';
				}else{
					$resultCode = $result_data['resultCode'];
					if($resultCode==10){ //支付成功
						$status = 1;
					}elseif($resultCode==00){
						$status = 2;
					}else{
						$status = 0;
					}
					$payment_result=orders::orders_payment_result($status, $UserName, $order_row, $result_data['errMsg']);
				}
			}else{
				$payment_result='Verification failed!';
			}

			ob_start();
			print_r($_GET);
			print_r($_POST);
			print_r($result);
			print_r($payment_result);
			print_r($result_data);
			$log=ob_get_contents();
			ob_end_clean();
			$ext=rand(0,9).rand(0,9);
			file::write_file('/_pay_log_/masapay/'.date('Y_m/', $c['time']), "curl-{$OId}-{$ext}.txt", $log);	//把返回数据写入文件
			js::location("/cart/success/{$OId}.html", '', '.top');
		}else{
			$title='Credit Card Payment';
			include($c['root_path'].'/static/js/plugin/payment/CreditCard.php');
		}
	}

	function notify($data){
		global $c;
		$OId=$_GET['OId'];
		$result_data = $_POST;
		$order_row = db::get_one('orders',"OId='$OId' and OrderStatus in (1,2,3)");
		$account = db::get_value('payment','Method="Masapay"','Attribute');
		$account_data = str::json_data($account,'decode');
		$AccountKey = $account_data['key'];
		$return_sign_data = array('version','signType','merchantOrderNo','currencyCode','orderAmount','paidAmount','submitTime','resultCode');
		$return_signMsg = urldecode(self::array2String($return_sign_data,$result_data)."key={$AccountKey}");
		$return_sign = strtoupper(md5($return_signMsg));
		$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
		
		$Currency = $result_data['currencyCode'];

		$payPrice = $result_data['orderAmount'];

		$total_price=sprintf('%01.2f', orders::orders_price($order_row, 1));

		if(!in_array($Currency,array('JPY','KRW'))){//日元、韩元为元 不处理
			if(in_array($Currency,array('KWD','OMR','BHD','JOD'))){ //KWD、OMR、BHD、JOD为厘
				$payPrice = $payPrice / 1000;
			}else{ //其他币种为分
				$payPrice = $payPrice / 100;
			}
		}
		
		if($order_row){
			if(abs($payPrice-$total_price)>1 && abs($payPrice-$total_price)>($total_price/100)){	//检查金额(相差小于1 / 相差小于总金额的1%)
				$payment_result='grand total error';
			}else{
				if($return_sign == $result_data['signMsg']){
					$resultCode = $result_data['resultCode'];
					if($resultCode==10){ //支付成功
						$status = 1;
					}elseif($resultCode==00){
						$status = 2;
					}else{
						$status = 0;
					}
					$payment_result=orders::orders_payment_result($status, $UserName, $order_row, $result_data['errMsg']);
				}else{
					$payment_result='Verification failed!';
				}
			}
		}


		ob_start();
		print_r($_GET);
		print_r($_POST);
		echo "\r\n\r\n$payPrice";
		echo "\r\n\r\n$total_price";
		echo "\r\n\r\n$payment_result";
		$log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/masapay/'.date('Y_m/', $c['time']), "notify-$OId.txt", $log);	//把返回数据写入文件
		echo 'OK';
	}
}
?>