<?php $pay_data=$data; //转换一下，防止其他地方已经调用这一变量?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Stripe</title>
<script src="https://js.stripe.com/v3/"></script>
<script>
var stripe = Stripe('<?=$pay_data['account']['Publishable_key']?>');
stripe.createSource({
  type: 'alipay',
  amount: '<?=$pay_data['total_price']*100?>',
  currency: '<?=$pay_data['order_row']['Currency']?>',
  redirect: {
    return_url: '<?=$pay_data['domain'].'/cart/complete/'.$pay_data['order_row']['OId'].'.html?check=1'?>',
  },
}).then(function(result) {
	if(result.source){	//正常返回
		window.location.href=result.source.redirect.url;
	}else{	//出错
			
	}
})
</script>
</head>
<body>
</body>
</html>
