<?php
$pay_data=$data; //转换一下，防止其他地方已经调用这一变量
$symbol=db::get_value('currency', "Currency='{$pay_data['order_row']['Currency']}'", 'Symbol');
$success_url="{$data['domain']}/cart/success/{$pay_data['order_row']['OId']}.html";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Stripe</title>
<?php include("{$c['static_path']}/inc/static.php");?>
<script>$('#payment_loading').remove();</script>
<script src="https://js.stripe.com/v3/"></script>
<script>
var stripe = Stripe('<?=$pay_data['account']['Publishable_key']?>');
stripe.createSource({
  type: 'wechat',
  amount: '<?=$pay_data['total_price']*100?>',
  currency: '<?=strtolower($pay_data['order_row']['Currency'])?>',
  statement_descriptor: '<?=$pay_data['order_row']['OId']?>',
}).then(function(result) {
	//console.log(result);
	if(result.source){	//正常返回
		$.post('/payment/stripewechatpay/creat_qrcode/<?=$pay_data['order_row']['OId'];?>.html', {'CodeUrl':result.source.wechat.qr_code_url, 'TradeId':result.source.wechat.prepay_id}, function(data){  //生成二维码
			if(data.ret==1){
				$('.qrcode').html('<img src="'+data.msg+'" />');
				var query_count=100;
				var timer='';
				query=function(){
					$.post('/payment/stripewechatpay/query/<?=$pay_data['order_row']['OId'];?>.html', {'qrcode':data.msg},function(query_data){
						clearTimeout(timer);
						if(query_data.ret==1){
							query_count=0;
						}else{
							query_count--;
						}
						if(query_count==0){
							if(query_data.ret==1){
								window.location.href='<?=$success_url?>';
							}
							return false;
						} 
						timer=setTimeout(function(){query()},5000);
					},'json');
				}
				setTimeout(function(){query();},2000);
			}
		}, 'json');
	}else if(result.error){  //错误信息
		alert(result.error.message);
	}else{	//出错
		alert('Failure to pay, an unknown error, Please contact the administrator!');
	}
});
</script>
</head>
<body>
    <style>
	body{ background:url(/plugins/payment/stripewechatpay/images/bg_pay.png) #d4d5d7;}
	body *{ font-family:"Microsoft YaHei",Helvetica,Verdana,Arial,Tahoma;}
    #wechat .pay_logo{ height:66px; background:url(/plugins/payment/stripewechatpay/images/logo_bg.png); text-align:center; padding-top:10px; box-sizing:border-box;}
	#wechat .pay_logo .h1{ height:50px;}
	#wechat .pay_con{ padding-top:7px; padding-bottom:40px;}
	#wechat .pay_con .mail_box{ width:920px; margin:0 auto; background:url(/plugins/payment/stripewechatpay/images/mail_box_bg.png) #fff repeat-x 0 -60px; box-shadow:0 1px 1px rgba(0,0,0,0.35); position:relative;}
	#wechat .pay_con .mail_box .corner{ position:absolute; top:0; width:6px; height:30px; background:url(/plugins/payment/stripewechatpay/images/mail_box_bg.png) no-repeat;}
	#wechat .pay_con .mail_box .corner.left{ background-position:0 0; left:-5px;}
	#wechat .pay_con .mail_box .corner.right{ background-position:0 -30px; right:-5px;}
	#wechat .pay_con .mail_box .inner{ overflow:hidden; padding:30px 170px 50px; background:url(/plugins/payment/stripewechatpay/images/mail_box_inner.png) repeat-x bottom left; text-align:center; position:relative; bottom:-10px;}
	#wechat #qr_normal .qrcode{ width:306px; height:306px; background:url(/static/themes/default/images/global/loading_oth.gif) center no-repeat; margin:0 auto;}
	#wechat #qr_normal img{ width:306px; height:306px;}
	#wechat #qr_normal .msg{ width:258px; padding:12px 0; border:1px solid #2b4d69; background:#445f85; border-radius:3px; letter-spacing:6px; color:#fff; display:inline-block; margin-top:5px;}
	#wechat #qr_normal .msg i{ background-position:0 -60px; margin-left:-16px; width:60px; height:60px; display:inline-block; vertical-align:middle; background:url(/plugins/payment/stripewechatpay/images/ico_qr.png) no-repeat 0 -60px;}
	#wechat #qr_normal .msg p{ font-size:16px; text-align:left; vertical-align:middle; letter-spacing:normal; display:inline-block; line-height:25px;}
	#wechat #pay_succ{ display:none;}
	#wechat #pay_succ i{ display:inline-block; width:110px; height:110px; background:url(/plugins/payment/stripewechatpay/images/ico_suc.png) no-repeat;}
	#wechat #pay_succ h3{ font-size:26px; padding:15px 0;}
	#wechat #pay_succ p{ font-size:14px; color:#565656; line-height:25px;}
	#wechat #pay_succ p a{ text-decoration:underline; color:#374673;}
	#wechat #price{ color:#585858; font-size:60px; border-bottom:1px solid #ddd; margin-top:30px; padding-bottom:20px;}
	#wechat #info{ padding:18px 0 10px;}
	#wechat #info p{ overflow:hidden; line-height:26px;}
	#wechat #info label{ float:left; font-size:14px; color:#8e8e8e;}
	#wechat #info span{ float:right; font-size:14px;}
	#wechat .aside{ clear:both; margin-top:14px; padding-top:20px; border-top:3px solid #e0e3eb;}
	<?php if($is_mobile){?>
	#wechat .pay_con .mail_box{ width:100%;}
	#wechat .pay_con .mail_box .inner{ padding:0;}
	<?php }?>
    </style>
    <div style="display:none;"><?php include("{$c['theme_path']}/inc/header.php");?></div>
    <div id="wechat">
    	<div class="pay_logo">
        	<h1><a href="/"><img src="/plugins/payment/stripewechatpay/images/logo_pay_en.png" class="pngFix"></a></h1>
        </div>
        <div class="pay_con">
        	<div class="mail_box">
            	<div class="inner">
                	<div class="top">
                        <div id="qr_normal">
                        	<div class="qrcode"></div>
                            <div class="msg">
                            	<i></i>
                                <p>Use the WeChat to<br/>scan the QR code</p>
                            </div>
                        </div>
                        <div id="pay_succ">
                        	<i></i>
                            <h3>Payment success</h3>
                            <p>
                                <span id="redirectTimer">5</span>
                                seconds after returning to the merchant page, you can also
                                <a href="<?=$success_url?>">click here</a>
                                to return immediately.
                            </p>
                        </div>
                    </div>
                    <div class="bot">
                    	<div id="price"> <span><?=$symbol?></span><?=$pay_data['total_price']?></div>
                        <div id="info">
                            <p><label>Order No.</label><span class="pay_bill_value"><?=$pay_data['order_row']['OId']?></span></p>
                            <p><label>Order Date</label><span class="pay_bill_value"><?=date("Y-m-d H:i:s",$pay_data['order_row']['OrderTime'])?></span></p>
                        </div>
                        <div class="aside"></div>
                    </div>
                </div>
                <div class="corner left"></div>
                <div class="corner right"></div>
            </div>
        </div>
    </div>
</body>
</html>
