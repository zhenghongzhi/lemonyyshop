<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  glbpay 九盈支付(Glbpay)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class stripewechatpay_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('stripewechatpay', $this, '__config');
        $pluginManager->register('stripewechatpay', $this, 'do_payment');
		$pluginManager->register('stripewechatpay', $this, 'creat_qrcode');
		$pluginManager->register('stripewechatpay', $this, 'webhooks');
		$pluginManager->register('stripewechatpay', $this, 'query');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment','creat_qrcode','webhooks','query'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		//目前官方还没支持移动端支付
		$is_mobile=ly200::is_mobile_client(1);
		include('stripe_wechatpay.php');
    }
	
	function creat_qrcode(){
		global $c;
		!$_POST['CodeUrl'] && exit();
		$wechat_qrcode=tx_wechat::qrcode($_POST['CodeUrl'],$_POST['TradeId'],6);
		ly200::e_json($wechat_qrcode, 1);
	} 
	
	function webhooks(){
		global $c;
		//现在stripe所有支付都返回到这,所以需要过滤,以后优化考虑整合到一起
		$webhooks = @file_get_contents('php://input');
		$webhooks = str::json_data($webhooks, 'decode');
		$OId=(int)$webhooks['data']['object']['statement_descriptor'];
		$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1,2,3)");
		if($webhooks['data']['object']['type']=='wechat' && $webhooks['type']=='source.chargeable' && $order_row){  //微信支付,成功付款,订单存在
			$payment_row=db::get_one('payment',"PId='{$order_row['PId']}'");
			$account=str::json_data($payment_row['Attribute'], 'decode');
			$json_data=array(
				'amount'		=>	$webhooks['data']['object']['amount'],
				'currency'		=>	$webhooks['data']['object']['currency'],
				'source'		=>	$webhooks['data']['object']['id'],
			);
			//发起收款请求(虽然已经支付成功才进入这里,但是不知道为什么官网叫这里才是发起支付,我也是蒙了)
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$account['Secret_key']}"));
			curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/charges');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($json_data));
			$output = curl_exec($ch);
			curl_close($ch);
			$output=str::json_data($output,'decode');
			if($output['status']=='succeeded'){ //已经支付完成
				$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
				orders::orders_payment_result(1, $UserName, $order_row, '');
			}
			file::write_file('/_pay_log_/stripe_wechatpay/'.date('Y_m/', $c['time']), $order_row['OId'].'_'.mt_rand(1,100).".txt", print_r($webhooks,true).print_r($output,true));	//把返回数据写入文件
		}
	}
	
	function query(){
		global $c;
		file::del_file($_POST['qrcode']);
		$OId=(int)$_GET['OId'];
		$order_row=db::get_one('orders', "OId='$OId'");
		$status=in_array($order_row['OrderStatus'],array(4,5,6))?1:0;
		ly200::e_json('',$status);
	}
}
?>