<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  fashionpay 速汇通支付(fashionpay)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class fashionpay_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('fashionpay', $this, '__config');
        $pluginManager->register('fashionpay', $this, 'do_payment');
        $pluginManager->register('fashionpay', $this, 'notify');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment', 'notify'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		$is_mobile=ly200::is_mobile_client(1);
		
		//支持货币：CAD(加元)、AUD(澳元)、SGD(新加坡元)、EUR(欧元)、JPY(日元)、RUB(俄罗斯卢布)、CNY(人民币)、USD(美元)、GBP(英镑)
		$fp_currency = array('USD'=>'1', 'EUR'=>'2','RMB'=>'3', 'GBP'=>'4', 'HKD'=>'5', 'JPY'=>'6', 'AUD'=>'7', 'NOK'=>'8', 'CAD'=>'11', 'DKK'=>'12','SEK'=>'13');
		!(int)$fp_currency[$data['order_row']['Currency']] && js::location('/', $c['lang_pack']['cart']['not_accept'], '.top');//不支持的货币
		!in_array($data['order_row']['OrderStatus'], array(1, 3)) && js::location("/account/orders/view{$data['order_row']['OId']}.html");
		
		$return_error_ary=array(
			'-1'=>array('订单号错误', 'Order Number Error'),
			'0'=>array('付款失败', 'Payment failed'),
			'1'=>array('高风险失败', 'high risk of failure'),
			'2'=>array('黑卡库黑卡', 'black card black card library'),
			'3'=>array('单笔限额超限', 'single limit overrun'),
			'4'=>array('月交易量超限', 'monthly trading volume exceeded'),
			'5'=>array('同一IP重复交易', 'repeat the same IP transactions'),
			'6'=>array('同一Email重复交易', 'Email repeat the same transaction'),
			'7'=>array('同一卡号重复交易', 'repeat the same card transactions'),
			'8'=>array('同一COOKIE重复交易', 'COOKIE repeat the same transaction'),
			'9'=>array('中风险失败', 'the risk of failure'),
			'10'=>array('商户号不存在', 'business number does not exist'),
			'11'=>array('商户MD5KEY不存在', 'business MD5KEY does not exist'),
			'12'=>array('货币未设置', 'money is not set'),
			'13'=>array('MD5验证错误', 'MD5 authentication error'),
			'14'=>array('返回网址未注册', 'return URL is not registered'),
			'44'=>array('网站黑库', 'black site library'),
			'15'=>array('商户未开通', 'businesses did not open'),
			'16'=>array('地道未开通', 'authentic not open'),
			'19'=>array('异常单失败', 'abnormal single failure'),
			'22'=>array('网站未注册', 'Web site is not registered'),
			'25'=>array('金额错误', 'the amount of error'),
			'26'=>array('卡号或CVV2或有效期错误', 'or CVV2 card number and expiration date wrong'),
			'30'=>array('同一电话号码重复交易', 'the same phone number repeat business'),
			'31'=>array('禁止交易地区', 'prohibited transaction area'),
			'88'=>array('付款成功', 'payment is successful')
		);
		
		$year=@date('Y', $c['time']);
		if($_POST){
			@extract($_POST, EXTR_PREFIX_ALL, 'p');
			
			$monthArr=array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
			$yearArr=array();
			for($i=0;$i<10;$i++){
				$yearArr[]=$year+$i;
			}
			
			(!$p_CardNo || !is_numeric($p_CardNo) || @strlen($p_CardNo)!=16 || !@in_array($p_CardExpireMonth, $monthArr) || !@in_array($p_CardExpireYear, $yearArr) || !$p_CardSecurityCode || !is_numeric($p_CardSecurityCode) || @strlen($p_CardSecurityCode)!=3 ) && js::back();
			
			$product_xml="<Goods><GoodsName>{$data['order_row']['OId']}</GoodsName><Price>{$data['total_price']}</Price><Qty>1</Qty><Currency>{$data['order_row']['Currency']}</Currency></Goods>";
			
			$form_data = array( 
				//***************************必填***************************
				'MerNo'			=>	$data['account']['MerNo'],	//商户号		account_id	merchantnoValue
				'BillNo'		=>	$data['order_row']['OId'],	//订单号
				'products'		=>	$product_xml,//str::json_data(array('GoodsName'=>$data['order_row']['OId'],'Price'=>$data['total_price'],'Qty'=>1)),
				'Amount'		=>	$data['total_price'],		//交易金额，必须包含 2 位小数
				'Currency'		=>	(int)$fp_currency[$data['order_row']['Currency']],//交易币种 1:美元;2:欧元;4:英镑
					
				//***************************信用卡信息***************************
				'cardnum'		=>	$p_CardNo,	//卡号	4414444444444444
				'year'			=>	$p_CardExpireYear,	//卡有效期年
				'month'			=>	$p_CardExpireMonth,	//卡有效期月
				'cvv2'			=>	$p_CardSecurityCode,	//CVV2
				'cardbank'		=>	'',	//发卡行
				
				//***************************持卡人信息 / 账单信息***************************
				'firstname'		=>	$data['order_row']['ShippingFirstName'],	//名
				'lastname'		=>	$data['order_row']['ShippingLastName'],	//姓
				'address'		=>	$data['order_row']['ShippingAddressLine1'],	//详细地址
				'city'			=>	$data['order_row']['ShippingCity'],	//城市
				'state'			=>	$data['order_row']['ShippingState'],	//州省
				'country'		=>	$data['order_row']['ShippingCountry'],	//国家
				'zipcode'		=>	$data['order_row']['ShippingZipCode'],	//邮政编码
				'email'			=>	$data['order_row']['Email'],	//买家邮件地址
				'phone'			=>	$data['order_row']['ShippingCountryCode'].$data['order_row']['ShippingPhoneNumber'],	//电话
			
				//***************************收货人信息(选填)***************************
				'shippingEmail'		=>	$data['order_row']['Email'],	//买家邮件地址
				'shippingFirstName'	=>	$data['order_row']['ShippingFirstName'],	// 名
				'shippingLastName'	=>	$data['order_row']['ShippingLastName'],	// 姓
				'shippingCity'		=>	$data['order_row']['ShippingCity'],	//城市
				'shippingSstate'	=>	$data['order_row']['ShippingState'],	//州省
				'shippingCountry'	=>	$data['order_row']['ShippingCountry'],	//国家
				'shippingAddress'	=>	$data['order_row']['ShippingAddressLine1'],	//详细地址
				'shippingPhone'		=>	$data['order_row']['ShippingCountryCode'].$data['order_row']['ShippingPhoneNumber'],	//电话
				'shippingZipcode'	=>	$data['order_row']['ShippingZipCode'],	//邮政编码
		
				//***************************选填***************************
				//'OrderDesc'		=>	$data['order_row']['OId'],	//订单描述
				//'OrderDate'		=>	@date('Y-m-d', $data['order_row']['OrderTime']),	//订单日期
				//'OrderTime'		=>	@date('H:i:s', $data['order_row']['OrderTime']),	//订单时间
				'Language'		=>	'en',	//1：中文；2:英文；
				'ReturnURL'		=>	"{$data['domain']}/account/orders/?utm_nooverride=1",
				'Remark'		=>	$data['domain'],	//商户备注信息
				'ip'			=>	ly200::get_ip()?ly200::get_ip():ly200::get_server_ip()
			);
			
			$md5src=$form_data['MerNo'].$form_data['BillNo'].$form_data['Currency'].$form_data['Amount'].$form_data['Language'].$form_data['ReturnURL'].$data['account']['MD5key'];//校验源字符串
			$form_data['MD5info']=strtoupper(md5($md5src));	//MD5检验结果
			
			//curl提交支付
			//$url='http://hpolineshop.com/sslWebsitpayment';	//支付网关
			$url='http://ssl.hpolineshop.com/sslWebsitpayment';	//支付网关
			$curl = @curl_init(); 
			@curl_setopt($curl, CURLOPT_URL, $url);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			@curl_setopt($curl, CURLOPT_HTTPHEADER, array('Expect: '));
			@curl_setopt($curl, CURLOPT_TIMEOUT, 300);
			@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
			@curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
			
			@curl_setopt($curl, CURLOPT_POST, true);
			@curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			@curl_setopt($curl, CURLOPT_REFERER, 'http://' . $_SERVER['HTTP_HOST']);
			@curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			@curl_setopt($curl, CURLOPT_HEADER, 0);
			@curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
			
			$result=@curl_exec($curl);
			$info=@curl_getinfo($curl);
			@curl_close($curl);
			
			$OId=$data['order_row']['OId'];
			$jumpUrl="/cart/success/{$OId}.html";
			if($info['http_code']=='200'){ //提交支付正常响应
				//校验源字符串
				@parse_str($result, $return); //print_r($return); //将字符串参数转成数组
				$md5src = $return['BillNo'].$return['Currency'].$return['Amount'].$return['Succeed'].$data['account']['MD5key'];
				$md5sign = strtoupper(md5($md5src));
				//$responseLog="Your payment operation failed! Reasons for failure: {$return['Result']}<br />Return: ".$return_error_ary[$return['Succeed']][($c['manage']['config']['ManageLanguage']=='en'?1:0)];
				$responseLog="{$return['Result']}<br />Return: ".$return_error_ary[$return['Succeed']][($c['manage']['config']['ManageLanguage']=='en'?1:0)];
				if($return['MD5info']!='' && $return['MD5info'] == $md5sign){ //检验成功
					$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
					if((int)$return['Succeed']==88){ //更新订单状态为支付成功
						$error=orders::orders_payment_result(1, $UserName, $data['order_row'], $responseLog);
					}elseif((int)$return['Succeed']==19){ //更新订单状态为待处理
						$error=orders::orders_payment_result(2, $UserName, $data['order_row'], $responseLog);
					}else{ //更新订单状态为其他状态
						$error=orders::orders_payment_result(0, $UserName, $data['order_row'], $responseLog);
					}
				}else{
					$error="VERIFIED Error!";
				}
			}else{	//提交支付无响应
				$error="Payment no response!";
			}
			
			ob_start();
			print_r($return);
			echo "\r\n\r\n $result";
			echo "\r\n\r\n md5sign: $md5sign";
			echo "\r\n\r\n MD5info: ".$return['MD5info'];
			echo "\r\n\r\n $responseLog";
			echo "\r\n\r\n $error";
			$log=ob_get_contents();
			ob_end_clean();
			file::write_file('/_pay_log_/fashionpay/'.date('Y_m/', $c['time']), $OId.'-'.mt_rand(10,99).".txt", $log);	//把返回数据写入文件
			js::location($jumpUrl, $error, '.top');	
		}else{
			$title='Credit Card Payment';
			include($c['root_path'].'/static/js/plugin/payment/CreditCard.php');
		}
    } 
	
	function notify($data){
		global $c;
		$account=str::json_data(db::get_value('payment', "Method='FashionPay'", 'Attribute'), 'decode');
		$data_ary=array(
			'BillNo'		=>	$_POST['BillNo'],	//订单号
			'Amount'		=>	$_POST['Amount'],	//支付金额
			'Currency'		=>	$_POST['Currency'],	//交易币种
			'CurrencyName'	=>	$_POST['CurrencyName'],	//支付币种符号
			'Succeed'		=>	$_POST['Succeed'],	//支付状态	$Success  88:支付成功  19: 待处理[现在不会返回]  其它状态失败
			'Result'		=>	$_POST['Result'],	//支付结果
			'MD5info'		=>	$_POST['MD5info']	//MD5info校验信息
		);
		$MD5key=$account['MD5key']; //商户的密钥
		$md5src=$data_ary['BillNo'].$data_ary['Currency'].$data_ary['Amount'].$data_ary['Succeed'].$MD5key; //校验源字符串
		$MD5sign=strtoupper(md5($md5src)); //校验结果
		if(strtolower($MD5sign)==strtolower($data_ary['MD5info'])){ //验证数字签名
			$OId=$data_ary['BillNo'];
			$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
			!$order_row && js::location('/');
			$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
			if($data_ary['Succeed']==88){ //支付成功
				$payment_result=orders::orders_payment_result(1, $UserName, $order_row, $data_ary['Result']);
			}elseif($data_ary['Succeed']==19){ //待处理
				$payment_result=orders::orders_payment_result(2, $UserName, $order_row, $data_ary['Result']);
			}else{ //支付失败
				$payment_result=orders::orders_payment_result(0, $UserName, $order_row, $data_ary['Result']);
			}
		}else{ //验证失败
			$payment_result='Validation failure';
		}
		
		ob_start();
		print_r($_GET);
		print_r($_POST);
		echo "\r\n\r\n$MD5sign";
		echo "\r\n\r\n$payment_result";
		$log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/fashionpay/'.date('Y_m/', $c['time']), "{$OId}-".rand(0,1000).".txt", $log);	//把返回数据写入文件
	}
}
?>