<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     payment
 * @subpackage  pingpong pingpong(pingpong)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class pingpong_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('pingpong', $this, '__config');
        $pluginManager->register('pingpong', $this, 'do_payment');
        $pluginManager->register('pingpong', $this, 'returnUrl');
		$pluginManager->register('pingpong', $this, 'notifyUrl');
    }
	
	function __config($data){
		return @in_array($data, array('do_payment', 'returnUrl', 'notifyUrl'))?'enable':'';
	}
     
    function do_payment($data){
		global $c;
		!in_array($data['order_row']['OrderStatus'], array(1, 3)) && js::location("/account/orders/view{$data['order_row']['OId']}.html");
		if(!in_array(strtoupper($data['order_row']['Currency']),array('AUD','BHD','CAD','CHF','CNY','EUR','GBP','HKD','INR','JOD','JPY','KRW','KWD','MOP','MYR','NZD','OMR','RUB','THB','TWD','USD'))){
			js::back('Does not support this currency transaction!');
		}
		$row=db::get_all('country', "CId in({$data['order_row']['ShippingCId']}, {$data['order_row']['BillCId']})", 'CId, Acronym');
		$Acronym_ary=array();
		foreach((array)$row as $v){
			$Acronym_ary[$v['CId']]=$v['Acronym'];
		}

		$clientId=$data['account']['clientId']; 
		$accId=$data['account']['accId'];
		$salt=$data['account']['salt'];
		// $clientId='2018092619314410015'; //$data['account']['clientId']
		// $accId='2018092619314410015133';
		// $salt='F52DA6EE004F3FE3E79C93DE';
		// clientId:2018092619314410015
		// accId:2018092619314410015133
		// salt:F52DA6EE004F3FE3E79C93DE

		// 测试信用卡卡号:4111 1111 1111 11111
		// 持卡人：任意三位以上字符，不能含数字
		// 卡过期时间：大于当前时间 如10/19
		$OId=$data['order_row']['OId'];
		$form_data=array( 
			//***************************必填***************************
			'clientId'					=>	$clientId,
			'accId'						=>	$accId,
			'signType'					=>	'MD5',
			// 'sign'						=>	'',
			'shopperResultUrl'			=>	"{$data['domain']}/cart/success/{$OId}.html?utm_nooverride=1",
			'amount'					=>	$data['total_price'],
			'currency'					=>	strtoupper($data['order_row']['Currency']),
			'merchantTransactionId'		=>	$OId,
			'goodsName'					=>	$OId,
			'goodsDesc'					=>	$OId,
			'cardHolderEmail'			=>	$data['order_row']['Email'],
			'cardHolderPhone'			=>	$data['order_row']['ShippingPhoneNumber'],
			'shippingFirstName'			=>	$data['order_row']['ShippingFirstName'],
			'shippingLastName'			=>	$data['order_row']['ShippingLastName'],
			'shippingPhone'				=>	$data['order_row']['ShippingPhoneNumber'],
			'shippingEmail'				=>	$data['order_row']['Email'],
			'shippingCountry'			=>	$Acronym_ary[$data['order_row']['ShippingCId']],
			'shippingState'				=>	$data['order_row']['ShippingState'],
			'shippingCity'				=>	$data['order_row']['ShippingCity'],
			'shippingStreet'			=>	$data['order_row']['ShippingAddressLine1'],
			'shippingPostcode'			=>	$data['order_row']['ShippingZipCode'],
			'billingFirstName'			=>	$data['order_row']['BillFirstName'],
			'billingLastName'			=>	$data['order_row']['BillLastName'],
			'billingPhone'				=>	$data['order_row']['BillPhoneNumber'],
			'billingEmail'				=>	$data['order_row']['Email'],
			'billingCountry'			=>	$Acronym_ary[$data['order_row']['BillCId']],
			'billingState'				=>	$data['order_row']['BillState'],
			'billingCity'				=>	$data['order_row']['BillCity'],
			'billingStreet'				=>	$data['order_row']['BillAddressLine1'],
			'billingPostcode'			=>	$data['order_row']['BillZipCode'],

			//***************************选填***************************
			'language'					=>	($c['lang']=='_zh_tw' ? 'cn' : 'en'),
			'registerUserEmail'			=>	$data['order_row']['Email'],
			// 'registerTime'				=>	'',
			// 'registerIp'				=>	'',
			// 'registerTerminal'			=>	'',
			// 'orderIp'					=>	'',
			// 'orderTerminal'				=>	'',
			'notificationUrl'			=>	"{$data['domain']}/payment/pingpong/notifyUrl/{$OId}.html?utm_nooverride=1",
		);
		ksort($form_data);
		$content='';
		foreach((array)$form_data as $k=>$v){
			if($v){
				$content.='&'.$k.'='.$v;
			}
		}
		$form_data['sign']=strtoupper(md5($salt.trim($content, '&')));
		$url='https://acquirer-pay.pingpongx.com/v1/checkout'; //支付网关
		// $url='https://test-acquirer-pay.pingpongx.com/v1/checkout'; //测试支付网关
		$curl=@curl_init(); 
		@curl_setopt($curl, CURLOPT_URL, $url);
		@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		@curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		@curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		@curl_setopt($curl, CURLOPT_REFERER, ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')?'https://':'http://').$_SERVER['HTTP_HOST']);
		@curl_setopt($curl, CURLOPT_POST, 1);
		@curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form_data));
		@curl_setopt($curl, CURLOPT_TIMEOUT, 300);
		@curl_setopt($curl, CURLOPT_HEADER, 0);
		@curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$result=@curl_exec($curl);
		@curl_close($curl);

		$result=str::json_data($result, 'decode');
		ob_start();
		print_r($_GET);
		print_r($_POST);
		print_r($form_data);
		print_r($result);
		$log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/pingpong/'.date('Y_m/', $c['time']), "{$OId}_payment_{$c['time']}.txt", $log);	//把返回数据写入文件

		if($result['code']=='0000'){
			js::location($result['paymentUrl']);
		}else{
			js::back($result['message']);
		}
		exit;
    } 
	
	function returnUrl($data){
		global $c;
	}
	
	function notifyUrl($data){
		global $c;
		
		$OId=trim($_POST['merchantTransactionId']);
		!$OId && js::location('/');
		$account=str::json_data(db::get_value('payment', "Method='PingPong'", 'Attribute'), 'decode');
		$result=str::str_code($_POST, 'trim');
		$clientId=$account['clientId']; 
		$accId=$account['accId'];
		$salt=$account['salt'];
		$domain=ly200::get_domain();
		$jumpUrl='/';

		//sign
		ksort($result);
		$content='';
		foreach((array)$result as $k=>$v){
			if($k!='sign'){
				$content.='&'.$k.'='.$v;
			}
		}
		$sign=strtoupper(md5($salt.trim($content, '&')));
		if($sign==$result['sign']){
			$jumpUrl="/cart/success/{$OId}.html";
			$order_row=db::get_one('orders', "OId='$OId' and OrderStatus in(1, 2, 3)");
			!$order_row && js::location('/');
			$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
			if($result['status']=='SUCCESS'){
				$error=orders::orders_payment_result(1, $UserName, $order_row, '');
			}elseif($result['status']=='FAILED'){
				$error=orders::orders_payment_result(0, $UserName, $order_row, '');
			}
			header("HTTP/1.1 200");
		}else{ //验证失败
			$error="Verification Failed";
		}
		
		ob_start();
		print_r($_GET);
		print_r($_POST);
		echo "\r\n\r\n$content";
		echo "\r\n\r\n$error";
		$log=ob_get_contents();
		ob_end_clean();
		file::write_file('/_pay_log_/pingpong/'.date('Y_m/', $c['time']), "{$OId}_notify.txt", $log);	//把返回数据写入文件
	}
}
?>