<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     app
 * @subpackage  paypal_dispute(Paypal争议)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**a52e1d9dcc06579804a56905f38e2379-us19
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class paypal_dispute_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager)
    {
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('paypal_dispute', $this, '__config'); 
        $pluginManager->register('paypal_dispute', $this, 'dispute_global');
		$pluginManager->register('paypal_dispute', $this, 'dispute_message_to_customer');
		$pluginManager->register('paypal_dispute', $this, 'dispute_escalate_to_claim');
		$pluginManager->register('paypal_dispute', $this, 'dispute_provide_evidence');
		$pluginManager->register('paypal_dispute', $this, 'dispute_refund');
        $pluginManager->register('paypal_dispute', $this, 'dispute_show_list');
        $pluginManager->register('paypal_dispute', $this, 'dispute_show_details');
    }
	
	function __config($data)
    {
		return @in_array($data, array('dispute_global', 'dispute_message_to_customer', 'dispute_escalate_to_claim', 'dispute_provide_evidence', 'dispute_refund', 'dispute_show_list', 'dispute_show_details'))?'enable':'';
	}
	
	function dispute_global($url)
    {
		//公共加载部分
		global $c;
		$Attribute = db::get_value('payment', 'Method="Excheckout"', 'NewAttribute');
		$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
		if (count($Account) < 2) {
            //快捷支付没有，去普通支付拿数据
			$Attribute = db::get_value('payment', 'Method="Paypal"', 'NewAttribute');
			$Account = str::json_data(htmlspecialchars_decode($Attribute), 'decode');
		}
		$client_id = $Account['ClientId'];
		$client_secret = $Account['ClientSecret'];
		//向Paypal请求
		$paypal_sdk = $c['root_path'] . '/static/themes/default/user/oauth/paypal_sdk_core/lib';
		include($paypal_sdk . '/common/PPApiContext.php');
		include($paypal_sdk . '/common/PPModel.php');
		include($paypal_sdk . '/common/PPUserAgent.php');
		include($paypal_sdk . '/common/PPReflectionUtil.php');
		include($paypal_sdk . '/common/PPArrayUtil.php');
		include($paypal_sdk . '/PPConfigManager.php');
		include($paypal_sdk . '/PPLoggingManager.php');
		include($paypal_sdk . '/PPHttpConfig.php');
		include($paypal_sdk . '/PPHttpConnection.php');
		include($paypal_sdk . '/PPLoggingLevel.php');
		include($paypal_sdk . '/PPConstants.php');
		include($paypal_sdk . '/transport/PPRestCall.php');
		include($paypal_sdk . '/exceptions/PPConnectionException.php');
		include($paypal_sdk . '/handlers/IPPHandler.php');
		include($paypal_sdk . '/handlers/PPOpenIdHandler.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdDispute.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdTokeninfo.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdUserinfo.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdAddress.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdError.php');
		include($paypal_sdk . '/auth/openid/PPOpenIdSession.php');
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
		$params = array(
			'grant_type'	=>	'client_credentials',
			'client_id'		=>	$client_id,
			'client_secret' =>	$client_secret
		);
		$token = PPOpenIdDispute::createFromAuthorizationCode($params, $apicontext);
		$access_token = $token->getAccessToken();
		return $access_token;
    }
	
	function dispute_message_to_customer($Data)
    {
		//商家向买家发送信息沟通
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
		$DisputeID = $Data['dispute_id'];
		$Content = stripslashes($Data['Content']);
		$params = array('message'=>$Content);
		$Return = PPOpenIdDispute::sendMessage($DisputeID, $params, $Data['access_token'], $apicontext);
        //对话记录
        $dispute_row = str::str_code(db::get_one('orders_paypal_dispute', "DisputeID='{$DisputeID}'"));
		$DId = $dispute_row['DId'];
        $dataTo = array(
            'DId'		=>	$DId,       //争议ID
            'PostedBy'	=>	0,          //发布者 SELLER:商家
            'Content'	=>	$Content,   //与通知事件相关的资源名称
            'Status'	=>	0,
            'AccTime'	=>	$c['time']  //创建事件通知的日期和时间
        );
        db::insert('orders_paypal_dispute_message', $dataTo);
		return $Return;
	}
	
	function dispute_escalate_to_claim($Data)
    {
		//商家直接要求进入PayPal仲裁阶段
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
		$DisputeID = $Data['dispute_id'];
		$Content = stripslashes($Data['Content']);
		$params = array('note'=>$Content);
		$Return = PPOpenIdDispute::escalateToClaim($DisputeID, $params, $Data['access_token'], $apicontext);
		return $Return;
	}
	
	function dispute_provide_evidence($Data)
    {
		//商家向paypal提供证据
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
		$DisputeID = $Data['dispute_id'];
		$Note = stripslashes($Data['Note']);
		$dispute_row = db::get_one('orders_paypal_dispute', "DisputeID='{$DisputeID}'");
		$DId = $dispute_row['DId'];
		//生成JSON文件
		$json_ary = array(
			'evidence_type'	=>	$Data['EvidenceType'],
			'evidence_info'	=>	array(
									'tracking_info'=>array(
										'carrier_name'=>$Data['CarrierName'],
										'tracking_number'=>$Data['TrackingNumber']
									)
								),
			'notes'			=>	$Note
		);
		$json = str::json_data($json_ary);
		ob_start();
		echo $json;
		$log = ob_get_contents();
		ob_end_clean();
		file::write_file('/u_file/evidence/', "{$DisputeID}.json", $log);//把返回数据写入文件
		//整理证据文件
		$type_ary = array('jpg'=>'image/jpeg', 'gif'=>'image/gif', 'png'=>'image/png', 'pdf'=>'application/pdf');
		$FilePath = $Data['EvidenceFile'];
		if (is_file($c['root_path'] . $FilePath)) {
			$ext_name = file::get_ext_name($FilePath);
			if ($type_ary[$ext_name]) {
				$FileType = $type_ary[$ext_name];
			} else {
				return '';
			}
		} else {
			return '';
		}
		//整理传递数据
		$boundary = uniqid();
		$delimiter = '-------------' . $boundary;
		$files = array(
			'input'=>array(
				'fileName'      =>  $c['root_path'] . 'u_file/evidence/'.$DisputeID.'.json',
				'Content-Type'  =>  'application/json'
			),
			'file1'=>array(
				'fileName'      =>  $c['root_path'] . $FilePath,
				'Content-Type'  =>  $FileType
			)
		);
		$data = '';
		foreach ($files as $name => $content) {
			$file_contents = file_get_contents($content['fileName']);
			$data .= "--" . $delimiter . "\r\n";
			$data .= "Content-Disposition: form-data; name=\"" . $name . "\"; filename=\"" . $content['fileName'] . "\"\r\n";
			$data .= "Content-Type: " . $content["Content-Type"] . "\r\n\r\n" . $file_contents . "\r\n";
		}
		$data .= "--" . $delimiter . "--\r\n";
		//执行函数
		//$Return = PPOpenIdDispute::provideEvidence($DisputeID, $delimiter, $data, $Data['access_token'], $apicontext);
        $url = "https://api.sandbox.paypal.com/v1/customer/disputes/{$DisputeID}/provide-evidence";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$Data['access_token'],
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($data)
            )
        ));
        $Return = curl_exec($curl);
        curl_close($curl);
		if ($Return) {
			$data=array(
				'Status'		=>	'UNDER_REVIEW',
				'EvidenceJson'	=>	addslashes($json),
				'EvidenceFile'	=>	$FilePath
			);
			db::update('orders_paypal_dispute', "DId='{$DId}'", $data);
		}
		return $Return;
	}
	
	function dispute_refund($Data)
    {
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));//sandbox | live
		$DisputeID = $Data['dispute_id'];
		$Note = stripslashes($Data['Note']);
		$dispute_row = db::get_one('orders_paypal_dispute', "DisputeID='{$DisputeID}'");
		$DId = $dispute_row['DId'];
		$shipAddress = array(
            'address_line_1'	=>	'14,Kimberly st',
            'address_line_2'	=>	'Open Road North',
            'country_code'		=>	'US',
            'admin_area_1'		=>	'Gotham City',
            'admin_area_2'		=>	'Gotham',
            'postal_code'		=>	'124566'
		);
		if ($dispute_row['Lifecycle'] == 'INQUIRY') {
			//提出解决争议的提议(协商阶段)
			$Type = 0;
			$params = array(
				'note'			=>	$Note, //'Offer refund with replacement item.',
				'offer_amount'	=>	array(
										'currency_code' =>  $dispute_row['Currency'],
										'value'         =>  $Data['Price']
									),
				'offer_type'	=>	$Data['OfferType'],
				'return_shipping_address'	=>	$shipAddress
			);
			$Return = PPOpenIdDispute::resolveDispute($DisputeID, $params, $Data['access_token'], $apicontext);
		} else {
			//接受索赔(仲裁阶段)
			$Type = 1;
			$params = array(
				'note'			=>	$Note, //'full refund with item return.',
				'refund_amount'	=>	array(
										'currency_code' =>  $dispute_row['Currency'],
										'value'         =>  $Data['Price']
									),
				'accept_claim_reason'	=>	$Data['ReasonType'],
				'return_shipping_address'	=>	$shipAddress
			);
			$Return = PPOpenIdDispute::acceptClaim($DisputeID, $params, $Data['access_token'], $apicontext);
		}
		if ($Return) {
			$Status = 1;
			if ($dispute_row['Lifecycle'] == 'INQUIRY' && $Data['OfferType'] == 'REFUND') {
                //提议 直接退款，进度完成
				$Status = 2;
			}
			if ($dispute_row['Lifecycle'] == 'CHARGEBACK') {
                //索赔 都是需要让Paypal进行审核
				db::update('orders_paypal_dispute', "DId='{$DId}'", array('Status'=>'UNDER_REVIEW'));
			}
			$data = array(
				'DId'			=>	$DId,//争议ID
				'Type'			=>	$Type,
				'Status'		=>	$Status,
				'Note'			=>	addslashes($Note),
				'Currency'		=>	$dispute_row['Currency'],
				'Amount'		=>	$Data['Price'],
				'OfferType'		=>	$Data['OfferType'],
				'ReasonType'	=>	$Data['ReasonType'],
				'ShippingAddress'=>	addslashes(str::json_data($shipAddress)),
				'AccTime'		=>	$c['time']
			);
			db::insert('orders_paypal_dispute_refund', $data);
			$RId = db::get_insert_id();
			$dataTo = array(
				'DId'		=>	$DId,
				'RId'		=>	$RId,
				'PostedBy'	=>	0,
				'Content'	=>	addslashes($Note),
				'Status'	=>	$Status,
				'AccTime'	=>	$c['time']
			);
			db::insert('orders_paypal_dispute_message', $dataTo);
		}
		return $Return;
	}
    
    function dispute_show_list($Data)
    {
		//查看列表信息
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
        $page = (int)$Data['page'];
		$Return = PPOpenIdDispute::listDisputes($page, $Data['access_token'], $apicontext);
		return $Return;
	}
    
    function dispute_show_details($Data)
    {
		//查看纠纷信息
		global $c;
		$apicontext = new PPApiContext(array('mode'=>$c['paypal']));
        $DisputeID = $Data['dispute_id'];
		$Return = PPOpenIdDispute::showDisputeDetails($DisputeID, $Data['access_token'], $apicontext);
		return $Return;
	}
}
?>