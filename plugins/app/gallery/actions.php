<?
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     app
 * @subpackage  gallery(卖家秀)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class gallery_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('gallery', $this, '__config'); 
        $pluginManager->register('gallery', $this, 'gallery_index');
		$pluginManager->register('gallery', $this, 'gallery_list');
    }
	
	function __config($data){
		return @in_array($data, array('gallery_index','gallery_list'))?'enable':'';
	}
     
    function gallery_index($data){
		global $c;
		include('gallery_index.php');		
    } 
	
	function gallery_list($data){
		global $c;
		include('gallery_list.php');
    } 
}
?>