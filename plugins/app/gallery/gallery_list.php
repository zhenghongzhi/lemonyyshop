<?php 
	$img_width = 230;
	$_GET['width']=='small' && $img_width = 188;
	$no_page_url=ly200::get_query_string(ly200::query_string('m, a, CateId, page'));
	$page_count = 15;
	$page=(int)$_GET['page'];
	$app_gallery_row = str::str_code(db::get_limit_page('app_gallery','1','*',$c['my_order'].'AppGId desc',$page,$page_count));
	if($app_gallery_row){
	?>
	<script type="text/javascript">$(document).ready(function(){plugins_obj.gallery_init();plugins_obj.gallery_list_init();});</script>
	<div class="title"><?=$c['lang_pack']['plugins']['gly_tit']; ?></div>
	<div class="s_title">&nbsp;</div>
	<div id="gly_list" class="gly_list BoardLayout">
		<div id="app_gallery_content">
			<?php foreach((array)$app_gallery_row[0] as $k=>$v){ 
				$img = $v['PicPath'];
				$img_info = @getimagesize($c['root_path'].$img);
				$img_h = $img_width/$img_info[0]*$img_info[1];
				$ProId = trim($v['ProId'],'|');
				$ProId && $ProId = (array)explode('|', $ProId);
				$pro_row=array();
				$ProId[0] && $pro_row = db::get_one('products',"ProId='{$ProId[0]}'");
				$url = ly200::get_url($pro_row,'products');
				$name = $pro_row['Name'.$c['lang']];
				$pro_img = ly200::get_size_img($pro_row['PicPath_0'],'240x240');
				?>
				<div class="item pin" data-id="<?=$k+(($app_gallery_row[2]-1)*$page_count);?>">
					<a href="javascript:;" data-gallery_img="<?=$img; ?>" data-pid="<?=$pro_row ? $pro_row['ProId'] : '0'; ?>" data-img="<?=$pro_row ? $pro_img : ''; ?>" data-name="<?=$pro_row ? htmlspecialchars($name) : ''; ?>" data-url="<?=$url; ?>" class="img" <?=$img_h ? "style='height:{$img_h}px;'" : ''; ?>>
						<img src="<?=$img; ?>" alt="" />
						<span class="view"><?=$c['lang_pack']['user']['view_more']; ?></span>
						<span class="bg"></span>
					</a>
					<?php if($pro_row){ ?>
						<div class="pro">
							<a href="<?=$url; ?>" class="pro_img pic_box" title="<?=$name; ?>"><img src="<?=$pro_img; ?>" alt="<?=$name; ?>" /><span></span></a>
							<a href="<?=$url; ?>" class="pro_name" title="<?=$name; ?>"><?=$name; ?></a>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			<?php
			$next=$app_gallery_row[2]+1>$app_gallery_row[3]?$app_gallery_row[3]:$app_gallery_row[2]+1;
			?>
			<input type="hidden" class="pageless_hide" value="<?=ly200::get_url_dir($_SERVER['REQUEST_URI'], '.html').$next.'.html?'.$no_page_url;?>" />
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
		    $.pageless.settings.complete = function(){
		        BoardLayout.newPins();
		    };
		    
		    var params = {'lazy':'0', 'type':'ajax', 'p': '1'};

		    $('#app_gallery_content').pageless({
		        "url":$('.pageless_hide:last').val(),
		        "totalPages": <?=$app_gallery_row[3];?>,
		        "currentPage": 1,
		        "loader":"LoadingPins",
		        "distance": 3000,
		        "marker": "",
		        "params":params
		    });
		});
	</script>
<?php } ?>