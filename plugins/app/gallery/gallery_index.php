<?=ly200::load_static('/static/themes/default/css/plugins.css','/static/themes/default/js/plugins.js'); ?>
<?php 
	$app_gallery_row = db::get_limit('app_gallery','1','*',$c['my_order'].'AppGId desc',0,12);
	if($app_gallery_row){
	?>
	<script type="text/javascript">$(document).ready(function(){plugins_obj.gallery_init()});</script>
	<div id="app_gallery" class="app">
		<div class="wide themes_box">
			<h2 class="title themes_title themes_box_title"><?=$c['lang_pack']['plugins']['gly_tit']; ?></h2>
			<div class="s_title themes_subtitle"><a href="/app/gallery/gallery_list/" title="<?=$c['lang_pack']['plugins']['gly_more']; ?>"><?=$c['lang_pack']['plugins']['gly_more']; ?></a></div>
			<div class="themes_box_main ind_gly_list gly_list">
				<?php
				$style = trim(db::get_value('config','GroupId = "app" and Variable = "gallery_index_style"','Value'));
				if($style=='style_1'){ 
					foreach((array)$app_gallery_row as $k=>$v){ 
					$img = $v['PicPath'];
					$img_info = @getimagesize($c['root_path'].$img);
					$ProId = trim($v['ProId'],'|');
					$ProId && $ProId = (array)explode('|', $ProId);
					$pro_row=array();
					$ProId[0] && $pro_row = db::get_one('products',"ProId='{$ProId[0]}'");
					$url = ly200::get_url($pro_row,'products');
					$name = $pro_row['Name'.$c['lang']];
					$pro_img = ly200::get_size_img($pro_row['PicPath_0'],'240x240');
				?>
					<div class="item <?=$k%6==0 ? 'fir' : ''; ?>">
						<a href="javascript:;" data-gallery_img="<?=$img; ?>" data-pid="<?=$pro_row ? $pro_row['ProId'] : '0'; ?>" data-img="<?=$pro_row ? $pro_img : ''; ?>" data-name="<?=$pro_row ? htmlspecialchars($name) : ''; ?>" data-url="<?=$url; ?>" class="img pic_box">
							<img src="<?=$img; ?>" style="<?=$img_info[0]<$img_info[1] ? 'width:100%;max-height:none;' : 'height:100%;max-width:none;'; ?>" alt="" />
							<span></span>
							<span class="view"><?=$c['lang_pack']['user']['view_more']; ?></span>
							<span class="bg"></span>
						</a>
					</div>
					<?php } ?>
				<?php }elseif($style=='style_2'){ 
					$style_ary = array(
						array(0,1,5,6),
						array(2),
						array(3,4,7,8)
					);
					foreach((array)$style_ary as $k => $v){
						?>
						<div class="item_box <?=$k==0 ? 'fir' : ''; ?> <?=$k==1 ? 'item_box_big' : ''; ?>">
							<?php foreach((array)$v as $k1 => $v1){ 
								$row =  $app_gallery_row[$v1];
								$img = $row['PicPath'];
								$img_info = @getimagesize($c['root_path'].$img);
								$ProId = trim($row['ProId'],'|');
								$ProId && $ProId = (array)explode('|', $ProId);
								$pro_row=array();
								$ProId[0] && $pro_row = db::get_one('products',"ProId='{$ProId[0]}'");
								$url = ly200::get_url($pro_row,'products');
								$name = $pro_row['Name'.$c['lang']];
								$pro_img = ly200::get_size_img($pro_row['PicPath_0'],'240x240');
								?>
								<div class="item <?=$k==0 && $k1%2==0 ? 'fir' : ''; ?>">
									<a href="javascript:;" data-gallery_img="<?=$img; ?>" data-pid="<?=$pro_row ? $pro_row['ProId'] : '0'; ?>" data-img="<?=$pro_row ? $pro_img : ''; ?>" data-name="<?=$pro_row ? htmlspecialchars($name) : ''; ?>" data-url="<?=$url; ?>" class="img pic_box">
										<img src="<?=$img; ?>" style="<?=$img_info[0]<$img_info[1] ? 'width:100%;max-height:none;' : 'height:100%;max-width:none;'; ?>" alt="" />
										<span></span>
										<span class="view"><?=$c['lang_pack']['user']['view_more']; ?></span>
										<span class="bg"></span>
									</a>
								</div>
							<?php } ?>
							<div class="clear"></div>
						</div>
					<?php } ?>
				<?php } ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<?php } ?>