<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     app
 * @subpackage  swap_chain(交换链接)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**a52e1d9dcc06579804a56905f38e2379-us19
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class swap_chain_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('swap_chain', $this, '__config'); 
        $pluginManager->register('swap_chain', $this, 'update_urls_list');
		$pluginManager->register('swap_chain', $this, 'get_urls_list');
    }
	
	function __config($data){
		return @in_array($data, array('update_urls_list','get_urls_list'))?'enable':'';
	}
	
    function update_urls_list(){
		global $c;
		$swap_chain=db::get_value('config',"GroupId='swap_chain' and Variable='public'",'Value');
		$swap_chain=str::json_data(htmlspecialchars_decode($swap_chain),'decode');
		if($c['time']-(int)$swap_chain['expire']<86400*3) return;	//3天更新一次数据
		$data=array('Action'=>'ueeshop_links_urls_list');	//拉取OA数据
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		$swap_chain_data=array();
		if($result) $swap_chain_data['data']=str::str_code($result['msg'], 'stripslashes');
		$swap_chain_data['expire']=$c['time'];
		$swap_chain_data=addslashes(str::json_data($swap_chain_data));
		manage::config_operaction(array('public'=>$swap_chain_data), 'swap_chain');
	}
	
	function get_urls_list(){
		global $c;
		$swap_chain=db::get_value('config',"GroupId='swap_chain' and Variable='public'",'Value');
		$swap_chain=str::json_data(htmlspecialchars_decode($swap_chain),'decode');
		$swap_chain_ueeseo=db::get_value('config',"GroupId='swap_chain' and Variable='ueeseo'",'Value');
		$swap_chain_ueeseo=str::json_data(htmlspecialchars_decode($swap_chain_ueeseo),'decode');
		$data=array_merge((array)$swap_chain['data'], (array)$swap_chain_ueeseo);
		if($data){
			$html='<div class="global_swap_chain"><div class="wide">'.$c['lang_pack']['plugins']['links'].':';
			foreach((array)$data as $v){
				if($v['Keyword']){
					$html.='<a href="'.$v['Url'].'" target="_blank">'.$v['Keyword'].'</a>';
				}
			}
			$html.='</div></div>';
			echo $html;
		}
	}
}
?>