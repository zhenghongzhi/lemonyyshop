<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     app
 * @subpackage  blog(交换链接)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**a52e1d9dcc06579804a56905f38e2379-us19
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class blog_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('blog', $this, '__config'); 
        $pluginManager->register('blog', $this, 'get_blog');
    }
	
	function __config($data){
		return @in_array($data, array('get_blog'))?'enable':'';
	}
	
    function get_blog(){
		global $c;
		if((int)$c['config']['global']['UeeSeo']==1 && !(int)$c['config']['global']['UeeSeoIsHidden']){
			$html='';
			$blog_row=str::str_code(db::get_limit('blog', '1', '*', $c['my_order'].'AId desc', 0, 3));
			$html.='<div id="ueeseo_blog">';
				$html.='<div class="wide themes_box">	';
					$html.='<h2 class="themes_title themes_box_title">From Our Blogs</h2>';
					$html.='<div class="themes_box_main blog_list">';
						foreach((array)$blog_row as $k=>$v){
							$name=$v['Title'];
							$url=ly200::get_url($v, 'blog');
							$time=@date('M d, Y', $v['AccTime']);
							$img=$v['PicPath'];
							$html.='<div class="list '.($k==0 ? 'first' : '').'">';
								$html.='<a class="blog_img" href="'.$url.'" />';
									$html.='<img src="'.$img.'" alt="'.$name.'" />';
								$html.='</a>';
								$html.='<div class="blog_time themes_subtitle">'.$time.'</div>';
								$html.='<a href="'.$url.'" class="blog_title themes_title">'.$name.'</a>';
							$html.='</div>';
						}
						$html.='<div class="clear"></div>';
					$html.='</div>';
				$html.='</div>';
			$html.='</div>';
			echo $html;
		}
	}
}
?>