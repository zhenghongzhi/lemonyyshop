<?php
/**
 * 这是一个很牛逼的插件实现
 * 
 * @package     app
 * @subpackage  mailchimp(邮件群发)
 * @category    payment
 * @author      鄙人
 * @link        http://www.ueeshop.com/
 */
/**a52e1d9dcc06579804a56905f38e2379-us19
 * 需要注意的几个默认规则：
 * 1. 本插件类的文件名必须是action
 * 2. 插件类的名称必须是{插件名_actions}
 */
class mailchimp_actions 
{ 
    //解析函数的参数是pluginManager的引用 
    function __construct(&$pluginManager){
        //注册这个插件 
        //第一个参数是钩子的名称 
        //第二个参数是pluginManager的引用 
        //第三个是插件所执行的方法 
        $pluginManager->register('mailchimp', $this, '__config'); 
        $pluginManager->register('mailchimp', $this, 'do_send');
    }
	
	function __config($data){
		return @in_array($data, array('do_send','get_key_info'))?'enable':'';
	}
}
?>