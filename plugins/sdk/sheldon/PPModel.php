<?php 
class PPModel
{
	private $_propMap = array();	
		
	public function __get($key)
    {
		return $this->_propMap[$key];
	}
	
	public function __set($key, $value)
    {
		$this->_propMap[$key] = $value;
	}
	
	private function _convertToArray($param)
    {
		$ret = array();		
		foreach($param as $k => $v) {
			if($v instanceof PPModel ) {				
				$ret[$k] = $v->toArray();
			} else if (is_array($v)) {
				$ret[$k] = $this->_convertToArray($v);
			} else {
				$ret[$k] = $v;
			}
		}
		return $ret;
	}
	
	public function fromArray($arr)
    {
		if(!empty($arr)) {
			foreach($arr as $k => $v) {
				$this->$k = $v;
			}
		}
	}
	
	public function fromJson($json)
    {
		$this->fromArray(json_decode($json, true));
	}
	
	public function toArray()
    {		
		return $this->_convertToArray($this->_propMap);
	}
	
	public function toJSON()
    {		
		return json_encode($this->toArray());
	}
}