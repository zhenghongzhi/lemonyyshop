<?php
class PPHttpConnection
{
	private $httpConfig;

	public function __construct($httpConfig, $config)
	{
		if (!function_exists("curl_init")) {
            str::dump("Curl module is not available on this system");
		}
		$this->httpConfig = $httpConfig;
	}

	private function getHttpHeaders()
    {
		$ret = array();
		foreach ($this->httpConfig->getHeaders() as $k => $v) {
			$ret[] = "$k: $v";
		}
		return $ret;
	}

	public function execute($data) {
        str::dump('开始执行CURL');
        str::dump("连接到 " . $this->httpConfig->getUrl());
        str::dump("数据 " . $data);
		$ch = curl_init($this->httpConfig->getUrl());
		curl_setopt_array($ch, $this->httpConfig->getCurlOptions());
		curl_setopt($ch, CURLOPT_URL, $this->httpConfig->getUrl());
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHttpHeaders());
		switch ($this->httpConfig->getMethod()) {
			case 'POST':
				curl_setopt($ch, CURLOPT_POST, true);
			case 'PUT':
			case 'PATCH':
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				break;
		}
		if ($this->httpConfig->getMethod() != NULL) {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->httpConfig->getMethod()); //注明请求方式 GET POST
		}
		foreach ($this->getHttpHeaders() as $header) {
            str::dump("添加头部信息 $header");
		}
        str::dump('正在执行CURL');
        str::dump($data);
		$result = curl_exec($ch);
		str::dump($result);
		$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$retries = 0;
        str::dump('结束执行CURL');
		curl_close($ch);
        
		return $result;
	}

}
