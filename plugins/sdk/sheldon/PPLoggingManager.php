<?php
class PPLoggingManager
{

    // Default Logging Level
    const DEFAULT_LOGGING_LEVEL = 0;

    // Logger name
    private $loggerName;

    // Log enabled
    private $isLoggingEnabled;

    // Configured logging level
    private $loggingLevel;

    // Configured logging file
    private $loggerFile;

    public function __construct($loggerName, $config = null) {
        $this->loggerName = $loggerName;
        $config = PPConfigManager::getConfigWithDefaults($config);

        $this->isLoggingEnabled = (array_key_exists('log.LogEnabled', $config) && $config['log.LogEnabled'] == '1');

        if($this->isLoggingEnabled) {
            $this->loggerFile = ($config['log.FileName']) ? $config['log.FileName'] : ini_get('error_log');
            $loggingLevel = strtoupper($config['log.LogLevel']);
            $this->loggingLevel = (isset($loggingLevel) && defined("PPLoggingLevel::$loggingLevel")) ? constant("PPLoggingLevel::$loggingLevel") : PPLoggingManager::DEFAULT_LOGGING_LEVEL;
        }
    }

    private function log($message, $level=PPLoggingLevel::INFO) {
        if($this->isLoggingEnabled && ($level <= $this->loggingLevel)) {
            error_log( $this->loggerName . ": $message\n", 3, $this->loggerFile);
        }
    }

    public function error($message) {
        $this->log($message, 0);
    }

    public function warning($message) {
        $this->log($message, 1);
    }

    public function info($message) {
        $this->log($message, 2);
    }

    public function fine($message) {
        $this->log($message, 3);
    }

}
