<?php
class PPConfigManager
{
	private $config;
	
	//default config values
	public static $defaults = array(
		"http.ConnectionTimeOut" => "30",
		"http.Retry" => "5",
	);
	
	public static function getConfigWithDefaults($config=null)
	{
		return array_merge(PPConfigManager::$defaults, $config);
	}
}
