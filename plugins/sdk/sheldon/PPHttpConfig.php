<?php
class PPHttpConfig
{
	public static $DEFAULT_CURL_OPTS = array(
		CURLOPT_SSLVERSION => 1,
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_TIMEOUT        => 60,	// maximum number of seconds to allow cURL functions to execute
		CURLOPT_USERAGENT      => 'PayPal-PHP-SDK',
		CURLOPT_HTTPHEADER     => array(),
		CURLOPT_SSL_VERIFYHOST => 2,
		CURLOPT_SSL_VERIFYPEER => 1,
		CURLOPT_SSL_CIPHER_LIST => 'TLSv1',
	);
	
	const HEADER_SEPARATOR = ';';	
	const HTTP_GET = 'GET';
	const HTTP_POST = 'POST';

	private $headers = array();

	private $curlOptions;

	private $url;

	private $method;
    
	private $retryCount;
    
	public function __construct($url=null, $method=self::HTTP_POST)
    {
		$this->url = $url;
		$this->method = $method;
		$this->curlOptions = self::$DEFAULT_CURL_OPTS;
		$curlVersion = curl_version();
		if (@substr_count(@strtoupper($curlVersion['ssl_version']), 'NSS/')) unset($this->curlOptions[CURLOPT_SSL_CIPHER_LIST]);
	}
	
	public function getUrl()
    {
		return $this->url;
	}
	
	public function getMethod()
    {
		return $this->method;
	}
	
	public function getHeaders()
    {
		return $this->headers;
	}
	
	public function getHeader($name)
    {
		if (array_key_exists($name, $this->headers)) {
			return $this->headers[$name];
		}
		return NULL;
	}

	public function setUrl($url)
    {
		$this->url = $url;
	}
	
	public function setHeaders(array $headers)
    {
		$this->headers = $headers;
	}

	public function addHeader($name, $value, $overWrite=true)
    {
		if (!array_key_exists($name, $this->headers) || $overWrite) {
			$this->headers[$name] = $value;
		} else {
			$this->headers[$name] = $this->headers[$name] . self::HEADER_SEPARATOR . $value;			
		}
	}
	
	public function removeHeader($name)
    {
		unset($this->headers[$name]);
	}
	
	public function getCurlOptions()
    {
		return $this->curlOptions;
	}

	public function addCurlOption($name, $value)
    {
		$this->curlOptions[$name] = $value;
	}

	public function setCurlOptions($options)
    {
		$this->curlOptions = $options;
	}
    
	public function setSSLCert($certPath, $passPhrase=NULL)
    {		
		$this->curlOptions[CURLOPT_SSLCERT] = realpath($certPath);
		if (isset($passPhrase) && trim($passPhrase) != "") {
			$this->curlOptions[CURLOPT_SSLCERTPASSWD] = $passPhrase;
		}
	}
    
	public function setHttpTimeout($timeout)
    {
		$this->curlOptions[CURLOPT_CONNECTTIMEOUT] = $timeout;
	}
    
	public function setHttpProxy($proxy)
    {
		$urlParts = parse_url($proxy);
		if ($urlParts == false || !array_key_exists("host", $urlParts))
			throw new PPConfigurationException("Invalid proxy configuration ".$proxy);

		$this->curlOptions[CURLOPT_PROXY] = $urlParts["host"];
		if (isset($urlParts["port"]))
			$this->curlOptions[CURLOPT_PROXY] .=  ":" . $urlParts["port"];
		if (isset($urlParts["user"]))
			$this->curlOptions[CURLOPT_PROXYUSERPWD] = $urlParts["user"] . ":" . $urlParts["pass"];
    }
    
	public function setHttpRetryCount($retryCount)
    {
		$this->retryCount = $retryCount;
	}	

	public function getHttpRetryCount()
    {
		return $this->retryCount;
	}
	
	public function setUserAgent($userAgentString)
    {
		$this->curlOptions[CURLOPT_USERAGENT] = $userAgentString;
	}
}
