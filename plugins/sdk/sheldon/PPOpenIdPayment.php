<?php

/**
 * Token grant resource
 */
class PPOpenIdPayment extends PPModel
{	
    public static function createPayment($params, $access_token, $apiContext=null)
    {
        if (is_null($apiContext)) {
            $apiContext = new PPApiContext();
        }
        
        $call = new PPRestCall($apiContext);
        $token = new PPOpenIdPayment();
        
        $result = $call->execute("/v1/payments/payment", "POST", json_encode($params),
            array(
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer '.$access_token,
                'PayPal-Partner-Attribution-Id' => 'ueeshop_Cart'
            )
        );
        str::dump($result);
        str::dump('处理JSON格式');
        $token->fromJson($result);
        return $token->toJSON();
    }
}
