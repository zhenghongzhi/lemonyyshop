<?php
class PPRestCall
{
	private $logger;
	
	private $apiContext;

	public function __construct($apiContext)
    {
		$this->apiContext = $apiContext;
        str::dump('开始执行 PPRestCall->__construct');
	}
    
	public function execute($path, $method, $data='', $headers=array())
    {
		$config = $this->apiContext->getConfig(); //获取apiContext配置数据
        str::dump('开始执行 PPRestCall->execute');
        str::dump($config);
        
        $Url = 'https://api.sandbox.paypal.com' . $path; //Paypal请求地址
		$httpConfig = new PPHttpConfig($Url, $method);
		$httpConfig->setHeaders($headers + 
			array(
				'Content-Type' => 'application/json'
			)	
		);
        
        str::dump('正在执行 PPRestCall->execute');
        str::dump($httpConfig);
        str::dump($config);
		$connection = new PPHttpConnection($httpConfig, $config);
		$response = $connection->execute($data);
		str::dump('结束执行 PPRestCall->execute');
        
		return $response;
	}
	
}
