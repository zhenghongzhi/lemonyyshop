<?php
include('../../../inc/global.php');

$Code=$_GET['code'];
$PlatformName='Wish';
$auth_row=db::get_one('authorization', 'Platform="wish"');
if($auth_row){
	$TokenAry=str::json_data(htmlspecialchars_decode($auth_row['Token']), 'decode');
	$TokenAry['code']=$Code;
	$Token=addslashes(str::json_data(str::str_code($TokenAry, 'stripslashes')));
	db::update('authorization', 'Platform="wish"', array('Token'=>$Token));
	$tips=$PlatformName."授权成功！";
	
	//Obtaining the Access Token
	$data=array(
		'ApiKey'		=>	'ueeshop_sync',
		'Number'		=>	$c['Number'],
		'ApiName'		=>	'wish',
		'Action'		=>	'authorization',
		'client_id'		=>	$TokenAry['client_id'],
		'client_secret'	=>	$TokenAry['client_secret'],
		'code'			=>	$TokenAry['code'],
		'redirect_uri'	=>	$TokenAry['redirect_uri'],
		'timestamp'		=>	$c['time']
	);
	$data['sign']=ly200::sign($data, $c['ApiKey']);
	$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
	if($result['ret']==1){
		echo '<script>closeWindow(10);</script>';
	}
}else{
	$tips=$PlatformName."授权失败，保存账号不存在！";
}

echo ly200::load_static('/static/js/jquery-1.7.2.min.js');
?>
<style>
body, html{background:#fff;}
.authorization{width:680px; margin:20px auto; text-align:center; font-size:16px;}
.authorization .tips_box{padding:12px 0; text-align:center; line-height:32px;}
.authorization .close_box{padding-top:20px;}
.authorization .close_box span{color:#333;}
.authorization .close_box a.close{text-decoration:none; color:#09C;}
.authorization .error{color:#f00;}
</style>
<div class="authorization">
	<div class="tips_box"><?=$tips;?></div>
	<div class="close_box"><span>5</span> 秒后，该页面自动关闭...<a href="javascript:;" class="close">立即关闭</a></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('title').text('<?=$tips;?>');
	//自动保存店铺名称
	window.opener && window.opener.submitPlatformAuth && window.opener.submitPlatformAuth();
	
	function closeWindow(num){
		$('.authorization .close_box>span').text(num);
		if(num--==0){
			window.opener=null;
			window.open('','_self');
			window.close();
		}else{
			setTimeout(function(){closeWindow(num);}, 1000);
		}
	}
	$('.close').click(function(){closeWindow(0);});
	closeWindow(60);
	//setTimeout(function(){closeWindow();}, 5000);
	
});
</script>
<?php exit;?>
