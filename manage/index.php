<?php
include('../inc/global.php');
include('static/inc/init.php');
$YY_code="11";
$dd=ly200::getyy_balance($YY_code);
ob_start();

$isHome=($c['manage']['module']=='account'?menu_expired1:0);
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta content="telephone=no" name="format-detection" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex,nofollow">
<meta name="renderer" content="webkit">
<link rel="shortcut icon" href="<?=$c['manage']['config']['IcoPath'];?>" />
<title>{/frame.system_name/}</title>
<?php
echo ly200::load_static('/static/css/global.css', '/static/manage/css/frame.css', '/static/manage/css/animate.css');
if($c['FunVersion']>=10) echo ly200::load_static('/static/css/cdx_global.css', '/static/manage/css/cdx_frame.css');
if($c['manage']['module']=="set" && $c['manage']['action']=="photo"){
	echo ly200::load_static('/static/js/jquery-1.8.3.js'); // 图片银行插件用的jq版本
}else if($c['manage']['module']=="account" && ($c['manage']['action']=="welfare" || $c['manage']['action']=="index" && db::get_row_count('config', 'GroupId="guide_pages" and Value="0"'))){
	echo ly200::load_static('/static/js/jquery-1.10.2.js'); // 视频播放要用新版的jq
}else{
	echo ly200::load_static('/static/js/jquery-1.7.2.min.js');
}
?>
<?=ly200::load_static("/static/js/lang/{$c['manage']['config']['ManageLanguage']}.js", '/static/js/global.js', '/static/manage/js/frame.js');?>
<?php if($c['manage']['is_plugins']){//判断是否是插件
	echo ly200::load_static('/static/manage/css/plugins.css', '/static/manage/js/plugins.js');
	if($c['FunVersion']>=10) echo ly200::load_static('/static/manage/css/cdx_plugins.css');
}else{
	echo ly200::load_static("/static/manage/css/{$c['manage']['module']}.css", "/static/manage/js/{$c['manage']['module']}.js");
	if($c['FunVersion']>=10) echo ly200::load_static("/static/manage/css/cdx_{$c['manage']['module']}.css");
}?>
<?=ly200::load_static('/static/js/plugin/tool_tips/tool_tips.js', '/static/js/plugin/jscrollpane/jquery.mousewheel.js', '/static/js/plugin/jscrollpane/jquery.jscrollpane.js', '/static/js/plugin/jscrollpane/jquery.jscrollpane.css');?>
<style type="text/css">body,html,h1,h2,h3,h4,h5,h6,input,select,textarea{<?=$c['manage']['config']['ManageLanguage']=='zh-cn'?'font-family:"微软雅黑";':'font-size:12px;';?>}</style>
<script type="text/javascript">
var session_id='<?=session_id();?>';
var ueeshop_config={"domain":"<?=ly200::get_domain();?>","curDate":"<?=date('Y/m/d H:i:s', $c['time']);?>","lang":"<?=substr($c['manage']['web_lang'], 1);?>","manage_language":"<?=$c['manage']['config']['ManageLanguage'];?>","currency":"<?=$c['manage']['currency_symbol'];?>","currSymbol":"<?=$c['manage']['currency_symbol'];?>","language":<?=str::json_data($c['manage']['config']['Language']);?>,"UserName":"<?=$_SESSION['Manage']['UserName'];?>","prodImageCount":"<?=$c['prod_image']['count'];?>","u_file_size":"<?=$_SESSION['u_file_check']['size']?$_SESSION['u_file_check']['size']:0;?>","FunVersion":"<?=$c['FunVersion'];?>"};
$(document).ready(function(){
	frame_obj.page_init();
	frame_obj.windows_init();
	<?php
	if((int)$c['manage']['config']['PromptSteps']){
		db::update('config', "GroupId='global' and Variable='PromptSteps'", array('Value'=>0));
		echo 'frame_obj.prompt_steps();';
	}
	?>
});
</script>
</head>

<body class="<?=$c['manage']['config']['ManageLanguage'];?>">
<?php
if($c['manage']['action']=='login'){
	include('account/login.php');
}elseif($c['manage']['iframe']==1){	//弹窗
	include("{$c['manage']['module']}/{$c['manage']['action']}.php");
}else{
	if($c['FunVersion']>=10){
		$LogoPath='/static/manage/images/frame/cdx/logo.png';
	}else{
		$LogoPath='/static/manage/images/frame/logo.png';
	}
	//(int)$c['UeeshopAgentId'] && $LogoPath='http://a.vgcart.com/agent/?do_action=action.agent_logo&AgentId='.(int)$c['UeeshopAgentId'];	//代理商LOGO
	
	$order_where='';
	(int)$_SESSION['Manage']['GroupId']==3 && $order_where.=" and ((o.SalesId>0 and o.SalesId='{$_SESSION['Manage']['UserId']}') or (o.SalesId=0 and u.SalesId='{$_SESSION['Manage']['UserId']}'))";//业务员账号过滤
	$order_wait_payment_count = (int)db::get_row_count('orders o left join user u on o.UserId=u.UserId', 'o.OrderStatus=1'.$order_where);
	$order_wait_delivery_count = (int)db::get_row_count('orders o left join user u on o.UserId=u.UserId', 'o.OrderStatus=4'.$order_where);
	$prod_warning_count = (int)db::get_row_count('products', "Stock<={$c['manage']['warning_stock']}" . $c['manage']['where']['products']);
	if((int)$c['FunVersion'] && manage::check_permit('user') && manage::check_permit('user', 0, array('a'=>'inbox'))){
		if(in_array('product_inbox', (array)$c['manage']['plugins']['Used'])){//APP应用开启产品咨询
			$unread_where='(Module in ("products", "others") and IsRead=0 and UserId>0) or (Module="products" and IsRead=0 and UserId=0 and CusEmail!="")';
		}else{
			$unread_where='Module="others" and IsRead=0 and UserId>0';
		}
		$inbox_noread_count=(int)db::get_row_count('user_message', $unread_where);
	}
	$inbox_where='';
	(int)$_SESSION['Manage']['GroupId']==3 && $inbox_where.=" and (u.SalesId>0 and u.SalesId='{$_SESSION['Manage']['UserId']}')";//业务员账号过滤
	$inbox_noread_orders_count=(int)db::get_row_count('user_message m left join user u on m.UserId=u.UserId', 'Module="orders" and Type=0 and IsRead=0'.$inbox_where);
	
	$caigou_count=(int)db::get_row_count('request_message m left join user u on m.UserId=u.UserId', 'Module="purchase" and Type=0 and IsRead=0'.$inbox_where);
	
	$error_count=$order_wait_payment_count+$order_wait_delivery_count+$prod_warning_count+$inbox_noread_count+$caigou_count;
?>
	<div id="header">
<audio src="./message.mp3" id="myaudio" controls="controls"  hidden="true" >

    </audio>
	 <input type="hidden" onclick="autoPlay()" value="播放" />
    <input type="hidden" onclick="closePlay()" value="关闭" />
    <input type="hidden" id="count" name="count" value="<?=$error_count ?>"> 
		<div class="logo pic_box"><a href="./"><img src="<?=$LogoPath;?>" /></a><span></span></div>
        <?php if($c['FunVersion']==10){?><div class="free_logo">{/global.free_ver/}</div><?php }?>
		<ul class="menu">
			<?php
            //30天提示即将过期
            $analytics_row = ly200::ueeshop_web_get_data(); //加载统计json
            $expiredTime = strtotime($analytics_row['ExpiredTime']);
            $IsExpiredTime = (($expiredTime - 30*86400 < $c['time']) && ($expiredTime - 30*86400 > 0)) ? 1 : 0;
            if ($IsExpiredTime) {
                $expiredDays = intval(($expiredTime - $c['time']) / 86400); 
			?>
<!--				<li class="menu_expired"><a href="javascript:;">--><?//=str_replace(array('%days%', '%expiredTime%'), array($expiredDays, $analytics_row['ExpiredTime']), $c['manage']['lang_pack']['global']['expired_tips']);?><!--</a></li>-->
			<?php }?>
			<?php if((int)$c['manage']['config']['UeeSeo']==1){?><li class="menu_seo"><a href="./?iframe=1&m=account&a=seo" target="_blank"><span>SEO 2.0</span></a></li><?php }?>
			<?php if($c['FunVersion']<10){?><li class="menu_welfare"><a href="./?m=account&a=welfare"><span>{/global.welfare/}</span><em>New</em></a></li><?php }?>
			<?php if($c['FunVersion']>=10 && $analytics_row['Service']['Wechat']){?>
            	<?php if($c['FunVersion']==10){?>
                <li class="menu_upgrade">
                    <a href="javascript:;"><span>{/global.free_upgrade/}</span></a>
                    <div class="ug_drop blue_line_style">
                        <div class="top_btn">{/global.upgrade_more/}</div>
                        <div class="item">{/global.upgrade_benf.0/}</div>
                        <div class="item">{/global.upgrade_benf.1/}</div>
                        <div class="item">{/global.upgrade_benf.2/}</div>
                        <div class="item">{/global.upgrade_benf.3/}</div>
                        <div class="item">{/global.upgrade_benf.4/}</div>
                        <div class="item">{/global.upgrade_benf.5/}</div>
                        <div class="item">{/global.upgrade_benf.6/}</div>
                        <span class="sLine s1"></span>
                        <span class="sLine s2"></span>
                    </div>
                </li>
                <?php }?>
                <li class="menu_qrcode">
                    <a href="javascript:;"><span>{/global.wechat_ser/}</span></a>
                    <div class="qrcode_box">
                        <div class="txt">{/global.scan_code/}</div>
                        <div class="txt blue">{/global.free_service/}</div>
                        <div class="image"><img src="<?=$analytics_row['Service']['WechatQrCode'];?>" /></div>
                        <div class="txt">{/global.wechat_chuang/} : <?=$analytics_row['Service']['Wechat'];?></div>
                    </div>
                </li>
            <?php }?>
            <li class="menu_help"><a href="<?=$c['FunVersion']>=10?'http://help.shop.shopcto.com/':'http://help.shop.ueeshop.com';?>" target="_blank"><span>{/global.help/}</span></a></li>
			<?php if(!$c['UeeshopAgentId']){
				manage::ueeshop_web_get_service_data();
			} ?>
			<li class="menu_home"><a href="<?=ly200::get_domain();?>" target="_blank"><em class="icon_head_menu_home"></em></a></li>
			<li class="menu_message menu_down">
			
			 
				<a href="javascript:;"><em class="icon_head_menu_message"></em><span class="message_count"><?=$error_count<99?$error_count:'99+';?></span></a>
				<div class="message_info">
					<dl class="drop_down">
						<?php if(manage::check_permit('orders') && manage::check_permit('orders', 0, array('a'=>'orders'))){?>
							<dt class="item"><a href="./?m=orders&a=orders&OrderStatus=1"><span>待付定金订单</span><i id="order_wait_payment_count"><?=$order_wait_payment_count;?></i></a></dt>
							<dt class="item"><a href="./?m=orders&a=orders&OrderStatus=4"><span>待付余款订单</span><i  id="order_wait_delivery_count"><?=$order_wait_delivery_count;?></i></a></dt>
							
							<dt class="item"><a href="./?m=orders&a=source_request"><span>采购订单消息</span><i id="caigou_count"><?=$caigou_count;?></i></a></dt>
							
							
							<?php /*
							<dt class="item"><a href="./?m=orders&a=orders&OrderStatus=1"><span>待付余款订单</span><i><?=$order_wait_payment_count;?></i></a></dt>
							<dt class="item"><a href="./?m=orders&a=orders&OrderStatus=1"><span>{/account.wait_payment_order/}</span><i><?=$order_wait_payment_count;?></i></a></dt>
							
							<dt class="item"><a href="./?m=orders&a=orders&OrderStatus=4"><span>{/account.wait_delivery_order/}</span><i><?=$order_wait_delivery_count;?></i></a></dt>
							*/?>
						<?php }?>
						<dt class="item"><a href="./?m=products&a=products&Other=6"><span>{/account.stock_warning/}</span><i id="prod_warning_count"><?=$prod_warning_count;?></i></a></dt>
						<?php if((int)$c['FunVersion'] && manage::check_permit('user') && manage::check_permit('user', 0, array('a'=>'inbox'))){?><dt class="item"><a href="./?m=user&a=inbox&d=products"><span>{/module.user.inbox/}</span><i><?=$inbox_noread_count;?></i></a></dt><?php }?>
						<dt class="item"><a href="javascript:;" class="btn_orders_message"><span>{/account.orders_messages/}</span><i id="inbox_noread_orders_count"><?=$inbox_noread_orders_count;?></i></a></dt>
					</dl>
				</div>
			</li>
			<li class="menu_user">
				<dl class="user_info fl">
					<dt><strong><?=$_SESSION['Manage']['UserName'];?></strong><em></em></dt>
					<dd>
						<div class="drop_down">
							<a class="item" href="?m=set&a=manage&d=edit&UserId=<?=$_SESSION['Manage']['UserId'];?>">{/account.change_password/}</a>
							<a class="item" href="?do_action=account.logout">{/account.logout/}</a>
						</div>
					</dd>
				</dl>
			</li>
		</ul>
	</div>
	<div id="main">
		<div class="fixed_loading">
			<div class="load">
				<div><div class="load_image"><?=$c['FunVersion']>=10?'创店':'UEESHOP';?></div><div class="load_loader"></div></div>
			</div>
		</div>
		<div class="menu <?=$c['FunVersion']>=10?'free_version_menu':'oth_version_menu';?>">
			<div class="menu_ico home_menu_ico">
				<?php
				//规划左侧栏
				$menu_name_ary=array();
				$host_ary=explode('.', $_SERVER['HTTP_HOST']);
				$mobile_link_url=' src="http://m.'.(in_array(reset($host_ary), ly200::subdomain_list())?implode('.', array_slice($host_ary, 1)):$_SERVER['HTTP_HOST']).'"';
				foreach($c['manage']['permit'] as $k=>$v){
					if($k=='mobile' || !manage::check_permit($k)) continue;
					$url=($k=='mpreview'?'javascript:;':"./?m={$k}");
					$mLink=($k=='mpreview'?$mobile_link_url:'');
					$menu_name_ary[]=array($c['manage']['lang_pack']['module'][$k]['module_name'],$url);
				?>
					<div class="menu_item" data-title="{/module.<?=$k;?>.module_name/}">
						<a href="<?=$url;?>"<?=$mLink;?>>
							<i class="icon_menu icon_menu_<?=$k;?><?=$c['manage']['module']==$k?' current':'';?>"><span>{/module.<?=$k;?>.module_name/}</span></i>
						</a>
					</div>
				<?php }?>
			</div>
            <?php if($c['FunVersion']>=10){?>
			<div class="menu_ico_name">
				<?php
				foreach($menu_name_ary as $k=>$v){
					echo "<div class='menu_item'><a href='{$v[1]}'>{$v[0]}</a></div>";
				}
				?>
			</div>
            <?php }?>
			<?php if(!in_array($c['manage']['module'], array('account', 'email', 'mobile', 'manage')) && @!in_array($c['manage']['module'], $un_menu_ary[$c['FunVersion']][0])){?>
				<div class="menu_list">
					<div class="menu_title">{/module.<?=$c['manage']['module'];?>.module_name/}</div>
					<dl>
						<?php
						foreach($c['manage']['permit'][$c['manage']['module']] as $k=>$v){
							if(!manage::check_permit($c['manage']['module'], 0, array('a'=>$k))) continue;
							$action_ary=@explode('.', $c['manage']['action']);
						?>
							<dt<?=((reset($action_ary)==$k && !is_numeric($k)) || $c['manage']['action']==$v||in_array($c['manage']['action'], $v["permit"]))?' class="current"':'';?>><?=isset($v['menu'])?'<a href="./?m='.$c['manage']['module'].'&a='.$k.'"><span>{/module.'.$c['manage']['module'].'.'.$k.'.module_name/}</span></a>':'<a href="./?m='.$c['manage']['module'].'&a='.$k.'"><span>{/module.'.$c['manage']['module'].'.'.$k.'/}</span></a>';?></dt>
						<?php }?>
					</dl>
				</div>
			<?php }?>
            <?php if($c['FunVersion']>=10){?>
			<div class="new_menu_list">
				<?php
				$host_ary=explode('.', $_SERVER['HTTP_HOST']);
				$mobile_link_url=' src="http://m.'.(in_array(reset($host_ary), ly200::subdomain_list())?implode('.', array_slice($host_ary, 1)):$_SERVER['HTTP_HOST']).'"';
				foreach($c['manage']['permit'] as $k=>$v){
					if($k=='mobile' || !manage::check_permit($k)) continue;
					if($c['FunVersion']==10 && $k=='sales') continue;
					$url=($k=='mpreview'?'javascript:;':"./?m={$k}");
					$mLink=($k=='mpreview'?$mobile_link_url:'');
					$isOpen=$c['manage']['module']==$k;
				?>
				<div class="menu_item <?=$isOpen?'open':'';?>">
					<div class="item_name">
						<i class="icon_menu icon_menu_<?=$k;?><?=$isOpen?' current':'';?>"></i>
						<?=$c['manage']['lang_pack']['module'][$k]['module_name'];?>
					</div>
					<?php if(!in_array($k, array('account', 'email', 'mobile', 'manage')) && @!in_array($k, $un_menu_ary[$c['FunVersion']][0])){?>
					<ul>
						<?php
						foreach($c['manage']['permit'][$k] as $k1=>$v1){
							if(!manage::check_permit($k, 0, array('a'=>$k1))) continue;
							$action_ary=@explode('.', $c['manage']['action']);
						?>
						<li<?=((reset($action_ary)==$k1 && !is_numeric($k1)) || $c['manage']['action']==$v1)?' class="current"':'';?>>
						<?=isset($v1['menu']) ?
						'<a href="./?m='.$k.'&a='.$k1.'"><span>{/module.'.$k.'.'.$k1.'.module_name/}</span></a>'
						:
						'<a href="./?m='.$k.'&a='.$k1.'"><span>{/module.'.$k.'.'.$k1.'/}</span></a>';?>
						</li>
						<?php }?>
					</ul>
					<?php }?>
				</div>
				<?php }?>
			</div>
            <?php }?>
		</div>
		<div id="righter" class="righter home_righter">
			<?php
				$permit_0=$permit_1=0;
				@in_array($c['manage']['module'], $un_menu_ary[$c['FunVersion']][0]) && $c['manage']['module']!='account' && $permit_0=1;
				@in_array("{$c['manage']['module']}.{$c['manage']['action']}", $un_menu_ary[$c['FunVersion']][1]) && $permit_1=1;
				if($permit_0 || $permit_1){
					echo '{/manage.manage.no_permit/}';
				}else{
					if($c['manage']['is_plugins']){//判断是否是插件
						include("plugins/app.php");
					}else{
						if($c['manage']['action']!='index' && !manage::check_permit($c['manage']['module'], 0, array('a'=>$c['manage']['action']))){//检查权限				
							echo '{/manage.manage.no_permit/}';
						}else{
							include("{$c['manage']['module']}/{$c['manage']['action']}.php");
						}
					}
				}
			?>
        </div>
		<div class="clear"></div>
	</div>
	<div class="pop_up pop_orders_message_list">
		<div class="pop_up_container">
			<div class="message_title">{/global.message/}<a href="javascript:;" class="btn_close"></a></div>
			<ul class="message_list">
				<?php
				$msg_orders_row=str::str_code(db::get_limit('user_message m left join user u on m.UserId=u.UserId', 'Module="orders" and Type=0'.$inbox_where, '*', 'IsRead asc, MId desc', 0, 20));
				foreach($msg_orders_row as $k=>$v){
				?>
					<li<?=$v['IsRead']==1?' class="readed"':'';?> data-id="<?=$v['MId'];?>">
						<div class="orders_number">No#<?=$v['Subject'];?></div>
						<span class="date"><?=date('m-d H:i', $v['AccTime']);?></span>
						<div class="clear"></div>
						<div class="message"><?=$v['Content'];?></div>
					</li>
				<?php }?>
			</ul>
		</div>
	</div>
	<div class="pop_up pop_orders_message">
		<div class="pop_up_container">
			<div class="message_title">
				<div class="title"><strong></strong><a href="javascript:;" class="btn_close"></a></div>
				<div class="email"></div>
			</div>
			<div class="message_dialogue clean"><div class="message_dialogue_list"></div></div>
			<div class="message_bottom">
				<form class="form_message" id="message_orders_form">
					<textarea name='Message' class="box_textarea clean" placeholder="{/global.please_enter/} ..." notnull></textarea>
					<div class="clear"></div>
					<div class="multi_img upload_file_multi" id="MsgPicDetail">
						<dl class="img" num="0">
							<dt class="upload_box preview_pic">
								<input type="button" id="MsgPicUpload" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" tips="" />
								<input type="hidden" name="MsgPicPath" value="" data-value="" save="0" />
							</dt>
							<dd class="pic_btn">
								<a href="javascript:;" class="edit"><i class="icon_edit_white"></i></a>
								<a href="javascript:;" class="del" rel="del"><i class="icon_del_white"></i></a>
								<a href="javascript:;" class="zoom" target="_blank"><i class="icon_search_white"></i></a>
							</dd>
						</dl>
					</div>
					<input type="button" class="btn_global btn_submit" value="{/global.send/}" />
					<input type="hidden" name="MId" value="" />
					<input type="hidden" name="do_action" value="action.message_orders_reply" />
				</form>
			</div>
		</div>
	</div>
	<?php if($c['FunVersion']==10){?>
		<div class="contact_qrcode">
			<div class="q_title">{/global.conatact_ser/}</div>
			<div class="blue_line_style">
				<div class="qr_code"><img src="<?=$analytics_row['Service']['WechatQrCode'];?>"></div>
				<div class="wc_name">{/global.support.3/}：<?=$analytics_row['Service']['Wechat'];?></div>
				<span class="sLine s1"></span>
				<span class="sLine s2"></span>
			</div>
			<div class="qr_tips">{/global.invoice/}</div>
		</div>
		<div class="qr_black_mask"></div>

		<div class="popup_limit_tips">
			<div class="img"><img src="/static/manage/images/frame/cdx/free_mode_limit.png" alt=""></div>
			<div class="txt">{/global.pay_func/}</div>
			<div class="go_back">{/global.return/}</div>
		</div>
		<div class="limit_mask"></div>
	<?php }?>
<?php
include('./account/global.php');
}
$html=ob_get_contents();
ob_end_clean();
echo manage::language($html);
?>

<script type="text/javascript">

// var myAuto = document.getElementById('myaudio');
// myAuto.play();
// myAuto.pause();
// myAuto.load();
if($("#count").val()>0)
{
// 	alert("新消息到了");
// 	autoPlay();
// 	closePlay();
}
function autoPlay() {
    var myAuto = document.getElementById('myaudio');
    myAuto.muted=false;
//     myAuto.play();

//     var index = 0;
//     myAuto.addEventListener('ended', function () {  
// //     setTimeout(function () {if(index<1){ myAuto.play(); index++}}, 500);  
//     }, false);  
    myAuto.play();

    
}
function closePlay() {
    var myAuto = document.getElementById('myaudio');
    myAuto.pause();
    myAuto.load();
}
var count=$("#count").val();

setInterval(toggleSound, 10000);
function toggleSound() {
	$.post('?','do_action=account.mp3_play',function(data){
		if(data.ret==1){
			if(data.msg.msg>count)
			{
				$("#count").val(data.msg.msg);
				$(".message_count").html(data.msg.msg);
				$("#order_wait_payment_count").html(data.msg._data.order_wait_payment_count);
				$("#order_wait_delivery_count").html(data.msg._data.order_wait_delivery_count);
				$("#prod_warning_count").html(data.msg._data.prod_warning_count);
				$("#inbox_noread_count").html(data.msg._data.inbox_noread_count);
				$("#caigou_count").html(data.msg._data.caigou_count);
				count=data.msg.msg;
// 				alert(data.msg);
				autoPlay();
			}
		}
	},'json');
    
}
</script>
</body>
</html>