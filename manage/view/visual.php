<?php !isset($c) && exit();?>
<?php
/*******************************************************************************************************************************************/
$pub_visual_home='{/view.visual.home/}';
$pub_visual_home_link='./?m=view&a=visual';
$pub_go_style_free='';
$pub_contents_tips='';
$pub_free='';
/*******************************************************************************************************************************************/
if($c['FunVersion']>=10){
	include('cdx_visual.php');
}
/*******************************************************************************************************************************************/
?>
<?php manage::check_permit('view', 1, array('a'=>'visual'));//检查权限?>
<div id="visual" class="r_con_wrap">
	<?php
    if($_GET['d']=='edit'){
		$global_row=db::get_all('web_visual', "Themes='global'");  //全部模板通用
		$web_index_row=db::get_all('web_visual', "Themes='{$c['manage']['web_themes']}' and (Pages='global' or Pages='index' or Pages='banner')");  //PC首页
		/*
		$web_themes_type=(int)db::get_value('config_module', "Themes='{$c['manage']['web_themes']}'", 'Themes_Type');
		$web_products_list_where=$themes_type==1?'t07':'t06';
		$web_products_list_row=db::get_all('web_visual', "Themes='{$web_products_list_where}' and Pages='{$pages}'");  //PC产品列表页(目前没用上)
		$web_products_detail_where=$themes_type==1?'t07':'t06';
		$web_products_detail_row=db::get_all('web_visual', "Themes='{$web_products_detail_where}' and Pages='{$pages}'");  //PC产品详细页(目前没用上)
		*/
		$mweb_index_where='m'.db::get_value('config', "GroupId='mobile' and Variable='HomeTpl'", 'value');
		$mweb_index_row=db::get_all('web_visual', "Themes='{$mweb_index_where}' and (Pages='mglobal' or Pages='mindex' or Pages='mbanner')");  //移动端首页
		$themes_row=array_merge((array)$web_index_row, (array)$mweb_index_row, (array)$global_row);
		//如果没有该语言版数组,补默认数组回去
		$update=0;
		foreach((array)$themes_row as $k => $v){
			$data=str::json_data($v['Data'], 'decode');
			foreach((array)$c['manage']['config']['Language'] as $k1 => $v1){
				if(!$data[$v1]){
					$update=1;
					$data[$v1]=reset($data);
				}
			}
			if($update){
				$data=str::json_data($data);
				$web_index_row[$k]['Data']=$data;
				$data=addslashes($data);
				db::update('web_visual', "WId='{$v['WId']}'", array('Data'=>$data));
			}
		}
		$txt_font_ary=array(
			'Arial',
			'Akkurat-Bold',
			'Akkurat-Light',
			'BebasNeue-Regular',
			'BauhausITCbyBt-Bold',
			'TradeGothicw01-Bold',
			'Baskervilleserial-Regular',
			'Cainfotypeoptu-Bold',
			'Cainfotypeoptu-Boldcon',
			'Gadugi',
			'HelveticaNeuelTPro-Regular',
			'Housegrindpersonaluseonly',
			'Kaushanscript-Regular',
			'MyriadPro-Regular',
			'MyriadPro-Bold',
			'Montserrat-Regular',
			'Montserrat-Bold',
			'Montserrat-Light',
			'Opensans-Regular',
			'OpenSans-Bold',
			'Opensans-Light',
			'PlayfairDisplay-Italic',
			'Roboto-Bold',
			'Twcentm',
			'Verdana',
			'Whitney-Medium',
			'微软雅黑',
		);
		$txt_font_fiter_ary=array(  //系统默认,不需要单独加载
			'Arial',
			'Verdana',
			'微软雅黑'
		);
		echo ly200::load_static('/static/js/plugin/colpick/colpick.js', '/static/js/plugin/colpick/colpick.css');
	?>
		<script>
        $(function(){
            view_obj.visual_init();
            $('#plugins_iframe_themes').load(function(){  //载入需要用到的字体
                <?php
                foreach((array)$txt_font_ary as $v){
                    if(in_array($v, $txt_font_fiter_ary)) continue;
                ?>
                $('<link>').attr({'rel':'stylesheet', 'type':'text/css', 'href':'<?="{$c['cdn']}static/font/{$v}/font.css"?>'}).appendTo($(this).contents().find('head'));
                <?php }?>
            });
			$(window).load(function(){
				<?php if($_GET['type']){?>$('#plugins_visual .top_bar .go_client a[data-client=<?=$_GET['type']?>]').trigger('click');<?php }  //跳转对应端口?>
				$('#plugins_visual .loading_mask').fadeOut();
			});
        })
        </script>
		<?php  /*************** 可视化编辑 Start ***************/ ?>
        <div id="plugins_visual">
        	<div class="loading_mask"></div>
            <div class="top_bar">
                <a class="go_home" href="<?=$pub_visual_home_link;?>"><i></i><?=$pub_visual_home;?></a>
                <?php /*?><div class="go_pages">
                    <div class="pages_name">{/view.visual.page.page/}： <span>{/view.visual.page.home/}<i></i></span></div>
                </div><?php */?>
                <div class="go_btn go_save">{/global.save/}</div>
                <div class="go_btn go_preview">{/global.preview/}</div>
                <div class="go_btn go_style switch_style_btn <?=$pub_go_style_free;?>">{/view.template.style/}</div>
                <div class="go_client">
                    <div class="terminal_list">
                        <a href="?m=view&a=visual&d=edit&type=website" class="pc <?=$_GET['type']=='mobile'?'':'cur'?>" data-client="website" ></a>
                        <a href="?m=view&a=visual&d=edit&type=mobile" class="mobile <?=$_GET['type']=='mobile'?'cur':''?>" data-client="mobile" ></a>
                    </div>
                </div>
            </div>
            <div class="main_bar">
            	<?php if($_GET['type']=='mobile'){?>
                	<iframe id="plugins_iframe_mobile" data-client="mobile" /></iframe>
                <?php }else{?>
                	<iframe id="plugins_iframe_themes" data-client="website"/></iframe>
                <?php }?>
            </div>
            <div class="tool_bar">
                <div class="close"></div>
                <div class="nav_bar fr">
                    <div class="item pic">{/view.visual.pic/}<i></i></div>
                    <div class="item add">{/global.add/}</div>
                </div>
                <div class="set_bar fr" onselectstart="return false">
                    <div class="set_item current">
                        <form id="form_visual" class="rows">
                            <?php
                            foreach((array)$themes_row as $k => $v){  //位置
                                $themes_data=str::json_data($v['Data'], 'decode');
                            ?>
                            <div class="loc_box input" plugins="<?="{$v['Pages']}-{$v['Position']}";?>">
                                <div class="lang_tab">
                                    <?php if(count($c['manage']['config']['Language'])>1){?>
									<div class="tab_title fl">{/view.visual.cur_lang/}</div>
									<?php }?>
                                    <div class="tab_box fr"><?=manage::html_tab_button();?></div>
                                    <div class="clear"></div>
                                </div>
                                <?php foreach((array)$c['manage']['config']['Language'] as $k1 => $v1){  //语言版?>
                                <div class="tab_txt tab_txt_<?=$v1;?>" <?=$c['manage']['config']['LanguageDefault']==$v1 ? 'style="display:block;"' : ''; ?>>
                                    <?php
                                    foreach((array)$themes_data[$v1] as $k2 => $v2){  //色块元素
                                        $class='';
                                        if($v['Pages']=='banner') $class.=' item_box_banner';
                                        if($v['Pages']=='banner' && !$k2) $class.=' item_box_banner_current';
                                        if($v['Pages']=='mbanner') $class.=' item_box_mbanner';
                                        if($v['Pages']=='mbanner' && !$k2) $class.=' item_box_mbanner_current';
                                    ?>
                                    <div class="item_box <?=$class;?>">
                                        <?php
                                        if($v['Pages']=='banner'){  //广告图模块
                                            $name="{$v['Pages']}[0][{$v1}][{$k2}]";
                                            $tips=$v2['Banner']['width']?$v2['Banner']['width']:$c['manage']['lang_pack']['global']['not_limit'];
                                            $tips.='*';
                                            $tips.=$v2['Banner']['height']?$v2['Banner']['height']:$c['manage']['lang_pack']['global']['not_limit'];
                                        ?>
                                            <div class="slide_contents current slide_img">
                                                <div class="title">{/global.pic/}<i></i></div>
                                                <div class="box" style="display:block;">
                                                    <div class="image" data-id="<?="{$v['Pages']}_0_{$v1}_{$k2}";?>">
                                                        <?=manage::multi_img("PicDetail_{$v['Pages']}_0_{$v1}_{$k2}", "{$name}[Banner][url]", $v2['Banner']['url'], $tips);?>
                                                        </div>
                                                    <?php /*?><a class="gallery" href="javascript:;">{/view.visual.gallery/}</a><?php */?>
                                                    <div class="blank25"></div>
                                                </div>
                                            </div>
                                            <div class="slide_contents slide_title">
                                                <div class="title">{/global.title/}<i></i></div>
                                                <div class="box">
                                                    <div class="name">{/global.content/}</div>
                                                    <div class="blank6"></div>
                                                    <div><textarea class="plugins_textarea" name="<?=$name;?>[Title][content]"><?=htmlspecialchars($v2['Title']['content']);?></textarea></div>
                                                    <?=$pub_contents_tips;?>
                                                    <div class="blank6"></div>
                                                    <div class="name">{/view.visual.font/}</div>
                                                    <div class="blank6"></div>
                                                    <div class="font">
                                                        <select class="plugins_select" name="<?=$name;?>[Title][font-family]">
                                                            <?php foreach((array)$txt_font_ary as $val){?>
                                                            <option value="<?=$val?>" <?=$v2['Title']['font-family']==$val?'selected':'';?>><?=$val;?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                    <div class="blank12"></div>
                                                    <div class="font_style">
                                                        <i class="italic <?=$v2['Title']['font-style']=='italic'?'current':'';?>"><input type="hidden" name="<?=$name;?>[Title][font-style]" value="<?=$v2['Title']['font-style']?$v2['Title']['font-style']:'normal';?>" /></i>
                                                        <i class="underline <?=$v2['Title']['text-decoration']=='underline'?'current':'';?>"><input type="hidden" name="<?=$name;?>[Title][text-decoration]" value="<?=$v2['Title']['text-decoration']?$v2['Title']['text-decoration']:'none';?>" /></i>
                                                        <i class="bold <?=$v2['Title']['font-weight']=='bold'?'current':'';?>"><input type="hidden" name="<?=$name;?>[Title][font-weight]" value="<?=$v2['Title']['font-weight']?$v2['Title']['font-weight']:'normal';?>" /></i>
                                                        <input class="color_style" name="<?=$name;?>[Title][color]" value="<?=$v2['Title']['color']?$v2['Title']['color']:'#333';?>" />
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="blank6"></div>
                                                    <div class="name">{/view.visual.font_size/}</div>
                                                    <div class="blank6"></div>
                                                    <div class="font_size">
                                                        <div class="progress"><div class="bar" style="width:<?=((int)$v2['Title']['font-size']-16)*1.69;?>px"><i></i></div></div>
                                                        <div class="text"><?=(int)$v2['Title']['font-size']?(int)$v2['Title']['font-size']:16;?></div>
                                                        <input type="hidden" name="<?=$name;?>[Title][font-size]" value="<?=$v2['Title']['font-size']?$v2['Title']['font-size']:'16px';?>" />
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="blank20"></div>
                                                </div>
                                            </div>
                                            <div class="slide_contents slide_subtitle">
                                                <div class="title">{/global.subtitle/}<i></i></div>
                                                <div class="box">
                                                    <div class="name">{/global.content/}</div>
                                                    <div class="blank6"></div>
                                                    <div><textarea class="plugins_textarea" name="<?=$name;?>[SubTitle][content]"><?=htmlspecialchars($v2['SubTitle']['content']);?></textarea></div>
                                                    <?=$pub_contents_tips;?>
                                                    <div class="blank6"></div>
                                                    <div class="name">{/view.visual.font/}</div>
                                                    <div class="blank6"></div>
                                                    <div class="font">
                                                        <select class="plugins_select" name="<?=$name;?>[SubTitle][font-family]">
                                                            <?php foreach((array)$txt_font_ary as $val){?>
                                                            <option value="<?=$val?>" <?=$v2['SubTitle']['font-family']==$val?'selected':'';?>><?=$val?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                    <div class="blank12"></div>
                                                    <div class="font_style">
                                                        <i class="italic <?=$v2['SubTitle']['font-style']=='italic'?'current':'';?>"><input type="hidden" name="<?=$name;?>[SubTitle][font-style]" value="<?=$v2['SubTitle']['font-style']?$v2['SubTitle']['font-style']:'normal';?>" /></i>
                                                        <i class="underline <?=$v2['SubTitle']['text-decoration']=='underline'?'current':'';?>"><input type="hidden" name="<?=$name;?>[SubTitle][text-decoration]" value="<?=$v2['SubTitle']['text-decoration']?$v2['SubTitle']['text-decoration']:'none';?>" /></i>
                                                        <i class="bold <?=$v2['SubTitle']['font-weight']=='bold'?'current':'';?>"><input type="hidden" name="<?=$name;?>[SubTitle][font-weight]" value="<?=$v2['SubTitle']['font-weight']?$v2['SubTitle']['font-weight']:'normal';?>" /></i>
                                                        <input class="color_style" name="<?=$name;?>[SubTitle][color]" value="<?=$v2['SubTitle']['color']?$v2['SubTitle']['color']:'#333';?>" />
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="blank6"></div>
                                                    <div class="name">{/view.visual.font_size/}</div>
                                                    <div class="blank6"></div>
                                                    <div class="font_size">
                                                        <div class="progress"><div class="bar" style="width:<?=((int)$v2['SubTitle']['font-size']-16)*1.69;?>px"><i></i></div></div>
                                                        <div class="text"><?=(int)$v2['SubTitle']['font-size']?(int)$v2['SubTitle']['font-size']:16;?></div>
                                                        <input type="hidden" name="<?=$name;?>[SubTitle][font-size]" value="<?=$v2['SubTitle']['font-size']?$v2['SubTitle']['font-size']:'16px';?>" />
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="blank20"></div>
                                                </div>
                                            </div>
                                            <div class="slide_contents slide_link">
                                                <div class="title">{/global.url/}<i></i></div>
                                                <div class="box"><textarea class="plugins_textarea" name="<?=$name;?>[Banner][link]"><?=htmlspecialchars($v2['Banner']['link']);?></textarea></div>
                                            </div>
                                            <div class="slide_contents slide_sort">
                                                <div class="title">{/global.my_order/}<i></i></div>
                                                <div class="box">
                                                	<select class="plugins_select"  name="<?=$name;?>[MyOrder]">
                                                    	<?php for($i=0;$i<count($themes_data[$v1]);$i++){?>
                                                        <option value="<?=$i;?>" <?=($k2)==$i?'selected':'';?>>{/view.visual.sort_prev/}<?=$i+1;?>{/view.visual.sort_next/}</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" name="<?=$name;?>[Banner][width]" value="<?=$v2['Banner']['width'];?>" />
                                            <input type="hidden" name="<?=$name;?>[Banner][height]" value="<?=$v2['Banner']['height'];?>" />
                                            <input type="hidden" name="<?=$name;?>[Banner][left]" value="<?=$v2['Banner']['left'];?>" />
                                            <input type="hidden" name="<?=$name;?>[Banner][top]" value="<?=$v2['Banner']['top'];?>" />
                                            <?php if($v2['Title']['oldcontent'] && !$v2['Title']['content']){  //旧客户旧数据有 && 没编辑过板块?>
                                            <input type="hidden" name="<?=$name;?>[Title][oldcontent]" value="<?=$v2['Title']['oldcontent'];?>" />
                                            <?php }?>
                                            <?php
											//初始没数值，默认50%居中
											if(!$v2['Title']['left'] && !$v2['Title']['top']) $v2['Title']['left']=$v2['Title']['top']='50%';
											if(!$v2['SubTitle']['left'] && !$v2['SubTitle']['top']) $v2['SubTitle']['left']=$v2['SubTitle']['top']='50%';
											?>
                                            <input type="hidden" name="<?=$name;?>[Title][left]" value="<?=$v2['Title']['left'];?>" />
                                            <input type="hidden" name="<?=$name;?>[Title][top]" value="<?=$v2['Title']['top'];?>" />
                                            <input type="hidden" name="<?=$name;?>[SubTitle][left]" value="<?=$v2['SubTitle']['left'];?>" />
                                            <input type="hidden" name="<?=$name;?>[SubTitle][top]" value="<?=$v2['SubTitle']['top'];?>" />
                                        <?php
                                        }elseif($v['Pages']=='mbanner'){  //手机版广告图
                                            $tips=explode('*', $themes_data[$v1][$k2]['PicSize']);
                                            !$tips[0] && $tips[0]=$c['manage']['lang_pack']['global']['not_limit'];
                                            !$tips[1] && $tips[1]=$c['manage']['lang_pack']['global']['not_limit'];
                                            $tips=implode('*', $tips);
                                            $title='{/global.pic/}';
                                            $name="{$v['Pages']}[{$v['Position']}][{$v1}][{$k2}]";    //页面-位置-语言-色块元素
                                        ?>    
                                            <div class="slide_contents slide_img current">
                                                <div class="title">{/global.pic/}<i></i></div>
                                                <div class="box" style="display:block;">
                                                    <div class="image" data-id="<?="{$v['Pages']}_0_{$v1}_{$k2}";?>">
                                                        <?=manage::multi_img("PicDetail_{$v['Pages']}_0_{$v1}_{$k2}", "{$name}[Pic]", $v2['Pic'], $tips);?>
                                                        </div>
                                                    <div class="blank25"></div>
                                                </div>
                                            </div>
                                            <div class="slide_contents slide_title">
                                                <div class="title">{/global.title/}<i></i></div>
                                                <div class="box"><input class="plugins_input" type="text" name="<?=$name;?>[Title]" value="<?=htmlspecialchars($v2['Title']);?>"/></div>
                                            </div>
                                            <div class="slide_contents slide_link">
                                                <div class="title">{/global.url/}<i></i></div>
                                                <div class="box"><textarea class="plugins_textarea" name="<?=$name;?>[Link]"><?=htmlspecialchars($v2['Link']);?></textarea></div>
                                            </div>
                                            <div class="slide_contents slide_sort">
                                                <div class="title">{/global.my_order/}<i></i></div>
                                                <div class="box">
                                                	<select class="plugins_select"  name="<?=$name;?>[MyOrder]">
                                                    	<?php for($i=0;$i<count($themes_data[$v1]);$i++){?>
                                                        <option value="<?=$i;?>" <?=($k2)==$i?'selected':'';?>>{/view.visual.sort_prev/}<?=$i+1;?>{/view.visual.sort_next/}</option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php }else{ //普通类型?>
                                            <?php
                                            if(isset($v2['Pic'])){  //有图片
                                                $tips=explode('*', $themes_data[$v1][$k2]['PicSize']);
                                                !$tips[0] && $tips[0]=$c['manage']['lang_pack']['global']['not_limit'];
                                                !$tips[1] && $tips[1]=$c['manage']['lang_pack']['global']['not_limit'];
                                                $tips=implode('*', $tips);
                                                $title='{/global.pic/}';
                                                $name="{$v['Pages']}[{$v['Position']}][{$v1}][{$k2}][Pic]";    //页面-位置-语言-色块元素
                                            ?>
                                                <div class="name"><?=$title?></div>
                                                <div class="blank6"></div>
                                                <div class="plugins_mod_Pic">
                                                    <div class="image" data-id="<?="{$v['Pages']}_{$v['Position']}_{$v1}_{$k2}";?>"><?=manage::multi_img("PicDetail_{$v['Pages']}_{$v['Position']}_{$v1}_{$k2}", $name, $v2['Pic'], $tips);?></div>
                                                    <div class="pic_size"><input type="hidden" name="<?="{$v['Pages']}[{$v['Position']}][{$v1}][{$k2}][PicSize]";?>" value="<?=$themes_data[$v1][$k2]['PicSize'];?>" /></div>
                                                </div>
                                                <div class="blank12"></div>
                                            <?php }?>
                                            <?php
                                            foreach((array)$v2 as $k3 => $v3){ //数据
                                                $title='';
                                                if($k3=='PicSize' || $k3=='Pic') continue;
                                                $k3=='Title' && $title='{/global.title/}';
                                                $k3=='SubTitle' && $title='{/global.subtitle/}';
                                                $k3=='Link' && $title='{/global.url/}';
                                                $k3=='Content' && $title='{/global.content/}';
                                                $k3=='Pic' && $title='{/global.pic/}';
                                                $name="{$v['Pages']}[{$v['Position']}][{$v1}][{$k2}][{$k3}]";    //页面-位置-语言-色块元素
                                            ?>
                                                <div class="name"><?=$title?></div>
                                                <div class="blank6"></div>
                                                <?php if($k3=='Title' || $k3=='SubTitle'){?>
                                                <div class="plugins_mod_<?=$k3?>"><input class="plugins_input" type="text" name="<?=$name;?>" value="<?=htmlspecialchars($v3);?>"/></div>
                                                <?php }elseif($k3=='Content' || $k3=='Link'){?>
                                                <div class="plugins_mod_<?=$k3;?>"><textarea class="plugins_textarea" name="<?=$name?>"><?=htmlspecialchars($v3);?></textarea></div>
                                                <?php }?>
                                                <div class="blank12"></div>
                                            <?php }?>
                                            <div class="blank_line"></div>
                                        <?php }?>
                                    </div>
                                    <?php }?>
                                </div>
                                <?php }?>
                            </div>
                            <?php }?>
                            <?php foreach((array)$themes_row as $v){?>
                            <input type="hidden" name="WId[]" value="<?=$v['WId']?>" />
                            <input type="hidden" name="Layout[<?="{$v['Pages']}-{$v['Position']}"?>]" value="<?=$v['Layout']?>" />
                            <?php }?>
                            <input type="hidden" name="do_action" value="view.plugins_visual_save" />
                            <?php if($_SESSION['Manage']['FirstEnterManage']) { //第一次登陆后台 ?>
                                <input type="hidden" name="first_enter_manage" value="1" />
                            <?php } ?>
                            <?php if($_SESSION['Manage']['FirstChangeThemes']) { //第一次默认进入切换风格 ?>
                                <input type="hidden" name="first_change_themes" value="1" />
                            <?php } ?>
                        </form >
                    </div>
                </div>
            </div>
        </div>
        <?php  /*************** 可视化编辑 End ***************/ ?>
    <?php 
	}else{
		$index_img = "{$c['cdn']}static/v0/themes/{$c['manage']['web_themes']}-2.jpg";
		($c['manage']['config']['CustomIndex']==1 && $c['manage']['web_themes']=='custom') && $index_img='/static/themes/custom/images/custom.jpg';
	?>
        <div class="blank12"></div>
        <div class="inside_container">
            <h1>{/view.index.title/}</h1>
        </div>
        <div class="container">
            <div class="box_list">
                <div class="fl view_box">
                    <div class="clear"></div>
                    <div class="view_pc"><div class="img"><img src="<?=$index_img;?>" /></div></div>
                    <?php 
                    $set_ary=array();
                    $mobile_set_row=db::get_all('config', "GroupId='mobile'");
                    foreach($mobile_set_row as $v){
                        $set_ary[$v['Variable']]=$v['Value'];
                    }
                    ?>
                    <div class="view_mobile"><em class="top"></em><div class="img"><img src="<?="{$c['cdn']}static/v0/themes/mobile/{$set_ary['HomeTpl']}-1.jpg";?>" /></div></div>
                    <div class="clear"></div>
                </div>
                <div class="fr btn_box">
                    <dl>
                        <dt><a class="edit_btn visual_edit_btn" href="./?m=view&a=visual&d=edit">{/global.mod/}</a></dt>
                        <dd>{/view.index.submod/}</dd>
                    </dl>
                    <dl>
                        <dt><a class="edit_btn switch_style_btn" href="javascript:;">{/view.index.switch/}</a></dt>
                        <dd>{/view.index.subswitch/}</dd>
                        <dd style="display:none;"><a class="download_all_themes" href="javascript:;">Download All Themes</a></dd>
                    </dl>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    <?php }?>
    <?php  /*************** 模板选择 Start ***************/ ?>
    <script>$(function(){view_obj.switch_style_init();})</script>
    <div id="template_list">
        <a href="javascript:;" class="close"></a>
        <div class="terminal_box">
            <div class="terminal_list">
                <a href="javascript:;" class="pc cur"></a>
                <a href="javascript:;" class="mobile"></a>
            </div>
            <div class="top_title">{/view.template.top/}</div>
            <div class="sub_title">{/view.template.sub/}</div>
            <div class="category">
                <a class="cur" href="javascript:;">{/view.template.all/}</a>
            </div>
        </div>
        <div class="tem_box" style="display:block;">
            <div class="head_fixed"></div>
            <div class="rows" style="display:block;">
                <div class="list">
                    <div class="edit_box">
                        <span class="fl">t085</span>
                        <a href="http://t085.shop.ueeshop.com/" target="_blank" class="fr view_btn">{/view.template.view/}</a>
                    </div>
                    <div class="img cur">
                        <img src="" _src="<?=$c['cdn']."static/v0/themes/t085-3.jpg"; ?>" alt="">
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="loading"><span>{/view.template.loading/}</span></div>
            <input type="hidden" name="scrollTop" value="0">
        </div>
        <div class="tem_box mobile" style="display: none;">
            <div class="head_fixed"></div>
            <?php 
                $tpl_dir=$c['mobile']['tpl_dir'].'index/';//模板目录
                $base_dir=$c['root_path'].$tpl_dir;//邮件模板目录绝对路径
                $themes_ary=array();
                for ($i=1; $i<=17; ++$i) {
                    $themes_ary[]=$i;
                }
                $themes_ary = array_reverse($themes_ary);
                foreach((array)$themes_ary as $k=>$v){
                    $tpl=sprintf('%02s', $v);
                    if(!is_dir($base_dir.$tpl)) continue;
                    $data_img=$c['cdn']."static/v0/themes/mobile/{$tpl}-1.jpg";
                ?>
                <?php if($k%8==0){ ?>
                    <div class="rows" style="display:<?=$k==0 ? 'block' : 'none'; ?>;">
                <?php } ?>
                <div class="list">
                    <div class="edit_box">
                        <a href="javascript:;" class="fr edit_btn" data-mobile="1" data-themes="<?=$tpl;?>">{/view.template.switch/}</a>
                    </div>
                    <div class="img <?=$c['manage']['web_themes']==$v['Themes'] ? 'cur' : ''; ?>">
                        <img src="" _src="<?=$data_img; ?>" alt="">
                    </div>
                </div>
                <?php if($k%8==7 || ($k==16 && $k%8!=7)){ ?>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="loading"><span>{/view.template.loading/}</span></div>
            <input type="hidden" name="scrollTop" value="0">
        </div>
    </div>
    <?php  /*************** 模板选择 End ***************/ ?>
</div>