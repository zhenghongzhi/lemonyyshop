<?php !isset($c) && exit();?>
<?php
manage::check_permit('view', 1, array('a'=>'nav'));//检查权限
echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
?>
<div id="nav" class="r_con_wrap">
	<div class="inside_container">
		<h1>
			{/module.view.nav/}
		</h1>
		<ul class="inside_menu"></ul>
	</div>
	<?php
	//导航管理
	$nav_row=db::get_value('config', "GroupId='themes' and Variable='NavData'", 'Value');
	if(!$nav_row){
		$nav_row=db::get_value('config_module', "IsDefault=1", 'NavData');
		db::get_row_count('config', "GroupId='themes' and Variable='NavData'")?db::update('config', "GroupId='themes' and Variable='NavData'", array('Value'=>addslashes(stripslashes($nav_row)))):db::insert('config', array('GroupId'=>'themes','Variable'=>'NavData','Value'=>addslashes(stripslashes($nav_row))));
	}
	$nav_data=str::json_data(htmlspecialchars_decode($nav_row), 'decode');
	$page_category=str::str_code(db::get_all('article_category', 'UId="0,"', '*', 'CateId asc'));
	//排序数组
	$my_order_ary=array();
	foreach((array)$nav_data as $k=>$v){
		$my_order_ary[$k+1]=$k+1;
	}
	//获取类别列表
	$PageCateAry=$InfoCateAry=$ProdCateAry=$category_ary=array();
	$page_cate_ary=str::str_code(db::get_all('article_category', '1', 'CateId, Category'.$c['manage']['web_lang']));
	$info_cate_ary=str::str_code(db::get_all('info_category', '1', 'CateId, Category'.$c['manage']['web_lang']));
	$products_cate_ary=str::str_code(db::get_all('products_category', '1', 'CateId, Category'.$c['manage']['web_lang']));
	foreach((array)$page_cate_ary as $v) $category_ary['Page'][$v['CateId']]=$v;
	foreach((array)$info_cate_ary as $v) $category_ary['Info'][$v['CateId']]=$v;
	foreach((array)$products_cate_ary as $v) $category_ary['Cate'][$v['CateId']]=$v;
	unset($art_cate_ary, $info_cate_ary, $products_cate_ary);
		?>    	
	<script type="text/javascript">$(document).ready(function(){view_obj.nav_init()});</script>
	<div class="center_container">
		<div class="global_container">
			<?php
			if($c['manage']['do']=='index'){
			?>
				<div class="config_table_body">
					<div class="box_explain">{/view.explain.order/}</div>
					<?php
					foreach((array)$nav_data as $k=>$v){
						$Name=(isset($v['Custom']) && $v['Custom'])?$v['Name'.$c['manage']['web_lang']]:$c['nav_cfg'][$v['Nav']]['name'.$c['manage']['web_lang']];
						if(isset($v['Page']) && $v['Page']){							
							$Name=$category_ary['Page'][$v['Page']]['Category'.$c['manage']['web_lang']]." ({/page.page.page/})";
						}elseif(isset($v['Info']) && $v['Info']){							
							$Name=$category_ary['Info'][$v['Info']]['Category'.$c['manage']['web_lang']]." ({/news.news.news/})";
						}elseif(isset($v['Cate']) && $v['Cate']){							
							$Name=$category_ary['Cate'][$v['Cate']]['Category'.$c['manage']['web_lang']]." ({/products.product/})";
						}
						$id=$k+1;
					?>
						<div class="table_item" data-id="<?=$k;?>">
							<table border="0" cellpadding="5" cellspacing="0" class="config_table">
								<thead>
									<tr><td nowrap="nowrap" class="myorder"><span class="icon_myorder"></span></td></tr>
								</thead>
								<tbody>
									<tr>
										<td width="80%" class="myorder_left"><div class="name"><?=$Name;?></div></td>
										<td width="15%" nowrap="nowrap" align="right">
											<a class="edit" href="./?m=view&a=nav&d=edit&id=<?=$id; ?>" data-id="<?=$id;?>">{/global.edit/}</a>&nbsp;&nbsp;&nbsp;<a class="del" href="./?do_action=view.nav_del&Type=nav&Id=<?=$id;?>" rel="del">{/global.del/}</a>
										</td>		
									</tr>
								</tbody>
							</table>
						</div>
					<?php }?>
				</div>
				<a href="./?m=view&a=nav&d=add" class="add set_add">{/global.add/}</a>
				<br />
			<?php
			}else{
				$id=(int)$_GET['id'];
				$nav_edit_row=$nav_data[$id-1];
				$c['manage']['do']=='add' && $nav_edit_row=array();
			?>
				<div class="box_nav_edit">
					<form id="nav_edit_form" class="themes_nav_form global_form">
						<div class="r_con_form">
							<a href="javascript:history.back(-1);" class="return_title">
								<span class="return">{/module.view.nav/}</span> 
								<span class="s_return">  /  {/global.<?=$c['manage']['do'];?>/}</span>
							</a>
							<div class="rows clean column_rows">
								<label>{/themes.nav.column/}<div class="tab_box" style="display:<?=$nav_edit_row['Custom']==1?'inline-block':'none';?>;"><?=manage::html_tab_button();?></div></label>
								<div class="input">
									<?php
									foreach($c['manage']['config']['Language'] as $k=>$v){
										$html='';
										$Value=array();
										if($nav_edit_row){//编辑
											if($nav_edit_row['Custom']){//自定义项
												$Type='add';
												$Select=$Value=$nav_edit_row['Name_'.$v];
											}else{
												$Type=$c['nav_cfg'][$nav_edit_row['Nav']]['page'];
												if($Type=='products' && $nav_edit_row['Cate']>0){//产品
													$Select=$nav_edit_row['Cate'];
													$Value=db::get_value('products_category', "CateId='$Select'", "Category_$v");
												}elseif($Type=='article' && $nav_edit_row['Page']>0){//单页
													$Select=$nav_edit_row['Page'];
													$Value=db::get_value('article_category', "CateId='$Select'", "Category_$v");
												}elseif($Type){//固定项
													$Select=$Type;
													$Value=$c['nav_cfg'][$nav_edit_row['Nav']]['name_'.$v];
												}
											}
											$ValueAry=array('Select'=>$Select, 'Input'=>$Value, 'Type'=>$Type);
										}
										$html.='<div class="tab_txt tab_txt_'.$v.'" '.($c['manage']['config']['LanguageDefault']==$v ? 'style="display:block;"' : '').' lang="'.$v.'">';
										if($v==substr($c['manage']['web_lang'], 1)){
											$SelectAry=array();
											foreach($c['nav_cfg'] as $k2=>$v2){
												$SelectAry[$v2['page']]=array('Name'=>$v2['name_'.$v], 'Type'=>$v2['page'], 'Table'=>$v2['table']);
											}
											$html.=manage::box_drop_double('Unit', 'UnitValue_'.$v, $SelectAry, $ValueAry, 0, 'notnull', 0, 0, $c['manage']['lang_pack']['view']['explain']['placeholder']);
										}else{
											!$Value && $Value='';
											$html.='<input type="text" name="UnitValue_'.$v.'" value="'.$Value.'" class="box_input" size="52" maxlength="255" placeholder="完善其他语言版名称" notnull />';
										}
										$html.='</div>';
										echo $html;
									}?>
								</div>
							</div>
							<div class="rows clean url_rows" style="display:<?=$nav_edit_row['Custom']?'block':'none';?>;">
								<label>{/themes.nav.url/}</label>
								<div class="input"><input name="Url" type="text" value="<?=$nav_edit_row['Url']; ?>" class="box_input" size="45" maxlength="200" /></div>
							</div>
							<div class="rows clean target_rows">
								<label>{/themes.nav.target/}</label>
								<div class="input">
									<div class="box_select"><select name="NewTarget" class="box_input"><option value="0">{/global.n_y_ary.0/}</option><option value="1" <?=$nav_edit_row['NewTarget']?'selected="selected"':''; ?>>{/global.n_y_ary.1/}</option></select></div>
								</div>
							</div>
							<div class="rows clean pic_rows" style="display:<?=$nav_edit_row['Nav']==3 ? 'block' : 'none'; ?>;">
								<label>{/ad.ad.photo/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
								<div class="input">
									<?php foreach($c['manage']['config']['Language'] as $k=>$v){?>
										<div class="tab_txt tab_txt_<?=$v;?>" <?=$c['manage']['config']['LanguageDefault']==$v ? 'style="display:block;"' : ''; ?>>
											<ul id="PicDetail_<?=$v;?>" class="ad_drag multi_img">
												<?php
												for($i=0; $i<4; ++$i){
													$pic_path=@is_file($c['root_path'].$nav_edit_row['PicPath_'.$i][$v])?$nav_edit_row['PicPath_'.$i][$v]:'';
													$isFile=$pic_path?1:0;
												?>
												<li class="adpic_row clean">
													<span class="upload_file_multi fl">
														<dl class="img <?=$isFile ? 'isfile' : ''; ?>" num="<?=$i;?>" lang="<?=$v;?>">
															<dt class="upload_box preview_pic">
																<input type="button" id="PicUpload_<?=$i;?>" lang="<?=$v;?>" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" data-count="<?=$ad_row['PicCount']?>" tips="<?=sprintf(manage::language('{/notes.pic_tips/}'), $ad_row['PicCount']);?>" />
																<input type="hidden" class="picpath" name="PicPath_<?=$v;?>[]" value="<?=$pic_path;?>" save="<?=$isFile?1:0;?>" />
															</dt>
															<dd class="pic_btn">
																<a href="javascript:;" class="edit"><i class="icon_edit_white"></i></a>
																<a href="<?=$isFile?$pic_path:'javascript:;';?>" class="zoom" target="_blank"><i class="icon_search_white"></i></a>
																<a href="javascript:;" class="del" rel="del"><i class="icon_del_white"></i></a>

																<!-- <a href="javascript:;" label="{/global.edit/}" class="tip_ico tip_min_ico edit" lang="<?=$v;?>" data-count="<?=$ad_row['PicCount']?>"><img src="/static/ico/edit.png" align="absmiddle" /></a>
																<a href="javascript:;" label="{/global.del/}" class="tip_ico tip_min_ico del" rel="del"><img src="/static/ico/del.png" align="absmiddle" /></a> -->
															</dd>
														</dl>
													</span>
													<div class="ad_info">
														<span class="unit_input"><b>{/ad.ad.name/}<div class='arrow'><em></em><i></i></div></b><input name="ImgName_<?=$v;?>[]" value="<?=$nav_edit_row['ImgName_'.$i][$v];?>" type="text" class="box_input" size="50" maxlength="50" /></span>
														<span class="unit_input"><b>{/ad.ad.url/}<div class='arrow'><em></em><i></i></div></b><input name="Url_<?=$v;?>[]" value="<?=$nav_edit_row['Url_'.$i][$v];?>" type="text" class="box_input" size="50" maxlength="200" /></span>
													</div>
													<div class="drag_bg icon_myorder"></div>
												</li>
												<?php }?>
											</ul>
										</div>
									<?php }?>
								</div>
							</div>
							<input type="hidden" id="Id" name="Id" value="<?=$id?$id:0; ?>" />
							<input type="hidden" name="do_action" value="view.nav_edit" />
							<input type="hidden" name="Type" value="nav" />
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.save/}">
									<a href="./?m=view&a=nav"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
								</div>
							</div>
						</div>
					</form>
				</div>
			<?php }?>
		</div>
	</div>
</div>
