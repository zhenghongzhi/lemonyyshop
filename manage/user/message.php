<?php !isset($c) && exit();?>
<?php
manage::check_permit('user', 1, array('a'=>'message'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('user', 0, array('a'=>'message', 'd'=>'add')),
	'edit'	=>	manage::check_permit('user', 0, array('a'=>'message', 'd'=>'edit')),
	'del'	=>	manage::check_permit('user', 0, array('a'=>'message', 'd'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'?'message':'message_inside');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
 	<div class="center_container_1000">
		<?php
        if($c['manage']['do']=='index'){
            //系统信息列表页
        ?>
            <script type="text/javascript">$(document).ready(function(){user_obj.message_init()});</script>
			<div class="inside_container">
				<h1>{/module.user.message/}</h1>
			</div>
			<div class="inside_table <?=$c['manage']['cdx_limit'];?>">
				<div class="list_menu">
					<ul class="list_menu_button">
						<?php if($permit_ary['add']){?><li><a class="add" href="./?m=user&a=message&d=edit">{/global.add/}</a></li><?php }?>
						<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					</ul>
					<div class="search_form">
						<form method="get" action="?">
							<div class="k_input">
								<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
								<input type="button" value="" class="more" />
							</div>
							<input type="submit" class="search_btn" value="{/global.search/}" />
							<input type="hidden" name="m" value="user" />
							<input type="hidden" name="a" value="message" />
						</form>
					</div>
				</div>
				<?php
				$Keyword=str::str_code($_GET['Keyword']);
				$where='1';//条件
				$page_count=10;//显示数量
				$Keyword && $where.=" and Title like '%{$Keyword}%'";
				$msg_row=str::str_code(db::get_limit_page('message', $where, '*', 'MId desc', (int)$_GET['page'], $page_count));
				
				if($msg_row[0]){
				?>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
								<td width="45%" nowrap="nowrap">{/inbox.title/}</td>
								<td width="25%" nowrap="nowrap">{/inbox.manager/}</td>
								<td width="15%" nowrap="nowrap">{/global.time/}</td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($msg_row[0] as $v){
							?>
								<tr>
									<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['MId']);?></td><?php }?>
									<td nowrap="nowrap"><?=$v['Title'];?></td>
									<td nowrap="nowrap"><?=$_SESSION['Manage']['UserName'];?></td>
									<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
									<?php if($permit_ary['edit'] || $permit_ary['del']){?>
										<td nowrap="nowrap" class="operation side_by_side">
											<?php if($permit_ary['edit']){?><a href="./?m=user&a=message&d=edit&MId=<?=$v['MId'];?>">{/global.edit/}</a><?php }?>
											<?php if($permit_ary['del']){?>
												<dl>
													<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
													<dd class="drop_down"><a class="del item" href="./?do_action=user.message_del&MId=<?=$v['MId'];?>" rel="del">{/global.del/}</a></dd>
												</dl>
											<?php }?>
										</td>
									<?php }?>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=html::turn_page($msg_row[1], $msg_row[2], $msg_row[3], '?'.ly200::query_string('page').'&page=');?>
				<?php
				}else{//没有数据
					echo html::no_table_data(($Keyword?0:1), './?m=user&a=message&d=edit');
				}?>
			</div>
        <?php
        }else{
            $MId=(int)$_GET['MId'];
            $msg_row=str::str_code(db::get_one('message', "MId='$MId'"));
            $lang=$c['manage']['web_lang'];
            $lang_all=$c['manage']['lang_pack']['global']['all'];
            echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
        ?>
            <script type="text/javascript">$(document).ready(function(){user_obj.message_edit_init()});</script>
            <div class="center_container_1000">
                <div class="global_container">
                    <a href="javascript:history.back(-1);" class="return_title">
                        <span class="return">{/inbox.message.message/}</span> 
                        <span class="s_return">/ <?=$MId?'{/global.edit/}':'{/global.add/}';?></span>
                    </a>
                    <form id="message_edit_form" class="global_form">
                        <div class="rows clean">
                            <label>{/inbox.title/}</label>
                            <div class="input"><input type="text" name="Title" value="<?=$msg_row['Title'];?>" class="box_input" size="50" maxlength="100" notnull="" /></div>					
                        </div>
                        <div class="rows clean option_rows">
                            <label>{/email.addressee/}</label>
                            <div class="input clean">
                                <?php
                                $SelectAry=array(
                                    'user'=>array('Name'=>'会员', 'Type'=>'user', 'Table'=>'user'),
                                    'level'=>array('Name'=>'等级', 'Type'=>'level', 'Table'=>'user_level')
                                );
                                $ValueAry=array();
                                if($msg_row['User'] && $msg_row['User']!='||'){
                                    $level_where_ary=$user_where_ary=array();
                                    $UserId=$msg_row['User'];
                                    !strstr($UserId, '|') && $UserId='|'.$UserId.'|';
                                    $UserAry=explode('|', substr($UserId, 1, -1));
                                    foreach($UserAry as $v){ $where_ary[]=$v; }
                                    if($where_ary){
                                        if(in_array('-1', $where_ary)){//所有会员
                                            $ValueAry[]=array('Name'=>$lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_user'], 'Value'=>-1, 'Type'=>'user');
                                        }
                                        $user_row=db::get_all('user', 'UserId in('.implode(',', $where_ary).')', 'UserId, Email', 'UserId desc');
                                        foreach($user_row as $v){
                                            $ValueAry[]=array('Name'=>$v['Email'], 'Value'=>$v['UserId'], 'Type'=>'user');
                                        }
                                    }
                                }
                                if($msg_row['Level'] && $msg_row['Level']!='||'){
                                    $where_ary=array();
                                    $LevelAry=explode('|', substr($msg_row['Level'], 1, -1));
                                    foreach($LevelAry as $v){ $where_ary[]=$v; }
                                    if($where_ary){
                                        if(in_array('-1', $where_ary)){//所有会员等级
                                            $ValueAry[]=array('Name'=>$lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_level'], 'Value'=>-1, 'Type'=>'level');
                                        }
                                        $level_row=db::get_all('user_level', 'LId in('.implode(',', $where_ary).')', "LId, Name{$lang}", 'LId asc');
                                        foreach($level_row as $v){
                                            $ValueAry[]=array('Name'=>$v['Name'.$lang], 'Value'=>$v['LId'], 'Type'=>'level');
                                        }
                                    }
                                }
                                echo manage::box_drop_double('Unit', 'UnitValue', $SelectAry, $ValueAry, 0, '', 1);
                                ?>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>{/inbox.content/}</label>
                            <div class="input"><?=manage::Editor('Content', $msg_row['Content']);?></div>					
                        </div>
                        <div class="rows clean clean">
                            <label></label>
                            <div class="input input_button">
                                <input type="button" class="btn_global btn_submit" value="{/global.save/}">
                                <a href="./?m=user&a=message"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
                            </div>
                        </div>
                        <input type="hidden" id="MId" name="MId" value="<?=$MId;?>" />
                        <input type="hidden" name="do_action" value="user.message_edit" />
                    </form>
                </div>
            </div>
        <?php }?>
	</div>
</div>