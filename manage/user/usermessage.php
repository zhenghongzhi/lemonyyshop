<?php !isset($c) && exit();?>
<?php
manage::check_permit('user', 1, array('a'=>'usermessage'));//检查权限

$permit_ary=array(
    'add'	=>	manage::check_permit('user', 0, array('a'=>'usermessage', 'd'=>'add')),
    'edit'	=>	manage::check_permit('user', 0, array('a'=>'usermessage', 'd'=>'edit')),
    'del'	=>	manage::check_permit('user', 0, array('a'=>'usermessage', 'd'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'?'message':'message_inside');
?>
<style>
    table {
        table-layout:fixed;
    }
    table td {
        width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
    <div class="center_container_1000">
        <?php
        if($c['manage']['do']=='index'){
            //系统信息列表页
            ?>
            <script type="text/javascript">$(document).ready(function(){user_obj.usermessage_init()});</script>
            <div class="inside_container">
                <h1>采购留言</h1>
            </div>
            <div class="inside_table <?=$c['manage']['cdx_limit'];?>">
                <div class="list_menu">
                    <ul class="list_menu_button">
                        <?php if($permit_ary['add']){?><li><a class="add" href="./?m=user&a=usermessage&d=edit">{/global.add/}</a></li><?php }?>
                        <?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
                    </ul>
                    <div class="search_form">
                        <form method="get" action="?">
                            <div class="k_input">
                                <input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
                                <input type="button" value="" class="more" />
                            </div>
                            <input type="submit" class="search_btn" value="{/global.search/}" />
                            <input type="hidden" name="m" value="user" />
                            <input type="hidden" name="a" value="usermessage" />
                        </form>
                    </div>
                </div>
                <?php
                $Keyword=str::str_code($_GET['Keyword']);
                $where='1';//条件
                $page_count=10;//显示数量
                $Keyword && $where.=" and (name like '%{$Keyword}%' or phone like '%{$Keyword}%' or content like '%{$Keyword}%')";
                $msg_row=str::str_code(db::get_limit_page('usermessage', $where, '*', 'addtime desc', (int)$_GET['page'], $page_count));

                if($msg_row[0]){
                    ?>
                    <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                        <thead>
                        <tr>
                            <?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
                            <td width="45%" nowrap="nowrap">姓名</td>
                            <td width="25%" nowrap="nowrap">电话</td>
                            <td width="15%" nowrap="nowrap">产品</td>
                            <td width="15%" nowrap="nowrap">来源</td>
                            <td width="15%" nowrap="nowrap">时间</td>
                            <?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i=1;
                        foreach($msg_row[0] as $v){
                            ?>
                            <tr>
                                <?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['id']);?></td><?php }?>
                                <td nowrap="nowrap"><?=$v['name'];?></td>
                                <td nowrap="nowrap"><?=$v['phone']; ?></td>
                                <td nowrap="nowrap"><?=$v['content']; ?></td>
                                <td nowrap="nowrap"><?=$v['source']=='index'?'首页留言':'领取土曼'; ?></td>
                                <td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['addtime']);?></td>
                                <?php if($permit_ary['edit'] || $permit_ary['del']){?>
                                    <td nowrap="nowrap" class="operation side_by_side">
                                        <?php if($permit_ary['edit']){?><a href="./?m=user&a=usermessage&d=edit&id=<?=$v['id'];?>">{/global.edit/}</a><?php }?>
                                        <?php if($permit_ary['del']){?>
                                            <dl>
                                                <dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
                                                <dd class="drop_down"><a class="del item" href="./?do_action=user.usermessage_del&id=<?=$v['id'];?>" rel="del">{/global.del/}</a></dd>
                                            </dl>
                                        <?php }?>
                                    </td>
                                <?php }?>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <?=html::turn_page($msg_row[1], $msg_row[2], $msg_row[3], '?'.ly200::query_string('page').'&page=');?>
                    <?php
                }else{//没有数据
                    echo html::no_table_data(($Keyword?0:1), './?m=user&a=usermessage&d=edit');
                }?>
            </div>
        <?php
        }else{
        $id=(int)$_GET['id'];
        $msg_row=str::str_code(db::get_one('usermessage', "id='$id'"));
        $lang=$c['manage']['web_lang'];
        $lang_all=$c['manage']['lang_pack']['global']['all'];
        echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
        ?>
            <script type="text/javascript">$(document).ready(function(){user_obj.usermessage_edit_init()});</script>
            <div class="center_container_1000">
                <div class="global_container">
                    <a href="javascript:history.back(-1);" class="return_title">
                        <span class="return">采购消息</span>
                        <span class="s_return">/ <?=$id?'{/global.edit/}':'{/global.add/}';?></span>
                    </a>
                    <form id="message_edit_form" class="global_form">
                        <div class="rows clean">
                            <label>姓名</label>
                            <div class="input"><input type="text" name="name" value="<?=$msg_row['name'];?>" class="box_input" size="50" maxlength="100" notnull="" /></div>
                        </div>

                        <div class="rows clean">
                            <label>手机</label>
                            <div class="input"><input type="text" name="phone" value="<?=$msg_row['phone'];?>" class="box_input" size="50" maxlength="100" notnull="" /></div>
                        </div>
                        <div class="rows clean">
                            <label>内容</label>
                            <div class="input"><input type="text" name="content" value="<?=$msg_row['content'];?>" class="box_input" size="50" maxlength="100" notnull="" /></div>
                        </div>
                        <div class="rows clean clean">
                            <label></label>
                            <div class="input input_button">
                                <input type="button" class="btn_global btn_submit" value="{/global.save/}">
                                <a href="./?m=user&a=usermessage"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
                            </div>
                        </div>
                        <input type="hidden" id="id" name="id" value="<?=$id;?>" />
                        <input type="hidden" name="do_action" value="user.usermessage_edit" />
                    </form>
                </div>
            </div>
        <?php }?>
    </div>
</div>