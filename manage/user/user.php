<?php !isset($c) && exit();?>
<?php
manage::check_permit('user', 1, array('a'=>'user'));//检查权限

//会员等级
$level_select_ary = array();
$level_ary = array();
$level_row = str::str_code(db::get_all('user_level', 'IsUsed=1'));
foreach ((array)$level_row as $k => $v) {
	$level_ary[$v['LId']] = $v;
	$level_select_ary[] = $v;
}

//业务员
$sales_ary = array();
$manage_row=db::get_all('manage_sales');
foreach ((array)$manage_row as $k => $v) {
	$sales_ary[$v['UserId']] = $v;
}

//来源 0:搜索引擎	1:分享平台 99:直接输入 100:其他
$source_ary = array(0=>'search_engine', 1=>'share_platform', 99=>'enter_directly', 100=>'other');

$permit_ary = array(
	'add'		=>	manage::check_permit('user', 0, array('a'=>'user', 'd'=>'add')),
	'edit'		=>	manage::check_permit('user', 0, array('a'=>'user', 'd'=>'edit')),
	'del'		=>	manage::check_permit('user', 0, array('a'=>'user', 'd'=>'del')),
	'export'	=>	manage::check_permit('user', 0, array('a'=>'user', 'd'=>'export'))
);
$UserType = (int)$_GET['UserType'];
if ($c['manage']['do'] == 'index') {
    $Keyword = str::str_code($_GET['Keyword']);
    $Level = (int)$_GET['Level'];
    $SalesId = (int)$_GET['SalesId'];
    $Sort = $_GET['Sort'];		
	$sort_ary = array(
		'1a'	=>	'Consumption asc,',
		'1d'	=>	'Consumption desc,'
	);
    $where = '1';//条件
    if ($UserType == 0) { //0:会员 1:已购买=会员+非会员 2:复购=会员+非会员 3:已屏蔽
        $where .= ' and IsRegistered=1 and Status=1';
    } elseif ($UserType == 1) {
        $where .= ' and ConsumptionTime=1 and Status=1';
    } elseif ($UserType == 2) {
        $where .= ' and ConsumptionTime>1 and Status=1';
    } elseif ($UserType == 3) {
        $where .= ' and Status!=1';
    } elseif ($UserType == 4) {
        $where .= ' and IsNewsletter=1';
    }
    $Keyword && $where.=" and (concat_ws(' ', FirstName, LastName) like '%$Keyword%' or concat_ws('', FirstName, LastName) like '%$Keyword%' or concat_ws(' ', LastName, FirstName) like '%$Keyword%' or concat_ws('', LastName, FirstName) like '%$Keyword%' or Email like '%$Keyword%' or Remark like '%$Keyword%' or yy_code like '%$Keyword%')"; //姓名顺序不限 可以没有空格
    $Level && $where.=" and Level='$Level'";
    $SalesId && $where.=" and SalesId='$SalesId'";
    (int)$_SESSION['Manage']['GroupId']==3 && $where.=" and SalesId='{$_SESSION['Manage']['SalesId']}'";//业务员账号过滤
    $user_row=str::str_code(db::get_limit_page('user', $where, '*', $sort_ary[$Sort].'UserId desc', (int)$_GET['page'], 20));
}

$top_id_name = ($c['manage']['do'] == 'base_info' ? 'user_inside' : 'user');
?>

<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php
	if ($c['manage']['do'] == 'index') {
		$no_sort_url = '?' . ly200::get_query_string(ly200::query_string('page, Sort'));
	?>
		<script type="text/javascript">$(document).ready(function(){user_obj.user_init()});</script>
		<div class="inside_container">
			<h1>{/module.user.user/}</h1>
			<ul class="inside_menu">
            	<?php for ($i = 0; $i < 5; $i++) {?>
				    <li><a href="./?m=user&a=user&UserType=<?=$i;?>"<?=$UserType == $i ? ' class="current"' : '';?>>{/user.menu.<?=$i;?>/}</a></li>
                <?php }?>
			</ul>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<?php
				$column_row = db::get_value('config', "GroupId='custom_column' and Variable='User'", 'Value');
				$custom_ary = str::json_data($column_row, 'decode');
				$column_fixed_ary = array('user.name', 'user.email', 'user.level.level', 'user.reg_time', 'user.last_login_time', 'user.consumption_price');
				$column_ary = array('user.name', 'user.email', 'user.title', 'user.level.level', 'user.reg_time', 'user.reg_ip', 'user.last_login_time', 'user.last_login_ip', 'user.login_times', 'user.consumption_price');
				if ($c['FunVersion'] > 1 || ($c['FunVersion'] == 1 && $c['NewFunVersion'] <= 1)) {//业务员
					$column_ary[] = 'user.sales';
				}?>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/user.level.level/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($level_select_ary, 'Level', $Level, 'Name'.$c['manage']['web_lang'], 'LId', '{/global.select_index/}');?></div>
								</div>
							</div>
							<?php
							if (($c['FunVersion'] > 1 || ($c['FunVersion'] == 1 && $c['NewFunVersion'] <= 1)) && count($sales_ary) && (int)$_SESSION['Manage']['GroupId'] != 3) { //业务员
							?>
								<div class="rows item clean">
									<label>{/manage.manage.permit_name.3/}</label>
									<div class="input">
										<div class="box_select"><?=ly200::form_select($sales_ary, 'SalesId', $SalesId, 'UserName', 'SalesId', '{/global.select_index/}');?></div>
									</div>
								</div>
							<?php }?>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="user" />
						<input type="hidden" name="a" value="user" />
                        <input type="hidden" name="UserType" value="<?=$UserType;?>" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=user&a=user&d=add">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<?php if((($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)) && (int)$_SESSION['Manage']['GroupId']<3) || db::get_row_count('user_level','IsUsed=1')){ ?>
		            	<?php if($permit_ary['edit']){?><li><a class="edit bat_close" href="javascript:;">{/global.edit_bat/}</a></li><?php }?>
		            <?php } ?>
					<?php if($permit_ary['export']){?><li><a class="explode" href="./?m=user&a=user&d=explode&UserType=<?=$UserType;?>">{/global.explode/}</a></li><?php }?>
				</ul>
			</div>
			<?php
			if($user_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="10%" nowrap="nowrap">{/user.yy_code/}</td>
							<td width="20%" nowrap="nowrap">{/user.email/}</td>
                            <td width="20%" nowrap="nowrap">{/user.name/}</td>
							<td width="10%" nowrap="nowrap">{/manage.manage.permit_name.3/}</td>
							<td width="10%" nowrap="nowrap">{/user.level.level/}</td>
							<td width="15%" nowrap="nowrap">{/user.last_login/}</td>
                            <td width="8%" nowrap="nowrap">{/user.consumption_times/}</td>
							<td width="10%" nowrap="nowrap">
								<a href="<?=$no_sort_url.'&Sort='.($Sort=='1a'?'1d':'1a');?>">{/user.consumption_price/}<i class="<?php if($Sort=='1d') echo 'sort_icon_arrow_down'; elseif($Sort=='1a') echo 'sort_icon_arrow_up'; else echo 'sort_icon_arrow';?>"></i></a>
							</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($user_row[0] as $v){
							$level_img=$level_ary[$v['Level']]['PicPath'];
							$level_name=$level_ary[$v['Level']]['Name'.$c['manage']['web_lang']];
						?>
							<tr class="<?=$v['Status']!=1 ? 'lock' : '';?>">
								<?php if($permit_ary['edit'] || $permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['UserId']);?></td><?php }?>
								<td nowrap="nowrap"><?
								if ($v['Other'])
								{
								    $str=str_replace("&quot;", "\"", $v['Other']);
								    $d=(json_decode($str,true));
								    echo $d["1"];
								}
								?></td>
								<td nowrap="nowrap">
									<a href="./?m=operation&a=email&d=send&Email=<?=$v['Email'];?>" class="btn_email"><i class="icon_email"></i></a>
									<span><?=$v['Email'];?></span>
									<div class="clean">
                                    	<?php if($v['IsNewsletter']==1 && $UserType!=4){?><div class="non_user">{/user.newsletter.subscription/}</div><?php }?>
                                    	<?php if(!$v['IsRegistered']){?><div class="non_user">{/user.tourist/}</div><?php }?>
                                    	<?php if($v['Status']==2){?><div class="non_user">{/user.newsletter.verification/}</div><?php }?>
                                        <?php if($v['RefererId']){?><div class="non_user">{/mta.conversion.<?=$source_ary[$v['RefererId']];?>/}</div><?php }?>
                                    </div>
								</td>
                                <td nowrap="nowrap"><a href="<?=$permit_ary['edit']?'./?m=user&a=user&d=base_info&UserId='.$v['UserId']:'javascript:;';?>"><?=$v['FirstName'].' '.$v['LastName'];?></a></td>
								<td nowrap="nowrap"<?=($permit_ary['edit'] && (int)$_SESSION['Manage']['GroupId']!=3)?' class="sales_select"':'';?> data-id="<?=$v['SalesId'];?>"><?=$v['SalesId']?$sales_ary[$v['SalesId']]['UserName']:'N/A';?></td>
								<td nowrap="nowrap"><?=$v['Level']?"<img src='{$level_img}' alt='{$level_name}' title='{$level_name}' />":'';?></td>
								<td nowrap="nowrap">
									<?=date('Y-m-d H:i:s', $v['LastLoginTime'])?>
									<div class="color_888"><?=$v['LastLoginIp'].'<br />'.ly200::ip($v['LastLoginIp']);?></div>
								</td>
								<td nowrap="nowrap"><?=$v['ConsumptionTime']?></td>
								<td nowrap="nowrap"><?=$c['manage']['currency_symbol'].$v['Consumption'];?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=user&a=user&d=base_info&UserId=<?=$v['UserId'];?>">{/global.view/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<a class="del item" href="./?do_action=user.user_del&UserId=<?=$v['UserId'];?>" rel="del">{/global.del/}</a>
													<a class="status item" style="display:<?=$v['Status']==1 ? 'block' : 'none'; ?>;" href="./?do_action=user.user_status_edit&UserId=<?=$v['UserId'];?>&Status=0" rel="del">{/user.status.3/}</a>
													<a class="status item" style="display:<?=$v['Status']!=1 ? 'block' : 'none'; ?>;" href="./?do_action=user.user_status_edit&UserId=<?=$v['UserId'];?>&Status=1" rel="del"><?=$v['Status']==0 ? '{/user.status.4/}' : '{/user.newsletter.approved/}'; ?></a>
                                                    <a class="item password_edit" href="javascript:;" data-userid="<?=$v['UserId']?>" rel="del">{/user.info.password_info/}</a>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($user_row[1], $user_row[2], $user_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=user&a=user&d=add');
			}?>
		</div>
		<div id="sales_select_hide" class="hide"><?=ly200::form_select($sales_ary, 'SalesId', '', 'UserName', 'SalesId', '{/global.select_index/}', 'class="box_input"');?></div>
        <div id="fixed_right">
            <div class="global_container password_edit">
                <script language="javascript">$(document).ready(function(){user_obj.user_password_edit_init();});</script>
                <div class="top_title">{/user.info.password_info/}<a href="javascript:;" class="close"></a></div>
                <form class="global_form" id="password_edit">
                    <div class="rows clean">
                        <label>{/user.info.new_password/}</label>
                        <div class="input"><input type="password" name="NewPassword" class="box_input" size="25" maxlength="16" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>{/user.info.confirm_password/}</label>
                        <div class="input"><input type="password" name="ReNewPassword" class="box_input" size="25" maxlength="16" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label></label>
                        <div class="input input_button">
                            <input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
                            <input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
                        </div>
                    </div>
                    <input type="hidden" name="UserId" value="<?=$UserId;?>" />
                    <input type="hidden" name="do_action" value="user.user_password_edit" />
                </form>
            </div>
        </div>
	<?php
	}elseif($c['manage']['do']=='add'){//添加会员
		$reg_ary=str::json_data(db::get_value('config', "GroupId='user' and Variable='RegSet'", 'Value'), 'decode');
		$set_row=str::str_code(db::get_all('user_reg_set', '1', '*', "{$c['my_order']} SetId asc"));
	?>
		<script type="text/javascript">$(document).ready(function(){user_obj.user_add_init()});</script>
		<div class="blank20"></div>
		<div class="center_container">
			<div class="global_container">
				<form id="user_edit_form" class="global_form">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/module.user.user/}</span> 
						<span class="s_return">/ {/global.add/}</span>
					</a>
					<div class="rows clean">
						<label>{/user.first_name/}<?=$reg_ary['Name'][1]?' <span class="fc_red">*</span>':'';?></label>
						<div class="input"><input name="FirstName" id="FirstName" class="box_input" type="text" size="30" maxlength="20"<?=$reg_ary['Name'][1]?' notnull':'';?> /></div>						
					</div>
					<div class="rows clean">
						<label>{/user.last_name/}<?=$reg_ary['Name'][1]?' <span class="fc_red">*</span>':'';?></label>
						<div class="input"><input name="LastName" id="LastName" class="box_input" type="text" size="30" maxlength="20"<?=$reg_ary['Name'][1]?' notnull':'';?> /></div>						
					</div>
<!-- 					<div class="rows clean"> -->
<!-- 						<label>{/user.yy_code/}</label> -->
<!-- 						<div class="input"><input name="yy_code" id="yy_code" class="box_input" type="text" size="30" maxlength="20" notnull /></div>						 -->
<!-- 					</div> -->
					<div class="rows clean">
						<label>{/user.email/} <span class="fc_red">*</span></label>
						<div class="input"><input name="Email" id="Email" class="box_input" type="text" size="40" maxlength="100" format="Email" notnull /></div>						
					</div>
					<div class="rows clean">
						<label>{/user.password/} <span class="fc_red">*</span></label>
						<div class="input"><input name="Password" id="Password" class="box_input" type="password" size="40" notnull /></div>						
					</div>
					<div class="rows clean">
						<label>{/user.info.confirm_password/} <span class="fc_red">*</span></label>
						<div class="input"><input name="Password2" id="Password2" class="box_input" type="password" size="40" notnull /></div>						
					</div>
					<?php
					if(($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)) && (int)$_SESSION['Manage']['GroupId']<3){
					?>
						<div class="rows clean">
							<label>{/manage.manage.permit_name.3/}</label>
							<div class="input">
								<div class="box_select">
									<select name="SalesId" class="box_input">
										<option value="">{/global.select_index/}</option>
										<?php
										foreach((array)$manage_row as $k=>$v){
										?>
											<option value="<?=$v['SalesId'];?>"<?=$user['SalesId']==$v['SalesId']?' selected':'';?>><?=$v['UserName'];?></option>
										<?php }?>
									</select>
								</div>
							</div>							
						</div>
					<?php }?>
					<?php
					foreach((array)$reg_ary as $k=>$v){
						if($k=='Name' || $k=='Email' || $k=='Code' || $k=='Country' || !$v[0]) continue;
					?>
						<div class="rows clean">
							<label>{/user.reg_set.<?=$k;?>/}<?=$v[1]?' <span class="fc_red">*</span>':'';?></label>
							<div class="input"><?=user::user_reg_edit($k, $v[1], 'box_input');?></div>							
						</div>
					<?php
					}
					foreach((array)$set_row as $k=>$v){
					?>
						<div class="rows clean">
							<label><?=$v['Name'.$c['manage']['web_lang']];?></label>
							<div class="input">
								<?php
								if($v['TypeId']){
									echo '<div class="box_select">'.ly200::form_select(explode("\r\n", $v['Option'.$c['manage']['web_lang']]), "Other[{$v['SetId']}]", '', '', '', 'Please select...', 'class="box_input"').'</div>';
								}else{
									echo user::form_edit('', 'text', "Other[{$v['SetId']}]", 30, 50, 'class="box_input"');
								}?>
							</div>							
						</div>
					<?php }?>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=user&a=user"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>						
					</div>
					<input type="hidden" name="do_action" value="user.user_add" />
				</form>
			</div>
		</div>
		<div class="blank20"></div>
	<?php
	}elseif($c['manage']['do']=='explode'){
		$where = '1'; //条件
		$page_count = 100; //显示数量
        if ($UserType == 0) { //0:会员 1:已购买=会员+非会员 2:复购=会员+非会员 3:已屏蔽
            $where .= ' and IsRegistered=1 and Status=1';
        } elseif ($UserType == 1) {
            $where .= ' and ConsumptionTime=1 and Status=1';
        } elseif ($UserType == 2) {
            $where .= ' and ConsumptionTime>1 and Status=1';
        } elseif ($UserType == 3) {
            $where .= ' and Status!=1';
        } elseif ($UserType == 4) {
            $where .= ' and IsNewsletter=1';
        }
		(int)$_SESSION['Manage']['GroupId'] == 3 && $where .= " and SalesId='{$_SESSION['Manage']['SalesId']}'";//业务员账号过滤
		$user_row = str::str_code(db::get_limit_page('user', $where, '*', 'UserId desc', (int)$_GET['page'], $page_count));
		echo ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
		?>
		<script type="text/javascript">$(document).ready(function(){user_obj.user_explode_init()});</script>
		<div class="center_container_1200">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">{/module.user.module_name/}</span> 
                <span class="s_return">/ {/global.explode/}</span>
            </a>
			<div class="global_container">
				<form id="explode_edit_form" class="global_form">
					<div class="rows clean">
						<label>{/user.reg_time/}</label>
						<div class="input">
							<input name="RegTime" type="text" value="" class="box_input input_time" size="55" />
						</div>
					</div>
					<div class="rows clean">
						<label>{/user.level.level/}</label>
						<div class="input">
							<div class="blank6"></div>
							<?php
							foreach((array)$level_row as $k=>$v){
							?>
								<span class="input_checkbox_box">
									<span class="input_checkbox">
										<input type="checkbox" name="Level[]" value="<?=$v['LId'];?>">
									</span><?=$v['Name'.$c['manage']['web_lang']];?>
								</span> &nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.explode/}">
							<a href="./?m=user&a=user"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>
					</div>
					<div id="explode_progress"></div>
					<input type="hidden" name="do_action" value="user.user_explode" />
					<input type="hidden" name="Number" value="0" />
                    <input type="hidden" name="UserType" value="<?=$UserType;?>" />
				</form>
			</div>
		</div>
	<?php
		}elseif($c['manage']['do']=='batch_edit'){	//批量修改
	?>
        <script language="javascript">$(document).ready(function(){user_obj.batch_edit_init();});</script>
        <div id="user_inside">
	        <div class="inside_container">
				<h1>{/products.products.batch_edit/}</h1>
			</div>
			<div class="global_container">
		        <form id="batch_edit_form" class="global_form">
	            	<?php if(($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)) && (int)$_SESSION['Manage']['GroupId']<3){ ?>
	                <div class="rows clean">
	                    <label>{/manage.manage.permit_name.3/}</label>
	                    <div class="input">
							<div class="box_select">
								<select name="SalesId">
									<option value="">{/global.select_index/}</option>
									<?php
									foreach((array)$manage_row as $k=>$v){
									?>
										<option value="<?=$v['SalesId'];?>"><?=$v['UserName'];?></option>
									<?php }?>
								</select>
							</div>
	                    </div>		                   
	                </div>
	                <?php }?>
	                <div class="rows clean">
	                    <label>{/user.level.level/}</label>
	                    <div class="input">
	                        <div class="box_select"><?=ly200::form_select($level_select_ary, 'Level', '', 'Name'.$c['manage']['web_lang'], 'LId', '{/global.select_index/}');?></div>
	                    </div>		                   
	                </div>
		            <div class="rows clean">
		                <label></label>
		                <div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=user&a=user"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>	               
		            </div>
		            <?php
						$userid_list=explode('-',trim($_GET['userid_list']));
						foreach((array)$userid_list as $v){
					?>
		            <input type="hidden" name="UserId[]" value="<?=$v;?>" />
		            <?php }?>
		            <input type="hidden" name="do_action" value="user.user_batch_edit" />
		        </form>
		    </div>
		</div>
	<?php
	}else{
		$UserId=(int)$_GET['UserId'];
		$user=str::str_code(db::get_one('user', "UserId={$UserId}"));
		$g_Page=(int)$_GET['page'];
		$g_Page<1 && $g_Page=1;
		if($c['manage']['do']=='base_info'){
			$RegSet=db::get_value('config', "GroupId='user' and Variable='RegSet'", 'Value');
			$set_ary=array();
			$set_row=str::str_code(db::get_all('user_reg_set', '1', '*', "{$c['my_order']} SetId asc"));
			foreach((array)$set_row as $v){
				$set_ary[$v['SetId']]=$v;
				if($v['TypeId']){
					$set_ary[$v['SetId']]['Option']=explode("\r\n", $v['Option'.$c['manage']['web_lang']]);
				}
			}
			$ship_row=str::str_code(db::get_all('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.UserId={$UserId} and a.IsBillingAddress=0", 'a.*, c.Country, c.Code, s.States as StateName', 'a.AccTime desc, a.AId desc'));
			$bill_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.UserId={$UserId} and a.IsBillingAddress=1", 'a.*, c.Country, c.Code, s.States as StateName'));
		}elseif($c['manage']['do']=='order_info'){
			$row_count=20;
			$row=str::str_code(db::get_limit_page('orders', "UserId={$UserId}", '*', 'OrderId desc', $g_Page, $row_count));
		}elseif($c['manage']['do']=='favorite_info'){
			$row_count=10;
			$row=str::str_code(db::get_limit_page('user_favorite u left join products p on u.ProId=p.ProId', "u.UserId={$UserId}", 'p.*, u.AccTime as AddTime', 'u.FId desc', $g_Page, $row_count));
			//获取类别列表
			$cate_ary=str::str_code(db::get_all('products_category','1','*'));
			$category_ary=array();
			foreach((array)$cate_ary as $v){
				$category_ary[$v['CateId']]=$v;
			}
			$category_count=count($category_ary);
			unset($cate_ary);
		}elseif($c['manage']['do']=='cart_info'){
			$row_count=10;
			$row=str::str_code(db::get_limit_page('shopping_cart c left join products p on c.ProId=p.ProId', "c.UserId={$UserId}", 'p.*, c.PicPath as CartPicPath, c.Price as CartPrice, c.Qty as CartQty, c.Property as CartProperty, c.PropertyPrice as CartPropertyPrice, c.Remark as CartRemark, c.AddTime as CartAddTime', 'c.CId desc', $g_Page, $row_count));
		}elseif($c['manage']['do']=='message_info'){
			$row_count=10;
			$row=str::str_code(db::get_limit_page('user_message', "((UserId like '%|{$UserId}|%' or UserId='-1') and Type=1) or (UserId='$UserId' and Type=0)", '*', 'MId desc', $g_Page, $row_count));
		}elseif($c['manage']['do']=='log_info'){
			$row_count=10;
			$row=str::str_code(db::get_limit_page('user_operation_log', "UserId='{$UserId}'", '*', 'LId desc', $g_Page, $row_count));
		}

	?>		
		<div class="inside_container">
			<h1>{/module.user.user/}</h1>
			<ul class="inside_menu">
				<?php
				$d_ary=array('edit', 'base_info', 'order_info', 'favorite_info', 'cart_info', 'log_info');//, 'explode'
				if(!manage::check_permit('user', 0, array('a'=>'user', 'd'=>'edit'))) unset($d_ary[6]);
				for($i=1,$l=count($d_ary); $i<$l; ++$i){
				?>
					<li><a <?=$c['manage']['do']==$d_ary[$i]?'class="current"':'';?> href="./?m=user&a=user&d=<?=$d_ary[$i];?>&UserId=<?=$UserId;?>">{/user.info.<?=$d_ary[$i];?>/}</a></li>
				<?php }?>
			</ul>
		</div>
		<?php
		/******************** 基本信息 ********************/
		if ($c['manage']['do'] == 'base_info') {
            echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
		?>
			<script language="javascript">$(document).ready(function(){user_obj.user_base_edit_init();});</script>
			<form id="edit_form" class="global_form center_container_1200">
				<div class="left_container">
					<div class="left_container_side">
						<div class="global_container base_info">
							<div class="big_title">{/user.info.base_info/}</div>
							<div class="email color_555"><?=$user['Email'];?>&nbsp;&nbsp;&nbsp;<a href="./?m=email&d=send&Email=<?=urlencode($user['Email'] . '/' . $user['FirstName'] . ' ' . $user['LastName']);?>" title="{/module.email.send/}"></a></div>
							<div class="u_info">
                                <?php if ($user['FirstName'] || $user['LastName']) {?>
                                    <span class="desc color_555 f16"><?=$user['FirstName'].' '.$user['LastName'];?></span>
                                    <span class="w50"></span>
                                <?php }?>
                                <span class="tit color_000">{/user.title/}:</span>
                                <span class="desc color_555"><?=$user['Gender']?$c['gender'][$user['Gender']]:'{/user.secrecy/}';?></span>
							</div>
                            <?php if($user['RefererId']){?>
                                <div class="source"><span class="color_000">{/mta.source/}: </span>{/mta.conversion.<?=$source_ary[$user['RefererId']];?>/}</div>
                            <?php }?>
							<?php
							if (($c['FunVersion'] > 1 || ($c['FunVersion'] == 1 && $c['NewFunVersion'] <= 1)) && (int)$_SESSION['Manage']['GroupId'] < 3) {
							?>
								<div class="rows clean">
									<label>{/manage.manage.permit_name.3/}</label>
									<div class="input">
										<div class="box_select">
											<select name="SalesId">
												<option value="">{/global.select_index/}</option>
												<?php foreach ((array)$manage_row as $k => $v) {?>
													<option value="<?=$v['SalesId'];?>"<?=$user['SalesId'] == $v['SalesId'] ? ' selected' : '';?>><?=$v['UserName'];?></option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
							<?php }?>
							<div class="rows clean">
								<label>{/user.level.level/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($level_select_ary, 'Level', $user['Level'], 'Name' . $c['manage']['web_lang'], 'LId', '{/global.select_index/}');?></div>
								</div>
							</div>
							<?php if ($c['FunVersion'] >= 1 && $c['manage']['config']['UserStatus']) {?>
			                    <div class="rows clean">
			                        <label>{/set.config.user_status/}</label>
			                        <div class="input">
			                            <div class="switchery<?=$user['Status'] ? ' checked' : '';?>">
			                                <input type="checkbox" name="Status" value="1"<?=$user['Status'] ? ' checked' : '';?>>
			                                <div class="switchery_toggler"></div>
			                                <div class="switchery_inner">
			                                    <div class="switchery_state_on"></div>
			                                    <div class="switchery_state_off"></div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
							<?php }?>
							<?php
							$reg_ary = str::json_data($RegSet, 'decode');
							foreach ((array)$reg_ary as $k => $v) {
								if ($k == 'Name' || $k == 'Email' || !$v[0] || $k == 'Country' || $k == 'Code') continue;
							?>
								<div class="rows clean">
			                        <label>{/user.reg_set.<?=$k;?>/}</label>
			                        <div class="input"><?=user::user_reg_edit($k, $v[1], 'box_input', $user);?></div>
			                    </div>
							<?php }?>							
							<?php
							if ($user['Other']) {
								$other_row = str::json_data(htmlspecialchars_decode($user['Other']), 'decode');
								$other_ary = array();
								foreach ((array)$other_row as $k => $v) {
									$other_ary['Other[' . $k . ']'] = $v;
								}
								foreach ((array)$set_row as $k => $v) {
							?>
									<div class="rows clean">
										<label><?=$v['Name' . $c['manage']['web_lang']];?></label>
										<div class="input">
											<?php
											if ($v['TypeId']) {
												echo '<div class="box_select">' . ly200::form_select(explode("\r\n", $v['Option' . $c['manage']['web_lang']]), "Other[{$v['SetId']}]", $other_ary["Other[{$v['SetId']}]"], '', '', '{/global.select_index/}', 'class="box_input"').'</div>';
											} else {
												echo user::form_edit($other_ary, 'text', "Other[{$v['SetId']}]", 30, 50, 'class="box_input"');
											}?>
										</div>							
									</div>
							<?php
                                }
							}?>
			                <div class="rows clean">
								<label>{/user.remark/}</label>
								<div class="input"><textarea class="box_textarea" name='Remark'><?=$user['Remark'];?></textarea></div>
							</div>
							<input type="hidden" name="UserId" value="<?=$UserId;?>" />
							<input type="hidden" name="do_action" value="user.user_base_info_edit" />
							
						</div>
						<div class="global_container">
							<div class="address_info">
								<div class="address_list">
									<div class="addr_tit">{/user.address.y_bill_address/}</div>
									<div class="addr_box">
										<p class="name"><?=$bill_row['FirstName'] . ' ' . $bill_row['LastName'];?></p>
										<ul>
											<li><span class="th">{/user.address.address_line1/}:</span><span class="desc"><?=$bill_row['AddressLine1'];?></span></li>
											<li><span class="th">{/user.address.address_line2/}:</span><span class="desc"><?=$bill_row['AddressLine2'];?></span></li>
											<li><span class="th">{/user.address.address/}:</span><span class="desc"><?=$bill_row['City'] . ', ' . ($bill_row['StateName'] ? $bill_row['StateName'] : $bill_row['State']) . ', ' . $bill_row['ZipCode'];?></span></li>
											<li><span class="th">{/user.address.country/}:</span><span class="desc"><?=$bill_row['Country'];?></span></li>
											<li><span class="th">{/user.address.phone/}:</span><span class="desc">+<?=$bill_row['Code'] . ' ' . $bill_row['PhoneNumber'];?></span></li>
										</ul>
									</div>
								</div>
								<div class="address_list">
									<div class="addr_tit">{/user.address.y_ship_address/}</div>
									<?php
									foreach ((array)$ship_row as $k => $v) {
									?>
										<div class="addr_box <?=$k > 0 ? 'hide' : '';?>">
											<p class="name"><?=$v['FirstName'] . ' ' . $v['LastName'];?></p>
											<ul>
												<li><span class="th">{/user.address.address_line1/}:</span><span class="desc"><?=$v['AddressLine1'];?></span></li>
												<li><span class="th">{/user.address.address_line2/}:</span><span class="desc"><?=$v['AddressLine2'];?></span></li>
												<li><span class="th">{/user.address.address/}:</span><span class="desc"><?=$v['City'] . ', ' . ($v['StateName'] ? $v['StateName'] : $v['State']) . ', ' . $v['ZipCode'];?></span></li>
												<li><span class="th">{/user.address.country/}:</span><span class="desc"><?=$v['Country'];?></span></li>
												<li><span class="th">{/user.address.phone/}:</span><span class="desc">+<?=$v['Code'].' '.$v['PhoneNumber'];?></span></li>
											</ul>
											<?php if ($k == 0 && count($ship_row) > 1){ ?>
												<a href="javascript:;" class="more">{/global.more/}</a>
											<?php }?>
										</div>
									<?php }?>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="right_container">
					<div class="global_container">
						<div class="big_title">{/user.info.order_view/}</div>
                        <table class="u_orders_info">
                        	<tr>
								<th>{/user.consumption_price/}</th>
								<td><?=$c['manage']['currency_symbol'] . $user['Consumption'];?></td>
                            </tr>
                            <tr>
								<th>{/user.consumption_times/}</th>
								<td><?=$user['ConsumptionTime'] == 1 ? '{/user.info.first_buy/}' : $user['ConsumptionTime'];?></td>
                            </tr>
                            <tr>
								<th>{/user.info.prod_num/}</th>
								<td><?=db::get_row_count('orders_products_list p left join orders o on p.OrderId=o.OrderId', "o.UserId='$UserId' and o.OrderStatus in (4,5,6)");?></td>
                            </tr>
                            <?php
                            if ($user['ConsumptionTime'] > 1 && db::get_row_count('orders', "UserId='$UserId' and OrderStatus in (4,5,6)") > 1) {
                                //上一次购买时间
                                $user_last_order = db::get_limit('orders', "UserId='$UserId' and OrderStatus in (4,5,6)", 'OrderTime', 'OrderId desc', 0, 2);
                                //计算复购周期
                                $user_order = db::get_all('orders', "UserId='$UserId' and OrderStatus in (4,5,6)", 'OrderTime', 'OrderId asc');
                                $order_times = 0;
                                $order_count = 0;
                                foreach ((array)$user_order as $k => $v) {
                                    if (!$k) continue; //第一张单不算
                                    $order_count++;
                                    $order_times += $v['OrderTime'] - $user_order[$k - 1]['OrderTime'];
                                }
                                $user_average_time = ceil($order_times / $order_count / 86400);
							?>
                                <tr>
                                    <th>{/user.info.last_buy_time/}</th>
                                    <td><?=date('Y-m-d H:i:s', $user_last_order[1]['OrderTime']);?></td>
                                </tr>
                                <tr>
                                    <th>{/user.info.re_buy_time/}</th>
                                    <td><?=$user_average_time;?>{/user.info.day/}</td>
                                </tr>
                            <?php }?>
                            <?php if($user['Status']==0){?>
                                <tr>
                                    <th>{/user.status.0/}</th>
                                    <td>{/user.status.1/}</td>
                                </tr>
                            <?php }?>
                        </table>
					</div>
					<div class="global_container">
						<div class="big_title">{/user.info.active/}</div>
						<table class="active_situation">
							<tr>
								<th>{/user.login_times/}</th>
								<td><?=$user['LoginTimes'];?></td>
							</tr>
							<tr>
								<th>{/user.last_login/}</th>
								<td>{/global.time/}: <?=date('Y-m-d H:i:s', $user['LastLoginTime']);?><br />Ip: <?=$user['LastLoginIp'] . '<br />【' . ly200::ip($user['LastLoginIp']) . '】';?></td>
							</tr>
							<tr>
								<th>{/user.reg_ip/}</th>
								<td><?=$user['RegIp'] . '<br />【' . ly200::ip($user['RegIp']) . '】';?></td>
							</tr>
							<tr>
								<th>{/user.reg_time/}</th>
								<td><?=date('Y-m-d H:i:s', $user['RegTime']);?></td>
							</tr>
						</table>
					</div>
					<?php /*?><div class="global_container">
						<div class="big_title">标签</div>
						<div class="rows clean">
							<div class="blank6"></div>
							<div class="input">
								<span class="btn_choice current"><b>老客户</b><input type="checkbox" value="1" name="Tags" checked="checked"><i></i></span>
								<span class="btn_choice"><b>美妆</b><input type="checkbox" value="1" name="Tags" checked="checked"><i></i></span>
							</div>
						</div>
						<div class="big_title "><!-- border_t -->
							<a href="javascript:;" class="password_edit">{/global.edit/}</a>
							{/user.password/}
						</div>
					</div><?php */?>
				</div>
				<div class="clear"></div>
			</form>
			<div class="rows fixed_btn_submit">
				<div class="center_container_1200">
					<div class="input">
						<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}">
						<a href="./?m=user&a=user" class="btn_global btn_cancel">{/global.return/}</a>
					</div>
				</div>
			</div>
		<?php
		/******************** 订单信息 ********************/
		} elseif ($c['manage']['do'] == 'order_info') {
		?>
			<div class="inside_table">
				<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="20%" nowrap="nowrap">{/orders.oid/}</td>
							<td width="15%" nowrap="nowrap">{/orders.info.product_price/}</td>
							<td width="15%" nowrap="nowrap">{/orders.info.charges_insurance/}</td>
							<td width="15%" nowrap="nowrap">{/orders.total_price/}</td>
							<td width="15%" nowrap="nowrap">{/orders.orders_status/}</td>
							<td width="15%" nowrap="nowrap">{/global.time/}</td>
							<td width="6%" nowrap="nowrap" class="operation">{/global.operation/}</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach ($row[0] as $v) {
							$isFee = ($v['OrderStatus'] >= 4 && $v['OrderStatus'] != 7) ? 1 : 0;
							$total_price = orders::orders_price($v, $isFee, 1);
						?>
                            <tr>
                                <td><?=$v['OId'];?></td>
                                <td><?=$c['manage']['currency_symbol'] . sprintf('%01.2f', $v['ProductPrice']);?></td>
                                <td><?=$c['manage']['currency_symbol'] . sprintf('%01.2f', $v['ShippingPrice'] + $v['ShippingInsurancePrice']);?></td>
                                <td><?=$c['manage']['currency_symbol'] . sprintf('%01.2f', $total_price);?></td>
                                <td><?=$c['orders']['status'][$v['OrderStatus']];?></td>
                                <td><?=$v['OrderTime'] ? date('Y-m-d', $v['OrderTime']) : 'N/A';?></td>
                                <td class="last operation"><a href="./?m=orders&a=orders&d=view&OrderId=<?=$v['OrderId'];?>" title="{/global.view/}">{/global.view/}</a></td>
                                <td></td>
                            </tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($row[1], $row[2], $row[3], '?' . ly200::query_string('page') . '&page=');?>
			</div>
		<?php
		/******************** 地址薄 ********************/
		}elseif($c['manage']['do']=='address_info'){
		?>
			<div id="lib_user_address" class="index_pro_list clearfix">
				<div class="billing_addr">
					<h3 class="big">Your Billing Address</h3>
					<div class="clear"></div>
					<ul class="addr_item">
						<li><strong><?=$bill_row['FirstName'].' '.$bill_row['LastName'];?></strong></li>
						<li><?=$bill_row['AddressLine1'];?></li>
						<li><?=$bill_row['City'];?>, <?=($bill_row['StateName']?$bill_row['StateName']:$bill_row['State']);?>, <?=$bill_row['ZipCode'];?></li>
						<li><?=$bill_row['Country'];?></li>
						<li><span class="phone_icon">Phone:+<?=$bill_row['Code'].' '.$bill_row['PhoneNumber'];?></span></li>
					</ul>
				</div>
				<div class="shipping_addr">
					<h3 class="big">Your Shipping Address</h3>
					<div class="clearfix addr_list">
						<?php foreach($ship_row as $v){?>
							<ul class="addr_item fl">
								<li><strong><?=$v['FirstName'].' '.$v['LastName'];?></strong></li>
								<li><?=$v['AddressLine1'];?></li>
								<li><?=$v['City'];?>, <?=($v['StateName']?$v['StateName']:$v['State']);?>, <?=$v['ZipCode'];?></li>
								<li><?=$v['Country'];?></li>
								<li><span class="phone_icon">Phone:+<?=$v['Code'].' '.$v['PhoneNumber'];?></span></li>
							</ul>
						<?php }?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		<?php
		/******************** 收藏夹 ********************/
		}elseif($c['manage']['do']=='favorite_info'){
		?>
			<div class="inside_table">
				<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="12%" nowrap="nowrap">&nbsp;&nbsp;{/products.picture/}</td>
							<td width="33%" nowrap="nowrap">{/products.name/}</td>
							<td width="15%" nowrap="nowrap">{/products.products.number/}</td>
							<td width="20%" nowrap="nowrap">{/products.classify/}</td>
							<td width="18%" nowrap="nowrap" class="last">{/user.info.add_time/}</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($row[0] as $v){
						?>
						<tr>
							<td class="img pic_box"><img class="photo" src="<?=ly200::get_size_img($v['PicPath_0'], '168x168');?>" align="absmiddle" /><span></span></td>
							<td><?=$v['Name'.$c['manage']['web_lang']];?></td>
							<td><?=$v['Number'];?></td>
							<td>
								<?php
								$UId=$category_ary[$v['CateId']]['UId'];
								if($UId){
									$key_ary=@explode(',',$UId);
									array_shift($key_ary);
									array_pop($key_ary);
									foreach((array)$key_ary as $k2=>$v2){
										echo $category_ary[$v2]['Category'.$c['manage']['web_lang']].'->';
									}
								}
								echo $category_ary[$v['CateId']]['Category'.$c['manage']['web_lang']];
								?>
							</td>
							<td class="last operation"><?=$v['AddTime']?date('Y-m-d', $v['AddTime']):'N/A';?></td>
							<td></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($row[1], $row[2], $row[3], '?'.ly200::query_string('page').'&page=');?>
			</div>
		<?php
		/******************** 购物车 ********************/
		}elseif($c['manage']['do']=='cart_info'){
		?>
			<div class="inside_table">
				<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="12%" nowrap="nowrap">&nbsp;&nbsp;{/products.picture/}</td>
							<td width="30%" nowrap="nowrap">{/products.name/}</td>
							<td width="15%" nowrap="nowrap">{/products.attribute/}</td>
							<td width="8%" nowrap="nowrap">{/user.info.unit_pirce/}</td>
							<td width="8%" nowrap="nowrap">{/products.products.qty/}</td>
							<td width="15%" nowrap="nowrap">{/user.info.subtotal/}</td>
							<td width="13%" nowrap="nowrap" class="last">{/user.info.add_time/}</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$cart_attr=$cart_attr_data=array();
						$i=1;
						foreach($row[0] as $v){
							$qty=$v['CartQty'];
							$img=ly200::get_size_img($v['CartPicPath'], '240x240');
							$price=$v['CartPrice']+$v['CartPropertyPrice'];
							$total_price=$qty*$price;
							// $attr = array();
							// $v['CartProperty']!='' && $attr=str::json_data(htmlspecialchars_decode($v['CartProperty']), 'decode');
							$v['CartProperty']!='' && $attr=ly200::get_order_property($v['CartProperty']);
						?>
						<tr>
							<td class="img pic_box"><img class="photo" src="<?=$img;?>" align="absmiddle" /><span></span></td>
							<td style="text-align:left;">
								<div style="margin-bottom:5px;"><?=$v['Name'.$c['manage']['web_lang']];?></div>
								<?=$v['Number']!=''?'<div style="margin-bottom:5px;">{/products.products.number/}: '.$v['Prefix'].$v['Number'].'</div>':'';?>
								<?=$v['SKU']!=''?'<div style="margin-bottom:5px;">{/products.products.sku/}: '.$v['SKU'].'</div>':'';?>
							</td>
							<td>
								<?php
								foreach((array)$attr as $k=>$z){
									if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
									echo "<p>{$k}: {$z}</p>";
								}?>
							</td>
							<td><?=$c['manage']['currency_symbol'].$price;?></td>
							<td><?=$qty;?></td>
							<td><?=$c['manage']['currency_symbol'].$total_price;?></td>
							<td class="last"><?=$v['CartAddTime']?date('Y-m-d', $v['CartAddTime']):'N/A';?></td>
							<td></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($row[1], $row[2], $row[3], '?'.ly200::query_string('page').'&page=');?>
			</div>
		<?php
		/******************** 操作记录 ********************/
		}elseif($c['manage']['do']=='log_info'){
		?>
			<div class="inside_table">
				<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="20%" nowrap="nowrap">{/user.log.title/}</td>
							<td width="20%" nowrap="nowrap">{/user.log.time/}</td>
							<td width="30%" nowrap="nowrap">{/user.log.content/}</td>
							<td width="33%" nowrap="nowrap" class="last">{/user.log.ip/}</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php foreach($row[0] as $v){?>
						<tr>
							<td><?=$v['Log'];?></td>
							<td><?=$v['AccTime']?date('Y-m-d H:i:s', $v['AccTime']):'N/A';?></td>
							<td class="left"><pre class="opt_log"><?=$v['Data'];?></pre></td>
							<td class="last"><?=$v['Ip'].'<br />【'.ly200::ip($v['Ip']).'】';?></td>
							<td></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($row[1], $row[2], $row[3], '?'.ly200::query_string('page').'&page=');?>
			</div>
		<?php
		/******************** 修改密码 ********************/
		}elseif($c['manage']['do']=='password_info'){
		?>
			<script language="javascript">$(document).ready(function(){user_obj.user_password_edit_init();});</script>
			<form id="edit_form" class="global_form">
				<div class="rows clean">
					<label>{/user.info.new_password/}</label>
					<div class="input"><input type="password" name="NewPassword" class="box_input" size="25" maxlength="16" notnull /></div>
				</div>
				<div class="rows clean">
					<label>{/user.info.confirm_password/}</label>
					<div class="input"><input type="password" name="ReNewPassword" class="box_input" size="25" maxlength="16" notnull /></div>
				</div>
				<div class="rows clean">
					<label></label>
					<div class="input"><input type="submit" class="btn_ok" name="submit_button" value="{/global.submit/}" /></div>
				</div>
				<input type="hidden" name="UserId" value="<?=$UserId;?>" />
				<input type="hidden" name="do_action" value="user.user_password_edit" />
			</form>
        <?php }?>
    <?php }?> 
</div>