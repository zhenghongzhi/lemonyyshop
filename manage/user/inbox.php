<?php !isset($c) && exit();?>
<?php
manage::check_permit('user', 1, array('a'=>'inbox'));//检查权限

$permit_ary=array(
	'orders_edit'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'orders',	'p'=>'edit')),
	'orders_del'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'orders',	'p'=>'del')),
	'orders_export'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'orders',	'p'=>'export')),
	'products_edit'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'products','p'=>'edit')),
	'products_del'		=>	manage::check_permit('user', 0, array('a'=>'inbox',	'd'=>'products','p'=>'del')),
	'products_export'	=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'products','p'=>'export')),
	'others_add'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'others',	'p'=>'add')),
	'others_edit'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'others',	'p'=>'edit')),
	'others_del'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'others',	'p'=>'del')),
	'others_export'		=>	manage::check_permit('user', 0, array('a'=>'inbox', 'd'=>'others',	'p'=>'export'))
);

$MId=(int)$_GET['MId'];
$page_count=20;
if(in_array('product_inbox', $c['manage']['plugins']['Used'])){//APP应用开启产品咨询
    $where='m.Module in ("products", "others") and m.UserId>0';
    $unread_where='(Module in ("products", "others") and IsRead=0 and UserId>0) or (Module="products" and IsRead=0 and UserId=0 and CusEmail!="")';
}else{
    $where='m.Module="others" and m.UserId>0';
    $unread_where='Module="others" and IsRead=0 and UserId>0';
}
$unread_count=(int)db::get_row_count('user_message', $unread_where);
$top_id_name=($c['manage']['do']!='others_edit'&&!$MId?'inbox':'inbox_inside');

$row_count=db::get_row_count('user_message m left join user u on m.UserId=u.UserId', $where);
$total_pages=ceil($row_count/$page_count);
?>
<script type="text/javascript">$(document).ready(function(){user_obj.inbox_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<div class="center_container_1000">
        <div class="inside_container">
            <h1>{/module.user.inbox/}</h1>
        </div>
        <div class="inbox_container clean">
            <div class="inbox_left">
                <div class="search">
                    <form id="search_form">
                        <input type="text" class="box_input" name="Keyword" value="" size="15" autocomplete="off" />
                        <input type="button" class="search_btn" value="{/global.search/}" />
                    </form>
                </div>
                <ul class="message_list" data-page="1" data-total-pages="<?=$total_pages;?>" data-count="<?=$page_count;?>" data-animate="0"></ul>
            </div>
            <div class="inbox_right">
                <div class="unread_message"><p class="color_aaa">{/notes.unread_message/}</p><i><?=$unread_count;?></i></div>
                <div class="message_title">
                    <h2 class="color_000"></h2>
                    <div class="products_view">
                        <div class="products_info">
                            <div class="img"><a href="javascript:;"><img src="" /></a></div>
                            <div class="name"><a href="javascript:;" title=""></a></div>
                        </div>
                        <a href="javascript:;" class="products_menu"><i></i></a>
                        <div class="menu_products_list" data-page="0">
                            <div class="arrow"><em></em><i></i></div>
                            <ul class="list"></ul>
                        </div>
                    </div>
                </div>
                <div class="message_dialogue clean"><div class="message_dialogue_list"></div></div>
                <div class="message_bottom">
                	<div class="send_box">
                        <form class="form_message" id="message_inbox_form">
                            <textarea name='Message' class="box_textarea clean" placeholder="{/global.please_enter/} ..." notnull></textarea>
                            <div class="clear"></div>
                            <?=manage::multi_img('MsgPicDetail', 'MsgPicPath'); ?>
                            <input type="button" class="btn_global btn_submit" value="{/global.send/}" />
                            <input type="hidden" name="MId" value="" />
                            <input type="hidden" name="do_action" value="user.inbox_reply" />
                        </form>
                    </div>
                    <div class="mail_box">
                        <a href="javascript:;" class="btn_global btn_submit">{/frame.email/}</a>
                        <div class="txt_tips">{/inbox.non_user_tips/}</div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>