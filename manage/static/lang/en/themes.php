<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
姓名：sheldon
日期: 2014-07-25
备注:分工的语言包开发，上线后该文件统一整合 
*/
return array(
	'pc'			=>	'PC side',
	'mobile'		=>	'Mobile side',
	'url'			=>	'link',
	'show'			=>	'Demonstration',
	'nav'			=>	array(
							'column'		=>	'Column',
							'add_item'		=>	'Add option',
							'custom'		=>	'Custom',
							'url'			=>	'Links address',
							'down'			=>	'SlideDown',
							'down_width'	=>	'SlideDown box width',
							'down_width_ary'=>	array('Small', 'Medium', 'Big'),
							'target'		=>	'New window',
							'myorder'		=>	'Navigation Sorting'
						),
	'products_list'	=>	array(
							'left_right'	=>	'Distribute on left and right',
							'screen'		=>	'Product Screening',
							'box'			=>	'Column Module',
							'leftbar_box'	=>	'Column Module on left side',
							'leftbar'		=>	'Left side',
							'rightbar'		=>	'Right side',
							'list_rank'		=>	'Product Arrange Mode',
							'list_count'	=>	'Product Arrange Number',
							'order'			=>	array('row_1'=>'One in A Row', 'row_2'=>'Two in A Row', 'row_3'=>'Three in A Row', 'row_4'=>'Four in A Row', 'row_5'=>'Five in A Row', 'column'=>'Pinterest'),
							'column_notes'	=>	'After turning on, product list page shall be shown on two-side column',
							'effects'		=>	'The product box effects',
							'effects_order'	=>	'Special way',
							'effects_ary'	=>	array('No special effects', 'Effects 1', 'Effects 2', 'Effects 3', 'Effects 4', 'Effects 5', 'Effects 6')
						),
	'left_bar'		=>	array(
							'Category'		=>	'Product Category',
							'Hot'			=>	'Popular Products',
							'Special'		=>	'Special Products',
							'Banner'		=>	'Advertising Picture',
							'Newsletter'	=>	'Email Subscription',
							'Recommended'	=>	'Recommend Products',
							'TheSay'		=>	'Recommend Comments',
							'Popular'		=>	'Top Search'
						),
	'products_detail'=>	array(
							'share'			=>	'Share Sort',
							'help'			=>	'Customer Help'
						),
	'style'			=>	array(
							'nav_style'					=>	'Navigation style',
							'FontColor'					=>	'Website main color',
							'PriceColor'				=>	'Price color',
							'AddtoCartBgColor'			=>	'Add to cart background',
							'BuyNowBgColor'				=>	'Buy Now Background',
							'ReviewBgColor'				=>	'Comment button background',
							'DiscountBgColor'			=>	'Discount display background',
							'NavBgColor'				=>	'background',
							'NavHoverBgColor'			=>	'Mouse effect',
							'NavBorderColor1'			=>	'Border color (1)',
							'NavBorderColor2'			=>	'Border color (2)',
							'CategoryBgColor'			=>	'Category heading background',
							'ProListBgColor'			=>	'Product list special effects background color',
							'ProListHoverBgColor'		=>	'Product list special effects mouse effect',
							'GoodBorderColor'			=>	'Product detailed color block border color',
							'GoodBorderHoverColor'		=>	'Product detailed color block border mouse color',
							'IndexMenuBg_0'				=>	'Menu background style 1',
							'IndexMenuBg_1'				=>	'Menu background style 2',
							'IndexMenuBg_2'				=>	'Menu background style 3',
						),
);