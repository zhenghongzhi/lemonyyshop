<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791

*/
return array(
	'default_open'	=>	'Default expansion',
	'billing'		=> array(
							'device'		=>	'Device',
							'device_ary'	=>	array('PC','Mobile'),
							'position'		=>	'Position',
							'position_ary'	=>	array('Head','Intermediate','Full Screen',10=>'Group buying',11=>'Spike',12=>'Left side of inside'),
							'url'			=>	'Link',
							'content'		=>	'Content',
							'position_tips'	=>	'Please select an ad slot',
	),
	'explain'		=> array(
							'order'		=>	'Move the cursor to the green dot in front of the customer service account, click and drag to achieve customer service sorting',
							'email'		=>	'After enabling the plugin, directly check the multiple recipients to send, you can achieve group sending',
	),
)
?>