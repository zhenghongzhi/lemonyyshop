<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
姓名：sheldon
日期: 2014-10-9
备注:分工的语言包开发，上线后该文件统一整合 
*/
return array(
	'config'		=>	array(
							'site_name'			=>	'Website Name',
							'logo'				=>	'Logo',
							'ico'				=>	'ICO',
							'web_display'		=>	'Website Display',
							'web_display_ary'	=>	array('Adaptive screen','Narrow screen','Widescreen'),
							'orders_sms'		=>	'Order SMS notification',
							'orders_sms_ary'	=>	array('Successful payment', 'Generate orders'),
							'quick_switch'		=>	'Quick switch',
							'user_view'			=>	'Only Member Browse',
							'user_status'		=>	'Member Audit',
							'tourists_shopping'	=>	'Tourists Shopping',
							'auto_register'		=>	'Automatic registration after the next single Member',
							'user_login'		=>	'Open the default login box',
							'user_verification'	=>	'Validate by mail',
							'shield_ip'			=>	'Shield Internal IP',
							'shield_browser'	=>	'Shield Chinese Browser',
							'cannot_copy'		=>	'Copy Protection',
							'browser_language'	=>	'Browser Select Language',
							'prompt_steps'		=>	'Prompt Steps',
							'301'				=>	'301 Redirect',
							'cdn'				=>	'Enable CDN',
							'cdn_notes'			=>	'After opening, the website static resources enable CDN global acceleration, static resources use [ueeshop.ly200-cdn.com] domain name',
							'is_mobile'			=>	'Mobile Open',
							'close_web'			=>	'Close Website',
							'bulletin_board'	=>	'Bulletin Board',
							'low_consumption'	=>	'Low Consumption',
							'checkout_email'	=>	'Mail notification on order',
							'recent_orders'		=>	'Recent Orders',
							'left_category'		=>	'Left Category',
							'default_open'		=>	'Default Expansion',
							'less_stock'		=>	'Less inventory',
							'less_stock_ary'	=>	array('Place an order', 'Payment'),
							'cart_weight'		=>	'Cart Displaying weight',
							'auto_canceled'		=>	'Automatically Canceled Order',
							'day_unit'			=>	'Days',
							'arrival_info'		=>	'Arrival Info',
							'language_list'		=>	'Language Edition',
							'default_language'	=>	'Website Default Language',
							'manage_language'	=>	'Back-end Language',
							'manage_currency'	=>	'Backend Default Currency',
							'admin_email'		=>	'Administrator E-mail',
							'skype'				=>	'Skype',
							'search_tips'		=>	'Search Prompt',
							'copyright'			=>	'Copyright',
							'search_keyword'	=>	'Search Keyword',
							'blog'				=>	'Blog Copyight',
							'header_content'	=>	'Head Contents',
							'index_content'		=>	'Home Contents',
							'topmenu'			=>	'Top Section',
							'contactmenu'		=>	'Contact Us',
							'contactmenu_ary'	=>	array('Address','Phone','Fax','Email'),
							'sharemenu'			=>	'Share Section',
							'watermark'			=>	'Watermark',
							'basic_settings'	=>	'Basic Setting',
							'is_watermark'		=>	'Open Watermark Added',
							'is_thumbnail'		=>	'Watermark Added to Thumbnail',
							'is_watermark_pro'	=>	'Not Product Picture Watermark',
							'watermark_upfile'	=>	'Upload Watermark',
							'opacity'			=>	'Opacity',
							'opaque'			=>	'Opaque',
							'alpha'				=>	'Image watermark opacity',
							'alpha_png'			=>	'(Parameter Invalid When Watermark to PNG)',
							'water_position_ary'=>	array('','Top is Left-set','Top is Middle-set','Top is Right-set','Middle Part is Left-set','Middle Part is Middle-set','Middle Part is Right-set','Bottom is Left-set','Bottom is Middle-set','Bottom is Right-set'),
							'water'				=>	array(
														'used'			=>	'Use watermark',
														'replace'		=>	'Old watermark replacement',
														'update'		=>	'Update',
														'processing'	=>	'Processing',
														'wait'			=>	'Procedure processing may take a long time, please wait patiently.',
														'proceed'		=>	'Carry on',
														'thumbnail'		=>	'Thumbnail watermarking (size 500X500 pixels or more)',
														'save_update'	=>	'Save and update old watermarks',
													),
							'water_position'	=>	'Watermark Position',
							'basic_info'		=>	'Basic Information',
							'user_info'			=>	'Member Information',
							'shopping_info'		=>	'Shopping Information',
							'site_info'			=>	'Site Information',
							'index_config'		=>	'Home Setting',
							'language_config'	=>	'Language Setting',
							'watermark_config'	=>	'Watermark Setting',
							'email_config'		=>	'Email Setting',
							'advanced_config'	=>	'Advanced Setting',
							'user_view_notes'	=>	'After turning on, must Login before browsing the site.',
							'user_login_notes'	=>	'When turned on, a recent first visit to the site, the front desk will automatically open the default login box.',
							'ip_notes'			=>	'After turning on, the system shall intercept the source access through internal IP.',
							'browser_notes'		=>	'After turning on, the system shall intercept the source access through Simplified Chinese Browser',
							'cannot_copy_notes'	=>	'After turning on, save shielding right-button and web page as…',
							'browser_language_notes'=>'Automatically recognize the browser language and switch the language version',
							'http_301_notes'	=>	'E.g.: open http://baidu.com, will automatically enter http://www.baidu.com.',
							'thumbnail_notes'	=>	'After turning on, the same watermark shall be shown on the product picture of product list-page.',
							'watermark_pro_notes'=>	'After turning on, watermark is added only to product picture within the entire website.',
							'search_keyword_notes'=>'Search in the form of keywords, keywords through the line to separate fill',
							'auto_canceled_notes'=>	'When you open, it will automatically cancel the order without payment, the expiration time is the number of days after the order time.',
							'edit_lang'			=>	'Language Edit',
							'company_info'		=>	'Company Information',
							'social'			=>	'Social Media',
							'products_set'		=>	'Product Settings',
							'default_review'	=>	'Default Review',
							'number_sort'		=>	'Automatic Sort',
							'user_level'		=>	'Member Level',
							'user_reg_set'		=>	'Registration parameter',
							'customer'			=>	'Customers can comment',
							'audit'				=>	'Display after audit',
							'sitemap'			=>	'Sitemap',
							'need_lang'			=>	'Please select at least one language version',
							'df_lang'			=>	'Cannot directly close the website default language',
							'no_find'			=>	'Did not find the relevant settings',
							'no_themes'			=>	'Sorry, the style file download failed!',
							'englist_manage'	=>	'English version manage',
							'mode'				=>	'Usage mode',
							'user_used'			=>	'Only for members',
							'non_user_used'		=>	'Both members and non-members can use',
						),
	'exchange'		=>	array(
							'currency'			=>	'Currency',
							'name'				=>	'Name of Currency',
							'symbol'			=>	'Symbol',
							'symbol_tops'		=>	'For example: $ / ¥ / €',
							'rate'				=>	'Exchange Rate',
							'exchange_rate'		=>	'Dollar Exchange Rate',
							'now_rate'			=>	'Current Exchange Rate',
							'manage_rate'		=>	'Backstage Exchange Rate',
							'rate_default'		=>	'Default exchange rate',
							'default'			=>	'Default Show',
							'manage_default'	=>	'Backstage Default',
							'default_rate_notes'		=>	'Dollar exchange rate of $ 1 for each currency',
							'now_rate_notes'	=>	'Backstage default currency exchange rate for each currency',
							'used_notes'		=>	'The default is “on”, this currency shall not be shown on website after turning off'
						),
	'oauth'			=>	array(
							'oauth'				=>	'Third-party Login Method',
							'platform_ary'		=>	array(
														'SignIn'	=>	'Login',
														'Pixel'		=>	'Statistics',
														'PageId'	=>	'Messenger'
													),
							'used_notes'		=>	'The default is “off”, customer can log in the member center through this method after turning on'
						),
	'payment'		=>	array(
							'payment'			=>	'Third-party Payment Method',
							'account_info'		=>	'Account Information',
							'basic_info'		=>	'Basic Information',
							'description'		=>	'Description',
							'method'			=>	'Payment Method',
							'limit'				=>	'Price limit',
							'addfee'			=>	'Service Charge',
							'online'			=>	'Online Payment',
							'money_transfer'	=>	'Moneyr Tansfer',
							'get'				=>	'Get',
							'choose'			=>	'Select payment interface',
							'credit_card_payment'=>	'Credit Card Payment',
							'used_notes'		=>	'The default is “on”, this payment method shall not be shown on website after turning off',
							'myorder_notes'		=>	'Please set order of payment and the first class the most superior',
							'error'				=>	array(
														'account'		=>	'Account information error!',
													),
							'tips'				=>	array(
														'paypal'		=>	'Support major mainstream banks and credit card payment, such as: Visa/MasterCard/American Express/Discover...',
														'excheckout'	=>	'Support major mainstream banks and credit card payment, such as: Visa/MasterCard/American Express/Discover...',
														'scoinpay'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'payease'		=>	'Support major mainstream banks and credit card payment, such as: Visa/MasterCard/JCB/American Express...',
														'dhpay'			=>	'Support major mainstream banks and credit card payment, such as: Visa/MasterCard/American Express...',
														'glbpay'		=>	'Support major mainstream banks and credit card payment, such as: Visa/MasterCard/JCB...',
														'payssion'		=>	' Powered by Payssion, Support overseas electronic wallet, online bank transfer, offline bank transfers, prepaid cards payments, such as: Qiwi/Webmoney/Yandex/CashU/Onecard...',
														'fashionpay'	=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'paydollar'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/American Express/JCB/Diners...',
														'globebill'		=>	'Support overseas electronic wallet, credit card payment, online bank transfer, such as: Qiwi/Webmoney/Yandex/Ideal/Boleto/Visa/ MasterCard...',
														'fucheckout'	=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'leaderspay'	=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'alipay'		=>	'Alipay payment',
														'wechat'		=>	'Wechat payment',
														'stripe'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'leaderspay'	=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'ipaylinks'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'bringall'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
														'pingpong'		=>	'Support Visa, Mastercard credit card and PayPal, iDEAL, giropay, Qiwi, sofort, CashU and other global electronic payment methods',
														'stripealipay'	=>	'AliPay',
														'stripewechatpay'	=>	'WechatPay',
														'oceanpayment'		=>	'Support major mainstream banks and credit card payment, such as: CUP/Visa/MasterCard/JCB...',
													)
						),
	'country'		=>	array(
							'country'			=>	'Area',
							'uesd_bat'			=>	'Batch Open',
							'close_bat'			=>	'Batch Close',
							'acronym'			=>	'Abbreviation',
							'continent'			=>	'Continent',
							'code'				=>	'International Dial Code',
							'flag'				=>	'Flag',
							'hot'				=>	'Favorite Countries',
							'state'				=>	'Province',
							'default'			=>	'Default',
							'default_country'	=>	'Default Area',
							'quick_search'		=>	'Quick Search',
							'edit_state'		=>	'Modify State',
							'used_notes'		=>	'The default is “on”, this option of area shall not be shown on website after turning off.',
							'hot_notes'			=>	'After turning on, this area shall be shown on the list of favorite area.',
							'state_notes'		=>	'After turning on, adding subordinate province to this area is allowed.',
							'tips'				=>	'Please don\'t change provinces at will. Changing provinces at will may result in Paypal speedy payment without shipping charges.',
						),
	'photo'				=> array(
							'category'			=>	'File',//'文件夹',
							'pic_list'			=>	'Picture List',
							'local'				=>	'Local Upload',
							'all'				=>	'All Pictures',
							'children'			=>	'Class of Category',
							'systempic'			=>	'System Image',
							'editorpic'			=>	'Editor Image',
							'upfile'			=>	'Upload Pictures',
							'choicepic'			=>	'Upload pictures from picture bank',
							'move_bat'			=>	'Batch Move',
							'clear_tmp'			=>	'Wipe Temporary Internet Files',
							'SystemType'		=>	array('products'=>'System Product Image', 'editor'=>'Editor Chart', 'other'=>'Other System Charts'),//键名与$c['manage']['photo_type']的配置对应
							'tmp_notes'			=>	'Wipe cache files uploaded before, periodic wipe is preferred',
							'max_count'			=>	'More than the number of pictures allowed to be uploaded',
							'no_photo'			=>	'No pictures added',
						),
	'chat'				=>	array(
							'module_name'		=>	'Floating Service',
							'color'				=>	'Color',
							'name'				=>	'Name',
							'type'				=>	'Service Type',
							'account'			=>	'Account',
							'pop'				=>	'Click Popup',
							'whatsapp_tips'		=>	'(International Code must be added, ex: 8613800138000)'
						),
	'authorization'		=>	array(
							'authorization'		=>	'Authorization',
							'url'				=>	' Url',
							'access_code'		=>	'AccessKey ID',
							'appkey'			=>	'AccessKey Secret',
							'api_disable'		=>	'Api is not enabled!',
							'add_store'			=>	'Add Store',
							'edit_store'		=>	'Modify Store',
							/************* Aliexpress (start) *************/
							'shop_name'			=>	' Shop Name',
							'shop_name_tips'	=>	'Please customize a shop name, to facilitate your future into the multi-store management!',
							'token_valid'		=>	'Token Valid Period',
							'authorization_time'=>	'Authorization Time',
							'to'				=>	' to ',
							'refresh'			=>	'Reauthorization',
							'add_authorization_tips'	=>	'No optional, please authorization first!',
							/************* Aliexpress (end) *************/
							/************* Amazon (start) *************/
							'seller_id'			=>	'Merchant ID / Seller ID',
							'marketplace'		=>	'Marketplace',
							'account_site'		=>	'Account Site',
							'merchant_id'		=>	'Merchant ID',
							'aws_access_key_id'	=>	'AWS Access Key ID',
							'secret_key'		=>	'Secret Key',
							'access_key_tips'	=>	'AWS Access Key ID',
							'secret_key_tips'	=>	'Secret Key',
							'go'				=>	'Go to Account Site',
							'US'				=>	'United States',
							'CA'				=>	'Canada',
							'DE'				=>	'Germany',
							'FR'				=>	'France',
							'UK'				=>	'United Kingdom',
							'IT'				=>	'Italy',
							'ES'				=>	'Spain',
							'IN'				=>	'India',
							'JP'				=>	'Japan',
							'CN'				=>	'China',
							'MX'				=>	'Mexico'
							/************* Amazon (end) *************/
						),
	'manage_logs'		=> array(
							'account'			=>	'My Account',
							'operation'			=>	'Operation',
							'orders'			=>	'Order',
							'plugins'			=>	'App',
							'products'			=>	'Products',
							'sales'				=>	'Sales Promotion',
							'set'				=>	'Set',
							'user'				=>	'Member'
						),
	'seo'			=> array(
							'ud_map'			=>	'Update map',
							'view'				=>	'View inclusion',
							'website'			=>	'Domain',
							'last_ud'			=>	'Last updated',
							'ud_smap'			=>	'Update sitemap',
						),
	'email'			=> array(
							'ver'				=>	'SMTP authentication',
							'tips'				=>	'The verification process may take a long time, please be patient',
							'error'				=>	'Please check if the SMTP account information is filled in correctly. The wrong account information will cause the website to not send the mail.',
							'vering'			=>	'In verification',
							'ver_succ'			=>	'Successful verification',
							'ver_fail'			=>	'verification failed',
						),
	'domain_binding'=> array(
							'new'				=>	'Bind a new domain name',
							'relieve'			=>	'Delete domain name',
							'website'			=>	'Domain name',
							'status'			=>	'Status',
							'operation'			=>	'Operating',
							'to_verify'			=>	'Domain verification',
							'no_verify'			=>	'Unverified',
							'how_verify'		=>	'How to analyze',
							'connected'			=>	'Connected',
							'write'				=>	'Fill in the domain name',
							'resolve_domain'	=>	'Resolve domain name',
							'domain_to_ip'		=>	'<em>%domain%</em> resolves to <em>%ip%</em>, after the resolution is complete, click the "Verify" button',
							'tips'				=>	'Please log in to your domain name service provider, modify the domain name resolution record, and follow the table below to set the parameters.',
							'record_type'		=>	'Record type',
							'host_record'		=>	'Host record',
							'record_value'		=>	'Current record value',
							'completed'			=>	'Verification',
							'in_verify'			=>	'Domain name resolution verification',
							'verify_tips'		=>	'The domain name resolution takes effect in about 10 minutes. If the verification fails, please re-verify after a period of time.',
							'success'			=>	'Successful domain name verification',
							'fail'				=>	'Domain verification failed',
							'domain_tips'		=>	'Fill in the domain without www, for example:abc.com',
						),
	'explain'			=> array(
							'erp'				=>	'This data is used for the docking of ERP',
							'edit_mail'			=>	'Modify the message content',
							'set_smtp'			=>	'Custom outbox',
							'send_from'			=>	'Customize the outbox of all mail for your website',
							'print'				=>	'This company information is used for order printing',
							'account'			=>	'Please fill in the account information in the detailed introduction.',
							'range'				=>	'Limit the use of this payment method to the order price range',
							'rate'				=>	'This exchange rate is not a real-time exchange rate. Please adjust it according to the exchange rate of the inquiry.',
							'insurance'			=>	'Freight insurance',
							'enable'			=>	'activated',
							'unenable'			=>	'Not Enabled',
							'docking'			=>	'Direct docking for DHL shipping',
							'country'			=>	'Enabled: Enabled area can be used for member registration selection or express area selection<br />Top area: When checked, it can be used as a priority when filling out the order address for customers.',
							'group'				=>	'Super administrator: own all permissions in the background of the website<br>Ordinary administrator: After selecting, you can click the permission, check the open permission',
						),
)
?>