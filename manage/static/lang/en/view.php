<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
return array(
	'index'		=>	array(
						'title'			=>	'Interface',
						'submod'		=>	'Modify the ad image, title, layout within the style',
						'switch'		=>	'Switch style',
						'subswitch'		=>	'Enter the style template gallery with lots of beautiful style templates',
					),
	'visual'	=>	array(
						'home'			=>	'Home page',
						'pic'			=>	'Image',
						'gallery'		=>	'Use gallery',
						'gallery_image'	=>	'Gallery image',
						'font'			=>	'Font',
						'font_size'		=>	'Font size',
						'cur_lang'		=>	'Current language',
						'sort_prev'		=>	'In',
						'sort_next'		=>	'place',
						'page'			=>	array(
											'page'			=>	'Page',
											'home'			=>	'Home',
											'products_list'	=>	'Product list',
											),
						'contents_tips'	=>	'After filling in the content, the mouse can be dragged and adjusted to display the position of the advertisement map.',
						'manage'		=>	'Enter the background',
						'search_tips'	=>	'Search prompt',
					),
	'template'	=>	array(
						'top'			=>	'Choose the template you like',
						'sub'			=>	'On any template, you can edit your own store on this basis.',
						'view'			=>	'View example',
						'switch'		=>	'Use',
						'loading'		=>	'Pull down to load more',
						'style'			=>	'Selection style',
						'all'			=>	'All',
					),
	'explain'	=>	array(
						'order'			=>	'Move the cursor to the green dot in front of the message, click and drag to achieve navigation sorting',
						'placeholder'	=>	'Optional drop down page or custom fill name link',
					),
)
?>