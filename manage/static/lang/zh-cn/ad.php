<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
姓名：sheldon
日期: 2014-07-25
备注:分工的语言包开发，上线后该文件统一整合 
*/
return array(
	'ad'	=>	array(
						'ad_add'		=>	'广告添加',
						'pagename'		=>	'页面名称',
						'version'		=>	'网站类型',
						'pagetype'		=>	'页面类型',
						'version_n'		=>	array('电脑版', '手机版'),
						'pagetype_n'	=>	array('首页', '列表页'),
						'position'		=>	'广告位置',
						'type'			=>	'广告类型',
						'showtype'		=>	'显示方式',
						'show'			=>	array('默认', '渐显', '上滚动', '左滚动'),
						'type_ary'		=>	array('图片', '编辑器'),
						'pic_qty'		=>	'图片数量',
						'width'			=>	'广告宽度',
						'height'		=>	'广告高度',
						'contents'		=>	'广告内容',
						'photo'			=>	'广告图片',
						'flash'			=>	'上传动画',
						'name'			=>	'广告名称',
						'brief'			=>	'简介',
						'url'			=>	'链接地址',
						'alt'			=>	'Alt',
						'cur_file'		=>	'当前文件',
						'preview'		=>	'预览',
					),
	'banner'	=>	array(
						'creat'			=>	'在线设计',
						'go_back'		=>	'返回主页',
						'step_1'		=>	'选择模板',
						'step_2'		=>	'设置内容',
						'step_3'		=>	'完成',
						'set_contents'	=>	'设置内容',
						'pic'			=>	'图片',
						'txt'			=>	'内容',
						'btn'			=>	'按钮',
						'gallery'		=>	'图库',
						'custom'		=>	'自定义图片',
						'all'			=>	'全部',
						'more_style'	=>	'更多风格',
						'upload_tips'	=>	'支持2M以下png/jpg文件',
						'edit_title'	=>	'大标题',
						'font'			=>	'字体',
						'font_0'		=>	'Roboto',
						'font_1'		=>	'Microsoft YaHei',
						'font_size'		=>	'字号',
						'edit_brief'	=>	'小标题',
						'edit_link'		=>	'链接',
						'shape'			=>	'形状',
						'color'			=>	'颜色',
						'bg_trans'		=>	'背景透明',
						'name'			=>	'名字',
						'no_image'		=>	'未能检测图片，请重新上传',
					),	
);