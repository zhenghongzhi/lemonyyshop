<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
姓名：sheldon
日期: 2014-07-25
备注:分工的语言包开发，上线后该文件统一整合 
*/
return array(
	'pc'			=>	'PC端',
	'mobile'		=>	'移动端',
	'url'			=>	'链接',
	'show'			=>	'演示',
	'nav'			=>	array(
							'column'		=>	'栏目',
							'add_item'		=>	'增加选项',
							'custom'		=>	'自定义',
							'url'			=>	'链接地址',
							'down'			=>	'下拉',
							'down_width'	=>	'下拉框宽度',
							'down_width_ary'=>	array('小', '中', '大'),
							'target'		=>	'新窗口',
							'myorder'		=>	'导航排序'
						),
	'products_list'	=>	array(
							'left_right'	=>	'左右分布',
							'screen'		=>	'产品筛选',
							'box'			=>	'栏目模块',
							'leftbar_box'	=>	'左侧栏目模块',
							'leftbar'		=>	'左侧',
							'rightbar'		=>	'右侧',
							'list_rank'		=>	'产品排列方式',
							'list_count'	=>	'产品排列数量',
							'order'			=>	array('row_1'=>'一行一个', 'row_2'=>'一行两个', 'row_3'=>'一行三个', 'row_4'=>'一行四个', 'row_5'=>'一行五个', 'column'=>'瀑布流'),
							'column_notes'	=>	'开启后，产品列表页会以左右栏目布局显示',
							'effects'		=>	'产品框特效',
							'effects_order'	=>	'特效方式',
							'effects_ary'	=>	array('无特效', '特效一', '特效二', '特效三', '特效四', '特效五', '特效六')
						),
	'left_bar'		=>	array(
							'Category'		=>	'产品分类',
							'Hot'			=>	'热门产品',
							'Special'		=>	'特价产品',
							'Banner'		=>	'广告图',
							'Newsletter'	=>	'邮件订阅',
							'Recommended'	=>	'推荐产品',
							'TheSay'		=>	'推荐评论',
							'Popular'		=>	'热门搜索'
						),
	'products_detail'=>	array(
							'share'			=>	'分享排序',
							'help'			=>	'客户帮助'
						),
	'style'			=>	array(
							'nav_style'					=>	'导航样式',
							'FontColor'					=>	'网站主色调',
							'PriceColor'				=>	'价格颜色',
							'AddtoCartBgColor'			=>	'加入购物车背景',
							'BuyNowBgColor'				=>	'立即购买背景',
							'ReviewBgColor'				=>	'评论按钮背景',
							'DiscountBgColor'			=>	'折扣显示背景',
							'NavBgColor'				=>	'背景',
							'NavHoverBgColor'			=>	'鼠标效果',
							'NavBorderColor1'			=>	'边框颜色(1)',
							'NavBorderColor2'			=>	'边框颜色(2)',
							'CategoryBgColor'			=>	'分类标题背景',
							'ProListBgColor'			=>	'产品列表特效背景色',
							'ProListHoverBgColor'		=>	'产品列表特效鼠标效果',
							'GoodBorderColor'			=>	'产品详细色块边框颜色',
							'GoodBorderHoverColor'		=>	'产品详细色块边框鼠标颜色',
							'IndexMenuBg_0'				=>	'菜单背景风格 1',
							'IndexMenuBg_1'				=>	'菜单背景风格 2',
							'IndexMenuBg_2'				=>	'菜单背景风格 3',
						),
);