<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
return array(
	'index'		=>	array(
						'title'			=>	'界面',
						'submod'		=>	'对风格内的广告图，标题，布局进行修改',
						'switch'		=>	'切换风格',
						'subswitch'		=>	'进入风格模板库，里面有大量精美风格模板',
					),
	'visual'	=>	array(
						'home'			=>	'返回后台',
						'pic'			=>	'图',
						'gallery'		=>	'使用图库',
						'gallery_image'	=>	'图库图片',
						'font'			=>	'字体',
						'font_size'		=>	'字号',
						'cur_lang'		=>	'当前语言',
						'sort_prev'		=>	'第',
						'sort_next'		=>	'位',
						'page'			=>	array(
											'page'			=>	'页面',
											'home'			=>	'首页',
											'products_list'	=>	'产品列表页',
											),
						'contents_tips'	=>	'填写好内容后，鼠标可拖动调整显示在广告图的位置',
						'manage'		=>	'进入后台',
						'search_tips'	=>	'搜索提示语',
					),
	'template'	=>	array(
						'top'			=>	'选择您喜欢的模板',
						'sub'			=>	'在任意模板上，您可以在此基础上编辑自己的店铺',
						'view'			=>	'查看示例',
						'switch'		=>	'使用',
						'loading'		=>	'下拉加载更多',
						'style'			=>	'选择风格',
						'all'			=>	'全部',
					),
	'explain'	=>	array(
						'order'			=>	'光标移动到信息前面的绿色小点，点击拖动可以实现导航排序',
						'placeholder'	=>	'可选择下拉页面或自定义填写名称链接',
					),
)
?>