<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

return array(
	'enter'			=>	'进入',
	'global'		=>	array(
							'category'		=>	array(													
													'sales'			=>	'促销',
													'marketing'		=>	'营销',
													'google'		=>	'Google',
													'facebook'		=>	'Facebook',
													'login'			=>	'快捷登录',
													'delivery'		=>	'物流',
													'products_rela'	=>	'产品',
													'others'		=>	'其他',
													'statistics'	=>	'流量统计',
												),
							'items'			=>	array(
													'seckill'		=>	'限时秒杀',
													'tuan'			=>	'团购管理',
													'package'		=>	'组合购买',
													'promotion'		=>	'组合促销',
													'holiday'		=>	'节日促销',
													'distribution'	=>	'分销',
													'gallery'		=>	'买家秀',
													'facebook_store'=>	'Facebook专页',
													'facebook_ads_extension'=>'Facebook官方商务插件',
													'googlefeed'	=>	'Google Feed',
													'review'		=>	'假发行业评论库',
													'import_delivery'=>	'导入快递',
													'overseas'		=>	'海外仓',
													'freight'		=>	'运费估算',
													'facebook_pixel'=>	'Facebook 像素',
													'aliexpress'	=>	'同步速卖通',
													'amazon'		=>	'同步亚马逊',
													'wish'			=>	'同步Wish',
													'facebook_login'=>	'Facebook登录',
													'google_login'	=>	'Google登录',
													'paypal_login'	=>	'Paypal登录',
													'vk_login'		=>	'VK登录',
													'twitter_login'	=>	'Twitter登录',
													'instagram_login'=>	'Instagram登录',
													'blog'			=>	'博客',
													'cdn'			=>	'CDN',
													'business'		=>	'供应商',
													'product_inbox'	=>	'产品咨询',
													'block_access'	=>	'屏蔽访问',
													'pdf'			=>	'PDF格式',
													'advanced_setup'=>	'高级设置',
													'upload'		=>	'批量上传',
													'wholesale'		=>	'批发',
													'platform'		=>	'平台导流',
													'screening'		=>	'属性筛选',
													'wish'			=>	'同步Wish',
													'facebook_messenger'=>'FB Messenger',
													'intelligent_translation'=>'智能翻译',
													'massive_email'	=>	'邮件群发',
													'swap_chain'	=>	'交换外链',
													'google_verification'=>'Google验证文件',
													'google_pixel'	=>	'GA 追踪代码',
													'batch_edit'	=>	'新站做旧',
													'paypal_marketing_solution'=>'Paypal追踪代码',
													'paypal_dispute'=>	'Paypal纠纷',
													'shopify'		=>	'同步Shopify',
													'dhl_account_open'=>'DHL在线开户',
													'dhl_account_info'=>'DHL账号信息',
													'asiafly'		=>	'亚翔供应链',
													'custom_comments'	=>	'自定义评论',
												),
							'parameter'		=>	array(
													'appId'			=>	'应用编号',
													'TrackingID'	=>	'跟踪ID',
												),
                            'other'         =>  array(
													'facebook_store'=>	'Faceboook店铺',
												),
							'install'		=>	'安装',
							'installed'		=>	'已安装',
							'enter'			=>	'进入',
							'import'		=>	'导入',
							'begin_import'	=>	'开始导入',
							'app_category'	=>	'应用分类',
							'next_title'	=>	'敬请期待',
							'next_txt'		=>	'新功能在路上了',
							'search_tips'	=>	'请输入您想了解的应用名称',
							'store_select'	=>	'去应用商店选择应用程序',
							'store_msg'		=>	'应用程序可以添加新功能，提升销售，并优化您的业务运营方式',
							'go_store'		=>	'进入应用商店',
						),
	'briefdescript'	=>	array(
							'seckill'		=>	'在某一预定的营销时间里，降低活动商品价格，买家只要在此时间购买，便可以以优恵价格购买',
							'tuan'			=>	'团购即团体购物，当购买人数达到商家原定的人数时，以最优价格成交的一种购物方式',
							'package'		=>	'搭配多种互补商品，进行组合购买，方便客户配套一次性购买，不需要再另外搜索产品',
							'promotion'		=>	'搭配多种商品，进行价格折扣促销，促销价格具有吸引力，可以增加组合商品的购买量',
							'holiday'		=>	'针对“情人节、圣诞节”等节庆假期，通过选择合适的节日专题页来开展促销活动刺激消费',
							'distribution'	=>	'邀请会员分销商，并且在社交平台上发布网站url吸引好友购物，分销商可以获取相应的佣金',
							'gallery'		=>	'商家上传买家真实图片，可以更直观的给其他买家们做参考。以此来推广网站的商品，招揽人气',
							'facebook_store'=>	'授权Facebook专页，可以在facebook上显示专页里面的商品，方便facebook用户浏览购买',
							'facebook_ads_extension'=>'一键轻松开启Facebook店铺，Facebook专页，Facebook Messenger，帮助网站从Facebook引入更多访客',
							'googlefeed'	=>	'主要运用于google shopping等谷歌推广方式，可把产品提交到google，省下整理产品的时间',
							'review'		=>	'评论内容采集平台的4星以上的不含关键字评论，系统根据排序随机导入评论。<br />（此评论库只适用于假发行业和假发类产品）',
							'import_delivery'=>	'批量提交发货状态订单的订单号，运单号，时间，和备注等内容，方便操作订单批量发货',
							'overseas'		=>	'可实现海外仓库发货，分别计算各仓库运费，客户购买时也可灵活选择发货地',
							'freight'		=>	'客户在产品详细页预选发货的国家，从而预先估算产品的运费，不需要等到达下单页面了解',
							'facebook_pixel'=>	'应用于网站的一段追踪代码，安装代码后能够在Facebook平台上直接观察分析各项数据来实现精准投放',
							'aliexpress'	=>	'通过速卖通的接口和商城后台对接，实现产品数据同步，更方便的管理产品',
							'amazon'		=>	'通过亚马逊的接口和商城后台对接，实现产品数据同步，更方便的管理产品',
							'facebook_login'=>	'用户使用自己的facebook帐号进行授权登录，无需通过注册流程，就能快速成为会员<br /><br /><span class="fc_red">提示：如登录按钮不显示，是因为Facebook插件无法加载，请检查浏览器是否正常打开Facebook主页</span>',
							'google_login'	=>	'用户使用自己的google帐号进行授权登录，无需通过注册流程，就能快速成为会员',
                            'paypal_login'=>	'用户使用自己的paypal帐号进行授权登录，无需通过注册流程，就能快速成为会员',
							'vk_login'		=>	'用户使用自己的vk帐号进行授权登录，无需通过注册流程，就能快速成为会员',
							'twitter_login'	=>	'用户使用自己的twitter帐号进行授权登录，无需通过注册流程，就能快速成为会员',
							'instagram_login'=>	'用户使用自己的instagram帐号进行授权登录，无需通过注册流程，就能快速成为会员',
							'blog'			=>	'独立的博客页面，支持无限添加博客类目及内容、博客评论等功能，方便发布日常消息，有利于网站的优化和推广',
							'cdn'			=>	'实时地根据网络流量和各节点的连接、用户的距离和响应时间等综合信息将用户的请求重新导向离用户最近的服务节点上',
							'business'		=>	'完善产品供应商资料，上传资质证书、协议等文件，方便对产品资料的整合统计',
							'product_inbox'	=>	'产品详细页上显示咨询按钮，方便购买的客户快速找到询问的位置，提问和回复的记录都会保存在客户的会员账号',
							'block_access'	=>	'分别针对国内IP或中文浏览器进行访问的屏蔽，有效防止同行恶意访问',
							'pdf'			=>	'产品详细页可针对某一个产品，进行pdf格式的生成并下载，有利于产品资料的传播',
							'advanced_setup'=>	'对会员、产品等模块进行多个细节功能调控，按实际商家需要，灵活开关',
							'upload'		=>	'进行多个产品的批量上传，以表格的方式提交产品数据，减少多个产品重复编辑的步骤',
							'wholesale'		=>	'分别设置产品的批发价格，使客户在购买一定数量的产品能享受优惠的单价，有助于客户对产品的批量采购。（开启后，除了可以设置批发价，还可以设置产品的起订量）',
							'platform'		=>	'支持亚马逊，速卖通，wish，ebay，阿里巴巴等平台客户引流，直接可在产品页面点击跳转，方便拥有多平台的客户进行站外引流',
							'screening'		=>	'买家可以根据产品的属性进行筛选，更精确快捷找到想要的产品',
							'wish'			=>	'通过wish平台的接口和商城后台对接，实现产品数据同步，更方便的管理产品',
							'facebook_messenger'=>	'对接facebook messenger聊天工具，通过facebook轻松发起咨询',
							'intelligent_translation'=>	'采用谷歌的翻译插件，对产品的标题、详细资料等进行直接的翻译，减少多语言版本自行翻译的时间',
							'massive_email'	=>	'通过mailchimp群发邮件给会员，订阅的用户,告诉他们网站发布了新品或者促销优惠，吸引更多人过来购买',
							'swap_chain'	=>	'通过互换链接，增加外链，从而提升关键词排名及网站质量',
							'google_verification'=>	'谷歌站长用来验证网站的所有权，把文件上传至网站，谷歌验证成功后，你对网站收录情况进行查看和优化',
							'google_pixel'	=>	'GA追踪代码，应用于网站的一段追踪代码，安装代码后能够在Google平台上直接观察分析各项数据来实现精准投放',
							'batch_edit'	=>	'卖家可自定义产品的销量，产品的收藏数量，默认评论平均分，默认评论人数，让新站看起来像运营了很久，很多买家光顾，给新买家增加对网站的信心',
							'paypal_marketing_solution'=>'Paypal追踪代码',
							'paypal_dispute'=>	'Paypal纠纷',
							'shopify'		=>	'通过shopify平台的接口和商城后台对接，实现产品数据同步，更方便的管理产品',
							'dhl_account_open'	=>	'使用线上注册DHL电子商务账号功能',
							'dhl_account_info'	=>	'客户可通过DHL的API对接实现DHL物流的获取标签、修改订单、重新打印标签、追踪信息查询功能。',
							'asiafly'		=>	'亚翔供应链，可以根据提供的时效，去询价显示各物流方式的收费，以及打印标签、追踪信息等功能。',
							'custom_comments'	=>	'卖家可自定义产品的销量，产品的收藏数量，默认评论平均分，默认评论人数，让新站看起来像运营了很久，很多买家光顾，给新买家增加对网站的信心',
						),
	'success_tips'	=>	array(
							'seckill'		=>	'限时秒杀可以在促销模块下进行管理或者进入我的应用，找到限时秒杀点击“进入”进入管理界面，路径是促销>限时秒杀',
							'tuan'			=>	'团购管理可以在促销模块下进行管理或者进入我的应用，找到团购管理点击“进入”进入管理界面，路径是促销>团购管理',
							'package'		=>	'组合购买可以在促销模块下进行管理或者进入我的应用，找到组合购买点击“进入”进入管理界面，路径是促销>组合购买',
							'promotion'		=>	'组合促销可以在促销模块下进行管理或者进入我的应用，找到组合促销点击“进入”进入管理界面，路径是促销>组合促销',
							'holiday'		=>	'节日促销可以在促销模块下进行管理或者进入我的应用，找到节日促销点击“进入”进入管理界面，路径是促销>节日促销',
							'distribution'	=>	'分销可以在运营模块下进行管理或者进入我的应用，找到平台分销点击“进入”进入管理界面，路径是运营>分销',
							'gallery'		=>	'买家秀可以在运营模块下进行管理或者进入我的应用，找到买家秀点击“进入”进入管理界面，路径是运营>买家秀',
							'facebook_store'=>	'Facebook专页可以在运营模块下进行管理或者进入我的应用，找到Facebook专页点击“进入”进入管理界面，路径是运营>Facebook专页',
							'facebook_ads_extension'=>'轻松安装像素并在Facebook上创建产品目录以销售更多产品。 使用像素构建合适的受众群体并衡量广告支出的回报率。 使用您的目录一次性宣传您的所有产品，而不必创建单独的广告。',
							'googlefeed'	=>	'Google Shopping Feed可以在运营模块下进行管理或者进入我的应用，找到Google Shopping Feed点击“进入”进入管理界面，路径是运营>Google Feed',
							'review'		=>	'假发行业评论库可进入我的应用，找到假发行业评论库点击“导入”进行操作，路径是应用>我的应用',
							'import_delivery'=>	'导入快递可以在订单模块下进行管理或者进入我的应用，找到导入快递点击“进入”进入管理界面，路径是订单>导入快递',
							'overseas'		=>	'海外仓可以在设置模块的运费管理下进行管理，路径是设置>运费管理，或进入我的应用，找到海外仓点击按钮进行关闭操作',
							'freight'		=>	'运费估算可进入我的应用，找到运费估算点击按钮进行关闭操作，路径是应用>我的应用',
							'facebook_pixel'=>	'Facebook像素可进入我的应用，找到Facebook像素点击“进入”进入管理界面，路径是应用>我的应用',
							'aliexpress'	=>	'同步速卖通可以在产品模块下进行管理或者进入我的应用，找到同步速卖通点击“进入”进入管理界面，路径是产品>数据同步',
							'amazon'		=>	'同步亚马逊可以在产品模块下进行管理或者进入我的应用，找到同步亚马逊点击“进入”进入管理界面，路径是产品>数据同步',
							'facebook_login'=>	'Facebook登录可进入我的应用，找到Facebook登录点击“进入”进入管理界面，路径是应用>我的应用',
							'google_login'	=>	'Google登录可进入我的应用，找到Google登录点击“进入”进入管理界面，路径是应用>我的应用',
							'paypal_login'	=>	'Paypal登录可进入我的应用，找到Paypal登录点击“进入”进入管理界面，路径是应用>我的应用',
							'vk_login'		=>	'VK登录可进入我的应用，找到VK登录点击“进入”进入管理界面，路径是应用>我的应用',
							'twitter_login'	=>	'Twitter登录可进入我的应用，找到Twitter登录点击“进入”进入管理界面，路径是应用>我的应用',
							'instagram_login'=>	'Instagram登录可进入我的应用，找到Instagram登录点击“进入”进入管理界面，路径是应用>我的应用',
							'blog'			=>	'博客可以在运营模块下进行管理或者进入我的应用，找到博客点击“进入”进入管理界面，路径是运营>博客管理',
							'cdn'			=>	'CDN可进入我的应用，找到CDN点击按钮进行关闭操作，路径是应用>我的应用',
							'business'		=>	'供应商可以在产品模块下进行管理或者进入我的应用，找到供应商点击“进入”进入管理界面，路径是产品>供应商',
							'product_inbox'	=>	'产品咨询可进入我的应用，找到产品咨询点击按钮进行关闭操作，路径是应用>我的应用',
							'block_access'	=>	'屏蔽访问可进入我的应用，找到屏蔽访问点击“进入”进入管理界面，路径是应用>我的应用',
							'pdf'			=>	'PDF格式可进入我的应用，找到产品咨询点击按钮进行关闭操作，路径是应用>我的应用',
							'advanced_setup'=>	'高级设置可进入我的应用，找到高级设置点击“进入”进入管理界面，路径是应用>我的应用',
							'upload'		=>	'批量上传可以在产品模块下进行管理，路径是产品>产品管理，或可进入我的应用，找到批量上传按钮进行关闭操作',
							'wholesale'		=>	'批发可以在产品模块下进行管理，路径是产品>产品管理，对单个产品进行批发价编辑，或可进入我的应用，找到批发按钮进行关闭操作',
							'platform'		=>	'平台导流可以在产品模块下进行管理，路径是产品>产品管理，对单个产品进行平台导流编辑，或可进入我的应用，找到平台导流点击按钮进行关闭操作',
							'screening'		=>	'属性筛选可进入我的应用，找到属性筛选点击按钮进行关闭操作，路径是应用>我的应用',
							'wish'			=>	'同步wish可进入我的应用，找到同步wish点击按钮进行关闭操作，路径是应用>我的应用',
							'facebook_messenger'=>	'FB Messenger可进入我的应用，找到FB Messenger点击按钮进行关闭操作，路径是应用>我的应用',
							'intelligent_translation'=>	'智能翻译可进入我的应用，找到智能翻译点击按钮进行关闭操作，路径是应用>我的应用',
							'massive_email'	=>	'如何使用群发邮件功能？<br/>1.注册一个mailchimp账号 <a target="_blank" href="http://mailchimp.com">http://mailchimp.com</a> ，之后点击授权登录<br/>2.进入左边导航，选择“营销”-》“邮件”，也可以点击“<a href="/manage/?m=operation&a=email">邮件群发</a>”进入',
							'swap_chain'	=>	'1.接受平台为你安排出现在网站的友情链接,<br/>2.每位用户将获得5-6个站点的外链,并被安排5-6个站点外链<br/>3.可以查询到自己的外链站点。链接可以接受更换一次<br/>4.无权擅自删除网站上的友情链接,可以退出链接计划,但你的站点外链将被取消<br/>5.所有输出的链接,均为Ueeshop平台的用户,平台将会随机匹配',
							'google_verification'	=>	'Google验证文件可进入我的应用，找Google验证文件点击按钮进行关闭操作，路径是应用>我的应用',
							'google_pixel'		=>	'GA追踪代码可进入我的应用，找到GA追踪代码点击“进入”进入管理界面，路径是应用>我的应用',
							'batch_edit'		=>	'新站做旧可进入我的应用，找到新站做旧点击“进入”进入管理界面，路径是应用>我的应用',
							'shopify'			=>	'同步shopify可进入我的应用，找到同步shopify点击按钮进行关闭操作，路径是应用>我的应用',
							'dhl_account_info'	=>	'通过DHL在线开户(之前已经开户无需再次开户)成功后，从DHL官方API中获取对应的ClientId 和 Password，填写后即可使用DHL物流查询功能。',
							'custom_comments'	=>	'自定义评论可进入我的应用，找到自定义评论点击“进入”进入管理界面，路径是应用>我的应用',
						),
	'authorization'	=>	array(
							'title'			=>	'应用授权',
							'store_info'	=>	'需要获取您的店铺信息',
							'tips'			=>	'授权后，表示您已同意授权及免责条款，并允许<span></span>应用获取您的店铺信息，从而为您提供服务。',
							'confirm'		=>	'确认授权',
							'follows'		=>	'安装成功，您可按照以下提示完成配置：',
							'got_it'		=>	'知道了',
							'help'			=>	'帮助',
							'plugins_tips'	=>	array(
													'swap_chain'	=>	'授权后，表示您已同意授权及免责条款。并同意和其他店铺交换链接，交换的链接显示在各自的网站底部',
												),
						),
	'app'			=>	array(
							'proary'		=>	'关联产品',
						),
	'distribution'	=>	array(
							'title'				=>	'分销',
							'config'			=>	'基本配置',
							'commission'		=>	'佣金记录',
							'withdraw'			=>	'提现记录',
							'percentage1'		=>	'一级返佣',
							'percentage2'		=>	'二级返佣',
							'percentage3'		=>	'三级返佣',
							'percentage1_tips'	=>	'一级分销佣金=订单总额*一级返佣百分比',
							'percentage2_tips'	=>	'二级分销佣金=订单总额*二级返佣百分比',
							'percentage3_tips'	=>	'三级级分销佣金=订单总额*三级返佣百分比',
							'distor'			=>	'分销商',
							'comm'				=>	'佣金',
							'level'				=>	'级别',
							'card_num'			=>	'账户',
							'card_acc'			=>	'名称',
							'amount'			=>	'金额',
							'status'			=>	'状态',
							'status_0'			=>	'待处理',
							'status_1'			=>	'已处理',
							'index_description'	=>	'首页介绍',
							'withdraw_description'=>'提现介绍',
							'all_distor'		=>	'所有分销商',
							'total_comm'		=>	'总佣金',
							'cash_balance'		=>	'提现余额',
							'mta'				=>	'统计',
							'total_order'		=>	'总订单',
							'distor_order'		=>	'分销订单',
							'total_distor'		=>	'分销商',
							'distor_percent'	=>	'分销率',
							'percentage_percent'=>	'佣金百分比',
							'order_count_to'	=>	'笔',
							'order_ratio'		=>	'订单占比',
							'count_ary'			=>	array('分销订单', '总订单', '分销率')
						),
	'review'		=>	array(
							'title'			=>	'评论库',
							'category'		=>	'分类',
							'category_tips'	=>	'评论库的分类',
							'category_0'	=>	'假发',
							'progress'		=>	'进度',
						),
	'facebook_store'=>	array(
							'title'			=>	'Facebook专页',
							'appid'			=>	'AppId',
							'page'			=>	'主页功能选项网址',
							'add_page'		=>	'添加主页功能选项',
							'authorized'	=>	'点击授权'
						),
	'googlefeed'	=>	array(
							'update'		=>	'更新',
							'start'			=>	'开始更新',
							'progress'		=>	'更新进度',
							'updating'		=>	'正在更新第%num%页......',
							'update_suc'	=>	'更新成功！',
							'add_info'		=>	'补充信息',
							'gtin'			=>	'全球贸易项目代码 (GTIN)',
							'brand'			=>	'品牌',
							'brand_tips'	=>	'不填品牌默认调取一级产品分类',
							'label'			=>	'自定义标签',
							'label_tips'	=>	'最多添加5个 用 "/" 隔开 例子： 标签1/标签2/标签3/标签4/标签5',
							'update_feed'	=>	'更新Google Feed',							
							'tips'			=>	'请将该链接复制到谷歌:',
							'shipping_price'=>	'运费-价格',
							'sp_coutry'		=>	'澳大利亚、捷克、法国、德国、以色列、意大利、荷兰、韩国、西班牙、瑞士、英国和美国',
							'tax_rate'		=>	'税费-税率',
							'tr_coutry'		=>	'仅限美国',
							'detail_tips'	=>	'提示：GTIN 和 MPN 可以填其中一个 也可以两个都填',
						),
	'swap_chain'	=>	array(
							'keyword'		=>	'关键字',
							'url'			=>	'链接',
							'website'		=>	'显示网站',
							'change'		=>	'换一个',
							'add_tips'		=>	'请填写完整的链接地址 如：%domain%/about.html',
						),
	'google_verification'=>array(
							'filename'		=>	'文件名称',
							'error'			=>	'文件内容错误',
							'repeat'		=>	'请勿重复上传相同的文件',
						),
	'batch_edit'	=>	array(
							'title'			=>	'系统会根据下面设置的值，批量对全站产品生成一个不一样的值',	
						),
	'paypal_dispute'=>	array(
							'status_1'			=>	'协商中',
							'status_2'			=>	'PayPal正在审查',
							'status_3'			=>	'等待顾客回复',
							'status_4'			=>	'来自顾客的消息',
							'status_5'			=>	'已结束',
							'case_id'			=>	'事件号',
							'reason'			=>	'原因',
							'amount'			=>	'维权金额',
							'last_update'		=>	'最后更新日期',
							'dispute_details'	=>	'纠纷详情',
							'messages'			=>	'沟通记录',
							'dispute_status'	=>	'纠纷状态',
							'life_cycle_status'	=>	'纠纷阶段',
							'to_claim'			=>	'进入仲裁阶段',
							'claim_title'		=>	'请求进入PayPal仲裁阶段',
							'provide_evidence'	=>	'提供证据',
							'evidence_file'		=>	'证据文件',
							'evidence_type'		=>	'证据类型',
							'evidence_note'		=>	'与证据有关的说明',
							'carrier_name'		=>	'快递公司名称',
							'refund_title'		=>	'提出解决纠纷的提议',
							'refund_note'		=>	'附加优惠说明，PayPal可以查看，但客户不能查看',
							'refund_price'		=>	'建议解决纠纷的金额',
							'refund_type'		=>	'商家提出的纠纷提议类型',
							'refund_to_title'	=>	'接受索赔',
							'refund_to_note'	=>	'附加索赔说明，PayPal可以查看，但顾客不能查看',
							'refund_to_price'	=>	'商家同意退还客户的金额',
							'refund_to_reason'	=>	'商家接受顾客索赔的理由',
							'contact'			=>	'顾客联系',
							'sent_an_offer'		=>	'已经向顾客发送了报价',
							'accepted_offer'	=>	'顾客接受了报价',
							'declined_offer'	=>	'顾客拒绝了报价',
							'offer_details'		=>	'报价详情',
							'accepted_claim'	=>	'商家愿意接受索赔的责任',
							'customer'			=>	'顾客',
							'you'				=>	'你',
							'reason_ary'		=>	array(
														'MERCHANDISE_OR_SERVICE_NOT_RECEIVED'=>'顾客未收到商品',
														'MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED'=>'顾客报告商品与描述不符',
														'UNAUTHORISED'=>'顾客未授权购买商品',
														'CREDIT_NOT_PROCESSED'=>'未为顾客处理退款或退款',
														'DUPLICATE_TRANSACTION'=>'该交易是重复的',
														'INCORRECT_AMOUNT'=>'顾客收取的金额不正确',
														'PAYMENT_BY_OTHER_MEANS'=>'顾客通过其他方式支付了交易费用',
														'CANCELED_RECURRING_BILLING'=>'顾客因订阅或已取消的定期交易而被收取费用',
														'PROBLEM_WITH_REMITTANCE'=>'汇款出现问题',
														'OTHER'=>'其它',
													),
							'status_ary'		=>	array(
														'OPEN'=>'协商中',
														'WAITING_FOR_BUYER_RESPONSE'=>'正在等待顾客的回应',
														'WAITING_FOR_SELLER_RESPONSE'=>'正在等待商家的回应',
														'UNDER_REVIEW'=>'PayPal正在审查',
														'RESOLVED'=>'已结束',
														'OTHER'=>'其它',
													),
							'life_cycle_ary'	=>	array(
														'Inquiry'=>'协商',
                                                        'Response'=>'等待回应',
														'Claim'=>'仲裁',
                                                        'UnderReview'=>'PayPal正在审查',
														'Settlement'=>'完成'
													),
							'LifeCycle_ary'		=>	array(
														'INQUIRY'=>'协商阶段',
														'CHARGEBACK'=>'仲裁阶段',
														'PRE_ARBITRATION'=>'第一次上诉阶段',
														'ARBITRATION'=>'第二次上诉阶段'
													),
							'note_ary'			=>	array(
														'send_message'=>'向顾客发送友好消息',
														'to_claim'=>'关于将维权升级为索赔的说明',
														'refund_with_return'=>'你可以在收到退回的商品后发送退款',
														'under_review'=>'你已经提供了证据给Paypal，正在审核'
													),
							'offer_ary'			=>	array(
														'buyer_requested_amount'=>'顾客提议金额',
														'seller_offered_amount'=>'商家提议金额',
													),
							'offer_type_ary'	=>	array(
														'REFUND'=>'商家必须在没有任何物品更换或退货的情况下退还顾客',
														'REFUND_WITH_RETURN'=>'顾客必须将商品退回商家，然后商家将退款',
														'REFUND_WITH_REPLACEMENT'=>'商家必须退款，然后将替换物品发送给顾客',
														'REPLACEMENT_WITHOUT_REFUND'=>'商家必须向顾客发送更换商品，无需额外退款'
													),
							'reason_type_ary'	=>	array(
														'DID_NOT_SHIP_ITEM'=>'商家接受顾客的索赔，因为他们无法将商品运回顾客',
														'TOO_TIME_CONSUMING'=>'商家接受顾客的索赔，因为商家需要很长时间才能完成订单',
														'LOST_IN_MAIL'=>'商家正在接受顾客的索赔，因为该物品在邮件或运输过程中丢失',
														'NOT_ABLE_TO_WIN'=>'商家正在接受顾客的索赔，因为商家无法找到足够的证据来赢得此维权',
														'COMPANY_POLICY'=>'商家接受顾客声称遵守其内部公司政策',
														'REASON_NOT_SET'=>'没有设置原因'
													),
							'evidence_type_ary'	=>	array(
														'PROOF_OF_FULFILLMENT'=>'履行证明',
														'PROOF_OF_REFUND'=>'退款证明',
														'PROOF_OF_DELIVERY_SIGNATURE'=>'交货证明',
														'PROOF_OF_RECEIPT_COPY'=>'收据复印证明',
														'RETURN_POLICY'=>'退货政策',
														'BILLING_AGREEMENT'=>'结算协议',
														'PROOF_OF_RESHIPMENT'=>'转运凭证',
														'ITEM_DESCRIPTION'=>'项目描述',
														'POLICE_REPORT'=>'警方报告',
														'AFFIDAVIT'=>'宣誓书',
														'PAID_WITH_OTHER_METHOD'=>'支付另一种方法',
														'COPY_OF_CONTRACT'=>'合同副本',
														'TERMINAL_ATM_RECEIPT'=>'ATM收据',
														'PRICE_DIFFERENCE_REASON'=>'价格差异的原因',
														'SOURCE_CONVERSION_RATE'=>'源转换率',
														'BANK_STATEMENT'=>'银行对账单',
														'CREDIT_DUE_REASON'=>'信用到期原因',
														'REQUEST_CREDIT_RECEIPT'=>'请求信用收据',
														'PROOF_OF_RETURN'=>'返回证明',
														'CREATE'=>'创建',
														'CHANGE_REASON'=>'改变原因',
														'OTHER'=>'其它'
													),
							'carrier_name_ary'	=>	array(
														'UPS'=>'联合包裹服务',
														'USPS'=>'美国邮政服务',
														'FEDEX'=>'联邦快递',
														'AIRBORNE_EXPRESS'=>'安邦快递',
														'DHL'=>'DHL',
														'AIRSURE'=>'AirSure',
														'ROYAL_MAIL'=>'英国皇家邮政',
														'PARCELFORCE'=>'英国皇家邮政国际运输',
														'SWIFTAIR'=>'Swiftair',
														'OTHER'=>'其他',
														'UK_PARCELFORCE'=>'英国Parcelforce',
														'UK_ROYALMAIL_SPECIAL'=>'英国皇家邮政特快专递',
														'UK_ROYALMAIL_RECORDED'=>'英国皇家邮政录制',
														'UK_ROYALMAIL_INT_SIGNED'=>'英国皇家邮政国际签署',
														'UK_ROYALMAIL_AIRSURE'=>'英国皇家邮政AirSure',
														'UK_UPS'=>'英国联合包裹服务',
														'UK_FEDEX'=>'英国联邦快递',
														'UK_AIRBORNE_EXPRESS'=>'英国Airborne Express',
														'UK_DHL'=>'英国DHL',
														'UK_OTHER'=>'英国其他物流',
														'UK_CANNOT_PROV_TRACK'=>'无法提供英国跟踪',
														'CA_CANADA_POST'=>'加拿大邮报',
														'CA_PUROLATOR'=>'加拿大Purolator',
														'CA_CANPAR'=>'加拿大Canpar Courier',
														'CA_LOOMIS'=>'加拿大Loomis Express',
														'CA_TNT'=>'加拿大TNT',
														'CA_OTHER'=>'加拿大其他物流',
														'CA_CANNOT_PROV_TRACK'=>'无法提供加拿大跟踪',
														'DE_DP_DHL_WITHIN_EUROPE'=>'欧洲DHL包裹',
														'DE_DP_DHL_T_AND_T_EXPRESS'=>'DHL T and T Express',
														'DE_DHL_DP_INTL_SHIPMENTS'=>'DHL DP国际发货',
														'DE_GLS'=>'德国综合物流系统',
														'DE_DPD_DELISTACK'=>'德国DPD跟踪',
														'DE_HERMES'=>'德国爱马仕',
														'DE_UPS'=>'德国联合包裹服务',
														'DE_FEDEX'=>'德国联邦快递',
														'DE_TNT'=>'德国TNT',
														'DE_OTHER'=>'德国其他物流',
														'FR_CHRONOPOST'=>'法国Chronopost',
														'FR_COLIPOSTE'=>'法国Coliposte',
														'FR_DHL'=>'法国DHL',
														'FR_UPS'=>'法国联合包裹服务',
														'FR_FEDEX'=>'法国联邦快递',
														'FR_TNT'=>'法国TNT',
														'FR_GLS'=>'法国综合物流系统',
														'FR_OTHER'=>'法国其他物流',
														'IT_POSTE_ITALIA'=>'Poste Italia',
														'IT_DHL'=>'意大利DHL',
														'IT_UPS'=>'意大利联合包裹服务',
														'IT_FEDEX'=>'意大利联邦快递',
														'IT_TNT'=>'意大利TNT',
														'IT_GLS'=>'意大利通用物流系统',
														'IT_OTHER'=>'意大利其他物流',
														'AU_AUSTRALIA_POST_EP_PLAT'=>'澳大利亚邮政EP平台',
														'AU_AUSTRALIA_POST_EPARCEL'=>'澳大利亚邮报Eparcel',
														'AU_AUSTRALIA_POST_EMS'=>'澳大利亚邮政EMS',
														'AU_DHL'=>'澳大利亚DHL',
														'AU_STAR_TRACK_EXPRESS'=>'澳大利亚StarTrack Express',
														'AU_UPS'=>'澳大利亚联合包裹服务',
														'AU_FEDEX'=>'澳大利亚联邦快递',
														'AU_TNT'=>'澳大利亚TNT',
														'AU_TOLL_IPEC'=>'澳大利亚收费IPEC',
														'AU_OTHER'=>'澳大利亚其他物流',
														'FR_SUIVI'=>'法国Suivi联邦快递',
														'IT_EBOOST_SDA'=>'Poste Italiane SDA',
														'ES_CORREOS_DE_ESPANA'=>'西班牙Correos de',
														'ES_DHL'=>'西班牙DHL',
														'ES_UPS'=>'西班牙联合包裹服务',
														'ES_FEDEX'=>'西班牙联邦快递',
														'ES_TNT'=>'西班牙TNT',
														'ES_OTHER'=>'西班牙其他物流',
														'AT_AUSTRIAN_POST_EMS'=>'奥地利EMS邮件服务',
														'AT_AUSTRIAN_POST_PPRIME'=>'奥地利邮政总理',
														'BE_CHRONOPOST'=>'Chronopost',
														'BE_TAXIPOST'=>'出租车邮报',
														'CH_SWISS_POST_EXPRES'=>'瑞士邮政快递',
														'CH_SWISS_POST_PRIORITY'=>'瑞士邮政优先权',
														'CN_CHINA_POST'=>'中国邮政',
														'HK_HONGKONG_POST'=>'香港邮政',
														'IE_AN_POST_SDS_EMS'=>'爱尔兰发布SDS EMS Express邮件服务',
														'IE_AN_POST_SDS_PRIORITY'=>'爱尔兰发布SDS优先权爱尔兰',
														'IE_AN_POST_REGISTERED'=>'爱尔兰邮政注册',
														'IE_AN_POST_SWIFTPOST'=>'爱尔兰Swift Post',
														'IN_INDIAPOST'=>'印度邮政',
														'JP_JAPANPOST'=>'日本邮报',
														'KR_KOREA_POST'=>'韩国邮政',
														'NL_TPG'=>'荷兰TPG邮政',
														'SG_SINGPOST'=>'新加坡SingPost',
														'TW_CHUNGHWA_POST'=>'中华台湾邮政',
														'CN_CHINA_POST_EMS'=>'中国邮政EMS',
														'CN_FEDEX'=>'中国联邦快递',
														'CN_TNT'=>'中国TNT',
														'CN_UPS'=>'中国联合包裹服务',
														'CN_OTHER'=>'中国其他物流',
														'NL_TNT'=>'荷兰TNT',
														'NL_DHL'=>'荷兰DHL',
														'NL_UPS'=>'荷兰联合包裹服务',
														'NL_FEDEX'=>'荷兰联邦快递',
														'NL_KIALA'=>'荷兰KIALA',
														'BE_KIALA'=>'比利时Kiala Point',
														'PL_POCZTA_POLSKA'=>'Poczta Polska',
														'PL_POCZTEX'=>'Pocztex',
														'PL_GLS'=>'波兰综合物流系统',
														'PL_MASTERLINK'=>'波兰Masterlink',
														'PL_TNT'=>'波兰TNT',
														'PL_DHL'=>'波兰DHL',
														'PL_UPS'=>'波兰联合包裹服务',
														'PL_FEDEX'=>'波兰联邦快递',
														'JP_SAGAWA_KYUU_BIN'=>'日本佐川Kyuu Bin',
														'JP_NITTSU_PELICAN_BIN'=>'日本Nittsu Pelican Bin',
														'JP_KURO_NEKO_YAMATO_UNYUU'=>'日本Kuro Neko Yamato Unyuu',
														'JP_TNT'=>'日本TNT',
														'JP_DHL'=>'日本DHL',
														'JP_UPS'=>'日本联合包裹服务',
														'JP_FEDEX'=>'日本联邦快递',
														'NL_PICKUP'=>'荷兰Pickup',
														'NL_INTANGIBLE'=>'荷兰Intangible',
														'NL_ABC_MAIL'=>'荷兰ABC Mail',
														'HK_FOUR_PX_EXPRESS'=>'香港4PX',
														'HK_FLYT_EXPRESS'=>'香港Flyt'
													),
						),
	'dhl_account_open'	=>	array(
							'title'				=>	'DHL eCommerce在线开通账号',
							'step_1'			=>	'请选择开户地点和账户类型',
							'step_2'			=>	'信息录入',
							'step_3'			=>	'签名盖章',
							'step_4'			=>	'上传资料',
							'step_5'			=>	'托运人隐含危险物品清单',
							'step_6'			=>	'个人资料',
							'step_7'			=>	'价格表',
							'step_8'			=>	'条款与条件',
							'step_9'			=>	'预付款信息页面',
							'account_address'	=>	'开户地点',
							'address'			=>	array('香港 HONG KONG', '深圳 SHEN ZHEN', '上海 SHANG HAI'),
							'account_type'		=>	'账号类型',
							'type'				=>	array('企业用户', '个人账号'),
							'referrer'			=>	'推荐人',
							'start_register'	=>	'开始注册',
							'cancel_register'	=>	'取消注册',
							'company_info'		=>	'信息',
							'company_name'		=>	'公司名称',
							'business_number'	=>	'营业执照/营业许可证',
							'register_date'		=>	'注册日期',
							'register_address'	=>	'注册地',
							'company_address'	=>	'公司地址',
							'street'			=>	'街道',
							'house_no'			=>	'门牌号',
							'zip_code'			=>	'邮编',
							'city'				=>	'城市',
							'state'				=>	'(地区)省份',
							'country'			=>	'国家或地区',
							'telephone_no'		=>	'电话号码',
							'website'			=>	'网址(选填)',
							'fax_no'			=>	'传真号码',
							'contact_name'		=>	'联络人姓名',
							'contact_email'		=>	'电子邮件地址',
							'contact_mobile'	=>	'联络人手机号码',
							'system_contact_name'	=>	'系统对接联络人姓名',
							'system_contact_email'	=>	'系统对接电子邮件地址',
							'system_contact_mobile'	=>	'系统对接联络人手机号码',
							'billing_address'		=>	'账单/发票地址',
							'same_register_address'	=>	'同注册地址',
							'bill_company_name'		=>	'注册公司名称',
							'bill_zipcode'			=>	'城市/邮编',
							'bill_authorized_name'	=>	'授权签字人姓名',
							'pickup_address'		=>	'取件地址',
							'credit_veri_details'	=>	'信用验证资讯（授权签字人确认说提供的所有资讯真实有效）',
							'annual_est_revenue'	=>	'预计年付运费',
							'owner_name'			=>	'经营者/董事姓名',
							'owner_address'			=>	'地址',
							'contact_number'		=>	'联络电话',
							'finance_contact_name'	=>	'财务联络人姓名',
							'office_premise'		=>	'办公场所',
							'office_premise_type'	=>	array('自有', '租赁'),
							'finance_address'		=>	'财务联络人地址',
							'finance_contact_number'=>	'财务联络人联络电话',
							'finance_contact_email'	=>	'财务联络人电子邮件地址',
							'VAT_invoice'			=>	'增值税发票信息征询表',
							'VAT_invoice_notes'		=>	'尊敬的客户：请完整填列下表，并确保相关税务信息与贵司税务登记证以及向税局备案信息完全一致，感谢您的支持与配合！',
							'vat_company'			=>	'公司名称（税务登记证或者营业执照的公司名称)',
							'taxpayer_type'			=>	'请选择所属纳税人类型',
							'taxpayer_type_ary'		=>	array('增值税一般纳税人', '非增值税一般纳税人'),
							'social_credit_code'	=>	'统一社会信用代码',
							'vat_company_address'	=>	'公司地址（营业执照注册）',
							'vat_company_telephone'	=>	'公司电话（营业执照注册）',
							'vat_bank_name'			=>	'开户银行名称（税局备案信息）',
							'vat_bank_account'		=>	'开户银行账号（税局备案信息）',
							'vat_invoice_type'		=>	'请选择上线后所需我司开具的发票类型',
							'vat_invoice_code_ary'	=>	array('增值税普通发票', '增值税专用发票（只有增值税一般纳税人方可选择此项）', '无需税务发票'),
							'vat_invoice_con_name'	=>	'税务发票事宜联系人',
							'vat_invoice_con_tele'	=>	'税务发票事宜联系人电话',
							'vat_invoice_con_email'	=>	'税务发票事宜联系人邮箱',
							'payment_auth'			=>	'委托付款',
							'is_payment_auth'		=>	'是否由第三方付款',
							'is_payment_auth_ary'	=>	array('是', '否'),
							'3rd_company_name'		=>	'第三方名称',
							'3rd_bank_account'		=>	'第三方银行账号',
							'is_charged'			=>	'是否有带电货品？',
							'has_charged_ary'		=>	array('无带电货品（无需要下载DG表）', '有带电货品（需要下载DG表签署盖章上传）'),
							'click_print'			=>	'点击打印',
							'charged_upload_tips'	=>	'请将已签名并盖章的(KC和DG)表格扫描至本地电脑内，然后上传，扫描文件体积应不超过 2 MB。',
							'kc_filepath'			=>	'KC文件路径',
							'dg_filepath'			=>	'DG文件路径',
							'kc_filename'			=>	'KC文件名称',
							'dg_filename'			=>	'DG文件名称',
							'upload_data_title'		=>	'请上传以下资料：',
							'upload_business'		=>	'营业执照',
							'ID_card_front'			=>	'身份证(照片面)',
							'reverse_ID_card'		=>	'身份证(芯片面)',
							'no_picture'			=>	'请上传图片',
							'please_update'			=>	'请上传',
							'prev_step'				=>	'上一步',
							'next_step'				=>	'下一步',
							'danger_warning'		=>	'为保护飞机及其乘客的安全，请不要错误申报危险物品',
							'reference_document'	=>	'参考文件',
							'shipper_name'			=>	'托运人名称',
							'shipper_telephone'		=>	'联系人号码',
							'cargo_description'		=>	'货物说明',
							'freight_reference'		=>	'货运参考号',
							'comfire_info'			=>	'我已阅读并同意以上注册条款细则',
							'please_comfire'		=>	'请您勾选确认“我已阅读并同意以上注册条款细则”',
							'please_comfire_2'		=>	'本人我们现确认以上提供的所有资讯均为真实完整。本人/我们已阅读、了解并现确认接受GTC。本人/ 我们同意本人/我们将被视为GTC中所定义的寄件人。除非双方另有书面协定,本人我们现同意:の在DHL电子商务公司不时有效的《上海费率》(現行版本诉列于上文第V节)中列出的费率、费用、价格和附加费适用于DHL电子商务公司接纳的所有货物:及(i)在DHL电子商务公司开出发之后的十四(14)天内支付该发票。本人我们进一歩确认如本人/我们在DHL电子商务公司的账户连续12月未有进行任何交易,DHL电子商务公司可以在不事先通知本人我们的情況下，自行決定停用该账户。',
							'end_title'				=>	'结束',
							'download_zip'			=>	'所有附件下载',
							'end_tips'				=>	'谢谢！<br /><br /><br />您的开户资料及支付信息已经成功提交，如资料审核通过即成功开户，我司将于三个工作日内发邮件通知您。<br /><br /><br />如您提交的资料有误或支付信息有误，48小时内我司将有专人与您联系跟进。',
							'upload_tips'			=>	'支持2M以下png/jpg文件',
							'payment_pic'			=>	'上传已付款截图',
							'payment_info'			=>	'支付账号信息如下图',
						),
	'dhl_account_info'	=>	array(
							'set'					=>	'设置信息',
							// ''
						),
	'custom_comments'	=>	array(
							'title'					=>	'新站做旧',
							'review'				=>	'评论',
							'pro_data'				=>	'操作',
							'review_edit'			=>	'自定义评论列表',
							'upload'				=>	'导入',
							'number'				=>	'产品编号',
							'content'				=>	'评论内容',
							'upload_bat'			=>	'批量导入',
							'add'					=>	'添加自定义评论',
							'star'					=>	'评论星星',
							'use_data'				=>	'使用虚拟数据',
							'edit_data'				=>	'添加虚拟数据',
							'open'					=>	'使用虚拟数据代替真实的数据',
							'sales'					=>	'产品销量',
							'fcount'				=>	'收藏数',
							'review_rating'			=>	'评论平均分',
							'review_total_rating'	=>	'评论人数',
							'ec_title'				=>	'导入自定义评论',
							'no_file'				=>	'文件不存在！',
							'upload_bat_succ'		=>	'批量上传完成',
							'upload_succ'			=>	'导入成功',
							'upload_fail'			=>	'导入失败（这个产品已经存在这条评论）',
						),
	'asiafly'			=>	array(
							'title'			=>	'亚翔供应链',
						),
);