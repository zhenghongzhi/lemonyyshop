<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791

*/
return array(
	'default_open'		=>	'默认展开',
	'billing'			=> array(
							'device'		=>	'设备',
							'device_ary'	=>	array('PC','手机'),
							'position'		=>	'位置',
							'position_ary'	=>	array('头部','中间','全屏',10=>'团购',11=>'秒杀',12=>'内页左侧'),
							'url'			=>	'链接',
							'content'		=>	'内容',
							'position_tips'	=>	'请选择广告位置',
	),
	'explain'			=> array(
							'order'		=>	'光标移动到客服账号前的绿点，点击拖动可实现客服排序',
							'email'		=>	'启用插件后，直接勾选多个收件人发送，即可实现群发',
	),
)
?>