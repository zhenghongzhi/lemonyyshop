<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

return array(
	'global'	=>	array(
						'n_y'			=>	array('否', '<font class="fc_red">是</font>'),
						'n_y_ary'		=>	array('否', '是'),
						'my_order_ary'	=>	array('默认'),
						'add'			=>	'添加',
						'del'			=>	'删除',
						'del_bat'		=>	'批量删除',
						'edit'			=>	'修改',
						'edit_bat'		=>	'批量修改',
						'mod'			=>	'编辑',
						'copy'			=>	'复制',
	                    'yunshu'=>"运输方式",
						'export'		=>	'导出',
						'batch'			=>	'批量',
						'set'			=>	'设置',
						'my_order'		=>	'排序',
						'date'			=>	'日期',
						'time'			=>	'时间',
						'submit'		=>	'提交',
						'confirm'		=>	'确认',
						'confirm_sure'	=>	'确定',
						'save'			=>	'保存',
						'reset'			=>	'重置',
						'return'		=>	'返回',
						'cancel'		=>	'取消',
						'close'			=>	'关闭',
						'all'			=>	'所有',
                        'all_to'        =>  '全部',
						'serial'		=>	'序号',
						'name'			=>	'名称',
						'email'			=>	'邮箱',
						'operation'		=>	'操作',
						'option'		=>	'选项',
						'search'		=>	'搜索',
						'default'		=>	'默认',
						'last'			=>	'最后',
						'print'			=>	'打印',
						'category'		=>	'分类',
						'sub_category'	=>	'子分类',
						'subjection'	=>	'隶属',
						'implode'		=>	'导入',
						'explode'		=>	'导出',
						'view'			=>	'查看',
						'move'			=>	'移动',
						'move_to'		=>	'移动到',
						'turn_on'		=>	'开启',
						'used'			=>	'启用',
						'use'			=>	'使用',
						'in_use'		=>	'已使用',
						'no_use'		=>	'不使用',
						'required'		=>	'必填',
						'select_all'	=>	'全选',
						'anti'			=>	'反选',
						'open'			=>	'展开',
						'pack_up'		=>	'收起',
						'pic'			=>	'图片',
						'preview'		=>	'预览',
						'release'		=>	'发布',
						'status'		=>	'状态',
						'synchronize'	=>	'同步',
						'key_sync'		=>	'一键同步',
						'logo'			=>	'Logo',
						'custom_column'	=>	'自定义列',
						'seo'			=>	'标题与标签',
						'title'			=>	'标题',
						'subtitle'		=>	'副标题',
						'keyword'		=>	'关键词',
						'depict'		=>	'描述',
						'url'			=>	'链接',
						'brief'			=>	'简短介绍',
						'description'	=>	'详细介绍',
						'myorder'		=>	'排序优先级',
						'pre_page'		=>	'<<上一页',
						'next_page'		=>	'下一页>>',
						'upload_pic'	=>	'上传图片',
						'select_index'	=>	'请选择',
						'select_write'	=>	'请选择或填写',
						'do_error'		=>	'操作无效',
						'isindex'		=>	'首页显示',
						'type'			=>	'类型',
						'domain'		=>	'域名',
						'reference'		=>	'参考文档',
						'excel_format'	=>	'Excel导出',
						'please_enter'	=>	'请输入',
						'placeholder'	=>	'请输入关键词',
						'old_version'	=>	'旧版',
						'new_version'	=>	'新版',
						'other'			=>	'其他',
						'total'			=>	'共',
						'item'			=>	'件',
						'more'			=>	'更多',
						'clear'			=>	'清除',
						'file_upload'	=>	'选择文件',
						'editor'		=>	'编辑器',
						'send'			=>	'发送',
						'management'	=>	'管理',
						'message'		=>	'消息',
						'amount'		=>	'金额',
						'content'		=>	'内容',
						'translation'	=>	'翻译',
						'translation_chars'=>'剩余%s字符',
						'language'		=>	'语言',
						'public'		=>	'公共',
						'help'			=>	'帮助中心',
						'help_doc'		=>	'帮助文档',
						'welfare'		=>	'营销视频',
						'upload_file'	=>	'上传文件',
						'down_table'	=>	'下载表格',
						'click_here'	=>	'请<a href="%url%">点击此处</a>下载表格,并按照要求填写好表格。',
						'upload_table'	=>	'上传表格并提交',
						'submit_table'	=>	'上传填写好的表格，然后提交。',
						'add_new'		=>	'新增',
						'load_more'		=>	'加载更多',
						'not_limit'		=>	'不限',
                        'how_to_get'    =>  '如何获取',
						'support' 		=>	array('企业QQ', '上班时间', '电话号码', '微信号', '投诉电话', '网站到期时间'),
						'tmp_domain_alert'	=>	'能更快访问网站管理后台，解决国内访问国外的网站常常会被屏蔽的问题，前台使用%domain%访问',
						'expired_tips'	=>	'网站%days%天后过期, 到期时间%expiredTime%',
						'inquiry'		=>	'查询',
						'wechat_ser'	=>	'微信客服',
						'scan_code'		=>	'扫码添加微信客服',
						'free_service'	=>	'获取30天一对一免费客户服务',
						'wechat_chuang'	=>	'创店客服微信',
						'free_ver'		=>	'免费版',
						'free_upgrade'	=>	'升级商业版',
						'paid_ver'		=>	'商业版',
						'upgrade_more'	=>	'升级至商业版的权益',
						'upgrade_benf'	=>	array('访问提速30%,解锁带宽上限','访问再提速20%,转美国服务器','增加SEO应用','增加运营应用','增加营销应用','增加管理应用'),
						'pay_func'		=>	'此应用为商业版功能',
						'conatact_ser'	=>	'联系您的创店客服',
						'invoice'		=>	'* 购买商业版可提供合同与正规发票',
						
					),
	'module'	=>	array(
						'account'		=>	array(
												'module_name'		=>	'我的帐号',
												'index'				=>	'首页',
												'password'			=>	'修改密码'
											),
						'orders'		=>	array(
												'module_name'		=>	'订单',
												'orders'			=>	'订单管理',
												'waybill'			=>	'运单管理',
												'import_delivery'	=>	'导入快递',
						                        'source_request'	=>	'采购需求'
											),
						'products'		=>	array(
												'module_name'		=>	'产品',
												'products'			=>	'产品管理',
												'category'			=>	'分类',
												'review'			=>	'评论',
                                                'business_catelog'	=>	'供应商产品分类目录',
                                                'business_cate'	    =>	'供应商产品分类',
												'business'			=>	array(
                                                    'module_name'	=>	'供应商'
                                                ),
                                                'business_product'	=>	'供应商产品',
                                                'logistics_line'	=>	'物流线路',
                                                'sync'				=>	'数据同步'
											),
						'user'			=>	array(
												'module_name'		=>	'顾客',
												'user'				=>	'顾客',
												'inbox'				=>	'站内信',
                                                'message'			=>	'系统消息',
                                                'usermessage'		=>	'采购留言'
											),
						'sales'			=>	array(
												'module_name'		=>	'促销',
												'sales'				=>	'限时促销',
												'seckill'			=>	'限时秒杀',
												'tuan'				=>	'团购管理',
												'package'			=>	'组合购买',
												'promotion'			=>	'组合促销',
												'coupon'			=>	'优惠券',
												'discount'			=>	'满减设置',
												'holiday'			=>	'节日促销'
											),
						'operation'		=>	array(
												'module_name'		=>	'运营',
												'page'				=>	array(
																			'module_name'	=>	'单页管理'
																		),
												'billing'			=>	'弹窗广告',
												'email'				=>	array(
																			'module_name'		=>	'邮件',
																			'send'				=>	'邮件发送',
																			'config'			=>	'邮件设置',
																			'system'			=>	'系统邮件设置',
																			'email_logs'		=>	'邮件发送日志'
																		),
												'arrival'			=>	'到货通知',
												'newsletter'		=>	'订阅管理',
												'chat'				=>	array(
																			'module_name'	=>	'在线客服',
																			'set'			=>	'设置',
																			'chat'			=>	'帐号管理'
																		),
												'googlefeed'		=>	'Google Feed',
												'gallery'			=>	'买家秀',
												'distribution'		=>	'分销',
												'facebook_store'	=>	'Facebook专页',
												'blog'				=>	array(
																			'module_name'	=>	'博客管理',
																			'set'			=>	'博客设置',
																			'blog'			=>	'博客管理',
																			'category'		=>	'博客分类',
																			'review'		=>	'博客评论'
																		),
												'translate'			=>	'Google翻译',
												'google_verification'=>	'Google验证文件',
												'partner'			=>	'友情链接',
												'mailchimp'			=>	array(
																			'module_name'		=>	'邮件群发',
																			'send'				=>	'邮件发送',
																			'email_logs'		=>	'邮件发送日志'
																		),
												'swap_chain'		=>	'交换链接',
												'paypal_dispute'	=>	'Paypal纠纷',
											),
						'mta'			=>	array(
												'module_name'		=>	'数据',
												'visits'			=>	'流量统计',
												'visits_conversion'	=>	'流量转化率',
												'orders'			=>	'订单统计',
												'orders_repurchase'	=>	'复购率',
												'products_sales'	=>	'产品销量排行',
												'user'				=>	'顾客概况',
												'search_logs'		=>	'搜索统计',
                                                'yyorders'		    =>	'采购订单统计',
                                                'yykehu'		    =>	'采购客户统计',
                                                'yytranslation'		=>	'采购业务统计',
                                                'yyshangjia'		=>	'采购商家统计',
                                                'yyproduct'		    =>	'采购产品统计',
											),
						'plugins'		=>	array(
												'module_name'		=>	'应用',
												'app'				=>	'应用商店',
												'my_app'			=>	'我的应用'
												),
						'set'			=>	array(
												'module_name'		=>	'设置',
												'test'				=>	'样板',
												'config'			=>	'基本设置',
												'domain_binding'	=>	'域名绑定',
												'themes'			=>	array(
																			'module_name'	=>	'风格模板',
																			'index_set'		=>	'首页设置',
																			'themes'		=>	'风格',
																			'nav'			=>	'头部导航',
																			'footer_nav'	=>	'底部导航',
																			'products_list'	=>	'产品列表页',
																			'products_detail'=>	'产品详细页',
																			'style'			=>	'色调',
																			'advanced'		=>	'其它',
																		),
												'orders_print'		=>	'打印设置',
												'exchange'			=>	'汇率设置',
                                                'newexchange'	    =>	'伊朗汇率设置',
                                                'newexchangelist'	=>	'伊朗历史汇率',
												'oauth'				=>	'平台设置',//授权
												'payment'			=>	'支付设置',
												'country'			=>	'国家管理',
												'photo'				=>	array(
																			'module_name'	=>	'图片管理'
																		),
												'shipping'			=>	array(
																			'module_name'	=>	'运费管理',
																			'set'			=>	'设置',
																			'overseas'		=>	'海外仓',
																			'express'		=>	'快递',
																			'air'			=>	'空运',
																			'ocean'			=>	'海运',
																			'insurance'		=>	'保险',
																			'overseas'		=>	'海外仓'
																		),
												'authorization'		=>	array(
																			'module_name'	=>	'平台授权',
																			'open'			=>	'开放接口',
																			'aliexpress'	=>	'速卖通',
																			'amazon'		=>	'亚马逊',
																			'wish'			=>	'Wish',
																			'shopify'		=>	'Shopify'
																		),
												'seo'				=>	'SEO优化',
												'third'				=>	'第三方代码',
												'manage'			=>	'管理员',
												'manage_logs'		=>	'系统日志'
											),
						'view'			=>	array(
												'module_name'		=>	'店铺',
												'visual'			=>	'界面',
												'nav'				=>	'头部导航',
												'footer_nav'		=>	'底部导航',
											),
						'skin'			=>	array(
												'module_name'		=>	'网站风格管理',
												'index'				=>	'首页风格管理',
												'page'				=>	'内页风格管理',
												'prodcuts_list'		=>	'产品列表页风格管理',
												'prodcuts_detail'	=>	'产品详细页风格管理',
												'news_list'			=>	'文章列表页风格管理',
												'news_detail'		=>	'文章详细页风格管理'
											),
						'mobile'	=>	array(
												'module_name'		=>	'移动端',
												'themes'			=>	'风格',
												'index_set'			=>	'首页设置'
											)
					),
	'language'	=>	array(
						'zh-cn'			=>	'简体中文',
						'en'			=>	'英文',
						'jp'			=>	'日语',
						'de'			=>	'德语',
						'fr'			=>	'法语',
						'es'			=>	'西班牙语',
						'ru'			=>	'俄语',
						'pt'			=>	'葡萄牙语',
						'zh_tw'			=>	'繁体中文',
						'cn'			=>	'简体中文',
						'fa'			=>	'波斯语',
					),
	'continent'	=>	array(
						1				=>	'亚洲',
						2				=>	'欧洲',
						3				=>	'非洲',
						4				=>	'北美洲',
						5				=>	'南美洲',
						6				=>	'大洋洲',
						7				=>	'南极洲'
					),
	'frame'		=>	array(
						'system_name'	=>	$c['FunVersion']>=10?'创店店铺系统':'外贸系统',
						'pc'			=>	'网站端',
						'mobile'		=>	'移动端',
						'email'			=>	'发送邮件',
						'mta'			=>	'数据统计',
						'course'		=>	'使用教程',
						'clear_cache'	=>	'清空缓存',
						'home'			=>	'网站首页',
					),
	'ckeditor'	=>	array(
						'file_type_err'	=>	'文件上传失败，文件类型错误！',
						'upload_success'=>	'文件上传成功！',
						'upload_fail'	=>	'文件上传失败！',
					),
	'notes'		=>	array(
						'discount_rule'	=>	'（填写范围：0至100，0为不打折，20等同于8折，100为免费）',
						'pic_tips'		=>	'每次可同时上传%s张图片',
						'png_tips'		=>	'建议上传png透明图片，',
						'jpg_tips'		=>	'建议上传jpg格式图片，',
						'pic_size_tips'	=>	'图片大小建议: %s像素',
						'ico_tips'		=>	'必须上传.ico格式的透明图片，',
						'new_pic_tips'	=>	'不超过%count%张，建议上传%format%格式图片，图片大小建议: %size%像素',
						'new_pic_tips_1'=>	'格式图片支持%format%，图片大小建议: %size%像素',
						'page_tips'		=>	'共 %num% 条',
						'unread_message'=>	'你有未读消息，请及时处理！',
					),
	'msg'		=>	array(
						'open_success'		=>	'开启成功！',
						'close_success'		=>	'关闭成功！',
						'add_success'		=>	'添加成功！',
						'edit_success'		=>	'修改成功！',
						'import_success'	=>	'导入成功！',
						'reset_success'		=>	'重置成功！',
						'send_success'		=>	'发送成功！',
						'add_error'			=>	'添加失败！',
						'edit_error'		=>	'修改失败！',
						'send_error'		=>	'发送失败！',
                        'operating_success' =>  '操作成功！',
                        'operating_error'   =>  '操作失败！',
					),
	'error'		=>	array(
						'no_data'			=>	'暂无数据',
						'no_table_data'		=>	'当前暂时没有数据',
						'add_now'			=>	'马上添加',
						'supplement_lang'	=>	'请填写其他语言版资料！',
					),
	'account'	=>	array(
						'welcome'			=>	'用户登录',
						'username'			=>	'用户名',
						'password'			=>	'密码',
						'login_btn'			=>	'登 录',
						'login_title'		=>	'越洋寰球',
						'wel_txt'			=>	'每一次尝试都是成功的开始！',
						'locking'			=>	'账号已被锁定，请联系管理员',
						'error'				=>	'用户名或密码错误，请重新登录',
						'timeout'			=>	'登录连接超时，请重试',
						'logout'			=>	'退出',
						'personal_center'	=>	'个人中心',
						'domain'			=>	'域名',
						'server'			=>	'服务器',
						'open_time'			=>	'开通时间',
						'trial_time'		=>	'试用时间',
						'customer_service'	=>	'客服专员',
						'hotline'			=>	'客服热线',
						'qq'				=>	'客服QQ',
						'proposal'			=>	'投诉与建议',
						'new_review'		=>	'最新评论',
						'system_messages'	=>	'系统消息',
						'orders_messages'	=>	'订单消息',
						'change_password'	=>	'更改密码',
						'month_quantity'	=>	'订单、流量统计',
						'today_statistics'	=>	'今天统计',
						'order_statistics'	=>	'订单统计',
						'today_income'		=>	'今天收入',
						'income_statistics'	=>	'收入统计',
						'today_user'		=>	'今天新增',
						'user_statistics'	=>	'会员统计',
						'today_review'		=>	'今天评论',
						'review_statistics'	=>	'评论统计',
						'wait_payment_order'=>	'待付款订单',
						'wait_delivery_order'=>	'待发货订单',
						'stock_warning'		=>	'产品库存警告',
						'return'			=>	'退货',
						'backup_time'		=>	'备份时间',
						'traffic_time'		=>	array('最近30天', '今天', '昨天', '最近7天', '最近15天', '全部（全部只是针对订单）'),
						'traffic_statistics'=>	'流量统计',
						'statistics_name'	=>	array('总订单数', '总销售额', '访客', '浏览量'),
						'source'			=>	'访客来源',
						'new_order'			=>	'最新订单',
						'from'				=>	'访客分布',
						'keyword_ranking'	=>	'Google关键词排名监控',
						'error_tips'		=>	'没有符合条件的结果！',
						'support'			=>	array('企业QQ', '上班时间', '电话号码', '微信号', '上级投诉人', '网站到期时间'),
						'data_backup'		=>	'数据备份',
						'backup'			=>	array('数据库', '文件夹'),
						'day_ary'			=>	array('今天', '昨天', '过去7天', '过去30天'),
						'open_store'		=>	'快速开启店铺',
						'exit_guide'		=>	'退出引导模式',
						'add_now'			=>	'马上添加',
						'set_themes'		=>	'选择模板',
						'add_pro'			=>	'添加产品',
						'set_shipping'		=>	'设置运费',
						'set_payment'		=>	'设置支付方式',
						'go_set'			=>	'去设置',
						'course'			=>	'推荐',
						'upload'			=>	'产品导入',
						'suggest'			=>	'建议',
						'analysis'			=>	'掌握使用数据分析',
						'analysis_desc'		=>	'收集访客访问数据并汇总各个方面的关键指标，快速掌握网站运营情况',
						'view_more'			=>	'查看更多',
                        'total_sales'       =>  '总销售额',
                        'total_orders'      =>  '总订单数',
                        'order_item'        =>  '单',
                        'detail'            =>  '详细',
						'ad'				=>	array(
													'global'	=>	array(
																		'do_it'=>'现在使用'
																	),
													'facebook'	=>	array(
																		'title'=>'Facebook广告',
																		'links'=>array('广告开通', '广告常见问题', '在线实操课程'),
																		'note_title'=>'插件功能包括:',
																		'note_list'=>array(
																			array('Facebook店铺', '一键发布到Facebook'),
																			array('Facebook专页', '在Facebook上创建主页'),
																			array('Facebook Messenger', '浏览网站时可以联系你'),
																			array('Facebook 像素', '应用于网站的追踪代码')
																		)
																	),
													'google'	=>	array(
																		'title'=>'Google广告',
																		'links'=>array('Google Analytics对接说明', '营销常见问题', '在线实操课程', '代理商开户推荐'),
																		'note_list'=>array(
																			array('GA追踪代码', '直接观察分析各项数据'),
																			array('Google验证文件', '验证网站的所有权'),
																			array('Google Feed', '把产品提交到google')
																		)
																	),
													'bing'		=>	array(
																		'title'=>'Bing广告'
																	)
												)
					),
	'set'		=>	include('zh-cn/set.php'),
	'operation'	=>	include('zh-cn/operation.php'),
	'themes'	=>	include('zh-cn/themes.php'),
	'shipping'	=>	include('zh-cn/shipping.php'),
	'page'		=>	include('zh-cn/page.php'),
	'news'		=>	include('zh-cn/news.php'),
	'partner'	=>	include('zh-cn/partner.php'),
	'products'	=>	include('zh-cn/products.php'),
	'orders'	=>	include('zh-cn/orders.php'),
    'source_request'	=>	include('zh-cn/source_request.php'),
	'user'		=>	include('zh-cn/user.php'),
	'inbox'		=>	include('zh-cn/inbox.php'),
	'email'		=>	include('zh-cn/email.php'),
	'ad'		=>	include('zh-cn/ad.php'),
	'mobile'	=>	include('zh-cn/mobile.php'),
	'sales'		=>	include('zh-cn/sales.php'),
	'blog'		=>	include('zh-cn/blog.php'),
	'seo'		=>	include('zh-cn/seo.php'),
	'business'	=>	include('zh-cn/business.php'),
	'mta'		=>	include('zh-cn/mta.php'),
	'plugins'	=>	include('zh-cn/plugins.php'),
	'view'		=>	include('zh-cn/view.php'),
	'search'	=>	array(
						'custom_url'		=>	'自定义链接',
						'custom_url_notes'	=>	'没有填写自定义链接时会自动按照产品关键词搜索',
						'edit_time'			=>	'修改时间',
					),
	'analytics'	=>	array(
						'set'		=>	'Google Analytics 设置',
						'client_id'	=>	'Client ID',
						'detail'	=>	'登录查看',
					),
	'search_logs'=>	array(
						'number'	=>	'搜索次数',
						'result'	=>	'搜索结果数',
						'time'		=>	'最后搜索',
					),
	'manage'	=>	array(
						'manage'		=>	array(
												'manager'			=>	'管理员',
												'username'			=>	'用户名',
												'oldpassword'		=>	'旧密码',
												'last_login_time'	=>	'最后登录时间',
												'last_login_ip'		=>	'最后登录IP',
												'locked'			=>	'允许登录',
												'create_time'		=>	'创建时间',
												'password'			=>	'登录密码',
												'confirm_password'	=>	'确认密码',
												'password_un_mod'	=>	'留空则不修改密码',
												'password_len_tips'	=>	'对不起，密码的长度必须为6位以上！',
												'password_confirm_tips'=>'对不起，新密码与确认密码不匹配，请重新输入！',
												'len_tips'			=>	'对不起，用户名和密码的长度必须为6位以上！',
												'manage_exists'		=>	'对不起，此用户名已经被占用，请换一个用户名！',
												'del_current_user'	=>	'不允许删除当前登录用户',
												'permit'			=>	'权限',
												'group'				=>	'用户组',
												'permit_name'		=>	array(
																			1=>'超级管理员',
																			2=>'普通管理员',
																			3=>'业务员'
																		),
												'no_permit'			=>	'您无权限进行此操作，请联系管理员！',
											),
						'manage_logs'	=>	array(
												'username'			=>	'用户名',
												'module'			=>	'功能模块',
												'log_contents'		=>	'日志内容',
												'ip'				=>	'IP地址',
												'ip_from'			=>	'IP地址来源',
												'tran_data'			=>	'提交数据',
											),
					),
);
?>