<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
$system_module=array(
	'account'	=>	array('index'),
	'orders'	=>	array(
						'orders'	=>	array('permit'	=>	array('edit', 'del', 'export')),
						'waybill'	=>	array('permit'	=>	array('edit')),
						'import_delivery'=>	'',
	    'source_request'=>array('permit'	=>	array('inbox')),
					),
	'products'	=>	array(
						'products'	=>	array('permit'	=>	array('add', 'edit', 'copy', 'del', 'export','yunshu')),
						'category'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'review'	=>	array('permit'	=>	array('edit', 'del')),
                        'business_catelog'=>array('permit'	=>	array('add', 'edit', 'del')),
                        'business_cate'=>array('permit'	=>	array('add', 'edit', 'del')),
						'business'	=>	array(
											'menu'		=>	array('category', 'business'),
											'permit'	=>	array(
																'category' => array('add', 'edit', 'del'),
																'business' => array('add', 'edit', 'del')
															)
										),
                        'business_product'=>array('permit'	=>	array('add', 'edit', 'del')),
                        'sync'		=>	array('permit'	=>	array('add', 'edit', 'del')),
                        'logistics_line'		=>	array('permit'	=>	array('add', 'edit', 'del')),
                        'yunshu'	=>	array('permit'	=>	array('yunshu')),
					),
	'user'		=>	array(
						'user'		=>	array('permit'	=>	array('add', 'edit', 'del', 'export')),
						'inbox'		=>	array('permit'	=>	array('add', 'edit', 'del')),
						'message'	=>	array('permit'	=>	array('add', 'edit', 'del')),
                        'usermessage'=>	array('permit'	=>	array('add', 'edit', 'del'))
					),
	'sales'		=>	array(
						'coupon'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'discount'	=>	'',
						'sales'		=>	array('permit'	=>	array('add', 'edit', 'del')),
						'seckill'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'tuan'		=>	array('permit'	=>	array('add', 'edit', 'del')),
						'package'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'promotion'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'holiday'	=>	''
					),
	'operation'	=>	array(
			    			'page'			=>	array(
												'menu'		=>	array('category', 'page'),
												'permit'	=>	array(
																	'category' => array('add', 'edit', 'del'),
																	'page' => array('add', 'edit', 'del')
																)
											),
						'billing'		=>	'',
						'email'			=>	array(
												'menu'		=>	array('send', 'email_logs'),
												'permit'	=>	array(
																	'send' => '',
																	'email_logs' => ''
																)
											),
						'partner'		=>	array('permit'	=>	array('add', 'edit', 'del')),
						'arrival'		=>	array('permit'	=>	array('del')),
						// 'newsletter'	=>	array('permit'	=>	array('edit', 'del')),
						'chat'			=>	array(
												'menu'		=>	array('set', 'chat'),
												'permit'	=>	array(
																	'chat' => array('add', 'edit', 'del')
																)
											),
						'googlefeed'	=>	'',
						'google_verification'	=>	'',
						'gallery'		=>	'',
						'distribution'	=>	'',
						'facebook_store'=>	'',
						'blog'			=>	array(
											'menu'		=>	array('blog', 'set', 'category', 'review'),
											'permit'	=>	array(
																'blog' => array('add', 'edit', 'del'),
																'category' => array('add', 'edit', 'del'),
																'review' => array('edit', 'del')
															)
											),
						'translate'		=>	'',
						'swap_chain'	=>	'',
						'paypal_dispute'=>	''
					),
	'mta'		=>	array(
						'visits'			=>	'',
						'visits_conversion'	=>	'',
						'orders'			=>	'',
						'orders_repurchase'	=>	'',
						'products_sales'	=>	'',
						'user'				=>	'',
                        'search_logs'		=>	'',
                        'yyorders'		    =>	'',
                        'yykehu'            =>  '',
                        'yytranslation'     =>  '',
                        'yyshangjia'        =>  '',
                        'yyproduct'         =>  ''
					),
	'plugins'	=>	array(
						'app'		=>	'',
						'my_app'	=>	'',
					),
	'view'		=>	array(
						'visual'		=>	array('permit'	=>	array('edit')),
						'nav'			=>	'',
						'footer_nav'	=>	'',
					),
	'set'		=>	array(
						//'test'		=>	'', 基本样式的样例文件
						'config'	=>	'',
						'domain_binding'	=>	'',
						// 'themes'	=>	array(
						// 					'menu'		=>	array('index_set', 'nav', 'footer_nav', 'themes', 'advanced'),//, 'products_list', 'products_detail'  , 'style'
						// 					'permit'	=>	array(
						// 											'nav' => array('add', 'edit', 'del'),
						// 											'footer_nav' => array('add', 'edit', 'del')
						// 									)
						// 				),
						'shipping'	=>	array(
											'menu'		=>	array('express', 'insurance', 'overseas'),
											'permit'	=>	array(
																'express' => array('add', 'edit', 'del')
															)
										),
						'payment'	=>	array('permit'	=>	array('edit')),
						'exchange'	=>	array('permit'	=>	array('add', 'edit', 'del')),
                        'newexchange'	=>	array('permit'	=>	array('add', 'edit', 'del')),
                        'newexchangelist'		=>	'',
						'photo'		=>	array(
											'menu'		=>	array('category', 'photo'),
											'permit'	=>	array(
																'category' => array('add', 'edit', 'del'),
																'photo' => array('add', 'edit', 'del')
															)
										),
						'country'	=>	array('permit'	=>	array('add', 'edit', 'del')),
						'seo'		=>	'',
						'third'		=>	array('permit'	=>	array('add', 'edit', 'del')),
						'manage'	=>	array('permit'	=>	array(/*'add', */'edit'/*, 'del'*/)),
						'manage_logs'=>	'',
					),
	'mobile'	=>	array(
						'index_set'	=>	'',
						'themes'	=>	''
					),
);

//版本权限
$un_menu_ary=array(
	0=>array(array('account'), array('orders.import_delivery', 'products.business', 'products.sync', 'operation.email', 'operation.gallery', 'operation.facebook_store', 'operation.facebook_ads_extension', 'operation.googlefeed', 'operation.review', 'sales.seckill', 'sales.tuan', 'sales.package', 'sales.promotion', 'sales.holiday', 'sales.coupon', 'sales.discount')),//标准版
	1=>array(array('account'), array('operation.gallery', 'operation.facebook_store', 'operation.facebook_ads_extension', 'operation.googlefeed', 'operation.review', 'sales.seckill', 'sales.tuan', 'sales.holiday')),//高级版
	2=>array(array('account'), array()),//专业版
	10=>array(array('account'), array('sales.coupon', 'sales.discount', 'sales.seckill', 'sales.tuan', 'sales.promotion', 'sales.holiday', 'sales.sales', 'user.message', 'user.inbox')),//创店免费版
	11=>array(array('account'), array()),//创店付费版
	100=>array(array('account'), array('set.themes', 'sales.holiday'))//定制版
);
//!$c['FunVersion'] && $c['NewFunVersion']==2 && $un_menu_ary[0][1]=array_merge($un_menu_ary[0][1], array('products.upload', 'products.upload_new'));
$c['FunVersion']<2 && $c['NewFunVersion']>2 && $un_menu_ary[1][1]=array_merge($un_menu_ary[1][1], array('products.sync'));
//$c['FunVersion']==1 && $c['NewFunVersion']>4 && $un_menu_ary[1][1]=array_merge($un_menu_ary[1][1], array('orders.import_delivery', 'products.sync', 'sales.package', 'sales.promotion'));
foreach($un_menu_ary[$c['FunVersion']] as $k=>$v){
	foreach($v as $k2=>$v2){
		$v2=explode('.', $v2);
		if(count($v2)>1){
			unset($system_module[$v2[0]][$v2[1]]);
		}else{
			unset($system_module[$v2[0]]);
		}
	}
}
//应用权限
$app_module_ary=array(
	'seckill'=>'sales',
	'tuan'=>'sales',
	'package'=>'sales',
	'promotion'=>'sales',
	'holiday'=>'sales',
	'distribution'=>'operation',
	'googlefeed'=>'operation',
	'gallery'=>'operation',
	'swap_chain'=>'operation',
	'google_verification'=>'operation',
	'facebook_store'=>'operation',
	'facebook_ads_extension'=>'operation',
	'blog'=>'operation',
	'business'=>'products',
	'import_delivery'=>'orders',
	'paypal_dispute'=>'operation',
	'dhl_account_open'=>'plugins',
	'custom_comments'=>'plugins',
	'dhl_account_info'=>'plugins',
	'asiafly'=>'plugins',
);
$isSync=0;
$app_row=db::get_all('plugins', 'Category="app" and (IsUsed=0 or IsInstall=0)');
foreach($app_row as $k=>$v){
	$ClassName=$v['ClassName'];
	unset($system_module[$app_module_ary[$ClassName]][$ClassName]);
	if($ClassName=='aliexpress' || $ClassName=='amazon' || $ClassName=='wish' || $ClassName=='shopify'){//数据同步权限
		$isSync+=1;
	}
}
if($isSync==4){//数据同步没有任何接口权限，整个管理系统自动关闭
	unset($system_module['products']['sync']);
}

$_SESSION['Manage']['Permit']['set'][1]['manage'][0]=1; //默认“管理员”都有权限打开

return $system_module;