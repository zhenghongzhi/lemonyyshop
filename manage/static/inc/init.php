<?php !isset($c) && exit();?>
<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

$config_row=db::get_all('config', "GroupId='global'");//网站基本设置
foreach($config_row as $v){
	if(in_array($v['Variable'], array('OrdersSmsStatus', 'SearchTips', 'CopyRight', 'CloseWeb', 'Notice', 'ArrivalInfo', 'IndexContent', 'HeaderContent', 'TopMenu', 'ContactMenu', 'ShareMenu'))){
		$c['manage']['config'][$v['Variable']]=str::json_data(htmlspecialchars_decode($v['Value']), 'decode');
	}elseif($v['Variable']=='Language'){
		$c['manage']['config'][$v['Variable']]=explode(',', $v['Value']);
	}else{
		$c['manage']['config'][$v['Variable']]=$v['Value'];
	}
}
if($c['FunVersion']==100) $c['manage']['config']['Language']=array('en');
$c['manage']['lang_pack']=@include("static/lang/{$c['manage']['config']['ManageLanguage']}.php");//后台语言包
$c['manage']['cache_timeout']=3600*24;//清除后台缓存文件间隔(s)
if(!is_array($_SESSION['Manage']) || !$_SESSION['Manage']['UserName']){//未登录
	if($_POST['do_action'] && in_array($_POST['do_action'],array('account.check_login_status','account.expire_login','account.login'))){
		include('static/do_action/account.php');
		$do_action=explode('.',$_POST['do_action']);
		eval("account_module::{$do_action[1]}();");
		exit;
	}else{
		$c['manage']['module']='account';
		$c['manage']['action']='login';
	}
}else{
	$c['cache_timeout']=3600*2;//更新接口缓存文件间隔(s)
	($c['FunVersion']<2 || $c['FunVersion']==100) && $c['lang_name'] = array($c['manage']['config']['LanguageDefault']=>$c['lang_name'][$c['manage']['config']['LanguageDefault']]);
	if($c['FunVersion']>=10 && (!$_SESSION['u_file_check']['size'] || ($c['time'] - $_SESSION['u_file_check']['check_time']) > 1800)){
		$total_ufile_ary = file::getDirectorySize();
		$_SESSION['u_file_check']['size'] = $total_ufile_ary['size']?round($total_ufile_ary['size']/(1024*1024),1):0;
		$_SESSION['u_file_check']['check_time'] = $c['time'];
	}
	$c['manage']=array_merge($c['manage'], array(
			'module'			=>	isset($_POST['m'])?$_POST['m']:$_GET['m'],
			'action'			=>	isset($_POST['a'])?$_POST['a']:$_GET['a'],
			'do'				=>	isset($_POST['d'])?$_POST['d']:$_GET['d'],
			'page'				=>	isset($_POST['p'])?$_POST['p']:$_GET['p'],
			'iframe'			=>	(int)$_GET['iframe'],
			'upload_dir'		=>	'/u_file/'.date('ym/'),   //网站所有上传的文件保存的基本目录
			'web_lang'			=>	'_'.$c['manage']['config']['LanguageDefault'],	//网站的默认语言版本
			'web_lang_list'		=>	array_keys($c['lang_name']),	//网站可用的语言列表
			'email_tpl_dir'		=>	'/static/email_tpl/',	//邮件模板存放目录
			'web_themes'		=>	db::get_value('config_module', 'IsDefault=1', 'Themes'),	//网站当前风格
			'currency_symbol'	=>	db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Symbol'),	//后台默认的币种符号
			'manage_lang_list'	=>	array('zh-cn', 'en'),	//后台可用的语言列表
			'field_ext'			=>	array('VARCHAR(50)', 'VARCHAR(150)', 'VARCHAR(255)', 'TEXT'), //数据库添加字段参数
			'my_order'			=>	array('{/global.default/}', '999'=>'{/global.last/}'),
			'permit'			=>	include('static/inc/permit.php'),
			'permit_base'		=>	array('account'),	//基本权限
			'shipping_method'	=>	array('DHL', 'EMS', 'UPS', 'TNT', 'FedEx', 'DPEX', 1000=>'其他'),
			'blog_set'			=>	array('Title', 'Brief', 'nav'),
			'sync_ary'			=>	array(//数据同步状态
										'aliexpress'	=>	array('onSelling', 'offline', 'auditing', 'editingRequired'),
										'amazon'		=>	array(//设置链接	MarketplaceId
																'US'	=>	array('https://developer.amazonservices.com/', 'https://www.amazon.com/', 'ATVPDKIKX0DER'),
																'CA'	=>	array('https://developer.amazonservices.ca/', 'https://www.amazon.ca/', 'A2EUQ1WTGCTBG2'),
																'MX'	=>	array('https://developer.amazonservices.com.mx/', 'https://www.amazon.com.mx/', 'A1AM78C64UM0Y8'),
																//'BR'	=>	array('', 'https://www.amazon.com.br/', ''),
																
																'DE'	=>	array('https://developer.amazonservices.de/', 'https://www.amazon.de/', 'A1PA6795UKMFR9'),
																'ES'	=>	array('https://developer.amazonservices.es/', 'https://www.amazon.es/', 'A1RKKUPIHCS9HS'),
																'FR'	=>	array('https://developer.amazonservices.fr/', 'https://www.amazon.fr/', 'A13V1IB3VIYZZH'),
																'IT'	=>	array('https://developer.amazonservices.it/', 'https://www.amazon.it/', 'APJ6JRA9NG5V4'),
																'UK'	=>	array('https://developer.amazonservices.co.uk/', 'https://www.amazon.co.uk/', 'A1F83G8C2ARO7P'),
																//'NL'	=>	array('', 'https://www.amazon.nl/', ''),
																
																'IN'	=>	array('https://developer.amazonservices.in/', 'https://www.amazon.in/', 'A21TJRUUN4KGV'),
																'JP'	=>	array('https://developer.amazonservices.jp/', 'https://www.amazon.co.jp/', 'A1VC38T7YXB528'),
																'CN'	=>	array('https://developer.amazonservices.com.cn/', 'https://www.amazon.cn/', 'AAHKV2X7AFYLW')
															),
										'wish'			=>	array(),
										'shopify'		=>	array()
									),
			//'explode_number'	=>	1000,//产品导出每页的个数
			'warning_stock'		=>	5,//产品警告库存数量上限
			'resize_ary'		=>	array(	//各系统的缩略图尺寸
										'products'	=>	array('default', '640x640', '500x500', '240x240'),
										'photo'		=>	array('120x120')
									),
			'sub_save_dir'		=>	array(	//各系统的缩略图存放位置
										'products'	=>	'products/'
									),
			'photo_type'		=>	array(	//图片银行基本系统图片类型
										0	=>	'other',
										1	=>	'products',
										2	=>	'editor'
									),
			'user_reg_field'	=>	array(	//会员注册事项，请勿改动 1:可改 0:固定
										'Email'		=>	0,//邮箱
										'Name'		=>	1,//姓名
										'Gender'	=>	1,//性别
										'Age'		=>	1,//年龄
										'NickName'	=>	1,//昵称
										'Country'	=>	1,//国家
										'Telephone'	=>	1,//电话
										'Fax'		=>	1,//传真
										'Birthday'	=>	1,//生日
										'Facebook'	=>	1,//Facebook
										'Company'	=>	1,//公司
										'Code'		=>	1//验证码
									),
			'mod_order_status'	=>	array(	//订单状态可变更状态，如：Awaiting Payment=>(Awaiting Confirm Payment, Payment Wrong, Awaiting Shipping)
										1	=>	array(/*2,*/2),
										2	=>	array(3),
										3	=>	array(4),
										4	=>	array(5),
										5	=>	array(6),
			    6	=>	array(7),
			    7	=>	array(8)
			    
									),
			'email_notice'		=>	array('order_payment', 'order_shipped', 'order_change', 'order_cancel', 'create_account', 'forgot_password', 'order_create', 'validate_mail'), //邮件通知
			'table_lang_field'	=>	array(	//config	config_module	mobile_config	
										'article'						=>	array('Title'=>1, 'SeoTitle'=>2, 'SeoKeyword'=>2, 'SeoDescription'=>2),
										'article_category'				=>	array('Category'=>1),//, 'SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2
										'article_content'				=>	array('Content'=>3),
										'info'							=>	array('Title'=>1, 'BriefDescription'=>2, 'SeoTitle'=>2, 'SeoKeyword'=>2, 'SeoDescription'=>2),
										'info_category'					=>	array('Category'=>1),//, 'SeoTitle', 'SeoKeyword', 'SeoDescription'
										'info_content'					=>	array('Content'=>3),
										'link'							=>	array('Keyword'=>1),
										'meta'							=>	array('SeoTitle'=>2, 'SeoKeyword'=>2, 'SeoDescription'=>2),
										'partners'						=>	array('Name'=>1),
										'payment'						=>	array('Name'=>0, 'Description'=>3),
										'products'						=>	array('Name'=>1, 'BriefDescription'=>2),
										'products_attribute'			=>	array('Name'=>0, 'Description'=>3, 'Value'=>3),
										'products_attribute_value'		=>	array('Value'=>2),
										'products_selected_attribute'	=>	array('Value'=>2),
										'products_category'				=>	array('Category'=>1, 'BriefDescription'=>2, 'SeoTitle'=>2, 'SeoKeyword'=>2, 'SeoDescription'=>2),
										'products_category_description'	=>	array('Description'=>3),
										'products_description'			=>	array('Description'=>3),
										'products_seo'					=>	array('SeoTitle'=>2, 'SeoKeyword'=>2, 'SeoDescription'=>2),
										'products_tab'					=>	array('Name'=>1, 'Description'=>3),
										'products_tags'					=>	array('Name'=>1),
										'shipping_overseas'				=>	array('Name'=>1),
										'user_level'					=>	array('Name'=>0),
										'user_reg_set'					=>	array('Name'=>0, 'Option'=>3)
									),
			'where'				=>	array(
										'products'	=>	" and (((SoldStatus!=2 and Stock>=0) or (SoldStatus=2 and Stock>0)) and (SoldOut=0 or SoldOut=2 or (SoldOut=1 and IsSoldOut=1 and {$c['time']} between SStartTime and SEndTime)))",
									),
			'cdx_path'			=>	$c['FunVersion']>=10?'cdx/':'',
			'cdx_limit'			=>	$c['FunVersion']==10?'cdx_limit':'', //fun_limit_class
		)
	);
	
	// $c['manage']['my_order']=range(1, 100);
	for($i=1; $i<101; ++$i){//排序数量增加到100
		if($i==1) $c['manage']['my_order'][$i]=$i.'(最前)';
		else $c['manage']['my_order'][$i]=$i;
	}
	!$c['manage']['web_themes'] && $c['manage']['web_themes']='default';
	if(@is_file("{$c['root_path']}/static/themes/{$c['manage']['web_themes']}/inc/themes_set.php")){//载入网站当前风格配置文件
		include("{$c['root_path']}/static/themes/{$c['manage']['web_themes']}/inc/themes_set.php");
	}
	$c['old_coupon_acctime']=(int)db::get_value('config', 'GroupId="delete_data" and Variable="old_coupon_acctime"', 'Value');//小于这个时间创建的优惠券是旧版优惠券
	/************************************** 应用App Start **************************************/
	$c['plugin_app']=new plugin('app');//插件类(app插件)
	$c['manage']['plugins']=array(
		'map'		=>	array(//放置在/plugins/app里面的文件所归属的位置
							'operation' => array('gallery', 'distribution', 'facebook_store', 'googlefeed', 'swap_chain', 'google_verification', 'paypal_dispute'),
							'plugins'	=>	array('dhl_account_open', 'dhl_account_info', 'custom_comments', 'asiafly'),
						),
		'module'	=>	array(//用作URL链接
							'orders'	=> array('import_delivery'),
							'products'	=> array('business', 'aliexpress', 'amazon', 'wish', 'shopify'),
							'sales'		=> array('seckill', 'tuan', 'package', 'promotion', 'holiday'),
							'operation' => array('gallery', 'distribution', 'facebook_store', 'blog', 'googlefeed', 'google_verification', 'swap_chain', 'paypal_dispute'),
							'plugins'	=>	array('dhl_account_open', 'custom_comments'),
						),
		'category'	=>	array(//应用分类
							'sales'			=> array('seckill', 'package', 'promotion', 'holiday', 'tuan', 'wholesale'),
							'marketing'		=> array('gallery', 'review', 'distribution', 'facebook_store', 'facebook_ads_extension', 'massive_email', 'platform', 'paypal_dispute', 'custom_comments'), //, 'batch_edit'
							'google'		=> array('googlefeed', 'google_verification', 'google_login', 'google_pixel'),
							'facebook'		=> array('facebook_store', 'facebook_ads_extension', 'facebook_login', 'facebook_pixel', 'facebook_messenger'),
							'login'			=> array('facebook_login', 'google_login', 'paypal_login', 'vk_login', 'twitter_login'),
							'delivery'		=> array('import_delivery', 'overseas', 'freight'/*, 'dhl_account_open', 'dhl_account_info', 'asiafly'*/),
							'products_rela'	=> array('aliexpress', 'amazon', 'wish', 'shopify', 'upload', 'screening', 'product_inbox', 'business', 'pdf', 'intelligent_translation', 'custom_comments'),
							'others'		=> array('blog', 'block_access', 'advanced_setup', 'swap_chain'),
							'statistics'	=> array('facebook_pixel', 'google_verification', 'google_pixel', 'paypal_marketing_solution'),
						)
	);
	$c['manage']['is_plugins']=is_array($c['manage']['plugins']['map'][$c['manage']['module']]) && in_array($c['manage']['action'], $c['manage']['plugins']['map'][$c['manage']['module']])?1:0;//判断是否是插件

	$c['manage']['plugins']['Used']=array();
	$app_used_row=db::get_all('plugins', 'Category="app" and IsUsed=1 and IsInstall=1');//已使用的APP
	foreach($app_used_row as $k=>$v){
		if(!in_array($v['ClassName'], $c['un_used_ary'][$c['FunVersion']])){
			$c['manage']['plugins']['Used'][]=$v['ClassName'];
		}
	}
	/************************************** 应用App End **************************************/
	//后台汇率
	$_SESSION['Manage']['Currency']=db::get_one('currency', 'IsUsed=1 and ManageDefault=1');
	
	//发货地
	$overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
	foreach($overseas_row as $v){
		if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 && $v['OvId']!=1) continue; //关闭海外仓功能，不是China的都踢走
		$v['Name'.$c['lang']]=='' && $v['Name'.$c['lang']]=$v['Name_en'];
		$c['config']['Overseas'][$v['OvId']]=$v;
	}
	
	//干活....
	$do_action=isset($_POST['do_action'])?$_POST['do_action']:$_GET['do_action'];
	$_GET['do_action']=='action.file_upload_plugin' && $do_action=$_GET['do_action'];// 通过文件上传进来的直接用get
	if($do_action){
		$_=explode('.', $do_action);
		$do_action_file="static/do_action/{$_[0]}.php";
		if(@is_file($do_action_file)){
			include($do_action_file);
			if(method_exists($_[0].'_module', $_[1])){
				eval("{$_[0]}_module::{$_[1]}();");
				exit;
			}
		}
	}
	
	!$c['manage']['module'] && $c['manage']['module']='account';
	!@is_dir($c['manage']['module']) && js::location('./');
	!@is_file("{$c['manage']['module']}/{$c['manage']['action']}.php") && !$c['manage']['is_plugins'] && $c['manage']['action']='index';
	!$c['manage']['do'] && $c['manage']['do']='index';
	!$c['manage']['page'] && $c['manage']['page']='index';
	($c['manage']['module']!='set' && $c['manage']['action']!='photo') && manage::check_permit($c['manage']['module'], 1);	//权限检测
}
?>