<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class orders_module{
    public static function inbox_view(){//加载产品消息详情
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        //初始化
        $p_MId		= (int)$p_MId;
        $p_UserId	= (int)$p_UserId;
        
//         $p_UserId=2;
        $ReturanData= array();
        $uesr_row	= str::str_code(db::get_one('purchase_request', "id='$p_UserId'"));//获取会员信息
        $row_message=db::get_one("request_message","UserId='$p_UserId' and SubJect='$p_OId' and Module='purchase'");
        if (!$row_message)
        {
            $data=array(
                'UserId'=>$p_UserId,
                'Module'=>"purchase",
                'IsRead'=>1,
                'SubJect'=>$p_OId,
                'AccTime'	=>	$c['time']
            );
            db::insert('request_message', $data);
        }
        $row_message=db::get_one("request_message","UserId='$p_UserId' and SubJect='$p_OId' and Module='purchase'");
        
        $ReturanData['Email']=$uesr_row['Email'];
//             //收件信息
            $MId_where='';
            $MIdAry=array();
            $MIdAry[]=$p_MId;
//             $where="Module='others' and UserId='$p_UserId'";
//             $msg_row=str::str_code(db::get_all('request_message', $where));
//             foreach($msg_row as $v){
//                 $MIdAry[]=$v['MId'];
//             }
            count($MIdAry)>0 && $MId_where=implode(',', $MIdAry);
            $reply_row=str::str_code(db::get_all('request_message_reply', "MId in ($MId_where)", '*', 'RId asc'));
            db::update('request_message', "MId in ($MId_where)", array('IsRead'=>1));
            
            $ReturanData['PicPath']='/static/manage/images/user/'.$c['manage']['cdx_path'].'bg_message.png';
            $ReturanData['Name']=$c['manage']['lang_pack']['global']['message'];
            $ReturanData['Url']='';
            
            foreach($reply_row as $k=>$v){
                $ReturanData['Reply'][$v['AccTime']]=array(
                    'MId'		=>	$v['MId'],
                    'UserId'	=>	($v['Type']==0?$v['UserId']:0),
                    'CusEmail'  =>  $v['CusEmail'],
                    'Type'		=>	$v['Type'], //0:追问 1:回答
                    'Content'	=>	$v['Content'],
                    'PicPath'	=>	$v['PicPath'],
                    'Time'		=>	date('Y-m-d H:i:s', $v['AccTime'])
                );
            }
            foreach($reply_row as $k=>$v){
                $ReturanData['Reply'][$v['AccTime']]=array(
                    'MId'		=>	$v['MId'],
                    'UserId'	=>	$v['UserId'],
                    'CusEmail'  =>  $v['CusEmail'],
                    'Type'		=>	($v['UserId']==0?1:0), //回答
                    'Content'	=>	$v['Content'],
                    'PicPath'	=>	$v['PicPath'],
                    'Time'		=>	date('Y-m-d H:i:s', $v['AccTime'])
                );
            }
            
            $last_ary=@end($ReturanData['Reply']);
            $ReturanData['MId']=$last_ary['MId'];
            if (!$ReturanData['MId'])
            {
                $ReturanData['MId']=$row_message["MId"];
            }
        ly200::e_json($ReturanData, 1);
    }
    public static function inbox_reply(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_MId=(int)$p_MId;
        if($p_MId>0){
            $data=array(
                'MId'		=>	$p_MId,
                'Content'	=>	$p_Message,
                'PicPath'	=>	$p_MsgPicPath,
                'AccTime'	=>	$c['time']
            );
            db::insert('request_message_reply', $data);
            db::update('request_message', "MId='$p_MId'", array('IsReply'=>1));
//             manage::operation_log('回复站内信');
            $data['Time']=date('Y-m-d H:i:s', $data['AccTime']);
            ly200::e_json($data, 1);
        }else{
            ly200::e_json('', 0);
        }
    }
	public static function orders_del(){//删除订单
		global $c;
		$OrderId=(int)$_GET['OrderId'];
		$orders_row=db::get_one('orders', "OrderId='$OrderId'");
		manage::auto_add_recycle_column('orders', 'orders_recycle'); //自动添加订单表新添加的字段
		foreach((array)$orders_row as $k=>$v){
			$orders_row[$k]=addslashes($v);
		}
		$orders_row['DelTime']=$c['time'];
		db::insert('orders_recycle', $orders_row);
		$new_orderid=db::get_insert_id();
		if($new_orderid){
			db::delete('orders', "OrderId={$OrderId}");
		}
		$lang=$c['manage']['lang_pack'];
		manage::operation_log($lang['global']['del'].$lang['orders']['orders'].':'.$orders_row['OId']);
		ly200::e_json('', 1);
	}
	public static function orders_request_del(){//删除采购
	    global $c;
	    $OrderId=(int)$_GET['OrderId'];
	   $row= db::get_one('purchase_request', "id={$OrderId}");
	   $OId=$row["OId"];
	  
	   $r_m=db::get_one("request_message","Subject='$OId'");
	   $MId=$r_m["MId"];
	   db::delete("request_message_reply","MId='$MId'");
	   db::delete("request_message","Subject='$OId'");
	   db::delete('purchase_request', "id={$OrderId}");
	   
	    $lang=$c['manage']['lang_pack'];
	    ly200::e_json('', 1);
	}
	public static function orders_del_bat(){//批量删除订单
		global $c;
		$del_bat_id=str_replace('-', ',', $_GET['id']);
		$del_bat_id=='' && ly200::e_json('');
		$orders_row=db::get_all('orders', "OrderId in($del_bat_id)");
		manage::auto_add_recycle_column('orders', 'orders_recycle'); //自动添加订单表新添加的字段
		foreach((array)$orders_row as $v){
			foreach((array)$v as $k1=>$v1){
				$v[$k1]=addslashes($v1);
			}
			$v['DelTime']=$c['time'];
			db::insert('orders_recycle', $v);
			$new_orderid=db::get_insert_id();
			if($new_orderid){
				db::delete('orders', "OrderId={$new_orderid}");
			}
		}
		$lang=$c['manage']['lang_pack'];
		manage::operation_log($lang['global']['del_bat'].$lang['orders']['orders']);
		ly200::e_json('', 1);
	}
	public static function orders_request_del_bat(){//批量删除采购需求
	    global $c;
	    $del_bat_id=str_replace('-', ',', $_GET['id']);
	    $del_bat_id=='' && ly200::e_json('');
	    
	    $rows= db::get_all('purchase_request', "id in ($del_bat_id)");
	    foreach ($rows as $row)
	    {
	        $OId=$row["OId"];
	        $r_m=db::get_one("request_message","Subject='$OId'");
	        $MId=$r_m["MId"];
	        db::delete("request_message_reply","MId='$MId'");
	        db::delete("request_message","Subject='$OId'");
	    }
	    db::delete('purchase_request', "id in ($del_bat_id)");
	    ly200::e_json('', 1);
	}
	
	public static function orders_edit_sales(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_SalesId=(int)$p_SalesId;
		$result='';
		if($p_SalesId>0 && (float)db::get_value('orders', "OrderId='{$p_OrderId}'", 'SalesId')!=$p_SalesId){
			db::update('orders', "OrderId='{$p_OrderId}'", array('SalesId'=>$p_SalesId, 'UpdateTime'=>$c['time']));
			manage::operation_log('订单修改业务员');
		}
		$result=db::get_value('manage_sales', "SalesId='{$p_SalesId}'", 'UserName');
		ly200::e_json($result, 1);
	}
	
	public static function get_shipping_address(){
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId=(int)$p_OrderId;
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$p_OrderId'"));
		$address_ary=array(
			'FirstName'		=>	$orders_row['ShippingFirstName'],
			'LastName'		=>	$orders_row['ShippingLastName'],
			'AddressLine1'	=>	$orders_row['ShippingAddressLine1'],
			'AddressLine2'	=>	$orders_row['ShippingAddressLine2'],
			'CountryCode'	=>	$orders_row['ShippingCountryCode'],
			'PhoneNumber'	=>	$orders_row['ShippingPhoneNumber'],
			'City'			=>	$orders_row['ShippingCity'],
			'SId'			=>	$orders_row['ShippingSId'],
			'State'			=>	$orders_row['ShippingState'],
			'Country'		=>	$orders_row['ShippingCountry'],
			'CId'			=>	$orders_row['ShippingCId'],
			'CodeOption'	=>	$orders_row['ShippingCodeOption'],
			'TaxCode'		=>	$orders_row['ShippingTaxCode'],
			'ZipCode'		=>	$orders_row['ShippingZipCode'],
		);
		ly200::e_json($address_ary, 1);
	}
	
	public static function select_country(){
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CId=(int)$p_CId;
		$country_row=str::str_code(db::get_one('country', "CId='{$p_CId}'"));
		$CountryCode=$country_row['Code'];
		if($country_row['HasState']==1){
			$state_row=str::str_code(db::get_all('country_states', "CId='{$p_CId}'", '*', 'States asc'));
			$data=$state_row;
		}else{
			$data=-1;
		}
		unset($country_row, $state_row);
		ly200::e_json(array('cid'=>$p_CId, 'code'=>$CountryCode, 'contents'=>$data), 1);
	}
	
	public static function orders_mod_address(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId		= (int)$p_OrderId;
		$p_country_id	= (int)$p_country_id;
		$p_Province		= (int)$p_Province;
		$order_row		= db::get_one('orders', "OrderId='$p_OrderId'");
		!$order_row && ly200::e_json('', 0);
		
		$v['CodeOption']=(int)$p_tax_code_type;
		$v['TaxCode']=$p_tax_code_value;
		$tax_ary=user::get_tax_info($v);
		$data=array(
			'ShippingFirstName'		=>	$p_FirstName,
			'ShippingLastName'		=>	$p_LastName,
			'ShippingAddressLine1'	=>	$p_AddressLine1,
			'ShippingAddressLine2'	=>	$p_AddressLine2,
			'ShippingCountryCode'	=>	$p_CountryCode,
			'ShippingPhoneNumber'	=>	$p_PhoneNumber,
			'ShippingCity'			=>	$p_City,
			'ShippingState'			=>	$p_State?$p_State:db::get_value('country_states', "SId='$p_Province' and CId='$p_country_id'", 'States'),
			'ShippingSId'			=>	$p_Province,
			'ShippingCountry'		=>	db::get_value('country', "CId='$p_country_id'", 'Country'),
			'ShippingCId'			=>	$p_country_id,
			'ShippingZipCode'		=>	$p_ZipCode,
			'ShippingCodeOption'	=>	$tax_ary['CodeOption'],
			'ShippingCodeOptionId'	=>	$tax_ary['CodeOptionId'],
			'ShippingTaxCode'		=>	$tax_ary['TaxCode'],
			'UpdateTime'			=>	$c['time']
		);
		db::update('orders', "OrderId='$p_OrderId'", $data);
		
		$info=array(
			'name'		=>	$data['ShippingFirstName'].' '.$data['ShippingLastName'],
			'address'	=>	$data['ShippingAddressLine1'].($data['ShippingAddressLine2']?', '.$data['ShippingAddressLine2']:'').'<br />'.$data['ShippingCity'].($data['ShippingState']?', '.$data['ShippingState']:'').', '.$data['ShippingCountry'].(($data['ShippingCodeOption']&&$data['ShippingTaxCode'])?'#'.$data['ShippingCodeOption'].': '.$data['ShippingTaxCode']:''),
			'zipcode'	=>	$data['ShippingZipCode'],
			'phone'		=>	$data['ShippingCountryCode'].'-'.$data['ShippingPhoneNumber'],
		);
		$returnData=array(
			'OrderId'		=>	$p_OrderId,
			'FirstName'		=>	$data['ShippingFirstName'],
			'LastName'		=>	$data['ShippingLastName'],
			'AddressLine1'	=>	$data['ShippingAddressLine1'],
			'AddressLine2'	=>	$data['ShippingAddressLine2'],
			'City'			=>	$data['ShippingCity'],
			'State'			=>	$data['ShippingState'],
			'SId'			=>	$data['ShippingSId'],
			'Country'		=>	$data['ShippingCountry'],
			'CId'			=>	$data['ShippingCId'],
			'ZipCode'		=>	$data['ShippingZipCode'],
			'CodeOption'	=>	$data['ShippingCodeOption'],
			'CodeOptionId'	=>	$data['ShippingCodeOptionId'],
			'TaxCode'		=>	$data['ShippingTaxCode'],
			'CountryCode'	=>	$data['ShippingCountryCode'],
			'PhoneNumber'	=>	$data['ShippingPhoneNumber'],
		);
		$json_data=str::json_data(str::str_code($returnData, 'stripslashes'));
		
		unset($order_row, $tax_ary, $data, $returnData);
		ly200::e_json(array('info'=>$info, 'text'=>$json_data), 1);
	}
	
	public static function orders_mod_paypal_address(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId=(int)$p_OrderId;
		$p_IsUsed=(int)$p_IsUsed;
		$row=db::get_one('orders_paypal_address_book', "OrderId='$p_OrderId'");
		if($row){
			db::update('orders_paypal_address_book', "OrderId='$p_OrderId'", array('IsUse'=>$p_IsUsed));
		}
		$msg=($p_IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}
	
	public static function orders_mod_info(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId	= (int)$p_OrderId;
		$order_row	= db::get_one('orders', "OrderId='$p_OrderId'");
		if(!$order_row){
			ly200::e_json($c['manage']['lang_pack']['msg']['edit_error'], 0);
		}
		!$order_row['ShippingInsurance'] && $p_ShippingInsurancePrice=0;
		$p_Discount=$p_Discount?(100-$p_Discount):0;
		$p_UserDiscount=$p_UserDiscount?$p_UserDiscount:0;
		$p_CouponDiscount=$p_CouponDiscount?(100-$p_CouponDiscount)/100:0;
		$data=array(
			'Discount'					=>	(float)$p_Discount,
			'DiscountPrice'				=>	(float)$p_DiscountPrice,
			'UserDiscount'				=>	(float)$p_UserDiscount,
			'CouponDiscount'			=>	(float)$p_CouponDiscount,
			'CouponPrice'				=>	(float)$p_CouponPrice,
			'ShippingPrice'				=>	(float)$p_ShippingPrice,
			'ShippingInsurancePrice'	=>	(float)$p_ShippingInsurancePrice,
			'PayAdditionalFee'			=>	(float)$p_PayAdditionalFee,
			'PayAdditionalAffix'		=>	(float)$p_PayAdditionalAffix,
			'UpdateTime'				=>	$c['time']
		);
		db::update('orders', "OrderId='$p_OrderId'", $data);
		$data['ProductPrice']=(float)$order_row['ProductPrice'];
		$total_price=sprintf('%01.2f', orders::orders_price($data, 1, 1));
		$data['OrderId']=$p_OrderId;
		$data['HandingFee']=$total_price-orders::orders_price($data, 0, 1);
		$data['TotalAmount']=$total_price;
		$p_UserDiscount || $data['UserDiscount']=100-(float)$p_UserDiscount;
		manage::operation_log('修改订单价格:'.$order_row['OId']);
		//下单所相应的货币价格
		$web_price_ary=array();
		if($order_row['Currency']!=$order_row['ManageCurrency']){
			$currency_row=db::get_one('currency', "Currency='{$order_row['Currency']}'", 'Symbol, Rate');
			$web_symbol=$currency_row['Symbol'];
			$Rate=($order_row['Rate']?$order_row['Rate']:$currency_row['Rate']);
			$web_price_ary=array(
				'TotalPrice'			=> manage::rate_price($total_price, 0, $order_row['Currency'], $web_symbol, $Rate),
				'ProductPrice'			=> manage::rate_price($data['ProductPrice'], 0, $order_row['Currency'], $web_symbol, $Rate),
				'ShippingPrice'			=> manage::rate_price($data['ShippingPrice'], 0, $order_row['Currency'], $web_symbol, $Rate),
				'ShippingInsurancePrice'=> manage::rate_price($data['ShippingInsurancePrice'], 0, $order_row['Currency'], $web_symbol, $Rate),
				'DiscountPrice'			=> manage::rate_price($data['DiscountPrice'], 0, $order_row['Currency'], $web_symbol, $Rate),
				'HandingFee'			=> manage::rate_price($data['HandingFee'], 0, $order_row['Currency'], $web_symbol, $Rate),
				'CouponPrice'			=> manage::rate_price($data['CouponPrice'], 0, $order_row['Currency'], $web_symbol, $Rate),
			);
		}
		ly200::e_json(array('tips'=>$c['manage']['lang_pack']['msg']['edit_success'], 'info'=>$data, 'web'=>$web_price_ary), 1);
	}
	
	public static function orders_shipping_method(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_OrderId	= (int)$p_OrderId;
		$p_ProId	= (int)$p_ProId;
		$orders_row	= db::get_one('orders', "OrderId='$p_OrderId'");
		!$orders_row && ly200::e_json('', -1);
		$shipping_template=(int)$orders_row['shipping_template'];
		//发货地
		if($orders_row['ShippingExpress']=='' && $orders_row['ShippingMethodSId']==0 && $orders_row['ShippingMethodType']==''){ //多个发货地
			$isOverseas=1;
		}else{ //单个发货地
			$isOverseas=0;
		}
		//整合订单信息
		$orders_ary=array(
			'OrderId'					=>	$orders_row['OrderId'],
			'ShippingMethodSId'			=>	$orders_row['ShippingMethodSId'],
			'ShippingPrice'				=>	$orders_row['ShippingPrice'],
			'ShippingInsurance'			=>	(int)$orders_row['ShippingInsurance'],
			'ShippingInsurancePrice'	=>	$orders_row['ShippingInsurancePrice'],
			'TotalWeight'				=>	$orders_row['TotalWeight'],
			'TotalVolume'				=>	$orders_row['TotalVolume'],
			'ShippingOvExpress'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvExpress']), 'decode'),
			'ShippingOvSId'				=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvSId']), 'decode'),
			'ShippingOvType'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvType']), 'decode'),
			'ShippingOvInsurance'		=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvInsurance']), 'decode'),
			'ShippingOvPrice'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvPrice']), 'decode'),
			'ShippingOvInsurancePrice'	=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvInsurancePrice']), 'decode')
		);
		$where ="";
		if ($p_ProId)
		{
		    $where=" and o.ProId='".$p_ProId."'";
		}
		//整合订单产品信息
		$info=$pro_info_ary=array();
		$sProdInfo=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$p_OrderId'".$where, "o.*,p.IsFreeShipping, p.TId");
		$total_weight=$total_volume=$s_total_weight=$s_total_volume=0;
		foreach($sProdInfo as $v){
			$price=($v['Price']+$v['PropertyPrice'])*$v['Qty']*($v['Discount']<100?$v['Discount']/100:1);
			if($shipping_template){
				if(!$pro_info_ary[$v['CId']]){
					$pro_info_ary[$v['CId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$v['OvId']);
				}
				$pro_info_ary[$v['CId']]['tWeight']=($v['Weight']*$v['Qty']);
				$pro_info_ary[$v['CId']]['tVolume']=($v['Volume']*$v['Qty']);
				$pro_info_ary[$v['CId']]['tQty']=$v['Qty'];
				$pro_info_ary[$v['CId']]['Price']=$price;
				$pro_info_ary[$v['CId']]['TId']=$v['TId'];
				if((int)$v['IsFreeShipping']==1){//免运费
					$pro_info_ary[$v['CId']]['IsFreeShipping']=1; //其中有免运费
				}else{
					$pro_info_ary[$v['CId']]['Weight']=($v['Weight']*$v['Qty']);
					$pro_info_ary[$v['CId']]['Volume']=($v['Volume']*$v['Qty']);
					$pro_info_ary[$v['CId']]['Qty']=$v['Qty'];
				}
			}else{
				if(!$pro_info_ary[$v['OvId']]){
					$pro_info_ary[$v['OvId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
				}
				$pro_info_ary[$v['OvId']]['tWeight']+=($v['Weight']*$v['Qty']);
				$pro_info_ary[$v['OvId']]['tVolume']+=($v['Volume']*$v['Qty']);
				$pro_info_ary[$v['OvId']]['tQty']+=$v['Qty'];
				$pro_info_ary[$v['OvId']]['Price']+=$price;
				if((int)$v['IsFreeShipping']==1){//免运费
					$pro_info_ary[$v['OvId']]['IsFreeShipping']=1; //其中有免运费
				}else{
					$pro_info_ary[$v['OvId']]['Weight']+=($v['Weight']*$v['Qty']);
					$pro_info_ary[$v['OvId']]['Volume']+=($v['Volume']*$v['Qty']);
					$pro_info_ary[$v['OvId']]['Qty']+=$v['Qty'];
				}
			}
		}
		/* //产品包装重量先隐藏
		//订单产品包装重量
		$cartProAry=orders::orders_product_weight($p_OrderId, 1);
		foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['tWeight']+=$v2;
			}
		}
		foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['Weight']+=$v2;
			}
		}*/
		ksort($pro_info_ary); //排列正序
		/************************************** 新增折后之后计算运费 开始**********************************/
		$_total_price=$orders_row['ProductPrice']*((100-$orders_row['Discount'])/100)*((($orders_row['UserDiscount']>0 && $orders_row['UserDiscount']<100)?$orders_row['UserDiscount']:100)/100)*(1-$orders_row['CouponDiscount'])-$orders_row['CouponPrice']-$orders_row['DiscountPrice'];//订单折扣后的总价;
		$pro_info_ary=orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
		/************************************** 新增折后之后计算运费 结束**********************************/
		//快递信息
		$shipping_cfg=db::get_one('shipping_config', "Id='1'");
		$weight=@ceil($total_weight);
		$config_ary=str::json_data(htmlspecialchars_decode(db::get_value('config', "GroupId='products_show' and Variable='Config'", 'Value')), 'decode');
		//整理
		$provincial_freight=@in_array('provincial_freight', (array)$c['manage']['plugins']['Used']) ? 1 : 0; //开启省份运费
		$StatesSId=(int)$orders_row['ShippingSId'];
		$provincial_freight && $orders_row['ShippingCId']==226 && $StatesSId && $area_where=" and states like '%|{$StatesSId}|%'";
		$row=db::get_all('shipping_area a left join shipping s on a.SId=s.SId', "a.AId in(select AId from shipping_country where CId='{$orders_row['ShippingCId']}' {$area_where})", 's.Express, s.IsWeightArea, s.WeightArea, s.ExtWeightArea, s.VolumeArea, s.IsUsed, s.IsAPI, s.FirstWeight, s.ExtWeight, s.StartWeight, s.MinWeight, s.MaxWeight, s.MinVolume, s.MaxVolume, s.FirstMinQty, s.FirstMaxQty, s.ExtQty, s.WeightType, a.*', 'if(s.MyOrder>0, s.MyOrder, 100000) asc, a.SId asc, a.AId asc');
		$shipping_row=db::get_all('shipping');
		$row_ary=$shipping_tid_ary=array();
		foreach($row as $v){
			!$row_ary[$v['SId']] && $row_ary[$v['SId']]=array('info'=>$v, 'overseas'=>array());
			$row_ary[$v['SId']]['overseas'][$v['OvId']]=$v;
		}
		$shipping_template && $DefaultTId=db::get_value('shipping_template', 'IsDefault=1', 'TId');
		foreach((array)$shipping_row as $v){
			if($shipping_template) $shipping_tid_ary[$v['TId']][]=$v['SId'];
		}
		unset($row);
		foreach($row_ary as $key=>$val){
			$row=$val['info'];
			$isOvId=0;
			foreach($pro_info_ary as $k=>$v){ 
				if($shipping_template){
					$val['overseas'][$v['OvId']] && $isOvId+=1;
				}else{
					$val['overseas'][$k] && $isOvId+=1;
				}
			}//循环产品数据
			if($isOvId==0){
				$info[1][]=array('SId'=>'', 'Name'=>'', 'Brief'=>'', 'IsAPI'=>'', 'type'=>'', 'ShippingPrice'=>'-1');
				continue;
			}
			//循环产品数据 Start
			foreach($pro_info_ary as $k=>$v){
				$overseas=$val['overseas'][$k];
				$shipping_template && $overseas=$val['overseas'][$v['OvId']];
				if($shipping_template && (!in_array($key, (array)$shipping_tid_ary[$v['TId']]) || ($DefaultTId && !$shipping_tid_ary[$v['TId']] && !in_array($key, (array)$shipping_tid_ary[$DefaultTId])))) continue;
				$open=0;//默认不通过
				if(in_array($row['IsWeightArea'], array(0,1,2)) && ((float)$row['MaxWeight']?($v['tWeight']>=$row['MinWeight'] && $v['tWeight']<=$row['MaxWeight']):($v['tWeight']>=$row['MinWeight']))){//重量限制
					$open=1;
				}elseif($row['IsWeightArea']==4 && ($v['tWeight']>=$row['MinWeight'] || $v['tVolume']>=$row['MinVolume'])){//重量限制+体积限制
					$open=1;
				}elseif($row['IsWeightArea']==3){//按数量计算，直接不限制
					$open=1;
				}
				if($overseas && $row['IsUsed']==1 && $open==1){
					$sv=array(
						'SId'		=>	$row['SId'],
						'Name'		=>	$row['Express'],
						'type'		=>	'',
					);
					if(($v['IsFreeShipping']==1 && $v['Weight']==0) || ((int)$config_ary['freeshipping'] && $v['Weight']==0) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']>0 && $v['Price']>=$overseas['FreeShippingPrice']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingWeight']>0 && $v['Weight']<$overseas['FreeShippingWeight']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']==0 && $overseas['FreeShippingWeight']==0)){
						$shipping_price=0;
					}else{
						$shipping_price=0;
						if($overseas['IsWeightArea']==1 || ($overseas['IsWeightArea']==2 && $v['Weight']>=$overseas['StartWeight'])){
							//重量区间 重量混合
							$WeightArea=str::json_data($overseas['WeightArea'], 'decode');
							$WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
							$areaCount=count($WeightArea)-1;
							foreach($WeightArea as $k2=>$v2){
								if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
									if($overseas['WeightType']==1){//按每KG计算
										$shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
									}else{//按整价计算
										$shipping_price=$WeightAreaPrice[$k2];
									}
									break;
								}
							}
							//$v['Weight']>$WeightArea[$areaCount] && $shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
						}elseif($overseas['IsWeightArea']==3){
							//按数量
							$shipping_price=$overseas['FirstQtyPrice'];//先收取首重费用
							$ExtQtyValue=$v['Qty']>$overseas['FirstMaxQty']?$v['Qty']-$overseas['FirstMaxQty']:0;//超出的数量
							if($ExtQtyValue){//续重
								$shipping_price+=(float)(@ceil($ExtQtyValue/$overseas['ExtQty'])*$overseas['ExtQtyPrice']);
							}
						}elseif($overseas['IsWeightArea']==4){
							//重量体积混合计算
							$weight_shipping_price=$volume_shipping_price=0;
							if($v['Weight']>=$overseas['MinWeight']){//重量
								$WeightArea=str::json_data($overseas['WeightArea'], 'decode');
								$WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
								$areaCount=count($WeightArea)-1;
								foreach($WeightArea as $k2=>$v2){
									if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
										if($overseas['WeightType']==1){//按每KG计算
											$weight_shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
										}else{//按整价计算
											$weight_shipping_price=$WeightAreaPrice[$k2];
										}
										break;
									}
								}
								$overseas['WeightType']==1 && $v['Weight']>$WeightArea[$areaCount] && $weight_shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
							}
							if($v['Volume']>=$overseas['MinVolume']){//体积
								$VolumeArea=str::json_data($overseas['VolumeArea'], 'decode');
								$VolumeAreaPrice=str::json_data($overseas['VolumeAreaPrice'], 'decode');
								$areaCount=count($VolumeArea)-1;
								foreach($VolumeArea as $k2=>$v2){
									if($k2<=$areaCount && (($VolumeArea[$k2+1] && $v['Volume']<$VolumeArea[$k2+1]) || (!$VolumeArea[$k2+1] && $v['Volume']>=$v2))){
										$volume_shipping_price=$VolumeAreaPrice[$k2]*$v['Volume'];
										break;
									}
								}
								$v['Volume']>$VolumeArea[$areaCount] && $volume_shipping_price=$VolumeAreaPrice[$areaCount]*$v['Volume'];
							}
							$shipping_price=max($weight_shipping_price, $volume_shipping_price);
						}else{
							//首重续重
							$ExtWeightArea=str::json_data($overseas['ExtWeightArea'], 'decode');
							$ExtWeightAreaPrice=str::json_data($overseas['ExtWeightAreaPrice'], 'decode');
							$areaCount=count($ExtWeightArea)-1;
							$ExtWeightValue=$v['Weight']>$overseas['FirstWeight']?$v['Weight']-$overseas['FirstWeight']:0;//超出的重量
							if($areaCount>0){
								$shipping_price=$overseas['FirstPrice'];//先收取首重费用
								foreach($ExtWeightArea as $k2=>$v2){
									if($v['Weight']>$v2 && $ExtWeightArea[$k2+1]){
										$ext=$v['Weight']>$ExtWeightArea[$k2+1]?($ExtWeightArea[$k2+1]-$v2):($v['Weight']-$v2);
										$shipping_price+=(float)(@ceil($ext/$overseas['ExtWeight'])*$ExtWeightAreaPrice[$k2]);
									}elseif($v['Weight']>$v2 && !$ExtWeightArea[$k2+1]){//达到以上费用
										$ext=$v['Weight']-$v2;
										$shipping_price+=(float)(@ceil($ext/$overseas['ExtWeight'])*$ExtWeightAreaPrice[$k2]);
									}
								}
							}else{
								$shipping_price=(float)(@ceil($ExtWeightValue/$overseas['ExtWeight'])*$ExtWeightAreaPrice[0]+$overseas['FirstPrice']);
							}
						}
						if($overseas['AffixPrice']){//附加费用
							$shipping_price+=$overseas['AffixPrice'];
						}
					}
					$sv['ShippingPrice']=$shipping_price;
					$sv['InsurancePrice']=cart::get_insurance_price_by_price($v['Price']);
					$info[$k][]=$sv;
				}
			}
		}
		unset($orders_row, $shipping_cfg, $air_row, $ocean_row, $sv, $row_ary, $shipping_data);
		if($info){
			$info_ary=array();
			foreach($info as $k=>$v){
				$sort_ary=array();
				foreach($v as $k2=>$v2){
					$sort_ary[$k2]=$v2['ShippingPrice'];
				}
				asort($sort_ary);
				foreach($sort_ary as $k2=>$v2){
					$info_ary[$k][]=$info[$k][$k2];
				}
			}
			ly200::e_json(array('isOverseas'=>$isOverseas, 'info'=>$info_ary, 'orders_info'=>$orders_ary, 'shipping_template'=>$shipping_template), 1);
		}else{
			ly200::e_json('', 0);
		}
	}

	public static function orders_mod_shipping()
    {
        //更新发货方式
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_OrderId  = (int)$p_OrderId;
		$orders_row = db::get_one('orders', "OrderId='$p_OrderId'");
		!$orders_row && ly200::e_json('', -1);
		$shipping_template      = (int)$orders_row['shipping_template'];
		$p_OvId                 = (int)$p_OvId;
		$p_ShippingMethodSId    = (int)$p_ShippingMethodSId;
		$p_ShippingMethodType	= $p_ShippingMethodType;
		$p_ShippingInsurance	= (int)$p_ShippingInsurance;
        $IsSingle               = 0;
        if ($orders_row['ShippingExpress'] == '' && $orders_row['ShippingMethodSId'] == 0 && $orders_row['ShippingMethodType'] == '' && $orders_row['ShippingOvExpress'] == '' && $orders_row['ShippingOvSId'] == '' && $orders_row['ShippingOvType'] == '') {
            //快递信息都没了
            $IsSingle = 1;
        }
		if ($orders_row['ShippingExpress'] == '' && $orders_row['ShippingMethodSId'] == 0 && $orders_row['ShippingMethodType'] == '') {
            //多个发货地
			$isOverseas = 1;
			$OvSId = str::json_data(htmlspecialchars_decode($orders_row['ShippingOvSId']), 'decode');
			$OvType = str::json_data(htmlspecialchars_decode($orders_row['ShippingOvType']), 'decode');
			$OvInsurance = str::json_data(htmlspecialchars_decode($orders_row['ShippingOvInsurance']), 'decode');
			$OvSId[$p_OvId] = $p_ShippingMethodSId;
			$OvType[$p_OvId] = $p_ShippingMethodType;
			$OvInsurance[$p_OvId] = $p_ShippingInsurance;
			$data = array(
				'ShippingOvExpress'			=>	addslashes($orders_row['ShippingOvExpress']),
				'ShippingOvSId'				=>	addslashes(str::json_data($OvSId)),
				'ShippingOvType'			=>	addslashes(str::json_data($OvType)),
				'ShippingOvInsurance'		=>	addslashes(str::json_data($OvInsurance)),
				'ShippingOvPrice'			=>	addslashes($orders_row['ShippingOvPrice']),
				'ShippingOvInsurancePrice'	=>	addslashes($orders_row['ShippingOvInsurancePrice'])
			);
		} else {
            //单个发货地
            $IsSingle = 1;
		}
        if ($IsSingle == 1) {
            //单个发货地
            $isOverseas = 0;
			$OvSId[$p_OvId] = $p_ShippingMethodSId;
			$OvType[$p_OvId] = $p_ShippingMethodType;
			$OvInsurance[$p_OvId] = $p_ShippingInsurance;
			$data = array(
				'ShippingExpress'			=>	$orders_row['ShippingExpress'],
				'ShippingMethodSId'			=>	$p_ShippingMethodSId,
				'ShippingMethodType'		=>	$p_ShippingMethodType,
				'ShippingInsurance'			=>	$p_ShippingInsurance,
				'ShippingPrice'				=>	$orders_row['ShippingPrice'],
				'ShippingInsurancePrice'	=>	$orders_row['ShippingInsurancePrice']
			);
        }
		if ((int)$p_AutoModShippingPrice == 1) {
            //更新运费
			$SId = $OvSId;
			$sType = $OvType;
			$sInsurance = $OvInsurance;
			$sProdInfo = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$p_OrderId'", "o.*, p.IsFreeShipping, p.TId");
			$total_weight = 0;
            $total_volume = 0;
			$pro_info_ary = array();
			foreach ($sProdInfo as $v) {
				$item_price = ($v['Price'] + $v['PropertyPrice']) * $v['Qty'] * ($v['Discount'] < 100 ? $v['Discount'] / 100 : 1);
				if ($shipping_template) {
					if (!$pro_info_ary[$v['CId']]) {
						$pro_info_ary[$v['CId']] = array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$v['OvId']);
					}
					$pro_info_ary[$v['CId']]['tWeight'] = ($v['Weight']*$v['Qty']);
					$pro_info_ary[$v['CId']]['tVolume'] = ($v['Volume']*$v['Qty']);
					$pro_info_ary[$v['CId']]['Price'] = $item_price;
					$pro_info_ary[$v['CId']]['TId'] = (int)$v['TId'];
					if ((int)$v['IsFreeShipping'] == 1) {
                        //免运费
						$pro_info_ary[$v['CId']]['IsFreeShipping'] = 1; //其中有免运费
					} else {
						$pro_info_ary[$v['CId']]['Weight'] = ($v['Weight'] * $v['Qty']);
						$pro_info_ary[$v['CId']]['Volume'] = ($v['Volume'] * $v['Qty']);
					}
				} else {
					if (!$pro_info_ary[$v['OvId']]) {
						$pro_info_ary[$v['OvId']] = array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
					}
					$pro_info_ary[$v['OvId']]['tWeight'] += ($v['Weight'] * $v['Qty']);
					$pro_info_ary[$v['OvId']]['tVolume'] += ($v['Volume'] * $v['Qty']);
					$pro_info_ary[$v['OvId']]['Price'] += $item_price;
					if ((int)$v['IsFreeShipping'] == 1) {
                        //免运费
						$pro_info_ary[$v['OvId']]['IsFreeShipping'] = 1; //其中有免运费
					} else {
						$pro_info_ary[$v['OvId']]['Weight'] += ($v['Weight'] * $v['Qty']);
						$pro_info_ary[$v['OvId']]['Volume'] += ($v['Volume'] * $v['Qty']);
					}
				}
			}
			/*
            //产品包装重量先隐藏
			//产品包装重量
			$cartProAry=orders::orders_product_weight($p_OrderId, 1);
			foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
				foreach((array)$v as $k2=>$v2){//$k2是ProId
					$pro_info_ary[$k]['tWeight']+=$v2;
				}
			}
			foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
				foreach((array)$v as $k2=>$v2){//$k2是ProId
					$pro_info_ary[$k]['Weight']+=$v2;
				}
			}
			*/
			/************************************** 新增折后之后计算运费 开始**********************************/
			$_total_price = $orders_row['ProductPrice'] * ((100 - $orders_row['Discount']) / 100) * ((($orders_row['UserDiscount'] > 0 && $orders_row['UserDiscount'] < 100) ? $orders_row['UserDiscount'] : 100) / 100) * (1 - $orders_row['CouponDiscount']) - $orders_row['CouponPrice'] - $orders_row['DiscountPrice']; //订单折扣后的总价
			$pro_info_ary = orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
			/************************************** 新增折后之后计算运费 结束**********************************/
			$shipping_ary = orders::orders_shipping_method($SId, $orders_row['ShippingCId'], $sType, $sInsurance, $pro_info_ary, array(), (int)$orders_row['ShippingSId']);
			!$shipping_ary && ly200::e_json('', -2);
			
			$data['ShippingExpress'] = addslashes($shipping_ary['ShippingExpress']);
			$data['ShippingPrice'] = $shipping_ary['ShippingPrice'];
			$data['ShippingInsurancePrice'] = $shipping_ary['ShippingInsurancePrice'];
			$data['ShippingOvExpress'] = addslashes($shipping_ary['ShippingOvExpress']);
			$data['ShippingOvPrice'] = addslashes($shipping_ary['ShippingOvPrice']);
			$data['ShippingOvInsurancePrice'] = addslashes($shipping_ary['ShippingOvInsurancePrice']);
		} else {
            //不更新运费
			$data['ShippingExpress'] = addslashes(str::str_code(db::get_value('shipping', "SId='{$p_ShippingMethodSId}'", 'Express')));
			$shipping_ov_express_ary = array();
			foreach ($p_ShippingMethodSId as $k => $v) {
				if ($v == 0 && $p_ShippingMethodType[$k] == 'air') {
					$shipping_ov_express_ary[$k] = str::str_code(db::get_value('shipping_config', "Id='1'", 'AirName'));
				} elseif ($v == 0 && $p_ShippingMethodType[$k] == 'ocean') {
					$shipping_ov_express_ary[$k] = str::str_code(db::get_value('shipping_config', "Id='1'", 'OceanName'));
				} else {
					$shipping_ov_express_ary[$k] = str::str_code(db::get_value('shipping', "SId='$v'", 'Express'));
				}
			}
			$data['ShippingOvExpress'] = addslashes(str::json_data($shipping_ov_express_ary));
		}
		$data['UpdateTime'] = $c['time'];
		//发货信息
		$IsChangeTrack = 0;
		if ($orders_row['OrderStatus'] > 4 && trim($p_TrackingNumber) && trim($p_ShippingTime)) {
			$data['TrackingNumber'] = @trim($p_TrackingNumber);
			$data['ShippingTime'] = @strtotime($p_ShippingTime);
			$data['Remarks'] = $p_Remarks;
			$IsChangeTrack = 1;
		}
		//更新数据
		db::update('orders', "OrderId='$p_OrderId'", $data);
		//返回数据
		$data['ShippingExpress'] = $data['ShippingExpress'];
		$data['ShippingMethodSId'] = (int)$data['ShippingMethodSId'];
		$data['ShippingMethodType'] = $data['ShippingMethodType'];
		$data['ShippingOvExpress'] = str::json_data(stripslashes($data['ShippingOvExpress']), 'decode');
		$data['ShippingOvSId'] = str::json_data(stripslashes($data['ShippingOvSId']), 'decode');
		$data['ShippingOvType'] = str::json_data(stripslashes($data['ShippingOvType']), 'decode');
		$data['ShippingOvInsurance'] = str::json_data(stripslashes($data['ShippingOvInsurance']), 'decode');
		$data['ShippingOvPrice'] = str::json_data(stripslashes($data['ShippingOvPrice']), 'decode');
		$data['ShippingOvInsurancePrice'] = str::json_data(stripslashes($data['ShippingOvInsurancePrice']), 'decode');
		$orders_row = db::get_one('orders', "OrderId='$p_OrderId'");
		$data['TotalAmount'] = orders::orders_price($orders_row, 1, 1);
		$ReturnData = array();
		if ($isOverseas == 1) {
            //多个发货地
			$ReturnData = array(
				'OvId'				=>	$p_OvId,
				'Express'			=>	$data['ShippingOvExpress'][$p_OvId],
				'SId'				=>	$data['ShippingOvSId'][$p_OvId],
				'Type'				=>	$data['ShippingOvType'][$p_OvId],
				'Insurance'			=>	$data['ShippingOvInsurance'][$p_OvId],
				'Price'				=>	sprintf('%01.2f', $data['ShippingOvPrice'][$p_OvId]),
				'InsurancePrice'	=>	sprintf('%01.2f', $data['ShippingOvInsurancePrice'][$p_OvId]),
				'TotalShippingPrice'=>	sprintf('%01.2f', $data['ShippingPrice']),
				'TotalInsurancePrice'=>	sprintf('%01.2f', $data['ShippingInsurancePrice']),
				'TotalAmount'		=>	sprintf('%01.2f', $data['TotalAmount'])
			);
		} else {
            //单个发货地
			$ReturnData = array(
				'OvId'				=>	0,
				'Express'			=>	$data['ShippingExpress'],
				'SId'				=>	$data['ShippingMethodSId'],
				'Type'				=>	$data['ShippingMethodType'],
				'Insurance'			=>	$data['ShippingInsurance'],
				'Price'				=>	sprintf('%01.2f', $data['ShippingPrice']),
				'InsurancePrice'	=>	sprintf('%01.2f', $data['ShippingInsurancePrice']),
				'TotalShippingPrice'=>	sprintf('%01.2f', $data['ShippingPrice']),
				'TotalInsurancePrice'=>	sprintf('%01.2f', $data['ShippingInsurancePrice']),
				'TotalAmount'		=>	sprintf('%01.2f', $data['TotalAmount'])
			);
		}
		//更改发货信息
		if ($IsChangeTrack == 1) {
			$ReturnData['TrackingNumber'] = $data['TrackingNumber'];
			$ReturnData['ShippingTime'] = date('Y-m-d', $data['ShippingTime']);
			$ReturnData['Remarks'] = $data['Remarks'];
		}
		//下单所相应的货币价格
		if ($orders_row['Currency'] != $orders_row['ManageCurrency']) {
			$currency_row = db::get_one('currency', "Currency='{$orders_row['Currency']}'", 'Symbol, Rate');
			$web_symbol = $currency_row['Symbol'];
			$Rate = ($orders_row['Rate'] ? $orders_row['Rate'] : $currency_row['Rate']);
			$ReturnData['WebPrice'] = manage::rate_price($ReturnData['Price'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			$ReturnData['WebInsurancePrice'] = manage::rate_price($ReturnData['InsurancePrice'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			$ReturnData['WebTotalShippingPrice'] = manage::rate_price($ReturnData['TotalShippingPrice'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			$ReturnData['WebTotalInsurancePrice'] = manage::rate_price($ReturnData['TotalInsurancePrice'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			$ReturnData['WebTotalAmount'] = manage::rate_price($ReturnData['TotalAmount'], 0, $orders_row['Currency'], $web_symbol, $Rate);
		}
		unset($orders_row, $shipping_ary, $json_ary, $data);
		ly200::e_json(array('info'=>$ReturnData), 1);
	}
	
	public static function orders_mod_track(){//更新包裹信息
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_OrderId	= (int)$p_OrderId;
		$p_OvId		= (int)$p_OvId;
		$p_WId		= (int)$p_WId;
		$order_row	= db::get_one('orders', "OrderId='$p_OrderId'");
		$data		= array();
		$table		= '';
		if($p_WId>0){//分单
			$table='orders_waybill';
			$where="WId='$p_WId'";
			$data['TrackingNumber']=@trim($p_TrackingNumber);
			$data['ShippingTime']=@strtotime($p_ShippingTime);
			$data['Remarks']=$p_Remarks;
		}elseif($p_OvId>0 && !$order_row['TrackingNumber'] && $order_row['ShippingTime']==0 && !$order_row['Remarks']){//多个发货地
			$table='orders';
			$where="OrderId='$p_OrderId'";
			$TrackingNumber=str::json_data(htmlspecialchars_decode($order_row['OvTrackingNumber']), 'decode');
			$ShippingTime=str::json_data(htmlspecialchars_decode($order_row['OvShippingTime']), 'decode');
			$Remarks=str::json_data(htmlspecialchars_decode($order_row['OvRemarks']), 'decode');
			$TrackingNumber[$p_OvId]=trim($p_TrackingNumber);
			$ShippingTime[$p_OvId]=strtotime($p_ShippingTime);
			$Remarks[$p_OvId]=$p_Remarks;
			$data['OvTrackingNumber']=addslashes(str::json_data($TrackingNumber));
			$data['OvShippingTime']=addslashes(str::json_data($ShippingTime));
			$data['OvRemarks']=addslashes(str::json_data($Remarks));
			$data['UpdateTime']=$c['time'];
		}else{//单个发货地
			$table='orders';
			$where="OrderId='$p_OrderId'";
			$data['TrackingNumber']=@trim($p_TrackingNumber);
			$data['ShippingTime']=@strtotime($p_ShippingTime);
			$data['Remarks']=$p_Remarks;
			$data['UpdateTime']=$c['time'];
		}
		db::update($table, $where, $data);
		manage::operation_log('更新订单包裹信息');
		unset($c, $data);
		ly200::e_json('', 1);
	}
	public static function orders_request_mod_status(){//更新采购需求状态
	    global $c;
	    @extract($_POST, EXTR_PREFIX_ALL, 'p');
	    //初始化
	    $p_OrderId			= (int)$p_OrderId;
	    $p_OrderStatus		= (int)$p_OrderStatus;
	    $data				= array();
	    $data['status']=$p_OrderStatus;
// 	    $orders_row			= db::get_one('orders', "OrderId='$p_OrderId'");
	    db::update('purchase_request', "id='$p_OrderId'", $data);
	    
	    manage::operation_log('更新订单状态');
	    ly200::e_json('', 1);
	}
	public static function orders_mod_status(){//更新采购需求状态
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_OrderId			= (int)$p_OrderId;
		$p_OrderStatus		= (int)$p_OrderStatus;
		$p_PassOrderStatus	= (int)$p_PassOrderStatus;
		$orders_row			= db::get_one('orders', "OrderId='$p_OrderId'");
		$data				= array();
		!$p_PassOrderStatus && $p_PassOrderStatus=(int)$orders_row['OrderStatus'];
		$data['OrderStatus']=$p_OrderStatus;
		if($p_OrderStatus==4){
			//检查是不是新客户
			if(!db::get_one('orders',"Email = '{$orders_row['Email']}' and OrderStatus in (4,5,6)")){
				$data['IsNewCustom'] =1;
			}
		}elseif($p_OrderStatus==7){ //已发货
			if(!$p_TrackingNumber && !$p_ShippingTime && !$p_Remarks){//多个发货地
				$data['OvTrackingNumber']=addslashes(str::json_data($p_OvTrackingNumber));
				$data['OvRemarks']=addslashes(str::json_data($p_OvRemarks));
				$OvShippingStatus=array();
				foreach($p_OvShippingTime as $k=>$v){
					$p_OvShippingTime[$k]=strtotime($v);
					$OvShippingStatus[$k]=1;
				}
				$data['OvShippingStatus']=addslashes(str::json_data($OvShippingStatus));
				$data['OvShippingTime']=addslashes(str::json_data($p_OvShippingTime));
			}else{//单个发货地
				$data['TrackingNumber']=@trim($p_TrackingNumber);
				$data['ShippingTime']=@strtotime($p_ShippingTime);
				$data['Remarks']=$p_Remarks;
			}
			orders::orders_shipping_api('pre_alert_order', $orders_row['OId']);//申请API运单发货
		}elseif($p_OrderStatus==9){
			$data['Remarks']=$p_Remarks;
		}
		$data['UpdateTime']=$c['time'];
		db::update('orders', "OrderId='$p_OrderId'", $data);
		$orders_row=db::get_one('orders', "OrderId='$p_OrderId'");//更新订单信息
		$Log='Update order status from '.$c['orders']['status'][$p_PassOrderStatus].' to '.$c['orders']['status'][$p_OrderStatus];
		orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $p_OrderId, $p_OrderStatus, $Log, 1);
		//处理订单信息
		if((int)$c['manage']['config']['LessStock']==1 && $p_OrderStatus>5 && $p_OrderStatus<9){
			orders::orders_products_update($p_OrderStatus, $orders_row);//付款减库存
			orders::orders_shipping_api('create_order', $orders_row['OId']);//创建API运单
		}
		if((int)$c['manage']['config']['LessStock']==0 && $p_OrderStatus>3 && $p_OrderStatus<7){
			orders::orders_user_update($p_OrderId);//付款清算会员数据
		}
		if($p_OrderStatus==9 && ((int)$c['manage']['config']['LessStock']==0 || ((int)$c['manage']['config']['LessStock']==1 && $p_PassOrderStatus>3))){
            //取消订单，(下单减库存 || 付款减库存) 才执行
			orders::orders_products_update($p_OrderStatus, $orders_row, 1);
		}
		if($p_OrderStatus==6 && $c['plugin_app']->trigger('distribution', '__config', 'DIST_order_received')=='enable'){//完成订单 分销APP是否存在
			$c['plugin_app']->trigger('distribution', 'DIST_order_received', $p_OrderId);//检查分销识别码
		}
		$OId=$orders_row['OId'];
		$p_OrderStatus==5 && orders::orders_sms($OId); //付款成功，发送短信
		/******************** 发邮件 ********************/
		$notice_config=array();
		$email_config=db::get_all('system_email_tpl', '1', 'Template, IsUsed');
		foreach($email_config as $v){
			$notice_config[$v['Template']]=(int)$v['IsUsed'];
		}
		$ToAry=array($orders_row['Email']);
		// $c['manage']['config']['AdminEmail'] && $ToAry[]=$c['manage']['config']['AdminEmail'];
		if($p_OrderStatus==5 && (int)$notice_config['order_payment']){//付款成功，等待发货
			include($c['root_path'].'/static/static/inc/mail/order_payment.php');
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
			//$c['manage']['config']['OrdersSms'] && ly200::sendsms($c['manage']['config']['OrdersSms'], "您的订单已经付款成功，订单号：{$OId}");
		}else if($p_OrderStatus==7 && (int)$notice_config['order_shipped']){//订单发货
			include($c['root_path'].'/static/static/inc/mail/order_shipped.php');
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
		}else if($p_OrderStatus==8 && (int)$notice_config['order_change']){//订单完成
			include($c['root_path'].'/static/static/inc/mail/order_change.php');
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
		}else if($p_OrderStatus==9 && (int)$notice_config['order_cancel']){//取消订单
			include($c['root_path'].'/static/static/inc/mail/order_cancel.php');
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
		}
		/******************** 发邮件结束 ********************/
		manage::operation_log('更新订单状态');
		unset($c, $data);
		ly200::e_json('', 1);
	}

	public static function orders_status_bat(){//批量更新订单状态
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'p');
		$del_bat_id=str_replace('-', ',', $_GET['id']);
		$del_bat_id=='' && ly200::e_json('');
		$OrderStatus=6;
		$orders_row=db::get_all('orders', "OrderId in($del_bat_id)", 'OId,OrderId,OrderTime,OrderStatus');
		foreach((array)$orders_row as $v){
			//初始化
			$data=array();
			$OrderId=(int)$v['OrderId'];
			$PassOrderStatus=(int)$v['OrderStatus'];
			$data['OrderStatus']=$OrderStatus;
			$data['UpdateTime']=$c['time'];
			db::update('orders', "OrderId='$OrderId'", $data);
			$orders_row=db::get_one('orders', "OrderId='$OrderId'");//更新订单信息
			$Log='Update order status from '.$c['orders']['status'][$PassOrderStatus].' to '.$c['orders']['status'][$OrderStatus];
			orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $OrderId, $OrderStatus, $Log, 1);
			//处理订单信息
			if((int)$c['manage']['config']['LessStock']==1 && $OrderStatus>3 && $OrderStatus<7){
				orders::orders_products_update($OrderStatus, $orders_row);//付款减库存
				orders::orders_shipping_api('create_order', $orders_row['OId']);//创建API运单
			}
			if((int)$c['manage']['config']['LessStock']==0 && $OrderStatus>3 && $OrderStatus<7){
				orders::orders_user_update($OrderId);//付款清算会员数据
			}
			if($OrderStatus==6 && $c['plugin_app']->trigger('distribution', '__config', 'DIST_order_received')=='enable'){//完成订单 分销APP是否存在
				$c['plugin_app']->trigger('distribution', 'DIST_order_received', $OrderId);//检查分销识别码
			}
		}

		manage::operation_log('更新订单状态:'.$_GET['id']);
		unset($c, $data);
		ly200::e_json('', 1);
	}
	public static function orders_request_status_bat(){//批量更新采购需求状态
	    global $c;
	    @extract($_GET, EXTR_PREFIX_ALL, 'p');
	    $del_bat_id=str_replace('-', ',', $_GET['id']);
	    $del_bat_id=='' && ly200::e_json('');
	    $OrderStatus=3;
	    $orders_row=db::get_all('purchase_request', "id in($del_bat_id)", 'OId,id,created_at,status');
	    foreach((array)$orders_row as $v){
	        //初始化
	        $data=array();
	        $OrderId=(int)$v['id'];
	        $PassOrderStatus=(int)$v['status'];
	        $data['status']=$OrderStatus;
	        $data['UpdateTime']=$c['time'];
	        db::update('purchase_request', "id='$OrderId'", $data);
	    }
	    manage::operation_log('更新订单状态:'.$_GET['id']);
	    unset($c, $data);
	    ly200::e_json('', 1);
	}
	public static function orders_track_no(){//更新运单号
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$OrderId=(int)$p_OrderId;
		db::update('orders', "OrderId='$OrderId'", array('TrackingNumber'=>@trim($p_TrackingNumber), 'UpdateTime'=>$c['time']));
		manage::operation_log('更新订单运单号');
		exit(str::json_data(array('status'=>1)));
	}
	
	public static function orders_remarks(){//更新备注内容
		global $c, $cfg;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$OrderId=(int)$p_OrderId;
		db::update('orders', "OrderId='$OrderId'", array('Remarks'=>@trim($p_Remarks), 'UpdateTime'=>$c['time']));
		manage::operation_log('更新订单备注内容');
		exit(str::json_data(array('status'=>1)));
	}
	
	public static function orders_remark_log(){//添加备注日志
		global $c, $cfg;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId=(int)$p_OrderId;
		$p_Log=@trim($p_Log);
		if($p_Log){
			$data=array(
				'OrderId'	=>	$p_OrderId,
				'Log'		=>	$p_Log,
				'AccTime'	=>	$c['time'],
				'UserId'	=>	(int)$_SESSION['Manage']['UserId'],
				'UserName'	=>	$_SESSION['Manage']['UserName']
			);
			db::insert('orders_remark_log', $data);
			manage::operation_log('添加订单备注日志');
			$data['AccTime']=date('Y-m-d', $data['AccTime']).'<br />'.date('H:i:s', $data['AccTime']);
			$data['Log']=stripslashes(htmlspecialchars_decode($data['Log']));
			ly200::e_json($data, 1);
		}else{
			ly200::e_json('', 0);
		}
	}
	
	public static function orders_refund(){//退款
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//货币汇率
		$all_currency_ary=array();
		$currency_row=db::get_all('currency', '1', 'Currency, Symbol, Rate');
		foreach($currency_row as $k=>$v){
			$all_currency_ary[$v['Currency']]=$v;
		}
		//订单信息
		$order_row=db::get_one('orders', "OrderId='{$p_OrderId}'");
		$payment_info=db::get_one('orders_payment_info', "OrderId='{$p_OrderId}'");
		if($order_row && $payment_info){
			if($order_row['Currency']!=$order_row['ManageCurrency']){
				$total_price=orders::orders_price($order_row, 1, 1);
				$web_symbol=$all_currency_ary[$order_row['Currency']]['Symbol'];
				$web_rate=($orders_row['Rate']?$order_row['Rate']:$all_currency_ary[$order_row['Currency']]['Rate']);
				$total_price=manage::rate_price($total_price, 2, $order_row['Currency'], $web_symbol, $web_rate, 0);
			}else{
				$total_price=orders::paypal_orders_price($order_row, 1);
			}
			$total_price=sprintf('%01.2f', $total_price);
			$SaleId=$payment_info['MTCNNumber'];
			if($order_row['PId']==1){
				//Paypal支付
				if($c['NewFunVersion']>=4){//新版本
					include("{$c['root_path']}/plugins/payment/paypal/REST/Refund.php");
				}else{//旧版本
					ly200::e_json('', 0);
				}
			}elseif($order_row['PId']==2){
				//Paypal快捷支付
				if($c['NewFunVersion']>=4){//新版本
					include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditRefund.php");
				}else{//旧版本
					include("{$c['root_path']}/plugins/payment/paypal_excheckout/RefundTransaction.php");
				}
			}
			ly200::e_json('', 1);
		}
		ly200::e_json('', 0);
	}
	
	public static function orders_dispute(){//Dispute
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//订单信息
		$order_row=db::get_one('orders', "OrderId='{$p_OrderId}'");
		$payment_info=db::get_one('orders_payment_info', "OrderId='{$p_OrderId}'");
		if($order_row && $payment_info){
			$total_price=orders::paypal_orders_price($order_row, 1);
			$total_price=sprintf('%01.2f', $total_price);
			$SaleId=$payment_info['MTCNNumber'];
			if($order_row['PId']==2){
				//Paypal快捷支付
				if($c['NewFunVersion']>=4){//新版本
					include("{$c['root_path']}/plugins/payment/paypal_excheckout/dispute/DisputeCreate.php");
				}else{//旧版本
					//include("{$c['root_path']}/plugins/payment/paypal_excheckout/RefundTransaction.php");
				}
			}
			ly200::e_json('', 1);
		}
		ly200::e_json('', 0);
	}
	
	public static function orders_prod_edit_edit(){//订单产品编辑
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$OrderId=(int)$p_OrderId;
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$OrderId'"));
		!$orders_row && ly200::e_json('', 0);
		//更新数据
		foreach($p_LId as $k=>$v){
			$Price=(float)$p_Price[$k];
			$Qty=(int)$p_Qty[$k];
			$Qty<1 && $Qty=1;
			$data=array(
				'Price'			=>	sprintf('%01.2f', $Price),
				'Qty'			=>	$Qty,
				'PropertyPrice'	=>	0, //属性价格清0
				'Discount'		=>	100 //折扣调成100
			);
			db::update('orders_products_list', "LId='$v'", $data);
		}
		//先检查有没有订单产品已被删除
		$delete_ary=array();
		$prod_row=db::get_all('orders_products_list', "OrderId='$OrderId'", 'LId, PicPath', 'LId asc');
		foreach($prod_row as $k=>$v){
			if(!in_array($v['LId'], $p_LId)){
				$delete_ary[]=$v;
			}
		}
		if(count($delete_ary)){
			foreach((array)$delete_ary as $k=>$v){
				file::del_file($v['PicPath']);//删除订单产品图片
				db::delete('orders_products_list', "LId={$v['LId']}");
			}
		}
		//统计数据
		$ProductPrice=0;
		$front_ProductPrice=0;
		$ReturnData=array('ProductPrice'=>0, 'TotalPrice'=>0, 'TotalQty'=>0, 'ProInfo'=>array());
		$prod_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "OrderId='$OrderId'", 'Price, PropertyPrice, Discount, Qty,price_rate', 'LId asc');
		foreach($prod_row as $v){
			$price=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1);
			$ProductPrice+=$price*$v['Qty'];
			$front_ProductPrice+=$price*$v['Qty']*$v['price_rate'];
			$ReturnData['TotalQty']+=$v['Qty'];
			$ReturnData['ProInfo'][]=array(
				'Price'	=>	sprintf('%01.2f', $price),
				'Qty'	=>	$v['Qty'],
			    'price_rate'	=>	$v['price_rate'],
			    'front_amount'	=>	sprintf('%01.2f', $price*$v['Qty']*$v['price_rate']),
				'Amount'=>	sprintf('%01.2f', $price*$v['Qty'])
			);
		}
		db::update('orders', "OrderId='$OrderId'", array('ProductPrice'=>(float)$ProductPrice, 'UpdateTime'=>$c['time']));
		manage::operation_log('修改订单产品 OId:'.$orders_row['OId']);
		$ReturnData['ProductPrice']=sprintf('%01.2f', $ProductPrice);
		$ReturnData['front_ProductPrice']=sprintf('%01.2f', $front_ProductPrice);
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$OrderId'"));
		$total_price=orders::orders_price($orders_row, 1, 1);
		$ReturnData['TotalPrice']=$total_price;
		//下单所相应的货币价格
		if($orders_row['Currency']!=$orders_row['ManageCurrency']){
			$currency_row=db::get_one('currency', "Currency='{$orders_row['Currency']}'", 'Symbol, Rate');
			$web_symbol=$currency_row['Symbol'];
			$Rate=($orders_row['Rate']?$orders_row['Rate']:$currency_row['Rate']);
			$ReturnData['WebProductPrice']=manage::rate_price($ReturnData['ProductPrice'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			$ReturnData['WebTotalPrice']=manage::rate_price($ReturnData['TotalPrice'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			foreach($ReturnData['ProInfo'] as $k=>$v){
				$ReturnData['WebProInfo'][$k]['Price']=manage::rate_price($v['Price'], 0, $orders_row['Currency'], $web_symbol, $Rate);
				$ReturnData['WebProInfo'][$k]['Amount']=manage::rate_price($v['Amount'], 0, $orders_row['Currency'], $web_symbol, $Rate);
			}
		}
		//返回数据
		ly200::e_json($ReturnData, 1);
	}
	
	public static function orders_prod_del(){//订单产品删除
		global $c;
		$LId=(int)$_GET['LId'];
		$prod_list_row=db::get_one('orders_products_list', "LId={$LId}");
		file::del_file($prod_list_row['PicPath']);//删除订单产品图片
		$OrderId=$prod_list_row['OrderId'];
		$orders_row=db::get_one('orders', "OrderId='$OrderId'", 'OId');
		db::delete('orders_products_list', "LId={$LId}");
		$ProductPrice=0;
		$prod_row=db::get_all('orders_products_list', "OrderId='$OrderId'", 'Price, PropertyPrice, Discount, Qty');
		foreach($prod_row as $v){
			$ProductPrice+=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1)*$v['Qty'];
		}
		db::update('orders', "OrderId='$OrderId'", array('ProductPrice'=>(float)$ProductPrice, 'UpdateTime'=>$c['time']));
		manage::operation_log('删除订单产品 OId:'.$orders_row['OId']);
		ly200::e_json('', 1);
	}
	
	public static function waybill_orders_products(){//获取相应的拆单内容（运单管理）
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId		= (int)$p_OrderId;
		$p_OvId			=(int)$p_OvId;
		$order_list_row	= db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$p_OrderId}' and o.OvId='{$p_OvId}'", 'o.*, p.PicPath_0', 'o.OvId asc, o.LId asc');
		$ReturnData		= array();
		$pro_ary=$lid_ary=$pro_qty_ary=array();
		foreach($order_list_row as $v){
			$lid_ary[]=$v['LId'];
			$pro_ary[$v['LId']]=$v;//订单产品信息
			$pro_qty_ary[$v['LId']]=$v['Qty'];
		}
		$orders_waybill_row=db::get_all('orders_waybill', "OrderId='{$p_OrderId}'");
		foreach($orders_waybill_row as $v){
			$ProInfo=str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode');
			foreach($ProInfo as $k2=>$v2){//$k2==LId $v2==QTY
				if(in_array($k2, $lid_ary)){//是同一个发货地的产品
					$pro_qty_ary[$k2]-=$v2;
				}
			}
		}
		foreach($pro_ary as $k=>$v){
			if($pro_qty_ary[$v['LId']]>0){//还有数量
				$v['Qty']=$pro_qty_ary[$v['LId']];
				$ReturnData[]=$v;
			}
		}
		ly200::e_json($ReturnData, 1);
	}
	
	public static function waybill_shipped(){ //提交运单号（运单管理）
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId=(int)$p_OrderId;
		$p_OvId=(int)$p_OvId;
		$order_row=db::get_one('orders', "OrderId='{$p_OrderId}'");
		if($order_row){
			if(!$p_TrackingNumber || !$p_ShippingTime){//不能没有运单号和发货时间
				ly200::e_json('', 0);
			}
			$data=array();
			$IsCompleted=0;
			$total_count=(int)db::get_row_count('orders_waybill', "OrderId='{$p_OrderId}'");
			$completed_count=(int)db::get_row_count('orders_waybill', "OrderId='{$p_OrderId}' and Status=1");
			if($p_Number=='00'){ //默认
				if($order_row['ShippingExpress']=='' && $order_row['ShippingMethodSId']==0 && $order_row['ShippingMethodType']==''){ //多个发货地
					$TrackingNumber=str::json_data(htmlspecialchars_decode($order_row['OvTrackingNumber']), 'decode');
					$ShippingTime=str::json_data(htmlspecialchars_decode($order_row['OvShippingTime']), 'decode');
					$Remarks=str::json_data(htmlspecialchars_decode($order_row['OvRemarks']), 'decode');
					$ShippingStatus=str::json_data(htmlspecialchars_decode($order_row['OvShippingStatus']), 'decode');
					$TrackingNumber[$p_OvId]=trim($p_TrackingNumber);
					$ShippingTime[$p_OvId]=strtotime($p_ShippingTime);
					$Remarks[$p_OvId]=$p_Remarks;
					$ShippingStatus[$p_OvId]=1;
					$data['OvTrackingNumber']=addslashes(str::json_data($TrackingNumber));
					$data['OvShippingTime']=addslashes(str::json_data($ShippingTime));
					$data['OvRemarks']=addslashes(str::json_data($Remarks));
					$data['OvShippingStatus']=addslashes(str::json_data($ShippingStatus));
				}else{ //单个发货地
					$data['TrackingNumber']=addslashes(trim($p_TrackingNumber));
					$data['ShippingTime']=strtotime($p_ShippingTime);
					$data['Remarks']=addslashes($p_Remarks);
				}
				if($total_count==0 || ($total_count>0 && $total_count==$completed_count)){ //没有分单 或者 其余的分单都已发货
					$IsCompleted=1;
				}
				$data['UpdateTime']=$c['time'];
				db::update('orders', "OrderId='{$p_OrderId}'", $data);
			}else{ //分单
				$data['TrackingNumber']=addslashes(trim($p_TrackingNumber));
				$data['ShippingTime']=strtotime($p_ShippingTime);
				$data['Remarks']=addslashes($p_Remarks);
				$data['Status']=1;
				$orders_waybill_row=db::get_one('orders_waybill', "OrderId='{$p_OrderId}' and Number='{$p_Number}'");
				db::update('orders_waybill', "WId='{$orders_waybill_row['WId']}'", $data);
				if($order_row['ShippingExpress']=='' && $order_row['ShippingMethodSId']==0 && $order_row['ShippingMethodType']==''){ //多个发货地
					$ShippingOvSId=str::json_data(htmlspecialchars_decode($order_row['ShippingOvSId']), 'decode');
					$TrackingNumber=str::json_data(htmlspecialchars_decode($order_row['OvTrackingNumber']), 'decode');
					$total_count+=count($ShippingOvSId);
					foreach($ShippingOvSId as $k=>$v){
						if(trim($TrackingNumber[$k])){//发货地已发货
							$completed_count+=1;
						}
					}
				}else{ //单个发货地
					$total_count+=1;
					$TrackingNumber=addslashes(trim($order_row['TrackingNumber']));//发货地已发货
					$TrackingNumber && $completed_count+=1;
				}
				if($total_count==$completed_count+1){ //其余的分单都已发货
					$IsCompleted=1;
				}
			}
			if($order_row['OrderStatus']==6 && $IsCompleted==1){
				db::update('orders', "OrderId='{$p_OrderId}'", array('OrderStatus'=>7, 'UpdateTime'=>$c['time']));
				$Log='Update order status from '.$c['orders']['status'][6].' to '.$c['orders']['status'][7];
				orders::orders_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $p_OrderId, 5, $Log, 1);
				/******************** 发邮件 ********************/
				$orders_row=$order_row; //传递给新的变量
				$ToAry=array($orders_row['Email']);
				// $c['manage']['config']['AdminEmail'] && $ToAry[]=$c['manage']['config']['AdminEmail'];
				if((int)db::get_value('system_email_tpl', "Template='order_shipped'", 'IsUsed')){//订单发货
					$trackingNumberStr=trim($p_TrackingNumber);
					$ShippingTimeStr=strtotime($p_ShippingTime);
					include($c['root_path'].'/static/static/inc/mail/order_shipped.php');
					ly200::sendmail($ToAry, $mail_title, $mail_contents);
				}
				/******************** 发邮件结束 ********************/
				manage::operation_log('更新订单状态');
			}
		}
		
		ly200::e_json('', 1);
	}
	
	public static function waybill_separate(){ //订单运单设置（运单管理）
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_OrderId=(int)$p_OrderId;
		$order_row=db::get_one('orders', "OrderId='{$p_OrderId}'");
		if($order_row){
			if($p_Number=='00'){ //未处理
				if(!$p_Waybill) ly200::e_json('', 0);
				$orders_waybill_row=db::get_all('orders_waybill', "OrderId='{$p_OrderId}'");
				foreach($orders_waybill_row as $v){
					$ProInfo=str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode');
					foreach($ProInfo as $k2=>$v2){//$k2==LId $v2==QTY
						$pro_qty_ary[$k2]-=$v2;
					}
				}
				foreach($p_Waybill as $k=>$v){
					if((int)$v<1) unset($p_Waybill[$k]);//没有产品数量
					$prod_row=db::get_one('orders_products_list', "OrderId='{$p_OrderId}' and LId='{$k}'");
					$Qty=$prod_row['Qty'];
					$pro_qty_ary[$k] && $Qty=$Qty+$pro_qty_ary[$k];
					$v>$Qty &&  $p_Waybill[$k]=$Qty;
					db::update('orders_products_list', "OrderId='{$p_OrderId}' and Status=0 and LId='{$k}'", array('Status'=>1));
				}
				if(!$p_Waybill) ly200::e_json('', 0);
				$ProInfo=addslashes(str::json_data(str::str_code($p_Waybill, 'stripslashes')));
				$max=(int)db::get_max('orders_waybill', "OrderId='{$p_OrderId}'", 'Number');
				if($max){
					$Num=$max+1;
				}else{ //新创建
					$Num=1;
				}
				$Num<10 && $Num='0'.$Num;
				$data=array(
					'OrderId'	=>	$p_OrderId,
					'Number'	=>	$Num,
					'ProInfo'	=>	$ProInfo
				);
				db::insert('orders_waybill', $data);
			}else{ //已处理
				$orders_waybill_row=db::get_one('orders_waybill', "OrderId='{$p_OrderId}' and Number='{$p_Number}'");
				db::delete('orders_waybill', "WId='{$orders_waybill_row['WId']}'");
			}
		}
		ly200::e_json('', 1);
	}
	
	//订单资料批量操作 Start
	public static function orders_import(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;//当前分开数
		$p_Worksheet=(int)$p_Worksheet;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		$StatusAry=array();
		!file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json('文件不存在！');
		
		$objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
		$sheet=$objPHPExcel->getSheet(0);//工作表0
		$highestRow=$sheet->getHighestRow();//取得总行数 
		$highestColumn=$sheet->getHighestColumn();//取得总列数
		


		//初始化第一阶段
		$Start=0;//开始执行位置
		$page_count=20;//每次分开导入的数量
		$total_pages=ceil(($highestRow-1)/$page_count);
		if($p_Number<$total_pages){//继续执行
			$Start=$page_count*$p_Number;
		}else{
			file::del_file($p_ExcelFile);
			manage::operation_log('订单批量上传快递');
			ly200::e_json('批量上传完成', 1);
		}
		//初始化第二阶段
		$all_orders_row=str::str_code(db::get_all('orders', 'OrderStatus=4', 'OId,OrderId', 'OrderId desc'));
		$orderid_where = '0';
		$all_order_ary = $all_order_id_ary = $pro_ary = $order_list_ary = $package_ary = array();
		foreach ((array)$all_orders_row as $v) {
			$all_order_id_ary[]=$v['OId'];
			$all_order_ary[$v['OId']]=$v['OrderId'];
			$orderid_where .= ','.$v['OrderId'];
		}
		$orders_products_row = db::get_all('orders_products_list', "OrderId in($orderid_where)", 'LId,OrderId,OvId', 'OvId asc');
		foreach ((array)$orders_products_row as $v) {
			$pro_ary[$v['LId']] = $v;
			$order_list_ary[$v['OrderId']][$v['OvId']][0]='00';
		}
		$orders_waybill_row = db::get_all('orders_waybill', "OrderId in($orderid_where)", 'OrderId,Number,ProInfo');
		foreach ((array)$orders_waybill_row as $v) {
			$ProInfo = str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode');
			foreach ($ProInfo as $k2 => $v2) { //$k2==LId $v2==QTY
				$ovid = $pro_ary[$k2]['OvId'];
				if (!@in_array($v['Number'], (array)$order_list_ary[$v['OrderId']][$ovid])) $order_list_ary[$v['OrderId']][$ovid][] = $v['Number'];
			}
		}
		foreach((array)$order_list_ary as $k => $v){
			$i = 1;
			foreach ((array)$v as $k1 => $v1) {
				foreach ((array)$v1 as $k2 => $v2) {
					$package_ary[$k][$i] = array($v2, $k1, );
					$i++;
				}
			}
		}
		//内容转换为数组 
		$data=$sheet->toArray();
		$data_ary=array();
		$i=-1;
		foreach($data as $k=>$v){//行
			if($k<1) continue;
			if($Start<=$k && $k<($Start+$page_count)){
				if($v[0]){
					$data_ary[]=$v;
				}
			}elseif($k>=($Start+$page_count)){
				break;
			}
		}
		unset($data, $all_orders_row);
		//开始导入
		$No=0;
		$update_sql=array();
		foreach((array)$data_ary as $key=>$val){
			$Package = (int)$val[4] ? (int)$val[4] : 1;
			$OId=trim($val[0]); //订单号
			$TrackingNumber=trim($val[1]); //运单号
			if(!in_array($OId, $all_order_id_ary)){
				$StatusAry[]=array(
					'OId'		=>	$OId,
					'TN'		=>	$TrackingNumber,
					'Package'	=>	$Package,
					'Status'	=>	0,
					'Tips'		=>	'上传失败(不属于“等待发货”状态)',
				);
				continue;
			}
			if(!$TrackingNumber){
				$StatusAry[]=array(
					'OId'		=>	$OId,
					'TN'		=>	$TrackingNumber,
					'Package'	=>	$Package,
					'Status'	=>	0,
					'Tips'		=>	'上传失败(运单号为空)',
				);
				continue;
			}
			$ShippingTime=trim($val[2]); //发货下单时间
			$ShippingTime=@strtotime($ShippingTime);
			if(!$ShippingTime){
				$StatusAry[]=array(
					'OId'		=>	$OId,
					'TN'		=>	$TrackingNumber,
					'Package'	=>	$Package,
					'Status'	=>	0,
					'Tips'		=>	'上传失败(发货下单时间为空)',
				);
				continue;
			}
			$Remarks=trim($val[3]); //备注内容
			$OrderId=$all_order_ary[$OId];
			if (!$package_ary[$OrderId][$Package]) continue;
			$change_orderstatus = 0;
			$Number = $package_ary[$OrderId][$Package][0]; //包裹编号
			$OvId = $package_ary[$OrderId][$Package][1]; //包裹海外仓
			if (count($order_list_ary[$OrderId])>1) {
				//多个海外仓
				if ($Number == '00') {
					$orders_one = str::str_code(db::get_one('orders', "OrderId='{$OrderId}'", 'OvShippingStatus,OvTrackingNumber,OvShippingTime,OvRemarks'));
					$OvShippingStatus = @str::json_data(htmlspecialchars_decode($orders_one['OvShippingStatus']), 'decode');
					$OvTrackingNumber = @str::json_data(htmlspecialchars_decode($orders_one['OvTrackingNumber']), 'decode');
					$OvShippingTime = @str::json_data(htmlspecialchars_decode($orders_one['OvShippingTime']), 'decode');
					$OvRemarks = @str::json_data(htmlspecialchars_decode($orders_one['OvRemarks']), 'decode');
					foreach ((array)$order_list_ary[$OrderId] as $k =>$v) {
						if ($OvId == $k) {
							$OvShippingStatus[$k] = 1;
							$OvTrackingNumber[$k] = $TrackingNumber;
							$OvShippingTime[$k] = $ShippingTime;
							$OvRemarks[$k] = $Remarks;
						}
					}
					ksort($OvShippingStatus);
					ksort($OvTrackingNumber);
					ksort($OvShippingTime);
					ksort($OvRemarks);
					$OvShippingStatus = str::json_data($OvShippingStatus);
					$OvTrackingNumber = str::json_data($OvTrackingNumber);
					$OvShippingTime = str::json_data($OvShippingTime);
					$OvRemarks = str::json_data($OvRemarks);
					$data=array(
						'OvShippingStatus'	=>	addslashes($OvShippingStatus),
						'OvTrackingNumber'	=>	addslashes($OvTrackingNumber),
						'OvShippingTime'	=>	addslashes($OvShippingTime),
						'OvRemarks'			=>	addslashes($OvRemarks),
						'UpdateTime'		=>	$c['time'],
					);
					db::update('orders', "OrderId='{$OrderId}'", $data);
				} else {
					$data=array(
						'TrackingNumber'	=>	addslashes($TrackingNumber),
						'ShippingTime'		=>	$ShippingTime,
						'Remarks'			=>	addslashes($Remarks),
						'Status'			=>	1,
					);
					db::update('orders_waybill', "OrderId='{$OrderId}' and Number='{$Number}'", $data);
				}
				$NewOvTrackingNumber = @str::json_data(db::get_value('orders', "OrderId='{$OrderId}'", 'OvTrackingNumber'), 'decode');
				$track_null = 0;
				foreach ((array)$order_list_ary[$OrderId] as $k =>$v) {
					if (!$NewOvTrackingNumber[$k]) {
						$track_null = 1;
					}
				}
				if (!db::get_row_count('orders_waybill', "OrderId='{$OrderId}' and Status=0") && $track_null == 0) { //修改订单状态
					$change_orderstatus = 1;
				}
			} else {
				//单个海外仓
				//记录数据资料
				$data=array(
					'TrackingNumber'	=>	addslashes($TrackingNumber),
					'ShippingTime'		=>	$ShippingTime,
					'Remarks'			=>	addslashes($Remarks),
				);
				if ($Number == '00') {
					$data['UpdateTime'] = $c['time'];
					db::update('orders', "OrderId='{$OrderId}'", $data);
				} else {
					$data['Status'] = 1;
					db::update('orders_waybill', "OrderId='{$OrderId}' and Number='{$Number}'", $data);
				}
				if (!db::get_row_count('orders_waybill', "OrderId='{$OrderId}' and Status=0") && db::get_value('orders', "OrderId='{$OrderId}'", 'TrackingNumber')) { //修改订单状态
					$change_orderstatus = 1;
				}
			}

			$StatusAry[]=array(
				'OId'		=>	$OId,
				'TN'		=>	$TrackingNumber,
				'Package'	=>	$Package,
				'Status'	=>	1,
				'Tips'	=>	'上传成功',
			);
			++$No;
			if($change_orderstatus){
				db::update('orders', "OrderId='{$OrderId}'", array('OrderStatus' => 5));
				orders::orders_log((int)$_SESSION['Manage']['SalesId'], $_SESSION['Manage']['UserName'], $OrderId, 5, 'Update order status from Awaiting Shipping to Shipment Shipped', 1);
				/******************** 发邮件 ********************/
				if((int)db::get_value('system_email_tpl', "Template='order_shipped'", 'IsUsed')){ //邮件通知开关【订单发货】
					$orders_row=db::get_one('orders', "OrderId='{$OrderId}'");
					if(!$orders_row) continue;
					$OId=$orders_row['OId'];
					$ToAry=array($orders_row['Email']);
					// $c['manage']['config']['AdminEmail'] && $ToAry[]=$c['manage']['config']['AdminEmail'];
					include($c['root_path'].'/static/static/inc/mail/order_shipped.php');
					ly200::sendmail($ToAry, $mail_title, $mail_contents);
				}
				/******************** 发邮件结束 ********************/
			}

		}
		unset($all_order_ary, $all_order_id_ary);
		if($p_Number<$total_pages){//继续执行
			$item=($No+1<$page_count)?($page_count*$p_Number+$No):($page_count*($p_Number+1));
			ly200::e_json(array(($p_Number+1), $StatusAry), 2);
		}
	}
	
	public static function import_excel_download(){
		global $c;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		$orders_row=str::str_code(db::get_all('orders', 'OrderStatus=4', 'OrderId,OId', 'OrderId desc'));
		$orderid_where = '0';
		$oid_ary = $pro_ary = $order_list_ary = array();
		foreach ((array)$orders_row as $v) {
			$oid_ary[$v['OrderId']] = $v['OId'];
			$orderid_where .= ','.$v['OrderId'];
		}
		$orders_products_row = db::get_all('orders_products_list', "OrderId in($orderid_where)", 'LId,OrderId,OvId', 'OrderId desc');
		foreach ((array)$orders_products_row as $v) {
			$pro_ary[$v['LId']] = $v;
			$order_list_ary[$v['OrderId']][$v['OvId']][0]='00';
		}
		$orders_waybill_row = db::get_all('orders_waybill', "OrderId in($orderid_where)", 'OrderId,Number,ProInfo');
		foreach ((array)$orders_waybill_row as $v) {
			$ProInfo = str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode');
			foreach ($ProInfo as $k2 => $v2) { //$k2==LId $v2==QTY
				$ovid = $pro_ary[$k2]['OvId'];
				if (!@in_array($v['Number'], (array)$order_list_ary[$v['OrderId']][$ovid])) $order_list_ary[$v['OrderId']][$ovid][] = $v['Number'];
			}
		}
		$objPHPExcel=new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
		$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
		$objPHPExcel->getProperties()->setCategory("Test result file");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $c['manage']['lang_pack']['orders']['oid']);
		$objPHPExcel->getActiveSheet()->setCellValue('B1', $c['manage']['lang_pack']['orders']['shipping']['track_no']);
		$objPHPExcel->getActiveSheet()->setCellValue('C1', $c['manage']['lang_pack']['orders']['info']['delivery_time'].' ('.date('Y/m/d H:i', $c['time']).')');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', $c['manage']['lang_pack']['orders']['payment']['contents']);
		$objPHPExcel->getActiveSheet()->setCellValue('E1', $c['manage']['lang_pack']['orders']['shipping']['package']);
		
		$i=2;
		foreach((array)$order_list_ary as $k => $v){
			ksort($v);
			$order_list_ary[$k] = $v;
		}
		foreach((array)$order_list_ary as $k => $v){
			$j=1;
			foreach((array)$v as $k1 => $v1){
				foreach((array)$v1 as $k2 => $v2){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $oid_ary[$k], PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $j);
					++$i;
					++$j;
				}
			}
		}
		
		//设置列的宽度  
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		
		//设置行的高度
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		
		$objPHPExcel->getActiveSheet()->setTitle('导入快递');
		$objPHPExcel->setActiveSheetIndex(0);
		
		//保存Excel文件
		$ExcelName='order_export_'.str::rand_code();
		$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($c['root_path']."/tmp/{$ExcelName}.xls");
		
		file::down_file("/tmp/{$ExcelName}.xls");
		file::del_file("/tmp/{$ExcelName}.xls");
		unset($c, $objPHPExcel, $ary, $attr_column);
		exit;
	}
	
	public static function orders_export(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;
		$p_TimeType=(int)$p_TimeType;
		$p_Status=(int)$p_Status;
		if(!$p_Number) unset($_SESSION['OrderZip']);
		if($p_Time){
			$Time_ary=@explode('/', $p_Time);
			$StartTime=@strtotime($Time_ary[0]);
			$EndTime=@strtotime($Time_ary[1]);
			$StartTime==$EndTime && $EndTime+=86400;
		}
		
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		//(A ~ EZ)
		$arr=range('A', 'Z');
		$ary=$arr;
		for($i=0; $i<5; ++$i){
			$num=$arr[$i];
			foreach($arr as $v){
				$ary[]=$num.$v;
			}
		}
		
		//所有产品属性
		$attribute_cart_ary=$vid_data_ary=array();
		$attribute_row=str::str_code(db::get_all('products_attribute', '1', "AttrId, Type, Name{$c['manage']['web_lang']}, ParentId, CartAttr, ColorAttr"));
		foreach($attribute_row as $v){
			$attribute_ary[$v['AttrId']]=array(0=>$v['Type'], 1=>$v["Name{$c['manage']['web_lang']}"]);
		}
		$value_row=str::str_code(db::get_all('products_attribute_value', '1', '*', $c['my_order'].'VId asc')); //属性选项
		foreach($value_row as $v){
			$vid_data_ary[$v['AttrId']][$v['VId']]=$v["Value{$c['manage']['web_lang']}"];
		}
		
		//发货地
		$overseas_ary=array();
		$overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
		foreach($overseas_row as $v){
			$overseas_ary[$v['OvId']]=$v;
		}
		
		//Add some data
		$page_count=600;//每次分开导出的数量
		$where='1';
		if($StartTime && $EndTime){
			if((int)$p_TimeType==1) $where.=" and o.PayTime>{$StartTime} and o.PayTime<{$EndTime}";
			elseif((int)$p_TimeType==2) $where.=" and o.ShippingTime>{$StartTime} and o.ShippingTime<{$EndTime}";
			else $where.=" and o.OrderTime>{$StartTime} and o.OrderTime<{$EndTime}";
		}
		$p_Status && $where.=" and o.OrderStatus='$p_Status'";
		(int)$_SESSION['Manage']['GroupId']==3 && $where.=" and ((o.SalesId>0 and o.SalesId='{$_SESSION['Manage']['UserId']}') or (o.SalesId=0 and u.SalesId='{$_SESSION['Manage']['UserId']}'))";//业务员账号过滤
		$p_OrderId && $where.=" and OrderId in($p_OrderId)";
		$row_count=db::get_row_count('orders o left join user u on o.UserId=u.UserId', $where, 'o.UserId');
		$total_pages=ceil($row_count/$page_count);
		$zipAry=array();//储存需要压缩的文件
		$save_dir='/tmp/';//临时储存目录
		file::mk_dir($save_dir);
		
		//$menu_ary=array('A'=>'订单ID', 'B'=>'订单号', 'C'=>'邮箱', 'D'=>'产品总额', 'E'=>'运费', 'F'=>'保险费', 'G'=>'订单总额', 'H'=>'总重量', 'I'=>'总体积', 'J'=>'订单状态', 'K'=>'配送方式', 'L'=>'付款方式', 'M'=>'时间', 'N'=>'优惠券', 'O'=>'收货姓名', 'P'=>'收货地址', 'Q'=>'收货地址2', 'R'=>'收货国家', 'S'=>'收货省份', 'T'=>'收货城市', 'U'=>'收货邮编', 'V'=>'收货电话', 'W'=>'账单姓名', 'X'=>'账单地址', 'Y'=>'账单地址2', 'Z'=>'账单国家', 'AA'=>'账单省份', 'AB'=>'账单城市', 'AC'=>'账单邮编', 'AD'=>'账单电话', 'AE'=>'运单号', 'AF'=>'备注内容', 'AG'=>'产品名称', 'AH'=>'产品编号', 'AI'=>'产品数量', 'AJ'=>'产品SKU');
		$menu_ary=array('A'=>$c['manage']['lang_pack']['orders']['orders'].' ID', 'B'=>$c['manage']['lang_pack']['manage']['manage']['permit_name'][3], 'C'=>$c['manage']['lang_pack']['orders']['oid'], 'D'=>$c['manage']['lang_pack']['global']['email'], 'E'=>$c['manage']['lang_pack']['orders']['info']['product_price'], 'F'=>$c['manage']['lang_pack']['orders']['info']['charges'], 'G'=>$c['manage']['lang_pack']['orders']['info']['insurance'], 'H'=>$c['manage']['lang_pack']['orders']['total_price'], 'I'=>$c['manage']['lang_pack']['orders']['info']['weight'], 'J'=>$c['manage']['lang_pack']['orders']['info']['volume'], 'K'=>$c['manage']['lang_pack']['orders']['orders_status'], 'L'=>$c['manage']['lang_pack']['orders']['info']['ship_info'], 'M'=>$c['manage']['lang_pack']['orders']['payment_method'], 'N'=>$c['manage']['lang_pack']['orders']['time'], 'O'=>$c['manage']['lang_pack']['orders']['info']['coupon'], 'P'=>$c['manage']['lang_pack']['orders']['export']['shipname'], 'Q'=>$c['manage']['lang_pack']['orders']['export']['shipaddress'], 'R'=>$c['manage']['lang_pack']['orders']['export']['shipaddress2'], 'S'=>$c['manage']['lang_pack']['orders']['export']['shipcountry'], 'T'=>$c['manage']['lang_pack']['orders']['export']['shipstate'], 'U'=>$c['manage']['lang_pack']['orders']['export']['shipcity'], 'V'=>$c['manage']['lang_pack']['orders']['export']['shipzip'], 'W'=>$c['manage']['lang_pack']['orders']['export']['shipphone'], 'X'=>$c['manage']['lang_pack']['orders']['export']['billname'], 'Y'=>$c['manage']['lang_pack']['orders']['export']['billaddress'], 'Z'=>$c['manage']['lang_pack']['orders']['export']['billaddress2'], 'AA'=>$c['manage']['lang_pack']['orders']['export']['billcountry'], 'AB'=>$c['manage']['lang_pack']['orders']['export']['billstate'], 'AC'=>$c['manage']['lang_pack']['orders']['export']['billcity'], 'AD'=>$c['manage']['lang_pack']['orders']['export']['billzip'], 'AE'=>$c['manage']['lang_pack']['orders']['export']['billphone'], 'AF'=>$c['manage']['lang_pack']['orders']['shipping']['track_no'], 'AG'=>$c['manage']['lang_pack']['orders']['payment']['contents'], 'AH'=>$c['manage']['lang_pack']['orders']['export']['proname'], 'AI'=>$c['manage']['lang_pack']['orders']['export']['pronumber'], 'AJ'=>$c['manage']['lang_pack']['orders']['export']['proqty'], 'AK'=>$c['manage']['lang_pack']['orders']['export']['prosku'], 'AL'=>$c['manage']['lang_pack']['orders']['export']['proattr'],'AM'=>'产品单价');
		$menu_row=str::str_code(db::get_value('config', 'GroupId="orders_export" and Variable="Menu"', 'Value'));
		$menu_value=str::json_data(htmlspecialchars_decode($menu_row), 'decode');
		if($p_Number<$total_pages){
			$page=$page_count*$p_Number;
			$orders_row=str::str_code(db::get_limit('orders o left join user u on o.UserId=u.UserId', $where, 'o.*,u.SalesId as USalesId, o.SalesId as OSalesId, o.Email as OEmail', 'o.OrderId desc', $page, $page_count));
			$objPHPExcel=new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
			$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
			$objPHPExcel->getProperties()->setCategory("Test result file");
			$objPHPExcel->setActiveSheetIndex(0);
			$i=0;
			foreach($menu_value as $k=>$v){
				if($v==1){
					$objPHPExcel->getActiveSheet()->setCellValue($ary[$i].'1', $menu_ary[$k]);
					++$i;
				}
			}
			$num=2;
			foreach($orders_row as $val){
				$OrderId=$val['OrderId'];
				$pro_info=array(); //初始化
				if($menu_value['AH']==1 || $menu_value['AI']==1 || $menu_value['AJ']==1 || $menu_value['AK']==1 || $menu_value['A']==1){ //开启产品信息
					$order_list_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$OrderId'", 'o.KeyId, o.BuyType, o.Name, o.Qty, o.Price, o.SKU as OrderSKU, o.Property, o.CId, p.Prefix, p.Number, p.SKU', 'o.LId asc');
					foreach((array)$order_list_row as $k=>$v){
						if($v['BuyType']==4){
							//组合促销
							$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
							if(!$package_row) continue;
							$data_ary=str::json_data($package_row['Data'], 'decode');
							$products_row=str::str_code(db::get_all('products', "SoldOut=0 and ProId='{$package_row['ProId']}'"));
							$pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
							$pro_where=='' && $pro_where=0;
							$products_row=array_merge($products_row, str::str_code(db::get_all('products', "SoldOut=0 and ProId in($pro_where)")));
							// $attr=array();
							// $v['Property']!='' && $attr=str::json_data($v['Property'], 'decode');
							$v['Property']!='' && $attr=ly200::get_order_property($v['Property']);
							foreach((array)$products_row as $k2=>$v2){
								$pro_info['AH'][]='[Sales]'.$v2['Name'.$c['manage']['web_lang']];
								$pro_info['AI'][]=$v2['Prefix'].$v2['Number'];
								$pro_info['AJ'][]=($k2==0?$v['Qty']:'');
								$pro_info['AK'][]=($v2['OrderSKU']?$v2['OrderSKU']:$v2['SKU']);
								$pro_info['CId'][]=$v['CId'];
								if($menu_value['AL']==1){
									$i=0;
									if($k2==0){
										foreach((array)$attr as $k3=>$z){
											if($k3=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
											$pro_info['AL'][]=($k3=='Overseas'?'Ships from':$k3).': '.$z;
											if($i>0) $pro_info['AH'][]=$pro_info['AI'][]=$pro_info['AJ'][]=$pro_info['AK'][]=$pro_info['CId'][]='';
											++$i;
										}
										if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
											$pro_info['AL'][]='Ships from: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']];
										}
									}else{
										$OvId=0;
										foreach((array)$data_ary[$v2['ProId']] as $k3=>$v3){
											if($k3=='Overseas'){ //发货地
												$OvId=str_replace('Ov:', '', $v3);
												if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $OvId==1) continue; //发货地是中国，不显示
												$pro_info['AL'][]='Ships from: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']];
											}else{
												$pro_info['AL'][]=$attribute_ary[$k3][1].': '.$vid_data_ary[$k3][$v3];
											}
											if($i>0) $pro_info['AH'][]=$pro_info['AI'][]=$pro_info['AJ'][]=$pro_info['AK'][]=$pro_info['CId'][]='';
											++$i;
										}
										if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $OvId==1){
											$pro_info['AL'][]='Ships from: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']];
										}
									}
								}
                                $pro_info['AM'][]=($k2==0?$v['Price']:'');
							}
						}else{
							$attr=str::json_data($v['Property'], 'decode');
							$pro_info['AH'][]=$v['Name'];
							$pro_info['AI'][]=$v['Prefix'].$v['Number'];
							$pro_info['AJ'][]=$v['Qty'];
							$pro_info['AK'][]=($v['OrderSKU']?$v['OrderSKU']:$v['SKU']);
							$pro_info['CId'][]=$v['CId'];
							if($menu_value['AL']==1){
								$i=0;
								foreach((array)$attr as $k2=>$v2){
									if($k2=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
									$pro_info['AL'][]=($k2=='Overseas'?'Ships from':$k2).': '.$v2;
									if($i>0) $pro_info['AH'][]=$pro_info['AI'][]=$pro_info['AJ'][]=$pro_info['AK'][]=$pro_info['CId'][]='';
									++$i;
								}
								if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
									$pro_info['AL'][]='Ships from: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']];
								}
							}
                            $pro_info['AM'][]=$v['Price'];
						}
					}
				}else{
					$pro_info=array('AH'=>array(0=>''), 'AI'=>array(0=>''), 'AJ'=>array(0=>''), 'AK'=>array(0=>''), 'AL'=>array(0=>''), 'AM'=>array(0=>''));
				}
				//业务员
				$Sales='';
				$SalesId=($val['OSalesId']?$val['OSalesId']:$val['USalesId']);
				$SalesId && $Sales=db::get_value('manage', "UserId='{$SalesId}'", 'UserName');
				//收货信息
				$paypal_address_row=str::str_code(db::get_one('orders_paypal_address_book', "OrderId='$OrderId' and IsUse=1"));
				if($paypal_address_row){//Paypal账号收货地址
					$shipto_ary=array(
						'FirstName'			=>	$paypal_address_row['FirstName'],
						'LastName'			=>	$paypal_address_row['LastName'],
						'AddressLine1'		=>	$paypal_address_row['AddressLine1'],
						'AddressLine2'		=>	'',
						'City'				=>	$paypal_address_row['City'],
						'State'				=>	$paypal_address_row['State'],
						'Country'			=>	$paypal_address_row['Country'],
						'ZipCode'			=>	$paypal_address_row['ZipCode'],
						'CountryCode'		=>	$paypal_address_row['CountryCode'],
						'PhoneNumber'		=>	$paypal_address_row['PhoneNumber']
					);
				}else{//会员账号收货地址
					$shipto_ary=array(
						'FirstName'			=>	$val['ShippingFirstName'],
						'LastName'			=>	$val['ShippingLastName'],
						'AddressLine1'		=>	$val['ShippingAddressLine1'],
						'AddressLine2'		=>	$val['ShippingAddressLine2'],
						'City'				=>	$val['ShippingCity'],
						'State'				=>	$val['ShippingState'],
						'Country'			=>	$val['ShippingCountry'],
						'ZipCode'			=>	$val['ShippingZipCode'],
						'CountryCode'		=>	$val['ShippingCountryCode'],
						'PhoneNumber'		=>	$val['ShippingPhoneNumber']
					);
				}
				//快递信息
				$Express_ary=$TrackingNumber_ary=$Remarks_ary=array();
				if(($val['ShippingExpress']=='' && $val['ShippingMethodSId']==0 && $val['ShippingMethodType']=='') || $val['shipping_template']){ //多个发货地
					$shipping_ary=array(
						'ShippingExpress'		=>	str::json_data(htmlspecialchars_decode($val['ShippingOvExpress']), 'decode'),
						'ShippingSId'			=>	str::json_data(htmlspecialchars_decode($val['ShippingOvSId']), 'decode'),
						'ShippingType'			=>	str::json_data(htmlspecialchars_decode($val['ShippingOvType']), 'decode'),
						'TrackingNumber'		=>	str::json_data(htmlspecialchars_decode($val['OvTrackingNumber']), 'decode'),
						'Remarks'				=>	str::json_data(htmlspecialchars_decode($val['OvRemarks']), 'decode')
					);
					foreach($shipping_ary['ShippingSId'] as $k=>$v){
						$Express_ary[]=$shipping_ary['ShippingExpress'][$k];
						if($shipping_ary['TrackingNumber'][$k]) $TrackingNumber_ary[]=$shipping_ary['TrackingNumber'][$k];
						if($shipping_ary['Remarks'][$k]) $Remarks_ary[]=$shipping_ary['Remarks'][$k];
					}
				}else{ //单个发货地
					$shipping_cfg=(int)$val['ShippingMethodSId']?db::get_one('shipping', "SId='{$val['ShippingMethodSId']}'"):db::get_one('shipping_config', "Id='1'");
					$Express_ary[]=(int)$val['ShippingMethodSId']?$shipping_cfg['Express']:($val['ShippingMethodType']=='air'?$shipping_cfg['AirName']:$shipping_cfg['OceanName']);
					$TrackingNumber_ary[]=$val['TrackingNumber'];
					$Remarks_ary[]=$val['Remarks'];
				}
				$orders_waybill_row=db::get_all('orders_waybill', "OrderId='{$OrderId}'");
				foreach($orders_waybill_row as $v){
					$TrackingNumber_ary[]=$v['TrackingNumber'];
					$Remarks_ary[]=$v['Remarks'];
				}
				
				$value_ary=array(
					'A'=>$OrderId,
					'B'=>$Sales,//业务员
					'C'=>$val['OId'],
					'D'=>$val['OEmail'],
					'E'=>$c['manage']['currency_symbol'].sprintf('%01.2f', $val['ProductPrice']),
					'F'=>$c['manage']['currency_symbol'].sprintf('%01.2f', $val['ShippingPrice']),
					'G'=>$c['manage']['currency_symbol'].sprintf('%01.2f', $val['ShippingInsurancePrice']),
					'H'=>$c['manage']['currency_symbol'].sprintf('%01.2f', orders::orders_price($val, 0, 1)),
					'I'=>$val['TotalWeight'],
					'J'=>$val['TotalVolume'],
					'K'=>$c['orders']['status'][$val['OrderStatus']],
					//'K'=>(int)$val['ShippingMethodSId']?$shipping_cfg['Express']:($val['ShippingMethodType']=='air'?$shipping_cfg['AirName']:$shipping_cfg['OceanName']),
					'L'=>implode('|', $Express_ary),
					'M'=>$val['PaymentMethod'],
					'N'=>date('Y-m-d H:i:s', $val['OrderTime']),
					'O'=>$val['CouponCode']?$val['CouponCode'].' '.($val['CouponPrice']?$c['manage']['currency_symbol'].sprintf('%01.2f', $val['CouponPrice']):$val['CouponDiscount'].'%'):'N/A',
					'P'=>$shipto_ary['FirstName'].' '.$shipto_ary['LastName'],
					'Q'=>$shipto_ary['AddressLine1'],
					'R'=>$shipto_ary['AddressLine2'],
					'S'=>$shipto_ary['Country'],
					'T'=>$shipto_ary['State'],
					'U'=>$shipto_ary['City'],
					'V'=>$shipto_ary['ZipCode'],
					'W'=>$shipto_ary['CountryCode'].' '.$shipto_ary['PhoneNumber'],
					'X'=>$val['BillFirstName'].' '.$val['BillLastName'],
					'Y'=>$val['BillAddressLine1'],
					'Z'=>$val['BillAddressLine2'],
					'AA'=>$val['BillCountry'],
					'AB'=>$val['BillState'],
					'AC'=>$val['BillCity'],
					'AD'=>$val['BillZipCode'],
					'AE'=>$val['BillCountryCode'].' '.$val['BillPhoneNumber'],
					'AF'=>implode('|', $TrackingNumber_ary),
					'AG'=>implode('|', $Remarks_ary),
					'AH'=>$pro_info['AH'],
					'AI'=>$pro_info['AI'],
					'AJ'=>$pro_info['AJ'],
					'AK'=>$pro_info['AK'],
                    'AL'=>$pro_info['AL'],
                    'AM'=>$pro_info['AM'],
				);
				if($val['shipping_template']){
					$value_ary['L']=array();
					$value_ary['AF']=array();
					$value_ary['AG']=array();
					foreach((array)$pro_info['AH'] as $k=>$v){
						if($v){
							$value_ary['L'][]=$shipping_ary['ShippingExpress'][$pro_info['CId'][$k]];
							$value_ary['AF'][]=$shipping_ary['TrackingNumber'][$pro_info['CId'][$k]];
							$value_ary['AG'][]=$shipping_ary['Remarks'][$pro_info['CId'][$k]];
						}else{
							$value_ary['L'][]='';
							$value_ary['AF'][]='';
							$value_ary['AG'][]='';
						}
					}
				}
				$in_array_ary=array('AH', 'AI', 'AJ', 'AK', 'AL','AM');
				$val['shipping_template'] && $in_array_ary=array('AH', 'AI', 'AJ', 'AK', 'AL','AM', 'L', 'AF', 'AG');
				foreach((array)$pro_info['AH'] as $k=>$v){
					$j=0;
					foreach((array)$menu_value as $kk=>$vv){
						if($vv==1){
							if($kk=='C' || ($kk=='AF' && !is_array($value_ary['AF']))){ //订单号、快递单号								
								$objPHPExcel->getActiveSheet()->setCellValueExplicit($ary[$j].$num, ($k?'':$value_ary[$kk]), PHPExcel_Cell_DataType::TYPE_STRING);
							}elseif(in_array($kk, $in_array_ary)){ //产品信息
								if($kk=='AF'){
									$objPHPExcel->getActiveSheet()->setCellValueExplicit($ary[$j].$num, (is_array($value_ary[$kk])?$value_ary[$kk][$k]:$value_ary[$kk]), PHPExcel_Cell_DataType::TYPE_STRING);
								}else{
									$objPHPExcel->getActiveSheet()->setCellValue($ary[$j].$num, (is_array($value_ary[$kk])?$value_ary[$kk][$k]:$value_ary[$kk]));
								}
							}else{								
								$objPHPExcel->getActiveSheet()->setCellValue($ary[$j].$num, ($k?'':$value_ary[$kk]));
							}
							++$j;
						}
					}
					++$num;
				}
			}
			//设置列的宽度
			$j=0;
			foreach($menu_value as $k=>$v){
				if($v==1){
					$objPHPExcel->getActiveSheet()->getColumnDimension($ary[$j])->setWidth(20);
					++$j;
				}
			}
			
			$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
			$objPHPExcel->getActiveSheet()->setTitle('Simple');
			$objPHPExcel->setActiveSheetIndex(0);
			$ExcelName='orders_'.str::rand_code();
			$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
			$objWriter->save($c['root_path']."{$save_dir}{$ExcelName}.xls");
			$_SESSION['OrderZip'][]="{$save_dir}{$ExcelName}.xls";
			unset($objPHPExcel, $objWriter, $orders_row, $shipping_cfg);
			ly200::e_json(array(($p_Number+1), "{$c['manage']['lang_pack']['global']['export']} {$save_dir}{$ExcelName}.xls<br />"), 2);
		}else{
			if(count($_SESSION['OrderZip'])){
				ly200::e_json('', 1);
			}else{
				ly200::e_json('');
			}
		}
	}
	
	public static function orders_export_down(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		
		if($g_Status=='ok' && count($_SESSION['OrderZip'])){	//开始打包
			$zip=new ZipArchive();
			$zipname='/tmp/orders_'.str::rand_code().'.zip';
			
			if($zip->open($c['root_path'].$zipname, ZIPARCHIVE::CREATE)===TRUE){
				foreach($_SESSION['OrderZip'] as $path){
					if(is_file($c['root_path'].$path)) $zip->addFile($c['root_path'].$path, $path);
				}
				$zip->close();
				file::down_file($zipname);
				file::del_file($zipname);
				foreach($_SESSION['OrderZip'] as $path){
					if(is_file($c['root_path'].$path)) file::del_file($path);
				}
			}
		}
		unset($_SESSION['OrderZip']);
		exit();
	}
	
	public static function orders_export_menu(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data_ary=array();
		$sort_ary=explode('|', $p_sort_order);
		foreach($sort_ary as $v){
			$data_ary[$v]=(in_array($v, $p_Menu)?1:0);
		}
		$MenuData=addslashes(str::json_data(str::str_code($data_ary, 'stripslashes')));
		manage::config_operaction(array('Menu'=>$MenuData), 'orders_export');
		ly200::e_json('', 1);
	}
	
	public static function orders_export_products(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_OrderId=(int)$g_OrderId;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		//Add some data
		//所有汇率资料
		$all_currency_ary=array();
		$currency_row=db::get_all('currency', '1', 'Currency, Symbol');
		foreach($currency_row as $k=>$v){
			$all_currency_ary[$v['Currency']]=$v;
		}
		//订单资料
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$g_OrderId'"));
		$order_list_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$g_OrderId'", 'o.*, p.Prefix, p.Number, p.PicPath_0, p.Business', 'o.LId asc');
		//业务员
		$orders_row['SalesId'] && $Sales=db::get_value('manage', "UserId='{$orders_row['SalesId']}'", 'UserName');
		//供应商资料
		$BId='0';
		foreach($order_list_row as $v){$v['Business'] && $BId.=','.$v['Business'];}
		if($BId!='0'){
			$business_row=str::str_code(db::get_all('business', "BId in($BId)", 'BId,Name,Url,Phone,TelePhone,Address'));
			$BusinessAry=array();
			foreach($business_row as $k=>$v){$BusinessAry[$v['BId']]=$v;}
		}
		//订单价格资料
		$Symbol=$orders_row['ManageCurrency']?$all_currency_ary[$orders_row['ManageCurrency']]['Symbol']:$c['manage']['currency_symbol'];
		$total_price=orders::orders_price($orders_row, 1, 1);
		$HandingFee=$total_price-orders::orders_price($orders_row, 0, 1);
		$UserDiscount=($orders_row['UserDiscount']>0 && $orders_row['UserDiscount']<100) ? $orders_row['UserDiscount'] : 100;
		$total_weight=$orders_row['TotalWeight'];
		//收货地址
		$paypal_address_row=str::str_code(db::get_one('orders_paypal_address_book', "OrderId='$g_OrderId' and IsUse=1"));
		if($paypal_address_row){//Paypal账号收货地址
			$shipto_ary=array(
				'FirstName'			=>	$paypal_address_row['FirstName'],
				'LastName'			=>	$paypal_address_row['LastName'],
				'AddressLine1'		=>	$paypal_address_row['AddressLine1'],
				'AddressLine2'		=>	'',
				'City'				=>	$paypal_address_row['City'],
				'State'				=>	$paypal_address_row['State'],
				'Country'			=>	$paypal_address_row['Country'],
				'ZipCode'			=>	$paypal_address_row['ZipCode'],
				'CountryCode'		=>	$paypal_address_row['CountryCode'],
				'PhoneNumber'		=>	$paypal_address_row['PhoneNumber']
			);
		}else{//会员账号收货地址
			$shipto_ary=array(
				'FirstName'			=>	$orders_row['ShippingFirstName'],
				'LastName'			=>	$orders_row['ShippingLastName'],
				'AddressLine1'		=>	$orders_row['ShippingAddressLine1'],
				'AddressLine2'		=>	$orders_row['ShippingAddressLine2'],
				'City'				=>	$orders_row['ShippingCity'],
				'State'				=>	$orders_row['ShippingState'],
				'Country'			=>	$orders_row['ShippingCountry'],
				'ZipCode'			=>	$orders_row['ShippingZipCode'],
				'CountryCode'		=>	$orders_row['ShippingCountryCode'],
				'PhoneNumber'		=>	$orders_row['ShippingPhoneNumber']
			);
		}
		//快递方式
		$shipping_cfg=(int)$orders_row['ShippingMethodSId']?db::get_one('shipping', "SId='{$orders_row['ShippingMethodSId']}'"):db::get_one('shipping_config', "Id='1'");
		$shipping_row=db::get_one('shipping_area', "AId in(select AId from shipping_country where CId='{$orders_row['ShippingCId']}' and  SId='{$orders_row['ShippingMethodSId']}' and type='{$orders_row['ShippingMethodType']}')");
		
		if($orders_row && $order_list_row){
			$objPHPExcel=new PHPExcel();
			 
			//Set properties 
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
			$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
			$objPHPExcel->getProperties()->setCategory("Test result file");
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			//设置列的宽度
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			
			//第一行
			$objPHPExcel->getActiveSheet()->setCellValue('A1', "{$c['manage']['lang_pack']['orders']['oid']}: {$orders_row['OId']}");
			$objPHPExcel->getActiveSheet()->mergeCells("A1:K1");//合并标题
			$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(30);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A2', $c['manage']['lang_pack']['global']['serial']);
			$objPHPExcel->getActiveSheet()->setCellValue('B2', $c['manage']['lang_pack']['products']['picture']);
			$objPHPExcel->getActiveSheet()->setCellValue('C2', $c['manage']['lang_pack']['orders']['export']['proname']);
			$objPHPExcel->getActiveSheet()->setCellValue('D2', $c['manage']['lang_pack']['orders']['export']['pronumber']);
			$objPHPExcel->getActiveSheet()->setCellValue('E2', $c['manage']['lang_pack']['orders']['export']['prosku']);
			$objPHPExcel->getActiveSheet()->setCellValue('F2', $c['manage']['lang_pack']['products']['attribute']);
			$objPHPExcel->getActiveSheet()->setCellValue('G2', $c['manage']['lang_pack']['orders']['global']['remark']);
			$objPHPExcel->getActiveSheet()->setCellValue('H2', $c['manage']['lang_pack']['products']['products']['business']);
			$objPHPExcel->getActiveSheet()->setCellValue('I2', $c['manage']['lang_pack']['products']['products']['price']);
			$objPHPExcel->getActiveSheet()->setCellValue('J2', $c['manage']['lang_pack']['products']['products']['qty']);
			$objPHPExcel->getActiveSheet()->setCellValue('K2', $c['manage']['lang_pack']['orders']['amount']);
			
			$i=3;
			$prod_qty=$prod_price=0;
			foreach((array)$order_list_row as $k=>$v){
				if($v['BuyType']==4){
					//组合促销
					$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
					if(!$package_row) continue;
					// $attr=array();
					// $v['Property']!='' && $attr=str::json_data($v['Property'], 'decode');
					$v['Property']!='' && $attr=ly200::get_order_property($v['Property']);
					$products_row=str::str_code(db::get_all('products', "SoldOut=0 and ProId='{$package_row['ProId']}'"));
					$pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
					$pro_where=='' && $pro_where=0;
					$products_row=array_merge($products_row, str::str_code(db::get_all('products', "SoldOut=0 and ProId in($pro_where)")));
					$data_ary=str::json_data($package_row['Data'], 'decode');
					$prod_qty+=$v['Qty'];
					$prod_price+=$price*$v['Qty'];
					
					$start=$i;
					foreach((array)$products_row as $k2=>$v2){
						$img=ly200::get_size_img($v2['PicPath_0'], '240x240');
						$jsonData=@str::json_data($BusinessAry[$v2['Business']]);
						$business_html='';
						if($v2['Business']){
							$business_html.=$BusinessAry[$v2['Business']]['Name']."\r\n";
							$business_html.=$c['manage']['lang_pack']['business']['business']['telephone'].': '.$BusinessAry[$v2['Business']]['Phone']."\r\n";
							$business_html.=$c['manage']['lang_pack']['business']['business']['phone'].': '.$BusinessAry[$v2['Business']]['Telephone']."\r\n";
							$business_html.=$c['manage']['lang_pack']['business']['business']['address'].': '.$BusinessAry[$v2['Business']]['Address']."\r\n";
						}
						$jsonProperty=@str::json_data($v2['Property'], 'decode');
						$Property_html='';
						$j=0;
						foreach((array)$jsonProperty as $k3=>$v3){
							if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 && $k3=='Overseas') continue;
							$Property_html.=($j?"\r\n":'').$k3.':'.$v3;
							++$j;
						}
						
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $k+1);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '');
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $v2['Name'.$c['manage']['web_lang']]);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $v2['Prefix'].$v2['Number'], PHPExcel_Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $v2['SKU']);
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $Property_html);
						$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $v2['Remark']);
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $business_html);
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, ($k2==0?$Symbol.sprintf('%01.2f', $v['Price']):''));
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, ($k2==0?$v['Qty']:''));
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, ($k2==0?$Symbol.sprintf('%01.2f', $v['Price']):''));
						
						//添加图片
						if(is_file($c['root_path'].$img)){
							$objDrawing=new PHPExcel_Worksheet_Drawing();
							$objDrawing->setName('ZealImg');
							$objDrawing->setDescription('Image inserted by Zeal');
							$objDrawing->setPath($c['root_path'].$img);
							$objDrawing->setWidth(80);
							$objDrawing->setHeight(80);
							$objDrawing->setCoordinates('B'.$i);
							$objDrawing->setOffsetX(15);
							$objDrawing->setOffsetY(15);
							$objDrawing->getShadow()->setVisible(true);
							$objDrawing->getShadow()->setDirection(36);
							$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						}
						
						$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(80);//设置行的宽度
						$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getAlignment()->setWrapText(true);//自动换行
						$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setWrapText(true);//自动换行
						$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
						
						$end=$i;
						++$i;
					}
					//合并单元格
					if($start<$end){
						$objPHPExcel->getActiveSheet()->mergeCells("I{$start}:I{$end}");
						$objPHPExcel->getActiveSheet()->mergeCells("J{$start}:J{$end}");
						$objPHPExcel->getActiveSheet()->mergeCells("K{$start}:K{$end}");
					}
				}else{
					$LId=$v['LId'];
					$jsonData=@str::json_data($BusinessAry[$v['Business']]);
					$business_html='';
					if($v['Business']){
						$business_html.=$BusinessAry[$v['Business']]['Name']."\r\n";
						$business_html.=$c['manage']['lang_pack']['business']['business']['telephone'].': '.$BusinessAry[$v['Business']]['Phone']."\r\n";
						$business_html.=$c['manage']['lang_pack']['business']['business']['phone'].': '.$BusinessAry[$v['Business']]['Telephone']."\r\n";
						$business_html.=$c['manage']['lang_pack']['business']['business']['address'].': '.$BusinessAry[$v['Business']]['Address']."\r\n";
					}
					$jsonProperty=$v['Property']!=''?@str::json_data($v['Property'], 'decode'):array();
					$Property_html='';
					$j=0;
					foreach((array)$jsonProperty as $k2=>$v2){
						if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 && $k2=='Overseas') continue;
						$Property_html.=($j?"\r\n":'').$k2.':'.$v2;
						++$j;
					}
					$prod_qty+=$v['Qty'];
					$price=$v['Price']+$v['PropertyPrice'];
					$v['Discount']<100 && $price*=$v['Discount']/100;
					$prod_price+=$price*$v['Qty'];
					
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $k+1);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '');
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $v['Name']);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $v['Prefix'].$v['Number'], PHPExcel_Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $v['SKU']);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $Property_html);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $v['Remark']);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $business_html);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $Symbol.sprintf('%01.2f', $price));
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $v['Qty']);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $Symbol.sprintf('%01.2f', $price*$v['Qty']));
					
					//添加图片
					if(is_file($c['root_path'].$v['PicPath'])){
						$objDrawing=new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('ZealImg');
						$objDrawing->setDescription('Image inserted by Zeal');
						$objDrawing->setPath($c['root_path'].$v['PicPath']);
						$objDrawing->setWidth(80);
						$objDrawing->setHeight(80);
						$objDrawing->setCoordinates('B'.$i);
						$objDrawing->setOffsetX(15);
						$objDrawing->setOffsetY(15);
						$objDrawing->getShadow()->setVisible(true);
						$objDrawing->getShadow()->setDirection(36);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					}elseif(is_file($c['root_path'].ly200::get_size_img($v['PicPath_0'], '240x240'))){
						$objDrawing=new PHPExcel_Worksheet_Drawing();
						$objDrawing->setName('ZealImg');
						$objDrawing->setDescription('Image inserted by Zeal');
						$objDrawing->setPath($c['root_path'].ly200::get_size_img($v['PicPath_0'], '240x240'));
						$objDrawing->setWidth(80);
						$objDrawing->setHeight(80);
						$objDrawing->setCoordinates('B'.$i);
						$objDrawing->setOffsetX(15);
						$objDrawing->setOffsetY(15);
						$objDrawing->getShadow()->setVisible(true);
						$objDrawing->getShadow()->setDirection(36);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					}
					
					$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(80);//设置行的宽度
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getAlignment()->setWrapText(true);//自动换行
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setWrapText(true);//自动换行
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
					
					++$i;
				}
			}
			
			//产品总价(相加)
			$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:I{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("J{$i}", $prod_qty);
			$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $prod_price));
			++$i;
			
			//产品总价不一致才显示
			if($prod_price!=$orders_row['ProductPrice']){
				$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
				$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['info']['product_price']);
				$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $orders_row['ProductPrice']));
				$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				++$i;
			}
			
			//运费及保险费
			$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['info']['charges_insurance'].' (+)');
			$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $orders_row['ShippingPrice']+$orders_row['ShippingInsurancePrice']));
			$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			++$i;
			
			//优惠券
			if($orders_row['CouponCode'] && ($orders_row['CouponPrice']>0 || $orders_row['CouponDiscount']>0)){
				$coupon_price=$orders_row['CouponPrice']>0?$orders_row['CouponPrice']:$orders_row['ProductPrice']*$orders_row['CouponDiscount'];
				$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
				$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['info']['coupon'].' (-)');
				$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $coupon_price));
				$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				++$i;
			}
			
			//会员优惠及其他优惠
			if((float)$orders_row['Discount'] > 0 || (float)$orders_row['DiscountPrice'] || $UserDiscount < 100){
				$discount_price=$orders_row['ProductPrice']-($orders_row['ProductPrice']*((100-$orders_row['Discount'])/100)*($UserDiscount/100))+$orders_row['DiscountPrice'];
				$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
				$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['export']['othdiscount'].' (-)');
				$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $discount_price));
				$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				++$i;
			}
			
			//手续费
			if($HandingFee!=0){
				$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
				$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['addfee'].' (+)');
				$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $HandingFee));
				$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				++$i;
			}
			
			//订单总额
			$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:J{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $c['manage']['lang_pack']['orders']['total_price']);
			$objPHPExcel->getActiveSheet()->setCellValue("K{$i}", $Symbol.sprintf('%01.2f', $total_price));
			$objPHPExcel->getActiveSheet()->getStyle("A{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			++$i;
			
			//订单总额
			$orders_info="{$c['manage']['lang_pack']['orders']['info']['order_info']}\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['oid']}: {$orders_row['OId']}\r\n";
			$Sales && $orders_info.="{$c['manage']['lang_pack']['manage']['manage']['permit_name'][3]}: {$Sales}\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['time']}: ".date('Y-m-d H:i:s', $orders_row['OrderTime'])."\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['orders_status']}: {$c['orders']['status'][$orders_row['OrderStatus']]}\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['info']['ship_info']}: ".((int)$orders_row['ShippingMethodSId']?$shipping_cfg['Express']:($orders_row['ShippingMethodType']=='air'?$shipping_cfg['AirName']:$shipping_cfg['OceanName']))."\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['info']['weight']}: {$total_weight}KG\r\n";
			$orders_info.="{$c['manage']['lang_pack']['orders']['payment_method']}: {$orders_row['PaymentMethod']}\r\n";
			if($orders_row['OrderStatus']>4){
				$orders_info.="{$c['manage']['lang_pack']['orders']['shipping']['track_no']}: {$orders_row['TrackingNumber']}\r\n";
				$orders_info.="{$c['manage']['lang_pack']['orders']['global']['remark']}: {$orders_row['Remarks']}\r\n";
			}
			$objPHPExcel->getActiveSheet()->mergeCells("A{$i}:C{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("A{$i}", $orders_info);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setWrapText(true);//自动换行
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
			
			$ship_info="{$c['manage']['lang_pack']['orders']['export']['shipinfo']}\r\n";
			$ship_info.="{$c['manage']['lang_pack']['orders']['export']['shipname']}: {$shipto_ary['FirstName']} {$shipto_ary['LastName']}\r\n";
			$ship_info.="{$c['manage']['lang_pack']['orders']['export']['shipaddress']}: ".($shipto_ary['AddressLine1'].($shipto_ary['AddressLine2']?', '.$shipto_ary['AddressLine2']:'').', '.$shipto_ary['City'].', '.$shipto_ary['State'].', '.$shipto_ary['ZipCode'].', '.$shipto_ary['Country'].(($shipto_ary['CodeOption']&&$shipto_ary['TaxCode'])?'#'.$shipto_ary['CodeOption'].': '.$shipto_ary['TaxCode']:''))."\r\n";
			$ship_info.="{$c['manage']['lang_pack']['orders']['export']['shipphone']}: {$shipto_ary['CountryCode']}-{$shipto_ary['PhoneNumber']}\r\n";
			$objPHPExcel->getActiveSheet()->mergeCells("D{$i}:F{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("D{$i}", $ship_info);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getAlignment()->setWrapText(true);//自动换行
			$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
			
			$bill_info="{$c['manage']['lang_pack']['orders']['export']['billinfo']}\r\n";
			$bill_info.="{$c['manage']['lang_pack']['orders']['export']['billname']}: {$orders_row['BillFirstName']} {$orders_row['BillLastName']}\r\n";
			$bill_info.="{$c['manage']['lang_pack']['orders']['export']['billaddress']}: ".($orders_row['BillAddressLine1'].($orders_row['BillAddressLine2']?', '.$orders_row['BillAddressLine2']:'').', '.$orders_row['BillCity'].', '.$orders_row['BillState'].', '.$orders_row['BillZipCode'].', '.$orders_row['BillCountry'].(($orders_row['BillCodeOption']&&$orders_row['BillTaxCode'])?'#'.$orders_row['BillCodeOption'].': '.$orders_row['BillTaxCode']:''))."\r\n";
			$bill_info.="{$c['manage']['lang_pack']['orders']['export']['billphone']}: {$orders_row['BillCountryCode']}-{$orders_row['BillPhoneNumber']}\r\n";
			$objPHPExcel->getActiveSheet()->mergeCells("G{$i}:K{$i}");//合并标题
			$objPHPExcel->getActiveSheet()->setCellValue("G{$i}", $bill_info);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getAlignment()->setWrapText(true);//自动换行
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
			
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(150);//设置行的宽度
			++$i;
			
			//Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('Simple');
			
			//Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			//Save Excel 2007 file
			$ExcelName='orders_prod_'.$orders_row['OId'];
			$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
			$objWriter->save($c['root_path']."/tmp/{$ExcelName}.xls");
			unset($c, $objPHPExcel, $objWriter, $orders_row, $shipping_cfg);
			
			file::down_file("/tmp/{$ExcelName}.xls");
			file::del_file("/tmp/{$ExcelName}.xls");
			
			ly200::e_json('', 1);
		}else{
			js::location('?m=orders&a=orders');
		}
	}
	//订单资料批量操作 End
	
	/**
	 * [获取亚翔供应链的所有符合条件物流]
	 * @return [type] [description]
	 */
	public static function get_asiafly_list()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');

		$OrderId = (int)$p_OrderId;
		$ExprectDay = (int)$p_ExprectDay;
		$ProductType = (int)$p_ProductType; // 1：普货，2：带电，3：敏感
		// 订单数据
		$result = asiafly::getExpressList($OrderId, $ExprectDay, $ProductType);

		$data = array();
		$ret = 0;
		if ($result['IsSucceed'] == 0) {
			// 接口请求错误
			$data = array($result['Message']);
		} else if($result['IsSucceed'] == 2 || ($result['IsSucceed'] == 1 && empty($result['Data']))) {
			// 接口处理错误
			$data = array($result['Msg']);
		} else if ($result['IsSucceed'] == 1) {
			// 成功
			$data = $result['Data'];
			$ret = 1;
		}
		ly200::e_json($data, $ret);
	}

	/**
	 * [选择了使用亚翔供应链物流，获取标签PDF文件，以及客户单号]
	 * @return [type] [description]
	 */
	public static function asiafly_choice_use()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');

		$OrderId = (int)$p_OrderId;
		$ProductCode = $p_ProductCode;
		$ProductName = $p_ProductName;
		$Amount = (float)$p_Amount;

		// 调用订单信息提交接口，获取标签PDF文件链接，客户单号
		$result = asiafly::getTable($OrderId, $ProductCode, $ProductName);
		
		$data = array();
		$ret = 0;
		if ($result['IsSucceed'] == 0) {
			// 接口请求错误
			$data = array($result['Message']);
		} else if($result['IsSucceed'] == 2 || ($result['IsSucceed'] == 1 && empty($result['Data']))) {
			// 接口处理错误
			$data = array($result['Msg']);
		} else if ($result['IsSucceed'] == 1) {
			// 成功
			$data = $result['Data'];
			$data['OrderId'] = $OrderId;
			$BusinessNo = $data['BusinessNo'];
			if(!db::get_row_count('orders_label_list', "OrderId='{$OrderId}' and BusinessNo='{$BusinessNo}'", 'LId')){
				// 没有保存过客户单号
				$insert_data = array(
					'OrderId'		=>	(int)$OrderId,
					'ProductCode'	=>	$ProductCode,
					'ProductName'	=>	$ProductName,
					'Amount'		=>	(float)$Amount,
					'LabelUrl'		=>	$data['LabelUrl'],
					'BusinessNo'	=>	$BusinessNo,
				);
				// 保存亚翔供应链信息
				db::insert('orders_label_list', $insert_data);

				// 修改订单亚翔供应链状态，改为已生成标签（临时，针对的只是单物流）
				db::update('orders', "OrderId='{$OrderId}'", array('AsiaflyStatus'=>1));
			}

			$ret = 1;
		}
		ly200::e_json($data, $ret);
	}

	// 下载PDF文件
	public static function asiafly_label_download()
	{
		global $c;
		extract($_GET, EXTR_PREFIX_ALL, 'g');
		// 获取参数值
		$BusinessNo = $g_BusinessNo;
		$OrderId = (int)$g_OrderId;
		
		// 获取对应的亚翔物流信息
		$LabelUrl = db::get_value('orders_label_list', "OrderId='{$OrderId}' and BusinessNo='{$BusinessNo}'", 'LabelUrl');
		// 下载文件
		$file = asiafly::httpCopy($LabelUrl, $BusinessNo);
		// 保存本地文件
		if(is_file($file)){
			db::update('orders_label_list', "OrderId='{$OrderId}' and BusinessNo='{$BusinessNo}'", array('FilePath'=>str_replace($c['root_path'], '', $file)));
		}
		asiafly::down_file($file, $BusinessNo.'.pdf');
	}

	/**
	 * [亚翔供应链物流跟踪]
	 * @return 
	 */
	public static function asiafly_track_check()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');
		$BusinessNo = $p_BusinessNo;

		// 查询物流信息
		$result = asiafly::getTrack($BusinessNo);
		
		$data = array();
		$ret = 0;
		if ($result['IsSucceed'] == 0) {
			// 接口请求错误
			$data = array($result['Message']);
		} else if ($result['IsSucceed'] == 2 || ($result['IsSucceed'] == 1 && empty($result['Data']))) {
			// 接口处理错误
			$data = array($result['Msg']);
		} else if ($result['IsSucceed'] == 1) {
			// 成功
			$data = $result['Data'];
			$trackTimes = asiafly::i_array_column($data, 'TrackTime');
			array_multisort($trackTimes, SORT_DESC, $data);
			$ret = 1;
		}
		ly200::e_json($data, $ret);
	}

	/**
	 * 获取物流产品信息接口
	 */
	public static function asiafly_get_products()
	{
		global $c;

		$result = asiafly::get_products_list();

		$data = array();
		$ret = 0;
		if ($result['IsSucceed'] == 0) {
			// 接口请求错误
			$data = array($result['Message']);
		} else if ($result['IsSucceed'] == 2 || ($result['IsSucceed'] == 1 && empty($result['Data']))) {
			// 接口处理错误
			$data = array($result['Msg']);
		} else if ($result['IsSucceed'] == 1) {
			// 成功
			foreach((array)$result['Data'] as $k=>$v){
				$data[$v['ProviderCode']] = $v['ProviderName'];
			}

			$AsiaflyProductsData = str::json_data($result['Data']);
			manage::config_operaction(array('AsiaflyProductsData' => addslashes($AsiaflyProductsData)), 'orders');
			$ret = 1;
		}
		ly200::e_json($data, $ret);
	}

	/**
	 * 获取物流产品信息接口
	 */
	public static function get_channel_product()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Type = $p_Type; // Provider/Channel 物流商/渠道
		$ProviderCode = $p_ProviderCode; // 物流商代码
		$ChannelCode = $p_ChannelCode; // 渠道代码

		$AsiaflyProductsData = db::get_value('config', "GroupId='orders' and Variable='AsiaflyProductsData'", 'value');
		$AsiaflyProducts = str::json_data($AsiaflyProductsData, 'decode');

		$data = array();
		foreach((array)$AsiaflyProducts as $k=>$v){
			if($Type == 'Provider' && $v['ProviderCode'] == $ProviderCode){
				// 根据物流商代码获取渠道列表
				$data[$v['ChannelCode']] = $v['ChannelName'];
			}else if($Type == 'Channel' && $v['ChannelCode'] == $ChannelCode){
				// 根据渠道代码获取渠道列表
				$data[$v['ProductCode']] = $v['ProductName'];
			}
		}

		ly200::e_json($data, 1);
	}

	/**
	 * [DHL获取标签]
	 * @return 
	 */
	public static function dhl_get_label()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$OrderId = (int)$p_OrderId;
		$CId = (int)$p_CId;
		$orders_row = db::get_one('orders', "OrderId='{$OrderId}'", '*');

		// 获取发货数据
		$data = array(
			'Address'		=>	$p_Address,
			'City'			=>	$p_City,
			'CId'			=>	$CId,
			'Name'			=>	$p_Name,
		);

		$shipperData = str::json_data($data);
		manage::config_operaction(array('DHLShipper' => addslashes($shipperData)), 'orders');
		manage::operation_log('DHL默认取件地址');

		$products_data = array();
		$IsContentIndicator = 0;
		foreach((array)$p_DHLDescription as $k=>$v) {
			$update_ary = array(
				'IsContentIndicator'	=>	(int)$p_IsContentIndicator[$k],
				'DHLDescription'		=>	$v,
				'DHLDescriptionExport'	=>	$p_DHLDescriptionExport[$k]
			);
			db::update('products', "ProId='{$k}'", $update_ary);
			(int)$p_IsContentIndicator[$k] && $IsContentIndicator = 1;
		}

		$Acronym = db::get_value('country', "CId='{$CId}'", 'Acronym');
		$info = array(
			'shipperAddress'	=>  array(
				'address1'			=>  trim($p_Address),
				'city'				=>  trim($p_City),
				'country'			=>  $Acronym,
				'name'				=>  trim($p_Name),
			),
			'shipmentItems' 	=>  array(
				'contentIndicator'  =>  $IsContentIndicator ? '04' : '00',
				'productCode'		=>  trim($p_productCode),
			),
			'shipmentContents'	=>  array(),
		);

		$products_list_row = db::get_all('orders_products_list', "OrderId='{$OrderId}'", 'ProId, Price, PropertyPrice, Discount, Qty, SKU', 'LId asc');
		foreach((array)$products_list_row as $k => $v){
			$info['shipmentContents']['contentIndicator'][$v['ProId']] = (int)$p_IsContentIndicator[$v['ProId']] ? '04' : '00';
			$info['shipmentContents']['description'][$v['ProId']] = $p_DHLDescription[$v['ProId']];
			$info['shipmentContents']['descriptionExport'][$v['ProId']] = $p_DHLDescriptionExport[$v['ProId']];
		}

		$result = dhl::getLabel($orders_row, $info);

		$ret = 0;
		$return = '';
		if (!$result['error']) {
			// 发货创建成功
			$labels_ary = $result['response']['labelResponse']['bd']['labels'];
			$base_name = '/u_file/'.date('ym/').'dhl/'.date('d/');
			$shipmentID_ary = array();
			foreach ((array)$labels_ary as $k => $v) {
				$FilePath = dhl::base64FileResolve($v['content'], $base_name);
				$shipmentID_ary[] = $v['shipmentID'];
				$insert_ary = array(
					'OrderId'		=>	$OrderId,
					'FilePath'		=>	$FilePath,
					'ProductName'	=>	'DHL',
					'BusinessNo'	=>	$v['shipmentID'],
				);
				db::insert('orders_label_list', $insert_ary);
			}
			$shipmentID_ary = array_unique($shipmentID_ary);
			$ret = 1;
			$return = $OrderId;
			db::update('orders', "OrderId='{$OrderId}'", array('DHLLabelStatus'=>1));
			dhl::Closeout($OrderId, $shipmentID_ary);
		}else{
			$return = $result['detail'];
		}

		$LogData = dhl::json_data($result);
		dhl::label_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $OrderId, 'DHL获取标签', $LogData);

		ly200::e_json($return, $ret);
	}

	// 下载PDF文件
	public static function dhl_label_download()
	{
		global $c;
		extract($_GET, EXTR_PREFIX_ALL, 'g');
		// 获取参数值
		$OrderId = (int)$g_OrderId;
		// 获取对应的亚翔物流信息
		$FilePath = db::get_value('orders_label_list', "OrderId='{$OrderId}'", 'FilePath');
		$OId = db::get_value('orders', "OrderId='{$OrderId}'", 'OId');
		// 下载文件
		file::down_file($FilePath, $OId.'.pdf');
	}

	/**
	 * [DHL物流轨迹查询]
	 * @return 
	 */
	public static function dhl_track_check()
	{
		global $c;
		extract($_POST, EXTR_PREFIX_ALL, 'p');
		$OrderId = (int)$p_OrderId;

		// 查询物流信息
		$result = dhl::trackingInformationQuery($OrderId);
		
		$ret = 0;
		
		if (!$result['error']) {
			// 查询成功
			$return = $result['response']['trackItemResponse']['items'][0]['events'];
			$trackTimes = asiafly::i_array_column($return, 'timestamp');
			array_multisort($trackTimes, SORT_DESC, $return);
			$ret = 1;
		}else{
			// 查询失败
			$return = $result['detail'];
		}
		/**
		$LogData = dhl::json_data($result);
		dhl::label_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $OrderId, 'DHL查询物流轨迹', $LogData);
		*/
	
		ly200::e_json($return, $ret);
	}


}
?>