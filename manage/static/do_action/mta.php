<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class mta_module{
	/*
	* 流量部分(products)
	* 1. 流量统计(api_get_data)、流量来源(api_get_data)、流量分布(api_get_data)、漏斗分析(api_get_data)
	* 2. 流量转化率(get_visits_conversion_data)
	*/
	public static function api_get_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		ksort($_POST);
		$save_dir=$c['tmp_dir'].'manage/';
		$filename=$p_Action.'-'.md5(implode('&', $_POST)).'.json';
		//if(!file::check_cache($c['root_path'].$save_dir.$filename, 0)){//文件是否存在、是否到更新时间
			unset($_POST['do_action']);
			$result=ly200::api($_POST, $c['ApiKey'], $c['api_url']);
			//if($result['ret']==1){
				@file::write_file($save_dir, $filename, str::json_data($result));
			//}else{
				//ly200::e_json('', 0);
			//}
		//}
		echo @file_get_contents($c['root_path'].$save_dir.$filename);
		exit;
	}

	public static function get_visits_conversion_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$p_Compare=(int)$p_Compare;
		$p_Terminal=(int)$p_Terminal;
		$p_Terminal>=1 && $complete_where=' and Source='.($p_Terminal-1);
		$p_TimeS=$p_TimeS!=''?$p_TimeS:0;
		$p_Compare && $p_TimeE=$p_TimeE!=''?$p_TimeE:0;
		
		$reset_data=array(
			'uv'				=>	0,
			'session'			=>	0,
			'addtocart'			=>	0,
			'addtocart_ratio'	=>	0,
			'addtocart_session'	=>	0,
			'placeorder'		=>	0,
			'placeorder_ratio'	=>	0,
			'placeorder_session'=>	0,
			'complete'			=>	0,
			'complete_ratio'	=>	0,
			'complete_session'	=>	0
		);
		$return_data=array(
			'ratio'				=>	$reset_data,
			'enter_directly'	=>	$reset_data,
			'share_platform'	=>	$reset_data,
			'search_engine'		=>	$reset_data,
			'other'				=>	$reset_data,
		);
		$p_Compare && $return_data['compare']=$return_data;
		
		$result=ly200::api($_POST, $c['ApiKey'], $c['api_url']);
        //var_dump($result);
        $chartAry = array();
		$ary = array('ratio', 'enter_directly', 'share_platform', 'search_engine', 'other');
        foreach ($ary as $value) {
            if($value == 'ratio') continue;
            $key = ($value == 'ratio' ? 'total' : $value);
            $totalUv = 0;
            $totalSession = 0;
            foreach ((array)$result['msg'][$key] as $time => $data) {
                $totalUv += $data['Uv'];
                $totalSession += $data['Session'];
                
                $addtocart = (int)$data['1']['TotalVisitors'] + (int)$data['2']['TotalVisitors'] + (int)$data['3']['TotalVisitors'];
                $return_data[$value]['addtocart'] += $addtocart;
                $addtocart_session = (int)$data['1']['TotalSession'] + (int)$data['2']['TotalSession'] + (int)$data['3']['TotalSession']; 
                $return_data[$value]['addtocart_session'] += $addtocart_session;
                
                $return_data[$value]['placeorder'] += (int)$data['5']['TotalVisitors'];
                $return_data[$value]['placeorder_session'] += (int)$data['5']['TotalSession'];
                
                $where = 1;
                $value == 'enter_directly' && $where = 'RefererId=99';
                $value == 'share_platform' && $where = 'RefererId=1';
                $value == 'search_engine' && $where = 'RefererId=0';
                $value == 'other' && $where = 'RefererId=100';
                $time_s = where::time($p_TimeS, '', !in_array($p_TimeS, array(0, -1)));
                $where = "$where and OrderTime between {$time_s[0]} and {$time_s[1]}";
                $where .= " and OrderStatus in(4,5,6)";
                $return_data[$value]['complete'] += (int)db::get_row_count('orders', $where . $complete_where);
                $return_data[$value]['complete_session'] += (int)$data['6']['TotalSession'];
                
                $date = date('m/d', $time);
                $chartAry[$value][$date]['total'] = $data['Session'];
                $chartAry[$value][$date]['sum'] = (int)$addtocart_session + (int)$data['5']['TotalSession'] + (int)$data['6']['TotalSession'];
            }
            
            $return_data[$value]['uv'] = $totalUv;
            $return_data[$value]['session'] = $totalSession;
            
            $return_data[$value]['addtocart_ratio'] = $totalUv ? sprintf('%01.1f', $return_data[$value]['addtocart'] * 100 / $totalUv) : 0;
            $return_data[$value]['addtocart_session_ratio'] = $totalSession ? sprintf('%01.1f', $return_data[$value]['addtocart_session'] * 100 / $totalSession) : 0;
            $return_data[$value]['placeorder_ratio'] = $return_data[$value]['addtocart'] ? sprintf('%01.1f', $return_data[$value]['placeorder'] * 100 / $return_data[$value]['addtocart']) : 0;
            $return_data[$value]['placeorder_session_ratio'] = $return_data[$value]['addtocart_session'] ? sprintf('%01.1f', $return_data[$value]['placeorder_session'] * 100 / $return_data[$value]['addtocart_session']) : 0;
            $return_data[$value]['complete_ratio'] = $return_data[$value]['placeorder'] ? sprintf('%01.1f', $return_data[$value]['complete'] * 100 / $return_data[$value]['placeorder']) : 0;
            $return_data[$value]['complete_session_ratio'] = $return_data[$value]['placeorder_session'] ? sprintf('%01.1f', $return_data[$value]['complete_session'] * 100 / $return_data[$value]['placeorder_session']) : 0;
            
            $total = (int)$return_data[$value]['addtocart_session'] + (int)$return_data[$value]['placeorder_session'] + (int)$return_data[$value]['complete_session'];
            $return_data[$value]['total_session_ratio'] = $totalSession ? sprintf('%01.1f', $total * 100 / $totalSession) : 0;
        }
        
        //全部
        foreach ($ary as $value) {
            if($value == 'ratio') continue;
            $return_data['ratio']['addtocart'] += $return_data[$value]['addtocart'];
            $return_data['ratio']['addtocart_session'] += $return_data[$value]['addtocart_session'];
            $return_data['ratio']['placeorder'] += $return_data[$value]['placeorder'];
            $return_data['ratio']['placeorder_session'] += $return_data[$value]['placeorder_session'];
            $return_data['ratio']['complete'] += $return_data[$value]['complete'];
            $return_data['ratio']['complete_session'] += $return_data[$value]['complete_session'];
            $return_data['ratio']['uv'] += $return_data[$value]['uv'];
            $return_data['ratio']['session'] += $return_data[$value]['session'];
        }
        $return_data['ratio']['addtocart_ratio'] = $return_data['ratio']['uv'] ? sprintf('%01.1f', $return_data['ratio']['addtocart'] * 100 / $return_data['ratio']['uv']) : 0;
        $return_data['ratio']['addtocart_session_ratio'] = $return_data['ratio']['session'] ? sprintf('%01.1f', $return_data['ratio']['addtocart_session'] * 100 / $return_data['ratio']['session']) : 0;
        $return_data['ratio']['placeorder_ratio'] = $return_data['ratio']['addtocart'] ? sprintf('%01.1f', $return_data['ratio']['placeorder'] * 100 / $return_data['ratio']['addtocart']) : 0;
        $return_data['ratio']['placeorder_session_ratio'] = $return_data['ratio']['addtocart_session'] ? sprintf('%01.1f', $return_data['ratio']['placeorder_session'] * 100 / $return_data['ratio']['addtocart_session']) : 0;
        $return_data['ratio']['complete_ratio'] = $return_data['ratio']['placeorder'] ? sprintf('%01.1f', $return_data['ratio']['complete'] * 100 / $return_data['ratio']['placeorder']) : 0;
        $return_data['ratio']['complete_session_ratio'] = $return_data['ratio']['placeorder_session'] ? sprintf('%01.1f', $return_data['ratio']['complete_session'] * 100 / $return_data['ratio']['placeorder_session']) : 0;
        $total = (int)$return_data['ratio']['addtocart_session'] + (int)$return_data['ratio']['placeorder_session'] + (int)$return_data['ratio']['complete_session'];
        $return_data['ratio']['total_session_ratio'] = $return_data['ratio']['session'] ? sprintf('%01.1f', $total * 100 / $return_data['ratio']['session']) : 0;
        
        $i = 0;
        $_Ary = array();
        foreach ((array)$chartAry as $k => $v) {
            if ($k != 'ratio') {
                foreach ($v as $k2 => $v2) {
                    if ($i == 0) {
                        $return_data['conversion_charts']['xAxis']['categories'][] = $k2;
                    }
                    $_Ary[$k2]['total'] += $v2['total'];
                    $_Ary[$k2]['sum'] += $v2['sum'];
                }
                ++$i;
            }
        }
        foreach ($_Ary as $k => $v) {
            $return_data['conversion_charts']['series'][0]['data'][] = $v['total'] ? (float)sprintf('%01.1f', $v['sum'] * 100 / $v['total']) : 0;
        }
		
		ly200::e_json($return_data, 1);
	}
	
	/*
	* 订单部分(products)
	* 1. 订单统计(get_orders_data)
	* 2. 复购率(get_orders_repurchase_data)
	*/
	public static function get_orders_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$p_Compare=(int)$p_Compare;
		$p_Terminal=(int)$p_Terminal;
		$p_TimeS=$p_TimeS!=''?$p_TimeS:0;
		$IsCharts=@!in_array($p_TimeS, array(0,-1))?1:0;
		$result=ly200::api($_POST, $c['ApiKey'], $c['api_url']);
		
		$time_s=where::time($p_TimeS, '', !in_array($p_TimeS, array(0,-1)));
		$orders_data=array(
			'total'		=>	array(),
			'detail'	=>	array(
								'country'	=>	array(),
								'payment'	=>	array()
							),
			'compare'	=>	array(
								'total'		=>	array(),
								'detail'	=>	array(
													'country'	=>	array(),
													'payment'	=>	array()
												),
							),
			'time_s'	=>	date('Y-m-d', $time_s[0]).date('/Y-m-d', $time_s[1]),
			'time_e'	=>	''
		);
		$w="OrderStatus in(4,5,6) and (OrderTime between {$time_s[0]} and {$time_s[1]})";
		$p_Terminal>=1 && $w.=' and Source='.($p_Terminal-1);
		
		$total_field='sum(((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*(1-CouponDiscount))+ShippingPrice+ShippingInsurancePrice-CouponPrice-DiscountPrice)*(1+PayAdditionalFee/100)+PayAdditionalAffix) as total';
		//$discount_price='sum((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*CouponDiscount)+CouponPrice) as discount_price';
		$discount_price='sum((ProductPrice*(1-(100-Discount)/100))+(ProductPrice*(1-if(UserDiscount>0, UserDiscount, 100)/100))+(ProductPrice*CouponDiscount)+CouponPrice+DiscountPrice) as discount_price';
		$order_count='count(OrderId) as order_count';
		$coupon_count='sum(if(CouponPrice>0||CouponDiscount>0, 1, 0)) as coupon_count';
		$order_row=db::get_one('orders', $w, "$total_field,$order_count,$coupon_count,$discount_price");
		$order_customer_count=db::get_row_count('orders', $w.' group by Email', 'OrderId');
		$orders_data['total']=array(
			'total_price'			=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $order_row['total']),
			'order_count'			=>	(int)$order_row['order_count'],
			'order_unit_price'		=>	$c['manage']['currency_symbol'].((int)$order_row['order_count']?sprintf('%01.2f', $order_row['total']/$order_row['order_count']):0),
			'order_customer_price'	=>	$c['manage']['currency_symbol'].((int)$order_customer_count?sprintf('%01.2f', $order_row['total']/$order_customer_count):0),
			'order_customer_count'	=>	(int)$order_customer_count,
			'ratio'					=>	(int)$result['msg']['uv']?sprintf('%01.2f', $order_row['order_count']*100/$result['msg']['uv']).'%':0,
			'visit_customer'		=>	(int)$result['msg']['uv'],
			'discount_price'		=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $order_row['discount_price']),
			'coupon_count'			=>	(int)$order_row['coupon_count']
		);
		//付款率
		$order_total_count=db::get_row_count('orders', "(OrderTime between {$time_s[0]} and {$time_s[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):''), 'OrderId');
		$orders_data['total']['payment_rate']=$order_total_count>0?sprintf('%01.2f', $order_row['order_count']/$order_total_count*100):0;
		//优惠券使用率
		$order_coupon_count=db::get_row_count('orders', "CouponCode!='' and (OrderTime between {$time_s[0]} and {$time_s[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):''), 'OrderId');
		$orders_data['total']['coupon_rate']=$order_total_count>0?sprintf('%01.2f', $order_coupon_count/$order_total_count*100):0;
		//国家分布
		$country_row=ly200::get_table_data_to_ary('orders', $w.' group by ShippingCId', 'ShippingCId', '', "ShippingCId,ShippingCountry,$total_field,count(OrderId) as order_count");
		foreach($country_row as $k=>$v){
			$orders_data['detail']['country'][$k]=array(
				'title'			=>	$v['ShippingCountry'],
				'price'			=>	sprintf('%01.2f', $v['total']),
				'count'			=>	(int)$v['order_count'],
				'average_price'	=>	$c['manage']['currency_symbol'].((int)$v['order_count']?sprintf('%01.2f', $v['total']/$v['order_count']):0)
			);
			if($p_Compare){
				$orders_data['compare']['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	0,
					'count'			=>	0,
					'average_price'	=>	0
				);
			}
		}
		//支付
		$payment_row=ly200::get_table_data_to_ary('orders', $w.' group by PId', 'PId', '', "PId,PaymentMethod,$total_field,count(OrderId) as order_count");
		$payment_total_count=db::get_row_count('orders', "(OrderTime between {$time_s[0]} and {$time_s[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):'').' group by PId', 'OrderId');
		foreach($payment_row as $k=>$v){
			$orders_data['detail']['payment'][$k]=array(
				'title'		=>	$v['PaymentMethod'],
				'rate'		=>	($order_row['total']>0?sprintf('%01.2f', (int)$v['total']/$order_row['total']*100):0),
				'price'		=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $v['total']),
				'count'		=>	(int)$v['order_count'],
				'pay_rate'	=>	($payment_total_count>0?sprintf('%01.2f', (int)$v['order_count']/$payment_total_count*100):0),
			);
			if($p_Compare){
				$orders_data['compare']['detail']['payment'][$k]=array(
					'title'		=>	$v['PaymentMethod'],
					'rate'		=>	0,
					'price'		=>	0,
					'count'		=>	0,
					'pay_rate'	=>	0
				);
			}
		}
		//优惠券
		$discount_price='sum((ProductPrice*CouponDiscount)+CouponPrice) as discount_price';
		$coupon_row=ly200::get_table_data_to_ary('orders', 'CouponCode!="" and '.$w.' group by CouponCode', 'CouponCode', '', "CouponCode,$total_field,$discount_price,$order_count");
		foreach($coupon_row as $k=>$v){
			$orders_data['detail']['coupon'][$k]=array(
				'code'	=>	$v['CouponCode'],
				'count'	=>	(int)$v['order_count'],
				'rate'	=>	($v['total']>0?sprintf('%01.2f', (int)$v['discount_price']/$v['total']*100):0)
			);
		}
		//设备占比
		$browser_count_ary=array(
			'pc'	=>	array(
							0=>(int)db::get_row_count('orders', "OrderStatus<4 and (OrderTime between {$time_s[0]} and {$time_s[1]}) and Source=0", 'OrderId'),
							1=>(int)db::get_row_count('orders', "OrderStatus in (4,5,6) and (OrderTime between {$time_s[0]} and {$time_s[1]}) and Source=0", 'OrderId')
						),
			'mobile'=>	array(
							0=>(int)db::get_row_count('orders', "OrderStatus<4 and (OrderTime between {$time_s[0]} and {$time_s[1]}) and Source=1", 'OrderId'),
							1=>(int)db::get_row_count('orders', "OrderStatus in (4,5,6) and (OrderTime between {$time_s[0]} and {$time_s[1]}) and Source=1", 'OrderId')
						)
		);
		$browser_total_count_ary=array(0=>$browser_count_ary['pc'][0]+$browser_count_ary['pc'][1], 1=>$browser_count_ary['mobile'][0]+$browser_count_ary['mobile'][1]);
		$browser_total_count=$browser_total_count_ary[0]+$browser_total_count_ary[1];
		$orders_data['coupon']=array(
			'pc'	=>	array(
							0=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_order'], 'count'=>$browser_total_count_ary[0]),//总订单
							1=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_unpaid'], 'count'=>$browser_count_ary['pc'][0]),//未付款
							2=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_paid'], 'count'=>$browser_count_ary['pc'][1]),//已付款
						),
			'mobile'=>	array(
							0=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_order'], 'count'=>$browser_total_count_ary[1]),//总订单
							1=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_unpaid'], 'count'=>$browser_count_ary['mobile'][0]),//未付款
							2=>array('title'=>$c['manage']['lang_pack']['mta']['order']['total_paid'], 'count'=>$browser_count_ary['mobile'][1]),//已付款
						)
		);
		$orders_data['browser_charts']['series'][0]['name']=$c['manage']['lang_pack']['mta']['global']['equipment_ratio'];//设备占比
		$orders_data['browser_charts']['series'][0]['data']=array(
			0	=>	array(//PC端
						'name'	=> $c['manage']['lang_pack']['mta']['terminal_ary'][1],
						'y'		=> (float)($browser_total_count>0?sprintf('%01.2f', $browser_total_count_ary[0]/$browser_total_count*100):0)
					),
			1	=>	array(//移动端
						'name'	=> $c['manage']['lang_pack']['mta']['terminal_ary'][2],
						'y'		=> (float)($browser_total_count>0?sprintf('%01.2f', $browser_total_count_ary[1]/$browser_total_count*100):0)
					)
		);
		
		//对比选项
		if($p_Compare){
			$time_e=where::time($p_TimeE);
			$orders_data['time_e']=date('Y-m-d', $time_e[0]).date('/Y-m-d', $time_e[1]);
			$where="OrderStatus in(4,5,6) and OrderTime between {$time_e[0]} and {$time_e[1]}";
			$p_Terminal>=1 && $where.=' and Source='.($p_Terminal-1);

			$compare_order_row=db::get_one('orders', $where, "$total_field,$order_count,$coupon_count,$discount_price");
			$compare_order_customer_count=db::get_row_count('orders', $where.' group by Email', 'OrderId');

			$orders_data['compare']['total']=array(
				'total_price'			=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $compare_order_row['total']),
				'order_count'			=>	(int)$compare_order_row['order_count'],
				'order_unit_price'		=>	$c['manage']['currency_symbol'].((int)$compare_order_row['order_count']?sprintf('%01.2f', $compare_order_row['total']/$compare_order_row['order_count']):0),
				'order_customer_price'	=>	$c['manage']['currency_symbol'].((int)$compare_order_customer_count?sprintf('%01.2f', $compare_order_row['total']/$compare_order_customer_count):0),
				'order_customer_count'	=>	(int)$compare_order_customer_count,
				'discount_price'		=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $compare_order_row['discount_price']),
				'coupon_count'			=>	(int)$compare_order_row['coupon_count'],
				'ratio'					=>	(int)$result['msg']['compare_uv']?sprintf('%01.2f', $compare_order_row['order_count']*100/$result['msg']['compare_uv']).'%':0,
				'visit_customer'		=>	(int)$result['msg']['compare_uv'],
				'payment_rate'			=>	$order_total_count>0?sprintf('%01.2f', $compare_order_row['order_count']/$order_total_count*100):0
			);
			//付款率
			$compare_order_total_count=db::get_row_count('orders', "(OrderTime between {$time_e[0]} and {$time_e[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):''), 'OrderId');
			$orders_data['compare']['total']['payment_rate']=$compare_order_total_count>0?sprintf('%01.2f', $compare_order_row['order_count']/$compare_order_total_count*100):0;
			//优惠券使用率
			$compare_order_coupon_count=db::get_row_count('orders', "CouponCode!='' and (OrderTime between {$time_e[0]} and {$time_e[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):''), 'OrderId');
			$orders_data['compare']['total']['coupon_rate']=$compare_order_total_count>0?sprintf('%01.2f', $compare_order_coupon_count/$compare_order_total_count*100):0;
			//国家分布
			$compare_country_row=ly200::get_table_data_to_ary('orders', $where.' group by ShippingCId', 'ShippingCId', '', "ShippingCId,ShippingCountry,$total_field,count(OrderId) as order_count");
			foreach($compare_country_row as $k=>$v){
				$orders_data['compare']['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	sprintf('%01.2f', $v['total']),
					'count'			=>	(int)$v['order_count'],
					'average_price'	=>	$c['manage']['currency_symbol'].((int)$v['order_count']?sprintf('%01.2f', $v['total']/$v['order_count']):0)
				);
				if(array_key_exists($k, $orders_data['detail']['country'])){continue;}
				$orders_data['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	0,
					'count'			=>	0,
					'average_price'	=>	0
				);
			}
			//支付
			$compare_payment_row=ly200::get_table_data_to_ary('orders', $where.' group by PId', 'PId', '', "PId,PaymentMethod,$total_field,count(OrderId) as order_count");
			$compare_payment_total_count=db::get_row_count('orders', "(OrderTime between {$time_e[0]} and {$time_e[1]})".($p_Terminal>=1?' and Source='.($p_Terminal-1):'').' group by PId', 'OrderId');
			foreach($compare_payment_row as $k=>$v){
				$orders_data['compare']['detail']['payment'][$k]=array(
					'title'			=>	$v['PaymentMethod'],
					'rate'			=>	($compare_order_row['total']>0?sprintf('%01.2f', (int)$v['total']/$compare_order_row['total']*100):0),
					'price'			=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $v['total']),
					'count'			=>	(int)$v['order_count'],
					'pay_rate'		=>	($compare_payment_total_count>0?sprintf('%01.2f', (int)$v['order_count']/$compare_payment_total_count*100):0),
				);
				if(array_key_exists($k, $orders_data['detail']['payment'])){continue;}
				$orders_data['detail']['payment'][$k]=array(
					'title'			=>	$v['PaymentMethod'],
					'rate'			=>	0,
					'price'			=>	0,
					'count'			=>	0,
					'pay_rate'		=>	0
				);
			}
		}
		
		//订单概况
		//if($IsCharts){
			$date_field="FROM_UNIXTIME(OrderTime, '%Y%m%d') as order_time";
			$order_row=ly200::get_table_data_to_ary('orders', $w.' group by order_time', 'order_time', 'total', "$total_field,$date_field");
			
			$charts_data=array();
			$charts_data['chart']['height']=350;
			$charts_data['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['order']['order_price'];
			$charts_data['plotOptions']['spline']['dataLabels']['enabled']=false;
			$time_s_obj=array(new DateTime(date('Ymd', $time_s[0])), new DateTime(date('Ymd', $time_s[1])));

			$days=0;
			while($time_s_obj[0]<=$time_s_obj[1]){
				if($days>30) break;
				$date_key=$time_s_obj[0]->format('Ymd');
				$d_date_key=$time_s_obj[0]->format('m/d');
				$charts_data['xAxis']['categories'][]=$d_date_key;
				$charts_data['series'][0]['data'][]=(float)sprintf('%01.2f', $order_row[$date_key]);
				$time_s_obj[0]->modify('1 days');
				$days++;
			}
			
			if($p_Compare){
				//开启对比选项
				$time_e=where::time($p_TimeE);
				$where="OrderStatus in(4,5,6) and OrderTime between {$time_e[0]} and {$time_e[1]}";
				$p_Terminal>=1 && $where.=' and Source='.($p_Terminal-1);
				$order_row=ly200::get_table_data_to_ary('orders', $where.' group by order_time', 'order_time', 'total', "$total_field,$date_field");
				$time_e_obj=array(new DateTime(date('Ymd', $time_e[0])), new DateTime(date('Ymd', $time_e[1])));
				
				$days=0;
				while($time_e_obj[0]<=$time_e_obj[1]){
					if($days>30) break;
					$date_key=$time_e_obj[0]->format('Ymd');
					$d_date_key=$time_e_obj[0]->format('m/d');
					if($charts_data['xAxis']['categories'][$days]){
						$charts_data['xAxis']['categories'][$days].=' vs '.$d_date_key;
						$charts_data['series'][1]['data'][]=(float)sprintf('%01.2f', $order_row[$date_key]);
					}
					$time_e_obj[0]->modify('1 days');
					$days++;
				}
			}
			
			$orders_data['orders_charts']=$charts_data;
		//}
		
		ly200::e_json($orders_data, 1);
	}
	
	//复购率
	public static function get_orders_repurchase_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Terminal=(int)$p_Terminal;
		$p_Cycle=(int)$p_Cycle?(int)$p_Cycle:2;
		$data=$return_data=array();
		$where="OrderStatus in(4,5,6)";
		$p_Terminal>=1 && $where.=' and Source='.($p_Terminal-1);
		
		if($p_Cycle==3){//按年
			$year=date('Y', $c['time']);
			for($i=($year-3); $i<=$year; $i++){
				$s_time=@strtotime("{$i}-01-01");
				$e_time=@(int)strtotime(($i+1)."-01-01")-1;
				$w=$where." and OrderTime between {$s_time} and {$e_time}";
				
				$reorder_count=db::get_row_count('orders', $w." and IsNewCustom=0");
				$first_order_count=db::get_row_count('orders', $w." and IsNewCustom=1");
				$key=@sprintf($c['manage']['lang_pack']['mta']['cycle_year'], $i);
				$data[$key]=array(
					'first'			=>	$first_order_count,
					'reorder'		=>	$reorder_count,
					'total'			=>	$first_order_count+$reorder_count,
					'reorder_rate'	=>	@round($reorder_count/($first_order_count+$reorder_count), 4)
				);
			}
		}else if($p_Cycle==1){//按月
			$year=date('Y', $c['time']);
			$month=date('m', $c['time']);
			$k=$month+1;
			$k>12 && $k=1;
			for($i=0;$i<12;$i++){
				$y=$k>$month?($year-1):$year;
				$s_time=@strtotime("{$y}-{$k}-01");
				
				$e_year=$y;
				$e_month=$k+1;
				$e_month>12 && $e_year=$e_year+1;
				$e_month>12 && $e_month=1;
				$e_time=@(int)strtotime("{$e_year}-{$e_month}-01")-1;
				
				$w=$where." and OrderTime between {$s_time} and {$e_time}";
				
				$reorder_count=db::get_row_count('orders', $w." and IsNewCustom=0");
				$first_order_count=db::get_row_count('orders', $w." and IsNewCustom=1");
				$key=$c['manage']['config']['ManageLanguage']=='en'?@date('F', $s_time).','.$y:@sprintf($c['manage']['lang_pack']['mta']['cycle_month'], $y, $k);
				$data[$key]=array(
					'first'			=>	$first_order_count,
					'reorder'		=>	$reorder_count,
					'total'			=>	$first_order_count+$reorder_count,
					'reorder_rate'	=>	@round($reorder_count/($first_order_count+$reorder_count), 4)
				);
				
				if($k==12) $k=1; else $k++;
			}
		}else{//按季
			$year=date('Y', $c['time']);
			$month=date('m', $c['time']);
			$season_ary=array(
				1	=>	array(1,3),
				2	=>	array(4,6),
				3	=>	array(7,9),
				4	=>	array(10,12)
			);
			foreach($season_ary as $k=>$v){
				if($month>=$v[0] && $month<=$v[1]){
					$CurrentQuarter=$k;
					break;
				}
			}
			
			$k=$CurrentQuarter+1;
			$k>4 && $k=4;
			for($i=0;$i<4;$i++){
				$y=$k>$CurrentQuarter?($year-1):$year;
				$s_year=$e_year=$y;
				$s_month=$season_ary[$k][0];
				$s_time=@strtotime("{$s_year}-{$s_month}-01");
				
				$e_month=$season_ary[$k][1]+1;
				$e_month>12 && $e_year=$e_year+1;
				$e_month>12 && $e_month=1;
				$e_time=@(int)strtotime("{$e_year}-{$e_month}-01")-1;
				
				$w=$where." and OrderTime between {$s_time} and {$e_time}";
				
				$reorder_count=db::get_row_count('orders', $w." and IsNewCustom=0");
				$first_order_count=db::get_row_count('orders', $w." and IsNewCustom=1");
				$key=@sprintf($c['manage']['lang_pack']['mta']['cycle_season_ary'][$k-1], $y);
				$data[$key]=array(
					'first'			=>	$first_order_count,
					'reorder'		=>	$reorder_count,
					'total'			=>	$first_order_count+$reorder_count,
					'reorder_rate'	=>	@round($reorder_count/($first_order_count+$reorder_count), 4)
				);
				
				if($k==4) $k=1; else $k++;
			}
		}
		
		//line charts
		$charts_data=array();
		$charts_data['title']['text']='';
		$charts_data['colors']=array('#4289ff', '#7646d0', '#0dae84', '#f7a35c', '#91e8e1',  '#f15c80', '#e4d354', '#8d4653');
		$charts_data['yAxis']=array(array(
										'labels'	=>array('format'=>'{value}%'),
										'title'		=>array('text'=>$c['manage']['lang_pack']['mta']['order']['repurchase_rate'])//复购率
									),array(
										'labels'	=>array('format'=>'{value}'),
										'title'		=>array('text'=>$c['manage']['lang_pack']['mta']['order']['order_count_to']),//订单笔数
										'opposite'	=>'true'
									));
		
		for($i=0;$i<4;$i++){
			if($i==3){
				$charts_data['series'][$i]['type']='spline';
			}else{
				$charts_data['series'][$i]['type']='column';
				$charts_data['series'][$i]['yAxis']=1;
			}
			
			$charts_data['series'][$i]['name']=$c['manage']['lang_pack']['mta']['order']['repurchase_ary'][$i];
			//$charts_data['series'][$i]['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['order']['repurchase_ary'][$i].': ';
		}

		foreach((array)$data as $k=>$v){
			$charts_data['xAxis']['categories'][]=$k;
			$charts_data['series'][0]['data'][]=(int)$v['first'];//新客户订单
			$charts_data['series'][1]['data'][]=(int)$v['reorder'];//老客户订单
			$charts_data['series'][2]['data'][]=(int)$v['total'];//总订单量
			$charts_data['series'][3]['data'][]=$v['reorder_rate']*100;//复购率				
		}


		$return_data['repurchase_charts']=$charts_data;
		
		ly200::e_json($return_data, 1);
	}
	
	/*
	* 产品部分(products)
	* 1. 产品销量排行(get_products_sales_data)
	*/
	public static function get_products_sales_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$page_count=50;
		//$page=(int)$p_page?(int)$p_page:1;
		$page=1;
		$start_row=($page-1)*$page_count;
		$p_Terminal=(int)$p_Terminal;

		$w="o.OrderStatus in(4,5,6)";
		if($p_TimeS){
			$time_s=where::time($p_TimeS, '', !in_array($p_TimeS, array(0,-1)));
			$w.=" and (o.OrderTime between {$time_s[0]} and {$time_s[1]})";
		}
		$p_Terminal>=1 && $w.=' and o.Source='.($p_Terminal-1);
		$products_list=db::get_limit('(orders_products_list p left join orders o on p.OrderId=o.OrderId) left join products p1 on p.ProId=p1.ProId', $w." group by p.ProId", 'p.ProId,p.Name,p.PicPath,sum(p.Qty) as count,count(o.OrderId) as order_count,p1.Prefix,p1.Number', 'count desc, p.ProId desc', $start_row, $page_count);
		
		$return_data=array();
		foreach($products_list as $key=>$val){
			$path=ly200::str_to_url($val['Name']);
			$url='/'.$path.'_p'.sprintf('%04d', $val['ProId']).'.html';

			$country_row=db::get_one('orders_products_list p left join orders o on p.OrderId=o.OrderId',  "p.ProId='{$val['ProId']}' and {$w} group by o.ShippingCId", 'o.ShippingCId,o.ShippingCountry,sum(p.Qty) as count,count(o.OrderId) as order_count', 'count desc');
			
			$related_row=db::get_one('orders_products_list p left join products p1 on p.ProId=p1.ProId', "p.ProId!='{$val['ProId']}' and p.ProId!=0 and OrderId in(select o.OrderId from orders o left join orders_products_list t2 on o.OrderId=t2.OrderId where $w and t2.ProId='{$val['ProId']}') group by p.ProId",  'p.ProId,p.Name,sum(p.Qty) as count,count(OrderId) as order_count,p1.Prefix,p1.Number', 'order_count desc, count desc, p.ProId desc');//,p.PicPath
			
			//$related_url='/'.ly200::str_to_url($related_row['Name']).'_p'.sprintf('%04d', $related_row['ProId']).'.html';
			
			$return_data[]=array(
				'No'				=>	$start_row+$key+1,
				'ProId'				=>	$val['ProId'],
				'Name'				=>	$val['Name'],
				'Number'			=>	$val['Prefix'].$val['Number'],
				'PicPath'			=>	is_file($c['root_path'].$val['PicPath'])?$val['PicPath']:'',
				'Url'				=>	$url,
				'BuyCount'			=>	$val['count'],
				'OrderCount'		=>	$val['order_count'],
				//'CId'				=>	$country_row['ShippingCId'],
				'Country'			=>	$country_row['ShippingCountry'],
				'CountryBuyCount'	=>	$country_row['count'],
				'CountryOrderCount'	=>	$country_row['order_count'],
				'CountryBuyRate'	=>	@round(($country_row['count']/$val['count'])*100).'%',
				'CountryOrderRate'	=>	@round(($country_row['order_count']/$val['order_count'])*100).'%',
				'IsRelated'			=>	$related_row?1:0
				/*'Related'			=>	array(
											'ProId'		=>	$related_row['ProId'],
											'Name'		=>	$related_row['Name'],
											'Number'	=>	$related_row['Prefix'].$related_row['Number'],
											//'PicPath'	=>	is_file($c['root_path'].$related_row['PicPath'])?$related_row['PicPath']:'',
											'Url'		=>	$related_url,
											'BuyCount'	=>	$related_row['count'],
											'OrderCount'=>	$related_row['order_count']
										),*/
			);
		}
		
		ly200::e_json($return_data, 1);
	}
	
	public static function get_products_sales_related_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		$p_Terminal=(int)$p_Terminal;
		$return_data=array();

		$w="o.OrderStatus in(4,5,6)";
		if($p_TimeS){
			$time_s=where::time($p_TimeS, '', !in_array($p_TimeS, array(0,-1)));
			$w.=" and (o.OrderTime between {$time_s[0]} and {$time_s[1]})";
		}
		$p_Terminal>=1 && $w.=' and o.Source='.($p_Terminal-1);
		
		$products_list=db::get_one('orders_products_list p left join orders o on p.OrderId=o.OrderId', $w." and p.ProId='{$p_ProId}'", 'sum(p.Qty) as count,count(o.OrderId) as order_count', 'order_count desc, count desc, p.ProId desc');
		
		$country_row=db::get_limit('orders_products_list p left join orders o on p.OrderId=o.OrderId',  "p.ProId='{$p_ProId}' and {$w} group by o.ShippingCId", 'o.ShippingCountry,sum(p.Qty) as count,count(o.OrderId) as order_count', 'count desc', 0, 20);//o.ShippingCId,
		foreach($country_row as $key=>$val){
			$return_data['country'][]=array(
				'No'				=>	$key+1,
				'Country'			=>	$val['ShippingCountry'],
				'CountryBuyCount'	=>	$val['count'],
				'CountryOrderCount'	=>	$val['order_count'],
				'CountryBuyRate'	=>	@round(($val['count']/$products_list['count'])*100).'%',
				'CountryOrderRate'	=>	@round(($val['order_count']/$products_list['order_count'])*100).'%',
			);
		}
		
		$related_row=db::get_limit('orders_products_list p left join products p1 on p.ProId=p1.ProId', "p.ProId!='{$p_ProId}' and p.ProId!=0 and OrderId in(select o.OrderId from orders o left join orders_products_list t2 on o.OrderId=t2.OrderId where $w and t2.ProId='{$p_ProId}') group by p.ProId",  'p.ProId,p.Name,sum(p.Qty) as count,count(OrderId) as order_count,p1.Prefix,p1.Number,p1.PicPath_0', 'order_count desc, count desc, p.ProId desc', 0, 20);
		foreach($related_row as $key=>$val){
			$url='/'.ly200::str_to_url($val['Name']).'_p'.sprintf('%04d', $val['ProId']).'.html';
			$return_data['related'][]=array(
				'No'				=>	$key+1,
				'ProId'				=>	$val['ProId'],
				'PicPath'			=>	ly200::get_size_img($val['PicPath_0'], end($c['manage']['resize_ary']['products'])),
				'Name'				=>	$val['Name'],
				'Number'			=>	$val['Prefix'].$val['Number'],
				'Url'				=>	$url,
				'BuyCount'			=>	$val['count'],
				'OrderCount'		=>	$val['order_count'],
			);
		}		
		
		ly200::e_json($return_data, 1);
	}
	
	/*
	* 会员部分(user)
	* 1. 会员统计(get_user_data)
	*/
	public static function get_user_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$month_list=array();
		for($i=0; $i<12; $i++){
			$month_list[]=@date('Y-m', strtotime("-{$i} month", strtotime(date('Y-m-01', $c['time']))));
		}
		$StartTime=@strtotime(end($month_list));
		$user_data=array('total'=>array());
		
		$month_ary=array_reverse($month_list);
		$save_dir=$c['tmp_dir'].'manage/';
		$filename='user-statistics.json';
		if(!file::check_cache($c['root_path'].$save_dir.$filename, 0)){//文件是否存在、是否到更新时间
			/***********************************会员趋势(start)***********************************/
			$where="RegTime>{$StartTime}";
			$user_count='count(UserId) as user_count';
			$date_field="FROM_UNIXTIME(RegTime, '%Y-%m') as reg_time";
			$new_user_row=ly200::get_table_data_to_ary('user', $where.' group by reg_time', 'reg_time', 'user_count', "$user_count,$date_field");
			$user_statistics=array();
			$total_user_count=0;//最近12个月新会员总数量
			foreach($month_ary as $k=>$v){
				$time_s=strtotime($v);
				$time_e=$k==11?$c['time']:strtotime($month_ary[$k+1]);
				/*
				//活跃会员
				$w="OperationType=1 and (AccTime between {$time_s} and {$time_e})";
				$active_member=db::get_row_count('user_operation_log', $w.' group by UserId', 'LId');
				//核心会员
				$w="OrderStatus in(4,5,6) and UserId>0 and (OrderTime between {$time_s} and {$time_e})";
				$core_member=db::get_row_count('orders', $w.' group by UserId', 'OrderId');
				*/
				$user_ary=array();
				$w="OrderStatus in(4,5,6) and UserId>0 and (OrderTime between {$time_s} and {$time_e})";
				$buy_member=db::get_all('orders', $w.' group by UserId', 'UserId');
				foreach($buy_member as $value){
					$user_ary[]=$value['UserId'];
				}
				$w=($buy_member?'UserId in('.implode(',', $user_ary).')':'UserId="-1"');
				//已购买会员
				$active_member=db::get_row_count('user', $w.' and ConsumptionTime=1', 'UserId');
				//复购会员
				$core_member=db::get_row_count('user', $w.' and ConsumptionTime>1', 'UserId');
				//总会员
				$w="RegTime<{$time_e}";
				$total_member=db::get_row_count('user', $w.' and (IsRegistered=1 or (IsRegistered=0 and ConsumptionTime>0)) group by UserId', 'UserId');

				$user_statistics['trend'][$v]=array(
					'new'		=>	(int)$new_user_row[$v],
					'active'	=>	(int)$active_member,
					'core'		=>	(int)$core_member,
					'total'		=>	(int)$total_member
				);
				$total_user_count+=(int)$new_user_row[$v];
			}
			$user_statistics['total_member']=$total_user_count;
			/***********************************会员趋势(end)***********************************/
			
			if($total_user_count){
				/***********************************会员性别、等级、年龄、快捷登录(start)***********************************/
				$lang=db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');
				//会员等级
				$level_row=db::get_all('user_level', 'IsUsed=1', "LId,Name_{$lang} as Name");
				$level_ary=array();
				foreach($level_row as $k=>$v){
					$key=$v['Name']?$v['Name']:'No Level';
					$level_user_count=db::get_row_count('user', $where." and Level='{$v['LId']}'", 'UserId');
					$level_ary[$key]=(int)$total_user_count?(float)sprintf('%01.2f', ($level_user_count/$total_user_count)*100):0;
				}
				$level_user_count=db::get_row_count('user', $where." and Level=0", 'UserId');
				$level_user_count && $level_ary['No Level']=(int)$total_user_count?(float)sprintf('%01.2f', ($level_user_count/$total_user_count)*100):0;
				@arsort($level_ary);
				$user_statistics['user_detail']['level']=$level_ary;
				
				//性别
				$gender_ary=array();
				foreach($c['gender'] as $k=>$v){
					$gender_count=db::get_row_count('user', "Gender='{$k}' and {$where}", 'UserId');
					$gender_ary[$v]=(int)$total_user_count?(float)sprintf('%01.2f', ($gender_count/$total_user_count)*100):0;
				}
				@arsort($gender_ary);
				$user_statistics['user_detail']['gender']=$gender_ary;
				
				//年龄
				$age_ary=$user_ary=$count_ary=array();
				$year=date('Y', $c['time']);//当前年份
				$user_row=db::get_all('user', $where, 'UserId, Birthday');
				foreach($user_row as $k=>$v){
					$y=date('Y', strtotime($v['Birthday']));
					if($y<$year-34){
						$key='18-34';
					}elseif($y>$year-55){
						$key='55+';
					}else{
						$key='35-54';
					}
					$count_ary[$key]+=1;
				}
				foreach((array)$count_ary as $k=>$v){
					$age_ary[$k]=(int)$total_user_count?(float)sprintf('%01.2f', ($v/$total_user_count)*100):0;
				}
				@arsort($age_ary);
				$user_statistics['user_detail']['age']=$age_ary;
				
				//快捷登录
				$birthday_field="FROM_UNIXTIME(Birthday, '%Y-%m') as age_time";
				$login_row=db::get_all('sign_in', '1');
				$login_ary=array();
				foreach($login_row as $k=>$v){
					$login_count=db::get_row_count('user', "{$v['Title']}Id!='' and {$where}", "UserId");
					$login_ary[$v['Title']]=(int)$total_user_count?(float)sprintf('%01.2f', ($login_count/$total_user_count)*100):0;
				}
				@arsort($login_ary);
				$user_statistics['user_detail']['login']=$login_ary;
				/***********************************会员性别、等级、年龄、快捷登录(end)***********************************/
	
	
				/***********************************国家分布(start)***********************************/
				$user_row=db::get_all('user_address_book a left join country c on a.CId=c.CId', "a.UserId in(select UserId from user where $where) group by a.UserId", "a.CId,c.Country");
				$country_ary=array();
				$country_user_count=0;//有国家记录的会员数量
				foreach($user_row as $v){
					if($v['Country']){
						$country_ary[$v['Country']]=(int)$country_ary[$v['Country']]+1;
						$country_user_count++;
					}
				}
				@arsort($country_ary);
				$user_statistics['country']['country_ary']=$country_ary;
				$user_statistics['country']['country_user_count']=$country_user_count;
				/***********************************国家分布(end)***********************************/
			}
			@file::write_file($save_dir, $filename, str::json_data($user_statistics));
		}else{
			//加载内容格式
			$theme_object=file_get_contents($c['root_path'].$save_dir.$filename);
			$user_statistics=str::json_data($theme_object, 'decode');
		}
		$user_data['total']=$user_statistics['trend'][$month_list[0]];

		if((int)$p_trend){//首次进入加载图表
			/******************************************会员趋势图(start)******************************************/
			//line charts
			$charts_data=array();
			$charts_data['title']=array('text'=>$c['manage']['lang_pack']['mta']['user']['member_trend'], 'align'=>'left', 'margin'=>45);
			$charts_data['yAxis']=array('title'=>array('text'=>$c['manage']['lang_pack']['mta']['user']['member_count']));
			$charts_data['legend']=array('layout'=>'horizontal', 'align'=>'left', 'verticalAlign'=>'top', 'x'=>100, 'y'=>-78);
			$charts_data['series'][0]['type']='spline';
			$charts_data['series'][1]['type']='spline';
			$charts_data['series'][2]['type']='spline';
			$charts_data['series'][3]['type']='spline';
			$charts_data['series'][0]['name']=$c['manage']['lang_pack']['mta']['user']['new_member'].': ';
			$charts_data['series'][1]['name']=$c['manage']['lang_pack']['mta']['user']['active_member'].': ';
			$charts_data['series'][2]['name']=$c['manage']['lang_pack']['mta']['user']['core_member'].': ';
			$charts_data['series'][3]['name']=$c['manage']['lang_pack']['mta']['user']['total_member'].': ';
			$charts_data['series'][0]['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['user']['new_member'].': ';
			$charts_data['series'][1]['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['user']['active_member'].': ';
			$charts_data['series'][2]['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['user']['core_member'].': ';
			$charts_data['series'][3]['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['user']['total_member'].': ';
	
			foreach($user_statistics['trend'] as $k=>$v){
				$charts_data['xAxis']['categories'][]=$k;
				$charts_data['series'][0]['data'][]=(int)$v['new'];//新会员
				$charts_data['series'][1]['data'][]=(int)$v['active'];//活跃会员
				$charts_data['series'][2]['data'][]=(int)$v['core'];//核心会员
				$charts_data['series'][3]['data'][]=(int)$v['total'];//总会员				
			}
			$user_data['trend_charts']=$charts_data;
			/******************************************会员趋势图(end)******************************************/


			/******************************************会员国家分布(start)******************************************/
			if($user_statistics['total_member']){
				//line charts
				$charts_data=array();
				$charts_data['title']=array('text'=>''/*$c['manage']['lang_pack']['mta']['user']['country_distribution']*/, 'align'=>'left', 'margin'=>45);
				$charts_data['legend']=array('enabled'=>false);
				$charts_data['yAxis']=array('title'=>array('text'=>$c['manage']['lang_pack']['mta']['user']['member_count']));
				$charts_data['series'][0]['name']='';
				$charts_data['series'][0]['type']='column';
				$charts_data['series'][0]['pointWidth']=40;
				$charts_data['series'][0]['tooltip']['valueSuffix']='%';
				/************************/
				$i=0;
				$total_ratio=0;
				$total_country_count=@count($user_statistics['country']['country_ary']);
				$other_count=$user_statistics['total_member']-$total_country_count;
				foreach($user_statistics['country']['country_ary'] as $k=>$v){
					if($i==10 || ($other_count>0 && $i++==9)){break;}
					
					$ratio=(int)$user_statistics['total_member']?(float)sprintf('%01.2f', ($v/$user_statistics['total_member'])*100):0;
					$total_ratio+=$ratio;
					$charts_data['xAxis']['categories'][]=$k;
					$charts_data['series'][0]['data'][]=$ratio;
				}
				if($total_country_count>10 || $other_count>0){
					$charts_data['xAxis']['categories'][]='Other';
					$charts_data['series'][0]['data'][]=100-$total_ratio;
				}
				/************************/
				$user_data['country_charts']=$charts_data;
			}
			/******************************************会员国家分布(end)******************************************/
			
			//会员等级
			@count($user_statistics['user_detail']['level']) && $user_data['user_detail']['level']=$user_statistics['user_detail']['level'];
			//性别
			@count($user_statistics['user_detail']['gender']) && $user_data['user_detail']['gender']=$user_statistics['user_detail']['gender'];
			//年龄
			@count($user_statistics['user_detail']['age']) && $user_data['user_detail']['age']=$user_statistics['user_detail']['age'];
			//快捷登录
			@count($user_statistics['user_detail']['login']) && $user_data['user_detail']['login']=$user_statistics['user_detail']['login'];
		}
		
		ly200::e_json($user_data, 1);
	}
}
?>