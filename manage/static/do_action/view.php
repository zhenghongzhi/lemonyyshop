<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class view_module{
	/*******************************可视化编辑(start)*****************************/
	public static function plugins_visual_save(){
		global $c;
		!$_POST['WId'] && ly200::e_json('', 0);
		$WId_ary=implode(',',$_POST['WId']);
		$themes_row=db::get_all('web_visual', "WId in ($WId_ary)");
		!$themes_row && ly200::e_json('', 0);
		foreach((array)$themes_row as $v){
			$update_data=array('Layout'=>(int)$_POST['Layout']["{$v['Pages']}-{$v['Position']}"]);
			$post_data=$_POST[$v['Pages']][$v['Position']];
			if($post_data){
				$post_data=str::str_code($post_data, 'stripslashes');
				$themes_data=str::json_data($v['Data'], 'decode');
				foreach((array)$c['manage']['config']['Language'] as $vv){
					/*
					if($v['Pages']=='banner' && $post_data[$vv]){  //Banner
						foreach((array)$post_data[$vv] as $key => $val){
							if(preg_match('/^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+/', $val['Banner']['url'])){	//远程图片下载
								$save_dir=$c['manage']['upload_dir'].'photo/';
								file::mk_dir($save_dir);
								$ext_name=file::get_ext_name($val['Banner']['url']);
								$save_name=str::rand_code().'.'.$ext_name;
								$filepath=ly200::curl($val['Banner']['url']);
								file::write_file($save_dir, $save_name, $filepath);
								$post_data[$vv][$key]['Banner']['url']=file::photo_add_item($save_dir.$save_name, $save_name, 0);  //加入图片银行
							}
						}
					}
					*/
					if(($v['Pages']=='banner' || $v['Pages']=='mbanner') && $post_data[$vv]){  //Banner排序
						$sort_ary=$new_sort_data=array();
						foreach((array)$post_data[$vv] as $k2 => $v2){
							if((!$v2['Banner']['url'] && $v['Pages']=='banner') || ($v['Pages']=='mbanner' && !$v2['Pic'])){  //没图片排到最后
								$sort_ary[$k2]=count($post_data[$vv])-1;
							}else{
								$sort_ary[$k2]=$v2['MyOrder'];
							}
							unset($post_data[$vv][$k2]['MyOrder']);
						}
						asort($sort_ary);
						foreach((array)$sort_ary as $k2 => $v2){
							$new_sort_data[]=$post_data[$vv][$k2];
						}
						$post_data[$vv]=$new_sort_data;
					}
					$post_data[$vv] && $themes_data[$vv]=$post_data[$vv];
				}
				$themes_data=str::json_data($themes_data);
				$themes_data=addslashes($themes_data);
				$update_data['Data']=$themes_data;
			}
			db::update('web_visual', "WId='{$v['WId']}'" , $update_data);
		}
		//生成移动端二维码
		file::mk_dir($c['tmp_dir'].'qrcode/');
		$filename=$c['tmp_dir'].'qrcode/visual_mobile.png';
		if(!is_file($c['root_path'].$filename)){
			require_once $c['root_path'].'inc/class/qrcode/qrlib.php';
			QRcode::png(ly200::get_domain(), $c['root_path'].$filename, 'M', 6, 2);
		}
		//清除页面缓存
		file::del_dir($c['tmp_dir'].'cache/');
		file::del_dir($c['tmp_dir'].'manage/');
		ly200::e_json($filename, 1);
	}

	public static function plugins_visual_statistics(){
		global $c;
		$AccTime=strtotime(date('Y-m-d', $c['time']));
		if (db::get_row_count('web_visual_statistics', "AccTime='{$AccTime}'")) {
			db::query("UPDATE `web_visual_statistics` SET `Count`=`Count`+1 where AccTime='{$AccTime}'");
		} else {
			db::insert('web_visual_statistics', array(
				'Count'		=>	1,
				'AccTime'	=>	$AccTime,
			));
		}
		// manage::operation_log('');
		ly200::e_json('', 1);
	}
	/*******************************可视化编辑(end)*****************************/

	/*******************************切换风格(start)*****************************/
	public static function switch_mobile_template(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$HomeTpl=$g_tpl;
		$data=array(
			'HomeTpl'	=>	$HomeTpl
		);
		manage::config_operaction($data, 'mobile');
		manage::operation_log('修改手机模板首页');
		ly200::e_json('', 1);
	}
	
	public static function download_all_themes(){  //下载所有风格
		global $c;
		if(substr_count(ly200::get_domain(), 'dev.shop') || substr_count(ly200::get_domain(), 'check.shop')) ly200::e_json('开发后台不允许下载', 0);
		if(!$_SESSION['Manage']['WebThemesList']['msg'][1]) ly200::e_json('查不到模板', 0);
		//设置session
		if(!$_SESSION['Manage']['WebDownloadThemes']) $_SESSION['Manage']['WebDownloadThemes']=$_SESSION['Manage']['WebThemesList']['msg'][1];
		//处理数组
		$deal=array_shift($_SESSION['Manage']['WebDownloadThemes']);
		$dir="{$c['root_path']}/static/themes/{$deal['Themes']}/";
		$p_themes=$deal['Themes'];
		$themes=(int)db::get_row_count('config_module', "Themes='{$deal['Themes']}'");  //数据库是否存在
		if(!@is_dir($dir) || !@is_dir($dir.'inc/') || !@is_file($dir.'index.php') || !@is_file($dir.'inc/themes_set.php') || !$p_themes){  //配置表没数据或者没前台文件才下载
			$data=array(
				'Action'	=>	'ueeshop_web_download_themes',
				'Project'	=>	0,
				'Themes'	=>	$deal['Themes'],
				'Version'	=>	$c['ProjectVersion']
			);
			$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
			if($result){
				$zip_file=file::write_file('/', $deal['Themes'].'.zip', $result['msg']);
				$zip=new ZipArchive;
				$res=$zip->open($c['root_path'].$zip_file);
				if($res===true){
					
					$zip->extractTo($dir, array('themes.php', 'visual.php'));
					include($dir.'themes.php');
					include($dir.'visual.php');
					(!@is_dir($dir) || !@is_dir($dir.'inc/') || !@is_file($dir.'index.php') || !@is_file($dir.'inc/themes_set.php')) && $zip->extractTo($dir);//文件不存在
					
					$zip->close();
					@unlink($dir.'themes.php');
					@unlink($dir.'visual.php');
					@unlink($dir.'zip.creating.txt');
				}
				@unlink($c['root_path'].$zip_file);
			}
		}
		if(count($_SESSION['Manage']['WebDownloadThemes'])){
			ly200::e_json($deal['Themes'].'风格成功下载...', 2);
		}else{
			ly200::e_json('风格已全部下载成功', 1);
		}
	}
	/*******************************切换风格(end)*****************************/

	/*******************************导航管理(start)*****************************/
	public static function nav_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Id=(int)$p_Id;
		$p_Unit=(int)$p_Unit;
		$p_UnitType=trim($p_UnitType);
		$p_NewTarget=(int)$p_NewTarget;
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($p_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		
		$p_UnitType=='' && $p_UnitType='add';
		
		if($p_UnitType=='add'){
			//自定义项
			$data=array();
			$data['Custom']=1;
			foreach($c['manage']['config']['Language'] as $k2=>$v2){
				$data["Name_{$v2}"]=${'p_UnitValue_'.$v2};
			}
			$data['Url']=$p_Url;
			$data['NewTarget']=$p_NewTarget;
		}else{
			//固定项
			$nav_ary=array();
			foreach($c['nav_cfg'] as $k=>$v){
				$nav_ary[$v['page']]=$k;
			}
			if($p_UnitType=='article'){
				$Page=$p_Unit;
			}elseif($p_UnitType=='products' || $p_UnitType=='category'){
				$p_UnitType='products';
				$Cate=$p_Unit;
			}
			$data=array(
				'Nav'		=>	$nav_ary[$p_UnitType],//导航栏目，详细请看 $c['nav_cfg']
				'Page'		=>	$Page?$Page:0,//单页
				'Cate'		=>	$Cate?$Cate:0,//产品
				'NewTarget'	=>	$p_NewTarget//新窗口
			);
			if($p_Type=='nav'){
				//图片
				$p_PicCount = 4;
				$Name=$Brief=$Url=$PicPath=array();
				$FormatAry=array();
				$save_dir=$c['manage']['upload_dir'].'photo/';
				foreach($c['manage']['web_lang_list'] as $v){
					for($i=0; $i<$p_PicCount; ++$i){
						$pic=$_POST['PicPath_'.$v][$i];
						if($pic && is_file($c['root_path'].$pic)){
							$pic=file::photo_tmp_upload($pic, $save_dir);
						}
						$FormatAry['ImgName'][$i][$v]=$_POST['ImgName_'.$v][$i];
						$FormatAry['Url'][$i][$v]=$_POST['Url_'.$v][$i];
						$FormatAry['PicPath'][$i][$v]=$pic;
					}
				}
				foreach($FormatAry as $k=>$v){
					for($i=0; $i<$p_PicCount; ++$i){
						${$k}[$i]=$v[$i];
					}
				}
				$data['ImgName_0']	=	$ImgName[0];
				$data['ImgName_1']	=	$ImgName[1];
				$data['ImgName_2']	=	$ImgName[2];
				$data['ImgName_3']	=	$ImgName[3];
				$data['Url_0']		=	$Url[0];
				$data['Url_1']		=	$Url[1];
				$data['Url_2']		=	$Url[2];
				$data['Url_3']		=	$Url[3];
				$data['PicPath_0']	=	$PicPath[0];
				$data['PicPath_1']	=	$PicPath[1];
				$data['PicPath_2']	=	$PicPath[2];
				$data['PicPath_3']	=	$PicPath[3];
			}
		}
		if($p_Id==0){//添加
			$nav_data[]=$data;
		}else{//修改
			$nav_data[$p_Id-1]=$data;
		}
		$NavData=addslashes(str::json_data(str::str_code($nav_data, 'stripslashes')));
		manage::config_operaction(array(($p_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log($p_Type=='nav'?'修改导航设置':'修改底部栏目设置');
		ly200::e_json('', 1);
	}
	
	public static function nav_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$MyOrderAry=@explode('|', $g_sort_order);
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($g_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		$data_ary=array();
		foreach((array)$MyOrderAry as $num){
			$data_ary[]=$nav_data[$num];
		}
		$NavData=addslashes(str::json_data(str::str_code($data_ary, 'stripslashes')));
		manage::config_operaction(array(($g_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log(($g_Type=='nav'?'导航排序':'底部栏目排序'));
		ly200::e_json('', 1);
	}
	
	public static function nav_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_Id=(int)$g_Id;
		!$g_Id && ly200::e_json('');
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($g_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		unset($nav_data[$g_Id-1]);
		$NavData=addslashes(str::json_data(str::str_code($nav_data, 'stripslashes')));
		manage::config_operaction(array(($g_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log(($g_Type=='nav'?'删除导航':'删除底部栏目'));
		ly200::e_json('', 1);
	}
	/*******************************导航管理(end)*****************************/
}
?>