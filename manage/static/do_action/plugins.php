<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class plugins_module{
	//全局start
	public static function app_install(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ClassName=trim($p_ClassName);
		$app_row=db::get_all('plugins', "Category='app' and ClassName='$p_ClassName'");
		if($app_row){
			if($p_ClassName=='swap_chain'){  //交换链接
				//连接OA设置开启状态
				$swap_chain_data=array(
					'Action'	=>	'ueeshop_links_set_status',
					'Status'	=>	1
				);
				$result=ly200::api($swap_chain_data, $c['ApiKey'], $c['api_url']);
				!$result['ret'] && ly200::e_json($result['msg'], 0);
			}
			//处理plugins表
			$data=array('IsInstall'=>1);
			if(!in_array($p_ClassName, (array)$c['manage']['plugins']['charge'])){//不是单独收费的APP
				$data['IsUsed']=1;
			}
			db::update('plugins', "Category='app' and ClassName='$p_ClassName'", $data);
			manage::operation_log('安装应用：'.$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName]);
			//处理config表
			if($p_ClassName=='overseas'){//海外仓
				manage::config_operaction(array('Overseas'=>1), 'global');
			}elseif($p_ClassName=='freight'){//运费估算
				$products_show=db::get_value('config', 'GroupId="products_show" and Variable="Config"', 'Value');
				$data=str::json_data(htmlspecialchars_decode($products_show), 'decode');
				$data['freight']=1;
				$json_data=str::json_data($data);
				manage::config_operaction(array('Config'=>$json_data), 'products_show');
			}elseif($p_ClassName=='pdf'){//PDF格式
				$products_show=db::get_value('config', 'GroupId="products_show" and Variable="Config"', 'Value');
				$data=str::json_data(htmlspecialchars_decode($products_show), 'decode');
				$data['pdf']=1;
				$json_data=str::json_data($data);
				manage::config_operaction(array('Config'=>$json_data), 'products_show');
			}elseif($p_ClassName){
				// manage::config_operaction(array('Config'=>$json_data), 'products_show');
			}
			ly200::e_json('安装成功', 1);
		}else{
			ly200::e_json('安装失败', 0);
		}
	}
	
	public static function app_account_info(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ClassName=trim($p_ClassName);
		$oauth_ary=array('Facebook'=>'facebook_login', 'Google'=>'google_login', 'Paypal'=>'paypal_login', 'VK'=>'vk_login', 'Twitter'=>'twitter_login', 'Instagram'=>'instagram_login');
		$ReturnData=array();
		if(in_array($p_ClassName, (array)$oauth_ary)){
			//第三方登录
			$Title=array_search($p_ClassName, $oauth_ary);
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['SignIn']){
				$ReturnData['Data']['SignIn']=$data['SignIn']['Data'];
			}
		}elseif($p_ClassName=='facebook_pixel'){
			//Facebook像素
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='Facebook'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['Pixel']){
				$ReturnData['Data']['Pixel']=$data['Pixel']['Data'];
			}
		}elseif($p_ClassName=='facebook_messenger'){
			//Facebook Messenger
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='Facebook'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['PageId']){
				$ReturnData['Data']['PageId']=$data['PageId']['Data'];
			}
		}elseif($p_ClassName=='facebook_store'){
			//Facebook专页
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='Facebook'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['SignIn']){
				$ReturnData['Data']['SignIn']=$data['SignIn']['Data'];
			}
			$ReturnData['Url']=ly200::get_domain().'/store/';
		}elseif($p_ClassName=='facebook_ads_extension'){
			//Facebook广告插件
			$ReturnData=array('FbData'=>array(), 'Config'=>array());
			$data=array(
				'ApiKey'		=>	'ueeshop_sync',
				'Action'		=>	'get_facebook_ads_toolbox',
				'ApiName'		=>	'facebook',
				'Number'		=>	$c['Number'],
				'timestamp'		=>	$c['time'],
				'Domain'		=>	ly200::get_domain(1),
				'Currency'		=>	$_SESSION['Manage']['Currency']['Currency']
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
			if($result['ret']==1){
				$ReturnData=$result['msg'];
                $ReturnData['domain']=ly200::get_domain();
			}
		}elseif($p_ClassName=='massive_email'){
			$MailChimpOauthData=db::get_value('config', 'GroupId="MailChimp" and Variable="MailChimpOauth"', 'Value');
			$MailChimpOauthAry=str::json_data($MailChimpOauthData, 'decode');
			if(!empty($MailChimpOauthAry)){
				$ReturnData['Email']=$MailChimpOauthAry['email'];
				$ReturnData['Key']=$MailChimpOauthAry['access_token'].'-'.$MailChimpOauthAry['dc'];
			}
		}elseif($p_ClassName=='google_pixel'){
			//GA代码
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='Google'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['Pixel']){
				$ReturnData['Data']['Pixel']=$data['Pixel']['Data'];
			}
		}elseif($p_ClassName=='batch_edit'){
			$batch_edit_row=str::json_data(str::str_code(db::get_value('config', 'GroupId="products" and Variable="batch_edit"', 'Value'), 'htmlspecialchars_decode'), 'decode');
			$html='';
			$html.='<div style="padding:10px 0;">'.$c['manage']['lang_pack']['plugins']['batch_edit']['title'].'</div><div class="rows fl" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['product_sales'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="Sales" value="'.$batch_edit_row['Sales'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="rows fr" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['favorite'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="FavoriteCount" value="'.$batch_edit_row['FavoriteCount'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="clear"></div>';
			$html.='<div class="rows fl" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['default_review_rating'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="DefaultReviewRating" value="'.$batch_edit_row['DefaultReviewRating'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="rows fr" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['default_review_count'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="DefaultReviewTotalRating" value="'.$batch_edit_row['DefaultReviewTotalRating'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="clear"></div>';
			$html.='<div class="rows clean">';
				$html.='<label></label>';
				$html.='<div class="input">';
					$html.='<input type="button" class="btn_global btn_submit btn_authorized btn_save" value="'.$c['manage']['lang_pack']['global']['save'].'">';
				$html.='</div>';
			$html.='</div>';
			$ReturnData['Content']=$html;
		}elseif($p_ClassName=='paypal_marketing_solution'){
			//Paypal营销解决方案
			$container_data=str::json_data(str::str_code(db::get_value('config', 'GroupId="paypal" and Variable="MarketingSolution"', 'Value'), 'htmlspecialchars_decode'), 'decode');
			if($container_data['ID']){
				$ReturnData['ContainerId']=$container_data['ID'];
			}
		} elseif ($p_ClassName == 'dhl_account_info') {
			$ReturnData['ClientId'] = $ReturnData['Password'] = $ReturnData['pickupAccountId'] = $ReturnData['soldToAccountId'] = '';
			//DHL账号信息
			if (db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountInfo"', 'Value')) {
				$account_info_data = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountInfo"', 'Value'), 'htmlspecialchars_decode'), 'decode');
				$ReturnData['ClientId'] = $account_info_data['ClientId'] ? $account_info_data['ClientId'] : '';
				$ReturnData['Password'] = $account_info_data['Password'] ? $account_info_data['Password'] : '';
				$ReturnData['pickupAccountId'] = $account_info_data['pickupAccountId'] ? $account_info_data['pickupAccountId'] : '';
				$ReturnData['soldToAccountId'] = $account_info_data['soldToAccountId'] ? $account_info_data['soldToAccountId'] : '';
			}
		} elseif ($p_ClassName == 'asiafly') {
			$ReturnData['merchantCode'] = $ReturnData['privateKey'] = '';
			//DHL账号信息
			if (db::get_value('config', 'GroupId="plugins" and Variable="Asiafly"', 'Value')) {
				$account_info_data = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="Asiafly"', 'Value'), 'htmlspecialchars_decode'), 'decode');
				$ReturnData['merchantCode'] = $account_info_data['merchantCode'];
				$ReturnData['privateKey'] = $account_info_data['privateKey'];
			}
		}
		$ReturnData['Title']=$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName];
		$ReturnData['Tips']=$c['manage']['lang_pack']['plugins']['success_tips'][$p_ClassName];
		$ReturnData['Label']=$c['manage']['lang_pack']['plugins']['global']['parameter'];
		//URL链接
		foreach($c['manage']['plugins']['module'] as $k=>$v){
			foreach($v as $k2=>$v2){
				if($p_ClassName==$v2){
					$s_url=$v2;
					if(@array_key_exists($v2, (array)$c['manage']['sync_ary'])){
						$s_url="sync&d={$v2}";
					}
					$url="./?m={$k}&a=".$s_url;
					$ReturnData['ReturnUrl']=$url;
				}
			}
		}
		ly200::e_json($ReturnData, 1);
	}
	
	public static function app_account_info_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_ClassName=trim($p_ClassName);
		$oauth_ary=array('Facebook'=>'facebook_login', 'Google'=>'google_login', 'Paypal'=>'paypal_login', 'VK'=>'vk_login', 'Twitter'=>'twitter_login', 'Instagram'=>'instagram_login');
		$data_ary=array();
		if(in_array($p_ClassName, (array)$oauth_ary)){
			//第三方登录
			$Title=array_search($p_ClassName, $oauth_ary);
			$row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$key='SignIn';
		}elseif($p_ClassName=='facebook_pixel'){
			//Facebook像素
			$Title='Facebook';
			$row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$key='Pixel';
		}elseif($p_ClassName=='facebook_messenger'){
			//Facebook Messenger
			$Title='Facebook';
			$row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$key='PageId';
		}elseif($p_ClassName=='facebook_store'){
			//Facebook店铺
			$Title='Facebook';
			$row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$key='SignIn';
		}elseif($p_ClassName=='google_pixel'){
			//GA代码
			$Title='Google';
			$row=str::str_code(db::get_one('sign_in', "Title='$Title'"));
			$key='Pixel';
		}
		$data=str::json_data(htmlspecialchars_decode($row['Data']), 'decode');
		//处理数据
		$json_ary=array();
		foreach((array)$p_Value as $Key=>$Val){
			$json_ary[$Key]['IsUsed']=1;//默认开启
			$error=0;
			$name=$p_Name[$Key];
			$value=$p_Value[$Key];
			for($i=0, $len=count($value); $i<$len; $i++){
				if(str::str_str($value[$i], array("'", '"', '&', '<', '>', '&quot;', '&#039;', '&amp;', '&lt;', '&gt;'))){
					$value[$i]='';
				}
				$json_ary[$Key]['Data'][$name[$i]]=$value[$i];
				!$value[$i] && $error=1;//其中一项内容为空
			}
			$error==1 && $json_ary[$Key]['IsUsed']=0;
		}
		if ($p_ClassName == 'massive_email') {
			//邮件群发
			// $json_data=addslashes(str::json_data(str::str_code($json_ary['Email']['Data'], 'stripslashes')));
			// $data=array('Massive'=>$json_data);
			// manage::config_operaction($data, 'email');
			// 授权请求
			mailchimp::request_authorization_data();
		} elseif ($p_ClassName == 'dhl_account_info') {
			//DHL账号信息
			$info_ary = array(
				'ClientId'  		=>	$p_ClientId,
				'Password'  		=>	$p_Password,
				'pickupAccountId'	=>	$p_pickupAccountId,
				'soldToAccountId'	=>	$p_soldToAccountId,
			);
			$InfoData=addslashes(str::json_data($info_ary));
			manage::config_operaction(array('DHLAccountInfo' => $InfoData), 'plugins');
		} elseif ($p_ClassName == 'batch_edit') {
			$batch_ary = array(
				'Sales'						=>	(int)$p_Sales,
				'FavoriteCount'				=>	(int)$p_FavoriteCount,
				'DefaultReviewRating'		=>	(int)$p_DefaultReviewRating,
				'DefaultReviewTotalRating'	=>	(int)$p_DefaultReviewTotalRating,
		    );
			$batch_ary = addslashes(str::json_data($batch_ary));
			$data['products'] = array(
				'batch_edit'    =>  $batch_ary,
			);
			foreach ($data as $k => $v) {
				manage::config_operaction($v, $k);
			}
			manage::operation_log('修改应用：'.$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName]);
		} else if ($p_ClassName == 'asiafly') {
			// 亚翔供应链
			$info_ary = array(
				'merchantCode'  =>	$p_merchantCode,
				'privateKey'    =>	$p_privateKey
			);
			$InfoData = addslashes(str::json_data($info_ary));
			manage::config_operaction(array('Asiafly' => $InfoData), 'plugins');
		} else {
			//其他
			$data = array_merge($data, $json_ary);
			$json_data = addslashes(str::json_data(str::str_code($data, 'stripslashes')));
			db::update('sign_in', "Title='$Title'", array('Data'=>$json_data));
		}
		ly200::e_json('', 1);
	}
    
    //获取Facebook口令数据
    public static function app_get_facebook_token_info()
    {
        global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
        $data = array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'get_facebook_token_info',
			'ApiName'		=>	'facebook',
			'Number'		=>	$c['Number'],
			'timestamp'		=>	$c['time']
		);
		$data['sign'] = ly200::sign($data, $c['ApiKey']);
		$result = str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
        if ($result) {
            ly200::e_json($result['msg'], 1);
        } else {
            ly200::e_json('', 0);
        }
        ly200::e_json($appId, 1);
    }
	
	public static function app_facebook_ads_extension_edit()
    {
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$data = array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'set_facebook_ads_extension',
			'ApiName'		=>	'facebook',
			'Number'		=>	$c['Number'],
			'timestamp'		=>	$c['time'],
			'catalog_id'	=>	$p_catalog_id,
			'pixel_id'		=>	$p_pixel_id,
			'setting_id'	=>	$p_setting_id,
			'page_id'		=>	$p_page_id,
			'page_token'	=>	$p_page_token
		);
		$data['sign'] = ly200::sign($data, $c['ApiKey']);
		$result = str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		//Facebook像素
		if ($p_pixel_id) {
			$oauth_row = str::str_code(db::get_one('sign_in', "Title='Facebook'"));
			$data = str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			//if($data['Pixel'] && !$data['Pixel']['Data']['PixelID']){
				$data['Pixel']['IsUsed'] = 1;
				$data['Pixel']['Data']['PixelID'] = $p_pixel_id;
			//}
			$json_data = addslashes(str::json_data(str::str_code($data, 'stripslashes')));
			db::update('sign_in', "Title='Facebook'", array('Data'=>$json_data));
		}
		ly200::e_json('', 1);
	}
	
	public static function app_facebook_messenger_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$IsUsed=($p_is_messenger_chat_plugin_enabled==='true'?1:0);//是否开启Facebook Messenger功能
		$json_data=addslashes(str::json_data(str::str_code($_POST, 'stripslashes')));
		manage::config_operaction(array('facebook_messenger'=>$json_data), 'facebook');
		//APP
		db::update('plugins', 'Category="app" and ClassName="facebook_messenger"', array('IsInstall'=>$IsUsed, 'IsUsed'=>$IsUsed));
		//Page ID
		if($p_page_id){
			$oauth_row=str::str_code(db::get_one('sign_in', "Title='Facebook'"));
			$data=str::json_data(htmlspecialchars_decode($oauth_row['Data']), 'decode');
			if($data['PageId'] && !$data['PageId']['Data']['page_id']){
				$data['PageId']['IsUsed']=$IsUsed;
				$data['PageId']['Data']['page_id']=$p_page_id;
			}
			$json_data=addslashes(str::json_data(str::str_code($data, 'stripslashes')));
			db::update('sign_in', "Title='Facebook'", array('Data'=>$json_data));
		}
		ly200::e_json('', 1);
	}
	
	public static function app_paypal_marketing_solution_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($p_ContainerId){
			$data=array('ID'=>$p_ContainerId);
			$json_data=addslashes(str::json_data(str::str_code($data, 'stripslashes')));
			$data=array('MarketingSolution'=>$json_data);
			manage::config_operaction($data, 'paypal');
		}
		ly200::e_json('', 1);
	}
	
	public static function app_switch(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ClassName=trim($p_ClassName);
		$html='';
		
		if($p_ClassName=='block_access'){
			//屏蔽访问
			$switch_ary=array(
				'IsIP'=>array('Name'=>'shield_ip', 'Note'=>'ip_notes'),
				'IsChineseBrowser'=>array('Name'=>'shield_browser', 'Note'=>'browser_notes'),
				'IsCopy'=>array('Name'=>'cannot_copy', 'Note'=>'cannot_copy_notes'),
				'IsCloseWeb'=>array('Name'=>'close_web', 'Note'=>''),
			);
			$html='<ul class="data_list">';
			foreach($switch_ary as $k=>$v){
				if($c['FunVersion']<1 && ($k=='IsIP' || $k=='IsChineseBrowser')) continue;
				if($c['FunVersion']==100 && $k=='IsCloseWeb') continue;
				$html.=	'<li>';
				$html.=		'<div class="switchery'.($c['manage']['config'][$k]?' checked':'').'">';
				$html.=			'<input type="checkbox" name="'.$k.'" value="1"'.($c['manage']['config'][$k]?' checked':'').'>';
				$html.=			'<div class="switchery_toggler"></div>';
				$html.=			'<div class="switchery_inner"><div class="switchery_state_on"></div><div class="switchery_state_off"></div></div>';
				$html.=		'</div>'.$c['manage']['lang_pack']['set']['config'][$v['Name']];
				if($v['Note']){
				$html.=		'<span class="tool_tips_ico" content="'.$c['manage']['lang_pack']['set']['config'][$v['Note']].'"></span>';
				}
				$html.=	'</li>';
			}
			if($c['FunVersion']!=100){
				$html.=	'<div class="rows clean close_web_box" style="display:'.($c['manage']['config']['IsCloseWeb']?'block':'none').';">';
				$html.=		'<label>'.$c['manage']['lang_pack']['set']['config']['close_web'].'<div class="tab_box">'.manage::html_tab_button().'</div></label>';
				$html.=		'<div class="input">';
				foreach($c['manage']['config']['Language'] as $k=>$v){
				$html.=			'<div class="tab_txt tab_txt_'.$v.'" '.($c['manage']['config']['LanguageDefault']==$v?'style="display:block;"':'').'>'.manage::Editor_Simple("CloseWeb_{$v}", $c['manage']['config']['CloseWeb']["CloseWeb_{$v}"], '100%', 100).'</div>';
				}
				$html.=		'</div>';
				$html.=		'<label></label>';
				$html.=		'<div class="input">';
					$html.=		'<input type="button" class="btn_global btn_submit btn_save" style="margin-right:15px;" value="'.$c['manage']['lang_pack']['global']['save'].'">';
					$html.=		'<div class="clear"></div>';
				$html.=		'</div>';
				$html.=	'</div>';
			}
			$ReturnData['Content']=$html;
		}elseif($p_ClassName=='batch_edit'){
			//批量修改数值
			$batch_edit_row=str::json_data(str::str_code(db::get_value('config', 'GroupId="products" and Variable="batch_edit"', 'Value'), 'htmlspecialchars_decode'), 'decode');
			$html.=$c['manage']['lang_pack']['plugins']['batch_edit']['title'].'<div class="rows fl" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['product_sales'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="Sales" value="'.$batch_edit_row['Sales'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="rows fr" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['favorite'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="FavoriteCount" value="'.$batch_edit_row['FavoriteCount'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="clear"></div>';
			$html.='<div class="rows fl" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['default_review_rating'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="DefaultReviewRating" value="'.$batch_edit_row['DefaultReviewRating'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="rows fr" style="clear:initial;">';
				$html.='<label>'.$c['manage']['lang_pack']['products']['products']['default_review_count'].'</label>';
				$html.='<div class="input">';
					$html.='<input type="text" class="box_input" rel="amount" name="DefaultReviewTotalRating" value="'.$batch_edit_row['DefaultReviewTotalRating'].'" size="55" maxlength="55" />';
				$html.='</div>';
			$html.='</div>';
			$html.='<div class="clear"></div>';
			$ReturnData['Content']=$html;
		}elseif($p_ClassName=='product_inbox'){
			//产品询盘
			$isChecked=(int)db::get_value('config', 'GroupId="product_inbox" and Variable="UserUsed"', 'Value');
			/*
			$html='<ul class="data_list">';
				$html.='<li>';
					$html.=	'<div class="switchery'.($isChecked?' checked':'').'">';
					$html.=	'<input type="checkbox" name="UserUsed" value="1"'.($isChecked?' checked':'').'>';
					$html.=	'<div class="switchery_toggler"></div>';
					$html.=	'<div class="switchery_inner"><div class="switchery_state_on"></div><div class="switchery_state_off"></div></div>';
					$html.=	'</div>'.$c['manage']['lang_pack']['set']['config']['user_used'];
					$html.=	'<span class="tool_tips_ico" content="'.$c['manage']['lang_pack']['set']['config']['user_used_notes'].'"></span>';
				$html.='</li>';
			$html.='</ul>';
			*/
			$html.='<div class="info_title">'.$c['manage']['lang_pack']['set']['config']['mode'].'</div>';
			$html.='<div>';
				$html.='<span class="input_radio_box input_radio_box_1 choice_btn '.($isChecked?'':'checked').'">';
					$html.='<span class="input_radio">';
						$html.='<input type="radio" name="UserUsed" value="1" '.($isChecked?'':'checked').'>';
					$html.='</span>'.$c['manage']['lang_pack']['set']['config']['non_user_used'];
				$html.='</span>';
				$html.='<span class="input_radio_box input_radio_box_1 choice_btn '.($isChecked?'checked':'').'">';
					$html.='<span class="input_radio">';
						$html.='<input type="radio" name="UserUsed" value="0" '.($isChecked?'checked':'').'>';
					$html.='</span>'.$c['manage']['lang_pack']['set']['config']['user_used'];
				$html.='</span>';
			$html.='</div>';
			$ReturnData['Content']=$html;
		}else{
			//高级设置
			$products_show_ary=array();
			$products_show_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
			foreach($products_show_row as $v){ $products_show_ary[$v['Variable']]=$v['Value']; }
			$switch_ary=array(
				'UserView'=>array('Name'=>'user_view', 'Note'=>'user_view_notes'),
				'UserLogin'=>array('Name'=>'user_login', 'Note'=>'user_login_notes'),
				// 'IsDefaultReview'=>array('Name'=>'default_review', 'Note'=>''), //默认评论 先隐藏 2018.08.23
				'NumberSort'=>array('Name'=>'number_sort', 'Note'=>''),
				'code_301'=>array('Name'=>'301', 'Note'=>'http_301_notes')
			);
			if($c['FunVersion']==10){
				unset($switch_ary['UserView']);
			}
			$html='<ul class="data_list">';
			foreach($switch_ary as $k=>$v){
				if($c['FunVersion']<1 && $k=='UserView') continue;
				if($k=='NumberSort'){//编号自动排序
					$isChecked=$products_show_ary[$k];
				}elseif($k=='code_301'){
					$isChecked=(int)db::get_value('config','GroupId="http" and Variable="code_301"','Value');
				}else{
					$isChecked=$c['manage']['config'][$k];
				}
				$html.=	'<li>';
				$html.=		'<div class="switchery'.($isChecked?' checked':'').'">';
				$html.=			'<input type="checkbox" name="'.$k.'" value="1"'.($isChecked?' checked':'').'>';
				$html.=			'<div class="switchery_toggler"></div>';
				$html.=			'<div class="switchery_inner"><div class="switchery_state_on"></div><div class="switchery_state_off"></div></div>';
				$html.=		'</div>'.$c['manage']['lang_pack']['set']['config'][$v['Name']];
				if($v['Note']){
					$html.=	'<span class="tool_tips_ico" content="'.$c['manage']['lang_pack']['set']['config'][$v['Note']].'"></span>';
				}
				if($k=='NumberSort'){//编号自动排序
					$html.=	'<div class="number_box" style="display:'.($isChecked?'inline-block':'none').';"><span class="unit_input"><b>'.$c['manage']['lang_pack']['products']['set']['prefix'].'</b><input type="text" class="box_input" name="Number" value="'.$products_show_ary['myorder'].'" size="8" maxlength="15" /></span></div>';
				}
				$html.=	'</li>';
			}
			$ReturnData['Content']=$html;
		}
		$ReturnData['Title']=$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName];
		ly200::e_json($ReturnData, 1);
	}
	
	public static function app_switch_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$data=array();
		$p_ClassName=trim($p_ClassName);
		if($p_ClassName=='block_access'){
			//屏蔽访问
			$p_IsIP=(int)$p_IsIP;
			$p_IsChineseBrowser=(int)$p_IsChineseBrowser;
			$p_IsCopy=(int)$p_IsCopy;
			$p_IsCloseWeb=(int)$p_IsCloseWeb;
			//自定义关闭网站
			$CloseWebAry=array();
			foreach($c['manage']['config']['Language'] as $k=>$v){
				$CloseWebAry['CloseWeb_'.$v]=${'p_CloseWeb_'.$v};
			}
			$CloseWebData=addslashes(str::json_data(str::str_code($CloseWebAry, 'stripslashes')));
			$data['global']=array(
				'IsIP'				=>	$c['FunVersion']>=1?(int)$p_IsIP:0,
				'IsChineseBrowser'	=>	$c['FunVersion']>=1?(int)$p_IsChineseBrowser:0,
				'IsCopy'			=>	(int)$p_IsCopy,
				'IsCloseWeb'		=>	(int)$p_IsCloseWeb,
				'CloseWeb'			=>	$CloseWebData,
			);
		}elseif($p_ClassName=='batch_edit'){
			$batch_ary=array(
				'Sales'						=>	(int)$p_Sales,
				'FavoriteCount'				=>	(int)$p_FavoriteCount,
				'DefaultReviewRating'		=>	(int)$p_DefaultReviewRating,
				'DefaultReviewTotalRating'	=>	(int)$p_DefaultReviewTotalRating,
				);
			$batch_ary=addslashes(str::json_data($batch_ary));
			$data['products']=array(
				'batch_edit'			=>	$batch_ary,
			);
		}else{
			//高级设置
			$p_UserView=(int)$p_UserView;
			$p_UserLogin=(int)$p_UserLogin;
			$p_IsDefaultReview=(int)$p_IsDefaultReview;
			$p_NumberSort=(int)$p_NumberSort;
			//产品编号前缀
			$p_Number=addslashes($p_Number);
			$data['global']=array(
				'UserView'			=>	$c['FunVersion']>=1?(int)$p_UserView:0,
				'UserLogin'			=>	$p_UserLogin,
				'IsDefaultReview'	=>	(int)$p_IsCopy,
			);
			$data['products_show']=array(
				'NumberSort'		=>	$p_NumberSort,
				'myorder'			=>	$p_Number
			);
		}
		foreach($data as $k=>$v){
			manage::config_operaction($v, $k);
		}
		manage::operation_log('修改应用：'.$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName]);
		ly200::e_json('', 1);
	}
	
	public static function app_switch_change(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_Type=trim($p_Type);
		$p_Status=(int)$p_Status; //1:正在关闭 0:正在开启
		$GroupId='global';
		$data=array();
		$data[$p_Type]=($p_Status==1?0:1);
		if($p_Type=='NumberSort'){//编号自动排序
			$GroupId='products_show';
		}elseif($p_Type=='code_301'){//301跳转
			$GroupId='http';
		}elseif($p_Type=='UserUsed'){
			$GroupId='product_inbox';
		}
		manage::config_operaction($data, $GroupId);
		ly200::e_json('', 1);
	}
	
	public static function app_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//处理plugins表
		$p_ClassName=trim($p_ClassName);
		$p_IsUsed=(int)$p_IsUsed;
		$app_row=db::get_all('plugins', "Category='app' and ClassName='$p_ClassName'");
		$data=array('IsInstall'=>$p_IsUsed);
		if(!in_array($p_ClassName, (array)$c['manage']['plugins']['charge'])){//不是单独收费的APP
			$data['IsUsed']=$p_IsUsed;
		}
		db::update('plugins', "Category='app' and ClassName='$p_ClassName'", $data);
		//处理config表
		if($p_ClassName=='overseas'){//海外仓
			manage::config_operaction(array('Overseas'=>$p_IsUsed), 'global');
		}elseif($p_ClassName=='freight'){//运费估算
			$products_show=db::get_value('config', 'GroupId="products_show" and Variable="Config"', 'Value');
			$data=str::json_data(htmlspecialchars_decode($products_show), 'decode');
			$data['freight']=$p_IsUsed;
			$json_data=str::json_data($data);
			manage::config_operaction(array('Config'=>$json_data), 'products_show');
		}elseif($p_ClassName=='pdf'){//PDF格式
			$products_show=db::get_value('config', 'GroupId="products_show" and Variable="Config"', 'Value');
			$data=str::json_data(htmlspecialchars_decode($products_show), 'decode');
			$data['pdf']=$p_IsUsed;
			$json_data=str::json_data($data);
			manage::config_operaction(array('Config'=>$json_data), 'products_show');
		}elseif($p_ClassName=='swap_chain'){
			//连接OA关闭状态
			$swap_chain_data=array(
				'Action'	=>	'ueeshop_links_set_status',
				'Status'	=>	0
			);
			ly200::api($swap_chain_data, $c['ApiKey'], $c['api_url']);
			manage::config_operaction(array('public'=>''), 'swap_chain');
		}
		//返回数据
		manage::operation_log(($p_IsUsed?'开启':'关闭').'应用：'.$c['manage']['lang_pack']['plugins']['global']['items'][$p_ClassName]);
		ly200::e_json(($p_IsUsed?'开启':'关闭').'成功', 1);
	}
	//全局end
	
	//买家秀start
	public static function gallery_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AppGId		= (int)$p_AppGId;
		$MyOrder	= $p_MyOrder;
		$PicPath	= $p_PicPath;
		$ProId		= (int)$p_ProId;
		if($ProId){
			$ProId='|'.$ProId.'|';
		}
		$data=array(
			'PicPath'	=>	$PicPath,
			'ProId'		=>	$ProId,
			'AccTime'	=>	$c['time'],
			'MyOrder'	=>	(int)$MyOrder
		);
		if(!$p_ProIdValue) $data['ProId']='';
		if($AppGId){
			db::update('app_gallery', "AppGId='$AppGId'", $data);
			manage::operation_log('修改卖家秀');
		}else{
			db::insert('app_gallery', $data);
			$AppGId=db::get_insert_id();
			manage::operation_log('添加卖家秀');
		}
		ly200::e_json('', 1);
	}
	
	public static function gallery_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$AppGId=(int)$g_AppGId;
		db::delete('app_gallery', "AppGId='$AppGId'");
		manage::operation_log('删除卖家秀');
		ly200::e_json('', 1);
	}
	
	public static function gallery_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_group_pid && ly200::e_json('');
		$del_where="AppGId in(".str_replace('-',',',$g_group_pid).")";
		db::delete('app_gallery', $del_where);
		manage::operation_log('批量删除卖家秀');
		ly200::e_json('', 1);
	}
	//买家秀end
	
	//分销start
    public static function distribution_config(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_Level_1=(float)$p_Level_1;
        $p_Level_2=(float)$p_Level_2;
		$p_Level_3=(float)$p_Level_3;
        db::update('app_distribution_config', 'CId=1', array(
            'Level_1'	=>	$p_Level_1,
            'Level_2'	=>	$p_Level_2,
			'Level_3'	=>	$p_Level_3
        ));
		manage::database_language_operation('app_distribution_config', 'CId=1', array('Description'=>3, 'W_Description'=>3));
        manage::operation_log('修改分销基本设置');
        ly200::e_json('', 1);
    }
    
    public static function distribution_withdraw(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_WId=(int)$p_WId;
        $p_Status=(int)$p_Status;
        db::update('app_distribution_withdraw', "WId='$p_WId'", array('Status'=>$p_Status));
        manage::operation_log('修改分销提现状态');
        ly200::e_json('', 1);
    }
	
	/*
	 * 订单统计(get_orders_data)
	 */
	public static function get_orders_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$p_Compare=(int)$p_Compare;
		$p_Terminal=(int)$p_Terminal;
		$p_TimeS=$p_TimeS!=''?$p_TimeS:0;
		$IsCharts=@!in_array($p_TimeS, array(0,-1))?1:0;
		$result=ly200::api($_POST, $c['ApiKey'], $c['api_url']);
		
		$time_s=where::time($p_TimeS, '', !in_array($p_TimeS, array(0,-1)));
		$orders_data=array(
			'total'		=>	array(),
			'detail'	=>	array(
								'country'	=>	array(),
								'payment'	=>	array()
							),
			'compare'	=>	array(
								'total'		=>	array(),
								'detail'	=>	array(
													'country'	=>	array(),
													'payment'	=>	array()
												),
							),
			'time_s'	=>	date('Y-m-d', $time_s[0]).date('/Y-m-d', $time_s[1]),
			'time_e'	=>	''
		);
		$w="OrderStatus in(4,5,6) and (OrderTime between {$time_s[0]} and {$time_s[1]})";
		$p_Terminal>=1 && $w.=' and Source='.($p_Terminal-1);
		
		$total_field='sum(((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*(1-CouponDiscount))+ShippingPrice+ShippingInsurancePrice-CouponPrice-DiscountPrice)*(1+PayAdditionalFee/100)+PayAdditionalAffix) as total';
		$order_count='count(OrderId) as order_count';
		$order_row=db::get_one('orders', $w, "$total_field,$order_count");
		$distor_order_row=db::get_one('orders', 'DISTInfo!="" and '.$w, "$total_field,$order_count");
		$orders_data['total']=array(
			'total_price'			=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $order_row['total']),
			'order_count'			=>	(int)$order_row['order_count'],
			'distor_order_price'	=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $distor_order_row['total']),
			'distor_order_count'	=>	(int)$distor_order_row['order_count'],
		);
		//分销商
		$distributor=db::get_row_count('user', "DISTDept>1 and (RegTime between {$time_s[0]} and {$time_s[1]})", 'UserId');
		$orders_data['total']['distributor']=(int)$distributor;
		//佣金
		$commission=db::get_sum('app_distribution_commission', "AccTime between {$time_s[0]} and {$time_s[1]}", 'Commission');
		$orders_data['total']['commission']=$c['manage']['currency_symbol'].sprintf('%01.2f', $commission);
		//国家分布
		$country_row=ly200::get_table_data_to_ary('orders', $w.' group by ShippingCId', 'ShippingCId', '', "ShippingCId,ShippingCountry,$total_field,count(OrderId) as order_count");
		foreach($country_row as $k=>$v){
			$orders_data['detail']['country'][$k]=array(
				'title'			=>	$v['ShippingCountry'],
				'price'			=>	sprintf('%01.2f', $v['total']),
				'count'			=>	(int)$v['order_count'],
				'average_price'	=>	$c['manage']['currency_symbol'].((int)$v['order_count']?sprintf('%01.2f', $v['total']/$v['order_count']):0)
			);
			if($p_Compare){
				$orders_data['compare']['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	0,
					'count'			=>	0,
					'average_price'	=>	0
				);
			}
		}
		
		//对比选项
		if($p_Compare){
			$time_e=where::time($p_TimeE);
			$orders_data['time_e']=date('Y-m-d', $time_e[0]).date('/Y-m-d', $time_e[1]);
			$where="OrderStatus in(4,5,6) and OrderTime between {$time_e[0]} and {$time_e[1]}";
			$p_Terminal>=1 && $where.=' and Source='.($p_Terminal-1);

			$compare_order_row=db::get_one('orders', $where, "$total_field,$order_count");
			$compare_distor_order_row=db::get_one('orders', 'DISTInfo!="" and '.$where, "$total_field,$order_count");

			$orders_data['compare']['total']=array(
				'total_price'			=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $compare_order_row['total']),
				'order_count'			=>	(int)$compare_order_row['order_count'],
				'distor_order_price'	=>	$c['manage']['currency_symbol'].sprintf('%01.2f', $compare_distor_order_row['total']),
				'distor_order_count'	=>	(int)$compare_distor_order_row['order_count'],
			);
			//分销商
			$distributor=db::get_row_count('user', "DISTDept>1 and (RegTime between {$time_e[0]} and {$time_e[1]})", 'UserId');
			$orders_data['compare']['total']['distributor']=(int)$distributor;
			//佣金
			$commission=db::get_sum('app_distribution_commission', "AccTime between {$time_e[0]} and {$time_e[1]}", 'Commission');
			$orders_data['compare']['total']['commission']=$c['manage']['currency_symbol'].sprintf('%01.2f', $commission);
			//国家分布
			$compare_country_row=ly200::get_table_data_to_ary('orders', $where.' group by ShippingCId', 'ShippingCId', '', "ShippingCId,ShippingCountry,$total_field,count(OrderId) as order_count");
			foreach($compare_country_row as $k=>$v){
				$orders_data['compare']['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	sprintf('%01.2f', $v['total']),
					'count'			=>	(int)$v['order_count'],
					'average_price'	=>	$c['manage']['currency_symbol'].((int)$v['order_count']?sprintf('%01.2f', $v['total']/$v['order_count']):0)
				);
				if(array_key_exists($k, $orders_data['detail']['country'])){continue;}
				$orders_data['detail']['country'][$k]=array(
					'title'			=>	$v['ShippingCountry'],
					'price'			=>	0,
					'count'			=>	0,
					'average_price'	=>	0
				);
			}
		}
		
		//订单概况
		//if($IsCharts){
			$date_field="FROM_UNIXTIME(OrderTime, '%Y%m%d') as order_time";
			$order_row=ly200::get_table_data_to_ary('orders', 'DISTInfo!="" and '.$w.' group by order_time', 'order_time', 'total', "$total_field,$date_field");//分销订单
			$order_count_row=ly200::get_table_data_to_ary('orders', $w.' group by order_time', 'order_time', 'order_count', "$order_count,$date_field");
			$distor_order_count_row=ly200::get_table_data_to_ary('orders', 'DISTInfo!="" and '.$w.' group by order_time', 'order_time', 'order_count', "$order_count,$date_field");
			
			$charts_data=array();
			$charts_data['chart']['height']=350;
			$charts_data['tooltip']['valuePrefix']=$c['manage']['lang_pack']['mta']['order']['order_price'];
			$charts_data['plotOptions']['spline']['dataLabels']['enabled']=false;
			$time_s_obj=array(new DateTime(date('Ymd', $time_s[0])), new DateTime(date('Ymd', $time_s[1])));
			
			$c_charts_data=array();
			$c_charts_data['title']['text']='';
			$c_charts_data['colors']=array('#7560ff', '#5bff5b', '#90ed7d', '#f7a35c', '#91e8e1',  '#f15c80', '#e4d354', '#8d4653');
			$c_charts_data['yAxis']=array(array(
											'labels'	=>array('format'=>'{value}%'),
											'title'		=>array('text'=>$c['manage']['lang_pack']['plugins']['distribution']['distor_percent'])//分销率
										),array(
											'labels'	=>array('format'=>'{value}'),
											'title'		=>array('text'=>$c['manage']['lang_pack']['mta']['order']['order_count_to']),//订单笔数
											'opposite'	=>'true'
										));
			for($i=0; $i<3; $i++){
				if($i==2){
					$c_charts_data['series'][$i]['type']='spline';
				}else{
					$c_charts_data['series'][$i]['type']='column';
					$c_charts_data['series'][$i]['yAxis']=1;
				}
				$c_charts_data['series'][$i]['name']=$c['manage']['lang_pack']['plugins']['distribution']['count_ary'][$i];
			}

			$days=0;
			while($time_s_obj[0]<=$time_s_obj[1]){
				if($days>30) break;
				$date_key=$time_s_obj[0]->format('Ymd');
				$d_date_key=$time_s_obj[0]->format('m/d');
				$charts_data['xAxis']['categories'][]=$d_date_key;
				$charts_data['series'][0]['data'][]=(float)sprintf('%01.2f', $order_row[$date_key]);
				$c_charts_data['xAxis']['categories'][]=$d_date_key;
				$c_charts_data['series'][0]['data'][]=(int)$distor_order_count_row[$date_key];//分销订单量
				$c_charts_data['series'][1]['data'][]=(int)$order_count_row[$date_key];//总订单量
				$c_charts_data['series'][2]['data'][]=($order_count_row[$date_key]>0?($distor_order_count_row[$date_key]/$order_count_row[$date_key])*100:0);//复购率	
				$time_s_obj[0]->modify('1 days');
				$days++;
			}
			
			if($p_Compare){
				//开启对比选项
				$time_e=where::time($p_TimeE);
				$where="OrderStatus in(4,5,6) and OrderTime between {$time_e[0]} and {$time_e[1]}";
				$p_Terminal>=1 && $where.=' and Source='.($p_Terminal-1);
				$order_row=ly200::get_table_data_to_ary('orders', $where.' group by order_time', 'order_time', 'total', "$total_field,$date_field");
				$time_e_obj=array(new DateTime(date('Ymd', $time_e[0])), new DateTime(date('Ymd', $time_e[1])));
				
				$days=0;
				while($time_e_obj[0]<=$time_e_obj[1]){
					if($days>30) break;
					$date_key=$time_e_obj[0]->format('Ymd');
					$d_date_key=$time_e_obj[0]->format('m/d');
					if($charts_data['xAxis']['categories'][$days]){
						$charts_data['xAxis']['categories'][$days].=' vs '.$d_date_key;
						$charts_data['series'][1]['data'][]=(float)sprintf('%01.2f', $order_row[$date_key]);
					}
					$time_e_obj[0]->modify('1 days');
					$days++;
				}
			}
			
			$orders_data['orders_charts']=$charts_data;
			$orders_data['count_charts']=$c_charts_data;
		//}
		
		ly200::e_json($orders_data, 1);
	}
	//分销end
	
	//评论库start
    public static function review(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');	
		$p_Page=(int)$p_Page;
		$deal=0;
		if(db::get_row_count('config', 'GroupId="products" and Variable="review_count"')){
			$review_count=(int)db::get_value('config', 'GroupId="products" and Variable="review_count"', 'Value');
		}else{
			$review_count=(int)db::get_row_count('products_review', 'AppDivision>0');
			db::insert('config', array(
				'GroupId'	=>	'products',
				'Variable'	=>	'review_count',
				'Value'		=>	$review_count,
			));
		}
		$Category='wigs';//分类暂时固定为“假发”
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'library_get_data',
			'ApiName'		=>	'library',
			'Number'		=>	$c['Number'],
			'timestamp'		=>	$c['time'],
			'Category'		=>	$Category,
			'Start'			=>	$review_count,//查询的初始位置（已经导入的评论数
			'Version'		=>	'2',
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		!$result['ret'] && ly200::e_json($result['msg'], 0);
		$lib_review=$result['msg'];
		$products=db::get_limit_page('products_development', 'IsReview=0', 'ProId', 'ProId asc', $p_Page, 500);//每次处理500个产品
		!$products && $deal=1;//没有要处理的产品，直接返回已处理		
		$start=0;
		$end=count($lib_review);
		@shuffle($products[0]);
		foreach((array)$products[0] as $v){
			//插入数据
			if($end-$start<10) break;//不够评论条数,直接不处理,返回结果
			$mt=mt_rand(10,50);//每个产品10-50条评论
			$inseet_sql=array();
			for($i=0; $i<$mt; $i++){
				$name=addslashes($lib_review[$start]['Name']);
				$content=addslashes($lib_review[$start]['Content']);
				$time=mt_rand(1451577600, $c['time']);//2016-01-01到现在随机评论时间
				if($name && $content){
					$inseet_sql[]="('{$v['ProId']}','{$name}','{$content}','{$lib_review[$start]['Star']}','1','{$time}','{$lib_review[$start]['Division']}')";
				}
				$start++;
			}
			if(count($inseet_sql)){
				$inseet_sql=implode(',',$inseet_sql);
				$inseet_sql='insert into `products_review` (ProId,CustomerName,Content,Rating,Audit,AccTime,AppDivision) values '.$inseet_sql;
				db::query($inseet_sql);
				db::update('products_development', "ProId='{$v['ProId']}'", array('IsReview' => 1));
			}
		}
		if($start){
			$new_review_count=(int)($review_count+$start);
			db::update('config', 'GroupId="products" and Variable="review_count"',array(
					'Value'		=>	$new_review_count,
				));
		}
		if($p_Page<$products[3] && !$deal){//继续执行
			ly200::e_json(array('page'=>($p_Page+1), 'percent'=>sprintf("%d", $p_Page/$products[3]*100)), 2);
		}
		manage::operation_log('应用评论库');
		ly200::e_json(array('tips'=>$c['manage']['lang_pack']['msg']['import_success'], 'percent'=>100), 1);
    }
	//评论库end

	//Facebook专页start
    public static function facebook_store(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
		$WId		= (int)$p_WId;
		$Type		= $p_Type;
		$FormatAry	= array();
		if($Type=='Products'){
			//产品数据
			$FormatAry['Products']=$p_ProId;
		}else{
			//广告图
			$store_type='Name_PicPath_Url';
			$Type=@explode('_', $store_type);
			$config=str::json_data(db::get_value('web_settings', "WId='{$WId}'", 'Config'), 'decode');
			$PicCount=(int)$config['PicCount'];
			$PicCount==0 && $PicCount=1;
			foreach((array)$Type as $v){
				for($i=0; $i<$PicCount; ++$i){
					foreach((array)$c['manage']['web_lang_list'] as $v1){
						$FormatAry[$v][$i][$v1]=$_POST[$v.'_'.$v1][$i];
						$FormatAry[$v][$i]=array_filter($FormatAry[$v][$i]);
					}
				}
			}
		}
		$Data=addslashes(str::json_data(str::str_code($FormatAry, 'stripslashes')));
		db::update('web_settings', "WId='{$WId}'", array('Data'=>$Data));
		manage::operation_log('编辑Facebook专页');
        ly200::e_json('', 1);
	}
	//Facebook专页end
	
	//Facebook店铺start
	public static function facebook_data_init(){
        global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ReturnAry=array();
		$Lang=$c['manage']['web_lang'];
		if($p_ProId){
			$pro_where="ProId in(".str_replace('-', ',', $p_ProId).")";
			$products_row=db::get_all('products', $pro_where);
			foreach($products_row as $k=>$v){
				if($v['FacebookId']){
					$ReturnAry['Update'][$v['FacebookId']]=array(
						'category'		=>	'Apparel & Accessories > Clothing > Dresses',
						'currency'		=>	$_SESSION['Manage']['Currency']['Currency'],
						'description'	=>	($v['BriefDescription']?$v['BriefDescription']:$v['Name'.$Lang]),
						'image_url'		=>	ly200::get_domain().$v['PicPath_0'],
						'price'			=>	(float)$v['Price_1']*100,
						'retailer_id'	=>	$v['ProId'],
						'name'			=>	$v['Name'.$Lang],
						'url'			=>	ly200::get_domain().ly200::get_url($v, 'products', $Lang),
						'brand'			=>	'Mi'
					);
				}else{
					$ReturnAry['Create'][$v['ProId']]=array(
						'category'		=>	'Apparel & Accessories > Clothing > Dresses',
						'currency'		=>	$_SESSION['Manage']['Currency']['Currency'],
						'description'	=>	($v['BriefDescription']?$v['BriefDescription']:$v['Name'.$Lang]),
						'image_url'		=>	ly200::get_domain().$v['PicPath_0'],
						'price'			=>	(float)$v['Price_1']*100,
						'retailer_id'	=>	$v['ProId'],
						'name'			=>	$v['Name'.$Lang],
						'url'			=>	ly200::get_domain().ly200::get_url($v, 'products', $Lang),
						'brand'			=>	'Mi'
					);
				}
			}
		}
		$ReturnAry['Data']=array();
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'get_facebook_ads_extension',
			'ApiName'		=>	'facebook',
			'Number'		=>	$c['Number'],
			'timestamp'		=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		if($result['ret']==1){
			$ReturnAry['Data']=$result['msg'];
		}
        ly200::e_json($ReturnAry, 1);
	}
	
	public static function facebook_products_process(){
        global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId	= (int)$p_ProId;
		$ret		= 0;
		$Lang		= $c['manage']['web_lang'];
		$products_row=db::get_one('products', "ProId='{$p_ProId}'");
		if($products_row){
			$ary=array(
				'category'		=>	'Apparel & Accessories > Clothing > Dresses',
				'currency'		=>	$_SESSION['Manage']['Currency']['Currency'],
				'description'	=>	($products_row['BriefDescription']?$products_row['BriefDescription']:$products_row['Name'.$Lang]),
				'image_url'		=>	ly200::get_domain().$products_row['PicPath_0'],
				'price'			=>	(float)$products_row['Price_1']*100,
				'retailer_id'	=>	$products_row['ProId'],
				'name'			=>	$products_row['Name'.$Lang],
				'url'			=>	ly200::get_domain().ly200::get_url($products_row, 'products', $Lang),
				'brand'			=>	'Mi'
			);
			$data=array(
				'ApiKey'		=>	'ueeshop_sync',
				'Action'		=>	'products_upload',
				'ApiName'		=>	'facebook',
				'Number'		=>	$c['Number'],
				'timestamp'		=>	$c['time'],
				'Type'			=>	$p_Type, //Delete or Upload
				'FacebookId'	=>	$products_row['FacebookId'],
				'Data'			=>	$ary
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
			$tips='';
			if($result['ret']==1){
				$result_txt=str::json_data($result['msg']['Result'], 'decode');
				if($result['msg']['Type']==1){
					//创建
					if($result_txt && $result_txt['id']){
						db::update('products', "ProId='{$p_ProId}'", array('FacebookId'=>$result_txt['id']));
						$tips='创建成功';
						$ret=1;
					}else{
						$tips=$result_txt['error']['message'];
						!$tips && $tips='创建失败';
					}
				}elseif($result['msg']['Type']==2){
					//更新
					if($result_txt && $result_txt['success']){
						$tips='更新成功';
						$ret=1;
					}else{
						$tips=$result_txt['error']['message'];
						!$tips && $tips='更新失败';
					}
				}else{
					//删除
					if($result_txt && $result_txt['success']){
						$tips='删除成功';
						db::update('products', "ProId='{$p_ProId}'", array('FacebookId'=>''));
						$ret=1;
					}else{
						$tips=$result_txt['error']['message'];
						!$tips && $tips='删除失败';
					}
				}
			}else{
				$tips='操作失败';
			}
		}
       ly200::e_json(array('Type'=>$result['msg']['Type'], 'Tips'=>$tips), $ret);
	}
	
	public static function facebook_products_update(){
        global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		if($p_Type=='Delete' || ($p_Type=='Add' && $p_FacebookId)){
			db::update('products', "ProId='$p_ProId'", array('FacebookId'=>$p_FacebookId));
		}
        ly200::e_json($p_ProId, 1);
	}
	//Facebook店铺end

	//Google Feed start
    public static function googlefeed_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		$data=array(
			'ProId'			=>	$p_ProId,
			'GTIN'			=>	$p_GTIN,
			'MPN'			=>	$p_MPN,
			'Brand'			=>	$p_Brand,
			'Label'			=>	$p_Label,
			'ShippingPrice'	=>	$p_ShippingPrice,
			'TaxRate'		=>	$p_TaxRate,
		);
		$add=0;
		if(db::get_row_count('app_googlefeed', "ProId='{$p_ProId}'")){
			db::update('app_googlefeed', "ProId='{$p_ProId}'", $data);
		}else{
			$add=1;
			db::insert('app_googlefeed', $data);
		}
		manage::operation_log((!$add?'编辑':'添加').'Google Feed ('.$p_ProId.')');
        ly200::e_json('', 1);
	}

	public static function googlefeed_update(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
		set_time_limit(0);
		$all_category_row = db::get_all('products_category','1',"Category{$c['manage']['web_lang']},CateId,UId,Dept");
		$new_all_category_row = array();
		foreach((array)$all_category_row as $v){
			$new_all_category_row[$v['CateId']] = $v;
		}
		$domain=((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')?'https://':'http://').$_SERVER['HTTP_HOST'];
		$Currency = db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Currency');
		$products_where = '1'.$c['manage']['where']['products'];		
		$page_count=1000;
		$all = db::get_row_count('products', $products_where);
		$all_page = ceil($all/$page_count);
		if(!db::get_row_count('config','Variable="googlefeedpage"')){
			db::insert('config',array(
				'GroupId'	=>	'plugins',
				'Variable'	=>	'googlefeedpage',
				'Value'		=>	'0'
				));
		}
		
		$page = (int)db::get_value('config','Variable="googlefeedpage"','Value');
		$num = $page+1;
		$xml='';
		$page==0 && $xml.="<?xml version=\"1.0\"?>
		<rss xmlns:g=\"http://base.google.com/ns/1.0\" version=\"2.0\">
			<channel>
				<title>Example - Online Store</title>
				<link>{$domain}</link>
				<description>This is a sample feed containing the required and recommended attributes for a variety of different products</description>
				";
				$products_row = db::get_limit('products', $products_where, "Name{$c['manage']['web_lang']},BriefDescription{$c['manage']['web_lang']},ProId,CateId,Prefix,Number,PicPath_0,PicPath_1,PicPath_2,PicPath_3,PicPath_4,PicPath_5,PicPath_6,PicPath_7,PicPath_8,PicPath_9,Price_1,Stock,Weight,PageUrl, IsPromotion, StartTime, EndTime, PromotionType, PromotionDiscount, PromotionPrice, MOQ, Price_0",$c['my_order'].'ProId desc',$page*$page_count,$page_count);
				// }
				$products_id_row = array();
				foreach ((array)$products_row as $v) {
					$products_id_row[] = $v['ProId'];
				}
				if ($products_id_row) {
					$products_id_row = @implode(',', $products_id_row);
				}
				$products_description_row = $googlefeed_row = array();
				$products_id_row && $all_products_description_row = db::get_all('products_description',"ProId in ({$products_id_row})",'ProId,Description'.$c['manage']['web_lang']);
				$products_id_row && $seckill_row = db::get_all('sales_seckill', "ProId in({$products_id_row}) and RemainderQty>0 and {$c['time']} between StartTime and EndTime");
				$seckill_one_row = array();
				foreach ((array)$seckill_row as $v) {
					$seckill_one_row[$v['ProId']] = $v;
				}
				foreach ((array)$all_products_description_row as $v) {
					$products_description_row[$v['ProId']]=$v['Description'.$c['manage']['web_lang']];
				}
				$products_id_row && $all_googlefeed = db::get_all('app_googlefeed',"ProId in ({$products_id_row})",'*');
				foreach ((array)$all_googlefeed as $v) {
					$googlefeed[$v['ProId']]=$v;
				}
				foreach ((array)$products_row as $v) {	
					$id = $v['Prefix'].$v['Number'];
					$ProId = $v['ProId'];
					$title = str::str_echo($v['Name'.$c['manage']['web_lang']],145,0,'...');
					$description = str::str_echo(str_replace(array("\r\n","\n","\r"), ' ',strip_tags($products_description_row[$ProId])),4500,0,'...');
					$link = $domain.ly200::get_url($v, 'products', $c['manage']['web_lang']);
					$image_link = $domain.ly200::get_size_img($v['PicPath_0'], '500x500');
					$availability = $v['Stock']?'in stock':'out of stock';
					$price_ary = cart::range_price_ext($v, 2);
					$is_promition = ($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
					$sale_price_num = 0;
					if ((int)$is_promition || $seckill_one_row[$ProId]) { //有促销和秒杀价格就用这个价格
						$price = cart::iconv_price($price_ary[1], '2', $Currency).' '.$Currency;
						$StartTime = @date(DATE_ISO8601, $v['StartTime']);
						$EndTime = @date(DATE_ISO8601, $v['EndTime']);
						$sale_price_num = $price_ary[0];
						$sale_price = cart::iconv_price($sale_price_num, '2', $Currency).' '.$Currency;
						if ($seckill_one_row[$ProId]) {
							$price = cart::iconv_price($v['Price_1'], '2', $Currency).' '.$Currency;
							$StartTime = @date(DATE_ISO8601, $seckill_one_row[$ProId]['StartTime']);
							$EndTime = @date(DATE_ISO8601, $seckill_one_row[$ProId]['EndTime']);
							$sale_price_num = $seckill_one_row[$ProId]['Price'];
							$sale_price = cart::iconv_price($sale_price_num, '2', $Currency).' '.$Currency;
						}
					} else {
						$price = cart::iconv_price($price_ary[0], '2', $Currency).' '.$Currency;
					}
					$shipping_price = cart::iconv_price((float)$googlefeed[$ProId]['ShippingPrice'], '2', $Currency);
					$tax_rate = (float)$googlefeed[$ProId]['TaxRate'];
					$category_row = $new_all_category_row[$v['CateId']];
					$google_product_category=$brand = '';
					$UId = trim($category_row['UId'], ',');
					$name = $category_row['Category'.$c['manage']['web_lang']];
					if($UId == '0'){
						$TopCateId = $v['CateId'];
						$google_product_category.=$name;  
						$brand = $name;
					}
					if($UId){
						$uid_ary = @explode(',', $UId);	
						foreach((array)$uid_ary as $k1 => $v1){
							if($k1 == 0) continue; 
							$k1 == 1 && $TopCateId=$v1;
							$k1 == 1 && $brand = $new_all_category_row[$v1]['Category'.$c['manage']['web_lang']];	
							$ext_name=$new_all_category_row[$v1]['Category'.$c['manage']['web_lang']];
							$google_product_category.=$ext_name.' > ';
						}
						$google_product_category.=$name;
					}
					$type = $brand;
					$googlefeed[$ProId]['Brand'] && $brand = $googlefeed[$ProId]['Brand'];
					$label_ary=@array_filter(@explode('/', $googlefeed[$ProId]['Label']));
				$xml.="<item>
					<g:id><![CDATA[{$id}]]></g:id>
					<g:title><![CDATA[{$title}]]></g:title>
					<g:description><![CDATA[{$description}]]></g:description>
					<g:link><![CDATA[$link]]></g:link>
					<g:image_link><![CDATA[{$image_link}]]></g:image_link>";
					for($i=1;$i<10;++$i){	
						if(is_file($c['root_path'].ly200::get_size_img($v['PicPath_'.$i], '500x500'))){
							$additional_image_link = $domain.ly200::get_size_img($v['PicPath_'.$i], '500x500');
							$xml.="<g:additional_image_link><![CDATA[{$additional_image_link}]]></g:additional_image_link>";
						}
					}
				$xml.="
					<g:condition>new</g:condition>
					<g:availability>{$availability}</g:availability>
					<g:price>{$price}</g:price>";
					if($sale_price_num>0){
						$xml.="<g:sale_price>{$sale_price}</g:sale_price>";
						$xml.="<g:sale_price_effective_date>{$StartTime}/{$EndTime}</g:sale_price_effective_date>";
					}
					if($googlefeed[$ProId]['GTIN']) $xml.="<g:gtin>{$googlefeed[$ProId]['GTIN']}</g:gtin>";
					if($googlefeed[$ProId]['MPN']) $xml.="<g:mpn>{$googlefeed[$ProId]['MPN']}</g:mpn>";
					if(!$googlefeed[$ProId]['GTIN'] && !$googlefeed[$ProId]['MPN']){
						$xml.='<g:identifier_exists>no</g:identifier_exists>';
					}
					foreach((array)$label_ary as $i => $j){
						$j=trim($j);
						$xml.="<g:custom_label_{$i}>{$j}</g:custom_label_{$i}>";
					}
				$xml.="
					<g:shipping>
					  <g:price>{$shipping_price} {$Currency}</g:price>
					</g:shipping>
					<g:shipping_weight>{$v['Weight']} kg</g:shipping_weight>
					<g:tax>
					  <g:rate>{$tax_rate}</g:rate>
					</g:tax>
					<g:brand><![CDATA[{$brand}]]></g:brand>
					<g:product_type><![CDATA[{$type}]]></g:product_type>
				</item>
				";
				//<g:google_product_category><![CDATA[{$google_product_category}]]></g:google_product_category>
				}
		if($page==0){
			@unlink($c['root_path'].'googlefeed.xml');
			manage::operation_log('开始更新 Google Feed -'.date('Y-m-d H:i:s',time()));
		}
		if($page>=$all_page){
			$page=0;
		}else{
			$page++;
		}
		db::update('config','Variable="googlefeedpage"',array('Value'=>(int)$page));

		$myfile = fopen($c['root_path'].'googlefeed.xml', 'a') or die("Unable to open file!");		
		fwrite($myfile, $xml);
		fclose($myfile);
		if($page>=$all_page){
			$xml="
			</channel>
		</rss>";
			$myfile = fopen($c['root_path'].'googlefeed.xml', 'a') or die("Unable to open file!");			
			fwrite($myfile, $xml);
			fclose($myfile);
			$page=0;
			db::update('config','Variable="googlefeedpage"',array('Value'=>(int)$page));
			manage::operation_log('更新完成 Google Feed -'.date('Y-m-d H:i:s',time()));
			ly200::e_json($c['manage']['lang_pack']['plugins']['googlefeed']['update_suc'], 1);
		}else{
			ly200::e_json(str_replace('%num%', $num, $c['manage']['lang_pack']['plugins']['googlefeed']['updating']), '-1');
		}
	}
	//Google Feed end
	
	//交换链接 Start
    public static function swap_chain_add(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_UrlsId=(int)$p_UrlsId;
		$data=array(
			'Action'	=>	'ueeshop_links_urls_edit',
			'Keyword'	=>	$p_Keyword,
			'Url'		=>	$p_Url
		);
		$p_UrlsId && $data['UrlsId']=$p_UrlsId;
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		!$result['ret'] && ly200::e_json($result['msg'],0);
		manage::operation_log(($p_UrlsId?'编辑':'添加').'交换链接');
        ly200::e_json('', 1);
	}
	public static function swap_chain_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'p');
		$p_UrlsId=(int)$p_UrlsId;
		$data=array(
			'Action'	=>	'ueeshop_links_urls_del',
			'UrlsId'	=>	$p_UrlsId,
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		!$result['ret'] && ly200::e_json($result['msg'],0);
		manage::operation_log('删除交换链接');
        ly200::e_json('', 1);
	}
	public static function swap_chain_refresh(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'p');
		$p_UrlsId=(int)$p_UrlsId;
		$p_AllocationId=(int)$p_AllocationId;
		$data=array(
			'Action'		=>	'ueeshop_links_urls_refresh',
			'UrlsId'		=>	$p_UrlsId,
			'AllocationId'	=>	$p_AllocationId
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		!$result['ret'] && ly200::e_json($result['msg'],0);
		manage::operation_log('刷新交换链接');
        ly200::e_json('', 1);
	}
	//交换链接 End
	
	//google验证文件 Start
	public static function google_verification_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$google_verification=db::get_value('config','GroupId="plugins" and Variable="google_verification"','Value');
		$google_verification=str::json_data(htmlspecialchars_decode($google_verification),'decode');
		$verification_contents=file_get_contents($c['root_path'].$p_FilePath);
		$verification=explode(': ',$verification_contents);
		!$verification[1] && ly200::e_json($c['manage']['lang_pack']['plugins']['google_verification']['error'], 0);
		if(in_array($verification[1],$google_verification)) ly200::e_json($c['manage']['lang_pack']['plugins']['google_verification']['repeat'], 0);
		$google_verification[]=$verification[1];
		manage::operation_log('添加google验证文件');
		$google_verification=str::json_data($google_verification);
		manage::config_operaction(array('google_verification'=>$google_verification), 'plugins');
        ly200::e_json('', 1);
	}
	public static function google_verification_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'p');
		$google_verification=db::get_value('config','GroupId="plugins" and Variable="google_verification"','Value');
		$google_verification=str::json_data(htmlspecialchars_decode($google_verification),'decode');
		unset($google_verification[(int)$p_Id]);
		$google_verification=str::json_data($google_verification);
		manage::config_operaction(array('google_verification'=>$google_verification), 'plugins');
		manage::operation_log('删除google验证文件');
		ly200::e_json('', 1);
	}
	//google验证文件 End

	public static function massive_email_url(){
		global $c;
		$url = mailchimp::mailchimp_get_oauth_url();
		ly200::e_json(array('url'=>$url), 1);
	}
	
	//Paypal纠纷 Start
    public static function dispute_check_list(){
		//检查纠纷信息
		global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $Start = (int)$p_Start;
        ($Start < 1) && $Start = 1;
        if (!$_SESSION['Manage']['Dispute'] && !isset($_SESSION['Manage']['Dispute'])) {
            $_SESSION['Manage']['Dispute'] = $c['time'];
        }
		if ($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_global') == 'enable') {
            //获取token
			$access_token = $c['plugin_app']->trigger('paypal_dispute', 'dispute_global');
		}
		$Data['access_token'] = $access_token;
		if ($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_show_list') == 'enable') {
            $Data['page'] = $Start;
			$result = $c['plugin_app']->trigger('paypal_dispute', 'dispute_show_list', $Data);
		}
        $IsShowDetails = ($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_show_details') == 'enable' ? 1 : 0);
		if ($result) {
			$result = str::json_data($result, 'decode');
            if (count($result['items']) == 0) {
                ly200::e_json('', 0);
            }
			foreach ($result['items'] as $k => $v) {
                $JSON = array();
                $dispute_row = db::get_one('orders_paypal_dispute', "DisputeID='{$v['dispute_id']}'");
                if ($dispute_row) {
                    //已有纠纷数据
                    if ($v['status'] == $dispute_row['Status']) continue; //状态一致，不处理
                    $DId = $dispute_row['DId'];
                    $OfferData = str::json_data(htmlspecialchars_decode($dispute_row['Offer']), 'decode');
                    $Status = $v['status']; //纠纷状态
                    $Lifecycle = $v['dispute_life_cycle_stage']; //纠纷生命周期的阶段
                    $UpdateTime = strtotime($v['update_time']); //纠纷最后更新的日期和时间
                    $Offer = $v['offer']; //纠纷互相要求条件
                    $Links = $v['links']; //事件地址
                    $IsRefund = 0;
                    $RefundStatus = 0;
                    if ($UpdateTime != $dispute_row['UpdateTime']) {
                        $data = array('UpdateTime'=>$UpdateTime); //争议最后更新的日期和时间
                        if ($Status != $dispute_row['Status']) {
                            //纠纷状态有改动
                            $data['Status'] = $Status;
                            if ($dispute_row['Status'] == 'OPEN' && $Status == 'RESOLVED') {
                                //解决纠纷
                                $data['ResolvedTime'] = $UpdateTime;
                            } else {
                                //升级到仲裁
                                $data['ClaimTime'] = $UpdateTime;
                            }
                        }
                        if ($Lifecycle != $dispute_row['Lifecycle']) {
                            //纠纷阶段有改动
                            $data['Lifecycle'] = $Lifecycle;
                        }
                        if ($Offer && $Offer['seller_offered_amount'] && $OfferData && !$OfferData['seller_offered_amount']) {
                            //顾客有返回建议结果
                            $IsRefund = 1;
                            $RefundStatus = 2;
                            $data['Offer'] = addslashes(str::json_data($Offer));
                            foreach ((array)$Links as $k2 => $v2) {
                                $v2['rel'] == 'make_offer' && $RefundStatus = 0; //有提议事件，说明顾客反对了提议
                            }
                        }
                        db::update('orders_paypal_dispute', "DId='{$DId}'", $data);
                        if ($IsRefund == 1) {
                            //退款程序处理
                            $refund_row = str::str_code(db::get_one('orders_paypal_dispute_refund', "DId='{$DId}' and Status=1"));
                            if ($refund_row) {
                                db::update('orders_paypal_dispute_refund', "RId='{$refund_row['RId']}'", array('Status'=>$RefundStatus));
                            }
                        }
                        $Count = count($v['messages']);
                        if ($Count > 1) {
                            //获取最新的对话
                            $Count -= 1;
                            $PostedBy = $v['messages'][$Count]['posted_by'];
                            if ($PostedBy == 'BUYER' || $refund_row) {
                                $dataTo=array(
                                    'DId'		=>	$DId,//纠纷ID
                                    'PostedBy'	=>	($PostedBy == 'BUYER' ? 1 : 0), //发布者 BUYER:顾客 SELLER:商家
                                    'Content'	=>	addslashes($v['messages'][$Count]['content']), //与通知事件相关的资源名称
                                    'Status'	=>	$RefundStatus,
                                    'AccTime'	=>	strtotime($v['messages'][$Count]['time_posted']) //创建事件通知的日期和时间
                                );
                                $refund_row && $dataTo['RId'] = $refund_row['RId'];
                                db::insert('orders_paypal_dispute_message', $dataTo);
                            }
                        }
                    }
                } else {
                    //没有纠纷数据
                    if ($v['disputed_transactions']) {
                        //商家看到的交易ID
                        $OrderId = db::get_value('orders_payment_info', "MTCNNumber='{$v['disputed_transactions'][0]['seller_transaction_id']}'", 'OrderId');
                    }
                    $data = array(
                        'DisputeID'			=>	$v['dispute_id'], //争议ID
                        'OrderId'			=>	$OrderId,
                        'CreateTime'		=>	strtotime($v['create_time']), //争议创建的日期和时间
                        'UpdateTime'		=>	strtotime($v['update_time']), //争议最后更新的日期和时间
                        'TransactionsInfo'	=>	addslashes(str::json_data($v['disputed_transactions'])), //争议相应的交易详情
                        'Reason'			=>	$v['reason'], //争议原因
                        'Status'			=>	$v['status'], //争议状态
                        'Currency'			=>	$v['dispute_amount']['currency_code'], //争议金额的货币
                        'Amount'			=>	$v['dispute_amount']['value'], //争议金额
                        'Offer'				=>	addslashes(str::json_data($v['offer'])), //顾客要求退还的争议
                        'Lifecycle'			=>	$v['dispute_life_cycle_stage'], //争议生命周期的阶段
                        'Channel'			=>	$v['dispute_channel'] //顾客创建争议的渠道
                    );
                    db::insert('orders_paypal_dispute', $data);
                    $DId = db::get_insert_id();
                    $dataTo = array(
                        'DId'		=>	$DId, //争议ID
                        'PostedBy'	=>	($v['messages'][0]['posted_by'] == 'BUYER' ? 1 : 0), //发布者 BUYER:顾客 SELLER:商家
                        'Content'	=>	addslashes($v['messages'][0]['content']), //与通知事件相关的资源名称
                        'AccTime'	=>	strtotime($v['messages'][0]['time_posted']) //创建事件通知的日期和时间
                    );
                    db::insert('orders_paypal_dispute_message', $dataTo);
                }
            }
		}
        ly200::e_json($Start+1, 1);
	}
    
	public static function dispute_message_to_customer(){
		//商家向买家发送信息沟通
		global $c;
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_global')=='enable'){//获取token
			$access_token=$c['plugin_app']->trigger('paypal_dispute', 'dispute_global');
		}
		$_POST['access_token']=$access_token;
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_message_to_customer')=='enable'){
			$result=$c['plugin_app']->trigger('paypal_dispute', 'dispute_message_to_customer', $_POST);
		}
		if($result){
			$result=str::json_data($result, 'decode');
			if($result){
                ly200::e_json(array('tips'=>$c['manage']['lang_pack']['msg']['send_success'], 'content'=>$_POST['Content'], 'time'=>date('Y-m-d H:i:s', $c['time'])), 1);
            }
		}
		ly200::e_json($c['manage']['lang_pack']['msg']['send_error'], 0);
	}
	
	public static function dispute_to_claim(){
		global $c;
		//商家直接要求进入PayPal仲裁阶段
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_global')=='enable'){//获取token
			$access_token=$c['plugin_app']->trigger('paypal_dispute', 'dispute_global');
		}
		$_POST['access_token']=$access_token;
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_escalate_to_claim')=='enable'){
			$result=$c['plugin_app']->trigger('paypal_dispute', 'dispute_escalate_to_claim', $_POST);
		}
		if($result){
			$result=str::json_data($result, 'decode');
			$result && ly200::e_json($c['manage']['lang_pack']['msg']['send_success'], 1);
		}
		ly200::e_json($c['manage']['lang_pack']['msg']['send_error'], 0);
	}
	
	public static function dispute_provide_evidence(){
		global $c;
		//商家向paypal提供证据
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_global')=='enable'){//获取token
			$access_token=$c['plugin_app']->trigger('paypal_dispute', 'dispute_global');
		}
		$_POST['access_token']=$access_token;
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_provide_evidence')=='enable'){
			$result=$c['plugin_app']->trigger('paypal_dispute', 'dispute_provide_evidence', $_POST);
		}
		if($result){
			$result=str::json_data($result, 'decode');
			$result && ly200::e_json($c['manage']['lang_pack']['msg']['send_success'], 1);
		}
		ly200::e_json($c['manage']['lang_pack']['msg']['send_error'], 0);
	}
	
	public static function dispute_refund(){
		global $c;
		//退款
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_global')=='enable'){//获取token
			$access_token=$c['plugin_app']->trigger('paypal_dispute', 'dispute_global');
		}
		$_POST['access_token']=$access_token;
		if($c['plugin_app']->trigger('paypal_dispute', '__config', 'dispute_refund')=='enable'){
			$result=$c['plugin_app']->trigger('paypal_dispute', 'dispute_refund', $_POST);
		}
		if($result){
			$result=str::json_data($result, 'decode');
			$result && ly200::e_json($c['manage']['lang_pack']['msg']['send_success'], 1);
		}
		ly200::e_json($c['manage']['lang_pack']['msg']['send_error'], 0);
	}
	//Paypal纠纷 End
	
	//同步Shopify Start
    public static function shopify(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_Page		= (int)$p_Page;
		$Limit		= 20;
		$Lang		= $c['manage']['web_lang'];
		$ShopifyData= str::str_code(db::get_value('config', 'GroupId="shopify" and Variable="ApiData"', 'Value'));
		$Account	= str::json_data(htmlspecialchars_decode($ShopifyData), 'decode');
		$resize_ary	= $c['manage']['resize_ary']['products'];
		$save_dir	= $c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');//图片储存位置
		//整合分类和属性数据
		$category_row=str::str_code(db::get_one('products_category', 'Source="Shopify"'));
		if($category_row){
			//有分类
			$CateId=$category_row['CateId'];
		}else{
			//没分类
			$data=array(
				"Category{$Lang}"	=>	'Shopify',
				'UId'				=>	'0,',
				'Dept'				=>	1,
				'Source'			=>	'Shopify'
			);
			db::insert('products_category', $data);
			$CateId=db::get_insert_id();
			db::insert('products_category_description', array('CateId'=>$CateId));
			manage::operation_log('添加产品分类');
		}
		$attr_data_ary=$vid_data_ary=array();
		$attr_row=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%'", "AttrId, Name{$Lang}, ColorAttr", 'AttrId desc'));
		if($attr_row){
			foreach((array)$attr_row as $v){
				$attr_data_ary[$v["Name{$Lang}"]]=$v['AttrId'];
			}
			$attribute_where=(count($attr_data_ary)>0?'AttrId in('.implode(',', $attr_data_ary).')':'AttrId="-1"');
			$attr_value_row=str::str_code(db::get_all('products_attribute_value', $attribute_where, '*', $c['my_order'].'VId asc'));//属性选项
			foreach((array)$attr_value_row as $v){
				$vid_data_ary[$v['AttrId']][$v["Value{$Lang}"]]=$v['VId'];
			}
		}
		unset($category_row, $attr_row, $attr_value_row);
		//开始
		if($Account['username'] && $Account['password'] && $Account['shop']){
			//获取产品总数
			$url="https://{$Account['username']}:{$Account['password']}@{$Account['shop']}.myshopify.com/admin/products/count.json";
			$result=ly200::curl($url);
			$Json=str::json_data($result, 'decode');
			$Count=(int)$Json['count'];
			$TotalPages=ceil($Count/$Limit);
			//获取产品数据
			$url="https://{$Account['username']}:{$Account['password']}@{$Account['shop']}.myshopify.com/admin/products.json?page={$p_Page}&limit={$Limit}";
			$result=ly200::curl($url);
			$Json=str::json_data($result, 'decode');
			//循环产品数据
			foreach((array)$Json['products'] as $val){
				//初始化
				$AddNum=$AddVIdNum=$IsHaveAttr=$Stock=0;
				$create_ary=$add_ary=$min_price_ary=$attr_ary=$ext_ary=array();
				$position_ary=array();
				$prod_row=db::get_one('products', "Source='shopify' and SourceId='{$val['id']}'", 'ProId, PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9');
				if($prod_row){//更新
					$ProId=$prod_row['ProId'];
				}else{//添加
					$ProId=0;
				}
				//规格属性参数
				foreach((array)$val['options'] as $v){
					if($v['name']!='Title'){
						$IsHaveAttr=1;//标记有属性
						$id=$attr_data_ary[$v['name']];
						if(!$id){//属性不存在
							$id="ADD:$AddNum";
							$create_ary['Cart'][$id]=$v['name'];
							$add_ary[$v['name']][0]=$id;
							++$AddNum;
						}
						$arr=array();
						foreach((array)$v['values'] as $v2){
							$v2=trim($v2);
							if($v2){
								if($vid_data_ary[$id][$v2]){//选项存在
									$arr[]=$vid_data_ary[$id][$v2];
								}else{//选项不存在
									$vid="ADDVId:$AddVIdNum";
									$arr[]=$vid;
									$create_ary['Value'][$id][$vid]=$v2;
									$add_ary[$v['name']][$v2]=$vid;
									++$AddVIdNum;
								}
							}
						}
						$attr_ary[$id]=$arr;
						$position_ary[$v['position']]=$v['name'];
					}
				}
				//组合属性
				foreach((array)$val['variants'] as $v){
					if($v['title']=='Default Title'){
						//没有属性
						$SKU=$v['sku'];
						$Price_0=$v['compare_at_price'];
						$Price_1=$v['price'];
						$Weight=$v['grams'];//克为单位的重量
						$Weight && $Weight/1000;//转为千克
						$Stock=$v['inventory_quantity'];
					}else{
						//规格属性
						$value=array();
						$OvId=1;
						for($i=1; $i<=3; ++$i){
							$name=trim($v["option{$i}"]);
							if(!$name) continue;
							if($vid_data_ary[$attr_data_ary[$position_ary[$i]]][$name]){//数据存在
								$value[]=$vid_data_ary[$attr_data_ary[$position_ary[$i]]][$name];
							}else{//数据不存在
								$value[]=$add_ary[$position_ary[$i]][$name];
							}
						}
						$min_price_ary[]=$v['price'];
						$Stock+=$v['inventory_quantity'];
						sort($value);
						$ext_ary['|'.implode('|', $value).'|'][$OvId]=array($v['price'], $v['inventory_quantity'], $v['grams']/1000, $v['sku'], 0);
					}
				}
				//图片 and 属性图片(暂时不取)
				$ImgPath=array();
				foreach((array)$val['images'] as $k=>$v){
					if($k>9) continue;//目前不支持保存超过10张图片
					$new_path='';
					preg_match('/^(.*)\?v=[0-9]+$/', $v['src'], $src);
					$src=$src[1];
					// var_dump($src);
					//$filepath=trim(ly200::curl($src));
					$curl=@curl_init(); 
					@curl_setopt($curl, CURLOPT_URL, $src);
					@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					@curl_setopt($curl, CURLOPT_TIMEOUT, 300);
					@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
					@curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
					@curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					@curl_setopt($curl, CURLOPT_REFERER, 'http://'.$_SERVER['HTTP_HOST']);
					@curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
					$filepath=@curl_exec($curl);
					@curl_close($curl);
					
					if($filepath){//检查图片是否存在
						$ext_name=file::get_ext_name($src);//图片文件后缀名
						// var_dump($ext_name);
						$new_path=file::write_file($save_dir, str::rand_code().'.'.$ext_name, $filepath);
					}
					if($new_path && is_file($c['root_path'].$new_path)){
						if($c['manage']['config']['IsWater']) $water_ary[]=$new_path;
						if($resize_ary){
							if(in_array('default', $resize_ary)){//保存不加水印的原图
								@copy($c['root_path'].$new_path, $new_path.".default.{$ext_name}");
							}
							if($c['manage']['config']['IsWater'] && $c['manage']['config']['IsThumbnail']){//缩略图加水印
								img::img_add_watermark($new_path);
								$water_ary=array();
							}
							foreach((array)$resize_ary as $value){
								if($value=='default') continue;
								$size_w_h=explode('x', $value);
								$resize_path=img::resize($new_path, $size_w_h[0], $size_w_h[1]);
							}
						}
						foreach((array)$water_ary as $value){
							img::img_add_watermark($value);
						}
						$ImgPath[$k]=$new_path;
						foreach((array)$resize_ary as $value){
							if(!is_file($c['root_path'].$ImgPath[$k].".{$value}.{$ext_name}")){
								$size_w_h=explode('x', $value);
								$resize_path=img::resize($ImgPath[$k], $size_w_h[0], $size_w_h[1]);
							}
						}
						if(!is_file($c['root_path'].$ImgPath[$k].".default.{$ext_name}")){
							@copy($c['root_path'].$ImgPath[$k], $c['root_path'].$ImgPath[$k].".default.{$ext_name}");
						}
						if($prod_row){//删除原图片
							foreach((array)$resize_ary as $value){
								$ext_name=file::get_ext_name($prod_row['PicPath_'.$k]);
								file::del_file($prod_row['PicPath_'.$k].".{$value}.{$ext_name}");
							}
							file::del_file($prod_row['PicPath_'.$k]);
						}
					}else{
						$ImgPath[$k]=$prod_row['PicPath_'.$k];
					}
				}
				//有属性的情况下，整合产品默认参数
				if($IsHaveAttr==1){
					$SKU=str_replace('-1', '', $val['variants'][0]['sku']);
					$Price_0=$val['variants'][0]['compare_at_price'];
					$Price_1=min($min_price_ary);
					$IsCombination=1;
				}
				//创建产品属性和选项
				if($create_ary){
					//新建产品属性
					$insert_attr_ary=$new_attr_id_ary=array();
					foreach((array)$create_ary as $k=>$v){
						if($k=='Cart'){//规格属性
							foreach((array)$v as $k2=>$v2){
								$insert_attr_ary[$k2]=array("Name{$Lang}"=>$v2, 'CateId'=>"|{$CateId}|", 'CartAttr'=>1, 'Type'=>1);
							}
						}
					}
					//var_dump($insert_attr_ary);
					foreach((array)$insert_attr_ary as $k=>$v){
						db::insert('products_attribute', $v);
						$AttrId=db::get_insert_id();
						//$AttrId='新属性('.$v["Name{$Lang}"].')'.$k;
						$new_attr_id_ary[$k]=$AttrId;//记录新属性
						$attr_data_ary[$v["Name{$Lang}"]]=$AttrId;//补充数据记录
					}
					//新建产品属性选项
					$insert_value_ary=$new_vid_ary=array();
					if($create_ary['Value']){
						foreach((array)$create_ary['Value'] as $k=>$v){
							$k1=(stripos($k, 'ADD:')!==false?$new_attr_id_ary[$k]:$k);
							foreach((array)$v as $k2=>$v2){
								$insert_value_ary[$k2]=array('AttrId'=>$k1, "Value{$Lang}"=>$v2);
							}
						}
					}
					foreach((array)$insert_value_ary as $k=>$v){
						db::insert('products_attribute_value', $v);
						$VId=db::get_insert_id();
						//$VId='新选项'.$k;
						$new_vid_ary[$k]=$VId;//记录新属性选项
						$vid_data_ary[$v['AttrId']][$v["Value{$Lang}"]]=$VId;//补充选项ID数据记录
					}
					//替换属性数据 ("ADD:"替换成"AttrId" "ADDVId:"替换成"VId")
					foreach((array)$attr_ary as $k=>$v){
						$k1=0;
						if(stripos($k, 'ADD:')!==false){//新添加
							$k1=$new_attr_id_ary[$k];
						}
						if(is_array($v)){//选项
							foreach((array)$v as $k2=>$v2){
								if(stripos($v2, 'ADDVId:')!==false){//新添加
									$attr_ary[$k][$k2]=$new_vid_ary[$v2];
								}
							}
						}
						if($k1){//转移数据
							$attr_ary[$k1]=$attr_ary[$k];
							unset($attr_ary[$k]);
						}
					}
					//替换属性价格数据 ("ADDVId:"替换成"VId")
					foreach((array)$ext_ary as $k=>$v){
						$k1=explode('|', trim($k, '|'));
						foreach((array)$k1 as $k2=>$v2){
							$new_vid_ary[$v2] && $k1[$k2]=$new_vid_ary[$v2];
						}
						$k1='|'.implode('|', $k1).'|';
						$ext_ary[$k1]=$ext_ary[$k];
						if($k!=$k1) unset($ext_ary[$k]);//键不相同的时候，才删掉
					}
				}
				//var_dump($attr_data_ary);
				//var_dump($vid_data_ary);
				//var_dump($attr_ary);
				//var_dump($ext_ary);
				//var_dump('-----------------------------------------------------------------------------------------------');
				//var_dump('-----------------------------------------------------------------------------------------------');
				//var_dump('-----------------------------------------------------------------------------------------------');
				//exit;
				$data=array(
					'Source'			=>	'shopify',
					'SourceId'			=>	$val['id'],
					'CateId'			=>	$CateId,
					"Name{$Lang}"		=>	addslashes(stripslashes($val['title'])),
					'Number'			=>	$SKU,
					'SKU'				=>	$SKU,
					'Price_0'			=>	(float)$Price_0,
					'Price_1'			=>	(float)$Price_1,
					'PicPath_0'			=>	$ImgPath[0],
					'PicPath_1'			=>	$ImgPath[1],
					'PicPath_2'			=>	$ImgPath[2],
					'PicPath_3'			=>	$ImgPath[3],
					'PicPath_4'			=>	$ImgPath[4],
					'PicPath_5'			=>	$ImgPath[5],
					'PicPath_6'			=>	$ImgPath[6],
					'PicPath_7'			=>	$ImgPath[7],
					'PicPath_8'			=>	$ImgPath[8],
					'PicPath_9'			=>	$ImgPath[9],
					'Weight'			=>	$Weight,
					'OpenParameter'		=>	'',
					'Stock'				=>	(int)$Stock,
					'IsCombination'		=>	(int)$IsCombination,
					'SoldOut'			=>	0,
					'Tags'				=>	$Tags	
				);
				$desc_data=array(
					"Description{$Lang}"=>addslashes(stripslashes($val['body_html']))
				);
				if($ProId){
					db::update('products', "ProId='{$ProId}'", $data);
				}else{
					db::insert('products', $data);
					$ProId=db::get_insert_id();
					db::insert('products_seo', array('ProId'=>$ProId));
					$desc_data['ProId']=$ProId;
					db::insert('products_description', $desc_data);
				}
				//属性选项勾选
				$insert_ary=$update_ary=$selected_attr_ary=$selected_value_ary=array();
				$attr_row=str::str_code(db::get_all('products_selected_attr', "ProId='$ProId'", '*', 'SId asc'));
				foreach((array)$attr_row as $v){
					$selected_attr_ary[$v['AttrId']]=$v['SId'];
				}
				$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'", '*', 'SeleteId asc'));
				foreach((array)$selected_row as $v){
					$selected_value_ary[$v['VId']]=$v['SeleteId'];
				}
				unset($attr_row, $selected_row);
				$i=1;
				foreach((array)$attr_ary as $k=>$v){//$k:AttrId $v:VId
					//products_selected_attr
					if($selected_attr_ary[$k]){//已有数据
						$update_ary['attr'][$selected_attr_ary[$k]]['IsUsed']=1;
					}else{//没有数据
						$insert_ary['attr'][]=array(
							'ProId'		=>	$ProId,
							'AttrId'	=>	$k,
							'IsUsed'	=>	1
						);
					}
					//products_selected_attribute
					foreach((array)$v as $v2){
						if($selected_value_ary[$v2]){//已有数据
							$update_ary['value'][$selected_value_ary[$v2]]['IsUsed']=1;
							$update_ary['value'][$selected_value_ary[$v2]]['MyOrder']=$i;
						}else{//没有数据
							$insert_ary['value'][]=array(
								'ProId'		=>	$ProId,
								'AttrId'	=>	$k,
								'VId'		=>	$v2,
								'OvId'		=>	1,
								'IsUsed'	=>	1,
								'MyOrder'	=>	$i
							);
						}
						++$i;
					}
				}
				db::update('products_selected_attr', "ProId='$ProId'", array('IsUsed'=>0));//先默认全部关闭
				db::update('products_selected_attribute', "ProId='$ProId' and IsUsed<2 and (VId>0 or (AttrId=0 and VId=0 and OvId>0))", array('IsUsed'=>0));//先默认全部关闭
				if(count($update_ary)){
					foreach((array)$update_ary as $k=>$v){
						foreach($v as $k2=>$v2){
							if($k=='attr'){
								$table='products_selected_attr';
								$w="ProId='$ProId' and SId='{$k2}'";
							}else{
								$table='products_selected_attribute';
								$w="ProId='$ProId' and SeleteId='{$k2}'";
							}
							db::update($table, $w, $v2);
						}
					}
				}
				if(count($insert_ary)){
					foreach((array)$insert_ary as $k=>$v){
						foreach($v as $v2){
							if($k=='attr'){
								$table='products_selected_attr';
							}else{
								$table='products_selected_attribute';
							}
							db::insert($table, $v2);
						}
					}
				}
				//组合数据
				$insert_ary=$update_ary=$combination_ary=$cid_ary=array();
				$combination_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'"));
				foreach((array)$combination_row as $v){
					$combination_ary[$v['Combination']][$v['OvId']]=$v['CId'];
				}
				unset($attr_row, $selected_row);
				foreach((array)$ext_ary as $Combination=>$v){
					foreach((array)$v as $OvId=>$v2){
						$CId=$combination_ary[$Combination][$OvId];
						if($CId){//已存在
							$cid_ary[]=$CId;
							$update_ary[$CId]=array(
								'Price'			=>	$v2[0],
								'Stock'			=>	$v2[1],
								'Weight'		=>	$v2[2],
								'SKU'			=>	$v2[3],
								'IsIncrease'	=>	$v2[4]
							);
						}else{ //不存在
							$insert_ary[]=array(
								'ProId'			=>	$ProId,
								'Combination'	=>	$Combination,
								'OvId'			=>	$OvId,
								'Price'			=>	$v2[0],
								'Stock'			=>	$v2[1],
								'Weight'		=>	$v2[2],
								'SKU'			=>	$v2[3],
								'IsIncrease'	=>	$v2[4]
							);
						}
					}
				}
				if(count($update_ary)){
					foreach((array)$update_ary as $k=>$v){
						db::update('products_selected_attribute_combination', "CId='$k'", $v);
					}
				}
				if(count($cid_ary)){//清空多余没用的选项数据
					$cid_str=implode(',', $cid_ary);
					db::delete('products_selected_attribute_combination', "ProId='$ProId' and CId not in($cid_str)");
				}else{//清空已经取消勾选的选项数据
					db::delete('products_selected_attribute_combination', "ProId='$ProId'");
				}
				if(count($insert_ary)){
					foreach((array)$insert_ary as $k=>$v){
						db::insert('products_selected_attribute_combination', $v);
					}
				}
			}
		}
		if($p_Page<$TotalPages){//继续执行
			ly200::e_json(array('page'=>($p_Page+1), 'now_count'=>($Count*$p_Page), 'total_count'=>$Count, 'percent'=>sprintf("%d", $p_Page/$TotalPages*100)), 2);
		}
		//manage::operation_log('同步Shopify');
		ly200::e_json(array('tips'=>$c['manage']['lang_pack']['msg']['import_success'], 'now_count'=>$Count, 'total_count'=>$Count, 'percent'=>100), 1);
	}
	//同步Shopify End
	
	/*****************************DHL在线开户 start******************************/
	/**
	 * DHL在线开户第一步：请选择开户地点和账户类型
	 * 1. 开户地点：香港、深圳、上海
	 * 2. 账号类型：企业用户、个人账号
	 * 3. 推荐人(可选) 
	 * 4. 保存到config数据库
	 * @return 无
	 */
	public static function dhl_account_open_step_1()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AccountAddress = (int)$p_AccountAddress;
		$AccountType = (int)$p_AccountType;
		$Referrer = $p_Referrer;

		if($AccountAddress == 0){
			// 1. 香港没有增值税发票信息征询表和委托付款内容，需要清除
			if(is_file($c['root_path']."/u_file/file/dhl/Inquiry Form for VAT Invoice_sample output file.xls")){
				file::del_file("/u_file/file/dhl/Inquiry Form for VAT Invoice_sample output file.xls");
			}
		}

		$Data = array(
			'AccountAddress'	=>	$AccountAddress,
			'AccountType'		=>	$AccountType,
			'Referrer'			=>	stripcslashes($Referrer),
		);
		$JsonData = str::json_data($Data);
		manage::config_operaction(array('DHLAccountOpenStep1' => addslashes($JsonData)), 'plugins');
		manage::operation_log('DHL选择开户地点和账户类型');
		ly200::e_json('', 1);
	}

	/**
	 * DHL在线开户第二步：录入信息
	 * 1. 公司信息：公司名称、营业执照、注册日期、注册地、公司地址、街道、门牌号、邮编、城市、省份/地区、国家、电话号码、网址(可选)、传真号码、联络人姓名、联络人邮箱、联络人手机号码、系统对接联系人名字、系统对接联系人邮箱、系统对接联系人手机号码
	 * 2. 账单/发票地址：注册公司名称、发票/账单地址、邮编/城市、电话号码、传真号码、授权签字人姓名、联络人姓名、联络人邮箱、联络人手机号码
	 * 3. 取件地址：注册公司名称、取件地址、邮编/城市、电话号码、传真号码、授权签字人姓名、联络人姓名、联络人邮箱、联络人手机号码
	 * 4. 信用验证资讯：预计年付运费、经营者/董事姓名、地址、联络人电话号码、联络人邮箱、财务联络人姓名、财务联络人地址、财务联络人电话号码、财务联络人邮箱、办公场所
	 * 5. 增值税发票信息征询表：公司名称、纳税人类型、统一社会信用代码、公司地址、公司电话、开户银行名称、开户银行账号、发票类型、税务发票事宜联系人、税务发票事宜联系人电话、税务发票事宜联系人邮箱
	 * 6. 委托付款：是否由第三方付款、第三方名称、第三方银行账号
	 * 7. 保存到config数据库
	 * @return 无
	 */
	public static function dhl_account_open_step_2()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

		// 更新数据
		$DHLAccountOpenStep2 = empty($DHLAccountOpenStep2) ? array() : $DHLAccountOpenStep2;
		$Data = array();
		foreach((array)$DHLAccountOpenStep2 as $k => $v){
			$Data[$k] = stripcslashes($v);
		}

		if($p_Step == 'Company'){
			/**************** 更新公司信息 start *******************/
			$Data['CompanyName'] = stripcslashes($p_CompanyName);
			$Data['BusinessNumber'] = stripcslashes($p_BusinessNumber);
			$Data['RegisterDate'] = stripcslashes($p_RegisterDate);
			$Data['RefisterAddress'] = stripcslashes($p_RefisterAddress);
			$Data['CompanyAddress'] = stripcslashes($p_CompanyAddress);
			$Data['Street'] = stripcslashes($p_Street);
			$Data['HouseNo'] = stripcslashes($p_HouseNo);
			$Data['ZipCode'] = stripcslashes($p_ZipCode);
			$Data['City'] = stripcslashes($p_City);
			$Data['State'] = stripcslashes($p_State);
			$Data['Country'] = stripcslashes($p_Country);
			$Data['Telephone'] = stripcslashes($p_Telephone);
			$Data['Website'] = stripcslashes($p_Website);
			$Data['FaxNo'] = stripcslashes($p_FaxNo);
			$Data['ContactName'] = stripcslashes($p_ContactName);
			$Data['ContactEmail'] = stripcslashes($p_ContactEmail);
			$Data['ContactMobile'] = stripcslashes($p_ContactMobile);
			$Data['SystemContactName'] = stripcslashes($p_SystemContactName);
			$Data['SystemContactEmail'] = stripcslashes($p_SystemContactEmail);
			$Data['SystemContactMobile'] = stripcslashes($p_SystemContactMobile);
			/**************** 更新公司信息 end *******************/
		}elseif($p_Step == 'Billing'){
			/**************** 账单/发票地址 start *******************/
			$Data['BillCompanyName'] = stripcslashes($p_BillCompanyName);
			$Data['BillCompanyAddress'] = stripcslashes($p_BillCompanyAddress);
			$Data['BillZipCode'] = stripcslashes($p_BillZipCode);
			$Data['BillTelephone'] = stripcslashes($p_BillTelephone);
			$Data['BillFaxNo'] = stripcslashes($p_BillFaxNo);
			$Data['BillAuthorizedName'] = stripcslashes($p_BillAuthorizedName);
			$Data['BillContactName'] = stripcslashes($p_BillContactName);
			$Data['BillContactEmail'] = stripcslashes($p_BillContactEmail);
			$Data['BillContactMobile'] = stripcslashes($p_BillContactMobile);
			$Data['BillSameRegisterAddress'] = (int)$p_BillSameRegisterAddress;

			if($p_BillCompanyName != $Data['CompanyName'] || $p_BillCompanyAddress != $Data['CompanyAddress'] || $p_BillZipCode != $Data['City'] || $p_BillTelephone != $Data['Telephone'] || $p_BillFaxNo != $Data['FaxNo'] || $p_BillAuthorizedName != $Data['ContactName'] || $p_BillContactName != $Data['ContactName'] || $p_BillContactEmail != $Data['ContactEmail'] || $p_BillContactMobile != $Data['ContactMobile']){
				$Data['BillSameRegisterAddress'] = 0;
			}
			/**************** 账单/发票地址 end *******************/
		}elseif($p_Step == 'Pickup'){
			/**************** 取件地址 start *******************/
			$Data['PickupSameRegisterAddress'] = (int)$p_PickupSameRegisterAddress;
			$Data['PickupAddress'] = stripcslashes($p_PickupAddress);
			$Data['PickupZipCode'] = stripcslashes($p_PickupZipCode);
			$Data['PickupTelephone'] = stripcslashes($p_PickupTelephone);
			$Data['PickupFaxNo'] = stripcslashes($p_PickupFaxNo);
			$Data['PickupAuthorizedName'] = stripcslashes($p_PickupAuthorizedName);
			$Data['PickupContactName'] = stripcslashes($p_PickupContactName);
			$Data['PickupContactEmail'] = stripcslashes($p_PickupContactEmail);
			$Data['PickupContactMobile'] = stripcslashes($p_PickupContactMobile);

			if($p_PickupAddress != $Data['CompanyAddress'] || $p_PickupZipCode != $Data['City'] || $p_PickupTelephone != $Data['Telephone'] || $p_PickupFaxNo != $Data['FaxNo'] || $p_PickupAuthorizedName != $Data['ContactName'] || $p_PickupContactName != $Data['ContactName'] || $p_PickupContactEmail!= $Data['ContactEmail'] || $p_PickupContactMobile != $Data['ContactMobile']){
				$Data['PickupSameRegisterAddress'] = 0;
			}
			/**************** 取件地址 end *******************/
		}elseif($p_Step == 'Finance'){
			/**************** 信用验证资讯 start *******************/
			$Data['AnnualEstRevenue'] = stripcslashes($p_AnnualEstRevenue);
			$Data['OwnerName'] = stripcslashes($p_OwnerName);
			$Data['OwnerAddress'] = stripcslashes($p_OwnerAddress);
			$Data['OwnerTelephone'] = stripcslashes($p_OwnerTelephone);
			$Data['OwnerEmail'] = stripcslashes($p_OwnerEmail);
			$Data['FinanceContactName'] = stripcslashes($p_FinanceContactName);
			$Data['FinanceContactAddress'] = stripcslashes($p_FinanceContactAddress);
			$Data['FinanceContactTelephone'] = stripcslashes($p_FinanceContactTelephone);
			$Data['FinanceContactEmail'] = stripcslashes($p_FinanceContactEmail);
			$Data['OfficePremise'] = (int)$p_OfficePremise;
			/**************** 信用验证资讯 end *******************/
		}elseif($p_Step == 'VATInvoice'){
			/**************** 增值税发票信息征询表 start *******************/
			$Data['VATCompany'] = stripcslashes($p_VATCompany);
			$Data['TaxpayerType'] = (int)$p_TaxpayerType;
			if($Data['TaxpayerType'] == 1){
				// 非增值税一般纳税人,无需填写统一社会信用代码，公司地址（营业执照注册），公司电话（营业执照注册），开户银行名称（税局备案信息），开户银行账号（税局备案信息）
				unset($Data['SocialCreditCode']);
				unset($Data['VATCompanyAddress']);
				unset($Data['VATCompanyTelephone']);
				unset($Data['VATBankName']);
				unset($Data['VATBankAccount']);
			}else{
				$Data['SocialCreditCode'] = stripcslashes($p_SocialCreditCode);
				$Data['VATCompanyAddress'] = stripcslashes($p_VATCompanyAddress);
				$Data['VATCompanyTelephone'] = stripcslashes($p_VATCompanyTelephone);
				$Data['VATBankName'] = stripcslashes($p_VATBankName);
				$Data['VATBankAccount'] = stripcslashes($p_VATBankAccount);
			}
			$Data['VATInvoiceType'] = (int)$p_VATInvoiceType;
			$Data['VATInvoiceContactName'] = stripcslashes($p_VATInvoiceContactName);
			$Data['VATInvoiceContactTelephone'] = stripcslashes($p_VATInvoiceContactTelephone);
			$Data['VATInvoiceContactEmail'] = stripcslashes($p_VATInvoiceContactEmail);
			/**************** 增值税发票信息征询表 end *******************/
			/**************** 委托付款 start *******************/
			$Data['IsPaymentAuth'] = (int)$p_IsPaymentAuth;
			if($Data['IsPaymentAuth'] == 1){
				// 选择否由第三方付款，那么无需第三方名称，第三方银行账号
				unset($Data['ThirdCompanyName']);
				unset($Data['ThirdBankAccount']);
			}else{
				$Data['ThirdCompanyName'] = stripcslashes($p_ThirdCompanyName);
				$Data['ThirdBankAccount'] = stripcslashes($p_ThirdBankAccount);
			}
			/**************** 委托付款 end *******************/
		}


		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
		if ((int)$DHLAccountOpenStep1['AccountAddress'] == 0) {
			// 1. 香港没有增值税发票信息征询表和委托付款内容，需要清除
			unset($Data['VATCompany']);
			unset($Data['TaxpayerType']);
			unset($Data['SocialCreditCode']);
			unset($Data['VATCompanyAddress']);
			unset($Data['VATCompanyTelephone']);
			unset($Data['VATBankName']);
			unset($Data['VATBankAccount']);
			unset($Data['VATInvoiceType']);
			unset($Data['VATInvoiceContactName']);
			unset($Data['VATInvoiceContactTelephone']);
			unset($Data['VATInvoiceContactEmail']);
			unset($Data['IsPaymentAuth']);
			unset($Data['ThirdCompanyName']);
			unset($Data['ThirdBankAccount']);
		}

		$JsonData = str::json_data($Data);
		manage::config_operaction(array('DHLAccountOpenStep2' => addslashes($JsonData)), 'plugins');
		manage::operation_log('DHL录入信息');

		// 生成PDF文件
		pdf::dhl_account_app_form_pdf();

		// 生成Inquiry Form for VAT Invoice_sample output file.xls(增值税)表格
		if ((int)$DHLAccountOpenStep1['AccountAddress']) {
			self::dhl_account_open_excel_vat();
		}
		self::dhl_account_open_excel();

		ly200::e_json('', 1);
	}

	public static function dhl_same_address(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

		$data = array(
			'CompanyName'	=>	$DHLAccountOpenStep2['CompanyName'],
			'CompanyAddress'=>	$DHLAccountOpenStep2['CompanyAddress'],
			'City'			=>	$DHLAccountOpenStep2['City'],
			'Telephone'		=>	$DHLAccountOpenStep2['Telephone'],
			'FaxNo'			=>	$DHLAccountOpenStep2['FaxNo'],
			'ContactName'	=>	$DHLAccountOpenStep2['ContactName'],
			'ContactEmail'	=>	$DHLAccountOpenStep2['ContactEmail'],
			'ContactMobile'	=>	$DHLAccountOpenStep2['ContactMobile'],
		);

		ly200::e_json($data, 1);
	}

	/**
	 * 下载文件
	 * @return 无
	 */
	public static function download_file()
	{
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$file = $g_file;
		if (is_file($c['root_path'].stripcslashes($file))) {
			file::down_file(stripcslashes($file));
		}
		ly200::e_json('', 1);
	}

	/**
	 * DHL在线开户第三步：签名盖章
	 * 1. 选择是否有带电货品
	 * 2. 上传文件
	 * 3. 保存到config数据库
	 * @return 无
	 */
	public static function dhl_account_open_step_3()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$HasCharged = (int)$p_HasCharged;
		$KCFilePath = $p_KCFilePath;
		$KCFileName = $p_KCFileName;
		$DGFilePath = $p_DGFilePath;
		$DGFileName = $p_DGFileName;

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
		$AccountAddress = (int)$DHLAccountOpenStep1['AccountAddress'];

		if($AccountAddress == 0 || $AccountAddress == 1){
			!is_file($c['root_path'] . $KCFilePath) && ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_update'] . ': Known Consignor Declaration of Compliance.pdf' ,0);
		}

		if($HasCharged){
			!is_file($c['root_path'] . $DGFilePath) && ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_update'] . ': DG Indemnity Letter.pdf' ,0);
		}

		$save_dir = '/u_file/file/dhl/';
		file::mk_dir($save_dir);
		if (is_file($c['root_path'] . $KCFilePath)) {
			$ext_name = file::get_ext_name($KCFilePath);
			@rename($c['root_path'] . $KCFilePath, $c['root_path'] . $save_dir . 'Known Consignor Declaration of Compliance.' . $ext_name);//移动文件
			$KCFilePath = $save_dir . 'Known Consignor Declaration of Compliance.' . $ext_name;
		}
		if (is_file($c['root_path'] . $DGFilePath)) {
			$ext_name = file::get_ext_name($DGFilePath);
			@rename($c['root_path'] . $DGFilePath, $c['root_path'] . $save_dir . 'DG Indemnity Letter.' . $ext_name);//移动文件
			$DGFilePath = $save_dir . 'DG Indemnity Letter.' . $ext_name;
		}

		$Data = array(
			'HasCharged'		=>	$HasCharged,
			'KCFilePath'		=>	stripcslashes($KCFilePath),
			'KCFileName'		=>	stripcslashes($KCFileName),
			'DGFilePath'		=>	stripcslashes($DGFilePath),
			'DGFileName'		=>	stripcslashes($DGFileName),
		);
		$JsonData = str::json_data($Data);
		manage::config_operaction(array('DHLAccountOpenStep3' => addslashes($JsonData)), 'plugins');
		manage::operation_log('DHL签名盖章');
		ly200::e_json('', 1);
	}

	/**
	 * DHL在线开户第四步：上传资料
	 * 1. 营业执照图片
	 * 2. 身份证(照片面)
	 * 3. 身份证(芯片面)
	 * 4. 保存到config数据库
	 * @return 无
	 */
	public static function dhl_account_open_step_4()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$DHLAccountOpenStep4Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep4"', 'Value');
		$DHLAccountOpenStep4 = str::json_data($DHLAccountOpenStep4Data, 'decode');

		$PicPath[0] = $p_PicPath_0;
		$PicPath[1] = $p_PicPath_1;
		$PicPath[2] = $p_PicPath_2;

		$title_ary = array('Business license', 'Copy of ID card (photo face)', 'Copy of ID card (chip side)');
		$save_dir = '/u_file/file/dhl/';
		for ($i = 0; $i < 3; $i++) {
			if (is_file($c['root_path'] . $PicPath[$i])) {
				$base_name = file::get_base_name($PicPath[$i]);
				$ext_name = file::get_ext_name($PicPath[$i]);
				// 删除已被更新的图片
				if(is_file($c['root_path'].$DHLAccountOpenStep4['PicPath_'.$i]) && $ext_name != file::get_ext_name($DHLAccountOpenStep4['PicPath_'.$i])){
					file::del_file($DHLAccountOpenStep4['PicPath_'.$i]);
				}
				@copy($c['root_path'] . $PicPath[$i], $c['root_path'] . $save_dir . $title_ary[$i] . '.' . $ext_name);//移动文件
				$PicPath[$i] = $save_dir . $title_ary[$i] . '.' . $ext_name;
			}else{
				if((int)$DHLAccountOpenStep1['AccountAddress']){
					$PicPath[$i] = '';
					ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_update'] . ': ' . $title_ary[$i], 0);
				}
			}
		}

		$Data = array(
			'PicPath_0'		=>	$PicPath[0],
			'PicPath_1'		=>	$PicPath[1],
			'PicPath_2'		=>	$PicPath[2],
		);
		$JsonData = str::json_data($Data);
		manage::config_operaction(array('DHLAccountOpenStep4' => addslashes($JsonData)), 'plugins');
		manage::operation_log('DHL上传资料');

		ly200::e_json('', 1);
	}

	/**
	 * DHL在线开户第五步：托运人隐含危险物品清单
	 * 1. 托运人名称
	 * 2. 公司名称
	 * 3. 联系人号码
	 * 4. 货物说明
	 * 5. 货运参考号
	 * 6. 保存到config数据库
	 * @return 无
	 */
	public static function dhl_account_open_step_5()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ShipperName = $p_ShipperName;
		$CompanyName = $p_CompanyName;
		$ShipperTelephone = $p_ShipperTelephone;
		$CargoDescription = $p_CargoDescription;
		$FreightReference = $p_FreightReference;
		
		$Data = array(
			'ShipperName'		=>	stripcslashes($ShipperName),
			'CompanyName'		=>	stripcslashes($CompanyName),
			'ShipperTelephone'	=>	stripcslashes($ShipperTelephone),
			'CargoDescription'	=>	stripcslashes($CargoDescription),
			'FreightReference'	=>	stripcslashes($FreightReference),
		);
		$JsonData = str::json_data($Data);
		manage::config_operaction(array('DHLAccountOpenStep5' => addslashes($JsonData)), 'plugins');
		manage::operation_log('DHL托运人隐含危险物品清单');

		// 生成Shipper's Checklist for Hidden Dangerous Goods.pdf
		pdf::shipper_dangerous_goods_pdf();
		ly200::e_json('', 1);
	}

	/**
	 * DHL在线开户第六步：个人资料
	 * 1. 我已阅读并同意以上注册条款细则
	 * 2. 保存到config数据库
	 * @return 已勾选，返回去到下一页，未勾选，提示请勾选
	 */
	public static function dhl_account_open_step_6()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ComfireInfomation = (int)$p_ComfireInfomation;
		
		if ($ComfireInfomation) {
			$Data = array(
				'ComfireInfomation'		=>	(int)$ComfireInfomation,
			);
			$JsonData = str::json_data($Data);
			manage::config_operaction(array('DHLAccountOpenStep6' => addslashes($JsonData)), 'plugins');
			manage::operation_log('DHL个人资料');
			pdf::personal_information_pdf();
			ly200::e_json('', 1);
		} else {
			ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_comfire'], 0);
		}
	}

	/**
	 * DHL在线开户第七步：价格表
	 * 1. 我已阅读并同意以上注册条款细则
	 * 2. 保存到config数据库
	 * @return 已勾选，返回去到下一页，未勾选，提示请勾选
	 */
	public static function dhl_account_open_step_7()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ComfireInfomation = (int)$p_ComfireInfomation;
		
		if ($ComfireInfomation) {
			$Data = array(
				'ComfireInfomation'		=>	(int)$ComfireInfomation,
			);
			$JsonData = str::json_data($Data);
			manage::config_operaction(array('DHLAccountOpenStep7' => addslashes($JsonData)), 'plugins');
			manage::operation_log('DHL价格表');

			// 根据不同地点，显示不同的价格表
			$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
			$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
			$AccountAddress = (int)$DHLAccountOpenStep1['AccountAddress'];
			$address = $AccountAddress == 0 ? 'HKG' : ($AccountAddress == 1 ? 'SZX' : 'SHA');
			pdf::RateCard_pdf($address);

			ly200::e_json('', 1);
		} else {
			ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_comfire'], 0);
		}
	}

	/**
	 * DHL在线开户第八步：条款与条件
	 * 1. 我已阅读并同意以上注册条款细则
	 * 2. 保存到config数据库
	 * @return 已勾选，返回去到下一页，未勾选，提示请勾选
	 */
	public static function dhl_account_open_step_8()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ComfireInfomation = (int)$p_ComfireInfomation;
		
		if ($ComfireInfomation) {
			$Data = array(
				'ComfireInfomation'		=>	(int)$ComfireInfomation,
			);
			$JsonData = str::json_data($Data);
			manage::config_operaction(array('DHLAccountOpenStep8' => addslashes($JsonData)), 'plugins');
			manage::operation_log('DHL个人资料');
			ly200::e_json('', 1);
		} else {
			ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['please_comfire'], 0);
		}
	}

	/**
	 * DHL在线开户第九步：预付款信息页面
	 * 1. 
	 * 2. 保存到config数据库
	 * @return 
	 */
	public static function dhl_account_open_step_9()
	{
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$PicPath = $p_PicPath;

		if($PicPath){
			$DHLAccountOpenStep9Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep9"', 'Value');
			$DHLAccountOpenStep9 = str::json_data($DHLAccountOpenStep9Data, 'decode');

			$save_dir = '/u_file/file/dhl/';
			if (is_file($c['root_path'] . $PicPath)) {
				$base_name = file::get_base_name($PicPath);
				$ext_name = file::get_ext_name($PicPath);
				// 删除已被更新的图片
				if(is_file($c['root_path'].$DHLAccountOpenStep9['PicPath']) && $ext_name != file::get_ext_name($DHLAccountOpenStep9['PicPath'])){
					file::del_file($DHLAccountOpenStep9['PicPath']);
				}
				@copy($c['root_path'] . $PicPath, $c['root_path'] . $save_dir . 'BankdetailsorHLe.' . $ext_name);//移动文件
				$PicPath = $save_dir . 'BankdetailsorHLe.' . $ext_name;
			}

			$Data = array(
				'PicPath'		=>	$PicPath,
			);
			$JsonData = str::json_data($Data);
			manage::config_operaction(array('DHLAccountOpenStep9' => addslashes($JsonData)), 'plugins');
			
			$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
			$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
			$AccountAddress = (int)$DHLAccountOpenStep1['AccountAddress'];
			$address = $AccountAddress == 0 ? 'HKG' : ($AccountAddress == 1 ? 'SZX' : 'SHA');
			$addressFullName = $AccountAddress == 0 ? 'HongKong' : ($AccountAddress == 1 ? 'ShenZhen' : 'ShangHai');

			$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
			$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');
			/****************************** 发送邮件通知DHL ******************************/
			$title = 'UEESHOP + ' . $addressFullName . ' + DHL eCommerce (' . (substr($address, 0, 2)) . ') Ltd';
			$html = '<div>';
				$html .= '<p>· Referred by ['.$DHLAccountOpenStep1['Referrer'].']</p>';
				$html .= '<p>· Company Name[' . $DHLAccountOpenStep2['CompanyName'] . ']</p>';
				$html .= '<p>· Company address City only[' . $addressFullName . ']</p>';
				$html .= '<p>· Company contact person name[' . $DHLAccountOpenStep2['ContactName'] . ']</p>';
				$html .= '<p>· Company contact person phone number[' . $DHLAccountOpenStep2['ContactMobile'] . ']</p>';
				$html .= '<p>· Company contact person email[' . $DHLAccountOpenStep2['ContactEmail'] . ']</p>';
				$html .= '<p></p>';
				$html .= '<p>· Implementation contact person name[' . $DHLAccountOpenStep2['SystemContactName'] . ']</p>';
				$html .= '<p>· Implementation contact person phone number[' . $DHLAccountOpenStep2['SystemContactMobile'] . ']</p>';
				$html .= '<p>· Implementation contact person email[' . $DHLAccountOpenStep2['SystemContactEmail'] . ']</p>';
				$html .= '<p></p>';
				$html .= '<p>The file is ready by /UEESHOP/' . $address . '/ or '. date('Ymd', $c['time']) . '-' . $c['Number'] . '-' . $c['time'] . '.zip.Please download. Thanks.</p>';
			$html .= '</div>';

			ly200::sendmail(array('343983282@qq.com'), $title, $html);
			/****************************** 发送邮件通知DHL ******************************/
			/****************************** 文件已经全部生成，打包压缩 ******************************/
			self::dhl_compressed_file();
			/****************************** 文件已经全部生成，打包压缩 ******************************/
			/****************************** 将压缩文件发送至SFTP服务器 ******************************/
			$config = array(
				'host'			=> 'ftp.dhlecommerce.asia',   // ftp地址
				'user'			=> 'UeeShop', 
				'key_path'		=> $c['root_path'] . 'inc/class/sftp/id_rsa',     // 私钥的存储地址
			);
			sftp::put($config, $c['root_path'] . 'u_file/file/' . $c['Number'] . '.zip', '/UeeShop/' . $address . '/' . date('Ymd', $c['time']) . '-' . $c['Number'] . '-' . $c['time'] . '.zip');
			/****************************** 将压缩文件发送至SFTP服务器 ******************************/

			manage::operation_log('DHL预付款信息页保存');
			ly200::e_json('', 1);
		}else{
			ly200::e_json($c['manage']['lang_pack']['plugins']['dhl_account_open']['no_picture'], 0);
		}
	}

	/**
	 * 生成DHL需要的 增值税发票信息征询表 Excel表格文件
	 * @return 无
	 */
	public static function dhl_account_open_excel_vat()
	{
		global $c;
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');

		// 增值税相关信息
		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

		$objPHPExcel=new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
		$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
		$objPHPExcel->getProperties()->setCategory("Test result file");
		$objPHPExcel->setActiveSheetIndex(0);
		for ($i = 1; $i <= 3; $i++) {
			if ($i == 2) {
				$objPHPExcel->getActiveSheet()->mergeCells('A2:C2')->setCellValue('A2', '增值税发票信息征询表');
			} else {
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '');
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '');
			}
		}


		$objPHPExcel->getActiveSheet()->mergeCells('A4:C4')->setCellValue('A4', '尊敬的客户：请完整填列下表（*为必填项），并确保相关税务信息与贵司税务登记证以及向税局备案信息完全一致，感谢您的支持与配合！');

		$objPHPExcel->getActiveSheet()->setCellValue('A5', "*公司名称\r\n（营业执照的公司名称）：");
		$objPHPExcel->getActiveSheet()->mergeCells('B5:C5')->setCellValue('B5', $DHLAccountOpenStep2['VATCompany']);

		$objPHPExcel->getActiveSheet()->mergeCells('A6:A8')->setCellValue('A6', "*请选择所属纳税人类型：");
		$objPHPExcel->getActiveSheet()->mergeCells('B6:B7')->setCellValue('B6', "*请填写所属纳税人类型的代码");
		$objPHPExcel->getActiveSheet()->setCellValue('C6', ($DHLAccountOpenStep2['TaxpayerType'] == 0 ? '☑' : '☐') . " 1.增值税一般纳税人");
		$objPHPExcel->getActiveSheet()->setCellValue('C7', ($DHLAccountOpenStep2['TaxpayerType'] == 1 ? '☑' : '☐') . " 2.非增值税一般纳税人");
		$objPHPExcel->getActiveSheet()->mergeCells('B8:C8')->setCellValue('B8', "如果您属于“1.增值税一般纳税人”，请务必完整填列下表全部信息，并提供贵公司一般纳税人认定通知书复印件。\r\n如果您属于“2.非增值税一般纳税人”，请填列下表带*项信息。");

		$objPHPExcel->getActiveSheet()->setCellValue('A9', '统一社会信用代码：');
		$objPHPExcel->getActiveSheet()->mergeCells('B9:C9')->setCellValue('B9', $DHLAccountOpenStep2['SocialCreditCode']);
		$objPHPExcel->getActiveSheet()->setCellValue('A10', '公司地址（营业执照注册）：');
		$objPHPExcel->getActiveSheet()->mergeCells('B10:C10')->setCellValue('B10', $DHLAccountOpenStep2['VATCompanyAddress']);
		$objPHPExcel->getActiveSheet()->setCellValue('A11', '公司电话（营业执照注册）：');
		$objPHPExcel->getActiveSheet()->mergeCells('B11:C11')->setCellValue('B11', $DHLAccountOpenStep2['VATCompanyTelephone']);
		$objPHPExcel->getActiveSheet()->setCellValue('A12', '开户银行名称（税局备案信息）：');
		$objPHPExcel->getActiveSheet()->mergeCells('B12:C12')->setCellValue('B12', $DHLAccountOpenStep2['VATBankName']);
		$objPHPExcel->getActiveSheet()->setCellValue('A13', '开户银行账号（税局备案信息）：');
		$objPHPExcel->getActiveSheet()->mergeCells('B13:C13')->setCellValue('B13', $DHLAccountOpenStep2['VATBankAccount']);
		$objPHPExcel->getActiveSheet()->mergeCells('A14:A16')->setCellValue('A14', '*请选择上线后所需我司开具的发票类型：');
		$objPHPExcel->getActiveSheet()->mergeCells('B14:C14')->setCellValue('B14', ($DHLAccountOpenStep2['VATInvoiceType'] == 0 ? '☑' : '☐') . ' 1.增值税普通发票');
		$objPHPExcel->getActiveSheet()->mergeCells('B15:C15')->setCellValue('B15', ($DHLAccountOpenStep2['VATInvoiceType'] == 1 ? '☑' : '☐') . " 2.增值税专用发票\r\n（只有增值税一般纳税人方可选择此项）");
		$objPHPExcel->getActiveSheet()->mergeCells('B16:C16')->setCellValue('B16', ($DHLAccountOpenStep2['VATInvoiceType'] == 2 ? '☑' : '☐') . ' 3.无需税务发票');
		$objPHPExcel->getActiveSheet()->setCellValue('A17', '*税务发票事宜联系人：');
		$objPHPExcel->getActiveSheet()->mergeCells('B17:C17')->setCellValue('B17', $DHLAccountOpenStep2['VATInvoiceContactName']);
		$objPHPExcel->getActiveSheet()->setCellValue('A18', '*税务发票事宜联系人电话：');
		$objPHPExcel->getActiveSheet()->mergeCells('B18:C18')->setCellValue('B18', $DHLAccountOpenStep2['VATInvoiceContactTelephone']);
		$objPHPExcel->getActiveSheet()->setCellValue('A19', '*税务发票事宜联系人邮箱：');
		$objPHPExcel->getActiveSheet()->mergeCells('B19:C19')->setCellValue('B19', $DHLAccountOpenStep2['VATInvoiceContactEmail']);

		//设置列的宽度  
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);

		//设置行的高度
		for ($i = 1; $i <= 19; $i++) {
			if ($i == 5 || $i == 8) {
				$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(50);
			} else {
				$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(20);
			}
		}

		// 设置边框
		$styleBorder = array(
	        'borders' => array(
	            'allborders' => array( //设置全部边框
	                'style' => PHPExcel_Style_Border::BORDER_THIN 
	            ),

	        ),
	    );
	    $objPHPExcel->getActiveSheet()->getStyle('A4:C19')->applyFromArray($styleBorder);
			
		//自动换行
		$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('A5')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('B8')->getAlignment()->setWrapText(true);

		// 设置居中
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		// 设置字体颜色
		$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );

		$objPHPExcel->getActiveSheet()->setTitle('增值税发票信息征询表');
		$objPHPExcel->setActiveSheetIndex(0);

		//保存Excel文件
		$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($c['root_path']."/u_file/file/dhl/Inquiry Form for VAT Invoice_sample output file.xls");

		unset($c, $objPHPExcel, $objWriter, $styleBorder, $DHLAccountOpenStep2Data, $DHLAccountOpenStep2);
	}

	/**
	 * 生成DHL需要的 Online Account Opening Form in Excel.xls 表格文件
	 * @return 无
	 */
	public static function dhl_account_open_excel()
	{
		global $c;
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include_once($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');

		// 注册相关信息
		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

		$objPHPExcel2=new PHPExcel();
		$objPHPExcel2->getProperties()->setCreator("Maarten Balliauw");
		$objPHPExcel2->getProperties()->setLastModifiedBy("Maarten Balliauw");
		$objPHPExcel2->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel2->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel2->getProperties()->setKeywords("office 2007 openxml php");
		$objPHPExcel2->getProperties()->setCategory("Test result file");
		$objPHPExcel2->setActiveSheetIndex(0);

		$arr=range('A', 'U');

		$title_ary = array(
			'',
			'Company Name',
			'Company Address',
			'House No.',
			'Street',
			'Postal Code',
			'City',
			'Region',
			'Country',
			'Telephone',
			'Name of Contact Person',
			'Email Address',
			'Mobile Number of Contact Person',
			'(Implementation) Name of Contact Person',
			'(Implementation) Mobile Number of Contact Person',
			'Email Address(Implementation person)',
			'Pick Up Address',
			'City/Zip Code',
			'Telephone Number',
			'Name of Contact Person(Pickup address)',
			'Mobile nUmber of Contact Person'
		);

		$value_ary = array(
			'1',
			$DHLAccountOpenStep2['CompanyName'],
			$DHLAccountOpenStep2['CompanyAddress'],
			$DHLAccountOpenStep2['HouseNo'],
			$DHLAccountOpenStep2['Street'],
			$DHLAccountOpenStep2['ZipCode'],
			$DHLAccountOpenStep2['City'],
			$DHLAccountOpenStep2['State'],
			$DHLAccountOpenStep2['Country'],
			$DHLAccountOpenStep2['Telephone'],
			$DHLAccountOpenStep2['ContactName'],
			$DHLAccountOpenStep2['ContactEmail'],
			$DHLAccountOpenStep2['ContactMobile'],
			$DHLAccountOpenStep2['SystemContactName'],
			$DHLAccountOpenStep2['SystemContactEmail'],
			$DHLAccountOpenStep2['SystemContactMobile'],
			$DHLAccountOpenStep2['PickupAddress'],
			$DHLAccountOpenStep2['PickupZipCode'],
			$DHLAccountOpenStep2['PickupTelephone'],
			$DHLAccountOpenStep2['PickupContactName'],
			$DHLAccountOpenStep2['PickupContactMobile'],
		);

		foreach ((array)$arr as $k => $v) {
			$objPHPExcel2->getActiveSheet()->setCellValue($v . '1', $title_ary[$k]);
			$objPHPExcel2->getActiveSheet()->setCellValue($v . '2', $value_ary[$k]);
			//设置列的宽度  
			$objPHPExcel2->getActiveSheet()->getColumnDimension($v)->setWidth(30);
		}


		$objPHPExcel2->getActiveSheet()->getRowDimension(1)->setRowHeight(20);
		$objPHPExcel2->getActiveSheet()->getRowDimension(2)->setRowHeight(20);

		// 设置居中
		$objPHPExcel2->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel2->getActiveSheet()->setTitle('在线开户信息');
		$objPHPExcel2->setActiveSheetIndex(0);

		//保存Excel文件
		$objWriter2=new PHPExcel_Writer_Excel5($objPHPExcel2);
		$objWriter2->save($c['root_path']."/u_file/file/dhl/Online Account Opening Form in Excel.xls");

		unset($c, $objPHPExcel2, $objWriter2, $title_ary, $value_ary, $arr, $DHLAccountOpenStep2Data, $DHLAccountOpenStep2);
	}

	/**
	 * 将文件打包压缩
	 * @param  [type] $file_ary 文件数组
	 * @return [type] boolean 是否成功打包压缩文件
	 */
	public static function dhl_compressed_file()
	{
		global $c;

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
		$DHLAccountOpenStep3Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep3"', 'Value');
		$DHLAccountOpenStep3 = str::json_data($DHLAccountOpenStep3Data, 'decode');
		$DHLAccountOpenStep4Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep4"', 'Value');
		$DHLAccountOpenStep4 = str::json_data($DHLAccountOpenStep4Data, 'decode');
		$DHLAccountOpenStep9Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep9"', 'Value');
		$DHLAccountOpenStep9 = str::json_data($DHLAccountOpenStep9Data, 'decode');
		$file_ary = array(
			'Account Application Form.pdf', // 账户申请表
			'Shipper\'s Checklist for Hidden Dangerous Goods.pdf', // 托运人隐藏危险货物清单
			'Personal Information Collection Statement for Customers.pdf', // 客户个人信息收集声明
			'Rate Card.pdf', // 价目表，目前仅有一份，以后会更新
			'Online Account Opening Form in Excel.xls', // Excel中的在线帐户开立表格
			$DHLAccountOpenStep4['PicPath_0'], // 营业执照复印件
			$DHLAccountOpenStep4['PicPath_1'], // 身份证复印件（照片面）
			$DHLAccountOpenStep9['PicPath'], // 已付款截图
		);
		if($DHLAccountOpenStep3['HasCharged']){
			$file_ary[] = 'DG Indemnity Letter.pdf'; // 是否有带电货品？（仅限有带电货品）
		}
		if((int)$DHLAccountOpenStep1['AccountAddress'] == 0){
			// 香港地区开户
			$file_ary[] = 'Known Consignor Declaration of Compliance.pdf'; // 已知发货人合规声明
		}else{
			$file_ary[] = 'Inquiry Form for VAT Invoice_sample output file.xls'; // 增值税发票样品输出文件的查询表(仅限深圳和上海)
			$file_ary[] = $DHLAccountOpenStep4['PicPath_2']; // 身份证复印件（芯片面）(只适用于开户地点深圳和上海)
		}
		setlocale(LC_ALL, 'zh_CN.UTF8');  
		if(!empty($file_ary)){	//开始打包
			$zip = new ZipArchive();
			$zipname = '/u_file/file/' . $c['Number'] . '.zip';
			if (is_file($c['root_path'].$zipname)) {
				file::del_file($zipname);
			}
			if($zip->open($c['root_path'] . $zipname, ZIPARCHIVE::CREATE) === TRUE){
				foreach((array)$file_ary as $path){
					/*
					if(substr_count(stripslashes($path), '/u_file/file/dhl/') && is_file($c['root_path'] . $path)){
						// 图片名称有中文，使用这个
						$zip->addFromString(iconv('utf-8', 'gbk//ignore', basename($path)), file_get_contents($c['root_path'] . $path));
					}else if(is_file($c['root_path'] . '/u_file/file/dhl/' . $path)){
						$zip->addFile($c['root_path'] . '/u_file/file/dhl/' . $path, $path);
					}
					*/
					if(substr_count(stripslashes($path), '/u_file/file/dhl/') && is_file($c['root_path'] . $path)){
						$zip->addFile($c['root_path'] . $path, str_replace('/u_file/file/dhl/', '', $path));
					}else if(is_file($c['root_path'] . '/u_file/file/dhl/' . $path)){
						$zip->addFile($c['root_path'] . '/u_file/file/dhl/' . $path, $path);
					}
				}
				$zip->close();
				return true;
			}
		}
		setlocale(LC_ALL, NULL);  
		unset($c, $DHLAccountOpenStep1Data, $DHLAccountOpenStep1, $DHLAccountOpenStep3Data, $DHLAccountOpenStep3, $DHLAccountOpenStep4Data, $DHLAccountOpenStep4, $file_ary, $zip, $zipname);
		return false;
	}

	/*****************************DHL在线开户 start******************************/


	/*****************************自定义评论 start******************************/
	public static function list_bat_edit(){ //批量修改产品销量和收藏数
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		foreach ((array)$p_select as $v) {
			$v = (int)$v;
			if (!$v) continue;
			$data = array(
				'Sales'			=>	(int)$p_Sales[$v],
				'FavoriteCount'	=>	(int)$p_FavoriteCount[$v],
			);

			db::update('products', "ProId='{$v}'", $data);
		}
		manage::operation_log('批量修改产品销量和收藏数');
		ly200::e_json('', 1);
	}

	public static function products_development(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		if((int)$p_select_data){
			$row1 = db::get_one('products', "ProId='{$p_ProId}'", 'Sales, FavoriteCount, Rating, TotalRating');
			$row2 = db::get_one('products_development', "ProId='{$p_ProId}'");
			$data=array(
				'IsVirtual'					=>	(int)$row2['IsVirtual'],
				'Sales'						=>	(int)((int)$row1['Sales'] > $row2['Sales'] ? $row1['Sales'] : $row2['Sales']),
				'FavoriteCount'				=>	(int)((int)$row1['FavoriteCount'] > $row2['FavoriteCount'] ? $row1['FavoriteCount'] : $row2['FavoriteCount']),
				'DefaultReviewRating'	=>	(int)(ceil($row1['Rating']) > $row2['DefaultReviewRating'] ? ceil($row1['Rating']) : $row2['DefaultReviewRating']),
				'DefaultReviewTotalRating'		=>	(int)((int)$row1['TotalRating'] > $row2['DefaultReviewTotalRating'] ? $row1['TotalRating'] : $row2['DefaultReviewTotalRating']),
			);
			ly200::e_json($data, 1);
			exit;
		}
		$data=array(
			'IsVirtual'					=>	(int)$p_IsVirtual,
			'Sales'						=>	(int)$p_Sales,
			'FavoriteCount'				=>	(int)$p_FavoriteCount,
			'DefaultReviewRating'		=>	(int)$p_DefaultReviewRating,
			'DefaultReviewTotalRating'	=>	(int)$p_DefaultReviewTotalRating,
		);
		db::update('products_development', "ProId='{$p_ProId}'", $data);
		ly200::e_json('', 1);
	}

	public static function review_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		$data=array(
			'ProId'			=>	$p_ProId,
			'CustomerName'	=>	str::random_user(),
			'Content'		=>	$p_Content,
			'Rating'		=>	(int)$p_Rating,
			'Audit'			=>	1,
			'AppDivision'	=>	'-1',
			'AccTime'		=>	((int)strtotime($p_AccTime)+rand(0, 59))
		);
		db::insert('products_review', $data);
		// $count=(int)db::get_row_count('products_review', "ProId='{$p_ProId}' and ReId=0");
		// $rating=(float)db::get_sum('products_review', "ProId='{$p_ProId}' and ReId=0", 'Rating');
		// db::update('products', "ProId='{$p_ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
		ly200::e_json('', 1);
	}

	public static function review_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$RId=(int)$g_RId;
		$resize_ary=array('85x85');
		$review_row=str::str_code(db::get_one('products_review', "RId='$RId'", 'Audit, UserId, ProId, Rating, PicPath_0, PicPath_1, PicPath_2'));
		if($review_row){
			for($i=0; $i<3; $i++){
				$PicPath=$review_row["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					foreach((array)$resize_ary as $v){
						$ext_name=file::get_ext_name($PicPath);
						file::del_file($PicPath.".{$v}.{$ext_name}");
					}
					file::del_file($PicPath);
				}
			}
			$ProId=$review_row['ProId'];
			db::delete('products_review', "RId='{$RId}' or ReId='{$RId}'");
			$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');
			// $count=(int)db::get_row_count('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'));
			// $rating=(float)db::get_sum('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'), 'Rating');
			// db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
			manage::operation_log('删除产品评论');
		}
		ly200::e_json('', 1);
	}
	
	public static function review_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$rid_ary=explode('-', $g_id);
		$resize_ary=array('85x85');
		$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');
		foreach((array)$rid_ary as $v){
			$review_row=str::str_code(db::get_one('products_review', "RId='$v'", 'Audit, UserId, ProId, Rating'));
			if($review_row){
				for($i=0; $i<3; $i++){
					$PicPath=$review_row["PicPath_$i"];
					if(is_file($c['root_path'].$PicPath)){
						foreach((array)$resize_ary as $v2){
							$ext_name=file::get_ext_name($PicPath);
							file::del_file($PicPath.".{$v2}.{$ext_name}");
						}
						file::del_file($PicPath);
					}
				}
				$ProId=$review_row['ProId'];
				db::delete('products_review', "RId='{$v}' or ReId='{$v}'");
				// $count=(int)db::get_row_count('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'));
				// $rating=(float)db::get_sum('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'), 'Rating');
				// db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
			}
		}
		manage::operation_log('批量删除产品评论');
		ly200::e_json('', 1);
	}

	public static function review_reply(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_RId=(int)$p_RId;
		$p_ProId=(int)$p_ProId;
		$p_Audit=(int)$p_Audit;
		$review_row=str::str_code(db::get_one('products_review', "RId='$p_RId'", 'Audit, UserId, CustomerName, Rating'));
		!$review_row && ly200::e_json('', 0);
			
		if($p_ReviewComment){//管理员回复
			$data=array(
				'ProId'			=>	$p_ProId,
				'UserId'		=>	0,
				'ReId'			=>	$p_RId,
				'CustomerName'	=>	'Manager',
				'Content'		=>	$p_ReviewComment,
				'Audit'			=>	1,
				'Ip'			=>	ly200::get_ip(),
				'AccTime'		=>	$c['time'],
			);
			db::insert('products_review', $data);
			$Email=str::str_code(db::get_value('user', "UserId='{$review_row['UserId']}'", 'Email'));
			ly200::sendmail($Email, $review_row['CustomerName'], ly200::get_domain().' administrator reply your comments', ly200::get_domain().' administrator reply your comments');
		}
		manage::operation_log("修改产品评论 产品ID:{$p_ProId} 评论ID:{$p_RId}");
		ly200::e_json('', 1);
	}

	public static function review_reply_audit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_RId=(int)$p_RId;
		$p_Audit=(int)$p_Audit;
		$p_Review=(int)$p_Review;
		$review_row=str::str_code(db::get_one('products_review', "RId='$p_RId'", 'Audit, ProId, UserId, CustomerName, Rating'));
		!$review_row && ly200::e_json('', 0);
		if($p_Review && $review_row['Audit']!=$p_Audit){
			// $w="ProId='{$review_row['ProId']}' and Audit=1 and ReId=0";
			// $count=(int)db::get_row_count('products_review', $w);
			// $rating=(float)db::get_sum('products_review', $w, 'Rating');
			// db::update('products', "ProId='{$review_row['ProId']}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
		}
		
		db::update('products_review', "RId='$p_RId'", array('Audit'=>$p_Audit));

		manage::operation_log("产品评论审核 评论ID:{$p_RId}");
		ly200::e_json('', 1);
	}

	public static function review_import_excel_download(){
		global $c;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		$objPHPExcel=new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
		$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
		$objPHPExcel->getProperties()->setCategory("Test result file");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', $c['manage']['lang_pack']['plugins']['custom_comments']['content'])->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); //$c['manage']['lang_pack']['orders']['oid']
		$objPHPExcel->getActiveSheet()->setCellValue('B1', $c['manage']['lang_pack']['plugins']['custom_comments']['star'])->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('C1', $c['manage']['lang_pack']['global']['time'])->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$ProId = $_GET['ProId'];
		$ProId && $products_row = db::get_one('products', "ProId='{$ProId}'");
		$i=2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'Perfect!');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '5');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, date('Y/m/d H:i', $c['time']));
		//设置列的宽度  
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		
		//设置行的高度
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(24);
		$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(20);//默认行高
		
		$objPHPExcel->getActiveSheet()->setTitle($c['manage']['lang_pack']['plugins']['custom_comments']['ec_title']);
		$objPHPExcel->setActiveSheetIndex(0);
		
		//保存Excel文件
		$ExcelName='review_export_'.str::rand_code();
		$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($c['root_path']."/tmp/{$ExcelName}.xls");
		
		file::down_file("/tmp/{$ExcelName}.xls");
		file::del_file("/tmp/{$ExcelName}.xls");
		unset($c, $objPHPExcel, $ary, $attr_column);
		exit;
	}

	public static function review_import(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;//当前分开数
		$ProId=(int)$p_ProId;//当前分开数
		$p_Worksheet=(int)$p_Worksheet;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		$StatusAry=array();
		!file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json($c['manage']['lang_pack']['plugins']['custom_comments']['no_file']);
		
		$objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
		$sheet=$objPHPExcel->getSheet(0);//工作表0
		$highestRow=$sheet->getHighestRow();//取得总行数 
		$highestColumn=$sheet->getHighestColumn();//取得总列数
		
		//初始化第一阶段
		$Start=0;//开始执行位置
		$page_count=200;//每次分开导入的数量
		$total_pages=ceil(($highestRow-1)/$page_count);
		if($p_Number<$total_pages){//继续执行
			$Start=$page_count*$p_Number;
		}else{
			file::del_file($p_ExcelFile);
			manage::operation_log('批量导入自定义评论');
			ly200::e_json($c['manage']['lang_pack']['plugins']['custom_comments']['upload_bat_succ'], 1);
		}
		//初始化第二阶段
		//内容转换为数组 
		$data=$sheet->toArray();
		$data_ary=array();
		$i=-1;
		foreach($data as $k=>$v){//行
			if($k<1) continue;
			if($Start<=$k && $k<($Start+$page_count)){
				if($v[0]){
					$data_ary[]=$v;
				}
			}elseif($k>=($Start+$page_count)){
				break;
			}
		}
		unset($data);
		//开始导入
		$No = 0;
		$insert_sql = '';
		$update_sql = $proid_ary = array();
		$Number = db::get_value('products', "ProId='{$ProId}'", 'Number');
		foreach((array)$data_ary as $key=>$val){
			if (!$ProId) continue;
			$Name = str::random_user();
			$Content = addslashes($val[0]);
			$Rating = (int)$val[1];
			$AccTime = (int)@strtotime(trim($val[2]))+rand(0, 59);
			//记录数据资料
			if (!db::get_row_count('products_review', "ProId='{$ProId}' and Content='$Content'")) {
				$insert_sql.=",('{$ProId}','{$Name}','{$Content}','{$Rating}','1','-1','{$AccTime}')";
				if (!@in_array($ProId, (array)$proid_ary)) {
					$proid_ary[] = $ProId;
				}
				$Status = 1;
				$Tips = $c['manage']['lang_pack']['plugins']['custom_comments']['upload_succ'];
			} else {
				$Status = 0;
				$Tips = $c['manage']['lang_pack']['plugins']['custom_comments']['upload_fail'];
			}
			$StatusAry[] = array(
				'Number'	=>	$Number,
				'Content'	=>	$Content,
				'Status'	=>	$Status,
				'Tips'		=>	$Tips,
			);
			++$No;
		}
		$insert_sql && db::query("insert into products_review (ProId, CustomerName, Content, Rating, Audit, AppDivision, AccTime) values".trim($insert_sql, ','));
		// foreach ((array)$proid_ary as $v) {
		// 	$ProId = (int)$v;
		// 	$count = (int)db::get_row_count('products_review', "ProId='{$ProId}' and ReId=0");
		// 	$rating = (float)db::get_sum('products_review', "ProId='{$ProId}' and ReId=0", 'Rating');
		// 	db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
		// }
		if($p_Number<$total_pages){//继续执行
			$item=($No+1<$page_count)?($page_count*$p_Number+$No):($page_count*($p_Number+1));
			ly200::e_json(array(($p_Number+1), $StatusAry), 2);
		}
	}
	/*****************************自定义评论 end******************************/
}
?>