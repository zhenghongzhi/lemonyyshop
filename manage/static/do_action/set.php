<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class set_module{
	/***************************** 基础设置 start *****************************/

	public static function config_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ImgPath=array();
		$PicPath=array($p_LogoPath, $p_WatermarkPath, $p_IcoPath);
		foreach((array)$PicPath as $k=>$v){
			$ImgPath[$k]=$v;
		}
		foreach((array)$p_Language as $k=>$v){	//防止非法提交系统未定义的语言
			if(!in_array($v, $c['manage']['web_lang_list'])){
				unset($p_Language[$k]);
			}
		}
		!$p_Language && ly200::e_json('请至少选择一个语言版本');
		!$p_LanguageDefault && ly200::e_json('请选择网站默认语言');
		!$p_ManageLanguage && ly200::e_json('请选择后台语言');
		!in_array($p_LanguageDefault, $p_Language) && $p_LanguageDefault=$p_Language[0];

		//自定义关闭网站
		$CloseWebAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$CloseWebAry['CloseWeb_'.$v]=${'p_CloseWeb_'.$v};
		}
		$CloseWebData=addslashes(str::json_data(str::str_code($CloseWebAry, 'stripslashes')));
		//通告栏
		$NoticeAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$NoticeAry['Notice_'.$v]=${'p_Notice_'.$v};
		}
		$NoticeData=addslashes(str::json_data(str::str_code($NoticeAry, 'stripslashes')));

		//搜索框提示语
		$SearchTipsAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$SearchTipsAry['SearchTips_'.$v]=${'p_SearchTips_'.$v};
		}
		$SearchTipsData=addslashes(str::json_data(str::str_code($SearchTipsAry, 'stripslashes')));

		$data=array(
			//******************基本信息******************
			'WebDisplay'		=>	(int)$p_WebDisplay,

			'IsIP'				=>	$c['FunVersion']>=1?(int)$p_IsIP:0,
			'IsChineseBrowser'	=>	$c['FunVersion']>=1?(int)$p_IsChineseBrowser:0,
			'IsCopy'			=>	(int)$p_IsCopy,
			'IsMobile'			=>	(int)$p_IsMobile,
			'IsCloseWeb'		=>	(int)$p_IsCloseWeb,
			'IsNotice'			=>	(int)$p_IsNotice,
			'CloseWeb'			=>	$CloseWebData,
			'Notice'			=>	$NoticeData,
			//******************会员设置******************
			'UserView'			=>	$c['FunVersion']>=1?(int)$p_UserView:0,
			'UserStatus'		=>	$c['FunVersion']>=1?(int)$p_UserStatus:0,
			'TouristsShopping'	=>	$c['FunVersion']>=1?(int)$p_TouristsShopping:0,
			'AutoRegister'		=>	$c['FunVersion']>=1?(int)$p_AutoRegister:0,
			'UserLogin'			=>	(int)$p_UserLogin,
			//******************购物设置******************
			'Overseas'			=>	$c['FunVersion']>1?(int)$p_Overseas:0,
			'RecentOrders'		=>	(int)$p_RecentOrders,

			'LeftCateOpen'		=>	(int)$p_LeftCateOpen,

			'CartWeight'		=>	(int)$p_CartWeight,
			'AutoCanceled'		=>	(int)$p_AutoCanceled,
			'AutoCanceledDay'	=>	(int)$p_AutoCanceledDay,

			//******************语言设置******************
			'BrowserLanguage'	=>	(int)$p_BrowserLanguage,
			'PromptSteps'		=>	(int)$p_PromptSteps,
			'Language'			=>	implode(',', $p_Language),
			'LanguageDefault'	=>	$p_LanguageDefault,
			'ManageLanguage'	=>	$p_ManageLanguage,
			//******************网站资料******************
			'Skype'				=>	$p_Skype,
			'SearchTips'		=>	$SearchTipsData,
			'ContactUrl'		=>	$p_ContactUrl,

		);

		//开启会员审核，现有会员全改为已审核
		((int)$p_UserStatus && $c['manage']['config']['UserStatus']!=(int)$p_UserStatus) && db::update('user', 1, array('Status'=>1));

		$c['FunVersion']>=2 && $data['Blog']=$p_Blog;
		manage::config_operaction($data, 'global');
		manage::config_operaction(array('code_301'=>(int)$p_code_301), 'http');
		manage::turn_on_language_database_operation($p_LanguageDefault, $p_Language);	//添加新增语言版多语言字段、默认内容
		manage::operation_log('修改网站基本设置');
		ly200::e_json('', 1);
	}
	public static function keep_config_scrolltop(){ //返回的时候滚动条回到原来位置
		global $c;
		$_SESSION['config_scrolltop']=$_POST['scrolltop'];
		ly200::e_json('', 1);
	}
	public static function config_basis_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ImgPath=array();
		$PicPath=array($p_LogoPath, $p_IcoPath);
		foreach((array)$PicPath as $k=>$v){
			$ImgPath[$k]=$v;
		}

		//版权信息
		$CopyRightAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$CopyRightAry['CopyRight_'.$v]=${'p_CopyRight_'.$v};
		}
		$CopyRightData=addslashes(str::json_data(str::str_code($CopyRightAry, 'stripslashes')));

		$data=array(
			//******************基本信息******************
			'SiteName'			=>	$p_SiteName,
			'LogoPath'			=>	$ImgPath[0],
			'IcoPath'			=>	$ImgPath[1],
			//******************网站资料******************
			'AdminEmail'		=>	$p_AdminEmail,
			'CopyRight'			=>	$CopyRightData,
		);

		manage::config_operaction($data, 'global');
		manage::operation_log('修改网站基本设置');
		ly200::e_json('', 1);
	}

	public static function config_seo_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$alone_ary=array('home', 'tuan', 'seckill', 'blog', 'new', 'hot', 'best_deals', 'special_offer');
		$much_ary=array('article'=>array('Title', 'AId'), 'info_category'=>array('Category', 'CateId'), 'info'=>array('Title', 'InfoId'), 'products_category'=>array('Category', 'CateId'), 'products'=>array('Name', 'ProId'));
		if(in_array($p_Type, $alone_ary)){
			$Id=(int)$p_MId;
			if(!$Id){
				db::insert('meta', array('Type'=>$p_Type));
				$Id=db::get_insert_id();
			}
			manage::database_language_operation('meta', "MId='$Id'", array('SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		}else{
			$Id=(int)${'p_'.$much_ary[$p_Type][1]};
			manage::database_language_operation($p_Type=='products'?'products_seo':$p_Type, "{$much_ary[$p_Type][1]}='$Id'", array('SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		}
		manage::operation_log('修改页面标题与标签管理');
		ly200::e_json('', 1);
	}

	public static function seo_edit(){//查询产品相关的关键词
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Type=trim($p_Type);
		//保存其它语言版的信息
		$seo_row=str::str_code(db::get_one('meta', "Type='$Type'"));
		foreach((array)$c['manage']['config']['Language'] as $lang){
			$_POST['SeoKeyword_'.$lang]=addslashes($seo_row['SeoKeyword_'.$lang]);
		}
		//SEO关键词
		$keys_str='';
		if($p_keysOption){
			$keys_ary=array();
			foreach((array)$p_keysOption as $k=>$v){
				if($p_keysCurrent[$k]){
					$keys_ary[]=$p_keysName[$k];
				}
			}
			$keys_str=implode(',', $keys_ary);
		}
		$_POST['SeoKeyword'.$c['manage']['web_lang']]=$keys_str;
		manage::database_language_operation('meta', "Type='$Type'", array('SeoTitle'=>1, 'SeoKeyword'=>2, 'SeoDescription'=>2));
		ly200::e_json('', 1);
	}

	public static function seo_keyword_select(){//查询产品相关的关键词
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Type=trim($p_Type);
		$seo_row=str::str_code(db::get_one('meta', "Type='$Type'"));
		$keyword_ary=array();
		foreach($c['manage']['config']['Language'] as $lang){
			$keys_id_ary=explode(',', $seo_row["SeoKeyword_{$lang}"]);
			$keys_id_ary=array_filter($keys_id_ary);
			$keyword_ary[$lang]=$keys_id_ary;
		}
		ly200::e_json($keyword_ary, 1);
	}

	public static function seo_keyword_edit(){//修改关键词
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Type	= trim($p_Type);
		$data_ary	= array();
		$data		= array();
		foreach((array)$p_Name as $k=>$v){
			foreach((array)$v as $lang=>$v2){
				$data_ary[$lang][]=$v2;
			}
		}
		foreach((array)$data_ary as $k=>$v){
			$data["SeoKeyword_$k"]=@implode(',', $v);
		}
		if($data){
			db::update('meta', "Type='$p_Type'", $data);
		}
		ly200::e_json($data_ary, 1);
	}

	public static function sitemap_create(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		include("{$c['root_path']}/inc/class/sitemap/sitemap.inc.php");
		include("{$c['root_path']}/inc/class/sitemap/config.inc.php");
		include("{$c['root_path']}/inc/class/sitemap/url_factory.inc.php");
		$obj=new Sitemap();

		/********* 生成索引sitemap.xml start *********/
		if(!$_SESSION['Manage']['sitemap']){
			$_SESSION['Manage']['SiteMapMax']=$max=45000; //一个文件最大的数量
			$pro=db::get_row_count('products', '1'.$c['manage']['where']['products']);
			$pro_cate = db::get_row_count('products_category');
			$art = db::get_row_count('article');
			$_SESSION['Manage']['SiteMapPage']=$page=ceil($pro/$max);
			$date=date('Y-m-d', time());
			$xmlHtml='';
			$xmlHtml.='<?xml version="1.0" encoding="UTF-8"?>';
			$xmlHtml.='<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
				foreach((array)$c['manage']['config']['Language'] as $v){
					for($i=1;$i<=$page;$i++){
						$url='/'.str_replace('_', '-', $v).'-sitemap'.$i.'.xml';
						if($page==1) $url='/'.str_replace('_', '-', $v).'-sitemap.xml';
						$xmlHtml.='<sitemap>';
							$xmlHtml.='<loc>'.$obj->_escapeXML(SITE_DOMAIN.$url).'</loc>';
							$xmlHtml.="<lastmod>{$date}</lastmod>";
						$xmlHtml.='</sitemap>';
					}
				}
			$xmlHtml.='</sitemapindex>';
			file::write_file('/', 'sitemap.xml', $xmlHtml);
			unset($xmlHtml);

			/********* 配置要生成sitemap的域名 start *********/
			$_SESSION['Manage']['lang_ary']=$_SESSION['Manage']['website_ary']=$_SESSION['Manage']['sitemap_name_ary']=$_SESSION['Manage']['page_item_ary']=array();
			foreach((array)$c['manage']['config']['Language'] as $v){
				$lang=str_replace('_', '-', $v);
				$website=str_replace(array('://www', '://'), array("://{$lang}", "://{$lang}."), SITE_DOMAIN);
				if($c['manage']['config']['LanguageDefault']==$v) $website=SITE_DOMAIN;
				for($i=1;$i<=$page;$i++){
					$sitemap_name=str_replace('_', '-', $v).'-sitemap'.$i.'.xml';
					if($page==1) $sitemap_name=str_replace('_', '-', $v).'-sitemap.xml';
					$_SESSION['Manage']['lang_ary'][]='_'.$v; //域名列表
					$_SESSION['Manage']['website_ary'][]=$website; //域名列表
					$_SESSION['Manage']['sitemap_name_ary'][]=$sitemap_name; //sitemap名字列表
					$_SESSION['Manage']['page_item_ary'][]=($i-1);//记录每一页的下标
				}
			}
			/********* 配置要生成sitemap的域名 end *********/

			$_SESSION['Manage']['sitemap']=1;
			$_SESSION['Manage']['Percent']=0;
		}
		/********* 生成索引sitemap.xml end *********/
		$Start=(int)$p_Start;
		$WebSiteNum=(int)$p_WebSiteNum;
		$lang=$_SESSION['Manage']['lang_ary'][$WebSiteNum];//当前语言
		$lang || $lang=$c['manage']['web_lang'];
		$do_amain=$_SESSION['Manage']['website_ary'][$WebSiteNum]; //当前域名
		$sitemap_name=$_SESSION['Manage']['sitemap_name_ary'][$WebSiteNum]; //当前域名
		$page_item=$_SESSION['Manage']['page_item_ary'][$WebSiteNum]; //当前域名的第几页
		if(!$do_amain && !$sitemap_name){
			unset($_SESSION['Manage']['Percent'], $_SESSION['Manage']['lang_ary'], $_SESSION['Manage']['website_ary'], $_SESSION['Manage']['sitemap_name_ary'], $_SESSION['Manage']['sitemap'], $_SESSION['Manage']['SiteMapMax'], $_SESSION['Manage']['SiteMapPage'], $_SESSION['Manage']['page_item_ary']);
			manage::operation_log('更新网站地图');
			ly200::e_json(array('Start'=> $Start, 'Percent'=> 100, 'WebSiteNum'=> $WebSiteNum), 3);
		}
		$xmlHtml='';
		if($Start==0){
			$xmlHtml.='<?xml version="1.0" encoding="UTF-8"?>';
			$xmlHtml.='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
		}
		//产品详细页
		$page_count=1000;
		$all=$save_all=db::get_row_count('products', '1'.$c['manage']['where']['products']);
		if($_SESSION['Manage']['SiteMapPage']>1){
			$all = $_SESSION['Manage']['SiteMapMax'];
			if(($_SESSION['Manage']['SiteMapPage']-1)==$page_item){
				$all=$save_all-$_SESSION['Manage']['SiteMapMax']*$page_item;
			}
		}
		if($page_count>$all) $page_count=$all; //总数没有分页数多 分页数调整为总数
		$all_page = ceil($all/$page_count); //总页数
		$Percent=$_SESSION['Manage']['SiteMapPage']/count($_SESSION['Manage']['website_ary'])/$all*$page_count; //当前项目占多少进度
		$limit_start=$page_item*$_SESSION['Manage']['SiteMapMax']+$Start*$page_count;
		$row=str::str_code(db::get_limit('products', '1'.$c['manage']['where']['products'], "ProId,PageUrl,Name{$lang}", $c['my_order'].'ProId desc', $limit_start, $page_count));
		foreach($row as $v){
			$xmlHtml.='<url>';
				$xmlHtml.='<loc>'.$obj->_escapeXML($do_amain.ly200::get_url($v, 'products', $lang)).'</loc>';
				$xmlHtml.='<changefreq>weekly</changefreq>';
			$xmlHtml.='</url>';
		}

		if($Start==0){
			@unlink($c['root_path'].$sitemap_name);
		}
		if($Start>=$all_page){
			$Start=0;
		}else{
			$Start++;
		}
		$myfile = fopen($c['root_path'].$sitemap_name, 'a') or die("Unable to open file!");
		fwrite($myfile, $xmlHtml);
		fclose($myfile);
		if($Start>=$all_page){ // 最后一页 把结尾拼上去
			$xmlHtml='';
			if(($_SESSION['Manage']['SiteMapPage']-1)==$page_item){ //最后一页加上其他的链接
				//首页
				$xmlHtml.='<url>';
					$xmlHtml.='<loc>'.$obj->_escapeXML($do_amain).'</loc>';
					$xmlHtml.='<changefreq>weekly</changefreq>';
				$xmlHtml.='</url>';
				//产品列表页
				$row=str::str_code(db::get_all('products_category', '1', "CateId,Category{$lang}", $c['my_order'].'CateId asc'));
				foreach($row as $v){
					$xmlHtml.='<url>';
						$xmlHtml.='<loc>'.$obj->_escapeXML($do_amain.ly200::get_url($v, 'products_category', $lang)).'</loc>';
						$xmlHtml.='<changefreq>weekly</changefreq>';
					$xmlHtml.='</url>';
				}
				//信息页
				$row=str::str_code(db::get_all('article', '1', "AId,PageUrl,Url,Title{$lang}", $c['my_order'].'AId asc'));
				foreach($row as $v){
					if($v['Url']) continue;
					$url=$v['Url']?$v['Url']:$do_amain.ly200::get_url($v, 'article', $lang);
					$xmlHtml.='<url>';
						$xmlHtml.='<loc>'.$obj->_escapeXML($url).'</loc>';
						$xmlHtml.='<changefreq>weekly</changefreq>';
					$xmlHtml.='</url>';
				}
			}
			$xmlHtml.='</urlset>';
			$myfile = fopen($c['root_path'].$sitemap_name, 'a') or die("Unable to open file!");
			fwrite($myfile, $xmlHtml);
			fclose($myfile);
			$Percent=($_SESSION['Manage']['Percent']*100)>=100 ? 99 : (int)($_SESSION['Manage']['Percent']*100);
			ly200::e_json(array('Start'=> $Start, 'Percent'=> $Percent, 'WebSiteNum'=> ($WebSiteNum+1)), 2);
		}else{
			if(($_SESSION['Manage']['SiteMapPage']-1)==$page_item){
				$_SESSION['Manage']['Percent']+=$Percent;
			}
			$Percent=($_SESSION['Manage']['Percent']*100)>=100 ? 99 : (int)($_SESSION['Manage']['Percent']*100);
			ly200::e_json(array('Start'=> $Start, 'Percent'=> $Percent, 'WebSiteNum'=> $WebSiteNum), 2);
		}
	}

	public static function config_shopping_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		//订单短信通知
		$SmsAry=array(0, 0);
		foreach((array)$p_OrdersSmsStatus as $v){
			$SmsAry[$v]=1;
		}
		$SmsData=addslashes(str::json_data(str::str_code($SmsAry, 'stripslashes')));
		//到货信息
		$ArrivalInfoAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$ArrivalInfoAry['ArrivalInfo_'.$v]=${'p_ArrivalInfo_'.$v};
		}
		$ArrivalInfoData=addslashes(str::json_data(str::str_code($ArrivalInfoAry, 'stripslashes')));
		$data=array(
			'LowConsumption'	=>	(int)$p_LowConsumption,
			'LowPrice'			=>	(float)$p_LowPrice,
			'LessStock'			=>	(int)$p_LessStock,
			'OrdersSms'			=>	$p_OrdersSms,
			'OrdersSmsStatus'	=>	$SmsData,
			'ArrivalInfo'		=>	$ArrivalInfoData
		);

		manage::config_operaction($data, 'global');
		manage::operation_log('修改网站购物设置');
		ly200::e_json('', 1);
	}

	/****** 会员设置 start ******/
	public static function user_level_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$LId=(int)$p_LId;
		$p_PicPath=$p_PicPath;

		$p_Discount=(int)$p_Discount;
		$p_Discount>100 && $p_Discount=100;
		$data=array(
			'PicPath'	=>	$p_PicPath,
			'Discount'	=>	$p_Discount,
			'FullPrice'	=>	(float)$p_FullPrice,
		);
		if($p_LId){
			db::update('user_level', "LId='$LId'", $data);
			manage::operation_log('修改会员等级');
		}else{
			db::insert('user_level', $data);
			$LId=db::get_insert_id();
			manage::operation_log('添加会员等级');
		}
		manage::database_language_operation('user_level', "LId='$LId'", array('Name'=>0));
		ly200::e_json('', 1);
	}

	public static function user_level_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$LId=(int)$p_LId;
		$IsUsed=(int)$p_IsUsed;
		if(!(int)$IsUsed && db::get_row_count('user', "Level='$LId'")){
			ly200::e_json(manage::get_language('user.level.close_tips'), 0);
		}
		$data=array(
			'IsUsed'	=>	$IsUsed,
		);
		db::update('user_level', "LId='$LId'", $data);
		manage::operation_log(($IsUsed ? '开启' : '关闭').'会员等级-'.$LId);
		$msg=($IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function user_level_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$LId=(int)$g_LId;
		$row=str::str_code(db::get_one('user_level', "LId='$LId'"));
		if($row){
			if(db::get_row_count('user', "Level='$LId'")){
				ly200::e_json(manage::get_language('user.level.close_tips'), 0);
			}
			//if(is_file($c['root_path'].$row['PicPath'])) file::del_file($row['PicPath']);
			file::photo_check_and_delete($row['PicPath']);
			db::delete('user_level', "LId='$LId'");
			manage::operation_log('删除会员等级');
		}
		ly200::e_json('', 1);
	}

	public static function user_reg_set(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_field=$g_field;
		$g_status=(int)$g_status;
		if(strpos($g_field, 'NotNull')){
			$field=str_replace('NotNull', '', $g_field);
			$key=1;
		}else{
			$field=$g_field;
			$key=0;
		}
		$RegSet=db::get_value('config', 'GroupId="user" and Variable="RegSet"', 'Value');
		if($RegSet){
			$reg_ary=str::json_data($RegSet, 'decode');
		}else{
			$reg_ary=array();
			foreach($c['manage']['user_reg_field'] as $k=>$v){
				$reg_ary[$k]=$v?array(0, 0):array(1, 1);
			}
		}
		$reg_ary[$field][$key]=$g_status?0:1;
		if(!$reg_ary[$field][0]) $reg_ary[$field][1]=0;
		$RegSet=addslashes(str::json_data(str::str_code($reg_ary, 'stripslashes')));
		db::update('config', 'GroupId="user" and Variable="RegSet"', array('Value'=>$RegSet));
		manage::operation_log('修改固定注册事项');
		$msg=($g_status?$c['manage']['lang_pack']['msg']['close_success']:$c['manage']['lang_pack']['msg']['open_success']);
		ly200::e_json($msg, 1);
	}

	public static function user_reg_set_edit(){
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_SetId=(int)$p_SetId;
		$p_TypeId=(int)$p_TypeId;
		$data=array('TypeId'=>$p_TypeId);
		if($p_SetId){
			db::update('user_reg_set', "SetId={$p_SetId}", $data);
			manage::operation_log('修改注册事项');
		}else{
			db::insert('user_reg_set', $data);
			$p_SetId=db::get_insert_id();
			manage::operation_log('添加注册事项');
		}
		manage::database_language_operation('user_reg_set', "SetId={$p_SetId}", array('Name'=>0, 'Option'=>3));
		ly200::e_json('', 1);
	}

	public static function user_reg_set_del(){
		$SetId=(int)$_GET['SetId'];
		db::delete('user_reg_set', "SetId={$SetId}");
		manage::operation_log('删除注册事项');
		ly200::e_json('', 1);
	}

	/****** 会员设置 end ******/

 	public static function config_watermark_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array(
			//******************水印设置******************
			'IsThumbnail'		=>	(int)$p_IsThumbnail,
			'Alpha'				=>	$p_Alpha,
			'WatermarkPath'		=>	$p_WatermarkPath,
			'WaterPosition'		=>	$p_WaterPosition,
			'UpdateWaterStatus'	=>	1, //改了水印 显示更新
			);
		if(db::get_row_count('config', 'GroupId="Water" and Variable="Number"')){
			db::update('config', 'GroupId="Water" and Variable="Number"', array('Value'=>'0'));
		}else{
			db::insert('config', array('GroupId'=>'Water', 'Variable'=>'Number', 'Value'=>'0'));
		}
		manage::config_operaction($data, 'global');
		manage::operation_log('修改水印设置');
		ly200::e_json('', 1);
	}

	public static function config_watermark_update(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Start=(int)$p_Start;//当前分页数
		$IsColor=(int)$p_IsColor; // 更新颜色图片
		if($IsColor){
			$where='PicPath_0 !=""';
			$prod_count=db::get_row_count('products_color', $where);//产品总数
		}else{
			$where='1';
			$prod_count=db::get_row_count('products', $where);//产品总数
		}
		//初始化
		$Start=0;//开始执行位置
		$page_count=10;//每次分开更新的数量
		$total_pages=ceil($prod_count/$page_count);
		if($p_Start<$total_pages){//继续执行
			$Start=$page_count*$p_Start;
		}else{
			db::update('config', 'GroupId="Water" and Variable="Number"', array('Value'=>'0'));
			if($IsColor){
				db::update('config', 'GroupId="global" and Variable="UpdateWaterStatus"', array('Value'=>'0'));
				db::update('config', 'GroupId="Water" and Variable="IsColor"', array('Value'=>'0'));
				manage::operation_log('产品水印颜色图片批量更新');
				ly200::e_json('', 3);
			}else{
				db::update('config', 'GroupId="Water" and Variable="IsColor"', array('Value'=>'1'));  //处理完产品 将状态改成处理颜色图片
				manage::operation_log('产品水印图片批量更新');
				ly200::e_json(array('Start'=>0, 'Percent'=>50, 'IsColor'=>1), 2);
			}
		}

		$table=$IsColor ? 'products_color' : 'products';
		$foreach_ary=db::get_limit($table, $where, 'ProId,PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9', 'ProId desc', $Start, $page_count);


		//图片储存位置
		$resize_ary=$c['manage']['resize_ary']['products'];
		$save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($save_dir);
		//开始更新
		$No=0;
		foreach((array)$foreach_ary as $ProId=>$obj){
			foreach((array)$obj as $key=>$val){	//产品主图片
				if($key=='ProId') continue;
				if(!is_file($c['root_path'].$val)) continue;
				//图片上传
				$water_ary=array($val);
				$ext_name=file::get_ext_name($val);
				@copy($c['root_path'].$val.".default.{$ext_name}", $c['root_path'].$val);//覆盖大图
				if($c['manage']['config']['IsThumbnail']){//缩略图加水印
					img::img_add_watermark($val);
					$water_ary=array();
				}
				foreach((array)$resize_ary as $v2){
					if($v2=='default') continue;
					$size_w_h=explode('x', $v2);
					$resize_img=$val;
					$resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
				}
				foreach((array)$water_ary as $v2){
					img::img_add_watermark($v2);
				}
			}
			++$No;
		}
		if($p_Start<$total_pages){//继续执行
			db::update('config', 'GroupId="Water" and Variable="Number"', array('Value'=>$p_Start));
			$item=($No<$page_count)?($page_count*$p_Start+$No):($page_count*($p_Start+1));
			$Percent=(int)($item/$prod_count*50);
			$IsColor && $Percent=50+(int)($item/$prod_count*50); //产品图片和颜色图片各占50%
			ly200::e_json(array('Start'=>($p_Start+1), 'Percent'=>$Percent, 'IsColor'=>$IsColor), 2);
		}
		ly200::e_json('', 1);
	}


	public static function config_share_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		$ShareMenu=array();
		foreach((array)$c['share'] as $v){
			$ShareMenu[$v]=${'p_Share'.$v};
		}
		$p_tax_code_type && $ShareMenu[$p_tax_code_type] = $p_Add;
		$ShareMenuData=addslashes(str::json_data(str::str_code($ShareMenu, 'stripslashes')));
		$data['ShareMenu']=$ShareMenuData;
		manage::config_operaction($data, 'global');
		manage::operation_log('修改分享设置');
		ly200::e_json('', 1);
	}

	public static function config_company_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$ImgPath=$p_LogoPath;
		$data=array(
			'LogoPath'	=>	$ImgPath,
			'Compeny'	=>	$p_Compeny,
			'Address'	=>	$p_Address,
			'Email'		=>	$p_Email,
			'Telephone'	=>	$p_Telephone,
			'Fax'		=>	$p_Fax,
			'City'		=>	$p_City,
			'State'		=>	$p_State,
            'ZipCode'	=>	$p_ZipCode,
            'xieyi'	    =>	$p_xieyi,
            'xieyi_fa'	    =>	$p_xieyi_fa,
		);
		manage::config_operaction($data, 'print');
		manage::operation_log('修改订单打印设置');
		// 更新mailchimp列表
		mailchimp::update_user_list();
		ly200::e_json('', 1);
	}
	/***************************** 基础设置 end *****************************/

	public static function language_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$FlagAry=str::json_data(htmlspecialchars_decode($c['manage']['config']['LanguageFlag']), 'decode');
		$FlagAry[$p_Language]=$p_FlagPath;
		$FlagData=addslashes(str::json_data(str::str_code($FlagAry, 'stripslashes')));
		$CurrencyAry=str::json_data(htmlspecialchars_decode($c['manage']['config']['LanguageCurrency']), 'decode');
		$CurrencyAry[$p_Language]=$p_Currency;
		$CurrencyData=addslashes(str::json_data(str::str_code($CurrencyAry, 'stripslashes')));
		$data=array(
			'LanguageFlag'		=>	$FlagData,
			'LanguageCurrency'	=>	$CurrencyData
		);
		if($p_LanguageDefault){
			$data['LanguageDefault']=$p_Language;
			manage::turn_on_language_database_operation($p_LanguageDefault, array($p_Language));//添加新增语言版多语言字段、默认内容
		}

		manage::config_operaction($data, 'global');
		manage::operation_log('修改语言设置');
		ly200::e_json('', 1);
	}

	public static function language_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Language=$p_Language;
		$IsUsed=(int)$p_IsUsed;
		$language_list=$c['manage']['config']['Language'];
		$LanguageDefault=$c['manage']['config']['LanguageDefault'];
		if($IsUsed){
			$language_list[]=$Language;
		}else{
			$language_list=@array_diff($language_list, array($Language));
		}
		!$language_list && ly200::e_json($c['manage']['lang_pack']['set']['config']['need_lang'], 0);
		!in_array($LanguageDefault, $language_list) && $LanguageDefault=$language_list[0];
		!$LanguageDefault && ly200::e_json($c['manage']['lang_pack']['set']['config']['df_lang'], 0);
		$language_list=@array_unique($language_list); //去除重复值
		$data=array(
			'Language'			=>	@implode(',', $language_list),
			'LanguageDefault'	=>	$LanguageDefault,
		);
		manage::config_operaction($data, 'global');
		manage::turn_on_language_database_operation($LanguageDefault, array($Language));//添加新增语言版多语言字段、默认内容
		manage::operation_log(($IsUsed ? '开启' : '关闭').'语言-'.$Language);
		$msg=($IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function manage_language_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$IsUsed=(int)$p_IsUsed;
		$Language=$p_Language;
		$data=array(
			'ManageLanguage'	=>	$IsUsed && $Language ? $Language : 'zh-cn',
		);
		manage::config_operaction($data, 'global');
		manage::operation_log('设置后台语言-'.$data['ManageLanguage']);
		$msg=($IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function config_switchery(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$IsUsed=(int)$p_IsUsed;
		$config=$p_config;
		if(!db::get_row_count('config', "GroupId='global' and Variable='{$config}'")){
			db::insert('config', array('GroupId'=>'global', 'Variable'=>$config, 'Value'=>''));
		}
		!$config && ly200::e_json($c['manage']['lang_pack']['set']['config']['no_find'], 0);
		$data=array(
			$config	=>	$IsUsed,
		);
		manage::config_operaction($data, 'global');
		manage::operation_log(($IsUsed ? '开启' : '关闭').'基础设置-'.$config);
		$msg=($IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function config_email_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array(
			'FromEmail'		=>	$p_FromEmail,
			'FromName'		=>	$p_FromName,
			'SmtpHost'		=>	$p_SmtpHost,
			'SmtpPort'		=>	(int)$p_SmtpPort?(int)$p_SmtpPort:25,
			'SmtpUserName'	=>	$p_SmtpUserName,
			'SmtpPassword'	=>	$p_SmtpPassword
		);
		$json_data=str::json_data($data);
		manage::config_operaction(array('config'=>$json_data), 'email');
		//底部签名内容
		$BottomContentAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$BottomContentAry['BottomContent_'.$v]=${'p_BottomContent_'.$v};
		}
		$BottomContentData=addslashes(str::json_data(str::str_code($BottomContentAry, 'stripslashes')));
		manage::config_operaction(array('bottom'=>$BottomContentData), 'email');
		manage::operation_log('编辑邮箱设置');
		ly200::e_json('', 1);
	}

	public static function config_email_verification(){
		global $c;
		$email_config_row=str::json_data(db::get_value('config', 'GroupId="email" and Variable="config"', 'Value'), 'decode');
		if (!ly200::smtpconnect($email_config_row['SmtpHost'], $email_config_row['SmtpUserName'], $email_config_row['SmtpPassword'], (int)$email_config_row['SmtpPort'])) {
			ly200::e_json(array($c['manage']['lang_pack']['set']['email']['ver_fail'], $c['manage']['lang_pack']['set']['email']['error']), 0);
		}
		ly200::e_json(array($c['manage']['lang_pack']['set']['email']['ver_succ']), 1);
	}

	public static function config_email_templete_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$template=$p_template;
		$SId=(int)db::get_value('system_email_tpl', "Template='{$template}'", 'SId');
		foreach($c['manage']['config']['Language'] as $k=>$v){//加上域名
			$_POST['Content_'.$v]=preg_replace("~([\"|'|(|=])/u_file/~i", '$1'.ly200::get_domain(1).'/u_file/', $_POST['Content_'.$v]);
		}
		manage::database_language_operation('system_email_tpl', "SId='$SId'", array('Title'=>1, 'Content'=>3));
		manage::operation_log('编辑邮件设置：'.$template);
		ly200::e_json('', 1);
	}

	public static function config_tourists_used_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_IsUsed=(int)$p_IsUsed;
		manage::config_operaction(array('TouristsShopping'=>$p_IsUsed), 'global');
		$msg=($p_IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function config_review_used_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_IsUsed=(int)$p_IsUsed;
		$set_ary=str::json_data(db::get_value('config', 'GroupId="products_show" and Variable="review"', 'Value'), 'decode');
		$set_ary[$p_type]=($p_IsUsed==1?1:2);
		$set_ary=addslashes(str::json_data(str::str_code($set_ary, 'stripslashes')));
		$data=array('review'=>$set_ary);
		manage::config_operaction($data, 'products_show');
		$msg=($p_IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function config_email_used_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$template=$p_template;
		$p_IsUsed=(int)$p_IsUsed;
		$SId=(int)db::get_value('system_email_tpl', "Template='{$template}'", 'SId');
		db::update('system_email_tpl', "SId='$SId'", array('IsUsed'=>$p_IsUsed));
		$msg=($p_IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function exchange_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CId=(int)$p_CId;
		$p_ExchangeRate=sprintf('%.4f', $p_ExchangeRate);
		$p_IsUsed=(int)$p_IsUsed;
		$FlagPath=$p_FlagPath;
		$data=array(
			'Currency'		=>	$p_Currency,
			'Symbol'		=>	$p_Symbol,
			'ExchangeRate'	=>	$p_ExchangeRate,
			'FlagPath'		=>	$FlagPath,
			'IsDefault'		=>	(int)$p_IsDefault,
			'ManageDefault'	=>	(int)$p_ManageDefault,
		);
		if($CId){
			$rows=str::str_code(db::get_one('currency', "CId='{$CId}'"));

			if($data['IsDefault']==1 && !$rows['IsDefault']){//默认前台货币
				$logs='修改货币默认：'.$rows['Currency'];
				db::update('currency', '1', array('IsDefault'=>0));
				db::update('currency', "CId='{$CId}'", array('IsDefault'=>1, 'IsUsed'=>1));
				manage::operation_log($logs);
			}
			if($data['ManageDefault']==1 && !$rows['ManageDefault']){//默认后台货币
				db::query("update currency set Rate=(100/((100/ExchangeRate/100)*{$p_ExchangeRate})/100), ManageDefault=0 where 1");
				$logs='修改货币后台默认：'.$rows['Currency'];
				db::update('currency', "CId='{$CId}'", array('ManageDefault'=>1, 'Rate'=>1, 'IsUsed'=>1));
				manage::operation_log($logs);
			}

			if($data['IsDefault']==0 && $rows['IsDefault']==1){//默认前台货币
				$data['IsDefault']=1;
			}
			if($data['ManageDefault']==0 && $rows['ManageDefault']==1){//默认后台货币
				$data['ManageDefault']=1;
			}

			$rows=str::str_code(db::get_one('currency', "CId='{$CId}'"));
			if((int)$rows['IsDefault']==1 || (int)$rows['ManageDefault']==1){
				$data['IsUsed']=1;
			}else{
				$data['Rate']=(float)$p_ExchangeRate;
			}
			if($rows['Currency']=='USD') $data['ExchangeRate']='1.0000';//固定美元对美元默认汇率
			$logs='修改货币：'.$rows['Currency'];
			db::update('currency', "CId='{$CId}'", $data);

			if($rows['ExchangeRate']!=$p_ExchangeRate){//更新其他现用汇率
				$currency_row=db::get_one('currency', 'ManageDefault=1');
				$rate_val=100*(100/(100*(float)$currency_row['ExchangeRate']));
				db::query("update currency set Rate=(ExchangeRate*$rate_val)/100 where ManageDefault=0");
			}
		}else{
			$currency_row=db::get_one('currency', 'ManageDefault=1');
			$currency_row || $currency_row['ExchangeRate']=1;
			$rate_val=100*(100/(100*(float)$currency_row['ExchangeRate']));
			$data['Rate']=($p_ExchangeRate*$rate_val)/100;
			$logs='添加货币：'.$p_Currency;
			db::insert('currency', $data);
			$CId=db::get_insert_id();
		}
		if($data['ManageDefault']==1){//即时更新默认后台货币
			$row=db::get_one('currency', "IsUsed=1 and ManageDefault=1");
			$_SESSION['Manage']['Currency']=$row;//后台调用
			$_SESSION['ManageCurrency']=$row;//前台调用
		}
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function exchange_switch(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_CId=(int)$g_CId;
		$g_Type=(int)$g_Type;
		$currency_row=db::get_one('currency', "CId='{$g_CId}'");
		if($g_Type==1){//默认前台货币
			if(!$currency_row['IsDefault']){
				$logs='修改货币默认：'.$currency_row['Currency'];
				db::update('currency', '1', array('IsDefault'=>0));
				db::update('currency', "CId='{$g_CId}'", array('IsDefault'=>1, 'IsUsed'=>1));
				manage::operation_log($logs);
				ly200::e_json('', 1);
			}else{
				ly200::e_json('');
			}
		}elseif($g_Type==2){//默认后台货币
			if(!$currency_row['ManageDefault']){
				db::query("update currency set Rate=(100/((100/ExchangeRate/100)*{$currency_row['ExchangeRate']})/100), ManageDefault=0 where 1");
				$logs='修改货币后台默认：'.$currency_row['Currency'];
				db::update('currency', "CId='{$g_CId}'", array('ManageDefault'=>1, 'Rate'=>1, 'IsUsed'=>1));
				manage::operation_log($logs);
				ly200::e_json('', 1);
			}else{
				ly200::e_json('');
			}
		}else{//启用货币
			$data=array();
			if($currency_row['IsUsed']){
				$data['IsUsed']=0;
			}else{
				$data['IsUsed']=1;
			}
			$logs=($data['IsUsed']?'启用':'关闭').'货币：'.$currency_row['Currency'];
			db::update('currency', "CId='{$g_CId}'", $data);
			manage::operation_log($logs);
			ly200::e_json('', 1);
		}
	}

	public static function exchange_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CId=(int)$g_CId;
		$currency_row=db::get_one('currency', "CId='{$CId}'");
		$logs='删除货币：'.$currency_row['Currency'];
		db::delete('currency', "CId='{$CId}'");
		if((int)$currency_row['ManageDefault']){
			$row=db::get_one('currency', 1, 'CId');
			db::update('currency', "CId='{$row['CId']}'", array('IsDefault'=>1));
		}
		if((int)$currency_row['ManageDefault']){
			$row=db::get_one('currency', 1, 'CId');
			db::update('currency', "CId='{$row['CId']}'", array('ManageDefault'=>1));
		}
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function newexchange_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $CId=(int)$p_CId;
        $data=array(
            'irrmoney'		=>	$p_irrmoney,
            'currency'		=>	$p_currency,
            'money'	        =>	$p_money,
            'fuhao'		    =>	$p_fuhao,
            'isuse'		    =>	$p_isuse,
            'sort'	        =>	$p_sort,
            'updatetime'	=>	$p_updatetime,
        );
        $logdata = array(
            'currency'		=>	$p_currency,
            'money'		    =>	$p_money,
            'irrmoney'		=>	$p_irrmoney,
            'time'		    =>	$p_updatetime,
        );
        if ($CId){
            $rows=str::str_code(db::get_one('exchange', "id='{$CId}'"));
            $logs='修改货币：'.$row['currency'];
            db::update('exchange', "id='{$CId}'", $data);

            $logdata['eid'] = $CId;
            $logdata['addtime'] = time();
            db::insert('exchange_log', $logdata);
        }else{
            $data['addtime'] = time();
            $logs='添加货币：'.$p_currency;
            db::insert('exchange', $data);
            $CId=db::get_insert_id();

            $logdata['eid'] = $CId;
            $logdata['addtime'] = time();
            db::insert('exchange_log', $logdata);
        }
        manage::operation_log($logs);
        ly200::e_json('', 1);
    }

	public static function oauth_edit(){
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_SId=(int)$p_SId;
		$json_ary=array();

		foreach($p_Value as $Key=>$Val){
			$json_ary[$Key]['IsUsed']=(int)$p_IsUsed[$Key];//开启
			$error=0;
			$name=$p_Name[$Key];
			$value=$p_Value[$Key];
			for($i=0, $len=count($value); $i<$len; $i++){
				if(str::str_str($value[$i], array("'", '"', '&', '<', '>', '&quot;', '&#039;', '&amp;', '&lt;', '&gt;'))){
					$value[$i]='';
				}
				$json_ary[$Key]['Data'][$name[$i]]=$value[$i];
				!$value[$i] && $error=1;//其中一项内容为空
			}
			$error==1 && $json_ary[$Key]['IsUsed']=0;
		}
		$json_data=addslashes(str::json_data(str::str_code($json_ary, 'stripslashes')));
		$data=array(
			'LogoPath'	=>	$p_LogoPath,
			//'IsUsed'	=>	(int)$p_IsUsed,
			'Data'		=>	$json_data,
		);
		db::update('sign_in', "SId='$p_SId'", $data);
		ly200::e_json('', 1);
	}

	public static function payment_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_PId		= (int)$p_PId;
		$p_MyOrder	= (int)$p_MyOrder;
		$LogoPath	= $p_LogoPath;
		$MinPrice	= $p_MinPrice;
		$MaxPrice	= $p_MaxPrice;
		$MaxPrice<$MinPrice && $MaxPrice=0;
		$payment_row= str::str_code(db::get_one('payment', "PId='$p_PId'"));
		$IsNewData	= ($c['NewFunVersion']>=4 && ($payment_row['Method']=='Paypal' || $payment_row['Method']=='Excheckout')?1:0);//新用户版本
		$json_ary	= array();
		for($i=0, $len=count($p_Value); $i<$len; $i++){
			$Value=@trim($p_Value[$i]);
			if($IsNewData==1 && strlen($Value)<50){//新版Paypal的“ClientId”和“ClientSecret”，需要50个字符以上
				ly200::e_json($c['manage']['lang_pack']['set']['payment']['error']['account'], 0);
			}
			$json_ary[$p_Name[$i]]=$Value;
		}
		$json_data=addslashes(str::json_data(str::str_code($json_ary, 'stripslashes')));
		$data=array(
			'LogoPath'		=>	$LogoPath,
			'IsGet'			=>	1,
			'IsCreditCard'	=>	(int)$p_IsCreditCard,
			'MinPrice'		=>	$MinPrice,
			'MaxPrice'		=>	$MaxPrice,
			'AdditionalFee'	=>	(float)$p_AdditionalFee,
			'AffixPrice'	=>	(float)$p_AffixPrice,
			'UpdateTime'	=>	$c['time']
		);
		if($IsNewData==1){//新用户版本
			$data['NewAttribute']=$json_data;
		}else{//其他版本
			$data['Attribute']=$json_data;
		}
		//判断新用户已经修改支付方式
		manage::config_operaction(array('payment'=>1), 'guide_pages');
		db::update('payment', "PId='$p_PId'", $data);
		manage::database_language_operation('payment', "PId='$p_PId'", array('Name'=>0, 'Description'=>3));
		manage::operation_log('编辑支付设置');
		ly200::e_json('', 1);
	}

	public static function payment_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_PId=(int)$p_PId;
		$p_IsUsed=(int)$p_IsUsed;
		db::update('payment',"PId='{$p_PId}'",array('IsUsed'=>$p_IsUsed));
		manage::operation_log(($p_IsUsed ? '开启' : '关闭').'支付设置-'.$p_PId);
		ly200::e_json('', 1);
	}

	public static function payment_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_PId=(int)$g_PId;
		db::update('payment',"PId='{$g_PId}'",array('IsGet'=>0,'IsUsed'=>0));
		manage::operation_log('删除支付设置-'.$g_PId);
		ly200::e_json('', 1);
	}

	public static function payment_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `payment` SET `MyOrder` = CASE `PId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `PId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('支付设置排序');
		ly200::e_json('', 1);
	}

	public static function country_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CId=(int)$p_CId;
		$p_IsUsed=(int)$p_IsUsed;
		$p_IsHot=(int)$p_IsHot;

		$country_ary=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$country_ary[$v]=${'p_Country_'.$v};
			if($v=='en') $p_Country=${'p_Country_'.$v};
		}
		if(!in_array('en', $c['manage']['config']['Language'])){//没有英文版
			$p_Country=${'p_Country_'.$c['manage']['config']['LanguageDefault']};//默认语言
		}
		$CountryData=addslashes(str::json_data(str::str_code($country_ary, 'stripslashes')));

		$data=array(
			'Acronym'	=>	strtoupper($p_Acronym),
			'Code'		=>	$p_Code,
			'Currency'	=>	(int)$p_Currency,
			'FlagPath'	=>	$p_FlagPath,
			'IsDefault'	=>	$p_IsUsed&&$p_IsHot?(int)$p_IsDefault:0,
			'HasState'	=>	$p_IsUsed?(int)$p_HasState:$p_IsUsed
		);
		(int)$p_Continent && $data['Continent']=$p_Continent;
		if(!$CId || $CId>240){
			$data['Country']=$p_Country;
			$data['CountryData']=$CountryData;
		}
		(int)$data['IsDefault'] && db::update('country', '1', array('IsDefault'=>0));
		if($CId){
			$logs='修改国家信息：'.str::str_code(db::get_value('country', "CId='{$CId}'", 'Country'));
			db::update('country', "CId='{$CId}'", $data);

			$default_count=(int)db::get_row_count('country', "IsDefault=1");
			if(!$default_count){//突然关闭整个默认国家，或者关闭当前国家的默认选项，防止没有默认国家
				db::update('country', "CId='".($CId==1?2:1)."'", array('IsUsed'=>1, 'IsHot'=>1, 'IsDefault'=>1));
			}
		}else{
			$logs='添加国家：'.$p_Country;
			db::insert('country', $data);
		}
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function country_switch(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_CId=(int)$g_CId;
		$g_Type=(int)$g_Type;
		$g_Check=(int)$g_Check;
		$country_row=db::get_one('country', "CId='{$g_CId}'");
		if($g_Type==0){//开启国家
			$logs=($g_Check?'开启':'关闭').'国家：'.$country_row['Country'];
			if($g_Check==1){//开启
				$data=array('IsUsed'=>1);
			}else{//关闭
				$data=array('IsUsed'=>0, 'IsHot'=>0);
			}
			db::update('country', "CId='{$g_CId}'", $data);
		}else{//开启热门国家
			$logs=($g_Check?'开启':'关闭').'热门国家：'.$country_row['Country'];
			if($g_Check==1){//开启
				$data=array('IsHot'=>1, 'IsUsed'=>1);
			}else{//关闭
				$data=array('IsHot'=>0);
			}
			db::update('country', "CId='{$g_CId}'", $data);
		}
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function country_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CId=(int)$g_CId;
		$logs='删除国家：'.str::str_code(db::get_value('country', "CId='{$CId}'", 'Country'));
		db::delete('country_states', "CId='{$CId}'");
		db::delete('country', "CId='{$CId}'");
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function country_used_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_group_cid && ly200::e_json('');
		$g_used=(int)$g_used;
		$bat_where="CId in(".str_replace('-',',',$g_group_cid).")";
		db::update('country', $bat_where, array('IsUsed'=>$g_used));
		manage::operation_log($g_used?'批量开启国家':'批量关闭国家');
		ly200::e_json('', 1);
	}

	public static function country_states_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CId=(int)$g_CId;
		$SId=(int)$g_SId;
		$rows=str::str_code(db::get_one('country_states s left join country c on s.CId=c.CId', "s.SId='{$SId}'", 'c.Country, s.States'));
		$logs="删除{$rows['Country']}省份：（{$rows['States']}）";
		db::delete('country_states', "CId='{$CId}' and SId='{$SId}'");
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function country_states_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		foreach($sort_order as $v){
			db::update('country_states', "SId='$v'", array('MyOrder'=>$order++));
		}
		manage::operation_log('省份排序');
		ly200::e_json('', 1);
	}

	public static function country_states_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CId=(int)$p_CId;
		!$CId && ly200::e_json('');
		$SId=(int)$p_SId;
		$data=array(
			'CId'			=>	(int)$p_CId,
			'States'		=>	$p_States,
			'AcronymCode'	=>	strtoupper($p_AcronymCode),
		);
		if($SId){
			$rows=str::str_code(db::get_one('country_states s left join country c on s.CId=c.CId', "s.SId='{$SId}'", 'c.Country, s.States'));
			$logs="修改{$rows['Country']}省份：（{$rows['States']}）";
			db::update('country_states', "CId='{$CId}' and SId='{$SId}'", $data);
		}else{
			$Country=str::str_code(db::get_value('country', "CId='{$CId}'", 'Country'));
			$logs="添加{$Country}省份：".$p_States;
			db::insert('country_states', $data);
		}
		manage::operation_log($logs);
		ly200::e_json('', 1);
	}

	public static function themes_mobile_themes_edit(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$HomeTpl=$g_tpl;
		$data=array(
			'HomeTpl'	=>	$HomeTpl
		);
		manage::config_operaction($data, 'mobile');
		manage::operation_log('修改手机模板首页');
		ly200::e_json('', 1);
	}

	public static function themes_index_set_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$AId=(int)$g_AId;
		db::delete('ad', "AId='$AId'");
		manage::operation_log('删除广告图片');
		ly200::e_json('', 1);
	}

	public static function themes_index_set_add(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$PageName=$p_PageName;
		$AdPosition=$p_AdPosition;
		$AdType=(int)$p_AdType;
		$PicCount=(int)$p_PicCount;
		$Width=(int)$p_Width;
		$Height=(int)$p_Height;
		$version=(int)$p_version;//0 电脑版，1手机版
		$pagetype=(int)$p_pagetype;//0首页， 1列表页
		$data=array(
			'Themes'		=>	!$version?$c['manage']['web_themes']:'',
			'MThemesHome'	=>	$version?(!$pagetype?'MHomeTpl':''):'',//手机首页
			'MThemesList'	=>	$version?($pagetype?'MListTPL':''):'',//手机列表页
			'PageName'		=>	$PageName,
			'AdPosition'	=>	$AdPosition,
			'AdType'		=>	$AdType,
			'ShowType'		=>	$AdType==0?1:'',
			'PicCount'		=>	$PicCount,
			'Width'			=>	$Width,
			'Height'		=>	$Height,
		);
		db::insert('ad', $data);
		manage::operation_log('添加广告图片');
		ly200::e_json('', 1);
	}

	public static function themes_index_set_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$WId=(int)$p_WId;
		$Type=$p_Type;
		if($Type=='Banner'){
			$p_AId=(int)$p_AId;
			$p_PicCount=(int)$p_PicCount;
			$p_AdType=(int)$p_AdType;
			$p_ShowType=(int)$p_ShowType;

			if($p_AdType==0){//图片
				$Name=$Brief=$Url=$PicPath=$BannerUrl=$BannerUrlData=array();
				$FormatAry=array();
				$save_dir=$c['manage']['upload_dir'].'photo/';
				foreach($c['manage']['web_lang_list'] as $v){
					for($i=0; $i<$p_PicCount; ++$i){
						$pic=$_POST['PicPath_'.$v][$i];
						if($pic && is_file($c['root_path'].$pic)){
							$pic=file::photo_tmp_upload($pic, $save_dir);
						}
						$FormatAry['Name'][$i][$v]=$_POST['Name_'.$v][$i];
						$FormatAry['Brief'][$i][$v]=$_POST['Brief_'.$v][$i];
						$FormatAry['Url'][$i][$v]=$_POST['Url_'.$v][$i];
						$FormatAry['PicPath'][$i][$v]=$pic;
					}
				}
				foreach($FormatAry as $k=>$v){
					for($i=0; $i<$p_PicCount; ++$i){
						${$k}[$i]=addslashes(str::json_data(str::str_code($v[$i], 'stripslashes')));
					}
				}
			}

			$data=array(
				'Name'			=>	$p_Name,
				'ShowType'		=>	$p_ShowType,
				'Name_0'		=>	$Name[0],
				'Name_1'		=>	$Name[1],
				'Name_2'		=>	$Name[2],
				'Name_3'		=>	$Name[3],
				'Name_4'		=>	$Name[4],
				'Brief_0'		=>	$Brief[0],
				'Brief_1'		=>	$Brief[1],
				'Brief_2'		=>	$Brief[2],
				'Brief_3'		=>	$Brief[3],
				'Brief_4'		=>	$Brief[4],
				'Url_0'			=>	$Url[0],
				'Url_1'			=>	$Url[1],
				'Url_2'			=>	$Url[2],
				'Url_3'			=>	$Url[3],
				'Url_4'			=>	$Url[4],
				'PicPath_0'		=>	$PicPath[0],
				'PicPath_1'		=>	$PicPath[1],
				'PicPath_2'		=>	$PicPath[2],
				'PicPath_3'		=>	$PicPath[3],
				'PicPath_4'		=>	$PicPath[4],
			);
			db::update('ad', "AId='$p_AId'", $data);
		}else{
			//Name_Brief_PicPath_Url
			$Type=@explode('_', $Type);
			$config=str::json_data(db::get_value('web_settings',"WId='{$WId}'",'Config'), 'decode');
			$PicCount=(int)$config['PicCount'];
			$PicCount || $PicCount=1;
			// $PicCount>1 && $FormatAry=array('ShowType'=>1);
			foreach((array)$Type as $v){
				for($i=0;$i<$PicCount;$i++){
					foreach((array)$c['manage']['web_lang_list'] as $v1){
						$FormatAry[$v][$i][$v1]=$_POST[$v.'_'.$v1][$i];
						$FormatAry[$v][$i]=array_filter($FormatAry[$v][$i]);
					}
				}
			}
			$Data=addslashes(str::json_data(str::str_code($FormatAry, 'stripslashes')));
			db::update('web_settings',"WId='{$WId}'",array('Data'=>$Data));
		}
		manage::operation_log('网站设置-'.$WId);
		ly200::e_json('', 1);
	}

	public static function themes_products_list_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$list_row=db::get_one('config_module', "Themes='".$c['manage']['web_themes']."'", 'Themes, ListData');
		$data_ary=str::json_data(htmlspecialchars_decode($list_row['ListData']), 'decode');
		foreach($data_ary['IsLeftbar'] as $k=>$v){
			if(${'p_Leftbar_'.$k}){
				$list_ary[$k]=1;
			}else{
				$list_ary[$k]=0;
			}
		}
		$data=array(
			'IsColumn'		=>	(int)$p_IsColumn,
			'Narrow'		=>	(int)$p_Narrow,
			'IsLeftbar'		=>	$list_ary,
			'Order'			=>	$p_Order,
			'OrderNumber'	=>	(int)$p_OrderNumber,
			'Effects'		=>	(int)$p_Effects,
		);
		$ListData=addslashes(str::json_data(str::str_code($data, 'stripslashes')));
		db::update('config_module', "Themes='".$c['manage']['web_themes']."'", array('ListData'=>$ListData));
		manage::operation_log('修改列表设置');
		ly200::e_json('', 1);
	}

	public static function themes_products_list_reset(){
		global $c;
		$themes_set_file=$c['root_path']."/static/themes/{$c['manage']['web_themes']}/inc/themes_set.php";
		if(@is_file($themes_set_file)){
			include($themes_set_file);
			$data=themes_set::themes_products_list_reset();
			if($data){
				db::update('config_module', "Themes='{$c['manage']['web_themes']}'", array('ListData'=>$data));
				manage::operation_log('重置为默认列表设置');
				ly200::e_json('', 1);
			}
			ly200::e_json('重置失败！');
		}
		ly200::e_json('重置失败！');
	}

	public static function themes_products_detail_themes_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$detail_row=db::get_one('config_module', "Themes='".$c['manage']['web_themes']."'", 'Themes, DetailData');
		$data_ary=str::json_data(htmlspecialchars_decode($detail_row['DetailData']), 'decode');
		foreach($data_ary as $k=>$v){
			$data_ary[$k]=0;
		}
		$data_ary[$p_Key]=1;
		$DetailData=addslashes(str::json_data(str::str_code($data_ary, 'stripslashes')));
		db::update('config_module', "Themes='".$c['manage']['web_themes']."'", array('DetailData'=>$DetailData));
		manage::operation_log('选择产品详细风格');
		ly200::e_json('', 1);
	}

	public static function themes_products_detail_nav_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Share=addslashes(str::json_data(str::str_code($p_Share, 'stripslashes')));
		$data_ary=array();
		foreach((array)$p_Url as $k=>$v){
			foreach($c['manage']['config']['Language'] as $k2=>$v2){
				$data_ary[$k]["Name_{$v2}"]=${'p_Name_'.$v2}[$k];
			}
			$data_ary[$k]['Url']=$p_Url[$k];
			$data_ary[$k]['NewTarget']=$p_NewTarget[$k];
		}
		$ProDetail=addslashes(str::json_data(str::str_code($data_ary, 'stripslashes')));
		$data=array(
			'Share'		=>	$Share,
			'ProDetail'	=>	$ProDetail
		);
		manage::config_operaction($data, 'global');
		manage::operation_log('修改产品详细设置');
		ly200::e_json('', 1);
	}

	public static function themes_nav_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Id=(int)$p_Id;
		$p_Unit=(int)$p_Unit;
		$p_UnitType=trim($p_UnitType);
		$p_NewTarget=(int)$p_NewTarget;
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($p_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');

		$p_UnitType=='' && $p_UnitType='add';

		if($p_UnitType=='add'){
			//自定义项
			$data=array();
			$data['Custom']=1;
			foreach($c['manage']['config']['Language'] as $k2=>$v2){
				$data["Name_{$v2}"]=${'p_UnitValue_'.$v2};
			}
			$data['Url']=$p_Url;
			$data['NewTarget']=$p_NewTarget;
		}else{
			//固定项
			$nav_ary=array();
			foreach($c['nav_cfg'] as $k=>$v){
				$nav_ary[$v['page']]=$k;
			}
			if($p_UnitType=='article'){
				$Page=$p_Unit;
			}elseif($p_UnitType=='products' || $p_UnitType=='category'){
				$p_UnitType='products';
				$Cate=$p_Unit;
			}
			$data=array(
				'Nav'		=>	$nav_ary[$p_UnitType],//导航栏目，详细请看 $c['nav_cfg']
				'Page'		=>	$Page?$Page:0,//单页
				'Cate'		=>	$Cate?$Cate:0,//产品
				'NewTarget'	=>	$p_NewTarget//新窗口
			);
			if($p_Type=='nav'){
				//图片
				$p_PicCount = 4;
				$Name=$Brief=$Url=$PicPath=array();
				$FormatAry=array();
				$save_dir=$c['manage']['upload_dir'].'photo/';
				foreach($c['manage']['web_lang_list'] as $v){
					for($i=0; $i<$p_PicCount; ++$i){
						$pic=$_POST['PicPath_'.$v][$i];
						if($pic && is_file($c['root_path'].$pic)){
							$pic=file::photo_tmp_upload($pic, $save_dir);
						}
						$FormatAry['ImgName'][$i][$v]=$_POST['ImgName_'.$v][$i];
						$FormatAry['Url'][$i][$v]=$_POST['Url_'.$v][$i];
						$FormatAry['PicPath'][$i][$v]=$pic;
					}
				}
				foreach($FormatAry as $k=>$v){
					for($i=0; $i<$p_PicCount; ++$i){
						${$k}[$i]=$v[$i];
					}
				}
				$data['ImgName_0']	=	$ImgName[0];
				$data['ImgName_1']	=	$ImgName[1];
				$data['ImgName_2']	=	$ImgName[2];
				$data['ImgName_3']	=	$ImgName[3];
				$data['Url_0']		=	$Url[0];
				$data['Url_1']		=	$Url[1];
				$data['Url_2']		=	$Url[2];
				$data['Url_3']		=	$Url[3];
				$data['PicPath_0']	=	$PicPath[0];
				$data['PicPath_1']	=	$PicPath[1];
				$data['PicPath_2']	=	$PicPath[2];
				$data['PicPath_3']	=	$PicPath[3];
			}
		}
		if($p_Id==0){//添加
			$nav_data[]=$data;
		}else{//修改
			$nav_data[$p_Id-1]=$data;
		}
		$NavData=addslashes(str::json_data(str::str_code($nav_data, 'stripslashes')));
		manage::config_operaction(array(($p_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log($p_Type=='nav'?'修改导航设置':'修改底部栏目设置');
		ly200::e_json('', 1);
	}

	public static function themes_nav_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$MyOrderAry=@explode('|', $g_sort_order);
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($g_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		$data_ary=array();
		foreach((array)$MyOrderAry as $num){
			$data_ary[]=$nav_data[$num];
		}
		$NavData=addslashes(str::json_data(str::str_code($data_ary, 'stripslashes')));
		manage::config_operaction(array(($g_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log(($g_Type=='nav'?'导航排序':'底部栏目排序'));
		ly200::e_json('', 1);
	}

	public static function themes_nav_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_Id=(int)$g_Id;
		!$g_Id && ly200::e_json('');
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($g_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		unset($nav_data[$g_Id-1]);
		$NavData=addslashes(str::json_data(str::str_code($nav_data, 'stripslashes')));
		manage::config_operaction(array(($g_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log(($g_Type=='nav'?'删除导航':'删除底部栏目'));
		ly200::e_json('', 1);
	}

	public static function themes_nav_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_group_id && ly200::e_json('');
		$IdAry=@explode('-', $g_group_id);
		$nav_row=db::get_value('config', "GroupId='themes' and Variable='".($g_Type=='nav'?'NavData':'FooterData')."'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		foreach((array)$IdAry as $v){
			unset($nav_data[$v]);
		}
		$NavData=addslashes(str::json_data(str::str_code($nav_data, 'stripslashes')));
		manage::config_operaction(array(($g_Type=='nav'?'NavData':'FooterData')=>$NavData), 'themes');
		manage::operation_log(($g_Type=='nav'?'批量删除导航':'批量删除底部栏目'));
		ly200::e_json('', 1);
	}

	public static function themes_style_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		@trim($p_FontColor, '#')		&& $data['FontColor']		='#'.trim($p_FontColor, '#');
		@trim($p_PriceColor, '#')		&& $data['PriceColor']		='#'.trim($p_PriceColor, '#');
		@trim($p_SearchBgColor, '#')	&& $data['SearchBgColor']	='#'.trim($p_SearchBgColor, '#');
		@trim($p_AddtoCartBgColor, '#')	&& $data['AddtoCartBgColor']='#'.trim($p_AddtoCartBgColor, '#');
		@trim($p_BuyNowBgColor, '#')	&& $data['BuyNowBgColor']	='#'.trim($p_BuyNowBgColor, '#');
		@trim($p_ReviewBgColor, '#')	&& $data['ReviewBgColor']	='#'.trim($p_ReviewBgColor, '#');
		@trim($p_DiscountBgColor, '#')	&& $data['DiscountBgColor']	='#'.trim($p_DiscountBgColor, '#');
		@trim($p_NavBgColor, '#')		&& $data['NavBgColor']		='#'.trim($p_NavBgColor, '#');
		@trim($p_NavHoverBgColor, '#')	&& $data['NavHoverBgColor']	='#'.trim($p_NavHoverBgColor, '#');
		@trim($p_NavBorderColor1, '#')	&& $data['NavBorderColor1']	='#'.trim($p_NavBorderColor1, '#');
		@trim($p_NavBorderColor2, '#')	&& $data['NavBorderColor2']	='#'.trim($p_NavBorderColor2, '#');
		@trim($p_CategoryBgColor, '#')	&& $data['CategoryBgColor']	='#'.trim($p_CategoryBgColor, '#');
		@trim($p_ProListBgColor, '#')	&& $data['ProListBgColor']	='#'.trim($p_ProListBgColor, '#');
		@trim($p_ProListHoverBgColor, '#')	&& $data['ProListHoverBgColor']	='#'.trim($p_ProListHoverBgColor, '#');
		@trim($p_GoodBorderColor, '#')	&& $data['GoodBorderColor']	='#'.trim($p_GoodBorderColor, '#');
		@trim($p_GoodBorderHoverColor, '#')	&& $data['GoodBorderHoverColor']	='#'.trim($p_GoodBorderHoverColor, '#');
		for($i=0;$i<3;++$i){
			@trim(${'p_IndexMenuBg_'.$i}, '#')	&& $data['IndexMenuBg_'.$i]	='#'.trim(${'p_IndexMenuBg_'.$i}, '#');
		}
		$StyleData=addslashes(str::json_data(str::str_code($data, 'stripslashes')));
		db::update('config_module', "Themes='".$c['manage']['web_themes']."'", array('StyleData'=>$StyleData));
		manage::operation_log('修改风格样式');
		ly200::e_json('', 1);
	}

	public static function themes_advanced_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//搜索框提示语
		$SearchTipsAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$SearchTipsAry['SearchTips_'.$v]=${'p_SearchTips_'.$v};
		}
		$SearchTipsData=addslashes(str::json_data(str::str_code($SearchTipsAry, 'stripslashes')));
		$data = array(
			'SearchTips'		=>	$SearchTipsData,
		);
		if($p_ContactMenu){
			$ContactMenuData=addslashes(str::json_data(str::str_code($p_ContactMenu, 'stripslashes')));
			$data['ContactMenu']=$ContactMenuData;
		}
		manage::config_operaction($data, 'global');
		manage::operation_log('修改风格高级设置');
		ly200::e_json('', 1);
	}

	public static function themes_style_reset(){
		global $c;
		$themes_set_file=$c['root_path']."/static/themes/{$c['manage']['web_themes']}/inc/themes_set.php";
		if(@is_file($themes_set_file)){
			include($themes_set_file);
			$data=themes_set::themes_style_reset();
			if($data){
				db::update('config_module', "Themes='".$c['manage']['web_themes']."'", array('StyleData'=>$data));
				manage::operation_log('重置为默认样式管理');
				ly200::e_json('', 1);
			}
			ly200::e_json('重置失败！');
		}
		ly200::e_json('重置失败！');
	}

	public static function shipping_area_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$AId=(int)$g_AId;
		$w="AId='$AId'";
		db::delete('shipping_area', $w);
		db::delete('shipping_country', $w);
		manage::operation_log('删除快递分区');
		ly200::e_json('', 1);
	}

	public static function shipping_set_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$IsAir=(int)$p_IsAir;
		$AirWeightCfg=(float)$p_AirWeightCfg;
		$AirVolumeCfg=(float)$p_AirVolumeCfg;
		$IsOcean=(int)$p_IsOcean;
		$OceanWeightCfg=(float)$p_OceanWeightCfg;
		$OceanVolumeCfg=(float)$p_OceanVolumeCfg;
		$data=array(
			'IsAir'				=>	$IsAir,
			'AirWeightCfg'		=>	$AirWeightCfg,
			'AirVolumeCfg'		=>	$AirVolumeCfg,
			'IsOcean'			=>	$IsOcean,
			'OceanWeightCfg'	=>	$OceanWeightCfg,
			'OceanVolumeCfg'	=>	$OceanVolumeCfg,
		);
		db::get_row_count('shipping_config')?db::update('shipping_config', '1', $data):db::insert('shipping_config', $data);
		manage::operation_log('修改运费设置');
		ly200::e_json('', 1);
	}

	public static function shipping_template_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		db::update('shipping_template', "1", array('IsDefault'=>0));
		foreach((array)$p_Name as $k=>$v){
			$data=array();
			$data['Name']=$v;
			$data['AccTime']=$c['time'];
			if($p_IsDefault==$k) $data['IsDefault']=1;
			if(db::get_row_count('shipping_template', "TId='$k'")){//确保运费模板数据存在
				db::update('shipping_template', "TId='$k'", $data);
			}else{
				$p_IsDelete[$k] || db::insert('shipping_template', $data);
			}
		}
		$delete_ary=array();
		foreach((array)$p_IsDelete as $k=>$v){
			if($v==1){//确定要删除运费模板
				$TId=$k;
				$template_row=str::str_code(db::get_one('shipping_template', "TId='$TId'"));//确保运费模板数据存在
				if($template_row){
					$delete_ary[]=$TId;
				}
			}
		}
		if(count($delete_ary)){
			$delete=implode(',', $delete_ary);
			$where="TId in ($delete)";
			db::delete('shipping_template', $where);
			db::update('shipping', $where, array('TId'=>0));
			manage::operation_log('删除运费模板');
		}
		if(db::get_row_count('shipping_template', '1') && !db::get_row_count('shipping_template', 'IsDefault=1')){ //必须设置一个默认模板
			db::update('shipping_template', "TId in (select TId from (select TId from shipping_template where 1 limit 1) as tmp)", array('IsDefault'=>1));
		}
		manage::operation_log('编辑运费模板');
		ly200::e_json('', 1);
	}

	public static function shipping_overseas_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		foreach((array)$p_Name as $k=>$v){
			$data=array();
			foreach((array)$v as $lang=>$v2){
				$data["Name_$lang"]=addslashes($v2);
			}
			if(db::get_row_count('shipping_overseas', "OvId='$k'")){//确保海外仓数据存在
				db::update('shipping_overseas', "OvId='$k'", $data);
			}else{
				db::insert('shipping_overseas', $data);
			}
		}
		$delete_ary=array();
		foreach((array)$p_IsDelete as $k=>$v){
			if($v==1){//确定要删除海外仓
				$OvId=$k;
				$overseas_row=str::str_code(db::get_one('shipping_overseas', "OvId='$OvId'"));//确保海外仓数据存在
				if($overseas_row){
					$delete_ary[]=$OvId;
				}
			}
		}
		if(count($delete_ary)){
			$delete=implode(',', $delete_ary);
			$where="OvId in ($delete)";
			db::delete('shipping_overseas', $where);
			db::delete('shipping_country', "AId in(select AId from shipping_area where $where)");
			db::delete('shipping_area', $where);
			db::delete('products_selected_attribute', $where);
			db::delete('products_selected_attribute_combination', $where);
			manage::operation_log('删除海外仓');
		}
		manage::operation_log('编辑海外仓');
		ly200::e_json('', 1);
	}

	public static function shipping_express_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$SId=(int)$g_SId;
		$Logo=db::get_value('shipping', "SId='$SId'", 'Logo');
		if($Logo){
			/*
			$resize_ary=$c['manage']['resize_ary']['shipping'];
			$ext_name=file::get_ext_name($Logo);
			file::del_file($Logo);//删除旧图
			foreach($resize_ary as $v){
				file::del_file($Logo.".{$v}.{$ext_name}");//删除旧图
			}
			*/
			file::photo_check_and_delete($Logo);
		}
		db::delete('shipping', "SId='$SId'");
		db::delete('shipping_area', "SId='$SId'");
		db::delete('shipping_country', "SId='$SId'");
		db::delete('shipping_api', "SId='$SId'");
		manage::operation_log('删除快递方式');
		ly200::e_json('', 1);
	}

	public static function shipping_express_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `shipping` SET `MyOrder` = CASE `SId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `SId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('快递方式排序');
		ly200::e_json('', 1);
	}

	public static function shipping_express_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$SId=(int)$p_SId;
		$Express=$p_Express;
		$Logo=$p_Logo;
		$IsUsed=(int)$p_IsUsed;
		$Brief=$p_Brief;
		$IsWeightArea=(int)$p_IsWeightArea;//是否按照重量区间计算运费 1是 0否 2两者一齐计
		$FirstWeight=$IsWeightArea!=1?$p_FirstWeight:0;
		$ExtWeight=$IsWeightArea!=1?$p_ExtWeight:0;
		$StartWeight=$IsWeightArea==2?(float)$p_StartWeight:0;//重量区间开始计算的重量
		$StartWeight=($FirstWeight>$StartWeight || $StartWeight<=0)?$FirstWeight:$StartWeight;
		$MinWeight=$p_MinWeight;
		$MaxWeight=$p_MaxWeight;
		$MinVolume=$p_MinVolume;
		$MaxVolume=$p_MaxVolume;
		$MaxWeight<$MinWeight && $MaxWeight=0;
		$FirstMinQty=$p_FirstMinQty;
		$FirstMaxQty=$p_FirstMaxQty;
		$ExtQty=$p_ExtQty;
		$WeightArea=array();//重量区间
		if($IsWeightArea==1 || $IsWeightArea==2){
			$WeightArea=$p_WeightArea;
			$WeightArea[0]<$StartWeight && $WeightArea[0]=$StartWeight;
		}
		if($IsWeightArea==0 || $IsWeightArea==2){
			$ExtWeightArea=$p_ExtWeightArea;
			$ExtWeightArea[0]<=$FirstWeight && $ExtWeightArea[0]=$FirstWeight+0.001;
		}
		if($IsWeightArea==3){//按数量
			if($FirstMaxQty<$FirstMinQty){//首重最高数量 < 首重最低数量，互换一下各自的位置
				$_FirstMaxQty=$FirstMaxQty;
				$FirstMaxQty=$FirstMinQty;
				$FirstMinQty=$_FirstMaxQty;
			}
		}
		if($IsWeightArea==4){//重量体积混合
			$WeightArea=$p_WeightArea;
			$WeightArea[0]<$MinWeight && $WeightArea[0]=$MinWeight;
			$VolumeArea=$p_VolumeArea;
			$VolumeArea[0]<$MinVolume && $VolumeArea[0]=$MinVolume;
		}
		if($IsWeightArea==2){
			$MinW=$StartWeight;
		}elseif($IsWeightArea==4){
			$MinW=$MinWeight;
		}else $MinW=0;
		$WeightArea=str::ary_del_min($WeightArea, $MinW);//清除重量区间低于区间开始重量的值
		$VolumeArea=str::ary_del_min($VolumeArea, $MinVolume);//清除体积区间低于最低限制体积的值
		$ExtWeightArea=str::ary_del_min($ExtWeightArea, $ExtWeight);//清除续重区间低于续重的值
		//接口的数据
		$json_ary=array();
		for($i=0, $len=count($p_Value); $i<$len; $i++){
			$json_ary[$p_Name[$i]]=$p_Value[$i];
		}
		$json_data=addslashes(str::json_data(str::str_code($json_ary, 'stripslashes')));
		$data=array(
			'TId'			=>	(int)$p_TId,
			'Express'		=>	$Express,
			'Logo'			=>	$Logo,
			'IsUsed'		=>	$IsUsed,
			'IsAPI'			=>	(int)$p_IsAPI,
			'Brief'			=>	$Brief,
			'Query'			=>	$p_Query,
			'IsWeightArea'	=>	$IsWeightArea,
			'WeightArea'	=>	addslashes(str::json_data(str::str_code($WeightArea, 'stripslashes'))),
			'ExtWeightArea'	=>	addslashes(str::json_data(str::str_code($ExtWeightArea, 'stripslashes'))),
			'VolumeArea'	=>	addslashes(str::json_data(str::str_code($VolumeArea, 'stripslashes'))),
			'FirstWeight'	=>	$FirstWeight,
			'ExtWeight'		=>	$ExtWeight,
			'StartWeight'	=>	$StartWeight,
			'MinWeight'		=>	$MinWeight,
			'MaxWeight'		=>	$MaxWeight,
			'MinVolume'		=>	$MinVolume,
			'MaxVolume'		=>	$MaxVolume,
			'FirstMinQty'	=>	$FirstMinQty,
			'FirstMaxQty'	=>	$FirstMaxQty,
			'ExtQty'		=>	$ExtQty,
			'WeightType'	=>	(int)$p_WeightType
		);
		if($SId){
			db::update('shipping', "SId='$SId'", $data);
			manage::operation_log('修改快递公司');
		}else{
			db::insert('shipping', $data);
			$SId=db::get_insert_id();
			manage::operation_log('添加快递公司');
		}
		if($p_IsAPI>0){//更新接口数据
			$ApiName=db::get_value('shipping_api', "AId='$p_IsAPI'", 'Name');
			if($api_row=db::get_one('shipping_api', "Name='$ApiName' and SId='$SId'")){
				db::update('shipping_api', "Name='$ApiName' and SId='$SId'", array('Attribute'=>$json_data));
				$AId=$api_row['AId'];
			}else{
				db::insert('shipping_api', array('Name'=>$ApiName, 'SId'=>$SId, 'Attribute'=>$json_data));
				$AId=db::get_insert_id();
			}
			db::update('shipping', "SId='$SId'", array('IsAPI'=>$AId));
		}
		//判断新用户已经修改运费方式
		manage::config_operaction(array('shipping'=>1), 'guide_pages');
		ly200::e_json('', 1);
	}

	public static function shipping_express_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_SId=(int)$p_SId;
		$p_IsUsed=(int)$p_IsUsed;
		db::update('shipping',"SId='{$p_SId}'",array('IsUsed'=>$p_IsUsed));
		manage::operation_log(($p_IsUsed ? '开启' : '关闭').'快递公司-'.$p_SId);
		ly200::e_json('', 1);
	}

	public static function shipping_express_area_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$SId=(int)$p_SId;
		$OvId=(int)$p_OvId;
		$AId=(int)$p_AId;
		$Name=$p_Name;
		$Brief=$p_Brief;
		$FirstPrice=$p_FirstPrice;
		$ExtPrice=$p_ExtPrice;
		$FirstQtyPrice=$p_FirstQtyPrice;
		$ExtQtyPrice=$p_ExtQtyPrice;
		$IsFreeShipping=(int)$p_IsFreeShipping;
		$FreeShippingPrice=$p_IsFreeShipping?$p_FreeShippingPrice:0;
		$FreeShippingWeight=$p_IsFreeShipping?$p_FreeShippingWeight:0;
		$WeightAreaPrice=addslashes(str::json_data(str::str_code((array)$p_WeightAreaPrice, 'stripslashes')));
		$ExtWeightAreaPrice=addslashes(str::json_data(str::str_code((array)$p_ExtWeightAreaPrice, 'stripslashes')));
		$VolumeAreaPrice=addslashes(str::json_data(str::str_code((array)$p_VolumeAreaPrice, 'stripslashes')));
		$data=array(
			'SId'				=>	$SId,
			'OvId'				=>	$OvId,
			'Name'				=>	$Name,
			'Brief'				=>	$Brief,
			'FirstPrice'		=>	$FirstPrice,
			'ExtPrice'			=>	$ExtPrice,
			'WeightAreaPrice'	=>	$WeightAreaPrice,
			'ExtWeightAreaPrice'=>	$ExtWeightAreaPrice,
			'VolumeAreaPrice'	=>	$VolumeAreaPrice,
			'IsFreeShipping'	=>	$IsFreeShipping,
			'FreeShippingPrice'	=>	$FreeShippingPrice,
			'FreeShippingWeight'=>	$FreeShippingWeight,
			'FirstQtyPrice'		=>	$FirstQtyPrice,
			'ExtQtyPrice'		=>	$ExtQtyPrice,
			'AffixPrice'		=>	(float)$p_AffixPrice,
		    'AffixPrice_rate'		=>	($p_AffixPrice_rate/100)
		);


		if($AId){
			db::update('shipping_area', "AId='$AId' and SId='$SId'", $data);
			manage::operation_log('修改快递分区');
		}else{
			db::insert('shipping_area', $data);
			$AId=db::get_insert_id();
			manage::operation_log('添加快递分区');
		}
		$CId=(array)$p_CId;
		$CountrySId=(array)$p_CountrySId;
		foreach((array)$CountrySId as $k=>$v){
			$CId[]=$k;
		}
		db::delete('shipping_country', "SId='$SId' and AId='$AId'");//删除原有的
		if($CId){
			$sql='INSERT INTO `shipping_country` (`Id`, `SId`, `AId`, `CId`, `type`, `states`) VALUES';
			foreach($CId as $k=>$v){
				$states='|'.@implode((array)$CountrySId[$v], '|').'|';
				$states=='||' && $states='';
				$sql.="('', '$SId', '$AId', '$v', '', '$states'),";
			}
			$sql=trim($sql, ',');
			db::query($sql);
		}
		$tips=$SId ? $c['manage']['lang_pack']['msg']['edit_success'] : $c['manage']['lang_pack']['msg']['add_success'];
		ly200::e_json(array('tips'=>$tips, 'OvId'=>$OvId), 1);
	}

	/* 20190507
	public static function shipping_express_area_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$SId=(int)$p_SId;
		$OvId=(int)$p_OvId;
		$AId=(int)$p_AId;
		$Name=$p_Name;
		$Brief=$p_Brief;
		$FirstPrice=$p_FirstPrice;
		$ExtPrice=$p_ExtPrice;
		$FirstQtyPrice=$p_FirstQtyPrice;
		$ExtQtyPrice=$p_ExtQtyPrice;
		$IsFreeShipping=(int)$p_IsFreeShipping;
		$FreeShippingPrice=$p_IsFreeShipping?$p_FreeShippingPrice:0;
		$FreeShippingWeight=$p_IsFreeShipping?$p_FreeShippingWeight:0;
		$WeightAreaPrice=addslashes(str::json_data(str::str_code((array)$p_WeightAreaPrice, 'stripslashes')));
		$ExtWeightAreaPrice=addslashes(str::json_data(str::str_code((array)$p_ExtWeightAreaPrice, 'stripslashes')));
		$VolumeAreaPrice=addslashes(str::json_data(str::str_code((array)$p_VolumeAreaPrice, 'stripslashes')));
		$data=array(
			'SId'				=>	$SId,
			'OvId'				=>	$OvId,
			'Name'				=>	$Name,
			'Brief'				=>	$Brief,
			'FirstPrice'		=>	$FirstPrice,
			'ExtPrice'			=>	$ExtPrice,
			'WeightAreaPrice'	=>	$WeightAreaPrice,
			'ExtWeightAreaPrice'=>	$ExtWeightAreaPrice,
			'VolumeAreaPrice'	=>	$VolumeAreaPrice,
			'IsFreeShipping'	=>	$IsFreeShipping,
			'FreeShippingPrice'	=>	$FreeShippingPrice,
			'FreeShippingWeight'=>	$FreeShippingWeight,
			'FirstQtyPrice'		=>	$FirstQtyPrice,
			'ExtQtyPrice'		=>	$ExtQtyPrice,
			'AffixPrice'		=>	(float)$p_AffixPrice
		);


		if($AId){
			db::update('shipping_area', "AId='$AId' and SId='$SId'", $data);
			manage::operation_log('修改快递分区');
		}else{
			db::insert('shipping_area', $data);
			$AId=db::get_insert_id();
			manage::operation_log('添加快递分区');
		}
		$CId=(array)$p_CId;
		db::delete('shipping_country', "SId='$SId' and AId='$AId'");//删除原有的
		if($CId){
			$sql='INSERT INTO `shipping_country` (`Id`, `SId`, `AId`, `CId`, `type`) VALUES';
			foreach($CId as $k=>$v){
				$sql.="('', '$SId', '$AId', '$v', ''),";
			}
			$sql=trim($sql, ',');
			db::query($sql);
		}
		$tips=$SId ? $c['manage']['lang_pack']['msg']['edit_success'] : $c['manage']['lang_pack']['msg']['add_success'];
		ly200::e_json(array('tips'=>$tips, 'OvId'=>$OvId), 1);
	}
	*/

	public static function shipping_express_area_country_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$SId=$p_SId;
		$AId=$p_AId;
		$CId=(array)$p_CId;
		db::delete('shipping_country', "SId='$SId' and AId='$AId'");//删除原有的
		if($CId){
			$sql='INSERT INTO `shipping_country` (`Id`, `SId`, `AId`, `CId`, `type`) VALUES';
			foreach($CId as $k=>$v){
				$sql.="('', '$SId', '$AId', '$v', ''),";
			}
			$sql=trim($sql, ',');
			db::query($sql);
		}
		manage::operation_log('修改分区国家');
		ly200::e_json('', 1);
	}

	public static function shipping_insurance_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ProPrice=$p_ProPrice;
		$AreaPrice=$p_AreaPrice;
		$arr=array();
		foreach($ProPrice as $k=>$v){
			if($v>=0) $arr[]=array((int)$v, (float)$AreaPrice[$k]);
		}
		$arr=str::ary_unique($arr);//删除重复，空值，0
		sort($arr);//从小到大排序，防止故意乱填
		$json_str=addslashes(str::json_data(str::str_code($arr, 'stripslashes')));
		$data=array('AreaPrice'=>$json_str,);
		db::get_row_count('shipping_insurance')?db::update('shipping_insurance', '1', $data):db::insert('shipping_insurance', $data);
		$p_IsInsurance=(int)$p_IsInsurance;
		db::update('shipping_config', '1', array('IsInsurance'=>$p_IsInsurance));
		manage::operation_log('修改运费保险');
		ly200::e_json('', 1);
	}

	public static function photo_choice(){//图片银行选择图片
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$PId=(array)$p_PId;
		$type=$p_type;//type类型,例如products,editor
		$maxpic=$p_maxpic;//最大允许图片数，0未不允许再上传图片，-1为没有数量限制
		$sum=0;//图片数量
		$Path=array('ret'=>1, 'msg'=>'', 'type'=>$type, 'Pic'=>array());//保存图片路径
		if($maxpic==0){
			$Path['ret']=0;
			$Path['msg']=$c['manage']['lang_pack']['set']['photo']['max_count'];
			exit(str::json_data($Path));
		}
		$save_dir='';
		$sub_save_dir=$c['manage']['sub_save_dir'];
		if($sub_save_dir[$type]){//来自缩略图，先保存到tmp临时文件夹
			$save_dir=$c['tmp_dir'].'photo/';
		}
		$save_dir && file::mk_dir($save_dir);
		if($p_sort){
			$id_ary=array();
			$p_sort=explode('|', $p_sort);
			foreach((array)$p_sort as $k=>$v){
				if(in_array($v, $PId)){
					$id_ary[]=$v;
				}
			}
			$PId=$id_ary;
		}
		foreach($PId as $k=>$v){//复制选择的图片到指定路径
			$sPic=db::get_value('photo', "PId='$v'", 'PicPath');
			$Pic=str_replace('\\', '/', $c['root_path']).ltrim($sPic, '/');
			if(is_file($Pic)){
				if ($save_dir){//有缩略图的保存到临时文件，没有使用图片银行路径
					$ext_name=file::get_ext_name($Pic);
					$temp=$Path['Pic'][]=$save_dir.str::rand_code().'.'.$ext_name;
					@copy($Pic, $c['root_path'].ltrim($temp, '/'));
				}else{
					$Path['Pic'][]=$sPic;
				}
				$sum++;
			}
			if($sum>=$maxpic && $maxpic>0){break;}//判断是否已超过允许图片数量
		}
		if(!$sum){//没有上传任何图片
			$Path['ret']=0;
			$Path['msg']=$c['manage']['lang_pack']['set']['photo']['no_photo'];
		}elseif ($type!='editor' && $save_dir){//非编辑器时，根据配置生成压缩图片
			$water_ary=array();
			$resize_ary=$c['manage']['resize_ary'];
			if(array_key_exists($type, $resize_ary)){
				foreach($Path['Pic'] as $key=>$value){
					(!$c['manage']['config']['IsWaterPro'] && $c['manage']['config']['IsWater']) && $water_ary[$key]=$value;
					if(in_array('default', $resize_ary[$type])){//保存不加水印的原图
						$ext_name=file::get_ext_name($value);
						@copy($c['root_path'].$value, $c['root_path'].$value.".default.{$ext_name}");
					}
					if(!$c['manage']['config']['IsWaterPro'] && $c['manage']['config']['IsWater'] && $c['manage']['config']['IsThumbnail']){//缩略图加水印
						img::img_add_watermark($value);
						unset($water_ary[$key]);
					}
					foreach((array)$resize_ary[$type] as $v){
						if($v=='default') continue;
						$size_w_h=explode('x', $v);
						$resize_path=img::resize($value, $size_w_h[0], $size_w_h[1]);
					}
				}
			}
			foreach((array)$water_ary as $v){
				img::img_add_watermark($v);
			}
		}
		exit(str::json_data($Path));
	}

	public static function photo_category(){//图片银行分类
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$Category=$p_Category;
		$UnderTheCateId=(int)$p_UnderTheCateId;
		if($UnderTheCateId==0){
			$UId='0,';
			$Dept=1;
		}else{
			$UId=category::get_UId_by_CateId($UnderTheCateId, 'photo_category');
			$Dept=substr_count($UId, ',');
		}
		$data=array(
			'Category'	=>	$Category,
			'UId'		=>	$UId,
			'Dept'		=>	$Dept
		);
		if($CateId){
			db::update('photo_category', "CateId='$CateId'", $data);
			manage::operation_log('修改图片银行分类');
		}else{
			db::insert('photo_category', $data);
			$CateId=db::get_insert_id();
			manage::operation_log('添加图片银行分类');
		}
		$UId!='0,' && $CateId=category::get_top_CateId_by_UId($UId);
		$statistic_where.=category::get_search_where_by_CateId($CateId, 'photo_category');
		category::category_subcate_statistic('photo_category', $statistic_where);
		ly200::e_json('', 1);
	}

	public static function photo_category_edit_myorder(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `photo_category` SET `MyOrder` = CASE `CateId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CateId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('图片管理分类修改排序');
		ly200::e_json('', 1);
	}

	public static function photo_category_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `photo_category` SET `MyOrder` = CASE `CateId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CateId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('批量图片管理分类排序');
		ly200::e_json('', 1);
	}

	public static function photo_category_del(){//图片银行分类删除
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CateId=(int)$g_CateId;
		$row=db::get_one('photo_category', "CateId='$CateId'", 'UId');
		$del_where=category::get_search_where_by_CateId($CateId, 'photo_category');
		db::delete('photo_category', $del_where);
		//删除分类下的图片
		$photo_row=db::get_all('photo', $del_where, 'PicPath');
		foreach($photo_row as $k=>$v){
			$PicPath=$c['root_path'].ltrim($v['PicPath'], '/');
			if(@is_file($PicPath)){
				$ext_name=file::get_ext_name($v['PicPath']);
				foreach($c['manage']['resize_ary']['photo'] as $v2){
					file::del_file($v['PicPath'].".{$v2}.{$ext_name}");
				}
				file::del_file($v['PicPath']);
			}
		}
		db::delete('photo', $del_where);
		if($row['UId']!='0,'){
			$CateId=category::get_top_CateId_by_UId($row['UId']);
			$statistic_where=category::get_search_where_by_CateId($CateId, 'photo_category');
			category::category_subcate_statistic('photo_category', $statistic_where);
		}
		manage::operation_log('删除图片管理分类');
		ly200::e_json('', 1);
	}

	public static function photo_category_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="CateId in(".str_replace('-', ',', $g_id).")";
		$row=str::str_code(db::get_all('photo_category', $del_where));
		db::delete('photo_category', $del_where);
		//删除分类下的图片
		$photo_row=db::get_all('photo', $del_where, 'PicPath');
		foreach($photo_row as $k=>$v){
			$PicPath=$c['root_path'].ltrim($v['PicPath'], '/');
			if(@is_file($PicPath)){
				$ext_name=file::get_ext_name($v['PicPath']);
				foreach($c['manage']['resize_ary']['photo'] as $v2){
					file::del_file($v['PicPath'].".{$v2}.{$ext_name}");
				}
				file::del_file($v['PicPath']);
			}
		}
		db::delete('photo', $del_where);
		manage::operation_log('批量删除图片管理分类');
		foreach($row as $v){
			if($v['UId']!='0,'){
				$CateId=category::get_top_CateId_by_UId($v['UId']);
				$statistic_where=category::get_search_where_by_CateId($CateId, 'photo_category');
				category::category_subcate_statistic('photo_category', $statistic_where);
			}
		}
		ly200::e_json('', 1);
	}

	public static function photo_category_select(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ParentId=(int)$p_ParentId;
		$p_CateId=(int)$p_CateId;
		$category_one=str::str_code(db::get_one('photo_category', "CateId='$p_CateId'"));
		$ext_where="CateId!='{$category_one['CateId']}' and Dept<2";
		echo category::ouput_Category_to_NewSelect('UnderTheCateId', ($ParentId?$ParentId:category::get_CateId_by_UId($category_one['UId'])), 'photo_category', "UId='0,' and $ext_where", $ext_where, 'class="box_input"', $c['manage']['lang_pack']['global']['select_index']);
		exit;
	}

	public static function photo_file_upload(){//图片银行图片上传
		global $c;
		exit(file::file_upload_swf($c['tmp_dir'].'photo/', '', false));
	}

	public static function photo_upload(){//图片银行图片添加提交处理函数
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$CateId && $CurName=db::get_value('photo_category', "CateId='{$CateId}'", 'Category');
		$CategoryName=trim($p_CateIdValue);
		if($CurName!=$CategoryName){
			$where="Category='{$CategoryName}'";
			$NewCateId=db::get_value('photo_category', $where, 'CateId');
			if($NewCateId){
				$CateId=$NewCateId;
			}else{
				$data=array(
					'Category'	=>	$CategoryName,
					'UId'		=>	'0,',
					'Dept'		=>	1
				);
				db::insert('photo_category', $data);
				$CateId=db::get_insert_id();
			}
		}
		//上传图片
		$PicPath=$p_PicPath;
		$Name=$p_Name;
		//检查图片
		foreach((array)$PicPath as $k=>$v){
			file::photo_add_item($v, $Name[$k], 0, $CateId);
		}
		ly200::e_json(array('jump'=>'./?m=set&a=photo&CateId='.$CateId), 1);
	}

	public static function photo_list_del(){//图片银行批量删除图片
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$PId=(array)$p_PId;
		$count=count($PId);
		foreach($PId as $k=>$v){
			$Pic=db::get_value('photo', "PId='$v'", 'PicPath');
			$PicPath=$c['root_path'].ltrim($Pic, '/');
			if(@is_file($PicPath)){
				$ext_name=file::get_ext_name($Pic);
				foreach($c['manage']['resize_ary']['photo'] as $v2){
					file::del_file($Pic.".{$v2}.{$ext_name}");
				}
				file::del_file($Pic);
			}
			db::delete('photo', "PId='$v'");
		}
		manage::operation_log('图片银行批量删除图片 数目：'.$count);
		ly200::e_json('', 1);
	}

	public static function photo_upload_del(){	//flash上传删除单个产品图片
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$Model=$g_Model;
		$PicPath=$g_Path;
		$Index=(int)$g_Index;
		if(is_file($c['root_path'].$PicPath)){
			file::del_file($PicPath);
		}
		ly200::e_json(array($Index), 1);
	}

	public static function photo_move(){//图片移动
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_PId=(array)$p_PId;
		$p_CateId=(int)$p_CateId;
		foreach($p_PId as $k=>$PId){
			$photo_row=str::str_code(db::get_one('photo', "PId='$PId'"));
			if($photo_row['CateId']!=$p_CateId){
				db::update('photo', "PId='$PId'", array('CateId'=>$p_CateId));
			}
		}
		ly200::e_json(array('jump'=>'./?m=set&a=photo&CateId='.$p_CateId), 1);
	}

	/*
	public static function photo_clear_folder(){//清空临时文件夹
		global $c;
		file::del_dir($c['tmp_dir'].'photo/');
		ly200::e_json('', 1);
	}
	*/

	public static function chat_set(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$w="GroupId='chat' and Variable='chat_bg'";
		$Color['Color'] = $p_Color;
		$Color['ColorTop'] = $p_ColorTop;
		foreach ((array)$c['chat']['type'] as $k=>$v){
			$Color[$k] = $_POST['Color'.$k];
		}
		$Color['Bg3_0'] = $p_Bg3_0;
		$Color['Bg3_1'] = $p_Bg3_1;
		$Color['Bg4_0'] = $p_Bg4_0;
		$Color['IsHide'] = (int)$p_IsHide;
		$ValueColor = json_encode($Color);

		$data=array(
			'GroupId'	=>	'chat',
			'Variable'	=>	'chat_bg',
			'Value'		=>	$ValueColor
		);
		(int)db::get_row_count('config', $w)?db::update('config', $w, $data):db::insert('config', $data);
		$w="GroupId='chat' and Variable='IsFloatChat'";
		$data=array(
			'GroupId'	=>	'chat',
			'Variable'	=>	'IsFloatChat',
			'Value'		=>	(int)$p_IsFloatChat
		);
		(int)db::get_row_count('config', $w)?db::update('config', $w, $data):db::insert('config', $data);
		manage::operation_log('修改浮动客服');

		ly200::e_json('', 1);
	}

	public static function chat_style(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Type=(int)$p_Type;
		$OldType=(int)db::get_value('config', 'GroupId="chat" and Variable="Type"', 'Value');
		if($OldType!=$p_Type){
			manage::config_operaction(array('Type'=>$p_Type), 'chat');
			manage::operation_log('修改浮动客服样式');
		}
		ly200::e_json('', 1);
	}

	public static function chat_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CId=(int)$p_CId;
		$data=array(
			'Name'		=>	$p_Name,
			'Type'		=>	$p_Type,
			'PicPath'	=>	$p_PicPath,
			'Account'	=>	$p_Account
		);
		if($p_CId){
			db::update('chat', "CId='{$p_CId}'", $data);
			$log='修改在线客服: '.$p_Name;
		}else{
			db::insert('chat', $data);
			$log='添加在线客服: '.$p_Name;
		}
		manage::operation_log($log);
		ly200::e_json('', 1);
	}

	public static function chat_my_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode('|', $g_sort_order));
		if ($sort_order){
			$sql = "UPDATE `chat` SET `MyOrder` = CASE `CId`";
			foreach((array)$sort_order as $v){
				$sql .= " WHEN $v THEN ".$order++;
			}
			$sql .= " END WHERE `CId` IN (".str_replace('|', ',', $g_sort_order).")";
			db::query($sql);
		}
		manage::operation_log('批量在线客服排序');
		ly200::e_json('', 1);
	}

	public static function chat_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_CId=(int)$g_CId;
		$name=db::get_value('chat', "CId='{$g_CId}'", 'Name');
		db::delete('chat', "CId='{$g_CId}'");
		manage::operation_log('删除在线客服: '.$name);
		ly200::e_json('', 1);
	}


	/*******************************第三方代码(start)*****************************/
	public static function seo_third_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_TId=(int)$p_TId;
		$p_IsMeta==1 && $p_IsBody=0;
		$p_IsBody==1 && $p_IsMeta=0;
		$data=array(
			'Title'		=>	$p_Title,
			'Code'		=>	$p_Code,
			'CodeType'	=>	(int)$p_CodeType,
			'IsMeta'	=>	(int)$p_IsMeta,
			'IsBody'	=>	(int)$p_IsBody,
		);
		if($p_TId){
			db::update('third', "TId='$p_TId'", $data);
			manage::operation_log('修改第三方代码');
		}else{
			$data['AccTime']=$c['time'];
			$data['IsUsed']=1;
			db::insert('third', $data);
			manage::operation_log('添加第三方代码');
		}
		ly200::e_json('', 1);
	}

	public static function seo_third_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$TId=(int)$p_TId;
		$IsUsed=(int)$p_IsUsed;
		$data=array(
			'IsUsed'	=>	$IsUsed,
		);
		db::update('third', "TId='$TId'", $data);
		manage::operation_log(($IsUsed ? '开启' : '关闭').'第三方代码-'.$TId);

		ly200::e_json('', 1);
	}

	public static function seo_third_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_TId=(int)$g_TId;
		db::delete('third', "TId='$g_TId'");
		manage::operation_log('删除第三方代码');
		ly200::e_json('', 1);
	}
	/*******************************第三方代码(end)*****************************/


	/*******************************管理员(start)*****************************/
	public static function manage_locked(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$post=@explode('.', $p_u);
		$p_Locked=(int)$p_Locked==1?1:0;
		$u=$post[0].'.'.$post[1].'.'.$p_Locked;
		$data=array(
			'Action'	=>	'ueeshop_web_manage_edit',
			'UserName'	=>	$post[0],
			'GroupId'	=>	$post[1],
			'Locked'	=>	$p_Locked,
			'Method'	=>	1
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);

		ly200::e_json($u, 1);
	}
	
	public static function manage_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		substr_count($p_UserName, '.') && ly200::e_json('用户名不能带有“.”');
		$p_Method=(int)$p_Method==1?1:0;
		$p_GroupId=(int)$p_GroupId?(int)$p_GroupId:1;
		$p_Locked=(int)$p_Locked==1?1:0;
		
		if($p_Method==1){
			$p_Password!='' && strlen($p_Password)<6 && ly200::e_json(manage::get_language('manage.manage.password_len_tips'));
		}else{
			(strlen($p_UserName)<6 || strlen($p_Password)<6) && ly200::e_json(manage::get_language('manage.manage.len_tips'));
		}
		$data=array(
			'Action'	=>	'ueeshop_web_manage_edit',
			'UserName'	=>	$p_UserName,
			'GroupId'	=>	$p_GroupId,
			'Locked'	=>	$p_Locked,
			'Method'	=>	$p_Method
		);
		$p_Password!='' && $data['Password']=ly200::password($p_Password);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		$p_GroupId>1 && manage::update_permit($p_UserName);
		if($p_GroupId==3){
			!db::get_row_count('manage_sales', "UserName='{$p_UserName}'") && db::insert('manage_sales', array('UserName'=>$p_UserName));
		}else{
			db::get_row_count('manage_sales', "UserName='{$p_UserName}'") && db::delete('manage_sales', "UserName='{$p_UserName}'");
		}
		
		manage::operation_log($p_Method==1?'编辑管理员':'添加管理员');
		ly200::e_json('', 1);
	}
		
	public static function manage_del(){
		global $c;
		$UserName=$_GET['u'];
		($_SESSION['Manage']['UserName'] && $UserName && $_SESSION['Manage']['UserName']==$UserName) && ly200::e_json(manage::get_language('manage.manage.del_current_user'));
		$w="UserName='$UserName'";
		db::delete('manage_operation_log', $w);
		db::delete('manage_permit', $w);
		db::delete('manage_sales', $w);
		
		$data=array(
			'Action'	=>	'ueeshop_web_manage_del',
			'UserName'	=>	$UserName
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		
		manage::operation_log('删除管理员');
		ly200::e_json('', 1);
	}
	/*******************************管理员(end)*****************************/

	/*******************************平台授权(start)*****************************/
	public static function set_open_api(){
		global $c;
		$appkey=str::rand_code(20);

		if(!db::get_row_count('config', "GroupId='API' and Variable='AppKey'", 'CId')){
			db::insert('config', array(
					'GroupId'	=>	'API',
					'Variable'	=>	'AppKey',
					'Value'		=>	$appkey
				)
			);
		}else{
			db::update('config', "GroupId='API' and Variable='AppKey'", array('Value'=>$appkey));
		}
		$return_data=array(
			'tips'		=>	$c['manage']['lang_pack']['msg']['reset_success'],
			'appkey'	=>	$appkey
		);

		ly200::e_json($return_data, 1);
	}

	public static function del_open_api(){
		global $c;

		db::delete('config', "GroupId='API' and Variable='AppKey'");
		ly200::e_json('', 1);
	}
	/*******************************平台授权(end)*****************************/

	/*******************************域名绑定(start)*****************************/
	public static function domain_binding_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_DomainBinding = stripslashes($p_DomainBinding);
		if(substr_count($p_DomainBinding, 'www.')){  //过滤带www的域名
			$p_DomainBinding=str_replace('www.', '', $p_DomainBinding);
		}
		$data=array(
			'Action'	=>	'ueeshop_web_get_domain_list',
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
		$website_ary=$result['msg'];
		if($c['FunVersion']>=10){
			$count = 0;
			foreach ((array)$website_ary as $k => $v) {
				if($v[1]) {
					$count++;
				}
			}
		}
		$p_DomainBinding || ly200::e_json(array($c['manage']['lang_pack']['msg']['operating_error']), 0);
		if ($p_Type == 'add') {
			if($c['FunVersion']>=10){
				if(!$count){
					$data=array(
						'Action'	=>	'ueeshop_web_domain_add',
						'AddDomain'	=>	$p_DomainBinding,
					);
				} else {
					ly200::e_json(array($c['manage']['lang_pack']['msg']['operating_error']), 0);
				}
			}else{
				$data=array(
					'Action'	=>	'ueeshop_web_domain_add',
					'AddDomain'	=>	$p_DomainBinding,
				);
			}
		} else {
			$data = array(
				'Action'	=>	'ueeshop_web_domain_del',
				'DelDomain'	=>	$p_DomainBinding,
			);
		}
		$result = ly200::api($data, $c['ApiKey'], $c['api_url']);
		if ($result['ret']==1) {
			if ($p_Type != 'add') {
				$domain_ary = (array)str::json_data(db::get_value('config', 'GroupId="domain_binding" and Variable="domain_list"', 'Value'), 'decode');
				foreach ((array) $domain_ary as $k=>$v) {
					if ($p_DomainBinding == $v) unset($domain_ary[$k]);
				}
				$domain_ary = addslashes(str::json_data($domain_ary));
				db::update('config', 'GroupId="domain_binding" and Variable="domain_list"', array('Value'=>$domain_ary));
			}
			ly200::e_json(array($c['manage']['lang_pack']['msg']['operating_success'], $p_DomainBinding), 1);
		} else {
			ly200::e_json(array(($result['msg'] ? $result['msg'] : $c['manage']['lang_pack']['msg']['operating_error'])), 0);
		}
	}
	public static function domain_binding_verification(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_DomainBinding = stripslashes($p_DomainBinding);
		$domain_ary = (array)str::json_data(db::get_value('config', 'GroupId="domain_binding" and Variable="domain_list"', 'Value'), 'decode');
		$status = 0;
		if ($p_Type == 'CNAME') {
			if ($row = dns_get_record($p_DomainBinding)) {
				foreach((array)$row as $v){
					if ($v['type'] == 'CNAME') $status = 1;
				}
			} 
		} else if (@gethostbyname(ly200::get_domain(0)) == @gethostbyname($p_DomainBinding)) {
			$status = 1;
		}
		if ($status) {
			if (!@in_array($p_DomainBinding, $domain_ary)) {
				$domain_ary[] = $p_DomainBinding;
			}
			$tips = $c['manage']['lang_pack']['set']['domain_binding']['connected'];
		} else {
			foreach ((array) $domain_ary as $k=>$v) {
				if ($p_DomainBinding == $v) unset($domain_ary[$k]);
			}
			$tips = $c['manage']['lang_pack']['set']['domain_binding']['fail'];
		}
		$domain_ary = addslashes(str::json_data($domain_ary));
		db::update('config', 'GroupId="domain_binding" and Variable="domain_list"', array('Value'=>$domain_ary));
		ly200::e_json($tips, $status);
	}
	/*******************************域名绑定(end)*****************************/
}
?>