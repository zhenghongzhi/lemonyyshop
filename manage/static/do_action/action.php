<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class action_module{
	public static function file_upload(){
		global $c;
		/*
		$size=$_POST['size'];
		if($size=='file_upload'){//文件上传
			$status=array('status'=>-1);
			if($filepath=file::file_upload($_FILES['Filedata'], $c['tmp_dir'].'file/')){
				$name=substr($_FILES['Filedata']['name'], 0, strrpos($_FILES['Filedata']['name'], '.'));
				$status=array(
					'status'	=>	1,
					'filepath'	=>	$filepath,
					'name'      =>  $name
				);
			}
			exit(str::json_data($status));
		}else{//图片上传
			$resize_ary=$c['manage']['resize_ary'];
			$is_water=0;
			if(($c['manage']['config']['IsWaterPro'] && $size=='editor') || $size=='products') $is_water=1;
			exit(file::file_upload_swf($c['tmp_dir'].'photo/', $resize_ary, true, $is_water));//暂时把图片都保存到临时文件夹里
		}
		*/
		$size=$_GET['size']?$_GET['size']:$_POST['size'];
		if($size=='file_upload'){//文件上传
			$status=array('status'=>-1);
			if($filepath=file::file_upload($_FILES['Filedata'], $c['tmp_dir'].'file/')){
				$name=substr($_FILES['Filedata']['name'], 0, strrpos($_FILES['Filedata']['name'], '.'));
				$status['files'][0]=array(
					'name'			=>	$name,
					'size'			=>	$_FILES['Filedata']['size'],
					'type'			=>	$_FILES['Filedata']['type'],
					'url'			=>	$filepath,
					'thumbnailUrl'	=>	$filepath,
					'deleteUrl'		=>	'',
					'deleteType'	=>	'DELETE'
				);
			}
			exit(str::json_data($status));
		}else{//图片上传
			$file_size = $_FILES["Filedata"]["size"];
			$file_size > 2097152 && exit(str::json_data(array("files"=>array(0=>array(
				'name'	=>	$_FILES['Filedata']['name'],
				'error'	=>	"file2big"
			)))));//图片太大跳过
			$resize_ary=$c['manage']['resize_ary'];
			$is_water=0;
			if(($c['manage']['config']['IsWaterPro'] && $size=='editor')) $is_water=1;
			exit(file::file_upload_swf($c['tmp_dir'].'photo/', $resize_ary, true, $is_water));//暂时把图片都保存到临时文件夹里
		}
	}
	
	public static function file_upload_plugin(){
		global $c;
		$type_ary=array('image/jpg', 'image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/ico');
		$_type=$_FILES['Filedata']['type'];
		if(!in_array($_type, $type_ary)){
			$_GET['size']='file_upload';
		}
		
		if($_GET['size']=='photo'){ //图片银行
			exit(file::file_upload_swf($c['tmp_dir'].'photo/', '', false));
		}else{
			self::file_upload();
		}
	}
	
	public static function file_upload_ckeditor(){
		global $c;
		file::file_upload_ckeditor($c['manage']['upload_dir'].'file/');
	}
	
	public static function file_del(){	//删除单个图片
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if(is_file($c['root_path'].$g_PicPath)){
			$ext_name=file::get_ext_name($g_PicPath);
			if(is_file($c['root_path'].$g_PicPath.".120x120.{$ext_name}")){//检查是否带有图片管理的缩略图
				file::del_file($g_PicPath.".120x120.{$ext_name}");
			}
			file::del_file($g_PicPath);
		}
		ly200::e_json('', 1);
	}
	
	public static function file_clear_cache(){//清空tmp/cache缓存
		global $c;
		file::del_dir($c['tmp_dir'].'cache/');
		file::del_dir($c['tmp_dir'].'manage/');
		ly200::e_json('', 1);
	}
	
	public static function message_orders_view(){//加载订单消息详情
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$p_MId=(int)$p_MId;
		$p_OId=(int)$p_OId;
		if($p_MId){//MId查询条件
			$msg_row=str::str_code(db::get_one('user_message', "Module='orders' and MId='$p_MId'"));
		}else{//OId查询条件
			$msg_row=str::str_code(db::get_one('user_message', "Module='orders' and Subject='$p_OId'", '*', 'MId desc'));
			if(!$msg_row){//没有订单询盘的记录，立即创建新的
				$orders_row=db::get_one('orders', "OId='$p_OId'");
				$data=array(
					'UserId'	=>	$orders_row['UserId'],
					'Module'	=>	'orders',
					'Subject'	=>	$p_OId,
					'Content'	=>	'No#'.$p_OId,
					'PicPath'	=>	'',
					'IsRead'	=>	1,
					'AccTime'	=>	$c['time'],
				);
				db::insert('user_message', $data);
				$p_MId=db::get_insert_id();
				$msg_row=$data;
			}else{
				$p_MId=$msg_row['MId'];
			}
		}
		//获取会员信息
		$UserId=(int)$msg_row['UserId'];
		$uesr_row=str::str_code(db::get_one('user', "UserId='$UserId'"));
		$reply_row=str::str_code(db::get_all('user_message_reply', "MId='$p_MId'", '*', 'RId asc'));
		(int)$msg_row['IsRead']==0 && db::update('user_message', "MId='$p_MId'", array('IsRead'=>1));
		//获取订单信息
		$OId=$msg_row['Subject'];
		$orders_row=db::get_one('orders', "OId='$OId'");
		//返回数据
		$ReturanData=array(
			'OId'	=>	$OId,
			'MId'	=>	$p_MId,
			'Email'	=>	$uesr_row['Email'],
		);
		if($msg_row){
			$ReturanData['Reply'][0]=array(
				'MId'		=>	$msg_row['MId'],
				'UserId'	=>	$msg_row['UserId'],
				'Content'	=>	$msg_row['Content'],
				'PicPath'	=>	$msg_row['PicPath'],
				'Time'		=>	date('Y-m-d H:i:s', $msg_row['AccTime'])
			);
		}
		foreach($reply_row as $k=>$v){
			$ReturanData['Reply'][$k+1]=$v;
			$ReturanData['Reply'][$k+1]['Time']=date('Y-m-d H:i:s', $v['AccTime']);
		}
		ly200::e_json($ReturanData, 1);
	}
	
	public static function message_orders_reply(){//回复订单消息
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_MId=(int)$p_MId;
		$data=array(
			'MId'		=>	$p_MId,
			'Content'	=>	$p_Message,
			'PicPath'	=>	$p_MsgPicPath,
			'AccTime'	=>	$c['time']
		);
		db::insert('user_message_reply', $data);
		db::update('user_message', "MId='$p_MId'", array('IsReply'=>1));
		manage::operation_log('回复站内信');
		$data['Time']=date('Y-m-d H:i:s', $data['AccTime']);
		ly200::e_json($data, 1);
	}
	
	public static function box_drop_double(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Html		= '';
		$Start		= 0;
		$Count		= 20;//查询总数上限
		$p_Value	= (int)$p_Value;
		$p_Top		= (int)$p_Top;
		$p_All		= (int)$p_All;
		$p_CheckAll	= (int)$p_CheckAll;
		$p_Checkbox	= (int)$p_Checkbox;
		$p_Start	= (int)$p_Start;
		$lang		= $c['manage']['web_lang'];
		$lang_all	= $c['manage']['lang_pack']['global']['all'];
		$lang_return= $c['manage']['lang_pack']['global']['return'];
		$DataAry	= array('Value'=>'', 'Type'=>$p_Type, 'Table'=>$p_Table, 'Top'=>1);
		if($p_Start>0){
			$Start=$p_Start*$Count;
		}
		if($p_Checkbox==1 && $p_CheckAll==1 && $p_Start==0){
			$Html.='<div class="item btn_check_all"><span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" /></span></span><span class="item_name">全选</span></div>';
		}
		if($p_Table=='products_category'){
			//产品分类
			if($p_Value){//下一级产品分类列表
				$_UId=db::get_value('products_category', "CateId='{$p_Value}'", 'UId');
				$UpCateId=category::get_FCateId_by_UId($_UId);//上一级分类ID
				$UId=category::get_UId_by_CateId($p_Value);
				$row=db::get_limit('products_category', "UId='{$UId}'", "CateId, Category{$lang}, SubCateCount", $c['my_order'].'CateId asc', $Start, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Category'.$lang], $v['CateId'], $p_Type, $p_Table, $p_Checkbox, $v['SubCateCount']);
				}
				if($UpCateId>0){
					$DataAry['Value']=$UpCateId;
				}else{
					$DataAry['Value']='category';
				}
				$DataAry['Top']=0;
			}else{//一级分类
				$row=db::get_limit('products_category', 'UId="0,"', "CateId, Category{$lang}, SubCateCount", $c['my_order'].'CateId asc', $Start, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Category'.$lang], $v['CateId'], $p_Type, $p_Table, $p_Checkbox, $v['SubCateCount']);
				}
			}
		}elseif($p_Table=='products'){
			//产品
			$row=db::get_limit('products', '1', "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc', $Start, $Count);
			foreach($row as $k=>$v){
				$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
				$Html.=manage::box_drop_double_option($v['Name'.$lang], $v['ProId'], $p_Type, $p_Table, $p_Checkbox, 0, '<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
			}
		}elseif($p_Table=='products_tags'){
			//标签
			$row=db::get_limit('products_tags', '1', "TId, Name{$lang}", $c['my_order'].'TId desc', $Start, $Count);
			foreach($row as $k=>$v){
				$Html.=manage::box_drop_double_option($v['Name'.$lang], $v['TId'], $p_Type, $p_Table, $p_Checkbox);
			}
		}elseif($p_Table=='photo_category'){
			//图片管理分类
			if($p_Value && $p_Value!='photo_category'){//下一级产品分类列表
				$_UId=db::get_value('photo_category', "CateId='{$p_Value}'", 'UId');
				$UpCateId=category::get_FCateId_by_UId($_UId);//上一级分类ID
				$UId=category::get_UId_by_CateId($p_Value);
				$row=db::get_limit('photo_category', "UId='{$_UId}{$p_Value},'", 'CateId, Category, SubCateCount', $c['my_order'].'CateId asc', $Start, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Category'], $v['CateId'], $p_Type, $p_Table, $p_Checkbox, $v['SubCateCount']);
				}
				if($UpCateId>0){
					$DataAry['Value']=$UpCateId;
				}else{
					$DataAry['Value']='photo_category';
				}
			}else{//一级分类
				$row=db::get_limit('photo_category', 'UId="0,"', "CateId, Category, SubCateCount", $c['my_order'].'CateId asc', $Start, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Category'], $v['CateId'], $p_Type, $p_Table, $p_Checkbox, $v['SubCateCount']);
				}
			}
		}elseif($p_Table=='user_level'){
			//会员等级
			$row=db::get_limit('user_level', '1', "LId, Name{$lang}", 'LId asc', $Start, $Count);
			$p_All==1 && $p_Start==0 && $Html.=manage::box_drop_double_option($lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_level'], -1, $p_Type, $p_Table, $p_Checkbox);//所有等级
			foreach($row as $k=>$v){
				$Html.=manage::box_drop_double_option($v['Name'.$lang], $v['LId'], $p_Type, $p_Table, $p_Checkbox);
			}
		}elseif($p_Table=='user'){
			//会员
			$row=db::get_limit('user', '1', 'UserId, Email', 'UserId desc', $Start, $Count);
			$p_All==1 && $p_Start==0 && $Html.=manage::box_drop_double_option($lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_user'], -1, $p_Type, $p_Table, $p_Checkbox);//所有会员
			foreach($row as $k=>$v){
				$Html.=manage::box_drop_double_option($v['Email'], $v['UserId'], $p_Type, $p_Table, $p_Checkbox);
			}
		}elseif($p_Table=='newsletter'){
			//订阅
			$row=db::get_limit('user', 'IsNewsletter=1', 'UserId, Email', 'UserId desc', $Start, $Count);
			foreach($row as $k=>$v){
				$Html.=manage::box_drop_double_option($v['Email'], $v['UserId'], $p_Type, $p_Table, $p_Checkbox);
			}
		}else{
			//单页
			$row=db::get_limit('article_category', '1', "CateId, Category{$lang}", 'CateId asc', $Start, $Count);
			foreach($row as $k=>$v){
				$Html.=manage::box_drop_double_option($v['Category'.$lang], $v['CateId'], $p_Type, $p_Table, $p_Checkbox);
			}
		}
		ly200::e_json(array(
			'Html'=>$Html,
			'Back'=>array('Value'=>$DataAry['Value'], 'Type'=>$DataAry['Type'], 'Table'=>$DataAry['Table'], 'Top'=>$DataAry['Top']),
			'Count'=>($Html?$k+1:0),
			'Start'=>$p_Start
		), 1);
	}
	
	public static function check_drop_double(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Html		= '';
		$Count		= 20;//查询总数上限
		$p_Keyword	= trim($p_Keyword);
		$p_Checkbox	= (int)$p_Checkbox;
		$lang		= $c['manage']['web_lang'];
		$Data		= str::json_data(str::attr_decode(stripslashes($p_Data)), 'decode');
		!$Data && $Data=str::json_data(stripslashes($p_Data), 'decode');
		foreach($Data as $k=>$v){
			if($v['Table']=='products_category'){
				//产品分类
				$row=db::get_limit('products_category', "Category{$lang} like '%$p_Keyword%'", "CateId, Category{$lang}", $c['my_order'].'CateId asc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Category'.$lang].' (分类)', $v['CateId'], 'category', $v['Table'], $p_Checkbox);
				}
			}elseif($v['Table']=='products'){
				//产品
				$row=db::get_limit('products', "Name{$lang} like '%$p_Keyword%'", "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc', 0, $Count);
				foreach($row as $k=>$v){
					$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
					$Html.=manage::box_drop_double_option($v['Name'.$lang].' (产品)', $v['ProId'], 'products', $v['Table'], $p_Checkbox, 0, '<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
				}
			}elseif($v['Table']=='products_tags'){
				//标签
				$row=db::get_limit('products_tags', "Name{$lang} like '%$p_Keyword%'", "TId, Name{$lang}", $c['my_order'].'TId desc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Name'.$lang].' (标签)', $v['TId'], 'tags', $v['Table'], $p_Checkbox);
				}
			}elseif($v['Table']=='user_level'){
				//会员等级
				$row=db::get_limit('user_level', "Name{$lang} like '%$p_Keyword%'", "LId, Name{$lang}", 'LId asc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Name'.$lang].' (等级)', $v['LId'], 'level', $v['Table'], $p_Checkbox);
				}
			}elseif($v['Table']=='user'){
				//会员
				$row=db::get_limit('user', "Email like '%$p_Keyword%'", 'UserId, Email', 'UserId desc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Email'].' (会员)', $v['UserId'], 'user', $v['Table'], $p_Checkbox);
				}
			}elseif($v['Table']=='newsletter'){
				//订阅
				$row=db::get_limit('newsletter', "Email like '%$p_Keyword%'", 'NId, Email', 'NId desc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Email'].' (订阅)', $v['NId'], 'newsletter', $v['Table'], $p_Checkbox);
				}
			}elseif($v['Table']=='article'){
				//单页
				$row=db::get_limit('article', "Title{$lang} like '%$p_Keyword%'", "AId, Title{$lang}", 'AId desc', 0, $Count);
				foreach($row as $k=>$v){
					$Html.=manage::box_drop_double_option($v['Title'.$lang].' (单页)', $v['AId'], 'article', $v['Table'], $p_Checkbox);
				}
			}
		}
		if($Html==''){
			foreach($Data as $k=>$v){
				if($v['Type']=='user'){
					//会员
					$row=db::get_limit('user', "Email like '%$p_Keyword%'", 'UserId, Email', 'UserId desc', 0, $Count);
					foreach($row as $k=>$v){
						$Html.=manage::box_drop_double_option($v['Email'], $v['UserId'], 'user', $v['Table'], $p_Checkbox);
					}
				}elseif($v['Type']=='products'){
					//产品
					$row=db::get_limit('products', "Name{$lang} like '%$p_Keyword%'", "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc', 0, $Count);
					foreach($row as $k=>$v){
						$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
						$Html.=manage::box_drop_double_option($v['Name'.$lang].' (产品)', $v['ProId'], 'products', $v['Table'], $p_Checkbox, 0, '<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
					}
				}
				break;
			}
		}
		if($p_Checkbox==0){//仅有单选才可以添加选项
			$Html=manage::box_drop_double_option("添加 $p_Keyword", $p_Keyword, 'add', $v['Table']).$Html;
		}
		ly200::e_json(array('Html'=>$Html, 'Count'=>($Html?$k+1:0)), 1);
	}
	
	public static function check_option(){//查询标签选项
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Keyword	= trim($p_Keyword);	//搜索关键词
		$p_Type		= trim($p_Type);	//搜索类型
		$count		= 20;				//结果数量
		$ReturnData	= array();			//返回结果
		if($p_Type=='email'){
			//邮箱
			$row=db::get_limit('user', "Email like '%$p_Keyword%'", 'UserId, Email', 'UserId desc', 0, $count);
			foreach($row as $k=>$v){
				$ReturnData['Option'][$v['UserId']]=$v['Email'];
			}
		}elseif($p_Type=='tags'){
			//标签
			$fixed_tags_ary=array('IsIndex'=>'is_index', 'IsNew'=>'is_new', 'IsHot'=>'is_hot', 'IsBestDeals'=>'is_best_deals');
			foreach($fixed_tags_ary as $k=>$v){//固有标签
				$ReturnData['Fixed'][$k]=$c['manage']['lang_pack']['products']['products'][$v];
			}
			$row=db::get_limit('products_tags', "Name_en like '%$p_Keyword%'", 'TId, Name_en', 'TId desc', 0, $count);
			foreach($row as $k=>$v){
				$ReturnData['Option'][$v['TId']]=$v['Name_en'];
			}
		}elseif($p_Type=='level'){
			//等级
			
		}
		ly200::e_json($ReturnData, 1);
	}
	
	public static function translation(){ //翻译器
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		!(int)$c['FunVersion'] && ly200::e_json('no_permit');
		$target=array();
		foreach($p_language as $k=>$v){
			$v=str::ary_format($v, 1);
			foreach($v as $v1){
				$target[$v1][]=$k;
			}
		}
		$translation=array();
		foreach($target as $k=>$v){
			$text=array();
			foreach($v as $v1){
				$text[$v1]=stripslashes($p_text[$v1]);
			}
			$result=manage::google_translation($text, $k);
			implode($result['msg'])=='' && $result['ret']=0;
			$translation[$k]=$result;
		}
		ly200::e_json($translation, 1);
	}
	
	public static function translation_get_chars(){	//翻译剩余的字符数
		global $c;
		!(int)$c['FunVersion'] && ly200::e_json('no_permit');
		$result=ly200::api(array('Action'=>'ueeshop_web_get_translation_chars'), $c['ApiKey'], $c['sync_url']);
		ly200::e_json(sprintf(manage::get_language('global.translation_chars'), '<font class="fc_red">'.(int)$result['msg'].'</font>'), $result['ret']);
	}
	
	public static function ueeshop_tmp_domain(){  //后台临时域名提示弹窗
		global $c;
		if(!substr_count($c['Number'], 'UT') && !substr_count($c['Number'], 'API')){  //正式用户才提示
			$time=db::get_value('manage', 1, 'AccTime', 'AccTime asc');
			if($time<1551283200){  //这个时间之前的更新用户提示    2019/2/28
				setcookie('ueeshop_tmp_domain', 1, $c['time']+3600*24*365);
				ly200::e_json(str_replace('%domain%', $_SERVER['HTTP_HOST'], manage::get_language('global.tmp_domain_alert')), 1);
			}
		}
		ly200::e_json('', 0);
	}

	public static function manage_page_statistics(){  //后台临时域名提示弹窗
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if (db::get_row_count('manage_page_statistics', "StatisticalID='{$p_StatisticalID}'")) {
			$StartTime = (int)db::get_value('manage_page_statistics', "StatisticalID='{$p_StatisticalID}'", 'AccTime');
			if ($StartTime + 3600 < $c['time']) {
				ly200::e_json('', 0);
			}
			$data = array(
				'StayTime' => $c['time'],
			);
			db::update('manage_page_statistics', "StatisticalID='{$p_StatisticalID}'", $data);
		} else {
			$data = array(
				'StatisticalID' => $p_StatisticalID,
				'Url' 			=> $p_Url,
				'StayTime' 		=> $c['time'],
				'AccTime' 		=> $c['time'],
			);
			db::insert('manage_page_statistics', $data);
		}
		ly200::e_json('', 1);
	}
}
?>