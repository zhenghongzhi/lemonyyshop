<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class products_module{
	public static function products_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$mLanguage=$c['manage']['web_lang'];
		$ProId=(int)$p_ProId;
		$AddProId=(int)$p_AddProId;
		//基本信息
		$CateId=(int)$p_CateId;
		//上传图片
		$PicPath=$p_PicPath;
		//其他参数
		$p_Cubage=@implode(',', $p_Cubage);
		$p_MOQ=(int)$p_MOQ;
		$p_MOQ=(1<$p_MOQ && (($p_MOQ<=$p_Stock && !$p_IsCombination) || $p_IsCombination))?$p_MOQ:1;
		$p_Stock=(int)$p_Stock;
		$p_SoldOut=(int)$p_SoldOut;//0:上架 1:下架 2:脱销
		$p_SoldStatus=(int)$p_SoldStatus;
		$p_SoldStatus==0 && $p_SoldOut==0 && $p_SoldOut=2; //库存为0 不允许购买
		$p_IsSoldOut=(int)$p_IsSoldOut;//定时上架
		$p_SoldOut!=1 && $p_IsSoldOut=0;
		$SoldOutTime=@explode('/', $p_SoldOutTime);
		$SStartTime=@strtotime($SoldOutTime[0]);
		$SEndTime=@strtotime($SoldOutTime[1]);
		$Description=$p_Description;
		
		//产品设置配置
		$cfg_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
		foreach((array)$cfg_row as $v){
			$cfg_ary[$v['GroupId']][$v['Variable']]=$v['Value'];
		}
		$used_row=str::json_data(htmlspecialchars_decode($cfg_ary['products_show']['Config']), 'decode');
		
		//产品编号自动排序
		$Prefix='';
		$Number=$p_Number;
		//if($cfg_ary['products_show']['NumberSort'] && $cfg_ary['products_show']['myorder']){
		//	$Prefix=$cfg_ary['products_show']['myorder'];
		//}
		if($p_Prefix) $Prefix=$p_Prefix;
		
		if($p_Number && db::get_row_count('products', "ProId!='$ProId' and Prefix='$Prefix' and Number='$Number'")) ly200::e_json(manage::get_language('products.products.number_tips'));
		if(!count($PicPath)) ly200::e_json(manage::get_language('products.products.pic_tips'));
		
		//扩展分类
		if($p_ExtCateId){
			$p_ExtCateId=array_unique($p_ExtCateId);
			if($zero=array_search(0, $p_ExtCateId)){
				unset($p_ExtCateId[$zero]);
			}
			$ExtCateId=','.implode(',',$p_ExtCateId).',';
		}
		
		//图片上传
		$ImgPath=array();
		$resize_ary=$c['manage']['resize_ary']['products'];
		$save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($save_dir);
		foreach((array)$PicPath as $k=>$v){
			if(!is_file($c['root_path'].$v)) continue;
			$ImgPath[]=file::photo_tmp_upload($v, $save_dir, $resize_ary);
		}
		if(!count($ImgPath)) ly200::e_json(manage::get_language('products.products.pic_tips'));
		$pro_img_ary=array();
		$pro_img_row=db::get_one('products', "ProId='{$ProId}'", $c['prod_image']['sql_field']);
		foreach((array)$pro_img_row as $v){
			if($v) $pro_img_ary[]=$v;
		}
		if($c['manage']['config']['IsWater']){//更新水印图片
			foreach((array)$ImgPath as $k=>$v){
				if(@in_array($v, (array)$pro_img_ary)) continue; //已经存在图片不更新水印
				$water_ary=array($v);
				$ext_name=file::get_ext_name($v);
				@copy($c['root_path'].$v.".default.{$ext_name}", $c['root_path'].$v);//覆盖大图
				if($c['manage']['config']['IsThumbnail']){//缩略图加水印
					img::img_add_watermark($v);
					$water_ary=array();
				}
				foreach((array)$resize_ary as $v2){
					if($v=='default') continue;
					$size_w_h=explode('x', $v2);
					$resize_img=$v;
					$resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
				}
				foreach((array)$water_ary as $v2){
					img::img_add_watermark($v2);
				}
			}
		}
		foreach((array)$ImgPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			foreach((array)$resize_ary as $v2){
				if(!is_file($c['root_path'].$v.".{$v2}.{$ext_name}")){
					$size_w_h=explode('x', $v2);
					$resize_img=$v;
					$resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
				}
			}
			if(!is_file($c['root_path'].$v.".default.{$ext_name}")){
				@copy($c['root_path'].$v, $c['root_path'].$v.".default.{$ext_name}");
			}
		}
		
		//批发价
		$wholesale_ary=array();
		foreach((array)$p_Qty as $k=>$v){
			$Qty=(int)$p_Qty[$k];
			$Price=(float)$p_Price[$k];
			if($Qty && $Price) $wholesale_ary[$Qty]=$Price;
		}
		ksort($wholesale_ary);//从小到大排序，防止故意乱填
		$Wholesale=addslashes(str::json_data(str::str_code($wholesale_ary, 'stripslashes')));
		
		//单位
		$Unit=trim($p_UnitValue);
		if($Unit){
			$unit_list=str::json_data(htmlspecialchars_decode(db::get_value('config', 'GroupId="products" and Variable="Unit"', 'Value')), 'decode');
			if(count($unit_list)){//检查有没有空余没用的单位
				foreach((array)$unit_list as $k=>$v){
					$pro_count=db::get_row_count('products', "Unit='$v' group by ProId", 'ProId');
					if($pro_count==0) unset($unit_list[$k]);//没有相对关联的产品，直接删掉
				}
			}
			if(!in_array($Unit, $unit_list)){ //追加新的单位
				$unit_list[]=$Unit;
			}
			$json_unit=addslashes(str::json_data(str::str_code($unit_list, 'stripslashes')));
			manage::config_operaction(array('Unit'=>$json_unit), 'products');
		}
		
		//单位 和 SKU 显示
		$p_OpenParameter=@implode('|', $p_OpenParameter);
		
		//平台导流
		$platform_ary=array('amazon', 'amazon_us', 'amazon_au', 'amazon_br', 'amazon_ca', 'amazon_fr', 'amazon_de', 'amazon_in', 'amazon_it', 'amazon_jp', 'amazon_mx', 'amazon_nl', 'amazon_es', 'amazon_tr', 'amazon_uk', 'aliexpress', 'wish', 'ebay', 'alibaba');
		$platform=array();
		foreach((array)$platform_ary as $k=>$v){
			foreach((array)$c['manage']['config']['Language'] as $lang){
				$Url=${'p_Platform_'.$v.'_Url_'.$lang};
				$Name=${'p_Platform_'.$v.'_Name_'.$lang};
				$Url=@array_filter($Url);
				if(!$Url) continue;
				//目前只支持1个，暂时不需要循环
				$platform[$v][0]['Name_'.$lang]=$Name[0];
				$platform[$v][0]['Url_'.$lang]=$Url[0];
			}
		}
		
		//标签
		$fixed_tags=array('IsIndex', 'IsNew', 'IsHot', 'IsBestDeals');//固有标签选项
		if($p_tagsOption){
			//初始化
			$tags_ary=$insert_ary=$new_tid_ary=array();
			$tags_row=db::get_all('products_tags', 1);
			foreach((array)$tags_row as $v){ $tags_ary[]=$v['TId']; }
			//标签选项
			foreach((array)$p_tagsOption as $k=>$v){
				if(in_array($v, $fixed_tags) && $p_tagsCurrent){//固有标签
					$$v=(in_array($v, $p_tagsCurrent)?1:0);
					continue;
				}
				if(!in_array($v, $tags_ary)){//标签没有保存
					$insert_ary[$v]['Name'.$mLanguage]=$p_tagsName[$k];
				}
			}
			if(count($insert_ary)){
				foreach((array)$insert_ary as $k=>$v){
					if(in_array($k, $fixed_tags)) continue;//固有标签不能添加
					db::insert('products_tags', $v);
					$TId=db::get_insert_id();
					$new_tid_ary[$k]=$TId;
				}
				manage::operation_log('添加产品标签');
			}
			//记录到产品数据
			foreach((array)$p_tagsCurrent as $k=>$v){
				if(in_array($v, $fixed_tags)){//固有标签
					unset($p_tagsCurrent[$k]);
				}else{//自定义标签
					$p_tagsCurrent[$k]=$new_tid_ary[$v]?$new_tid_ary[$v]:$v;
				}
			}
			$Tags='|'.@implode('|', $p_tagsCurrent).'|';
			//检查闲置的标签
			$delete_ary=array();
			foreach((array)$tags_row as $v){
				if(!in_array($v['TId'], $p_tagsOption)){//标签已经被踢走
					$delete_ary[]=$v['TId'];
				}
			}
			if(count($delete_ary)){
				foreach((array)$delete_ary as $k=>$v){
					$pro_count=db::get_row_count('products', "Tags like '%|{$v['TId']}|%'", 'ProId');
					if($pro_count>0) unset($delete_ary[$k]);//还有关联的产品，不能删掉
				}
				$delete=implode(',', $delete_ary);
				if($delete){//删掉闲置没用的属性
					db::delete('products_tags', "TId in ($delete)");
					manage::operation_log('删除产品标签');
				}
			}
		}
		
		//保存其它语言版的信息
		$products_seo=db::get_one('products_seo', "ProId='{$ProId}'");
		foreach((array)$c['manage']['config']['Language'] as $lang){
			$_POST['SeoKeyword_'.$lang]=addslashes($products_seo['SeoKeyword_'.$lang]);
		}


		//SEO关键词
		$keys_str='';
		if($p_keysOption){
			$keys_ary=array();
			foreach((array)$p_keysOption as $k=>$v){
				if($p_keysCurrent[$k]){
					$keys_ary[]=$p_keysName[$k];
				}
			}
			$keys_str=implode(',', $keys_ary);
		}
		$_POST['SeoKeyword'.$c['manage']['web_lang']]=$keys_str;

		//自定义链接
		if($p_PageUrl){
			$un_ary=array(ly200::get_domain().'/', '.html');
			$ProId && $un_ary[]="-p{$ProId}";
			$p_PageUrl=str_replace($un_ary, '', $p_PageUrl);
		}
		$data=array(
			'CateId'					=>	$CateId,
			'ExtCateId'					=>	$ExtCateId,
			'Prefix'					=>	$Prefix,
			'Number'					=>	$Number,
			'SKU'						=>	$p_SKU,
			'Business'					=>	(int)$p_Business,
			'Price_0'					=>	(float)$p_Price_0,
			'Price_1'					=>	(float)$p_Price_1,
		    'price_rate'				=>	($p_price_rate/100),
			'PicPath_0'					=>	$ImgPath[0],
			'PicPath_1'					=>	$ImgPath[1],
			'PicPath_2'					=>	$ImgPath[2],
			'PicPath_3'					=>	$ImgPath[3],
			'PicPath_4'					=>	$ImgPath[4],
			'PicPath_5'					=>	$ImgPath[5],
			'PicPath_6'					=>	$ImgPath[6],
			'PicPath_7'					=>	$ImgPath[7],
			'PicPath_8'					=>	$ImgPath[8],
			'PicPath_9'					=>	$ImgPath[9],
            'PicPath_10'				=>	$ImgPath[10],
            'PicPath_11'				=>	$ImgPath[11],
            'PicPath_12'				=>	$ImgPath[12],
            'PicPath_13'				=>	$ImgPath[13],
            'PicPath_14'				=>	$ImgPath[14],
			'Weight'					=>	(float)$p_Weight,
			'Cubage'					=>	$p_Cubage,
			'OpenParameter'				=>	$p_OpenParameter,
			'Unit'						=>	$Unit,
			'MOQ'						=>	$p_MOQ,
			'Stock'						=>	(int)$p_Stock,
			'StockOut'					=>	(int)$p_StockOut,
			'IsCombination'				=>	(int)$p_IsCombination,
			'IsOpenAttrPrice'			=>	(int)$p_IsOpenAttrPrice,
			'SoldOut'					=>	$p_SoldOut,
			'IsSoldOut'					=>	$p_IsSoldOut,
			'SStartTime'				=>	$SStartTime,
			'SEndTime'					=>	$SEndTime,
			'SoldStatus'				=>	(int)$p_SoldStatus,
			'TId'						=>	(int)$p_TId,
			'IsFreeShipping'			=>	(int)$p_IsFreeShipping,
			'IsVolumeWeight'			=>	(int)$p_IsVolumeWeight,
			'IsNew'						=>	(int)$IsNew,
			'IsHot'						=>	(int)$IsHot,
			'IsBestDeals'				=>	(int)$IsBestDeals,
			'IsIndex'					=>	(int)$IsIndex,
			'PackingStart'				=>	(int)$p_PackingStart,
			'PackingQty'				=>	(int)$p_PackingQty,
			'PackingWeight'				=>	(float)$p_PackingWeight,
			'PageUrl'					=>	ly200::str_to_url($p_PageUrl),
			'Tags'						=>	$Tags
		);
		
		if(in_array('wholesale', $c['manage']['plugins']['Used'])){//开启了批发价APP
			$data['Wholesale']=$Wholesale;
			$data['MOQ']=$p_MOQ;
			$data['MaxOQ']=$p_MaxOQ;
		}
		
		if(in_array('platform', $c['manage']['plugins']['Used'])){//开启了平台导流APP
			$data['Platform']=str::json_data($platform);
		}
		
		if($ProId){
			$data['EditTime']=$c['time'];
			db::update('products', "ProId='$ProId'", $data);
			if(!db::get_row_count('products_seo', "ProId='$ProId'")){
				db::insert('products_seo', array('ProId'=>$ProId));
			}
			if(!db::get_row_count('products_description', "ProId='$ProId'")){
				db::insert('products_description', array('ProId'=>$ProId));
			}
			if(!db::get_row_count('products_development', "ProId='$ProId'")){//产品功能关联表
				db::insert('products_development', array('ProId'=>$ProId));
			}
			manage::operation_log('修改产品');
		}else{
			$data['AccTime']=$c['time'];
			$data['IsDesc']='["1","1"]';
			db::insert('products', $data);
			$ProId=db::get_insert_id();
			//db::insert('products_seo', array('ProId'=>$ProId));
			if(!$_SESSION['AddProduct'][$AddProId]['Keyword']){
				db::insert('products_seo', array('ProId'=>$ProId));
			}
			db::insert('products_development', array('ProId'=>$ProId));//产品功能关联表
			db::insert('products_description', array('ProId'=>$ProId));
			manage::operation_log('添加产品');
		}
		
		//添加产品，“颜色图片”和“选项卡”的数据转移
		if($AddProId>0 && $_SESSION['AddProduct'][$AddProId]){
			if($_SESSION['AddProduct'][$AddProId]['Color']){//颜色图片
				$w=@implode(',', $_SESSION['AddProduct'][$AddProId]['Color']);
				$w && db::update('products_color', "CId in ($w)", array('ProId'=>$ProId));
			}
			if($_SESSION['AddProduct'][$AddProId]['Tab']){//选项卡
				$w=@implode(',', $_SESSION['AddProduct'][$AddProId]['Tab']);
				$w && db::update('products_tab', "TId in ($w)", array('ProId'=>$ProId));
			}
			if($_SESSION['AddProduct'][$AddProId]['Attr']){//属性
				$w=@implode(',', $_SESSION['AddProduct'][$AddProId]['Attr']);
				$w && db::update('products_attribute', "AttrId in ($w)", array('CateId'=>"|{$CateId}|"));
			}
			if($_SESSION['AddProduct'][$AddProId]['Keyword']){//SEO关键词
				db::update('products_seo', 'SId="'.$_SESSION['AddProduct'][$AddProId]['Keyword'].'"', array('ProId'=>$ProId));
			}
			if($_SESSION['AddProduct'][$AddProId]['SelectedAttrId']){//颜色图片
				$w=@implode(',', $_SESSION['AddProduct'][$AddProId]['SelectedAttrId']);
				$w && db::update('products_selected_attr', "SId in ($w)", array('ProId'=>$ProId));
			}
			unset($_SESSION['AddProduct'][$AddProId]);//删掉临时数据
		}
		
		//规格属性 or 普通属性（选项）
		if($p_AttrOption){
			//初始化
			$attr_ary=$all_attr_id_ary=array();
			foreach((array)$p_AttrOption as $k=>$v){
				$attr_ary[$p_AttrParent[$k]][]=array('Name'=>$p_AttrName[$k], 'Id'=>$v);
				$all_attr_id_ary[$v]=$p_AttrParent[$k];
			}
			//检查属性
			$new_attr_id_ary=$new_vid_ary=array();
			foreach((array)$p_AttrTitle as $k=>$v){//属性名称
				if($k=='Overseas') continue;//海外仓排除
				$IsCart=(int)$p_AttrCart[$k];//是否为规格属性
				$IsNewCateId=0;
				$AttrId=$k;
				$ParentId=db::get_value('products_attribute', "AttrId='$AttrId'", 'CateId');//当前属性归属的分类ID
				if(strstr($k, 'ADD:')){//新添加的属性
					$AttrId=0;
				}elseif(!strstr($ParentId, "|{$CateId}|")){//当前产品分类没有该属性，把当前产品分类记录到该属性上
					$IsNewCateId=1;
				}
				//产品属性
				//针对自身属性
				if(!$AttrId){
					$attr_data=array(
						'CateId'	=>	"|{$CateId}|",
						'CartAttr'	=>	$IsCart,
						'Type'		=>	1
					);
					if($IsCart==1){ //规格属性
						foreach((array)$v as $lang=>$v2){
							$attr_data["Name_$lang"]=addslashes($v2);
						}
					}
					db::insert('products_attribute', $attr_data);
					$AttrId=db::get_insert_id();
					$new_attr_id_ary[$k]=$AttrId;//记录新属性
					manage::operation_log('添加产品属性');
				}else{
					if($IsNewCateId==1){//注入新分类
						db::update('products_attribute', "AttrId='$AttrId'", array('CateId'=>$ParentId.$CateId.'|'));
					}
				}
				//针对当前产品
				if(db::get_row_count('products_selected_attr', "ProId='$ProId' and AttrId='$AttrId'", 'SId')){
					db::update('products_selected_attr', "ProId='$ProId' and AttrId='$AttrId'", array('IsUsed'=>1));
				}else{
					db::insert('products_selected_attr', array(
						'ProId'		=>	$ProId,
						'AttrId'	=>	$AttrId,
						'IsUsed'	=>	1
					));
				}
				//检查属性选项
				if($attr_ary[$k]){
					$insert_ary=$update_ary=$delete_ary=$vid_ary=$vid_name_ary=array();
					$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId='{$AttrId}'", '*', $c['my_order'].'VId asc'));
					foreach((array)$value_row as $k2=>$v2){
						$vid_ary[$k2]=$v2['VId'];
						$vid_name_ary[$v2['Value'.$mLanguage]]=$v2['VId'];
					}
					unset($value_row);
					$i=1;
					foreach((array)$attr_ary[$k] as $k2=>$v2){
						$v2['Name']=str_replace('	', ' ', $v2['Name']);//把制表符换成空格（防止导出后自动变成空格）
						if(in_array($v2['Id'], $vid_ary)){//已存在
							//!$update_ary[$v2['Id']]['MyOrder'] && $update_ary[$v2['Id']]['MyOrder']=$i;//未设置排序，设置一下
							!$update_ary[$v2['Id']]['AttrId'] && $update_ary[$v2['Id']]['AttrId']=$AttrId;//未设置AttrId，设置一下
						}else{//新选项
							$insert_ary[$v2['Id']]['Value'.$mLanguage]=$v2['Name'];
							//!$insert_ary[$v2['Id']]['MyOrder'] && $insert_ary[$v2['Id']]['MyOrder']=$i;//未设置排序，设置一下
							!$insert_ary[$v2['Id']]['AttrId'] && $insert_ary[$v2['Id']]['AttrId']=$AttrId;//未设置AttrId，设置一下
						}
						++$i;
					}
					if(count($update_ary)){
						foreach((array)$update_ary as $k2=>$v2){
							db::update('products_attribute_value', "VId='$k2'", $v2);
						}
						manage::operation_log('修改产品属性选项');
					}
					if(count($insert_ary)){
						foreach((array)$insert_ary as $k2=>$v2){
							db::insert('products_attribute_value', $v2);
							$VId=db::get_insert_id();
							$new_vid_ary[$k2]=$VId;//记录新选项
						}
						manage::operation_log('添加产品属性选项');
					}
					foreach((array)$vid_ary as $v){
						if(!$update_ary[$v]){//如果在更新数组里，没有相应的VId数值，就证明该选项已经被客户主动删掉
							$delete_ary[]=$v;
						}
					}
					if(count($delete_ary)){//删掉被客户主动删除的选项数据
						$w=implode(',', $delete_ary);
						db::update('products_selected_attribute', "ProId='$ProId' and VId in ($w)", array('IsUsed'=>2));//记录到隐藏状态
					}
				}
			}
			//检查属性选项勾选
			if($p_AttrCurrent){
				//勾选情况
				$insert_ary=$update_ary=$value_ary=array();
				$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'", '*', 'SeleteId asc'));
				foreach((array)$selected_row as $v){
					$value_ary['Id'][]=$v['VId'];//属性选项ID
					$v['AttrId']==0 && $v['VId']==0 && $v['OvId']>0 && $value_ary['Overseas'][]=$v['OvId'];//属性选项ID 发货地
				}
				unset($selected_row);
				$i=1;
				foreach((array)$p_AttrCurrent as $k=>$v){
					if($all_attr_id_ary[$v]=='Overseas'){//发货地
						$_AttrId=0;
						$VId=0;
						$OvId=str_replace('Ov:', '', $v);
					}else{//规格属性
						$_AttrId=$all_attr_id_ary[$v];
						$_AttrId=($new_attr_id_ary && $new_attr_id_ary[$_AttrId]?$new_attr_id_ary[$_AttrId]:$_AttrId);//new? now?
						$VId=($new_vid_ary[$v]?$new_vid_ary[$v]:$v);//new? now?
						$OvId=1;
					}
					if(($VId==0 && @in_array($OvId, $value_ary['Overseas'])) || ($VId>0 && @in_array($VId, $value_ary['Id']))){//已有数据
						$update_ary[$v]['IsUsed']=1;
						$update_ary[$v]['MyOrder']=$i;
					}else{//没有数据
						$insert_ary[]=array(
							'ProId'		=>	$ProId,
							'AttrId'	=>	$_AttrId,
							'VId'		=>	$VId,
							'OvId'		=>	$OvId,
							'IsUsed'	=>	1,
							'MyOrder'	=>	$i
						);
					}
					++$i;
				}
				db::update('products_selected_attribute', "ProId='$ProId' and IsUsed<2 and (VId>0 or (AttrId=0 and VId=0 and OvId>0))", array('IsUsed'=>0));//先默认全部关闭
				if(count($update_ary)){
					foreach((array)$update_ary as $k=>$v){
						if(strstr($k, 'Ov:')){//海外仓
							$w="ProId='$ProId' and AttrId=0 and OvId='".str_replace('Ov:', '', $k)."'";
						}else{//规格属性
							$w="ProId='$ProId' and VId='$k'";
						}
						db::update('products_selected_attribute', $w, $v);
					}
				}
				foreach((array)$insert_ary as $k=>$v){
					db::insert('products_selected_attribute', $v);
				}
				//组合数据
				$insert_ary=$update_ary=$combination_ary=$key_ary=$ext_ary=array();
				$combination_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'"));
				foreach((array)$combination_row as $v){ $combination_ary[$v['Combination']][$v['OvId']]=$v['CId']; }
				unset($combination_row);
				foreach((array)$p_AttrPrice as $k=>$v){
					if($k=='XXX') continue;//把默认数据去掉
					$key_ary=explode('_', $k);
					$OvId=1;
					$key=array();
					foreach((array)$key_ary as $v2){
						if(strstr($v2, 'Ov:')){
							$OvId=(int)str_replace('Ov:', '', $v2);
						}else{
							$key[]=($new_vid_ary[$v2]?$new_vid_ary[$v2]:$v2);//new? now?
						}
					}

					sort($key);
					$key='|'.@implode('|', $key).'|';
					$ext_ary[$key][$OvId]=array($p_AttrSKU[$k], (int)$p_AttrIsIncrease[$k], $p_AttrPrice[$k], $p_AttrStock[$k], ($used_row['weight']?(float)$p_AttrWeight[$k]:abs((float)$p_AttrWeight[$k])));
				}
				foreach((array)$ext_ary as $k=>$v){
					foreach((array)$v as $k2=>$v2){
						$stock=$v2[3];
						$stock<0 && $stock=0;//最低值为0，0属于没库存
						$total_stock+=$stock;
						$attr_count+=1;
						if($exist_ary[$k][$k2]){//已存在
							$cid_ary[]=$exist_ary[$k][$k2];
							$update_ary[$exist_ary[$k][$k2]]=array(
								'SKU'			=>	$v2[0],
								'IsIncrease'	=>	$v2[1],
								'Price'			=>	$v2[2],
								'Stock'			=>	$stock,
								'Weight'		=>	$v2[4]
							);
						}else{ //不存在
							$insert_ary[]=array(
								'ProId'			=>	$ProId,
								'Combination'	=>	$k,
								'OvId'			=>	$k2,
								'SKU'			=>	$v2[0],
								'IsIncrease'	=>	$v2[1],
								'Price'			=>	$v2[2],
								'Stock'			=>	$stock,
								'Weight'		=>	$v2[4]
							);
						}
					}
				}
				if(count($update_ary)){
					foreach((array)$update_ary as $k=>$v){
						db::update('products_selected_attribute_combination', "CId='$k'", $v);
					}
				}
				if(count($cid_ary)){//清空多余没用的选项数据
					$cid_str=implode(',', $cid_ary);
					db::delete('products_selected_attribute_combination', "ProId='$ProId' and CId not in($cid_str)");
				}else{//清空已经取消勾选的选项数据
					db::delete('products_selected_attribute_combination', "ProId='$ProId'");
				}
				if(count($insert_ary)){
					foreach((array)$insert_ary as $k=>$v){
						db::insert('products_selected_attribute_combination', $v);
					}
				}
				if($p_IsCombination){//开启组合属性，统计产品属性总库存，覆盖到产品库存上
					/*if((int)$used_row['stock']==0 && $total_stock<1){//规格属性无限库存 和 合计库存小于1
						$total_stock=($data['Stock']<1?99999:$data['Stock']);
					}elseif((int)$used_row['stock']==1 && $total_stock<1){//规格属性无限库存 和 合计库存小于1
						$total_stock=$data['Stock'];
					}*/
					(int)$attr_count && db::update('products', "ProId='$ProId'", array('Stock'=>$total_stock));
				}
			}else{
				//没有规格属性勾选，清空所有数据
				$color_row = db::get_all('products_color', "ProId='$ProId'", $c['prod_image']['sql_field']);
				foreach ((array)$color_row as $v) {
					for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
						$PicPath = $v["PicPath_$i"];
						if (is_file($c['root_path'] . $PicPath)) {
							foreach ((array)$resize_ary as $v2) {
								$ext_name = file::get_ext_name($PicPath);
								file::del_file($PicPath . ".{$v2}.{$ext_name}");
							}
							file::del_file($PicPath);
						}
					}
				}
                if ($p_AttrOption) {
                    //判断属性是否还有选项存在，不应该立即删除掉所有的选项记录
                    $w = str::ary_format($p_AttrOption, 2, '', '', ',');
                    db::update('products_selected_attribute', "ProId='$ProId' and VId in($w)", array('IsUsed'=>0)); //存在的选项，默认为未勾选状态
                    db::update('products_selected_attribute', "ProId='$ProId' and VId not in($w)", array('IsUsed'=>2));//主动删除的选项，默认记录为隐藏状态，待检查是否删掉
                }
				db::delete('products_selected_attribute_combination', "ProId='$ProId'");
				db::delete('products_color', "ProId='$ProId'");
			}
		}
		//普通属性
		if($p_AttrTxt){
			$insert_ary=$update_ary=$selected_ary=array();
			$w="ProId='$ProId' and AttrId>0 and VId=0 and OvId=1";
			$selected_row=str::str_code(db::get_all('products_selected_attribute', $w, '*', 'SeleteId asc'));
			foreach((array)$selected_row as $v){ $selected_ary[$v['AttrId']]=$v['SeleteId']; }
			unset($selected_row);
			foreach((array)$p_AttrTxt as $k=>$v){
				if($selected_ary[$k]){ //已存在
					$update_ary[$selected_ary[$k]]=array('IsUsed'=>1);
					foreach((array)$v as $lang=>$v2){
						$update_ary[$selected_ary[$k]]["Value_$lang"]=addslashes($v2);
					}
				}else{ //不存在
					$insert_ary[$k]=array(
						'ProId'		=>	$ProId,
						'AttrId'	=>	$k,
						'VId'		=>	0,
						'IsUsed'	=>	1
					);
					foreach((array)$v as $lang=>$v2){
						$insert_ary[$k]["Value_$lang"]=addslashes($v2);
					}
				}
			}
			db::update('products_selected_attribute', $w, array('IsUsed'=>0)); //先默认全部关闭
			if(count($update_ary)){
				foreach((array)$update_ary as $k=>$v){
					db::update('products_selected_attribute', "ProId='$ProId' and SeleteId='$k'", $v);
				}
			}
			foreach((array)$insert_ary as $k=>$v){
				db::insert('products_selected_attribute', $v);
			}
			if(db::get_row_count('products_selected_attribute', $w.' and IsUsed=0')){//记录到隐藏状态
				db::update('products_selected_attribute', $w.' and IsUsed=0', array('IsUsed'=>2));
			}
		}else{
			//默认都改成隐藏状态
			db::update('products_selected_attribute', "ProId='$ProId' and AttrId>0 and VId=0 and OvId=1", array('IsUsed'=>2));
		}
		//检查闲置的属性
		$hide_ary=$delete_ary=array();
		$attr_row=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%' and DescriptionAttr=0", '*', $c['my_order'].'AttrId asc'));
		foreach((array)$attr_row as $v){
			if(in_array($v['AttrId'], $new_attr_id_ary)) continue;//新添加属性排除
			$attrType=($v['CartAttr']==1 || ($v['CartAttr']==0 && $v['Type']==1))?1:0;//1:选项 0:文本框
			if($attrType==1 && !isset($p_AttrTitle[$v['AttrId']])){//如果规格属性已经踢掉
				$delete_ary[]=$v['AttrId'];
				$hide_ary[]=$v['AttrId'];
			}
			if($attrType==0 && !isset($p_AttrTxt[$v['AttrId']])){//如果普通属性已经踢掉
				$delete_ary[]=$v['AttrId'];
			}
		}
		if(count($hide_ary)){//规格属性，属性删除按钮事件
			foreach((array)$hide_ary as $k=>$v){
				if(db::get_row_count('products_selected_attr', "ProId='$ProId' and AttrId='$v'", 'SId')){
					db::update('products_selected_attr', "ProId='$ProId' and AttrId='$v'", array('IsUsed'=>2));
				}else{
					db::insert('products_selected_attr', array(
						'ProId'		=>	$ProId,
						'AttrId'	=>	$v,
						'IsUsed'	=>	2
					));
				}
			}
		}
		/*剔除回收站的产品 start */
		$proid_ary=db::get_all('products_recycle', '1', 'ProId');
		$proid_str_ary=array($ProId);
		foreach((array)$proid_ary as $v){
			$proid_str_ary[]=$v['ProId'];
		}
		$proid_str_ary=str::ary_format($proid_str_ary, 2);
		if(db::get_row_count('products_recycle')<1000){
			$out_where=" and ProId not in($proid_str_ary) group by ProId";
		}else{
			$out_where=" and ProId not in($ProId) and ProId not in(select ProId from products_recycle) group by ProId";
		}
		/*剔除回收站的产品 end */
		if(count($delete_ary)){
			foreach((array)$delete_ary as $k=>$v){
				$pro_count=db::get_row_count('products_selected_attribute', "AttrId='$v' and IsUsed=1".$out_where, 'AttrId');
				if($pro_count>0) unset($delete_ary[$k]);//还有关联的产品，不能删掉
			}
			$delete=implode(',', $delete_ary);
			if($delete){//删掉闲置没用的属性
				db::delete('products_attribute', "AttrId in ($delete)");
				db::delete('products_attribute_value', "AttrId in ($delete)");
				db::delete('products_selected_attr', "AttrId in ($delete)");
				db::delete('products_selected_attribute', "AttrId in ($delete)");
				//manage::operation_log('删除产品属性选项:');
			}
		}
		//检查闲置的属性选项
		$delete_ary=array();
		$hide_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId' and IsUsed=2"));
		if($hide_row){
			foreach((array)$hide_row as $v){
				if($v['VId']>0){//规格属性
					$pro_count=db::get_row_count('products_selected_attribute', "VId='{$v['VId']}' and IsUsed=1", 'SeleteId');
					if($pro_count<1){//没有任何关联产品的数据
						$delete_ary[1][]=$v['VId'];
					}
				}else{//普通属性
					$pro_count=db::get_row_count('products_selected_attribute', "AttrId='{$v['AttrId']}' and IsUsed=1", 'SeleteId');
					if($pro_count<1){//没有任何关联产品的数据
						$delete_ary[0][]=$v['AttrId'];
					}
				}
			}
		}
		if (count($delete_ary)) {
			foreach ((array)$delete_ary as $key => $val) {
				$delete = implode(',', $val);
				if ($key == 1) {
                    //规格属性
					//删除相应的产品选项图片
					$color_row = str::str_code(db::get_all('products_color', "VId in ($delete)", '*'));
					if ($color_row) {
						foreach ((array)$color_row as $v) {
							for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
								$PicPath = $v["PicPath_$i"];
								if (is_file($c['root_path'] . $PicPath)) {
									foreach ((array)$resize_ary as $v2) {
										$ext_name = file::get_ext_name($PicPath);
										file::del_file($PicPath . ".{$v2}.{$ext_name}");
									}
									file::del_file($PicPath);
								}
							}
						}
						db::delete('products_color', "VId in ($delete)");						
					}					
					db::delete('products_selected_attribute', "VId in ($delete)");
					db::delete('products_attribute_value', "VId in ($delete)");
					manage::custom_operation_log('删除产品规格属性选项', array('VId'=>$val));
				} else {
                    //普通属性
					db::delete('products_selected_attribute', "AttrId in ($delete)");
					db::delete('products_attribute_value', "AttrId in ($delete)");
					db::delete('products_attribute', "AttrId in ($delete)");
					manage::custom_operation_log('删除产品普通属性选项', array('AttrId'=>$val));
				}
				
			}
		}
		db::delete('products_selected_attribute', 'ProId=0');//临时删除多余的勾选项，日后要去掉此代码
		
		manage::database_language_operation('products', "ProId='$ProId'", array('Name'=>1, 'BriefDescription'=>2));
		manage::database_language_operation('products_seo', "ProId='$ProId'", array('SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		$desc_data=array('Description'=>3);
		manage::database_language_operation('products_description', "ProId='$ProId'", $desc_data);
		//产品最低价格 Start
		$LowestPrice=cart::range_price_ext(db::get_one('products', "ProId='{$ProId}'"));
		db::update('products', "ProId='{$ProId}'", array('LowestPrice'=>$LowestPrice[0]));
		//产品最低价格 End
		//产品描述属性 Start
		if(db::get_row_count('products_attribute', 'CateId="|0|" and DescriptionAttr=1')){
			db::update('products_attribute', 'CateId="|0|" and DescriptionAttr=1', array('CateId'=>"|{$CateId}|"));
		}
		$tab_ary=$tab_id_ary=$delete_ary=$check_ary=array();
		//产品描述属性
		$tab_row=str::str_code(db::get_all('products_tab', "ProId='$ProId'"));
		foreach((array)$tab_row as $k=>$v){
			$del=0;
			$Num=($v['AttrId']==0?"Num:{$k}":$v['AttrId']);//AttrId为空 是全局描述属性
			foreach((array)$c['manage']['config']['Language'] as $lang){
				$tab_ary[$Num][$lang]=$_POST["Tab_{$Num}_{$lang}"];
				if(!isset($_POST["Tab_{$Num}_{$lang}"])){//删除了描述属性
					$del+=1;
				}
			}
			$tab_id_ary[$Num]=$v['TId'];
			if($del>0){
				$delete_ary[]=$v['TId'];
				$check_ary[]=$v['AttrId'];
			}
		}
		//分类描述属性
		$desc_attr_row=str::str_code(db::get_all('products_attribute', "DescriptionAttr=1 and CateId like '%|{$CateId}|%'", '*', $c['my_order'].'AttrId asc'));
		foreach((array)$desc_attr_row as $k=>$v){
			if($tab_id_ary[$v['AttrId']]) continue;//已有数据
			$del=0;
			foreach((array)$c['manage']['config']['Language'] as $lang){
				$tab_ary[$v['AttrId']][$lang]=$_POST["Tab_{$v['AttrId']}_{$lang}"];
				if(!isset($_POST["Tab_{$v['AttrId']}_{$lang}"])){//删除了描述属性
					$del+=1;
				}
			}
			if($del==0){//处理描述属性数据
				if($TId=db::get_value('products_tab', "ProId='$ProId' and AttrId='{$v['AttrId']}'", 'TId')){
					db::update('products_tab', "ProId='$ProId' and AttrId='{$v['AttrId']}'", array('IsUsed'=>1));
					$tab_id_ary[$v['AttrId']]=$TId;
				}else{
					$insert_ary=array(
						'ProId'		=>	$ProId,
						'AttrId'	=>	$v['AttrId'],
						'IsUsed'	=>	1
					);
					db::insert('products_tab', $insert_ary);
					$TId=db::get_insert_id();
					$tab_id_ary[$v['AttrId']]=$TId;
				}
			}else{
				$check_ary[]=$v['AttrId'];
			}
		}
		//全局描述属性
		$global_desc_attr_row=str::str_code(db::get_all('products_attribute', '(CateId="0" or CateId="") and DescriptionAttr=1', '*', $c['my_order'].'AttrId asc'));
		foreach((array)$global_desc_attr_row as $k=>$v){
            //旧数据判断
			$Num="Num:{$k}";
			if($tab_id_ary[$Num]) continue; //已有数据
			$del=0;
			foreach((array)$c['manage']['config']['Language'] as $lang){
				$tab_ary[$Num][$lang]=$_POST["Tab_{$Num}_{$lang}"];
				if(!isset($_POST["Tab_{$Num}_{$lang}"])){
                    //删除了描述属性
					$del+=1;
				}
			}
			if($del==0){//添加描述属性数据
				$insert_ary=array(
					'ProId'		=>	$ProId,
					'AttrId'	=>	0,
					'IsUsed'	=>	1
				);
				db::insert('products_tab', $insert_ary);
				$TId=db::get_insert_id();
				$tab_id_ary[$Num]=$TId;
			}
            /*
            //新数据判断
			$del=0;
			foreach((array)$c['manage']['config']['Language'] as $lang){
				$tab_ary[$v['AttrId']][$lang]=$_POST["Tab_{$v['AttrId']}_{$lang}"];
				if(!isset($_POST["Tab_{$v['AttrId']}_{$lang}"])){
                    //删除了描述属性
					$del+=1;
				}
			}
			if($del==1){//处理描述属性数据
				$check_ary[]=$v['AttrId'];
			}
            */
		}
		if(count($tab_ary)){//描述属性
			$global_desc_attr_ary=$desc_attr_ary=$is_pro_ary=array();
			foreach((array)$desc_attr_row as $k=>$v){
				$desc_attr_ary[$v['AttrId']]=$v;
			}
			foreach((array)$global_desc_attr_row as $k=>$v){
				$global_desc_attr_ary['Num:'.$k]=$v;
			}
			//检查描述属性内容是否不一致
			foreach((array)$tab_ary as $k=>$v){
				if($is_pro_ary[$k]==0){
					$ary=(strstr($k, 'Num:')?$global_desc_attr_ary[$k]:$desc_attr_ary[$k]);
					foreach((array)$c['manage']['config']['Language'] as $lang){
						if($v[$lang]!=htmlspecialchars_decode($ary["Description_$lang"])){
							$is_pro_ary[$k]=1;
							break;
						}
					}
				}
			}
			foreach((array)$tab_ary as $k=>$v){
				$tab_data=array('IsUsed'=>1);
				if($is_pro_ary[$k]==1){//绑定到产品自身
					foreach((array)$tab_ary[$k] as $lang=>$v2){
						$tab_data["Description_{$lang}"]=$v2;
					}
				}else{//使用分类属性数据
					foreach((array)$v as $lang=>$v2){
						$tab_data["Description_{$lang}"]='';//清空内容
					}
				}
				db::update('products_tab', "ProId='$ProId' and TId='".$tab_id_ary[$k]."'", $tab_data);
			}
			if($delete_ary){//记录到隐藏状态
				$w=@implode(',', $delete_ary);
				db::update('products_tab', "ProId='$ProId' and TId in ($w)", array('IsUsed'=>2));
			}
		}else{
			db::update('products_tab', "ProId='$ProId'", array('IsUsed'=>2));
		}
		//检查产品描述属性闲置的情况
		if(count($check_ary)){//检查删掉的描述属性本身
			foreach((array)$check_ary as $k=>$v){
				$pro_count=db::get_row_count('products_tab', "AttrId='$v' and IsUsed=1".$out_where, 'AttrId');
				if($pro_count>0) unset($check_ary[$k]);//还有关联的产品，不能删掉属性本身
			}
			$delete=implode(',', $check_ary);
			if($delete){
				db::delete('products_attribute', "AttrId in ($delete)");
				db::delete('products_tab', "AttrId in ($delete)");
			}
		}

		//判断新用户已经修改产品
		manage::config_operaction(array('products'=>1), 'guide_pages');
		//产品描述属性 End
		ly200::e_json(array('jump'=>$p_back_action?$p_back_action:"?m=products&a=products&CateId=$CateId"), 1);
	}
	
	/**
	 *
	 * 读取产品属性的相关信息
	 */
	public static function products_get_attr(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$Content=$html_attr=$html_item='';
		$p_CateId		= (int)$p_CateId;	//产品分类ID
		$VId			= (int)$p_VId;		//产品属性选项ID
		$ProId			= (int)$p_ProId;	//产品ID
		$Type			= (int)$p_Type;		//执行类型 0:获取当前分类里的所有属性 1:获取当前的属性 2:获取当前属性的选项
		$p_IsCartAttr	= (int)$p_IsCartAttr; //是否为规格属性
		$p_IsDescriptionAttr=(int)$p_IsDescriptionAttr;//是否为描述属性
		$Lang			= $c['manage']['web_lang'];//语言
		//预先加载数据
		$attr_ary=array();
		$attr_row=str::str_code(db::get_all('products_selected_attr', "ProId='$ProId'", '*', 'SId asc'));
		foreach((array)$attr_row as $v){
			$attr_ary['Id'][$v['AttrId']]=$v['IsColor'];
			$v['IsUsed']==1 && $attr_ary['Selected'][]=$v['AttrId'];//已在使用的属性ID
			$v['IsUsed']==2 && $attr_ary['Hide'][]=$v['AttrId'];//已隐藏属性ID
		}
		$selected_ary=$combinatin_ary=$cart_attr_ary=$all_value_ary=$attrid=array();
		$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'", '*', 'SeleteId asc'));
		foreach((array)$selected_row as $v){
			$selected_ary['Id'][$v['AttrId']][]=$v['VId'];//所有属性ID
			if($v['IsUsed']==1){ // 已勾选属性ID
				$selected_ary['Selected'][$v['AttrId']][]=$v['VId'];
				$selected_ary['MyOrder'][$v['VId']]=$v['MyOrder'];
			}
			$v['IsUsed']==2 && $selected_ary['Hide'][$v['AttrId']][]=$v['VId'];//已隐藏属性ID
			$v['AttrId']==0 && $v['VId']==0 && $v['OvId']>0 && $v['IsUsed']==1 && $selected_ary['Overseas'][]=$v['OvId'];//勾选属性ID 发货地
			foreach((array)$c['manage']['config']['Language'] as $k2=>$v2){
				$v['AttrId']>0 && $v['VId']==0 && $v['OvId']<2 && $selected_ary['Value'][$v['AttrId']][$v2]=stripslashes($v["Value_$v2"]);//文本框内容
			}
		}
		unset($selected_row);
		$combinatin_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'", '*', 'CId asc'));//属性组合数据
		foreach((array)$combinatin_row as $v){
			if($v['Combination']=='||'){//纯海外仓信息
				$combinatin_ary["|Ov:{$v['OvId']}|"]=array($v['SKU'], $v['IsIncrease'], $v['Price'], $v['Stock'], $v['Weight']);
			}else{
				$combinatin_ary[$v['Combination']][$v['OvId']]=array($v['SKU'], $v['IsIncrease'], $v['Price'], $v['Stock'], $v['Weight']);
			}
		}
		unset($combinatin_row);
		$attr_data=array();
		$num=0;
		//海外仓
		$shipping_overseas_row=db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc');
		$oversea_len=count($shipping_overseas_row);
		$Lang=substr($c['manage']['web_lang'], 1);
		if($oversea_len>0 && $p_IsCartAttr==1){
			$attr_data[$num]['AttrId']='Overseas';
			$attr_data[$num]['Name']=$c['manage']['lang_pack']['module']['set']['shipping']['overseas'];
			$attr_data[$num]['Option']=array();
			foreach((array)$shipping_overseas_row as $k2=>$v2){
				$OvId='Ov:'.$v2['OvId'];
				$value_data=array(
					'VId'	=>	$OvId,
					'Name'	=>	$v2['Name_'.$Lang],
					'Info'	=>	array(
									'Column'	=>	$c['manage']['lang_pack']['module']['set']['shipping']['overseas'],
									'VId'		=>	$OvId,
									'Name'		=>	$v2['Name_'.$Lang]
								),
					'Data'	=>	$combinatin_ary['||'][$v2['OvId']],
				);
				if(@in_array($v2['OvId'], $selected_ary['Overseas'])){//该产品已有该选项并已勾选
					if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 || ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 && $OvId=='Ov:1')){//开启海外仓 或者 没开启海外仓，仅允许默认海外仓
						$attr_data[$num]['Selected'][$Lang][$OvId]=$value_data;
					}
				}else{//该产品没有该选项
					$attr_data[$num]['Option'][$Lang][$OvId]=$value_data;
				}
			}
		}
		//属性数据
		if($p_CateId){
			$row=str::str_code(db::get_all('products_attribute', "CateId like '%|{$p_CateId}|%' and CartAttr='{$p_IsCartAttr}' and DescriptionAttr=0", '*', $c['my_order'].'AttrId asc'));
			foreach((array)$row as $v){ $attrid[]=$v['AttrId']; }
			$attrid_list=implode(',', $attrid);
			!$attrid_list && $attrid_list='0';
			$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
			foreach((array)$value_row as $v){ $all_value_ary[$v['AttrId']][$v['VId']]=$v; }
			unset($value_row);
			foreach((array)$row as $k=>$v){
				$num=$k+1;
				$ID=$v['AttrId'];
				$attrType=($v['CartAttr']==1 || ($v['CartAttr']==0 && $v['Type']==1))?1:0;//1:选项 0:文本框
				if($attrType==1 && $attr_ary['Hide'] && in_array($ID, $attr_ary['Hide'])){//已隐藏的属性(规格属性 or 普通属性（选项）)
					continue;
				}
				if($attrType==0 && $selected_ary['Hide'] && $selected_ary['Hide'][$ID]){//已隐藏的属性(普通属性（文本框）)
					continue;
				}
				$IsColor=($attr_ary['Selected'] && in_array($ID, $attr_ary['Selected'])?$attr_ary['Id'][$ID]:$v['ColorAttr']);
				$attr_data[$num]['AttrId']=$ID;
				$attr_data[$num]['Name']=$v["Name_$Lang"];
				$attr_data[$num]['Type']=$v['Type'];
				$attr_data[$num]['Color']=$IsColor;
				$attr_data[$num]['Screen']=$v['ScreenAttr'];
				$attr_data[$num]['Option']=array();
				foreach((array)$c['manage']['config']['Language'] as $lang){
					if($v['CartAttr']==1){ //规格属性
						foreach((array)$all_value_ary[$ID] as $k3=>$v3){
							if(count($selected_ary['Hide'][$ID])>0 && in_array($k3, $selected_ary['Hide'][$ID])) continue;//已隐藏的选项
							$value_data=array(
								'VId'	=>	$v3['VId'],
								'Name'	=>	$v3["Value_$lang"],
								'Info'	=>	array(
												'Column'	=>	$v["Name_$lang"],
												'VId'		=>	$v3['VId'],
												'Name'		=>	$v3["Value_$lang"],
												'Cart'		=>	$v['CartAttr'],
												'Color'		=>	$IsColor,
												'Desc'		=>	$v['DescriptionAttr']
											),
								'Data'	=>	$combinatin_ary["|{$k3}|"][1],
							);
							if(@in_array($k3, $selected_ary['Selected'][$ID])){//已勾选
								$MyOrder=$selected_ary['MyOrder'][$k3];
								if($MyOrder){
									$attr_data[$num]['Selected'][$lang][$MyOrder]=$value_data;
								}else{
									$attr_data[$num]['Selected'][$lang][]=$value_data;
									
								}
							}else{
								$attr_data[$num]['Option'][$lang][]=$value_data;
							}
						}
					}else{ //普通属性
						if($v['Type']==1){ //普通属性（选项）
							$value_data=array();
							foreach((array)$all_value_ary[$ID] as $k3=>$v3){
                                if(count($selected_ary['Hide'][$ID])>0 && in_array($k3, $selected_ary['Hide'][$ID])) continue;//已隐藏的选项
								$value_data=array(
									'VId'	=>	$v3['VId'],
									'Name'	=>	$v3["Value_$lang"]
								);
								if(@in_array($k3, $selected_ary['Selected'][$ID])){//已勾选
									$attr_data[$num]['Selected'][$lang][]=$value_data;
								}else{
									$attr_data[$num]['Option'][$lang][]=$value_data;
								}
							}
						}else{ //普通属性（文本框）
							$attr_data[$num]['Option'][$lang]=$selected_ary['Value'][$ID][$lang];
						}
					}
				}
			}
		}
		//选项卡(普通属性)
		if($p_IsCartAttr==0){
			$tab_data=$tab_sort_data=array();
			$tab_ary=ly200::product_attribute_tab($ProId, $p_CateId, 1);
			foreach((array)$tab_ary as $k=>$v){
				$tab_sort_data[]=$k;
				$tab_data[$k]['Name']=$v["Name_$Lang"];
				foreach((array)$c['manage']['config']['Language'] as $lang){
					$tab_data[$k]['Option'][$lang]=htmlspecialchars_decode(str::str_code($v["Description_$lang"], 'stripslashes'));
				}
			}
		}
		//返回数据
		$result=array(
			'Attr'		=>	$attr_data,
			'Tab'		=>	$tab_data,
			'TabSort'	=>	$tab_sort_data,
			'Combinatin'=>	$combinatin_ary,
			'Callback'	=>	$callback
		);
		ly200::e_json($result, 1);
	}
	
	/**
	 *
	 * 产品关联图片
	 */
	public static function products_get_color(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId	= (int)$p_ProId;	//产品ID
		$p_CateId	= (int)$p_CateId;	//产品分类ID
		$p_AttrId	= (int)$p_AttrId;	//产品属性ID
		$vid_where	= '-1';
		$ReturnData=$path_ary=array();
		$vid_ary=explode(',', $p_VId);
		foreach((array)$vid_ary as $k=>$v){
			if(!strstr($v, 'ADD:')){
				$vid_where.=",{$v}";
			}
		}
		$color_row=str::str_code(db::get_all('products_color', "ProId='$p_ProId' and VId in($vid_where)"));
		foreach ((array)$color_row as $k => $v) {
			for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
				$path_ary[$v['VId']][] = $v["PicPath_{$i}"];
			}
		}
		foreach ((array)$vid_ary as $v) {
			for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
				$path = $path_ary[$v][$i];
				$isFile = is_file($c['root_path'].$path) ? 1 : 0;
				$ReturnData['Img'][$v][$i] = array(
					'isfile'=>	$isFile,
					'big'	=>	$path,
					'small'	=>	ly200::get_size_img($path, '240x240')
				);
			}
		}
		$attr_row=str::str_code(db::get_one('products_attribute', "AttrId='{$p_AttrId}'", 'ColorAttr'));
		$ReturnData['IsColor']=(int)$attr_row['ColorAttr'];
		$attr_selected_row=str::str_code(db::get_one('products_selected_attr', "ProId='{$p_ProId}' and AttrId='{$p_AttrId}'", 'IsColor'));
		$attr_selected_row && $ReturnData['IsColor']=(int)$attr_selected_row['IsColor'];
		ly200::e_json($ReturnData, 1);
	}
	
	/**
	 *
	 * 产品视频
	 */
	public static function products_video_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId=(int)$p_ProId;
		$p_VideoFirst=(int)$p_VideoFirst;
		$p_VideoUrl=trim($p_VideoUrl);
		db::update('products', "ProId='$p_ProId'", array('VideoUrl'=>$p_VideoUrl));
		if(!db::get_row_count('products_development', "ProId='$p_ProId'")){
			db::insert('products_development', array('VideoFirst'=>$p_VideoFirst, 'ProId'=>$p_ProId));
		}else{
			db::update('products_development', "ProId='$p_ProId'", array('VideoFirst'=>$p_VideoFirst));
		}
		ly200::e_json(array($p_VideoUrl, $p_VideoFirst), 1);
	}
	
	/**
	 *
	 * 检查产品属性信息 (规格属性 or 普通属性 or 描述属性)
	 */
	public static function products_check_attribute(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId		= (int)$p_CateId;			//产品分类ID
		$Lang		= $c['manage']['web_lang'];	//语言
		$IsCartAttr	= (int)$p_IsCartAttr;		//是否为规格属性
		$Type		= (int)$p_Type;				//0-1:普通属性 2:描述属性 (当不属于规格属性时候的才调用)
		$ReturnData	= array();
		$isRepeat	= 0;						//是否有重复的
		$Where		= "CartAttr='$IsCartAttr' and binary Name{$Lang}='$p_Name'"; //区分大小写
		$Where		= $Where.($IsCartAttr==0?($p_Type==2?' and DescriptionAttr=1':' and DescriptionAttr=0'):'');
		$attr_row	= str::str_code(db::get_limit('products_attribute', $Where, '*', 'AttrId desc', 0, 10));
		foreach((array)$attr_row as $k=>$v){
			$sCateId=explode('|', trim($v['CateId'], '|'));
			$cate_row=str::str_code(db::get_one('products_category', "CateId='{$sCateId[0]}'", "Category{$Lang}"));
			$ReturnData[$k+1]=array(
				'AttrId'	=>	$v['AttrId'],
				'Name'		=>	$c['manage']['lang_pack']['global']['copy'].' '.$v['Name'.$Lang],
				'Category'	=>	($cate_row?str_replace('%catalog%', $cate_row['Category'.$Lang], $c['manage']['lang_pack']['products']['products']['check_attr_notes']):$c['manage']['lang_pack']['global']['public'])
			);
			if(in_array($CateId, $sCateId) && $v["Name$Lang"]==$p_Name){
				$isRepeat+=1;
			}
		}
		if($isRepeat==0){//没有名称重复的属性
			$ReturnData=array_merge(array(
				0=>array(//追加“添加”选项
					'AttrId'	=>	'',
					'Name'		=>	$c['manage']['lang_pack']['global']['add_new'].' '.$p_Name,
					'Category'	=>	''
				)
			), $ReturnData);
		}
		ly200::e_json($ReturnData, $ReturnData?1:0);
	}
	
	/**
	 *
	 * 检查产品属性选项，下拉选项所用到 (规格属性 or 普通属性 or 描述属性)
	 */
	public static function products_check_attribute_option(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ProId		= (int)$p_ProId;			//产品ID
		$AttrId		= (int)$p_AttrId;			//产品属性ID
		$IsCartAttr	= (int)$p_IsCartAttr;		//是否为规格属性
		$Lang		= $c['manage']['web_lang'];	//语言
		$AttrRow	= str::str_code(db::get_one('products_attribute', "AttrId='$AttrId'"));//属性数据
		//已选中的数据
		$selected_ary=array();
		$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId' and AttrId='$AttrId'", '*', 'SeleteId asc'));
		foreach((array)$selected_row as $v){
			$selected_ary['Id'][]=$v['VId'];//所有属性ID
			$v['IsUsed']==1 && $selected_ary['Selected'][]=$v['VId'];//已勾选属性ID
			$v['AttrId']>0 && $v['VId']==0 && $v['Value'] && $v['OvId']<2 && $selected_ary['Value']=stripslashes($v['Value']);//文本框内容
			$v['AttrId']==0 && $v['VId']==0 && $v['OvId']>0 && $selected_ary['Overseas'][]=$v['OvId'];//勾选属性ID 发货地
		}
		unset($selected_row);
		//产品自身已保存的选项数据
		$combinatin_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'", '*', 'CId asc')); //属性组合数据
		foreach((array)$combinatin_row as $v){ $combinatin_ary[$v['Combination']][$v['OvId']]=array($v['SKU'], $v['IsIncrease'], $v['Price'], $v['Stock'], $v['Weight']); }
		unset($combinatin_row);
		//属性选项数据
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId='$AttrId'"));
		if($IsCartAttr==1){//规格属性
			$ReturnData=array('Option'=>array(), 'Selected'=>array());//返回数据
			foreach((array)$c['manage']['config']['Language'] as $k=>$v){
				$ReturnData['Name'][$v]=$AttrRow['Name_'.$v];
			}
			foreach((array)$value_row as $k=>$v){
				$value_data=array(
					'VId'	=>	$v['VId'],
					'AttrId'=>	$v['AttrId'],
					'Value'	=>	$v['Value'.$Lang],
					'Info'	=>	array(
									'Column'	=>	$AttrRow['Name'.$Lang],
									'VId'		=>	$v['VId'],
									'Name'		=>	$v['Value'.$Lang],
									'Cart'		=>	$AttrRow['CartAttr'],
									'Color'		=>	$AttrRow['ColorAttr'],
									'Desc'		=>	$AttrRow['DescriptionAttr']
								),
					'Data'	=>	$combinatin_ary["|{$k2}|"][1],
				);
				if(@in_array($v['VId'], $selected_ary['Selected'])){//已勾选
					$ReturnData['Selected'][]=$value_data;
				}else{
					$ReturnData['Option'][]=$value_data;
				}
			}
		}else{//普通属性
			//注意：以后将会陆续把类型给去掉，默认统一为“手工录入”
			$ReturnData='';
			foreach((array)$c['manage']['config']['Language'] as $k=>$v){
				$ReturnData['Name'][$v]=$AttrRow['Name_'.$v];
				if($AttrRow['DescriptionAttr']==1){//描述属性
					$ReturnData['Option'][$v]=htmlspecialchars_decode($AttrRow['Description_'.$v]);
				}else{//普通属性
					if($AttrRow['Type']==1){//列表选择
						$value_data=array();
						foreach((array)$value_row as $k2=>$v2){
							$value_data[]=$v2['Value_'.$v];
						}
						$ReturnData['Option'][$v]=@implode(', ', $value_data);
					}else{//手工录入
						$ReturnData['Option'][$v]=$selected_ary['Value_'.$v];
					}
				}
			}
			$ReturnData['Type']=($AttrRow['DescriptionAttr']==1?2:$AttrRow['Type']);
		}
		ly200::e_json($ReturnData, ($ReturnData['Option'] || $ReturnData['Selected']?1:0));
	}
	
	/**
	 *
	 * 检查产品属性相应的关联产品信息 (规格属性 or 普通属性 or 描述属性)
	 */
	public static function products_check_attribute_pro_count(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
        //初始化
		$p_ProId = (int)$p_ProId;
		$Attr = @explode(',', $p_Attr);
		$Editor = @explode(',', $p_Editor);
		$ReturnData = array();
		//剔除回收站的产品
		$proid_ary = db::get_all('products_recycle', '1', 'ProId');
		$proid_str_ary = array($p_ProId);
		foreach ((array)$proid_ary as $v) {
			$proid_str_ary[] = $v['ProId'];
		}
		$proid_str_ary = str::ary_format($proid_str_ary, 2);
		if (db::get_row_count('products_recycle') < 1000) {
			$out_where = " and ProId not in($proid_str_ary) group by ProId";
		} else {
			$out_where = " and ProId not in($p_ProId) and ProId not in(select ProId from products_recycle) group by ProId";
		}
		//开始处理
		foreach ((array)$Attr as $k => $v) {
			//规格属性 or 普通属性
			if (!$v) continue;
			$attr_row = db::get_one('products_attribute', "AttrId='$v'");
			foreach ((array)$c['manage']['config']['Language'] as $lang) {
				$ReturnData['Attr']['Name'][$lang][$v] = $attr_row['Name_' . $lang];
			}
			$ReturnData['Attr']['Screen'][$v] = (int)$attr_row['ScreenAttr'];
			$pro_count = db::get_row_count('products_selected_attribute', "AttrId='$v'" . $out_where, 'AttrId');			
			$ReturnData['Attr']['ProCount'][$v] = (int)$pro_count;
		}
		foreach ((array)$Editor as $k => $v) {
			//描述属性
			if (!$v) continue;
			if (!strstr($v, 'Num:')) {
                //分类描述属性
				$attr_row = db::get_one('products_attribute', "AttrId='$v'");
                $IsGlobal = $attr_row['CateId'] == '0' ? 1 : 0;
			} else {
                //全局描述属性
				$num = (int)str_replace('Num:', '', $v);
				$row = db::get_limit('products_attribute', '(CateId="0" or CateId="") and DescriptionAttr=1', '*', 'MyOrder asc', $num, 1);
				$attr_row = $row[0];
                $IsGlobal = 1;
			}
			foreach ((array)$c['manage']['config']['Language'] as $lang) {
				$ReturnData['Editor']['Name'][$lang][$v] = $attr_row['Name_' . $lang];
				$ReturnData['Editor']['Description'][$lang][$v] = $attr_row['Description_' . $lang];
			}
            $ReturnData['Editor']['IsGlobal'][$v] = (int)$IsGlobal; //“所有产品可用”选项卡是不限制使用数量
			$pro_count = db::get_row_count('products_tab', "AttrId='$v'" . $out_where, 'AttrId');
			$ReturnData['Editor']['ProCount'][$v] = (int)$pro_count;
		}
		ly200::e_json($ReturnData, 1);
	}
	
	/**
	 *
	 * 添加普通属性 (普通属性 or 描述属性)
	 */
	public static function products_attribute_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId			= (int)$p_ProId;
		$p_AddProId			= (int)$p_AddProId;
		$p_CateId			= (int)$p_CateId;
		$p_AttrId			= (int)$p_AttrId;
		$p_CartAttr			= (int)$p_CartAttr;			//固定是0 普通属性
		$p_Type				= (int)$p_Type;				//0:文本框 1:选项 2:描述属性
		$p_IsScreen			= (int)$p_IsScreen;			//是否为产品筛选
		$p_IsGlobal			= (int)$p_IsGlobal;			//是否设置为公共
		$IsNewCateId		= 0;						//是否添置新的产品分类ID
		$Lang				= $c['manage']['web_lang'];	//语言
		$ParentId			= db::get_value('products_attribute', "AttrId='$p_AttrId'", 'CateId');//当前属性归属的分类ID
		if(!($ParentId=='0' && $p_AttrId) && !strstr($ParentId, "|{$p_CateId}|")){//当前产品分类没有该属性，把当前产品分类记录到该属性上
			$IsNewCateId=1;
		}
		$DescriptionAttr=$p_Type==2?1:0;//是否为描述属性
		//编辑器数据转换
		foreach((array)$c['manage']['config']['Language'] as $k=>$v){
			$_POST["Description_$v"]=${"p_Editor_$v"};
		}
		//检查是否已有相同属性名称的数据(仅限于新添加)
		if($p_AttrId==0){
			$isRepeat=0;
			$attr_row=db::get_all('products_attribute', "(CateId like '%|$p_CateId|%' or CateId like '%|$ParentId|%') and CartAttr='{$CartAttr}' and DescriptionAttr='{$DescriptionAttr}'");
			foreach((array)$attr_row as $v){
				foreach((array)$c['manage']['config']['Language'] as $lang){
					if($v["Name_$lang"]==$_POST["Name_$lang"]){
						$isRepeat+=1;
					}
				}
			}
			if($isRepeat==1){
				ly200::e_json($c['manage']['lang_pack']['products']['products']['attr_repeat_tips'], 0);
			}
		}else{
			if($p_ProId && db::get_row_count('products_tab', "ProId='{$p_ProId}' and AttrId='{$p_AttrId}' and IsUsed<2")){
				ly200::e_json($c['manage']['lang_pack']['products']['products']['attr_repeat_tips'], 0);
			}
		}
		//产品属性
		if(!$p_AttrId){
			$CateIdStr="|{$p_CateId}|";
			($DescriptionAttr==1 && $p_IsGlobal==1) && $CateIdStr=0;
			$attr_data=array(
				'CateId'			=>	$CateIdStr,
				'CartAttr'			=>	$p_CartAttr,
				'DescriptionAttr'	=>	$DescriptionAttr,
				'ScreenAttr'		=>	$p_IsScreen,
				'Type'				=>	$p_Type,
			);
			($p_CateId==0 || $CateIdStr==0) && $attr_data['AccTime']=$c['time'];//记录添加时间
			db::insert('products_attribute', $attr_data);
			$p_AttrId=db::get_insert_id();
			manage::operation_log('添加产品普通属性');
			//多语言处理
			$language_data=array('Name'=>0);
			if($p_Type==2){//描述属性
				$language_data['Description']=3;
			}
			manage::database_language_operation('products_attribute', "AttrId='$p_AttrId'", $language_data);
			if($p_AddProId>0){//添加产品，记录添加的数据ID
				$_SESSION['AddProduct'][$p_AddProId]['Attr'][]=$p_AttrId;
			}
		}else{
			if($IsNewCateId==1){
				$attr_data=array('CateId'=>$ParentId.$p_CateId.'|');//注入新分类
				db::update('products_attribute', "AttrId='$p_AttrId'", $attr_data);
			}
		}
		//记录选项 普通属性（文本框）
		if($p_Type==0){
			$insert_ary=$update_ary=array();
			$SeleteId=0;
			$selected_row=str::str_code(db::get_one('products_selected_attribute', "ProId='$p_ProId' and AttrId='{$p_AttrId}' and VId=0 and OvId=1", '*', 'SeleteId asc'));
			$SeleteId=(int)$selected_row['SeleteId'];//文本框ID
			unset($selected_row);
			//普通属性数据整理
			if($SeleteId>0){//已有数据
				$update_ary['IsUsed']=1;
			}else{//没有数据
				$insert_ary=array(
					'ProId'		=>	$p_ProId,
					'AttrId'	=>	$p_AttrId,
					'VId'		=>	0,
					'IsUsed'	=>	1
				);
			}
			if($update_ary){//更新
				db::update('products_selected_attribute', "SeleteId='$SeleteId'", $update_ary);
			}
			if($insert_ary && $p_ProId>0){//插入
				db::insert('products_selected_attribute', $insert_ary);
				$SeleteId=db::get_insert_id();
			}
		}
		//产品描述属性内容
		//if($p_Type==2 && $CateIdStr!=0){
        if($p_Type==2){
			if($p_ProId && db::get_row_count('products_tab', "ProId='{$p_ProId}' and AttrId='{$p_AttrId}'")){
				db::update('products_tab', "ProId='{$p_ProId}' and AttrId='{$p_AttrId}'", array('IsUsed'=>1));
			}else{
				$insert_ary=array(
					'ProId'			=>	$p_ProId,
					'AttrId'		=>	$p_AttrId,
					'IsUsed'		=>	1
				);
				$p_AddProId>0 && $insert_ary['AccTime']=$c['time'];//记录添加时间
				db::insert('products_tab', $insert_ary);
				$TId=db::get_insert_id();
				manage::database_language_operation('products_tab', "TId='$TId'", array('Description'=>3));
				if($p_AddProId>0 && !in_array($TId, (array)$_SESSION['AddProduct'][$p_AddProId]['Tab'])){//添加产品，记录添加的数据ID
					$_SESSION['AddProduct'][$p_AddProId]['Tab'][]=$TId;
				}
			}
		}
		//返回数据
		if($p_Type==1 && $p_AttrId>0){
			$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId='$p_AttrId'", '*', $c['my_order'].'VId asc')); //属性选项
		}
		$return_data=array();
		$return_data['AttrId']=$p_AttrId;
		$return_data['Type']=$p_Type;
		$return_data['Screen']=$p_IsScreen;
		$return_data['Name']=${"p_Name$Lang"};
		foreach((array)$c['manage']['config']['Language'] as $lang){
			if($p_Type==2){
				//描述属性
				$return_data['Option'][$lang]=htmlspecialchars_decode(str::str_code(${"p_Editor_$lang"}, 'stripslashes'));
			}elseif($p_Type==1){
				//普通属性（选项）
				foreach((array)$value_row as $v){
					$value_data=array(
						'VId'	=>	$v['VId'],
						'Name'	=>	$v["Value_$lang"]
					);
					$attr_data[$num]['Option'][$lang][]=$value_data;
				}
			}else{
				//普通属性（文本框）
				$return_data['Option'][$lang]='';
			}
		}
		ly200::e_json($return_data, 1);
	}
	
	/**
	 *
	 * 修改产品属性的名称、默认内容 (规格属性 or 普通属性 or 描述属性)
	 */
	public static function products_attribute_name_edit()
    {
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
        //检查相同属性名称的数据
        $isRepeat = 0;
        $attr_ary = array();
        $attr_row = db::get_all('products_attribute', "(CateId like '%|$p_CateId|%') and CartAttr='{$p_CartAttr}'");
        foreach ((array)$attr_row as $v) {
            $attr_ary[$v['DescriptionAttr']][$v['AttrId']] = $v;
        }
		if ($p_Name) {
			//规格属性 or 普通属性
			$MyOrder = 0;
			$p_IsScreen = (array)$p_IsScreen;
			foreach ((array)$p_Name as $k => $v) {
                //检查是否已有相同属性名称的数据
                foreach ((array)$attr_ary[0] as $k2 => $v2) {
                    if ($k == $k2) continue; //排除自身属性
                    foreach ((array)$c['manage']['config']['Language'] as $lang) {
                        if ($v2["Name_$lang"] == $v[$lang]) {
                            $isRepeat += 1;
                        }
                    }
                }
                if ($isRepeat > 0) {
                    ly200::e_json($c['manage']['lang_pack']['products']['products']['attr_repeat_tips'], 0);
                }
            }
            foreach ((array)$p_Name as $k => $v) {
				if (db::get_row_count('products_attribute', "AttrId='$k'")) {
                    //确保属性存在
					$data = array();
					foreach ((array)$v as $lang => $v2) {
						$data["Name_$lang"] = addslashes($v2);
					}
					$data['ScreenAttr'] = (int)$p_IsScreen[$k];
					$data['MyOrder'] = (++$MyOrder);
					db::update('products_attribute', "AttrId='$k'", $data);
				}
			}
			$delete_ary = array();
			foreach ((array)$p_IsDelete as $k => $v) {
				if ($v == 1) {
                    //确定要删除属性
					$AttrId = $k;
					$attr_row = str::str_code(db::get_one('products_attribute', "AttrId='$AttrId'")); //确保属性存在
					if ($attr_row) {
						$delete_ary[] = $AttrId;
					}
				}
			}
			if (count($delete_ary)) {
				$delete = implode(',', $delete_ary);
				db::delete('products_attribute', "AttrId in ($delete)");
				db::delete('products_attribute_value', "AttrId in ($delete)");
				db::delete('products_selected_attr', "AttrId in ($delete)");
				db::delete('products_selected_attribute', "AttrId in ($delete)");
				manage::operation_log('删除产品属性');
			}
		}
		if ($p_Tab_Name) {
			//描述属性
            $update_tab_ary = array();
            foreach ((array)$p_Tab_Name as $k => $v) {
                if (!strstr($k, 'Num:')) {
                    //分类描述属性
					$attr_row = db::get_one('products_attribute', "AttrId='$k'");
					$_AttrId = $attr_row['AttrId'];
				} else {
                    //全局描述属性
					$num = (int)str_replace('Num:', '', $k);
					$row = db::get_limit('products_attribute', '(CateId="0" or CateId="") and DescriptionAttr=1', '*', 'MyOrder asc', $num, 1);;
					$_AttrId = $row[0]['AttrId'];
				}
                if ($_AttrId) {
                    //确保描述属性存在
                    //检查是否已有相同属性名称的数据
                    foreach ((array)$attr_ary[1] as $k2 => $v2) {
                        if ($_AttrId == $k2) continue; //排除自身属性
                        foreach ((array)$c['manage']['config']['Language'] as $lang) {
                            if ($v2["Name_$lang"] == $v[$lang]) {
                                $isRepeat += 1;
                            }
                        }
                    }
                    if ($isRepeat > 0) {
                        ly200::e_json($c['manage']['lang_pack']['products']['products']['attr_repeat_tips'], 0);
                    }
                    $update_tab_ary[$k] = array($_AttrId, $v);
                }
            }
			foreach ((array)$update_tab_ary as $k => $v) {
				if ($v[0]) {
                    //确保描述属性存在
					$data = array();
					foreach ((array)$v[1] as $lang => $v2) {
						$data["Name_$lang"] = addslashes($v2);
						$data["Description_$lang"] = addslashes($p_Tab_Txt[$k][$lang]);
					}
					count($data) > 0 && db::update('products_attribute', "AttrId='{$v[0]}'", $data);
				}
			}
			$tab_delete_ary = array();
			foreach ((array)$p_IsTabDelete as $k => $v) {
				if ($v == 1) {
                    //确定要删除描述属性
					$AttrId = $k;
					$attr_row = str::str_code(db::get_one('products_attribute', "AttrId='$AttrId'")); //确保描述属性存在
					if ($attr_row) {
						$tab_delete_ary[] = $AttrId;
					}
				}
			}
			if (count($tab_delete_ary)) {
				$delete = implode(',', $tab_delete_ary);
				db::delete('products_attribute', "AttrId in ($delete)");
				db::delete('products_tab', "AttrId in ($delete)");
				manage::operation_log('删除产品描述属性');
			}
		}
		if ($p_MyOrder) {
			//描述属性(排序)
			$Num = 0;
			$p_ProId = (int)$p_ProId;
			foreach ((array)$p_MyOrder as $v) {
				db::update('products_attribute', "AttrId='$v'", array('MyOrder'=>++$Num));
			}
		}
		if ($p_Name || $p_Tab_Name || $p_MyOrder) {
            //修改成功
			manage::operation_log('修改产品属性');
			ly200::e_json(array('Name'=>$p_Name, 'Delete'=>$delete_ary, 'TabName'=>$p_Tab_Name, 'TabDelete'=>$tab_delete_ary, 'TabOrder'=>$p_MyOrder), 1);
		}
		ly200::e_json('', 0);
	}
	
	/**
	 *
	 * 修改产品属性选项 (规格属性 or 普通属性（选项）)
	 */
	public static function products_attribute_option_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_AttrId=(int)$p_AttrId;
		$insert_ary=$update_ary=$vid_ary=$new_vid_ary=array();
		$MaxMyOrder=db::get_value('products_attribute_value', "AttrId='{$p_AttrId}'", 'MyOrder', 'MyOrder desc');
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId='{$p_AttrId}'", '*', $c['my_order'].'VId asc'));
		foreach((array)$value_row as $k2=>$v2){ $vid_ary[$k2]=$v2['VId']; }
		unset($value_row);
		foreach((array)$p_Name as $k=>$v){
			if(in_array($k, $vid_ary)){//已存在
				foreach((array)$v as $lang=>$v2){
					$update_ary[$k]["Value_$lang"]=$v2;
				}
			}else{//新选项
				foreach((array)$v as $lang=>$v2){
					$insert_ary[$k]["Value_$lang"]=$v2;
				}
				$insert_ary[$k]['AttrId']=$p_AttrId;
				$insert_ary[$k]['MyOrder']=$MaxMyOrder++;
			}
		}
		if(count($update_ary)){
			foreach((array)$update_ary as $k=>$v){
				db::update('products_attribute_value', "VId='$k'", $v);
			}
		}
		if(count($insert_ary)){
			foreach((array)$insert_ary as $k=>$v){
				db::insert('products_attribute_value', $v);
				$VId=db::get_insert_id();
				$new_vid_ary[$k]=$VId;
			}
		}
		$option_ary=$vid_ary=array();
		foreach((array)$p_Name as $k=>$v){
			$key=$new_vid_ary[$k]?$new_vid_ary[$k]:$k;
			$vid_ary[$k]=$key;
			foreach((array)$v as $lang=>$v2){
				$option_ary[$k][$lang]=$v2;
			}
		}
		ly200::e_json(array('Option'=>$option_ary, 'VId'=>$vid_ary, 'AttrId'=>$p_AttrId), 1);
	}
	
	/**
	 *
	 * ??????????????????
	 */
	public static function model_attribute_value_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_AttrId=(int)$p_AttrId;
		$MaxMyOrder=db::get_value('products_attribute_value', "AttrId='{$p_AttrId}'", 'MyOrder', 'MyOrder desc');
		db::insert('products_attribute_value', array(
				'AttrId'	=>	$p_AttrId,
				'MyOrder'	=>	$MaxMyOrder+1
			)
		);
		$VId=db::get_insert_id();
		manage::database_language_operation('products_attribute_value', "VId='$VId'", array('Value'=>0));
		manage::operation_log('添加产品属性自定义项');
		ly200::e_json(array('AttrId'=>$p_AttrId, 'VId'=>$VId), 1);
	}
	
	/**
	 *
	 * 提交关联图片的数据
	 */
	public static function products_associate_pic(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId	= (int)$p_ProId;
		$p_CateId	= (int)$p_CateId;
		$p_AddProId	= (int)$p_AddProId;
		$AttrId		= (int)$p_AttrId;
		$p_IsColor	= (int)$p_IsColor;
		$resize_ary	= $c['manage']['resize_ary']['products'];
		$save_dir	= $c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		$lang		= $c['manage']['web_lang'];
		file::mk_dir($save_dir);
		//针对自身属性
		if(!$AttrId){
			$attr_data=array(
				'CateId'	=>	"|{$p_CateId}|",
				'CartAttr'	=>	1, //规格属性
				'Type'		=>	1,
				"Name_{$c['manage']['config']['LanguageDefault']}"=>addslashes($p_AttrTitle),
			);
			db::insert('products_attribute', $attr_data);
			$AttrId=db::get_insert_id();
			manage::operation_log('添加产品属性');
		}
		//检查属性选项是否存在，没有就自动添加选项
		$insert_ary=$vid_ary=$new_vid_ary=array();
		$MaxMyOrder=db::get_value('products_attribute_value', "AttrId='{$AttrId}'", 'MyOrder', 'MyOrder desc');
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId='{$AttrId}'", '*', $c['my_order'].'VId asc'));
		foreach((array)$value_row as $k2=>$v2){ $vid_ary[$k2]=$v2['VId']; }
		unset($value_row);
		foreach((array)$p_ColorPath as $k=>$v){
			if(!in_array($k, $vid_ary)){//新选项
				$insert_ary[$k]["Value{$lang}"]=addslashes($p_ColorName[$k]);
				$insert_ary[$k]['AttrId']=$AttrId;
				$insert_ary[$k]['MyOrder']=$MaxMyOrder++;
			}
		}
		if(count($insert_ary)){
			foreach((array)$insert_ary as $k=>$v){
				db::insert('products_attribute_value', $v);
				$VId=db::get_insert_id();
				$new_vid_ary[$k]=$VId;
			}
		}
		//关联图片处理
		$CPath=array();
		foreach((array)$p_ColorPath as $k=>$v){
			$k=($new_vid_ary[$k]?$new_vid_ary[$k]:$k);//注入新选项ID
			for($i=0; $i<14; $i++){
				if(!is_file($c['root_path'].$v[$i])) continue;
				$CPath[$k][]=file::photo_tmp_upload($v[$i], $save_dir, $resize_ary);
			}
			for($i=0; $i<15; ++$i){//填补空白
				if(!$CPath[$k][$i]){
					$CPath[$k][$i]='';
				}
			}
		}
		//属性选项数据
		$vid_ary=array();
		foreach((array)$p_ColorPath as $k=>$v){
			$key=($new_vid_ary[$k]?$new_vid_ary[$k]:$k);//注入新选项ID
			$vid_ary[$k]=$key;
		}
		//设置是否为主图
		if($AttrId && $p_ProId){
			if(db::get_row_count('products_selected_attr', "ProId='{$p_ProId}' and AttrId='{$AttrId}'", 'SId')){
				db::update('products_selected_attr', "ProId='{$p_ProId}' and AttrId='{$AttrId}'", array('IsColor'=>$p_IsColor));
			}else{
				$select_data=array(
					'ProId'		=>	$p_ProId,
					'AttrId'	=>	$AttrId,
					'IsUsed'	=>	1,
					'IsColor'	=>	$p_IsColor
				);
				db::insert('products_selected_attr', $select_data);
			}
		}
		ly200::e_json(array('VId'=>$vid_ary, 'AttrId'=>$AttrId, 'AttrDataId'=>$p_AttrId, 'IsColor'=>$p_IsColor, 'Data'=>$CPath), 1);
	}
	
	public static function products_associate_pic_ajax()
    {
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId = (int)$p_ProId;
		$p_AddProId = (int)$p_AddProId;
		$AttrId = (int)$p_AttrId;
		$p_IsColor = (int)$p_IsColor;
		$resize_ary	= $c['manage']['resize_ary']['products'];
        $Num = (int)$p_Num;
		$ret = 1;
		$i = 0;
        if ($Num == 1) {
            //第一次 检查数据
            foreach ((array)$p_Data as $k => $v) {
                $j = 0;
                foreach ((array)$v as $k2 => $v2) {
                    if (trim($v2)=='') ++$j;
                }
                if (count($v) == $j) {
                    unset($p_Data[$k]); //没有一张图片，请删掉
                }
            }
        }
		foreach ((array)$p_Data as $k => $v) {
			if ($i > 0) break;
			$VId = $k;
			if ($c['manage']['config']['IsWater']) {
                //更新水印图片
				if ($p_ProId) {
					$pro_img_ary = array();
					$pro_img_row = db::get_one('products_color', "ProId='{$p_ProId}' and VId='{$VId}'", $c['prod_image']['sql_field']);
					foreach ((array)$pro_img_row as $v1) {
						if ($v1) $pro_img_ary[] = $v1;
					}
				}
				foreach ((array)$v as $k2 => $v2) {
					if (($p_ProId && @in_array($v2, (array)$pro_img_ary)) || !$v2) continue; //已经存在图片不更新水印
					$water_ary = array($v2);
					$ext_name = file::get_ext_name($v2);
					@copy($c['root_path'] . $v2 . ".default.{$ext_name}", $c['root_path'] . $v2); //覆盖大图
					if($c['manage']['config']['IsThumbnail']){
                        //缩略图加水印
						img::img_add_watermark($v2);
						$water_ary = array();
					}
					foreach ((array)$resize_ary as $v3) {
						if ($v2 == 'default') continue;
						$size_w_h = explode('x', $v3);
						$resize_img = $v2;
						$resize_path = img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
					}
					foreach ((array)$water_ary as $v3) {
						img::img_add_watermark($v3);
					}
				}
			}
			$c_data = array();
			foreach ((array)$v as $k2 => $v2) {
				$c_data['PicPath_' . $k2] = $v2;
			}
			if (db::get_one('products_color', "ProId='$p_ProId' and VId='$VId'")) {
				db::update('products_color', "ProId='$p_ProId' and VId='$VId'", $c_data);
			} else {
				$c_data['ProId'] = $p_ProId;
				$c_data['VId'] = $VId;
				$p_AddProId > 0 && $c_data['AccTime'] = $c['time']; //记录添加时间
				db::insert('products_color', $c_data);
				if ($p_AddProId > 0) {
                    //添加产品，记录添加的数据ID
					$CId = db::get_insert_id();
					$_SESSION['AddProduct'][$p_AddProId]['Color'][] = $CId;
					$select_data = array(
						'ProId'		=>	$p_ProId,
						'AttrId'	=>	$AttrId,
						'IsUsed'	=>	1,
						'IsColor'	=>	$p_IsColor
					);
					db::insert('products_selected_attr', $select_data);
					$select_id = db::get_insert_id();
					$_SESSION['AddProduct'][$p_AddProId]['SelectedAttrId'][] = $select_id;
				}
			}
			++$i;
		}
		unset($p_Data[$VId]);
		if (count($p_Data) == 0) {
			$p_Data = '';
			$ret = 2;
		}
		ly200::e_json(array('Data'=>$p_Data, 'AttrId'=>$AttrId, 'IsColor'=>$p_IsColor, 'VId'=>$VId), $ret);
	}
	
	public static function products_img_del(){//删除单个产品图片
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$Model=$g_Model;
		$PicPath=$g_Path;
		$Index=(int)$g_Index;
		$resize_ary=$c['manage']['resize_ary'][$Model];	//products
		if(is_file($c['root_path'].$PicPath)){
			foreach((array)$resize_ary as $v){
				$ext_name=file::get_ext_name($PicPath);
				file::del_file($PicPath.".{$v}.{$ext_name}");
			}
			file::del_file($PicPath);
		}
		//参数纯粹作记录
		$g_ProId=(int)$g_ProId;
		$pro_row=db::get_one('products', "ProId='{$g_ProId}'", 'Prefix, Number');
		$_GET['Number']=$pro_row['Prefix'].$pro_row['Number'];
		manage::operation_log('删除产品图片');
		ly200::e_json(array($Index), 1);
	}
	
	public static function products_unit_del(){//删除单个产品单位
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$unit_list=str::json_data(htmlspecialchars_decode(db::get_value('config', 'GroupId="products" and Variable="Unit"', 'Value')), 'decode');
		if($unit_list[$p_Key]){
			unset($unit_list[$p_Key]);
			$json_unit=addslashes(str::json_data(str::str_code($unit_list, 'stripslashes')));
			manage::config_operaction(array('Unit'=>$json_unit), 'products');
		}
		$item=db::get_value('config', 'GroupId="products_show" and Variable="item"', 'Value');
		if($item==$p_Unit){ //如果删除单位的名称 跟 产品自定义单位的名称 一致
			manage::config_operaction(array('item'=>''), 'products_show');
		}
		ly200::e_json('', 1);
	}
	
	public static function products_seo_keywords(){//查询产品相关的关键词
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$ProId=(int)$p_ProId;
		$seo_row=str::str_code(db::get_one('products_seo', "ProId='$ProId'"));
		$keyword_ary=array();
		foreach($c['manage']['config']['Language'] as $lang){
			$keys_id_ary=explode(',', $seo_row["SeoKeyword_{$lang}"]);
			$keys_id_ary=array_filter($keys_id_ary);
			$keyword_ary[$lang]=$keys_id_ary;
		}
		ly200::e_json($keyword_ary, 1);
	}
	
	public static function products_keyword_edit(){//修改关键词
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ProId	= (int)$p_ProId;
		$p_AddProId	= (int)$p_AddProId;
		$data_ary	= array();
		$data		= array();
		foreach((array)$p_Name as $k=>$v){
			foreach((array)$v as $lang=>$v2){
				$data_ary[$lang][]=$v2;
			}
		}
		foreach((array)$data_ary as $k=>$v){
			$data["SeoKeyword_$k"]=@implode(',', $v);
		}
		if($data){
			if(db::get_one('products_seo', "ProId='$p_ProId'")){
				db::update('products_seo', "ProId='$p_ProId'", $data);
			}else{
				$data['ProId']=$p_ProId;
				db::insert('products_seo', $data);
				if($p_AddProId>0){//添加产品，记录添加的数据ID
					$SId=db::get_insert_id();
					$_SESSION['AddProduct'][$p_AddProId]['Keyword']=$SId;
				}
			}
		}
		ly200::e_json($data_ary, 1);
	}
	
	public static function products_edit_category(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CateId=(int)$p_CateId;
		$result='';
		if((float)db::get_value('products', "ProId='{$p_ProId}'", 'CateId')!=$p_CateId){
			db::update('products', "ProId='{$p_ProId}'", array('CateId'=>$p_CateId));
			manage::operation_log('产品修改分类');
			
			//返回分类资料
			$cate_ary=str::str_code(db::get_all('products_category', '1', '*'));
			$category_ary=array();
			foreach((array)$cate_ary as $v) $category_ary[$v['CateId']]=$v;
			$UId=$category_ary[$p_CateId]['UId'];
			if($UId){
				$key_ary=@explode(',',$UId);
				array_shift($key_ary);
				array_pop($key_ary);
				foreach((array)$key_ary as $k2=>$v2){
					$result.=$category_ary[$v2]['Category'.$c['manage']['web_lang']].'->';
				}
			}
			$result.=$category_ary[$p_CateId]['Category'.$c['manage']['web_lang']];
			unset($cate_ary, $category_ary);
		}
		ly200::e_json($result, 1);
	}
	
	public static function products_edit_price(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$result = array();
		$p_Price = (float)$p_Price;
		if ((float)db::get_value('products', "ProId='{$p_ProId}'", $p_Type) != $p_Price) {
			db::update('products', "ProId='{$p_ProId}'", array(
                $p_Type     =>  $p_Price,
                'EditTime'  =>  $c['time']
            ));
            $result['Price'] = $p_Price;
            $result['Time'] = date('Y-m-d', $c['time']);
			manage::operation_log('产品修改价格');
		}
		ly200::e_json($result, 1);
	}
	
	public static function products_edit_myorder(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number = (int)$p_Number;
		db::update('products', "ProId='{$p_ProId}'", array(
            'MyOrder'   =>  $p_Number,
            'EditTime'  =>  $c['time']
        ));
		manage::operation_log('产品修改排序');
        $result = array(
            'MyOrder'   =>  manage::language($c['manage']['my_order'][$p_Number]),
            'Time'      =>  date('Y-m-d', $c['time'])
        );
		ly200::e_json($result, 1);
	}
	
	public static function products_copy(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$ProId=(int)$g_ProId;
		$data=$seo_data=$desc_data=array();
		$row=db::get_one('products', "ProId='$ProId'");
		foreach((array)$row as $k=>$v){
			$data[$k]=addslashes($v);
		}
		unset($data['ProId'], $data['Prefix'], $data['Number'], $data['Sales']);
		$seo_row=db::get_one('products_seo', "ProId='$ProId'");
		foreach((array)$seo_row as $k=>$v){
			$seo_data[$k]=addslashes($v);
		}
		unset($seo_data['SId']);
		$desc_row=db::get_one('products_description', "ProId='$ProId'");
		foreach((array)$desc_row as $k=>$v){
			$desc_data[$k]=addslashes($v);
		}
		unset($desc_data['DId']);
		$resize_ary=$c['manage']['resize_ary']['products'];
		$temp_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($temp_dir);
		for($i=0; $i<$c['prod_image']['count']; ++$i){
			$PicPath=$row["PicPath_$i"];
			if(is_file($c['root_path'].$PicPath)){
				$ext_name=file::get_ext_name($PicPath);
				$data["PicPath_$i"]=$temp=$temp_dir.str::rand_code().'.'.$ext_name;
				foreach((array)$resize_ary as $v){
					$RePicPath=$PicPath.".{$v}.{$ext_name}";
					@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$v}.{$ext_name}", '/'));
				}
				@copy($c['root_path'].$PicPath, $c['root_path'].ltrim($temp, '/'));
			}
		}
		db::insert('products', $data);
		$proid=db::get_insert_id();
		$seo_data['ProId']=$desc_data['ProId']=$proid;
		db::insert('products_seo', $seo_data);
		db::insert('products_description', $desc_data);
		// 复制products_development表
		db::insert('products_development',array('ProId'=>$proid));
		//属性
		$attr_row=db::get_all('products_selected_attr', "ProId='$ProId'");
		foreach((array)$attr_row as $k=>$v){
			$attr_data=array();
			foreach((array)$v as $k2=>$v2){
				$attr_data[$k2]=addslashes($v2);
			}
			$attr_data['ProId']=$proid;
			unset($attr_data['SId']);
			db::insert('products_selected_attr', $attr_data);
		}
		//属性选项
		$attr_row=db::get_all('products_selected_attribute', "ProId='$ProId'");
		foreach((array)$attr_row as $k=>$v){
			$attr_data=array();
			foreach((array)$v as $k2=>$v2){
				$attr_data[$k2]=addslashes($v2);
			}
			$attr_data['ProId']=$proid;
			unset($attr_data['SeleteId']);
			db::insert('products_selected_attribute', $attr_data);
		}
		//组合属性选项
		$insert_combination='';
		$combination_row=db::get_all('products_selected_attribute_combination', "ProId='$ProId'");
		foreach((array)$combination_row as $k=>$v){
			$insert_combination.=($k?',':'')."('{$proid}', '{$v['Combination']}', '{$v['OvId']}', '{$v['SKU']}', '{$v['IsIncrease']}', '{$v['Price']}', '{$v['Stock']}', '{$v['Weight']}')";
		}
		$insert_combination && db::query('insert into products_selected_attribute_combination (ProId, Combination, OvId, SKU, IsIncrease, Price, Stock, Weight) values'.$insert_combination);
		//颜色图片
		$color_row=db::get_all('products_color', "ProId='$ProId'");
		foreach((array)$color_row as $row){
			$color_data=array(
				'ProId'		=>	$proid,
				'VId'		=>	$row['VId'],
				'AccTime'	=>	$c['time'],
			);
			for($i=0; $i<$c['prod_image']['count']; ++$i){
				$PicPath=$row["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					$ext_name=file::get_ext_name($PicPath);
					$color_data["PicPath_$i"]=$temp=$temp_dir.str::rand_code().'.'.$ext_name;
					foreach((array)$resize_ary as $v){
						$RePicPath=$PicPath.".{$v}.{$ext_name}";
						@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$v}.{$ext_name}", '/'));
					}
					@copy($c['root_path'].$PicPath, $c['root_path'].ltrim($temp, '/'));
				}
			}
			db::insert('products_color', $color_data);
		}
		//选项卡
		$tab_row=db::get_all('products_tab', "ProId='$ProId'");
		foreach((array)$tab_row as $k=>$v){
			$tab_data=array();
			foreach((array)$v as $k2=>$v2){
				$tab_data[$k2]=addslashes($v2);
			}
			unset($tab_data['TId']);
			$tab_data['ProId']=$proid;
			db::insert('products_tab', $tab_data);
		}
		$_GET['NewProId']=$proid;
		manage::operation_log('复制产品');
		ly200::e_json($proid, 1);
	}
	
	public static function products_sold_bat(){
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$g_sold=(int)$g_sold;
		$bat_where="ProId in(".str_replace('-', ',', $g_id).")";
		db::update('products', $bat_where, array('SoldOut'=>$g_sold));
		manage::operation_log($g_sold?'批量产品下架':'批量产品上架');
		ly200::e_json('', 1);
	}
	
	public static function products_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$ProId=(int)$g_ProId;
		$products_row=db::get_one('products', "ProId='$ProId'");
		manage::auto_add_recycle_column('products', 'products_recycle'); //自动添加产品表新添加的字段
		foreach((array)$products_row as $k=>$v){
			$products_row[$k]=addslashes($v);
		}
		$products_row['DelTime']=$c['time'];
		db::insert('products_recycle', $products_row);
		$new_proid=db::get_insert_id();
		if($new_proid){
			db::delete('products', "ProId={$ProId}");
		}
		manage::operation_log('删除产品');
		ly200::e_json('', 1);
	}
	
	public static function products_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="ProId in(".str::ary_format(str_replace('-',',',$g_id), 2).")";
		$products_row=db::get_all('products', $del_where);
		manage::auto_add_recycle_column('products', 'products_recycle'); //自动添加产品表新添加的字段
		foreach((array)$products_row as $v){
			foreach((array)$v as $k1=>$v1){
				$v[$k1]=addslashes($v1);
			}
			$v['DelTime']=$c['time'];
			db::insert('products_recycle', $v);
			$new_proid=db::get_insert_id();
			if($new_proid){
				db::delete('products', "ProId={$new_proid}");
			}
		}
		manage::operation_log('批量删除产品');
		ly200::e_json('', 1);
	}
	
	//批量修改产品价格
	public static function products_batch_price(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($p_ProId){
			$p_ProId=(array)$p_ProId;
			foreach((array)$p_ProId as $k=>$v){
				$ProId=(int)$v;
				if(!$ProId) continue;
				
				$SoldOutTime=@explode('/', $p_SoldOutTime[$k]);
				$SStartTime=@strtotime($SoldOutTime[0]);
				$SEndTime=@strtotime($SoldOutTime[1]);
				$SoldStatus=(int)$p_SoldStatus[$ProId];
				$SoldOut=(int)$p_SoldOut[$ProId];
				$SoldStatus==0 && $SoldOut==0 && $SoldOut=2; //库存为0 不允许购买
				$data=array(
					'Price_0'		=>	$p_Price_0[$k],
					'Price_1'		=>	$p_Price_1[$k],
					//'PurchasePrice'	=>	$p_PurchasePrice[$k],
					'SoldOut'		=>	(int)$SoldOut,
					'IsSoldOut'		=>	(int)$p_IsSoldOut[$ProId],
					'SStartTime'	=>	$SStartTime,
					'SEndTime'		=>	$SEndTime,
					'IsIndex'		=>	(int)$p_IsIndex[$ProId],
					'IsNew'			=>	(int)$p_IsNew[$ProId],
					'IsHot'			=>	(int)$p_IsHot[$ProId],
					'IsBestDeals'	=>	(int)$p_IsBestDeals[$ProId],
					'IsFreeShipping'=>	(int)$p_IsFreeShipping[$ProId],
					'MyOrder'		=>	(int)$p_MyOrder[$k]
				);
				db::update('products', "ProId='$ProId'", $data);
				$LowestPrice=cart::range_price_ext(db::get_one('products', "ProId='{$ProId}'"));
				db::update('products', "ProId='{$ProId}'", array('LowestPrice'=>$LowestPrice[0]));
			}
		}else{
			$p_CateId=(int)$p_CateId;
			$p_Type=(int)$p_Type;
			$where=1;
			if($p_CateId){
				$cate_row=db::get_one('products_category', "CateId='$p_CateId'");
				$where.=' and '.category::get_search_where_by_CateId($p_CateId, 'products_category');
			}
			if($p_Type){//百分比
				$p_Rate=(float)$p_Rate;
				$p_Rate=1+($p_Rate/100);
				db::query("update products set Price_1=Price_1*$p_Rate where $where");
			}else{//价格
				$p_Price=(float)$p_Price;
				db::query("update products set Price_1=Price_1+$p_Price where $where");
			}
			db::update('products', "{$where} and Price_1<0", array('Price_1'=>0));
		}
		ly200::e_json($c['manage']['lang_pack']['products']['action']['batch_edit_success'].'<br />', 1);
	}
	
	//批量移动产品
	public static function products_batch_move(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CateId=(int)$p_CateId;//来源分类
		$p_CateIdTo=(int)$p_CateIdTo;//目标分类
		if(!$p_CateId || !$p_CateIdTo) ly200::e_json('', 0);
		if($p_CateId==$p_CateIdTo) ly200::e_json(manage::language('{/products.batch.equal_tips/}'), 0);
		
		$where="1 and CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($p_CateId)."%') or CateId='{$p_CateId}'";
		$prod_count=db::get_row_count('products', $where);
		
		if($prod_count){
			db::update('products', $where, array('CateId'=>$p_CateIdTo));
		}else{
			ly200::e_json($c['manage']['lang_pack']['products']['action']['no_products'], 0);
		}
		unset($prod_count);
		ly200::e_json($c['manage']['lang_pack']['products']['action']['batch_move_success'], 1);
	}
	
	//批量更新水印
	public static function products_batch_watermark(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;//当前分类
		$p_Number=(int)$p_Number;//当前分开数
		
		if($CateId){
			$where="CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($CateId)."%') or CateId='{$CateId}'";
			$prod_count=db::get_row_count('products', $where);//产品总数
		}
		
		//初始化
		$Start=0;//开始执行位置
		$page_count=10;//每次分开更新的数量
		$total_pages=ceil($prod_count/$page_count);
		if($p_Number<$total_pages){//继续执行
			$Start=$page_count*$p_Number;
		}else{
			manage::operation_log('产品水印图片批量更新');
			ly200::e_json('<p>'.$c['manage']['lang_pack']['products']['action']['batch_updata_success'].'</p>', 1);
		}
		$data_ary=$pro_id_ary=array();
		$products_row=db::get_limit('products', $where, 'ProId, '.$c['prod_image']['sql_field'], 'ProId desc', $Start, $page_count);
		foreach((array)$products_row as $v){
			$pro_id_ary[]=$v['ProId'];
			$data_ary[$v['ProId']]['pro']=$v;
		}
		$products_color_row=db::get_all('products_color', 'ProId in ('.implode(',', $pro_id_ary).')', '*', 'ProId desc');
		foreach((array)$products_color_row as $v){
			$data_ary[$v['ProId']]['color'][$v['CId']]=$v;
		}
		//图片储存位置
		$resize_ary=$c['manage']['resize_ary']['products'];
		$save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($save_dir);
		//开始更新
		$No=0;
		foreach((array)$data_ary as $ProId=>$obj){
			$ImgPath=$CPath=array();
			foreach((array)$obj['pro'] as $key=>$val){	//产品主图片
				if($key=='ProId') continue;
				if(!is_file($c['root_path'].$val)) continue;
				//图片上传
				$ImgPath[]=$val;
				$water_ary=array($val);
				$ext_name=file::get_ext_name($val);
				@copy($c['root_path'].$val.".default.{$ext_name}", $c['root_path'].$val);//覆盖大图
				if($c['manage']['config']['IsThumbnail']){//缩略图加水印
					img::img_add_watermark($val);
					$water_ary=array();
				}
				foreach((array)$resize_ary as $v2){
					if($v2=='default') continue;
					$size_w_h=explode('x', $v2);
					$resize_path=img::resize($val, $size_w_h[0], $size_w_h[1]);
				}
				foreach((array)$water_ary as $v2){
					img::img_add_watermark($v2);
				}
			}
			foreach((array)$obj['color'] as $ColorId=>$row){	//产品颜色图片
				foreach((array)$row as $key=>$val){
					if($key=='CId' || $key=='ProId' || $key=='ColorId') continue;
					if(!is_file($c['root_path'].$val)) continue;
					$CPath[$ColorId][]=$val;
					$water_ary=array($val);
					$ext_name=file::get_ext_name($val);
					@copy($c['root_path'].$val.".default.{$ext_name}", $c['root_path'].$val);//覆盖大图
					if($c['manage']['config']['IsThumbnail']){//缩略图加水印
						img::img_add_watermark($val);
						$water_ary=array();
					}
					foreach((array)$resize_ary as $v2){
						if($v2=='default') continue;
						$size_w_h=explode('x', $v2);
						$resize_path=img::resize($val, $size_w_h[0], $size_w_h[1]);
					}
					foreach((array)$water_ary as $v2){
						img::img_add_watermark($v2);
					}
				}
			}
			++$No;
		}
		if($p_Number<$total_pages){//继续执行
			$item=($No<$page_count)?($page_count*$p_Number+$No):($page_count*($p_Number+1));
			ly200::e_json(array(($p_Number+1), '<p>'.$c['manage']['lang_pack']['products']['action']['updata_success'].$item.'个</p>'), 2);
		}
	}
	
	public static function products_explode(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CateId=(int)$p_CateId;
		$p_Number=(int)$p_Number;
		if(!$p_Number) unset($_SESSION['ProZip']);
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		//(A ~ EZ)
		$arr=range('A', 'Z');
		$ary=$arr;
		for($i=0; $i<10; ++$i){
			$num=$arr[$i];
			foreach((array)$arr as $v){
				$ary[]=$num.$v;
			}
		}
		//myorder
		$myOrderAry=array(
			0	=>	'',
			1	=>	'p.Sales desc,',
			2	=>	'p.Sales asc,'
		);
		//语言版本
		$language=$p_Language;
		if(!$language || !in_array($language, $c['manage']['web_lang_list'])){//找不到相对应的语言，默认为可用语言里面的第一个
			$language=$c['manage']['web_lang_list'][0];
		}
		//初始化
		//$cart_attr_count=0;
		$cart_attr_ary=array();
		$attribute_ary=$attribute_top_ary=$all_value_ary=$vid_data_ary=$selected_ary=$combination_ary=array();
		//产品分类
		$CateId=$p_CateId;
		$uid=category::get_UId_by_CateId($CateId);
		$uid && $uid!='0,' && $CateId=category::get_top_CateId_by_UId($uid);
		$products_category_row=str::str_code(db::get_one('products_category', "CateId='{$CateId}'"));
		$AttrId=$products_category_row['AttrId'];
		
		//准备工作
		$page_count=$p_PageNum;//每次分开导出的数量
		$page=$page_count*$p_Number;
		$where='1';
		$p_CateId && $where.=" and (p.CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($p_CateId)."%') or p.CateId='{$p_CateId}')";
		$row_count=db::get_row_count('products p', $where, 'ProId');
		$total_pages=ceil($row_count/$page_count);
		
		//产品属性
		$cart_attr_ary=array();//记录规格属性的总数
		$whereValue="CateId like '%|{$p_CateId}|%'";
		$all_attribute_row=db::get_all('products_category', "UId like '0,{$p_CateId}%'", 'CateId');
		foreach($all_attribute_row as $v){
			$whereValue.=" or CateId like '%|{$v['CateId']}|%'";
		}
		$attribute_row=str::str_code(db::get_all('products_attribute', "($whereValue) and DescriptionAttr=0", "AttrId, Type, Name_{$language}, CateId, CartAttr, ColorAttr"));
		$_attribute_value_where='0';
		foreach((array)$attribute_row as $v){
			if($v['CartAttr']){//规格属性
				$_type='Cart';
			}else{//普通属性
				$_type='Common';
			}
			$attribute_ary[$_type][$v['AttrId']]=array(0=>$v['Type'], 1=>$v["Name_{$language}"]);
			$_attribute_value_where.=",{$v['AttrId']}";
		}
		unset($attribute_row);
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in($_attribute_value_where)", "VId, AttrId, DelId, Value_{$language}", $c['my_order'].'VId asc')); //所有属性选项
		foreach((array)$value_row as $v){
			$all_value_ary[$v['AttrId']][$v['VId']]=$v;
			$vid_data_ary[$v['VId']]=$v;
		}
		unset($value_row);
		$selected_row=db::get_all('products_selected_attribute', "IsUsed=1 and AttrId in($_attribute_value_where)", "SeleteId, AttrId, ProId, VId, OvId, IsUsed, MyOrder, Value_{$language}", 'SeleteId asc');
		foreach((array)$selected_row as $v){
			if($v['VId']==0){
				if($v['AttrId']==0){//记录勾选属性ID 发货地
					$selected_ary[$v['ProId']]['Overseas'][]=$v['OvId'];
				}else{//文本框内容
					$selected_ary[$v['ProId']]['Value'][$v['AttrId']]=$v['Value_'.$language];
				}
			}else{
				$selected_ary[$v['ProId']]['Id'][$v['AttrId']][]=$v['VId']; //记录勾选属性ID
				if($attribute_ary['Cart'][$v['AttrId']] && !in_array($v['AttrId'], (array)$cart_attr_ary[$v['ProId']])){ $cart_attr_ary[$v['ProId']][]=$v['AttrId']; }//记录规格属性的总数
			}
		}
		//$cart_attr_count=count($cart_attr_ary);
		unset($selected_row);
		$explode_ProId_row=db::get_limit('products p', $where, 'ProId', $myOrderAry[$g_MyOrder].'if(p.MyOrder>0, p.MyOrder, 100000) asc, p.ProId desc', $page, $page_count);
		$explode_ProId_ary=array();
		foreach((array)$explode_ProId_row as $v){
			$explode_ProId_ary[]=$v['ProId'];
		}
		$explode_ProId=implode(',', $explode_ProId_ary);
		unset($explode_ProId_row, $explode_ProId_ary);
		!$explode_ProId && $explode_ProId=-1;
		$combinatin_row=db::get_all('products_selected_attribute_combination', "ProId in({$explode_ProId})", '*', 'CId asc');
		//$combinatin_row=db::get_all('products_selected_attribute_combination', "ProId in(select ProId from products p where {$where})", '*', 'CId asc');
		foreach((array)$combinatin_row as $v){
			$combinatin_ary[$v['ProId']][$v['Combination']][$v['OvId']]=array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
		}
		unset($combinatin_row);
		
		//海外仓
		$shipping_overseas_row=db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc');
		$oversea_len=count($shipping_overseas_row);
		$oversea_name_ary=array();
		foreach((array)$shipping_overseas_row as $v){
			$oversea_name_ary[$v['OvId']]=$v["Name_{$language}"];
		}
				
		$zipAry=array();//储存需要压缩的文件
		$save_dir='/tmp/';//临时储存目录
		file::mk_dir($save_dir);
		if($p_Number<$total_pages){
			$num=0;
			$products_row=str::str_code(db::get_limit('products p left join products_seo s on p.ProId=s.ProId left join products_description d on p.ProId=d.ProId', $where, "p.Cubage,p.ProId, p.Name_{$language}, p.CateId, p.ExtCateId, p.Prefix, p.Number, p.SKU, p.Price_0, p.Price_1, p.Wholesale, p.PageUrl, p.PicPath_0, p.PicPath_1, p.PicPath_2, p.PicPath_3, p.PicPath_4, p.PicPath_5, p.PicPath_6, p.PicPath_7, p.PicPath_8, p.PicPath_9, p.PicPath_10, p.PicPath_11, p.PicPath_12, p.PicPath_13, p.PicPath_14, p.IsOpenAttrPrice, p.IsCombination, p.Weight, p.IsVolumeWeight, p.MOQ, p.Stock, p.StockOut, p.SoldOut, p.IsSoldOut, p.SStartTime, p.SEndTime, p.IsFreeShipping, p.IsNew, p.IsHot, p.IsBestDeals, p.IsIndex, s.SeoTitle_{$language}, s.SeoKeyword_{$language}, s.SeoDescription_{$language}, s.SId, d.Description_{$language}", $myOrderAry[$g_MyOrder].'if(p.MyOrder>0, p.MyOrder, 100000) asc, p.ProId desc', $page, $page_count));
			
			$objPHPExcel=new PHPExcel();
			//Set properties 
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
			$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
			$objPHPExcel->getProperties()->setCategory("Test result file");
			$objPHPExcel->setActiveSheetIndex(0);
			
			$attr_column=array(
				$c['manage']['lang_pack']['orders']['export']['proname'],//产品名称
				$c['manage']['lang_pack']['products']['classify'],//产品分类
				$c['manage']['lang_pack']['products']['products']['expand'],//产品多分类
				$c['manage']['lang_pack']['products']['products']['number'],//产品编号
				$c['manage']['lang_pack']['products']['products']['sku'],//SKU
				$c['manage']['lang_pack']['products']['products']['price_ary'][0],//市场价
				$c['manage']['lang_pack']['products']['products']['price_ary'][1],//商城价
				$c['manage']['lang_pack']['products']['products']['wholesale_price'],//批发价
				$c['manage']['lang_pack']['page']['page']['custom_url'],//自定义地址
				$c['manage']['lang_pack']['products']['picture'],//图片
				$c['manage']['lang_pack']['products']['explode']['common_attr_0'],//普通属性（文本）
				$c['manage']['lang_pack']['products']['explode']['common_attr_1'],//普通属性（选项）
				$c['manage']['lang_pack']['products']['explode']['common_attr_2'],//普通属性（筛选项）
				$c['manage']['lang_pack']['products']['products']['attr_info'],//规格属性
				$c['manage']['lang_pack']['products']['products']['attr_price_info'],//属性价格
				$c['manage']['lang_pack']['products']['products']['associated_picture'],//关联图片
				$c['manage']['lang_pack']['products']['explode']['switch_0'],//属性价格开关
				$c['manage']['lang_pack']['products']['explode']['switch_1'],//属性组合开关
				$c['manage']['lang_pack']['products']['products']['weight'],//重量
				$c['manage']['lang_pack']['products']['products']['cubage'],//体积
				$c['manage']['lang_pack']['products']['products']['volume_weight'],//体积重
				$c['manage']['lang_pack']['products']['products']['moq'],//起订量
				$c['manage']['lang_pack']['products']['products']['maxoq'],//最大购买量
				$c['manage']['lang_pack']['products']['products']['stock'],//总库存
				$c['manage']['lang_pack']['products']['products']['stock_out_status'],//脱销状态
				$c['manage']['lang_pack']['products']['products']['sold_out'],//下架
				$c['manage']['lang_pack']['products']['products']['sold_in_time'],//定时上架
				$c['manage']['lang_pack']['products']['products']['sold_in_time'].$c['manage']['lang_pack']['global']['time'],//定时上架时间
				$c['manage']['lang_pack']['products']['products']['free_shipping'],//免运费
				$c['manage']['lang_pack']['products']['products']['is_new'],//新品
				$c['manage']['lang_pack']['products']['products']['is_hot'],//热卖
				$c['manage']['lang_pack']['products']['products']['is_best_deals'],//畅销
				$c['manage']['lang_pack']['products']['products']['is_index'],//首页显示
				$c['manage']['lang_pack']['products']['products']['seo_title'],//标题
				$c['manage']['lang_pack']['news']['news']['seo_keyword'],//关键词
				$c['manage']['lang_pack']['products']['products']['seo_brief'],//描述
				$c['manage']['lang_pack']['products']['products']['briefdescription'],//简短介绍
				$c['manage']['lang_pack']['products']['products']['description']//详细介绍
			);
			
			$m=2;
			$lang_ary=array('SeoTitle_', 'SeoKeyword_', 'SeoDescription_', 'BriefDescription_', 'Description_');
			foreach((array)$products_row as $v){
				$num=$mMax_1=0;
				$mMax=1;
				$ProId=$v['ProId'];
				$ExtCateId=$v['ExtCateId'];
				$ExtCateId && $ExtCateId=substr($v['ExtCateId'], 1, -1);
				$Wholesale=str_replace(array('{', '}', '"'), '', htmlspecialchars_decode($v['Wholesale']));
				$Wholesale=='[]' && $Wholesale='';
				$Cubage=str_replace(',', '*', $v['Cubage']);
				$data=array($v['Name_'.$language], $v['CateId'], $ExtCateId, $v['Prefix'].$v['Number'], $v['SKU'], (float)$v['Price_0'], (float)$v['Price_1'], $Wholesale, $v['PageUrl'], $v['PicPath_0'], '', '', '', '', '', '', (int)$v['IsOpenAttrPrice'], (int)$v['IsCombination'], (float)$v['Weight'], $Cubage, (int)$v['IsVolumeWeight'], (int)$v['MOQ'], (int)$v['MaxOQ'], (int)$v['Stock'], (int)$v['StockOut'], (int)$v['SoldOut'], (int)$v['IsSoldOut'], date('Y/m/d H:i:s', $v['SStartTime']).' - '.date('Y/m/d H:i:s', $v['SEndTime']), (int)$v['IsFreeShipping'], (int)$v['IsNew'], (int)$v['IsHot'], (int)$v['IsBestDeals'], (int)$v['IsIndex']);
				//设置单元格的值
				foreach((array)$data as $k2=>$v2){
					$objPHPExcel->getActiveSheet()->setCellValue($ary[$k2].$m, $v2);
					++$num;
				}
				foreach((array)$lang_ary as $k2=>$v2){
					$objPHPExcel->getActiveSheet()->setCellValue($ary[$num+$k2].$m, htmlspecialchars_decode($v[$v2.$language]));
				}
				//图片
				for($i=1; $i<$c['prod_image']['count']; ++$i){
					if(is_file($c['root_path'].$v["PicPath_{$i}"])){
						$objPHPExcel->getActiveSheet()->setCellValue($ary[9].($m+$i), $v["PicPath_{$i}"]);
						++$mMax;
					}
				}
				//普通属性（文本）
				if($selected_ary[$ProId]['Value']){
					$mMax_1=0;
					foreach((array)$selected_ary[$ProId]['Value'] as $k2=>$v2){
						if(!$v2) continue;
						$objPHPExcel->getActiveSheet()->setCellValue($ary[10].($m+$mMax_1), $attribute_ary['Common'][$k2][1].'['.$v2.']');
						++$mMax_1;
					}
					$mMax_1>$mMax && $mMax=$mMax_1;
				}
				//普通属性（选项）
				if($selected_ary[$ProId]['Id']){
					$mMax_1=0;
					foreach((array)$selected_ary[$ProId]['Id'] as $k2=>$v2){
						if(!$v2) continue;
						if($attribute_ary['Common'][$k2]){
							$value='';
							$_arr=array();
							foreach((array)$v2 as $v3){
								$_arr[]=$vid_data_ary[$v3]['Value_'.$language];
							}
							$value=implode(',', $_arr);
							$objPHPExcel->getActiveSheet()->setCellValue($ary[11].($m+$mMax_1), $attribute_ary['Common'][$k2][1].'['.$value.']');
							++$mMax_1;
						}
					}
					$mMax_1>$mMax && $mMax=$mMax_1;
				}
				//规格属性
				if($selected_ary[$ProId]['Id']){
					$mMax_1=0;
					foreach((array)$selected_ary[$ProId]['Id'] as $k2=>$v2){
						if(!$v2) continue;
						if($attribute_ary['Cart'][$k2]){
							$value='';
							$_arr=array();
							foreach((array)$v2 as $v3){
								$_arr[]=$vid_data_ary[$v3]['Value_'.$language];
							}
							$value=implode(',', $_arr);
							$objPHPExcel->getActiveSheet()->setCellValue($ary[13].($m+$mMax_1), $attribute_ary['Cart'][$k2][1].'['.$value.']');
							++$mMax_1;
						}
					}
					if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && count($selected_ary[$ProId]['Overseas'])>1){//发货地
						$value='';
						foreach((array)$selected_ary[$ProId]['Overseas'] as $k2=>$v2){
							if(!$v2) continue;
							$value.=($k2?',':'').$oversea_name_ary[$v2];
						}
						$objPHPExcel->getActiveSheet()->setCellValue($ary[13].($m+$mMax_1), 'Oversea['.$value.']');
						++$mMax_1;
					}
					$mMax_1>$mMax && $mMax=$mMax_1;
				}
				//属性价格
				if($combinatin_ary[$ProId]){
					$mMax_1=0;
					foreach((array)$combinatin_ary[$ProId] as $k2=>$v2){
						$_arr=array();
						$key=explode('|', substr($k2, 1, -1));
						if(((int)$v['IsCombination']==0 && count($key)==1) || ((int)$v['IsCombination']==1 && count($key)==count($cart_attr_ary[$ProId]))){
							foreach((array)$v2 as $ovid=>$v3){
								$value=$com_data='';
								foreach((array)$key as $v4){
									$_attrid=$vid_data_ary[$v4]['AttrId'];
									$value.=$attribute_ary['Cart'][$_attrid][1].'['.$vid_data_ary[$v4]['Value_'.$language].']';
								}
								if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && count($selected_ary[$ProId]['Overseas'])>1){ //开启海外仓
									$value.='Oversea['.$oversea_name_ary[$ovid].']';
								}
								$com_data=implode(',', $v3);
								$objPHPExcel->getActiveSheet()->setCellValue($ary[14].($m+$mMax_1), $value.'='.$com_data);
								++$mMax_1;
							}
						}
					}
					$mMax_1>$mMax && $mMax=$mMax_1;
				}
				//关联图片
				$row=str::str_code(db::get_all('products_color', "ProId='$ProId'"));
				if($row){
					$mMax_1=0;
					foreach((array)$row as $v2){
						$value='';
						$PicPathAry=array();
						for($i=0; $i<$c['prod_image']['count']; ++$i){
							if(is_file($c['root_path'].$v2["PicPath_$i"])){
								$PicPathAry[]=$v2["PicPath_$i"];
							}
						}
						if(count($PicPathAry)>0){
							$_attrid=$vid_data_ary[$v2['VId']]['AttrId'];
							$value=$attribute_ary['Cart'][$_attrid][1].'['.$vid_data_ary[$v2['VId']]['Value_'.$language].']';
							$objPHPExcel->getActiveSheet()->setCellValue($ary[15].($m+$mMax_1), $value.'='.implode(',', $PicPathAry));
							++$mMax_1;
						}
					}
					$mMax_1>$mMax && $mMax=$mMax_1;
				}
				$m+=$mMax;
			}
			foreach((array)$attr_column as $k=>$v){
				//设置单元格的值(第一二行标题)
				$objPHPExcel->getActiveSheet()->setCellValue($ary[$k].'1', $v);
				$objPHPExcel->getActiveSheet()->getStyle($ary[$k].'1')->getAlignment()->setWrapText(true);//自动换行
				$objPHPExcel->getActiveSheet()->getStyle($ary[$k].'1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
				//设置列的宽度
				$objPHPExcel->getActiveSheet()->getColumnDimension($ary[$k])->setWidth($k>8 && $k<16?30:13);
			}
			
			//设置行的高度
			$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
			$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);//默认行高
			
			$objPHPExcel->getActiveSheet()->setTitle('产品导出');//Rename sheet
			$objPHPExcel->setActiveSheetIndex(0);//指针返回第一个工作表
			$ExcelName='products_'.str::rand_code();
			$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
			$objWriter->save($c['root_path']."{$save_dir}{$ExcelName}.xls");
			$_SESSION['ProZip'][]="{$save_dir}{$ExcelName}.xls";
			unset($objPHPExcel, $objWriter, $products_row);
			ly200::e_json(array(($p_Number+1), "{$c['manage']['lang_pack']['global']['export']} {$save_dir}{$ExcelName}.xls<br />"), 2);
		}else{
			if(count($_SESSION['ProZip'])){
				ly200::e_json('', 1);
			}else{
				ly200::e_json('');
			}
		}
	}
	
	public static function products_explode_down(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($g_Status=='ok' && count($_SESSION['ProZip'])){	//开始打包
			$zip=new ZipArchive();
			$zipname='/tmp/products_'.str::rand_code().'.zip';
			if($zip->open($c['root_path'].$zipname, ZIPARCHIVE::CREATE)===TRUE){
				foreach((array)$_SESSION['ProZip'] as $path){
					if(is_file($c['root_path'].$path)) $zip->addFile($c['root_path'].$path, $path);
				}
				$zip->close();
				file::down_file($zipname);
				file::del_file($zipname);
				foreach((array)$_SESSION['ProZip'] as $path){
					if(is_file($c['root_path'].$path)) file::del_file($path);
				}
			}
		}
		unset($_SESSION['UserZip']);
		exit();
	}
	
	public static function model_category_select(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_ParentId=(int)$p_ParentId;
		$p_AttrId=(int)$p_AttrId;
		$ParentId=db::get_value('products_attribute', "AttrId='$p_AttrId'", 'ParentId');
		echo ly200::form_select(db::get_all('products_attribute','ParentId=0','*','AttrId asc'), 'ParentId', ($ParentId?$ParentId:$p_ParentId), 'Name'.$c['manage']['web_lang'], 'AttrId', $c['manage']['lang_pack']['global']['select_index'], 'notnull');
		exit;
	}
	
	public static function category_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$UnderTheCateId=(int)$p_UnderTheCateId;
		$p_AttrId=(int)$p_AttrId;
		//$IsDesc=addslashes(str::json_data(str::str_code($p_IsDesc, 'stripslashes')));
		$PicPath=$p_PicPath;
		$status=0;
		if($UnderTheCateId==0){
			$UId='0,';
			$Dept=1;
		}else{
			$UId=category::get_UId_by_CateId($UnderTheCateId, 'products_category');
			$Dept=substr_count($UId, ',');
		}
		if($CateId){
			$cate_row=db::get_one('products_category', "CateId='$CateId'");
			$status=1;
		}
		$data=array(
			'UId'		=>	$UId,
			//'AttrId'	=>	$p_AttrId,
			'PicPath'	=>	$PicPath,
			'Dept'		=>	$Dept,
			//'IsIndex'	=>	(int)$p_IsIndex,
			//'IsSoldOut'	=>	(int)$p_IsSoldOut,
			//'IsDesc'	=>	$IsDesc
		);
		if($CateId){
			db::update('products_category', "CateId='$CateId'", $data);
			manage::operation_log('修改产品分类');
		}else{
			db::insert('products_category', $data);
			$CateId=db::get_insert_id();
			db::insert('products_category_description', array('CateId'=>$CateId));
			manage::operation_log('添加产品分类');
		}
		manage::database_language_operation('products_category', "CateId='$CateId'", array('Category'=>1, 'BriefDescription'=>2, 'SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		$desc_data=array('Description'=>3);
		manage::database_language_operation('products_category_description', "CateId='$CateId'", $desc_data);
		if($status && $cate_row['UId']!=$UId){
			$s_where="UId like '{$cate_row['UId']}{$CateId},%' and CateId!='$CateId'";
			if($cate_row['Dept']==1) $s_where="UId like '0,{$CateId},%'";
			$category_row=db::get_all('products_category', $s_where, 'UId, CateId', 'MyOrder desc, CateId asc');
			foreach((array)$category_row as $v){
				$uid=$cate_row['Dept']==1?$UId.substr($v['UId'], 2):str_replace($cate_row['UId'], $UId, $v['UId']);
				db::update('products_category', "CateId='{$v['CateId']}'", array(
						'UId'	=>	$uid,
						'Dept'	=>	substr_count($uid, ',')
					)
				);
			}
		}
		$UId!='0,' && $CateId=category::get_top_CateId_by_UId($UId);
		$statistic_where.=category::get_search_where_by_CateId($CateId, 'products_category');
		if($status && $cate_row['Dept']!=1){
			$statistic_where.=' or CateId='.category::get_FCateId_by_UId($cate_row['UId']);//父
		}else{
			$statistic_where.=" or CateId='$CateId'";//自己本身
		}
		category::category_subcate_statistic('products_category', $statistic_where);
		ly200::e_json('', 1);
	}
	
	public static function category_change_type(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CateId=(int)$p_CateId;
		$p_IsUsed=(int)$p_IsUsed;
		db::update('products_category', "CateId='$p_CateId'", array($p_Type=>$p_IsUsed));
		$Msg=$c['manage']['lang_pack']['msg'][($p_IsUsed==1?'open_success':'close_success')];
		ly200::e_json($Msg, 1);
	}
	
	public static function category_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `products_category` SET `MyOrder` = CASE `CateId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CateId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('批量产品分类排序');
		ly200::e_json('', 1);
	}
	
	public static function category_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CateId=(int)$g_CateId;
		$cate_row=db::get_one('products_category', "CateId='$CateId'");
		$del_where=category::get_search_where_by_CateId($CateId, 'products_category');
		db::delete('products_category_description', $del_where);
		db::delete('products_category', $del_where);
		manage::operation_log('删除产品分类');
		if($cate_row['UId']!='0,'){
			$CateId=category::get_top_CateId_by_UId($cate_row['UId']);
			$statistic_where=category::get_search_where_by_CateId($CateId, 'products_category');
			category::category_subcate_statistic('products_category', $statistic_where);
		}
		ly200::e_json('', 1);
	}
	
	public static function category_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="CateId in(".str_replace('-',',',$g_id).")";
		$row=str::str_code(db::get_all('products_category', $del_where));
		foreach((array)$row as $v){
			if(is_file($c['root_path'].$v['PicPath'])) file::del_file($v['PicPath']);
			$sub_del_where=category::get_search_where_by_CateId($v['CateId'], 'products_category');
			db::delete('products_category_description', $sub_del_where);
			db::delete('products_category', $sub_del_where);
		}
		db::delete('products_category_description', $del_where);
		db::delete('products_category', $del_where);
		manage::operation_log('批量删除产品分类');
		foreach((array)$row as $v){
			if($v['UId']!='0,'){
				$CateId=category::get_top_CateId_by_UId($v['UId']);
				$statistic_where=category::get_search_where_by_CateId($CateId, 'products_category');
				category::category_subcate_statistic('products_category', $statistic_where);
			}
		}
		ly200::e_json('', 1);
	}
	
	public static function business_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$BId=(int)$p_BId;
		$CateId=(int)$p_CateId;
		$Name=$p_Name;
		$Address=$p_Address;
		$Remark=$p_Remark;
		$Entity=$p_Entity;
		$Contacts=$p_Contacts;
		$Phone=$p_Phone;
		$Telephone=$p_Telephone;
		$Fax=$p_Fax;
        $QQ=$p_QQ;
        $memberNo=$p_memberNo;
        $enname=$p_enname;
        $openBank=$p_openBank;
        $openBranch=$p_openBranch;
        $openName=$p_openName;
        $bankNo=$p_bankNo;
        $companyDesc=$p_companyDesc;
        $encompanyDesc=$p_encompanyDesc;
        $facompanyDesc=$p_facompanyDesc;
        $sort=$p_sort;
        $city=$p_city;
        $laabel=$p_laabel;
        $mycateid=$p_mycateid?','.implode(',',$p_mycateid).',':'';
        $product=$p_product;
        $type=$p_type;
		$ImgPath=array();
		$PicPath=array($p_ImgPath, $p_PicPath, $p_logoPath);
		foreach((array)$PicPath as $k=>$v){
			$ImgPath[$k]=$v;
		}
		!$Name && ly200::e_json(manage::get_language('business.business.name_tips'));
		$data=array(
			'CateId'	=>	$CateId,
			'Name'		=>	$Name,
			'Url'		=>	$p_Url,
			'Address'	=>	$Address,
			'Remark'	=>	$Remark,
			'ImgPath'	=>	$ImgPath[0],
			'PicPath'	=>	$ImgPath[1],
			'Entity'	=>	$Entity,
			'Contacts'	=>	$Contacts,
			'Phone'		=>	$Phone,
			'Telephone'	=>	$Telephone,
			'Fax'		=>	$Fax,
			'QQ'		=>	$QQ,
            'MyOrder'	=>	$p_MyOrder,
            'memberNo'	=>	$memberNo,
            'enname'	=>	$enname,
            'openBank'	=>	$openBank,
            'openBranch'=>	$openBranch,
            'openName'  =>	$openName,
            'bankNo'    =>	$bankNo,
            'companyDesc'=>	$companyDesc,
            'encompanyDesc'=> $encompanyDesc,
            'facompanyDesc'=> $facompanyDesc,
            'city'      => $city,
            'sort'      => $sort,
            'laabel'    => $laabel,
            'mycateid'  => $mycateid,
            'product'   => $product,
            'logoPath'	=>	$ImgPath[2],
            'type'	=>	$type,
            'AccTime'	=>	$c['time']
		);
		if($BId){
			db::update('business',"BId='$BId'",$data);
			manage::operation_log('修改供应商');
		}else{
			db::insert('business',$data);
			manage::operation_log('添加供应商');
		}
		ly200::e_json('', 1);
	}
	
	public static function business_my_order(){
		global $c;
		$BIdAry=@array_filter(@explode('-', $_GET['group_id']));
		$MyOrderAry=@array_filter(@explode('-', $_GET['my_order_value']));
		foreach((array)$BIdAry as $k=>$v){
			db::update('business', "BId='$v'", array('MyOrder'=>$MyOrderAry[$k]));
		}
		manage::operation_log('批量供应商排序');
		ly200::e_json('', 1);
	}
	
	public static function business_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$BId=(int)$g_BId;
		$row=str::str_code(db::get_one('business', "BId='{$BId}'", 'ImgPath, PicPath'));
		if($row['ImgPath'] && is_file($c['root_path'].$row['ImgPath'])){
			file::del_file($row['ImgPath']);
		}
		if($row['PicPath'] && is_file($c['root_path'].$row['PicPath'])){
			file::del_file($row['PicPath']);
		}
		db::delete('business', "BId='$BId'");
		manage::operation_log('删除供应商');
		ly200::e_json('', 1);
	}
	
	public static function business_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="BId in(".str_replace('-', ',', $g_id).")";
		$row=str::str_code(db::get_all('business', $del_where, 'ImgPath, PicPath'));
		foreach((array)$row as $v){
			if($v['ImgPath'] && is_file($c['root_path'].$v['ImgPath'])){
				file::del_file($v['ImgPath']);
			}
			if($v['PicPath'] && is_file($c['root_path'].$v['PicPath'])){
				file::del_file($v['PicPath']);
			}
		}
		db::delete('business', $del_where);
		manage::operation_log('批量删除供应商');
		ly200::e_json('', 1);
	}
	
	public static function business_uesd(){
		global $c;
		$row=str::str_code(db::get_one('config', 'GroupId="business" and Variable="IsUsed"'));
		if((int)$row['Value']){
			$status='关闭';
			$is_used=0;
		}else{
			$status='开启';
			$is_used=1;
		}
		manage::config_operaction(array('IsUsed'=>$is_used), 'business');
		manage::operation_log($status.'供应商功能');
		ly200::e_json('', 1);
	}
	
	public static function business_category_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$Category=$p_Category;
		$UnderTheCateId=(int)$p_UnderTheCateId;
		if($UnderTheCateId==0){
			$UId='0,';
			$Dept=1;
		}else{
			$UId=category::get_UId_by_CateId($UnderTheCateId, 'business_category');
			$Dept=substr_count($UId, ',');
		}
		$data = array(
			'Category'	=>	$Category,
			'UId'		=>	$UId,
			'Dept'		=>	$Dept,
			'MyOrder'	=>	$p_MyOrder,
		);
		if($CateId){
			db::update('business_category', "CateId='$CateId'", $data);
			manage::operation_log('修改供应商分类');
		}else{
			db::insert('business_category', $data);
			$CateId=db::get_insert_id();
			manage::operation_log('添加供应商分类');
		}
		$UId!='0,' && $CateId=category::get_top_CateId_by_UId($UId);
		$statistic_where.=category::get_search_where_by_CateId($CateId, 'business_category');
		category::category_subcate_statistic('business_category', $statistic_where);
		ly200::e_json('', 1);
	}
	
	public static function business_category_my_order(){
		global $c;
		$CateIdAry=@array_filter(@explode('-', $_GET['group_id']));
		$MyOrderAry=@array_filter(@explode('-', $_GET['my_order_value']));
		foreach((array)$CateIdAry as $k=>$v){
			db::update('business_category', "CateId='$v'", array('MyOrder'=>$MyOrderAry[$k]));
		}
		manage::operation_log('批量供应商分类排序');
		ly200::e_json('', 1);
	}
	
	public static function business_category_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CateId=(int)$g_CateId;
		$row=db::get_one('business_category', "CateId='$CateId'", 'UId');
		$del_where=category::get_search_where_by_CateId($CateId, 'business_category');
		db::delete('business_category', $del_where);
		manage::operation_log('删除供应商分类');
		if($row['UId']!='0,'){
			$CateId=category::get_top_CateId_by_UId($row['UId']);
			$statistic_where=category::get_search_where_by_CateId($CateId, 'business_category');
			category::category_subcate_statistic('business_category', $statistic_where);
		}
		ly200::e_json('', 1);
	}
	
	public static function business_category_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="CateId in(".str_replace('-', ',', $g_id).")";
		$row=str::str_code(db::get_all('business_category', $del_where));
		db::delete('business_category', $del_where);
		manage::operation_log('批量删除供应商分类');
		foreach((array)$row as $v){
			if($v['UId']!='0,'){
				$CateId=category::get_top_CateId_by_UId($v['UId']);
				$statistic_where=category::get_search_where_by_CateId($CateId, 'business_category');
				category::category_subcate_statistic('business_category', $statistic_where);
			}
		}
		ly200::e_json('', 1);
	}
	
	public static function review_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$RId=(int)$g_RId;
		$resize_ary=array('85x85');
		$review_row=str::str_code(db::get_one('products_review', "RId='$RId'", 'Audit, UserId, ProId, Rating, PicPath_0, PicPath_1, PicPath_2'));
		if($review_row){
			for($i=0; $i<3; $i++){
				$PicPath=$review_row["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					foreach((array)$resize_ary as $v){
						$ext_name=file::get_ext_name($PicPath);
						file::del_file($PicPath.".{$v}.{$ext_name}");
					}
					file::del_file($PicPath);
				}
			}
			$ProId=$review_row['ProId'];
			db::delete('products_review', "RId='{$RId}' or ReId='{$RId}'");
			$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');
			$count=(int)db::get_row_count('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'));
			$rating=(float)db::get_sum('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'), 'Rating');
			db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
			manage::operation_log('删除产品评论');
		}
		ly200::e_json('', 1);
	}
	
	public static function review_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$rid_ary=explode('-', $g_id);
		$resize_ary=array('85x85');
		$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');
		foreach((array)$rid_ary as $v){
			$review_row=str::str_code(db::get_one('products_review', "RId='$v'", 'Audit, UserId, ProId, Rating'));
			if($review_row){
				for($i=0; $i<3; $i++){
					$PicPath=$review_row["PicPath_$i"];
					if(is_file($c['root_path'].$PicPath)){
						foreach((array)$resize_ary as $v2){
							$ext_name=file::get_ext_name($PicPath);
							file::del_file($PicPath.".{$v2}.{$ext_name}");
						}
						file::del_file($PicPath);
					}
				}
				$ProId=$review_row['ProId'];
				db::delete('products_review', "RId='{$v}' or ReId='{$v}'");
				$count=(int)db::get_row_count('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'));
				$rating=(float)db::get_sum('products_review', "ProId='{$ProId}'".($review_cfg['display']==2?'':' and Audit=1 and IsMove=1'), 'Rating');
				db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
			}
		}
		manage::operation_log('批量删除产品评论');
		ly200::e_json('', 1);
	}
	
	public static function review_reply(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_RId=(int)$p_RId;
		$p_ProId=(int)$p_ProId;
		$p_Audit=(int)$p_Audit;
		$review_row=str::str_code(db::get_one('products_review', "RId='$p_RId'", 'Audit, UserId, CustomerName, Rating'));
		!$review_row && ly200::e_json('', 0);
			
		if($p_ReviewComment){//管理员回复
			$data=array(
				'ProId'			=>	$p_ProId,
				'UserId'		=>	0,
				'ReId'			=>	$p_RId,
				'CustomerName'	=>	'Manager',
				'Content'		=>	$p_ReviewComment,
				'Audit'			=>	1,
				'Ip'			=>	ly200::get_ip(),
				'AccTime'		=>	$c['time'],
			);
			db::insert('products_review', $data);
			$Email=str::str_code(db::get_value('user', "UserId='{$review_row['UserId']}'", 'Email'));
			ly200::sendmail($Email, $review_row['CustomerName'], ly200::get_domain().' administrator reply your comments', ly200::get_domain().' administrator reply your comments');
		}
		manage::operation_log("修改产品评论 产品ID:{$p_ProId} 评论ID:{$p_RId}");
		ly200::e_json('', 1);
	}
	
	public static function review_reply_audit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_RId=(int)$p_RId;
		$p_Audit=(int)$p_Audit;
		$p_Review=(int)$p_Review;
		$review_row=str::str_code(db::get_one('products_review', "RId='$p_RId'", 'Audit, ProId, UserId, CustomerName, Rating'));
		!$review_row && ly200::e_json('', 0);
		if($p_Review && $review_row['Audit']!=$p_Audit){
			$w="ProId='{$review_row['ProId']}' and Audit=1 and ReId=0";
			$count=(int)db::get_row_count('products_review', $w);
			$rating=(float)db::get_sum('products_review', $w, 'Rating');
			db::update('products', "ProId='{$review_row['ProId']}'", array('Rating'=>($count?ceil($rating/$count):0), 'TotalRating'=>$count));
		}
		
		db::update('products_review', "RId='$p_RId'", array('Audit'=>$p_Audit));

		manage::operation_log("产品评论审核 评论ID:{$p_RId}");
		ly200::e_json('', 1);
	}
	
	public static function upload(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;//当前分开数
		$p_Current=(int)$p_Current;//行数开始位置
		$p_Worksheet=(int)$p_Worksheet;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		$errerTxt='';
		!file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json('文件不存在！');
		$objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
		$sheet=$objPHPExcel->getSheet(0);//工作表0
		$highestRow=$sheet->getHighestRow();//取得总行数 
		$highestColumn=$sheet->getHighestColumn();//取得总列数
		
		if($highestColumn!='AM'){//提示用新的
			ly200::e_json($c['manage']['lang_pack']['products']['upload']['tips_error'], 0);
		}
		
		//Add some data (A ~ EZ)
		$arr=range('A', 'Z');
		$ary=$arr;
		for($i=0; $i<10; ++$i){
			$num=$arr[$i];
			foreach((array)$arr as $v){
				$ary[]=$num.$v;
			}
		}
		//初始化第一阶段
		$Start=1;//开始执行位置
		$page_count=20;//每次分开导入的数量
		if($p_Number>0 && $p_Current==0){//已完成
			file::del_file($p_ExcelFile);
			ly200::e_json($c['manage']['lang_pack']['products']['upload']['tips_success_0'], 1);//产品资料导入完成，正在处理上传图片...
		}else{//继续执行
			$Start=$p_Current;
		}
		//初始化第二阶段
		$language=$p_Language;//语言版本
		if(!$language || !in_array($language, $c['manage']['web_lang_list'])){//找不到相对应的语言，默认为可用语言里面的第一个
			$language=$c['manage']['web_lang_list'][0];
		}
		$insert_ary=$update_ary=$category_ary=$sub_category_ary=$attribute_ary=$attribute_top_ary=$attribute_cart_ary=$attribute_name_ary=$attribute_color_ary=$vid_data_ary=$vid_to_attrid_ary=array();
		//产品分类
		$category_row=db::get_all('products_category', '1', 'CateId, UId, AttrId', 'UId asc');
		foreach((array)$category_row as $v){
			$category_ary[$v['CateId']]=$v['UId'];
			if($v['UId']!='0,'){
				$TopCateId=category::get_top_CateId_by_UId($v['UId']);
				$sub_category_ary[$v['CateId']]=$TopCateId;
			}
		}
		//产品属性
		$attribute_row=str::str_code(db::get_all('products_attribute', '1', "AttrId, Type, Name_{$language}, CateId, CartAttr, ColorAttr, DescriptionAttr", 'AttrId desc'));
		foreach((array)$attribute_row as $v){
			$sCateId=($v['CateId']?explode('|', trim($v['CateId'], '|')):array(0));
			$attribute_ary[$v['AttrId']]=array(0=>$v['Type'], 1=>$v["Name_{$language}"]);
			foreach((array)$sCateId as $v2){
				$attribute_top_ary[$v['CartAttr']][$v2][$v["Name_{$language}"]]=$v['AttrId'];//收录父亲属性
				if($v['CartAttr']){//规格属性
					$attribute_cart_ary[$v2][]=$v['AttrId'];
				}
				if($sub_category_ary[$v2]){//顶级分类
					$attribute_name_ary[$v['CartAttr']][$v["Name_{$language}"]][$sub_category_ary[$v2]]=$v['AttrId'];
				}
			}
		}
		unset($attribute_row);
		$value_row=str::str_code(db::get_all('products_attribute_value', '1', '*', $c['my_order'].'VId asc')); //属性选项
		foreach((array)$value_row as $v){
			$vid_data_ary[$v['AttrId']][htmlspecialchars_decode($v["Value_{$language}"])]=$v['VId'];
            $vid_to_attrid_ary[$v['VId']]=$v['AttrId'];
		}
		unset($value_row);
		//海外仓
		$shipping_overseas_row=db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc');
		$oversea_len=count($shipping_overseas_row);
		$oversea_name_ary=array();
		foreach((array)$shipping_overseas_row as $v){
			$oversea_name_ary[$v["Name_{$language}"]]=$v['OvId'];
		}
		//自动排序
		$cfg_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
		foreach((array)$cfg_row as $v){
			$cfg_ary[$v['GroupId']][$v['Variable']]=$v['Value'];
		}
		$used_row=str::json_data(htmlspecialchars_decode($cfg_ary['products_show']['Config']), 'decode');
		//字段数组
		$column_products_ary=db::get_table_fields('products', 1);
		$column_products_description_ary=db::get_table_fields('products_description', 1);
		$column_products_seo_ary=db::get_table_fields('products_seo', 1);
		//内容转换为数组 
		$pro_count=0;
		$max_rows=0;
		$data=array();
		for($i=$Start; $i<=$highestRow; ++$i){ 
			foreach((array)$ary as $k=>$v){
				$address=$v.$i;//单元格坐标 
				$data[$i][$v]=$sheet->getCell($address)->getFormattedValue(); 
				if($v==$highestColumn) break;
			}
			if($i>1 && $data[$i]['A'] && $data[$i]['D']){//一个产品的资料
				$pro_count+=1;
			}
			if($pro_count>$page_count){//超出每次导入的数量
				$max_rows=$i;
				unset($data[$i]);
				break;
			}
		}
		$num=count($c['manage']['web_lang_list']);
		$un_data_ary=$data_ary=array();
		$i=-1;
		foreach((array)$data as $k=>$v){//行
			if($k<2) continue;
			if($v['A'] && $v['D']){//一个产品的资料
				++$i;
				$un_data_ary[$i]=$v;
			}else{//产品的附属参数
				$v['J'] && $un_data_ary[$i]['J'].=','.trim($v['J']);//图片
				$v['K'] && $un_data_ary[$i]['K'].=';'.$v['K'];//普通属性（文本）
				$v['L'] && $un_data_ary[$i]['L'].=';'.$v['L'];//普通属性（选项）
				$v['M'] && $un_data_ary[$i]['M'].=';'.trim($v['M']);//普通属性（筛选项）
				$v['N'] && $un_data_ary[$i]['N'].=';'.$v['N'];//规格属性
				$v['O'] && $un_data_ary[$i]['O'].=';'.$v['O'];//属性价格
				$v['P'] && $un_data_ary[$i]['P'].=';'.$v['P'];//关联图片
			}
		}
		foreach((array)$un_data_ary as $k=>$v){//产品
			$v['D']=addslashes(trim($v['D']));//优先转义
			if(db::get_row_count('products', "concat_ws('', Prefix, Number)='{$v['D']}'")){//更新数据库
				$data_ary['update'][]=$v;
			}else{//插入数据库
				$data_ary['insert'][]=$v;
			}
		}
		$tab_title_ary=array(0=>$data[0]['AU'], 1=>$data[0]['AV'], 2=>$data[0]['AW']);//选项卡标题
		unset($data, $un_data_ary);
		//过滤敏感词
		@include_once($c['root_path'].'/manage/static/inc/filter.library.php');
		@include_once($c['root_path'].'/inc/un_filter_keywords.php');
		$filter_keywords_ary=$FilterKeyArr['Keyword'];
		$un_filter_keywords_ary=(array)@str::str_code($un_filter_keywords, 'strtolower');
		//返回数据
		$return_data=array();
		$return_number=array();
		//开始导入
		$number = 0;
		foreach ((array)$data_ary as $a => $b) {
			$No = 0;
			$insert_sql = array();
            $update_sql = array();
            $database_sql = array();
			foreach ((array)$b as $key => $val) {
				$number += 1;
				$Name = trim($val['A']); //名称
				$return_data[$number] = array('Name'=>$Name, 'ProId'=>0, 'Number'=>'', 'Status'=>1, 'Tips'=>$c['manage']['lang_pack']['products']['upload']['tips_awaiting']);
				$CateId = (int)trim($val['B']); //分类
				if (!$CateId || ($CateId && !$category_ary[$CateId])) {
					$return_data[$number]['Status'] = 0;
					$return_data[$number]['Tips'] = $c['manage']['lang_pack']['products']['upload']['error_category'];
					continue;
				}
				$ExtCateId = trim($val['C']); //多分类
				if ($ExtCateId) {
					$ExtCateId_Data = explode(',', $ExtCateId);
					foreach ((array)$ExtCateId_Data as $k => $v) {
						if (!$v || ($v && !$category_ary[$v])) unset($ExtCateId_Data[$k]);
					}
					if (count($ExtCateId_Data) > 0) {
						$ExtCateId = ','.implode(',', $ExtCateId_Data).',';
					} else {
						$ExtCateId = '';
					}
				}
				$Number = trim($val['D']); //编号
				$return_number[$number] = strtolower($Number);
				$return_data[$number]['Number'] = $Number;
				if (!$Number) {
					$return_data[$number]['Status'] = 0;
					$return_data[$number]['Tips'] = $c['manage']['lang_pack']['products']['upload']['error_number'];
					continue;
				}
				$SKU = trim($val['E']); //SKU
				$Price_0 = (float)trim($val['F']); //市场价
				$Price_1 = (float)trim($val['G']); //会员价
				$Wholesale = trim($val['H']); //批发价
				$PageUrl = trim($val['I']); //自定义地址
				$PicPath = @explode(',', trim($val['J'])); //图片
				$AttrTxt = trim($val['K']); //普通属性（文本）
				$AttrOption = trim($val['L']); //普通属性（选项）
				$AttrScreen = trim($val['M']); //普通属性（筛选项）
				$AttrCart = trim($val['N']); //规格属性
				$ExtAttr = trim($val['O']); //属性价格
				$ColorAttr = trim($val['P']); //关联图片
				$IsOpenAttrPrice = (int)trim($val['Q']); //属性价格开关
				$IsCombination = (int)trim($val['R']); //属性组合开关
				$Weight = (float)trim($val['S']); //重量
				$Cubage = str_replace('*', ',', trim($val['T'])); //体积
				$IsVolumeWeight = (int)trim($val['U']); //体积重
				$MOQ = (int)trim($val['V']); //起订量
				$MaxOQ = (int)trim($val['W']); //最大购买量
				$Stock = (int)trim($val['X']); //总库存
				$StockOut = (int)trim($val['Y']); //脱销状态
				$SoldOut = (int)trim($val['Z']); //下架
				$IsSoldOut = (int)trim($val['AA']); //定时上架
				$SoldOut && $IsSoldOut = 0;
				$SoldOutTime = @explode('/', trim($val['AB'])); //定时上架时间
				$SStartTime = (int)@strtotime($SoldOutTime[0]);
				$SEndTime = (int)@strtotime($SoldOutTime[1]);
				$IsFreeShipping = (int)trim($val['AC']); //免运费
				$IsNew = (int)trim($val['AD']); //新品
				$IsHot = (int)trim($val['AE']); //热卖
				$IsBestDeals = (int)trim($val['AF']); //畅销
				$IsIndex = (int)trim($val['AG']); //首页显示
				$SeoTitle = trim($val['AH']); //标题
				$SeoKeyword = trim($val['AI']); //关键字
				$SeoDescription = trim($val['AJ']); //描述
				$BriefDescription = trim($val['AK']); //简单介绍
				$Description = trim($val['AL']); //详细介绍
				//$Tab0 = trim($val['AL']); //选项卡1
				//$Tab1 = trim($val['AM']); //选项卡2
				//$Tab2 = trim($val['AN']); //选项卡3
				$MyOrder = trim($val['AM']); //排序
                $MyOrder == '最后' && $MyOrder = 999;
                $MyOrder = (int)$MyOrder;
				//更新代码
				if ($a == 'update') {
					$prod_row = db::get_one('products', "concat_ws('', Prefix, Number)='{$Number}'", 'ProId, IsPromotion, StartTime, EndTime, PromotionDiscount, PromotionPrice, MyOrder, '.$c['prod_image']['sql_field']);
					$return_data[$number]['ProId'] = $prod_row['ProId'];
                    $MyOrder = (int)$prod_row['MyOrder']; //排序
				}
				//批发价
                $LowestPrice = $Price_1; //最低价格
				if ($Wholesale) {
					$Qty = @explode(',', $Wholesale);
					$wholesale_ary = array();
					foreach ((array)$Qty as $k => $v) {
						if ($k > 4) break;
						$arr = @explode(':', $v);
						$wholesale_ary[$arr[0]] = $arr[1];
                        if ($k == 0 && $arr[1] < $LowestPrice) $LowestPrice = $arr[1]; //最低价格
					}
					$Wholesale = addslashes(str::json_data(str::str_code($wholesale_ary, 'stripslashes')));
				}
                //促销（更新产品）
                $is_promition = ($prod_row['IsPromotion'] && $prod_row['StartTime'] < $c['time'] && $c['time'] < $prod_row['EndTime']) ? 1 : 0;
                if ((int)$is_promition) {
                    $LowestPrice = $prod_row['PromotionType'] == 1 ? (float)substr(sprintf('%01.3f', $Price_1 * $prod_row['PromotionDiscount'] / 100), 0, -1) : $prod_row['PromotionPrice'];
                }
				//过滤敏感词
				$arr_filter = array($Name, $SeoTitle, $SeoKeyword, $SeoDescription, $BriefDescription, $Description);
				if ((int)count($arr_filter)) {
					$filterArr = $arr_filter;
				} else {
					$filterArr = $_POST;
					unset($filterArr['do_action'], $filterArr['PicPath'], $filterArr['FilePath'], $filterArr['UId'], $filterArr['ColorPath'], $filterArr['Number']);
				}
				$str = ' ' . @implode(' -- ', $filterArr) . ' ';
				$result = 0;
				$keyword = '';
				foreach ((array)$filter_keywords_ary as $v2) {
					if (@count($un_filter_keywords_ary) && @in_array(strtolower(trim($v2)), $un_filter_keywords_ary)) continue;
					if (@substr_count(strtolower(stripslashes($str)), strtolower($v2))) {
						$keyword = $v2;
						$result = 1;
						break;
					}
				}
				unset($arr_filter, $filterArr);
				if ($result == 1) {
					$return_data[$number]['Status'] = 0;
					$return_data[$number]['Tips'] = $c['manage']['lang_pack']['products']['upload']['error_keyword'] . ":{$keyword}";
					continue;
				}
				//产品属性
				if($CateId){
					$AddNum=$AddVIdNum=0;
					$create_ary=$upgrade_ary=$add_ary=$attr_ary=$ext_ary=$color_ary=$color_id_ary=$screen_ary=array();
					//顶级分类
					$TopCateId=0;
					$sUId=db::get_value('products_category', "CateId='{$CateId}'", 'UId');
					if($sUId!='0,'){//不是顶级分类
						$TopCateId=(int)category::get_top_CateId_by_UId($sUId);
					}
					//普通属性（文本）
					if($AttrTxt){
						$AttrTxt=explode(';', $AttrTxt);
						foreach((array)$AttrTxt as $v){
							$ary=@explode('[', substr($v, 0, -1));
							$value=trim($ary[1]);
							$id=$attribute_top_ary[0][$CateId][$ary[0]];
							$ids=$attribute_name_ary[0][$ary[0]][$TopCateId];
							if(!$id){//数据不存在
								if(!$ids){//数据不存在 (同在一级分类下的其他子分类，相同名称的属性ID)
									$id="ADD:$AddNum";
									$create_ary['Txt'][$id]=$ary[0];
									++$AddNum;
								}else{
									$id=$ids;
									$upgrade_ary[$id]=$CateId;
								}
							}
							$attr_ary[$id]=$value;
						}
					}
					//普通属性（选项）
					if($AttrOption){
						$AttrOption=explode(';', $AttrOption);
						foreach((array)$AttrOption as $v){
							$ary=@explode('[', substr($v, 0, -1));
							$value=trim($ary[1]);
							$id=$attribute_top_ary[0][$CateId][$ary[0]];
							$ids=$attribute_name_ary[0][$ary[0]][$TopCateId];
							if(!$id){//数据不存在
								if(!$ids){//数据不存在 (同在一级分类下的其他子分类，相同名称的属性ID)
									$id="ADD:$AddNum";
									$create_ary['Option'][$id]=$ary[0];
									++$AddNum;
								}else{
									$id=$ids;
									$upgrade_ary[$id]=$CateId;
								}
							}
							$arr=array();
							$value=@explode(',', $value);
							foreach((array)$value as $v2){
								$v2=trim($v2);
								if($v2){
									if($vid_data_ary[$id][$v2]){
										$arr[]=$vid_data_ary[$id][$v2];
									}else{
										$vid="ADDVId:$AddVIdNum";
										$arr[]=$vid;
										$create_ary['Value'][$id][$vid]=$v2;
										++$AddVIdNum;
									}
								}
							}
							$attr_ary[$id]=$arr;
						}
					}
					//普通属性（筛选项）
					if($AttrScreen){
						$AttrScreen=explode(';', $AttrScreen);
						foreach((array)$AttrScreen as $v){
							$screen_ary[$v]=1;
						}
					}
					//规格属性
					if($AttrCart){
						$AttrCart=explode(';', $AttrCart);
						foreach((array)$AttrCart as $v){
							$ary=@explode('[', substr($v, 0, -1));
							$value=trim($ary[1]);
							if($ary[0]=='Oversea'){//海外仓
								$id='Oversea';
								$arr=array();
								$value=@explode(',', $value);
								foreach((array)$value as $v2){
									if($v2) $arr[]=$oversea_name_ary[$v2];
								}
							}else{//属性
								$id=$attribute_top_ary[1][$CateId][$ary[0]];
								$ids=$attribute_name_ary[1][$ary[0]][$TopCateId];
								if(!$id){//数据不存在
									if(!$ids){//数据不存在 (同在一级分类下的其他子分类，相同名称的属性ID)
										$id="ADD:$AddNum";
										$create_ary['Cart'][$id]=$ary[0];
										$add_ary[$ary[0]][0]=$id;
										++$AddNum;
									}else{
										$id=$ids;
										$upgrade_ary[$id]=$CateId;
									}
								}
								$arr=array();
								$value=@explode(',', $value);
								foreach((array)$value as $v2){
									$v2=trim($v2);
									if($v2){
										if($vid_data_ary[$id][$v2]){
											$arr[]=$vid_data_ary[$id][$v2];
										}else{
											$vid="ADDVId:$AddVIdNum";
											$arr[]=$vid;
											$create_ary['Value'][$id][$vid]=$v2;
											$add_ary[$ary[0]][$v2]=$vid;
											++$AddVIdNum;
										}
									}
								}
							}
							$attr_ary[$id]=$arr;
						}
					}
					//属性价格
					if($ExtAttr){
						$ExtAttr=explode(';', $ExtAttr);
						foreach((array)$ExtAttr as $v){
							$ary=@explode('=', $v);
							$name_ary=@explode(']', $ary[0]);
							$value=array();
							$OvId=1;
							foreach((array)$name_ary as $v2){
								if(!$v2) continue;
								$arr=@explode('[', $v2);
								if($arr[0]=='Oversea'){//海外仓
									$OvId=$oversea_name_ary[$arr[1]];
								}else{//属性
									if($vid_data_ary[$attribute_top_ary[1][$CateId][$arr[0]]][$arr[1]]){//数据存在
										$value[]=$vid_data_ary[$attribute_top_ary[1][$CateId][$arr[0]]][$arr[1]];
									}else{//数据不存在
										$value[]=$add_ary[$arr[0]][$arr[1]];
									}
								}
							}
							sort($value);
							$ext_ary['|'.implode('|', $value).'|'][$OvId]=explode(',', $ary[1]);
						}
					}
					/************* 检查规格属性，是否有缺少填写勾选规格属性的属性关联项 Start *************/
					$check_ary=$key_ary=$oversea_ary=array();
					if(is_array($attribute_cart_ary[$CateId])){
						if($attr_ary['Oversea'] && count($attr_ary['Oversea'])>0){
							$oversea_ary=$attr_ary['Oversea'];
						}else{
							$oversea_ary=array(1);
						}
						foreach((array)$attr_ary as $k=>$v){
							if(in_array($k, $attribute_cart_ary[$CateId])){
								$key_ary[]=$k;
								$check_ary[$k]=$v;
							}
						}
						$check_ext_ary=self::check_cart_attr($check_ary, $key_ary, 0);
						foreach((array)$oversea_ary as $OvId){
							foreach((array)$check_ext_ary as $v){
								$value=@explode('|', $v);
								sort($value);
								$value='|'.implode('|', $value).'|';
								if(!$ext_ary[$value][$OvId]){ $ext_ary[$value][$OvId]=array(0, 0, 0, '', 0); }
							}
						}
					}
					/************* 检查规格属性，是否有缺少填写勾选规格属性的属性关联项 End *************/
					if(!$attr_ary['Oversea']){//检查海外仓是否遗漏勾选China
						$attr_ary['Oversea'][0]=1;
					}
					//关联图片
					if($ColorAttr){
						$ColorAttr=explode(';', $ColorAttr);
						foreach((array)$ColorAttr as $k=>$v){
							$ary=@explode('=', $v);
							$_ary=@explode('[', substr($ary[0], 0, -1));
							if($k==0){//第一次
								if($attribute_top_ary[1][$CateId][$_ary[0]]){
									$ColorId=$attribute_top_ary[1][$CateId][$_ary[0]];
								}
							}
							$_vid=$vid_data_ary[$ColorId][$_ary[1]];
							if($add_ary[$_ary[0]]){
								$ColorId=$add_ary[$_ary[0]][0];
								$_vid=$add_ary[$_ary[0]][$_ary[1]];
							}
							if($_vid){
								$color_ary[$_vid]=explode(',', $ary[1]);
							}
						}
					}
				}
				//创建产品属性和选项
				if($create_ary){
					//新建产品属性
					$insert_attr_ary=$new_attr_id_ary=array();
					foreach((array)$create_ary as $k=>$v){
						if($k=='Txt'){//普通属性（文本）
							foreach((array)$v as $k2=>$v2){
								$insert_attr_ary[$k2]=array("Name_$language"=>$v2, 'CateId'=>"|{$CateId}|", 'CartAttr'=>0, 'Type'=>0, 'ScreenAttr'=>(int)$screen_ary[$v2]);
							}
						}elseif($k=='Option'){//普通属性（选项）
							foreach((array)$v as $k2=>$v2){
								$insert_attr_ary[$k2]=array("Name_$language"=>$v2, 'CateId'=>"|{$CateId}|", 'CartAttr'=>0, 'Type'=>1, 'ScreenAttr'=>(int)$screen_ary[$v2]);
							}
						}elseif($k=='Cart'){//规格属性
							foreach((array)$v as $k2=>$v2){
								$insert_attr_ary[$k2]=array("Name_$language"=>$v2, 'CateId'=>"|{$CateId}|", 'CartAttr'=>1, 'Type'=>1);
							}
						}
					}
					foreach((array)$insert_attr_ary as $k=>$v){
						db::insert('products_attribute', $v);
						$AttrId=db::get_insert_id();
						$new_attr_id_ary[$k]=$AttrId; //记录新属性
						$attribute_ary[$AttrId]=array(0=>$v['Type'], 1=>$v["Name_{$language}"]); //补充数据记录
						$attribute_top_ary[$v['CartAttr']][$CateId][$v["Name_{$language}"]]=$AttrId; //补充“收录父亲属性”的数据记录
						$v['CartAttr']==1 && $attribute_cart_ary[$CateId][]=$AttrId; //补充“规格属性”的数据记录
					}
					//新建产品属性选项
					$insert_value_ary=$new_vid_ary=array();
					if($create_ary['Value']){
						foreach((array)$create_ary['Value'] as $k=>$v){
							$k1=(stripos($k, 'ADD:')!==false?$new_attr_id_ary[$k]:$k);
							foreach((array)$v as $k2=>$v2){
								$insert_value_ary[$k2]=array('AttrId'=>$k1, "Value_$language"=>$v2);
							}
						}
					}
					foreach((array)$insert_value_ary as $k=>$v){
						db::insert('products_attribute_value', $v);
						$VId=db::get_insert_id();
						$new_vid_ary[$k]=$VId; //记录新属性选项
						$vid_data_ary[$v['AttrId']][$v["Value_{$language}"]]=$VId; //补充选项ID数据记录
					}
					//替换属性数据 ("ADD:"替换成"AttrId" "ADDVId:"替换成"VId")
					foreach((array)$attr_ary as $k=>$v){
						$k1=0;
						if(stripos($k, 'ADD:')!==false){//新添加
							$k1=$new_attr_id_ary[$k];
						}
						if(is_array($v)){//选项
							foreach((array)$v as $k2=>$v2){
								if(stripos($v2, 'ADDVId:')!==false){//新添加
									$attr_ary[$k][$k2]=$new_vid_ary[$v2];
								}
							}
						}
						if($k1){//转移数据
							$attr_ary[$k1]=$attr_ary[$k];
							unset($attr_ary[$k]);
						}
					}
					//替换属性价格数据 ("ADDVId:"替换成"VId")
					foreach((array)$ext_ary as $k=>$v){
						$k1=explode('|', trim($k, '|'));
						foreach((array)$k1 as $k2=>$v2){
							$new_vid_ary[$v2] && $k1[$k2]=$new_vid_ary[$v2];
						}
						$k1='|'.implode('|', $k1).'|';
						$ext_ary[$k1]=$ext_ary[$k];
						if($k!=$k1) unset($ext_ary[$k]); //键不相同的时候，才删掉
					}
					//替换关联图片数据 ("ADD:"替换成"AttrId" "ADDVId:"替换成"VId")
					foreach((array)$color_ary as $k=>$v){
						$k1=0;
						if(stripos($k, 'ADDVId:')!==false){//新添加
							$k1=$new_vid_ary[$k];
						}
						if($k1){//转移数据
							$color_ary[$k1]=$color_ary[$k];
							unset($color_ary[$k]);
						}
					}
				}
				//更新产品属性 (绑定新的分类数值)
				if($upgrade_ary){
					foreach((array)$upgrade_ary as $k=>$v){
						if(!$k || !$v) continue;
						$ParentId=db::get_value('products_attribute', "AttrId='$k'", 'CateId'); //当前属性归属的分类ID
						db::update('products_attribute', "AttrId='$k'", array('CateId'=>$ParentId.$v.'|'));
					}
				}
				//记录数据资料
				$data=array(
					"Name_{$language}"	=>	addslashes($Name),
					'CateId'			=>	$CateId,
					'ExtCateId'			=>	$ExtCateId,
					'Prefix'			=>	addslashes($Prefix),
					'Number'			=>	$Number,
					'SKU'				=>	addslashes($SKU),
					'Price_0'			=>	$Price_0,
					'Price_1'			=>	$Price_1,
					'Wholesale'			=>	$Wholesale,
					'Weight'			=>	$Weight,
					'Cubage'			=>	$Cubage,
					'IsVolumeWeight'	=>	$IsVolumeWeight,
					'MOQ'				=>	(1<$MOQ && $MOQ<=$Stock)?$MOQ:1,
					'MaxOQ'				=>	($MaxOQ<0)?0:$MaxOQ,
					'Stock'				=>	$Stock,
					'StockOut'			=>	$StockOut,
					'IsIncrease'		=>	0,
					'IsOpenAttrPrice'	=>	$IsOpenAttrPrice,
					'IsCombination'		=>	$IsCombination,
					'SoldOut'			=>	$SoldOut,
					'IsSoldOut'			=>	$IsSoldOut,
					'SStartTime'		=>	$SStartTime,
					'SEndTime'			=>	$SEndTime,
					'IsFreeShipping'	=>	$IsFreeShipping,
					'IsNew'				=>	$IsNew,
					'IsHot'				=>	$IsHot,
					'IsBestDeals'		=>	$IsBestDeals,
					'IsIndex'			=>	$IsIndex,
					'PageUrl'			=>	ly200::str_to_url($PageUrl),
					"BriefDescription_{$language}"=>addslashes($BriefDescription),
					'AccTime'			=>	$c['time'],
					'MyOrder'			=>	$MyOrder
				);
				$database_sql[strtolower($data['Prefix'].$data['Number'])]=array(
					'Pic'		=>	$PicPath,//产品图片
					'Seo'		=>	array(//标题与标签
										"SeoTitle_{$language}"		=>	addslashes($SeoTitle),
										"SeoKeyword_{$language}"	=>	addslashes($SeoKeyword),
										"SeoDescription_{$language}"=>	addslashes($SeoDescription)
									),
					'Desc'		=>	array("Description_{$language}"	=>	addslashes($Description)),//详细介绍
					'Tab_0'		=>	array(//选项卡1
										"TabName_0_{$language}"		=>	addslashes($tab_title_ary[0]),
										"Tab_0_{$language}"			=>	addslashes($Tab0)
									),
					'Tab_1'		=>	array(//选项卡2
										"TabName_1_{$language}"		=>	addslashes($tab_title_ary[1]),
										"Tab_1_{$language}"			=>	addslashes($Tab1)
									),
					'Tab_2'		=>	array(//选项卡3
										"TabName_2_{$language}"		=>	addslashes($tab_title_ary[2]),
										"Tab_2_{$language}"			=>	addslashes($Tab2)
									),
					'Attr'		=>	$attr_ary,//属性
					'ExtAttr'	=>	$ext_ary,//组合属性
					'Color'		=>	$color_ary//颜色属性
				);
				if($a=='update'){//更新数据库
					$ProId=$prod_row['ProId'];
					unset($data['Prefix'], $data['Number'], $data['MyOrder']); //不更新产品编号,MyOrder
					foreach((array)$data as $k=>$v){
						$update_sql[$k][$ProId]=$v;
					}
				}else{//插入数据库
					$insert_sql['Product'].=($No?',':'')."('".$data["Name_{$language}"]."', {$data['CateId']}, '{$data['ExtCateId']}', '{$data['Prefix']}', '{$data['Number']}', '{$data['SKU']}', {$data['Price_0']}, {$data['Price_1']}, '{$data['Wholesale']}', '{$data['PicPath_0']}', '{$data['PicPath_1']}', '{$data['PicPath_2']}', '{$data['PicPath_3']}', '{$data['PicPath_4']}', '{$data['PicPath_5']}', '{$data['PicPath_6']}', '{$data['PicPath_7']}', '{$data['PicPath_8']}', '{$data['PicPath_9']}', '{$data['PicPath_10']}', '{$data['PicPath_11']}', '{$data['PicPath_12']}', '{$data['PicPath_13']}', '{$data['PicPath_14']}', {$data['Weight']}, '{$data['Cubage']}', {$data['MOQ']}, {$data['MaxOQ']}, {$data['Stock']}, {$data['StockOut']}, {$data['IsIncrease']}, {$data['IsOpenAttrPrice']}, {$data['IsCombination']}, {$data['SoldOut']}, {$data['IsSoldOut']}, '{$data['SStartTime']}', '{$data['SEndTime']}', {$data['IsFreeShipping']}, {$data['IsNew']}, {$data['IsHot']}, {$data['IsBestDeals']}, {$data['IsIndex']}, '{$data['PageUrl']}', '".$data["BriefDescription_{$language}"]."', {$data['MyOrder']}, {$data['AccTime']})";
				}
				++$No;
			}
			
			if($a=='update'){//更新数据库
				if(is_array($update_sql) && count($update_sql)){
					$ides=implode(',', array_keys($update_sql['CateId'])); 
					$len=count($update_sql);
					$i=0;
					$sql="update products set";
						foreach((array)$update_sql as $k=>$v){
							$sql.=" {$k} = case ProId";
							foreach((array)$v as $k2=>$v2){
								$sql.=sprintf(" when %s then '%s' ", $k2, $v2); 
							}
							$sql.='end'.(++$i<$len?',':'');
						}
					$sql.=" where ProId in($ides)";
					$sql && db::query($sql);
				}
			}else{//插入数据库
				$insert_sql['Product'] && db::query('insert into products (Name_'.$language.', CateId, ExtCateId, Prefix, Number, SKU, Price_0, Price_1, Wholesale, PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9, PicPath_10, PicPath_11, PicPath_12, PicPath_13, PicPath_14, Weight, Cubage, MOQ, MaxOQ, Stock, StockOut, IsIncrease, IsOpenAttrPrice, IsCombination, SoldOut, IsSoldOut, SStartTime, SEndTime, IsFreeShipping, IsNew, IsHot, IsBestDeals, IsIndex, PageUrl, BriefDescription_'.$language.', MyOrder, AccTime) values'.$insert_sql['Product']);
			}
			
			//其他数据表的内容更新
			if($database_sql){
				$proid_where=$sid_where=$did_where=$seid_where=$comid_where=$cid_where='';
				$insert_sql=$update_sql=$proid_ary=array();
				reset($database_sql);
				$num_where=@implode("','", array_keys($database_sql));
				$row=str::str_code(db::get_all('products', "concat_ws('', Prefix, Number) in('{$num_where}')", 'ProId, Prefix, Number'));
				$insert_sql['Pic']='';
				$j=0;
				foreach((array)$row as $k=>$v){
					$sNumber=htmlspecialchars_decode(strtolower($v['Prefix'].$v['Number']));
					$sNumber=addslashes($sNumber);//优先转义
					$proid_ary[$sNumber]=$v['ProId'];
					if(in_array($sNumber, $return_number)){//记录到返回数据里面的ProId
						$return_data[array_search($sNumber, $return_number)]['ProId']=$v['ProId'];
					}
					//图片处理记录
					$insert_sql['Pic'].=($j?',':'')."('{$v['ProId']}'";
					$pic_data=$database_sql[$sNumber]['Pic'];
					$color_data=addslashes(str::json_data(str::str_code($database_sql[$sNumber]['Color'], 'stripslashes')));
					for($i=0; $i<$c['prod_image']['count']; ++$i){
						if(stripos($pic_data[$i], 'u_file')!==false){
							$insert_sql['Pic'].=",''";
						}else{
							$insert_sql['Pic'].=",'".($pic_data[$i]?addslashes($pic_data[$i]):'')."'";
						}
					}
					$insert_sql['Pic'].=",'{$color_data}'";
					$insert_sql['Pic'].=')';
					++$j;
				}
				$insert_sql['Pic'] && db::query("insert into products_upload_tmp (ProId, ".$c['prod_image']['sql_field'].", ColorData) values".$insert_sql['Pic']);//图片处理记录
				$proid_where=@implode(',', $proid_ary);
				if($proid_where){
					if($a=='update'){//更新数据库
						$sid_ary=$did_ary=$atid_ary=$seid_ary=$comid_ary=$_comid_ary=$cid_ary=$pic_color_ary=$un_pic_color_ary=$update_ary=$insert_ary=array();
						$insert_sql['Desc']=$insert_sql['Attr']=$insert_sql['ExtAttr']='';
						//列出SEO资料
						$seo_row=str::str_code(db::get_all('products_seo', "ProId in($proid_where)", 'SId, ProId'));
						foreach((array)$seo_row as $k=>$v){
							$sid_ary[$v['ProId']]=$v['SId'];
						}
						$sid_where=@implode(',', $sid_ary);
						!$sid_where && $sid_where='0';
						//列出详细介绍资料
						$desc_row=str::str_code(db::get_all('products_description', "ProId in($proid_where)", 'DId, ProId'));
						foreach((array)$desc_row as $k=>$v){
							$did_ary[$v['ProId']]=$v['DId'];
						}
						$did_where=@implode(',', $did_ary);
						!$did_where && $did_where='0';
						//列出产品属性勾选资料
						$attr_row=str::str_code(db::get_all('products_selected_attr', "ProId in($proid_where)"));
						foreach((array)$attr_row as $k=>$v){
							$atid_ary[$v['ProId'].'_'.$v['AttrId']]=$v['SId'];
						}
						unset($attr_row);
						$atid_where=@implode(',', $atid_ary);
						!$atid_where && $atid_where='0';
						//列出产品属性选项勾选资料
						$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId in($proid_where)"));
						foreach((array)$selected_row as $k=>$v){
							$seid_ary[$v['ProId'].'_'.$v['AttrId'].'_'.$v['VId'].'_'.$v['OvId']]=$v['SeleteId'];
						}
						unset($selected_row);
						$seid_where=@implode(',', $seid_ary);
						!$seid_where && $seid_where='0';
						//列出产品组合属性关联项资料
						$combination_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId in($proid_where)"));
						foreach((array)$combination_row as $k=>$v){
							$comid_ary[$v['ProId']][$v['Combination']][$v['OvId']]=$v['CId'];
							$_comid_ary[]=$v['CId'];
						}
						$comid_where=@implode(',', $_comid_ary);
						!$comid_where && $comid_where='0';
						//准备
						$i=$j=$ExtAttr_update=0;
						$len=count($database_sql);
						//更新数值重新排列
						foreach((array)$database_sql as $k=>$v){
							foreach((array)$v as $k2=>$v2){
								foreach((array)$v2 as $k3=>$v3){
									if($k2=='Color'){
										foreach((array)$v3 as $k4=>$v4){
											$update_ary[$k2][$k4][$k3][$k]=$v4;
										}
									}else{
										$update_ary[$k2][$k3][$k]=$v3;
									}
								}
							}
						}
						foreach((array)$update_ary as $k=>$v){
							if($k=='Seo'){//SEO
								$j=0;
								foreach((array)$v as $k2=>$v2){
									$update_sql['Seo'].=($j?',':'')." {$k2} = case SId";
									foreach((array)$v2 as $k3=>$v3){
										$id=(int)$proid_ary[$k3];//产品ID
										$sid_ary[$id] && $update_sql['Seo'].=sprintf(" when %s then '%s' ", $sid_ary[$id], $v3);
									}
									$update_sql['Seo'].='end';
									++$j;
								}
							}
							if($k=='Desc'){//详细介绍
								$j=0;
								foreach((array)$v as $k2=>$v2){
									$update_sql['Desc'].=($j?',':'')." {$k2} = case DId";
									foreach((array)$v2 as $k3=>$v3){
										$id=(int)$proid_ary[$k3];//产品ID
										//$update_sql['Desc'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3);
										if($did_ary[$id]){//已存在
											$update_sql['Desc'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3);
										}else{//不存在
											$insert_ary['Desc'][$id]="'{$v3}'";
										}
									}
									$update_sql['Desc'].='end';
									++$j;
								}
							}
							/*
							if($k=='Tab_0'){//选项卡1
								$j=0;
								foreach((array)$v as $k2=>$v2){
									$update_sql['Tab_0'].=($j?',':'')." {$k2} = case DId";
									foreach((array)$v2 as $k3=>$v3){
										$id=(int)$proid_ary[$k3];//产品ID
										//$update_sql['Tab_0'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3); 
										if($did_ary[$id]){//已存在
											$update_sql['Tab_0'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3);
										}else{//不存在
											$insert_ary['Desc'][$id].=",'{$v3}'";
										}
									}
									$update_sql['Tab_0'].='end';
									++$j;
								}
							}
							if($k=='Tab_1'){//选项卡2
								$j=0;
								foreach((array)$v as $k2=>$v2){
									$update_sql['Tab_1'].=($j?',':'')." {$k2} = case DId";
									foreach((array)$v2 as $k3=>$v3){
										$id=(int)$proid_ary[$k3];//产品ID
										//$update_sql['Tab_1'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3); 
										if($did_ary[$id]){//已存在
											$update_sql['Tab_1'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3);
										}else{//不存在
											$insert_ary['Desc'][$id].=",'{$v3}'";
										}
									}
									$update_sql['Tab_1'].='end';
									++$j;
								}
							}
							if($k=='Tab_2'){//选项卡3
								$j=0;
								foreach((array)$v as $k2=>$v2){
									$update_sql['Tab_2'].=($j?',':'')." {$k2} = case DId";
									foreach((array)$v2 as $k3=>$v3){
										$id=(int)$proid_ary[$k3];//产品ID
										//$update_sql['Tab_2'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3); 
										if($did_ary[$id]){//已存在
											$update_sql['Tab_2'].=sprintf(" when %s then '%s' ", $did_ary[$id], $v3);
										}else{//不存在
											$insert_ary['Desc'][$id].=",'{$v3}'";
										}
									}
									$update_sql['Tab_2'].='end';
									++$j;
								}
							}
							*/
							if($k=='Attr'){//产品属性
								$j=$m=$n=0;
								$IsUsed_sql=" IsUsed = case SeleteId";
								$Value_sql=", Value_{$language} = case SeleteId";
                                $At_IsUsed_sql=" IsUsed = case SId";
                                $At_IsColor_sql=", IsColor = case SId";
                                $_color_ary=array();
                                if($database_sql[$k3]['Color']){
                                    foreach($database_sql[$k3]['Color'] as $k2=>$v2){
                                        $_color_ary[$vid_to_attrid_ary[$k2]]=1;
                                    }
                                }
								foreach((array)$v as $k2=>$v2){
                                    $_IsColor=($_color_ary && $_color_ary[$k2]?1:0);
									if($k2=='Oversea' || $attribute_ary[$k2][0]==1){//海外仓 or 列表选择
										foreach((array)$v2 as $k3=>$v3){
											$id=(int)$proid_ary[$k3];//产品ID
											foreach((array)$v3 as $v4){
												$OvId=1;
												if($k2=='Oversea'){//海外仓
													$OvId=$v4;
													$k2=$v4=0;
												}
												if($seid_ary[$id.'_'.$k2.'_'.$v4.'_'.$OvId]){//已存在
													$IsUsed_sql.=sprintf(" when %s then '%s' ", $seid_ary[$id.'_'.$k2.'_'.$v4.'_'.$OvId], 1); 
													++$n;
												}else{//不存在
													$insert_sql['Attr'].=", ({$id}, {$k2}, '{$v4}', '{$OvId}', '', 1)"; 
													++$m;
												}
											}
                                            if($k2!='Oversea'){ //海外仓除外
                                                if($atid_ary[$id.'_'.$k2]){//已存在
                                                    $At_IsUsed_sql.=sprintf(" when %s then '%s' ", $atid_ary[$id.'_'.$k2], 1);
                                                    $At_IsColor_sql.=sprintf(" when %s then '%s' ", $atid_ary[$id.'_'.$k2], $_IsColor); 
                                                }else{//不存在
                                                    $insert_sql['At'].=", ({$id}, {$k2}, 1, {$_IsColor})"; 
                                                }
                                            }
										}
									}else{//文本框
										foreach((array)$v2 as $k3=>$v3){
											$id=(int)$proid_ary[$k3];//产品ID
											if($seid_ary[$id.'_'.$k2.'_0_1']){//已存在
												$IsUsed_sql.=sprintf(" when %s then '%s' ", $seid_ary[$id.'_'.$k2.'_0_1'], 1); 
												$Value_sql.=sprintf(" when %s then '%s' ", $seid_ary[$id.'_'.$k2.'_0_1'], $v3); 
												++$n;
											}else{//不存在
												if((int)$k2){
													$insert_sql['Attr'].=", ({$id}, {$k2}, 0, 1, '".addslashes($v3)."', 1)"; 
													++$m;
												}
											}
                                            if($atid_ary[$id.'_'.$k2]){//已存在
                                                $At_IsUsed_sql.=sprintf(" when %s then '%s' ", $atid_ary[$id.'_'.$k2], 1);
                                                $At_IsColor_sql.=sprintf(" when %s then '%s' ", $atid_ary[$id.'_'.$k2], $_IsColor); 
                                            }else{//不存在
                                                $insert_sql['At'].=", ({$id}, {$k2}, 1, {$_IsColor})"; 
                                            }
										}
									}
									++$j;
								}
								$IsUsed_sql.='end';
								$Value_sql.=' end';
                                $At_IsUsed_sql.=' end';
                                $At_IsColor_sql.=' end';
								$update_sql['Attr']=($n?$IsUsed_sql:'').($Value_sql!=", Value_{$language} = case SeleteId end"?$Value_sql:'');
                                $update_sql['At']=($n && $At_IsUsed_sql!=' IsUsed = case SId end'?$At_IsUsed_sql:'').($At_IsColor_sql!=', IsColor = case SId end'?$At_IsColor_sql:'');
							}
							if($k=='ExtAttr'){//产品组合属性关联项
								$j=$m=0;
								$OvId_sql=" OvId = case CId";
								$SKU_sql=", SKU = case CId";
								$IsIncrease_sql=", IsIncrease = case CId";
								$Price_sql=", Price = case CId";
								$Stock_sql=", Stock = case CId";
								$Weight_sql=", Weight = case CId";
								foreach((array)$v as $k2=>$v2){//$k2=Combination
									foreach((array)$v2 as $k3=>$v3){//
										$id=(int)$proid_ary[$k3];//产品ID
										foreach((array)$v3 as $k4=>$v4){//$k4=OvId
											$k4<1 && $k4=1;
											if($comid_ary[$id][$k2][$k4]){//已存在
												$ExtAttr_update++;
												$OvId_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $k4);
												$SKU_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $v4[3]);
												$IsIncrease_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $v4[4]);
												$Price_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $v4[0]);
												$Stock_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $v4[1]);
												$Weight_sql.=sprintf(" when %s then '%s' ", $comid_ary[$id][$k2][$k4], $v4[2]);
											}else{//不存在
												$insert_sql['ExtAttr'].=($m?',':'')."({$id}, '{$k2}', '{$k4}', '{$v4[3]}', '{$v4[4]}', '{$v4[0]}', '{$v4[1]}', '{$v4[2]}')"; 
												++$m;
											}
										}
									}
									++$j;
								}
								$OvId_sql.='end';
								$SKU_sql.='end';
								$IsIncrease_sql.='end';
								$Price_sql.='end';
								$Stock_sql.='end';
								$Weight_sql.='end';
								$update_sql['ExtAttr']=$OvId_sql.$SKU_sql.$IsIncrease_sql.$Price_sql.$Stock_sql.$Weight_sql;
							}
							++$i;
						}
						$i=0;
						foreach((array)$insert_ary['Desc'] as $k=>$v){//目前只单单针对详细介绍
							$insert_sql['Desc'].=($i?',':'')."('{$k}',{$v})";
							++$i;
						}
						$seid_where && db::update('products_selected_attribute', "SeleteId in($seid_where)", array('IsUsed'=>0)); //先默认全部关闭
						$insert_sql['At'] && $insert_sql['At']=ltrim($insert_sql['At'], ',');//去掉多余的逗号
                        $insert_sql['Attr'] && $insert_sql['Attr']=ltrim($insert_sql['Attr'], ',');//去掉多余的逗号
						($update_sql['Seo'] && $sid_where) && db::query("update products_seo set".$update_sql['Seo']." where SId in({$sid_where})");
						($update_sql['Desc'] && $did_where) && db::query("update products_description set".$update_sql['Desc']." where DId in($did_where)");
						//($update_sql['Tab_0'] && $did_where) && db::query("update products_description set".$update_sql['Tab_0']." where DId in($did_where)");
						//($update_sql['Tab_1'] && $did_where) && db::query("update products_description set".$update_sql['Tab_1']." where DId in($did_where)");
						//($update_sql['Tab_2'] && $did_where) && db::query("update products_description set".$update_sql['Tab_2']." where DId in($did_where)");
                        ($update_sql['At'] && $atid_where) && db::query("update products_selected_attr set".$update_sql['At']." where SId in($atid_where)");
						($update_sql['Attr'] && $seid_where) && db::query("update products_selected_attribute set".$update_sql['Attr']." where SeleteId in($seid_where)");
						($update_sql['ExtAttr'] && $comid_where && $ExtAttr_update) && db::query("update products_selected_attribute_combination set".$update_sql['ExtAttr']." where CId in($comid_where)");
						//$insert_sql['Desc'] && db::query("insert into products_description (ProId, Description_{$language}, TabName_0_{$language}, Tab_0_{$language}, TabName_1_{$language}, Tab_1_{$language}, TabName_2_{$language}, Tab_2_{$language}) values".$insert_sql['Desc']);
						$insert_sql['Desc'] && db::query("insert into products_description (ProId, Description_{$language}) values".$insert_sql['Desc']);
                        $insert_sql['At'] && db::query("insert into products_selected_attr (ProId, AttrId, IsUsed, IsColor) values".$insert_sql['At']);
						$insert_sql['Attr'] && db::query("insert into products_selected_attribute (ProId, AttrId, VId, OvId, Value_{$language}, IsUsed) values".$insert_sql['Attr']);
						$insert_sql['ExtAttr'] && db::query("insert into products_selected_attribute_combination (ProId, Combination, OvId, SKU, IsIncrease, Price, Stock, Weight) values".$insert_sql['ExtAttr']);
					}else{//插入数据库
						$insert_sql['Seo']=$insert_sql['Desc']=$insert_sql['Attr']=$insert_sql['ExtAttr']='';
						$i=0;
						$len=count($database_sql);
						foreach((array)$database_sql as $k=>$v){
							$id=(int)$proid_ary[$k];//产品ID
                            $_color_ary=array();
                            if($database_sql[$k]['Color']){
                                foreach($database_sql[$k]['Color'] as $k2=>$v2){
                                    $_color_ary[$vid_to_attrid_ary[$k2]]=1;
                                }
                            }
							foreach((array)$v as $k2=>$v2){
								if($k2=='Seo'){//SEO
									$insert_sql['Seo'].=($i?',':'')."({$id}";
									foreach((array)$v2 as $k3=>$v3){
										$insert_sql['Seo'].=",'{$v3}'"; 
									}
									$insert_sql['Seo'].=')';
								}
								/*
								if($k2=='Desc' || $k2=='Tab_0' || $k2=='Tab_1' || $k2=='Tab_2'){//详细介绍，选项卡1，选项卡2，选项卡3
									if($k2=='Desc') $insert_sql['Desc'].=($i?',':'')."({$id}";
									foreach((array)$v2 as $k3=>$v3){
										$insert_sql['Desc'].=",'{$v3}'"; 
									}
									if($k2=='Tab_2') $insert_sql['Desc'].=')';
								}
								*/
								if($k2=='Desc'){//详细介绍
									$insert_sql['Desc'].=($i?',':'')."({$id}";
									foreach((array)$v2 as $k3=>$v3){
										$insert_sql['Desc'].=",'{$v3}'"; 
									}
									$insert_sql['Desc'].=')';
								}
								if($k2=='Attr'){//产品属性
									$j=0;
                                    $_IsColor=($_color_ary && $_color_ary[$k3]?1:0);
									foreach((array)$v2 as $k3=>$v3){
										if($k3=='Oversea' || $attribute_ary[$k3][0]==1){//海外仓 or 列表选择
											foreach((array)$v3 as $v4){
												$OvId=1;
												if($k3=='Oversea'){//海外仓
													$OvId=$v4;
													$k3=$v4=0;
												}
												$insert_sql['Attr'].=", ({$id}, {$k3}, '{$v4}', '{$OvId}', '', 1)"; 
												++$j;
											}
										}else{//文本框
											if((int)$k3){
												$insert_sql['Attr'].=", ({$id}, {$k3}, 0, 1, '".addslashes($v3)."', 1)"; 
												++$j;
											}
										}
                                        $k3>0 && $insert_sql['At'].=", ({$id}, {$k3}, 1, {$_IsColor})"; 
										++$j;
									}
								}
								if($k2=='ExtAttr'){//产品组合属性关联项
									$j=0;
									foreach((array)$v2 as $k3=>$v3){//$k3=Combination
										foreach((array)$v3 as $k4=>$v4){//$k4=OvId
											$k4<1 && $k4=1;
											$insert_sql['ExtAttr'].=(($i || $j)?',':'')."({$id}, '{$k3}', '{$k4}', '{$v4[3]}', '{$v4[4]}', '{$v4[0]}', '{$v4[1]}', '{$v4[2]}')"; 
											++$j;
										}
									}
								}
							}
							++$i;
						}
                        $insert_sql['At'] && $insert_sql['At']=ltrim($insert_sql['At'], ',');//去掉多余的逗号
						$insert_sql['Attr'] && $insert_sql['Attr']=ltrim($insert_sql['Attr'], ',');//去掉多余的逗号
						$insert_sql['Seo'] && db::query("insert into products_seo (ProId, SeoTitle_{$language}, SeoKeyword_{$language}, SeoDescription_{$language}) values".$insert_sql['Seo']);
						//$insert_sql['Desc'] && db::query("insert into products_description (ProId, Description_{$language}, TabName_0_{$language}, Tab_0_{$language}, TabName_1_{$language}, Tab_1_{$language}, TabName_2_{$language}, Tab_2_{$language}) values".$insert_sql['Desc']);
						$insert_sql['Desc'] && db::query("insert into products_description (ProId, Description_{$language}) values".$insert_sql['Desc']);
                        $insert_sql['At'] && db::query("insert into products_selected_attr (ProId, AttrId, IsUsed, IsColor) values".$insert_sql['At']);
						$insert_sql['Attr'] && db::query("insert into products_selected_attribute (ProId, AttrId, VId, OvId, Value_{$language}, IsUsed) values".$insert_sql['Attr']);
						$insert_sql['ExtAttr'] && db::query("insert into products_selected_attribute_combination (ProId, Combination, OvId, SKU, IsIncrease, Price, Stock, Weight) values".$insert_sql['ExtAttr']);
					}
				}
			}
		}
		$Msg=array(
			'Num'	=>	($p_Number+1),
			'Cur'	=>	$max_rows,
			'Data'	=>	$return_data,
			//'Tips'	=>	'<p>已上传'.$item.'个</p>',
		);
		ly200::e_json($Msg, 2);
	}
	
	public static function upload_picture(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$ProId		= (int)$p_ProId;
		$PicPath	= array();
		$prod_row	= db::get_one('products', "ProId='$ProId'", $c['prod_image']['sql_field']);
		$tmp_row	= db::get_one('products_upload_tmp', "ProId='$ProId'", '*', 'TId desc');
		$resize_ary	= $c['manage']['resize_ary']['products'];
		$save_dir	= $c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');//图片储存位置
		$PicCount	= 0;
		$SaveCount	= 0;
		$Ret		= 1;
		!$prod_row && ly200::e_json('', 0);
		file::mk_dir($save_dir);
		for($i=0; $i<$c['prod_image']['count']; ++$i){
			$PicPath[$i]=$tmp_row["PicPath_{$i}"];
			if(trim($PicPath[$i])) $PicCount+=1;
		}
		//图片上传
		$ImgPath=self::upload_picture_create($PicPath, $prod_row, $save_dir, $resize_ary);
		//检查上传成功数量
		foreach((array)$ImgPath as $v){
			if(trim($v)) $SaveCount+=1;
		}
		if($PicCount!=$SaveCount){//数量上，对不上
			$Ret=0;
		}
		//记录数据资料
		$data=array(
			'PicPath_0'	=>	$ImgPath[0]?$ImgPath[0]:$prod_row['PicPath_0'],
			'PicPath_1'	=>	$ImgPath[1]?$ImgPath[1]:$prod_row['PicPath_1'],
			'PicPath_2'	=>	$ImgPath[2]?$ImgPath[2]:$prod_row['PicPath_2'],
			'PicPath_3'	=>	$ImgPath[3]?$ImgPath[3]:$prod_row['PicPath_3'],
			'PicPath_4'	=>	$ImgPath[4]?$ImgPath[4]:$prod_row['PicPath_4'],
			'PicPath_5'	=>	$ImgPath[5]?$ImgPath[5]:$prod_row['PicPath_5'],
			'PicPath_6'	=>	$ImgPath[6]?$ImgPath[6]:$prod_row['PicPath_6'],
			'PicPath_7'	=>	$ImgPath[7]?$ImgPath[7]:$prod_row['PicPath_7'],
			'PicPath_8'	=>	$ImgPath[8]?$ImgPath[8]:$prod_row['PicPath_8'],
			'PicPath_9'	=>	$ImgPath[9]?$ImgPath[9]:$prod_row['PicPath_9'],
            'PicPath_10'=>	$ImgPath[10]?$ImgPath[10]:$prod_row['PicPath_10'],
            'PicPath_11'=>	$ImgPath[11]?$ImgPath[11]:$prod_row['PicPath_11'],
            'PicPath_12'=>	$ImgPath[12]?$ImgPath[12]:$prod_row['PicPath_12'],
            'PicPath_13'=>	$ImgPath[13]?$ImgPath[13]:$prod_row['PicPath_13'],
            'PicPath_14'=>	$ImgPath[14]?$ImgPath[14]:$prod_row['PicPath_14'],
		);
		db::update('products', "ProId='$ProId'", $data);
		//颜色图片上传
		if($tmp_row['ColorData']){
			$insert_sql=$update_sql='';
			$cid_ary=$pic_color_ary=$update_ary=array();
			$color_row=str::str_code(db::get_all('products_color', "ProId='$ProId'"));
			foreach((array)$color_row as $k=>$v){
				$cid_ary[$v['VId']]=$v['CId'];
				$pic_color_ary[$v['CId']]=$v;
			}
			$cid_where=@implode(',', $cid_ary);
			!$cid_where && $cid_where='0';
			$ColorData=str::json_data(htmlspecialchars_decode($tmp_row['ColorData']), 'decode');
			$m=0;
			foreach((array)$ColorData as $k=>$v){
				if($v && count($v)>0){
					$pic_ary=self::upload_picture_create($v, $pic_color_ary[$cid_ary[$k]], $save_dir, $resize_ary);
					if($cid_ary[$k]){//数据已存在
						foreach((array)$pic_ary as $k2=>$v2){
							$v2=($v2?$v2:$pic_color_ary[$cid_ary[$k]]["PicPath_{$k2}"]);
							$update_ary[$k2][$cid_ary[$k]]=$v2;
						}
					}else{//数据不存在
						$insert_sql.=($m?',':'')."('{$ProId}', '{$k}', '{$pic_ary[0]}', '{$pic_ary[1]}', '{$pic_ary[2]}', '{$pic_ary[3]}', '{$pic_ary[4]}', '{$pic_ary[5]}', '{$pic_ary[6]}', '{$pic_ary[7]}', '{$pic_ary[8]}', '{$pic_ary[9]}', '{$pic_ary[10]}', '{$pic_ary[11]}', '{$pic_ary[12]}', '{$pic_ary[13]}', '{$pic_ary[14]}')";
						++$m;
					}
				}
			}
			foreach((array)$update_ary as $k=>$v){
				$update_sql.=($k?',':'')." PicPath_{$k} = case CId";
				foreach((array)$v as $k2=>$v2){
					$update_sql.=sprintf(" when %s then '%s' ", $k2, $v2);
				}
				$update_sql.='end';
			}
			($update_sql && $cid_where) && db::query("update products_color set{$update_sql} where CId in({$cid_where})");//更新
			$insert_sql && db::query("insert into products_color (ProId, VId, PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9, PicPath_10, PicPath_11, PicPath_12, PicPath_13, PicPath_14) values".$insert_sql);//新增
		}
		//db::delete('products_upload_tmp', "ProId='$ProId'");
		ly200::e_json('', $Ret);
	}
	
	public static function upload_picture_create($pic_ary, $now_pic_ary, $save_dir, $resize_ary){
		global $c;
		$ImgPath=array();
		foreach((array)$pic_ary as $k=>$v){
			if(stripos($v, 'u_file')!==false || !trim($v)) continue; //本地图片，跳出执行
			$new_path='';
			$water_ary=array();
			if(stripos($v, 'https://')!==false || stripos($v, 'http://')!==false){
				//远程获取图片
				$filepath=trim(ly200::curl($v));
				if($filepath){//检查图片是否存在
					$ext_name=file::get_ext_name($v);//图片文件后缀名
					$new_path=file::write_file($save_dir, str::rand_code().'.'.$ext_name, $filepath);
				}
			}else{
				//本地图片
				$filepath='/tmp/madeimg/'.$v;
				if(is_file($c['root_path'].$filepath)){//检查图片是否存在
					$ext_name=file::get_ext_name($filepath);//图片文件后缀名
					$new_path=$save_dir.str::rand_code().'.'.$ext_name;//图片重新命名
					@copy($c['root_path'].$filepath, $c['root_path'].$new_path);//先把目标图片复制到u_file
				}
			}
			//生成缩略图 and 注入水印
			if($new_path && is_file($c['root_path'].$new_path)){
				if($c['manage']['config']['IsWater']) $water_ary[]=$new_path;
				if($resize_ary){
					if(in_array('default', $resize_ary)){//保存不加水印的原图
						@copy($c['root_path'].$new_path, $new_path.".default.{$ext_name}");
					}
					if($c['manage']['config']['IsWater'] && $c['manage']['config']['IsThumbnail']){//缩略图加水印
						img::img_add_watermark($new_path);
						$water_ary=array();
					}
					foreach((array)$resize_ary as $value){
						if($value=='default') continue;
						$size_w_h=explode('x', $value);
						$resize_img=$new_path;
						$resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
					}
				}
				foreach((array)$water_ary as $value){
					img::img_add_watermark($value);
				}
				$ImgPath[$k]=$new_path;
				foreach((array)$resize_ary as $value){
					if(!is_file($c['root_path'].$ImgPath[$k].".{$value}.{$ext_name}")){
						$size_w_h=explode('x', $value);
						$resize_img=$ImgPath[$k];
						$resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
					}
				}
				if(!is_file($c['root_path'].$ImgPath[$k].".default.{$ext_name}")){
					@copy($c['root_path'].$ImgPath[$k], $c['root_path'].$ImgPath[$k].".default.{$ext_name}");
				}
				//删除原图片
				foreach((array)$resize_ary as $value){
					$ext_name=file::get_ext_name($now_pic_ary['PicPath_'.$k]);
					file::del_file($now_pic_ary['PicPath_'.$k].".{$value}.{$ext_name}");
				}
				file::del_file($now_pic_ary['PicPath_'.$k]);
			}else{
				$ImgPath[$k]=$now_pic_ary['PicPath_'.$k];
			}
		}
		return $ImgPath;
	}
	
	//自动组合规格属性的关联项
	public static function check_cart_attr($check_ary, $key_ary, $num, $ary=array()){
		$_arr=array();
		$count=count($check_ary);
		if($num==0){
			foreach((array)$check_ary[$key_ary[$num]] as $v){
				$ary[]=$v;
			}
		}else{
			foreach((array)$ary as $v){
				foreach((array)$check_ary[$key_ary[$num]] as $v2){
					$_arr[]=$v.'|'.$v2;
				}
			}
			$ary=$_arr;
		}
		++$num;
		if($num<$count){
			return self::check_cart_attr($check_ary, $key_ary, $num, $ary);
		}else{
			return $ary;
		}
	}
	
	public static function upload_new_excel_download(){
		global $c;
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		$objPHPExcel=new PHPExcel();
		
		//Set properties 
		$objPHPExcel->getProperties()->setCreator("Sheldon");//创建者
		$objPHPExcel->getProperties()->setLastModifiedBy("Sheldon");//最后修改者
		$objPHPExcel->getProperties()->setTitle("Products Upload");//标题
		$objPHPExcel->getProperties()->setSubject("Products Upload");//主题
		$objPHPExcel->getProperties()->setKeywords("Products Upload");//标记
		$objPHPExcel->getProperties()->setDescription('Products Upload');//备注
		$objPHPExcel->getProperties()->setCategory("Products Upload");//类别
		
		//Add some data
		//(A ~ EZ)
		$arr=range('A', 'Z');
		$ary=$arr;
		for($i=0; $i<10; ++$i){
			$num=$arr[$i];
			foreach((array)$arr as $v){
				$ary[]=$num.$v;
			}
		}
		$m_lang=$c['manage']['config']['ManageLanguage']=='en'?0:1;
		
		$fixed_ary=array();
		$fixed_ary[]=array($m_lang?'产品名称':'Name', 'test');
		$fixed_ary[]=array($m_lang?'产品分类':'Category', '304');
		$fixed_ary[]=array($m_lang?'产品多分类':'Category List', '334,202,247');
		$fixed_ary[]=array($m_lang?'产品编号':'Serial Number', 'EZ019384');
		$fixed_ary[]=array('SKU', 'EZ019384');
		$fixed_ary[]=array($m_lang?'市场价':'Market Price', '21.50');
		$fixed_ary[]=array($m_lang?'商城价':'Shop Price', '14.99');
		$fixed_ary[]=array($m_lang?'批发价':'Whole Sale Price', '5:29.5,10:28,50:25');
		$fixed_ary[]=array($m_lang?'自定义地址':'Custom Link', 'about-us');
		$fixed_ary[]=array($m_lang?'图片':'Picture', '001.jpg', '002.jpg', '003.jpg', '004.jpg', '005.jpg', '006.jpg', '007.jpg', '008.jpg', '009.jpg', '010.jpg');
		$fixed_ary[]=array($m_lang?'普通属性（文本）':'General Attribute (text)', 'Txt 1[Content 111]', 'Txt 2[Content 222]');
		$fixed_ary[]=array($m_lang?'普通属性（选项）':'General Attribute (option)', 'Search[111,222,333]', 'Content[aaa,bbb,ccc]');
		$fixed_ary[]=array($m_lang?'普通属性（筛选项）':'General Attribute (screen)', 'Search', 'Content');
		$fixed_ary[]=array($m_lang?'规格属性':'Shopping Cart Attribute', 'Color[black,white,blue]', 'Size[S,M,XL]');
		$fixed_ary[]=array($m_lang?'属性价格':'Attribute & Price', 'Color[black]Size[S]=5.99,999,0.523,EZ019350,1', 'Color[black]Size[M]=5.99,999,0.523,EZ019351,1', 'Color[black]Size[XL]=5.99,999,0.523,EZ019352,1', 'Color[white]Size[S]=5.99,999,0.523,EZ019353,1', 'Color[white]Size[M]=5.99,999,0.523,EZ019354,1', 'Color[white]Size[XL]=5.99,999,0.523,EZ019355,1', 'Color[blue]Size[S]=5.99,999,0.523,EZ019356,1', 'Color[blue]Size[M]=5.99,999,0.523,EZ019357,1', 'Color[blue]Size[XL]=5.99,999,0.523,EZ019358,1');
		$fixed_ary[]=array($m_lang?'关联图片':'Associated Picture', 'Color[black]=101.jpg,102.jpg,103.jpg', 'Color[white]=101.jpg,102.jpg,103.jpg', 'Color[blue]=101.jpg,102.jpg,103.jpg');
		$fixed_ary[]=array($m_lang?'属性价格开关':'Attribute & Price switch', '1');
		$fixed_ary[]=array($m_lang?'属性组合开关':'Combination switch', '1');
		$fixed_ary[]=array($m_lang?'重量':'Weight', '90');
		$fixed_ary[]=array($m_lang?'体积':'Volume', '10*20*5');
		$fixed_ary[]=array($m_lang?'体积重':'Volume weight', '0');
		$fixed_ary[]=array($m_lang?'起订量':'Minimum Order Quantity', '1');
		$fixed_ary[]=array($m_lang?'最大购买量':'Maximum Order Quantity', '999');
		$fixed_ary[]=array($m_lang?'总库存':'Inventory', '999');
		$fixed_ary[]=array($m_lang?'脱销状态':'Out-of-stock status', '0');
		$fixed_ary[]=array($m_lang?'下架':'Off Sales', '0');
		$fixed_ary[]=array($m_lang?'定时上架':' Timing Sales', '0');
		$fixed_ary[]=array($m_lang?'定时上架时间':'On SalesTime', '2012/11/10 11:51:04 - 2019/11/10 11:51:04');
		$fixed_ary[]=array($m_lang?'免运费':'Freight Free', '1');
		$fixed_ary[]=array($m_lang?'新品':'New Product', '1');
		$fixed_ary[]=array($m_lang?'热卖':'Hot Product', '1');
		$fixed_ary[]=array($m_lang?'畅销':'Best Selling', '1');
		$fixed_ary[]=array($m_lang?'首页显示':'Home Page', '1');
		//$lang_ary=array($m_lang?'标题':'Title', $m_lang?'关键字':'Keywords', $m_lang?'描述':'Sketch', $m_lang?'简短介绍':'Brief Introduction', $m_lang?'详细介绍':'Detailed Introduction', $m_lang?'选项卡1':'Tab1', $m_lang?'选项卡2':'Tab2', $m_lang?'选项卡3':'Tab3');
		$lang_ary=array($m_lang?'标题':'Title', $m_lang?'关键字':'Keywords', $m_lang?'描述':'Sketch', $m_lang?'简短介绍':'Brief Introduction', $m_lang?'详细介绍':'Detailed Introduction');
		for($i=0; $i<count($lang_ary); ++$i){
			$fixed_ary[]=array($lang_ary[$i], 'test');
		}
        $fixed_ary[]=array($m_lang?'排序':'Ranking', '1');
		$fixed_number=count($fixed_ary);
		
		//按语言版本
		$objPHPExcel->setActiveSheetIndex(0);//设置当前的sheet
		$objPHPExcel->getActiveSheet()->setTitle('批量上传');//设置sheet的name
		
		$colmun_ary=$fixed_ary;
		$attr_column=array();//初始化
		
		foreach((array)$colmun_ary as $k=>$v){//固定项
			$attr_column[$k]=$v[0];
		}
		ksort($attr_column);
		
		foreach((array)$attr_column as $k=>$v){
			//设置单元格的值(第一二行标题)
			$objPHPExcel->getActiveSheet()->setCellValue($ary[$k].'1', $v);
			$objPHPExcel->getActiveSheet()->getStyle($ary[$k].'1')->getAlignment()->setWrapText(true);//自动换行
			$objPHPExcel->getActiveSheet()->getStyle($ary[$k].'1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);//垂直居中
			
			//设置单元格的值
			for($i=0; $i<10; ++$i){
				$value=$colmun_ary[$k][$i+1];
				$objPHPExcel->getActiveSheet()->setCellValue($ary[$k].($i+2), $value);
			}
			
			//设置列的宽度
			$objPHPExcel->getActiveSheet()->getColumnDimension($ary[$k])->setWidth($k>9 && $k<16?30:13);
		}
		
		//设置行的高度
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);//默认行高
		
		$objPHPExcel->setActiveSheetIndex(0);//指针返回第一个工作表
		
		//保存Excel文件
		$ExcelName='upload_'.str::rand_code();
		$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save($c['root_path']."/tmp/{$ExcelName}.xls");
		
		file::down_file("/tmp/{$ExcelName}.xls");
		file::del_file("/tmp/{$ExcelName}.xls");
		unset($c, $objPHPExcel, $ary, $attr_column, $fixed_ary);
		exit;
	}
	
	/***********************************************************数据同步(start)*******************************************************************/
	/*****************************************
	速卖通同步部分(start)
	*****************************************/
	public static function change_aliexpress_authorization_account(){//切换当前速卖通账号
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AccountId=(int)$p_AccountId;
		!$AccountId && ly200::e_json('请选择切换的账号！');
		$AccountId==$_SESSION['Manage']['Aliexpress']['Token']['AuthorizationId'] && ly200::e_json('');
		
		aliexpress::set_default_authorization($AccountId);
		ly200::e_json('', 1);
	}

	public static function aliexpress_products_sync(){//同步产品
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$ConditionAry=array(
			//onSelling、offline、auditing、editingRequired
			'productStatusType'	=>	@in_array($p_productStatusType, $c['manage']['sync_ary']['aliexpress'])?$p_productStatusType:'onSelling'
		);
		(int)$p_GroupId && $ConditionAry['groupId']=(int)$p_GroupId;
		$Account=aliexpress::set_default_authorization();
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'sync_products',
			'ApiName'		=>	'aliexpress',
			'Number'		=>	$c['Number'],
			'Account'		=>	$Account,
			'ConditionInfo'	=>	str::json_data($ConditionAry),
			'notify_url'	=>	ly200::get_domain().'/gateway/',
			'timestamp'		=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		if($result['ret']==1){
			$TaskId=$result['msg']['TaskId'];
			$taskData=array(
				'Platform'	=>	'aliexpress',
				'TaskId'	=>	$TaskId,
				'AccTime'	=>	time()
			);
			!db::get_row_count('products_sync_task', "Platform='aliexpress' and TaskId='{$TaskId}'") && db::insert('products_sync_task', $taskData);
		}
		
		ly200::e_json($TaskId?array('TaskId'=>$TaskId):$result['msg'], (int)$result['ret']);
	}
	
	public static function aliexpress_products_sync_status(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$TaskId=(int)$p_TaskId;
		!$TaskId && ly200::e_json('');
		$ret=1;
		$row=db::get_one('products_sync_task', "Platform='aliexpress' and TaskId='$TaskId'");
		$data=array('Percent'=>$row['CompletionRate']);
		$row['TaskStatus']==2 && $ret=3;//产品数据已同步完成
		ly200::e_json($data, $ret);
	}
	
	public static function aliexpress_grouplist_sync(){//同步产品分组
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$data=array(
			'ApiKey'	=>	'ueeshop_sync',
			'ApiName'	=>	'aliexpress',
			'Action'	=>	'sync_grouplist',
			'Number'	=>	$c['Number'],
			'Account'	=>	$_SESSION['Manage']['Aliexpress']['Token']['Account'],
			'notify_url'=>	ly200::get_domain().'/gateway/',
			'timestamp'	=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');

		$result['ret']==1 && $groupHtml=self::get_group_list($_SESSION['Manage']['Aliexpress']['Token']['Account'], (int)$p_GroupId);
		ly200::e_json($groupHtml, $result['ret']);
	}
	
	public static function get_group_list($Account, $GroupId){//分类下拉内容
		global $c;
		$group_row=str::str_code(db::get_all('products_aliexpress_grouplist', "Account='{$Account}'"));
		foreach((array)$group_row as $v){
			if($v['UpperAliexpressGroupId']){
				$group_data[$v['UpperAliexpressGroupId']]['childGroup'][]=$v;
			}else{
				$group_data[$v['AliexpressGroupId']]=$v;
			}
		}
		$groupData="<option value=''>{$c['manage']['lang_pack']['global']['select_index']}</option>";
		foreach((array)$group_data as $v){
			if(@count((array)$v['childGroup'])){
				$groupData.="<optgroup label=\"{$v['AliexpressGroupName']}\">";
				foreach((array)$v['childGroup'] as $val){
					$selected=((int)$GroupId==$val['AliexpressGroupId']?' selected':'');
					$groupData.="<option value=\"{$val['AliexpressGroupId']}\"{$selected}>{$val['AliexpressGroupName']}</option>";
				}
				$groupData.="</optgroup>";
			}else{
				$selected=((int)$GroupId==$v['AliexpressGroupId']?' selected':'');
				$groupData.="<option value=\"{$v['AliexpressGroupId']}\"{$selected}>{$v['AliexpressGroupName']}</option>";
			}
		}
		return $groupData;
	}
	
	public static function aliexpress_service_template_sync(){//同步服务模板
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$Account=aliexpress::set_default_authorization();
		$data=array(
			'ApiKey'	=>	'ueeshop_sync',
			'ApiName'	=>	'aliexpress',
			'Action'	=>	'sync_service_template',
			'Number'	=>	$c['Number'],
			'Account'	=>	$Account,
			'notify_url'=>	ly200::get_domain().'/gateway/',
			'timestamp'	=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		
		if($result['ret']==1){
			$service_row=str::str_code(db::get_all('products_aliexpress_service_template', "Account='{$Account}'"));
			$serviceHtml=ly200::form_select($service_row, 'promiseTemplateId', $p_ServiceId, 'Name', 'templateId', $c['manage']['lang_pack']['global']['select_index'], 'notnull parent_null="1"');
			$serviceHtml='<div class="box_select" parent_null>'.$serviceHtml.'<div>';
		}
		ly200::e_json($serviceHtml, $result['ret']);
	}
	
	public static function aliexpress_freight_template_sync(){//同步运费模板
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$Account=aliexpress::set_default_authorization();
		$data=array(
			'ApiKey'	=>	'ueeshop_sync',
			'ApiName'	=>	'aliexpress',
			'Action'	=>	'sync_freight_template',
			'Number'	=>	$c['Number'],
			'Account'	=>	$Account,
			'notify_url'=>	ly200::get_domain().'/gateway/',
			'timestamp'	=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		
		if($result['ret']==1){
			$freight_row=str::str_code(db::get_all('products_aliexpress_freight_template', "Account='{$Account}'"));
			$freightHtml=ly200::form_select($freight_row, 'freightTemplateId', $p_FId, 'templateName', 'templateId', $c['manage']['lang_pack']['global']['select_index'], 'notnull parent_null="1"');
			$freightHtml='<div class="box_select" parent_null>'.$freightHtml.'<div>';
		}
		ly200::e_json($freightHtml, $result['ret']);
	}
	
	public static function aliexpress_sizechart_template_sync(){//同步尺码表
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		!(int)$p_categoryId && ly200::e_json('');
		
		$Account=aliexpress::set_default_authorization();
		$data=array(
			'ApiKey'	=>	'ueeshop_sync',
			'ApiName'	=>	'aliexpress',
			'Action'	=>	'sync_sizechart_template',
			'Number'	=>	$c['Number'],
			'Account'	=>	$Account,
			'categoryId'=>	$p_categoryId,
			'notify_url'=>	ly200::get_domain().'/gateway/',
			'timestamp'	=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		
		ly200::e_json('', 1);
	}

	public static function aliexpress_load_account(){	//速卖通账号相关信息
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$group_data=array();
		
		$groupHtml=self::get_group_list($p_Account, (int)$p_GroupId);
		
		$freight_row=str::str_code(db::get_all('products_aliexpress_freight_template', "Account='{$p_Account}'"));
		$freightHtml=ly200::form_select($freight_row, 'freightTemplateId', $p_freightId, 'templateName', 'templateId', $c['manage']['lang_pack']['global']['select_index'], 'notnull parent_null="1"');
		$freightHtml='<div class="box_select" parent_null>'.$freightHtml.'<div>';
		
		$service_row=str::str_code(db::get_all('products_aliexpress_service_template', "Account='{$p_Account}'"));
		$serviceHtml=ly200::form_select($service_row, 'promiseTemplateId', $p_serviceId, 'Name', 'templateId', $c['manage']['lang_pack']['global']['select_index'], 'notnull parent_null="1"');
		$serviceHtml='<div class="box_select" parent_null>'.$serviceHtml.'<div>';
		
		ly200::e_json(array($groupHtml, $freightHtml, $serviceHtml), 1);
	}
	
	public static function sync_aliexpress_products_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		/*******************图片上传(end)*******************/
		//产品主图 6 张
		$ImgPath=$CustomImgPath=array();
		$resize_ary=$c['manage']['resize_ary']['products'];
		$save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($save_dir);
		foreach((array)$p_PicPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			if(!is_file($c['root_path'].$v)) continue;	//速卖通仅允许jpg图片 || $ext_name!='jpg'
			$ImgPath[]=file::photo_tmp_upload($v, $save_dir, $resize_ary);
		}
		if(!count($ImgPath)) ly200::e_json(manage::get_language('products.products.pic_tips'));
		foreach((array)$ImgPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			foreach((array)$resize_ary as $v2){
				if(!is_file($c['root_path'].$v.".{$v2}.{$ext_name}")){
					$size_w_h=explode('x', $v2);
					$resize_path=img::resize($v, $size_w_h[0], $size_w_h[1]);
				}
			}
			if(!is_file($c['root_path'].$v.".default.{$ext_name}")){
				@copy($c['root_path'].$v, $c['root_path'].$v.".default.{$ext_name}");
			}
		}
		
		//自定义图片
		foreach((array)$p_ImagePath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			if(!is_file($c['root_path'].$v)) continue;	//速卖通仅允许jpg图片 || $ext_name!='jpg'
			$CustomImgPath[$k]=file::photo_tmp_upload($v, $save_dir, $resize_ary);
		}
		foreach((array)$CustomImgPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			foreach((array)$resize_ary as $v2){
				if(!is_file($c['root_path'].$v.".{$v2}.{$ext_name}")){
					$size_w_h=explode('x', $v2);
					$resize_path=img::resize($v, $size_w_h[0], $size_w_h[1]);
				}
			}
			if(!is_file($c['root_path'].$v.".default.{$ext_name}")){
				@copy($c['root_path'].$v, $c['root_path'].$v.".default.{$ext_name}");
			}
		}
		/*******************图片上传(end)*******************/
		
		/*******************属性部分(start)*******************/
		$PropertysData=$SKUsData=$sku_key_list=array();
		//普通属性
		foreach((array)$p_property as $k=>$v){
			foreach((array)$v as $key=>$val){
				if(!$val) continue;
				if(@is_numeric($key)){
					$PropertysData[]=array('attrValueId'=>$val, 'attrNameId'=>$k);
				}else{
					$PropertysData[]=array('attrNameId'=>$k, 'attrValue'=>$val);
				}
			}
		}
		foreach((array)$p_attrName as $k=>$v){
			$PropertysData[]=array('attrName'=>$v, 'attrValue'=>$p_attrValue[$k]);
		}
		$aeopAeProductPropertys=str::json_data($PropertysData);
		
		//SKU属性
		foreach((array)$p_sku as $k=>$v){
			foreach((array)$v as $val){
				$sku_key_list[$val]="{$k}:{$val}";
			}
		}
		foreach((array)$p_skuPrice as $k=>$v){
			if($k=='XXX') continue;
			$idList=$attrList=array();
			$key_list=@explode('_', $k);
			foreach((array)$key_list as $val){
				$propertyList=array(
					'propertyValueId'	=>	$val,
					'skuPropertyId'		=>	@reset(explode(':', $sku_key_list[$val]))
				);
				$CustomImgPath[$val] && $propertyList['skuImage']=$CustomImgPath[$val];
				$p_CustomName[$val] && $propertyList['propertyValueDefinitionName']=$p_CustomName[$val];
				
				$attrList[]=$propertyList;
				$idList[]=$sku_key_list[$val];
			}
			$data_ary=array(
				'id'				=>	@implode(';', $idList),
				'currencyCode'		=>	'USD',
				'ipmSkuStock'		=>	(int)$p_skuStock[$k],
				'skuPrice'			=>	(float)$v,
				'skuStock'			=>	(int)$p_skuStock[$k]?1:0,
				'aeopSKUProperty'	=>	$attrList,
				'skuCode'			=>	$p_skuCode[$k]
			);
			$SKUsData[]=$data_ary;
		}
		!count($SKUsData) && $SKUsData=array(
			array(
				'id'				=>	'<none>',
				'currencyCode'		=>	'USD',
				'ipmSkuStock'		=>	(int)$p_ipmSkuStock,
				'skuPrice'			=>	(float)$p_productPrice,
				'skuStock'			=>	(int)$p_ipmSkuStock?1:0,
				'aeopSKUProperty'	=>	array(),
				'skuCode'			=>	$p_ipmSkuCode
			)
		);
		$aeopAeProductSKUs=str::json_data($SKUsData);
		/*******************属性部分(end)*******************/

		$data=array(
			'Account'			=>	$p_Account,
			'categoryId'		=>	(int)$p_categoryId,
			'GroupId'			=>	(int)$p_GroupId,
			'GroupIds'			=>	str::json_data(array((int)$p_GroupId)),
			'Subject'			=>	addslashes(stripslashes($p_Subject)),
			'productPrice'		=>	$SKUsData[0]['skuPrice'],
			'PicPath_0'			=>	$ImgPath[0],
			'PicPath_1'			=>	$ImgPath[1],
			'PicPath_2'			=>	$ImgPath[2],
			'PicPath_3'			=>	$ImgPath[3],
			'PicPath_4'			=>	$ImgPath[4],
			'PicPath_5'			=>	$ImgPath[5],
			'aeopAeProductPropertys'	=>	$aeopAeProductPropertys,
			'aeopAeProductSKUs'	=>	$aeopAeProductSKUs,
			'deliveryTime'		=>	(int)$p_deliveryTime,
			'packageType'		=>	(int)$p_packageType,
			'packageLength'		=>	(int)$p_packageLength,
			'packageWidth'		=>	(int)$p_packageWidth,
			'packageHeight'		=>	(int)$p_packageHeight,
			'grossWeight'		=>	(float)$p_grossWeight,
			'isPackSell'		=>	(int)$p_isPackSell,
			'reduceStrategy'	=>	$p_reduceStrategy,
			'productUnit'		=>	(int)$p_productUnit,
			'wsValidNum'		=>	(int)$p_wsValidNum,
			'freightTemplateId'	=>	(int)$p_freightTemplateId,
			'promiseTemplateId'	=>	(int)$p_promiseTemplateId,					//服务模板
			'gmtModified'		=>	$c['time'],									//商品最后更新时间
		);
		(int)$p_packageType && $data['lotNum']=(int)$p_lotNum;//销售方式：打包出售
		if((int)$p_isPackSell){//自定义计重
			$data['baseUnit']=(int)$p_baseUnit;
			$data['addUnit']=(int)$p_addUnit;
			$data['addWeight']=(float)$p_addWeight;
		}
		if((int)$p_IsWholeSale){//批发价设置
			$data['bulkOrder']=(int)$p_bulkOrder;
			$data['bulkDiscount']=(int)$p_bulkDiscount;
		}
		
		$ProId=(int)$p_ProId;
		if($ProId){
			db::update('products_aliexpress', "ProId='$ProId'", $data);
			if(!db::get_row_count('products_aliexpress_description', "ProId='$ProId'")){
				db::insert('products_aliexpress_description', array(
						'Account'		=>	$p_Account,	
						'ProId'			=>	$ProId,
						'productId'		=>	db::get_value('products_aliexpress', "ProId='$ProId'", 'productId'),
						'Detail'		=>	$p_Detail,
						'mobileDetail'	=>	$p_mobileDetail
					)
				);
			}else{
				db::update('products_aliexpress_description', "ProId='$ProId'", array(
						'Account'		=>	$p_Account,
						'Detail'		=>	$p_Detail,
						'mobileDetail'	=>	$p_mobileDetail
					)
				);
			}
			manage::operation_log('修改速卖通产品');
		}else{
			$data['gmtCreate']=$c['time'];
			db::insert('products_aliexpress', $data);
			$ProId=db::get_insert_id();
			db::insert('products_aliexpress_description', array(
					'Account'		=>	$p_Account,	
					'ProId'			=>	$ProId,
					'Detail'		=>	$p_Detail,
					'mobileDetail'	=>	$p_mobileDetail
				)
			);
			manage::operation_log('添加速卖通产品');
		}

		ly200::e_json(array('jump'=>$p_back_action?$p_back_action:"?m=products&a=sync&d=aliexpress"), 1);
	}
	
	public static function aliexpress_products_img_del(){	//删除单个产品图片
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$Model=$g_Model;
		$PicPath=$g_Path;
		$Index=(int)$g_Index;
		$resize_ary=$c['manage']['resize_ary'][$Model];	//products
		if(is_file($c['root_path'].$PicPath)){
			foreach((array)$resize_ary as $v){
				$ext_name=file::get_ext_name($PicPath);
				file::del_file($PicPath.".{$v}.{$ext_name}");
			}
			file::del_file($PicPath);
			aliexpress::remove_aliexpress_images($PicPath);
		}
		manage::operation_log('删除速卖通产品图片');
		ly200::e_json(array($Index), 1);
	}
	
	public static function aliexpress_get_category_list_by_UId($UId){
		if(!$UId) return;
		
		$str='';
		$row=db::get_all('products_aliexpress_category', "AliexpressCateId in({$UId})", 'AliexpressCategory', 'Level asc');
		foreach((array)$row as $k=>$v){
			$str.=($k==0?'':' &gt; ')."{$v['AliexpressCategory']}";
		}
		return $str;
	}

	public static function aliexpress_get_attr(){	//产品属性
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Content=$html_attr=$html_item='';
		$all_attr_data=$property_data=$property_ary=$sku_data=$attribute_data=$sku_property_data=$sku_selected_data=$attributeAry=array();
		$Lang=@substr($c['manage']['config']['ManageLanguage'],0,2);//语言
		$language=$c['manage']['lang_pack'];
		
		$categoryId=(int)$p_categoryId; //产品分类ID
		$AttrId=(int)$p_AttrId; //切换前的分类ID
		$ProId=(int)$p_ProId; //产品ID
		if(!$categoryId) ly200::e_json('请选择分类', -1);//未选择分类
		if($AttrId && $AttrId==$categoryId) ly200::e_json('相同分类，不切换！', -2);//相同的分类就不用继续执行
		
		$products_category_row=str::str_code(db::get_one('products_aliexpress_category', "AliexpressCateId='{$categoryId}'"));
		//分类面包屑
		$categoryStr=self::aliexpress_get_category_list_by_UId($products_category_row['AliexpressUId'].$products_category_row['AliexpressCateId']);
		//分类属性内容
		$AttributeValue=str::str_code($products_category_row['AttributeValue'], 'htmlspecialchars_decode');
		if($AttributeValue==''){
			$Account=aliexpress::set_default_authorization();
			$sync_data=array(
				'ApiKey'	=>	'ueeshop_sync',
				'ApiName'	=>	'aliexpress',
				'Action'	=>	'sync_attributes',
				'Number'	=>	$c['Number'],
				'Account'	=>	$Account,
				'categoryId'=>	$products_category_row['AliexpressCateId'],
				'timestamp'	=>	$c['time']
			);
			$sync_data['sign']=ly200::sign($sync_data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $sync_data, '', $curl_opt), 'decode');
			$AttributeValue=@gzuncompress(base64_decode($result['msg']['AttributeValue']));
			db::update('products_aliexpress_category', "AliexpressCateId='{$categoryId}'", array('AttributeValue'=>addslashes($AttributeValue)));
		}
		$attributeAry=@str::json_data($AttributeValue, 'decode');
		foreach((array)$attributeAry as $v){
			if($v['sku']==1){
				$sku_property_data[$v['spec']]=$v;
			}else $attribute_data[]=$v;
		}
		
		//当前产品选中项
		if($ProId){
			$products_row=db::get_one('products_aliexpress', "ProId='$ProId'", 'aeopAeProductPropertys,aeopAeProductSKUs');
			$property_ary=str::json_data(str::str_code($products_row['aeopAeProductPropertys'], 'htmlspecialchars_decode'), 'decode');
			$sku_data=str::json_data(str::str_code($products_row['aeopAeProductSKUs'], 'htmlspecialchars_decode'), 'decode');
			foreach((array)$property_ary as $v){
				$key=$v['attrNameId']?'system':'custom';
				$property_data[$key][(int)$v['attrNameId']][]=$v;
			}
			$ext_attr_data=array();
			foreach((array)$sku_data as $v){
				$key='';
				foreach((array)$v['aeopSKUProperty'] as $v1){
					$key.=(!$key?'':'_').$v1['propertyValueId'];
					$sku_selected_data[$v1['skuPropertyId']][$v1['propertyValueId']]=$v1;
					!@in_array($v1['propertyValueId'], $sku_selected_data[$v1['skuPropertyId']]['selected']) && $sku_selected_data[$v1['skuPropertyId']]['selected'][]=$v1['propertyValueId'];
				}
				$ext_attr_data[$key]=array($v['skuPrice'], $v['ipmSkuStock'], $v['skuCode']);
			}
		}
		
		//普通属性内容
		$generalAttrHtml=$customAttrHtml=$skuAttrHtml='';
		//print_r($attribute_data);exit;
		foreach((array)$attribute_data as $v){
			$names=str::json_data($v['names'], 'decode');
			$important=(int)$v['required']?'<font class="fc_red">*</font> ':((int)$v['keyAttribute']?'<font color="#009900">!</font> ':'');
			$required=(int)$v['required']?' notnull':'';
			$required_select=(int)$v['required']?' notnull parent_null="1"':'';
			$required_select_parent=(int)$v['required']?' parent_null':'';
			
			$checkboxClass=($v['attributeShowTypeValue']=='check_box' && (int)$v['required'])?' form-checkbox-notnull':'';
			$generalAttrHtml.='<div class="form-item"><em>'.($important . $names[$Lang]).'</em><div class="form-control'.$checkboxClass.'" title="'.$names[$Lang].'">';
			if($v['attributeShowTypeValue']=='list_box'){
				$generalAttrHtml.="<div class='box_select'{$required_select_parent}><select name=\"property[{$v['id']}][]\"{$required_select}>";
				$generalAttrHtml.='<option value="">'.$language['global']['select_index'].'</option>';
					foreach((array)$v['values'] as $k1=>$v1){
						$v1names=str::json_data($v1['names'], 'decode');
						$selected=$v1['id']==$property_data['system'][$v['id']][0]['attrValueId']?' selected="selected"':'';
						$generalAttrHtml.="<option value=\"{$v1['id']}\"{$selected}>{$v1names['en']}({$v1names['zh']})</option>";
					}
				$generalAttrHtml.="</select></div>";
			}elseif($v['attributeShowTypeValue']=='check_box'){
				$data=array();
				foreach((array)$property_data['system'][$v['id']] as $v0){$data[]=$v0['attrValueId'];}
				foreach((array)$v['values'] as $k1=>$v1){
					$checked=@in_array($v1['id'], $data)?' checked="checked"':'';
					$v1names=str::json_data($v1['names'], 'decode');
					$generalAttrHtml.='<label for="value_'.$v1['id'].'">';
					$generalAttrHtml.='<input type="checkbox" id="value_'.$v1['id'].'" name="property['.$v['id'].'][]" value="'.$v1['id'].'"'.$required.$checked.' /> '.$v1names['en'].'('.$v1names['zh'].')';
					$generalAttrHtml.='</label>';
				}
			}elseif($v['attributeShowTypeValue']=='input'){
				$generalAttrHtml.='<input name="property['.$v['id'].'][value]" value="'.$property_data['system'][$v['id']][0]['attrValue'].'" type="text" class="box_input" size="40" maxlength="70"'.$required.' />';
			}
			$generalAttrHtml.='</div><div class="clear"></div></div>';
		}
		
		//自定义属性，首次加载页面才返回，切换分类保持不变
		if(!$AttrId){
			foreach((array)$property_data['custom'][0] as $v){
				$customAttrHtml.='<div class="custom-property-item">';
				$customAttrHtml.='<input name="attrName[]" value="'.$v['attrName'].'" type="text" class="box_input" size="25" maxlength="40" notnull /> : ';
				$customAttrHtml.='<input name="attrValue[]" value="'.$v['attrValue'].'" type="text" class="box_input" size="45" maxlength="70" notnull /> ';
				$customAttrHtml.='<a href="javascript:;" class="green del">'.$language['global']['del'].'</a></div>';
			}
			$customAttrHtml.='<div class="add-property"><a href="javascript:;" class="btn_ok add">+'.$language['global']['add'].$language['products']['custom'].$language['products']['attribute'].'</a></div>';
			$customAttrHtml.='<div class="clear"></div>';
		}
		
		//SKU属性
		$callback=''; //返回函数
		foreach((array)$sku_property_data as $v){
			$names=str::json_data($v['names'], 'decode');
			$skuAttrHtml.='<div class="rows sku-label-list">';
				$skuAttrHtml.='<label>'.$names[$Lang].'</label>';
				$skuAttrHtml.='<span class="input"><div class="sku-property-list'.($v['skuStyleValue']=='colour_atla'?' color-property':'').'">';
					foreach((array)$v['values'] as $k1=>$v1){
						$v1names=str::json_data($v1['names'], 'decode');
						$data_ary=array(
							'AttrId'		=>	$v['id'],
							'Column'		=>	str_replace(array('&quot;', '"', "'"), array('"', '\"', '@8#'), $names[$Lang]),
							'Name'			=>	str_replace(array('&quot;', '"', "'"), array('"', '\"', '@8#'), $v1names['en']),
							'Num'			=>	$v1['id'],
							'customizedName'=>	$v['customizedName'],
							'customizedPic'	=>	$v['customizedPic']
						);
						$sku_selected_data[$v['id']][$v1['id']]['propertyValueDefinitionName'] && $data_ary['customName']=$sku_selected_data[$v['id']][$v1['id']]['propertyValueDefinitionName'];
						if(@is_file($c['root_path'].$sku_selected_data[$v['id']][$v1['id']]['skuImage'])){
							$data_ary['skuImage']=$sku_selected_data[$v['id']][$v1['id']]['skuImage'];
							$data_ary['skuImageExt']=ly200::get_size_img($sku_selected_data[$v['id']][$v1['id']]['skuImage'], '240x240');
						}
						$data=str::json_data($data_ary);
						
						$all_attr_data[$v['id']][$v1['id']]=$v1names['en'];
						$checked=@in_array($v1['id'], $sku_selected_data[$v['id']]['selected'])?' checked="checked"':'';
					   
						$skuAttrHtml.='<label title="'.$v1names[$Lang].'">';
						$skuAttrHtml.="<input type='checkbox' id='Attr_{$v1['id']}' name='sku[{$v['id']}][]' value='{$v1['id']}' data='{$data}'{$checked} />";//value_
						$v['skuStyleValue']=='colour_atla' && $skuAttrHtml.='<span class="property-title" style="background-color:'.str_replace(' ','',$v1names['en']).';"></span> ';
						$skuAttrHtml.=$v1names['zh'].'('.$v1names['en'].')';
						$skuAttrHtml.='</label>';

						if(@in_array($v1['id'], $sku_selected_data[$v['id']]['selected'])){
							$callback.="sync_obj.aliexpress_edit_attr_init('{$data}', 'Attr_{$v1['id']}', 0);";
						}

					}
				$skuAttrHtml.='</div><div class="clear"></div>';
				
				if($v['customizedName'] || $v['customizedPic']){//有自定义项内容
					$skuAttrHtml.='<div id="sku-custom-property-'.$v['id'].'" class="sku-custom-property hide">';
						$skuAttrHtml.='<table border="0" cellpadding="5" cellspacing="0" class="relation_box"><thead><tr>';
							$skuAttrHtml.='<td width="20%">'.$names[$Lang].'</td>';
							$v['customizedName'] && $skuAttrHtml.='<td width="40%">'.$language['products']['sync']['customName'].'</td>';
							$v['customizedPic'] && $skuAttrHtml.='<td width="40%">'.$language['products']['pic_upfile'].' '.$language['products']['sync']['picTips'].'</td>';
						$skuAttrHtml.='</tr></thead><tbody></tbody></table>';
					$skuAttrHtml.='</div>';
				}
				$skuAttrHtml.='</span><div class="clear"></div>';
			$skuAttrHtml.='</div>';
		}
		
		ly200::e_json(array($categoryStr, $generalAttrHtml, $customAttrHtml, $skuAttrHtml, str::json_data($all_attr_data), str::json_data($ext_attr_data), $callback), 1);
	}
	
	public static function aliexpress_products_del(){	//删除速卖通产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$ProId=$g_ProId;
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_one('products_aliexpress', "ProId='$ProId'", 'PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, aeopAeProductSKUs');
		for($i=0;$i<6;$i++){
			$PicPath=$row["PicPath_$i"];
			if(is_file($c['root_path'].$PicPath)){
				foreach((array)$resize_ary as $v){
					$ext_name=file::get_ext_name($PicPath);
					file::del_file($PicPath.".{$v}.{$ext_name}");
				}
				file::del_file($PicPath);
				aliexpress::remove_aliexpress_images($PicPath);
			}
		}
		//删除产品颜色图片
		$aeopAeProductSKUs=str::json_data($row['aeopAeProductSKUs'], 'decode');
		foreach((array)$aeopAeProductSKUs as $v){
			foreach((array)$v['aeopSKUProperty'] as $val){
				if($val['skuImage'] && is_file($c['root_path'].$val['skuImage'])){
					foreach((array)$resize_ary as $value){
						$ext_name=file::get_ext_name($val['skuImage']);
						file::del_file($val['skuImage'].".{$value}.{$ext_name}");
					}
					file::del_file($val['skuImage']);
				}
			}
		}
		db::delete('products_aliexpress', "ProId='$ProId'");
		db::delete('products_aliexpress_description', "ProId='$ProId'");
		manage::operation_log('删除速卖通产品');
		ly200::e_json('', 1);
	}
	
	public static function aliexpress_products_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_group_proid && ly200::e_json('');
		$del_where="ProId in(".str_replace('-',',',$g_group_proid).")";
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_all('products_aliexpress', $del_where, 'PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, aeopAeProductSKUs');
		foreach((array)$row as $v){
			for($i=0; $i<6; $i++){
				$PicPath=$v["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					foreach((array)$resize_ary as $v2){
						$ext_name=file::get_ext_name($PicPath);
						file::del_file($PicPath.".{$v2}.{$ext_name}");
					}
					file::del_file($PicPath);
					aliexpress::remove_aliexpress_images($PicPath);
				}
			}
			//删除产品颜色图片
			$aeopAeProductSKUs=str::json_data($v['aeopAeProductSKUs'], 'decode');
			foreach((array)$aeopAeProductSKUs as $v2){
				foreach((array)$v2['aeopSKUProperty'] as $val){
					if($val['skuImage'] && is_file($c['root_path'].$val['skuImage'])){
						foreach((array)$resize_ary as $value){
							$ext_name=file::get_ext_name($val['skuImage']);
							file::del_file($val['skuImage'].".{$value}.{$ext_name}");
						}
						file::del_file($val['skuImage']);
					}
				}
			}
		}

		db::delete('products_aliexpress', $del_where);
		db::delete('products_aliexpress_description', $del_where);
		manage::operation_log('批量删除速卖通产品');
		ly200::e_json('', 1);
	}
	
	public static function copy_alexpress_to_products(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$group_ids=str_replace('-', ',', $p_group_id);
		$CateId=(int)$p_CateId;
		!$group_ids && ly200::e_json('请选择要复制的产品!');
		!$CateId && ly200::e_json('请选择产品分类!');
		
		$row=str::str_code(db::get_all('products_aliexpress', "ProId in($group_ids)"));
		foreach((array)$row as $v){
			//库存、SKU
			$skuCode='';
			$Stock=0;
			$aeopAeProductSKUs=str::json_data(htmlspecialchars_decode($v['aeopAeProductSKUs']), 'decode');
			foreach((array)$aeopAeProductSKUs as $val){
				$Stock+=$val['ipmSkuStock'];
				(!$skuCode && $val['skuCode']) && $skuCode=$val['skuCode'];
			}
			!$skuCode && $skuCode=$v['productId']+200;
			
			$data=array(
				'CateId'							=>	$CateId,
				'Source'							=>	'aliexpress',
				'SourceId'							=>	$v['productId'],
				"Name{$c['manage']['web_lang']}"	=>	addslashes(stripslashes($v['Subject'])),
				'SKU'								=>	$skuCode,
				'Number'							=>	$skuCode,
				'Price_1'							=>	$v['productPrice'],
				'Stock'								=>	$Stock,
				'Weight'							=>	$v['grossWeight'],
				'Cubage'							=>	@implode(',', array(sprintf('%01.2f', $v['packageLength']/100), sprintf('%01.2f', $v['packageWidth']/100), sprintf('%01.2f', $v['packageHeight']/100))),
				'SoldOut'							=>	1
			);
			//批发价
			($v['bulkOrder'] && $v['bulkDiscount']) && $data['Wholesale']=str::json_data(array($v['bulkOrder']=>sprintf('%01.2f', $v['productPrice']*(100-$v['bulkDiscount'])/100)));
			
			$ProId=(int)db::get_value('products', "Source='aliexpress' and SourceId='{$v['productId']}'", 'ProId');
			if(!$ProId){
				$resize_ary=$c['manage']['resize_ary']['products'];
				$temp_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
				file::mk_dir($temp_dir);
				$j=0;
				for($i=0; $i<10; ++$i){
					$PicPath=$v["PicPath_$i"];
					if(is_file($c['root_path'].$PicPath)){
						$ext_name=file::get_ext_name($PicPath);
						$data['PicPath_'.$j++]=$temp=$temp_dir.str::rand_code().'.'.$ext_name;
						foreach((array)$resize_ary as $size){
							$RePicPath=$PicPath.".{$size}.{$ext_name}";
							@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$size}.{$ext_name}", '/'));
						}
						@copy($c['root_path'].$PicPath, $c['root_path'].ltrim($temp, '/'));
					}
				}
			}

			if($ProId){
				$w="ProId='{$ProId}'";
				$data['EditTime']=$c['time'];
				db::update('products', $w, $data);
				!db::get_row_count('products_seo', $w) && db::insert('products_seo', array('ProId'=>$products_row['ProId']));
				if(!db::get_row_count('products_description', $w)){
					db::insert('products_description', array(
							'ProId'									=>	$ProId,
							"Description{$c['manage']['web_lang']}"	=>	addslashes(stripslashes(db::get_value('products_aliexpress_description', "productId='{$v['productId']}'", 'Detail')))
						)
					);
				}else{
					db::update('products_description', $w, array(
							"Description{$c['manage']['web_lang']}"	=>	addslashes(stripslashes(db::get_value('products_aliexpress_description', "productId='{$v['productId']}'", 'Detail')))
						)
					);
				}
			}else{
				$data['AccTime']=$c['time'];
				db::insert('products', $data);
				$ProId=db::get_insert_id();
				db::insert('products_seo', array('ProId'=>$ProId));
				db::insert('products_description', array(
						'ProId'									=>	$ProId,
						"Description{$c['manage']['web_lang']}"	=>	addslashes(stripslashes(db::get_value('products_aliexpress_description', "productId='{$v['productId']}'", 'Detail')))
					)
				);
			}
		}
		manage::operation_log('批量复制速卖通产品到本地');
		ly200::e_json('复制完成!', 1);
	}
	/*****************************************
	速卖通同步部分(end)
	*****************************************/


	/*****************************************
	亚马逊同步部分(start)
	*****************************************/
	public static function change_amazon_authorization_account(){//切换当前速卖通账号
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AccountId=(int)$p_AccountId;
		!$AccountId && ly200::e_json('请选择切换的账号！');
		$AccountId==$_SESSION['Manage']['Amazon']['AId'] && ly200::e_json('');
		
		amazon::set_default_authorization($AccountId);
		ly200::e_json('', 1);
	}

	public static function amazon_products_sync(){//同步产品
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;

		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'sync_products',
			'ApiName'		=>	'amazon',
			'Number'		=>	$c['Number'],
			'Account'		=>	$_SESSION['Manage']['Amazon']['Account'],
			'notify_url'	=>	ly200::get_domain().'/gateway/',
			'timestamp'		=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		if($result['ret']==1){
			$TaskId=$result['msg']['TaskId'];
			$taskData=array(
				'Platform'	=>	'amazon',
				'TaskId'	=>	$TaskId,
				'AccTime'	=>	time()
			);
			!db::get_row_count('products_sync_task', "Platform='amazon' and TaskId='{$TaskId}'") && db::insert('products_sync_task', $taskData);
		}
		
		ly200::e_json($TaskId?array('TaskId'=>$TaskId):$result['msg'], (int)$result['ret']);
	}
	
	public static function amazon_products_sync_status(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$TaskId=(int)$p_TaskId;
		!$TaskId && ly200::e_json('');
		$ret=1;
		$row=db::get_one('products_sync_task', "Platform='amazon' and TaskId='$TaskId'");
		$data=array('Percent'=>$row['CompletionRate']);
		$row['TaskStatus']==2 && $ret=3;//产品数据已同步完成
		if($row['TaskStatus']==2 && $row['ReturnData']){
			$ReturnData=str::json_data($row['ReturnData'], 'decode');
			$ret=2;
			$data['Start']=(int)$ReturnData['PageNext'];
		}
		ly200::e_json($data, $ret);
	}
	
	public static function sync_amazon_products_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		
		$ItemDimensions=$p_ItemDimensions;
		if(!$ItemDimensions['Length'][0]) unset($ItemDimensions['Length']);
		if(!$ItemDimensions['Width'][0]) unset($ItemDimensions['Width']);
		if(!$ItemDimensions['Height'][0]) unset($ItemDimensions['Height']);
		$PackageDimensions=$p_PackageDimensions;
		if(!$PackageDimensions['Weight'][0]) unset($PackageDimensions['Weight']);
		if(!$PackageDimensions['Length'][0]) unset($PackageDimensions['Length']);
		if(!$PackageDimensions['Width'][0]) unset($PackageDimensions['Width']);
		if(!$PackageDimensions['Height'][0]) unset($PackageDimensions['Height']);
		
		$data=array(
			'`MerchantId`'			=>	$p_Account,
			'`item-name`'			=>	addslashes(stripslashes($_POST['item-name'])),
			'`price`'				=>	sprintf('%01.2f', $p_price),
			'`image-url`'			=>	$p_ImagePath,
			'`ListPrice`'			=>	$p_ListPrice['Amount']?str::json_data($p_ListPrice):'',
			'`quantity`'			=>	(int)$p_quantity,
			'`ItemDimensions`'		=>	$ItemDimensions?str::json_data($ItemDimensions):'',
			'`PackageDimensions`'	=>	$PackageDimensions?str::json_data($PackageDimensions):'',
			'`Feature`'				=>	str::str_code(str::json_data(str::str_code($p_Feature, 'stripslashes')), 'addslashes'),
			'`item-description`'	=>	addslashes(stripslashes($_POST['item-description']))
		);
		
		db::update('products_amazon', "ProId='{$p_ProId}'", $data);
		manage::operation_log('编辑亚马逊产品：'.$_POST['seller-sku']);
		ly200::e_json('', 1);
	}
	
	public static function amazon_products_del(){	//删除亚马逊产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;

		$ProId=(int)$g_ProId;
		$where="ProId='$ProId'";
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_one('products_amazon', $where, '`image-url`');
		$ImgPath=$row['image-url'];
		if(is_file($c['root_path'].$ImgPath)){
			foreach((array)$resize_ary as $v){
				$ext_name=file::get_ext_name($ImgPath);
				file::del_file($ImgPath.".{$v}.{$ext_name}");
			}
			file::del_file($ImgPath);
		}
		db::delete('products_amazon', $where);
		manage::operation_log('删除亚马逊产品');
		ly200::e_json('', 1);
	}
	
	public static function amazon_products_del_bat(){//批量删除亚马逊产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;

		!$g_group_proid && ly200::e_json('');
		$del_where="ProId in(".str_replace('-',',',$g_group_proid).")";
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_all('products_amazon', $del_where, '`image-url`');
		foreach((array)$row as $v){
			$ImgPath=$v["image-url"];
			if(is_file($c['root_path'].$ImgPath)){
				foreach((array)$resize_ary as $v2){
					$ext_name=file::get_ext_name($ImgPath);
					file::del_file($ImgPath.".{$v2}.{$ext_name}");
				}
				file::del_file($ImgPath);
			}
		}

		db::delete('products_amazon', $del_where);
		manage::operation_log('批量删除亚马逊产品');
		ly200::e_json('', 1);
	}
	
	public static function copy_amazon_to_products(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;

		$group_ids=str_replace('-', ',', $p_group_id);
		$CateId=(int)$p_CateId;
		!$group_ids && ly200::e_json('请选择要复制的产品!');
		!$CateId && ly200::e_json('请选择产品分类!');
		
		$row=str::str_code(db::get_all('products_amazon', "ProId in($group_ids)"));
		foreach((array)$row as $v){
			$Feature=$ListPrice=$ItemDimensions=$PackageDimensions=$Cubage=array();
			$v['Feature'] && $Feature=str::json_data(htmlspecialchars_decode($v['Feature']), 'decode');
			$v['ListPrice'] && $ListPrice=str::json_data(htmlspecialchars_decode($v['ListPrice']), 'decode');
			$v['ItemDimensions'] && $ItemDimensions=str::json_data(htmlspecialchars_decode($v['ItemDimensions']), 'decode');
			$v['PackageDimensions'] && $PackageDimensions=str::json_data(htmlspecialchars_decode($v['PackageDimensions']), 'decode');
			
			$v=str::str_code(str::str_code($v, 'stripslashes'), 'addslashes');
			$data=array(
				'CateId'							=>	$CateId,
				'Source'							=>	'amazon',
				'SourceId'							=>	$v['product-id']?$v['product-id']:$v['asin1'],
				"Name{$c['manage']['web_lang']}"	=>	$v['item-name'],
				'SKU'								=>	$v['seller-sku']?$v['seller-sku']:$v['asin1'],
				'Number'							=>	$v['listing-id']?$v['listing-id']:$v['asin1'],
				'Price_0'							=>	$ListPrice['Amount'],
				'Price_1'							=>	$v['price'],
				'Stock'								=>	$v['quantity'],
				'SoldOut'							=>	1
			);
			//产品重量
			$PackageDimensions['Weight'] && $data['Weight']=$PackageDimensions['Weight'][0];//包装后重量
			$ItemDimensions['Weight'] && $data['Weight']=$ItemDimensions['Weight'][0];//产品毛重
			//产品长宽高
			($PackageDimensions['Length'] || $PackageDimensions['Width'] || $PackageDimensions['Height']) && $data['Cubage']=@implode(',', array(sprintf('%01.2f', $PackageDimensions['Length'][0]/100), sprintf('%01.2f', $PackageDimensions['Width'][0]/100), sprintf('%01.2f', $PackageDimensions['Height'][0]/100)));//包装后长宽高
			($ItemDimensions['Length'] || $ItemDimensions['Width'] || $ItemDimensions['Height']) && $data['Cubage']=@implode(',', array(sprintf('%01.2f', $ItemDimensions['Length'][0]/100), sprintf('%01.2f', $ItemDimensions['Width'][0]/100), sprintf('%01.2f', $ItemDimensions['Height'][0]/100)));//包装前长宽高
			
			
			$ProId=(int)db::get_value('products', "Source='amazon' and SourceId!='' and SourceId='{$v['product-id']}'", 'ProId');
			!$ProId && $ProId=(int)db::get_value('products', "Source='amazon' and SourceId!='' and SourceId='{$v['asin1']}'", 'ProId');
			if(!$ProId && is_file($c['root_path'].$v['image-url'])){
				$resize_ary=$c['manage']['resize_ary']['products'];
				$save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
				file::mk_dir($save_dir);
				
				$ext_name=file::get_ext_name($v['image-url']);
				$data['PicPath_0']=$save_dir.str::rand_code().'.'.$ext_name;
				//直接复制产品不同规格图片
				@copy($c['root_path'].$v['image-url'], $c['root_path'].ltrim($data['PicPath_0'], '/'));
				foreach((array)$resize_ary as $size){
					$RePicPath=$v['image-url'].".{$size}.{$ext_name}";
					@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($data['PicPath_0'].".{$size}.{$ext_name}", '/'));
				}
				
			}
			
			$Description=addslashes(stripslashes(@implode($Feature, "<br />")."<br /><br /><br />".htmlspecialchars_decode($v['item-description'])));
			if($ProId){
				$w="ProId='{$ProId}'";
				$data['EditTime']=$c['time'];
				db::update('products', $w, $data);
				!db::get_row_count('products_seo', $w) && db::insert('products_seo', array('ProId'=>$products_row['ProId']));
				if(!db::get_row_count('products_description', $w)){
					db::insert('products_description', array(
							'ProId'									=>	$ProId,
							"Description{$c['manage']['web_lang']}"	=>	$Description
						)
					);
				}else{
					db::update('products_description', $w, array("Description{$c['manage']['web_lang']}"=>$Description));
				}
			}else{
				$data['AccTime']=$c['time'];
				db::insert('products', $data);
				$ProId=db::get_insert_id();
				db::insert('products_seo', array('ProId'=>$ProId));
				db::insert('products_description', array(
						'ProId'									=>	$ProId,
						"Description{$c['manage']['web_lang']}"	=>	$Description
					)
				);
			}
		}
		manage::operation_log('批量复制亚马逊产品到本地');
		ly200::e_json('复制完成!', 1);
	}
	/*****************************************
	亚马逊同步部分(end)
	*****************************************/
	
	
	/*****************************************
	Wish同步部分(start)
	*****************************************/
    public static function change_wish_authorization_account()
    {
        //切换当前Wish账号
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AccountId = (int)$p_AccountId;
		!$AccountId && ly200::e_json('请选择切换的账号！');
		$AccountId == $_SESSION['Manage']['Wish']['Token']['AuthorizationId'] && ly200::e_json('');
		
		wish::set_default_authorization($AccountId);
		ly200::e_json('', 1);
	}
    
	public static function wish_products_sync(){//同步产品
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		
		//$row=db::get_one('authorization', 'Platform="wish"');
		//$token=str::json_data($row['Token'], 'decode');
		
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'sync_products',
			'ApiName'		=>	'wish',
			'Number'		=>	$c['Number'],
			'Account'		=>	$_SESSION['Manage']['Wish']['Account'], //$row['Account'],
			'notify_url'	=>	ly200::get_domain().'/gateway/',
			'Start'			=>	(int)$p_Start,
			'timestamp'		=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		if($result['ret']==1){
			$TaskId=$result['msg']['TaskId'];
			$taskData=array(
				'Platform'	=>	'wish',
				'TaskId'	=>	$TaskId,
				'AccTime'	=>	time()
			);
			!db::get_row_count('products_sync_task', "Platform='wish' and TaskId='{$TaskId}'") && db::insert('products_sync_task', $taskData);
		}
		
		ly200::e_json($TaskId?array('TaskId'=>$TaskId):$result['msg'], (int)$result['ret']);
	}
	
	public static function wish_products_sync_status(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$TaskId=(int)$p_TaskId;
		!$TaskId && ly200::e_json('');
		$ret=1;
		$row=db::get_one('products_sync_task', "Platform='wish' and TaskId='$TaskId'");
		$data=array('Percent'=>$row['CompletionRate']);
		$row['TaskStatus']==2 && $ret=3;//产品数据已同步完成
		if($row['TaskStatus']==2 && $row['ReturnData']){
			$ReturnData=str::json_data($row['ReturnData'], 'decode');
			$ret=2;
			$data['Start']=(int)$ReturnData['PageNext'];
		}
		ly200::e_json($data, $ret);
	}
	
	public static function sync_wish_products_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		
		$ItemDimensions=$p_ItemDimensions;
		if(!$ItemDimensions['Length'][0]) unset($ItemDimensions['Length']);
		if(!$ItemDimensions['Width'][0]) unset($ItemDimensions['Width']);
		if(!$ItemDimensions['Height'][0]) unset($ItemDimensions['Height']);
		$PackageDimensions=$p_PackageDimensions;
		if(!$PackageDimensions['Weight'][0]) unset($PackageDimensions['Weight']);
		if(!$PackageDimensions['Length'][0]) unset($PackageDimensions['Length']);
		if(!$PackageDimensions['Width'][0]) unset($PackageDimensions['Width']);
		if(!$PackageDimensions['Height'][0]) unset($PackageDimensions['Height']);
		
		$data=array(
			'`MerchantId`'			=>	$p_Account,
			'`item-name`'			=>	addslashes(stripslashes($_POST['item-name'])),
			'`price`'				=>	sprintf('%01.2f', $p_price),
			'`image-url`'			=>	$p_ImagePath,
			'`ListPrice`'			=>	$p_ListPrice['Amount']?str::json_data($p_ListPrice):'',
			'`quantity`'			=>	(int)$p_quantity,
			'`ItemDimensions`'		=>	$ItemDimensions?str::json_data($ItemDimensions):'',
			'`PackageDimensions`'	=>	$PackageDimensions?str::json_data($PackageDimensions):'',
			'`Feature`'				=>	str::str_code(str::json_data(str::str_code($p_Feature, 'stripslashes')), 'addslashes'),
			'`item-description`'	=>	addslashes(stripslashes($_POST['item-description']))
		);
		
		db::update('products_wish', "ProId='{$p_ProId}'", $data);
		manage::operation_log('编辑Wish产品：'.$_POST['seller-sku']);
		ly200::e_json('', 1);
	}
	
	public static function wish_products_del(){	//删除Wish产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;
		$ProId=(int)$g_ProId;
		$where="ProId='$ProId'";
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_one('products_wish', "ProId='$ProId'", 'PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9');
		for($i=0; $i<10; ++$i){
			$PicPath=$row["PicPath_$i"];
			if(is_file($c['root_path'].$PicPath)){
				foreach($resize_ary as $v){
					$ext_name=file::get_ext_name($PicPath);
					file::del_file($PicPath.".{$v}.{$ext_name}");
				}
				file::del_file($PicPath);
				wish::remove_wish_images($PicPath);
			}
		}
		//删除
		db::delete('products_wish', $where);
		db::delete('products_wish_attribute', $where);
		manage::operation_log('删除Wish产品');
		ly200::e_json('', 1);
	}
	
	public static function wish_products_del_bat(){//批量删除Wish产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;
		!$g_group_proid && ly200::e_json('');
		$del_where='ProId in('.str_replace('-', ',', $g_group_proid).')';
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_all('products_wish', $del_where, 'PicPath_0, PicPath_1, PicPath_2, PicPath_3, PicPath_4, PicPath_5, PicPath_6, PicPath_7, PicPath_8, PicPath_9');
		foreach($row as $v){
			for($i=0; $i<10; ++$i){
				$PicPath=$v["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					foreach($resize_ary as $v2){
						$ext_name=file::get_ext_name($PicPath);
						file::del_file($PicPath.".{$v2}.{$ext_name}");
					}
					file::del_file($PicPath);
					wish::remove_wish_images($PicPath);
				}
			}
		}
		//删除
		db::delete('products_wish', $del_where);
		db::delete('products_wish_attribute', $del_where);
		manage::operation_log('批量删除Wish产品');
		ly200::e_json('', 1);
	}
	
	public static function copy_wish_to_products(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		
		$mLanguage=$c['manage']['web_lang'];
		$group_ids=str_replace('-', ',', $p_group_id);
		$CateId=(int)$p_CateId;
		!$group_ids && ly200::e_json('请选择要复制的产品!');
		!$CateId && ly200::e_json('请选择产品分类!');
		
		//产品属性
		$attribute_ary=$vid_data_ary=$new_vid_name_ary=array();
		$attribute_row=str::str_code(db::get_all('products_attribute', "CateId like '%|$CateId|%' and CartAttr=1", "AttrId, Name{$mLanguage}, CateId, ColorAttr"));
		foreach((array)$attribute_row as $v){
			$attribute_ary[$v["Name{$mLanguage}"]]=$v['AttrId'];
		}
		unset($attribute_row);
		$value_row=str::str_code(db::get_all('products_attribute_value', (count($attribute_ary)?'AttrId in('.implode(',', $attribute_ary).')':'1'), '*', $c['my_order'].'VId asc')); //属性选项
		foreach((array)$value_row as $v){
			$Value=htmlspecialchars_decode($v["Value{$mLanguage}"]);
			$vid_data_ary[$v['AttrId']][$Value]=$v['VId'];
			$new_vid_name_ary[$Value]=$v['VId'];
		}
		unset($value_row);
		
		$resize_ary=$c['manage']['resize_ary']['products'];
		$temp_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($temp_dir);
		
		$row=str::str_code(db::get_all('products_wish', "ProId in($group_ids)"));
		foreach($row as $val){
			$val=str::str_code(str::str_code($val, 'stripslashes'), 'addslashes');
			$data=array(
				'CateId'			=>	$CateId,
				'Source'			=>	'wish',
				'SourceId'			=>	$val['product_id'],
				"Name{$mLanguage}"	=>	$val['Name'],
				'SKU'				=>	$val['parent_sku'],
				'Number'			=>	$val['parent_sku'],
				'Price_0'			=>	$val['msrp'],
				'Price_1'			=>	$val['price'],
				'Stock'				=>	$val['inventory'],
				'SoldOut'			=>	1
			);
			$ProId=(int)db::get_value('products', "Source='wish' and SourceId!='' and SourceId='{$val['product_id']}'", 'ProId');
			//图片处理
			if(!$ProId){
				$j=0;
				for($i=0; $i<10; ++$i){
					$PicPath=$val["PicPath_$i"];
					if(is_file($c['root_path'].$PicPath)){
						$ext_name=file::get_ext_name($PicPath);
						$data['PicPath_'.$j++]=$temp=$temp_dir.str::rand_code().'.'.$ext_name;
						foreach($resize_ary as $size){
							$RePicPath=$PicPath.".{$size}.{$ext_name}";
							@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$size}.{$ext_name}", '/'));
						}
						@copy($c['root_path'].$PicPath, $c['root_path'].ltrim($temp, '/'));
					}
				}
			}
			//属性处理
			$attr_row=db::get_one('products_wish_attribute', "ProId='{$val[ProId]}'");
			if($attr_row){
				$attr_ary=$ext_ary=$color_ary=$size_ary=array();
				$attr_value=str::json_data(htmlspecialchars_decode($attr_row['AttributeValue']), 'decode');
				foreach($attr_value as $sku=>$v){
					$ext_ary[$v['color_name'].'|'.$v['size']]=array($v['price'], $v['inventory'], 0, $sku, 0);
					$color_ary[$v['color']]=$v['color_name'];
					$size_ary[$v['size']]=$v['size'];
				}
				//颜色
				$AddVIdNum=0;
				$create_ary=array();
				if($color_ary){
					if($attribute_ary['Color']){
						$id=$attribute_ary['Color'];
					}else{
						$id='ADD:Color';
						$create_ary['Attr'][$id]='Color';
					}
					foreach((array)$color_ary as $v){
						$v=trim($v);
						if($v){
							if($vid_data_ary[$id][$v]){
								$attr_ary[$id][]=$vid_data_ary[$id][$v];
							}else{
								$vid="ADDVId:$AddVIdNum";
								$attr_ary[$id][]=$vid;
								$create_ary['Value'][$id][$vid]=$v;
								++$AddVIdNum;
							}
						}
					}
				}
				//尺寸
				if($size_ary){
					if($attribute_ary['Size']){
						$id=$attribute_ary['Size'];
					}else{
						$id='ADD:Size';
						$create_ary['Attr'][$id]='Size';
					}
					foreach((array)$size_ary as $v){
						$v=trim($v);
						if($v){
							if($vid_data_ary[$id][$v]){
								$attr_ary[$id][]=$vid_data_ary[$id][$v];
							}else{
								$vid="ADDVId:$AddVIdNum";
								$attr_ary[$id][]=$vid;
								$create_ary['Value'][$id][$vid]=$v;
								++$AddVIdNum;
							}
						}
					}
				}
				//创建产品属性和选项
				if($create_ary){
					//新建产品属性
					$insert_attr_ary=$new_attr_id_ary=array();
					if($create_ary['Attr']){
						foreach((array)$create_ary['Attr'] as $k=>$v){
							$insert_attr_ary[$k]=array("Name{$mLanguage}"=>$v, 'CateId'=>"|{$CateId}|", 'CartAttr'=>1, 'Type'=>1);
						}
					}
					foreach((array)$insert_attr_ary as $k=>$v){
						db::insert('products_attribute', $v);
						$AttrId=db::get_insert_id();
						$new_attr_id_ary[$k]=$AttrId;//记录新属性
						$attribute_ary[$v["Name{$mLanguage}"]]=$AttrId;//补充数据记录
					}
					//新建产品属性选项
					$insert_value_ary=$new_vid_ary=array();
					if($create_ary['Value']){
						foreach((array)$create_ary['Value'] as $k=>$v){
							$k1=(stripos($k, 'ADD:')!==false?$new_attr_id_ary[$k]:$k);
							foreach((array)$v as $k2=>$v2){
								$insert_value_ary[$k2]=array('AttrId'=>$k1, "Value{$mLanguage}"=>$v2);
							}
						}
					}
					foreach((array)$insert_value_ary as $k=>$v){
						db::insert('products_attribute_value', $v);
						$VId=db::get_insert_id();
						$new_vid_ary[$k]=$VId;//记录新属性选项
						$new_vid_name_ary[$v["Value{$mLanguage}"]]=$VId;
						$vid_data_ary[$v['AttrId']][$v["Value{$mLanguage}"]]=$VId;//补充选项ID数据记录
					}
					//替换属性数据 ("ADD:"替换成"AttrId" "ADDVId:"替换成"VId")
					foreach($attr_ary as $k=>$v){
						$k1=0;
						if(stripos($k, 'ADD:')!==false){//新添加
							$k1=$new_attr_id_ary[$k];
						}
						if(is_array($v)){//选项
							foreach((array)$v as $k2=>$v2){
								if(stripos($v2, 'ADDVId:')!==false){//新添加
									$attr_ary[$k][$k2]=$new_vid_ary[$v2];
								}
							}
						}
						if($k1){//转移数据
							$attr_ary[$k1]=$attr_ary[$k];
							unset($attr_ary[$k]);
						}
					}
				}
				//替换属性价格数据
				foreach($ext_ary as $k=>$v){
					$k1=explode('|', trim($k, '|'));
					foreach($k1 as $k2=>$v2){
						$new_vid_name_ary[$v2] && $k1[$k2]=$new_vid_name_ary[$v2];
					}
					$k1='|'.implode('|', $k1).'|';
					$ext_ary[$k1]=$ext_ary[$k];
					if($k!=$k1) unset($ext_ary[$k]);//键不相同的时候，才删掉
				}
				//颜色图片
				$extra_ary=array();
				if(!$ProId){
					$ColorId=$attribute_ary['Color'];
					$color_value=str::json_data(htmlspecialchars_decode($attr_row['ColorValue']), 'decode');
					foreach((array)$color_value as $k=>$v){
						$VId=$vid_data_ary[$ColorId][$color_ary[$k]];
						$extra_ary[$VId]=$v;
					}
				}
			}
			//处理产品数据
			if($ProId){
				$w="ProId='{$ProId}'";
				$data['EditTime']=$c['time'];
				db::update('products', $w, $data);
				!db::get_row_count('products_seo', $w) && db::insert('products_seo', array('ProId'=>$ProId));
				if(!db::get_row_count('products_description', $w)){
					db::insert('products_description', array(
							'ProId'						=>	$ProId,
							"Description{$mLanguage}"	=>	addslashes($val['description'])
						)
					);
				}else{
					db::update('products_description', $w, array(
							"Description{$mLanguage}"	=>	addslashes($val['description'])
						)
					);
				}
			}else{
				$data['AccTime']=$c['time'];
				db::insert('products', $data);
				$ProId=db::get_insert_id();
				db::insert('products_seo', array('ProId'=>$ProId));
				db::insert('products_description', array(
						'ProId'						=>	$ProId,
						"Description{$mLanguage}"	=>	addslashes($val['description'])
					)
				);
			}
			//勾选属性处理
			$insert_ary=$update_ary=$value_ary=array();
			$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'", '*', 'SeleteId asc'));
			foreach($selected_row as $v){ $value_ary[]=$v['VId']; }
			unset($selected_row);
			if($attr_ary){
				foreach((array)$attr_ary as $k=>$v){
					foreach((array)$v as $v2){
						if(@in_array($v2, $value_ary)){//已有数据
							$update_ary[$v2]['IsUsed']=1;
						}else{//没有数据
							$insert_ary[]=array(
								'ProId'		=>	$ProId,
								'AttrId'	=>	$k,
								'VId'		=>	$v2,
								'IsUsed'	=>	1
							);
						}
					}
				}	
			}
			db::update('products_selected_attribute', "ProId='$ProId' and IsUsed<2 and (VId>0 or (AttrId=0 and VId=0 and OvId>0))", array('IsUsed'=>0));//先默认全部关闭
			if(count($update_ary)){
				foreach((array)$update_ary as $k=>$v){
					db::update('products_selected_attribute', "ProId='$ProId' and VId='$k'", $v);
				}
			}
			foreach((array)$insert_ary as $k=>$v){
				db::insert('products_selected_attribute', $v);
			}
			//组合数据
			$insert_ary=$update_ary=$combination_ary=$cid_ary=array();
			$combination_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'"));
			foreach((array)$combination_row as $v){ $combination_ary[$v['Combination']][$v['OvId']]=$v['CId']; }
			unset($combination_row);
			foreach((array)$ext_ary as $k=>$v){
				if($combination_ary[$k][1]){//已存在
					$cid_ary[]=$combination_ary[$k][1];
					$update_ary[$combination_ary[$k][1]]=array(
						'SKU'			=>	$v[3],
						'IsIncrease'	=>	$v[4],
						'Price'			=>	$v[0],
						'Stock'			=>	$v[1],
						'Weight'		=>	$v[2]
					);
				}else{ //不存在
					$insert_ary[]=array(
						'ProId'			=>	$ProId,
						'Combination'	=>	$k,
						'SKU'			=>	$v[3],
						'IsIncrease'	=>	$v[4],
						'Price'			=>	$v[0],
						'Stock'			=>	$v[1],
						'Weight'		=>	$v[2]
					);
				}
			}
			if(count($update_ary)){
				foreach((array)$update_ary as $k=>$v){
					db::update('products_selected_attribute_combination', "CId='$k'", $v);
				}
			}
			if(count($cid_ary)){//清空多余没用的选项数据
				$cid_str=implode(',', $cid_ary);
				db::delete('products_selected_attribute_combination', "ProId='$ProId' and CId not in($cid_str)");
			}else{//清空已经取消勾选的选项数据
				db::delete('products_selected_attribute_combination', "ProId='$ProId'");
			}
			if(count($insert_ary)){
				foreach((array)$insert_ary as $k=>$v){
					db::insert('products_selected_attribute_combination', $v);
				}
			}
			//图片数据
			if($extra_ary){
				foreach((array)$extra_ary as $k=>$v){
					if(!$v) continue;
					$ImgPath=wish::save_wish_images($v);
					if($ImgPath){
						$ext_name=file::get_ext_name($ImgPath);
						foreach($resize_ary as $v2){
							if(!is_file($c['root_path'].$ImgPath.".{$v2}.{$ext_name}")){
								$size_w_h=explode('x', $v2);
								$resize_path=img::resize($ImgPath, $size_w_h[0], $size_w_h[1]);
							}
						}
						if(!is_file($c['root_path'].$ImgPath.".default.{$ext_name}")){
							@copy($c['root_path'].$ImgPath, $c['root_path'].$ImgPath.".default.{$ext_name}");
						}
						$c_data=array('PicPath_0'=>$ImgPath);
						if(db::get_one('products_color', "ProId='$ProId' and VId='$k'")){
							db::update('products_color', "ProId='$ProId' and VId='$k'", $c_data);
						}else{
							$c_data['ProId']=$ProId;
							$c_data['VId']=$k;
							db::insert('products_color', $c_data);
						}
					}
				}
			}
		}
		manage::operation_log('批量复制Wish产品到本地');
		ly200::e_json('复制完成!', 1);
	}
	/*****************************************
	Wish同步部分(end)
	*****************************************/
	
	/*****************************************
	Shopify同步部分(start)
	*****************************************/
	public static function change_shopify_authorization_account(){//切换当前Shopify账号
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AccountId=(int)$p_AccountId;
		!$AccountId && ly200::e_json('请选择切换的账号！');
		$AccountId==$_SESSION['Manage']['Shopify']['AuthorizationId'] && ly200::e_json('');
		
		shopify::set_default_authorization($AccountId);
		ly200::e_json('', 1);
	}
	
	public static function shopify_products_sync(){
		//同步产品
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		//$row	= db::get_one('authorization', 'Platform="shopify"');
		//$token	= str::json_data($row['Token'], 'decode');
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'sync_products',
			'ApiName'		=>	'shopify',
			'Number'		=>	$c['Number'],
			'Account'		=>	$_SESSION['Manage']['Shopify']['Account'], //$row['Account'],
			'notify_url'	=>	ly200::get_domain().'/gateway/',
			'Start'			=>	(int)$p_Start,
			'timestamp'		=>	$c['time']
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
		if($result['ret']==1){
			$TaskId=$result['msg']['TaskId'];
			$taskData=array(
				'Platform'	=>	'shopify',
				'TaskId'	=>	$TaskId,
				'AccTime'	=>	time()
			);
			!db::get_row_count('products_sync_task', "Platform='shopify' and TaskId='{$TaskId}'") && db::insert('products_sync_task', $taskData);
		}
		ly200::e_json($TaskId?array('TaskId'=>$TaskId):$result['msg'], (int)$result['ret']);
	}
	
	public static function shopify_products_sync_status(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$TaskId=(int)$p_TaskId;
		!$TaskId && ly200::e_json('');
		$ret=1;
		$row=db::get_one('products_sync_task', "Platform='shopify' and TaskId='$TaskId'");
		$data=array('Percent'=>$row['CompletionRate']);
		$row['TaskStatus']==2 && $ret=3;//产品数据已同步完成
		if($row['TaskStatus']==2 && $row['ReturnData']){
			$ReturnData=str::json_data($row['ReturnData'], 'decode');
			$ret=2;
			$data['Start']=(int)$ReturnData['PageNext'];
			$data['ProCount']=(int)$ReturnData['ProCount'];
		}
		ly200::e_json($data, $ret);
	}
	
	public static function sync_shopify_products_edit(){//编辑Shopify产品
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		//初始化
		$ProId		= (int)$p_ProId;
		$resize_ary	= $c['manage']['resize_ary']['products'];
		$save_dir	= $c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($save_dir);
		//图片处理
		$ImgPath=array();
		foreach((array)$p_PicPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			if(!is_file($c['root_path'].$v)) continue;
			$ImgPath[]=file::photo_tmp_upload($v, $save_dir, $resize_ary);
		}
		if(!count($ImgPath)) ly200::e_json(manage::get_language('products.products.pic_tips'));
		foreach((array)$ImgPath as $k=>$v){
			$ext_name=file::get_ext_name($v);
			foreach((array)$resize_ary as $v2){
				if(!is_file($c['root_path'].$v.".{$v2}.{$ext_name}")){
					$size_w_h=explode('x', $v2);
					$resize_path=img::resize($v, $size_w_h[0], $size_w_h[1]);
				}
			}
			if(!is_file($c['root_path'].$v.".default.{$ext_name}")){
				@copy($c['root_path'].$v, $c['root_path'].$v.".default.{$ext_name}");
			}
		}
		//数据
		$data=array(
			'title'				=>	addslashes(stripslashes($p_title)),
			'body_html'			=>	addslashes(stripslashes($p_body_html)),
            'product_type'		=>	addslashes(stripslashes($p_product_type)),
			'sku'				=>	$p_sku,
			'compare_at_price'	=>	(float)$p_compare_at_price,
			'price'				=>	(float)$p_price,
			'weight'			=>	(float)$p_weight,
			'inventory_quantity'=>	(int)$p_inventory_quantity,
			'PicPath_0'			=>	$ImgPath[0],
			'PicPath_1'			=>	$ImgPath[1],
			'PicPath_2'			=>	$ImgPath[2],
			'PicPath_3'			=>	$ImgPath[3],
			'PicPath_4'			=>	$ImgPath[4],
			'PicPath_5'			=>	$ImgPath[5],
			'PicPath_6'			=>	$ImgPath[6],
			'PicPath_7'			=>	$ImgPath[7],
			'PicPath_8'			=>	$ImgPath[8],
			'PicPath_9'			=>	$ImgPath[9],
            'PicPath_10'		=>	$ImgPath[10],
            'PicPath_11'		=>	$ImgPath[11],
            'PicPath_12'		=>	$ImgPath[12],
            'PicPath_13'		=>	$ImgPath[13],
            'PicPath_14'		=>	$ImgPath[14],
		);
		if($ProId){
			db::update('products_shopify', "ProId='$ProId'", $data);
			manage::operation_log('修改Shopify产品');
		}
		ly200::e_json(array('jump'=>$p_back_action?$p_back_action:"?m=products&a=sync&d=shopify"), 1);
	}
	
	public static function shopify_products_img_del(){//删除单个产品图片
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$PicPath=$g_Path;
		$Index=(int)$g_Index;
		$resize_ary=$c['manage']['resize_ary']['products'];
		if(is_file($c['root_path'].$PicPath)){
			foreach((array)$resize_ary as $v){
				$ext_name=file::get_ext_name($PicPath);
				file::del_file($PicPath.".{$v}.{$ext_name}");
			}
			file::del_file($PicPath);
			shopify::remove_shopify_images($PicPath);
		}
		manage::operation_log('删除Shopify产品图片');
		ly200::e_json(array($Index), 1);
	}
	public static function shipping_products_used(){
	    global $c;
	    @extract($_POST, EXTR_PREFIX_ALL, 'p');
	    $p_SId=(int)$p_SId;
	    $p_IsUsed=(int)$p_IsUsed;
	    $p_Pid=(int)$p_Pid;
	    if ($p_IsUsed){
	        $exist=db::get_all('product_shipping',"products_id='".$p_Pid."' and shipping_id='".$p_SId."'");
	        if ($exist&&(count($exist)>0))
	        {
	          db::delete('product_shipping',"products_id='".$p_Pid."' and shipping_id='".$p_SId."'");
	        }
	        db::insert('product_shipping',array('products_id'=>$p_Pid,'shipping_id'=>$p_SId));
	    }else 
	        db::delete('product_shipping',"products_id='".$p_Pid."' and shipping_id='".$p_SId."'");
// 	    manage::operation_log(($p_IsUsed ? '开启' : '关闭').'快递公司-'.$p_SId);
	    ly200::e_json('', 1);
	}
	public static function shopify_products_del(){//删除Shopify产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;
		$ProId=(int)$g_ProId;
		$where="ProId='$ProId'";
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_one('products_shopify', "ProId='$ProId'", 'saved_images, '.$c['prod_image']['sql_field']);
		for($i=0; $i<$c['prod_image']['count']; ++$i){
			$PicPath=$row["PicPath_$i"];
			if(is_file($c['root_path'].$PicPath)){
				foreach($resize_ary as $v){
					$ext_name=file::get_ext_name($PicPath);
					file::del_file($PicPath.".{$v}.{$ext_name}");
				}
				file::del_file($PicPath);
				shopify::remove_shopify_images($PicPath);
			}
		}
		$saved_images_data=str::json_data(htmlspecialchars_decode($row['saved_images']), 'decode');
		foreach($saved_images_data as $Pic){
			if(is_file($c['root_path'].$Pic)){
				foreach($resize_ary as $v){
					$ext_name=file::get_ext_name($Pic);
					file::del_file($Pic.".{$v}.{$ext_name}");
				}
				file::del_file($Pic);
				shopify::remove_shopify_images($Pic);
			}
		}
		//删除
		db::delete('products_shopify', $where);
		db::delete('products_shopify_attribute', $where);
		manage::operation_log('删除Shopify产品');
		ly200::e_json('', 1);
	}
	
	public static function shopify_products_del_bat(){//批量删除Shopify产品
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if($c['FunVersion']<2) return;
		!$g_group_proid && ly200::e_json('');
		$del_where='ProId in('.str_replace('-', ',', $g_group_proid).')';
		//删除产品图片
		$resize_ary=$c['manage']['resize_ary']['products'];
		$row=db::get_all('products_shopify', $del_where, 'saved_images, '.$c['prod_image']['sql_field']);
		foreach($row as $v){
			for($i=0; $i<$c['prod_image']['count']; ++$i){
				$PicPath=$v["PicPath_$i"];
				if(is_file($c['root_path'].$PicPath)){
					foreach($resize_ary as $v2){
						$ext_name=file::get_ext_name($PicPath);
						file::del_file($PicPath.".{$v2}.{$ext_name}");
					}
					file::del_file($PicPath);
					shopify::remove_shopify_images($PicPath);
				}
			}
			$saved_images_data=str::json_data(htmlspecialchars_decode($v['saved_images']), 'decode');
			foreach($saved_images_data as $Pic){
				if(is_file($c['root_path'].$Pic)){
					foreach($resize_ary as $v2){
						$ext_name=file::get_ext_name($Pic);
						file::del_file($Pic.".{$v2}.{$ext_name}");
					}
					file::del_file($Pic);
					shopify::remove_shopify_images($Pic);
				}
			}
		}
		//删除
		db::delete('products_shopify', $del_where);
		db::delete('products_shopify_attribute', $del_where);
		manage::operation_log('批量删除Shopify产品');
		ly200::e_json('', 1);
	}
	
	public static function copy_shopify_to_products(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($c['FunVersion']<2) return;
		//初始化
		$mLanguage	= $c['manage']['web_lang'];
		$group_ids	= str_replace('-', ',', $p_group_id);
		$CateId		= (int)$p_CateId;
		$resize_ary	= $c['manage']['resize_ary']['products'];
		$temp_dir	= $c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
		file::mk_dir($temp_dir);
		!$group_ids && ly200::e_json('请选择要复制的产品!');
		!$CateId && ly200::e_json('请选择产品分类!');
        //扩展分类
		if($p_ExtCateId){
            $ExtCateId=substr($p_ExtCateId, 2);
            $ExtCateId=explode(',', $ExtCateId);
			$ExtCateId=array_unique($ExtCateId);
			if($zero=array_search(0, $ExtCateId)){
				unset($ExtCateId[$zero]);
			}
			$ExtCateId=','.implode(',',$ExtCateId).',';
		}
		//整合属性数据
		$attr_data_ary=$vid_data_ary=array();
		$attr_row=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%'", "AttrId, Name{$mLanguage}, ColorAttr", 'AttrId desc'));
		if($attr_row){
			foreach((array)$attr_row as $v){
				$attr_data_ary[$v["Name{$mLanguage}"]]=$v['AttrId'];
			}
			$attribute_where=(count($attr_data_ary)>0?'AttrId in('.implode(',', $attr_data_ary).')':'AttrId="-1"');
			$attr_value_row=str::str_code(db::get_all('products_attribute_value', $attribute_where, '*', $c['my_order'].'VId asc'));//属性选项
			foreach((array)$attr_value_row as $v){
				$Value=htmlspecialchars_decode($v["Value{$mLanguage}"]);
				$vid_data_ary[$v['AttrId']][$Value]=$v['VId'];
			}
		}
		unset($attr_row, $attr_value_row);
		//开始
		$row=str::str_code(db::get_all('products_shopify', "ProId in($group_ids)"));
		foreach($row as $val){
			//初始化
			$AddNum=$AddVIdNum=$IsHaveAttr=0;
			$create_ary=$add_ary=$min_price_ary=$attr_ary=$ext_ary=$extra_ary=array();
			$position_ary=array();
			$val=str::str_code(str::str_code($val, 'stripslashes'), 'addslashes');
			$data=array(
				'CateId'			=>	$CateId,
                'ExtCateId'         =>  $ExtCateId,
				'Source'			=>	'shopify',
				'SourceId'			=>	$val['product_id'],
				"Name{$mLanguage}"	=>	$val['title'],
				'SKU'				=>	$val['sku'],
				'Number'			=>	$val['sku'],
				'Price_0'			=>	$val['compare_at_price'],
				'Price_1'			=>	$val['price'],
				'Weight'			=>	$val['weight'],
				'Stock'				=>	abs($val['inventory_quantity']),
                'SoldStatus'        =>  ($val['inventory_policy']=='continue'?1:0),
				'SoldOut'			=>	1
			);
			$ProId=(int)db::get_value('products', "Source='shopify' and SourceId!='' and SourceId='{$val['product_id']}'", 'ProId');
			//图片处理 
			$saved_images_data=str::json_data(htmlspecialchars_decode($val['saved_images']), 'decode');
			if(!$ProId){
				$j=0;
				for($i=0; $i<$c['prod_image']['count']; ++$i){
					$PicPath=$val["PicPath_$i"];
					if(is_file($c['root_path'].$PicPath)){
						$ext_name=file::get_ext_name($PicPath);
						$data['PicPath_'.$j++]=$temp=$temp_dir.str::rand_code().'.'.$ext_name;
						foreach($resize_ary as $size){
							$RePicPath=$PicPath.".{$size}.{$ext_name}";
							@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$size}.{$ext_name}", '/'));
						}
						@copy($c['root_path'].$PicPath, $c['root_path'].ltrim($temp, '/'));
					}
				}
			}
			//属性处理
			$attr_row=db::get_one('products_shopify_attribute', "ProId='{$val[ProId]}'");
			if($attr_row){
				$options_data=str::json_data(htmlspecialchars_decode($attr_row['Options']), 'decode');
				$variants_data=str::json_data(htmlspecialchars_decode($attr_row['Variants']), 'decode');
				//规格属性参数
				foreach((array)$options_data as $v){
					if($v['name']!='Title'){
						$IsHaveAttr=1;//标记有属性
						$id=$attr_data_ary[$v['name']];
						if(!$id){//属性不存在
							$id="ADD:$AddNum";
							$create_ary['Cart'][$id]=$v['name'];
							$add_ary[$v['name']][0]=$id;
							++$AddNum;
						}
						$arr=array();
						foreach((array)$v['values'] as $v2){
							$v2=trim($v2);
							if($v2){
								if($vid_data_ary[$id][$v2]){//选项存在
									$arr[]=$vid_data_ary[$id][$v2];
								}else{//选项不存在
									$vid="ADDVId:$AddVIdNum";
									$arr[]=$vid;
									$create_ary['Value'][$id][$vid]=$v2;
									$add_ary[$v['name']][$v2]=$vid;
									++$AddVIdNum;
								}
							}
						}
						$attr_ary[$id]=$arr;
						$position_ary[$v['position']]=$v['name'];
					}
				}
				//组合属性
				foreach((array)$variants_data as $v){
					$value=array();
					$OvId=1;
					$FirstVId=0;
					$_VId=0;
					for($i=1; $i<=3; ++$i){
						$name=trim($v["option{$i}"]);
						if(!$name) continue;
						if($vid_data_ary[$attr_data_ary[$position_ary[$i]]][$name]){//数据存在
							$_VId=$vid_data_ary[$attr_data_ary[$position_ary[$i]]][$name];
						}else{//数据不存在
							$_VId=$add_ary[$position_ary[$i]][$name];
						}
						$value[]=$_VId;
						$i==1 && $FirstVId=$_VId;
					}
					sort($value);
					$ext_ary['|'.implode('|', $value).'|'][$OvId]=array($v['price'], $v['inventory'], $v['weight'], $v['sku'], 0);
					if($v['image_id']){//带有颜色图片
						$extra_ary[$FirstVId][]=$saved_images_data[$v['image_id']];
					}
				}
			}
			if($IsHaveAttr==1) $data['IsCombination']=1;
			//创建产品属性和选项
			if($create_ary){
				//新建产品属性
				$insert_attr_ary=$new_attr_id_ary=array();
				foreach((array)$create_ary as $k=>$v){
					if($k=='Cart'){//规格属性
						foreach((array)$v as $k2=>$v2){
							$insert_attr_ary[$k2]=array("Name{$mLanguage}"=>$v2, 'CateId'=>"|{$CateId}|", 'CartAttr'=>1, 'Type'=>1);
						}
					}
				}
				foreach((array)$insert_attr_ary as $k=>$v){
					db::insert('products_attribute', $v);
					$AttrId=db::get_insert_id();
					$new_attr_id_ary[$k]=$AttrId;//记录新属性
					$attr_data_ary[$v["Name{$mLanguage}"]]=$AttrId;//补充数据记录
				}
				//新建产品属性选项
				$insert_value_ary=$new_vid_ary=array();
				if($create_ary['Value']){
					foreach((array)$create_ary['Value'] as $k=>$v){
						$k1=(stripos($k, 'ADD:')!==false?$new_attr_id_ary[$k]:$k);
						foreach((array)$v as $k2=>$v2){
							$insert_value_ary[$k2]=array('AttrId'=>$k1, "Value{$mLanguage}"=>$v2);
						}
					}
				}
				foreach((array)$insert_value_ary as $k=>$v){
					db::insert('products_attribute_value', $v);
					$VId=db::get_insert_id();
					$new_vid_ary[$k]=$VId;//记录新属性选项
					$vid_data_ary[$v['AttrId']][$v["Value{$mLanguage}"]]=$VId;//补充选项ID数据记录
				}
				//替换属性数据 ("ADD:"替换成"AttrId" "ADDVId:"替换成"VId")
				foreach((array)$attr_ary as $k=>$v){
					$k1=0;
					if(stripos($k, 'ADD:')!==false){//新添加
						$k1=$new_attr_id_ary[$k];
					}
					if(is_array($v)){//选项
						foreach((array)$v as $k2=>$v2){
							if(stripos($v2, 'ADDVId:')!==false){//新添加
								$attr_ary[$k][$k2]=$new_vid_ary[$v2];
							}
						}
					}
					if($k1){//转移数据
						$attr_ary[$k1]=$attr_ary[$k];
						unset($attr_ary[$k]);
					}
				}
				//替换属性价格数据 ("ADDVId:"替换成"VId")
				foreach((array)$ext_ary as $k=>$v){
					$k1=explode('|', trim($k, '|'));
					foreach((array)$k1 as $k2=>$v2){
						$new_vid_ary[$v2] && $k1[$k2]=$new_vid_ary[$v2];
					}
					$k1='|'.implode('|', $k1).'|';
					$ext_ary[$k1]=$ext_ary[$k];
					if($k!=$k1) unset($ext_ary[$k]);//键不相同的时候，才删掉
				}
				//替换颜色图片数据 ("ADDVId:"替换成"VId")
				foreach((array)$extra_ary as $k=>$v){
					$new_vid_ary[$k] && $k1=$new_vid_ary[$k];
					$extra_ary[$k1]=$extra_ary[$k];
				}
			}
			//处理产品数据
			if($ProId){
				$w="ProId='{$ProId}'";
				$data['EditTime']=$c['time'];
				db::update('products', $w, $data);
				!db::get_row_count('products_seo', $w) && db::insert('products_seo', array('ProId'=>$ProId));
				if(!db::get_row_count('products_description', $w)){
					db::insert('products_description', array(
							'ProId'						=>	$ProId,
							"Description{$mLanguage}"	=>	addslashes(htmlspecialchars_decode($val['body_html']))
						)
					);
				}else{
					db::update('products_description', $w, array(
							"Description{$mLanguage}"	=>	addslashes(htmlspecialchars_decode($val['body_html']))
						)
					);
				}
			}else{
				$data['AccTime']=$c['time'];
				db::insert('products', $data);
				$ProId=db::get_insert_id();
				db::insert('products_seo', array('ProId'=>$ProId));
				db::insert('products_description', array(
						'ProId'						=>	$ProId,
						"Description{$mLanguage}"	=>	addslashes(htmlspecialchars_decode($val['body_html']))
					)
				);
			}
			//属性选项勾选
			$insert_ary=$update_ary=$selected_attr_ary=$selected_value_ary=array();
			$attr_row=str::str_code(db::get_all('products_selected_attr', "ProId='$ProId'", '*', 'SId asc'));
			foreach((array)$attr_row as $v){
				$selected_attr_ary[$v['AttrId']]=$v['SId'];
			}
			$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'", '*', 'SeleteId asc'));
			foreach((array)$selected_row as $v){
				$selected_value_ary[$v['VId']]=$v['SeleteId'];
			}
			unset($attr_row, $selected_row);
			$i=1;
			foreach((array)$attr_ary as $k=>$v){//$k:AttrId $v:VId
				//products_selected_attribute
				$IsColor=0;
				foreach((array)$v as $v2){
					if($selected_value_ary[$v2]){//已有数据
						$update_ary['value'][$selected_value_ary[$v2]]['IsUsed']=1;
						$update_ary['value'][$selected_value_ary[$v2]]['MyOrder']=$i;
					}else{//没有数据
						$insert_ary['value'][]=array(
							'ProId'		=>	$ProId,
							'AttrId'	=>	$k,
							'VId'		=>	$v2,
							'OvId'		=>	1,
							'IsUsed'	=>	1,
							'MyOrder'	=>	$i
						);
					}
					if($extra_ary[$v2]){//带有颜色图片
						$IsColor+=1;
					}
					++$i;
				}
				//products_selected_attr
				if($selected_attr_ary[$k]){//已有数据
					$update_ary['attr'][$selected_attr_ary[$k]]['IsUsed']=1;
					$update_ary['attr'][$selected_attr_ary[$k]]['IsColor']=($IsColor>0?1:0);
				}else{//没有数据
					$insert_ary['attr'][]=array(
						'ProId'		=>	$ProId,
						'AttrId'	=>	$k,
						'IsUsed'	=>	1,
						'IsColor'	=>	($IsColor>0?1:0)
					);
				}
			}
			db::update('products_selected_attr', "ProId='$ProId'", array('IsUsed'=>0));//先默认全部关闭
			db::update('products_selected_attribute', "ProId='$ProId' and IsUsed<2 and (VId>0 or (AttrId=0 and VId=0 and OvId>0))", array('IsUsed'=>0));//先默认全部关闭
			if(count($update_ary)){
				foreach((array)$update_ary as $k=>$v){
					foreach($v as $k2=>$v2){
						if($k=='attr'){
							$table='products_selected_attr';
							$w="ProId='$ProId' and SId='{$k2}'";
						}else{
							$table='products_selected_attribute';
							$w="ProId='$ProId' and SeleteId='{$k2}'";
						}
						db::update($table, $w, $v2);
					}
				}
			}
			if(count($insert_ary)){
				foreach((array)$insert_ary as $k=>$v){
					foreach($v as $v2){
						if($k=='attr'){
							$table='products_selected_attr';
						}else{
							$table='products_selected_attribute';
						}
						db::insert($table, $v2);
					}
				}
			}
			//组合数据
			$insert_ary=$update_ary=$combination_ary=$cid_ary=array();
			$combination_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId='$ProId'"));
			foreach((array)$combination_row as $v){
				$combination_ary[$v['Combination']][$v['OvId']]=$v['CId'];
			}
			unset($attr_row, $selected_row);
			foreach((array)$ext_ary as $Combination=>$v){
				foreach((array)$v as $OvId=>$v2){
					$CId=$combination_ary[$Combination][$OvId];
					if($CId){//已存在
						$cid_ary[]=$CId;
						$update_ary[$CId]=array(
							'Price'			=>	$v2[0],
							'Stock'			=>	abs($v2[1]),
							'Weight'		=>	$v2[2],
							'SKU'			=>	$v2[3],
							'IsIncrease'	=>	$v2[4]
						);
					}else{ //不存在
						$insert_ary[]=array(
							'ProId'			=>	$ProId,
							'Combination'	=>	$Combination,
							'OvId'			=>	$OvId,
							'Price'			=>	$v2[0],
							'Stock'			=>	abs($v2[1]),
							'Weight'		=>	$v2[2],
							'SKU'			=>	$v2[3],
							'IsIncrease'	=>	$v2[4]
						);
					}
				}
			}
			if(count($update_ary)){
				foreach((array)$update_ary as $k=>$v){
					db::update('products_selected_attribute_combination', "CId='$k'", $v);
				}
			}
			if(count($cid_ary)){//清空多余没用的选项数据
				$cid_str=implode(',', $cid_ary);
				db::delete('products_selected_attribute_combination', "ProId='$ProId' and CId not in($cid_str)");
			}else{//清空已经取消勾选的选项数据
				db::delete('products_selected_attribute_combination', "ProId='$ProId'");
			}
			if(count($insert_ary)){
				foreach((array)$insert_ary as $k=>$v){
					db::insert('products_selected_attribute_combination', $v);
				}
			}
			//图片数据
			if($extra_ary){
				foreach((array)$extra_ary as $VId=>$v){
                    $v = array_unique($v); //去掉重复值
					foreach((array)$v as $k2=>$ImgPath){
						if($ImgPath){
							$ext_name=file::get_ext_name($ImgPath);
							$temp=$temp_dir.str::rand_code().'.'.$ext_name;
							foreach($resize_ary as $size){
								$RePicPath=$ImgPath.".{$size}.{$ext_name}";
								@copy($c['root_path'].$RePicPath, $c['root_path'].ltrim($temp.".{$size}.{$ext_name}", '/'));
							}
							@copy($c['root_path'].$ImgPath, $c['root_path'].ltrim($temp, '/'));
							$c_data=array("PicPath_$k2"=>$temp);
							if(db::get_one('products_color', "ProId='$ProId' and VId='$VId'")){
								db::update('products_color', "ProId='$ProId' and VId='$VId'", $c_data);
							}else{
								$c_data['ProId']=$ProId;
								$c_data['VId']=$VId;
								db::insert('products_color', $c_data);
							}
						}
					}
				}
			}
		}
		manage::operation_log('批量复制Shopify产品到本地');
		ly200::e_json('复制完成!', 1);
	}
	/*****************************************
	Shopify同步部分(end)
	*****************************************/
	/***********************************************************数据同步(end)*******************************************************************/

		/*******************************平台授权(start)*****************************/
	public static function set_open_api(){
		global $c;
		$appkey=str::rand_code(20);
		
		if(!db::get_row_count('config', "GroupId='API' and Variable='AppKey'", 'CId')){
			db::insert('config', array(
					'GroupId'	=>	'API',
					'Variable'	=>	'AppKey',
					'Value'		=>	$appkey
				)
			);
			$return_data['jump']=1;
		}else{
			db::update('config', "GroupId='API' and Variable='AppKey'", array('Value'=>$appkey));
			$return_data['appkey']=$appkey;
		}
		
		ly200::e_json($return_data, 1);
	}

	public static function del_open_api(){
		global $c;
		
		db::delete('config', "GroupId='API' and Variable='AppKey'");
		ly200::e_json('', 1);
	}

	public static function authhz_url(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		
		if($p_d=='aliexpress'){
			$notify_url=@base64_encode(ly200::get_domain(1,1).'/gateway/');
			//$return_url=@base64_encode(ly200::get_domain().'/manage/?m=set&a=authorization&d=aliexpress&callback=1&iframe=1');
			$return_url=@base64_encode(ly200::get_domain(1,1).'/manage/?m=products&a=sync&d=aliexpress&callback=1');
			$query_string="notify_url={$notify_url}&return_url={$return_url}";
			$p_Name && $query_string.="&name={$p_Name}";
			$p_Account && $query_string.="&account={$p_Account}";
			
			$data=array(
				'ApiKey'		=>	'ueeshop_sync',
				'Number'		=>	$c['Number'],
				'ApiName'		=>	'aliexpress',
				'Action'		=>	'authhz_url',
				'query_string'	=>	$query_string,
				'timestamp'		=>	$c['time']
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
			if($result['ret']==1){
				ly200::e_json($result['msg'], 1);
			}
		}
		
		ly200::e_json('');
	}

	public static function authorization_add(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		
		if($p_d=='amazon'){
			//亚马逊
			if($c['FunVersion']<2) return;
			db::get_row_count('authorization', "Platform='amazon' and Name='$p_Name'") && ly200::e_json("店铺名({$p_Name})已存在，请重新设置店铺名！");
			db::get_row_count('authorization', "Platform='amazon' and Account='$p_MerchantId'") && ly200::e_json("Merchant ID({$p_MerchantId})已授权，不可重复授权！");
			
			$Token=array(
				'MerchantId'	=>	$p_MerchantId,
				'AWSAccessKeyId'=>	$p_AWSAccessKeyId,
				'SecretKey'		=>	$p_SecretKey,
				'MarkectPlace'	=>	$p_MarkectPlace
			);
			$data=@array_merge($Token, array(
					'ApiKey'		=>	'ueeshop_sync',
					'Number'		=>	$c['Number'],
					'ApiName'		=>	'amazon',
					'Action'		=>	'authorization',
					'timestamp'		=>	$c['time']
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
			if($result['ret']==1){//授权成功
				db::insert('authorization', array(
						'Platform'	=>	'amazon',
						'Name'		=>	$p_Name,
						'Account'	=>	$p_MerchantId,
						'Token'		=>	str::json_data($Token),
						'Data'		=>	@implode(',', $result['msg']),
						'AccTime'	=>	$c['time']
					)
				);

				ly200::e_json('', 1);
			}else ly200::e_json('授权失败，请检查输入信息是否正确!');
		}elseif($p_d=='wish'){
			//Wish
            if($c['FunVersion']<2) return;
			db::get_row_count('authorization', "Platform='wish' and Name='$p_Name'") && ly200::e_json("店铺名({$p_Name})已存在，请重新设置店铺名！");
			db::get_row_count('authorization', "Platform='wish' and Account='$p_ClientId'") && ly200::e_json("username({$p_ClientId})已授权，不可重复授权！");
            
			$TokenAry=array(
				'client_id'		=>	$p_ClientId,
				'client_secret'	=>	$p_ClientSecret,
				'code'			=>	'',
				'redirect_uri'	=>	ly200::get_domain().'/plugins/api/wish/'
			);
			$Token=addslashes(str::json_data(str::str_code($TokenAry, 'stripslashes')));
			$data=array(
				'ApiKey'		=>	'ueeshop_sync',
				'Number'		=>	$c['Number'],
				'ApiName'		=>	'wish',
				'Action'		=>	'authhz_url',
				'ClientId'		=>	$p_ClientId,
				'timestamp'		=>	$c['time']
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
            if($result['ret']==1){//授权成功
				db::insert('authorization', array(
						'Platform'	=>	'wish',
						'Name'		=>	$p_Name,
						'Account'	=>	$p_ClientId,
						'Token'		=>	$Token,
						'AccTime'	=>	$c['time']
				));
				ly200::e_json($result['msg'], 1);
			}else ly200::e_json('授权失败，请检查输入信息是否正确!');
		}elseif($p_d=='shopify'){
			//Shopify
			if($c['FunVersion']<2) return;
			db::get_row_count('authorization', "Platform='shopify' and Name='$p_Name'") && ly200::e_json("店铺名({$p_Name})已存在，请重新设置店铺名！");
			db::get_row_count('authorization', "Platform='shopify' and Account='$p_client_username'") && ly200::e_json("username({$p_client_username})已授权，不可重复授权！");
			
			$Token=array(
				'UserName'	=>	$p_client_username,
				'Password'	=>	$p_client_password,
				'Shop'		=>	$p_client_shop
			);
			$data=@array_merge($Token, array(
					'ApiKey'		=>	'ueeshop_sync',
					'Number'		=>	$c['Number'],
					'ApiName'		=>	'shopify',
					'Action'		=>	'authorization',
					'timestamp'		=>	$c['time']
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
			if($result['ret']==1){//授权成功
				db::insert('authorization', array(
						'Platform'	=>	'shopify',
						'Name'		=>	$p_Name,
						'Account'	=>	$p_client_username,
						'Token'		=>	str::json_data($Token),
						'AccTime'	=>	$c['time']
					)
				);
				ly200::e_json('', 1);
			}else ly200::e_json('授权失败，请检查输入信息是否正确!');
		}
		ly200::e_json('', 1);
	}

	public static function authorization_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_AId=(int)$p_AId;
		(!$p_AId || !$p_Name || !$p_d) && ly200::e_json('数据缺失，请重试！');
		db::get_row_count('authorization', "Platform='{$p_d}' and Name='{$p_Name}' and AId!='{$p_AId}'") && ly200::e_json('数据缺失，请重试！');
		$return=array('AId'=>$p_AId, 'Name'=>$p_Name, 'token'=>$data['Token']);		
		
		$data=array('Name'=>addslashes(stripslashes($p_Name)));
		if($p_d=='amazon'){
			if($c['FunVersion']<2) return;
			$row=db::get_one('authorization', "Platform='{$p_d}' and AId='{$p_AId}'");
			$MarkectPlaceAry=@explode(',', $row['Data']);
			!in_array($p_MarkectPlace, $MarkectPlaceAry) && ly200::e_json('开户站选择不正确！');
			$token=str::json_data($row['Token'], 'decode');
			if($p_MarkectPlace && $p_MarkectPlace!=$token['MarkectPlace']){
				$token['MarkectPlace']=$p_MarkectPlace;
				$data['Token']=str::json_data($token);
				$return['token']=str::str_code($data['Token']);
			}
		}
		if($p_d=='wish'){
			$row=db::get_one('authorization', "Platform='{$p_d}' and AId='{$p_AId}'");
			$return['token']=$row['Token'];
		}
		if($p_d=='shopify'){
			$row=db::get_one('authorization', "Platform='{$p_d}' and AId='{$p_AId}'");
			$return['token']=$row['Token'];
		}
		db::update('authorization', "AId='{$p_AId}'", $data);
		
		ly200::e_json($return, 1);
	}
	
	public static function authorization_del(){//删除授权
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_AId=(int)$g_AId;
		!$g_AId && ly200::e_json('删除的对象不存在！');
		
		$config=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Number'		=>	$c['Number'],
			'Action'		=>	'authorization_del',
			'timestamp'		=>	$c['time']
		);
		
		$row=db::get_one('authorization', "AId='{$g_AId}'", '*');
		if($row['Platform']=='aliexpress'){
			//速卖通
			($row['Account']==$_SESSION['Manage']['Aliexpress']['Token']['Account']) && aliexpress::set_default_authorization();
			$data=@array_merge($config, array(
					'Account'		=>	$row['Account'],
					'ApiName'		=>	'aliexpress',
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			ly200::curl($c['sync_url'], $data);
		}elseif($row['Platform']=='amazon'){
			//亚马逊
			if($c['FunVersion']<2) return;
			$data=@array_merge($config, array(
					'MerchantId'	=>	$row['Account'],
					'ApiName'		=>	'amazon',
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			ly200::curl($c['sync_url'], $data);
		}elseif($row['Platform']=='wish'){
			//Wish
			if($c['FunVersion']<2) return;
			$data=@array_merge($config, array(
					'ClientId'	=>	$row['Account'],
					'ApiName'	=>	'wish',
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			ly200::curl($c['sync_url'], $data);
		}elseif($row['Platform']=='shopify'){
			//Shopify
			if($c['FunVersion']<2) return;
			$data=@array_merge($config, array(
					'UserName'	=>	$row['Account'],
					'ApiName'	=>	'shopify',
				)
			);
			$data['sign']=ly200::sign($data, $c['ApiKey']);
			ly200::curl($c['sync_url'], $data);
		}
		
		db::delete('authorization', "AId='{$g_AId}'");
		ly200::e_json('', 1);
	}
	/*******************************平台授权(end)*****************************/

    //add by zhz start
    public static function moreshipping(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_CateId=(int)$p_CateId;//物流
        $p_SId=(int)$p_SId;
        $where="1 and CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($p_CateId)."%') or CateId='{$p_CateId}'";
        $prod_count=db::get_row_count('products', $where);
        if($prod_count){
            $procatearr = db::get_all('products',$where,'ProId');
            $procatearr = array_column($procatearr, 'ProId');
            $proshipparr = db::get_all("product_shipping","shipping_id = '{$p_SId}'","products_id");
            $proshipparr = array_column($proshipparr, 'products_id');

            $proid=array_diff($procatearr,$proshipparr);
            foreach ($proid as $key => $value){
                db::insert('product_shipping',array('products_id'=>$value,'shipping_id'=>$p_SId));
            }
        }else{
            ly200::e_json($c['manage']['lang_pack']['products']['action']['no_products'], 0);
        }
        unset($prod_count);
        ly200::e_json('添加成功', 1);
    }

    public static function businessapi(){
        global $c;
        $url = 'http://pushapi.guangzhouyueyang.com/v1/api/supplierapi?pageSize=1&pageNo=1';
        $result = ly200::zhzcurl($url);
        $result = json_decode($result);

        if ($result->code == 0){
            $page = ceil(($result->data->count/200));

            for ($i = 1; $i <=$page; $i++){
                $url1 = 'http://pushapi.guangzhouyueyang.com/v1/api/supplierapi?pageSize=200&pageNo='.$i;
                $result1 = ly200::zhzcurl($url1);
                $result1 = json_decode($result1);
                foreach ($result1->data->rows as $key => $val){
                    $res = db::get_one("business","memberNo = '$val->memberNo'");
                    if ($res){
                        $data['name'] = $val->companyName;
                        $data['memberNo'] = $val->memberNo;
                        $data['Contacts'] = $val->contacts;
                        $data['Phone'] = $val->mobile;
                        $data['Address'] = $val->address;
                        $data['openBank'] = $val->openBank;
                        $data['openBranch'] = $val->openBranch;
                        $data['openName'] = $val->openName;
                        $data['bankNo'] = $val->bankNo;
                        db::update('business', "memberNo='$val->memberNo'", $data);
                    }else{
                        $data['name'] = $val->companyName;
                        $data['memberNo'] = $val->memberNo;
                        $data['Contacts'] = $val->contacts;
                        $data['Phone'] = $val->mobile;
                        $data['Address'] = $val->address;
                        $data['openBank'] = $val->openBank;
                        $data['openBranch'] = $val->openBranch;
                        $data['openName'] = $val->openName;
                        $data['bankNo'] = $val->bankNo;
                        $data['AccTime'] = time();
                        $data['type'] = 1;
                        db::insert('business', $data);
                    }
                }

            }
            ly200::e_json('同步成功', 1);
        }else{
            ly200::e_json('同步失败', 1);
        }
    }

    public static function business_cate_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $CateId = $p_CateId;
        if ($p_cid == 0){
            $uid = '0,';
        }else{
            $a = db::get_one("business_cate","id = '{$p_cid}'");
            $uidarr = @explode(',',$a['uid']);
            $uid = $a['uid'].$a['id'].',';
        }
//        print_r($uid);die;
        if (count($uidarr) <= 4){
            $data=array(
                'name'		=>	$p_name,
                'name_en'   =>	$p_name_en,
                'name_fa'	=>	$p_name_fa,
                'uid'		=>	$uid,
                'number'	=>	$p_number,
                'color'	    =>	$p_color,
                'sort'	    =>	$p_sort,
                'catelogid'	=>	$p_catelogid,
                'img'	    =>	$p_img,
                'is_del'	=>	$p_is_del?$p_is_del:1
            );
//            print_r($data);die;
            if($CateId){
                db::update('business_cate', "id='$CateId'", $data);
                manage::operation_log('修改供应商分类');
            }else{
                $check = db::get_one("business_cate","number = '".$p_number."'");
                if($check){

                }else{
                    db::insert('business_cate', $data);
                    manage::operation_log('添加供应商分类');
                }
            }
            ly200::e_json('', 1);
        }
    }

    public static function business_cate_del(){
        global $c;
        @extract($_GET, EXTR_PREFIX_ALL, 'g');
        $data=array(
            'is_del'	=>	2
        );
        db::update('business_cate', "id='$g_CateId'", $data);
        manage::operation_log('删除供应商分类');
        ly200::e_json('', 1);
    }

    public static function business_product_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');

        $PicPath      = $p_PicPath;
        $cid          = $p_cid;
        $pid          = $p_pid;
        $name         = $p_name;
        $name_en      = $p_name_en;
        $sku          = $p_sku;
        $msku         = $p_msku;
        $money        = $p_money;
        $startqty     = $p_startqty;
        $deliverytime = $p_deliverytime;
        $material     = $p_material;
        $spec         = $p_spec;
        $content      = $p_content;
        $remake       = $p_remake;
        $ProId        = (int)$p_ProId;
        $seo          = $p_seo;
        $seo_en       = $p_seo_en;
        $seo_fa       = $p_seo_fa;


        //上传图片
        $ImgPath=array();
        $resize_ary=$c['manage']['resize_ary']['products'];
        $save_dir=$c['manage']['upload_dir'].$c['manage']['sub_save_dir']['products'].date('d/');
        file::mk_dir($save_dir);
        foreach((array)$PicPath as $k=>$v){
            if(!is_file($c['root_path'].$v)) continue;
            $ImgPath[]=file::photo_tmp_upload($v, $save_dir, $resize_ary);
        }
        if(!count($ImgPath)) ly200::e_json(manage::get_language('products.products.pic_tips'));
        $pro_img_ary=array();

        $pro_img_row=db::get_one('business_product', "id='{$ProId}'", $c['prod_image']['sql_field']);

        foreach((array)$pro_img_row as $v){
            if($v) $pro_img_ary[]=$v;
        }

        if($c['manage']['config']['IsWater']){//更新水印图片
            foreach((array)$ImgPath as $k=>$v){
                if(@in_array($v, (array)$pro_img_ary)) continue; //已经存在图片不更新水印
                $water_ary=array($v);
                $ext_name=file::get_ext_name($v);
                @copy($c['root_path'].$v.".default.{$ext_name}", $c['root_path'].$v);//覆盖大图
                if($c['manage']['config']['IsThumbnail']){//缩略图加水印
                    img::img_add_watermark($v);
                    $water_ary=array();
                }
                foreach((array)$resize_ary as $v2){
                    if($v=='default') continue;
                    $size_w_h=explode('x', $v2);
                    $resize_img=$v;
                    $resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
                }
                foreach((array)$water_ary as $v2){
                    img::img_add_watermark($v2);
                }
            }
        }
        foreach((array)$ImgPath as $k=>$v){
            $ext_name=file::get_ext_name($v);
            foreach((array)$resize_ary as $v2){
                if(!is_file($c['root_path'].$v.".{$v2}.{$ext_name}")){
                    $size_w_h=explode('x', $v2);
                    $resize_img=$v;
                    $resize_path=img::resize($resize_img, $size_w_h[0], $size_w_h[1]);
                }
            }
            if(!is_file($c['root_path'].$v.".default.{$ext_name}")){
                @copy($c['root_path'].$v, $c['root_path'].$v.".default.{$ext_name}");
            }
        }

        $data  =  array(
            'cid' => $cid,
            'pid' => $pid,
            'name' => $name,
            'name_en' => $name_en,
            'sku' => $sku,
            'msku' => $msku,
            'PicPath_0'					=>	$ImgPath[0],
            'PicPath_1'					=>	$ImgPath[1],
            'PicPath_2'					=>	$ImgPath[2],
            'PicPath_3'					=>	$ImgPath[3],
            'PicPath_4'					=>	$ImgPath[4],
            'PicPath_5'					=>	$ImgPath[5],
            'PicPath_6'					=>	$ImgPath[6],
            'PicPath_7'					=>	$ImgPath[7],
            'PicPath_8'					=>	$ImgPath[8],
            'PicPath_9'					=>	$ImgPath[9],
            'PicPath_10'				=>	$ImgPath[10],
            'PicPath_11'				=>	$ImgPath[11],
            'PicPath_12'				=>	$ImgPath[12],
            'PicPath_13'				=>	$ImgPath[13],
            'PicPath_14'				=>	$ImgPath[14],
            'money' => $money,
            'startqty' => $startqty,
            'deliverytime' => $deliverytime,
            'material' => $material,
            'spec' => $spec,
            'content' => $content,
            'seo' => $seo,
            'seo_en' => $seo_en,
            'seo_fa' => $seo_fa,
            'remake' => $remake
        );
        if ($ProId){ //更新
            db::update('business_product', "id='$ProId'", $data);
            manage::operation_log('修改供应商产品');
            ly200::e_json('', 1);
        }else{//添加
            db::insert('business_product', $data);
            manage::operation_log('添加供应商产品');
            ly200::e_json('', 1);
        }
    }

    public static function gysproducts_sold_bat(){
        @extract($_GET, EXTR_PREFIX_ALL, 'g');
        !$g_id && ly200::e_json('');
        $g_sold=(int)$g_sold;
        $bat_where="id in(".str_replace('-', ',', $g_id).")";
        db::update('business_product', $bat_where, array('is_show'=>$g_sold));
        manage::operation_log($g_sold==2?'批量产品下架':'批量产品上架');
        ly200::e_json('', 1);
    }
    public static function gysproducts_del(){
        @extract($_GET, EXTR_PREFIX_ALL, 'g');
        !$g_id && ly200::e_json('');
        $g_sold=(int)$g_sold;
        $bat_where="id in(".str_replace('-', ',', $g_id).")";
        db::delete('business_product', $bat_where);

        ly200::e_json('', 1);
    }
    //供应商分类导入
    public static function zhzupload(){
        global $c;

        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_Number=(int)$p_Number;//当前分开数
        $p_Current=(int)$p_Current;//行数开始位置
        $p_Worksheet=(int)$p_Worksheet;
        include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
        $errerTxt='';
        !file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json('文件不存在！');

        $objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
        $sheet=$objPHPExcel->getSheet(0);//工作表0
        $highestRow=$sheet->getHighestRow();//取得总行数
        $highestColumn=$sheet->getHighestColumn();//取得总列数
        $data = []; //数组形式获取表格数据




        for ($i = 2; $i <= $highestRow; $i ++) {
            if ($sheet->getCell("E".$i)->getValue()){
                $pid= $sheet->getCell("A".$i)->getValue();
                if($pid == 0){
                    $data[$i]['uid'] = '0,';
                }else{
                    $cate = db::get_one("business_cate","id = ".$pid);
                    if ($cate){
                        $data[$i]['uid'] = $cate['uid'].$cate['id'].',';
                    }
                    unset($cate);
                }
                $data[$i]['name'] = $sheet->getCell("B".$i)->getValue();
                $data[$i]['name_en'] = $sheet->getCell("C".$i)->getValue();
                $data[$i]['name_fa'] = $sheet->getCell("D".$i)->getValue();
                $data[$i]['number'] = $sheet->getCell("E".$i)->getValue();
                $data[$i]['sort'] = $sheet->getCell("F".$i)->getValue()?$sheet->getCell("F".$i)->getValue():1;
                $data[$i]['catelogid'] = $sheet->getCell("G".$i)->getValue();
                $data[$i]['is_del'] = 1;
            }
        }
        //开始导入
        $number = 0;
        $unumber = 0;
        foreach ((array)$data as $a => $v) {
            $check = db::get_one("business_cate","number = '".$v['number']."'");
            if ($check){
                $Msg[$number]['data'] = $v['name'];
                $Msg[$number]['s'] = "有重复编号";
                if($unumber == 0){
                    $usql .= 'UPDATE business_cate set `name`= "'.$v['name'].'" , `name_en` = "'.$v['name_en'].'" , `name_fa` = "'.$v['name_fa'].'" , `uid`= "'.$v['uid'].'" , `sort` = "'.$v['sort'].'" , `uid`= "'.$v['uid'].'" , `is_del` = "'.$v['is_del'].'",`catelogid` = "'.$v['catelogid'].'" WHERE number = "'.$v['number'].'"';
                }else{
                    $usql .= ';UPDATE business_cate set `name`= "'.$v['name'].'" , `name_en` = "'.$v['name_en'].'" , `name_fa` = "'.$v['name_fa'].'" , `uid`= "'.$v['uid'].'" , `sort` = "'.$v['sort'].'" , `uid`= "'.$v['uid'].'" , `is_del` = "'.$v['is_del'].'",`catelogid` = "'.$v['catelogid'].'" WHERE number = "'.$v['number'].'"';
                }
                $unumber ++;
                $usql = db::update('business_cate', "number='".$v['number']."'", $v);
                unset($usql);

            }else{
                if ($number == 0){
                    $sql .= '("'.$v['name'].'","'.$v['name_en'].'","'.$v['name_fa'].'","'.$v['uid'].'","'.$v['number'].'","'.$v['sort'].'","'.$v['is_del'].'","'.$v['catelogid'].'")';
                }else{
                    $sql .= ',("'.$v['name'].'","'.$v['name_en'].'","'.$v['name_fa'].'","'.$v['uid'].'","'.$v['number'].'","'.$v['sort'].'","'.$v['is_del'].'","'.$v['catelogid'].'")';
                }
                $Msg[$number]['data'] = $v['name'];
                $Msg[$number]['s'] = "成功";
                $number++;
            }
        }
        if($sql){
            db::query("INSERT INTO business_cate (name,name_en,name_fa,uid,number,sort,is_del,catelogid) values ".$sql);
        }
        file::del_file($p_ExcelFile);
        ly200::e_json($Msg, 2);
    }
    //供应商产品导入
    public static function zhzbpupload(){
        global $c;

        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_Number=(int)$p_Number;//当前分开数
        $p_Current=(int)$p_Current;//行数开始位置
        $p_Worksheet=(int)$p_Worksheet;
        include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
        $errerTxt='';
        !file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json('文件不存在！');
        $objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
        $sheet=$objPHPExcel->getSheet(0);//工作表0
        $highestRow=$sheet->getHighestRow();//取得总行数
        $highestColumn=$sheet->getHighestColumn();//取得总列数


        //内容转换为数组
        $pro_count=0;
        $max_rows=0;
        $data=array();
        for ($i = 2; $i <= $highestRow; $i ++) {
            if ($sheet->getCell("A".$i)->getValue() && $sheet->getCell("D".$i)->getValue()){
                $bus = db::get_one("business","memberNo = '".$sheet->getCell("A".$i)->getValue()."'","BId");
                if ($bus){
                    $data[$i]['pid'] = $bus['BId'];
                    $data[$i]['name'] = $sheet->getCell("B".$i)->getValue();
                    $data[$i]['name_en'] = $sheet->getCell("C".$i)->getValue();
                    $cate = db::get_one("business_cate","number = '".$sheet->getCell("D".$i)->getValue()."'","id");
                    if(!$cate){
                        $Msg[$i]['s'] = "匹配不了分类";
                    }else{
                        $Msg[$i]['s'] = "成功";
                    }

                    $data[$i]['cid'] = $cate['id'];
                    $data[$i]['money'] = $sheet->getCell("E".$i)->getValue();
                    $data[$i]['startqty'] = $sheet->getCell("F".$i)->getValue();
                    $data[$i]['deliverytime'] = $sheet->getCell("G".$i)->getValue();
                    $data[$i]['spec'] = $sheet->getCell("H".$i)->getValue();
                    $data[$i]['material'] = $sheet->getCell("I".$i)->getValue();
                    $data[$i]['content'] = $sheet->getCell("J".$i)->getValue();
                    $data[$i]['seo'] = $sheet->getCell("K".$i)->getValue();
                    $data[$i]['seo_en'] = $sheet->getCell("L".$i)->getValue();
                    $data[$i]['seo_fa'] = $sheet->getCell("M".$i)->getValue();
                    $data[$i]['is_show'] = $sheet->getCell("N".$i)->getValue()?$sheet->getCell("N".$i)->getValue():1;

                    $Msg[$i]['data'] = $sheet->getCell("B".$i)->getValue();
                    unset($bus);
                    unset($cate);
                }else{
                    $Msg[$i]['data'] = $sheet->getCell("B".$i)->getValue();
                    $Msg[$i]['s'] = "匹配不了供应商添加失败";
                }
            }
        }

        //开始导入
        $number = 0;
        foreach ((array)$data as $a => $v) {
            if ($number == 0){
                $sql .= '("'.$v['name'].'","'.$v['name_en'].'","'.$v['cid'].'","'.$v['pid'].'","'.$v['money'].'","'.$v['startqty'].'","'.$v['deliverytime'].'","'.$v['material'].'","'.$v['spec'].'","'.$v['content'].'","'.$v['seo'].'","'.$v['seo_en'].'","'.$v['seo_fa'].'","'.$v['is_show'].'")';
            }else{
                $sql .= ',("'.$v['name'].'","'.$v['name_en'].'","'.$v['cid'].'","'.$v['pid'].'","'.$v['money'].'","'.$v['startqty'].'","'.$v['deliverytime'].'","'.$v['material'].'","'.$v['spec'].'","'.$v['content'].'","'.$v['seo'].'","'.$v['seo_en'].'","'.$v['seo_fa'].'","'.$v['is_show'].'")';
            }

            $number++;
        }
//        echo "INSERT INTO business_product (name,name_en,cid,pid,money,startqty,deliverytime,material,spec,content,seo,seo_en,seo_fa,is_show) values ".$sql;die;
        db::query("INSERT INTO business_product (name,name_en,cid,pid,money,startqty,deliverytime,material,spec,content,seo,seo_en,seo_fa,is_show) values ".$sql);
        file::del_file($p_ExcelFile);
        ly200::e_json($Msg, 2);
    }
    //供应商导入
    public static function bupload(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_Number=(int)$p_Number;//当前分开数
        $p_Current=(int)$p_Current;//行数开始位置
        $p_Worksheet=(int)$p_Worksheet;
        include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
        $errerTxt='';
        !file_exists($c['root_path'].$p_ExcelFile) && ly200::e_json('文件不存在！');

        $objPHPExcel=PHPExcel_IOFactory::load($c['root_path'].$p_ExcelFile);
        $sheet=$objPHPExcel->getSheet(0);//工作表0
        $highestRow=$sheet->getHighestRow();//取得总行数
        $highestColumn=$sheet->getHighestColumn();//取得总列数
//        echo $highestRow;
//        echo $highestColumn;die;

        //内容转换为数组
        $pro_count=0;
        $max_rows=0;
        $data=array();
        for ($i = 2; $i <= $highestRow; $i ++) {
            if ($sheet->getCell("A".$i)->getValue()){
                $data[$i]['memberNo'] = $sheet->getCell("A".$i)->getValue();
                $data[$i]['Name'] = $sheet->getCell("B".$i)->getValue();
                $data[$i]['mycateid'] = $sheet->getCell("C".$i)->getValue();
                $data[$i]['Address'] = $sheet->getCell("D".$i)->getValue();
                $data[$i]['Contacts'] = $sheet->getCell("E".$i)->getValue();
                $data[$i]['Phone'] = $sheet->getCell("F".$i)->getValue();
                $data[$i]['Telephone'] = $sheet->getCell("G".$i)->getValue();
                $data[$i]['city'] = $sheet->getCell("H".$i)->getValue();
                $data[$i]['companyDesc'] = $sheet->getCell("I".$i)->getValue();
                $data[$i]['encompanyDesc'] = $sheet->getCell("J".$i)->getValue();
                $data[$i]['facompanyDesc'] = $sheet->getCell("K".$i)->getValue();
                $data[$i]['Remark'] = $sheet->getCell("L".$i)->getValue();
                $data[$i]['laabel'] = $sheet->getCell("M".$i)->getValue();
                $data[$i]['type'] = 3;
            }
        }
//        print_r($data);die;
        //开始导入
        $number = 0;
        foreach ((array)$data as $a => $v) {
            if ($v['memberNo']){
                $check = db::get_one("business","memberNo = '".$v['memberNo']."'");
                if ($check){
                    $Msg[$number]['data'] = $v['Name'];
                    $Msg[$number]['s'] = "有重复编号";
                    unset($check);
                    continue;
                }else{
                    if ($number == 0){
                        $sql .= '("'.$v['memberNo'].'","'.$v['Name'].'","'.$v['mycateid'].'","'.$v['Address'].'","'.$v['Contacts'].'","'.$v['Phone'].'","'.$v['Telephone'].'","'.$v['city'].'","'.$v['companyDesc'].'","'.$v['encompanyDesc'].'","'.$v['facompanyDesc'].'","'.$v['Remark'].'","'.$v['laabel'].'","'.$v['type'].'")';
                    }else{
                        $sql .= ',("'.$v['memberNo'].'","'.$v['Name'].'","'.$v['mycateid'].'","'.$v['Address'].'","'.$v['Contacts'].'","'.$v['Phone'].'","'.$v['Telephone'].'","'.$v['city'].'","'.$v['companyDesc'].'","'.$v['encompanyDesc'].'","'.$v['facompanyDesc'].'","'.$v['Remark'].'","'.$v['laabel'].'","'.$v['type'].'")';
                    }
                    $Msg[$number]['data'] = $v['Name'];
                    $Msg[$number]['s'] = "成功";
                    $number++;
                }
            }
        }
        db::query("INSERT INTO business (memberNo,Name,mycateid,Address,Contacts,Phone,Telephone,city,companyDesc,encompanyDesc,facompanyDesc,Remark,laabel,type) values ".$sql);
        file::del_file($p_ExcelFile);
        ly200::e_json($Msg, 2);
    }
    public static function business_catelog_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $CateId = $p_CateId;
        $data=array(
            'name'		=>	$p_name,
            'name_en'   =>	$p_name_en,
            'name_fa'	=>	$p_name_fa,
            'sort'	    =>	$p_sort,
            'is_del'    =>  $p_is_del?$p_is_del:1

        );
        if($CateId){
            db::update('business_catelog', "id='$CateId'", $data);
            manage::operation_log('修改供应商分类目录');
        }else{
            db::insert('business_catelog', $data);
            manage::operation_log('添加供应商分类目录');

        }
        ly200::e_json('', 1);

    }
    public static function business_catelog_del(){
        global $c;
        @extract($_GET, EXTR_PREFIX_ALL, 'g');
        $data=array(
            'is_del'	=>	2
        );
        db::update('business_catelog', "id='$g_CateId'", $data);
        manage::operation_log('删除供应商分类目录');
        ly200::e_json('', 1);
    }
    public static function logistics_line_edit(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $CateId = $p_CateId;
//        print_r($_POST);die;
        $data=array(
            'name'		=>	$p_name,
            'name_en'   =>	$p_name_en,
            'name_fa'	=>	$p_name_fa,
            'sfdgj'		=>	$p_sfdgj,
            'sfdgj_en'	=>	$p_sfdgj_en,
            'sfdgj_fa'	=>	$p_sfdgj_fa,
            'sfdcs'		=>	$p_sfdcs,
            'sfdcs_en'	=>	$p_sfdcs_en,
            'sfdcs_fa'	=>	$p_sfdcs_fa,
            'mddgj'		=>	$p_mddgj,
            'mddgj_en'	=>	$p_mddgj_en,
            'mddgj_fa'	=>	$p_mddgj_fa,
            'mddcs'		=>	$p_mddcs,
            'mddcs_en'	=>	$p_mddcs_en,
            'mddcs_fa'	=>	$p_mddcs_fa,
            'psfs'		=>	$p_psfs,
            'protype'	=>	$p_protype,
            'protype_en'=>	$p_protype_en,
            'protype_fa'=>	$p_protype_fa,
            'prescription'	=>	$p_prescription,
            'prescriptiondw'	=>	$p_prescriptiondw,
            'xsdanjia'	=>	$p_xsdanjia,
            'tjdanjia'	=>	$p_tjdanjia,
            'tjqs'	    =>	$p_tjqs,
            'zldanjia'	=>	$p_zldanjia,
            'zlqs'	    =>	$p_zlqs,
            'hzb'	    =>	$p_hzb,
            'content'	=>	$p_content,
            'content_en'=>	$p_content_en,
            'content_fa'=>	$p_content_fa,
            'sort'	    =>	$p_sort,
            'is_show'   =>  $p_is_show?$p_is_show:1

        );
        if($CateId){
            db::update('logistics_line', "id='$CateId'", $data);
            manage::operation_log('修改物流线路');
        }else{
            db::insert('logistics_line', $data);
            manage::operation_log('添加物流线路');

        }
        ly200::e_json('', 1);

    }
    public static function business_search(){
        if ($_GET['keyword']){

            $where = "1 and (memberNo like '%".$_GET['keyword']."%' or Name like '%".$_GET['keyword']."%')";
//            echo $where;;die;
            $business_row=db::get_all('business', $where, 'BId,Name,memberNo', 'BId desc limit 20');

            ly200::e_json($business_row, 1);
        }
    }

    //add by zhz end
}
?>