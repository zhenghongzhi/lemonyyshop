<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class operation_module{
	public static function page_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$AId=(int)$p_AId;
		$data=array(
			'CateId'	=>	(int)$p_CateId,
			'Url'		=>	$p_Url,
			'AccTime'	=>	$c['time'],
			'PageUrl'	=>	ly200::str_to_url($p_PageUrl),
			'MyOrder'	=>	(int)$p_MyOrder
		);
		if($AId){
			db::update('article', "AId='$AId'", $data);
			if(!db::get_row_count('article_content', "AId='$AId'")){
				db::insert('article_content', array('AId'=>$AId));
			}
			manage::operation_log('修改单页');
		}else{
			db::insert('article', $data);
			$AId=db::get_insert_id();
			db::insert('article_content', array('AId'=>$AId));
			manage::operation_log('添加单页');
		}
		manage::database_language_operation('article', "AId='$AId'", array('Title'=>1, 'SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		manage::database_language_operation('article_content', "AId='$AId'", array('Content'=>3));
		ly200::e_json('', 1);
	}
	
	public static function page_edit_myorder(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;
		db::update('article', "AId='{$p_Id}'", array('MyOrder'=>$p_Number));
		manage::operation_log('单页修改排序');
		ly200::e_json(manage::language($c['manage']['my_order'][$p_Number]), 1);
	}
	
	public static function page_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_AId=(int)$g_AId;
		db::delete('article', "AId='$g_AId'");
		db::delete('article_content', "AId='$g_AId'");
		manage::operation_log('删除单页');
		ly200::e_json('', 1);
	}
	
	public static function page_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="AId in(".str_replace('-', ',', $g_id).")";
		db::delete('article', $del_where);
		db::delete('article_content', $del_where);
		manage::operation_log('批量删除单页');
		ly200::e_json('', 1);
	}
	
	public static function page_category_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$CateId=(int)$p_CateId;
		$data=array(
			'UId'	=>	'0,',
			'Dept'	=>	1,
			'IsHelp'=>	(int)$p_IsHelp,
			'IsIndex'=>	(int)$p_IsIndex,
		);
		if($CateId){
			db::update('article_category', "CateId='$CateId'", $data);
			manage::operation_log('修改单页分类');
		}else{
			db::insert('article_category', $data);
			$CateId=db::get_insert_id();
			manage::operation_log('添加单页分类');
		}
		//manage::database_language_operation('article_category', "CateId='$CateId'", array('Category'=>1, 'SeoTitle'=>1, 'SeoKeyword'=>1, 'SeoDescription'=>2));
		manage::database_language_operation('article_category', "CateId='$CateId'", array('Category'=>1));
		ly200::e_json('', 1);
	}
	
	public static function page_category_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `article_category` SET `MyOrder` = CASE `CateId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CateId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('批量单页分类排序');
		ly200::e_json('', 1);
	}
	
	public static function page_category_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_CateId=(int)$g_CateId;
		db::delete('article_category', "CateId='$g_CateId'");
		manage::operation_log('删除单页分类');
		ly200::e_json('', 1);
	}
	
	public static function page_category_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="CateId in(".str_replace('-',',',$g_id).")";
		db::delete('article_category', $del_where);
		manage::operation_log('批量删除单页分类');
		ly200::e_json('', 1);
	}
	
	public static function email_send(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$Subject	= $p_Subject; //主题
		$Content	= stripslashes($p_Content); //邮件内容
		$Email		= @array_merge((array)$p_userName, (array)$p_newsletterName, (array)$p_To); //邮箱地址
		$ToAry		= array(); //发送
		//会员等级信息
		if($p_levelCurrent){
			$level_where=@implode(',', $p_levelCurrent);
			!$level_where && $level_where='-1';
			$level_row=db::get_all('user', "Level in ($level_where)", 'Email');
			foreach((array)$level_row as $v){
				$Email[]=$v['Email'];
			}
		}
		//检查邮件内容
		if(!$Content){
			ly200::e_json('邮件内容为空！');
		}
		if($Email){
			$i=0;
			foreach((array)$Email as $v){
				$v=str_replace(array('(会员)', '(等级)', '(订阅)'), '', $v);
				$v=trim($v);
				if(!preg_match('/^\w+[a-zA-Z0-9-.+_]+@[a-zA-Z0-9-.+_]+\.\w*$/i', $v)){//邮箱地址格式不正确
					++$i;
					continue;
				}
				if(!in_array($v, $ToAry)){//防止邮箱地址重复
					$ToAry[]=$v;//放置到待发送数组
				}
			}
			if($i==count($Email)){
				ly200::e_json('邮箱地址格式不正确！');
			}
		}else{
			ly200::e_json('缺少邮箱地址！');
		}
		if(count($ToAry)<1){
			ly200::e_json('邮箱地址格式不正确！');
		}
		$Content=preg_replace("~([\"|'|(|=])/u_file/~i", '$1'.ly200::get_domain(1).'/u_file/', $Content);
		$Content=preg_replace("~([\"|'|(|=])/search/~i", '$1'.ly200::get_domain(1).'/search/', $Content);
		
		$status=ly200::sendmail($ToAry, $Subject, $Content, 1);

		manage::operation_log('发送邮件');
		manage::email_log($ToAry, $Subject, $Content);//邮件发送记录
		if($status['error']==1){
			if($status['response']['status']==400) $status['detail']='您的邮件未正常发送。 帐户已禁用。';
			$status['detail'] || $status['detail']='账号信息不对！';
			ly200::e_json($status['detail'], 0);
		}
		if((int)$p_Arrival){//更新到货通知信息
			db::update('arrival_notice', "AId='{$p_Arrival}'", array('IsSend'=>1, 'SendTime'=>$c['time']));
		}
		ly200::e_json('发送成功！', 1);
	}
	
	public static function email_template_choice(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//$p_Type 模板分类文件夹; $p_Template 模板文件夹
		$domain=ly200::get_domain();//网站域名
		$tpl_dir=$c['manage']['email_tpl_dir'].$p_Type.'/'.$p_Template;
        $base_dir=$c['root_path'].$tpl_dir;//邮件模板目录绝对路径
		$txt='';
		if($p_Type=='customize'){//自定义模板
			if((int)$p_Template){
				$txt=db::get_value('email_list', "EId='{$p_Template}'", 'Content');
				$txt=preg_replace('/{tpl_dir}/i', $tpl_dir.'/', preg_replace('/{domain}/i', ly200::get_domain(1), $txt));
			}
		}else{
			if(is_dir($base_dir)){
				$tpl_file=$base_dir.'/template.html';
				if(file_exists($tpl_file)){
					$txt=preg_replace('/{tpl_dir}/i', $tpl_dir.'/', preg_replace('/{domain}/i', ly200::get_domain(1), file_get_contents($tpl_file)));
				}
			}
		}
		ly200::e_json($txt, $txt!='');
	}
	
	public static function email_template_save(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($p_Title && $p_Content){
			$data=array(
				'Title'		=>	$p_Title,
				'PicPath'	=>	$p_PicPath,
				'Content'	=>	trim(htmlspecialchars_decode(str::unescape($p_Content))),
				'AccTime'	=>	$c['time']
			);
			db::insert('email_list', $data);
			manage::operation_log('添加邮件自定义模板');
			ly200::e_json('', 1);
		}else{
			ly200::e_json('邮件标题或者内容为空！', 0);
		}
	}
	
	public static function email_template_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		if((int)$g_EId){
			db::delete('email_list', "EId='$g_EId'");
			manage::operation_log('删除邮件自定义模板');
		}
		ly200::e_json('', 1);
	}
	
	public static function config(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array(
			'FromEmail'		=>	$p_FromEmail,
			'FromName'		=>	$p_FromName,
			'SmtpHost'		=>	$p_SmtpHost,
			'SmtpPort'		=>	(int)$p_SmtpPort?(int)$p_SmtpPort:25,
			'SmtpUserName'	=>	$p_SmtpUserName,
			'SmtpPassword'	=>	$p_SmtpPassword
		);
		$json_data=str::json_data($data);
		manage::config_operaction(array('config'=>$json_data), 'email');
		
		//邮件通知
		$data=array();
		foreach($c['manage']['email_notice'] as $k=>$v){
			if($v=='order_create') continue; //下单，另外储存
			$data[$v]=in_array($v, $p_Notice)?1:0;
		}
		$json_data=str::json_data($data);
		manage::config_operaction(array('notice'=>$json_data), 'email');
		manage::config_operaction(array('CheckoutEmail'=>in_array('order_create', $p_Notice)?1:0), 'global');//下单
		
		//底部签名内容
		$BottomContentAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$BottomContentAry['BottomContent_'.$v]=${'p_BottomContent_'.$v};
		}
		$BottomContentData=addslashes(str::json_data(str::str_code($BottomContentAry, 'stripslashes')));
		manage::config_operaction(array('bottom'=>$BottomContentData), 'email');
		ly200::e_json('', 1);
	}
	
	public static function newsletter_cancel(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_NId=(int)$g_NId;
		$newsletter_row=db::get_one('newsletter', "NId='{$g_NId}'");
		if($newsletter_row){
			db::delete('newsletter', "NId='{$g_NId}'");
			manage::operation_log('删除订阅: '.$newsletter_row['Email']);
		}
		ly200::e_json('', 1);
	}
	
	public static function newsletter_status(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_NId=(int)$p_NId;
		$p_IsUsed=(int)$p_IsUsed;
		$newsletter_row=db::get_one('newsletter', "NId='{$p_NId}'");
		if($newsletter_row){
			db::update('newsletter', "NId='{$p_NId}'", array('IsUsed'=>$p_IsUsed));
			manage::operation_log(($p_IsUsed?'开启':'取消').'订阅: '.$newsletter_row['Email']);
		}
		$msg=($p_IsUsed?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}
	
	public static function newsletter_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_NId=(int)$g_NId;
		$newsletter_row=db::get_one('newsletter', "NId='{$g_NId}'");
		if($newsletter_row){
			db::delete('newsletter', "NId='{$g_NId}'");
			manage::operation_log('删除订阅: '.$newsletter_row['Email']);
		}
		ly200::e_json('', 1);
	}
	
	//邮件订阅导出
	public static function newsletter_explode(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');	
		include($c['root_path'].'/inc/class/excel.class/PHPExcel.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/Writer/Excel5.php');
		include($c['root_path'].'/inc/class/excel.class/PHPExcel/IOFactory.php');
		
		//Add some data
		$result=db::get_all('newsletter', '1', '*', 'AccTime desc');
		
		if($result){
			$objPHPExcel=new PHPExcel();
			 
			//Set properties 
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
			$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setKeywords("office 2007 openxml php");
			$objPHPExcel->getProperties()->setCategory("Test result file");
			
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValue('A1', '邮箱');
			$objPHPExcel->getActiveSheet()->setCellValue('B1', '时间');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', '已订阅');
			
			$i=2;
			foreach((array)$result as $k=>$v){
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $v['Email']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, date('Y-m-d H:i:s', $v['AccTime']));
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, (int)$v['IsUsed']?'是':'否');
				
				//设置行的高度
				$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(30);
				
				++$i;
			}
			
			//设置列的宽度
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			
			//Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('Simple');
			
			//Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			//Save Excel 2007 file
			$ExcelName='newsletter_'.str::rand_code();
			$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);
			$objWriter->save($c['root_path']."/tmp/{$ExcelName}.xls");
			unset($c, $objPHPExcel, $objWriter, $row, $prod_ary);
			
			file::down_file("/tmp/{$ExcelName}.xls");
			file::del_file("/tmp/{$ExcelName}.xls");
			
			ly200::e_json('', 1);
		}else{
			js::location('?m=operation&a=newsletter');
		}
	}
	
	public static function arrival_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_AId=(int)$g_AId;
		$arrival_row=db::get_one('arrival_notice', "AId='{$g_AId}'");
		if($arrival_row){
			db::delete('arrival_notice', "AId='{$g_AId}'");
			manage::operation_log('删除到货通知');
		}
		ly200::e_json('', 1);
	}
	
	public static function sys_get_tpl(){//获取系统模板
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$template = $p_template;
		!in_array($template, $c['sys_email_tpl']) && ly200::e_json('', 0);
		$lang= trim($p_lang, ',');//语言版
		$lang = (array)@explode(',', $lang);
		$row = db::get_one('system_email_tpl', "Template='{$template}'");//查询模板是否存在
		$data = array('lang'=>array(), 'template'=>$template);//返回的数据
		$data['IsUsed'] = $row['IsUsed']?1:0;
		foreach ($lang as $k=>$v){
			$data['lang'][]=$v;
			if ($row['Content_'.$v]){//存在读取数据库
				$data['Title'][$v] = $row['Title_'.$v];
				$data['Content'][$v] = $row['Content_'.$v];
			}else{//不存在读取系统文件默认模板
				$tpl_lang = $v;//定义语言包
				include $c['root_path'].'/static/static/inc/mail/source/'.$template.'.php';
				$data['Title'][$v] = $c['sys_email_tpl_title'][$template];
				$data['Content'][$v] = $mail_contents;
			}
		}
		ly200::e_json($data, 1);
	}
	
	public static function system_tpl_edit(){//编辑系统模板
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$template = $p_template;
		$IsUsed = (int)$p_IsUsed?1:0;
		!in_array($template, $c['sys_email_tpl']) && ly200::e_json(manage::get_language('email.select_tpl'), 0);
		$SId = db::get_value('system_email_tpl', "Template='{$template}'", 'SId');
		if (!$SId){//判断数据库是否存在
			db::insert('system_email_tpl', array(
				'Template'	=>	$template,
				'IsUsed'	=>	$IsUsed,
			));
			$SId = db::get_insert_id();
		}else{
			db::update('system_email_tpl', "SId='{$SId}'", array('IsUsed'=>$IsUsed));
		}
		foreach($c['manage']['config']['Language'] as $k=>$v){//加上域名
			$_POST['Content_'.$v]=preg_replace("~([\"|'|(|=])/u_file/~i", '$1'.ly200::get_domain(1).'/u_file/', $_POST['Content_'.$v]);
		}
		manage::database_language_operation('system_email_tpl', "SId='$SId'", array('Title'=>1, 'Content'=>3));
		manage::operation_log('编辑系统模板：'.$template);
		ly200::e_json(manage::get_language('email.edit_success'), 1);
	}

	public static function chat_set(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$w="GroupId='chat' and Variable='chat_bg'";
		$Color['Color'] = $p_Color;
		$Color['ColorTop'] = $p_ColorTop;
		foreach ((array)$c['chat']['type'] as $k=>$v){
			$Color[$k] = $_POST['Color'.$k];
		}
		$Color['Bg3_0'] = $p_Bg3_0;
		$Color['Bg3_1'] = $p_Bg3_1;
		$Color['Bg4_0'] = $p_Bg4_0;
		$Color['IsHide'] = (int)$p_IsHide;
		$ValueColor = json_encode($Color);
		
		$data=array(
			'GroupId'	=>	'chat',
			'Variable'	=>	'chat_bg',
			'Value'		=>	$ValueColor
		);
		(int)db::get_row_count('config', $w)?db::update('config', $w, $data):db::insert('config', $data);
		$w="GroupId='chat' and Variable='IsFloatChat'";
		$data=array(
			'GroupId'	=>	'chat',
			'Variable'	=>	'IsFloatChat',
			'Value'		=>	(int)$p_IsFloatChat
		);
		(int)db::get_row_count('config', $w)?db::update('config', $w, $data):db::insert('config', $data);
		manage::operation_log('修改浮动客服');

		ly200::e_json('', 1);
	}
	
	public static function chat_style(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Type=(int)$p_Type;
		$OldType=(int)db::get_value('config', 'GroupId="chat" and Variable="Type"', 'Value');
		if($OldType!=$p_Type){
			manage::config_operaction(array('Type'=>$p_Type), 'chat');
			manage::operation_log('修改浮动客服样式');
		}
		ly200::e_json('', 1);
	}

	public static function chat_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CId=(int)$p_CId;
		$data=array(
			'Name'		=>	$p_Name,
			'Type'		=>	$p_Type,
			'PicPath'	=>	$p_PicPath,
			'Account'	=>	$p_Account
		);
		if($p_CId){
			db::update('chat', "CId='{$p_CId}'", $data);
			$log='修改在线客服: '.$p_Name;
		}else{
			db::insert('chat', $data);
			$log='添加在线客服: '.$p_Name;
		}
		manage::operation_log($log);
		ly200::e_json('', 1);
	}
	
	public static function chat_my_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `chat` SET `MyOrder` = CASE `CId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('批量在线客服排序');
		ly200::e_json('', 1);
	}

	public static function chat_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_CId=(int)$g_CId;
		$name=db::get_value('chat', "CId='{$g_CId}'", 'Name');
		db::delete('chat', "CId='{$g_CId}'");
		manage::operation_log('删除在线客服: '.$name);
		ly200::e_json('', 1);
	}

	public static function billing_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_BId=(int)$p_BId;
		$p_Device=(int)$p_Device;
		$p_Position=(int)$p_Position;
		$only_pc=array(0,10,11,12);
		$only_mobile=array(2);
		$pc_glabal=db::get_value('billing', "Device=0 and Position=1", 'BId');
		$mobile_glabal=db::get_value('billing', "Device=1 and Position=1", 'BId');
		((!$p_BId && db::get_row_count('billing',"Device='{$p_Device}' and Position='{$p_Position}'")) || 
			(($p_Device==0 && in_array($p_Position, $only_mobile)) || ($p_Device==1 && in_array($p_Position, $only_pc))) 
			|| ($p_Position==1 && ($p_Device==0 && $pc_glabal && $pc_glabal !=$p_BId || $p_Device==1 && $mobile_glabal && $mobile_glabal !=$p_BId))
			) && ly200::e_json(manage::get_language('operation.billing.position_tips'), 0);

		$PicPathAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$PicPathAry[$v]=${'p_PicPath_'.$v};
		}
		$PicPathData=addslashes(str::json_data(str::str_code($PicPathAry, 'stripslashes')));

		$ContentAry=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			$ContentAry[$v]=${'p_Content_'.$v};
		}
		$ContentData=addslashes(str::json_data(str::str_code($ContentAry, 'stripslashes')));

		$data=array(
			'Device'	=>	$p_Device,
			'Position'	=>	$p_Position,
			'PicPath'	=>	$PicPathData,
			'Url'		=>	$p_Url,
			'Content'	=>	$ContentData,
		);
		if($p_BId){
			db::update('billing', "BId='{$p_BId}'", $data);
			$log='修改广告图: '.$p_BId;
		}else{
			db::insert('billing', $data);
			$log='添加广告图: '.$p_BId;
		}
		manage::operation_log($log);
		ly200::e_json('', 1);
	}

	public static function billing_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_BId=(int)$p_BId;
		$p_IsUsed=(int)$p_IsUsed;
		db::update('billing',"BId='{$p_BId}'",array('IsUsed'=>$p_IsUsed));
		manage::operation_log(($p_IsUsed ? '开启' : '关闭').'广告图-'.$p_BId);
		ly200::e_json('', 1);
	}

	public static function billing_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_BId=(int)$g_BId;
		db::delete('billing', "BId='$g_BId'");
		manage::operation_log('删除广告图');
		ly200::e_json('', 1);
	}
	
	public static function translate_lang_set(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Status=(int)$p_Status;
		$p_Lang=trim($p_Lang);
		if(!db::get_row_count('config', "GroupId='translate' and Variable='TranLangs'")){
			db::insert('config', array(
					'GroupId'	=>	'translate',
					'Variable'	=>	'TranLangs',
					'Value'		=>	'[]'
				)
			);
		}
		$rows=db::get_value('config', "GroupId='translate' and Variable='TranLangs'", 'Value');
		$LangArr=str::json_data($rows, 'decode');
		if($p_Status==1){
			$LangArr[]=$p_Lang;
		}else{
			foreach($LangArr as $k=>$v){
				if($v==$p_Lang) unset($LangArr[$k]);
			}
		}
		db::update('config', "GroupId='translate' and Variable='TranLangs'", array('Value'=>str::json_data($LangArr)));
		$msg=($p_Status==1?$c['manage']['lang_pack']['msg']['open_success']:$c['manage']['lang_pack']['msg']['close_success']);
		ly200::e_json($msg, 1);
	}

	public static function blog_set(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$name=(array)$p_name;
		$link=(array)$p_link;
		$Nav=array();
		foreach($name as $k=>$v){//导航
			$v && $Nav[]=array($v, $link[$k]);
		}
		$Nav=addslashes(str::json_data(str::str_code($Nav, 'stripslashes')));
		$data=array(
			'Title'				=>	$p_Title,
			'BriefDescription'	=>	$p_BriefDescription,
			'NavData'			=>	$Nav,
			'Banner'			=>	$p_Banner
		);
		manage::config_operaction($data, 'blog');
		manage::config_operaction(array('Blog'=>$p_Blog), 'global');
		manage::operation_log('修改博客设置');
		ly200::e_json('', 1);
	}
	
	public static function blog_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_AId=(int)$p_AId;
		$p_CateId=(int)$p_CateId;
		$p_IsHot=(int)$p_IsHot;
		
		$Tags=explode('|',$p_Tag);
		$Tags=array_filter($Tags);
		$Tags=$Tags?'|'.implode('|',$Tags).'|':'';
		
		$data=array(
			'CateId'			=>	(int)$p_CateId,
			'Title'				=>	$p_Title,
			'PicPath'			=>	$p_PicPath,
			'Author'			=>	$p_Author,
			'SeoTitle'			=>	$p_SeoTitle,
			'SeoKeyword'		=>	$p_SeoKeyword,
			'SeoDescription'	=>	$p_SeoDescription,
			'BriefDescription'	=>	$p_BriefDescription,
			'IsHot'				=>	$p_IsHot,
			'Tag'				=>	$Tags,
			'AccTime'			=>	@strtotime($p_AccTime),
		);
		if($p_AId){
			db::update('blog', "AId='$p_AId'", $data);
			db::update('blog_content', "AId='$p_AId'", array('Content'=>$p_Content));
			manage::operation_log('修改博客');
		}else{
			db::insert('blog', $data);
			$AId=db::get_insert_id();
			db::insert('blog_content', array('AId'=>$AId, 'Content'=>$p_Content));
			manage::operation_log('添加博客');
		}
		ly200::e_json('', 1);
	}
	
	public static function blog_edit_myorder(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_Number=(int)$p_Number;
		db::update('blog', "AId='{$p_Id}'", array('MyOrder'=>$p_Number));
		manage::operation_log('博客修改排序');
		ly200::e_json(manage::language($c['manage']['my_order'][$p_Number]), 1);
	}
	
	public static function blog_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$g_AId=(int)$g_AId;
		db::delete('blog', "AId='$g_AId'");
		db::delete('blog_content', "AId='$g_AId'");
		manage::operation_log('删除博客');
		ly200::e_json('', 1);
	}
	
	public static function blog_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="AId in(".str_replace('-', ',', $g_id).")";
		db::delete('blog', $del_where);
		db::delete('blog_content', $del_where);
		manage::operation_log('批量删除博客');
		ly200::e_json('', 1);
	}
	
	public static function blog_category_edit(){
		global $c;
		str::keywords_filter();
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CateId=(int)$p_CateId;
		$p_UnderTheCateId=(int)$p_UnderTheCateId;
		if($p_UnderTheCateId==0){
			$UId='0,';
			$Dept=1;
		}else{
			$UId=category::get_UId_by_CateId($p_UnderTheCateId, 'blog_category');
			$Dept=substr_count($UId, ',');
		}
		$data=array(
			'UId'			=>	$UId,
			'Category_en'	=>	$p_Category_en,
			'Dept'			=>	$Dept
		);
		if($p_CateId){
			db::update('blog_category', "CateId='$p_CateId'", $data);
			manage::operation_log('修改博客分类');
		}else{
			db::insert('blog_category', $data);
			$p_CateId=db::get_insert_id();
			manage::operation_log('添加博客分类');
		}
		$UId!='0,' && $p_CateId=category::get_top_CateId_by_UId($UId);
		$statistic_where.=category::get_search_where_by_CateId($p_CateId, 'blog_category');
		category::category_subcate_statistic('blog_category', $statistic_where);
		ly200::e_json('', 1);
	}
	
	public static function blog_category_order(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$order=1;
		$sort_order=@array_filter(@explode(',', $g_sort_order));
		if($sort_order){
			$sql="UPDATE `blog_category` SET `MyOrder` = CASE `CateId`";
			foreach((array)$sort_order as $v){
				$sql.=" WHEN $v THEN ".$order++;
			}
			$sql.=" END WHERE `CateId` IN ($g_sort_order)";
			db::query($sql);
		}
		manage::operation_log('批量博客分类排序');
		ly200::e_json('', 1);
	}
	
	public static function blog_category_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$CateId=(int)$g_CateId;
		$row=str::str_code(db::get_one('blog_category', "CateId='$CateId'", 'UId'));
		$del_where=category::get_search_where_by_CateId($CateId, 'blog_category');
		db::delete('blog_category', $del_where);
		manage::operation_log('删除博客分类');
		if($row['UId']!='0,'){
			$CateId=category::get_top_CateId_by_UId($row['UId']);
			$statistic_where=category::get_search_where_by_CateId($CateId, 'blog_category');
			category::category_subcate_statistic('blog_category', $statistic_where);
		}
		ly200::e_json('', 1);
	}
	
	public static function blog_category_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="CateId in(".str_replace('-',',',$g_id).")";
		$row=str::str_code(db::get_all('blog_category', $del_where));
		db::delete('blog_category', $del_where);
		manage::operation_log('批量删除博客分类');
		foreach($row as $v){
			if($v['UId']!='0,'){
				$CateId=category::get_top_CateId_by_UId($v['UId']);
				$statistic_where=category::get_search_where_by_CateId($CateId, 'blog_category');
				category::category_subcate_statistic('blog_category', $statistic_where);
			}
		}
		ly200::e_json('', 1);
	}
	
	public static function blog_review_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$RId=(int)$g_RId;
		$RId && db::delete('blog_review', "RId='$RId'");
		manage::operation_log('删除博客评论');
		ly200::e_json('', 1);
	}
	
	public static function blog_review_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="RId in(".str_replace('-', ',' ,$g_id).")";
		db::delete('blog_review', $del_where);
		manage::operation_log('批量删除博客评论');
		ly200::e_json('', 1);
	}
	
	public static function blog_review_reply(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_RId=(int)$p_RId;
		if($p_Reply){//管理员回复
			db::update('blog_review', "RId='$p_RId'", array('Reply'=>$p_Reply,'ReplyTime'=>time()));
			manage::operation_log('修改博客评论');
		}
		ly200::e_json('', 1);
	}

	public static function partner_edit(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$PId=(int)$p_PId;
		$Url=$p_Url;
		$MyOrder=$p_MyOrder;
		$PicPath=$p_PicPath;
		$data=array(
			'Url'		=>	$Url,
			'PicPath'	=>	$PicPath,
			'AccTime'	=>	$c['time'],
			'MyOrder'	=>	(int)$MyOrder
		);
		if($PId){
			db::update('partners', "PId='$PId'", $data);
			manage::operation_log('修改友情链接');
		}else{
			db::insert('partners', $data);
			$PId=db::get_insert_id();
			manage::operation_log('添加友情链接');
		}
		manage::database_language_operation('partners', "PId='$PId'", array('Name'=>1));
		ly200::e_json('', 1);
	}
	
	public static function partner_used(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'g');
		$PId=(int)$g_PId;
		$IsUsed=(int)$g_IsUsed==1?1:0;
		!$PId && ly200::e_json('请勿非法操作');
		db::update('partners', "PId='$PId'", array('IsUsed'=>$IsUsed));
		manage::operation_log(($IsUsed==1?'启用':'关闭').'友情链接');
		$Msg=$c['manage']['lang_pack']['msg'][($IsUsed==1?'open_success':'close_success')];
		ly200::e_json($Msg, 1);
	}
	
	public static function partner_del(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$PId=(int)$g_PId;
		db::delete('partners', "PId='$PId'");
		manage::operation_log('删除友情链接');
		ly200::e_json('', 1);
	}
	
	public static function partner_del_bat(){
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		!$g_id && ly200::e_json('');
		$del_where="PId in(".str_replace('-',',',$g_id).")";
		db::delete('partners', $del_where);
		manage::operation_log('批量删除友情链接');
		ly200::e_json('', 1);
	}

	public static function mailchimp_template_choice(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//$p_Type 模板分类文件夹; $p_Template 模板文件夹
		$p_Template=(int)$p_Template;
		$templates_data=mailchimp::get_info('/templates/'.$p_Template);
		if(!$templates_data['error']){
			$PicPath=$templates_data['response']['thumbnail'];
			$data=array(
				'PicPath'	=>	$PicPath,
				'Id'		=>	$templates_data['response']['id'],
			);
			ly200::e_json($data,1);
		}else{
			ly200::e_json('',0);
		}
	}

	public static function mailchimp_send(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$Id	= $p_Id; //模板ID
		//通过GET,获取联系人列表list
		$lists_data=get_info('/lists');
		$members_ary=array();
		if(!$lists_data['error']){ // 获取lists成功
			foreach((array)$lists_data['response']['lists'] as $k=>$v){
				$members_url='/lists/'.$v['id'].'/members';
				$members_data=get_info('/lists/'.$v['id'].'/members'); // 根据列表获取联系人
				if(!$members_data['error']){ // 获取lists中members列表成功
					foreach((array)$members_data['response']['members'] as $k1=>$v1){
						$members_ary[$v['id']][]=array(
							'email_address'	=>	$v1['email_address'],
							'status'		=>	$v1['status'],
						);
					}
				}else{
					echo '<script>alert("'.$members_data['detail'].'");</script>';
					exit();
				}
			}
		}else{
			echo '<script>alert("'.$lists_data['detail'].'");</script>';
			exit();
		}
		if(!$Id){
			ly200::e_json('邮件模板未选择！');
		}

		//发送邮件
		ly200::sendmail($ToAry[0], $SubjectAry[0], $ContentsAry[0], 1);
		manage::operation_log('发送邮件');
		manage::email_log($ToAry[0], $SubjectAry[0], $ContentsAry[0]);//邮件发送记录
		if((int)$p_Arrival){//更新到货通知信息
			db::update('arrival_notice', "AId='{$p_Arrival}'", array('IsSend'=>1, 'SendTime'=>$c['time']));
		}
		ly200::e_json('发送成功！', 1);
	}
}