<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class account_module{
	public static function login(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');

		$data=array(
			'Action'	=>	'ueeshop_web_manage_login',
			'UserName'	=>	$p_UserName,
			'Password'	=>	ly200::password(trim($p_Password)),
			'Ip'		=>	ly200::get_ip()
		);
		$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
// 		$result=array (
// 		    'msg' =>
// 		    array (
// 		        'UserName' => 'zhenghongzhi',
// 		        'Password' => '6844725d241770a95d81cefb81d5f1f4',
// 		        'LastLoginTime' => 1571468300,
// 		        'LastLoginIp' => '183.234.189.178',
// 		        'Locked' => '0',
// 		        'GroupId' => '1',
// 		        'AccTime' => '1570970778',
// 		    ),
// 		    'ret' => 1,
// 		);
		if($result['ret']==1 && $result['msg'] && @is_array($result['msg'])){
			$userinfo=$result['msg'];
			if($userinfo['Locked']==1){	//账号被锁定
				ly200::e_json(manage::language('{/account.locking/}'));
			}else{
				$_SESSION['Manage']=@is_array($_SESSION['Manage'])?@array_merge($_SESSION['Manage'], $userinfo):$userinfo;
				//---------------------------------非超级管理员加载权限(Start)--------------------------------------
				if($userinfo['GroupId']!=1){
					$permit_row=db::get_all('manage_permit', "UserName='{$userinfo['UserName']}'");
					foreach($permit_row as $v){
						$_SESSION['Manage']['Permit'][$v['Module']]=array(
							0=>$v['Permit'],
							1=>@json_decode(stripslashes($v['DetailsPermit']), true)
						);
					}
				}
				//---------------------------------非超级管理员加载权限(End)--------------------------------------
				$_SESSION['Manage']['GroupId']==3 && $_SESSION['Manage']['SalesId']=db::get_value('manage_sales', "UserName='{$_SESSION['Manage']['UserName']}'", 'SalesId');
				manage::operation_log("管理员登录【{$p_UserName}】");

                $cache_time=db::get_value('config', 'GroupId="tmp" and Variable="CacheTime"', 'Value');
                $set_cache=$c['time']-$cache_time-$c['manage']['cache_timeout'];
                if($set_cache>0){//超出缓存保存期限
                    file::del_dir($c['tmp_dir'].'manage/');//清空后台数据统计
                    file::del_dir($c['tmp_dir'].'photo/');//清空图片临时文件夹
                    self::sitemap_create();//更新网站地图
                    manage::config_operaction(array('CacheTime'=>$c['time']), 'tmp');
                }
                //清除掉多余的 颜色数据 and 选项卡数据
                $delete_time=3600*24;//一天
                db::delete('products_color', "ProId=0 and AccTime+{$delete_time}<{$c['time']}");
                db::delete('products_attribute', "CateId='|0|' and DescriptionAttr=1 and AccTime+{$delete_time}<{$c['time']}");
                db::delete('products_tab', "ProId=0 and AccTime+{$delete_time}<{$c['time']}");

                //自动取消订单
                $offline=db::get_all('payment', 'IsOnline=0', 'PId');
                $pid_str='';
                foreach((array)$offline as $k => $v){
                    $pid_str.=($k?',':'').$v['PId'];
                }
                $pid_str && $pid_str=' and PId not in ('.$pid_str.')';
                $time=86400*30;//一天*日数
                db::update('orders', "({$c['time']}-OrderTime)>{$time} and OrderStatus<4{$pid_str}", array('OrderStatus'=>7));

                ly200::e_json('', 1);
            }
        }else{
            ly200::e_json(manage::language('{/account.error/}'));
        }
    }

    public static function logout(){
        global $c;
        $log="退出登录【{$_SESSION['Manage']['UserName']}】";
        manage::operation_log($log);
        unset($_SESSION['Manage']);
        js::location('./');
    }

    public static function video_play(){
        global $c;
        @extract($_GET, EXTR_PREFIX_ALL, 'p');
        if($p_alid){
            $data=array(
                'Action'	=>	'ueeshop_kuihuadao_get_play_auth',
                'vid'		=>	$p_alid,
            );
            $result=ly200::api($data, $c['ApiKey'], $c['api_url']);
            ly200::e_json($result['msg'], 1);
        }else{
            echo 0;
        }
    }

    public static function mp3_play(){
        global $c;
        @extract($_GET, EXTR_PREFIX_ALL, 'p');
        $order_where='';
        (int)$_SESSION['Manage']['GroupId']==3 && $order_where.=" and ((o.SalesId>0 and o.SalesId='{$_SESSION['Manage']['UserId']}') or (o.SalesId=0 and u.SalesId='{$_SESSION['Manage']['UserId']}'))";//业务员账号过滤
        $order_wait_payment_count = (int)db::get_row_count('orders o left join user u on o.UserId=u.UserId', 'o.OrderStatus=1'.$order_where);
        $order_wait_delivery_count = (int)db::get_row_count('orders o left join user u on o.UserId=u.UserId', 'o.OrderStatus=4'.$order_where);
        $prod_warning_count = (int)db::get_row_count('products', "Stock<={$c['manage']['warning_stock']}" . $c['manage']['where']['products']);
        if((int)$c['FunVersion'] && manage::check_permit('user') && manage::check_permit('user', 0, array('a'=>'inbox'))){
            if(in_array('product_inbox', (array)$c['manage']['plugins']['Used'])){//APP应用开启产品咨询
                $unread_where='(Module in ("products", "others") and IsRead=0 and UserId>0) or (Module="products" and IsRead=0 and UserId=0 and CusEmail!="")';
            }else{
                $unread_where='Module="others" and IsRead=0 and UserId>0';
            }
            $inbox_noread_count=(int)db::get_row_count('user_message', $unread_where);
        }
        $inbox_where='';
        (int)$_SESSION['Manage']['GroupId']==3 && $inbox_where.=" and (u.SalesId>0 and u.SalesId='{$_SESSION['Manage']['UserId']}')";//业务员账号过滤
        $inbox_noread_orders_count=(int)db::get_row_count('user_message m left join user u on m.UserId=u.UserId', 'Module="orders" and Type=0 and IsRead=0'.$inbox_where);
        $caigou_count=(int)db::get_row_count('request_message m left join user u on m.UserId=u.UserId', 'Module="purchase" and Type=0 and IsRead=0'.$inbox_where);
        $error_count=$order_wait_payment_count+$order_wait_delivery_count+$prod_warning_count+$inbox_noread_count+$caigou_count;
        $result['msg']=$error_count;
        $d=array();
        $d["order_wait_payment_count"]=$order_wait_payment_count;
        $d["order_wait_delivery_count"]=$order_wait_delivery_count;
        $d["prod_warning_count"]=$prod_warning_count;
        $d["inbox_noread_count"]=$inbox_noread_count;
        $d["caigou_count"]=$caigou_count;
        $result['_data']=$d;
        ly200::e_json($result, 1);

    }

    public static function ueeshop_web_get_data(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_key=(int)$p_key;
        $day_ary=array(30,1,2,7,15,30);
        $p_day=$day_ary[$p_key];
        !$p_day && $p_key=0 && $p_day=$day_ary[0];

        if(substr_count($_SERVER['HTTP_HOST'], '.vgcart.com') || substr_count($_SERVER['HTTP_HOST'], '.ly200.net')){//substr_count($_SERVER['HTTP_HOST'], '.myueeshop.com') || substr_count($_SERVER['HTTP_HOST'], '.ueeshopweb.com') ||
            $data_object=@file_get_contents($c['root_path'].'/inc/file/analytics.json');
            $analytics_row=str::json_data($data_object, 'decode');
        }else{
            $analytics_row=ly200::ueeshop_web_get_data();//加载统计json
        }

        $data=array(
            'key'	=>	$p_key,
            'ip'	=>	(int)$analytics_row['Analytics'][0][$p_day]['Ip'],
            'pv'	=>	(int)$analytics_row['Analytics'][0][$p_day]['Pv']
        );

        $area_ary=$source_ary=array();
        $other=0;
        if((int)$data['ip']){//国家地区分布
            foreach((array)$analytics_row['Analytics'][1][$p_day] as $k=>$v){
                if(sprintf('%01.4f', $v/$data['ip'])<0.001){
                    $other+=$v;
                }else{
                    $area_ary[]=array(
                        'country'	=>	$k,
                        'pv'		=>	$v,
                        'rate'		=>	sprintf('%01.4f', $v/$data['ip'])
                    );
                }
            }
            $area_ary[]=array(
                'country'	=>	'其他',
                'pv'		=>	$other,
                'rate'		=>	sprintf('%01.4f', $other/$data['ip'])
            );
        }
        if((int)$data['pv']){//搜索引擎分布
            foreach((array)$analytics_row['Analytics'][2][$p_day] as $k=>$v){
                $source_ary[]=array(
                    'engine'	=>	$k,
                    'pv'		=>	$v,
                    'rate'		=>	sprintf('%01.4f', $v/$data['pv'])
                );
            }
        }
        $data['area']=$area_ary;
        $data['source']=$source_ary;

        //$StartTime=$c['time']-$p_day*86400;
        $StartTime=$c['time']-($p_day-1)*86400;
        $StartTime=@strtotime(@date('Y/m/d', $StartTime).' 00:00:00');
        $EndTime=$c['time'];
        $p_key==2 && $EndTime=@strtotime(@date('Y/m/d', $c['time']).' 00:00:00');//昨天
        $where=$p_key==5?1:"OrderTime>{$StartTime} and OrderTime<{$EndTime}";
        /* 总订单数 */
        $order_total_count=(int)db::get_row_count('orders', $where);
        /* 总销售额 */
        //$order_total_row=db::get_all('orders', $where." and OrderStatus in(4,5,6)", 'sum((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*(1-CouponDiscount))+ShippingPrice+PayAdditionalFee+ShippingInsurancePrice) as TotalPrice');
        $order_total_row=db::get_all('orders', $where." and OrderStatus in(4,5,6)", 'sum(((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*(1-CouponDiscount))+ShippingPrice+ShippingInsurancePrice-CouponPrice-DiscountPrice)*(1+PayAdditionalFee/100)+PayAdditionalAffix) as TotalPrice');
        $order_total_price=0;
        foreach($order_total_row as $k=>$v){
            $order_total_price+=sprintf('%0d', $v['TotalPrice']);
        }
        $s_order_total_price=$order_total_price;
        $order_total_price>9999 && $s_order_total_price=substr($order_total_price, 0, -3).'k';

        $data['order_total_count']=$order_total_count;
        $data['order_total_price']=$c['manage']['currency_symbol'].$order_total_price;
        $data['s_order_total_price']=$c['manage']['currency_symbol'].$s_order_total_price;

        ly200::e_json($data, 1);
    }

    public static function get_account_data(){
        global $c;
        $data=self::get_account_data_class();
        ly200::e_json($data, 1);
    }

    //站群系统也需要对接该数据，整合为公用类
    public static function get_account_data_class(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_TimeS=$p_TimeS!=''?$p_TimeS:0;
        $days_ary=array(0, -1, -7, -30);
        if(!file::check_cache($c['root_path']."tmp/manage/get_account_data{$p_TimeS}.json", 0)){
            ob_start();
            $time_s=where::time($p_TimeS, '', !in_array($p_TimeS, array(0,-1)));
            $data=array(
                'orders'	=>	array(
                    'information'	=>	array(),
                    'country'		=>	array()
                ),
                'products'	=>	array(
                    'sales'			=>	array(),
                    'search'		=>	array()
                ),
                'conversion'=>	array(
                    'ratio'			=>	array(),
                    'share_platform'=>	array(),
                    'terminal'		=>	array()
                ),
                'visits'	=>	array(),
                'referrer'	=>	array(),
                'time_s'	=>	date('Y-m-d', $time_s[0]).date('/Y-m-d', $time_s[1]),
                'time_e'	=>	''
            );
            $w="OrderStatus in(4,5,6) and (OrderTime between {$time_s[0]} and {$time_s[1]})";
            $p_Terminal>=1 && $w.=' and Source='.($p_Terminal-1);

            $total_field = 'sum(((ProductPrice*((100-Discount)/100)*(if(UserDiscount>0, UserDiscount, 100)/100)*(1-CouponDiscount))+ShippingPrice+ShippingInsurancePrice-CouponPrice-DiscountPrice)*(1+PayAdditionalFee/100)+PayAdditionalAffix) as total';
            $order_count = 'count(OrderId) as order_count';

            /*
            //获取最近六个月的时间戳
            $time_ary = array();
            $year = date('Y', $c['time']);
			$month = date('m', $c['time']);
			$k = $month + 1;
			$k > 12 && $k = 1;
			for ($i = 0; $i < 12; $i++) {
				$y = $k > $month ? ($year - 1) : $year;
				$s_time = @strtotime("{$y}-{$k}-01");
				$e_year = $y;
				$e_month = $k + 1;
				$e_month > 12 && $e_year = $e_year + 1;
				$e_month > 12 && $e_month = 1;
				$e_time = @(int)strtotime("{$e_year}-{$e_month}-01")-1;
				if ($i > 5) {
                    //最近六个月
                    $title = ($c['manage']['config']['ManageLanguage']=='en' ? @date('F', $s_time) : @sprintf($c['manage']['lang_pack']['mta']['account_month'], $k));
                    $time_ary[] = array('Title'=>$title, 'Start'=>$s_time, 'End'=>$e_time);
				}
				if ($k == 12) $k = 1; else $k++;
			}
            */

            $time_ary = array();
            if ($p_TimeS == '0' || $p_TimeS == '-1') {
                //今天 or 昨天
                if ($p_TimeS == '0') {
                    //今天
                    $date = date('Y-m-d', $c['time']);
                    $hour = date('H', $c['time']);
                }else {
                    //昨天
                    $date = date('Y-m-d', $c['time']);
                    $yesterday = (int)strtotime("{$date} 00:00:00")-1;
                    $date = date('Y-m-d', $yesterday);
                    $hour = 0;
                }
                for ($i = 0; $i < 24; $i++) {
                    if ($p_TimeS == '-1' || ($p_TimeS == '0' && $i <= $hour)) {
                        $s_time = @strtotime("{$date} {$i}:00:00");
                        $e_time = @strtotime("{$date} {$i}:59:59");
                        $title = @date('H:i', $s_time);
                        $time_ary[] = array('Title'=>$title, 'Start'=>$s_time, 'End'=>$e_time);
                    }
                }
            } elseif ($p_TimeS == '-7' || $p_TimeS == '-30') {
                //过去7天 or 过去30天
                $count = 8;
                if($p_TimeS == '-30') $count = 31;
                for ($i = 0; $i < $count; $i++) {
                    $s_time = $time_s[0] + 86400 * $i;
                    $e_time = $s_time +86399;;
                    $title = @date('m/d', $s_time);
                    $time_ary[] = array('Title'=>$title, 'Start'=>$s_time, 'End'=>$e_time);
                }
            }

            //总销售量
            $data['total']['orders_sales'] = 0;
            $data['orders_sales_charts']['colors'][0] = '#27ca9f';
            $data['orders_sales_charts']['tooltip']['valuePrefix'] = $c['manage']['currency_symbol'];
            $data['orders_sales_charts']['series'][0] = array('name'=>$c['manage']['lang_pack']['mta']['order']['order_price']); //订单金额
            foreach ((array)$time_ary as $v) {
                $order_row = db::get_one('orders', "OrderStatus in(4,5,6) and (OrderTime between {$v['Start']} and {$v['End']})", "$total_field");
                $data['orders_sales_charts']['xAxis']['categories'][] = $v['Title'];
                $data['orders_sales_charts']['series'][0]['data'][] = (float)sprintf('%01.2f', $order_row['total']);
                $data['total']['orders_sales'] += (float)sprintf('%01.2f', $order_row['total']);
            }
            $data['total']['orders_sales'] = $c['manage']['currency_symbol'] . ' ' . $data['total']['orders_sales']; //加上货币符号

            //流量
            $data['total']['session'] = 0;
            $data['total']['uv'] = 0;
            $data['session_charts']['series'][0]=array('name'=>$c['manage']['lang_pack']['mta']['conversion']['conversation']); //会话
            $data['session_charts']['series'][1]=array('name'=>$c['manage']['lang_pack']['mta']['conversion']['visitors']); //访客
            $result = ly200::api(array('Action'=>'ueeshop_analytics_get_session_data', 'TimeS'=>$p_TimeS, 'Terminal'=>0), $c['ApiKey'], $c['api_url']);
            if ($p_TimeS == '0' || $p_TimeS == '-1') {
                //今天 or 昨天
                foreach ((array)$time_ary as $v) {
                    $session = $uv = 0;
                    if ($result['msg'][$v['Start']]) {
                        $session = (int)$result['msg'][$v['Start']]['TSession'];
                        $uv = (int)$result['msg'][$v['Start']]['TUv'];
                    }
                    $data['session_charts']['xAxis']['categories'][] = $v['Title'];
                    $data['session_charts']['series'][0]['data'][] = $session;
                    $data['session_charts']['series'][1]['data'][] = $uv;
                    $data['total']['session'] += $session;
                    $data['total']['uv'] += $uv;
                }
            } else {
                //过去7天 or 过去30天
                $session_ary = array();
                foreach ((array)$result['msg'] as $k => $v) {
                    $date = @date('m/d', $k);
                    $session_ary[$date]['session'] += $v['TSession'];
                    $session_ary[$date]['uv'] += $v['TUv'];
                }
                foreach ((array)$time_ary as $v) {
                    $session = (int)$session_ary[$v['Title']]['session'];
                    $uv = (int)$session_ary[$v['Title']]['uv'];
                    $data['session_charts']['xAxis']['categories'][] = $v['Title'];
                    $data['session_charts']['series'][0]['data'][] = $session;
                    $data['session_charts']['series'][1]['data'][] = $uv;
                    $data['total']['session'] += $session;
                    $data['total']['uv'] += $uv;
                }
            }
            /*
			//流量
			foreach($days_ary as $k=>$v){
				$visits=ly200::api(array('Action'=>'ueeshop_analytics_get_visits_data', 'TimeS'=>$v, 'Terminal'=>0), $c['ApiKey'], $c['api_url']);
				$data['visits'][$k]=array(
					'key'	=>	$days_ary[$k],
					'title'	=>	$c['manage']['lang_pack']['account']['day_ary'][$k],
					'pv'	=>	(int)$visits['msg']['total']['pv'],
					'uv'	=>	(int)$visits['msg']['total']['uv']
				);
			}
			
			//流量统计
			$data['visits_charts']['colors'][0]='#4489ff';
			$data['visits_charts']['series'][0]=array(
				'name'		=>	'Pv',
				'pointWidth'=>	15
			);
			$visits=ly200::api(array('Action'=>'ueeshop_analytics_get_analytics_month_data', 'Terminal'=>0), $c['ApiKey'], $c['api_url']);
			if(is_array($visits['msg'])){
				$j=0;
				for($i=count($visits['msg'])-1; $i>=0; --$i){
					$visits_ary[$j]=$visits['msg'][$i];
					++$j;
				}
				foreach($visits_ary as $k=>$v){
					$month=@date('m', $v['AccTime']);
					$data['visits_charts']['series'][0]['data'][]=$v['PcPv']+$v['MobilePv'];
					$data['visits_charts']['xAxis']['categories'][]=($c['manage']['config']['ManageLanguage']=='en'?@date('F', $v['AccTime']):@sprintf($c['manage']['lang_pack']['mta']['account_month'], $month));
					
				}
			}
            */

            //总订单数
            $data['total']['orders_count'] = 0;
            $data['orders_count_charts']['colors'][0] = '#4489ff';
            $data['orders_count_charts']['tooltip']['valueSuffix'] = ' '.$c['manage']['lang_pack']['account']['order_item'];
            $data['orders_count_charts']['series'][0] = array('name'=>$c['manage']['lang_pack']['mta']['order']['order_count']); //订单数
            foreach ((array)$time_ary as $v) {
                $order_row=db::get_one('orders', "OrderStatus in(4,5,6) and (OrderTime between {$v['Start']} and {$v['End']})", "$order_count");
                $data['orders_count_charts']['xAxis']['categories'][] = $v['Title'];
                $data['orders_count_charts']['series'][0]['data'][] = (float)sprintf('%01.2f', $order_row['order_count']);
                $data['total']['orders_count'] += (float)sprintf('%01.2f', $order_row['order_count']);
            }

            //转化率
            $return_data['session']=0;
            $result=ly200::api(array('Action'=>'ueeshop_analytics_get_visits_conversion_data', 'TimeS'=>$p_TimeS, 'Terminal'=>0), $c['ApiKey'], $c['api_url']);
            if($result['ret']==1 && $result['msg']['total']['Session']){//有流量才进入统计
                $key='total';
                $value='ratio';
                $return_data['session']=$result['msg'][$key]['Session'];

                if($return_data['session']){//有流量才进入统计
                    $addtocart=(int)$result['msg'][$key]['1']['TotalSession']+(int)$result['msg'][$key]['2']['TotalSession']+(int)$result['msg'][$key]['3']['TotalSession'];
                    $return_data['addtocart']=$addtocart;
                    $return_data['addtocart_ratio']=$return_data['session']?sprintf('%01.1f', $addtocart*100/$return_data['session']):0;

                    $return_data['placeorder']=(int)$result['msg'][$key]['5']['TotalSession'];
                    $return_data['placeorder_ratio']=$addtocart?sprintf('%01.1f', $return_data['placeorder']*100/$addtocart):0;

                    $return_data['complete']=(int)$result['msg'][$key]['6']['TotalSession'];
                    $return_data['complete_ratio']=$return_data['placeorder']?sprintf('%01.1f', $return_data['complete']*100/$return_data['placeorder']):0;

                    $total = (int)$return_data['addtocart'] + (int)$return_data['placeorder'] + (int)$return_data['complete'];
                    $return_data['total'] = $return_data['session']?sprintf('%01.1f', $total*100/$return_data['session']):0;
                }
            }
            $data['conversion']['ratio']=$return_data;

            //流量来源 And 社交平台 And 来源网址
            $share_ary=$referrer_ary=array();
            $share_platform_name=array('facebook', 'twitter', 'google', 'pinterest', 'linkedin', 'digg', 'reddit', 'blogger', 'vk', 'youtube', 'instagram');
            $share_platform=array('facebook', 'twitter', 'plus.google.com', 'pinterest', 'linkedin', 'digg', 'reddit', 'blogger', 'vk.com', 'youtube', 'instagram');
            $visits_referrer=ly200::api(array('Action'=>'ueeshop_analytics_get_visits_referrer_data', 'TimeS'=>$p_TimeS, 'Terminal'=>0), $c['ApiKey'], $c['api_url']);
            if($visits_referrer['msg']){
                if(is_array($visits_referrer['msg']['detail'])){
                    foreach((array)$visits_referrer['msg']['detail']['share_platform'] as $k=>$v){
                        foreach((array)$share_platform as $k2=>$v2){
                            if(substr_count($v['title'], $v2)){
                                $share_ary['uv'][$share_platform_name[$k2]]+=$v['uv'];
                                break;
                            }
                        }
                        $referrer_ary['uv'][$v['title']]+=$v['uv'];
                    }
                    $uv=(int)$visits_referrer['msg']['total']['share_platform']['uv'];
                    $uv==0 && $uv=0;
                    foreach((array)$share_ary['uv'] as $k=>$v){
                        $share_ary['total'][$k]=($uv?sprintf('%01.1f', ($v/$uv)*100).'%':0);
                    }
                    foreach((array)$referrer_ary['uv'] as $k=>$v){
                        $referrer_ary['total'][$k]=($uv?sprintf('%01.1f', ($v/$uv)*100).'%':0);
                    }
                }
            }
            $data['conversion']['share_platform']=$share_ary;//社交平台
            $data['referrer']=$referrer_ary;//来源网址
            //流量来源
            $data['referrer_charts']['title']['floating']=true;
            $data['referrer_charts']['chart']['spacingTop']=0;
            $data['referrer_charts']['chart']['spacingBottom']=30;
            $data['referrer_charts']['colors']=array('#4388ff', '#ff870f', '#fcdb50', '#24d39a');
            $data['referrer_charts']['series'][0]['type']='pie';
            $data['referrer_charts']['series'][0]['innerSize']='80%';
            $data['referrer_charts']['series'][0]['name']='Pv';
            $visits_referrer_ary=array();
            $total=0;
            foreach($visits_referrer['msg']['total'] as $k=>$v){
                $visits_referrer_ary[]=$v['pv'];
                $total+=$v['pv'];
            }
            $max=max($visits_referrer_ary);
            $is_max=0;
            foreach($visits_referrer['msg']['total'] as $k=>$v){
                $percent=(float)($total?sprintf('%01.1f', ($v['pv']/$total)*100):'0');
                $ary=array('name'=>$v['title'], 'y'=>$percent);
                if($is_max==0 && $max==$v['pv']){
                    $ary['sliced']=true;
                    $ary['selected']=true;
                    $is_max=1;
                    $data['referrer_charts']['title']['text']="{$v['title']} {$percent}%";
                }
                $data['referrer_charts']['series'][0]['data'][]=$ary;
                ++$i;
            }

            //销量排行
            $sales_row=db::get_limit('(orders_products_list p left join orders o on p.OrderId=o.OrderId) left join products p1 on p.ProId=p1.ProId', $w." group by p.ProId", 'p.ProId, p.Name, p.PicPath, sum(p.Qty) as count, count(o.OrderId) as order_count, p1.Prefix, p1.Number', 'count desc, p.ProId desc', 0, 8);
            foreach((array)$sales_row as $k=>$v){
                $img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
                $path=ly200::str_to_url($val['Name']);
                $url='/'.$path.'_p'.sprintf('%04d', $v['ProId']).'.html';
                $data['products']['sales'][$k]=array(
                    'picture'	=>	$v['PicPath'],
                    'name'		=>	$v['Name'],
                    'count'		=>	(int)$v['count'],
                    'url'		=>	$url
                );
            }

            //订单分布
            $order_row=db::get_one('orders', $w, "$order_count");
            $data['orders']['total']=array(
                'order_count'	=>	(int)$order_row['order_count'],
            );
            $country_row=db::get_all('country', '1', 'CId,Acronym');
            $country_ary=array();
            foreach((array)$country_row as $v){
                $country_ary[$v['CId']]=strtolower($v['Acronym']);
            }
            $order_row=ly200::get_table_data_to_ary('orders', $w.' group by ShippingCId', 'ShippingCId', '', "ShippingCId,ShippingCountry,count(OrderId) as order_count");
            foreach((array)$order_row as $k=>$v){
                $data['orders']['country'][$k]=array(
                    'acronym'	=>	$country_ary[$k],
                    'title'		=>	$v['ShippingCountry'],
                    'count'		=>	(int)$v['order_count'],
                    'percent'	=>	(float)sprintf('%01.2f', ($v['order_count']/$data['orders']['total']['order_count'])*100).'%'
                );
            }

            /*
			//搜索排行
			$search_row=db::get_one('search_logs', '1', 'sum(Number+0) as search_count');
			$search_list_row=db::get_limit('search_logs', '1', '*', 'Number+0 desc', 0, 8);
			foreach((array)$search_list_row as $k=>$v){
				$data['products']['search'][$k]=array(
					'title'		=>	$v['Keyword'],
					'percent'	=>	ceil(($v['Number']/$search_row['search_count'])*100).'%',
					'count'		=>	(int)$v['Number'],
				);
			}
            */

            //访问终端
            $terminal_pc=ly200::api(array('Action'=>'ueeshop_analytics_get_visits_conversion_data', 'TimeS'=>$p_TimeS, 'Terminal'=>1), $c['ApiKey'], $c['api_url']);//PC端
            if(is_array($terminal_pc['msg']['total']) && $terminal_pc['msg']['total']['Uv']){
                $data['conversion']['terminal']['pc']=sprintf('%01.1f', ($terminal_pc['msg']['total']['Uv']/$data['conversion']['ratio']['uv'])*100).'%';
            }
            $terminal_mobile=ly200::api(array('Action'=>'ueeshop_analytics_get_visits_conversion_data', 'TimeS'=>$p_TimeS, 'Terminal'=>2), $c['ApiKey'], $c['api_url']);//移动端
            if(is_array($terminal_mobile['msg']['total']) && $terminal_mobile['msg']['total']['Uv']){
                $data['conversion']['terminal']['mobile']=sprintf('%01.1f', ($terminal_mobile['msg']['total']['Uv']/$data['conversion']['ratio']['uv'])*100).'%';
            }
            echo str::json_data($data);
            $cache_contents=ob_get_contents();
            ob_end_clean();
            file::write_file('tmp/manage/', "get_account_data{$p_TimeS}.json", $cache_contents);
        }
        //加载统计json
        $data_object=@file_get_contents($c['root_path']."tmp/manage/get_account_data{$p_TimeS}.json");
        $data=str::json_data($data_object, 'decode');
        return $data;
    }

    public static function sitemap_create(){
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        include("{$c['root_path']}/inc/class/sitemap/sitemap.inc.php");
        include("{$c['root_path']}/inc/class/sitemap/config.inc.php");
        include("{$c['root_path']}/inc/class/sitemap/url_factory.inc.php");
        $obj=new Sitemap();
        $xmlHtml='';
        //header('Content-type: text/xml');
        $xmlHtml.='<?xml version="1.0" encoding="UTF-8"?>';
        $xmlHtml.='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        //首页
        $xmlHtml.='<url>';
        $xmlHtml.='<loc>'.$obj->_escapeXML(SITE_DOMAIN).'</loc>';
        $xmlHtml.='<changefreq>weekly</changefreq>';
        $xmlHtml.='</url>';
        //产品列表页
        $row=str::str_code(db::get_all('products_category', '1', '*', $c['my_order'].'CateId asc'));
        foreach($row as $v){
            $xmlHtml.='<url>';
            $xmlHtml.='<loc>'.$obj->_escapeXML(SITE_DOMAIN.ly200::get_url($v, 'products_category', $c['manage']['web_lang'])).'</loc>';
            $xmlHtml.='<changefreq>weekly</changefreq>';
            $xmlHtml.='</url>';
        }
        //产品详细页
        $row=str::str_code(db::get_limit('products', "1 and (SoldStatus<2 && (SoldOut!=1 or (SoldOut=1 and IsSoldOut=1 and {$c['time']} between SStartTime and SEndTime)) or (SoldStatus=2 and Stock>0))", '*', $c['my_order'].'ProId desc', 0, 500));
        foreach($row as $v){
            $xmlHtml.='<url>';
            $xmlHtml.='<loc>'.$obj->_escapeXML(SITE_DOMAIN.ly200::get_url($v, 'products', $c['manage']['web_lang'])).'</loc>';
            $xmlHtml.='<changefreq>weekly</changefreq>';
            $xmlHtml.='</url>';
        }
        //信息页
        $row=str::str_code(db::get_all('article', '1', '*', $c['my_order'].'AId asc'));
        foreach($row as $v){
            if($v['Url']) continue;
            $url=$v['Url']?$v['Url']:SITE_DOMAIN.ly200::get_url($v, 'article', $c['manage']['web_lang']);
            $xmlHtml.='<url>';
            $xmlHtml.='<loc>'.$obj->_escapeXML($url).'</loc>';
            $xmlHtml.='<changefreq>weekly</changefreq>';
            $xmlHtml.='</url>';
        }
        //文章页
        // $row=str::str_code(db::get_all('info', '1', '*', $c['my_order'].'CateId asc, InfoId desc'));
        // foreach($row as $v){
        // 	$url=$v['Url']?$v['Url']:SITE_DOMAIN.ly200::get_url($v, 'info', $c['manage']['web_lang']);
        // 	$xmlHtml.='<url>';
        // 		$xmlHtml.='<loc>'.$obj->_escapeXML($url).'</loc>';
        // 		$xmlHtml.='<changefreq>weekly</changefreq>';
        // 	$xmlHtml.='</url>';
        // }
        $xmlHtml.='</urlset>';
        file::write_file('/', 'sitemap.xml', $xmlHtml);
        //manage::config_operaction(array('AccTime'=>$c['time']), 'sitemap');
        //manage::operation_log('生成网站地图');
        unset($xmlHtml);
        //ly200::e_json('', 1);
    }

    public static function update_version_status(){
        global $c;
        db::update('config', 'GroupId="global" and Variable="UpdateVersion"', array('Value'=>1));
    }

    /* SESSION过期重新验证 Start */
    public static function set_login_status(){
        global $c;
        setcookie('session_time', $c['time'], $c['time']+7200);
        ly200::e_json(array('FirstEnterManage' => (int)$_SESSION['Manage']['FirstEnterManage']), 1);
    }

    public static function check_login_status(){
        global $c;
        if($_COOKIE['session_time']) ly200::e_json('', 0);
        $html='<form id="expire_login_form">';
        $html.='<h2>'.manage::language('{/account.welcome/}').'</h2>';
        $html.='<div class="main">';
        $html.='<div class="input username"><span></span><input name="UserName" type="text" maxlength="50" value="" notnull autocomplete="off" placeholder="'.manage::language('{/account.username/}').'" readonly></div>';
        $html.='<div class="input password"><span></span><input name="Password" type="password" maxlength="50" value="" notnull autocomplete="off" placeholder="'.manage::language('{/account.password/}').'"></div>';
        $html.='<div class="tips"></div>';
        $html.='<input type="submit" class="submit btn_submit" value="'.manage::language('{/account.login_btn/}').'">';
        $html.='</div>';
        $html.='<input type="hidden" name="do_action" value="account.expire_login">';
        $html.='</form>';
        ly200::e_json($html, 1);
    }

    public static function expire_login(){
        global $c;
        if($c['NewFunVersion']>=5){
            $data=array(
                'do_action'		=>	'expire_login',
                'MobilePhone'	=>	$_POST['UserName'],
                'Password'		=>	$_POST['Password'],
            );
            $result=ly200::curl('https://www.ueeshop.com/action/web.php',$data);
            $result=str::json_data($result,'decode');
            !$result['ret'] && ly200::e_json($result['msg'], 0);
            $_POST['Data']=$result['msg'];
        }
        self::login();
    }
    /* SESSION过期重新验证 End */

    public static function products_recycle_del(){
        global $c;
        $clear_time=60*60*24*60;
        $ProId=db::get_value('products_recycle', "DelTime+{$clear_time}<{$c['time']}",'ProId');
        if($ProId && !db::get_row_count('products', "ProId='{$ProId}'")){
            //删除产品图片
            $resize_ary=$c['manage']['resize_ary']['products'];
            $row=str::str_code(db::get_one('products_recycle', "ProId='$ProId'", $c['prod_image']['sql_field']));
            for($i=0; $i<$c['prod_image']['count']; $i++){
                $PicPath=$row["PicPath_$i"];
                if(is_file($c['root_path'].$PicPath)){
                    foreach((array)$resize_ary as $v){
                        $ext_name=file::get_ext_name($PicPath);
                        file::del_file($PicPath.".{$v}.{$ext_name}");
                    }
                    file::del_file($PicPath);
                }
            }
            //删除产品颜色图片
            $row=str::str_code(db::get_all('products_color', "ProId='$ProId'", $c['prod_image']['sql_field']));
            foreach((array)$row as $v){
                for($i=0; $i<$c['prod_image']['count']; $i++){
                    $PicPath=$v["PicPath_$i"];
                    if(is_file($c['root_path'].$PicPath)){
                        foreach((array)$resize_ary as $v2){
                            $ext_name=file::get_ext_name($PicPath);
                            file::del_file($PicPath.".{$v2}.{$ext_name}");
                        }
                        file::del_file($PicPath);
                    }
                }
            }

            //检查产品相关的闲置属性
            $delete_ary=array();
            $selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='$ProId'"));
            foreach((array)$selected_row as $v){ $delete_ary[]=$v['AttrId']; }//等候删除的属性ID
            if(count($delete_ary)){
                foreach((array)$delete_ary as $k=>$v){
                    $pro_count=db::get_row_count('products_selected_attribute', "AttrId='$v' and ProId!='{$ProId}'  group by ProId", 'AttrId');
                    if($pro_count>0) unset($delete_ary[$k]);//还有关联的产品，不能删掉
                }
                $delete=implode(',', $delete_ary);
                if($delete){//删掉闲置没用的属性
                    db::delete('products_attribute', "AttrId in ($delete)");
                    db::delete('products_attribute_value', "AttrId in ($delete)");
                }
            }
            //检查产品相关的选项卡闲置情况
            $delete_ary=array();
            $tab_row=str::str_code(db::get_all('products_tab', "ProId='$ProId'"));
            foreach((array)$tab_row as $k=>$v){
                $delete_ary[]=$v['AttrId'];
            }
            if(count($delete_ary)){//删掉闲置没用的描述属性
                foreach((array)$delete_ary as $k=>$v){
                    $pro_count=db::get_row_count('products_tab', "AttrId='$v' and ProId!='{$ProId}'  group by ProId", 'AttrId');
                    if($pro_count>0) unset($delete_ary[$k]);//还有关联的产品，不能删掉属性本身
                }
                $delete=implode(',', $delete_ary);
                if($delete){
                    db::delete('products_attribute', "AttrId in ($delete)");
                }
            }
            //删除数据库数据
            db::delete('products_seo', "ProId='$ProId'");
            db::delete('products_development', "ProId='$ProId'");//产品功能关联表
            db::delete('products_description', "ProId='$ProId'");
            db::delete('products_selected_attr', "ProId='$ProId'");
            db::delete('products_selected_attribute', "ProId='$ProId'");
            db::delete('products_selected_attribute_combination', "ProId='$ProId'");
            db::delete('products_color', "ProId='$ProId'");
            db::delete('products_tab', "ProId='$ProId'");
            db::delete('user_favorite', "ProId='$ProId'");
            db::delete('products_recycle', "ProId='$ProId'");
            manage::operation_log('删除回收站产品-'.$ProId);
        }
    }

    public static function orders_recycle_del(){
        global $c;
        $clear_time=60*60*24*60;
        $orders_row=db::get_one('orders_recycle', "DelTime+{$clear_time}<{$c['time']}", 'OId,OrderId,OrderTime');
        $OrderId=$orders_row['OrderId'];
        if($OrderId && !db::get_row_count('orders', "OrderId='{$OrderId}'")){
            $month_dir=$c['orders']['path'].date('ym', $orders_row['OrderTime']).'/';
            file::del_dir($month_dir.$orders_row['OId'].'/');
            db::delete('orders_products_list', "OrderId={$OrderId}");
            db::delete('orders_payment_info', "OrderId={$OrderId}");
            db::delete('orders_log', "OrderId={$OrderId}");
            db::delete('orders_recycle', "OrderId={$OrderId}");
            manage::operation_log('删除回收站订单-'.$OrderId);
        }
    }

    public static function clear_first_enter_manage_session(){
        @extract($_GET, EXTR_PREFIX_ALL, 'g');
        //清理第一次进入后台的session
        if($g_type=='guide'){
            unset($_SESSION['Manage']['FirstEnterManage']);
        }else{
            unset($_SESSION['Manage']['FirstChangeThemes']);
        }
    }
}
?>