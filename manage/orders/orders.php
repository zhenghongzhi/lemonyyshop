<?php !isset($c) && exit();?>
<?php
manage::check_permit('orders', 1, array('a'=>'orders'));//检查权限
//货币汇率
$all_currency_ary=array();
$currency_row=db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach($currency_row as $k=>$v){
	$all_currency_ary[$v['Currency']]=$v;
}
//业务员
$sales_ary=array();
$manage_row=db::get_all('manage_sales');
foreach((array)$manage_row as $k=>$v){
	$sales_ary[$v['SalesId']]=$v;
}

$permit_ary=array(
	'edit'		=>	manage::check_permit('orders', 0, array('a'=>'orders', 'd'=>'edit')),
	'del'		=>	manage::check_permit('orders', 0, array('a'=>'orders', 'd'=>'del')),
	'export'	=>	manage::check_permit('orders', 0, array('a'=>'orders', 'd'=>'export')),
);
echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
?>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<?php
$top_id_name=($c['manage']['do']=='index'?'orders':'orders_inside');
$c['manage']['do']=='export' && $top_id_name='export';
?>
<style type="text/css">
.front_end{
	color: #999;
}
.front_end{
	color: #f5222d;
}
.inside_container .inside_menu > li{margin-right:0px;}
.orders_status_list .item{
	width: 11%;
}
</style>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php
	if($c['manage']['do']=='index'){
		$OrderStatus=(int)$_GET['OrderStatus'];//订单状态（搜索）
		$query_string=ly200::query_string('OrderStatus');
		$no_sort_url='?'.ly200::get_query_string(ly200::query_string('page, Sort'));
		$country_row=db::get_all('country','1','CId,Country,Acronym,FlagPath');
		$cid_country=array();
		foreach((array)$country_row as $v){
			$cid_country[$v['CId']]=$v;
		}
	?>
    	<script type="text/javascript">$(document).ready(function(){orders_obj.orders_init()});</script>
		<div class="inside_container">
			<h1>{/module.orders.orders/}</h1>
			<ul class="inside_menu">
				<li><a href="./?m=orders&a=orders"<?=$OrderStatus==0?' class="current"':'';?>>{/global.all/}</a></li>
				<?php foreach($c['orders']['status'] as $k=>$v){?>
					<li><a href="./?m=orders&a=orders&OrderStatus=<?=$k;?>" status="<?=$k;?>"<?=$OrderStatus==$k?' class="current"':'';?>>{/orders.status.<?=$k;?>/}</a></li>
				<?php }?>
			</ul>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<?php if($permit_ary['export']){?><li><a class="explode" href="./?m=orders&a=orders&d=export">{/orders.explain.export_bat/}</a></li><?php } ?>
					<li><a class="change_status" href="javascript:;">{/orders.change_status_success/}</a></li>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/set.country.country/}</label>
								<?php
								$country_where='0';
								$order_country_row=db::get_all('orders', '1 group by ShippingCId', 'ShippingCId');
								foreach($order_country_row as $v){ $country_where.=",{$v['ShippingCId']}"; }
								$country_row=str::str_code(db::get_all('country', "CId in ($country_where)", '*', 'Country asc'));
								foreach($country_row as $v){ $country_ary[$v['CId']]=$v['Country']; }
								?>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($country_ary, 'CId', (int)$_GET['CId'], '', '', '{/global.select_index/}');?></div>
								</div>
							</div>
							<?php
							if(($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)) && count($sales_ary) && (int)$_SESSION['Manage']['GroupId']!=3){//业务员
							?>
								<div class="rows item clean">
									<label>{/manage.manage.permit_name.3/}</label>
									<div class="input">
										<div class="box_select"><?=ly200::form_select($sales_ary, 'SalesId', '', 'UserName', 'SalesId', '{/global.select_index/}');?></div>
									</div>
								</div>
							<?php }?>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="orders" />
						<input type="hidden" name="a" value="orders" />
						<input type="hidden" name="OrderStatus" value="<?=$OrderStatus;?>" />
					</form>
				</div>
			</div>
			<?php
			$Keyword=str::str_code($_GET['Keyword']);
			$ShippingCId=(int)$_GET['CId'];
			$g_SalesId=(int)$_GET['SalesId'];
			$where='1';//条件
			$page_count=25;//显示数量
			/*** 通过产品搜索订单 ***/
			$Keyword && $orders_id_row=db::get_all('orders_products_list',"Name like '%$Keyword%'",'OrderId');
			$orders_id = '';
			foreach((array)$orders_id_row as $k=>$v){
				$orders_id.=($k?',':'').$v['OrderId'];
			}
			/*** 通过产品搜索订单 ***/
			$Keyword && $where.=" and (o.OId like '%$Keyword%' or o.Email like '%$Keyword%' or concat(o.ShippingFirstName, ' ', o.ShippingLastName) like '%$Keyword%' or o.TrackingNumber like '%$Keyword%'".($orders_id?" or o.OrderId in ($orders_id)":'').')';
			$ShippingCId && $where.=" and o.ShippingCId='$ShippingCId'";
			$OrderStatus && $where.=" and o.OrderStatus='$OrderStatus'";
			$g_SalesId && $where.=" and ((o.SalesId>0 and o.SalesId='$g_SalesId') or (o.SalesId=0 and u.SalesId='$g_SalesId'))";
			(int)$_SESSION['Manage']['GroupId']==3 && $where.=" and ((o.SalesId>0 and o.SalesId='{$_SESSION['Manage']['SalesId']}') or (o.SalesId=0 and u.SalesId='{$_SESSION['Manage']['SalesId']}'))";//业务员账号过滤
			$orders_row=str::str_code(db::get_limit_page('orders o left join user u on o.UserId=u.UserId', $where, 'o.*, o.SalesId as OSalesId, u.SalesId,u.yy_code', $sort_ary[$Sort].'o.OrderId desc', (int)$_GET['page'], $page_count));
			$query_string=ly200::query_string(array('m', 'a', 'd'));
			
			if($orders_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="20%" nowrap="nowrap">{/orders.oid/}</td>
							<td width="5%" nowrap="nowrap" class="center">{/orders.device/}</td>
							<td width="20%" nowrap="nowrap">{/orders.user/}</td>
							<td width="10%" nowrap="nowrap">{/orders.total_price/}</td>
							<td width="10%" nowrap="nowrap">{/orders.orders_status/}</td>
							<td width="15%" nowrap="nowrap">
								<a href="<?=$no_sort_url.'&Sort='.($Sort=='1a'?'1d':'1a');?>">{/orders.info.payment_time/}<i class="<?php if($Sort=='1d') echo 'sort_icon_arrow_down'; elseif($Sort=='1a') echo 'sort_icon_arrow_up'; else echo 'sort_icon_arrow';?>"></i></a>
							</td>
							<td width="15%" nowrap="nowrap">
								<a href="<?=$no_sort_url.'&Sort='.($Sort=='2a'?'2d':'2a');?>">{/orders.info.order_time/}<i class="<?php if($Sort=='2d') echo 'sort_icon_arrow_down'; elseif($Sort=='2a') echo 'sort_icon_arrow_up'; else echo 'sort_icon_arrow';?>"></i></a>
							</td>
							<?php if($permit_ary['edit'] || $permit_ary['copy'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($orders_row[0] as $v){
// 							$isFee=($v['OrderStatus']>=4 && $v['OrderStatus']!=7)?1:0;
							$total_price=orders::orders_price($v, 1, 1);
							$Symbol=$v['ManageCurrency']?$all_currency_ary[$v['ManageCurrency']]['Symbol']:$c['manage']['currency_symbol'];
							$SalesId=$v['OSalesId']?$v['OSalesId']:$v['SalesId'];
							$msg_count=(int)db::get_row_count('user_message', "Module='orders' and Subject='{$v['OId']}' and IsRead=0");
							if($v['yy_code']){
                                $balance = ly200::getyy_balance($v['yy_code']);
                            }else{
							    $balance = '';
                            }
						?>
							<tr>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['OrderId']);?></td><?php }?>
								<td nowrap="nowrap">
									<a href="./?m=orders&a=orders&d=view&OrderId=<?=$v['OrderId'];?>&query_string=<?=urlencode($query_string);?>" title="<?=$v['OId'];?>" class="green"><?=$v['OId'];?></a>
									<div class="sales_show" data-id="<?=$SalesId;?>"><?=$SalesId?$sales_ary[$SalesId]['UserName']:'';?></div>
								</td>
								<td nowrap="nowrap" class="center"><img src="/static/manage/images/orders/source_<?=$v['Source']; ?>.png" alt=""></td>
								<td nowrap="nowrap">
									<a href="./?m=operation&a=email&d=send&Email=<?=$v['Email'];?>" class="btn_email"><i class="icon_email"></i></a>
									<span class="green"><?=$v['Email'];?></span><br />
									<span class="gory"><?=$v['ShippingFirstName'].' '.$v['ShippingLastName'];?>  <?=$v['yy_code'];?></span><br />
									<?=$cid_country[$v['ShippingCId']]['FlagPath'] ? "<img src='{$cid_country[$v['ShippingCId']]['FlagPath']}' />" : '<div class="icon_flag flag_'.strtolower($cid_country[$v['ShippingCId']]['Acronym']).'"></div>';?>
									<span class="color_888"><?=$v['ShippingCountry'];?></span>
								</td>
								<td nowrap="nowrap"><?=$Symbol.sprintf('%01.2f', $total_price);?><? if($balance){ ?><br>余额：<?=$balance; ?><? } ?></td>
								<td nowrap="nowrap"><?=str::str_color('{/orders.status.'.$v['OrderStatus'].'/}', $v['OrderStatus']);?></td>
								<td nowrap="nowrap"><?=($v['PayTime']?date('Y-m-d', $v['PayTime']).'<br />'.date('H:i:s', $v['PayTime']):'N/A')?><div class="color_888"><?=$v['PaymentMethod'];?></div></td>
								<td nowrap="nowrap"><?=date('Y-m-d', $v['OrderTime']).'<br />'.date('H:i:s', $v['OrderTime']);?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=orders&a=orders&d=view&OrderId=<?=$v['OrderId'];?>&query_string=<?=urlencode($query_string);?>">{/global.view/}</a><?php }?>
										<?php if($permit_ary['edit'] || $permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i><?=$msg_count>0?"<em></em>":'';?></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['edit']){?>
														<a class="inbox item" href="javascript:;" data-id="<?=$v['OId'];?>">{/global.message/}<?=$msg_count>0?"<em></em>":'';?></a>
														<a class="print item" href="./?m=orders&a=orders&d=print&iframe=1&OrderId=<?=$v['OrderId'];?>" target="_blank">{/global.print/}</a>
														<a class="explode item <?=$c['manage']['cdx_limit'];?>" href="./?do_action=orders.orders_export_products&OrderId=<?=$v['OrderId'];?>">{/global.export/}</a>
													<?php }?>
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=orders.orders_del&OrderId=<?=$v['OrderId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($orders_row[1], $orders_row[2], $orders_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(0);
			}?>
		</div>
		<?php /*<div id="sales_select_hide" class="hide"><div class="box_select"><?=ly200::form_select($sales_ary, 'SalesId', '', 'UserName', 'UserId', '{/global.select_index/}');?></div></div>*/?>
	<?php
	}elseif($c['manage']['do']=='view'){
		//基本信息
		$OrderId=(int)$_GET['OrderId'];
		$query_string=urldecode($_GET['query_string']);
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$OrderId'"));
		$shipping_template=(int)$orders_row['shipping_template'];
		!$orders_row && js::location('./?m=orders');
		$Symbol=$orders_row['ManageCurrency']?$all_currency_ary[$orders_row['ManageCurrency']]['Symbol']:$c['manage']['currency_symbol'];
		$total_price=orders::orders_price($orders_row, 1, 1);
		$front_products_total_price=0;
		$end_products_price=0;
		$HandingFee=$total_price-orders::orders_price($orders_row, 0, 1);
		$total_weight=$orders_row['TotalWeight'];
		// $before_orders_count=db::get_row_count('orders', "Email='{$orders_row['Email']}' and OrderId<{$OrderId}", 'OrderId');//在这订单之前，这邮箱的订单次数
		$orders_count_total=db::get_row_count('orders', "Email='{$orders_row['Email']}'", 'OrderId');//这邮箱的订单总数
		$orders_count=db::get_row_count('orders', "Email='{$orders_row['Email']}' and (OrderStatus between 4 and 6)", 'OrderId');//这邮箱的付款的订单总数
		//配送信息
		$shipping_cfg=(int)$orders_row['ShippingMethodSId']?db::get_one('shipping', "SId='{$orders_row['ShippingMethodSId']}'"):db::get_one('shipping_config', "Id='1'");
		$shipping_row=db::get_one('shipping_area', "AId in(select AId from shipping_country where CId='{$orders_row['ShippingCId']}' and  SId='{$orders_row['ShippingMethodSId']}' and type='{$orders_row['ShippingMethodType']}')");
		$shipping_ary=array(
			'OrderId'					=>	$orders_row['OrderId'],
			'ShippingMethodSId'			=>	$orders_row['ShippingMethodSId'],
			'ShippingPrice'				=>	$orders_row['ShippingPrice'],
			'ShippingInsurance'			=>	(int)$orders_row['ShippingInsurance'],
			'ShippingInsurancePrice'	=>	$orders_row['ShippingInsurancePrice'],
			'TotalWeight'				=>	$orders_row['TotalWeight'],
			'TotalVolume'				=>	$orders_row['TotalVolume'],
			'ShippingOvExpress'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvExpress']), 'decode'),
			'ShippingOvSId'				=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvSId']), 'decode'),
			'ShippingOvType'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvType']), 'decode'),
			'ShippingOvInsurance'		=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvInsurance']), 'decode'),
			'ShippingOvPrice'			=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvPrice']), 'decode'),
			'ShippingOvInsurancePrice'	=>	str::json_data(htmlspecialchars_decode($orders_row['ShippingOvInsurancePrice']), 'decode')
		);
		//Paypal地址
		$paypal_address_row=str::str_code(db::get_one('orders_paypal_address_book', "OrderId='$OrderId'"));
		//订单日志
		$orders_log_row=db::get_all('orders_log', "OrderId='{$orders_row['OrderId']}'", '*', 'LId desc');
		//订单备注日志
		$orders_remark_log_row=db::get_all('orders_remark_log', "OrderId='{$orders_row['OrderId']}'", '*', 'RId desc');
		//订单付款信息
		$payment_info=db::get_one('orders_payment_info', "OrderId='{$orders_row['OrderId']}'", '*', 'InfoId desc');
		//订单退款信息
		$orders_refund_row=db::get_one('orders_refund_info', "OrderId='{$orders_row['OrderId']}'", '*', 'ReId desc');
		//会员信息
		(int)$orders_row['UserId'] && $user_row=str::str_code(db::get_one('user', "UserId='{$orders_row['UserId']}'"));
		//订单产品信息
		$oversea_id_ary=$oversea_cid_ary=$oversea_weight_ary=array();
		$order_list_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId left join shipping s on s.SId=o.ShippingMethodSId', "o.OrderId='$OrderId'", 'o.*, o.SKU as OrderSKU, p.Prefix, p.Number, p.SKU, p.PicPath_0, p.Business, p.CateId,p.price_rate,s.Express', 'o.LId asc');
		foreach($order_list_row as $k=>$v){
			if($shipping_template){
				!in_array($v['CId'], $oversea_id_ary) && $oversea_id_ary[]=$v['CId'];
				$oversea_weight_ary[$v['CId']]+=($v['Weight']*$v['Qty']);
			}else{
				!in_array($v['OvId'], $oversea_id_ary) && $oversea_id_ary[]=$v['OvId'];
				$oversea_weight_ary[$v['OvId']]+=($v['Weight']*$v['Qty']);
			}
			$oversea_cid_ary[$v['CId']]=$v;
			$price=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1);
			$ProductPrice+=$price*$v['Qty'];
			$front_ProductPrice+=$price*$v['Qty']*$v['price_rate'];
		}
		$end_products_price=$total_price-$front_ProductPrice;
		sort($oversea_id_ary);//排列正序
		//发货地
		$overseas_ary=array();
		$overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
		foreach($overseas_row as $v){
			$overseas_ary[$v['OvId']]=$v;
		}
		//订单产品相关属性
		$CateId_ary=$prod_ovid_ary=array();
		foreach($order_list_row as $v){
			if($v['BuyType']==4){//组合促销
				$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
				if(!$package_row) continue;
				$pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
				$products_row=str::str_code(db::get_all('products', "ProId in($pro_where)", 'CateId'));
				foreach((array)$products_row as $v2){
					$CateId_ary[$v2['CateId']]="CateId like '%|{$v2['CateId']}|%'";//记录产品分类
				}
			}
			$CateId_ary[$v['CateId']]="CateId like '%|{$v['CateId']}|%'";//记录产品分类
			$prod_ovid_ary[$v['LId']]=$v['OvId'];
		}
		$attribute_ary=$vid_data_ary=array();
		$CateIdStr=implode(' or ', $CateId_ary);
		$attribute_row=str::str_code(db::get_all('products_attribute', '1'.($CateIdStr?" and $CateIdStr":''), "AttrId, Type, Name{$c['manage']['web_lang']}, ParentId, CartAttr, ColorAttr"));
		foreach($attribute_row as $v){
			$attribute_ary[$v['AttrId']]=array(0=>$v['Type'], 1=>$v["Name{$c['manage']['web_lang']}"]);
		}
		$AttrIdStr=implode(',', array_keys($attribute_ary));
		$value_row=str::str_code(db::get_all('products_attribute_value', '1'.($AttrIdStr?" and AttrId in($AttrIdStr)":''), '*', $c['my_order'].'VId asc')); //属性选项
		foreach($value_row as $v){
			$vid_data_ary[$v['AttrId']][$v['VId']]=$v["Value{$c['manage']['web_lang']}"];
		}
		//下单所相应的货币价格
		$web_price_ary=array();
		if($orders_row['Currency']!=$orders_row['ManageCurrency']){
			$web_symbol=$all_currency_ary[$orders_row['Currency']]['Symbol'];
			$web_rate=($orders_row['Rate']?$orders_row['Rate']:$all_currency_ary[$orders_row['Currency']]['Rate']);
			$web_price_ary=array(
				'TotalPrice'			=> manage::rate_price($total_price, 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'ProductPrice'			=> manage::rate_price($orders_row['ProductPrice'], 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'ShippingPrice'			=> manage::rate_price($orders_row['ShippingPrice'], 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'ShippingInsurancePrice'=> manage::rate_price($orders_row['ShippingInsurancePrice'], 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'DiscountPrice'			=> manage::rate_price($orders_row['DiscountPrice'], 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'HandingFee'			=> manage::rate_price($HandingFee, 0, $orders_row['Currency'], $web_symbol, $web_rate),
				'CouponPrice'			=> manage::rate_price($orders_row['CouponPrice'], 0, $orders_row['Currency'], $web_symbol, $web_rate),
			);
		}
		echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
		
		//查询运费
		echo ly200::load_static('/static/js/plugin/track/17track/externalcall.js');
		$UILang=substr($c['manage']['web_lang'], 1);//UI语言
		$UILang=str_replace(array('jp','zh_tw'), array('ja','zh-tw'), $UILang);//日文、繁体中文

        //查余额
        if($user_row){
            $balance = ly200::getyy_balance($user_row['yy_code']);
        }
	?>
		<script type="text/javascript">
			<?php if($orders_row['ManageCurrency']){?>ueeshop_config.currency='<?=$all_currency_ary[$orders_row['ManageCurrency']]['Symbol'];?>';<?php }?>
			$(document).ready(function(){orders_obj.orders_view()});
		</script>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.orders.orders/}</span> 
				<span class="s_return">/ NO.#<?=$orders_row['OId'];?></span>
			</a>
			<ul class="orders_status_list clean">
				<?php
				$normal_status_ary = array(1, 2, 3, 4, 5, 6,7,8,9);
// 				$error_status_ary = array(1, 3);
// 				$cancel_status_ary = array(1, 3);
				$StatusTimeArr = array();
				foreach ($orders_log_row as $v) {
					$StatusTimeArr[$v['OrderStatus']] = $v['AccTime'];
					$v['AccTime'] ? date('F d, Y', $v['AccTime']) : 'N/A';
					$v['AccTime'] ? date('h:i A', $v['AccTime']) : 'N/A';
				}
				foreach ($c['orders']['status'] as $k => $v) {
					if ($orders_row['OrderStatus'] == 3) {
                        //支付失败
// 						if (!$StatusTimeArr[$k]) continue;
// 						if ($k > 3) break;
					} elseif ($orders_row['OrderStatus'] == 9) {
                        //订单取消
						if ((!$StatusTimeArr[$k] && $k !=9)) continue;
					} else {
// 						if ($k == 3 || $k == 7) continue;
					}
				?>
				
					<li class="item item_<?=$k;?><?=$k <= $orders_row['OrderStatus'] ? ' current' : '';?>">
						<strong>{/orders.status.<?=$k;?>/}</strong>
						<div class="border"></div>
						<div class="choice"><i></i></div>
						<div class="time"><?=$StatusTimeArr[$k] ? date('Y-m-d', $StatusTimeArr[$k]).'<br />'.date('H:i:s', $StatusTimeArr[$k]) : '';?></div>
						<div class="block_box block_left"></div>
						<div class="block_box block_right"></div>
					</li>
				<?php }?>
			</ul>
			<div class="left_container">
				<div class="left_container_side">
					<?php /***************************** 订单详情 Start *****************************/?>
					<div class="global_container box_order_info">
						<div class="big_title">
							<strong>{/orders.info.order_details/}</strong>
							<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit" id="edit_price_list" data-save="0">{/global.edit/}</a><?php }?>
						</div>
						<div class="orders_number">
							<strong>NO.#<?=$orders_row['OId'];?></strong>
							<span><?=str_replace('%time%', date('Y-m-d H:i:s', $orders_row['OrderTime']), $c['manage']['lang_pack']['orders']['info']['created_in']);?></span>
							<?php
							$SalesId = $orders_row['SalesId'] ? $orders_row['SalesId'] : $user_row['SalesId'];
							if ($SalesId && $sales_ary[$SalesId]) {
                                echo '<span>'.str_replace('%name%', $sales_ary[$SalesId]['UserName'], $c['manage']['lang_pack']['orders']['info']['belongs_to']).'</span>';
							}?>
						</div>
						<?php
						if($orders_refund_row){
						?>
							<div class="global_container orders_refund_box">
								<strong class="refund_left">{/orders.refund.status.<?=$orders_refund_row['Status'];?>/}</strong>
								<span class="refund_right">
									{/orders.refund.transaction/}: <?=$orders_refund_row['Number'];?>&nbsp;&nbsp;&nbsp;&nbsp;
									{/orders.refund.refund_amount/}: <?=$all_currency_ary[$orders_refund_row['Currency']]['Symbol'] . $orders_refund_row['Amount'];?>&nbsp;&nbsp;&nbsp;&nbsp;
									{/orders.refund.refund_time/}: <?=date('Y-m-d H:i:s', $orders_refund_row['UpdateTime']);?>
								</span>
							</div>
						<?php }?>
						<div class="orders_parent_box orders_parent_top_box mb20">
							<div class="clean mr5">
								<div class="orders_parent_box">
									<div class="global_container orders_info mr5">
										<ul class="list">
											<li class="clean">
												<strong>{/orders.total_price/}</strong>
												<span id="orders_total_price"><?=$Symbol.sprintf('%01.2f', $total_price);?></span>
												<span id="orders_total_price_web" class="web_price"><?=$web_price_ary['TotalPrice'];?></span>
											</li>
											<li class="clean">
												<strong>定金:</strong>
												<span id="orders_front_product_price" class="front_end"><?=$Symbol.sprintf('%01.2f', $front_ProductPrice);?></span>
											</li>
											<li class="clean">
												<strong>余款:</strong>
												<span id="orders_end_product_price" class="front_end"><?=$Symbol.sprintf('%01.2f',$end_products_price);?></span>
											</li>
											<li class="clean">
												<strong>{/orders.info.product_price/}:</strong>
												<span id="orders_product_price"><?=$Symbol.sprintf('%01.2f', $orders_row['ProductPrice']);?></span>
												<span id="orders_product_price_web" class="web_price"><?=$web_price_ary['ProductPrice'];?></span>
											</li>
                                            <? $res = file_get_contents("http://119.23.214.213/yy_app/finance/yyorder.php?orderid=".$orders_row['OId']);
                                                $res = json_decode($res);
//                                                print_r($res);die;
                                            ?>
                                            <li class="clean">
                                                <strong>付款申请状态:</strong>
                                                <span>
                                                    <? if($res->shenpi){
                                                        echo $res->shenpi->spno;
                                                        if($res->shenpi->status == 1){
                                                            echo '待审核';
                                                        }elseif($res->shenpi->status == 2){
                                                            echo '待支付';
                                                        }elseif($res->shenpi->status == 9){
                                                            echo '支付复核';
                                                        }elseif($res->shenpi->status == 10 && $res->shenpi->entrystatus == 0){
                                                            echo '待入账';
                                                        }elseif($res->shenpi->status == 10 && $res->shenpi->entrystatus == 1){
                                                            echo '已入账';
                                                        }elseif($res->shenpi->status == 8 && $res->shenpi->entrystatus == 1){
                                                            echo '应付未付';
                                                        }
                                                    }else{
                                                        echo '未申请';
                                                    } ?>
                                                </span>
                                                <? if($res->orderid){ ?>
                                                    <a href="http://119.23.214.213/yy_app/moblie/addshenpi.php?id=<?=$res->orderid?>" target="_blank" style="display: inline-block;text-align:center;height:40px;line-height:40px;width: 100px;background: #0cb083;color: #fff;">提交付款申请</a>
                                                <? }else{ ?>
                                                    yy1.0暂无订单数据
                                               <?  } ?>
                                            </li>
											<li class="clean">
												<strong>总{/orders.info.charges/}:</strong>
												<span id="orders_shipping_price"><?=$Symbol.sprintf('%01.2f', $orders_row['ShippingPrice']);?></span>
												<span id="orders_shipping_price_web" class="web_price"><?=$web_price_ary['ShippingPrice'];?></span>
											</li>
											<li class="clean">
												<strong>总清关费:</strong>
												<span id="orders_shipping_price"><?=$Symbol.sprintf('%01.2f', $orders_row['QingguanPrice']);?></span>
												<span id="orders_shipping_price_web" class="web_price"><?=$web_price_ary['QingguanPrice'];?></span>
											</li>
											<li class="clean">
												<strong>{/orders.info.insurance/}:</strong>
												<span id="orders_shipping_insurance_price"><?=$Symbol.sprintf('%01.2f', $orders_row['ShippingInsurancePrice']);?></span>
												<span id="orders_shipping_insurance_price_web" class="web_price"><?=$web_price_ary['ShippingInsurancePrice'];?></span>
											</li>
											<?php if($orders_row['Discount']>0){?>
												<li class="clean">
													<strong>{/orders.discount/}:</strong>
													<span id="orders_discount">-<?=(100-$orders_row['Discount']);?>%</span>
												</li>
											<?php }?>
											<?php if($orders_row['DiscountPrice']>0){?>
												<li class="clean">
													<strong>{/orders.discount/}:</strong>
													<span id="orders_discount_price">-<?=$Symbol.sprintf('%01.2f', $orders_row['DiscountPrice']);?></span>
													<span id="orders_discount_price_web" class="web_price"><?=$web_price_ary['DiscountPrice'];?></span>
												</li>
											<?php }?>
											<li class="clean">
												<strong>{/orders.user_discount/}:</strong>
												<span id="orders_user_discount">-<?=$orders_row['UserDiscount']>0?100-$orders_row['UserDiscount']:0;?>%</span>
											</li>
											<?php if($HandingFee!=0){?>
												<li class="clean">
													<strong>{/orders.info.handing_fee/}:</strong>
													<span id="orders_handing_fee"><?=$Symbol.sprintf('%01.2f', $HandingFee);?></span>
													<span id="orders_handing_fee_web" class="web_price"><?=$web_price_ary['HandingFee'];?></span>
												</li>
											<?php }?>
											<?php
											if($orders_row['CouponCode'] && ($orders_row['CouponPrice']>0 || $orders_row['CouponDiscount']>0)){
												if($orders_row['CouponPrice']>0){
													$discount_coupon=$Symbol.sprintf('%01.2f', $orders_row['CouponPrice']);
												}else{
													$discount_coupon=(100-$orders_row['CouponDiscount']*100).'%';
												}
											?>
												<li class="clean">
													<strong>{/orders.info.coupon/}:</strong>
													<span id="orders_coupon">-<?=$discount_coupon;?></span>
													<span class="web_price"><?=$web_price_ary['CouponPrice'];?></span>
													<div class="clear"></div>
													<div class="coupon_code"><?=$orders_row['CouponCode'];?></div>
												</li>
											<?php }?>
										</ul>
									</div>
								</div>
								<div class="orders_parent_box">
									<?php /***************************** 订单状态 Start *****************************/?>
									<div class="global_container orders_status ml5">
										<div class="box_status">
											<a href="javascript:;" class="btn_edit edit_btn_orders_status">{/orders.change_status/}</a>
											<span class="title">{/orders.orders_status/}</span>
											<span class="status" id="order_status_module" data-status="<?=$orders_row['OrderStatus'];?>">{/orders.status.<?=$orders_row['OrderStatus'];?>/}</span>
										</div>
										<div class="box_button clean">
											<?php if((int)@in_array('asiafly', (array)$c['manage']['plugins']['Used'])==1 && $orders_row['OrderStatus'] == 7 && !(int)$orders_row['AsiaflyStatus']){ ?>
												<?php if($permit_ary['edit']){?><a href="javascript:;" class="fl btn_global asiafly_btn" data-id="<?=$OrderId;?>" id="asiafly_btn">{/orders.asiafly.asiafly_inquiry/}</a><?php }?>
											<?php }?>
											<?php 
											$btn_ary=$c['manage']['mod_order_status'][$orders_row['OrderStatus']];
											foreach((array)$btn_ary as $k=>$v){
                                                $status_str="{/orders.status.{$v}/}";
//                                                 $v==1 && $status_str='{/orders.global.continue/}';
// 												$v==4 && $status_str='{/orders.global.paid/}';
											?>
												<input type="button" class="btn_global btn_orders_status btn_next" data-status="<?=$v;?>" value="<?=$status_str;?>" />
											<?php }?>
											<?php /*if($orders_row['OrderStatus']<6){?>
												<input type="button" class="btn_global btn_orders_status btn_cancel" data-status="7" value="{/orders.info.cancel_orders/}" />
											<?php }*/ ?>
											<?php
											if(!$orders_refund_row && $payment_info && ($orders_row['PId']==2 || ($orders_row['PId']==1 && $c['NewFunVersion']>=4))){
											?>
												<input type="button" class="btn_global btn_refund" value="{/orders.refund.refund/}" />
											<?php }?>
										</div>
										<?php if($orders_row['OrderStatus']==7){?>
											<ul class="list">
												<li class="clean">
													<strong>{/orders.info.cancel_reason/}:</strong>
													<span><?=$orders_row['CancelReason'];?></span>
												</li>
											</ul>
										<?php }?>
									</div>
									<?php /***************************** 订单状态 End *****************************/?>
									<?php /***************************** 付款信息 Start *****************************/?>
									<div class="global_container orders_payment orders ml5">
										<div class="orders_user">
											<strong><?=$orders_row['Email'];?></strong> <?=$user_row['yy_code'];?> <? if($balance){ ?><br>余额：<?=$balance; ?><? } ?><br>
											<span>(<?=$user_row['IsRegistered']==1 ? '{/user.menu.0/}' : '{/user.tourist/}'; ?>) <?=str_replace(array('%M%', '%N%'), array($orders_count_total, $orders_count), $c['manage']['lang_pack']['orders']['info']['total_orders']); ?></span>
										</div>
										<div class="orders_source">
											<img src="/static/manage/images/orders/source_small_<?=$orders_row['Source'];?>.png" alt="" />
											<?=$orders_row['IP']?'<span>IP: '.$orders_row['IP'].' <em>'.ly200::ip($orders_row['IP']).'</em></span>':'';?>
											<a href="javascript:;" class="btn_message" data-id="<?=$orders_row['OId'];?>"><i class="icon_message"></i></a>
											<a href="./?m=operation&a=email&d=send&Email=<?=$orders_row['Email'];?>" class="btn_email"><i class="icon_email"></i></a>
										</div>
										<ul class="list">
											<li><strong>{/orders.payment_method/}:</strong><span><?=$orders_row['PaymentMethod'];?></span></li>
											<?php 
											$IsOnline = (int)db::get_value('payment', "PId='{$orders_row['PId']}'", 'IsOnline');
											if ($payment_info) {
												if (!$IsOnline) {
                                                    //线下付款信息
											?>
													<li><strong>{/orders.info.payment_account/}:</strong><span><?=$payment_info['FirstName'] . ' ' . $payment_info['LastName'];?></span></li>
													<li><strong>{/orders.info.serial_number/}:</strong><span><?=$payment_info['MTCNNumber'];?></span></li>
                                                    <li><strong>{/orders.payment.money/}:</strong><span><?=$payment_info['Currency'] . ' ' . $payment_info['SentMoney'];?></span></li>
                                                    <li><strong>{/orders.payment.contents/}:</strong><span><?=$payment_info['Contents'];?></span></li>
													<li><strong>{/orders.info.payment_time/}:</strong><span><?=$payment_info['AccTime'] ? date('Y-m-d H:i:s', $payment_info['AccTime']) : 'N/A';?></span></li>
											<?php
												} else {
                                                    //线上付款信息
											?>
													<li><strong>{/orders.info.payment_account/}:</strong><span><?=$payment_info['Account'];?></span></li>
													<li><strong>{/orders.info.serial_number/}:</strong><span><?=$payment_info['MTCNNumber'];?></span></li>
													<li><strong>{/orders.info.payment_time/}:</strong><span><?=$payment_info['AccTime'] ? date('Y-m-d H:i:s', $payment_info['AccTime']) : 'N/A';?></span></li>
											<?php
												}
											}?>
											<?php
											if (!$payment_info && $paypal_address_row) {
                                                //Paypal付款信息
											?>
												<li><strong>{/orders.info.payment_account/}:</strong><span><?=$paypal_address_row['Account'];?></span></li>
												<li><strong>{/orders.info.payment_time/}:</strong><span><?=$orders_row['PayTime'] ? date('Y-m-d H:i:s', $orders_row['PayTime']) : 'N/A';?></span></li>
											<?php }?>
										</ul>
									</div>
									<?php /***************************** 付款信息 End *****************************/?>
								</div>
							</div>
						</div>
					</div>
					<?php /***************************** 订单详情 End *****************************/?>
					<?php /***************************** 商品清单 Start *****************************/?>
					<div class="global_container">
						<div class="big_title">
							<strong>{/orders.info.product_list/}</strong>
							<?php /*
							<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit" id="edit_products_list" data-save="0">{/global.edit/}</a><?php }?>
						*/?>
						
						</div>
						<form id="orders_products_form">
							<table border="0" cellpadding="5" cellspacing="0" class="r_con_table" id="orders_products_list">
								<thead>
									<tr>
										<td width="5%" nowrap="nowrap">{/global.serial/}</td>
										<td nowrap="nowrap">{/products.product/}</td>
										<td nowrap="nowrap" style="width:90px;">{/products.products.price/}</td>
										<td nowrap="nowrap" style="width:50px;">{/orders.quantity/}</td>
										<td nowrap="nowrap" style="width:50px;">定金比例</td>
										<td nowrap="nowrap" style="width:50px;">定金</td>
										<td nowrap="nowrap" style="width:50px;" class="last">{/orders.amount/}</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;
									$BId='0';
									foreach($order_list_row as $v){$v['Business'] && $BId.=','.$v['Business'];}
									$amount=$quantity=0;
									$total_pro_count=count($order_list_row);
									foreach($order_list_row as $v){
										$v['Name'.$c['manage']['web_lang']]=$v['Name'];
										if(!$v["price_rate"])
										    $v["price_rate"]=0;
										$price=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1);
										$front_amount+=$price*$v['Qty']*$v["price_rate"];//单个产品定金
										$amount+=$price*$v['Qty'];
										$subtotal=floatval(substr(sprintf('%01.3f', $price*$v['Qty']), 0, -1));  //小计
										$front_subtotal=floatval(substr(sprintf('%01.3f', $price*$v['Qty']*$v["price_rate"]), 0, -1));  //小计定金
									?>
										<tr>
											<td><?=$i++;?></td>
											<td>
												<?php 
												if($v['BuyType']==4){
													$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
													if(!$package_row) continue;
													$products_row=str::str_code(db::get_all('products', "ProId='{$package_row['ProId']}'"));
													$pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
													$pro_where=='' && $pro_where=0;
													$products_row=array_merge($products_row, str::str_code(db::get_all('products', "ProId in($pro_where)")));
													$data_ary=str::json_data(htmlspecialchars_decode($package_row['Data']), 'decode');
													// $attr=array();
													// $attr = ($v['Property']!=''?str::json_data($v['Property'], 'decode'):$data_ary[$products_row[0]['ProId']]);	
													$v['Property']!='' && $attr=ly200::get_order_property($v['Property']);
													
												?>
													<h4>[ Sales ]</h4>
													<div class="blank6"></div>
													<?php
													foreach((array)$products_row as $k2=>$v2){
														$quantity+=$v['Qty'];
														$img=ly200::get_size_img($v2['PicPath_0'], '240x240');
														$url=ly200::get_url($v2, 'products', $c['manage']['web_lang']);
													?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" alt="<?=$v2['Name'.$c['manage']['web_lang']];?>" /></a></dt>
														<dd<?=$_GET['do']=='print'?' style="padding-right:0;"':'';?>>
															<h4><a href="<?=$url;?>" title="<?=$v2['Name'.$c['manage']['web_lang']]?>" class="green" target="_blank"><?=$v2['Name'.$c['manage']['web_lang']];?></a></h4>
															<?=$v2['Number']!=''?'<p>{/products.products.number/}: '.$v2['Prefix'].$v2['Number'].'</p>':'';?>
															<?=$v2['SKU']!=''?'<p>{/products.products.sku/}: '.$v2['SKU'].'</p>':'';?>
															<?php if($k2==0){?>
																<div>
																	<?php
																	foreach((array)$attr as $k=>$z){
																		if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
																		echo '<p class="attr_'.$k.'">'.($k=='Overseas'?'{/shipping.area.ships_from/}':$attribute_ary[$k][1]).': '.$vid_data_ary[$k][$z].'</p>';
																	}
																	if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
																		echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']].'</p>';
																	}?>
																</div>
															<?php }elseif($data_ary[$v2['ProId']]){?>
																<div>
																	<?php
																	$OvId=0;
																	foreach((array)$data_ary[$v2['ProId']] as $k3=>$v3){
																		if($k3=='Overseas'){ //发货地
																			$OvId=str_replace('Ov:', '', $v3);
																			if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $OvId==1) continue; //发货地是中国，不显示
																			echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']].'</p>';
																		}else{
																			echo '<p class="attr_'.$k3.'">'.$attribute_ary[$k3][1].': '.$vid_data_ary[$k3][$v3].'</p>';
																		}
																	}
																	if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $OvId==1){
																		echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']].'</p>';
																	}
																	?>
																</div>
															<?php }?>
														</dd>
													</dl>
													<div class="blank6"></div>
													<?php }?>
													<?php if($v['Remark']){?><dl><dd><p class="remark">{/orders.global.remark/}: <?=$v['Remark'];?></p></dd></dl><?php }?>
												<?php 
												}else{
													$attr=str::json_data($v['Property'], 'decode');
													!$attr && $attr=str::json_data($v['Property'], 'decode');
													$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
													$quantity+=$v['Qty'];
													$SKU=$v['OrderSKU']?$v['OrderSKU']:$v['SKU'];
													$img=@is_file($c['root_path'].$v['PicPath'])?$v['PicPath']:ly200::get_size_img($v['PicPath_0'], '240x240');
												?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" title="<?=$v['Name']?>" /></a></dt>
														<dd<?=$_GET['do']=='print'?' style="padding-right:0;"':'';?>>
															<h4><a href="<?=$url;?>" title="<?=$v['Name']?>" class="green" target="_blank"><?=$v['Name'];?></a></h4>
															<?php
															echo $v['Number']!=''?'<p>{/products.products.number/}: '.$v['Prefix'].$v['Number'].'</p>':'';
															echo $SKU!=''?'<p>{/products.products.sku/}: '.$SKU.'</p>':'';
															foreach((array)$attr as $k=>$vv){
																if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
																echo '<p>'.($k=='Overseas'?'{/shipping.area.ships_from/}':$k).': '.$vv.'</p>';
															}
															if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
																echo '<p>{/shipping.area.ships_from/}: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']].'</p>';
															}
															echo $v['Remark']?'<p>{/orders.global.remark/}: '.$v['Remark'].'</p>':'';
															?>
														</dd>
													</dl>
												<?php }?>
											</td>
											<td data-type="price">
												<div class="show_txt"><?=$Symbol.sprintf('%01.2f', $price);?></div>
												<div class="show_input"><span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="Price[]" value="<?=sprintf('%01.2f', $price);?>" size="4" maxlength="10" rel="amount" disabled /></span></div>
												<?php if($web_price_ary){?>
													<div class="show_web"><?=manage::rate_price($price, 0, $orders_row['Currency'], $web_symbol, $web_rate);?></div>
												<?php }?>
											</td>
											<td data-type="qty">
												<div class="show_txt"><?=$v['Qty'];?></div>
												<div class="show_input"><input type="text" class="box_input" name="Qty[]" value="<?=$v['Qty'];?>" size="4" maxlength="8" disabled /><input type="hidden" name="LId[]" value="<?=$v['LId'];?>" /></div>
											</td>
											<td data-type="price_rate">
											<span><?=empty($v['price_rate'])?0:($v['price_rate']*100)."%";?></span>
											</td>
											<td data-type="front_amount">
												<span><?=$Symbol.$front_subtotal;?></span>
											</td>
											<td data-type="amount">
												<span><?=$Symbol.$subtotal;?></span>
												<?php if($web_price_ary){?>
													<div class="show_web"><?=manage::rate_price($subtotal, 0, $orders_row['Currency'], $web_symbol, $web_rate);?></div>
												<?php }?>
												<?php if($permit_ary['del'] && $total_pro_count>1){?><a href="javascript:;" class="del" rel="del" style="display:none;">{/global.del/}</a><?php }?>
											</td>
										</tr>
									<?php }?>
									<tr>
										<td colspan="3">&nbsp;</td>
										<td><?=$quantity;?></td>
										<td></td>
										<td><span><?=$Symbol.substr(sprintf('%01.3f', $front_amount), 0, -1);?></span></td>
										<td>
											<span><?=$Symbol.substr(sprintf('%01.3f', $amount), 0, -1);?></span>
											<?php if($web_price_ary){?>
												<div class="show_web"><?=manage::rate_price(substr(sprintf('%01.3f', $amount), 0, -1), 0, $orders_row['Currency'], $web_symbol, $web_rate);?></div>
											<?php }?>
										</td>
									</tr>
								</tbody>
							</table>
							<input type="hidden" id="OrderId" name="OrderId" value="<?=$OrderId;?>" />
							<input type="hidden" name="do_action" value="orders.orders_prod_edit_edit" />
						</form>
					</div>
					<?php /***************************** 商品清单 End *****************************/?>
					<?php /***************************** 运输方式 Start *****************************/?>
					<div class="global_container">
						<div class="big_title">
							<strong>运输方式</strong>
							<?php /*
							<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit" id="edit_products_list" data-save="0">{/global.edit/}</a><?php }?>
							*/?>
						</div>
						<form id="orders_products_form">
							<table border="0" cellpadding="5" cellspacing="0" class="r_con_table" id="orders_products_list">
								<thead>
									<tr>
										<td width="3%" nowrap="nowrap">{/global.serial/}</td>
										<td nowrap="nowrap">{/products.product/}</td>
										<td nowrap="nowrap" style="width:90px;">运输方式</td>
										<td nowrap="nowrap" style="width:50px;">运费</td>
										<td nowrap="nowrap" style="width:50px;">清关费</td>
										<td nowrap="nowrap" style="width:70px;" class="last">小计</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;
									$BId='0';
									foreach($order_list_row as $v){$v['Business'] && $BId.=','.$v['Business'];}
									$amount=$quantity=0;
									$total_pro_count=count($order_list_row);
									$total_ShippingPrice=0;
									foreach($order_list_row as $v){
										$v['Name'.$c['manage']['web_lang']]=$v['Name'];
										
										$_total_ShippingPrice+=$v["ShippingPrice"];
										$_total_QingguanPrice+=$v["QingguanPrice"];
										$total_ShippingPrice+=$v["ShippingPrice"]+$v["QingguanPrice"];
										
										if(!$v["price_rate"])
										    $v["price_rate"]=0;
										$price=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1);
										$front_amount+=$price*$v['Qty']*$v["price_rate"];//单个产品定金
										$amount+=$price*$v['Qty'];
										$subtotal=floatval(substr(sprintf('%01.3f', $price*$v['Qty']), 0, -1));  //小计
										$front_subtotal=floatval(substr(sprintf('%01.3f', $price*$v['Qty']*$v["price_rate"]), 0, -1));  //小计定金
									?>
										<tr>
											<td><?=$i++;?></td>
											<td>
												<?php 
												if($v['BuyType']==4){
													$package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
													if(!$package_row) continue;
													$products_row=str::str_code(db::get_all('products', "ProId='{$package_row['ProId']}'"));
													$pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
													$pro_where=='' && $pro_where=0;
													$products_row=array_merge($products_row, str::str_code(db::get_all('products', "ProId in($pro_where)")));
													$data_ary=str::json_data(htmlspecialchars_decode($package_row['Data']), 'decode');
													// $attr=array();
													// $attr = ($v['Property']!=''?str::json_data($v['Property'], 'decode'):$data_ary[$products_row[0]['ProId']]);	
													$v['Property']!='' && $attr=ly200::get_order_property($v['Property']);
													
												?>
													<h4>[ Sales ]</h4>
													<div class="blank6"></div>
													<?php
													foreach((array)$products_row as $k2=>$v2){
														$quantity+=$v['Qty'];
														$img=ly200::get_size_img($v2['PicPath_0'], '240x240');
														$url=ly200::get_url($v2, 'products', $c['manage']['web_lang']);
													?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" alt="<?=$v2['Name'.$c['manage']['web_lang']];?>" /></a></dt>
														<dd<?=$_GET['do']=='print'?' style="padding-right:0;"':'';?>>
															<h4><a href="<?=$url;?>" title="<?=$v2['Name'.$c['manage']['web_lang']]?>" class="green" target="_blank"><?=$v2['Name'.$c['manage']['web_lang']];?></a></h4>
															<?=$v2['Number']!=''?'<p>{/products.products.number/}: '.$v2['Prefix'].$v2['Number'].'</p>':'';?>
															<?=$v2['SKU']!=''?'<p>{/products.products.sku/}: '.$v2['SKU'].'</p>':'';?>
															<?php if($k2==0){?>
																<div>
																	<?php
																	foreach((array)$attr as $k=>$z){
																		if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
																		echo '<p class="attr_'.$k.'">'.($k=='Overseas'?'{/shipping.area.ships_from/}':$attribute_ary[$k][1]).': '.$vid_data_ary[$k][$z].'</p>';
																	}
																	if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
																		echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']].'</p>';
																	}?>
																</div>
															<?php }elseif($data_ary[$v2['ProId']]){?>
																<div>
																	<?php
																	$OvId=0;
																	foreach((array)$data_ary[$v2['ProId']] as $k3=>$v3){
																		if($k3=='Overseas'){ //发货地
																			$OvId=str_replace('Ov:', '', $v3);
																			if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $OvId==1) continue; //发货地是中国，不显示
																			echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']].'</p>';
																		}else{
																			echo '<p class="attr_'.$k3.'">'.$attribute_ary[$k3][1].': '.$vid_data_ary[$k3][$v3].'</p>';
																		}
																	}
																	if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $OvId==1){
																		echo '<p class="attr_Overseas">{/shipping.area.ships_from/}: '.$overseas_ary[$OvId]['Name'.$c['manage']['web_lang']].'</p>';
																	}
																	?>
																</div>
															<?php }?>
														</dd>
													</dl>
													<div class="blank6"></div>
													<?php }?>
													<?php if($v['Remark']){?><dl><dd><p class="remark">{/orders.global.remark/}: <?=$v['Remark'];?></p></dd></dl><?php }?>
												<?php 
												}else{
													$attr=str::json_data($v['Property'], 'decode');
													!$attr && $attr=str::json_data($v['Property'], 'decode');
													$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
													$quantity+=$v['Qty'];
													$SKU=$v['OrderSKU']?$v['OrderSKU']:$v['SKU'];
													$img=@is_file($c['root_path'].$v['PicPath'])?$v['PicPath']:ly200::get_size_img($v['PicPath_0'], '240x240');
												?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" title="<?=$v['Name']?>" /></a></dt>
														<dd<?=$_GET['do']=='print'?' style="padding-right:0;"':'';?>>
															<h4><a href="<?=$url;?>" title="<?=$v['Name']?>" class="green" target="_blank"><?=$v['Name'];?></a></h4>
															<?php
															echo $v['Number']!=''?'<p>{/products.products.number/}: '.$v['Prefix'].$v['Number'].'</p>':'';
															echo $SKU!=''?'<p>{/products.products.sku/}: '.$SKU.'</p>':'';
															foreach((array)$attr as $k=>$vv){
																if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
																echo '<p>'.($k=='Overseas'?'{/shipping.area.ships_from/}':$k).': '.$vv.'</p>';
															}
															if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
																echo '<p>{/shipping.area.ships_from/}: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']].'</p>';
															}
															echo $v['Remark']?'<p>{/orders.global.remark/}: '.$v['Remark'].'</p>':'';
															?>
														</dd>
													</dl>
												<?php }?>
											</td>
											<td data-type="shipping_Express">
												<span><?=$v["Express"];?></span>
											</td>
											<td data-type="ShippingPrice">
												<span><?=$Symbol.$v["ShippingPrice"];?></span>
												</td>
											<td data-type="QingguanPrice">
											<span><?=$Symbol.$v["QingguanPrice"];?></span>
											</td>
											<td data-type="amount_yunshu">
												<span><?=$Symbol.($v["ShippingPrice"]+$v["QingguanPrice"]);?></span>
											</td>
										</tr>
									<?php }?>
									<tr>
										<td colspan="3">&nbsp;</td>
										<td><?=$Symbol.substr(sprintf('%01.3f', $_total_ShippingPrice), 0, -1);?></td>
										<td><span><?=$Symbol.substr(sprintf('%01.3f', $_total_QingguanPrice), 0, -1);?></span></td>
										<td>
											<span><?=$Symbol.substr(sprintf('%01.3f', $total_ShippingPrice), 0, -1);?></span>
										</td>
									</tr>
								</tbody>
							</table>
							<?php /*
							<input type="hidden" id="OrderId" name="OrderId" value="<?=$OrderId;?>" />
							<input type="hidden" name="do_action" value="orders.orders_prod_edit_edit" />
							*/?>
						</form>
					</div>
					<?php /*****************************  运输方式 End *****************************/?>
				</div>
			</div>
			
			<div class="right_container">
				<?php /***************************** 配送地址 Start *****************************/?>
				<div class="global_container orders_address">
					<div class="big_title">
						<strong>{/orders.info.address_info/}</strong>
						<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit" id="edit_address">{/global.edit/}</a><?php }?>
					</div>
					<div class="right_list">
						<div class="rows clean">
							<strong>{/orders.info.name/}:</strong>
							<span data-type="name"><?=$orders_row['ShippingFirstName'].' '.$orders_row['ShippingLastName'];?></span>
						</div>
						<div class="rows clean">
							<strong>{/orders.info.address/}:</strong>
							<span data-type="address">
								<?=$orders_row['ShippingAddressLine1'].($orders_row['ShippingAddressLine2']?', '.$orders_row['ShippingAddressLine2']:'');?><br />
								<?=$orders_row['ShippingCity'].($orders_row['ShippingState']?', '.$orders_row['ShippingState']:'').', '.$orders_row['ShippingCountry'].(($orders_row['ShippingCodeOption']&&$orders_row['ShippingTaxCode'])?'#'.$orders_row['ShippingCodeOption'].': '.$orders_row['ShippingTaxCode']:'');?>
							</span>
						</div>
						<div class="rows clean">
							<strong>{/orders.info.postal_code/}:</strong>
							<span data-type="zipcode"><?=$orders_row['ShippingZipCode'];?></span>
						</div>
						<div class="rows clean">
							<strong>{/orders.info.phone/}:</strong>
							<span data-type="phone"><?=($orders_row['ShippingCountryCode']?$orders_row['ShippingCountryCode'].'-':'').$orders_row['ShippingPhoneNumber'];?></span>
						</div>
					</div>
				</div>
				<?php /***************************** 配送地址 End *****************************/?>
				<?php /***************************** Paypal地址 Start *****************************/?>
				<?php if($paypal_address_row){?>
					<div class="global_container orders_paypal_address">
						<div class="big_title">{/orders.info.paypal_address/}</div>
						<div class="switchery<?=$paypal_address_row['IsUse']==1?' checked':'';?>">
							<input type="checkbox" name="IsUsedPayPal" value="1" data-id="<?=$orders_row['OrderId'];?>"<?=$paypal_address_row['IsUse']==1?' checked':'';?> />
							<div class="switchery_toggler"></div>
							<div class="switchery_inner">
								<div class="switchery_state_on"></div>
								<div class="switchery_state_off"></div>
							</div>
						</div>
						<div class="right_list">
							<div class="rows clean">
								<strong>{/orders.info.name/}:</strong>
								<span><?=$paypal_address_row['FirstName'].' '.$paypal_address_row['LastName'];?></span>
							</div>
							<div class="rows clean">
								<strong>{/orders.info.address/}:</strong>
								<span>
									<?=$paypal_address_row['AddressLine1'].($paypal_address_row['AddressLine2']?', '.$paypal_address_row['AddressLine2']:'');?><br />
									<?=$paypal_address_row['City'].', '.$paypal_address_row['State'].', '.$paypal_address_row['Country'].(($paypal_address_row['CodeOption']&&$paypal_address_row['TaxCode'])?'#'.$paypal_address_row['CodeOption'].': '.$paypal_address_row['TaxCode']:'');?>
								</span>
							</div>
							<div class="rows clean">
								<strong>{/orders.info.postal_code/}:</strong>
								<span><?=$paypal_address_row['ZipCode'];?></span>
							</div>
							<div class="rows clean">
								<strong>{/orders.info.phone/}:</strong>
								<span><?=$paypal_address_row['CountryCode'].'-'.$paypal_address_row['PhoneNumber'];?></span>
							</div>
						</div>
					</div>
				<?php }?>
				<?php /***************************** Paypal地址 End *****************************/?>
				<?php /***************************** 配送信息 Start *****************************/?>
				
				
				<?php /*
				
				<div class="global_container">
					<div class="big_title">
						<strong>{/orders.info.ship_info/}</strong>
					</div>
					<dl class="orders_shipping">
						<?php
						$shipped_ary=array();
						if(!$orders_row['TrackingNumber'] && $orders_row['ShippingTime']==0 && !$orders_row['Remarks']){ //多个发货地
							$TrackingNumber=str::json_data(htmlspecialchars_decode($orders_row['OvTrackingNumber']), 'decode');
							$ShippingTime=str::json_data(htmlspecialchars_decode($orders_row['OvShippingTime']), 'decode');
							$Remarks=str::json_data(htmlspecialchars_decode($orders_row['OvRemarks']), 'decode');
							$ShippingSId=str::json_data(htmlspecialchars_decode($orders_row['ShippingOvSId']), 'decode');
							$ShippingStatus=str::json_data(htmlspecialchars_decode($orders_row['OvShippingStatus']), 'decode');
							foreach($TrackingNumber as $k=>$v){
								$shipped_ary[]=array(
									'OvId'				=>	$k,
									'TrackingNumber'	=>	$v,
									'ShippingTime'		=>	$ShippingTime[$k],
									'Remarks'			=>	$Remarks[$k],
									'ShippingSId'		=>	$ShippingSId[$k],
									'Status'			=>	$ShippingStatus[$k]
								);
							}
						}else{ //单个发货地
							$shipped_ary[]=array(
								'OvId'				=>	1,
								'TrackingNumber'	=>	$orders_row['TrackingNumber'],
								'ShippingTime'		=>	$orders_row['ShippingTime'],
								'Remarks'			=>	$orders_row['Remarks'],
								'ShippingSId'		=>	$orders_row['ShippingMethodSId'],
								'Status'			=>	($orders_row['TrackingNumber'] && $orders_row['ShippingTime'])?1:0
							);
						}
						$orders_waybill_row=db::get_all('orders_waybill', "OrderId='{$orders_row['OrderId']}'");
						foreach($orders_waybill_row as $v){
							$shipped_ary[]=array(
								'WId'				=>	$v['WId'],
								'ProInfo'			=>	str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode'),
								'TrackingNumber'	=>	$v['TrackingNumber'],
								'ShippingTime'		=>	$v['ShippingTime'],
								'Remarks'			=>	$v['Remarks'],
								'ShippingSId'		=>	0,
								'Status'			=>	$v['Status']
							);
						}?>
						<?php
						$j=0;
						if(($orders_row['ShippingExpress']=='' && $orders_row['ShippingMethodSId']==0 && $orders_row['ShippingMethodType']=='' && count($oversea_id_ary)>1) || count($orders_waybill_row)>=1 || $shipping_template){
							//多个发货地		
							foreach($oversea_id_ary as $k=>$v){
								++$j;
						?>
								<dt data-id="<?=$v;?>"<?=$j==1?' class="first"':'';?>>
									<div class="title">
										<strong class="shipping_name"><?=$shipping_ary['ShippingOvExpress'][$v];?></strong>
										<span>From <?=$overseas_ary[($oversea_cid_ary[$v]['OvId']?$oversea_cid_ary[$v]['OvId']:$v)]['Name'.$c['manage']['web_lang']];?></span>
										<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit btn_shipping_edit">{/global.edit/}</a><?php }?>
									</div>
									<div class="right_list">
										<?php
										if($shipping_template){
											$img=@is_file($c['root_path'].$oversea_cid_ary[$v]['PicPath'])?$oversea_cid_ary[$v]['PicPath']:ly200::get_size_img($oversea_cid_ary[$v]['PicPath_0'], '240x240');
										?>
											<div class="rows shipping_tem">
												<div class="img"><img src="<?=$img;?>" alt="" /></div>
												<div class="name"><?=$oversea_cid_ary[$v]['Name'];?></div>
											</div>
										<?php }?>
										<div class="rows clean">
											<strong>{/orders.info.weight/}:</strong>
											<span><?=$oversea_weight_ary[$v];?>{/orders.info.unit/}</span>
										</div>
										<div class="rows clean">
											<strong>{/orders.info.charges/}:</strong>
											<span class="shipping_price"><?=$Symbol.sprintf('%01.2f', $shipping_ary['ShippingOvPrice'][$v]);?></span>
										</div>
										<?php if(!$shipping_template){?>
											<div class="rows clean">
												<strong>{/orders.info.insurance/}:</strong>
												<span class="shipping_insurance"><?=$Symbol.sprintf('%01.2f', $shipping_ary['ShippingOvInsurancePrice'][$v]);?></span>
											</div>
										<?php }?>
										<?php if((int)@in_array('dhl_account_info', (array)$c['manage']['plugins']['Used']) == 1 && substr_count('dhl', strtolower($shipping_ary['ShippingOvExpress'][$v])) && $orders_row['OrderStatus'] == 4){
											// 判断是否包含DHL字段，以及是否开启DHL信息应用
											if(!$orders_row['DHLLabelStatus']){
										?>
											<div class="rows clean">
												<strong>{/orders.dhl_account_info.dhl_label/}:</strong>
												<span class="dhl_label"><a href="javascript:;" id="get_label" class="btn_global get_label">{/orders.dhl_account_info.get_label/}</a></span>
											</div>
										<?php }else{
											$FilePath = db::get_value('orders_label_list', "OrderId='{$OrderId}'", 'FilePath');
										?>
											<div class="rows clean">
												<strong>{/orders.dhl_account_info.dhl_label/}:</strong>
												<span class="dhl_label"><a href="<?=$FilePath;?>"  download="<?=$orders_row['OId'].'.pdf';?>" class="down_file">{/orders.dhl_account_info.dhl_label_download/}</a></span>
											</div>
											<div class="rows clean">
												<strong>{/orders.dhl_account_info.track_trackin/}:</strong>
												<span class="track_trackin"><a href="javascript:;" id="dhl_track_check" data-no="<?=$OrderId;?>">{/orders.dhl_account_info.track_check/}</a></span>
											</div>
										<?php }}?>
									</div>
								</dt>
								<?php
								if(count($shipped_ary)>1 || $shipping_template){
									foreach($shipped_ary as $k2=>$v2){
										$prod_ovid_ary[$v['LId']];
										$_ovid=$v2['OvId'];
										if($v2['WId']>0){
											foreach($v2['ProInfo'] as $k3=>$v3){
												$_ovid=$prod_ovid_ary[$k3];
											}
										}
										if($_ovid!=$v || $v2['Status']==0) continue;
										$tracking_list=array();
										$api_row=db::get_one('shipping_api', "1 and AId in(select IsAPI from shipping where IsAPI>0 and SId='{$v2['ShippingSId']}')");
								?>
										<dd>
											<div class="title">
												<strong>{/orders.shipping.package/}<?=$k2+1;?></strong>
												<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit btn_track_edit" data-ovid="<?=$v2['OvId'];?>" data-wid="<?=$v2['WId'];?>">{/global.edit/}</a><?php }?>
											</div>
											<div class="right_list">
												<div class="rows clean">
													<strong>{/orders.shipping.track_no/}:</strong>
													<span class="shipping_track query" id="<?=$orders_row['OId'].($k2+1);?>"><?=$v2['TrackingNumber'];?></span>
												</div>
												<div class="rows clean">
													<strong>{/orders.info.delivery_time/}:</strong>
													<span class="shipping_delivery_time"><?=$v2['ShippingTime']?date('Y-m-d', $v2['ShippingTime']):'N/A';?></span>
												</div>
												<div class="rows clean">
													<strong>{/orders.global.remark/}:</strong>
													<span class="shipping_remark"><?=$v2['Remarks'];?></span>
												</div>
											</div>
										</dd>
										<script type="text/javascript">
										YQV5.trackSingleF1({
											YQ_ElementId:"<?=$orders_row['OId'].($k2+1);?>",	//必须，指定悬浮位置的元素ID。
											YQ_TargetId:"orders_inside",	//可选，指定悬浮位置的父级元素ID
											YQ_Width:600,	//可选，指定查询结果宽度，最小宽度为600px，默认撑满容器。
											YQ_Height:400,	//可选，指定查询结果高度，最大高度为800px，默认撑满容器。
											YQ_Lang:"<?=$UILang;?>",	//可选，指定UI语言，默认根据浏览器自动识别。
											YQ_Num:"<?=$v2['TrackingNumber'];?>"	//必须，指定要查询的单号。
										});
										</script>
								<?php
									}
								}?>
						<?php
							}
						}else{
							//单个发货地
							++$j;
                            if($orders_row['ShippingExpress']=='' && $orders_row['ShippingMethodSId']==0 && $orders_row['ShippingMethodType']==''){
                                //没快递
                                $IsExpress=0;
                                $ExpressTitle=$c['manage']['lang_pack']['orders']['shipping']['no_express'];
                            }else{
                                $IsExpress=1;
                                $ExpressTitle=(int)$orders_row['ShippingMethodSId']?$shipping_cfg['Express']:($orders_row['ShippingMethodType']=='air'?$shipping_cfg['AirName']:$shipping_cfg['OceanName']);
                            }
						?>
							<dt data-id="0"<?=$j==1?' class="first"':'';?>>
								<div class="title">
									<strong class="shipping_name<?=$IsExpress==0?' error':''?>"><?=$ExpressTitle;?></strong>
									<?php if($permit_ary['edit']){?><a href="javascript:;" class="btn_edit btn_shipping_edit">{/global.edit/}</a><?php }?>
								</div>
								<div class="right_list">
									<div class="rows clean">
										<strong>{/orders.info.weight/}:</strong>
										<span><?=$total_weight;?>{/orders.info.unit/}</span>
									</div>
									<div class="rows clean<?=$IsExpress==0?' hide':''?>">
										<strong>{/orders.info.charges/}:</strong>
										<span class="shipping_price"><?=$Symbol.sprintf('%01.2f', $orders_row['ShippingPrice']);?></span>
									</div>
									<div class="rows clean<?=$IsExpress==0?' hide':''?>">
										<strong>{/orders.info.insurance/}:</strong>
										<span class="shipping_insurance"><?=$Symbol.sprintf('%01.2f', $orders_row['ShippingInsurancePrice']);?></span>
									</div>
									<?php if((int)@in_array('dhl_account_info', (array)$c['manage']['plugins']['Used']) == 1 && substr_count('dhl', strtolower($ExpressTitle)) && $orders_row['OrderStatus'] == 4){
											// 判断是否包含DHL字段，以及是否开启DHL信息应用
											if(!$orders_row['DHLLabelStatus']){
										?>
										<div class="rows clean">
											<strong>{/orders.dhl_account_info.dhl_label/}:</strong>
											<span class="dhl_label"><a href="javascript:;" id="get_label" class="btn_global get_label">{/orders.dhl_account_info.get_label/}</a></span>
										</div>
									<?php }else{
											$FilePath = db::get_value('orders_label_list', "OrderId='{$OrderId}'", 'FilePath');
										?>
											<div class="rows clean">
												<strong>{/orders.dhl_account_info.dhl_label/}:</strong>
												<span class="dhl_label"><a href="<?=$FilePath;?>"  download="<?=$orders_row['OId'].'.pdf';?>" class="down_file">{/orders.dhl_account_info.dhl_label_download/}</a></span>
											</div>
											<div class="rows clean">
												<strong>{/orders.dhl_account_info.track_trackin/}:</strong>
												<span class="track_trackin"><a href="javascript:;" id="dhl_track_check" data-no="<?=$OrderId;?>">{/orders.dhl_account_info.track_check/}</a></span>
											</div>
									<?php }}?>
									<?php if(count($shipped_ary)==1 && $shipped_ary[0]['Status']>0){?>
										<div class="rows clean">
											<strong>{/orders.shipping.track_no/}:</strong>
											<span class="query" id="<?=$orders_row['OId'];?>"><?=$shipped_ary[0]['TrackingNumber'];?></span>
										</div>
										<div class="rows clean">
											<strong>{/orders.info.delivery_time/}:</strong>
											<span><?=$shipped_ary[0]['ShippingTime']?date('Y-m-d', $shipped_ary[0]['ShippingTime']):'N/A';?></span>
										</div>
										<div class="rows clean">
											<strong>{/orders.global.remark/}:</strong>
											<span><?=$shipped_ary[0]['Remarks'];?></span>
										</div>
										<script type="text/javascript">
										YQV5.trackSingleF1({
											YQ_ElementId:"<?=$orders_row['OId'];?>",	//必须，指定悬浮位置的元素ID。
											YQ_TargetId:"orders_inside",	//可选，指定悬浮位置的父级元素ID
											YQ_Width:600,	//可选，指定查询结果宽度，最小宽度为600px，默认撑满容器。
											YQ_Height:400,	//可选，指定查询结果高度，最大高度为800px，默认撑满容器。
											YQ_Lang:"<?=$UILang;?>",	//可选，指定UI语言，默认根据浏览器自动识别。
											YQ_Num:"<?=$shipped_ary[0]['TrackingNumber'];?>"	//必须，指定要查询的单号。
										});
										</script>
									<?php }?>
								</div>
							</dt>
						<?php }?>
					</dl>
				</div>
				*/?>
				<?php /***************************** 配送信息 End *****************************/?>
				<?php /***************************** 亚翔供应链 Start *****************************/?>
				<?php 
					$asiafly_row = db::get_one('orders_label_list', "OrderId='{$OrderId}'", '*');
					if((int)@in_array('asiafly', (array)$c['manage']['plugins']['Used'])==1 && (int)$orders_row['AsiaflyStatus'] && !empty($asiafly_row)){
				?>
				<div class="global_container">
					<div class="big_title">
						<strong>{/orders.asiafly.asiafly/}</strong>
					</div>
					<dl class="orders_asiafly">	
						<dd>
							<div class="right_list">
								<div class="rows clean">
									<strong>{/orders.asiafly.asiafly_business_no/}:</strong>
									<span class="asiafly_business_no"><?=$asiafly_row['BusinessNo'];?></span>
								</div>
								<div class="rows clean">
									<strong>{/orders.asiafly.asiafly_name/}:</strong>
									<span class="asiafly_name"><?=$asiafly_row['ProductName'].'[ '.$asiafly_row['ProductCode'].' ]';?></span>
								</div>
								<div class="rows clean">
									<strong>{/orders.asiafly.asiafly_price/}:</strong>
									<span class="asiafly_price"><?='CNY ￥'.$asiafly_row['Amount'];?></span>
								</div>
								<div class="rows clean">
									<strong>{/orders.asiafly.asiafly_label/}:</strong>
									<span class="asiafly_label"><a href="<?=$asiafly_row['FilePath'];?>" download="<?=$asiafly_row['BusinessNo'].'.pdf';?>" class="down_file">{/orders.asiafly.download/}</a></span>
								</div>
								<div class="rows clean">
									<strong>{/orders.asiafly.track_trackin/}:</strong>
									<span class="track_trackin"><a href="javascript:;" id="track_check" data-no="<?=$asiafly_row['BusinessNo'];?>">{/orders.asiafly.track_check/}</a></span>
								</div>
							</div>
						</dd>
					</dl>
				</div>
				<?php }?>
				<?php /***************************** 亚翔供应链 End *****************************/?>
				<?php /***************************** 备注 Start *****************************/?>
				<div class="global_container orders_remark_log">
					<div class="big_title">{/orders.global.remark/}&nbsp;&nbsp;<span class="box_explain">( {/orders.explain.remarks/} )</span></div>
					<div class="form_remark_log">
						<div class="form_box">
							<div class="remark_left"><div><input type="text" class="box_input" name="Log" placeholder="{/global.please_enter/} ..." /></div></div>
							<input type="button" class="btn_save" value="{/global.send/}" />
						</div>
					</div>
					<ul class="log_list">
						<?php
						foreach($orders_remark_log_row as $k=>$v){
						?>
							<li>
								<div class="time"><?=$v['AccTime']?date('Y-m-d', $v['AccTime']).'<br />'.date('H:i:s', $v['AccTime']):'N/A';?></div>
								<div class="information">
									<i class="radio"></i>
									<div class="log"><?=stripslashes(htmlspecialchars_decode($v['Log']));?></div>
									<div class="user"><?='({/manage.manage.manager/})&nbsp;'.$v['UserName'];?></div>
								</div>
							</li>
						<?php }?>
					</ul>
				</div>
				<?php /***************************** 备注 End *****************************/?>
				<?php /***************************** 日志 Start *****************************/?>
				<div class="global_container orders_log">
					<div class="big_title">{/orders.global.remark_log/}<i></i></div>
					<ul class="log_list">
						<?php
						foreach($orders_log_row as $k=>$v){
						?>
							<li>
								<div class="time"><?=$v['AccTime']?date('Y-m-d', $v['AccTime']).'<br />'.date('H:i:s', $v['AccTime']):'N/A';?></div>
								<div class="information">
									<i class="radio"></i>
									<div class="status">{/orders.status.<?=$v['OrderStatus'];?>/}</div>
									<div class="log"><?=$v['Log'];?></div>
									<div class="user">
										<?php
										if($v['UserId'] && $v['IsAdmin']){
											echo '({/manage.manage.manager/})';
										}else{
											echo '({/orders.user/})';
										}
										echo '&nbsp;'.$v['UserName'];
										?>
									</div>
								</div>
							</li>
						<?php }?>
					</ul>
				</div>
				<?php /***************************** 日志 End *****************************/?>
			</div>
			<div class="clear"></div>
		</div>
    <?php
		//销毁变量
		//unset($orders_row, $order_list_row);
	}elseif($c['manage']['do']=='print'){
		$print_row=str::str_code(db::get_all('config', "GroupId='print'"));
		$print_ary=array();
		foreach($print_row as $v){
			$print_ary[$v['Variable']]=$v['Value'];
		}
		$OrderId=(int)$_GET['OrderId'];
		$query_string=urldecode($_GET['query_string']);
		$orders_row=str::str_code(db::get_one('orders', "OrderId='$OrderId'"));
		$order_list_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$OrderId'", 'o.*, p.PicPath_0, p.Prefix, p.Number', 'o.LId asc');
		$sum_row=db::get_one('orders_products_list', "OrderId='$OrderId'", "sum(Qty) as TotalQty");
		$Symbol=$orders_row['ManageCurrency']?$all_currency_ary[$orders_row['ManageCurrency']]['Symbol']:$c['manage']['currency_symbol'];
		$paypal_address_row=str::str_code(db::get_one('orders_paypal_address_book', "OrderId='{$orders_row['OrderId']}' and IsUse=1"));
		if($paypal_address_row){//Paypal账号收货地址
			$shipto_ary=array(
				'FirstName'			=>	$paypal_address_row['FirstName'],
				'LastName'			=>	$paypal_address_row['LastName'],
				'AddressLine1'		=>	$paypal_address_row['AddressLine1'],
				'AddressLine2'		=>	'',
				'City'				=>	$paypal_address_row['City'],
				'State'				=>	$paypal_address_row['State'],
				'SId'				=>	0,
				'Country'			=>	$paypal_address_row['Country'],
				'CId'				=>	0,
				'ZipCode'			=>	$paypal_address_row['ZipCode'],
				'CodeOption'		=>	'',
				'CodeOptionId'		=>	0,
				'TaxCode'			=>	'',
				'CountryCode'		=>	$paypal_address_row['CountryCode'],
				'PhoneNumber'		=>	$paypal_address_row['PhoneNumber']
			);
		}else{//会员账号收货地址
			$shipto_ary=array(
				'FirstName'			=>	$orders_row['ShippingFirstName'],
				'LastName'			=>	$orders_row['ShippingLastName'],
				'AddressLine1'		=>	$orders_row['ShippingAddressLine1'],
				'AddressLine2'		=>	$orders_row['ShippingAddressLine2'],
				'City'				=>	$orders_row['ShippingCity'],
				'State'				=>	$orders_row['ShippingState'],
				'SId'				=>	$orders_row['ShippingSId'],
				'Country'			=>	$orders_row['ShippingCountry'],
				'CId'				=>	$orders_row['ShippingCId'],
				'ZipCode'			=>	$orders_row['ShippingZipCode'],
				'CodeOption'		=>	$orders_row['ShippingCodeOption'],
				'CodeOptionId'		=>	$orders_row['ShippingCodeOptionId'],
				'TaxCode'			=>	$orders_row['ShippingTaxCode'],
				'CountryCode'		=>	$orders_row['ShippingCountryCode'],
				'PhoneNumber'		=>	$orders_row['ShippingPhoneNumber']
			);
		}
		
		$printLogo=db::get_value('config', "GroupId='print' and Variable='LogoPath'", 'Value');
	?>
		<script language="javascript">
		$(document).ready(function(){
			var head='<html><head><title></title></head><body><object classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" height="0" id="WebBrowser3" width="0" viewastext></object>';
			var foot='</body></html>';
			var content=$('#orders_print').html();
			document.body.innerHTML=head+content+foot;
			window.print();
		});</script>
		<div id="orders_print">
			<div class="orders_print">
				<table class="head_table">
					<tr>
						<td rowspan="2" width="90%">
							<?php if(is_file($c['root_path'].$printLogo)){?>
								<div class="logo"><img src="<?=$printLogo;?>" /></div>
							<?php }?>
						</td>
						<td></td>
						<td>NO.:</td>
						<td>
							<?=$orders_row['OId'];?>						
						</td>
					</tr>
					<tr>
						<td></td>
						<td>Date:&nbsp;&nbsp;</td>
						<td><?=@date('m/d/Y', $orders_row['OrderTime']);?></td>
					</tr>
				</table>
				<ul class="addr_list">
					<li><?=$print_ary['Compeny'];?></li>
					<li><?=$print_ary['Address'];?></li>
					<li>Email: <?=$print_ary['Email'];?>&nbsp;&nbsp;&nbsp;Tel: <?=$print_ary['Telephone'];?>&nbsp;&nbsp;&nbsp;Fax: <?=$print_ary['Fax'];?></li>
				</ul>
				<ul class="addr_list">
					<li><strong style="font-weight: bold;">Ship Information</strong></li>
					<li><?=$shipto_ary['FirstName'].' '.$shipto_ary['LastName'];?></li>
					<li><?=$shipto_ary['AddressLine1'].($shipto_ary['AddressLine2']?', '.$shipto_ary['AddressLine2']:'').', '.$shipto_ary['City'].', '.$shipto_ary['State'].', '.$shipto_ary['ZipCode'].', '.$shipto_ary['Country'].(($shipto_ary['CodeOption']&&$shipto_ary['TaxCode'])?'#'.$orders_row['shipto_ary'].': '.$shipto_ary['TaxCode']:'');?></li>
					<li><?=$shipto_ary['CountryCode'].'-'.$shipto_ary['PhoneNumber'];?></li>
				</ul>
				<table class="print_table" width="612" border="0" align="center" style="margin:0 auto;">
					<?php /*?><tr>
						<td class="bd_b0">Destionation:</td>
						<td colspan="3" class="bd_b0"><?=$orders_row['ShippingCountry'];?></td>
						<td>Total:</td>
						<td colspan="2"><?=$Symbol.sprintf('%01.2f', orders::orders_price($orders_row, 0, 1));?></td>
					</tr><?php */?>
					<tr>
						<th width="5%">No.</th>
						<th width="15%">Item ID</th>
						<th width="">Product</th>
						<th width="12%">Price</th>
						<th width="9%">Quantity</th>
						<th width="">Amount</th>
					</tr>
					<?php
					$total=0;
					$order_list_row=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='$OrderId'", 'o.*,p.PicPath_0,p.Number,p.Prefix', 'o.LId asc');
					foreach((array)$order_list_row as $k=>$v){
						$attr=str::json_data($v['Property'], 'decode');
						$url="/?a=goods&ProId={$v['ProId']}";
						$border=$k<$len?' class="bd_b0"':'';
						$price=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1);
						$total+=($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1)*$v['Qty'];
						$name=$v['Name'];
						$attr=str::json_data($v['Property'], 'decode');
						$SKU=$v['SKU'];
						?>
						<tr>
							<td><?=$k+1; ?></td>
							<td><?=$v['Prefix'].$v['Number']; ?></td>
							<td>
								<dl>
									<dt><img src="<?=$v['PicPath'];?>" title="<?=$name;?>" alt="<?=$name;?>"></dt>
									<dd>
										<h4><?=$name;?></h4>
										<?=$SKU!=''?'<p class="pro_attr">{/products.products.sku/}: '.$SKU.'</p>':'';?>
										<?php
										foreach((array)$attr as $k=>$v2){
											if($k=='Overseas' && ((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
											echo '<p class="pro_attr">'.($k=='Overseas'?'{/shipping.area.ships_from/}':$k).': '.$v2.'</p>';
										}
										if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['OvId']==1){
											echo '<p class="pro_attr">{/shipping.area.ships_from/}: '.$overseas_ary[$v['OvId']]['Name'.$c['manage']['web_lang']].'</p>';
										}
										?>
										<p><?=$v['Remark'];?></p>
									</dd>
								</dl>
							</td>
							<td><?=$Symbol.sprintf('%01.2f', $price);?></td>
							<td><?=$v['Qty']?></td>
							<td><?=$Symbol.sprintf('%01.2f', ($price)*$v['Qty']);?></td>
						</tr>
					<?php } ?>
					<tr>
						<td colspan="3"></td>
						<td class="big" colspan="2" align="right">Subtotal:</td>
						<td class="big"><?=$Symbol.sprintf('%01.2f', $total);?></td>
					</tr>
					<tr>
						<td colspan="6" class="tips">* All transactions are subject to the Company＇s Standard Trading Conditions (copy is available upon request), which in certain circumstances, limit or exempt the Company＇s liability.</td>
					</tr>
				</table>
			</div>
		</div>
	<?php
	}elseif($c['manage']['do']=='export'){
		//导出订单
		$month_time=3600*24*30;//30天内
		$StartTime=@strtotime(date('Y-m-d', $c['time']-$month_time).' 00:00:00');
		$EndTime=@strtotime(date('Y-m-d', $c['time']).' 23:59:59');
		$Time=@date('Y-m-d H:i', $StartTime).'/'.@date('Y-m-d H:i', $EndTime);
		echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
	?>
		<script type="text/javascript">$(document).ready(function(){orders_obj.orders_export_init()});</script>
		<div class="center_container">
           
                <div class="global_container">
                 	<form id="export_edit_form" class="global_form">
                        <a href="javascript:history.back(-1);" class="return_title">
                            <span class="return">{/module.orders.orders/}</span> 
                            <span class="s_return">/ {/orders.export.export/}</span>
                        </a>
                        <div class="rows clean">
                            <label>
                                <div class="tab_box">
                                    <dl class="tab_box_row">
                                        <dt><span>{/orders.info.order_time/}</span><i></i></dt>
                                        <dd class="drop_down">
                                            <a class="tab_box_btn item" style="display:none;" data-value="0"><span>{/orders.info.order_time/}</span><i></i></a>
                                            <a class="tab_box_btn item" data-value="1"><span>{/orders.info.payment_time/}</span><i></i></a>
                                            <a class="tab_box_btn item" data-value="2"><span>{/orders.info.delivery_time/}</span><i></i></a>
                                        </dd>
                                        <dd class="tips">{/error.supplement_lang/}</dd>
                                    </dl>
                                </div>
                            </label>
                            <div class="input">
                                <input type="text" class="box_input input_time" name="Time" value="<?=$Time;?>" size="38" readonly />
                                <input type="hidden" name="TimeType" value="0" />
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>{/orders.orders_status/}</label>
                            <div class="input">
                                <div class="box_select">
                                    <select name='Status'>
                                        <option value=''>{/global.select_index/}</option>
                                        <?php foreach($c['orders']['status'] as $k=>$v){?>
                                            <option value='<?=$k;?>'>{/orders.status.<?=$k;?>/}</option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows">
                            <label></label>
                            <div class="box_explain">{/orders.explain.export/}</div>
                            <span class="input input_button">
                                <input type="submit" class="btn_global btn_submit <?=$c['manage']['cdx_limit'];?>" value="{/global.explode/}" />
                                <a href="./?m=orders&a=orders"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
                                <a href="javascript:;" class="btn_cancel btn_option_set">{/orders.export.options_config/}</a>
                            </span>
                            <div class="clear"></div>
                        </div>
                        <div class="rows clean">
                            <label>{/orders.global.export_progress/}</label>
                            <div id="export_progress_export"></div>
                        </div>
                        <input type="hidden" name="do_action" value="orders.orders_export" />
                        <input type="hidden" name="Number" value="0" />
                	</form>	
                </div>
        </div>
	<?php }?>
</div>
<?php
if($c['manage']['do']=='view'){ //订单详情页
?>
<div id="fixed_right">
	<div class="global_container fixed_orders_info">
		<div class="top_title">{/orders.global.edit_price_info/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="orders_info_form">
			<div class="rows clean">
				<label>{/orders.info.product_price/}</label>
				<div class="input">
					<span class="fixed_price"><?=$Symbol.sprintf('%01.2f', $orders_row['ProductPrice']);?></span>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.discount/}(-)</label>
				<div class="input">
					<?php if($orders_row['Discount']>0){?>
						<span class="unit_input"><input type="text" class="box_input" name="Discount" value="<?=(100-$orders_row['Discount']);?>" size="10" maxlength="10" /><b class="last">%</b></span>
					<?php }else{?>
						<span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="DiscountPrice" value="<?=sprintf('%01.2f', $orders_row['DiscountPrice']);?>" size="10" maxlength="10" /></span>
					<?php }?>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.user_discount/}(-)</label>
				<div class="input">
					<span class="unit_input"><input type="text" class="box_input" name="UserDiscount" value="<?=$orders_row['UserDiscount'];?>" size="10" maxlength="10" /><b class="last">%</b></span>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.info.charges/}</label>
				<div class="input">
					<span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="ShippingPrice" value="<?=sprintf('%01.2f', $orders_row['ShippingPrice']);?>" size="10" maxlength="10" /></span>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.info.insurance/}</label>
				<div class="input">
					<span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="ShippingInsurancePrice" value="<?=sprintf('%01.2f', $orders_row['ShippingInsurancePrice']);?>" size="10" maxlength="10" /></span>
				</div>
			</div>
			<?php
			if($orders_row['CouponCode'] && ($orders_row['CouponPrice']>0 || $orders_row['CouponDiscount']>0)){
			?>
				<div class="rows clean">
					<label>{/orders.info.coupon/}(-)</label>
					<div class="input">
						<?php if($orders_row['CouponPrice']>0){?>
							<span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="CouponPrice" value="<?=sprintf('%01.2f', $orders_row['CouponPrice']);?>" size="10" maxlength="10" /></span>
						<?php }else{?>
							<span class="unit_input"><input type="text" class="box_input" name="CouponDiscount" value="<?=(100-$orders_row['CouponDiscount']*100);?>" size="10" maxlength="10" /><b class="last">%</b></span>
						<?php }?>
					</div>
				</div>
			<?php }?>
			<div class="rows clean">
				<label>{/orders.addfee/}</label>
				<div class="input">
					<span class="unit_input"><input type="text" class="box_input" name="PayAdditionalFee" value="<?=$orders_row['PayAdditionalFee'];?>" size="10" maxlength="10" /><b class="last">%</b></span>
					<div id="orders_fee_value">({/orders.info.product_price/} - {/orders.discount/} - {/orders.user_discount/} - {/orders.info.coupon/} + {/orders.info.charges/} + {/orders.info.insurance/}) X {/orders.addfee/}</div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.addfee_affix/}</label>
				<div class="input">
					<span class="unit_input"><b><?=$Symbol;?></b><input type="text" class="box_input" name="PayAdditionalAffix" value="<?=sprintf('%01.2f', $orders_row['PayAdditionalAffix']);?>" size="10" maxlength="10" /></span>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.total_price/}</label>
				<div class="input">
					<span class="fixed_price" id="orders_info_total_price"><?=$Symbol.sprintf('%01.2f', $total_price);?></span>
				</div>
			</div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="do_action" value="orders.orders_mod_info" />
		</form>
	</div>
	<div class="global_container fixed_shipping_address">
		<div class="top_title">{/orders.global.edit_address_info/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="shipping_address_form">
			<div class="rows">
				<div class="form_box clean">
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.name/}</span>
							<input type="text" class="box_input" name="FirstName" notnull />
						</label>
						<p class="error"></p>
					</div>
					<div class="box">
						<label class="input_box">
							<span class="input_box_label"></span>
							<input type="text" class="box_input" name="LastName" notnull />
						</label>
						<p class="error"></p>
					</div>
				</div>
			</div>
			<div class="rows">
				<div class="input clean">
					<label class="input_box">
						<span class="input_box_label">{/orders.address.address_line1/}</span>
						<input type="text" class="box_input" name="AddressLine1" notnull />
					</label>
					<p class="error"></p>
				</div>
			</div>
			<div class="rows">
				<div class="input clean">
					<label class="input_box">
						<span class="input_box_label">{/orders.address.address_line2/}</span>
						<input type="text" class="box_input" name="AddressLine2" />
					</label>
					<p class="error"></p>
				</div>
			</div>
			<div class="rows">
				<div class="form_box clean">
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.city/}</span>
							<input type="text" class="box_input" name="City" notnull />
						</label>
						<p class="error"></p>
					</div>
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.country/}</span>
							<div class="box_select">
								<select class="addr_select" name="country_id" id="country" notnull>
									<?php
									$shipto_country_ary=array();
									if($IsShipping){
										$cart_row=db::get_all('shopping_cart', $c['where']['cart'], 'OvId', 'CId desc');
										$OvId_where='a.OvId in(-1';
										foreach($cart_row as $v){ $OvId_where.=",{$v['OvId']}"; }
										$OvId_where.=')';
										$shipping_country_row=db::get_all('shipping_area a left join shipping_country c on a.AId=c.AId', $OvId_where.' Group By c.CId', 'c.CId');
										foreach($shipping_country_row as $v){ $shipto_country_ary[]=$v['CId']; }
									}
									$country_row=str::str_code(db::get_all('country', "IsUsed=1", '*', 'Country asc'));
									foreach($country_row as $v){
										if($IsShipping && !in_array($v['CId'], $shipto_country_ary)) continue;//所有快递方式都没有的国家，给过滤掉
										$selected=((int)$address_row[0]['CId']?$address_row[0]['CId']==$v['CId']:$v['IsDefault']==1)?' selected="selected"':'';
									?>
										<option value="<?=$v['CId'];?>"<?=$selected;?>><?=$v['Country'];?></option>
									<?php }?>
								</select>
							</div>
						</label>
					</div>
				</div>
			</div>
			<div class="rows">
				<div class="form_box clean">
					<div class="box">
						<div id="zoneId">
							<label class="input_box">
								<span class="input_box_label">{/orders.address.state/}</span>
								<div class="box_select">
									<select class="addr_select" name="Province"></select>
								</div>
							</label>
						</div>
						<div id="state" style="display:none;">
							<label class="input_box">
								<span class="input_box_label">{/orders.address.state/}</span>
								<input type="text" class="box_input" name="State" disabled />
							</label>
							<p class="error"></p>
						</div>
					</div>
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.zip/}</span>
							<input type="text" class="box_input" name="ZipCode" notnull />
						</label>
						<p class="error"></p>
					</div>
				</div>
			</div>
			<div class="rows" id="taxCode" style="display:none;">
				<div class="form_box clean">
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.cpf_cnpj/}</span>
							<div class="box_select">
								<select name="tax_code_type" class="addr_select" id="taxCodeOption" disabled>
									<option value="1">{/orders.address.cpf/}</option>
									<option value="2">{/orders.address.cnpj/}</option>
								</select>
							</div>
						</label>
					</div>
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.cpf_cnpj/}</span>
							<input type="text" class="box_input" name="tax_code_value" id="taxCodeValue" maxlength="11" disabled />
						</label>
						<p class="error"></p>
					</div>
				</div>
			</div>
			<div class="rows" id="tariffCode" style="display:none;">
				<div class="form_box clean">
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.personal_vatid/}</span>
							<div class="box_select">
								<select name="tax_code_type" class="addr_select" id="tariffCodeOption" disabled>
									<option value="3">{/orders.address.personal/}</option>
									<option value="4">{/orders.address.vatid/}</option>
								</select>
							</div>
						</label>
					</div>
					<div class="box">
						<label class="input_box">
							<span class="input_box_label">{/orders.address.personal_vatid/}</span>
							<input type="text" class="box_input" name="tax_code_value" id="tariffCodeValue" maxlength="14" disabled />
						</label>
						<p class="error"></p>
					</div>
				</div>
			</div>
			<div class="rows">
				<div class="input clean">
					<label class="input_box">
						<span class="input_box_label">{/orders.address.phone/}</span>
						<span class="unit_input" notnull><b id="countryCode">+0000</b><input type="text" class="box_input" name="PhoneNumber" size="5" maxlength="255" /><input type="hidden" name="CountryCode" value="" /></span>
					</label>
					<p class="error"></p>
				</div>
			</div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="do_action" value="orders.orders_mod_address" />
		</form>
	</div>
	<div class="global_container fixed_shipping_info">
		<div class="top_title">{/orders.global.edit_ship_info/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="shipping_info_form">
			<div class="rows clean">
				<label>{/orders.shipping.method/}</label>
				<div class="input">
					<div class="box_select">
						<select name="ShippingMethodSId" class="shipping_method_sid" tips="{/orders.address.select/}">
							<option value="-1">{/orders.address.select/}</option>
						</select>
					</div>
					<input type="hidden" name="ShippingMethodType" value="" />
				</div>
			</div>
			<?php if(!$orders_row['shipping_template']){ ?>
				<div class="insurance_info">
					<span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="ShippingInsurance" value="1" /></span></span>  {/orders.shipping.insurance/}
					<span class="insurance_price"><span class="shipping_method_insurance"></span></span>
				</div>
			<?php }?>
			<div class="insurance_update">
				<span class="input_checkbox_box checked"><span class="input_checkbox"><input type="checkbox" name="AutoModShippingPrice" value="1" checked /></span></span>
 				{/orders.shipping.auto_mod_price/}
			</div>
			<?php if(count($shipped_ary)==1 && $shipped_ary[0]['Status']>0){?>
				<div class="rows clean">
					<label>{/orders.shipping.track_no/}</label>
					<div class="input">
						<input type="text" class="box_input" name="TrackingNumber" value="<?=$shipped_ary[0]['TrackingNumber'];?>" notnull size="46" maxlength="255" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/orders.info.delivery_time/}</label>
					<div class="input">
						<input type="text" class="box_input input_time shipping_time" name="ShippingTime" value="<?=@date('Y-m-d', $shipped_ary[0]['ShippingTime']);?>" size="15" maxlength="10" readonly />
					</div>
				</div>
				<div class="rows clean">
					<label>{/orders.payment.contents/}</label>
					<div class="input">
						<textarea name='Remarks' class="box_textarea"><?=$shipped_ary[0]['Remarks'];?></textarea>
					</div>
				</div>
			<?php }?>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="OvId" value="" />
			<input type="hidden" name="do_action" value="orders.orders_mod_shipping" />
		</form>
	</div>
	<div class="global_container fixed_shipped">
		<div class="top_title">{/orders.global.edit_ship_info/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="shipped_form">
			<?php
			if($orders_row['OrderStatus']==6){
				if($orders_row['ShippingExpress']=='' && $orders_row['ShippingMethodSId']==0 && $orders_row['ShippingMethodType']==''){ //多个发货地
					foreach($oversea_id_ary as $k=>$v){
			?>
						<div class="oversea_title">Form <?=$overseas_ary[($oversea_cid_ary[$v]['OvId'] ? $oversea_cid_ary[$v]['OvId'] : $v)]['Name'.$c['manage']['web_lang']];?></div>
						<div class="rows clean">
							<label>{/orders.shipping.track_no/}</label>
							<div class="input">
								<input type="text" class="box_input" name="OvTrackingNumber[<?=$v;?>]" value="" notnull size="46" maxlength="255" />
							</div>
						</div>
						<div class="rows clean">
							<label>{/orders.info.delivery_time/}</label>
							<div class="input">
								<input type="text" class="box_input input_time shipping_time" name="OvShippingTime[<?=$v;?>]" value="<?=@date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly />
							</div>
						</div>
						<div class="rows clean">
							<label>{/orders.payment.contents/}</label>
							<div class="input">
								<textarea name='OvRemarks[<?=$v;?>]' class="box_textarea"><?=$orders_row['Remarks'];?></textarea>
							</div>
						</div>
			<?php
					}
				}else{ //单个发货地
			?>
					<div class="rows clean">
						<label>{/orders.shipping.track_no/}</label>
						<div class="input">
							<input type="text" class="box_input" name="TrackingNumber" value="" notnull size="46" maxlength="255" />
						</div>
					</div>
					<div class="rows clean">
						<label>{/orders.info.delivery_time/}</label>
						<div class="input">
							<input type="text" class="box_input input_time shipping_time" name="ShippingTime" value="<?=@date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly />
						</div>
					</div>
					<div class="rows clean">
						<label>{/orders.payment.contents/}</label>
						<div class="input">
							<textarea name='Remarks' class="box_textarea"><?=$orders_row['Remarks'];?></textarea>
						</div>
					</div>
			<?php
				}
			}?>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/orders.status.7/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="OrderStatus" value="7" />
			<input type="hidden" name="PassOrderStatus" value="<?=$orders_row['OrderStatus'];?>" />
			<input type="hidden" name="do_action" value="orders.orders_mod_status" />
		</form>
	</div>
	<div class="global_container edit_form_orders_status">
		<div class="top_title">{/orders.change_status/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_orders_status_form">
			<div class="rows clean">
                <label>{/orders.orders_status/}</label>
                <div class="input">
                    <div class="box_select">
                        <select name="OrderStatus">
                            <?php foreach($c['orders']['status'] as $k=>$v){
//                             	if($k==5) continue;
                            	?>
                                <option <?=$orders_row['OrderStatus'] == $k ? 'selected="selected"' : ''; ?> value='<?=$k;?>'>{/orders.status.<?=$k;?>/}</option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <?php /*
			<div class="shipping_box <?=$orders_row['OrderStatus'] == 5 ? '' : 'hide'; ?>">
				<?php
					if($orders_row['ShippingExpress']=='' && $orders_row['ShippingMethodSId']==0 && $orders_row['ShippingMethodType']==''){ //多个发货地
						foreach($oversea_id_ary as $k=>$v){
						?>
							<div class="rows"><label></label></div>
							<div class="oversea_title">Form <?=$overseas_ary[($oversea_cid_ary[$v]['OvId'] ? $oversea_cid_ary[$v]['OvId'] : $v)]['Name'.$c['manage']['web_lang']];?></div>
							<div class="rows clean">
								<label>{/orders.shipping.track_no/}</label>
								<div class="input">
									<input type="text" class="box_input" name="OvTrackingNumber[<?=$v;?>]" value="<?=$shipped_ary[$k]['TrackingNumber'];?>" notnull size="46" maxlength="255" />
								</div>
							</div>
							<div class="rows clean">
								<label>{/orders.info.delivery_time/}</label>
								<div class="input">
									<input type="text" class="box_input input_time shipping_time" name="OvShippingTime[<?=$v;?>]" value="<?=@date('Y-m-d', ($shipped_ary[$k]['ShippingTime'] ? $shipped_ary[$k]['ShippingTime'] : $c['time']));?>" size="15" maxlength="10" readonly />
								</div>
							</div>
							<div class="rows clean">
								<label>{/orders.payment.contents/}</label>
								<div class="input">
									<textarea name='OvRemarks[<?=$v;?>]' class="box_textarea"><?=$shipped_ary[$k]['Remarks'];?></textarea>
								</div>
							</div>
				<?php
						}
					}else{ //单个发货地
				?>
						<div class="rows clean">
							<label>{/orders.shipping.track_no/}</label>
							<div class="input">
								<input type="text" class="box_input" name="TrackingNumber" value="<?=$orders_row['TrackingNumber'];?>" notnull size="46" maxlength="255" />
							</div>
						</div>
						<div class="rows clean">
							<label>{/orders.info.delivery_time/}</label>
							<div class="input">
								<input type="text" class="box_input input_time shipping_time" name="ShippingTime" value="<?=@date('Y-m-d', ($orders_row['ShippingTime'] ? $orders_row['ShippingTime'] : $c['time']));?>" size="15" maxlength="10" readonly />
							</div>
						</div>
						<div class="rows clean">
							<label>{/orders.payment.contents/}</label>
							<div class="input">
								<textarea name='Remarks' class="box_textarea"><?=$orders_row['Remarks'];?></textarea>
							</div>
						</div>
				<?php } ?>
			</div>
			 */ ?>
			<div class="rows clean box_button">
				<label></label>
				<label></label>
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/orders.change_status/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="PassOrderStatus" value="<?=$orders_row['OrderStatus'];?>" />
			<input type="hidden" name="do_action" value="orders.orders_mod_status" />
		</form>
	</div>
	<div class="global_container fixed_track_info">
		<div class="top_title">{/orders.global.edit_track_info/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="track_info_form">
			<div class="rows clean">
				<label>{/orders.shipping.track_no/}</label>
				<div class="input">
					<input type="text" class="box_input" name="TrackingNumber" value="" notnull size="46" maxlength="255" />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.info.delivery_time/}</label>
				<div class="input">
					<input type="text" class="box_input input_time shipping_time" name="ShippingTime" value="" size="15" maxlength="10" readonly />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.payment.contents/}</label>
				<div class="input">
					<textarea name='Remarks' class="box_textarea"></textarea>
				</div>
			</div>
			<div class="blank15"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="OvId" value="" />
			<input type="hidden" name="WId" value="" />
			<input type="hidden" name="do_action" value="orders.orders_mod_track" />
		</form>
	</div>
	<div class="global_container fixed_asiafly_list">
		<div class="top_title">{/orders.asiafly.get_asiafly_list/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="asiafly_list_form">
			<div class="rows clean">
				<label>{/orders.asiafly.exprect_day/}</label>
				<div class="input">
					<input type="number" class="box_input" name="ExprectDay" value="" notnull size="5" maxlength="4"  />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.asiafly.products_type/}</label>
				<div class="input">
					<div class="box_select">
						<select name="ProductType" id="ProductType">
							<option value="1">{/orders.asiafly.products_type_ary.0/}</option>
							<option value="2">{/orders.asiafly.products_type_ary.1/}</option>
							<option value="3">{/orders.asiafly.products_type_ary.2/}</option>
						</select>
					</div>
				</div>
			</div>
			<div class="blank15"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit btn_inquiry" value="{/orders.asiafly.inquiry/}" />
				</div>
			</div>
			<div class="blank15"></div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="do_action" value="orders.get_asiafly_list" />
		</form>
		<form class="global_form hide" id="asiafly_choice_form">
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
				<thead>
					<tr>
						<td width="50%" nowrap="nowrap">{/orders.asiafly.asiafly_name/}</td>
						<td width="10%" nowrap="nowrap">{/orders.asiafly.asiafly_item_price/}(CNY)</td>
						<td width="10%" nowrap="nowrap">{/orders.asiafly.asiafly_item_unit/}</td>
						<td width="10%" nowrap="nowrap">{/orders.asiafly.asiafly_price/}(CNY)</td>
						<td width="20%" nowrap="nowrap"></td>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div class="rows clean box_button hide">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.use/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="ProductCode" value="" />
			<input type="hidden" name="ProductName" value="" />
			<input type="hidden" name="Amount" value="" />
			<input type="hidden" name="do_action" value="orders.asiafly_choice_use" />
		</form>
		<form class="global_form" id="asiafly_use_product">
			<div class="rows clean">
				<label>{/orders.asiafly.choice_provider/}</label>
				<div class="input">
					<div class="box_select">
						<select name="asiaflyProvider" id="asiaflyProvider" notnull></select>
					</div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.asiafly.choice_phannel/}</label>
				<div class="input">
					<div class="box_select">
						<select name="asiaflyChannel" id="asiaflyChannel" notnull></select>
					</div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.asiafly.choice_product/}</label>
				<div class="input">
					<div class="box_select">
						<select name="asiaflyProduct" id="asiaflyProduct" notnull></select>
					</div>
				</div>
			</div>
			<div class="blank15"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit btn_inquiry" value="{/global.use/}" />
				</div>
			</div>
			<div class="blank15"></div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="ProductCode" value="" />
			<input type="hidden" name="ProductName" value="" />
			<input type="hidden" name="Amount" value="" />
			<input type="hidden" name="do_action" value="orders.asiafly_choice_use" />
		</form>
	</div>
	<div class="global_container fixed_asiafly_track">
		<div class="top_title">{/orders.asiafly.track_check/} <a href="javascript:;" class="close"></a></div>
		<ul class="log_list"></ul>
	</div>
	<div class="global_container fixed_dhl_track">
		<div class="top_title">{/orders.dhl_account_info.track_check/} <a href="javascript:;" class="close"></a></div>
		<ul class="log_list"></ul>
	</div>
	<?php 
		$DHLShipperData = db::get_value('config', 'GroupId="orders" and Variable="DHLShipper"', 'Value');
		$DHLShipper = str::json_data($DHLShipperData, 'decode');
		$orders_products_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$OrderId}'", 'o.*, p.IsContentIndicator, p.DHLDescription, p.DHLDescriptionExport', 'o.LId asc');
		$country_row=str::str_code(db::get_all('country', "1", '*', 'Country asc'));
		foreach($country_row as $v){ $country_ary[$v['CId']]=$v['Country']; }
	?>
	<div class="global_container fixed_get_label">
		<div class="top_title">{/orders.dhl_account_info.get_label/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="get_label_form">
			<div class="rows clean">
				<label>{/orders.dhl_account_info.address1/}</label>
				<div class="input">
					<input type="text" class="box_input" name="Address" value="<?=$DHLShipper['Address']?>" size="46" maxlength="50" notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.dhl_account_info.city/}</label>
				<div class="input">
					<input type="text" class="box_input" name="City" value="<?=$DHLShipper['City']?>" size="46" maxlength="30" notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.dhl_account_info.country/}</label>
				<div class="input">
					<div class="box_select"><?=ly200::form_select($country_ary, 'CId', $DHLShipper['CId'] ? $DHLShipper['CId'] : '44', '', '', '');?></div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.dhl_account_info.name/}</label>
				<div class="input">
					<input type="text" class="box_input" name="Name" value="<?=$DHLShipper['Name']?>" size="46" maxlength="30" notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.dhl_account_info.productCode/}</label>
				<div class="input">
					<input type="text" class="box_input" name="productCode" value="" size="5" maxlength="3" notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.dhl_account_info.products_information/}</label>
				<table border="1">
					<tr>
						<th style="padding: 5px;">{/orders.dhl_account_info.products_pic/}</th>
						<th style="padding: 5px;">{/orders.dhl_account_info.contentIndicator/}</th>
						<th style="padding: 5px;">{/orders.dhl_account_info.description/}</th>
						<th style="padding: 5px;">{/orders.dhl_account_info.descriptionExport/}</th>
					</tr>
					<?php foreach((array)$orders_products_list_row as $k=>$v){ ?>
						<tr>
							<td style="padding: 5px;">
								<div class="pic" style="width: 90px"><img src="<?=$v['PicPath'];?>" style="max-width: 90px;"></div>
							</td>
							<td style="padding: 5px;">
								<div class="insurance_update">
									<span class="input_checkbox_box <?=$v['IsContentIndicator'] ? 'checked':'';?>"><span class="input_checkbox"><input type="checkbox" name="IsContentIndicator[<?=$v['ProId'];?>]" value="1" <?=$v['IsContentIndicator'] ? 'checked':'';?> /></span></span>
								</div>
							</td>
							<td style="padding: 5px;">
								<input type="text" class="box_input" name="DHLDescription[<?=$v['ProId'];?>]" value="<?=$v['DHLDescription'];?>" size="26" maxlength="50" notnull />
							</td>
							<td style="padding: 5px;">
								<input type="text" class="box_input" name="DHLDescriptionExport[<?=$v['ProId'];?>]" value="<?=$v['DHLDescriptionExport'];?>" size="26" maxlength="50" notnull />
							</td>
						</tr>
					<?php }?>
				</table>
			</div>
			<div class="blank15"></div>	
			<div class="rows clean">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="<?=$OrderId;?>" />
			<input type="hidden" name="do_action" value="orders.dhl_get_label" />
		</form>
	</div>
</div>
<?php
}elseif($c['manage']['do']=='export'){
	//订单导出
	$lang_orders=$c['manage']['lang_pack']['orders'];
	$menu_ary=array(
		'A'	=>	$lang_orders['orders'].' ID',		//订单ID
		'B'	=>	$c['manage']['lang_pack']['manage']['manage']['permit_name'][3],	//业务员
		'C'	=>	$lang_orders['oid'],				//订单号
		'D'	=>	$c['manage']['lang_pack']['global']['email'],//邮箱
		'E'	=>	$lang_orders['info']['product_price'],//产品总额
		'F'	=>	$lang_orders['info']['charges'],	//运费
		'G'	=>	$lang_orders['info']['insurance'],	//保险费
		'H'	=>	$lang_orders['total_price'],		//订单总额
		'I'	=>	$lang_orders['info']['weight'],		//总重量
		'J'	=>	$lang_orders['info']['volume'],		//总体积
		'K'	=>	$lang_orders['orders_status'],		//订单状态
		'L'	=>	$lang_orders['info']['ship_info'],	//配送方式
		'M'	=>	$lang_orders['payment_method'],		//付款方式
		'N'	=>	$lang_orders['time'],				//时间
		'O'	=>	$lang_orders['info']['coupon'],		//优惠券
		'P'	=>	$lang_orders['export']['shipname'],
		'Q'	=>	$lang_orders['export']['shipaddress'],
		'R'	=>	$lang_orders['export']['shipaddress2'],
		'S'	=>	$lang_orders['export']['shipcountry'],
		'T'	=>	$lang_orders['export']['shipstate'],
		'U'	=>	$lang_orders['export']['shipcity'],
		'V'	=>	$lang_orders['export']['shipzip'],
		'W'	=>	$lang_orders['export']['shipphone'],
		'X'	=>	$lang_orders['export']['billname'],
		'Y'	=>	$lang_orders['export']['billaddress'],
		'Z'	=>	$lang_orders['export']['billaddress2'],
		'AA'=>	$lang_orders['export']['billcountry'],
		'AB'=>	$lang_orders['export']['billstate'],
		'AC'=>	$lang_orders['export']['billcity'],
		'AD'=>	$lang_orders['export']['billzip'],
		'AE'=>	$lang_orders['export']['billphone'],
		'AF'=>	$lang_orders['shipping']['track_no'],//运单号
		'AG'=>	$lang_orders['payment']['contents'],//备注内容
		'AH'=>	$lang_orders['export']['proname'],	//产品名称
		'AI'=>	$lang_orders['export']['pronumber'],//产品编号
		'AJ'=>	$lang_orders['export']['proqty'],	//产品数量
		'AK'=>	$lang_orders['export']['prosku'],	//产品SKU
        'AL'=>	$lang_orders['export']['proattr'],	//产品属性
		'AM'=>	'产品单价'	//产品单价
	);
	$menu_row=str::str_code(db::get_value('config', 'GroupId="orders_export" and Variable="Menu"', 'Value'));
	$menu_value=str::json_data(htmlspecialchars_decode($menu_row), 'decode');
	if(count($menu_ary)!=count($menu_value)){ //数量不一致
		foreach($menu_ary as $k=>$v){
			if(!$menu_value[$k]) $menu_value[$k]=0;
		}
	}
?>
<div id="fixed_right">
	<div class="global_container fixed_export_config">
		<div class="top_title">{/orders.export.options_config/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="export_config_form">
			<ul class="export_menu">
				<?php
				foreach($menu_value as $k=>$v){
				?>
					<li><span class="input_checkbox_box<?=$v?' checked':'';?>"><span class="input_checkbox"><input type="checkbox" name="Menu[]" value="<?=$k;?>"<?=$v?' checked':'';?> /></span><?=$menu_ary[$k];?></span></li>
				<?php }?>
			</ul>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="do_action" value="orders.orders_export_menu" />
		</form>
	</div>
</div>
<?php }?>