<?php !isset($c) && exit();?>
<?php
manage::check_permit('orders', 1, array(
    'a' => 'inbox'
)); // 检查权限

if ($c['manage']['do'] == 'message') {
    $OrderId = (int) $_GET['OrderId'];
    $NeedId = (int) $_GET['NeedId'];
}
$data_status = 2;
$row = db::get_one('purchase_request', "id='$OrderId'");
$data_status = $row["status"];
$OId=$row["OId"];
$user_id=$row["user_id"];
$message_row = db::get_one('request_message', "Module='purchase' and Subject='$OId'");
$message_mid=$message_row["MId"];

$NeedId=$OId;
//获取图片
$pic_rows = db::get_all('purchase_img', "pr_id='$OrderId'");


$MId = (int) $_GET['MId'];
$page_count = 20;
if (in_array('product_inbox', $c['manage']['plugins']['Used'])) { // APP应用开启产品咨询
    $where = 'm.Module in ("products", "others") and m.UserId>0';
    $unread_where = '(Module in ("products", "others") and IsRead=0 and UserId>0) or (Module="products" and IsRead=0 and UserId=0 and CusEmail!="")';
} else {
    $where = 'm.Module="others" and m.UserId>0';
    $unread_where = 'Module="others" and IsRead=0 and UserId>0';
}
$unread_count = (int) db::get_row_count('user_message', $unread_where);
$top_id_name = ($c['manage']['do'] != 'others_edit' && ! $MId ? 'inbox' : 'inbox_inside');

$row_count = db::get_row_count('user_message m left join user u on m.UserId=u.UserId', $where);
$total_pages = ceil($row_count / $page_count);

if ($row['spid']){
    $pro = db::get_one("business_product","id = ".$row['spid']);
    if ($pro['pid']){
        $shangjia = db::get_one("business","BId = ".$pro['pid']);
    }
}

?>
<style type="text/css">
#main .home_righter .new {
	margin-left: 0px;
}

.inbox_container {
	width: 1000px;
	margin: 0 auto 20px;
	background-color: #fff;
	border: 1px #e5e5e5 solid;
	border-radius: 5px;
}

.inbox_left {
	width: 260px;
	float: left;
}

.inbox_right {
	width: 739px;
	border-left: 1px #e5e5e5 solid;
	float: right;
}

#inbox {
	padding: 0;
}

#inbox .search {
	height: 60px;
	overflow: hidden;
}

#inbox .search>form {
	height: 24px;
	margin: 19px 20px 0;
	padding: 0 8px;
	background-color: #f3f3f3;
	border: 1px transparent solid;
	border-radius: 50px;
	display: inline-block;
}

#inbox .search>form .box_input {
	width: 0;
	height: 24px;
	line-height: 24px;
	padding: 0;
	border: 0;
	float: left;
	transition: width 0.3s;
	-moz-transition: width 0.3s;
	-webkit-transition: width 0.3s;
}

#inbox .search>form .box_input:focus {
	box-shadow: none;
}

#inbox .search>form .search_btn {
	width: 24px;
	height: 24px;
	overflow: hidden;
	text-indent: 99px;
	cursor: pointer;
	background: url(../images/user/icon_search.png) no-repeat center;
	border: 0;
	float: left;
	display: block;
}

#inbox .search>form.show .box_input {
	width: 170px;
	margin-right: 8px;
}

#inbox .search>form:hover .box_input {
	width: 170px;
	margin-right: 8px;
}

#inbox .message_list {
	max-width: 100%;
	height: 100%;
	overflow-x: hidden;
	overflow-y: scroll;
	border-top: 1px #f1f1f1 solid;
	position: relative;
	-webkit-overflow-scrolling: touch;
}

#inbox .message_list li {
	height: 64px;
	line-height: 21px;
	overflow: hidden;
	padding: 0 15px;
	font-size: 12px;
	cursor: pointer;
	border-top: 1px #f1f1f1 solid;
	border-left: 2px transparent solid;
	position: relative;
}

#inbox .message_list li>i {
	min-width: 13px;
	height: 18px;
	line-height: 18px;
	padding: 0 6px;
	text-align: center;
	font-size: 12px;
	color: #fff;
	background-color: #fb4729;
	border-radius: 50px;
	position: absolute;
	top: 14px;
	right: 20px;
	display: block;
}

#inbox .message_list li .email {
	width: 170px;
	height: 21px;
	overflow: hidden;
	margin-top: 12px;
	font-size: 14px;
	white-space: nowrap;
	text-overflow: ellipsis;
	-webkit-text-overflow: ellipsis;
}

#inbox .message_list li .message {
	height: 21px;
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	-webkit-text-overflow: ellipsis;
}

#inbox .message_list li:hover, #inbox .message_list li.current {
	background-color: #f3fcfb;
	border-left-color: #13b287;
}

#inbox .message_list li:first-child {
	border-top: 0;
}

#inbox .message_title {
	height: 59px; /*background-color:#fafafa;*/
}

#inbox .message_title>h2 {
	height: 59px;
	line-height: 59px;
	margin-left: 30px;
	font-size: 16px;
	float: left;
}

#inbox .message_title .products_view {
	height: 38px;
	line-height: 38px;
	padding-top: 12px;
	padding-right: 20px;
	position: relative;
	float: right;
	display: none;
}

#inbox .message_title .products_info {
	display: inline-block;
	vertical-align: top;
}

#inbox .message_title .products_info .img {
	width: 38px;
	height: 38px;
	text-align: center;
	background-color: #fff;
	float: left;
}

#inbox .message_title .products_info .img img {
	max-width: 100%;
	max-height: 100%;
}

#inbox .message_title .products_info .name {
	width: auto;
	max-width: 150px;
	height: 38px;
	overflow: hidden;
	margin-left: 13px;
	font-size: 12px;
	float: left;
	white-space: nowrap;
	text-overflow: ellipsis;
	-webkit-text-overflow: ellipsis;
}

#inbox .message_title .products_info_msg .name {
	max-width: 200px;
}

#inbox .message_title .products_menu {
	width: 43px;
	height: 38px;
	background: url(../images/user/icon_menu.png) no-repeat center;
	position: relative;
	display: inline-block;
	vertical-align: top;
}

#inbox .message_title .products_menu>i {
	width: 8px;
	height: 8px;
	background-color: #fb4729;
	border-radius: 50px;
	position: absolute;
	top: 7px;
	right: 7px;
	display: none;
}

#inbox .message_title .menu_products_list {
	width: 248px;
	position: absolute;
	top: 51px;
	right: -1px;
	z-index: 1;
	display: none;
}

#inbox .message_title .menu_products_list .arrow {
	height: 8px;
	position: relative;
}

#inbox .message_title .menu_products_list .arrow>em, #inbox .message_title .menu_products_list .arrow>i
	{
	width: 0;
	height: 0;
	border-width: 0 7px 8px 7px;
	border-color: transparent transparent #e5e5e5 transparent;
	border-style: solid;
	position: absolute;
	right: 34px;
	bottom: 0;
	display: block;
}

#inbox .message_title .menu_products_list .arrow>i {
	border-color: transparent transparent #fff transparent;
	bottom: -2px;
}

#inbox .message_title .menu_products_list .list {
	overflow-x: hidden;
	overflow-y: scroll;
	background-color: #fff;
	border: 1px #e5e5e5 solid;
}

#inbox .message_title .menu_products_list .list .item {
	height: 50px;
	padding: 15px 19px 15px 20px;
	cursor: pointer;
	border-top: 1px #f1f1f1 solid;
	position: relative;
}

#inbox .message_title .menu_products_list .list .item>i {
	min-width: 13px;
	height: 18px;
	line-height: 18px;
	padding: 0 6px;
	text-align: center;
	font-size: 12px;
	color: #fff;
	background-color: #fb4729;
	border-radius: 50px;
	position: absolute;
	top: 11px;
	left: 64px;
	display: block;
}

#inbox .message_title .menu_products_list .list .item .img {
	width: 50px;
	height: 50px;
	background-color: #fff;
	border: 1px #f1f1f1 solid;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

#inbox .message_title .menu_products_list .list .item .img>img {
	max-width: 100%;
	max-height: 100%;
}

#inbox .message_title .menu_products_list .list .item .name {
	width: 116px;
	height: 40px;
	line-height: 20px;
	overflow: hidden;
	margin: 5px 0 5px 24px;
	font-size: 12px;
	float: left;
}

#inbox .message_title .menu_products_list .list .item:first-child {
	border: 0;
}

#inbox .message_title .menu_products_list .list .item:hover, #inbox .message_title .menu_products_list .list .item.current
	{
	background-color: #f6f6f6;
}

#inbox .message_title .menu_products_list li.more {
	height: 50px;
	padding: 0;
	text-align: center;
}

#inbox .message_title .menu_products_list li.more .btn_more {
	height: 26px;
	line-height: 26px;
	margin-top: 10px;
	text-decoration: none;
	font-size: 14px;
	color: #fff;
	background-color: #ddd;
	border: 0;
}

#inbox .message_dialogue {
	overflow-x: hidden;
	overflow-y: scroll;
	padding: 5px 35px; /*background-color:#fafafa;*/
	border-top: 1px #f1f1f1 solid;
	-webkit-overflow-scrolling: touch;
}

#inbox .dialogue_box {
	width: 425px;
	margin: 10px;
}

#inbox .dialogue_box .time {
	height: 27px;
	line-height: 27px;
	font-size: 12px;
}

#inbox .dialogue_box .message {
	line-height: 25px;
	padding: 15px;
	background-color: #eaeaea;
	border-radius: 5px;
	position: relative;
}

#inbox .dialogue_box .picture {
	width: 100px;
	height: 100px;
	margin-top: 10px;
	padding: 15px;
	text-align: center;
	background-color: #eaeaea;
	border-radius: 5px;
	float: left;
	position: relative;
}

#inbox .dialogue_box .picture img {
	max-width: 100%;
	max-height: 100%;
}

#inbox .dialogue_box_left .message:before, #inbox .dialogue_box_left .picture:before
	{
	width: 11px;
	height: 13px;
	background: url(../images/frame/icon_dialogue_box_left_gray.png)
		no-repeat;
	position: absolute;
	bottom: 6px;
	left: -10px;
	display: block;
	content: '';
}

#inbox .dialogue_box_right {
	text-align: right;
	float: right;
}

#inbox .dialogue_box_right .message {
	text-align: left;
	background-color: #b9f2e2;
	overflow: hidden;
}

#inbox .dialogue_box_right .picture {
	background-color: #b9f2e2;
	float: right;
}

#inbox .dialogue_box_right .message:after, #inbox .dialogue_box_right .picture:after
	{
	width: 11px;
	height: 13px;
	background: url(../images/frame/icon_dialogue_box_right.png) no-repeat;
	position: absolute;
	bottom: 6px;
	right: -10px;
	display: block;
	content: '';
}

#inbox .message_bottom {
	height: 130px;
	padding: 0 20px;
	background-color: #fff;
	border-top: 1px #f1f1f1 solid;
}

#inbox .form_message {
	padding: 17px 0;
}

#inbox .form_message .box_textarea {
	width: 699px;
	height: 54px;
	line-height: 18px;
	padding: 0;
	overflow-x: hidden;
	overflow-y: auto;
	border: 0;
	-webkit-overflow-scrolling: touch;
}

#inbox .form_message .box_textarea:focus {
	box-shadow: none;
}

#inbox .form_message .btn_submit {
	margin-top: 10px;
	color: #fff;
	background-color: #0cb083;
	float: right;
}

#inbox .mail_box {
	padding-top: 85px;
}

#inbox .mail_box .txt_tips {
	float: right;
	font-size: 12px;
	color: #999;
	margin-right: 15px;
	margin-top: 8px;
}

#inbox .mail_box .btn_submit {
	float: right;
}

#inbox .message_title, #inbox .message_dialogue, #inbox .message_bottom
	{ /*display:none;*/
	
}

#inbox .unread_message {
	width: 100%;
	text-align: center;
	position: relative;
	display: inline-block;
}

#inbox .unread_message>p {
	width: 100%;
	height: 34px;
	line-height: 34px;
	padding-top: 110px;
	text-align: center;
	font-size: 18px;
	background: url(../images/user/bg_unread_message.png) no-repeat center
		top;
	position: absolute;
	top: 35%;
	left: 0;
}

#inbox .unread_message>i {
	width: 35px;
	height: 35px;
	line-height: 35px;
	text-align: center;
	font-size: 18px;
	color: #fff;
	background-color: #fb4729;
	border-radius: 50px;
	position: absolute;
	top: 34%;
	left: 415px;
}

.request_status {
	color: #fff;
	background-color: #0cb083;
	border-color: #0cb083;
}
</style>

<div id="orders_inside" class="r_con_wrap" style="height: 343px;">
	<div class="center_container_1200">
		<a href="javascript:history.back(-1);" class="return_title"> <span
			class="return">采购需求</span> <span class="s_return">/ NO.#<?=$NeedId ?></span><br>
			<span class="s_return">创建于  <?=date('Y-m-d H:i:s', $row['created_at']);?></span>
		</a>

		<div class="left_container">
			<div class="left_container_side" style="width: 100%;">
				<div class="global_container box_order_info">
					<div class="big_title">
						<strong>状态</strong>

						<div class="orders_parent_box orders_parent_top_box mb20">
							<div class="clean mr5">
								<div id="request_status" class="orders_parent_box"
									style="width: 100%;">
									<div class="global_container orders_info mr5">
										<input id="data_status1" type="button"
											class="btn_global btn_orders_status btn_next" data-status="1"
											value="新建"> <input id="data_status2" type="button"
											class="btn_global btn_orders_status btn_next "
											data-status="2" value="处理中"> <input id="data_status3"
											type="button" class="btn_global btn_orders_status btn_next"
											data-status="3" value="已完成">
											 <input id="data_status4"
											type="button" class="btn_global btn_orders_status btn_next"
											data-status="4" value="已转为订单">
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="global_container">
						<div class="big_title">

							<strong>详情</strong>
							<div class="orders_parent_box orders_parent_top_box mb20">

								<div class="clean mr5">
									<div class="orders_parent_box" style="width: 100%;">
										<div class="global_container orders_info mr5"
											>
											<ul class="list">
												<li class="clean"><strong>产品名称:</strong> <span
													id="orders_total_price"><?=$row["product_name"]?></span> <span
													id="orders_total_price_web" class="web_price"></span></li>
												<li class="clean"><strong>描述:</strong> <span
													id="orders_product_price"><?=$row["description"]?></span> <span
													id="orders_product_price_web" class="web_price"></span></li>
												<li class="clean"><strong>需求数量:</strong> <span
													id="orders_shipping_price"><?=$row["quantity"]?></span> <span
													id="orders_shipping_price_web" class="web_price"></span></li>
												<li class="clean"><strong>产品链接:</strong> <span
													id="orders_shipping_insurance_price"><?=$row["factory_info"]?></span>
													<span id="orders_shipping_insurance_price_web"
													class="web_price"></span></li>
												<li class="clean"><strong>工厂信息:</strong> <span
													id="orders_shipping_insurance_price"><?=$row["product_url"]?></span>
													<span id="orders_shipping_insurance_price_web"
													class="web_price"></span></li>
												<li class="clean"><strong>参考价格:</strong> <span
													id="orders_user_discount">￥<?=$row["refer_price"]?></span>
												</li>
                                                <li class="clean"><strong>来源:</strong>
                                                    <span><?=$row["spid"]?"http://lemonyyshop.com/supplierproduct/?sp=".$row['spid']:"无"; ?></span>
                                                </li>

												<li class="clean"><strong>图片:</strong>

													<div class="input">
														<div class="multi_img upload_file_multi pro_multi_img"
															id="PicDetail" data-listidx="0">
															<?php foreach ($pic_rows as $pic_item ){?>
															<dl class="img isfile" num="<?=$pic_item["id"] ?>">
																<dt class="upload_box preview_pic">
																		<a href="<?=$pic_item["img_url"] ?>" target="_blank"><img
																		src="<?=$pic_item["img_url"] ?>"><em></em></a>
																</dt>
															</dl>
															<?php }?>
<!-- 															<dl class="img isfile" num="2"> -->
<!-- 																<dt class="upload_box preview_pic"> -->
<!-- 																	<a href="/u_file/1910/products/18/4d9de22e25.jpg.240x240.jpg" target="_blank"><img -->
<!-- 																		src="/u_file/1910/products/18/4d9de22e25.jpg.240x240.jpg"><em></em></a> -->
<!-- 																</dt> -->
																
<!-- 															</dl> -->
<!-- 															<dl class="img isfile" num="3"> -->
<!-- 																<dt class="upload_box preview_pic"> -->
<!-- 																<a href="/u_file/1910/products/18/4c571f9ce4.jpg.240x240.jpg" target="_blank"><img -->
<!-- 																		src="/u_file/1910/products/18/4c571f9ce4.jpg.240x240.jpg"><em></em></a> -->
<!-- 																</dt> -->
																
<!-- 															</dl> -->
															
														</div>
													</div></li>

												</li>
												<li class="clean"></li>
											</ul>
										</div>
									</div>

								</div>
							</div>
                            <? if($shangjia){ ?>
                                <strong>商家信息</strong>
                                <div class="orders_parent_box orders_parent_top_box mb20">

                                    <div class="clean mr5">
                                        <div class="orders_parent_box" style="width: 100%;">
                                            <div class="global_container orders_info mr5">
                                                <ul class="list">
                                                    <li class="clean"><strong>商家编号:</strong><?=$shangjia["memberNo"]?></li>
                                                    <li class="clean"><strong>商家名称:</strong><?=$shangjia["Name"]?></li>
                                                    <li class="clean"><strong>地址:</strong><?=$shangjia["Address"]?></li>
                                                    <li class="clean"><strong>联系人:</strong><?=$shangjia["Contacts"]?></li>
                                                    <li class="clean"><strong>联系电话:</strong><?=$shangjia["Phone"]?></li>

                                                    <li class="clean"></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <? } ?>
						</div>
					</div>
					<div class="global_container">
						<div class="big_title">
							<strong>跟单会话</strong>
							<strong id="inbox_count" style="font-size: 13px;"></strong>
							
						</div>

						<!-- 跟单会话 跟单开始-->
						<div id="inbox" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>"
							style="overflow-x: hidden;">
							<div class="center_container_1000"
								style="max-width: 100%; margin: 0;">
								<!--         <div class="inside_container"> -->
								<!--           <a href="javascript:history.back(-1);" class="return_title"> <span -->
								<!-- 						class="return">采购需求</span> <span class="s_return">/ 跟单会话</span> -->
								<!-- 					</a> -->
								<!--         </div> -->
								<div class="inbox_container clean" style="width: 100%;">
									<div class="inbox_left" style="display: none;">
										<div class="search">
											<form id="search_form">
												<input type="text" class="box_input" name="Keyword" value=""
													size="15" autocomplete="off" /> <input type="button"
													class="search_btn" value="{/global.search/}" />
											</form>
										</div>
										<ul class="message_list" data-page="1"
											data-total-pages="<?=$total_pages;?>"
											data-count="<?=$page_count;?>" data-animate="0"></ul>
									</div>
									<div class="inbox_right" style="width: 100%;">
										<div class="unread_message" style="display: none;">
											<p class="color_aaa">{/notes.unread_message/}</p>
											<i><?=$unread_count;?></i>
										</div>
										<div class="message_title">
											<h2 class="color_000"><?=$NeedId; ?></h2>
											<div class="products_view">
												<div class="products_info">
													<div class="img">
														<a href="javascript:;"><img src="" /></a>
													</div>
													<div class="name">
														<a href="javascript:;" title=""></a>
													</div>
												</div>
												<a href="javascript:;" class="products_menu"><i></i></a>
												<div class="menu_products_list" data-page="0">
													<div class="arrow">
														<em></em><i></i>
													</div>
													<ul class="list">
														<li class="item current" data-id="<?=$message_mid ?>"
															data-user-id="<?=$user_id; ?>" data-oid="<?=$OId; ?>">
															<div class="img">
																<img src="/static/manage/images/user/bg_message.png">
															</div>
															<div class="name color_555">消息</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="message_dialogue clean" style="width: 93%;">
											<div class="message_dialogue_list"></div>
										</div>
										<div class="message_bottom">
											<div class="send_box">
												<form class="form_message" id="message_inbox_form">
													<textarea name='Message' class="box_textarea clean"
														placeholder="{/global.please_enter/} ..." notnull></textarea>
													<div class="clear"></div>
                            <?=manage::multi_img('MsgPicDetail', 'MsgPicPath'); ?>
                            <input type="button"
														class="btn_global btn_submit" value="{/global.send/}" /> <input
														type="hidden" name="MId" value="<?=$OrderId; ?>" /> <input
														type="hidden" name="do_action" value="orders.inbox_reply" />
												</form>
											</div>
											<div class="mail_box" style="display: none;">
												<a href="javascript:;" class="btn_global btn_submit">{/frame.email/}</a>
												<div class="txt_tips">{/inbox.non_user_tips/}</div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- 跟单会话结束 -->



					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="OrderId" name="OrderId"
			value="<?=$OrderId; ?>" /> <input type="hidden"
			id="data_status_value" name="data_status_value"
			value="<?=$data_status; ?>" />
	</div>



	<script type="text/javascript">
$(function(){
	orders_obj.inbox_init();
	$("#request_status").find("input").bind("click",function(){
		var $Status=$(this).attr('data-status'),
		$OrderId=$('#OrderId').val();
// 		$Status=$('#data_status_value').val();
		$.post('./?do_action=orders.orders_request_mod_status', {'OrderId':$OrderId, 'OrderStatus':$Status}, function(data){
			if(data.ret==1){
				window.location.reload();
			}
		}, 'json');
	return false;
    });
    $data_status=$('#data_status_value').val();
    $("#data_status"+$data_status).addClass('request_status');
	
});
</script>