<?php !isset($c) && exit();?>
<?php
manage::check_permit('orders', 1, array('a'=>'import_delivery'));//检查权限
if(!in_array('import_delivery', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}
echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js','/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
?>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<div id="import_delivery" class="r_con_wrap">
	<script type="text/javascript">$(document).ready(function(){orders_obj.orders_import_init()});</script>
	<form id="import_delivery_form" class="global_form center_container" name="import_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
		<h1>{/module.orders.import_delivery/}</h1>
		<div class="global_container">
			<div class="explode_box">
				<div class="box_item">
					<div class="tit"><em>1</em>{/global.down_table/}</div>
					<div class="desc"><?=str_replace('%url%', './?do_action=orders.import_excel_download', $c['manage']['lang_pack']['global']['click_here']); ?></div>
				</div>
				<div class="box_item">
					<div class="tit"><em>2</em>{/global.upload_table/}</div>
					<div class="desc">{/global.submit_table/}</div>
					<div class="input upload_file">
						<input type="text" class="box_input" id="excel_path" name="ExcelFile" value="" size="50" maxlength="100" readonly notnull />
						<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
						<div class="row fileupload-buttonbar">
							<span class="btn_file btn-success fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>{/global.file_upload/}</span>
								<input type="file" name="Filedata" multiple>
							</span>
							<div class="fileupload-progress fade"><div class="progress-extended"></div></div>
							<div class="clear"></div>
							<div class="photo_multi_img template-box files"></div>
							<div class="photo_multi_img" id="PicDetail"></div>
						</div>
						<script id="template-upload" type="text/x-tmpl">
						{% for (var i=0, file; file=o.files[i]; i++) { %}
							<div class="template-upload fade">
								<div class="clear"></div>
								<div class="items">
									<p class="name">{%=file.name%}</p>
									<strong class="error text-danger"></strong>
								</div>
								<div class="items">
									<p class="size">Processing...</p>
									<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
								</div>
								<div class="items">
									{% if (!i) { %}
										<button class="btn_file btn-warning cancel">
											<i class="glyphicon glyphicon-ban-circle"></i>
											<span>{/global.cancel/}</span>
										</button>
									{% } %}
								</div>
								<div class="clear"></div>
							</div>
						{% } %}
						</script>
						<script id="template-download" type="text/x-tmpl">
						{% for (var i=0, file; file=o.files[i]; i++) { %}
							{% if (file.thumbnailUrl) { %}
								<div class="pic template-download fade hide">
									<div>
										<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
										<a href="{%=file.url%}" class="zoom" target="_blank"></a>
										{% if (file.deleteUrl) { %}
											<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
											<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
										{% } %}
										<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
									</div>
									<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
								</div>
							{% } else { %}
								<div class="template-download fade hide">
									<div class="clear"></div>
									<div class="items">
										<p class="name">
											{% if (file.url) { %}
												<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
											{% } else { %}
												<span>{%=file.name%}</span>
											{% } %}
										</p>
										{% if (file.error) { %}
											<div><span class="label label-danger">Error</span> {%=file.error%}</div>
										{% } %}
									</div>
									<div class="items">
										<span class="size">{%=o.formatFileSize(file.size)%}</span>
									</div>
									<div class="items">
										{% if (file.deleteUrl) { %}
											<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
												<i class="glyphicon glyphicon-trash"></i>
												<span>{/global.del/}</span>
											</button>
											<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
										{% } else { %}
											<button class="btn_file btn-warning cancel">
												<i class="glyphicon glyphicon-ban-circle"></i>
												<span>{/global.cancel/}</span>
											</button>
										{% } %}
									</div>
									<div class="clear"></div>
								</div>
							{% } %}
						{% } %}
						</script>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input">
							<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
							<input type="hidden" name="do_action" value="orders.orders_import" />
							<input type="hidden" name="Number" value="0" />
						</div>
					</div>
				</div>
			</div>
			<div id="progress_container">
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="6%" nowrap="nowrap">{/global.serial/}</td>
							<td width="32%" nowrap="nowrap">{/orders.oid/}</td>
							<td width="32%" nowrap="nowrap">{/orders.shipping.track_no/}</td>
							<td width="30%" nowrap="nowrap">{/global.status/}</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<div id="progress_loading"></div>
			</div>
		</div>
    </form>
</div>