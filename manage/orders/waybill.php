<?php !isset($c) && exit();?>
<?php
manage::check_permit('orders', 1, array('a'=>'waybill'));//检查权限
//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol');
foreach ($currency_row as $k => $v) {
	$all_currency_ary[$v['Currency']] = $v;
}
//发货地
$overseas_ary = array();
$overseas_row = str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
foreach ($overseas_row as $v) {
	$overseas_ary[$v['OvId']] = $v;
}
//权限
$permit_ary = array(
	'edit'	=>	manage::check_permit('orders', 0, array('a'=>'waybill', 'd'=>'edit'))
);

$OrderStatus = (int)$_GET['OrderStatus']; //订单状态（搜索）
$query_string = ly200::query_string('OrderStatus');

$Keyword = str::str_code($_GET['Keyword']);
$page_count = 10;//显示数量
$where = 'o.OrderStatus>5 and o.OrderStatus<8'; //条件
$OrderStatus && $where .= " and o.OrderStatus='$OrderStatus'";
(int)$_SESSION['Manage']['GroupId'] == 3 && $where .= " and ((o.SalesId>0 and o.SalesId='{$_SESSION['Manage']['SalesId']}') or (o.SalesId=0 and u.SalesId='{$_SESSION['Manage']['SalesId']}'))"; //业务员账号过滤
if ($Keyword) {
    //搜索模式
	$where .= " and ((o.OId like '%$Keyword%' or concat(o.OId, '', w.Number) like '%$Keyword%' or o.TrackingNumber like '%$Keyword%' or w.TrackingNumber like '%$Keyword%')";
    $products_row = db::get_all('products', "(Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%')", 'ProId');
    $pro_id_ary='';
    foreach((array)$products_row as $v){
    	$pro_id_ary.=','.$v['ProId'];
    }
    $pro_id_ary=trim($pro_id_ary, ',');
    $pro_id_ary && $orderid_row = db::get_all('orders_products_list', "ProId in ({$pro_id_ary})", 'OrderId');
    $orderid_ary='';
    foreach((array)$orderid_row as $v){
    	$orderid_ary.=','.$v['OrderId'];
    }
    $orderid_ary=trim($orderid_ary, ',');
    $orderid_ary && $where.=" or o.OrderId in ({$orderid_ary})";
    $where.=')';
	$orders_row = str::str_code(db::get_limit_page('orders o left join orders_waybill w on o.OrderId=w.OrderId left join user u on o.UserId=u.UserId', $where." group by o.OrderId", "o.*, concat(o.OId, '', w.Number) as Num, w.WId , w.Number, w.ProInfo, w.Status, w.TrackingNumber as WTrackingNumber, w.ShippingTime as WShippingTime, w.Remarks as WRemarks, u.SalesId", 'o.OrderId desc', (int)$_GET['page'], $page_count));
} else {
    //全部显示
	$orders_row = str::str_code(db::get_limit_page('orders o left join user u on o.UserId=u.UserId', $where, 'o.*, u.SalesId', 'o.OrderId desc', (int)$_GET['page'], $page_count));
}
$i = 1;

$orderid_where = '0';
$orders_shipping_template = array();
foreach ($orders_row[0] as $v) { 
	$orderid_where .= ','.$v['OrderId'];
	$orders_shipping_template[$v['OrderId']] = $v['shipping_template'];
}

$order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId in($orderid_where)", 'o.*, o.SKU as OrderSKU, p.Prefix, p.Number, p.SKU, p.PicPath_0', 'o.OvId asc, o.LId asc');
$BId = '0';
$pro_ary = $order_list_ary = $pro_qty_ary = $waybill_ary = $OvId_ary = array();
foreach($order_list_row as $v){
	$pro_ary[$v['LId']] = $v; //订单产品信息
	if ($orders_shipping_template[$v['OrderId']]) {
		$order_list_ary[$v['OrderId']][$v['CId']]['00'][] = $pro_ary[$v['LId']]; //总信息
	} else {
		$order_list_ary[$v['OrderId']][$v['OvId']]['00'][] = $pro_ary[$v['LId']]; //总信息
	}
	$pro_qty_ary[$v['LId']] = $v['Qty'];
	$OvId_ary[$v['OrderId']] = $v['OvId'];
}

$orders_waybill_row = db::get_all('orders_waybill', "OrderId in($orderid_where)");
foreach ($orders_waybill_row as $v) {
	$i = 0;
	$waybill_ary[$v['OrderId']][$v['Number']] = $v;
	$ProInfo = str::json_data(htmlspecialchars_decode($v['ProInfo']), 'decode');
	foreach ($ProInfo as $k2 => $v2) { //$k2==LId $v2==QTY
		$ovid = $pro_ary[$k2]['OvId'];
		$order_list_ary[$v['OrderId']][$ovid][$v['Number']][$i] = $pro_ary[$k2];
		$order_list_ary[$v['OrderId']][$ovid][$v['Number']][$i]['Qty'] = $v2;
		$pro_qty_ary[$k2] -= $v2;
		++$i;
	}
}

$orders_ary = array();
foreach ($orders_row[0] as $v) {
	$orders_ary[$v['OrderId']] = $v;
	//发货信息
	if(!$v['TrackingNumber'] && $v['ShippingTime'] == 0 && !$v['Remarks']){
        //多个发货地
		$ship_ary[$v['OrderId']] = array(
			'ShippingExpress'		=>	str::json_data(htmlspecialchars_decode($v['ShippingOvExpress']), 'decode'),
			'ShippingMethodSId'		=>	str::json_data(htmlspecialchars_decode($v['ShippingOvSId']), 'decode'),
			'ShippingType'			=>	str::json_data(htmlspecialchars_decode($v['ShippingOvType']), 'decode'),
			'ShippingInsurance'		=>	str::json_data(htmlspecialchars_decode($v['ShippingOvInsurance']), 'decode'),
			'ShippingPrice'			=>	str::json_data(htmlspecialchars_decode($v['ShippingOvPrice']), 'decode'),
			'ShippingInsurancePrice'=>	str::json_data(htmlspecialchars_decode($v['ShippingOvInsurancePrice']), 'decode'),
			'TrackingNumber'		=>	str::json_data(htmlspecialchars_decode($v['OvTrackingNumber']), 'decode'),
			'ShippingTime'			=>	str::json_data(htmlspecialchars_decode($v['OvShippingTime']), 'decode'),
			'Remarks'				=>	str::json_data(htmlspecialchars_decode($v['OvRemarks']), 'decode')
		);
	}else{
        //单个发货地
		$OvId = $OvId_ary[$v['OrderId']];
		$ship_ary[$v['OrderId']] = array(
			'ShippingExpress'		=>	array($OvId=>$v['ShippingExpress']),
			'ShippingMethodSId'		=>	array($OvId=>(int)$v['ShippingMethodSId']),
			'ShippingType'			=>	array($OvId=>$v['ShippingType']),
			'ShippingInsurance'		=>	array($OvId=>(int)$v['ShippingInsurance']),
			'ShippingPrice'			=>	array($OvId=>(float)$v['ShippingPrice']),
			'ShippingInsurancePrice'=>	array($OvId=>(float)$v['ShippingInsurancePrice']),
			'TrackingNumber'		=>	array($OvId=>$v['TrackingNumber']),
			'ShippingTime'			=>	array($OvId=>$v['ShippingTime']),
			'Remarks'				=>	array($OvId=>$v['Remarks'])
		);
	}
}
krsort($order_list_ary); //倒序

echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');

//查询运费
echo ly200::load_static('/static/js/plugin/track/17track/externalcall.js');
$UILang=substr($c['manage']['web_lang'], 1); //UI语言
$UILang=str_replace(array('jp', 'zh_tw'), array('ja', 'zh-tw'), $UILang); //日文、繁体中文
?>
<div id="waybill" class="r_con_wrap">
	<script type="text/javascript">$(document).ready(function(){orders_obj.waybill_init();});</script>
	<div class="inside_container">
		<h1>{/module.orders.waybill/}</h1>
		<ul class="inside_menu unusual">
			<li><a href="./?m=orders&a=waybill" status="0"<?=$OrderStatus==0 ? ' class="current"' : '';?>>{/global.all/}</a></li>
			<?php
			foreach ($c['orders']['status'] as $k => $v) {
				if ($k<6 || $k>7) continue;
			?>
				<li><a href="./?OrderStatus=<?=$k;?>&<?=$query_string;?>" status="<?=$k;?>"<?=$OrderStatus==$k ? ' class="current"' : '';?>>{/orders.status.<?=$k;?>/}</a></li>
			<?php }?>
		</ul>
	</div>
	<div class="inside_waybill">
		<div class="list_menu">					
			<form method="get" action="?">
				<div class="k_input">
					<input type="text" name="Keyword" value="" placeholder="{/orders.product.search/}" class="form_input long_form_input" size="15" autocomplete="off">
					
				</div>
				<input type="submit" class="search_btn" value="{/global.search/}">
				<div class="clear"></div>
				<input type="hidden" name="m" value="orders">
				<input type="hidden" name="a" value="waybill">
				<input type="hidden" name="OrderStatus" value="waybill">
			</form>
		</div>
		<?php
		foreach ((array)$order_list_ary as $OrderId => $obj) {
			$Symbol=$obj['ManageCurrency'] ? $all_currency_ary[$orders_ary[$OrderId]['ManageCurrency']]['Symbol'] : $c['manage']['currency_symbol'];
		?>
			<div class="global_container">
				<div class="orders_info"><span class="orders_number color_000">No.#<?=$orders_ary[$OrderId]['OId'];?></span></div>
				<?php
				$num=1;
				foreach ($obj as $OvId => $row) {
					foreach ($row as $key => $val) {
						$amount = $quantity = $is_edit = 0;
				?>
						<div class="waybill_list" data-ovid="<?=$OvId;?>" data-number="<?=$key;?>" data-orderid="<?=$OrderId;?>">
							<div class="waybill_title clean">
								<div class="package_name color_000">{/orders.shipping.package/}<?=$num;?></div>
								<div class="package_view">
									<?php
									$ShipAry = array();
                                    $is_edit = 1;
									if ($key == '00' && $ship_ary[$OrderId]['ShippingExpress'][$OvId]) {
                                        //多个海外仓 主体产品
										$ShipAry['ShippingExpress'] = $ship_ary[$OrderId]['ShippingExpress'][$OvId];
										$ShipAry['ShippingInsurancePrice'] = $ship_ary[$OrderId]['ShippingInsurancePrice'][$OvId];
										$ShipAry['TrackingNumber'] = $ship_ary[$OrderId]['TrackingNumber'][$OvId];
										$ShipAry['ShippingTime'] = $ship_ary[$OrderId]['ShippingTime'][$OvId];
										$ShipAry['Remarks'] = $ship_ary[$OrderId]['Remarks'][$OvId];
									} elseif ($key != '00' && $waybill_ary[$OrderId][$key]) {
                                        //多个海外仓 分体产品
										$ShipAry['ShippingExpress'] = $ship_ary[$OrderId]['ShippingExpress'][$OvId];
										$ShipAry['ShippingInsurancePrice'] = 0;
										$ShipAry['TrackingNumber'] = $waybill_ary[$OrderId][$key]['TrackingNumber'];
										$ShipAry['ShippingTime'] = $waybill_ary[$OrderId][$key]['ShippingTime'];
										$ShipAry['Remarks'] = $waybill_ary[$OrderId][$key]['Remarks'];
									} else {
                                        //单个海外仓
										$ShipAry['ShippingExpress'] = $orders_ary[$OrderId]['ShippingExpress'];
										$ShipAry['ShippingInsurancePrice'] = $orders_ary[$OrderId]['ShippingInsurancePrice'];
										$ShipAry['TrackingNumber'] = $orders_ary[$OrderId]['TrackingNumber'];
										$ShipAry['ShippingTime'] = $orders_ary[$OrderId]['ShippingTime'];
										$ShipAry['Remarks'] = $orders_ary[$OrderId]['Remarks'];
									}
                                    if($ShipAry['TrackingNumber']){
                                        $is_edit = 0;
                                    }?>
									<span class="orders_shipping color_888">{/orders.shipping.method/}：<?=$ShipAry['ShippingExpress'];?></span>
									<?php if (!$orders_shipping_template[$OrderId]) { ?>
										<span class="orders_insurance color_888">{/orders.info.insurance/}：<?=$Symbol.sprintf('%01.2f', $ShipAry['ShippingInsurancePrice']);?></span>
									<?php }?>
									<?php if ($is_edit == 0) {?>
										<span class="package_track color_888">{/orders.shipping.track_no/}: <span class="query" id="<?=$orders_ary[$OrderId]['OId'] . $OvId . $key;?>"><?=$ShipAry['TrackingNumber'];?></span>&nbsp;&nbsp;&nbsp;&nbsp;{/orders.info.delivery_time/}: <?=date('Y-m-d', $ShipAry['ShippingTime']);?><?=$ShipAry['Remarks'] ? '&nbsp;&nbsp;&nbsp;&nbsp;{/orders.payment.contents/}: ' . $ShipAry['Remarks'] : '';?></span>
										<script type="text/javascript">
										YQV5.trackSingleF1({
											YQ_ElementId:"<?=$orders_ary[$OrderId]['OId'] . $OvId . $key;?>",	//必须，指定悬浮位置的元素ID。
											YQ_TargetId:"waybill",	//可选，指定悬浮位置的父级元素ID
											YQ_Width:600,	//可选，指定查询结果宽度，最小宽度为600px，默认撑满容器。
											YQ_Height:400,	//可选，指定查询结果高度，最大高度为800px，默认撑满容器。
											YQ_Lang:"<?=$UILang;?>",	//可选，指定UI语言，默认根据浏览器自动识别。
											YQ_Num:"<?=$ShipAry['TrackingNumber'];?>"	//必须，指定要查询的单号。
										});
										</script>
									<?php }?>
								</div>
								<?php
								if ($is_edit == 1 || $orders_shipping_template[$OrderId]) {
									$total_qty = 0; //统计产品总数量
									foreach ($val as $k => $v) {
										if ($key == '00') {
                                            $qty = (int)$pro_qty_ary[$v['LId']];
                                        } else {
                                            $qty = (int)$v['Qty'];
                                        }
										$total_qty += $qty;
									}
								?>
									<div class="package_button">
										<input type="button" data-number="<?=htmlspecialchars($ShipAry['TrackingNumber']);?>" data-time="<?=@date('Y-m-d', (int)$ShipAry['ShippingTime']);?>" data-remarks="<?=htmlspecialchars($ShipAry['Remarks']);?>" class="btn_global btn_delivery" value="{/orders.shipping.shipped/}" />
										<?php
										if (!$orders_shipping_template[$OrderId]) {
											if ($key == '00') { //拆单
												if ($total_qty > 1) echo '<input type="button" class="btn_global btn_separate" value="{/orders.product.separate/}" />';//产品数量必须大于1
											} else { //合单
												echo '<input type="button" class="btn_global btn_merge" value="{/orders.product.merge/}" />&nbsp;&nbsp;<span class="box_explain">( {/orders.explain.merge/} )</span>';
											}
										}?>
									</div>
								<?php }?>
							</div>
							<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
								<thead>
									<tr>
										<td width="5%" nowrap="nowrap">{/global.serial/}</td>
										<td width="50%" nowrap="nowrap">{/products.product/}</td>
										<td width="37%" nowrap="nowrap">{/products.attribute/}</td>
										<td width="8%" nowrap="nowrap">{/orders.quantity/}</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									foreach ($val as $k => $v) {
										if ($key == '00') {
											$qty = (int)$pro_qty_ary[$v['LId']];
										} else {
											$qty = (int)$v['Qty'];
										}
										if ($qty < 1) continue;
										$v['Name' . $c['manage']['web_lang']] = $v['Name'];
										$price = $v['Price'] + $v['PropertyPrice'];
										$v['Discount'] < 100 && $price *= $v['Discount'] / 100;
										$price = (float)substr(sprintf('%01.3f', $price), 0, -1);
										$amount += $price * $qty;
										if ($k == 3) echo '</tbody><tbody class="more_product">';
									?>
										<tr>
											<td><?=$i;?></td>
											<td>
												<?php
												if ($v['BuyType'] == 4) {
													$package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
													$attr = array();
													$v['Property'] != '' && $attr = str::json_data(($v['Property']), 'decode');
													$products_row = str::str_code(db::get_all('products', "SoldOut=0 and ProId='{$package_row['ProId']}'"));
													$pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
													$pro_where == '' && $pro_where = 0;
													$products_row = array_merge($products_row, str::str_code(db::get_all('products', "SoldOut=0 and ProId in($pro_where)")));
													$data_ary = str::json_data(htmlspecialchars_decode($package_row['Data']), 'decode');
												?>
													<h4>[ Sales ]</h4>
													<div class="blank6"></div>
													<?php
													foreach ((array)$products_row as $k2 => $v2) {
														$quantity += $qty;
														$img = ly200::get_size_img($v2['PicPath_0'], '240x240');
														$url = ly200::get_url($v2, 'products', $c['manage']['web_lang']);
													?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" alt="<?=$v2['Name' . $c['manage']['web_lang']];?>" /></a></dt>
														<dd<?=$_GET['do'] == 'print' ? ' style="padding-right:0;"' : '';?>>
															<h4><a href="<?=$url;?>" title="<?=$v2['Name' . $c['manage']['web_lang']]?>" class="green" target="_blank"><?=$v2['Name'.$c['manage']['web_lang']];?></a></h4>
															<?=$v2['Number'] != '' ? '<p>{/products.products.number/}: ' . $v2['Prefix'] . $v2['Number'] . '</p>' : '';?>
															<?=$v2['SKU'] != '' ? '<p>{/products.products.sku/}: ' . $v2['SKU'] . '</p>' : '';?>
															<?php if ($k2 == 0){ ?>
																<div>
																	<?php
																	foreach ((array)$attr as $k3 => $z) {
																		if ($k3 != 'Overseas') {
																			echo '<p class="attr_' . $k3 . '">' . $k3 . ': ' . $z . '</p>';
																		}
																	}?>
																</div>
															<?php } elseif ($data_ary[$v2['ProId']]){ ?>
																<div>
																	<?php
																	$OvId=0;
																	foreach ((array)$data_ary[$v2['ProId']] as $k3 => $v3) {
																		if ($k3 != 'Overseas') {
																			echo '<p class="attr_' . $k3 . '">' . $attribute_ary[$k3][1] . ': ' . $vid_data_ary[$k3][$v3] . '</p>';
																		}
																	}?>
																</div>
															<?php }?>
														</dd>
													</dl>
													<div class="blank6"></div>
													<?php }?>
													<?php if ($v['Remark']){ ?>
                                                        <dl><dd><p class="remark">{/orders.remark/}: <?=$v['Remark'];?></p></dd></dl>
                                                    <?php }?>
												<?php 
												} else {
													$attr = str::json_data(($v['Property']), 'decode');
													$url = ly200::get_url($v, 'products', $c['manage']['web_lang']);
													$quantity += $qty;
													$SKU = $v['OrderSKU'] ? $v['OrderSKU'] : $v['SKU'];
													$img = @is_file($c['root_path'] . $v['PicPath']) ? $v['PicPath'] : ly200::get_size_img($v['PicPath_0'], '240x240');
												?>
													<dl>
														<dt><a href="<?=$url;?>" target="_blank" class="pic_box"><img src="<?=$img;?>" title="<?=$v['Name'];?>" /><span></span></a></dt>
														<dd<?=$_GET['do'] == 'print' ? ' style="padding-right:0;"' : '';?>>
															<h4><a href="<?=$url;?>" title="<?=$v['Name'];?>" class="green" target="_blank"><?=$v['Name'];?></a></h4>
															
														</dd>
													</dl>
												<?php }?>
											</td>
											<td>
												<?php
												echo $v['Number'] != '' ? '<p>{/products.products.number/}: ' . $v['Prefix'] . $v['Number'].'</p>':'';
												echo $SKU != '' ? '<p>{/products.products.sku/}: ' . $SKU . '</p>' : '';
												foreach ((array)$attr as $k3 => $z) {
													if ($k3 != 'Overseas') {
														echo '<p>'.$k3.': '.$z.'</p>';
													}
												}
												echo $v['Remark'] ? '<p>{/orders.remark/}: ' . $v['Remark'] . '</p>' : '';
												if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used']) == 1 && $overseas_ary[$OvId]['Name' . $c['manage']['web_lang']]) {
												   echo '<p>{/shipping.area.ships_from/}: ' . $overseas_ary[$OvId]['Name' . $c['manage']['web_lang']] . '</p>';
												}?>
											</td>
											<td class="last"><?=$qty;?></td>
										</tr>
									<?php
										++$i;
									}?>
								</tbody>
							</table>
						</div>
				<?php
						++$num;
					}
				}?>
			</div>
		<?php }?>
		<div class="box_turn_page clean"><?=html::turn_page($orders_row[1], $orders_row[2], $orders_row[3], '?'.ly200::query_string('page').'&page=');?></div>
	</div>
</div>
<div id="fixed_right">
	<div class="global_container fixed_separate">
		<div class="top_title">{/orders.product.separate/} <a href="javascript:;" class="close"></a></div>
		<div class="box_explain">{/orders.explain.split/}</div>
		<form class="global_form" id="separate_form">
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
				<thead>
					<tr>
						<td width="70%" nowrap="nowrap">{/products.product/}</td>
						<td width="30%" nowrap="nowrap">{/orders.shipping.qty/}</td>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.return/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="" />
			<input type="hidden" name="Number" value="" />
			<input type="hidden" name="OvId" value="" />
			<input type="hidden" name="do_action" value="orders.waybill_separate" />
		</form>
	</div>
	<div class="global_container fixed_shipped">
		<div class="top_title">{/orders.shipping.shipped/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="shipped_form">
			<div class="rows clean">
				<label>{/orders.shipping.track_no/}</label>
				<div class="input">
					<input type="text" class="box_input" name="TrackingNumber" value="" size="46" maxlength="255" notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.info.delivery_time/}</label>
				<div class="input">
					<input type="text" class="box_input input_time shipping_time" name="ShippingTime" value="<?=@date('Y-m-d', $c['time']);?>" default-value="<?=@date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly notnull />
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.payment.contents/}</label>
				<div class="input">
					<textarea name='Remarks' class="box_textarea"></textarea>
				</div>
			</div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/orders.status.7/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.return/}" />
				</div>
			</div>
			<input type="hidden" name="OrderId" value="" />
			<input type="hidden" name="Number" value="" />
			<input type="hidden" name="OvId" value="" />
			<input type="hidden" name="do_action" value="orders.waybill_shipped" />
		</form>
	</div>
</div>