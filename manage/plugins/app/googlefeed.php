<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'googlefeed'));//检查权限
if(!in_array('googlefeed', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

if($c['manage']['do']=='index'){
	//产品列表页面
	$prod_show=array();
	$cfg_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
	foreach($cfg_row as $v){
		$prod_show[$v['Variable']]=$v['Value'];
	}
	$used_row=str::json_data(htmlspecialchars_decode($prod_show['Config']), 'decode');
	
	//获取类别列表
	$cate_ary=str::str_code(db::get_all('products_category', '1', '*'));
	$category_ary=array();
	foreach((array)$cate_ary as $v){
		$category_ary[$v['CateId']]=$v;
	}
	$category_count=count($category_ary);
	unset($cate_ary);
	
	
	//产品列表
	$Keyword=str::str_code($_GET['Keyword']);
	$CateId=(int)$_GET['CateId'];
	$Other=(int)$_GET['Other'];
	$Other==0 && $Other=5;//默认仅显示上架产品
	
	$where='1';//条件
	$page_count=50;//显示数量
	//$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%' or ProId in(select ProId from products_selected_attribute_combination where SKU like '%$Keyword%'))";
	$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%')";
	if($CateId){
		$where.=" and (CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($CateId)."%') or CateId='{$CateId}')";
		$category_one=str::str_code(db::get_one('products_category', "CateId='$CateId'"));
		$UId=$category_one['UId'];
		$UId!='0,' && $TopCateId=category::get_top_CateId_by_UId($UId);
	}
	if($Other){
		switch($Other){
			case 1: $where.=' and IsNew=1'; break;
			case 2: $where.=' and IsHot=1'; break;
			case 3: $where.=' and IsBestDeals=1'; break;
			case 4: $where.=' and IsIndex=1'; break;
			case 5: $where.=$c['manage']['where']['products']; break;
			case 6: $where.=" and (IsSoldOut=1 and SStartTime>{$c['time']})"; break;
			case 7: $where.=" and (Stock<=0 or SoldOut=1 or (IsSoldOut=1 and SEndTime<{$c['time']}))"; break;
			case 8: $where.=" and Stock<=WarnStock"; break;
			case 9: $where.=" and Stock<MOQ && Stock<=0"; break;
		}
	}
	$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
	
	$column_row=db::get_value('config', "GroupId='custom_column' and Variable='Products'", 'Value');
	$custom_ary=str::json_data($column_row, 'decode');
	$column_fixed_ary=array('picture', 'name', 'classify', 'products.price', 'products.other');
	$column_ary=array('picture', 'name', 'products.business', 'classify', 'products.price', 'products.other', 'products.weight', 'products.cubage', 'products.stock', 'products.edit_time');
}
$top_id_name=($c['manage']['do']=='index'?'googlefeed':'googlefeed_inside');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
<div class="center_container_1000">
	<?php if($c['manage']['do']=='index'){?>
		<div class="inside_container">
			<h1>{/module.operation.googlefeed/}</h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<li><a class="edit" href="./?m=operation&a=googlefeed&d=create">{/plugins.googlefeed.update_feed/}</a></li>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
								</div>
							</div>
							<div class="rows item clean">
								<label>{/products.products.other/}</label>
								<div class="input">
									<div class="box_select">
										<select name="Other">
											<option value="0">{/global.select_index/}</option>
											<option value="1">{/products.products.is_new/}</option>
											<option value="2">{/products.products.is_hot/}</option>
											<option value="3">{/products.products.is_best_deals/}</option>
											<option value="4">{/products.products.is_index/}</option>
											<!--<option value="5">{/products.products.sold_in/}</option>-->
											<option value="6">{/products.products.sold_in_time/}</option>
											<!--<option value="7">{/products.products.sold_out/}</option>-->
											<option value="8">{/products.products.warn_stock/}</option>
											<option value="9">{/products.products.not_stock/}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="operation" />
						<input type="hidden" name="a" value="googlefeed" />
						<input type="hidden" name="Other" value="<?=$_GET['Other'];?>" />
					</form>
				</div>
			</div>
			<div class="feed_tips">
				{/plugins.googlefeed.tips/}
				<?=ly200::get_domain().'/googlefeed.xml'; ?>
			</div>
			<?php
			if($products_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="85" nowrap="nowrap">{/products.picture/}</td>
							<td width="60%" nowrap="nowrap">{/products.name/} / {/products.classify/}</td>
							<td width="20%" nowrap="nowrap">{/products.products.number/}</td>
							<td width="60" nowrap="nowrap" class="operation">{/global.operation/}</td>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							$name=htmlspecialchars_decode($v['Name'.$c['manage']['web_lang']]);
							$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
						?>
							<tr>
								<td class="img"><a href="<?=$url;?>" target="_blank" class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
								<td class="info">
									<a class="name" href="<?=$url;?>" target="_blank"><?=$name;?></a>
									<div class="classify color_888<?php /*<?=$permit_ary['edit']?' category_select':'';*/?>" cateid="<?=$v['CateId'];?>">
										<?php
										$UId=$category_ary[$v['CateId']]['UId'];
										if($UId){
											$key_ary=@explode(',',$UId);
											array_shift($key_ary);
											array_pop($key_ary);
											foreach((array)$key_ary as $k2=>$v2){
												echo $category_ary[$v2]['Category'.$c['manage']['web_lang']].' / ';
											}
										}
										echo $category_ary[$v['CateId']]['Category'.$c['manage']['web_lang']];
										?>
									</div>
								</td>
								<td nowrap="nowrap">#<?=$v['PreFix'].$v['Number'];?></td>
								<td nowrap="nowrap" class="operation side_by_side">
									<a href="./?m=operation&a=googlefeed&d=edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a>
								</td>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=operation&a=googlefeed&d=edit');
			}?>
		</div>
		<?php /*<div id="category_select_hide" class="hide"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>*/?>
		<div id="myorder_select_hide" class="hide"><?=ly200::form_select($c['manage']['my_order'], "MyOrder[]", '');?></div>
	<?php
	}else if($c['manage']['do']=='create'){ ?>
		<script type="text/javascript">$(document).ready(function(){plugins_obj.googlefeed_update()});</script>
		<div class="global_container">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.operation.googlefeed/}</span> 
				<span class="s_return">/ {/plugins.googlefeed.update/}</span>
			</a>
			<form class="global_form" id="googlefeed_update_form">
				<div class="rows">
					<label></label>
					<div class="input">
						<input type="submit" class="btn_global btn_submit" value="{/plugins.googlefeed.start/}">
						<a href="./?m=operation&a=googlefeed"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						<input type="hidden" name="do_action" value="plugins.googlefeed_update">
					</div>
				</div>
				<div class="rows">
					<label>{/plugins.googlefeed.progress/}</label>
					<div class="input">
						<ul id="updating">
							
						</ul>
					</div>
				</div>
			</form>
		</div>
	<?php
	}else{
		//产品编辑
		$ProId=(int)$_GET['ProId'];
		$products_row=str::str_code(db::get_one('app_googlefeed', "ProId='$ProId'"));
		?>
		<script type="text/javascript">$(document).ready(function(){plugins_obj.googlefeed_edit()});</script>
		<a href="javascript:history.back(-1);" class="return_title">
			<span class="return">{/module.operation.googlefeed/}</span> 
			<span class="s_return">/ <?=$ProId?'{/global.edit/}':'{/global.add/}';?></span>
		</a>
		<form id="googlefeed_edit_form" class="global_form">

			<?php /***************************** 基础信息 Start *****************************/?>
			<div class="global_container" >
				<div class="big_title">{/plugins.googlefeed.add_info/}</div>
				<div class="rows clean">
					<label></label>
					<div class="input">
						{/plugins.googlefeed.detail_tips/}
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.googlefeed.gtin/}</label>
					<div class="input">
						<input type="text" class="box_input" name="GTIN" autocomplete="off" value="<?=$products_row['GTIN']; ?>" size="70" maxlength="255" />
					</div>
				</div>
				<div class="rows clean">
					<label>MPN</label>
					<div class="input">
						<input type="text" class="box_input" name="MPN" autocomplete="off" value="<?=$products_row['MPN']; ?>" size="70" maxlength="255" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.googlefeed.brand/} <span>( {/plugins.googlefeed.brand_tips/} )</span></label>
					<div class="input">
						<input type="text" class="box_input" name="Brand" autocomplete="off" value="<?=$products_row['Brand']; ?>" size="70" maxlength="255" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.googlefeed.label/} <span>( {/plugins.googlefeed.label_tips/} )</span></label>
					<div class="input">
						<input type="text" class="box_input" name="Label" autocomplete="off" value="<?=$products_row['Label']; ?>" size="70" maxlength="255" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.googlefeed.shipping_price/} <span>({/plugins.googlefeed.sp_coutry/})</span></label>
					<div class="input">
						<input type="text" class="box_input" rel="amount" name="ShippingPrice" value="<?=$products_row['ShippingPrice']; ?>" size="70" maxlength="255" placeholder="0.00" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.googlefeed.tax_rate/} <span>({/plugins.googlefeed.tr_coutry/})</span></label>
					<div class="input">
						<input type="text" class="box_input" rel="amount" name="TaxRate" value="<?=$products_row['TaxRate']; ?>" size="70" maxlength="255" placeholder="0.00" />
					</div>
				</div>
				<div class="rows">
					<label></label>
					<div class="input">
						<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.submit/}" />
						<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=operation&a=googlefeed';?>" class="btn_global btn_cancel">{/global.return/}</a>
					</div>
				</div>
			</div>
			<?php /***************************** 基础信息 End *****************************/?>
			
			<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="do_action" value="plugins.googlefeed_edit" />
		</form>
	<?php }?>
</div>
</div>