<?php !isset($c) && exit();?>
<?php
if(!in_array('swap_chain', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}
$data=array('Action'=>'ueeshop_links_urls_get_owner');
$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
$result['ret'] && $swap_chain=$result['msg'];
?>
<script type="text/javascript">$(function(){plugins_obj.swap_chain_init()});</script>
<div id="swap_chain" class="r_con_wrap">
	<div class="center_container_1000">
        <div class="inside_container">
            <h1>{/module.operation.swap_chain/}</h1>
        </div>
        <div class="inside_table">
            <div class="list_menu">
                <ul class="list_menu_button">
                    <li><a class="add" href="javascript:;">{/global.add/}</a></li>
                </ul>
            </div>
            <?php 
            if($swap_chain){ 
                ?>
                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                        <tr>
                            <td width="24%" nowrap="nowrap">{/plugins.swap_chain.keyword/}</td>
                            <td width="34%" nowrap="nowrap">{/plugins.swap_chain.url/}</td>
                            <td width="34%" nowrap="nowrap">{/plugins.swap_chain.website/}</td>
                            <td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php foreach((array)$swap_chain as $k => $v){?>
                        <tr>
                        	<td nowrap="nowrap"><?=$v['Keyword']?></td>
                            <td nowrap="nowrap"><?=$v['Url']?></td>
                            <td nowrap="nowrap">
                            	<?php
									foreach((array)$v['Urls'] as $k1 => $v1){
										echo ($k1?'<br/>':'').$v1['Domain'];
										if(!$v1['RefreshCount']) echo '<a class="refresh" href="javascript:;" data-urlsid="'.$v['UrlsId'].'" data-allocationid="'.$v1['AllocationId'].'">{/plugins.swap_chain.change/}</a>';
									}
								?>
                            </td>
                            <td nowrap="nowrap" class="operation side_by_side">
                               <a class="del item gray" href="./?do_action=plugins.swap_chain_del&UrlsId=<?=$v['UrlsId'];?>" rel="del">{/global.del/}</a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{
                echo html::no_table_data();
            } ?>
        </div>
	</div>
</div>
<?php /***************************** 添加 Start *****************************/?>
<div id="fixed_right">
    <div class="global_container swap_chain_add">
        <form id="swap_chain_form" class="global_form">
            <div class="top_title">{/module.operation.swap_chain/} <a href="javascript:;" class="close"></a></div>
            <div class="rows">
                <label>{/plugins.swap_chain.keyword/}</label>
                <span class="input"><input type="text" class='box_input' value="" name="Keyword" maxlength="50" size="35" notnull /></span>
                <div class="clear"></div>
            </div>
            <div class="rows">
                <label>{/plugins.swap_chain.url/}</label>
                <div class="tips color_aaa"><?=str_replace('%domain%',ly200::get_domain(),$c['manage']['lang_pack']['plugins']['swap_chain']['add_tips'])?></div>
                <span class="input"><input type="text" class='box_input' value="" name="Url" maxlength="50" size="50" notnull /></span>
                <div class="clear"></div>
            </div>
            <div class="rows">
                <label></label>
                <div class="input input_button">
                    <input type="button" class="btn_global btn_submit" value="{/global.save/}">
                    <input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
                </div>
            </div>
            <input type="hidden" name="UrlsId" value="" />
            <input type="hidden" name="do_action" value="plugins.swap_chain_add" />
        </form>
    </div>
</div>
<?php /***************************** 添加 End *****************************/?>