<?php
!isset($c) && exit();
$web_lang=$c['manage']['web_lang'];
$AppId=db::get_value('config', 'GroupId="facebook_store" and Variable="AppId"', 'Value');
$page_url=($_SERVER['HTTP_PORT']=='443'?'https://':'http://').$_SERVER['HTTP_HOST'].'/';
$page_url='https://www.tf-direct.com/';

$where='1';
$page_count=30;//显示数量
$Name=str::str_code($_GET['Keyword']);
$CateId=(int)$_GET['CateId'];
$Name && $where.=" and (Name{$web_lang} like '%$Name%' or concat(Prefix, Number) like '%$Name%')";
$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";
if($CateId){
	$UId=category::get_UId_by_CateId($CateId);
	$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
}
// $where.=" and ((SoldOut=0 and IsSoldOut=0) or (IsSoldOut=1 and SStartTime<{$c['time']} and {$c['time']}<SEndTime))";
$where.=$c['manage']['where']['products'];
$row_count=db::get_row_count('products', $where);
$total_pages=ceil($row_count/$page_count);
$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));

echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
?>
<script type="text/javascript">$(function(){plugins_obj.facebook_store_init()});</script>
<div id="facebook_store" class="r_con_wrap">
	<?php
	$WId=(int)$_GET['WId'];
	$web_setting_row=db::get_one('web_settings', "WId='{$WId}'");
	
	$all_ad_row=db::get_all('web_settings', 'Themes="store"', '*', 'WId asc');
	$ad_themes_type=themes_set::themes_banner_init();
	$ad_themes_type=(int)$ad_themes_type['Type'];
	$index_img=$c['cdn'].'static/v0/themes/mobile/12.jpg';
	?>
	<div id="index_set">
		<div class="themes_box">
			<img src="<?=$index_img;?>" alt="" class="themes_pic" />
			<?php foreach((array)$all_ad_row as $v){?>
				<div class="abs_item<?=$WId==$v['WId']?' cur':'';?>" data-type="<?=$v['Type'];?>" data-wid="<?=$v['WId'];?>" data-relaid="<?=$v['Position'];?>"<?=$v['PositionStyle']?' style="'.$v['PositionStyle'].'"':'';?>></div>
			<?php }?>
			<input type="hidden" name="return_url" value="./?m=operation&a=facebook_store&d=index_set" />
		</div>
		<div class="global_container index_set_exit">
			<form id="index_set_edit_form" class="global_form">
				<?php 
				if($web_setting_row['Type']=='Products'){
					$pro_data_ary=array();
					$pro_ary=str::str_code(str::json_data($web_setting_row['Data'], 'decode'));
				?>
					<div class="set_products_btn"><a href="javascript:;" class="set_add">{/global.add/}</a></div>
					<div class="set_products_list" id="set_products_list">
						<?php
						if($pro_ary['Products']){
							$pro_row=str::str_code(db::get_all('products', 'ProId in('.implode(',', $pro_ary['Products']).')', '*', 'ProId desc'));
							foreach($pro_row as $v){
								$pro_data_ary[$v['ProId']]=$v;
							}
							foreach((array)$pro_ary['Products'] as $v){
								$proid=$v;
								$row=$pro_data_ary[$proid];
								$url=ly200::get_url($row, 'products');
								$img=ly200::get_size_img($row['PicPath_0'], '240x240');
								$name=$row['Name'.$web_lang];
							?>
									<div class="set_products_item" data-proid="<?=$proid;?>">
										<div class="edit_box"><a href="javascript:;" class="del"></a></div>
										<div class="pic"><img src="<?=$img;?>" alt=""><span></span></div>
										<div class="content"><a href="<?=$url;?>" target="_blank" class="name"><?=$name;?></a></div>
										<input type="hidden" name="ProId[]" value="<?=$proid;?>" />
										<div class="clear"></div>
									</div>
						<?php
							}
						}?>
					</div>
				<?php
				}else{ 
					$type_ary = @explode('_', $web_setting_row['Type']);
					//Name_Brief_PicPath_Url
					$web_date=str::str_code(str::json_data($web_setting_row['Data'], 'decode'));
					$web_config=str::json_data($web_setting_row['Config'], 'decode');
					$PicCount=(int)$web_config['PicCount'];
					$PicCount || $PicCount=1;
				?>
						<div class="rows">
							<label><div class="tab_box"><?=manage::html_tab_button();?></div></label>
							<div class="input">
								<?php foreach($c['manage']['config']['Language'] as $k=>$v){?>
									<div class="tab_txt tab_txt_<?=$v;?>" <?=$c['manage']['config']['LanguageDefault']==$v ? 'style="display:block;"' : ''; ?>>
										<?php for($i=0;$i<$PicCount;$i++){ ?>
											<?php if(@in_array('Name',$type_ary)){ ?>
												<div class="rows clean">
													<label>{/global.title/}</label>
													<div class="input"><input type="text" class="box_input" name="Name_<?=$v;?>[]" value="<?=$web_date['Name'][$i][$v]; ?>" size="50"></div>
												</div>
											<?php } ?>
											<?php if(@in_array('Brief',$type_ary)){ ?>
												<div class="rows clean">
													<label>{/global.brief/}</label>
													<div class="input">
														<textarea name="Brief_<?=$v;?>[]" class="box_textarea"><?=$web_date['Brief'][$i][$v]; ?></textarea>
													</div>
												</div>
											<?php } ?>
											<?php if(@in_array('PicPath',$type_ary)){ ?>
												<div class="rows clean">
													<label>{/global.picture/}</label>
													<div class="input">
														<?=manage::multi_img('PicDetail_'.$i.'_'.$v, 'PicPath_'.$v.'[]', $web_date['PicPath'][$i][$v], 0, $v); ?>
													</div>
												</div>
											<?php } ?>
											<?php if(@in_array('Url',$type_ary)){ ?>
												<div class="rows clean">
													<label>{/themes.url/}</label>
													<div class="input"><input type="text" class="box_input" name="Url_<?=$v;?>[]" value="<?=$web_date['Url'][$i][$v]; ?>" size="50"></div>
												</div>
											<?php } ?>
											<div style="border-bottom:1px solid #ccc; padding-top:20px;"></div>
										<?php } ?>
									</div>
								<?php }?>
							</div>
						</div>
				<?php }?>
				<div class="rows clean">
					<label></label>
					<div class="input">
						<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
					</div>
				</div>
				<input type="hidden" name="WId" value="<?=$WId;?>" />
				<input type="hidden" name="Type" value="<?=$web_setting_row['Type'];?>" />
				<input type="hidden" name="do_action" value="plugins.facebook_store" />
			</form>
		</div>
	</div>
</div>
<div id="fixed_right">
	<div class="global_container fixed_add_products">
		<div class="top_title">{/global.add/} <a href="javascript:;" class="close"></a></div>
		<div class="list_menu">
			<?=manage::sales_right_search_form('operation', 'facebook_store', 'edit');?>
		</div>
		<div class="add_products_list" data-page="1" data-total-pages="<?=$total_pages;?>">
			<?php
			foreach($products_row[0] as $k=>$v){
				$proid=$v['ProId'];
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '240x240');
				$name=$v['Name'.$web_lang];
			?>
				<div class="add_products_item" data-proid="<?=$proid;?>">
					<div class="img">
						<img src="<?=$img;?>" /><span></span>
						<div class="img_mask"></div>
					</div>
					<div class="name"><?=$name;?></div>
					<div class="set_products_item" data-proid="<?=$proid;?>">
						<div class="edit_box"><a href="javascript:;" class="del"></a></div>
						<div class="pic"><a href="<?=$url;?>" target="_blank" title="<?=$name;?>"><img src="<?=$img;?>" alt=""></a><span></span></div>
						<div class="content"><a href="<?=$url;?>" target="_blank" class="name"><?=$name;?></a></div>
						<input type="hidden" name="ProId[]" value="<?=$proid;?>" />
						<div class="clear"></div>
					</div>
				</div>
			<?php }?>
		</div>
	</div>
</div>