<?php !isset($c) && exit();?>
<?php
if(!in_array('google_verification', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}
$google_verification=db::get_value('config','GroupId="plugins" and Variable="google_verification"','Value');
$google_verification=str::json_data(htmlspecialchars_decode($google_verification),'decode');
echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
?>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<script type="text/javascript">$(function(){plugins_obj.google_verification_init()});</script>
<div id="google_verification" class="r_con_wrap">
	<div class="center_container_1000">
        <div class="inside_container">
            <h1>{/module.operation.google_verification/}</h1>
        </div>
        <div class="google_verification_box">
        	<?php foreach((array)$google_verification as $k => $v){?>
            <div class="item">
            	<a href="./?do_action=plugins.google_verification_del&Id=<?=$k?>" class="del icon_delete_1"></a>
                <div class="name">{/plugins.google_verification.filename/}</div>
                <div class="url"><?=$v?></div>
            </div>
            <?php }?>
        	<div class="item add"><div class="add_bg"><i></i><em></em></div></div>
            <div class="clear"></div>
        </div>
        
        
        
        <?php /*?><div class="inside_table">
            <div class="list_menu">
                <ul class="list_menu_button">
                    <li><a class="add" href="javascript:;">{/global.add/}</a></li>
                </ul>
            </div>
            <?php 
            if($google_verification){ 
                ?>
                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                        <tr>
                            <td width="92%" nowrap="nowrap">{/plugins.google_verification.url/}</td>
                            <td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
                        	foreach((array)$google_verification as $k => $v){
								$exp=explode(': ',$v);
								$url=ly200::get_domain().'/'.$exp[1];
						?>
                        <tr>
                        	<td nowrap="nowrap">
                            	<?php if($exp[1]){?>
                                <a href="<?=$url?>" target="_blank"><?=$url?></a>
                                <?php }else{?>
                                {/plugins.google_verification.error/}
                                <?php }?>
                            </td>
                            <td nowrap="nowrap" class="operation side_by_side">
                               <a class="add" href="javascript:;" data-id="<?=$k?>" data-url="<?=$v?>">{/global.edit/}</a>
                                <dl>
                                    <dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
                                    <dd class="drop_down">
                                       <a class="del item" href="./?do_action=plugins.google_verification_del&Id=<?=$k;?>" rel="del">{/global.del/}</a>
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{
                echo html::no_table_data();
            } ?>
        </div><?php */?>
        
        
	</div>
</div>
<?php /***************************** 添加 Start *****************************/?>
<div id="fixed_right">
    <div class="global_container google_verification_edit">
        <form id="google_verification_form" class="global_form">
            <div class="top_title">{/module.operation.google_verification/} <a href="javascript:;" class="close"></a></div>
            <div class="rows">
                <label></label>
                <span class="input">
                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                    <div class="row fileupload-buttonbar">
                        <span class="btn_file btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>{/global.upload_file/}</span>
                            <input type="file" name="Filedata" multiple>
                        </span>
                        <div class="fileupload-progress fade"><div class="progress-extended">&nbsp;</div></div>
                        <div class="clear"></div>
                        <div class="photo_multi_img template-box files"></div>
                        <div class="photo_multi_img" id="PicDetail"></div>
                    </div>
                    <script id="template-upload" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <div class="template-upload fade">
                            <div class="clear"></div>
                            <div class="items">
                                <p class="name">{%=file.name%}</p>
                                <strong class="error text-danger"></strong>
                            </div>
                            <div class="items">
                                <p class="size">Processing...</p>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                            </div>
                            <div class="items">
                                {% if (!i) { %}
                                    <button class="btn_file btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>{/global.cancel/}</span>
                                    </button>
                                {% } %}
                            </div>
                            <div class="clear"></div>
                        </div>
                    {% } %}
                    </script>
                    <script id="template-download" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                        {% if (file.thumbnailUrl) { %}
                            <div class="pic template-download fade hide">
                                <div>
                                    <a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
                                    <a href="{%=file.url%}" class="zoom" target="_blank"></a>
                                    {% if (file.deleteUrl) { %}
                                        <button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
                                        <input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
                                    {% } %}
                                    <input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
                                </div>
                                <input type="text" maxlength="30" class="box_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
                            </div>
                        {% } else { %}
                            <div class="template-download fade hide">
                                <div class="clear"></div>
                                <div class="items">
                                    <p class="name">
                                        {% if (file.url) { %}
                                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                                        {% } else { %}
                                            <span>{%=file.name%}</span>
                                        {% } %}
                                    </p>
                                    {% if (file.error) { %}
                                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                                    {% } %}
                                </div>
                                <div class="items">
                                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                                </div>
                                <div class="items">
                                    {% if (file.deleteUrl) { %}
                                        <button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>{/global.del/}</span>
                                        </button>
                                        <input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
                                    {% } else { %}
                                        <button class="btn_file btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>{/global.cancel/}</span>
                                        </button>
                                    {% } %}
                                </div>
                                <div class="clear"></div>
                            </div>
                        {% } %}
                    {% } %}
                    </script>
                    <div class="tips"></div>
                    <input id="FileName" type="hidden" name="FileName" value="">
                    <input id="FilePath" type="hidden" name="FilePath" value="">
                </span>
                <div class="clear"></div>
            </div>
            <div class="rows">
                <label></label>
                <div class="input input_button">
                    <input type="button" class="btn_global btn_submit" value="{/global.save/}">
                    <input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
                </div>
            </div>
            <input type="hidden" name="do_action" value="plugins.google_verification_edit" />
        </form>
    </div>
</div>
<?php /***************************** 添加 End *****************************/?>