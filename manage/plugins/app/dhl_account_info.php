<?php !isset($c) && exit();?>
<?php
if(!in_array('dhl_account_info', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

$module = $c['manage']['do'];
$p_ary = array('step_1', 'step_2', 'step_3', 'step_4', 'step_5', 'step_6', 'step_7', 'step_8', 'step_9', 'step_10');
if (!$module || !in_array($module,$p_ary)) {
	$module = 'step_1';
}

echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
?>
<script src="/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/static/js/plugin/file_upload/js/external/tmpl.js"></script>
<script src="/static/js/plugin/file_upload/js/external/load-image.js"></script>
<script src="/static/js/plugin/file_upload/js/external/canvas-to-blob.js"></script>
<script src="/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.iframe-transport.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-process.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-image.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-audio.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-video.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-validate.js"></script>
<script src="/static/js/plugin/file_upload/js/jquery.fileupload-ui.js"></script>
<script type="text/javascript">$(function(){plugins_obj.dhl_account_open_init()});</script>
<div id="dhl_account_open" class="r_con_wrap">
	<div class="inside_container">
		<h1><?=$module == 'step_1' ? '{/plugins.dhl_account_open.title/}' : '{/plugins.dhl_account_open.end_title/}';?></h1>
	</div>
	<?php if ($module == 'step_1') {
		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_2">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_1/}</span> 
						</a>
					</div>
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.account_address/}</label>
						<div class="input">
							<?php for ($i = 0; $i < 3; $i++) { ?>
								<span class="input_radio_box <?=$i == $DHLAccountOpenStep1['AccountAddress'] ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="AccountAddress" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep1['AccountAddress'] ? 'checked="checked"' : ''; ?>>
									</span>{/plugins.dhl_account_open.address.<?=$i; ?>/}
								</span>&nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>			
					</div>
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.account_type/}</label>
						<div class="input">
							<?php for ($i = 0; $i < 2; $i++) { ?>
								<span class="input_radio_box <?=$i == $DHLAccountOpenStep1['AccountType'] ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="AccountType" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep1['AccountType'] ? 'checked="checked"' : ''; ?>>
									</span>{/plugins.dhl_account_open.type.<?=$i; ?>/}
								</span>&nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>				
					</div>
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.referrer/}</label>
						<div class="input">
							<input name="Referrer" value="<?=str::str_code($DHLAccountOpenStep1['Referrer']);?>" type="text" class="box_input"  size="53" />
						</div>				
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/plugins.dhl_account_open.start_register/}">
							<a href="./?m=plugins&a=my_app"><input type="button" class="btn_global btn_cancel" value="{/plugins.dhl_account_open.cancel_register/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_1">
				</form>
			</div>
		</div>
    <?php } else if ($module == 'step_2') {
    	// 深圳和上海需要填写增值税发票信息
    	$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_3">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_2/}</span> 
						</a>
					</div>
					<div class="blank30"></div>
					<!-- 公司信息 start -->
					<div class="rows rows_title">
						<label>1. {/plugins.dhl_account_open.company_info/}</label>
						<div class="input"></div>
					</div>
					<!-- 公司名称 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.company_name/}</label>
						<div class="input"><input type="text" class="box_input" name="CompanyName" value="<?=str::str_code($DHLAccountOpenStep2['CompanyName']);?>" size="39" notnull /></div>
					</div>
					<!-- 营业执照 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.business_number/}</label>
						<div class="input"><input type="text" class="box_input" name="BusinessNumber" value="<?=str::str_code($DHLAccountOpenStep2['BusinessNumber']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 注册日期 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.register_date/}</label>
						<div class="input"><input type="text" class="box_input" name="RegisterDate" value="<?=str::str_code($DHLAccountOpenStep2['RegisterDate']);?>" size="39" notnull /></div>
					</div>
					<!-- 注册地 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.register_address/}</label>
						<div class="input"><input type="text" class="box_input" name="RefisterAddress" value="<?=str::str_code($DHLAccountOpenStep2['RefisterAddress']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 公司地址 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.company_address/}</label>
						<div class="input"><input type="text" class="box_input" name="CompanyAddress" value="<?=str::str_code($DHLAccountOpenStep2['CompanyAddress']);?>" size="39" notnull /></div>
					</div>
					<!-- 街道 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.street/}</label>
						<div class="input"><input type="text" class="box_input" name="Street" value="<?=str::str_code($DHLAccountOpenStep2['Street']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 门牌号 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.house_no/}</label>
						<div class="input"><input type="text" class="box_input" name="HouseNo" value="<?=str::str_code($DHLAccountOpenStep2['HouseNo']);?>" size="39" notnull /></div>
					</div>
					<!-- 邮编 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.zip_code/}</label>
						<div class="input"><input type="text" class="box_input" name="ZipCode" value="<?=str::str_code($DHLAccountOpenStep2['ZipCode']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 城市 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.city/}</label>
						<div class="input"><input type="text" class="box_input" name="City" value="<?=str::str_code($DHLAccountOpenStep2['City']);?>" size="39" notnull /></div>
					</div>
					<!-- 省份/地区 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.state/}</label>
						<div class="input"><input type="text" class="box_input" name="State" value="<?=str::str_code($DHLAccountOpenStep2['State']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 国家 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.country/}</label>
						<div class="input"><input type="text" class="box_input" name="Country" value="<?=str::str_code($DHLAccountOpenStep2['Country']);?>" size="39" notnull /></div>
					</div>
					<!-- 电话号码 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.telephone_no/}</label>
						<div class="input"><input type="text" class="box_input" name="Telephone" value="<?=str::str_code($DHLAccountOpenStep2['Telephone']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 网址 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.website/}</label>
						<div class="input"><input type="text" class="box_input" name="Website" value="<?=str::str_code($DHLAccountOpenStep2['Website']);?>" size="39" /></div>
					</div>
					<!-- 传真号码 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.fax_no/}</label>
						<div class="input"><input type="text" class="box_input" name="FaxNo" value="<?=str::str_code($DHLAccountOpenStep2['FaxNo']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 联络人姓名 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.contact_name/}</label>
						<div class="input"><input type="text" class="box_input" name="ContactName" value="<?=str::str_code($DHLAccountOpenStep2['ContactName']);?>" size="39" notnull /></div>
					</div>
					<!-- 联络人邮箱 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.contact_email/}</label>
						<div class="input"><input type="text" class="box_input" name="ContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['ContactEmail']);?>" size="39" notnull format="Email" /></div>
					</div>
					<div class="clear"></div>
					<!-- 联络人手机号码 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.contact_mobile/}</label>
						<div class="input"><input type="text" class="box_input" name="ContactMobile" value="<?=str::str_code($DHLAccountOpenStep2['ContactMobile']);?>" size="39" notnull /></div>
					</div>
					<!-- 系统对接联系人名字 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.system_contact_name/}</label>
						<div class="input"><input type="text" class="box_input" name="SystemContactName" value="<?=str::str_code($DHLAccountOpenStep2['SystemContactName']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 系统对接联系人邮箱 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.system_contact_email/}</label>
						<div class="input"><input type="text" class="box_input" name="SystemContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['SystemContactEmail']);?>" size="39" notnull format="Email" /></div>
					</div>
					<!-- 系统对接联系人手机号码 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.system_contact_mobile/}</label>
						<div class="input"><input type="text" class="box_input" name="SystemContactMobile" value="<?=str::str_code($DHLAccountOpenStep2['SystemContactMobile']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 公司信息 end -->

					<div class="blank30"></div>

					<!-- 账单/发票地址 start -->
					<div class="rows rows_title">
						<label>1.1 {/plugins.dhl_account_open.billing_address/}</label>
						<div class="input">
							<span class="input_checkbox_box <?=($DHLAccountOpenStep2['BillSameRegisterAddress'] || empty($DHLAccountOpenStep2)) ? 'checked' : '';?>">
								<span class="input_checkbox">
									<input type="checkbox" name="BillSameRegisterAddress" value="1" <?=($DHLAccountOpenStep2['BillSameRegisterAddress'] || empty($DHLAccountOpenStep2)) ? 'checked="checked"' : ''; ?> />
								</span>{/plugins.dhl_account_open.same_register_address/}
							</span>
						</div>
					</div>
					<div class="address_row">
						<!-- 账单 - 注册公司名称 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.bill_company_name/}</label>
							<div class="input"><input type="text" class="box_input" name="BillCompanyName" value="<?=str::str_code($DHLAccountOpenStep2['BillCompanyName']);?>" size="39" notnull /></div>
						</div>
						<!-- 账单 - 发票/账单地址 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.billing_address/}</label>
							<div class="input"><input type="text" class="box_input" name="BillCompanyAddress" value="<?=str::str_code($DHLAccountOpenStep2['BillCompanyAddress']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 账单 - 邮编/城市 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.bill_zipcode/}</label>
							<div class="input"><input type="text" class="box_input" name="BillZipCode" value="<?=str::str_code($DHLAccountOpenStep2['BillZipCode']);?>" size="39" notnull /></div>
						</div>
						<!-- 账单 - 电话号码 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.telephone_no/}</label>
							<div class="input"><input type="text" class="box_input" name="BillTelephone" value="<?=str::str_code($DHLAccountOpenStep2['BillTelephone']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 账单 - 传真号码 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.fax_no/}</label>
							<div class="input"><input type="text" class="box_input" name="BillFaxNo" value="<?=str::str_code($DHLAccountOpenStep2['BillFaxNo']);?>" size="39" notnull /></div>
						</div>
						<!-- 账单 - 授权签字人姓名 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.bill_authorized_name/}</label>
							<div class="input"><input type="text" class="box_input" name="BillAuthorizedName" value="<?=str::str_code($DHLAccountOpenStep2['BillAuthorizedName']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 账单 - 联络人姓名 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.contact_name/}</label>
							<div class="input"><input type="text" class="box_input" name="BillContactName" value="<?=str::str_code($DHLAccountOpenStep2['BillContactName']);?>" size="39" notnull /></div>
						</div>
						<!-- 账单 - 联络人邮箱 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.contact_email/}</label>
							<div class="input"><input type="text" class="box_input" name="BillContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['BillContactEmail']);?>" size="39" notnull format="Email" /></div>
						</div>
						<div class="clear"></div>
						<!-- 账单 - 联络人手机号码 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.contact_mobile/}</label>
							<div class="input"><input type="text" class="box_input" name="BillContactMobile" value="<?=str::str_code($DHLAccountOpenStep2['BillContactMobile']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- 账单/发票地址 end -->

					<div class="blank30"></div>
					
					<!-- 取件地址 start -->
					<div class="rows rows_title">
						<label>1.2 {/plugins.dhl_account_open.pickup_address/}</label>
						<div class="input">
							<span class="input_checkbox_box <?=($DHLAccountOpenStep2['PickupSameRegisterAddress'] || empty($DHLAccountOpenStep2)) ? 'checked' : '';?>">
								<span class="input_checkbox">
									<input type="checkbox" name="PickupSameRegisterAddress" value="1" <?=($DHLAccountOpenStep2['PickupSameRegisterAddress'] || empty($DHLAccountOpenStep2)) ? 'checked="checked"' : ''; ?> />
								</span>{/plugins.dhl_account_open.same_register_address/}
							</span>
						</div>
					</div>
					<div class="address_row">
						<!-- 取件 - 取件地址 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.pickup_address/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupAddress" value="<?=str::str_code($DHLAccountOpenStep2['PickupAddress']);?>" size="39" notnull /></div>
						</div>
						<!-- 取件 - 邮编/城市 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.bill_zipcode/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupZipCode" value="<?=str::str_code($DHLAccountOpenStep2['PickupZipCode']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 取件 - 电话号码 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.telephone_no/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupTelephone" value="<?=str::str_code($DHLAccountOpenStep2['PickupTelephone']);?>" size="39" notnull /></div>
						</div>
						<!-- 取件 - 传真号码 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.fax_no/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupFaxNo" value="<?=str::str_code($DHLAccountOpenStep2['PickupFaxNo']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 取件 - 授权签字人姓名 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.bill_authorized_name/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupAuthorizedName" value="<?=str::str_code($DHLAccountOpenStep2['PickupAuthorizedName']);?>" size="39" notnull /></div>
						</div>
						<!-- 取件 - 联络人姓名 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.contact_name/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupContactName" value="<?=str::str_code($DHLAccountOpenStep2['PickupContactName']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 取件 - 电子邮件地址 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.contact_email/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['PickupContactEmail']);?>" size="39" notnull format="Email" /></div>
						</div>
						<!-- 取件 - 联络人手机号码 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.contact_mobile/}</label>
							<div class="input"><input type="text" class="box_input" name="PickupContactMobile" value="<?=str::str_code($DHLAccountOpenStep2['PickupContactMobile']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- 取件地址 end -->

					<div class="blank30"></div>
					
					<!-- 信用验证资讯 start -->
					<div class="rows rows_title">
						<label>1.3 {/plugins.dhl_account_open.credit_veri_details/}</label>
						<div class="input"></div>
					</div>
					<!-- 信用验证 - 预计年付运费 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.annual_est_revenue/}</label>
						<div class="input"><input type="text" class="box_input" name="AnnualEstRevenue" value="<?=str::str_code($DHLAccountOpenStep2['AnnualEstRevenue']);?>" size="39" notnull /></div>
					</div>
					<!-- 信用验证 - 经营者/董事姓名 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.owner_name/}</label>
						<div class="input"><input type="text" class="box_input" name="OwnerName" value="<?=str::str_code($DHLAccountOpenStep2['OwnerName']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 信用验证 - 地址 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.owner_address/}</label>
						<div class="input"><input type="text" class="box_input" name="OwnerAddress" value="<?=str::str_code($DHLAccountOpenStep2['OwnerAddress']);?>" size="39" notnull /></div>
					</div>
					<!-- 信用验证 - 联络人电话号码 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.contact_number/}</label>
						<div class="input"><input type="text" class="box_input" name="OwnerTelephone" value="<?=str::str_code($DHLAccountOpenStep2['OwnerTelephone']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 信用验证 - 联络人邮箱 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.contact_email/}</label>
						<div class="input"><input type="text" class="box_input" name="OwnerEmail" value="<?=str::str_code($DHLAccountOpenStep2['OwnerEmail']);?>" size="39" notnull format="Email" /></div>
					</div>
					<!-- 信用验证 - 财务联络人姓名 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.finance_contact_name/}</label>
						<div class="input"><input type="text" class="box_input" name="FinanceContactName" value="<?=str::str_code($DHLAccountOpenStep2['FinanceContactName']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 信用验证 - 财务联络人地址 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.finance_address/}</label>
						<div class="input"><input type="text" class="box_input" name="FinanceContactAddress" value="<?=str::str_code($DHLAccountOpenStep2['FinanceContactAddress']);?>" size="39" notnull /></div>
					</div>
					<!-- 信用验证 - 财务联络人电话号码 -->
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.finance_contact_number/}</label>
						<div class="input"><input type="text" class="box_input" name="FinanceContactTelephone" value="<?=str::str_code($DHLAccountOpenStep2['FinanceContactTelephone']);?>" size="39" notnull /></div>
					</div>
					<div class="clear"></div>
					<!-- 信用验证 - 财务联络人邮箱 -->
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.finance_contact_email/}</label>
						<div class="input"><input type="text" class="box_input" name="FinanceContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['FinanceContactEmail']);?>" size="39" notnull format="Email" /></div>
					</div>
					<div class="clear"></div>
					<!-- 信用验证 - 办公场所 -->
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.office_premise/}</label>
						<div class="input">
							<?php for ($i = 0; $i < 2; $i++) { ?>
								<span class="input_radio_box <?=$i == $DHLAccountOpenStep2['OfficePremise'] ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="OfficePremise" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep2['OfficePremise'] ? 'checked="checked"' : ''; ?>>
									</span>{/plugins.dhl_account_open.office_premise_type.<?=$i; ?>/}
								</span>&nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>				
					</div>
					<!-- 信用验证资讯 end -->
					<?php if ($DHLAccountOpenStep1['AccountAddress'] == 1 || $DHLAccountOpenStep1['AccountAddress'] == 2) {?>
						<div class="blank30"></div>
						<!-- 增值税发票信息征询表 start -->
						<div class="rows rows_title">
							<label>1.4 {/plugins.dhl_account_open.VAT_invoice/}</label>
							<div class="input"></div>
						</div>
						<!-- 增值税 - 公司名称（税务登记证或者营业执照的公司名称 -->
						<div class="rows clean">
							<label>{/plugins.dhl_account_open.vat_company/}</label>
							<div class="input"><input type="text" class="box_input" name="VATCompany" value="<?=str::str_code($DHLAccountOpenStep2['VATCompany']);?>" size="117" notnull /></div>
						</div>
						<!-- 增值税 - 请选择所属纳税人类型 -->
						<div class="rows clean">
							<label>{/plugins.dhl_account_open.taxpayer_type/}</label>
							<div class="input taxpayer_type">
								<?php for ($i = 0; $i < 2; $i++) { ?>
									<span class="input_radio_box <?=$i == $DHLAccountOpenStep2['TaxpayerType'] ? 'checked' : '';?>">
										<span class="input_radio">
											<input type="radio" name="TaxpayerType" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep2['TaxpayerType'] ? 'checked="checked"' : ''; ?>>
										</span>{/plugins.dhl_account_open.taxpayer_type_ary.<?=$i; ?>/}
									</span>&nbsp;&nbsp;&nbsp;
								<?php }?>
							</div>				
						</div>
						<!-- 如果您属于"1.增值税一般纳税人"，请务必完整填列下表全部信息，并提供贵公司一般纳税人认定通知书复印件。
							如果您属于"2.非增值税一般纳税人"，请填列下表带*项信息。 -->
						<div class="vat_company_row">
							<!-- 增值税 - 统一社会信用代码 -->
							<div class="rows clean twice_box twice_first">
								<label>{/plugins.dhl_account_open.social_credit_code/}</label>
								<div class="input"><input type="text" class="box_input" name="SocialCreditCode" value="<?=str::str_code($DHLAccountOpenStep2['SocialCreditCode']);?>" size="39" notnull /></div>
							</div>
							<!-- 增值税 - 公司地址（营业执照注册） -->
							<div class="rows clean twice_box">
								<label>{/plugins.dhl_account_open.vat_company_address/}</label>
								<div class="input"><input type="text" class="box_input" name="VATCompanyAddress" value="<?=str::str_code($DHLAccountOpenStep2['VATCompanyAddress']);?>" size="39" notnull /></div>
							</div>
							<div class="clear"></div>
							<!-- 增值税 - 公司电话（营业执照注册） -->
							<div class="rows clean twice_box twice_first">
								<label>{/plugins.dhl_account_open.vat_company_telephone/}</label>
								<div class="input"><input type="text" class="box_input" name="VATCompanyTelephone" value="<?=str::str_code($DHLAccountOpenStep2['VATCompanyTelephone']);?>" size="39" notnull /></div>
							</div>
							<!-- 增值税 - 开户银行名称（税局备案信息） -->
							<div class="rows clean twice_box">
								<label>{/plugins.dhl_account_open.vat_bank_name/}</label>
								<div class="input"><input type="text" class="box_input" name="VATBankName" value="<?=str::str_code($DHLAccountOpenStep2['VATBankName']);?>" size="39" notnull /></div>
							</div>
							<div class="clear"></div>
							<!-- 增值税 - 开户银行账号（税局备案信息） -->
							<div class="rows clean twice_box twice_first">
								<label>{/plugins.dhl_account_open.vat_bank_account/}</label>
								<div class="input"><input type="text" class="box_input" name="VATBankAccount" value="<?=str::str_code($DHLAccountOpenStep2['VATBankAccount']);?>" size="39" notnull /></div>
							</div>
							<div class="clear"></div>
						</div>
						<!-- 增值税 - 请选择上线后所需我司开具的发票类型 -->
						<div class="rows clean">
							<label>{/plugins.dhl_account_open.vat_invoice_type/}</label>
							<div class="input vat_invoice_type">
								<?php for ($i = 0; $i < 3; $i++) { ?>
									<span class="input_radio_box <?=$i == $DHLAccountOpenStep2['VATInvoiceType'] ? 'checked' : '';?>">
										<span class="input_radio">
											<input type="radio" name="VATInvoiceType" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep2['VATInvoiceType'] ? 'checked="checked"' : ''; ?> class="<?=$i == 1 ? 'only_taxpayer_type' : '';?>">
										</span>{/plugins.dhl_account_open.vat_invoice_code_ary.<?=$i; ?>/}
									</span>&nbsp;&nbsp;&nbsp;
								<?php }?>
							</div>
						</div>
						<!-- 增值税 - 税务发票事宜联系人 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.vat_invoice_con_name/}</label>
							<div class="input"><input type="text" class="box_input" name="VATInvoiceContactName" value="<?=str::str_code($DHLAccountOpenStep2['VATInvoiceContactName']);?>" size="39" notnull /></div>
						</div>
						<!-- 增值税 - 税务发票事宜联系人电话 -->
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.vat_invoice_con_tele/}</label>
							<div class="input"><input type="text" class="box_input" name="VATInvoiceContactTelephone" value="<?=str::str_code($DHLAccountOpenStep2['VATInvoiceContactTelephone']);?>" size="39" notnull /></div>
						</div>
						<div class="clear"></div>
						<!-- 增值税 - 税务发票事宜联系人邮箱 -->
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.vat_invoice_con_email/}</label>
							<div class="input"><input type="text" class="box_input" name="VATInvoiceContactEmail" value="<?=str::str_code($DHLAccountOpenStep2['VATInvoiceContactEmail']);?>" size="39" notnull format="Email" /></div>
						</div>
						<!-- 增值税发票信息征询表 end -->
						<div class="clear"></div>

						<div class="blank30"></div>

						<!-- 委托付款 start -->
						<div class="rows rows_title">
							<label>1.5 {/plugins.dhl_account_open.payment_auth/}</label>
							<div class="input"></div>
						</div>
						<!-- 委托付款 - 是否由第三方付款 -->
						<div class="rows clean">
							<label>{/plugins.dhl_account_open.is_payment_auth/}</label>
							<div class="input is_payment_auth">
								<?php for ($i = 0; $i < 2; $i++) { ?>
									<span class="input_radio_box <?=$i == $DHLAccountOpenStep2['IsPaymentAuth'] ? 'checked' : '';?>">
										<span class="input_radio">
											<input type="radio" name="IsPaymentAuth" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep2['IsPaymentAuth'] ? 'checked="checked"' : ''; ?>>
										</span>{/plugins.dhl_account_open.is_payment_auth_ary.<?=$i;?>/}
									</span>&nbsp;&nbsp;&nbsp;
								<?php }?>
							</div>
						</div>
						<div class="payment_row">
							<!-- 委托付款 - 第三方名称 -->
							<div class="rows clean twice_box twice_first">
								<label>{/plugins.dhl_account_open.3rd_company_name/}</label>
								<div class="input"><input type="text" class="box_input" name="ThirdCompanyName" value="<?=str::str_code($DHLAccountOpenStep2['ThirdCompanyName']);?>" size="39" notnull /></div>
							</div>
							<!-- 委托付款 - 第三方银行账号 -->
							<div class="rows clean twice_box">
								<label>{/plugins.dhl_account_open.3rd_bank_account/}</label>
								<div class="input"><input type="text" class="box_input" name="ThirdBankAccount" value="<?=str::str_code($DHLAccountOpenStep2['ThirdBankAccount']);?>" size="39" notnull /></div>
							</div>
							<div class="clear"></div>
						</div>
						<!-- 委托付款 end -->

					<?php }?>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.submit/}">
							<a href="./?m=plugins&a=dhl_account_open&d=step_1"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_2">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_3') {
    	// 签名盖章
    	// 香港有单独的Known Consignor Declaration of Compliance.pdf文件
    	$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$DHLAccountOpenStep3Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep3"', 'Value');
		$DHLAccountOpenStep3 = str::json_data($DHLAccountOpenStep3Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_3/}</span> 
						</a>
					</div>
					
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.is_charged/}</label>
						<div class="input">
							<?php for ($i = 0; $i < 2; $i++) { ?>
								<span class="input_radio_box <?=$i == $DHLAccountOpenStep3['HasCharged'] ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="HasCharged" value="<?=$i;?>" <?=$i == $DHLAccountOpenStep3['HasCharged'] ? 'checked="checked"' : ''; ?>>
									</span>{/plugins.dhl_account_open.has_charged_ary.<?=$i; ?>/}
								</span>&nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>				
					</div>
					
					<div class="rows clean">
						<label></label>
						<div class="input">
							{/plugins.dhl_account_open.click_print/}: &nbsp;
							<?php if ($DHLAccountOpenStep1['AccountAddress'] == 0) {?>
								<a href="./?do_action=plugins.download_file&file=/inc/class/pdf/resource/Known Consignor Declaration of Compliance.pdf" class="input_link">Known Consignor Declaration of Compliance.pdf</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php }?>
							<a href="./?do_action=plugins.download_file&file=/inc/class/pdf/resource/DG Indemnity Letter.pdf" class="input_link">DG Indemnity Letter.pdf</a>
						</div>
					</div>
					<!-- 增值税 - 统一社会信用代码 -->
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.charged_upload_tips/}</label>
						<div class="input">
							<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
							<div class="row fileupload-buttonbar">
								<span class="btn_file btn-success fileinput-button">
									<i class="glyphicon glyphicon-plus"></i>
									<span>{/global.upload_file/}</span>
									<input type="file" name="Filedata" multiple>
								</span>
								<div class="fileupload-progress fade"><div class="progress-extended">&nbsp;</div></div>
								<div class="clear"></div>
								<div class="photo_multi_img template-box files"></div>
								<div class="photo_multi_img" id="PicDetail"></div>
							</div>
							<script id="template-upload" type="text/x-tmpl">
							{% for (var i=0, file; file=o.files[i]; i++) { %}
								<div class="template-upload fade">
									<div class="clear"></div>
									<div class="items">
										<p class="name">{%=file.name%}</p>
										<strong class="error text-danger"></strong>
									</div>
									<div class="items">
										<p class="size">Processing...</p>
										<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
									</div>
									<div class="items">
										{% if (!i) { %}
											<button class="btn_file btn-warning cancel">
												<i class="glyphicon glyphicon-ban-circle"></i>
												<span>{/global.cancel/}</span>
											</button>
										{% } %}
									</div>
									<div class="clear"></div>
								</div>
							{% } %}
							</script>
							<script id="template-download" type="text/x-tmpl">
							{% for (var i=0, file; file=o.files[i]; i++) { %}
								{% if (file.thumbnailUrl) { %}
									<div class="pic template-download fade hide">
										<div>
											<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
											<a href="{%=file.url%}" class="zoom" target="_blank"></a>
											{% if (file.deleteUrl) { %}
												<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
												<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
											{% } %}
											<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
										</div>
										<input type="text" maxlength="30" class="box_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
									</div>
								{% } else { %}
									<div class="template-download fade hide">
										<div class="clear"></div>
										<div class="items">
											<p class="name">
												{% if (file.url) { %}
													<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
												{% } else { %}
													<span>{%=file.name%}</span>
												{% } %}
											</p>
											{% if (file.error) { %}
												<div><span class="label label-danger">Error</span> {%=file.error%}</div>
											{% } %}
										</div>
										<div class="items">
											<span class="size">{%=o.formatFileSize(file.size)%}</span>
										</div>
										<div class="items">
											{% if (file.deleteUrl) { %}
												<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
													<i class="glyphicon glyphicon-trash"></i>
													<span>{/global.del/}</span>
												</button>
												<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
											{% } else { %}
												<button class="btn_file btn-warning cancel">
													<i class="glyphicon glyphicon-ban-circle"></i>
													<span>{/global.cancel/}</span>
												</button>
											{% } %}
										</div>
										<div class="clear"></div>
									</div>
								{% } %}
							{% } %}
							</script>
						</div>
					</div>
					<?php if ($DHLAccountOpenStep1['AccountAddress'] == 0) {?>
						<div class="rows clean twice_box twice_first">
							<label>{/plugins.dhl_account_open.kc_filepath/}</label>
							<div class="input">
								<input name="KCFilePath" value="<?=$DHLAccountOpenStep3['KCFilePath'];?>" type="text" class="box_input filepath" id="KCFilePath" size="50" maxlength="100" />
							</div>
						</div>
						<div class="rows clean twice_box">
							<label>{/plugins.dhl_account_open.kc_filename/}</label>
							<div class="input">
								<input name="KCFileName" value="<?=$DHLAccountOpenStep3['KCFileName'];?>" type="text" class="box_input filename" id="KCFileName" size="50" maxlength="100" data-name="Known Consignor Declaration of Compliance" />
							</div>
						</div>
						<div class="clear"></div>
					<?php }?>
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.dg_filepath/}</label>
						<div class="input">
							<input name="DGFilePath" value="<?=$DHLAccountOpenStep3['DGFilePath'];?>" type="text" class="box_input filepath" id="DGFilePath" size="50" maxlength="100" />
						</div>
					</div>
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.dg_filename/}</label>
						<div class="input">
							<input name="DGFileName" value="<?=$DHLAccountOpenStep3['DGFileName'];?>" type="text" class="box_input filename" id="DGFileName" size="50" maxlength="100" data-name="DG Indemnity Letter" />
						</div>
					</div>
					<div class="clear"></div>

					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.submit/}">
							<a href="./?m=plugins&a=dhl_account_open&d=step_2"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_3">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_4') {
    	// 上传资料
		$DHLAccountOpenStep4Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep4"', 'Value');
		$DHLAccountOpenStep4 = str::json_data($DHLAccountOpenStep4Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_5">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_4/}</span> 
						</a>
					</div>
					<div class="blank30"></div>
					<div class="rows rows_title">
						<label>{/plugins.dhl_account_open.upload_data_title/}</label>
						<div class="input"></div>
					</div>
					
					<div class="rows clean">
						<label>{/plugins.dhl_account_open.upload_business/}</label>
						<div class="input picpath">
							<?=manage::multi_img('PicDetail_0', 'PicPath_0', $DHLAccountOpenStep4['PicPath_0']); ?>
						</div>
					</div>

					<div class="rows clean">
						<label>{/plugins.dhl_account_open.ID_card_front/}</label>
						<div class="input picpath">
							<?=manage::multi_img('PicDetail_1', 'PicPath_1', $DHLAccountOpenStep4['PicPath_1']); ?>
						</div>
					</div>

					<div class="rows clean">
						<label>{/plugins.dhl_account_open.reverse_ID_card/}</label>
						<div class="input picpath">
							<?=manage::multi_img('PicDetail_2', 'PicPath_2', $DHLAccountOpenStep4['PicPath_2']); ?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<a href="./?m=plugins&a=dhl_account_open&d=step_3"><input type="button" class="btn_global btn_prev" value="{/plugins.dhl_account_open.prev_step/}" /></a>
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/plugins.dhl_account_open.next_step/}">
							<a href="./?m=plugins&a=my_app"><input type="button" class="btn_global btn_cancel" value="{/plugins.dhl_account_open.cancel_register/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_4">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_5') {
    	// 上传资料
		$DHLAccountOpenStep5Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep5"', 'Value');
		$DHLAccountOpenStep5 = str::json_data($DHLAccountOpenStep5Data, 'decode');

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_6">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_5/}</span> 
						</a>
					</div>
					<div class="blank30"></div>
					<div class="rows">
						<label></label>
						<div class="input">{/plugins.dhl_account_open.danger_warning/}</div>
					</div>
					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.shipper_name/}</label>
						<div class="input">
							<input name="ShipperName" value="<?=$DHLAccountOpenStep5['ShipperName'];?>" type="text" class="box_input" size="50" maxlength="100" notunll />
						</div>
					</div>
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.company_name/}</label>
						<div class="input">
							<input name="CompanyName" value="<?=$DHLAccountOpenStep5['CompanyName'];?>" type="text" class="box_input" size="50" maxlength="100" notnull />
						</div>
					</div>
					<div class="clear"></div>

					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.shipper_telephone/}</label>
						<div class="input">
							<input name="ShipperTelephone" value="<?=$DHLAccountOpenStep5['ShipperTelephone'];?>" type="text" class="box_input" size="50" maxlength="100" notunll />
						</div>
					</div>
					<div class="rows clean twice_box">
						<label>{/plugins.dhl_account_open.cargo_description/}</label>
						<div class="input">
							<input name="CargoDescription" value="<?=$DHLAccountOpenStep5['CargoDescription'];?>" type="text" class="box_input" size="50" maxlength="100" notnull />
						</div>
					</div>
					<div class="clear"></div>

					<div class="rows clean twice_box twice_first">
						<label>{/plugins.dhl_account_open.freight_reference/}</label>
						<div class="input">
							<input name="FreightReference" value="<?=$DHLAccountOpenStep5['FreightReference'];?>" type="text" class="box_input" size="50" maxlength="100" notunll />
						</div>
					</div>
					<div class="clear"></div>

					<div class="rows clean">
						<label></label>
						<div class="input">
							<span class="input_text">
								<?php if ($DHLAccountOpenStep1['AccountAddress'] != 0) {?>
									<p>I hereby declare that my consignments handed over to DHL eCommerce/Global Forwarding as per above mentioned shipment reference number do NOT contain any of these products stated herein:</p>
									<p>我在此声明，我交付给DHL电子商务/全球货运的上述货运参考号的托运货物不含有此处所列的任何一种产品</p><br>

									<p>Electronic products with lithium batteries, mobile phones, Powerbank chargers, photographic equipment, torch/flashlight with lithium batteries, loose lithium batteries, prototype lithium batteries, rechargeable batteries, wet cell batteries, alkaline batteries, batteries contained in equipment, speaker, solar charger, ultra capacitor, Defective lithium batteries (either loose or packed with or contained in equipment)</p>
									<p>含锂电池的电子产品、移动电话、充电宝、照相设备、含锂电池的手电筒、散装的锂电池、原型锂电池、充电电池、湿电池、碱性电池、设备所带的电池、扬声器、太阳能充电器、超电容、有缺陷的锂电池（包括散装、包装的或设备中带的电池）</p><br>

									<p>Motor vehicles, motorcycles or electric vehicles parts with flammable lubricants, engines, lawn mowers, generator, carburettor, fuel tank, fuel, fuel controllers, fire extinguishers, air bag, ships or aircraft parts, barometer, radio controlled toy vehicles (cars/airplanes/boats) powered by flammable liquid or gas engines</p>
									<p>带可燃润滑油的机动车、摩托车或电动车辆零件、发动机、割草机、发电机、化油器、燃油箱、燃油、燃油控制器、灭火器、气囊、轮船或飞机配件、气压计、可燃液体或燃气发动机驱动的无线控制的玩具车（汽车/飞机/船）</p><br>

									<p>Personal care products in pressurized canisters with flammable gas or liquid, perfumes or body sprays, nail polish, household chemicals, cleaning agents, aerosols, bleach, drain cleaners, pesticides, wax, toolbox with flammable liquid or solvents, glue, spray paint, lubricants, lighter</p>
									<p>含可燃气体或液体的压力罐装的个人护理产品、香水或身体喷雾器、指甲油、家用化学品、清洁剂、气溶胶、漂白剂、下水道清洁剂、杀虫剂、石蜡、带可燃液体或溶剂的工具箱、胶水、喷漆、润滑油、打火机</p><br>

									<p>Agricultural or industrial chemicals, chemicals with strong odour or volatile chemicals, metal powder, pigment, flammable solids, oxidizers, toxic or corrosive substances, pesticides, compressed gases, flammable liquids, resins, inks, paints, sealants, adhesives, mercury</p>
									<p>农药或工业用化学品、有强烈气味的化学品或挥发性化学品、金属粉末、颜料、可燃固体、氧化剂、有毒或腐蚀性物质、杀虫剂、压缩气体、可燃液体、树脂、墨水、油漆、密封剂、粘合剂、水银</p><br>

									<p>Medical equipment, radioactive substances, pharmaceuticals raw materials, vaccines or virus samples, compressed gases, cryogenic liquefied gases, dry ice, sphygmomanometer, thermometer</p>
									<p>医疗设备、放射性物质、医药原材料、疫苗或病毒样本、压缩气体、低温液化气、干冰、血压计、温度计</p><br>

									<p>Electrical appliances with wet batteries or mercury contents, refrigerators or air conditioners with flammable gas, vacuum tube, magnetic tubes, industrial or high strength magnets, neodymium magnets, smoke detectors</p>
									<p>带湿电池或含水银的电器、含可燃气体的冰箱或空调、真空管、磁偏转电子射线管、工业用或高强度磁铁、钕磁铁、烟雾探测器</p><br>

									<p>Fireworks, ammunition, flares, signal flares, nail gun, drilling and mining equipment, adventure equipment</p>
									<p>鞭炮、火药、照明弹、信号弹、射钉枪、钻探和采矿设备、探险装备</p>
								<?php } else {?>
									<p>I hereby declare that my consignments handed over to DHL eCommerce/Global Forwarding as per above mentioned shipment reference number do NOT contain any of these products stated herein:</p>
									<p>我在此聲明，我交付給DHL電子商務 /全球貨運的上述貨運參考號的托運貨物不含有此處所列的任何一種產品</p><br>

									<p>Electronic products with lithium batteries, mobile phones, Powerbank chargers, photographic equipment, torch/flashlight with lithium batteries, loose lithium batteries, prototype lithium batteries, rechargeable batteries, wet cell batteries, alkaline batteries, batteries contained in equipment, speaker, solar charger, ultra capacitor, Defective lithium batteries (either loose or packed with or contained in equipment)</p>
									<p>含鋰電池的電子產品、行動電話、充電寶、照相設備、含鋰電池的手電筒筒、散裝的鋰電池、原型鋰電池、充電電池、濕電池、鹼性電池、設備所帶的電池、揚聲器、太陽能充電器、超電容、有缺陷的鋰電池（包括散裝、包裝的或設備中帶的電池）</p><br>

									<p>Motor vehicles, motorcycles or electric vehicles parts with flammable lubricants, engines, lawn mowers, generator, carburettor, fuel tank, fuel, fuel controllers, fire extinguishers, air bag, ships or aircraft parts, barometer, radio controlled toy vehicles (cars/airplanes/boats) powered by flammable liquid or gas engines</p>
									<p>帶可燃潤滑油的機動車、摩托車或電動車輛零件、發動機、割草機、發電機、化油器、燃油箱、燃油、燃油控制器、滅火器、氣囊、輪船或飛機配件、氣壓計、可燃液體或燃氣發動機驅動的無線控制的玩具車（汽車/飛機/船）</p><br>

									<p>Personal care products in pressurized canisters with flammable gas or liquid, perfumes or body sprays, nail polish, household chemicals, cleaning agents, aerosols, bleach, drain cleaners, pesticides, wax, toolbox with flammable liquid or solvents, glue, spray paint, lubricants, lighter</p>
									<p>含可燃氣體或液體的壓力罐裝的個人護理產品、香水或身體噴霧器、指甲油、家用化學品、清潔劑、氣溶膠、漂白劑、下水道清潔劑、殺蟲劑、石蠟、帶可燃液體或溶劑的工具箱、膠水、噴漆、潤滑油、打火機</p><br>

									<p>Agricultural or industrial chemicals, chemicals with strong odour or volatile chemicals, metal powder, pigment, flammable solids, oxidizers, toxic or corrosive substances, pesticides, compressed gases, flammable liquids, resins, inks, paints, sealants, adhesives, mercury</p>
									<p>農藥或工業用化學品、有強烈氣味的化學品或揮發性化學品、金屬粉末、顏料、可燃固體、氧化劑、有毒或腐蝕性物質、殺蟲劑、壓縮氣體、可燃液體、樹脂、墨水、油漆、密封劑、粘合劑、水銀</p><br>

									<p>Medical equipment, radioactive substances, pharmaceuticals raw materials, vaccines or virus samples, compressed gases, cryogenic liquefied gases, dry ice, sphygmomanometer, thermometer</p>
									<p>醫療設備、放射性物質、醫藥原材料、疫苗或病毒樣本、壓縮氣體、低溫液化氣、乾冰、血壓計、溫度計</p><br>

									<p>Electrical appliances with wet batteries or mercury contents, refrigerators or air conditioners with flammable gas, vacuum tube, magnetic tubes, industrial or high strength magnets, neodymium magnets, smoke detectors</p>
									<p>帶濕電池或含水銀的電器、含可燃氣體的冰箱或空調、真空管、磁偏轉電子射線管、工業用或高強度磁鐵、釹磁鐵、煙霧探測器</p><br>

									<p>Fireworks, ammunition, flares, signal flares, nail gun, drilling and mining equipment, adventure equipment</p>
									<p>鞭炮、火藥、照明彈、信號彈、射釘槍、鑽探和採礦設備、探險裝備</p>
								<?php }?>
							</span>
						</div>
					</div>
					
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.submit/}">
							<a href="./?m=plugins&a=dhl_account_open&d=step_4"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_5">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_6') {
    	// 上传资料
		$DHLAccountOpenStep6Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep6"', 'Value');
		$DHLAccountOpenStep6 = str::json_data($DHLAccountOpenStep6Data, 'decode');
		// 根据不同地点，显示不同的
		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_7">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_6/}</span> 
						</a>
					</div>
					<div class="blank30"></div>
					<div class="rows">
						<label></label>
						<div class="input">
							<span class="input_text">
								<?php if ($DHLAccountOpenStep1['AccountAddress'] == 0) {?>
									<p>DHL eCommerce (Hong Kong) Limited ("DHL") is committed to protecting and respecting your privacy in accordance with the Personal Data (Privacy) Ordinance of Hong Kong (the "Ordinance") and DHL’s Personal Data Protection Policy (the "Policy") at <span class="text_link">https://www.logistics.dhl/hk-en/home/footer/local-privacy-notice.html</span></p><br>

									<p>The purpose of this statement is to bring to your attention DHL’s practices and policies relating to the collection, use, processing, disclosure and retention of your personal data. Such personal data is sourced from information provided by you in past dealings with DHL and/or may be provided by you through your use of DHL’s services or DHL’s websites or that may otherwise be collected by us.</p><br>

									<p>DHL collects, uses, processes and retains personal data for its legitimate functions:</p><br>

									<p>• effecting delivery of shipments locally or abroad;</p>
									<p>• providing, monitoring and improving DHL’s services including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services; </p>
									<p>• managing and administering your account(s) with DHL; </p>
									<p>• advertising products and services provided by DHL including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services and other services which are ancillary to the aforesaid core products and business of DHL including insurance services of DHL’s business partners;</p>
									<p>• advertising products and services for environmental conservation and charitable purposes; and</p>
									<p>• such other purposes as may be required for the running of DHL’s services and businesses or by applicable laws or regulations, including the communication of the same to customs authorities.</p><br>

									<p>Without this personal data, DHL may be faced with delays or be unable to provide you with the contracted services which you require.</p><br>

									<p>Personal data collected by DHL may be transferred to, and stored or processed by a third party and/or at a destination outside Hong Kong (either within the Deutsche Post DHL group of companies or by third parties) in accordance with the Ordinance and the Policy. By submitting your personal data and/or using DHL’s services, products or websites, you agree to this transfer, storing or processing.  </p><br>

									<p>DHL has also implemented reasonable and practical security measures designed to protect against loss, misuse and/or alteration of your personal data under its control.</p><br>

									<p>You have the right to seek access to and correction of your personal data held by DHL. In addition, you may request us to delete your personal data that is no longer required for the relevant purposes which you have given consent. Such rights may be exercised by you at any time. If you wish to exercise such rights, or if you have any question regarding your personal data or this statement, please contact our Data Protection Officer in writing at 2/F EDGE. 30-34 Kwai Wing Road, Kwai Chung New Territories, Hong Kong. DHL reserves the right to charge you a reasonable fee for complying with a data access request as permitted by the Ordinance.</p>
								<?php } else {?>
									<p>DHL Global Forwarding (China) Co., Ltd.("DHL") is committed to protecting and respecting your privacy in accordance with applicable law of the People’s Republic of China (the "Law") and DHL’s Personal Data Protection Policy (the "Policy") at https://www.logistics.dhl/cn-zh/home/footer/privacy-notice.html</p><br>

									<p>The purpose of this statement is to bring to your attention DHL’s practices and policies relating to the collection, use, processing, disclosure and retention of your personal data. Such personal data is sourced from information provided by you in past dealings with DHL and/or may be provided by you through your use of DHL’s services or DHL’s websites or that may otherwise be collected by us.</p><br>

									<p>DHL collects, uses, processes and retains personal data for its legitimate functions: </p><br>

									<p>• effecting delivery of shipments locally or abroad;</p>
									<p>• providing, monitoring and improving DHL’s services including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services; </p>
									<p>• managing and administering your account(s) with DHL; </p>
									<p>• advertising products and services provided by DHL including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services and other services which are ancillary to the aforesaid core products and business of DHL including insurance services of DHL’s business partners;</p>
									<p>• advertising products and services for environmental conservation and charitable purposes; and</p>
									<p>• such other purposes as may be required for the running of DHL’s services and businesses or by applicable laws or regulations, including the communication of the same to customs authorities.</p><br>

									<p>Without this personal data, DHL may be faced with delays or be unable to provide you with the contracted services which you require.</p><br>

									<p>Personal data collected by DHL may be transferred to, and stored or processed by a third party and/or at a destination outside the People’s Republic of China (either within the Deutsche Post DHL group of companies or by third parties) in accordance with the Law and the Policy. By submitting your personal data and/or using DHL’s services, products or websites, you agree to this transfer, storing or processing.  </p><br>

									<p>DHL has also implemented reasonable and practical security measures designed to protect against loss, misuse and/or alteration of your personal data under its control.</p><br>

									<p>You have the right to seek access to and correction of your personal data held by DHL. In addition, you may request us to delete your personal data that is no longer required for the relevant purposes which you have given consent. Such rights may be exercised by you at any time. If you wish to exercise such rights, or if you have any question regarding your personal data or this statement, please contact our Data Protection Officer in writing at 2/F EDGE. 30-34 Kwai Wing Road, Kwai Chung New Territories, Hong Kong. DHL reserves the right to charge you a reasonable fee for complying with a data access request as permitted by the Law.</p>
								<?php }?>
							</span>
							<div class="blank30"></div>
							<hr style="height: 1px;border:none;background: #ccc">
						</div>
					</div>

					<div class="rows clean">
						<label></label>
						<div class="input">
							<span class="input_checkbox_box <?=$DHLAccountOpenStep6['ComfireInfomation'] ? 'checked' : '';?>">
								<span class="input_checkbox">
									<input type="checkbox" name="ComfireInfomation" value="1" <?=$DHLAccountOpenStep6['ComfireInfomation'] ? 'checked="checked"' : ''; ?> />
								</span>{/plugins.dhl_account_open.comfire_info/}
							</span>
						</div>
					</div>
					
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<a href="./?m=plugins&a=dhl_account_open&d=step_5"><input type="button" class="btn_global btn_prev" value="{/plugins.dhl_account_open.prev_step/}" /></a>
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/plugins.dhl_account_open.next_step/}">
							<a href="./?m=plugins&a=my_app"><input type="button" class="btn_global btn_cancel" value="{/plugins.dhl_account_open.cancel_register/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_6">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_7') {
    	// 价格表
		$DHLAccountOpenStep7Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep7"', 'Value');
		$DHLAccountOpenStep7 = str::json_data($DHLAccountOpenStep7Data, 'decode');
		// 根据不同地点，显示不同的
		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');
	?>
		<div class="center_container_1000">
			<div class="global_container">
				<form id="edit_form" class="global_form" data-url="./?m=plugins&a=dhl_account_open&d=step_8">
					<div class="center_container_1200">
						<a href="javascript:history.back(-1);" class="return_title">
							<span class="return">{/plugins.dhl_account_open.step_7/}</span> 
						</a>
					</div>
					<div class="blank30"></div>
					<div class="rows">
						<label></label>
						<div class="input">
							<span class="input_text">
								<p style="width:600px;margin:0 auto;text-align: center;font-size: 20px;color: #333;font-weight: bold;">General Terms and Conditions of DHL eCommerce International Transport and Delivery Services</p>
								<p style="width:600px;margin:20px auto;text-align: center;font-size: 20px;color: #999;font-weight: bold;">DHL电子商务通用条款与条件国际运输及配送服务</p>
								<div>
									<p>1. Definitions  定义</p>
									<div style="padding-left:30px">
										<p>"DHL eCommerce" means DHL eCommerce (Hong Kong) Limited.</p>
										<p>"DHL 电子商务公司"指敦豪电子商务物流(香港)有限公司。</p>
										<p>"GTC" means these General Terms and Conditions of DHL eCommerce.</p>
										<p>"GTC"指DHL电子商务公司的通用条款与条件。</p>
										<p>"Parties" refers to Sender and DHL eCommerce, and "Party" refers to each of Sender and DHL eCommerce.</p>
										<p>"双方"指寄件人与DHL电子商务公司，而"一方"指寄件人或DHL电子商务公司。</p>
										<p>"Sender" means the party receiving delivery services from DHL eCommerce in accordance with a respective agreement.</p>
										<p>"寄件人"指按照各自协议获得DHL电子商务公司配送服务的一方。</p>
										<p>"Recipient" means the addressee to whom the Shipment is sent.</p>
										<p>"收件人"指货物配送至的收件人。</p>
										<p>"Shipment" means one or more packet or parcel items containing goods or merchandise or dialogue marketing material or other items or materials which the Sender hands over to DHL eCommerce and which may be transported and delivered by any means of transport selected by DHL eCommerce, whether by air, road or any other means of transport. Each such shipment is transported under limited liability pursuant to these GTC.</p>
										<p>"货物"指由寄件人移交予DHL电子商务公司，且可以通过DHL电子商务公司选择的运输方式，无论是空运、陆运或其它运输方式，进行运输与配送的一个或多个小包或包裹物品，包含货物或商品或互动营销材料或其它物品或材料。对每票货物的运输和配送， DHL电子商务公司按照本GTC的规定承担有限责任。</p>
										<p>"E-Commerce Related Services" mean E-Facilitation and E-Fulfillment Services as offered by DHL eCommerce.</p>
										<p>"电子商务相关服务"指由DHL电子商务公司提供的电子便利及电子库存管理服务。</p>
										<p>"E-Facilitation Services" include but is not limited to web-shop or store management, including program and order management services as well as operation and maintenance of customer’s web-shop or store, including product listing, online sales, customer relationship management, customer service support and coordination of contacts with third party service providers as individually agreed between the Parties.</p>
										<p>"电子便利服务"包括但不限于网店或商店管理，包括项目与订单管理服务及客户网店或商店的运营与维护，包括产品清单、网上销售、客户关系管理、客户服务支持及双方单独约定且与第三方服务提供商之间联络的协调。</p>
										<p>"E-Fulfillment Services" include but is not limited to storage and fulfillment services as individually agreed between the Parties.</p>
										<p>"电子库存管理服务"包括但不限于双方单独约定的存储与库存管理服务。</p>
										<p>"Montreal Convention" means the Convention for the Unification of Certain Rules for International Carriage by Air.</p>
										<p>"蒙特利尔公约"指统一国际航空运输某些规则的公约。</p>
										<p>"Warsaw Convention" is the Convention for the Unification of Certain Rules Relating to International Carriage by Air, signed on 12 October 1929.</p>
										<p>"华沙公约"指于1929年10月12日签署的统一国际航空运输某些规则的公约。</p>
										<p>"UPUC" stands for Universal Postal Convention and the supplementary documents as applicable in the latest version.</p>
										<p>"UPUC"指万国邮政公约及其适用的最新版的补充文件。</p>
										<p>"Prohibited Goods" have the meaning as defined in Section 4 of these GTC.</p>
										<p>"禁运品" 的定义详见本GTC第4条。</p>
									</div>
									<br>
									<p>2. Scope 范围</p>
									<div style="padding-left: 30px">
										<p>(1) These GTC shall apply to all agreements between DHL eCommerce and the Sender regarding the transport and delivery of Shipments and any possible ancillary E-Commerce Related Services, unless otherwise agreed in writing by DHL eCommerce.</p>
										<p>除DHL电子商务公司另有书面确认外，本GTC适用于DHL电子商务公司与寄件人针对货物运输与配送及电子商务任何相关辅助服务而签订的所有协议。</p>
										<p>(2) As a part of the E-Commerce Related Services, DHL eCommerce may provide links to websites operated by third parties. DHL eCommerce is not responsible for the collection or processing of personal data or the operation or contents of such third party sites. Users should check the terms of use and privacy policies of such websites prior to use. </p>
										<p>DHL电子商务公司可提供由第三方运营的网站链接，作为电子商务相关服务的一部分。DHL电子商务公司对个人数据的收集或处理，或该第三方网站的操作或内容不承担任何责任。在使用之前，用户应仔细查阅网站使用条款与隐私政策。</p>
										<p>(3) The Sender agrees to be bound by the GTC at the time of account opening. Any revisions to the GTC will be posted at www.dhl.com or may be obtained from DHL eCommerce directly upon request. The Sender’s continued use of DHL e-Commerce’s services including but not limited to transport and delivery of Shipments, E-Commerce Related Services or of any DHL eCommerce website shall constitute the Sender’s agreement to the revised version of the GTC, and the Sender also agrees to abide by the terms of use and privacy policy posted at www.dhl.com.</p>
										<p>寄件人同意遵守於开户时的GTC并受其约束。有关任何对GTC的修订，请访问www.dhl.com或直接向DHL电子商务公司索取。若寄件人继续使用DHL电子商务公司的服务，包括但不限于货物的运输与配送、电子商务相关服务或DHL电子商务公司的任何网站，则视为寄件人同意GTC修订版内容，并同意遵守www.dhl.com公布的使用条款与隐私政策。</p>
										<p>(4) The Sender’s general terms and conditions shall not apply and are herewith explicitly excluded, even if DHL eCommerce has accepted the Shipment without any express objection. Any terms and conditions which amend or modify these GTC shall be agreed in writing between the Parties.</p>
										<p>在此，寄件人的通用条款与条件不适用于DHL电子商务公司提供的运输服务且被明确排除，即使DHL电子商务公司接受了货物且无任何明确异议。任何对本GTC做出修改和修订的条款及条件必须经双方的书面同意。</p>
									</div>
									<br>
									<p>3. Agreements and Services 协议与服务</p>
									<div style="padding-left: 30px;">
										<p>(1) Contracts regarding transport and delivery of Shipments are concluded between the Sender and DHL eCommerce, either in written form or by way of hand-over of the Shipment and acceptance of the same for transport and delivery in accordance with these GTC.</p>
										<p>寄件人与DHL电子商务公司以书面形式或通过按本GTC交付运送货物和接受运送货物的形式，订立货物运输与配送合同。</p>
										<p>(2) DHL eCommerce accepts Shipments for transport and delivery from the Sender at the sites of DHL eCommerce, or picks up such Shipments at agreed sites of Sender, in order to deliver such Shipments to the Recipient directly or have them delivered by another service provider.</p>
										<p>DHL电子商务公司同意在其网点接受寄件人需运输与配送的货物，或至约定的寄件人地点收取该货物，从而直接或通过其他服务提供商将货物发运至收件人。</p>
										<p>(3) The Sender shall label the Shipment correctly and provide all necessary details to enable DHL eCommerce to perform the services including transport and delivery, settling of damages claims and/or return of the Shipment, as the case may be.</p>
										<p>寄件人应正确标记货物并提供所需详细信息，便于DHL电子商务公司履行其服务，包括货物的运输与配送、解决损害赔偿问题和/或退运，视乎具体情况而定。</p>
										<p>(4) DHL eCommerce will accept special instructions from the Sender for Shipments only if these instructions are notified in the agreed form or in a separate agreement between the Parties. DHL eCommerce is not obliged to comply with any special instructions if these are issued only after the Shipment has been handed over for transport and delivery.</p>
										<p>DHL电子商务公司接受寄件人对货物的特殊说明，但是，该说明必须以双方同意的形式或由双方签署单独协议明示。若该特殊说明在货物已交付运输与配送后才予以发布，则DHL电子商务公司无需遵守该说明。</p>
										<p>(5) The Sender agrees to all routing and diversion, including the possibility that the Shipment will be transported via intermediate stops, at the sole and absolute discretion of DHL eCommerce.</p>
										<p>寄件人同意所有的配送路线与转移行为，包括DHL电子商务公司可以酌情自行决定的货物有可能通过中途站运输的事项。</p>
									</div>
									<br>
									<p>4. Shipments 货物</p>
									<div style="padding-left: 30px;">
										<p>(1) DHL eCommerce shall not undertake the transport and delivery of any Shipment which contains Prohibited Goods (as defined below). Prohibited Goods include, but are not limited to the items listed below, and as may be updated from time to time by DHL eCommerce (the latest updated list is available at www.dhl.com):</p>
										<p>DHL电子商务公司不得运输及配送包含禁运品（定义如下）的货物。禁运品包括但不限于下文所列物品及DHL电子商务公司随时更新的物品清单（最新更新清单请访问www.dhl.com获取）:</p>
											<div style="padding-left: 30px;">
												<p>1. Shipments the content, external form, transportation or storage of which violates a statutory prohibition or a prohibition by a public authority, in particular – but without limitation – regulations regarding export, import or customs law of the countries of origin, destination or transit, or goods for which special equipment (e.g. for temperature-controlled goods), safety precautions or authorizations are required;</p>
												<p>其内容、外形、对其的运输或存储违反了法定禁令或公共机构公布的禁令的货物，特别，但不限于违反原产国、目的国或转运国的出口、进口或海关法，或对设备（如温控商品）、安全措施或批准有特殊要求的商品；</p>
												<p>2. Shipments or items the transportation of which is prohibited or is subject to special restrictions under UPUC, International Air Transport Association (IATA) or International Civil Aviation Organisation (ICAO) rules;</p>
												<p>UPUC、国际航空运输协会（IATA）或国际民航组织（ICAO）规定禁止运输或特殊限制的货物或物品；</p>
												<p>3. Shipments the transportation and/or storage of which is subject to hazardous goods regulations, including but not limited to goods that are not completely free from restrictions under current IATA and ICAO hazardous goods regulations;</p>
												<p>其运输和/或存储属于危险品监管范围内的货物，包括但不限于任何受当前IATA及ICAO危险品监管规定限制的商品；</p>
												<p>4. Shipments the content of which violates intellectual property rights, including forged, counterfeit or unlicensed copies of products (brand and trademark piracy);</p>
												<p>其内容违反了知识产权，包括对产品的伪造、仿造或未经授权复制（品牌与商标盗用）的货物；</p>
												<p>5. Shipments the content or external characteristics of which may cause death or injury to or infection of persons or damage to property;</p>
												<p>其内容或外形特征可能会致人死亡或伤残或感染或损坏财产的货物；</p>
												<p>6. Shipments containing live animals or human remains; with the exception of invertebrates if and where permitted under the provisions of the UPUC such as queen bees;</p>
												<p>包含活畜或人体残骸；UPUC条款允许的无脊椎动物除外，如蜂王；</p>
												<p>7. Shipments containing narcotics or intoxicants;</p>
												<p>包含麻醉剂或麻醉品；</p>
												<p>8. Shipments containing cash or other methods of payment, precious metals, works of art, jewelry, watches, precious stones or other valuables or securities (unless otherwise agreed by DHL eCommerce in its sole and absolute discretion);</p>
												<p>包含现金或其它支付工具、贵金属、艺术品、珠宝、手表、宝石或其它贵重物品或有价证券（DHL电子商务公司自行或酌情同意的除外）;</p>
												<p>9. Unfranked or insufficiently franked Shipments and Shipments transported or delivered with the intention of fraudulently obtaining the transport service without paying for it;</p>
												<p>非免税或不足以免税的货物及欺诈性获取免费运输或投递服务的货物；</p>
												<p>10. Shipments that contain weapons, especially firearms, or parts thereof, imitation weapons or ammunition; and</p>
												<p>包含武器，特别是枪支或武器配件、仿制武器或弹药的货物；</p>
												<p>11. Shipments which contain obscene or pornographic articles.</p>
												<p>包含淫秽或色情物品的货物。</p>
											</div>
										<p>(2) The Sender warrants that the Shipment does not contain any Prohibited Goods and has been correctly packaged and is appropriately protected. Notwithstanding any other rights of DHL eCommerce, the Sender shall indemnify DHL eCommerce from any liability for third-party claims resulting from the transportation or delivery of Prohibited Goods or other inadmissible or unlawful goods. The contractual liability of DHL eCommerce for its own conduct and that of its agents or subcontractors remains unaffected.</p>
										<p>寄件人保证货物不包含任何禁运品，并且货物经过正确包装，得到妥当的保护。尽管DHL电子商务公司拥有其它权利，寄件人应赔偿DHL电子商务公司因禁运品或其它不被许可或非法商品的运输或配送所引起的第三方的索赔责任。合同规定的DHL电子商务公司对其自身及其代理商或分包商行为承担的责任不受影响。</p>
										<p>(3) The Sender undertakes to indemnify DHL eCommerce promptly upon first demand against any loss or damages arising out of any alleged third-party claims and any other loss or damage that DHL eCommerce incurs as a result of the transportation or delivery of the Prohibited Goods. The indemnity by the Sender shall also cover the expenses incurred by DHL eCommerce in connection with the provision of information, confiscation by the customs authorities or border seizure which are required by law or have been ordered by a court or a government authority.</p>
										<p>寄件人承诺，一经DHL电子商务公司提出请求，寄件人应立即赔偿DHL电子商务公司因禁运品的运输或配送所引起的第三方的索赔责任而产生的损失或损害及任何其他损失或损害。寄件人的赔偿也应涵盖DHL电子商务公司因应法律要求或法庭或政府当局命令的信息提供、海关当局的没收或边境扣押而产生的相关的费用。</p>
										<p>(4) If a Shipment contains Prohibited Goods or the Shipment – because of its nature (size, format, weight, contents, etc.) or for other reasons – does not comply with Section 4(2) above or with the other provisions of these GTC, DHL eCommerce shall be entitled to:</p>
										<p>若货物包含禁运品，或因其性质（尺寸、样式、重量及货物内容等）或其它原因，货物未能遵守上述第4（2）条的规定或本GTC其它条款的规定，则DHL电子商务公司有权:</p>
											<div style="padding-left: 30px">
												<p>1. refuse acceptance of the Shipment;</p>
												<p>拒收货物；</p>
												<p>2. if the Shipment has already been handed over, abandon it, dispose of it, hand it over to the relevant authorities, return it or store it until its collection and to invoice the Sender for any additional costs incurred as a result of taking any of the aforementioned measures; or</p>
												<p>若货物已进行交接，则可以将其丢弃、处置、交付给有关当局、退回或存储直至收回，并且向寄件人开具因采取以上措施产生的额外费用的发票；或</p>
												<p>3. transport the Shipment without notifying the Sender and, if necessary and/or required by law, to choose a different route (e.g. by road and sea instead of by air freight as planned) and to invoice the Sender for any additional costs incurred as a result,without incurring any liability to the Sender, Recipient or any other third party.</p>
												<p>无需通知寄件人的情况下，对货物进行运输，若情况需要和/或法律要求，可选择不同路线（如陆运或海运，而不是计划的空运）进行运输，并且向寄件人开具由此产生的任何额外费用的发票，且不会招致对寄件人、收件人或其他任何第三方的任何责任。</p>
												<p>DHL eCommerce shall also be entitled to exercise the rights referred to in the paragraph above if it suspects that the Shipment contains Prohibited Goods or that there are any breaches of contract and the Sender fails to comply with DHL eCommerce’s request to supply information.</p>
												<p>若DHL电子商务公司怀疑货物包含禁运品，或货物违反合同约定且寄件人未能按照要求提供相关信息，则DHL电子商务公司也有权行使其上述权利。</p>
											</div>
										<p>(5) DHL eCommerce is not obliged to check whether a Shipment contains Prohibited Goods. However, DHL eCommerce shall be entitled to open a Shipment and to inspect the contents if it suspects that the Shipment contains Prohibited Goods. In addition to the foregoing, DHL eCommerce has the right to open and inspect a Shipment without notice for security or customs or other valid reasons. Further, DHL eCommerce carries out regular checks in accordance with the applicable statutory aviation security regulations and if goods which may not be transported by air are found, or if there is reason to suspect that these goods ought not to be transported by air, DHL eCommerce shall be entitled to transport the goods by land or sea, notwithstanding its other rights under Section 4(4).</p>
										<p>DHL电子商务公司并无开箱检查货物是否包含禁运品的义务。但是，若其怀疑货物包含禁运品，则DHL电子商务公司有权打开货物检查内部货物。此外，DHL电子商务公司有权以安全或海关要求或其它合法理由不经通知对货物进行开箱检查。再者，DHL电子商务公司应按照适用的法定航空安全条例规定对货物进行检查，尽管存在第4（4）条规定的其它权利，若DHL电子商务公司发现不能空运的货物，或有理由怀疑该货物不得进行空运，则其有权通过陆运或海运的形式运输该货物。</p>
									</div>
									<br>
									<p>5.Customs Clearance and Customs Regulations 海关放行与海关法规</p>
									<div style="padding-left: 30px">
										<p>(1) The Sender is obliged to comply with the applicable import and export regulations and the customs regulations of the country of origin, destination and transit. The Sender shall complete the necessary accompanying documents (customs declaration, export licenses etc.) truthfully and completely, and shall hand these over with the Shipment.</p>
										<p>寄件人有义务遵守所适用的原产国、目的国及转运国的进出口法规和海关规定。寄件人应如实完整地填写必要的随附文件（报关单、出口许可证等），且在交接货物时提供。</p>
										<p>(2) DHL eCommerce does not assume any liability for the content of the Shipment and the accompanying documents, even if these are prepared by or on behalf of DHL eCommerce upon the Sender’s request. The Sender remains solely responsible for all risks and consequences of importing and exporting goods. This shall apply independently of the grounds on which the dispatch is restricted or prohibited, either by applicable statutory provisions or is restricted or excluded under these GTC or other contractual provisions. Sections 2(3) and 4(3) remain unaffected.</p>
										<p>DHL电子商务公司对于货物内容及随附文件不承担任何责任，即使这些文件应寄件人的请求由DHL电子商务公司或以DHL电子商务公司名义准备。寄件人应独立承担进出口商品的所有风险及后果。即使按照适用的法定条款规定，发货受限或禁止，或按照本GTC或其它合同条款规定，发货受到限制或被拒绝，上述规定仍然适用，并且不会影响第2（3）条与4（3）条的规定。</p>
										<p>(3) The Sender shall indemnify DHL eCommerce from third-party claims arising from or in connection with violations against the provisions specified in this Section 5.</p>
										<p>寄件人应赔偿DHL电子商务公司因违反第5条规定引起或相关的第三方索赔责任。</p>
									</div>
									<br>
									<p>6.Delivery and Non-Deliverable Shipments 交付及无法交付的货物</p>
									<div style="padding-left: 30px">
										<p>(1) The Shipments shall be delivered to the Recipient's address specified by the Sender, though not necessarily personally to a Recipient named in person. Shipments to addresses with central delivery departments shall be delivered to these departments.</p>
										<p>货物应配送至寄件人指定的收件人的地址，但不一定由收件人亲自接收。送货地址有中心配送部门的，货物应送至该部门。</p>
										<p>(2) If necessary for the return of undeliverable Shipments, the Sender agrees that a corresponding return label as per DHL eCommerce’s requirements shall be attached to such Shipment. The Sender shall use best efforts to assist DHL eCommerce in returning such Shipment and particularly to furnish all necessary customs documents and all other documents and information which may be required for the return.</p>
										<p>若有必要对无法交付的货物进行退货处理的，寄件人同意按照DHL电子商务公司的要求在货物上贴上相应的退货标签。在该货物退回时，收件人应尽其所能为DHL电子商务公司提供协助，特别是海关文件及所有退货所需的其他文件与信息。</p>
										<p>(3) If the Recipient refuses to accept a Shipment or refuses to make payment, DHL eCommerce shall be entitled to release, sell, destroy or otherwise dispose of such Shipment without incurring any liability to the Sender, Recipient or any other third party, provided that DHL eCommerce has made reasonable efforts to return such Shipment at the expense of the Sender or if applicable law prohibits or prevents the return of such Shipment to the Sender.</p>
										<p>若收件人拒收货物或拒绝付款，则DHL电子商务公司有权放弃、销售、销毁或以其他方式处理该货物，且不会招致对寄件人、收件人或其它第三方的任何法律责任，前提是 DHL电子商务公司已经就，在费用由寄件人承担的条件下将该货物退回至寄件人，做出过合理的努力；或适用法律禁止或阻止将该货物退回至寄件人。</p>
										<p>(4) Unless special instructions are issued in accordance with Section 3(4), if an undeliverable Shipment is returned in accordance with Sections 6(2) and 6(3), DHL eCommerce reserves the right to determine the timeframe for, the manner (i.e. whether individually or on a consolidated basis) and the mode of transport for such returned Shipment.</p>
										<p>除非按照第3（4）条规定发布了其它特殊说明，若未交付货物按照第6（2）与6（3）条的规定退回至寄件人，则DHL电子商务公司有权决定退货时间、方式（即单独或拼箱退货）与运输方式。</p>
									</div>
									<br>
									<p>7.Charges 费用</p>
									<div style="padding-left: 30px">
										<p>(1) The Sender shall pay to DHL eCommerce the agreed charges for the agreed services.</p>
										<p>寄件人应向DHL电子商务公司支付约定服务的协定费用。</p>
										<p>(2) All prices indicated are net prices and are exclusive of any taxes, customs duties and fees. Such taxes, customs duties and fees shall be invoiced to and payable or reimbursable by Sender.</p>
										<p>所有标明的价格均为净价，不包含任何税费、关税和费用。该等税费、关税与费用应向寄件人开具发票，并由其支付与报销。</p>
										<p>(3) All invoices shall be due and payable by the Sender, without deduction or set-off, within the credit period granted by DHL eCommerce.</p>
										<p>在DHL电子商务公司授予的付款期限内，所有发票为到期应付并应由寄件人支付，不得进行扣减或抵消。</p>
										<p>(4) In case of non-payment by the Sender of any outstanding amount, DHL eCommerce shall be entitled to suspend any or all of the services, charge interest on all overdue amounts from the due date until payment and/or exercise such other right or remedy in respect of such outstanding amount.</p>
										<p>若寄件人未能按时支付任何未清偿款项，则DHL电子商务公司有权中止其所有服务，并收取自到期日起至付款日止的所有到期未付款项的利息和/或行使其他类似权利或救济。</p>
										<p>(5) In the event that the Sender’s original choice of service and/or product is no longer applicable or available for any reason, DHL eCommerce reserves the right to select the next best available or appropriate service and/or product in respect of the Sender’s Shipment and the charges for the service and/or product actually performed shall constitute the charges for the said Shipment.</p>
										<p>若因某些原因，无法提供寄件人最初选择的服务和/或产品，则DHL电子商务公司有权为寄件人货物选择次佳可用或合适的服务和/或产品，且以实际提供的服务和/或产品发生的费用为准，构成所述运送服务的服务费用。</p>
										<p>(6) DHL eCommerce reserves the right to charge based on the higher of actual or volumetric weight per piece and any Shipment may be re-weighed and re-measured by DHL eCommerce to confirm this calculation. This is referred to as "chargeable weight" and may be billed on a separate invoice.</p>
										<p>DHL电子商务公司有权根据每件货物实际或体积重量部分以高者计进行计费，且任何货物可由DHL电子商务公司进行重新称重及重新测量，从而确定计算结果。上述重量被称为"计费重量"，可分别开具发票。</p>
									</div>
									<br>
									<p>8.Liability 责任</p>
									<div style="padding-left: 30px">
										<p>(1) DHL eCommerce’s liability for any and all services is strictly limited to direct loss and damage to a Shipment only and to the limits of liability set out in this Section 8. All other types of loss or damage are excluded (including but not limited to lost profits, income, interest, future business), whether such loss or damage is special or indirect, and even if the risk of such loss or damage was brought to DHL eCommerce’s attention before or after acceptance of the Shipment.</p>
										<p>DHL电子商务公司对任何和所有服务所承担的责任仅限于货物的直接损失及损害，且仅限于第8条规定的责任。即便DHL电子商务公司已在接受货物之前或之后知悉相关的损失或损害风险，DHL电子商务公司无需承担任何其他类型的损失或损害（包括但不限于利润、收入、利息及未来业务的损失），无论该损失或损害是否是特殊的或间接的。</p>
										<p>(2) DHL eCommerce’s liability in respect of any one international Shipment is limited as follows:</p>
										<p>DHL电子商务公司对任何一票国际货物的责任仅限于以下内容：</p>
										<div style="padding-left: 30px">
											<p>1. For DHL Packet Plus International and DHL Parcel International Standard Shipments, DHL eCommerce’s liability shall be limited to the Shipment’s declared value or 40 Euros, whichever is lower. If any of the aforesaid limitation amounts are in a currency other than the currency in which the invoices are normally presented, such amounts shall be converted at DHL eCommerce’s then-prevailing Network Exchange Rate (NER) to the currency in which the invoices are normally presented.</p>
											<p>对于DHL Packet Plus International及DHL Parcel International Standard货物，DHL电子商务公司的责任仅限于货物的申报价值或40欧元，以较低者为准。若上述限制金额并非为发票常用货币形式，则该金额应按照DHL电子商务公司届时通行的网络汇率转换至发票常用货币计算金额。</p>
											<p>2. For DHL Parcel International Direct Shipments, DHL eCommerce’s liability shall be limited to (i) the Shipment’s declared value, (ii) 100 Euros, or (iii) 25 Euros per kilogram, whichever is the lowest. If any of the aforesaid limitation amounts are in a currency other than the currency in which the invoices are normally presented, such amounts shall be converted at DHL eCommerce’s then-prevailing Network Exchange Rate (NER) to the currency in which the invoices are normally presented.</p>
											<p>对于DHL Parcel International Direct货物，DHL电子商务公司的责任仅限于（i）货物的申报价值，（ii）100欧元，或（iii）25欧元/公斤，以最低者为准。若上述限制金额并非为发票常用货币形式，则该金额应按照DHL电子商务公司届时通行的网络汇率转换至发票常用货币计算金额。</p>
										</div>
										<p>(3) If Sender regards the limits set out in Sections 8(2)(1) and 8(2)(2) as insufficient it must either request for Shipment Value Protection as described in Section 8(5) below (which will entail the payment of a supplementary charge) or make its own insurance arrangements.</p>
										<p>若寄件人认为第8（2）（1）及8（2）（2）所述限额不足以弥补货物损失价值，则其须按照以下第8（5）条的规定申请货物保价服务（其寄件人须支付额外费用）或自行购买保险。</p>
										<p>(4) All claims must be submitted in writing to DHL eCommerce within the timeframes set out below, failing which DHL eCommerce shall have no liability whatsoever:</p>
										<p>所有的索赔必须以书面形式在以下规定的时间内提交DHL电子商务公司，若未能按照上述要求进行提交，则DHL电子商务公司无需承担任何责任：</p>
										<div style="padding-left: 30px">
											<p>1. For DHL Packet Plus International and DHL Parcel International Standard Shipments, all claims must be submitted in writing to DHL eCommerce within sixty (60) days from the date that DHL eCommerce accepted the Shipment.</p>
											<p>对于DHL Packet Plus International与DHL Parcel International Standard货物，所有的索赔必须在DHL电子商务公司接受货物之日起六十（60）天内以书面形式提交DHL电子商务公司。</p>
											<p>2. For DHL Parcel International Direct Shipments, all claims must be submitted in writing to DHL eCommerce within thirty (30) days from the date that DHL eCommerce accepted the Shipment.</p>
											<p>对于DHL Parcel International Direct货物，所有的索赔必须在DHL电子商务公司接受货物之日起三十（30）天内以书面形式提交DHL电子商务公司。</p>
											<p>3. For Shipments covered by Shipment Value Protection, all claims must be submitted in writing to DHL eCommerce within thirty (30) days from the date that DHL eCommerce accepted the Shipment.</p>
											<p>对于接受货物保价服务的货物，所有的索赔必须在DHL电子商务公司接受货物之日起三十（30）天内以书面形式提交DHL电子商务公司。</p>
											<p>Claims are limited to one claim per Shipment, settlement of which will be full and final settlement for all loss or damage in connection therewith. All of the original shipping cartons, packing and contents must be made available for DHL eCommerce’s inspection and retained until the claim is concluded. DHL eCommerce is not obliged to act on any claim until all service charges have been paid.</p>
											<p>每件货物仅限一次索赔，理赔为对相关的损失与损害的全部和最终的结算。所有的原始装运箱、包装及内容须供DHL电子商务公司检查及保留，直至理赔结束。在所有服务费用结清之前， DHL电子商务公司无需支付任何索赔金额。</p>
										</div>
										<p>(5) DHL eCommerce can arrange Shipment Value Protection ("SVP") (to the extent that SVP is available) for Sender covering higher liability limit bands in respect of loss of or physical damage to the Shipments referred to in Section 8(3) above, provided the Sender requests and signs up for SVP before the Shipment is consigned to DHL eCommerce and pays the applicable supplementary charge for the relevant liability limit band. Additional terms and conditions relating to SVP shall apply and these shall be communicated to the Sender at the time the Sender signs up for SVP. For the avoidance of doubt, Section 8(4) shall apply to a claim relating to a SVP Shipment.</p>
										<p>对于上述第8（3）条所提及的对货物造成的损失或物理损坏，DHL电子商务公司可为寄件人安排货物保价服务（"SVP"）（在可行的情况下）以提供更高的责任限额，但是寄件人应在将货物交付于DHL电子商务公司之前申请SVP服务并为相关的责任限额级别支付适用的额外费用。关于SVP的附加条款也应适用，该等SVP附加条款将在寄件人申请SVP服务之时与寄件人沟通。为防止歧义，第8（4）条规定适用于与SVP货物相关的索赔。</p>
										<p>(6) Notwithstanding the liability limits set out in Section 8(2), the Montreal Convention, the Warsaw Convention, or UPUC may apply to certain Shipments. In such cases, the liability limits set out in these conventions shall apply (as appropriate) to limit DHL eCommerce’s liability for loss or damage.</p>
										<p>尽管第8（2）条所述责任限制，蒙特利尔公约、华沙公约或UPUC可能适用于某些货物。在该情况下，上述公约所述责任限制应适用于（视情况而定）限制DHL电子商务公司对损失或损害的责任。</p>
									</div>
									<br>
									<p>9.Sanctions 制裁</p>
									<div style="padding-left: 30px">
										<p>(1) Sender warrants that neither the receipt, transportation nor the delivery of its Shipments will expose DHL eCommerce or its employees, servants, agents, subcontractors, insurers or reinsurers to any sanction, prohibition or penalty (or any risk of sanction, prohibition or penalty) whatsoever imposed by any state, country, international governmental organization or other relevant authority (collectively "Sanctions") by reason of the content of the Shipments, any insurance of the Shipments taken out by the Sender or any other person with an interest in the Shipments, the destination of the Shipments, the intended consignee of the Shipments or the purchaser or end user of the content of the Shipments.</p>
										<p>寄件人保证货物的接收、运输及配送不会因所装载的货物内容、寄件人或货物其他权益相关方购买的保险、货物的目的地、货物的收货人或货物内容的买方或最终用户，使DHL电子商务公司或其员工、雇员、代理商、转包商、保险公司或再保人受到任何州、国家、国际政府组织或其它相关当局的任何制裁、禁令或处罚（或任何制裁、禁令或处罚风险）（统称为"制裁"）。</p>
										<p>(2) Sender warrants in particular, that:</p>
										<p>寄件人特别保证：</p>
										<div style="padding-left: 30px">
											<p>1. Shipments shall not include any goods which appear on any applicable list of prohibited goods as shall be determined from time to time by the United States, the United Nations, the European Union, the country of origin, country of destination and any transit countries;</p>
											<p>货物并不包含美国、联合国、欧盟、原产国、目的国及任何转运国随时决定的任何禁运品清单中的货物；</p>
											<p>2. delivery of its Shipment to the intended consignee will not, in and of itself, contravene any of the prohibitions set forth from time to time by the United States, the United Nations, the European Union, the country of origin, country of destination and any transit countries; and</p>
											<p>将货物交付至收货人，就其本身而言，不会违反美国、联合国、欧盟、原产国、目的国及任何转运国随时宣布的任何禁令；</p>
											<p>3. delivery of this Shipment to the intended consignee will not, in and of itself, result in any funds or economic resources being made available directly or indirectly to or for the benefit of any person entity or body which is listed or designated in any Sanctions or legislation covering Denied Parties as set forth from time to time by the United States, the United Nations, the European Union, the country of origin, country of destination and any transit countries.</p>
											<p>将货物交付至收货人，就其本身而言，不会导致任何资金或经济资源被美国、联合国、欧盟、原产国、目的国及任何转运国随时宣布的包含黑名单在内的任何制裁或法律中指明或指定的任何个人、实体或组织，直接或间接获得，或获益。</p>
										</div>
										<p>(3) Sender agrees to provide DHL eCommerce immediately on request with full information about the nature of its Shipment and their intended use, as well as the identities of all parties which have any legal, financial or commercial interest in the Shipment.</p>
										<p>应DHL电子商务公司的要求，寄件人同意立即向DHL电子商务公司提供有关其货物属性及预期用途的完整信息，以及对该货物具有任何法定、经济或商务利益的各方的身份信息。</p>
										<p>(4) DHL eCommerce is entitled to inspect Shipments and, in particular, is entitled to access any data or information contained in any electronic storage medium and DHL eCommerce shall not be responsible for any delay or damage caused as a result of that inspection provided that DHL eCommerce shall take reasonable care in inspecting the Shipment. Where data or information is protected by a password, details of that password shall be provided to DHL eCommerce by Sender on request.</p>
										<p>DHL电子商务公司有权对货物进行检查，特别是，有权获取任何电子存储媒介中包含的任何数据或信息，且DHL电子商务公司无需就检查所产生延误或损坏承担任何责任，前提是，DHL电子商务公司应以合理的小心谨慎检查货物。若数据或信息受密码保护，则寄件人应DHL电子商务公司的要求应将密码信息提供予DHL电子商务公司。</p>
										<p>(5) Sender shall indemnify DHL eCommerce against all loss, damage, fines and expenses whatsoever, including but not limited to exposure of DHL eCommerce, its employees, servants, agents, subcontractors, insurers or re-insurers to any Sanctions arising or resulting from any non-declaration or illegal, inaccurate and/or inadequate declaration in respect of the Shipment by the Sender or from any other cause in connection with the Shipment for which DHL eCommerce is not responsible.</p>
										<p>寄件人应赔偿DHL电子商务公司任何形式的损失、损害、罚款与费用，包括但不限于使DHL电子商务公司或其员工、雇员、代理商、转包商、保险公司或再保人因寄件人就货物有任何未申报或非法、不准确和/或不充分申报或任何其他与货物有关而非DHL电子商务公司责任的原因而引起或招致的任何制裁。</p>
										<p>(6) If it appears, in the reasonable judgment of DHL eCommerce that Shipments (or any activities required in respect of the Shipments by DHL eCommerce or any other person) may expose DHL eCommerce or their employees, servants, agents, subcontractors, insurers or reinsurers to any breach of Sanctions or risk of breach of Sanctions, then:</p>
										<p>若根据DHL电子商务公司合理判断，货物（或与货物相关由DHL电子商务公司或任何其它人进行的任何活动）可能使DHL电子商务公司或其员工、雇员、代理商、转包商、保险公司或再保人违反任何制裁或承受违反制裁的风险，则：</p>
										<div style="padding-left: 30px">
											<p>1. DHL eCommerce may refuse to carry Shipments or alternatively DHL eCommerce may without notice to Sender (but as his agent only) take any measure(s) and/or incur any additional expense to carry or to continue the Shipment thereof, and/or abandon the Shipment and/or store the Shipment ashore or afloat, under cover or in the open, at any place, which abandonment or storage shall be deemed to constitute due performance by DHL eCommerce of all of its obligations in respect of that Shipment;</p>
											<p>DHL电子商务公司可拒绝运送货物，或无需通知寄件人（仅作为其代理）采取任何措施和/或支出任何额外费用运送或继续运送该货物，和/或遗弃该货物，和/或将该货物存储到岸上或海上，置于遮蔽物之下或露天中的任何地方，且，该遗弃或存储应视为DHL电子商务公司已适当履行所有与货物相关的合同义务；</p>
											<p>2. Sender shall indemnify DHL eCommerce against any additional expense so incurred;</p>
											<p>寄件人应赔偿DHL电子商务公司任何由此产生的额外费用；</p>
											<p>3. Sender shall indemnify DHL eCommerce against any and all claims whatsoever brought by any third party in respect of the Shipments; and</p>
											<p>寄件人应赔偿DHL电子商务公司任何及所有第三方就货物提起的任何形式的索赔；</p>
											<p>4. DHL eCommerce may, without notice to Sender, provide any state, country, international governmental organization or other relevant authority with full information about the Shipments, including the identities of all parties which have any legal, financial or commercial interest in the Shipments.</p>
											<p>DHL电子商务公司不需经通知寄件人可向任何州、国家、国际政府组织或其它相关当局提供该货物的完整信息，包括对该货物具有任何法定、经济或商务利益的各方的身份信息。</p>
										</div>
									</div>
									<br>
									<p>10.Limitation of Liability regarding Delay of Delivery 延迟交货的责任限制</p>
									<div style="padding-left: 30px">
										<p>DHL eCommerce will make every reasonable effort to deliver the Shipment according to DHL eCommerce’s regular delivery schedules, but these schedules are not binding and do not form part of the contract. DHL eCommerce is not liable for any damages or loss caused by delay.</p>
										<p>DHL电子商务公司将采取一切合理措施确保按照正常交货进度交付，但是该进度并不具有法律约束力，也不构成本合同的一部分。DHL电子商务公司对因延迟所产生的任何损害或损失不承担任何责任。</p>
									</div>
									<br>
									<p>11.Force Majeure 不可抗力</p>
									<div style="padding-left: 30px">
										<p>DHL eCommerce is not liable for any loss or damage arising out of circumstances beyond DHL eCommerce’s control. These include but are not limited to electrical or magnetic damage to, or erasure of, electronic or photographic images, data or recordings; any defect or characteristic related to the nature of the Shipment, even if known to DHL eCommerce; any act or omission by a person not employed or contracted by DHL eCommerce, e.g. Sender, Recipient, third party, customs or other government official; "Force Majeure" - e.g. earthquake, cyclone, hurricane, storm, flood, fog, radiation contamination, pandemic, war, plane crash or embargo, riot or civil commotion, industrial action or disputes.</p>
										<p>DHL电子商务公司对于超出其控制范围的情形所引起的任何损失或损害不承担任何责任，包括但不限于：电子或摄影影像、数据或记录的电或磁介质损坏、或消除；即使DHL电子商务公司知晓与货物属性相关的任何瑕疵或特征；DHL电子商务公司并未雇佣或未与其签署合同的个人，如寄件人、收件人、第三方、海关或其它政府官员的任何行为或疏忽；"不可抗力"，如地震、飓风、台风、暴风雨、洪水、雾、放射性污染、传染病、战争、空难或禁运、暴动或民众骚乱、劳工行动或争议。</p>
									</div>
									<br>
									<p>12.Warranties of Sender and Indemnification 寄件人保证与赔偿</p>
									<div style="padding-left: 30px">
										<p>The Sender shall indemnify DHL eCommerce from liability for loss or damage resulting from Sender’s failure to comply with the following warranties and representations:</p>
										<p>寄件人应赔偿DHL电子商务公司因寄件人未能遵守以下保证与陈述而导致的损失或损害：</p>
										<div style="padding-left: 30px">
											<p>1.all documents and information provided by the Sender or its representatives are complete and accurate;</p>
											<p>寄件人或其代表提供的所有文件与信息完整准确；</p>
											<p>2.the Shipment is acceptable for transport under Section 4 above;</p>
											<p>按照以上第4条的规定，货物可进行运输；</p>
											<p>3.the Shipment was prepared in secure premises by reliable persons and was protected against unauthorized interference during preparation, storage and any transportation to DHL eCommerce;</p>
											<p>货物由可靠人员在安全的场所准备，并在准备、存储及运输至DHL电子商务公司的过程中未受到未经授权的干预；</p>
											<p>4.the Shipment is correctly labeled, addressed and packaged so as to ensure safe transportation with ordinary care in handling;</p>
											<p>货物已正确标记、编址及包装，从而保证正常处理与安全运输；</p>
											<p>5.the Sender has complied with all applicable customs, import, export, data protection laws and regulations, sanctions, embargoes and other laws and regulations; and</p>
											<p>寄件人已遵守所有适用的海关、进口、出口、数据保护法律与法规、制裁、禁运与其它法律和法规；</p>
											<p>6.the Sender has obtained all necessary consents to provide DHL eCommerce with personal data including Recipient’s data as may be required for transport, customs clearance and delivery.</p>
											<p>寄件人已取得所有必要的同意向DHL电子商务公司提供个人数据，包括运输、清关及交付时所需的收件人的数据。</p>
										</div>
									</div>
									<br>
									<p>13.Data Protection 数据保护</p>
									<div style="padding-left: 30px">
										<p>In the event any information submitted by Sender to DHL eCommerce contains personal data that is subject to the protection of applicable privacy and data protection laws and regulations, DHL eCommerce shall limit the disclosure and processing of the personal data (to other members of the Deutsche Post DHL group of companies worldwide and relevant third parties) to such extent as is reasonably required to effect performance of the services, to manage and administer the Sender’s account(s) with DHL eCommerce, to advertise products and services provided by DHL eCommerce (subject at all times to the Sender’s right to decline and DHL eCommerce’s compliance with applicable privacy and data protection laws and regulations) and for such other purposes as may be required by law, including, communicating the same to customs authorities. The Sender warrants that all personal data provided to DHL eCommerce has been fairly and lawfully obtained and the Sender has authority to disclose such personal data to DHL eCommerce for the purposes mentioned above. The Sender shall fully indemnify and keep DHL eCommerce fully indemnified against any and all liability incurred by DHL eCommerce as a result of such breach howsoever arising.</p>
										<p>若由寄件人提交DHL电子商务公司的任何信息包含应受适用的隐私权及数据保护法律和法规保护的个人数据，则DHL电子商务公司对个人数据（向德国邮政DHL集团的全球公司及相关第三方）的披露与处理限于DHL电子商务公司履行其服务、管理寄件人账户，及对DHL电子商务公司提供的产品与服务进行广告宣传（寄件人始终有权拒绝且DHL电子商务公司必须遵守适用的隐私权与数据保护法律与法规）及法律规定的其它目的，包括将上述信息发送至海关当局。寄件人保证向DHL电子商务公司提供的所有个人数据通过合法正当方式获得，且寄件人有权按照上述目的向DHL电子商务公司披露该个人数据。在任何情况下，寄件人应完全赔偿DHL电子商务公司因违反上述事项而产生的任何及所有责任。</p>
									</div>
									<br>
									<p>14.Final Provisions 最终条款</p>
									<div style="padding-left: 30px">
										<p>(1) Any dispute arising under or in any way connected with these GTC shall be subject to the non-exclusive jurisdiction of the courts of, and governed by the law of, the country of origin of the Shipment.</p>
										<p>因本GTC引起或相关的任何争议应受制于货物原产国法庭的非专属管辖权，并受该货物原产国法律约束。</p>
										<p>(2) The invalidity or unenforceability of any provision of these GTC shall not affect any other part of these GTC.</p>
										<p>本GTC任何条款的无效性或不可执行性不得影响本GTC其他条款的有效性及可执行性。</p>
										<p>(3) A person who is not a party to these GTC may not enforce any term of these GTC under any laws purporting to grant such rights which is hereby excluded to the extent permissible but this does not affect any right or remedy of a third party which exists or is available apart from such laws.</p>
										<p>非本GTC缔约方的个人（即第三方）不得要求执行本GTC的任何条款，即使依照某些法律规定第三方被授予该等权利（此类规定将在法律允许的前提下被排除，但第三方在此类规定以外所享有的权利或救济将不受影响）。</p>
										<p>(4) This GTC is written in both Chinese and English versions.  In case of any discrepancy or conflict between both versions, the English version shall prevail. </p>
										<p>本GTC以中文和英文书写，如两种文字相冲突矛盾的，以英文文本为准。</p>
									</div>
								</div>
							</span>
							<div class="blank30"></div>
							<hr style="height: 1px;border:none;background: #ccc">
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input">
							<span class="input_checkbox_box <?=$DHLAccountOpenStep7['ComfireInfomation'] ? 'checked' : '';?>">
								<span class="input_checkbox fl" style="margin: 7px -30px 0 0;">
									<input type="checkbox" name="ComfireInfomation" value="1" <?=$DHLAccountOpenStep7['ComfireInfomation'] ? 'checked="checked"' : ''; ?> />
								</span>
								<span class="input_text fl" style="padding-left: 30px;">{/plugins.dhl_account_open.please_comfire_2/}</span>
							</span>
						</div>
					</div>
					
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<a href="./?m=plugins&a=dhl_account_open&d=step_6"><input type="button" class="btn_global btn_prev" value="{/plugins.dhl_account_open.prev_step/}" /></a>
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/plugins.dhl_account_open.next_step/}">
							<a href="./?m=plugins&a=my_app"><input type="button" class="btn_global btn_cancel" value="{/plugins.dhl_account_open.cancel_register/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="plugins.dhl_account_open_step_7">
				</form>
			</div>
		</div>
	<?php } else if ($module == 'step_8') {
		// 图片
		$DHLAccountOpenStep4Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep4"', 'Value');
		$DHLAccountOpenStep4 = str::json_data($DHLAccountOpenStep4Data, 'decode');

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$file_ary = array(
			'Account Application Form.pdf',
			'DG Indemnity Letter.pdf',
			'Inquiry Form for VAT Invoice_sample output file.xls',
			'Online Account Opening Form in Excel.xls',
			'Personal Information Collection Statement for Customers.pdf',
			'Rate Card ' . (int)$DHLAccountOpenStep1['AccountAddress'] . '.pdf',
			'Shipper\'s Checklist for Hidden Dangerous Goods.pdf',
			$DHLAccountOpenStep4['PicPath_0'],
			$DHLAccountOpenStep4['PicPath_1'],
			$DHLAccountOpenStep4['PicPath_2'],
		);

	?>	
		<div class="inside_table">
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
				<thead>
					<tr>
						<td width="30%" nowrap="nowrap">{/plugins.dhl_account_open.filename/}</td>
						<td width="10%" nowrap="nowrap">{/plugins.dhl_account_open.size/}</td>
						<td width="10%" nowrap="nowrap">{/plugins.dhl_account_open.package_size/}</td>
						<td width="12%" nowrap="nowrap">{/plugins.dhl_account_open.edit_time/}</td>
						<td width="12%" nowrap="nowrap">{/plugins.dhl_account_open.create_time/}</td>
						<td width="12%" nowrap="nowrap">{/plugins.dhl_account_open.accessed/}</td>
						<td width="12%" nowrap="nowrap">{/plugins.dhl_account_open.attributes/}</td>
						<td width="12%" nowrap="nowrap">{/plugins.dhl_account_open.encrypted/}</td>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ((array)$file_ary as $k => $v) {
						$info = file::get_file_info(($k < 7 ? '/u_file/file/dhl/' : '') . $v);
					?>
						<tr>
							<td nowrap="nowrap"><?=$info['fileName'];?></td>
							<td nowrap="nowrap"><?=$info['fileSize'];?></td>
							<td nowrap="nowrap"><?=$info['fileSize'];?></td>
							<td nowrap="nowrap"><?=$info['createTime'];?></td>
							<td nowrap="nowrap"><?=$info['editTime'];?></td>
							<td nowrap="nowrap"><?=$info['AccessedTime'];?></td>
							<td nowrap="nowrap"><?=$info['Attributes'];?></td>
							<td nowrap="nowrap">-</td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
    <?php }?>
</div>