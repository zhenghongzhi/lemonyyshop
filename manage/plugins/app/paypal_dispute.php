<?php !isset($c) && exit();?>
<?php
if(!in_array('paypal_dispute', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}
//货币汇率
$all_currency_ary=array();
$currency_row=db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach($currency_row as $k=>$v){
	$all_currency_ary[$v['Currency']]=$v;
}
?>
<div id="paypal_dispute" class="r_con_wrap">
	<?php
	if($c['manage']['do']=='index'){
		$Status=(int)$_GET['Status'];//订单状态（搜索）
		$query_string=ly200::query_string('Status');
		
		$where='1';//条件
		$page_count=25;//显示数量
		switch($Status){
			case 1: $where.=" and Status='OPEN'"; break;
			case 2: $where.=" and Status='UNDER_REVIEW'"; break;
			case 3: $where.=" and Status='WAITING_FOR_BUYER_RESPONSE'"; break;
			case 4: $where.=" and Status='WAITING_FOR_SELLER_RESPONSE'"; break;
			case 5: $where.=" and Status='RESOLVED'"; break;
		}
		$dispute_row=str::str_code(db::get_limit_page('orders_paypal_dispute', $where, '*', 'DId desc', (int)$_GET['page'], $page_count));
        if (!$_SESSION['Manage']['Dispute'] || ($_SESSION['Manage']['Dispute'] && ($_SESSION['Manage']['Dispute'] + 1800) < $c['time'])) {
            //每隔半个小时，便可执行
            unset($_SESSION['Manage']['Dispute']);
            echo '<script type="text/javascript">$(function(){plugins_obj.paypal_dispute_init();});</script>';
        }
	?>
		<script type="text/javascript">$(function(){plugins_obj.paypal_dispute_init()});</script>
        <div class="inside_container">
            <h1>{/module.operation.paypal_dispute/}</h1>
            <ul class="inside_menu">
                <li><a href="./?m=operation&a=paypal_dispute"<?=$Status==0?' class="current"':'';?>>{/global.all/}</a></li>
                <?php for($i=1; $i<6; ++$i){?>
                    <li><a href="./?m=operation&a=paypal_dispute&Status=<?=$i;?>" status="<?=$i;?>"<?=$Status==$i?' class="current"':'';?>>{/plugins.paypal_dispute.status_<?=$i;?>/}</a></li>
                <?php }?>
            </ul>
        </div>
        <div class="inside_table">
            <?php
            if($dispute_row){
            ?>
                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                        <tr>
                            <td width="20%" nowrap="nowrap">{/plugins.paypal_dispute.case_id/}</td>
                            <td width="30%" nowrap="nowrap">{/plugins.paypal_dispute.reason/}</td>
                            <td width="15%" nowrap="nowrap">{/plugins.paypal_dispute.last_update/}</td>
                            <td width="15%" nowrap="nowrap">{/global.amount/}</td>
                            <td width="20%" nowrap="nowrap">{/global.status/}</td>
                            <td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach((array)$dispute_row[0] as $k=>$v){
                            $Symbol=$all_currency_ary[$v['Currency']]['Symbol'];
                        ?>
                            <tr>
                                <td nowrap="nowrap"><?=$v['DisputeID'];?></td>
                                <td nowrap="nowrap">{/plugins.paypal_dispute.reason_ary.<?=$v['Reason'];?>/}</td>
                                <td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['UpdateTime']);?></td>
                                <td nowrap="nowrap"><?=$Symbol.$v['Amount'].' '.$v['Currency'];?></td>
                                <td nowrap="nowrap">{/plugins.paypal_dispute.status_ary.<?=$v['Status'];?>/}</td>
                                <td nowrap="nowrap" class="operation side_by_side">
                                   <a href="./?m=operation&a=paypal_dispute&d=view&DId=<?=$v['DId'];?>&query_string=<?=urlencode($query_string);?>">{/global.view/}</a>
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php
            }else{
                echo html::no_table_data();
            }?>
        </div>
	<?php
	}elseif($c['manage']['do']=='view'){
		$DId=(int)$_GET['DId'];
		$dispute_row=str::str_code(db::get_one('orders_paypal_dispute', "DId='$DId'"));
		$offer_data=str::json_data(htmlspecialchars_decode($dispute_row['Offer']), 'decode');
		//相关消息
		$message_row=str::str_code(db::get_all('orders_paypal_dispute_message', "DId='$DId'", '*', 'AccTime desc'));
		//相关退款
		$refund_ary=array();
		$refund_row=str::str_code(db::get_all('orders_paypal_dispute_refund', "DId='$DId'"));
		$refund_count=count($refund_row);
		$refunding=0;
		if($refund_count){
			foreach((array)$refund_row as $k=>$v){
				$refund_ary[$v['RId']]=$v;
				$v['Status']>0 && $refunding+=1;
			}
		}
		//相关订单信息
		$order_row=str::str_code(db::get_one('orders', "OrderId='{$dispute_row['OrderId']}'"));
		$total_price=orders::orders_price($order_row, 1, 1);
		//相关货币信息
		$currency_row=str::str_code(db::get_one('currency', "Currency='{$dispute_row['Currency']}'"));
		$Symbol=$currency_row['Symbol'];
		//该争议的状态
		if($dispute_row['Status']=='OPEN'){//协商阶段
			$DisputeStatus=1;
		}elseif($dispute_row['Status']=='RESOLVED'){//完成阶段
			$DisputeStatus=3;
		}else{//仲裁阶段
			$DisputeStatus=2;
		}
		if($dispute_row['Status']=='UNDER_REVIEW' && $dispute_row['EvidenceJson'] && $dispute_row['EvidenceFile']){//主动提交证据，等候Paypal审核结果
			$IsUnder=1;
			$refunding+=1;
		}else $IsUnder=0;
	?>
		<script src="/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js"></script>
		<script src="/static/js/plugin/file_upload/js/external/tmpl.js"></script>
		<script src="/static/js/plugin/file_upload/js/external/load-image.js"></script>
		<script src="/static/js/plugin/file_upload/js/external/canvas-to-blob.js"></script>
		<script src="/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.iframe-transport.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-process.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-image.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-audio.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-video.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-validate.js"></script>
		<script src="/static/js/plugin/file_upload/js/jquery.fileupload-ui.js"></script>
		<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
		<script type="text/javascript">$(function(){plugins_obj.paypal_dispute_view()});</script>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.operation.paypal_dispute/}</span> 
				<span class="s_return">/ <?=$dispute_row['DisputeID'];?></span>
			</a>
			<ul class="dispute_status_list clean<?=($DisputeStatus == 2 || $dispute_row['ClaimTime']) ? ' claim' : '';?>">
				<?php
				//$status_ary=array(1=>'Inquiry', 2=>'Claim', 3=>'Settlement');
				//$status_time_ary=array(1=>'CreateTime', 2=>'ClaimTime', 3=>'ResolvedTime');
                //$DisputeStatus = 1;
                //$Rate = 0;
                if ($DisputeStatus == 1 || ($DisputeStatus == 3 && !$dispute_row['ClaimTime'])) {
                    //协商阶段
                    $status_ary = array(1=>'Inquiry', 2=>'Response', 3=>'Settlement');
                    $status_time_ary = array(1=>'CreateTime', 2=>'', 3=>'ResolvedTime');
                    $Rate = 1;
                    if ($dispute_row['Status'] == 'WAITING_FOR_BUYER_RESPONSE' || $dispute_row['Status'] == 'WAITING_FOR_SELLER_RESPONSE') {
                        //等待回应
                        $Rate = 2;
                    }
                    if ($DisputeStatus == 3) {
                        //已完成
                        $Rate = 3;
                    }
                } else {
                    $status_ary = array(1=>'Inquiry', 2=>'Response', 3=>'Claim', 4=>'UnderReview', 5=>'Settlement');
                    $status_time_ary = array(1=>'CreateTime', 2=>'', 3=>'ClaimTime', 4=>'', 5=>'ResolvedTime');
                    $Rate = 3;
                    if ($DisputeStatus == 2) {
                        //仲裁阶段
                        if ($dispute_row['Status'] == 'UNDER_REVIEW') {
                            $Rate = 4;
                        }
                    } else {
                        //已完成
                        $Rate = 5;
                    }
                }
                
				foreach($status_ary as $k=>$v){
				?>
					<li class="item item_<?=$k;?><?=$k<=$Rate?' current':'';?>">
						<strong>{/plugins.paypal_dispute.life_cycle_ary.<?=$v;?>/}</strong>
						<div class="border"></div>
						<div class="choice"><i></i></div>
                        <?php if ($status_time_ary[$k]) {?>
						    <div class="time"><?=$dispute_row[$status_time_ary[$k]]?date('Y-m-d', $dispute_row[$status_time_ary[$k]]).'<br />'.date('H:i:s', $dispute_row[$status_time_ary[$k]]):'';?></div>
                        <?php }?>
						<div class="block_box block_left"></div>
						<div class="block_box block_right"></div>
					</li>
				<?php }?>
			</ul>
			<div class="left_container">
				<div class="left_container_side">
					<?php /***************************** 争议详情 Start *****************************/?>
					<div class="global_container box_dispute_info">
						<div class="big_title">
							<strong>{/plugins.paypal_dispute.dispute_details/}</strong>
						</div>
						<div class="dispute_number">
							<strong><?=$dispute_row['DisputeID'];?></strong>
							<span><?=str_replace('%time%', date('Y-m-d H:i:s', $dispute_row['CreateTime']), $c['manage']['lang_pack']['orders']['info']['created_in']);?></span>
						</div>
						<div class="dispute_parent_box dispute_parent_top_box mb20">
                            <div class="dispute_info">
                                <ul class="list">
                                    <li class="clean">
                                        <strong>{/plugins.paypal_dispute.reason/}:</strong>
                                        <span>{/plugins.paypal_dispute.reason_ary.<?=$dispute_row['Reason'];?>/}</span>
                                    </li>
                                    <li class="clean">
                                        <strong>{/plugins.paypal_dispute.amount/}:</strong>
                                        <span><?=$Symbol.$dispute_row['Amount'].' '.$dispute_row['Currency'];?></span>
                                    </li>
                                    <?php
                                    if($offer_data){
                                        foreach($offer_data as $k=>$v){
                                    ?>
                                            <li class="clean">
                                                <strong>{/plugins.paypal_dispute.offer_ary.<?=$k;?>/}:</strong>
                                                <span><?=$Symbol.$v['value'].' '.$v['currency_code'];?></span>
                                            </li>
                                    <?php
                                        }
                                    }?>
                                </ul>
                                <div class="box_button clean">
                                    <?php
                                    if($refunding==0){
                                        if($DisputeStatus==1){
                                            echo '<input type="button" class="btn_global btn_claim btn_next" value="{/plugins.paypal_dispute.to_claim/}" />';
                                        }
                                        if($DisputeStatus==2){
                                            echo '<input type="button" class="btn_global btn_evidence btn_next" value="{/plugins.paypal_dispute.provide_evidence/}" />';
                                        }
                                        echo '<input type="button" class="btn_global btn_refund" value="{/orders.refund.refund/}" />';
                                    }?>
                                </div>
                            </div>
						</div>
					</div>
					<?php /***************************** 争议详情 End *****************************/?>
					<?php /***************************** 沟通记录 Start *****************************/?>
					<div class="global_container">
						<div class="big_title">
							<strong>{/plugins.paypal_dispute.messages/}</strong>
						</div>
						<div id="messages">
                            <?php
							if($refunding==0){
							?>
								<div class="compose">
									<form id="dispute_message_form">
										<div class="row clean">
											<div class="col_box col_left"><div class="avatar isMe">{/plugins.paypal_dispute.you/}</div></div>
											<div class="col_box col_right">
												<div class="form_group"><textarea class="form_control" name="Content" rows="7" cols="72" placeholder="{/plugins.paypal_dispute.note_ary.send_message/}" notnull></textarea></div>
											</div>
										</div>
										<div class="row clean">
											<div class="col_box col_left">&nbsp;</div>
											<div class="col_box col_right">
												<input type="submit" name="submit_button" value="{/global.send/}" class="btn_global btn_submit" />
												<input type="hidden" name="dispute_id" value="<?=$dispute_row['DisputeID'];?>" id="dispute_id" />
												<input type="hidden" name="do_action" value="plugins.dispute_message_to_customer" />
											</div>
										</div>
									</form>
								</div>
							<?php }?>
							<div class="message_list">
                                <?php
								if($IsUnder==1){
									//已发送证据提供给Paypal审核
									$evidence_data=str::json_data(htmlspecialchars_decode($dispute_row['EvidenceJson']), 'decode');
									$evidence_file=file::get_base_name($dispute_row['EvidenceFile']);
								?>
									<div class="row clean">
										<div class="col_box col_left"><div class="avatar isMe">{/plugins.paypal_dispute.you/}</div></div>
										<div class="col_box col_right">
											<div class="message_bubble">
												<p class="message_tips">{/plugins.paypal_dispute.note_ary.under_review/}</p>
												<p class="offer_detail">
													<strong>{/plugins.paypal_dispute.evidence_file/}:</strong> <a href="<?=$dispute_row['EvidenceFile'];?>" target="_blank"><?=$evidence_file;?></a><br />
													<strong>{/plugins.paypal_dispute.evidence_type/}:</strong> {/plugins.paypal_dispute.evidence_type_ary.<?=$evidence_data['evidence_type'];?>/}<br />
													<strong>{/plugins.paypal_dispute.evidence_note/}:</strong> <?=$evidence_data['notes'];?><br />
													<strong>{/plugins.paypal_dispute.carrier_name/}:</strong> {/plugins.paypal_dispute.carrier_name_ary.<?=$evidence_data['evidence_info']['tracking_info']['carrier_name'];?>/}<br />
													<strong>{/orders.shipping.track_no/}:</strong> <?=$evidence_data['evidence_info']['tracking_info']['tracking_number'];?><br />
												</p>
											</div>
										</div>
									</div>
								<?php }?>
								<?php
								foreach((array)$message_row as $k=>$v){
									if($v['RId']){
										//提议 Or 索赔
										$row=$refund_ary[$v['RId']];
										if((int)$row['Type']==0){
											//提议
								?>
											<div class="row clean">
												<div class="col_box col_left"><div class="avatar<?=$v['PostedBy']==0?' isMe':'';?>">{/plugins.paypal_dispute.<?=$v['PostedBy']==1?'customer':'you';?>/}</div></div>
												<div class="col_box col_right">
													<div class="message_bubble">
														<p class="message_tips">{/plugins.paypal_dispute.<?=$v['PostedBy']==0?'sent_an_offer':($v['Status']==2?'accepted_offer':'declined_offer');?>/}</p>
														<?php if($v['PostedBy']==0){//商家发送?>
															<p class="offer_detail"><strong>{/plugins.paypal_dispute.offer_details/}:</strong> {/plugins.paypal_dispute.offer_type_ary.<?=$row['OfferType'];?>/}<br />{/global.amount/}: <?=$Symbol.$row['Amount'].' '.$row['Currency'];?></p>
														<?php }?>
														<p class="message_body"><?=$v['Content'];?></p>
														<p class="message_footer"><span class="timestamp"><?=date('Y-m-d H:i:s', $row['AccTime'])?></span></p>
													</div>
												</div>
											</div>
											<?php if($v['PostedBy']==1 && $v['Status']==2){?>
												<div class="row clean">
													<div class="col_box col_left"><div class="avatar isMe">{/plugins.paypal_dispute.you/}</div></div>
													<div class="col_box col_right">
														<div class="message_bubble">{/plugins.paypal_dispute.note_ary.refund_with_return/}</div>
													</div>
												</div>
											<?php }?>
								<?php
										}else{
											//索赔
								?>
											<div class="row clean">
												<div class="col_box col_left"><div class="avatar<?=$v['PostedBy']==0?' isMe':'';?>">{/plugins.paypal_dispute.<?=$v['PostedBy']==1?'customer':'you';?>/}</div></div>
												<div class="col_box col_right">
													<div class="message_bubble">
														<p class="message_tips">{/plugins.paypal_dispute.<?=$v['PostedBy']==0?'accepted_claim':'';?>/}</p>
														<?php if($v['PostedBy']==0){//商家发送?>
															<p class="offer_detail"><strong>{/plugins.paypal_dispute.offer_details/}:</strong> {/plugins.paypal_dispute.reason_type_ary.<?=$row['ReasonType'];?>/}<br />{/global.amount/}: <?=$Symbol.$row['Amount'].' '.$row['Currency'];?></p>
														<?php }?>
														<p class="message_body"><?=$v['Content'];?></p>
														<p class="message_footer"><span class="timestamp"><?=date('Y-m-d H:i:s', $row['AccTime'])?></span></p>
													</div>
												</div>
											</div>
											<?php if($v['PostedBy']==1 && $v['Status']==2){?>
												<div class="row clean">
													<div class="col_box col_left"><div class="avatar isMe">{/plugins.paypal_dispute.you/}</div></div>
													<div class="col_box col_right">
														<div class="message_bubble">{/plugins.paypal_dispute.note_ary.refund_with_return/}</div>
													</div>
												</div>
											<?php }?>
								<?php
										}
									}else{
										//消息对话
								?>
										<div class="row clean">
											<div class="col_box col_left"><div class="avatar<?=$v['PostedBy']==0?' isMe':'';?>">{/plugins.paypal_dispute.<?=$v['PostedBy']==1?'customer':'you';?>/}</div></div>
											<div class="col_box col_right">
												<div class="message_bubble">
													<p class="message_body"><?=$v['Content'];?></p>
													<p class="message_footer"><span class="timestamp"><?=date('Y-m-d H:i:s', $v['AccTime'])?></span></p>
												</div>
											</div>
										</div>
								<?php
									}
								}?>
							</div>
						</div>
					</div>
					<?php /***************************** 沟通记录 Start *****************************/?>
				</div>
			</div>
			<div class="right_container">
				<?php /***************************** 顾客联系 Start *****************************/?>
				<div class="global_container dispute_contact">
					<div class="big_title">
						<strong>{/plugins.paypal_dispute.contact/}</strong>
					</div>
					<div class="right_list">
						<div class="rows clean">
							<span data-type="name"><?=$order_row['ShippingFirstName'].' '.$order_row['ShippingLastName'];?></span>
						</div>
						<div class="rows clean">
							<span data-type="email"><?=$order_row['Email'];?></span>
						</div>
						<div class="rows clean">
							<span data-type="phone"><?=($order_row['ShippingCountryCode']?$order_row['ShippingCountryCode'].'-':'').$order_row['ShippingPhoneNumber'];?></span>
						</div>
					</div>
				</div>
				<?php /***************************** 顾客联系 End *****************************/?>
				<?php /***************************** 订单详情 Start *****************************/?>
				<div class="global_container order_details">
					<div class="big_title">
						<strong>{/orders.info.order_details/}</strong>
					</div>
					<div class="right_list">
						<div class="rows clean">
							<strong>{/orders.oid/}:</strong>
							<span><a href="./?m=orders&a=orders&d=view&OrderId=<?=$order_row['OrderId'];?>"><?=$order_row['OId'];?></a></span>
						</div>
						<div class="rows clean">
							<strong>{/orders.total_price/}:</strong>
							<span><?=$Symbol.sprintf('%01.2f', $total_price).' '.$order_row['ManageCurrency'];?></span>
						</div>
						<div class="rows clean">
							<strong>{/orders.orders_status/}:</strong>
							<span>{/orders.status.<?=$order_row['OrderStatus'];?>/}</span>
						</div>
					</div>
				</div>
				<?php /***************************** 订单详情 End *****************************/?>
			</div>
			<div class="clear"></div>
		</div>
	<?php }?>
</div>
<?php
if($c['manage']['do']=='view'){//争议详情页
?>
<div id="fixed_right">
	<div class="global_container fixed_claim">
		<div class="top_title">{/plugins.paypal_dispute.claim_title/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="claim_form">
			<div class="rows clean">
				<label>{/plugins.paypal_dispute.note_ary.to_claim/}</label>
				<div class="input">
					<textarea name='Content' class="box_textarea" notnull></textarea>
				</div>
			</div>
			<div class="blank30"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="dispute_id" value="<?=$dispute_row['DisputeID'];?>" />
			<input type="hidden" name="do_action" value="plugins.dispute_to_claim" />
		</form>
	</div>
	<div class="global_container fixed_evidence">
		<div class="top_title">{/plugins.paypal_dispute.provide_evidence/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="provide_evidence_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
			<div class="rows clean">
				<label>{/plugins.paypal_dispute.evidence_file/}</label>
				<div class="input upload_file">
					<input name="EvidenceFile" value="" type="text" class="box_input" id="file_path" size="50" maxlength="100" readonly notnull />
					<div class="blank6"></div>
					<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
					<div class="row fileupload-buttonbar">
						<span class="btn_file btn-success fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>{/global.file_upload/}</span>
							<input type="file" name="Filedata" multiple>
						</span>
						<div class="fileupload-progress fade"><div class="progress-extended">&nbsp;</div></div>
						<div class="clear"></div>
						<div class="photo_multi_img template-box files"></div>
						<div class="photo_multi_img" id="PicDetail"></div>
					</div>
					<script id="template-upload" type="text/x-tmpl">
					{% for (var i=0, file; file=o.files[i]; i++) { %}
						<div class="template-upload fade">
							<div class="clear"></div>
							<div class="items">
								<p class="name">{%=file.name%}</p>
								<strong class="error text-danger"></strong>
							</div>
							<div class="items">
								<p class="size">Processing...</p>
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
							</div>
							<div class="items">
								{% if (!i) { %}
									<button class="btn_file btn-warning cancel">
										<i class="glyphicon glyphicon-ban-circle"></i>
										<span>{/global.cancel/}</span>
									</button>
								{% } %}
							</div>
							<div class="clear"></div>
						</div>
					{% } %}
					</script>
					<script id="template-download" type="text/x-tmpl">
					{% for (var i=0, file; file=o.files[i]; i++) { %}
						{% if (file.thumbnailUrl) { %}
							<div class="pic template-download fade hide">
								<div>
									<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
									<a href="{%=file.url%}" class="zoom" target="_blank"></a>
									{% if (file.deleteUrl) { %}
										<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
										<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
									{% } %}
									<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
								</div>
								<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
							</div>
						{% } else { %}
							<div class="template-download fade hide">
								<div class="clear"></div>
								<div class="items">
									<p class="name">
										{% if (file.url) { %}
											<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
										{% } else { %}
											<span>{%=file.name%}</span>
										{% } %}
									</p>
									{% if (file.error) { %}
										<div><span class="label label-danger">Error</span> {%=file.error%}</div>
									{% } %}
								</div>
								<div class="items">
									<span class="size">{%=o.formatFileSize(file.size)%}</span>
								</div>
								<div class="items">
									{% if (file.deleteUrl) { %}
										<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
											<i class="glyphicon glyphicon-trash"></i>
											<span>{/global.del/}</span>
										</button>
										<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
									{% } else { %}
										<button class="btn_file btn-warning cancel">
											<i class="glyphicon glyphicon-ban-circle"></i>
											<span>{/global.cancel/}</span>
										</button>
									{% } %}
								</div>
								<div class="clear"></div>
							</div>
						{% } %}
					{% } %}
					</script>
				</div>
			</div>
			<div class="rows clean">
				<label>{/plugins.paypal_dispute.evidence_type/}</label>
				<div class="input">
					<div class="box_select">
						<select name="EvidenceType">
							<?php foreach($c['manage']['lang_pack']['plugins']['paypal_dispute']['evidence_type_ary'] as $k=>$v){?>
								<option value="<?=$k;?>"><?=$v;?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/plugins.paypal_dispute.evidence_note/}</label>
				<div class="input">
					<textarea name='Note' class="box_textarea" notnull></textarea>
				</div>
			</div>
			<div class="rows clean">
				<label>{/plugins.paypal_dispute.carrier_name/}</label>
				<div class="input">
					<div class="box_select">
						<select name="CarrierName">
							<?php foreach($c['manage']['lang_pack']['plugins']['paypal_dispute']['carrier_name_ary'] as $k=>$v){?>
								<option value="<?=$k;?>"><?=$v;?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="rows clean">
				<label>{/orders.shipping.track_no/}</label>
				<div class="input">
					<input type="text" class="box_input" name="TrackingNumber" value="<?=$order_row['TrackingNumber'];?>" notnull size="46" maxlength="255" />
				</div>
			</div>
			<div class="blank30"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="dispute_id" value="<?=$dispute_row['DisputeID'];?>" />
			<input type="hidden" name="do_action" value="plugins.dispute_provide_evidence" />
		</form>
	</div>
	<div class="global_container fixed_refund">
		<div class="top_title">{/plugins.paypal_dispute.<?=$DisputeStatus==1?'refund_title':'refund_to_title';?>/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="refund_form">
			<?php
			if($DisputeStatus==1){
				//提出解决争议的提议(协商阶段)
			?>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_note/}</label>
					<div class="input">
						<textarea name='Note' class="box_textarea" notnull></textarea>
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_price/}</label>
					<div class="input">
						<span class="unit_input" parent_null><b><?=$Symbol;?><div class="arrow"><em></em><i></i></div></b><input name="Price" value="<?=$dispute_row['Amount'];?>" type="text" class="box_input" size="10" maxlength="10" rel="amount" notnull parent_null="1" /></span>
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_type/}</label>
					<div class="input">
						<div class="box_select">
							<select name="OfferType">
								<?php foreach($c['manage']['lang_pack']['plugins']['paypal_dispute']['offer_type_ary'] as $k=>$v){?>
									<option value="<?=$k;?>"><?=$v;?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
			<?php
			}else{
				//接受索赔(仲裁阶段)
			?>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_to_note/}</label>
					<div class="input">
						<textarea name='Note' class="box_textarea" notnull></textarea>
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_to_price/}</label>
					<div class="input">
						<span class="unit_input" parent_null><b><?=$Symbol;?><div class="arrow"><em></em><i></i></div></b><input name="Price" value="<?=$dispute_row['Amount'];?>" type="text" class="box_input" size="10" maxlength="10" rel="amount" notnull parent_null="1" /></span>
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.paypal_dispute.refund_to_reason/}</label>
					<div class="input">
						<div class="box_select">
							<select name="ReasonType">
								<?php foreach($c['manage']['lang_pack']['plugins']['paypal_dispute']['reason_type_ary'] as $k=>$v){?>
									<option value="<?=$k;?>"><?=$v;?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
			<?php }?>
			<div class="blank30"></div>
			<div class="rows clean box_button">
				<div class="input input_button">
					<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="dispute_id" value="<?=$dispute_row['DisputeID'];?>" />
			<input type="hidden" name="do_action" value="plugins.dispute_refund" />
		</form>
	</div>
</div>
<?php }?>