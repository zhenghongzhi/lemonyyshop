<?php !isset($c) && exit();?>
<?php
if(!in_array('custom_comments', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

if($c['manage']['do']=='index'){
	//产品列表页面
	$prod_show=array();
	$cfg_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
	foreach($cfg_row as $v){
		$prod_show[$v['Variable']]=$v['Value'];
	}
	$used_row=str::json_data(htmlspecialchars_decode($prod_show['Config']), 'decode');
	
	//获取类别列表
	$cate_ary=str::str_code(db::get_all('products_category', '1', '*'));
	$category_ary=array();
	foreach((array)$cate_ary as $v){
		$category_ary[$v['CateId']]=$v;
	}
	$category_count=count($category_ary);
	unset($cate_ary);
	
	//获取所属供应商
	$business_row=db::get_all('business', 1, 'BId, Name', 'BId desc');
	$business_ary=array();
	foreach($business_row as $v){
		$business_ary[$v['BId']]=$v;
	}
	unset($business_row);
	
	//产品列表
	$Keyword=str::str_code($_GET['Keyword']);
	$CateId=(int)$_GET['CateId'];
	$Other=(int)$_GET['Other'];
	$Status=(int)$_GET['Status'];
	$Status==0 && $Status=1;//默认仅显示上架产品
	
	$where='1';//条件
	$page_count=50;//显示数量
	$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%')";
	if($CateId){
		$where.=" and (CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($CateId)."%') or CateId='{$CateId}')";
		$category_one=str::str_code(db::get_one('products_category', "CateId='$CateId'"));
		$UId=$category_one['UId'];
		$UId!='0,' && $TopCateId=category::get_top_CateId_by_UId($UId);
	}
	if($Status){
		switch($Status){
			case 1: $where.=$c['manage']['where']['products']; break;
			case 2: $where.=" and ((SoldStatus!=1 and Stock<=0) or (SoldOut=1 and IsSoldOut=0) or (SoldOut=1 and IsSoldOut=1 and (SStartTime>{$c['time']} or SEndTime<{$c['time']})))"; break;
		}
	}
	if($Other){
		switch($Other){
			case 1: $where.=' and IsNew=1'; break;
			case 2: $where.=' and IsHot=1'; break;
			case 3: $where.=' and IsBestDeals=1'; break;
			case 4: $where.=' and IsIndex=1'; break;
			case 5: $where.=' and IsSoldOut=1'; break;
			case 6: $where.=" and Stock<={$c['manage']['warning_stock']}"; break;
			case 7: $where.=" and Stock<MOQ && Stock<=0"; break;
			case 8: $where.=" and FacebookId!=''"; break;
		}
	}
	$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
}

$pro_menu_ary=array('pic_info', 'classify_info', 'basic_info', 'sales_info', 'attr_info', 'attr_price_info', 'tag_info', 'seo_info', 'freight_info', 'platform_info');
$top_id_name=($c['manage']['do']=='index'?'products':'products_inside');

echo ly200::load_static('/static/js/plugin/facebook/store.css', '/static/js/plugin/facebook/store.js');
?>
<div id="custom_comments" class="r_con_wrap">
	<script type="text/javascript">$(document).ready(function(){plugins_obj.review_init();});</script>
	<?php if($c['manage']['do']=='index'){?>
		<div class="inside_container">
			<h1>{/plugins.custom_comments.title/}</h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<!-- <li><a class="add" href="./?m=plugins&a=custom_comments&d=upload">批量导入</a></li> -->
					<!-- <li><a class="edit bat_close" href="javascript:;">{/global.edit_bat/}</a></li> -->
					<!-- <li><a class="save" href="javascript:;">{/global.save/}</a></li> -->
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', $CateId, 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="plugins" />
						<input type="hidden" name="a" value="custom_comments" />
					</form>
				</div>
			</div>
			<?php
			if($products_row[0]){ ?>
				<form id="list_bat_edit" class="global_form">
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="2%" class="bat_edit" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td>
								<td width="6%" nowrap="nowrap">{/products.picture/}</td>
								<td width="36%" nowrap="nowrap">{/products.name/} / {/products.classify/}</td>
								<td width="" nowrap="nowrap">{/plugins.custom_comments.sales/}</td>
								<td width="" nowrap="nowrap">{/plugins.custom_comments.fcount/}</td>
								<td width="" nowrap="nowrap">{/plugins.custom_comments.review_rating/}</td>
								<td width="" nowrap="nowrap">{/plugins.custom_comments.review_total_rating/}</td>
								<td width="12%" nowrap="nowrap">{/plugins.custom_comments.use_data/}</td>
								<td width="120" nowrap="nowrap">{/plugins.custom_comments.pro_data/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($products_row[0] as $v){
								$custom_row=db::get_one('products_development', "ProId='{$v['ProId']}'");
								$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
								$name=htmlspecialchars_decode($v['Name'.$c['manage']['web_lang']]);
								$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
								$Sales = $v['Sales'];
								$FavoriteCount = $v['FavoriteCount'];
								$DefaultReviewRating = ceil($v['Rating']);
								$DefaultReviewTotalRating = $v['TotalRating'];
								if ($custom_row['IsVirtual']) { //开启了虚拟数据就加入虚拟数据
									$Sales = $v['Sales']>$custom_row['Sales'] ? $v['Sales'] : $custom_row['Sales'];
									$FavoriteCount = $v['FavoriteCount']>$custom_row['FavoriteCount'] ? $v['FavoriteCount'] : $custom_row['FavoriteCount'];
									$DefaultReviewRating = ceil($v['Rating'])>(int)$custom_row['DefaultReviewRating'] ? ceil($v['Rating']) : (int)$custom_row['DefaultReviewRating'];
									$DefaultReviewTotalRating = $v['TotalRating']>$custom_row['DefaultReviewTotalRating'] ? $v['TotalRating'] : $custom_row['DefaultReviewTotalRating'];
								}
							?>
								<tr data-id="<?=$v['ProId'];?>">
									<td class="bat_edit" nowrap="nowrap"><?=html::btn_checkbox('select[]', $v['ProId']);?></td>
									<td class="img"><a href="<?=$url;?>" target="_blank" class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
									<td class="info">
										<a class="name" href="<?=$url;?>" target="_blank"><?=$name;?></a>
										<div class="classify color_888" cateid="<?=$v['CateId'];?>">
											<?php
											$UId=$category_ary[$v['CateId']]['UId'];
											if($UId){
												$key_ary=@explode(',',$UId);
												array_shift($key_ary);
												array_pop($key_ary);
												foreach((array)$key_ary as $k2=>$v2){
													echo $category_ary[$v2]['Category'.$c['manage']['web_lang']].' / ';
												}
											}
											echo $category_ary[$v['CateId']]['Category'.$c['manage']['web_lang']];
											?>
										</div>
									</td>
									<td nowrap="nowrap"><?=$Sales; ?></td>
									<td nowrap="nowrap"><?=$FavoriteCount; ?></td>
									<td nowrap="nowrap"><span class="star star_s<?=$DefaultReviewRating; ?>"></span></td>
									<td nowrap="nowrap"><?=$DefaultReviewTotalRating; ?></td>
									<td nowrap="nowrap"><?=$custom_row['IsVirtual'] ? '{/global.n_y_ary.1/}' : '{/global.n_y_ary.0/}'; ?></td>
									<td class="operation">
										<a class="development_btn" data-proid="<?=$v['ProId']; ?>" href="javascript:;">{/plugins.custom_comments.edit_data/}</a> <br>
										<a href="./?m=plugins&a=custom_comments&d=list&ProId=<?=$v['ProId'];?>">{/plugins.custom_comments.review_edit/}</a>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=str_replace('条', '个产品', html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page='));?>
					<input type="hidden" name="do_action" value="plugins.list_bat_edit" />
					<!-- <input type="submit" class="btn_submit" name="submit_button" value="{/global.save/}"> -->
				</form>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=products&a=products&d=edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='upload'){
	//评论导入
	$ProId = (int)$_GET['ProId'];
	echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js','/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
		?>
		<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
		<div class="center_container">
			<script type="text/javascript">$(document).ready(function(){plugins_obj.review_import_init()});</script>
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/plugins.custom_comments.review/}</span> 
				<span class="s_return">/ {/plugins.custom_comments.upload/}</span>
			</a>
			<form id="review_import_form" class="global_form" name="import_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				<div class="global_container">
					<div class="explode_box">
						<div class="box_item">
							<div class="tit"><em>1</em>{/global.down_table/}</div>
							<div class="desc"><?=str_replace('%url%', './?do_action=plugins.review_import_excel_download'.($ProId ? "&ProId={$ProId}" : ''), $c['manage']['lang_pack']['global']['click_here']); ?></div>
						</div>
						<div class="box_item">
							<div class="tit"><em>2</em>{/global.upload_table/}</div>
							<div class="desc">{/global.submit_table/}</div>
							<div class="input upload_file">
								<input type="text" class="box_input" id="excel_path" name="ExcelFile" value="" size="50" maxlength="100" readonly notnull />
								<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
								<div class="row fileupload-buttonbar">
									<span class="btn_file btn-success fileinput-button">
										<i class="glyphicon glyphicon-plus"></i>
										<span>{/global.file_upload/}</span>
										<input type="file" name="Filedata" multiple>
									</span>
									<div class="fileupload-progress fade"><div class="progress-extended"></div></div>
									<div class="clear"></div>
									<div class="photo_multi_img template-box files"></div>
									<div class="photo_multi_img" id="PicDetail"></div>
								</div>
								<script id="template-upload" type="text/x-tmpl">
								{% for (var i=0, file; file=o.files[i]; i++) { %}
									<div class="template-upload fade">
										<div class="clear"></div>
										<div class="items">
											<p class="name">{%=file.name%}</p>
											<strong class="error text-danger"></strong>
										</div>
										<div class="items">
											<p class="size">Processing...</p>
											<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
										</div>
										<div class="items">
											{% if (!i) { %}
												<button class="btn_file btn-warning cancel">
													<i class="glyphicon glyphicon-ban-circle"></i>
													<span>{/global.cancel/}</span>
												</button>
											{% } %}
										</div>
										<div class="clear"></div>
									</div>
								{% } %}
								</script>
								<script id="template-download" type="text/x-tmpl">
								{% for (var i=0, file; file=o.files[i]; i++) { %}
									{% if (file.thumbnailUrl) { %}
										<div class="pic template-download fade hide">
											<div>
												<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
												<a href="{%=file.url%}" class="zoom" target="_blank"></a>
												{% if (file.deleteUrl) { %}
													<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
													<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
												{% } %}
												<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
											</div>
											<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
										</div>
									{% } else { %}
										<div class="template-download fade hide">
											<div class="clear"></div>
											<div class="items">
												<p class="name">
													{% if (file.url) { %}
														<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
													{% } else { %}
														<span>{%=file.name%}</span>
													{% } %}
												</p>
												{% if (file.error) { %}
													<div><span class="label label-danger">Error</span> {%=file.error%}</div>
												{% } %}
											</div>
											<div class="items">
												<span class="size">{%=o.formatFileSize(file.size)%}</span>
											</div>
											<div class="items">
												{% if (file.deleteUrl) { %}
													<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
														<i class="glyphicon glyphicon-trash"></i>
														<span>{/global.del/}</span>
													</button>
													<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
												{% } else { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
								{% } %}
								</script>
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input">
									<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
									<input type="hidden" name="do_action" value="plugins.review_import" />
									<input type="hidden" name="ProId" value="<?=$ProId; ?>" />
									<input type="hidden" name="Number" value="0" />
								</div>
							</div>
						</div>
					</div>
					<div id="progress_container">
						<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
							<thead>
								<tr>
									<td width="6%" nowrap="nowrap">{/global.serial/}</td>
									<td width="32%" nowrap="nowrap">{/plugins.custom_comments.number/}</td>
									<td width="32%" nowrap="nowrap">{/plugins.custom_comments.content/}</td>
									<td width="30%" nowrap="nowrap">{/global.status/}</td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div id="progress_loading"></div>
					</div>
					<div class="rows clean">
						<label></label>
					</div>
				</div>
		    </form>
		</div>
	<?php
	}elseif($c['manage']['do']=='list'){ 
		if((int)$_GET['RId']){
			$RId=(int)$_GET['RId'];
			$review_row=str::str_code(db::get_one('products_review', "RId='$RId'"));
			!$review_row && js::location('./?m=products&a=review');
			(int)$review_row['IsRead']==0 && db::update('products_review', "RId='$RId'", array('IsRead'=>1));
			$products_row=db::get_one('products', "ProId='{$review_row['ProId']}'", '*');
			$rating_ary=explode(',', $review_row['Assess']);
			$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');
			?>
			<div class="center_container">	
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.products.review/}</span> 
					<span class="s_return">/ {/global.view/}</span>
				</a>
				<div class="global_container">
					<form id="review_edit_form" class="global_form">
						<div class="review_box">
							<div class="star_box">
								<span class="star star_b<?=$review_row['Rating'];?>"></span>
								<?php if($review_row['Agree']){ ?><span class="agree"><?=$review_row['Agree'];?></span><?php } ?>
							</div>
							<div class="msg"><?=$review_row['Content'];?></div>
							<?php 
							$count=0;
							for($i=0; $i<3; ++$i){
								if(!$review_row['PicPath_'.$i] || !is_file($c['root_path'].$review_row['PicPath_'.$i])) continue;
								$count++;
							}
							if($count){
							?>
								<div class="r_img">
									<?php
									for($i=0; $i<3; ++$i){
										if(!$review_row['PicPath_'.$i] || !is_file($c['root_path'].$review_row['PicPath_'.$i])) continue;
									?>
										<div class="list"><a href="<?=$review_row['PicPath_'.$i];?>" target="_blank"><img src="<?=$review_row['PicPath_'.$i];?>" alt=""></a></div>
									<?php }?>
									<div class="clear"></div>
								</div>
							<?php }?>
							<div class="info">
								<?php
								if($review_cfg['display']==1){
								?>
									<div class="fr hide">
										{/products.review.audit/} &nbsp;&nbsp;
										<div class="switchery review<?=$review_row['Audit']?' checked':'';?>" data-id="<?=$RId;?>">
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
									</div>
								<?php
								}
								echo $review_row['CustomerName'];
								echo $review_row['UserId']?'('.str::str_code(db::get_value('user', "UserId='{$review_row['UserId']}'", 'Email')).')':'';
								echo '<span>'.date('Y-m-d H:i:s', $review_row['AccTime']).'</span>';
								echo $review_row['Ip'] ? '<span>'.$review_row['Ip'].' 【'.ly200::ip($review_row['Ip']).'】</span>' : '';
								?>
							</div>
						</div>
						<?php
						$reply_row=str::str_code(db::get_all('products_review', "ProId='{$review_row['ProId']}' and ReId='{$review_row['RId']}'", '*', 'RId asc'));
						$reply_len=count($reply_row);
						if($reply_len){
							foreach($reply_row as $v){
						?>
								<div class="review_box">
									<a href="./?do_action=plugins.review_del&RId=<?=$v['RId'];?>" rel="del" class="del icon_delete_1 fr"></a>
									<div class="msg"><?=$v['Content'];?></div>
									<div class="info">
										<?php
										if(!($v['CustomerName']=='Manager' && !$v['UserId']) && $review_cfg['display']==1){
										?>
											<div class="fr">
												{/products.review.audit/} &nbsp;&nbsp;
												<div class="switchery<?=$v['Audit']?' checked':'';?>" data-id="<?=$v['RId'];?>">
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>
											</div>
										<?php
										}
										echo ($v['CustomerName']=='Manager' && !$v['UserId'])?'{/manage.manage.manager/}':$v['CustomerName'];
										echo ($v['CustomerName']!='Manager' && $v['UserId'])?'('.str::str_code(db::get_value('user', "UserId='{$v['UserId']}'", 'Email')).')':'';
										echo '<span>'.date('Y-m-d H:i:s', $v['AccTime']).'</span>';
										echo '<span>'.$v['Ip'].' 【'.ly200::ip($v['Ip']).'】</span>';
										?>
									</div>
								</div>
						<?php
							}
						}?>
						<div class="form_remark_log" parent_null>
							<div class="form_box">
								<div class="remark_left"><div><input type="text" class="box_input" name="ReviewComment" notnull parent_null="1" placeholder="{/global.please_enter/} ..."></div></div>
								<input type="button" class="btn_save btn_submit" value="{/products.review.reply/}">
							</div>
						</div>
						<input type="hidden" id="RId" name="RId" value="<?=$RId;?>" />
						<input type="hidden" name="ProId" value="<?=$review_row['ProId']; ?>" />
						<input type="hidden" name="do_action" value="plugins.review_reply" />
					</form>
				</div>
				<div class="clear"></div>
			</div>
		<?php }else{ 
			$ProId=(int)$_GET['ProId'];
			$ProId && $products_row=str::str_code(db::get_one('products', "ProId='{$ProId}'"));
			$Keyword=$_GET['Keyword'];
			$where="ProId='{$ProId}' and AppDivision=-1";//条件
			$page_count=30;//显示数量
			$Keyword && $where.=" and (CustomerName like '%{$Keyword}%' or Content like '%{$Keyword}%')";
			?>
			<a href="javascript:history.back(-1);" class="return_title list_title">
				<span class="return">{/plugins.custom_comments.title/}</span> 
				<span class="s_return">/ <?=str::str_echo(htmlspecialchars_decode($products_row['Name'.$c['manage']['web_lang']]), 100, 0, '...'); ?></span>
			</a>
			<div class="inside_table">
				<div class="list_menu">
					<ul class="list_menu_button">
						<li><a class="del" href="javascript:;">{/global.del_bat/}</a></li>
						<li><a class="add review_add_btn" data-proid="<?=$ProId; ?>" href="javascript:;">{/global.add/}</a></li>
						<li>
							<a class="more" href="javascript:;">{/global.more/}<em></em></a>
							<div class="more_menu drop_down">
								<a class="item" href="./?m=plugins&a=custom_comments&d=upload&ProId=<?=$ProId; ?>">{/plugins.custom_comments.upload_bat/}</a>
							</div>
						</li>
					</ul>
					<div class="search_form">
						<form method="get" action="?">
							<div class="k_input">
								<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
								<input type="button" value="" class="more" />
							</div>
							<input type="submit" class="search_btn" value="{/global.search/}" />
							<input type="hidden" name="m" value="plugins" />
							<input type="hidden" name="a" value="custom_comments" />
							<input type="hidden" name="d" value="list" />
							<input type="hidden" name="ProId" value="<?=$ProId; ?>" />
						</form>
					</div>
				</div>
				<?php
				$review_row=str::str_code(db::get_limit_page('products_review', $where, '*', 'RId desc', (int)$_GET['page'], $page_count));	
				if($review_row[0]){
				?>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="2%"><?=html::btn_checkbox('select_all');?></td>
								<td width="15%" nowrap="nowrap">{/products.review.customer_name/}</td>
								<td>{/products.review.content/}</td>
								<td width="10%" nowrap="nowrap">{/products.review.time/}</td>
								<td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($review_row[0] as $v){
							?>
								<tr>
									<td><?=html::btn_checkbox('select', $v['RId']);?></td>
									<td><?=$v['CustomerName'];?></td>
									<td><span class="star star_s<?=$v['Rating'];?>"></span><br> <?=str::str_code($v['Content'], 'htmlspecialchars_decode');?></td>
									<td><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
									<td nowrap="nowrap" class="operation side_by_side">
										<a href="./?m=plugins&a=custom_comments&d=list&RId=<?=$v['RId'];?>">{/global.view/}</a>
										<dl>
											<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
											<dd class="drop_down"><a class="del item" href="./?do_action=plugins.review_del&RId=<?=$v['RId'];?>" rel="del">{/global.del/}</a></dd>
										</dl>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=html::turn_page($review_row[1], $review_row[2], $review_row[3], '?'.ly200::query_string('page').'&page=');?>
				<?php
				}else{//没有数据
					echo html::no_table_data(0);
				}?>
			</div>

		<?php } ?>
	<?php } ?>
</div>
<div id="fixed_right">
	<?=ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js'); ?>
	<div class="global_container review_edit_box">
		<div class="top_title">{/plugins.custom_comments.add/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="review_reply_form">
			<div class="rows clean">
				<label>{/plugins.custom_comments.content/}</label>
				<div class="input">
					<textarea name="Content" notnull class="box_textarea"></textarea>
				</div>
			</div>
			<div class="rows clean">
				<label>{/global.time/}</label>
				<div class="input">
					<input name="AccTime" value="<?=date('Y-m-d H:i:00', $c['time']);?>" type="text" class="box_input input_time" size="42" readonly>
				</div>
			</div>
			<div class="rows clean">
				<label>{/plugins.custom_comments.star/}</label>
				<div class="input">
					<span class="review_star">
						<?php for ($i=0;$i<5;$i++) { ?>
							<span class="star_1"></span>
						<?php } ?>
					</span>					
				</div>
			</div>
			<div class="rows clean box_button">
				<label></label>
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="ProId" value="" />
			<input type="hidden" name="Rating" value="5" />
			<input type="hidden" name="do_action" value="plugins.review_edit" />
		</form>
	</div>
	<div class="global_container products_development">
		<div class="top_title">{/plugins.custom_comments.edit_data/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="products_development_form">
			<div class="rows clean">
				<label>{/plugins.custom_comments.open/}</label>
				<div class="input">
					<div class="blank6"></div>
					<div class="switchery">
						<input type="checkbox" name="IsVirtual" value="1" />
						<div class="switchery_toggler"></div>
						<div class="switchery_inner">
							<div class="switchery_state_on"></div>
							<div class="switchery_state_off"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="virtual_box">
				<div class="rows clean">
					<label>{/plugins.custom_comments.sales/}</label>
					<div class="input">
						<input type="text" class="box_input" name="Sales" value="" size="10" maxlength="255" rel="amount" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.custom_comments.fcount/}</label>
					<div class="input">
						<input type="text" class="box_input" name="FavoriteCount" value="" size="10" maxlength="255" rel="amount" />
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.custom_comments.review_rating/}</label>
					<div class="input">
						<span class="review_star">
							<?php for ($i=0;$i<5;$i++) { ?>
								<span class="star_1"></span>
							<?php } ?>
						</span>
					</div>
				</div>
				<div class="rows clean">
					<label>{/plugins.custom_comments.review_total_rating/}</label>
					<div class="input">
						<input type="text" class="box_input" name="DefaultReviewTotalRating" value="" size="10" maxlength="255" rel="amount" />
					</div>
				</div>
			</div>
			<div class="rows clean box_button">
				<label></label>
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="ProId" value="" />
			<input type="hidden" name="DefaultReviewRating" value="5" />
			<input type="hidden" name="do_action" value="plugins.products_development" />
		</form>
	</div>
</div>