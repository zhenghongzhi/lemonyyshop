<?php !isset($c) && exit();?>
<?php
if(!in_array('gallery', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}
if(!db::get_row_count('config','Variable="gallery_index_style"')){
	db::insert('config', array(
		'GroupId'	=> 'app',
		'Variable'	=> 'gallery_index_style',
		'Value'		=> 'style_1'
		));
}
$lang=$c['manage']['web_lang'];
$app_row=db::get_one('plugins','ClassName="gallery"');
$app_name=$name=str::json_data(htmlspecialchars_decode(str_replace("'", '"', $app_row['Name'])),'decode');
$top_id_name=($c['manage']['do']=='index'?'gallery':'gallery_inside');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php
	if($c['manage']['do']=='edit'){
		$AppGId=(int)$_GET['AppGId'];
		$AppGId && $app_gallery_row=str::str_code(db::get_one('app_gallery', "AppGId='$AppGId'"));
		?>
		<script type="text/javascript">$(document).ready(function(){plugins_obj.gallery_edit_init()});</script>
		<div class="center_container_1000">
			<div class="global_container">
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return"><?=$app_name[$manage_lang];?></span> 
					<span class="s_return">/ <?=$AppGId?'{/global.edit/}':'{/global.add/}';?></span>
				</a>
				<form id="gallery_edit_form" class="global_form" data-url="./?m=<?=$c['manage']['module']; ?>&a=gallery">
					<div class="rows clean">
						<label>{/global.pic/}</label>
						<div class="input">
							<?=manage::multi_img('PicDetail', 'PicPath', $app_gallery_row['PicPath']); ?>
						</div>
					</div>
					<div class="rows clean proary_rows">
						<label>{/plugins.app.proary/}</label>
						<div class="input clean">
							<?php
							$SelectAry=array();
							$row=db::get_limit('products', '1', "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc', 0, 20);
							foreach($row as $k=>$v){
								$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
								$SelectAry[$v['ProId']]=array('Name'=>$v['Name'.$lang], 'Type'=>'products', 'Table'=>'', 'Icon'=>'<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
							}
							$ValueAry=array();
							if($app_gallery_row['ProId']){
								$where_ary=array();
								$ProAry=explode('|', trim($app_gallery_row['ProId'], '|'));
								foreach($ProAry as $v){ $where_ary[]=$v; }
								$where_ary=@implode(',', $where_ary);
								if($where_ary){
									$products_row=db::get_all('products', 'ProId in('.$where_ary.')', "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc');
									foreach($products_row as $v){
										$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
										$ValueAry=array('Input'=>$v['Name'.$lang], 'Select'=>$v['ProId'], 'Type'=>'products', 'Icon'=>'<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
									}
								}
							}
							echo manage::box_drop_double('ProId', 'ProIdValue', $SelectAry, $ValueAry, 0, '', 0, 1);
							?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/partner.myorder/}</label>
						<div class="input">
							<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $app_gallery_row['MyOrder'], '', '', '', 'class="box_input"');?></div>
						</div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=<?=$c['manage']['module']; ?>&a=gallery" class=""><input type="button" class="btn_global btn_cancel" value="{/global.cancel/}"></a>
						</div>
					</div>
					<input type="hidden" id="AppGId" name="AppGId" value="<?=$AppGId;?>" />
					<input type="hidden" name="do_action" value="plugins.gallery_edit" />
				</form>
			</div>
		</div>
	<?php
	}else{
		?>	
		<script type="text/javascript">$(document).ready(function(){plugins_obj.gallery_init()});</script>
        <div class="center_container_1000">
            <div class="inside_container">
                <h1><?=$app_name[$manage_lang];?></h1>
            </div>
            <div class="inside_table">
                <div class="list_menu">
                    <ul class="list_menu_button">
                        <li><a class="add" href="./?m=<?=$c['manage']['module']; ?>&a=gallery&d=edit">{/global.add/}</a></li>
                        <li><a class="del" href="javascript:;">{/global.del_bat/}</a></li>
                    </ul>
                </div>
                <?php
                $app_gallery_row=str::str_code(db::get_limit_page('app_gallery', '1', '*', $c['my_order'].'AppGId desc', (int)$_GET['page'], 20));
                if($app_gallery_row[0]){
                ?>
                    <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                        <thead>
                            <tr>
                                <td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td>
                                <td width="4%" nowrap="nowrap">{/global.serial/}</td>
                                <td width="70%" nowrap="nowrap">{/global.pic/}</td>
                                <td width="20%" nowrap="nowrap">{/global.time/}</td>
                                <td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($app_gallery_row[0] as $k => $v){
                            ?>
                                <tr pid="<?=$v['AppGId'];?>">
                                    <td nowrap="nowrap"><?=html::btn_checkbox('select', $v['AppGId']);?></td>
                                    <td nowrap="nowrap"><?=$k+1; ?></td>
                                    <td nowrap="nowrap" class="img"><a href="<?=$v['PicPath'];?>" title="<?=$v['Name'.$c['manage']['web_lang']];?>" target="_blank"><img class="photo" src="<?=$v['PicPath'];?>" alt="<?=$v['Name'.$c['manage']['web_lang']];?>" align="absmiddle" /></a></td>
                                    <td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
                                    <td nowrap="nowrap" class="operation side_by_side">
                                        <a class="edit" href="./?m=<?=$c['manage']['module']; ?>&a=gallery&d=edit&AppGId=<?=$v['AppGId'];?>">{/global.edit/}</a>
                                        <dl>
                                            <dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
                                            <dd class="drop_down">
                                                <a class="del item" href="./?do_action=plugins.gallery_del&AppGId=<?=$v['AppGId'];?>" rel="del">{/global.del/}</a>
                                            </dd>
                                        </dl>
                                    </td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    <?=html::turn_page($app_gallery_row[1], $app_gallery_row[2], $app_gallery_row[3], '?'.ly200::query_string('page').'&page=');?>
                <?php
                }else{//没有数据
                    echo html::no_table_data(0);
                }?>
            </div>
        </div>
	<?php }?>
</div>