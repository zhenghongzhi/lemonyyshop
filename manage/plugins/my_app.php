<?php !isset($c) && exit();?>
<?php
manage::check_permit('plugins', 1, array('a'=>'my_app'));//检查权限

if ($c['manage']['do'] == 'index') {
	$app_ary = array();
    $app_used_ary = array();
	$where = 'Category="app"';
	if ($_GET['ClassName']) {
		$ClassName = trim($_GET['ClassName']);
		$where .= " and ClassName='{$ClassName}'";
	}
	$app_row = db::get_all('plugins', $where);
	foreach ($app_row as $k => $v) {
		$app_ary[$v['ClassName']] = $v;
		$v['IsInstall'] == 1 && $app_used_ary[] = $v['ClassName'];
	}
	$app_category_save_count = 0;
	$app_category_save_ary = array();
    $all_app_category_ary = array();
	foreach ($c['manage']['plugins']['category'] as $k => $v) {
		foreach ($v as $k2 => $v2) {
			if (in_array($v2, $app_used_ary) && in_array($v2, (array)$c['manage']['plugins']['Used'])) {
                //已安装
				$app_category_save_ary[$k] += 1;
				$app_category_save_count++;
                if (!in_array($v2, (array)$all_app_category_ary)) {
                	$all_app_category_ary[] = $v2;
                }
			}
		}
	}
    //整合数据
    $app_category_ary = array();
    $app_category_ary['All'] = $all_app_category_ary; //全部
    foreach ($c['manage']['plugins']['category'] as $k => $v) {
        if ($app_category_save_ary[$k] == 0) continue; //没有应用内容
        $app_category_ary[$k] = $v;
    }
	//URL链接
	$app_item_url_ary = array();
	foreach ($c['manage']['plugins']['module'] as $k => $v) {
		foreach ($v as $k2 => $v2) {
			$app_item_url_ary[$v2] = $k;
		}
	}
	//第三方APP
	$oauth_ary = array('facebook_store', 'facebook_pixel', 'facebook_login', 'google_login', 'paypal_login', 'vk_login', 'twitter_login', 'facebook_messenger', 'facebook_ads_extension', 'massive_email','google_pixel', 'paypal_marketing_solution', 'dhl_account_info', 'asiafly');//, 'instagram_login'
	//开关APP
	$switch_ary = array('block_access', 'advanced_setup', 'batch_edit', 'product_inbox');
	//单独设置APP
	$once_ary = array('overseas', 'freight', 'cdn', 'pdf', 'upload', 'wholesale', 'platform', 'screening', 'intelligent_translation');
	echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
	
	$InChina = 1;
	$ChinaProvince = "中国/北京/浙江/天津/安徽/上海/福建/重庆/江西/山东/河南/内蒙古/湖北/新疆维吾尔/湖南/宁夏回族/广东/西藏/海南/广西壮族/四川/河北/贵州/山西/云南/辽宁/陕西/吉林/甘肃/黑龙江/青海/江苏";
	$IsTry = substr_count(db::get_value('config', 'GroupId="global" and Variable="tryDomain"', 'Value'), $_SERVER['HTTP_X_FROM']) ? 1 : 0;
	$IpArea = ly200::ip(ly200::get_ip($IsTry));
	if (substr_count($ChinaProvince, substr($IpArea, 0, 6)) == 0) {
		$InChina = 0;
		echo '<script type="text/javascript" src="//sync.ly200.com/plugin/facebook/ads_extension.js"></script>';
	}
?>
	<div id="app" class="r_con_wrap <?=$app_category_save_count?'no_app':''; ?>" data-in-china="<?=$InChina;?>">
		<script type="text/javascript">
        $(document).ready(function(){
            plugins_obj.plugins_my_app_init();
        });
        </script>
		<?php if($app_category_save_count){ ?>
			<div class="inside_container">
				<h1>{/module.plugins.my_app/}</h1>
				<ul class="inside_menu unusual">
                    <li><a href="javascript:;" class="current">{/global.all_to/}</a></li>
	            	<?php 
	            	$j=1;
	            	foreach ($c['manage']['plugins']['category'] as $k => $v) { 
	            	    if ($app_category_save_ary[$k] == 0) continue;//没有应用内容
	            	?>
						<li name="{/plugins.global.category.<?=$k;?>/}"><a href="javascript:;">{/plugins.global.category.<?=$k;?>/}</a></li>
	                <?php
                        ++$j;
                    }?>
				</ul>
				<?php if($_GET['from']=='facebook'){ ?>
					<script>
						$(function(){
							$('.unusual li[name=Facebook]').click();
						});
					</script>
				<?php } ?>
			</div>
		<?php }else{ ?>
			<div class="no_plugins">
				<div class="title">{/plugins.global.store_select/}</div>
				<div class="subtitle">{/plugins.global.store_msg/}</div>
				<a href="./?m=plugins&a=app" class="go_app_store">{/plugins.global.go_store/}</a>
			</div>
		<?php } ?>
		<div class="my_app_list app_main">
			<div class="app_container clean">
				<?php
				$j = 1;
				foreach ($app_category_ary as $k => $v) {
				?>
					<ul class="app_category_list"<?=$j==1 ? ' style="display:block;"' : '';?>>
						<?php
						foreach ($v as $k2 => $v2) {
							if ($app_ary[$v2]['IsInstall'] == 0 || !in_array($v2, (array)$c['manage']['plugins']['Used'])) continue;
							if ($app_item_url_ary[$v2]) {
								$s_url = $v2;
								if (@array_key_exists($v2, (array)$c['manage']['sync_ary'])) {
									$s_url = "sync&d={$v2}";
								}
								$url = "./?m={$app_item_url_ary[$v2]}&a=" . $s_url;
							}else{
								$url = 'javascript:;';
							}
							$IsOauth = in_array($v2, $oauth_ary) ? 1 : 0;
							$IsSwitch = in_array($v2, $switch_ary) ? 1 : 0;
							$IsReview = ($v2 == 'review' ? 1 : 0);
						?>
							<li class="app_item icon_item_<?=$v2;?>" data-type="<?=$v2;?>">
								<div class="btn_box view">
									<div class="box_used">
										<div class="switchery<?=$app_ary[$v2]['IsUsed']?' checked':'';?>">
											<input type="checkbox" value="1" name="IsUsed[]"<?=$app_ary[$v2]['IsUsed']?' checked':'';?> />
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
									</div>
									<?php
									if (!in_array($v2, $once_ary)) {
										if ($IsReview) {
											echo '<a href="javascript:;" class="btn_view btn_review" data-page="1">{/plugins.global.import/}</a>';
										} else {
											echo '<a href="'.($IsOauth ? 'javascript:;' : $url).'" class="btn_view'.($IsOauth ? ' btn_oauth' : '').($IsSwitch ? ' btn_switch' : '').'">{/plugins.global.enter/}</a>';
										}
									}?>
								</div>
								<div class="box_view info">
									<?php 
										$name = str::json_data($app_ary[$v2]['Name'], 'de_code');
										$manage_lang = ($c['manage']['config']['ManageLanguage'] == 'zh-cn' ? 'cn' : $c['manage']['config']['ManageLanguage']);
										$name = $name[$manage_lang];
									?>
									<strong><?=$name; ?></strong>
									<p>{/plugins.briefdescript.<?=$v2;?>/}</p>
									<?php if ($v2 == 'review'){ ?>
										<div class="import_progress">
											<div class="progress_bar"><div class="progress"></div></div>
											<div class="progress_percent"><span>0</span> / 100</div>
										</div>
									<?php }?>
								</div>
							</li>
						<?php }?>
					</ul>
				<?php
                    ++$j;
                }?>
			</div>
		</div>
		<div class="pop_form pop_app_oauth">
			<form id="app_account_info_form" class="global_form">
				<div class="t"><h1><span></span></h1><h2>×</h2></div>
				<div class="success_bodyer clean">
					<div class="infomation">
						<div class="data_list"></div>
						<div class="content"></div>
						<div class="button">
							<input type="button" class="btn_global btn_save" value="{/global.save/}" />
							<input type="button" class="btn_global btn_know" value="{/plugins.authorization.got_it/}" />
							<input type="button" class="btn_global btn_authorized" value="{/plugins.facebook_store.authorized/}" />
							<input type="button" class="btn_global btn_set" value="{/plugins.dhl_account_info.set/}" />
							<a href="" target="_blank" class="help">{/plugins.authorization.help/}</a>
						</div>
					</div>
					<div class="picture"><img src="" /></div>
				</div>
				<input type="hidden" name="ClassName" value="" />
				<input type="hidden" name="do_action" value="plugins.app_account_info_edit" />
			</form>
		</div>
		<div class="pop_form pop_app_switch">
			<form id="app_switch_form" class="global_form">
				<div class="t"><h1><span></span></h1><h2>×</h2></div>
				<div class="switch_bodyer clean">
					<div class="infomation clean"></div>
					<div class="button">
						<input type="button" class="btn_global btn_save" value="{/global.save/}" />
					</div>
				</div>
				<input type="hidden" name="ClassName" value="" />
				<input type="hidden" name="do_action" value="plugins.app_switch_edit" />
			</form>
		</div>
	</div>
<?php }else{
	include("app/{$c['manage']['do']}.php");
} ?>