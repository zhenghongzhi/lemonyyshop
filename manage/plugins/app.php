<?php !isset($c) && exit();?>
<?php
/*******************************************************************************************************************************************/
$pub_app_item_ary=array('seckill', 'tuan', 'package', 'promotion', 'holiday', 'distribution', 'gallery', 'facebook_store', 'facebook_ads_extension', 'googlefeed', 'review', 'import_delivery', 'overseas', 'freight', 'facebook_pixel', 'aliexpress', 'amazon', 'facebook_login', 'google_login', 'paypal_login', 'vk_login', 'twitter_login', 'blog', 'business', 'product_inbox', 'block_access', 'pdf', 'advanced_setup', 'upload', 'wholesale', 'platform', 'screening', 'wish', 'shopify', 'facebook_messenger', 'intelligent_translation', 'massive_email','swap_chain','google_verification','google_pixel', 'paypal_marketing_solution', 'paypal_dispute', 'custom_comments'); //, 'instagram_login' ,'batch_edit', 'dhl_account_open', 'dhl_account_info', 'asiafly'
/*******************************************************************************************************************************************/
if($c['FunVersion']>=10){
	include('cdx_app.php');
}
/*******************************************************************************************************************************************/

$manage_lang=$c['manage']['config']['ManageLanguage']=='zh-cn'?'cn':$c['manage']['config']['ManageLanguage'];
$Keyword=trim($_GET['Keyword']);
if($c['manage']['action']=='app'){
	manage::check_permit('plugins', 1, array('a'=>'app'));//检查权限
	$permit_ary=array(
		'edit'	=>	manage::check_permit('plugins', 0, array('a'=>'app', 'd'=>'edit')),
	);
	$app_ary=$app_category_ary=array();
	$app_row=db::get_all('plugins', 'Category="app"');
	foreach($app_row as $k=>$v){
		$app_ary[$v['ClassName']]=$v;
	}
	$app_item_ary=$pub_app_item_ary;
	if($c['FunVersion']>=10){
		//创店应用排序重组 start
		$free_app_ary=$pay_app_ary=array();
		$free_app=array('shopify','amazon','wish', 'aliexpress','facebook_pixel');
		//区分免费和付费插件
		foreach((array)$app_item_ary as $k=>$v){
			if(in_array($v, $free_app)){  //免费版插件
				$free_app_ary[]=$v;
			}else{  //付费版插件
				$pay_app_ary[]=$v;
			}
		}
		//已安装排在前面-免费版
		foreach((array)$free_app_ary as $k => $v){
			if($app_ary[$v]['IsInstall']==1){
				unset($free_app_ary[$k]);
				$free_app_ary[]=$v;
			}
		}
		//已安装排在前面-付费版
		foreach((array)$pay_app_ary as $k => $v){
			if($app_ary[$v]['IsInstall']==1){
				unset($pay_app_ary[$k]);
				$pay_app_ary[]=$v;
			}
		}
		$app_item_ary=array_merge($free_app_ary, $pay_app_ary);	
		//创店应用排序重组 end
	}else{
		//重新排序，未安装的应用靠前显示
		foreach($app_item_ary as $k=>$v){
			if($app_ary[$v]['IsInstall']==1){//已安装
				unset($app_item_ary[$k]);
				$app_item_ary[]=$v;
			}
		}
	}
	
	$app_category_ary['All'] = $app_item_ary;
	
	$InChina=1;
	$ChinaProvince="中国/北京/浙江/天津/安徽/上海/福建/重庆/江西/山东/河南/内蒙古/湖北/新疆维吾尔/湖南/宁夏回族/广东/西藏/海南/广西壮族/四川/河北/贵州/山西/云南/辽宁/陕西/吉林/甘肃/黑龙江/青海/江苏";
	$IpArea=ly200::ip(ly200::get_ip());
	if(substr_count($ChinaProvince, substr($IpArea, 0, 6))==0){
		$InChina=0;
		echo '<script type="text/javascript" src="//sync.ly200.com/plugin/facebook/ads_extension.js"></script>';
	}
	if($_GET['ClassName']){
		$InChina=0;
		$app_item_ary=array(trim($_GET['ClassName']));
	}
?>
	<div id="app" class="r_con_wrap" data-in-china="<?=$InChina;?>">
		<script type="text/javascript">
		$(document).ready(function(){
			plugins_obj.plugins_init();
			<?php if($Keyword){
				echo '$("#form_app_search .search_btn").click();';//点击搜索按钮
			}?>
		});
		</script>
		<div class="app_header">
			<div class="app_container clean">
				<h3>{/module.plugins.app/}<a href="javascript:;" class="btn_app_menu"><i></i></a></h3>
				<div class="app_search">
					<form class="form_search" id="form_app_search">
						<input type="text" name="Keyword" value="<?=$Keyword;?>" class="form_input" size="15" autocomplete="off" placeholder="{/plugins.global.search_tips/} ..." />
						<input type="submit" class="search_btn" value="{/global.search/}" />
					</form>
				</div>
                <?php if($c['FunVersion']<10){?>
				<div class="category_menu">
					<a href="javascript:;" class="current">{/global.all_to/}</a>
					<?php foreach ((array)$c['manage']['plugins']['category'] as $k => $v) { 
						$app_category_ary[$k]=$v;
						?>
						<a href="javascript:;">{/plugins.global.category.<?=$k;?>/}</a>
					<?php } ?>
				</div>
                <?php }?>
			</div>
		</div>
		<div class="app_main">
			<div class="app_container clean">
				<?php
				foreach ((array)$app_category_ary as $key => $val) {
					if($c['FunVersion']>=10 && $key!='All') continue;
				?>
					<div class="category_box" <?=$key=='All' ? 'style="display:block;"' : ''; ?>>
                    	<?php if($c['FunVersion']>=10){?><div class="app_cate_title">{/global.free_ver/}</div><?php }?>
						<?php	
						foreach((array)$val as $k=>$v){
							$NameAry=str::json_data($app_ary[$v]['Name'], 'decode');
							$Name=$DataName=$NameAry[$manage_lang];
							// $Name=str_replace(' ', '<br />', $Name);
							$DataName=='Google Shopping Feed' && $DataName='Google Feed';
							$Install=(!in_array($v, $c['un_used_ary'][$c['FunVersion']]));//公共应用 or 收费应用
							?>
                            <?php if($c['FunVersion']>=10 && $k==count($free_app)){?><div class="app_cate_title padding_top">{/global.paid_ver/}</div><?php }?>
							<div class="app_item icon_item_<?=$v;?> app_item_<?=$v;?><?=$app_ary[$v]['IsInstall']==0?' app_uninstall':'';?>" data-name="<?=strtolower($DataName);?>" data-tips="<?=$c['manage']['lang_pack']['plugins']['authorization']['plugins_tips'][$v]?>" data-type="<?=$v;?>">
								<?php if($Install){?>
									<div class="btn_box">
										<a href="javascript:;" class="btn_install <?=($c['FunVersion']==10 && !in_array($v,$free_app))?$c['manage']['cdx_limit']:'';?>" style="display:<?=$app_ary[$v]['IsInstall']==0?'block':'none';?>;">{/plugins.global.install/}</a>
										<span class="installed" style="display:<?=$app_ary[$v]['IsInstall']==1?'block':'none';?>;">{/plugins.global.installed/}</span>
									</div>
								<?php }?>
								<div class="box_view">
									<strong><?=$Name;?>
									</strong>
									<p>{/plugins.briefdescript.<?=$v;?>/}</p>
								</div>
							</div>
						<?php }?>
						<div class="app_item app_item_next">
							<strong class="color_000">{/plugins.global.next_title/}</strong>
							<span class="color_555">{/plugins.global.next_txt/}</span>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="pop_form pop_app_menu">
			<form>
				<div class="t"><h1><span></span>{/plugins.global.app_category/}</h1><h2>×</h2></div>
				<div class="r_con_form">
					<div class="app_category_list clean">
						<div id="app_category_list_box">
							<?php foreach((array)$c['manage']['plugins']['category'] as $k=>$v){ ?>
								<div class="item">
									<dl>
										<dt class="color_000">{/plugins.global.category.<?=$k; ?>/}</dt>
										<?php
										foreach((array)$v as $k1=>$v1){
										?>
											<dd><a href="javascript:;" data-value="{/plugins.global.items.<?=$v1;?>/}" class="color_555">{/plugins.global.items.<?=$v1;?>/}</a></dd>
										<?php }?>
									</dl>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="pop_form pop_app_install">
			<form id="app_install_form">
				<div class="t"><h2>×</h2></div>
				<div class="install_header">
					<h3>{/plugins.authorization.title/}</h3>
				</div>
				<div class="install_bodyer">
					<div class="title"><strong></strong> {/plugins.authorization.store_info/}</div>
					<div class="infomation">{/plugins.authorization.tips/}</div>
					<div class="button"><input type="button" name="" value="{/plugins.authorization.confirm/}" class="btn_global btn_submit" /></div>
				</div>
				<input type="hidden" name="ClassName" value="" />
				<input type="hidden" name="do_action" value="plugins.app_install" />
			</form>
		</div>
		<div class="pop_form pop_app_oauth">
			<form id="app_account_info_form" class="global_form">
				<div class="t success"><h1><span>{/plugins.authorization.follows/}</span></h1><h2>×</h2></div>
				<div class="success_bodyer clean">
					<div class="infomation">
						<div class="data_list"></div>
						<div class="content"></div>
						<div class="button">
							<input type="button" class="btn_global btn_save" value="{/global.save/}" />
							<input type="button" class="btn_global btn_enter" value="{/plugins.enter/}" />
							<input type="button" class="btn_global btn_know" value="{/plugins.authorization.got_it/}" />
							<input type="button" class="btn_global btn_authorized" value="{/plugins.facebook_store.authorized/}" data-url="" />
							<a href="" target="_blank" class="help">{/plugins.authorization.help/}</a>
						</div>
					</div>
					<div class="picture"><img src="" /></div>
				</div>
				<input type="hidden" name="ClassName" value="" />
				<input type="hidden" name="do_action" value="plugins.app_account_info_edit" />
			</form>
		</div>
	</div>
<?php }else{
	include("app/{$c['manage']['action']}.php");
} ?>