<?php !isset($c) && exit();?>
<?php
$pub_set_themes='<div class="inline-block">';
	$pub_set_themes.='<span class="title">{/account.set_themes/}</span>';
$pub_set_themes.='</div>';	

$pub_add_pro='<div class="inline-block">';	
	$pub_add_pro.='<span class="title">{/account.add_pro/}</span>';
	$pub_add_pro.='<div class="sub_title">（上架您的产品，让全世界的顾客看到）</div>';
$pub_add_pro.='</div>';		

$pub_domain_binding='<div class="inline-block">';
	$pub_domain_binding.='<span class="title">{/module.set.domain_binding/}</span>';
	$pub_domain_binding.='<div class="sub_title">（把您注册好的域名提交绑定，让您的客户通过您的专属域名访问您的店铺）</div>';
$pub_domain_binding.='</div>';	
						
$pub_set_shipping='<div class="inline-block">';
	$pub_set_shipping.='<span class="title">{/account.set_shipping/}</span>';
	$pub_set_shipping.='<div class="sub_title">（添加设置您合作的物流、快递方式，并可根据它们的计算方式设置运费计算公式）</div>';
$pub_set_shipping.='</div>';

$pub_set_payment='<div class="inline-block">';
	$pub_set_payment.='<span class="title">{/account.set_payment/}</span>';
	$pub_set_payment.='<div class="sub_title">（已接入palpal、stripe等数十种外贸常用信用卡收款平台，注册账号并在后台填写账号信息即可实现收款）</div>';
$pub_set_payment.='</div>';

$pub_isExitGuide='<a href="./?isExitGuide=1" class="fr">{/account.exit_guide/}</a>';
?>