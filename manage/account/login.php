<?php !isset($c) && exit();?>
<script type="text/javascript">$(document).ready(function(){account_obj.login_init();});</script>
<?php
/*******************************************************************************************************************************************/
$pub_login_logo='<h1><img src="/static/manage/images/account/login_logo.png" /></h1>';
$pub_welcome='<h2>{/account.welcome/}</h2>';
/*******************************************************************************************************************************************/
if($c['FunVersion']>=10){
	include('cdx_login.php');
}
/*******************************************************************************************************************************************/
?>
<style type="text/css">body,html{ width:100%; height:100%;background:url(/static/manage/images/account/login_bg.jpg) no-repeat center 0; background-size:cover; overflow:hidden;}</style>
<div id="login">
	<div class="lbar">
    	<?=$pub_login_logo;?>
        <p>{/account.login_title/}</p>
    </div>
    <div class="rbar">
    	<form>
        	<?=$pub_welcome;?>
            <div class="main">
                <div class="input username"><span></span><input name="UserName" id="UserName" type="text" maxlength="50" value="" autocomplete="off" placeholder="{/account.username/}"></div>
                <div class="input password"><span></span><input name="Password" id="Password" type="password" maxlength="50" value="" autocomplete="off" placeholder="{/account.password/}"></div>
               	<div class="tips"></div>
                <input type="submit" class="submit" value="{/account.login_btn/}">
            </div>
            <input type="hidden" name="do_action" value="account.login">
        </form>
    </div>
    <div class="clear"></div>
</div>