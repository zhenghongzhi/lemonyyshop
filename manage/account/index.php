<?php !isset($c) && exit();?>
<?php
$days_ary=array(0, -1, -7, -30);

$country_row=str::str_code(db::get_all('country', 1, 'CId, CountryData, Acronym', 'CId asc'));//国家简写
echo ly200::load_static('/static/js/plugin/jqvmap/jquery.vmap.js', '/static/js/plugin/jqvmap/maps/jquery.vmap.world.js', '/static/js/plugin/jqvmap/jqvmap.css', '/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js');
if(is_file($c['root_path'].'/manage/account/upgrade.php')) @include('upgrade.php');	//后台更新文件用
/*******************************************************************************************************************************************/
$pub_set_themes='<span class="title">{/account.set_themes/}</span>';
$pub_add_pro='<span class="title">{/account.add_pro/}</span>';
$pub_domain_binding='<span class="title">{/module.set.domain_binding/}</span>';
$pub_set_shipping='<span class="title">{/account.set_shipping/}</span>';
$pub_set_payment='<span class="title">{/account.set_payment/}</span>';
$pub_isExitGuide='';
/*******************************************************************************************************************************************/
if($c['FunVersion']>=10){
	include('cdx_index.php');
}
/*******************************************************************************************************************************************/
?>
<script type="text/javascript">
var country_acronym_data={<?php foreach($country_row as $k=>$v){ $country_data=str::json_data(htmlspecialchars_decode($v['CountryData']), 'decode'); echo "'{$country_data['zh-cn']}':'".strtolower($v['Acronym'])."',";}?>};
</script>
<div id="account" class="r_con_wrap">
	<?php
	if(!db::get_row_count('config', 'GroupId="guide_pages"')){
		manage::config_operaction(array('themes'=>0,'shipping'=>0,'products'=>0,'payment'=>0), 'guide_pages');
	}
	$guide_row=db::get_all('config', 'GroupId="guide_pages"');
	$guide_status=0;
	$guide_ary=array();
	foreach((array)$guide_row as $v){
		$guide_ary[$v['Variable']]=(int)$v['Value'];
		if((int)$v['Value']) continue;
		$guide_status++;
	}
	if($c['FunVersion']>=10 && (int)$_GET['isExitGuide'] && $guide_status){	//退出引导模式
		db::update('config', 'GroupId="guide_pages"',array('Value'=>1));
		js::back();
	}
	if($guide_status && !(int)$_GET['isdata']){
		if($_SESSION['Manage']['welfare']){
			$result=$_SESSION['Manage']['welfare'];
		}else{
			$data=array('Action'=>'ueeshop_kuihuadao_get_course_list');
			$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
			$_SESSION['Manage']['welfare']=$result;
			$analytics_row=ly200::ueeshop_web_get_data();
			if($analytics_row['Service']['QQ'] && $analytics_row['Service']['Contacts']){
				$_SESSION['Manage']['Service']=1;
			}
		}
		if($result['ret']){
			$category_ary=$result['msg'][0];
			$video_ary=$result['msg'][1];
			$permission_ary=str::json_data($result['msg'][2], 'decode');
		}
	?>
		<script>$(document).ready(function(){account_obj.welfare_init();});</script>
		<div id="guide_pages">
			<div class="open_store">
				<div class="top_title">
                	{/account.open_store/}
                    <?=$pub_isExitGuide;?>
                </div>
				<?php if(!$guide_ary['themes']){?>
					<div class="list">
						<a href="./?m=view&a=visual">{/account.go_set/}</a>
						<img src="/static/manage/images/account/guide_img_themes.png" alt="">
						<?=$pub_set_themes;?>
					</div>
				<?php }?>
				<?php if(!$guide_ary['products']){?>
					<div class="list">
						<a href="./?m=products&a=products">{/account.add_now/}</a>
						<img src="/static/manage/images/account/guide_img_products.png" alt="">
						<?=$pub_add_pro;?>
					</div>
				<?php }?>
				<?php if(!$guide_ary['domain_binding']){?>
					<div class="list">
						<a href="./?m=set&a=domain_binding">{/account.go_set/}</a>
						<img src="/static/manage/images/account/guide_img_domain_binding.png" alt="">
                        <?=$pub_domain_binding;?>
					</div>
				<?php }?>
				<?php if(!$guide_ary['shipping']){?>
					<div class="list">
						<a href="./?m=set&a=shipping">{/account.go_set/}</a>
						<img src="/static/manage/images/account/guide_img_shipping.png" alt="">
                        <?=$pub_set_shipping;?>
					</div>
				<?php }?>
				<?php if(!$guide_ary['payment']){?>
					<div class="list">
						<a href="./?m=set&a=payment">{/account.go_set/}</a>
						<img src="/static/manage/images/account/guide_img_payment.png" alt="">
                        <?=$pub_set_payment;?>
					</div>
				<?php }?>
				<div class="clear"></div>
			</div>
			<div class="open_store suggest">
				<div class="top_title">{/account.suggest/}</div>
				<?php
				$manage_lang=$c['manage']['config']['ManageLanguage']=='zh-cn'?'cn':$c['manage']['config']['ManageLanguage'];
				$plug_row=db::get_one('plugins', 'ClassName="upload" and Category="app"');
				$NameAry=@str::json_data($plug_row['Name'], 'decode');
				$Name=$NameAry[$manage_lang];
				if($c['FunVersion']){
				?>
					<div class="list">
						<a href="./?m=plugins&a=<?=$plug_row['IsInstall']?'my_app':'app';?>&ClassName=upload">{/plugins.facebook_store.authorized/}</a>
						<img src="/static/manage/images/account/<?=$c['manage']['cdx_path'];?>guide_sug_pro.png" alt="">
						<div class="info">
							<div class="tit">{/account.upload/}</div>
							<div class="desc">{/plugins.briefdescript.upload/}</div>
						</div>
					</div>
				<?php
				}
				$plug_row=db::get_one('plugins', 'ClassName="facebook_ads_extension" and Category="app"');
				$NameAry=@str::json_data($plug_row['Name'], 'decode');
				$Name=$NameAry[$manage_lang];
				if($c['FunVersion']>=2 && $c['FunVersion']!=10){
				?>
					<div class="list">
						<a href="./?m=plugins&a=<?=$plug_row['IsInstall']?'my_app':'app';?>&ClassName=facebook_ads_extension">{/plugins.facebook_store.authorized/}</a>
						<img src="/static/manage/images/account/<?=$c['manage']['cdx_path'];?>guide_sug_store.png" alt="">
						<div class="info">
							<div class="tit"><?=$Name;?></div>
							<div class="desc">{/plugins.briefdescript.facebook_ads_extension/}</div>
						</div>
					</div>
				<?php }?>
				<div class="list">
					<a href="./?isdata=1">{/plugins.enter/}</a>
					<img src="/static/manage/images/account/<?=$c['manage']['cdx_path'];?>guide_sug_data.png" alt="">
					<div class="info">
						<div class="tit">{/account.analysis/}</div>
						<div class="desc">{/account.analysis_desc/}</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
            <?php if($c['FunVersion']<10){?>
			<div class="course">
				<div class="video_box">
					<a href="javascript:;" class="cloce"></a>
					<div id="video_play"></div>
				</div>
				<div class="top_title">{/account.course/}</div>
				<div class="box video_list">
					<?php
					$i=0;
					foreach((array)$video_ary as $v){
						if(!in_array($v['CourseId'], $permission_ary)) continue;
						if($i>5) continue;
					?>
						<div class="list <?=$i%2==0?'fl':'fr';?>" data-alid="<?=$v['ALId'];?>" title="<?=$v['Name'];?>">
							<div class="img"><img src="<?=$v['Photo'];?>" alt="" /></div>
							<div class="info">
								<div class="tit"><?=$v['Name'];?></div>
								<div class="desc"><?=str::format($v['LearnPoint']);?></div>
							</div>
							<div class="clear"></div>
						</div>
					<?php
						$i++;
					}?>
					<div class="clear"></div>
				</div>
			</div>
			<a href="./?m=account&a=welfare" class="more_video">{/account.view_more/}</a>
            <?php }?>
			<div class="no_play_tips">
				<table class="tips">
					<tr><td>
						<?php if(substr_count($_SERVER['HTTP_HOST'], '.myueeshop.com')){?>
							“请联系Ueeshop建站顾问”<br />
							索取观看权限
						<?php }else{?>
							友情提醒：进阶课程需先完善网站资料，<br>
							我们的客服专员<?=$_SESSION['Manage']['Service']?'（QQ：800031052）':'';?>会为您网站检查后开启课程
						<?php }?>
					</td></tr>
				</table>
			</div>
		</div>
		<script language="javascript" src="https://g.alicdn.com/de/prismplayer/2.7.4/aliplayer-min.js"></script>
		<link href="https://g.alicdn.com/de/prismplayer/2.7.4/skins/default/aliplayer-min.css" rel="stylesheet" type="text/css" />
	<?php
	}else{
	?>
		<script>$(document).ready(function(){account_obj.index_init()});</script>
		<div class="home_container clean">
			<dl class="home_choice_day">
				<dt><strong>{/account.day_ary.0/}</strong><em><i></i></em></dt>
				<dd class="drop_down">
					<?php
					foreach($days_ary as $k=>$v){
					?>
						<a href="javascript:;" data-rel="<?=$v;?>"<?=$v==0?' class="current"':'';?>>{/account.day_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<div class="clear"></div>
			<div class="home_parent_box home_parent_top_box mb20">
                <div class="clean mr10">
					<div class="home_parent_box">
						<!-- 总销售量 Start -->
                        <div class="global_container home_total_sales mr10">
							<div class="title">{/account.total_sales/}</div>
                            <strong class="big_number"><span><?=$c['manage']['currency_symbol'] . ' 0.00';?></span></strong>
							<div class="content">
								<div class="chart_data" id="orders_sales_charts"></div>
							</div>
                            <a href="./?m=mta&a=orders" class="more">{/account.detail/}</a>
						</div>
                        <!-- 总销售量 End -->
					</div>
					<div class="home_parent_box">
                        <!-- 总订单数 Start -->
                        <div class="global_container home_total_orders ml10">
							<div class="title">{/account.total_orders/}</div>
                            <strong class="big_number"><span>389</span> {/account.order_item/}</strong>
							<div class="content">
								<div class="chart_data" id="orders_count_charts"></div>
							</div>
                            <a href="./?m=mta&a=orders" class="more">{/account.detail/}</a>
						</div>
                        <!-- 总订单数 End -->
                        
					</div>
				</div>
			</div>
            <div class="home_parent_box home_parent_top_box mb20">
				<div class="clean ml10">
					<div class="home_parent_box">
						<!-- 流量 Start -->
						<div class="global_container home_session mr10">
							<div class="title">{/mta.global.traffic/}</div>
                            <div class="traffic_session">
                                <strong class="big_number_session"><p class="color_555">0</p><span class="color_aaa">{/mta.conversion.conversation/}</span></strong>
                                <strong class="big_number_visitors"><p class="color_555">0</p><span class="color_aaa">{/mta.conversion.visitors/}</span></strong>
                            </div>
							<div class="content">
								<div class="chart_data" id="session_charts"></div>
							</div>
                            <a href="./?m=mta&a=visits" class="more">{/account.detail/}</a>
						</div>
						<!-- 流量 End -->
					</div>
					<div class="home_parent_box">
						<!-- 转化率 Start -->
						<div class="global_container home_conversion ml10">
							<div class="title">{/mta.ratio/}</div>
                            <strong class="big_number"><span>0%</span></strong>
							<div class="content">
								<ul class="conversion_list">
									<li class="clean">
										<div class="contents_left">
											<div class="operate">{/mta.conversion.enter/}</div>
											<div class="operate_data"><span class="ratio_session">0</span> {/mta.conversion.conversation/}</div>
										</div>
									</li>
									<li class="clean">
										<div class="contents_left">
											<div class="operate">{/mta.conversion.addtocart/}</div>
											<div class="operate_data"><span class="ratio_addtocart">0</span> {/mta.conversion.conversation/}</div>
										</div>
										<div class="contents_right">
											<div class="operate_ratio"><span class="ratio_operate_addtocart">0</span>%</div>
										</div>
									</li>
									<li class="clean">
										<div class="contents_left">
											<div class="operate">{/mta.conversion.placeorder/}</div>
											<div class="operate_data"><span class="ratio_placeorder">0</span> {/mta.conversion.conversation/}</div>
										</div>
										<div class="contents_right">
											<div class="operate_ratio"><span class="ratio_operate_placeorder">0</span>%</div>
										</div>
									</li>
									<li class="clean">
										<div class="contents_left">
											<div class="operate">{/mta.conversion.complete/}</div>
											<div class="operate_data"><span class="ratio_complete">0</span> {/mta.conversion.conversation/}</div>
										</div>
										<div class="contents_right">
											<div class="operate_ratio"><span class="ratio_operate_complete">0</span>%</div>
										</div>
									</li>
								</ul>
							</div>
                            <a href="./?m=mta&a=visits_conversion" class="more">{/account.detail/}</a>
						</div>
						<!-- 转化率 End -->
					</div>
				</div>
			</div>
            <div class="clear"></div>
			<div class="home_parent_box home_parent_top_box mb20">
				<div class="clean mr10">
					<div class="home_parent_box">
						<!-- 流量来源 Start -->
						<div class="global_container home_visits_referrer mr10">
							<div class="title">{/mta.global.traffic_source/}</div>
							<div class="content">
								<div id="referrer_charts"></div>
							</div>
						</div>
						<!-- 流量来源 End -->
					</div>
					<div class="home_parent_box">
						<!-- 社交平台 Start -->
						<div class="global_container home_share_platform ml10">
							<div class="title">{/mta.conversion.share_platform/}</div>
							<div class="content">
								<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
									<thead>
										<tr>
											<td width="10%" nowrap></td>
											<td width="50%" nowrap></td>
											<td width="20%" nowrap></td>
											<td width="20%" nowrap></td>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								<div class="no_data">{/error.no_data/}</div>
							</div>
						</div>
						<!-- 社交平台 End -->
					</div>
				</div>
			</div>
			<div class="home_parent_box home_parent_top_box mb20">
				<div class="clean ml10">
					<div class="home_parent_box">
                        <?php /*
						<!-- 搜索排行 Start -->
						<div class="global_container home_search mr10">
							<div class="title">{/mta.search.search_ranking/}</div>
							<div class="content">
								<div class="div_report_list"></div>
								<div class="no_data">{/error.no_data/}</div>
							</div>
						</div>
						<!-- 搜索排行 End -->
                        */?>
                        <!-- 销量排行 Start -->
						<div class="global_container home_sales mr10">
							<div class="title">{/mta.product.sales_ranking/}</div>
							<div class="content">
								<?php /*
								<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
									<thead>
										<tr>
											<td width="40" nowrap></td>
											<td width="90%" nowrap></td>
											<td width="10%" nowrap></td>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								*/?>
								<div class="div_report_list"></div>
								<div class="no_data">{/error.no_data/}</div>
							</div>
						</div>
						<!-- 销量排行 End -->
					</div>
					<div class="home_parent_box">
						<!-- 订单分布 Start -->
						<div class="global_container home_orders_country ml10">
							<div class="title">{/mta.order.order_distribution/}</div>
							<div class="content">
								<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
									<thead>
										<tr>
											<td width="10%" nowrap></td>
											<td width="50%" nowrap></td>
											<td width="20%" nowrap></td>
											<td width="20%" nowrap></td>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								<div class="no_data">{/error.no_data/}</div>
							</div>
						</div>
						<!-- 订单分布 End -->
					</div>
				</div>
			</div>
            <div class="clear"></div>
            <div class="home_parent_box home_parent_top_box mb20">
				<!-- 广告 Start -->
				<div class="global_container home_ad mb20 mr10">
					<div class="title">
						<a href="javascript:;" class="current" data-type="facebook">{/account.ad.facebook.title/}</a>
						<a href="javascript:;" class="hide" data-type="google">{/account.ad.google.title/}</a>
						<a href="javascript:;" class="hide" data-type="bing">{/account.ad.bing.title/}</a>
					</div>
					<div class="content">
						<div class="home_facebook">
							<ul class="links fl">
								<li><a href="./?m=account&a=welfare&Keyword=Facebook营销">{/account.ad.facebook.links.2/}</a></li>
							</ul>
							<div class="info">
								<div class="info_head">
									<div class="icon fl"></div>
									<strong class="color_000">{/plugins.global.items.facebook_ads_extension/}</strong>
									<a href="./?m=plugins&a=app&Keyword={/plugins.global.items.facebook_ads_extension/}" class="btn_global btn_submit">{/account.ad.global.do_it/}</a>
								</div>
								<div class="info_body">
									<strong class="color_555">{/account.ad.facebook.note_title/}</strong>
									<div class="info_list">
										<div class="item"><p>{/account.ad.facebook.note_list.0.0/}</p><span>{/account.ad.facebook.note_list.0.1/}</span></div>
										<div class="item"><p>{/account.ad.facebook.note_list.1.0/}</p><span>{/account.ad.facebook.note_list.1.1/}</span></div>
										<div class="item"><p>{/account.ad.facebook.note_list.2.0/}</p><span>{/account.ad.facebook.note_list.2.1/}</span></div>
										<div class="item"><p>{/account.ad.facebook.note_list.3.0/}</p><span>{/account.ad.facebook.note_list.3.1/}</span></div>
									</div>
								</div>
							</div>
						</div>
						<div class="home_google hide">
							<ul class="links fl">
								<li><a href="">{/account.ad.google.links.0/}</a></li>
								<li><a href="">{/account.ad.google.links.1/}</a></li>
								<li><a href="">{/account.ad.google.links.2/}</a></li>
								<li><a href="">{/account.ad.google.links.3/}</a></li>
							</ul>
							<div class="info">
								<div class="info_list">
									<div class="item item_0"><em></em><p>{/account.ad.google.note_list.0.0/}</p><span>{/account.ad.google.note_list.0.1/}</span></div>
									<div class="item item_1"><em></em><p>{/account.ad.google.note_list.1.0/}</p><span>{/account.ad.google.note_list.1.1/}</span></div>
									<div class="item item_2"><em></em><p>{/account.ad.google.note_list.2.0/}</p><span>{/account.ad.google.note_list.2.1/}</span></div>
								</div>
								<div class="info_button">
									<a href="./?m=plugins&a=app&Keyword=google" class="btn_global btn_submit">{/account.ad.global.do_it/}</a>
								</div>
							</div>
						</div>
						<div class="home_bing hide"></div>
					</div>
				</div>
				<!-- 广告 End -->
			</div>
			<div class="home_parent_box home_parent_top_box mb20">
				<div class="clean ml10">
					<!-- 来源网址 Start -->
					<div class="global_container home_referrer_url">
						<div class="title">{/mta.global.source_url/}</div>
						<div class="content">
							<?php /*
							<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
								<thead>
									<tr>
										<td width="40" nowrap></td>
										<td width="90%" nowrap></td>
										<td width="10%" nowrap></td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
							*/?>
							<div class="div_report_list"></div>
							<div class="no_data">{/error.no_data/}</div>
						</div>
					</div>
					<!-- 来源网址 End -->
					<?php /*
					<div class="home_parent_box">
						<!-- 来源网址 Start -->
						<div class="global_container home_referrer_url mr10">
							<div class="title">来源网址</div>
							<div class="content">
								<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
									<thead>
										<tr>
											<td width="60%" nowrap></td>
											<td width="20%" nowrap></td>
											<td width="20%" nowrap></td>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
								<div class="no_data">{/error.no_data/}</div>
							</div>
						</div>
						<!-- 来源网址 End -->
					</div>
					<div class="home_parent_box">
						<!-- 访问终端 Start -->
						<div class="global_container home_terminal ml10">
							<div class="title">访问终端</div>
							<div class="content">
								<ul class="terminal_list">
									<li><p>PC端</p><strong>0%</strong></li>
									<li><p>移动端</p><strong>0%</strong></li>
								</ul>
							</div>
						</div>
						<!-- 访问终端 End -->
					</div>
					*/?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>