<?php !isset($c) && exit();?>
<?php
if($c['FunVersion']>=10){
	exit($c['manage']['lang_pack']['manage']['manage']['no_permit']);
}
if($_SESSION['Manage']['welfare']){
	$result=$_SESSION['Manage']['welfare'];
	
}else{
	$data=array(
		'Action'	=>	'ueeshop_kuihuadao_get_course_list',
	);
	$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
	$_SESSION['Manage']['welfare']=$result;
	$analytics_row=ly200::ueeshop_web_get_data();
	if($analytics_row['Service']['QQ'] && $analytics_row['Service']['Contacts']){
		$_SESSION['Manage']['Service']=1;
	}
}
if($result['ret']){
	$category_ary=$result['msg'][0];
	$video_ary=$result['msg'][1];
	$permission_ary=str::json_data($result['msg'][2], 'decode');
	$uid_categroy_ary=$cate_video_ary=array();
	foreach((array)$category_ary as $v){
		$uid_categroy_ary[$v['UId']][]=$v;
	}
	foreach((array)$video_ary as $v){
		$cate_video_ary[$v['CategoryId']][]=$v;
	}
}
if($_SESSION['Manage']['resource_data']){
	$resource_data=$_SESSION['Manage']['resource_data'];
}else{
	$resource_data=str::json_data(ly200::curl('http://ueeshop.ly200-cdn.com/static/partner/resource.json'), 'decode');
	$_SESSION['Manage']['resource_data']=$resource_data;
}
$try_web=substr_count($_SERVER['HTTP_HOST'], '.myueeshop.com')?1:0;//试用网站保留已开放
$Keyword=trim($_GET['Keyword']);
?>
<script>
$(document).ready(function(){
	account_obj.welfare_init();
	<?php
	if($Keyword){
		echo '$(".welfare_box:eq(0) .category>a[data-title='.$Keyword.']").click();';
	}?>
});
</script>
<div id="account" class="r_con_wrap">
	<div id="welfare">
		<div class="welfare_menu">
			<a href="javascript:;" class="cur" rel="nofollow">营销课程</a>
			<a href="javascript:;" rel="nofollow">营销资源</a>
		</div>
		<div class="video_box">
			<a href="javascript:;" class="cloce"></a>
			<div id="video_play"></div>	
		</div>
		<div class="welfare_box" style="display: block;">
			<div class="category">
				<?php
				if($try_web){
					echo '<a href="javascript:;" class="cur" rel="nofollow" data-title="已开放">已开放</a>';
				}
				foreach((array)$uid_categroy_ary['0,'] as $k=>$v){ 
					if(!count($uid_categroy_ary['0,'.$v['CategoryId'].','])) continue;
					echo '<a href="javascript:;"'.($k==0 && !$try_web?' class="cur"':'').' rel="nofollow" data-title="'.$v['Category'].'">'.$v['Category'].'</a>';
				}?>
			</div>
			<?php if($try_web){?>
				<div class="category_box" style="display:block;">
					<div class="category_name">已开放</div>
					<div class="category_list video_list">
						<?php
						foreach((array)$video_ary as $v){ 
							if(!in_array($v['CourseId'], $permission_ary)) continue;
						?>
							<div class="list" data-alid="<?=$v['ALId']; ?>" title="<?=$v['Name']; ?>">
								<div class="img">
									<img src="<?=$v['Photo']; ?>" alt="">
								</div>
								<div class="name">
									<?=$v['Name']; ?>
								</div>
							</div>
						<?php }?>
						<div class="clear"></div>
					</div>
				</div>
			<?php }?>
			<?php
			foreach((array)$uid_categroy_ary['0,'] as $k=>$v){ 
				if(!count($uid_categroy_ary['0,'.$v['CategoryId'].','])) continue;
			?>
				<div class="category_box" <?=$k==0 && !$try_web ? ' style="display:block;"' : ''; ?>>
					<?php
					foreach((array)$uid_categroy_ary['0,'.$v['CategoryId'].','] as $v1){ 
						if(!count($cate_video_ary[$v1['CategoryId']])) continue;
					?>
						<div class="category_name"><?=$v1['Category']; ?></div>
						<div class="category_list video_list">
							<?php
							foreach((array)$cate_video_ary[$v1['CategoryId']] as $v2){ 
								$play=in_array($v2['CourseId'], $permission_ary) ? 1 : 0;
							?>
								<div class="list <?=$play ? '' : 'no_play'; ?>" data-alid="<?=$v2['ALId']; ?>" title="<?=$v2['Name']; ?>">
									<div class="img">
										<img src="<?=$v2['Photo']; ?>" alt="">
									</div>
									<div class="name">
										<?=$v2['Name']; ?>
									</div>
								</div>
							<?php }?>
							<div class="clear"></div>
						</div>
					<?php }?>
				</div>
			<?php }?>
		</div>
		<div class="welfare_box resource">
			<div class="category" style="display: none;">
				<?php
				$i=0; 
				foreach((array)$resource_data as $k=>$v){
				?>
					<a href="javascript:;" <?=$i==0 ? ' class="cur"' : ''; ?> rel="nofollow"><?=$k; ?></a>
				<?php
					$i++;
				}?>
			</div>
			<?php
			$i=0; 
			foreach((array)$resource_data as $k=>$v){
			?>
				<div class="category_box" <?=$i==0 ?'style="display:block;"':'';?>>
					<?php foreach((array)$v as $k1=>$v1){ ?>
						<table class="list <?=$v1['Sales'] ? 'sales' : ''; ?>" <?=$v1['Sales'] ? 'sales="'.$v1['Sales'].'"' : ''; ?>>
							<tr>
								<td width="29.7%"><img src="<?=$v1['Logo']; ?>" alt=""></td>
								<td class="info">
									<div class="title"><?=$v1['Name']; ?></div>
									<div class="desc"><?=$v1['Brief']; ?></div>
									<?php if($v1['Phone']){ ?><div class="contact phone"><span>电话</span><?=$v1['Phone']; ?></div><?php } ?>
									<?php if($v1['Wechat']){ ?><div class="contact wechat"><span>微信</span><?=$v1['Wechat']; ?></div><?php } ?>
									<?php if($v1['QQ']){ ?><div class="contact qq"><span>QQ</span><?=$v1['QQ']; ?></div><?php } ?>
									<?php if($v1['Email']){ ?><div class="contact email"><span>邮箱</span><?=$v1['Email']; ?></div><?php } ?>
									<?php if($v1['Website']){ ?><div class="contact web"><span>网址</span><?=$v1['Website']; ?></div><?php } ?>
								</td>
								<td width="15.3%"><a href="javascript:;" rel="nofollow" class="more">了解更多</a></td>
								<td class="html" style="display: none;">
									<div class="title"><?=$v1['Name']; ?></div>
									<div class="content">
										<div class="img"><img src="<?=$v1['Logo']; ?>" alt=""></div>
										<?=$v1['Description']; ?>
									</div>
									<?php if($v1['QrCode']){ ?>
										<div class="qrcode">
											<div class="img"><img src="<?=$v1['QrCode']; ?>" alt=""></div>
											<div class="tips"><?=$v1['QrCodeMsg']; ?></div>
										</div>
									<?php } ?>
								</td>
							</tr>
						</table>
					<?php }?>
				</div>
			<?php
				$i++;
			}?>
			<table id="alert_box"><tr><td>
				<div class="alert_box">
					<a href="javascript:;" rel="nofollow" class="close"></a>
					<div class="html"></div>
				</div>
			</td></tr></table>
			<div id="statement">
				声明：该展示并不视为我司（广州联雅网络科技有限公司）对展示公司包括但不限于资信、服务能力、经营能力以及其他第三方据以与展示公司开展合作的考量因素等的保证或承诺。任何第三方应自行判断是否与展示公司开展商业往来，并自行承担商业风险。
			</div>
		</div>
		<div class="no_play_tips">
			<table class="tips">
				<tr><td>
					<?php if(substr_count($_SERVER['HTTP_HOST'], '.myueeshop.com')){ ?>
						“请联系Ueeshop建站顾问”<br />
						索取观看权限
					<?php }else{ ?>
						友情提醒：进阶课程需先完善网站资料，<br>
						我们的客服专员<?php if($_SESSION['Manage']['Service']){ ?>（QQ：800031052）<?php } ?>会为您网站检查后开启课程
					<?php } ?>
				</td></tr>
			</table>
		</div>
	</div>
</div>
<script language="javascript" src="https://g.alicdn.com/de/prismplayer/2.7.4/aliplayer-min.js"></script>
<link href="https://g.alicdn.com/de/prismplayer/2.7.4/skins/default/aliplayer-min.css" rel="stylesheet" type="text/css" />
