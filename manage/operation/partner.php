<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'partner'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('operation', 0, array('a'=>'partner', 'd'=>'add')),
	'edit'	=>	manage::check_permit('operation', 0, array('a'=>'partner', 'd'=>'edit')),
	'del'	=>	manage::check_permit('operation', 0, array('a'=>'partner', 'd'=>'del'))
);
?>

<div id="partner" class="r_con_wrap">
	<div class="center_container_1000">
		<?php
		if($c['manage']['do']=='index'){
		?>
			<script type="text/javascript">$(document).ready(function(){operation_obj.partner_init()});</script>
			<div class="inside_container">
				<h1>{/module.operation.partner/}</h1>
			</div>
			<div class="inside_table">
				<div class="list_menu">
					<ul class="list_menu_button">
						<?php if($permit_ary['add']){?><li><a class="add" href="./?m=operation&a=partner&d=edit">{/global.add/}</a></li><?php }?>
						<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					</ul>
				</div>
				<?php 
				$partner_row=str::str_code(db::get_limit_page('partners', '1', '*', $c['my_order'].'PId desc', (int)$_GET['page'], 20));
				if($partner_row[0]){ 
					?>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td width="4%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td>
								<?php }?>
								<td width="16%" nowrap="nowrap">{/global.name/}</td>
								<td width="" nowrap="nowrap">{/global.pic/}</td>
								<td width="" nowrap="nowrap">{/partner.url/}</td>
								<td width="10%" nowrap="nowrap">{/global.used/}</td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
								<?php }?>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach((array)$partner_row[0] as $v){
							?>
								<tr pid="<?=$v['PId'];?>">
									<?php if($permit_ary['edit'] || $permit_ary['del']){?>
										<td nowrap="nowrap"><?=html::btn_checkbox('select', $v['PId']);?></td>
									<?php }?>
									<td nowrap="nowrap"><?=$v['Name'.$c['manage']['web_lang']];?></td>
									<td nowrap="nowrap" class="img"><a href="<?=$v['PicPath'];?>" title="<?=$v['Name'.$c['manage']['web_lang']];?>" target="_blank"><img class="photo" src="<?=$v['PicPath'];?>" alt="<?=$v['Name'.$c['manage']['web_lang']];?>" align="absmiddle" /></a></td>
									<td nowrap="nowrap"><a href="<?=$v['Url'];?>" target="_blank"><?=$v['Url'];?></a></td>
									<td nowrap="nowrap" class="used_checkbox">
										<?php if($permit_ary['edit']){?>
											<div class="switchery<?=(int)$v['IsUsed']==1?' checked':'';?>">
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										<?php
										}else{
											echo (int)$v['IsUsed']==1?'{/global.n_y.1/}':'';
										}
										?>
									</td>
									<?php if($permit_ary['edit'] || $permit_ary['del']){?>
										<td nowrap="nowrap" class="operation side_by_side">
											<?php if($permit_ary['edit']){?><a href="./?m=operation&a=partner&d=edit&PId=<?=$v['PId'];?>">{/global.edit/}</a><?php }?>
											<?php if($permit_ary['del']){?>
												<dl>
													<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
													<dd class="drop_down">
														<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=operation.partner_del&PId=<?=$v['PId'];?>" rel="del">{/global.del/}</a><?php }?>
													</dd>
												</dl>
											<?php }?>
										</td>
									<?php }?>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=html::turn_page($partner_row[1], $partner_row[2], $partner_row[3], '?'.ly200::query_string('page').'&page=');?>
				<?php }else{
					echo html::no_table_data();
				} ?>
			</div>
		<?php
		}else{
			$PId=(int)$_GET['PId'];
			$PId && $partner_row=str::str_code(db::get_one('partners', "PId='$PId'"));
		?>
	    	<script type="text/javascript">$(document).ready(function(){operation_obj.partner_edit_init()});</script>
	    	<div class="global_container">
	    		<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.operation.partner/}</span> 
					<span class="s_return">/ <?=$PId ? '{/global.edit/}' : '{/global.add/}'; ?></span>
				</a>
				<form id="partners_edit_form" class="global_form">
					<div class="rows">
						<label>{/global.name/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input"><?=manage::form_edit($partner_row, 'text', 'Name', 50, 150, 'notnull');?></div>					
					</div>
					<div class="rows">
						<label>{/global.pic/}</label>
						<div class="input">
							<?=manage::multi_img('PicDetail', 'PicPath', $partner_row['PicPath']); ?>
						</div>					
					</div>
					<div class="rows">
						<label>{/partner.url/}</label>
						<div class="input"><input name="Url" value="<?=$partner_row['Url'];?>" type="text" class="box_input" size="50" maxlength="200" /></div>					
					</div>
					<div class="rows">
						<label>{/partner.myorder/}</label>
						<div class="input">
							<div class="box_select">
								<?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $partner_row['MyOrder']);?>
							</div>
						</div>					
					</div>
					<div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/global.save/}" name="submit_button">
							<a href="./?m=operation&a=partner"><input type="button" class="btn_global btn_cancel" value="{/global.cancel/}"></a>
						</div>			
					</div>
					<input type="hidden" id="PId" name="PId" value="<?=$PId;?>" />
					<input type="hidden" name="do_action" value="operation.partner_edit" />
				</form>
			</div>
		<?php }?>
	</div>
</div>