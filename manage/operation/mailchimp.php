<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'mailchimp'));//检查权限

if(!$c['manage']['do'] || $c['manage']['do']=='index'){//重新指向“邮件发送”页面
	$c['manage']['do']='send';
}

$top_id_name=(($c['manage']['do']=='email_logs' && $c['manage']['page']=='index')?'email_logs':'mailchimp');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<div class="inside_container">
		<h1>{/module.operation.email.module_name/}</h1>
		<ul class="inside_menu">
			<?php
			$out=0;
			$open_ary=array();
			foreach($c['manage']['permit']['operation']['mailchimp']['menu'] as $k=>$v){
				if(!manage::check_permit('operation', 0, array('a'=>'mailchimp', 'd'=>$v)) || ($c['FunVersion']==0 && ($v=='send' || $v=='email_logs'))){//没权限，标准版
					if($v=='send' && $c['manage']['do']=='send') $out=1;
					continue;
				}else{
					$open_ary[]=$v;
				}
			?>
				<li><a href="./?m=operation&a=mailchimp&d=<?=$v;?>"<?=$c['manage']['do']==$v?' class="current"':'';?>>{/module.operation.email.<?=$v;?>/}</a></li>
			<?php
			}
			if($out) js::location('?m=operation&a=mailchimp&d='.$open_ary[0]);//当第一个选项没有权限打开，就跳转能打开的第一个页面
			?>
		</ul>
	</div>
	<?php
	if($c['manage']['do']=='send'){
		$templates_data=mailchimp::get_info('/templates');
		$templates_ary=array();
		if(!$templates_data['error']){
			foreach((array)$templates_data['response']['templates'] as $k=>$v){
				if($v['category']=='pages' || $v['category']=='E-commerce' || $v['category']=='Newsletters' || $v['category']=='Events'){ continue; }
				$templates_ary[$v['category']][]=$v;
			}
		}
		//邮件发送
		echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js', '/static/js/plugin/ckeditor/ckeditor.js');
	?>
		<script type="text/javascript">$(document).ready(function(){operation_obj.mailchimp_send_init()});</script>
		<div class="center_container_1200 clean">
			<form id="edit_form" class="global_form">
				<div class="left_container">
					<div class="left_container_side">
						<div class="global_container">
							<?php if($c['FunVersion']>=1){?>
								<div class="rows clean">
									<label>{/email.templates/}</label>
									<div class="input">
										<input type="button" class="btn_global btn_tpl_choice" value="{/email.email_tpl/}" />
										<div class="blank12"></div>
									</div>
								</div>
							<?php }?>
							<div class="rows clean">
								<label>所选模板图</label>
								<div class="input"><img src="" alt="" id="TemplatesDetail"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="right_container">
					<div class="global_container">
						<div class="rows clean">
							<label>{/email.addressee/}</label>
							<div class="input">
								<?php
								$SelectAry=array();
								$row=db::get_limit('user', '1', 'UserId, Email', 'UserId desc', 0, 20);
								foreach($row as $k=>$v){
									$SelectAry[$v['UserId']]=array('Name'=>$v['Email'], 'Type'=>'user', 'Table'=>'');
								}
								$ValueAry=array('Select'=>$Email, 'Input'=>$Email, 'Type'=>'user');
								echo manage::box_drop_double('EmailSelect', 'Email', $SelectAry, $ValueAry, 0, '', 0);
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="rows">
					<div class="center_container_1200">
						<div class="input clean">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.send/}" />
						</div>
					</div>
				</div>
				<input type="hidden" name="TemplatesId" id="TemplatesId" value="" />
				<input type="hidden" name="do_action" value="operation.mailchimp_send" />
			</form>
			<div id="fixed_right">
				<div class="global_container fixed_template_choice">
					<div class="top_title">{/email.email_tpl/} 
                    	<a href="javascript:;" class="close"></a>
                        <ul class="top_menu">
                        	<?php foreach((array)$templates_ary as $k=>$v){?>
								<li><a href="javascript:;" data-type="<?=$k?$k:'uncategorized';?>"><?=$k?$k:'uncategorized';?></a></li>
                        	<?php }?>
                        </ul>
                    </div>
					<form class="global_form" id="template_choice_form">
						<div class="template_choice_list">
							<?php foreach((array)$templates_ary as $k=>$v){?>
								<div class="list clean" data-type="<?=$k?$k:'uncategorized';?>">
									<?php foreach((array)$v as $k1=>$v1){ ?>
											<div class="item" data-tpl="<?=$v1['id'];?>">
												<div class="img"><img src="<?=$v1['thumbnail'];?>" /><span></span><div class="img_mask"></div></div>
												<div class="name"><?=$v1['name'];?></div>
											</div>
									<?php }?>
								</div>
							<?php }?>
						</div>
						<div class="rows clean box_button">
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
								<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
							</div>
						</div>
						<input type="hidden" name="Template" value="" />
						<input type="hidden" name="Type" value="" />
						<input type="hidden" name="do_action" value="operation.mailchimp_template_choice" />
					</form>
				</div>
			</div>
		</div>
	<?php
	}elseif($c['manage']['do']=='email_logs'){
		if($c['manage']['page']=='view'){ 
			//邮件发送记录查看
			$LId=(int)$_GET['LId'];
			$log_row=db::get_one('email_log', "LId='$LId'");
	?>
			<div class="global_form center_container_1200">
				<div class="left_container">
					<div class="left_container_side">
						<div class="global_container">
							<a href="javascript:history.back(-1);" class="return_title">
								<span class="return">{/module.operation.email.email_logs/}</span> 
								<span class="s_return">/ {/global.view/}</span>
							</a>
							<div class="rows">
								<label>{/email.email_logs.content/}</label>
								<div class="input email_content"><?=$log_row['Body'];?></div>
								<?php /*<span class="input"><?=manage::Editor("Body", $log_row['Body']);?></span>*/?>
							</div>
						</div>
					</div>
				</div>
				<div class="right_container">
					<div class="global_container">
						<div class="rows">
							<label>{/email.email_logs.to_email/}</label>
							<div class="input"><?=$log_row['Email'];?></div>								
						</div>
						<div class="rows">
							<label>{/email.email_logs.status/}</label>
							<div class="input">{/email.email_logs.status_ary.0/}</div>								
						</div>
						<div class="rows">
							<label>{/global.time/}</label>
							<div class="input"><?=date('Y-m-d H:i:s', $log_row['AccTime']);?></div>								
						</div>
						<div class="rows">
							<label>{/email.email_logs.subject/}</label>
							<div class="input"><?=$log_row['Subject'];?></div>								
						</div>
					</div>
				</div>
			</div>
		<?php
		}else{
		?>
			<div class="inside_table center_container_1000">
				<?php
				$w='1';
				$UserId=$_GET['UserId'];
				$Keyword=$_GET['Keyword'];
				$Module=$_GET['Module'];
				$UserId && $w.=" and UserId='$UserId'";
				$Module && $w.=" and Module='$Module'";
				$Keyword && $w.=" and Log like '%$Keyword%'";
				$manage_logs_row=db::get_limit_page('email_log', $w, '*', 'LId desc', (int)$_GET['page'], 20);
				
				if($manage_logs_row[0]){
				?>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="5%" nowrap="nowrap">{/global.serial/}</td>
								<td width="15%" nowrap="nowrap">{/email.email_logs.to_email/}</td>
								<td width="30%" nowrap="nowrap">{/email.email_logs.subject/}</td>
								<td width="10%" nowrap="nowrap">{/email.email_logs.status/}</td>
								<td width="10%" nowrap="nowrap">{/global.time/}</td>
								<td width="10%" nowrap="nowrap" class="operation">{/global.operation/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($manage_logs_row[0] as $v){
							?>
								<tr>
									<td nowrap="nowrap"><?=$manage_logs_row[4]+$i++;?></td>
									<td nowrap="nowrap"><?=$v['Email'];?></td>
									<td nowrap="nowrap"><?=$v['Subject'];?></td>
									<td nowrap="nowrap">{/email.email_logs.status_ary.0/}</td>
									<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
									<td nowrap="nowrap" class="operation">
										<a class="" href="./?m=operation&a=email&d=email_logs&p=view&LId=<?=$v['LId'];?>">{/global.view/}</a>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=html::turn_page($manage_logs_row[1], $manage_logs_row[2], $manage_logs_row[3], '?'.ly200::query_string('page').'&page=');?>
				<?php
				}else{//没有数据
					echo html::no_table_data(0);
				}?>
			</div>
	<?php
		}
	}?>
</div>