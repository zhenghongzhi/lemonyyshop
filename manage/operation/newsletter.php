<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'newsletter'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('operation', 0, array('a'=>'newsletter', 'd'=>'add')),
	'edit'	=>	manage::check_permit('operation', 0, array('a'=>'newsletter', 'd'=>'edit')),
	'del'	=>	manage::check_permit('operation', 0, array('a'=>'newsletter', 'd'=>'del'))
);
?>

<div id="newsletter" class="r_con_wrap">
	<script type="text/javascript">$(document).ready(function(){operation_obj.newsletter_init()});</script>
	<div class="inside_container">
		<h1>{/module.operation.newsletter/}</h1>
	</div>
	<div class="inside_table">
		<div class="list_menu">
			<ul class="list_menu_button">
				<li><a class="explode" id="excel_format" href="javascript:;">{/global.explode/}</a></li>
			</ul>
			<div class="search_form">
				<form method="get" action="?">
					<div class="k_input">
						<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
						<input type="button" value="" class="more" />
					</div>
					<input type="submit" class="search_btn" value="{/global.search/}" />
					<input type="hidden" name="m" value="operation" />
					<input type="hidden" name="a" value="newsletter" />
				</form>
			</div>
		</div>
		<div class="clear"></div>
		<?php
		$Keyword=str::str_code($_GET['Keyword']);
		$where='1';
		$Keyword && $where.=" and Email like '%$Keyword%'";
		$newsletter_row=db::get_limit_page('newsletter', $where, '*', 'NId desc', (int)$_GET['page'], 20);
		
		if($newsletter_row[0]){
		?>
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
				<thead>
					<tr>
						<td width="60%" nowrap="nowrap">{/global.email/}</td>
						<td width="25%" nowrap="nowrap">{/global.time/}</td>
						<td width="16%" nowrap="nowrap">{/email.newsletter.status/}</td>
						<?php if($permit_ary['del']){?><td width="10%" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
					</tr>
				</thead>
				<tbody>
					<?php
					$i=1;
					foreach($newsletter_row[0] as $v){
					?>
						<tr data-id="<?=$v['NId'];?>">
							<td nowrap="nowrap">
								<a href="./?m=operation&a=email&d=send&Email=<?=$v['Email'];?>" class="btn_email"><i class="icon_email"></i></a>
								<span><?=$v['Email'];?></span>
							</td>
							<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
							<td nowrap="nowrap">
								<?php
								if($permit_ary['edit']){
								?>
									<div class="switchery<?=(int)$v['IsUsed']?' checked':'';?>">
										<input type="checkbox" name="IsUsed" value="1" data-id="<?=$v['NId'];?>"<?=(int)$v['IsUsed']?' checked':'';?>>
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
								<?php
								}else{
									echo $v['IsUsed']?'{/global.n_y.1/}':'{/global.n_y.0/}';
								}?>
							</td>
							<?php if($permit_ary['del']){?>
								<td nowrap="nowrap" class="operation">
									<a class="del" href="./?do_action=operation.newsletter_del&NId=<?=$v['NId'];?>" rel="del">{/global.del/}</a>
								</td>
							<?php }?>
						</tr>
					<?php }?>
				</tbody>
			</table>
			<?=html::turn_page($newsletter_row[1], $newsletter_row[2], $newsletter_row[3], '?'.ly200::query_string('page').'&page=');?>
		<?php
		}else{//没有数据
			echo html::no_table_data(0);
		}?>
	</div>
</div>