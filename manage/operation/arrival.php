<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'arrival'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('operation', 0, array('a'=>'arrival', 'd'=>'add')),
	'edit'	=>	manage::check_permit('operation', 0, array('a'=>'arrival', 'd'=>'edit')),
	'del'	=>	manage::check_permit('operation', 0, array('a'=>'arrival', 'd'=>'del'))
);
?>

<div id="arrival" class="r_con_wrap">
	<script type="text/javascript">$(document).ready(function(){operation_obj.arrival_init()});</script>
	<div class="inside_container">
		<h1>{/module.operation.arrival/}</h1>
	</div>
	<div class="inside_table">
		<div class="list_menu">
			<div class="search_form">
				<form method="get" action="?">
					<div class="k_input">
						<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
						<input type="button" value="" class="more" />
					</div>
					<input type="submit" class="search_btn" value="{/global.search/}" />
					<input type="hidden" name="m" value="operation" />
					<input type="hidden" name="a" value="arrival" />
				</form>
			</div>
		</div>
		<?php
		$Keyword=str::str_code($_GET['Keyword']);
		$where='1';
		$Keyword && $where.=" and (p.Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(p.Prefix, p.Number) like '%$Keyword%' or p.SKU like '%$Keyword%' or u.Email like '%$Keyword%')";
		$arrival_row=str::str_code(db::get_limit_page('arrival_notice a left join products p on a.ProId=p.ProId left join user u on a.UserId=u.UserId', $where, "a.*, p.ProId, p.Name{$c['manage']['web_lang']}, u.Email", 'a.AId desc', (int)$_GET['page'], 20));
		
		if($arrival_row[0]){
		?>
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
				<thead>
					<tr>
						<td width="5%" nowrap="nowrap">{/global.serial/}</td>
						<td width="20%" nowrap="nowrap">{/products.product/}{/products.name/}</td>
						<td width="20%" nowrap="nowrap">{/global.email/}</td>
						<td width="15%" nowrap="nowrap">{/global.time/}</td>
						<?php /*<td width="10%" nowrap="nowrap">{/email.send_status/}</td>*/?>
						<td width="15%" nowrap="nowrap">{/email.send_time/}</td>
						<?php if($permit_ary['del']){?><td width="10%" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
					</tr>
				</thead>
				<tbody>
					<?php
					$i=1;
					foreach($arrival_row[0] as $v){
					?>
						<tr>
							<td nowrap="nowrap"><?=$newsletter_row[4]+$i++;?></td>
							<td><a href="<?=ly200::get_url($v, 'products');?>" target="_blank"><?=$v['Name'.$c['manage']['web_lang']];?></a></td>
							<td nowrap="nowrap">
								<a href="./?m=operation&a=email&d=send&Email=<?=$v['Email'];?>" class="btn_email"><i class="icon_email"></i></a>
								<span><?=$v['Email'];?></span>
							</td>
							<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
							<?php /*<td nowrap="nowrap"><?=$v['IsSend']?'<span class="fc_red">{/email.send_status_ary.1/}</span>':'{/email.send_status_ary.0/}';?></td>*/?>
							<td nowrap="nowrap"><?=$v['SendTime']?date('Y-m-d H:i:s', $v['SendTime']):'N/A';?></td>
							<?php if($permit_ary['del']){?>
								<td nowrap="nowrap" class="operation">
									<a class="del" href="./?do_action=operation.arrival_del&AId=<?=$v['AId'];?>" rel="del">{/global.del/}</a>
								</td>
							<?php }?>
						</tr>
					<?php }?>
				</tbody>
			</table>
			<?=html::turn_page($arrival_row[1], $arrival_row[2], $arrival_row[3], '?'.ly200::query_string('page').'&page=');?>
		<?php
		}else{//没有数据
			echo html::no_table_data(0);
		}?>
	</div>
</div>