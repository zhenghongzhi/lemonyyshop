<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'billing'));//检查权限
?>
<div id="billing" class="r_con_wrap">
	<div class="center_container">
		<div class="global_container">
			<script type="text/javascript">$(function(){operation_obj.billing_edit_init()});</script>
			<?php if($c['manage']['do']=='index'){?>
				<div class="big_title">{/module.operation.billing/}</div>
				<div class="blank20"></div>
			<?php }else{?>
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.operation.billing/}</span> 
					<span class="s_return">/ {/global.edit/}</span>
				</a>
			<?php }?>
			<?php
			$billing_row=db::get_all('billing', "1" ,'*', 'Device asc');
			$bill_pos_ary=array();
			foreach((array)$billing_row as $v){
				$bill_pos_ary[$v['Device']][]=$v['Position'];
			}
			if($c['manage']['do']=='index'){ 
			?>
				<div class="config_table_body">
					<?php
					foreach($billing_row as $k=>$v){		
					?>
						<div class="table_item" data-id="<?=$v['BId'];?>">
							<table border="0" cellpadding="5" cellspacing="0" class="config_table">
								<tbody>
									<tr>
										<td width="45%" nowrap="nowrap" class="myorder_left">
											<div class="name">{/operation.billing.device_ary.<?=$v['Device'];?>/}</div>
										</td>
										<td width="25%" nowrap="nowrap">
											<img src="/static/manage/images/operation/<?=$c['manage']['cdx_path']?>img_ad_pis_<?=$v['Position'];?>_cur.png" alt="">
										</td>
										<td width="15%" nowrap="nowrap">
											<a href="./?m=operation&a=billing&d=edit&BId=<?=$v['BId'];?>" class="edit">{/global.edit/}</a><br />
											<a href="./?do_action=operation.billing_del&BId=<?=$v['BId'];?>" class="del">{/global.del/}</a>
										</td>	
										<td width="10%" nowrap="nowrap" align="center" class="billing_used">
											<div class="switchery<?=$v['IsUsed']?' checked':'';?>" data-bid="<?=$v['BId'];?>">
												<input type="checkbox" name="IsUsed" value="1"<?=$v['IsUsed']?' checked':'';?>>
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										</td>				
									</tr>
								</tbody>
							</table>
						</div>
					<?php }?>
				</div>
				<?php 
				$billing_count=db::get_row_count('billing');
				$total_count=5;
				in_array('tuan', (array)$c['manage']['plugins']['Used']) &&  $total_count++;
				in_array('seckill', (array)$c['manage']['plugins']['Used']) &&  $total_count++;
				if($billing_count<$total_count){
				?>
					<a href="./?m=operation&a=billing&d=add" class="add set_add">{/global.add/}</a>
				<?php }?>
				<br /><br /><br />
			<?php
			}else{
				$BId=(int)$_GET['BId'];
				$billing_row=db::get_one('billing', "BId='$BId'");
				$billing_ary=array();
				$billing_ary['PicPath']=str::str_code(str::json_data($billing_row['PicPath'], 'decode'));
				$billing_ary['Content']=str::str_code(str::json_data($billing_row['Content'], 'decode'));
				echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
				?>
				<form id="billing_edit_form" class="global_form">
					<div class="rows clean">
						<label>{/operation.billing.device/}</label>
						<div class="input">
							<?php for($i=0;$i<2;$i++){ ?>
								<span class="input_radio_box <?=$billing_row['Device']==$i ? 'checked' : ''; ?>" data-position="<?=htmlspecialchars(str::json_data($bill_pos_ary[$i])); ?>">
									<span class="input_radio">
										<input type="radio" name="Device" value="<?=$i; ?>" <?=$billing_row['Device']==$i ? 'checked="checked"' : ''; ?>>
									</span>{/operation.billing.device_ary.<?=$i; ?>/}
								</span>&nbsp;&nbsp;&nbsp;
							<?php }?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/operation.billing.position/}</label>
						<div class="input">
							<?php 
								$position_ary=array(0,1,2,10,11,12);
								$only_pc=array(0,10,11,12);
								$only_mobile=array(2);
							?>
							<div class="ad_position">
								<?php foreach((array)$position_ary as $v){ 
									if(!in_array('tuan', (array)$c['manage']['plugins']['Used']) && $v==10) continue;
									if(!in_array('seckill', (array)$c['manage']['plugins']['Used']) && $v==11) continue;
									?>
									<div class="item item_<?=$v; ?> <?=in_array($v, $only_pc) ? 'pc' : ''; ?> <?=in_array($v, $only_mobile) ? 'mobile' : ''; ?> <?=$billing_row['Position']==$v && $BId ? 'cur used' : ''; ?>">
										{/operation.billing.position_ary.<?=$v; ?>/}
										<input type="radio" name="Position" value="<?=$v; ?>" <?=$billing_row['Position']==$v && $BId ? 'checked' : ''; ?> />
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="pic_box <?=$billing_row['Device']==0 && $billing_row['Position']==0 ? 'hide' : ''; ?>">
						<div class="rows clean">
							<label>
								{/global.pic/}
								<div class="tab_box"><?=manage::html_tab_button();?></div>
							</label>
							<div class="input">
								<?php
								foreach($c['manage']['config']['Language'] as $k=>$v){
								?>
		                            <div class="tab_txt tab_txt_<?=$v;?>"<?=$c['manage']['config']['LanguageDefault']==$v?'style="display:block;"':'';?>>
		                            	<?=manage::multi_img('PicDetail_'.$v, 'PicPath_'.$v, $billing_ary['PicPath'][$v]); ?>
									</div>
								<?php }?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/operation.billing.url/}</label>
							<div class="input">
								<input type="text" size="80" name="Url" value="<?=$billing_row['Url']; ?>" class="box_input" />
							</div>
						</div>
					</div>
					<div class="rows desc_box <?=$billing_row['Device']==0 && $billing_row['Position']==0 ? '' : 'hide'; ?> clean">
						<label>{/operation.billing.content/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?php foreach($c['manage']['config']['Language'] as $k=>$v){?>
								<div class="tab_txt tab_txt_<?=$v;?>" <?=$c['manage']['config']['LanguageDefault']==$v?'style="display:block;"':''; ?>><?=manage::Editor_Simple("Content_{$v}", $billing_ary['Content'][$v]);?></div>
							<?php }?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" name="submit_button" />
							<a href="./?m=operation&a=billing"><input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" /></a>
						</div>
					</div>
					<input type="hidden" name="OldDevice" value="<?=(int)$billing_row['Device'];?>" />
					<input type="hidden" name="BId" value="<?=$BId;?>" />
					<input type="hidden" name="do_action" value="operation.billing_edit" />
				</form>
			<?php }?>
		</div>
	</div>
</div>