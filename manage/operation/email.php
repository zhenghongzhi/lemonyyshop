<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'email'));//检查权限

if(!$c['manage']['do'] || $c['manage']['do']=='index'){//重新指向“邮件发送”页面
	$c['manage']['do']='send';
}

$Email=$_GET['Email'];
$top_id_name=(($c['manage']['do']=='email_logs' && $c['manage']['page']=='index')?'email_logs':'email');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<div class="inside_container">
		<h1>{/module.operation.email.module_name/}</h1>
		<ul class="inside_menu unusual">
			<?php
			$out=0;
			$open_ary=array();
			foreach($c['manage']['permit']['operation']['email']['menu'] as $k=>$v){
				if(!manage::check_permit('operation', 0, array('a'=>'email', 'd'=>$v)) || ($c['FunVersion']==0 && ($v=='send' || $v=='email_logs'))){//没权限，标准版
					if($v=='send' && $c['manage']['do']=='send') $out=1;
					continue;
				}else{
					$open_ary[]=$v;
				}
			?>
				<li><a href="./?m=operation&a=email&d=<?=$v;?>"<?=$c['manage']['do']==$v?' class="current"':'';?>>{/module.operation.email.<?=$v;?>/}</a></li>
			<?php
			}
			if($out) js::location('?m=operation&a=email&d='.$open_ary[0]);//当第一个选项没有权限打开，就跳转能打开的第一个页面
			?>
		</ul>
	</div>
	<?php
	if($c['manage']['do']=='send'){
		//邮件发送
		echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js', '/static/js/plugin/ckeditor/ckeditor.js');
	?>
		<script type="text/javascript">$(document).ready(function(){operation_obj.email_send_init()});</script>
		<div class="center_container_1200 clean">
			<form id="edit_form" class="global_form">
				<div class="left_container">
					<div class="left_container_side">
						<div class="global_container">
							<div class="rows clean">
								<label>{/email.subject/}</label>
								<div class="input"><input type="text" class="box_input" name="Subject" value="" size="53" maxlength="50" notnull /></div>
							</div>
							<?php if($c['FunVersion']>=1){?>
								<div class="rows clean">
									<label>{/email.templates/}</label>
									<div class="input">
                                    	<?php if($c['FunVersion']!=10){?>
										<input type="button" class="btn_global btn_tpl_choice" value="{/email.email_tpl/}" />
										<input type="button" class="btn_global btn_tpl_save" value="{/email.save_email_tpl/}" />
                                        <?php }?>
										<div class="blank12"></div>
										<?=manage::Editor('Content', '', false);?>
									</div>
								</div>
							<?php }?>
						</div>
					</div>
				</div>
				<div class="right_container">
					<div class="global_container">
						<div class="rows clean">
							<label>{/email.addressee/}</label>
							<div class="input">
								<?php
								$SelectAry=array(
									'user'=>array('Name'=>'会员', 'Type'=>'user', 'Table'=>'user', 'All'=>0, 'CheckAll'=>1),
									'level'=>array('Name'=>'等级', 'Type'=>'level', 'Table'=>'user_level', 'All'=>0, 'CheckAll'=>1),
									'newsletter'=>array('Name'=>'订阅', 'Type'=>'newsletter', 'Table'=>'newsletter', 'All'=>0, 'CheckAll'=>1)
								);
								$isCheckbox=0;
								if(in_array('massive_email', $c['manage']['plugins']['Used'])){//开启邮件群发功能
									$isCheckbox=1;
								}else{
									unset($SelectAry['level']);//踢走等级
								}
								if($Email){
									$ValueAry=array(0=>array('Name'=>$Email, 'Value'=>(int)db::get_value('user', "Email='$Email'", 'UserId'), 'Type'=>'user'));
								}
								echo manage::box_drop_double('EmailSelect', 'To', $SelectAry, $ValueAry, 0, '', $isCheckbox);
								?>
                                <div class="icon_tips">{/email.plugins.tips/}<a href="?m=plugins&a=app&ClassName=massive_email">{/email.plugins.name/}</a><br>{/operation.explain.email/}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="rows">
					<div class="center_container_1200">
						<div class="input clean">
							<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.send/}" />
						</div>
					</div>
				</div>
				<input type="hidden" name="Arrival" value="<?=$_GET['Arrival'];?>" />
				<input type="hidden" name="do_action" value="operation.email_send" />
			</form>
			<div id="fixed_right">
				<div class="global_container fixed_template_save">
					<div class="top_title">{/email.save_email_tpl/} <a href="javascript:;" class="close"></a></div>
					<form class="global_form" id="template_save_form">
						<div class="rows clean">
							<label>{/global.title/}</label>
							<div class="input">
								<input type="text" class="box_input" name="Title" size="30" maxlength="50" autocomplete="off" notnull />
							</div>
						</div>
						<div class="rows clean">
							<label>{/global.pic/}</label>
							<div class="input">
								<?=manage::multi_img('PicDetail', 'PicPath'); ?>
							</div>
						</div>
						<div class="rows clean box_button">
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
								<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
							</div>
						</div>
						<textarea name="Content" class="hide"></textarea>
						<input type="hidden" name="do_action" value="operation.email_template_save" />
					</form>
				</div>
				<div class="global_container fixed_template_choice">
					<div class="top_title">{/email.email_tpl/} 
                    	<a href="javascript:;" class="close"></a>
                        <ul class="top_menu">
                            <?php
							$type=array(//邮件模板分类，键名与目录分类文件夹同名，且必须为英文
								'promotions'	=>	$c['manage']['lang_pack']['email']['email_tpl_class']['promotions'],//促销模板
								'festival'		=>	$c['manage']['lang_pack']['email']['email_tpl_class']['festival'],//节日模板
								'invitation'	=>	$c['manage']['lang_pack']['email']['email_tpl_class']['invitation'],//邀请函模板
							);
                            $i=0;
                            foreach($type as $k=>$v){
                                echo '<li><a href="javascript:;" data-type="'.$k.'"'.($i==0?' class="current"':'').'>'.$v.'</a></li>';
                                ++$i;
                            }
                            echo '<li><a href="javascript:;" data-type="customize">{/email.email_tpl_class.customize/}</a></li>';
                            ?>
                        </ul>
                    </div>
					<form class="global_form" id="template_choice_form">
						<div class="template_choice_list">
							<?php
							foreach($type as $k=>$v){
							?>
								<div class="list clean" data-type="<?=$k;?>">
									<?php
									//读取模板目录
									$tpl_dir=$c['manage']['email_tpl_dir'].$k.'/';//模板目录
									$base_dir=$c['root_path'].$tpl_dir;//邮件模板目录绝对路径
									$handle=opendir($base_dir);
									while($tpl=readdir($handle)){
										if($tpl!='.' && $tpl!='..' && is_dir($base_dir.$tpl)){
											$tpl_img=$tpl_dir.$tpl.'/cover.jpg';//封面
											$tpl_name_file=$base_dir.$tpl.'/name.txt';//模板名
											if(!file_exists($base_dir.$tpl.'/template.html')) continue;//模板不存在，跳过
											if(file_exists($tpl_name_file)) $tpl_name=iconv("gbk", "UTF-8", file_get_contents($tpl_name_file));
									?>
											<div class="item" data-tpl="<?=$tpl;?>">
												<div class="img"><img src="<?=$tpl_img;?>" /><span></span><div class="img_mask"></div></div>
												<div class="name"><?=$tpl_name;?></div>
											</div>
									<?php
										}
									}
									closedir($handle);
									?>
								</div>
							<?php }?>
							<div class="list clean" data-type="customize">
								<?php
								$email_list_row=db::get_all('email_list', '1');
								foreach($email_list_row as $v){
								?>
									<div class="item" data-tpl="<?=$v['EId'];?>">
										<div class="img"><img src="<?=$v['PicPath'];?>" /><span></span><div class="img_mask"></div><a href="javascript:;" class="del_mask" rel="del">{/global.del/}</a></div>
										<div class="name"><?=$v['Title'];?></div>
									</div>
								<?php }?>
							</div>
						</div>
						<div class="rows clean box_button">
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
								<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
							</div>
						</div>
						<input type="hidden" name="Template" value="" />
						<input type="hidden" name="Type" value="" />
						<input type="hidden" name="do_action" value="operation.email_template_choice" />
					</form>
				</div>
			</div>
		</div>
	<?php
	}elseif($c['manage']['do']=='email_logs'){
		if($c['manage']['page']=='view'){ 
			//邮件发送记录查看
			$LId=(int)$_GET['LId'];
			$log_row=db::get_one('email_log', "LId='$LId'");
	?>
			<div class="global_form center_container_1200">
				<div class="left_container">
					<div class="left_container_side">
						<div class="global_container">
							<a href="javascript:history.back(-1);" class="return_title">
								<span class="return">{/module.operation.email.email_logs/}</span> 
								<span class="s_return">/ {/global.view/}</span>
							</a>
							<div class="rows">
								<label>{/email.email_logs.content/}</label>
								<div class="input email_content"><?=$log_row['Body'];?></div>
								<?php /*<span class="input"><?=manage::Editor("Body", $log_row['Body']);?></span>*/?>
							</div>
						</div>
					</div>
				</div>
				<div class="right_container">
					<div class="global_container">
						<div class="rows">
							<label>{/email.email_logs.to_email/}</label>
							<div class="input"><?=$log_row['Email'];?></div>								
						</div>
						<div class="rows">
							<label>{/email.email_logs.status/}</label>
							<div class="input">{/email.email_logs.status_ary.0/}</div>								
						</div>
						<div class="rows">
							<label>{/global.time/}</label>
							<div class="input"><?=date('Y-m-d H:i:s', $log_row['AccTime']);?></div>								
						</div>
						<div class="rows">
							<label>{/email.email_logs.subject/}</label>
							<div class="input"><?=$log_row['Subject'];?></div>								
						</div>
					</div>
				</div>
			</div>
		<?php
		}else{
		?>
			<div class="inside_table center_container_1000">
				<?php
				$w='1';
				$UserId=$_GET['UserId'];
				$Keyword=$_GET['Keyword'];
				$Module=$_GET['Module'];
				$UserId && $w.=" and UserId='$UserId'";
				$Module && $w.=" and Module='$Module'";
				$Keyword && $w.=" and Log like '%$Keyword%'";
				$manage_logs_row=db::get_limit_page('email_log', $w, '*', 'LId desc', (int)$_GET['page'], 20);
				
				if($manage_logs_row[0]){
				?>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="5%" nowrap="nowrap">{/global.serial/}</td>
								<td width="15%" nowrap="nowrap">{/email.email_logs.to_email/}</td>
								<td width="30%" nowrap="nowrap">{/email.email_logs.subject/}</td>
								<td width="10%" nowrap="nowrap">{/email.email_logs.status/}</td>
								<td width="10%" nowrap="nowrap">{/global.time/}</td>
								<td width="10%" nowrap="nowrap" class="operation">{/global.operation/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach($manage_logs_row[0] as $v){
							?>
								<tr>
									<td nowrap="nowrap"><?=$manage_logs_row[4]+$i++;?></td>
									<td nowrap="nowrap"><?=$v['Email'];?></td>
									<td nowrap="nowrap"><?=$v['Subject'];?></td>
									<td nowrap="nowrap">{/email.email_logs.status_ary.0/}</td>
									<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
									<td nowrap="nowrap" class="operation">
										<a class="" href="./?m=operation&a=email&d=email_logs&p=view&LId=<?=$v['LId'];?>">{/global.view/}</a>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
					<?=html::turn_page($manage_logs_row[1], $manage_logs_row[2], $manage_logs_row[3], '?'.ly200::query_string('page').'&page=');?>
				<?php
				}else{//没有数据
					echo html::no_table_data(0);
				}?>
			</div>
	<?php
		}
	}?>
</div>