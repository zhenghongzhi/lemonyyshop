<?php !isset($c) && exit();?>
<?php
manage::check_permit('operation', 1, array('a'=>'page'));//检查权限

$out=0;
$open_ary=array();
foreach($c['manage']['permit']['operation']['page']['menu'] as $k=>$v){
	if(!manage::check_permit('operation', 0, array('a'=>'page', 'd'=>$v))){
		if($v=='page' && $c['manage']['do']=='index') $out=1;
		continue;
	}else{
		$v=='page' && $v='index';
		$open_ary[]=$v;
	}
}
if($out) js::location('?m=operation&a=page&d='.$open_ary[0]);//当第一个选项没有权限打开，就跳转能打开的第一个页面

if($c['manage']['do']=='index' || $c['manage']['do']=='edit' || $c['manage']['do']=='category'|| $c['manage']['do']=='category_edit'){
	$cate_ary=str::str_code(db::get_all('article_category', '1'));//获取类别列表
	$category_ary=array();
	foreach((array)$cate_ary as $v){
		$category_ary[$v['CateId']]=$v;
	}
	$category_count=count($category_ary);
	unset($cate_ary);
	
	$CateId=(int)$_GET['CateId'];
	if($CateId){
		$category_one=str::str_code(db::get_one('article_category', "CateId='$CateId'"));
		!$category_one && js::location('./?m=operation&a=page');
		$UId=$category_one['UId'];
		$column=$category_one['Category'.$c['manage']['web_lang']];
	}
	
	$Keyword=str::str_code($_GET['Keyword']);
}

$permit_ary=array(
	'add'		=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'category', 'p'=>'add')),
	'edit'		=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'category', 'p'=>'edit')),
	'del'		=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'category', 'p'=>'del')),
	'page_add'	=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'page', 'p'=>'add')),
	'page_edit'	=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'page', 'p'=>'edit')),
	'page_del'	=>	manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'page', 'p'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'||$c['manage']['do']=='category'?'page':'page_inside');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php if($c['manage']['do']=='index' || $c['manage']['do']=='category'){?>
		<div class="inside_container">
			<h1>{/module.operation.page.module_name/}</h1>
			<ul class="inside_menu unusual">			
				<?php if(manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'page'))){?>
					<li><a href="./?m=operation&a=page"<?=($c['manage']['do']=='index' || $c['manage']['do']=='edit')?' class="current"':'';?>>{/page.page.page/}<?=$column?" ($column)":'';?></a></li>
				<?php }?>
				<?php if(manage::check_permit('operation', 0, array('a'=>'page', 'd'=>'category'))){?>
					<li><a href="./?m=operation&a=page&d=category"<?=($c['manage']['do']=='category' || $c['manage']['do']=='category_edit')?' class="current"':'';?>>{/global.category/}</a></li>
				<?php }?>
			</ul>
		</div>
	<?php }?>
	<?php
	if($c['manage']['do']=='index'){
		//单页列表
	?>
		<script type="text/javascript">$(document).ready(function(){operation_obj.page_init()});</script>
		<div class="inside_table center_container_1000">
			<div class="list_menu">
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'article_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="operation" />
						<input type="hidden" name="a" value="page" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if($permit_ary['page_add']){?><li><a class="add" href="./?m=operation&a=page&d=edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['page_del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
			</div>
			<div class="clear"></div>
			<?php
			$where='1';//条件
			$page_count=20;//显示数量
			$Keyword && $where.=" and Title{$c['manage']['web_lang']} like '%$Keyword%'";
			$CateId && $where.=" and CateId='$CateId'";
			$page_row=str::str_code(db::get_limit_page('article', $where, '*', 'CateId>1, '.$c['my_order'].'AId desc', (int)$_GET['page'], $page_count));
			
			if($page_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['page_edit'] || $permit_ary['page_del']){?>
								<td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td>
							<?php }?>
							<td width="41%" nowrap="nowrap">{/global.title/}</td>
							<td width="40%" nowrap="nowrap">{/global.category/}{/global.subjection/}</td>
							<td width="10%" nowrap="nowrap">{/global.my_order/}</td>
							<?php if($permit_ary['page_edit'] || $permit_ary['page_del']){?>
								<td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
							<?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach((array)$page_row[0] as $v){
							$title=$v['Title'.$c['manage']['web_lang']];
							$url=ly200::get_url($v, 'article', $c['manage']['web_lang']);
						?>
							<tr>
								<?php if($permit_ary['page_edit'] || $permit_ary['page_del']){?>
									<td nowrap="nowrap"><?=html::btn_checkbox('select', $v['AId']);?></td>
								<?php }?>
								<td><a href="<?=$url;?>" title="<?=$title;?>" target="_blank"><?=$title;?></a></td>
								<td class="category_select" cateid="<?=$v['CateId'];?>">
									<?php
									$UId=$category_ary[$v['CateId']]['UId'];
									if($UId){
										$key_ary=@explode(',',$UId);
										array_shift($key_ary);
										array_pop($key_ary);
										foreach((array)$key_ary as $k2=>$v2){
											echo $category_ary[$v2]['Category'.$c['manage']['web_lang']];
										}
									}
									echo $category_ary[$v['CateId']]['Category'.$c['manage']['web_lang']];
									?>
								</td>
								<td nowrap="nowrap"><div  class="myorder_select" data-num="<?=$v['MyOrder'];?>"><span class="ajax_upload_btn"><?=$c['manage']['my_order'][$v['MyOrder']];?></span></div></td>
								<?php if($permit_ary['page_edit'] || $permit_ary['page_del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['page_edit']){?><a class="tip_min_ico" href="./?m=operation&a=page&d=edit&AId=<?=$v['AId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['page_del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['page_del']){?><a class="del item" href="./?do_action=operation.page_del&AId=<?=$v['AId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($page_row[1], $page_row[2], $page_row[3], '?'.ly200::query_string('page').'&page=');?>
				<div id="myorder_select_hide" class="hide"><div class="box_select"><?=ly200::form_select($c['manage']['my_order'], "MyOrder[]", '');?></div></div>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=operation&a=page&d=edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='edit'){
		//单页编辑
		$AId=(int)$_GET['AId'];
		if($AId){
			$page_row=str::str_code(db::get_one('article', "AId='$AId'"));
			$page_content_row=str::str_code(db::get_one('article_content', "AId='$AId'"));
		}
		echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
	?>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.operation.page.module_name/}</span> 
				<span class="s_return">/ {/page.page.page/} / <?=$AId?'{/global.edit/}':'{/global.add/}';?></span>
			</a>
		</div>
		<script type="text/javascript">$(document).ready(function(){operation_obj.page_edit_init()});</script>
		<form id="edit_form" class="global_form wrap_content center_container_1200">
			<div class="left_container">
				<div class="left_container_side">
					<div class="global_container">
						<div class="rows clean">
							<label>{/page.title/}<div class="tab_box"><?=manage::html_tab_button('border');?></div></label>
							<div class="input"><?=manage::form_edit($page_row, 'text', 'Title', 53, 150, 'notnull');?></div>				
						</div>
						<div class="rows clean">
							<label>{/page.classify/}</label>
							<div class="input">
								<div class="box_select"><?=category::ouput_Category_to_Select('CateId', $page_row['CateId'], 'article_category', 'UId="0," and CateId!=1', 1, 'notnull class="box_input"', '{/global.select_index/}');?></div>				
							</div>
						</div>
						<div class="rows clean">
							<label>{/page.page.external_links/}</label>
							<div class="input"><input name="Url" value="<?=$page_row['Url'];?>" type="text" class="box_input" size="53" maxlength="150" /><span class="tool_tips_ico" content="{/page.page.links_notes/}"></span></div>				
						</div>
						<div class="rows clean myorder_rows">
							<label>{/products.myorder/}</label>
							<div class="input">
								<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $page_row['MyOrder'], '', '', '', 'class="box_input"');?></div>
							</div>
						</div>
						<div class="rows clean">
							<label>{/page.page.description/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
							<span class="input">
								<?=manage::form_edit($page_content_row, 'editor', 'Content');?>
							</span>
							<div class="clear"></div>
						</div>
						<div class="rows clean">
							<div class="center_container_1200">
								<div class="input">
									<input type="button" class="btn_global btn_submit" value="{/global.submit/}">
									<a href="./?m=operation&a=page"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="right_container">
				<div class="global_container">
					<div class="big_title">SEO</div>
					<div class="rows clean">
						<label>{/news.news.seo_title/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($page_row, 'text', 'SeoTitle', 47, 255, '');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/news.news.seo_keyword/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($page_row, 'textarea', 'SeoKeyword');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/news.news.seo_brief/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($page_row, 'textarea', 'SeoDescription');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/page.page.custom_url/}</label>
						<div class="input">
							<textarea name='PageUrl' class="box_textarea"><?=$page_row['PageUrl'];?></textarea>
						</div>				
					</div>
				</div>
			</div>
			<input type="hidden" id="AId" name="AId" value="<?=$AId;?>" />
			<input type="hidden" name="do_action" value="operation.page_edit" />
		</form>
	<?php
	}elseif($c['manage']['do']=='category'){
		//单页分类列表
		echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
	?>
		<script type="text/javascript">$(document).ready(function(){operation_obj.page_category_init()});</script>
		<div class="inside_table center_container_1000">
			<div class="list_menu">
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="clear"></div>
						<input type="hidden" name="m" value="operation" />
						<input type="hidden" name="a" value="page" />
						<input type="hidden" name="d" value="category" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=operation&a=page&d=category_edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
			</div>
			<div class="clear"></div>
			<?php
			$where='1';//条件
			$Keyword && $where.=" and Category{$c['manage']['web_lang']} like '%$Keyword%'";
			$category_row=str::str_code(db::get_all('article_category', $where, '*', $c['my_order'].'CateId asc'));
			
			if($category_row){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit']){?>
								<td width="1%" nowrap="nowrap" class="myorder"></td>
							<?php }?>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td>
							<?php }?>
							<td width="87%" nowrap="nowrap">{/global.category/}{/global.name/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
							<?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($category_row as $v){
						?>
							<tr data-id="<?=$v['CateId'];?>">
								<?php if($permit_ary['edit']){?>
									<td nowrap="nowrap" class="myorder move_myorder" data="move_myorder"><i class="icon_myorder"></i></td>
								<?php }?>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap"><?=html::btn_checkbox('select', $v['CateId']);?></td>
								<?php }?>
								<td><?=$v['Category'.$c['manage']['web_lang']];?><?=$v['IsHelp']==1?'&nbsp;&nbsp;<span class="fc_red">{/page.page.help_center/}</span>':'';?><?php if($v['IsIndex']==1 && in_array($c['manage']['web_themes'], array('t008', 't012', 't068'))){ ?>&nbsp;&nbsp;<span class="fc_red">{/global.isindex/}</span><?php } ?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a class="" href="./?m=operation&a=page&d=category_edit&CateId=<?=$v['CateId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=operation.page_category_del&CateId=<?=$v['CateId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=operation&a=page&d=category_edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='category_edit'){
		//单页分类编辑
	?>
		<script type="text/javascript">$(document).ready(function(){operation_obj.page_category_edit_init()});</script>
		<div class="center_container_1000">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.operation.page.module_name/}</span> 
				<span class="s_return">/ {/products.classify/} / <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
			</a>
			<div class="global_container">
				<form id="cate_edit_form" class="global_form wrap_content">
					<div class="rows">
						<label>{/page.title/}<div class="tab_box"><?=manage::html_tab_button('border');?></div></label>
						<div class="input"><?=manage::form_edit($category_one, 'text', 'Category', 35, 50, 'notnull');?></div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<span class="input_checkbox_box <?=$category_one['IsHelp']==1?'checked':'';?>">
								<span class="input_checkbox">
									<input type="checkbox" name="IsHelp" <?=$category_one['IsHelp']==1?'checked':'';?> value="1">
								</span>{/page.page.help_center/}
							</span>
							<?php if(in_array($c['manage']['web_themes'], array('t008', 't012', 't068'))){ ?>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="input_checkbox_box <?=$category_one['IsIndex']==1?'checked':'';?>">
									<span class="input_checkbox">
										<input type="checkbox" name="IsIndex" <?=$category_one['IsIndex']==1?'checked':'';?> value="1">
									</span>{/global.isindex/}
								</span>
							<?php } ?>
						</div>
					</div>
					<?php
					/* 暂时屏蔽
					<div class="rows">
						<label>
							{/global.seo/}
							<div class="tab_box"><?=manage::html_tab_button('border');?></div>
						</label>
						<div class="input">
							<?php foreach($c['manage']['config']['Language'] as $k=>$v){?>
								<div class="tab_txt tab_txt_<?=$v;?>">
									<span class="unit_input lang_input"><b>{/news.news.seo_title/}<div class='arrow'><em></em><i></i></div></b><input type="text" name="SeoTitle_<?=$v;?>" value="<?=htmlspecialchars(htmlspecialchars_decode($category_one["SeoTitle_{$v}"]), ENT_QUOTES);?>" class="box_input" size="35" maxlength="150" /></span>
									<div class="blank9"></div>
									<span class="unit_input lang_input"><b>{/news.news.seo_keyword/}<div class='arrow'><em></em><i></i></div></b><input type="text" name="SeoKeyword_<?=$v;?>" value="<?=htmlspecialchars(htmlspecialchars_decode($category_one["SeoKeyword_{$v}"]), ENT_QUOTES);?>" class="box_input" size="35" maxlength="255" /></span>
									<div class="blank9"></div>
									<span class='unit_input lang_input'><b>{/news.news.seo_brief/}<div class='arrow'><em></em><i></i></div></b><textarea name='SeoDescription_<?=$v;?>' class="box_textarea"><?=$category_one["SeoDescription_{$v}"];?></textarea></span>
								</div>
							<?php }?>
						</div>
					</div>
					*/
					?>
					<div class="rows clean">
						<label></label>
						<div class="input">
							<input type="button" class="btn_global btn_submit" value="{/global.submit/}">
							<a href="./?m=operation&a=page&d=category"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>
					</div>
					<input type="hidden" name="CateId" value="<?=$CateId;?>" />
					<input type="hidden" name="do_action" value="operation.page_category_edit">
				</form>
			</div>
		</div>
	<?php }?>
</div>