<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'promotion'));//检查权限
if(!in_array('promotion', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

$lang=$c['manage']['web_lang'];

$permit_ary=array(
	'add'	=>	manage::check_permit('sales', 0, array('a'=>'promotion', 'd'=>'add')),
	'edit'	=>	manage::check_permit('sales', 0, array('a'=>'promotion', 'd'=>'edit')),
	'del'	=>	manage::check_permit('sales', 0, array('a'=>'promotion', 'd'=>'del'))
);

echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
?>
<script type="text/javascript">var lang_str_obj={'currency':'<?=$c['manage']['currency_symbol'];?>', 'now_time':'<?=date('Y-m-d H:i', $c['time']);?>'};</script>
<div id="sales" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php
	if($c['manage']['do']=='index'){
		//组合促销列表
		?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.promotion_list_init()});</script>
		<div class="inside_container">
			<h1>{/module.sales.promotion/}</h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=sales&a=promotion&d=edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
				<div class="search_form" style="width:auto;">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<input type="hidden" name="m" value="sales" />
						<input type="hidden" name="a" value="promotion" />
					</form>
				</div>
			</div>
			<?php
			//获取类别列表
			$cate_ary=str::str_code(db::get_all('products_category','1','*'));
			$category_ary=array();
			foreach((array)$cate_ary as $v){
				$category_ary[$v['CateId']]=$v;
			}
			$category_count=count($category_ary);
			unset($cate_ary);
			
			//列表
			$Keyword=str::str_code($_GET['Keyword']);
			
			$where='Type=1';//条件
			$page_count=10;//显示数量
			$Keyword && $where.=" and ProId in(select ProId from products where Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(Prefix, Number) like '%$Keyword%')";
			$promotion_row=str::str_code(db::get_limit_page('sales_package', $where, '*', 'PId desc', (int)$_GET['page'], $page_count));
			
			$pro_where='ProId in(0';
			foreach($promotion_row[0] as $v) $pro_where.=",{$v['ProId']}".str_replace('|', ',', substr($v['PackageProId'], 0, -1));
			$pro_where.=')';
			$pro_ary=array();
			$pro_row=str::str_code(db::get_all('products', $pro_where, '*', 'ProId desc'));
			foreach($pro_row as $v) $pro_ary[$v['ProId']]=$v;
			
			if($promotion_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="15%" nowrap="nowrap">{/user.info.subject/}</td>
							<td width="45%" nowrap="nowrap">{/sales.package.product_info/}</td>
							<td width="10%" nowrap="nowrap">{/sales.package.promotion_price/}</td>
							<td width="10%" nowrap="nowrap">{/sales.package.reverse_associate/}</td>
							<td width="15%" nowrap="nowrap">{/global.time/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($promotion_row[0] as $v){
							$img=ly200::get_size_img($pro_ary[$v['ProId']]['PicPath_0'], '240x240');
							$name=$pro_ary[$v['ProId']]['Name'.$c['manage']['web_lang']];
							$promotion_ary=explode('|', substr($v['PackageProId'], 1, -1));
							$price=manage::range_price($pro_ary[$v['ProId']], 1);
							$biref=$pro_ary[$v['ProId']]['BriefDescription'.$c['manage']['web_lang']];
							$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
						?>
						<tr>
							<?php if($permit_ary['del']){?><td class="va_t"><?=html::btn_checkbox('select', $v['PId']);?></td><?php }?>
							<td><?=$v['Name'];?></td>
							<td class="left is_main">
								<div class="p_row fl">
									<div class="p_img fl"><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" alt="<?=$name;?>" price="<?=$price;?>" number="<?=$pro_ary[$v['ProId']]['Number'];?>" biref="<?=$biref;?>" align="absmiddle" height="50" /></a></div>
									<div class="p_info">
										<a class="p_name" href="<?=$url;?>" target="_blank"><?=$name;?></a>
									</div>
									<div class="clear"></div>
								</div>
								<ul class="p_list fl">
									<?php
									foreach((array)$promotion_ary as $v2){
										if(!$pro_ary[$v2]) continue;
										$img=ly200::get_size_img($pro_ary[$v2]['PicPath_0'], '240x240');
										$name=$pro_ary[$v2]['Name'.$c['manage']['web_lang']];
										$price=manage::range_price($pro_ary[$v2], 1);
										$biref=$pro_ary[$v2]['BriefDescription'.$c['manage']['web_lang']];
									?>
									<li>
										<div class="p_img fl"><a href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" alt="<?=$name;?>" price="<?=$price;?>" number="<?=$pro_ary[$v['ProId']]['Number'];?>" biref="<?=$biref;?>" align="absmiddle" height="50" /></a></div>
										<a href="<?=ly200::get_url($pro_ary[$v2], 'products', $c['manage']['web_lang']);?>" target="_blank" img="<?=$img;?>" price="<?=$price;?>" number="<?=$pro_ary[$v2]['Number'];?>" biref="<?=$biref;?>"><?=$name;?></a>
										<div class="clear"></div>
									</li>
									<?php }?>
								</ul>
								<div class="clear"></div>
							</td>
							<td><?=$c['manage']['currency_symbol'].sprintf('%01.2f', $v['CurPrice']);?></td>
							<td><?=$v['ReverseAssociate']?'<span class="fc_red">{/global.n_y.1/}</span>':'{/global.n_y.0/}';?></td>
							<td><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td nowrap="nowrap" class="operation side_by_side">
									<?php if($permit_ary['edit']){?><a href="./?m=sales&a=promotion&d=edit&PId=<?=$v['PId'];?>">{/global.edit/}</a><?php }?>
									<?php if($permit_ary['del']){?>
										<dl>
											<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
											<dd class="drop_down">
												<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=sales.promotion_del&PId=<?=$v['PId'];?>" rel="del">{/global.del/}</a><?php }?>
											</dd>
										</dl>
									<?php }?>
								</td>
							<?php }?>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($promotion_row[1], $promotion_row[2], $promotion_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=sales&a=promotion&d=edit');
			}?>
		</div>
	<?php
	}else{
		//组合促销编辑
		$PId=(int)$_GET['PId'];
		$promotion_row=str::str_code(db::get_one('sales_package', "PId='$PId'"));
		$ProId=$promotion_row['ProId'];
		$PackageProId=$promotion_row['PackageProId'];
		
		$PackageProId && $data_ary=str::json_data(htmlspecialchars_decode($promotion_row['Data']), 'decode');
		if($PackageProId){
			$pro_ary=array();
			$PackageProId!='|' && $p_where=','.str_replace('|', ',', substr($PackageProId, 1, -1));
			$pro_row=str::str_code(db::get_all('products', "ProId in({$ProId}{$p_where})", '*', 'ProId desc'));
			foreach($pro_row as $v) $pro_ary[$v['ProId']]=$v;
			$promotion_ary=explode('|', substr($PackageProId, 1, -1));
			array_unshift($promotion_ary,$ProId);
			$promotion_where_ary = $promotion_ary;
		}
		$p_remove_pid=$_POST['remove_pid'] ? $_POST['remove_pid'] : $_GET['remove_pid'];
		$p_remove_pid || $p_remove_pid=$promotion_row['PackageProId'].$ProId;
		$p_remove_pid=str::ary_format($p_remove_pid, 2, '', '|');
		
		$where='1';
		$page_count=30;//显示数量
		$Name=str::str_code($_GET['Keyword']);
		$CateId=(int)$_GET['CateId'];
		$Name && $where.=" and (Name{$c['manage']['web_lang']} like '%$Name%' or concat(Prefix, Number) like '%$Name%')";
		$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}
		$where.=$c['manage']['where']['products'];
		$row_count=db::get_row_count('products',$where);
		$total_pages=ceil($row_count/$page_count);
		$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
		$selected_where='ProId in(-1';
		foreach((array)$products_row[0] as $v){
			$selected_where.=",{$v['ProId']}";
		}
		foreach((array)$pro_row as $v){
			$selected_where.=",{$v['ProId']}";
		}
		$selected_where.=')';
		
		//列出所有购物车属性
		$parent_ary=$all_attr_ary=$all_value_ary=$vid_data_ary=$selected_ary=array();
		$cart_attr_row=str::str_code(db::get_all('products_attribute', 'CateId!="" and CartAttr=1', "AttrId, Name{$lang}, CateId", $c['my_order'].'AttrId asc')); //所有购物车属性
		$_attribute_value_where='-1';
		foreach((array)$cart_attr_row as $v){
			$sCateId=explode('|', trim($v['CateId'], '|'));
			foreach($sCateId as $v2){
				$parent_ary[$v2][]=$v['AttrId'];
			}
			$all_attr_ary[$v['AttrId']]=$v;
			$_attribute_value_where.=",{$v['AttrId']}";
		}
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in($_attribute_value_where)", '*', $c['my_order'].'VId asc')); //所有属性选项
		foreach($value_row as $v){
			$all_value_ary[$v['AttrId']][$v['VId']]=$v;
			$vid_data_ary[$v['VId']]=$v;
		}
		$selected_row=str::str_code(db::get_all('products_selected_attribute', "IsUsed=1 and {$selected_where} and AttrId in($_attribute_value_where, 0)", 'SeleteId, ProId, AttrId, VId, OvId', 'SeleteId asc'));
		foreach($selected_row as $v){
			$selected_ary[$v['ProId']]['Id'][$v['AttrId']][]=$v['VId'];
			$v['AttrId']==0 && $v['VId']==0 && $v['OvId']>=0 && $selected_ary[$v['ProId']]['Overseas'][]=$v['OvId']; //记录勾选属性ID 发货地
		}
		
		//发货地
		$overseas_ary=array();
		$overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
		foreach($overseas_row as $v){
			$overseas_ary[$v['OvId']]=$v;
		}
		echo ly200::load_static('/static/js/plugin/drag/drag.js');
	?>
    <script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init()});</script>
    <div class="inside_container">
		<h1>{/module.sales.promotion/}</h1>
	</div>
	<div class="sales_content">
		<form id="edit_form" class="global_form" data-return="./?m=sales&a=promotion">
			<div class="date_rows">
				<a href="javascript:;" class="set_add fr">{/global.add/}</a>
				<input name="Name" value="<?=$promotion_row['Name'];?>" type="text" class="box_input" placeholder="{/user.info.subject/}" size="40" maxlength="255" notnull />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="tit">{/sales.package.promotion_price/}</span>
				<span class="unit_input"><b><?=$c['manage']['currency_symbol'];?></b><input type="text" class="box_input" name="CurPrice" size="5" maxlength="255" value="<?=(float)$promotion_row['CurPrice'];?>" rel="amount" notnull=""></span>

				&nbsp;
				<span class="input_checkbox_box <?=$promotion_row['ReverseAssociate']?' checked':'';?>">
					<span class="input_checkbox">
						<input type="checkbox" name="ReverseAssociate" value="1" <?=$promotion_row['ReverseAssociate']?' checked':'';?> />
					</span>{/sales.package.reverse_associate/}
				</span>
				&nbsp;&nbsp;<span class="box_explain">{/sales.explain.package/}</span>
			</div>
			<div class="set_sales_list">
				<?php
				if($PackageProId){				
					foreach((array)$promotion_ary as $v){
					$proid=$v;
					$row=$pro_ary[$proid];
					$url=ly200::get_url($row, 'products');
					$img=ly200::get_size_img($row['PicPath_0'], '240x240');
					$name=$row['Name'.$lang];
					$attr_ext_ary=array();
					isset($data_ary[$proid]) && is_array($data_ary[$proid]) && $attr_ext_ary=$data_ary[$proid];
					?>
					<div class="set_sales_item" data-proid="<?=$proid; ?>">
						<div class="edit_box">
							<a href="javascript:;" class="main <?=$proid==$ProId ? 'p_main' : ''; ?>"></a>
							<a href="javascript:;" class="del" type="<?=$proid==$ProId ? 'main' : 'related'; ?>"></a>
						</div>
						<div class="pic">
							<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
							<span></span>
						</div>
						<div class="content">
							<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
							<div class="attr_list">
								<div class="attr_abs">
									<?php
									foreach((array)$parent_ary[$row['CateId']] as $k2=>$v2){
										if(!$selected_ary[$proid]['Id'][$v2]) continue;
									?>
										<div class="box_select" parent_null>
											<select name="Attr_<?=$proid;?>[<?=$v2;?>]" attr="<?=$v2;?>" parent_null="1" notnull>
												<option value="">--<?=$all_attr_ary[$v2]['Name'.$lang];?>--</option>
												<?php
												foreach((array)$all_value_ary[$v2] as $k3=>$v3){
													if(!in_array($k3, $selected_ary[$proid]['Id'][$v2])) continue;
												?>
													<option value="<?=$k3;?>"<?=in_array($k3, $attr_ext_ary)?' selected':'';?>><?=$vid_data_ary[$k3]['Value'.$lang];?></option>
												<?php }?>
											</select>
										</div>
									<?php
									}
									if(count($overseas_ary)>0 && $selected_ary[$proid]['Overseas']){
									?>
										<div class="box_select<?=(@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $row['IsCombination'])?'':' hide';?>" parent_null>
											<select name="Attr_<?=$proid;?>[Overseas]" attr="Overseas" parent_null="1" notnull>
												<?php
												foreach((array)$overseas_ary as $k2=>$v2){
													if(!in_array($v2['OvId'], $selected_ary[$proid]['Overseas'])) continue;
													$Ovid='Ov:'.$v2['OvId'];
												?>
													<option value="<?=$Ovid;?>"<?=in_array($Ovid, $attr_ext_ary)?' selected':'';?>><?=$overseas_ary[$v2['OvId']]['Name'.$lang];?></option>
												<?php }?>
											</select>
										</div>
									<?php }?>
								</div>
							</div>							
						</div>
						<div class="clear"></div>
					</div>
				<?php
					}
				}
				?>
			</div>
			<input type="hidden" name="PId" value="<?=$PId;?>" />
			<input type="hidden" name="ProId" id="proid_hide" value="<?=$ProId;?>" />
			<input type="hidden" name="PackageProId" id="packageproid_hide" value="<?=$PackageProId;?>" />
			<input type="hidden" name="do_action" value="sales.promotion_edit" />
			<input type="hidden" name="Type" id="type_hide" value="0" />
			<input type="hidden" name="IsMain" id="is_main" value="1" />
			<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
		</form>
		<div class="rows clean fixed_btn_submit">
			<div class="input">
				<input type="button" class="btn_global btn_submit" value="{/global.save/}">
				<a href="./?m=sales&a=promotion"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
			</div>
		</div>
		<div id="fixed_right">
			<div class="global_container fixed_add_seles">
				<div class="top_title"><?=$PId ? '{/global.edit/}' : '{/global.add/}'; ?> <a href="javascript:;" class="close"></a></div>
				<div class="list_menu">
					<?=manage::sales_right_search_form('sales', 'promotion', 'edit', '<input type="hidden" name="PId" value="'.$PId.'">'); ?>
				</div>
				<div class="add_sales_list" data-page="1" data-total-pages="<?=$total_pages; ?>">
					<?php
					foreach($products_row[0] as $k=>$v){
						$proid=$v['ProId'];
						$url=ly200::get_url($v, 'products');
						$img=ly200::get_size_img($v['PicPath_0'], '240x240');
						$name=$v['Name'.$lang];
						?>
						<div class="add_sales_item" data-proid="<?=$proid; ?>">
							<div class="img">
								<img src="<?=$img;?>" /><span></span>
								<div class="img_mask"></div>
							</div>
							<div class="name"><?=$name; ?></div>
							<div class="set_sales_item" data-proid="<?=$proid; ?>">
								<div class="edit_box">
									<a href="javascript:;" class="main"></a>
									<a href="javascript:;" class="del"></a>
								</div>
								<div class="pic">
									<a href="<?=$url;?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
									<span></span>
								</div>
								<div class="content">
									<a href="<?=$url;?>" target="_blank" class="name"><?=$name; ?></a>
									<div class="attr_list">
										<div class="attr_abs">
											<?php
											foreach((array)$parent_ary[$v['CateId']] as $k2=>$v2){
												if(!$selected_ary[$proid]['Id'][$v2]) continue;
											?>
												<div class="box_select" parent_null>
													<select name="Attr_<?=$proid;?>[<?=$v2;?>]" attr="<?=$v2;?>" parent_null="1" notnull>
														<option value="">--<?=$all_attr_ary[$v2]['Name'.$lang];?>--</option>
														<?php
														foreach((array)$all_value_ary[$v2] as $k3=>$v3){
															if(!in_array($k3, $selected_ary[$proid]['Id'][$v2])) continue;
														?>
															<option value="<?=$k3;?>"><?=$vid_data_ary[$k3]['Value'.$lang];?></option>
														<?php }?>
													</select>
												</div>
											<?php
											}
											if(count($overseas_ary)>0 && $selected_ary[$proid]['Overseas']){
											?>
												<div class="box_select<?=(@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1 && $v['IsCombination'])?'':' hide';?>" parent_null>
													<select name="Attr_<?=$proid;?>[Overseas]" attr="Overseas" parent_null="1" notnull>
														<?php
														foreach((array)$overseas_ary as $k2=>$v2){
															if(!in_array($v2['OvId'], $selected_ary[$proid]['Overseas'])) continue;
															$Ovid='Ov:'.$v2['OvId'];
														?>
															<option value="<?=$Ovid;?>"><?=$overseas_ary[$v2['OvId']]['Name'.$lang];?></option>
														<?php }?>
													</select>
												</div>
											<?php }?>
										</div>
									</div>									
								</div>
								<div class="clear"></div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
    <?php }?>
</div>