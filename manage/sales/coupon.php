<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'coupon'));//检查权限

$permit_ary=array(
	'add'		=>	manage::check_permit('sales', 0, array('a'=>'coupon', 'd'=>'add')),
	'edit'		=>	manage::check_permit('sales', 0, array('a'=>'coupon', 'd'=>'edit')),
	'del'		=>	manage::check_permit('sales', 0, array('a'=>'coupon', 'd'=>'del')),
	'export'	=>	manage::check_permit('sales', 0, array('a'=>'coupon', 'd'=>'export'))
);
$CouponWay = (int)$_GET['CouponWay'];

$top_id_name=($c['manage']['do']=='index'?'coupon':'coupon_inside');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php
    if($c['manage']['do']=='index'){
    	$status_arr_select=$status_arr=array(
			1=>manage::language('{/sales.coupon.normal/}'),//正常
			2=>manage::language('{/sales.coupon.use_end/}'),//次数已用完
			3=>manage::language('{/sales.coupon.not_start/}'),//未开始
			4=>manage::language('{/sales.coupon.expire/}'),//已过期
		);
		unset($status_arr_select[2]);
		$i=1;
		$where='CouponWay=0';
		$CouponWay && $where="CouponWay='{$CouponWay}'";
		$CouponType=(int)$_GET['CouponType'];
		$IsUser=(int)$_GET['IsUser'];//是否绑定会员了
		$Keyword=$_GET['Keyword'];
		$status=(int)$_GET['status'];//状态
		(isset($_GET['CouponType']) && $CouponType==0 || $CouponType==1) && $where.=" and CouponType='{$CouponType}'";
		(isset($_GET['IsUser']) && $IsUser==0) && $where.=' and (UserId="" or UserId is null or UserId="|" or LevelId="" or LevelId is null or LevelId="|")';
		$IsUser==1 && $where.=' and (UserId!=0 or UserId!="" or LevelId!=0 or LevelId!="")';
		$Keyword && $where.=" and CouponNumber like '%{$Keyword}%'";
		switch ($status){//根据状态筛选优惠券
			case 1:
				$where.=" and StartTime<={$c['time']} and EndTime>={$c['time']}";
				break;
			case 2:
				// $where.=" and UseNum>0 and BeUseTimes>=UseNum";
				break;
			case 3:
				$where.=" and StartTime>{$c['time']}";
				break;
			case 4:
				$where.=" and EndTime<{$c['time']}";
				break;
		}
		$coupon_row=str::str_code(db::get_limit_page('sales_coupon', $where, '*', 'AccTime desc', (int)$_GET['page'], 20));
	?>
    	<script type="text/javascript">$(document).ready(function(){sales_obj.coupon_list_init()});</script>
		<div class="inside_container">
			<h1>{/module.sales.coupon/}</h1>
			<ul class="inside_menu">
				<?php for($i=0; $i<3; ++$i){?>
					<li><a href="./?m=sales&a=coupon&CouponWay=<?=$i;?>"<?=$CouponWay==$i?' class="current"':'';?>>{/sales.coupon.rele_type_<?=$i;?>/}</a></li>
				<?php }?>
			</ul>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=sales&a=coupon&d=edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<li>
						<a class="more" href="javascript:;">{/global.more/}<em></em></a>
						<div class="more_menu drop_down">
							<?php if($permit_ary['export']){?>
								<a class="explode item" href="./?m=sales&a=coupon&d=explode">{/global.explode/}</a>
							<?php }?>
						</div>
					</li>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/sales.coupon.status/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($status_arr_select, 'status', '', '', '', '{/global.select_index/}');?></div>
								</div>
							</div>
							<div class="rows item clean">
								<label>{/sales.coupon.type/}</label>
								<div class="input">
									<div class="box_select">
										<select name="CouponType">
											<option value="-1">{/global.select_index/}</option>
											<option value="0">{/sales.coupon.discount/}</option>
											<option value="1">{/sales.coupon.over_money/}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="rows item clean">
								<label>{/sales.coupon.bind/}</label>
								<div class="input">
									<div class="box_select">
										<select name="IsUser">
											<option value="-1">{/global.select_index/}</option>
											<option value="1">{/global.n_y.1/}</option>
											<option value="0">{/global.n_y.0/}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="sales" />
						<input type="hidden" name="a" value="coupon" />
					</form>
				</div>
			</div>
			<?php
			if($coupon_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="5%" nowrap="nowrap">{/global.serial/}</td>
							<td width="12%" nowrap="nowrap">{/sales.coupon.code/}</td>
							<td width="13%" nowrap="nowrap">{/sales.coupon.type/}</td>
							<td width="13%" nowrap="nowrap">{/sales.coupon.condition/}</td>
							<td width="15%" nowrap="nowrap">{/sales.coupon.deadline/}</td>
							<td width="13%" nowrap="nowrap">{/sales.coupon.acctime/}</td>
							<td width="8%" nowrap="nowrap">{/sales.coupon.use/}{/sales.coupon.times/}</td>
							<td width="8%" nowrap="nowrap">{/sales.coupon.status/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['copy'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($coupon_row[0] as $k => $v){
							$BeUseTimes=0;
							$IsUser=db::get_row_count('sales_coupon_relation', "CId={$v['CId']} and UserId>0");
							$BeUseTimes=db::get_row_count('sales_coupon_log', "CId={$v['CId']}");
							if($v['UseNum'] && $BeUseTimes>=$v['UseNum'] && $v['CouponWay']!=2 && !($IsUser && $v['CouponWay']==0)){
								$s_key=2;//次数已用完
							}elseif($v['StartTime']>$c['time']){
								$s_key=3;//未开始
							}elseif($v['EndTime']<$c['time']){
								$s_key=4;//已过期
							}else{
								$s_key=1;//正常
							}
							if(!$v['UseNum']){
								$BeUseTimes+=$v['BeUseTimes'];
							}
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['CId']);?></td><?php }?>
								<td><?=$coupon_row[4]+$k+1;?></td>
								<td><?=$v['CouponNumber'];?></td>
								<td><?=$v['CouponType']==1?'{/sales.coupon.money/}('.$c['manage']['currency_symbol'].$v['Money'].')':'{/sales.coupon.discount/}('.$v['Discount'].'%)';?></td>
								<td><?=$v['UseCondition']!='0.00'?'{/sales.coupon.over/}'.$c['manage']['currency_symbol'].$v['UseCondition'].'<br />':'';?><?=($v['UserId'] || $v['LevelId'])?'{/sales.coupon.onlyuser/}':''?></td>
								<td><?=date('Y-m-d H:i:s', $v['StartTime']).'<br /> '.date('Y-m-d H:i:s', $v['EndTime']);?></td>
								<td><?=date('Y-m-d H:i:s',$v['AccTime'])?></td>
								<td><?=$BeUseTimes.' / '.($v['UseNum']==0?'{/sales.coupon.unlimit/}':$v['UseNum']);?></td>
								<td nowrap="nowrap"><?=$status_arr[$s_key];?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=sales&a=coupon&d=edit&CId=<?=$v['CId']?>&CouponWay=<?=$CouponWay;?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down"><a class="del item" href="./?do_action=sales.coupon_del&CId=<?=$v['CId'];?>" rel="del">{/global.del/}</a></dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($coupon_row[1], $coupon_row[2], $coupon_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=sales&a=coupon&d=edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='explode'){
		//优惠券导出页面
		
		$is_user_ary = array(
			'0'		=>	'{/global.all/}',
			'1'		=>	'{/sales.coupon.bind/}',
			'2'		=>	'{/sales.coupon.unbind/}',
		);
		$coupon_type_ary = array(
			'0'		=>	'{/global.all/}',
			'1'		=>	'{/sales.coupon.discount/}',
			'2'		=>	'{/sales.coupon.over_money/}',
		);
		$status_ary = array(
			'0'		=>	'{/global.all/}',
			'1'		=>	'{/sales.coupon.normal/}',
			// '2'		=>	'{/sales.coupon.use_end/}',
			'3'		=>	'{/sales.coupon.not_start/}',
			'4'		=>	'{/sales.coupon.expire/}',
		);
		echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
	?>
        <script type="text/javascript">$(document).ready(function(){sales_obj.coupon_explode_init()});</script>
		<div class="center_container">	
			<form id="explode_edit_form" class="global_form">
				<div class="global_container">
	            	<a href="javascript:history.back(-1);" class="return_title">
	                    <span class="return">{/module.sales.coupon/}</span> 
	                    <span class="s_return">/ {/sales.coupon.coupon_explode/}</span>
	                </a>
	                <div class="rows clean">
						<label>{/sales.coupon.deadline/}</label>
						<div class="input">
							<input name="DeadLine" type="text" value="" class="box_input input_time" size="55" autocomplete="off"/>
						</div>
					</div>   
					<div class="rows clean">
						<label>{/sales.coupon.bind/}</label>
						<span class="input">
							<div class="blank6"></div>
							<?php foreach((array)$is_user_ary as $k=>$v){?>
								<span class="input_radio_box <?=$k == 0 ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="IsUser" value="<?=$k;?>" <?=$k == 0 ? 'checked' : '';?>>
									</span><?=$v;?>
								</span> &nbsp;&nbsp;&nbsp;
							<?php }?>
						</span>
						<div class="clear"></div>
					</div>
					<div class="rows clean">
						<label>{/sales.coupon.type/}</label>
						<span class="input">
							<div class="blank6"></div>
							<?php foreach((array)$coupon_type_ary as $k=>$v){?>
								<span class="input_radio_box coupon_type <?=$k == 0 ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="CouponType" value="<?=$k;?>" <?=$k == 0 ? 'checked' : '';?>>
									</span><?=$v;?>
								</span> &nbsp;&nbsp;&nbsp;
							<?php }?>
						</span>
						<div class="clear"></div>
					</div>
					<div class="rows discount none">
						<label>{/sales.coupon.discount/}</label>
						<div class="box_explain">{/sales.explain.off/}</div>
						<div class="input">
							<span class="unit_input"><input name="Discount" value="100" type="text" class="box_input" maxlength="5" size="5" /><b class="last">%</b></span>
							<span class="tool_tips_ico" content="{/sales.coupon.discount_tips/}"></span>
						</div>
					</div>
					<div class="rows money none">
						<label>{/sales.coupon.money/}</label>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input name="Money" value="0" type="text" class="box_input" maxlength="5" size="5" /></span>
							<span class="tool_tips_ico" content="{/sales.coupon.money_tips/}"></span>
						</div>
					</div>
					<div class="rows">
						<label>{/sales.coupon.status/}</label>
						<span class="input">
							<?php foreach((array)$status_ary as $k=>$v){?>
								<span class="input_radio_box <?=$k == 0 ? 'checked' : '';?>">
									<span class="input_radio">
										<input type="radio" name="Status" value="<?=$k;?>" <?=$k == 0 ? 'checked' : '';?>>
									</span><?=$v;?>
								</span> &nbsp;&nbsp;&nbsp;
							<?php }?>
						</span>
						<div class="clear"></div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/global.explode/}" />
							<a href="./?m=sales&a=coupon" class="btn_global btn_cancel">{/global.return/}</a>
						</div>
					</div>
					<div id="explode_progress"></div>
					<input type="hidden" name="do_action" value="sales.coupon_explode" />
					<input type="hidden" name="Number" value="0" />
				</div>
			</form>
	    </div>
	<?php
	}elseif($c['manage']['do']=='edit'){
		//编辑页
		$CId=(int)$_GET['CId'];
		$CId && $coupon_row=str::str_code(db::get_one('sales_coupon', "CId='$CId'"));
		$level_row=str::str_code(db::get_all('user_level', 'IsUsed=1', "LId, Name{$c['manage']['web_lang']}", 'FullPrice desc'));//会员等级
		$level_row[count($level_row)]=array('LId'=>0, 'Name'.$c['manage']['web_lang']=>'No level');
		
		$Keyword=$_GET['Keyword'];
		$where='1';//条件
		$Keyword && $where.=" and (FirstName like '%$Keyword%' or LastName like '%$Keyword%' or concat(FirstName, LastName) like '%$Keyword%' or Email like '%$Keyword%')";
		$page_count=100;//显示数量
		$user_row=str::str_code(db::get_limit_page('user', $where, '*', 'UserId desc', (int)$_GET['page'], $page_count));
		$lang=$c['manage']['web_lang'];
		$lang_all=$c['manage']['lang_pack']['global']['all'];
		echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/themes/default/css/user.css');
	?>
		<script type="text/javascript">$(function(){sales_obj.coupon_edit_init()});</script>
		<div class="center_container">
			<div class="global_container">
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.sales.coupon/}</span> 
					<span class="s_return">/ <?=$CId?'{/global.edit/}':'{/global.add/}';?></span>
				</a>
				<form id="coupon_form" class="global_form">
					<div class="rows clean"<?=$coupon_row?' style="display:none;"':'';?>>
						<label>{/sales.coupon.rele_type/}</label>
						<div class="box_explain">{/sales.explain.coupon/}</div>
						<div class="input">
							<div class="coupon_type">
								<div class="ty_list<?=$coupon_row['CouponWay']==0?' checked':'';?>">
									<input type="radio" name="CouponWay" value="0"<?=$coupon_row['CouponWay']==0?' checked':'';?> />
									<span>{/sales.coupon.rele_type_0/}</span>
								</div>
								<div class="ty_list ty_list_1<?=$coupon_row['CouponWay']==1?' checked':'';?>">
									<input type="radio" name="CouponWay" value="1"<?=$coupon_row['CouponWay']==1?' checked':'';?> /> 
									<span>{/sales.coupon.rele_type_1/}</span>
								</div>
								<?php if(!db::get_row_count('sales_coupon','CouponWay=2')){ ?>
									<div class="ty_list ty_list_2<?=$coupon_row['CouponWay']==2?' checked':'';?>">
										<input type="radio" name="CouponWay" value="2"<?=$coupon_row['CouponWay']==2?' checked':'';?> /> 
										<span>{/sales.coupon.rele_type_2/}</span>
									</div>
								<?php }?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<?php
					if(!$coupon_row){
					?>
						<div class="rows">
							<label>{/sales.coupon.coupon/}</label>
							<div class="box_explain">{/sales.explain.num/}</div>
							<div class="input">					
								<div class="fl cou_inp">
									<div class="rows clean">
										<label>{/sales.coupon.prefix/}</label>
										<div class="input">
											<input name="Prefix" value="" type="text" class="box_input" maxlength="5" size="10" />
										</div>
									</div>
								</div>
								<div class="fl cou_inp">
									<div class="rows clean">
										<label>{/sales.coupon.code_len/}</label>
										<div class="input">
											<input name="CodeLen" value="8" type="text" class="box_input" maxlength="2" size="10" notnull />
										</div>
									</div>
								</div>
								<div class="fl cou_inp">
									<div class="rows couponway"<?=$coupon_row['CouponWay']?' style="display:none;"':'';?>>
										<label>{/sales.coupon.qty/}</label>
										<div class="input">
											<input name="Qty" value="1" type="text" class="box_input" maxlength="5" size="10" notnull />
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="rows chars clean">
							<label>{/sales.coupon.chars/}</label>
							<div class="box_explain">{/sales.explain.chars/}</div>
							<div class="input">
								<span class="input_checkbox_box">
									<span class="input_checkbox">
										<input type="checkbox" name="c1" value="1" />
									</span>{/sales.coupon.character/}
								</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="input_checkbox_box">
									<span class="input_checkbox">
										<input type="checkbox" name="c2" value="1"/>
									</span>{/sales.coupon.number/}
								</span>		 
							</div>
						</div>
					<?php }else{?>
						<div class="rows clean">
							<label>{/sales.coupon.code/}</label>
							<div class="input">
								<input name="CouponNumber" value="<?=$coupon_row['CouponNumber'];?>" type="text" class="box_input" maxlength="20" size="20" notnull />
							</div>
						</div>
					<?php }?>
					<div class="rows clean">
						<label>{/sales.coupon.deadline/}</label>
						<div class="input">
							<input name="DeadLine" value="<?=($coupon_row['StartTime'] && $coupon_row['EndTime'])?date('Y-m-d H:i:s', $coupon_row['StartTime']).'/'.date('Y-m-d H:i:s', $coupon_row['EndTime']):'';?>" type="text" class="box_input input_time" size="55" readonly notnull /> 
							<font class="fc_red">*</font>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.coupon.type/}</label>
						<div class="input">
							<div class="discount_type">
								<div class="ty_list<?=$coupon_row['CouponType']==0?' checked':'';?>">
									<input type="radio" name="CouponType" value="0"<?=$coupon_row['CouponType']==0?' checked':'';?> />
									{/sales.coupon.discount/}
								</div>
								<div class="ty_list ty_list_1<?=$coupon_row['CouponType']==1?' checked':'';?>">
									<input type="radio" name="CouponType" value="1"<?=$coupon_row['CouponType']==1?' checked':'';?> /> 
									{/sales.coupon.over_money/}
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="rows discount <?=$coupon_row['CouponType']==0?'':'none';?>">
						<label>{/sales.coupon.discount/}</label>
						<div class="box_explain">{/sales.explain.off/}</div>
						<div class="input">
							<span class="unit_input"><input name="Discount" value="<?=$coupon_row['Discount']?$coupon_row['Discount']:100;?>" type="text" class="box_input" maxlength="5" size="5" /><b class="last">%</b></span>
						</div>
					</div>
					<div class="rows money <?=$coupon_row['CouponType']==1?'':'none';?>">
						<label>{/sales.coupon.money/}</label>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input name="Money" value="<?=$coupon_row['Money']?$coupon_row['Money']:0;?>" type="text" class="box_input" maxlength="5" size="5" /></span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.coupon.condition/}</label>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input name="UseCondition" value="<?=$coupon_row['UseCondition']?$coupon_row['UseCondition']:0;?>" type="text" class="box_input" maxlength="10" size="5" /></span>
							<span class="tool_tips_ico" content="{/sales.coupon.condition_tips/}"></span>
						</div>
					</div>
					<?php
					//限制条件
					$UserAry=$LevelAry=array();
					?>
					<div class="rows clean couponway"<?=$coupon_row['CouponWay']?' style="display:none;"':'';?>>
						<label>{/sales.coupon.limiting/}</label>
						<div class="input clean">
							<?php
							$SelectAry=array(
								'user'=>array('Name'=>'会员', 'Type'=>'user', 'Table'=>'user'),
								'level'=>array('Name'=>'等级', 'Type'=>'level', 'Table'=>'user_level')
							);
							$ValueAry=array();
							if($coupon_row['UserId']){
								$level_where_ary=$user_where_ary=array();
								$UserId=$coupon_row['UserId'];
								!strstr($UserId, '|') && $UserId='|'.$UserId.'|';
								$UserAry=explode('|', substr($UserId, 1, -1));
								foreach($UserAry as $v){ $where_ary[]=$v; }
								$where_str=@implode(',', $where_ary);
								if($where_str){
									if(@in_array('-1', $where_ary)){//所有会员
										$ValueAry[]=array('Name'=>$lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_user'], 'Value'=>-1, 'Type'=>'user');
									}
									$user_row=db::get_all('user', 'UserId in('.$where_str.')', 'UserId, Email', 'UserId desc');
									foreach($user_row as $v){
										$ValueAry[]=array('Name'=>$v['Email'], 'Value'=>$v['UserId'], 'Type'=>'user');
									}
								}
							}
							if($coupon_row['LevelId']){
								$where_ary=array();
								$LevelAry=explode('|', substr($coupon_row['LevelId'], 1, -1));
								foreach($LevelAry as $v){ $where_ary[]=$v; }
								$where_str=@implode(',', $where_ary);
								if($where_str){
									if(@in_array('-1', $where_ary)){//所有会员等级
										$ValueAry[]=array('Name'=>$lang_all.$c['manage']['lang_pack']['sales']['coupon']['individual_level'], 'Value'=>-1, 'Type'=>'level');
									}
									$level_row=db::get_all('user_level', 'LId in('.$where_str.')', "LId, Name{$lang}", 'LId asc');
									foreach($level_row as $v){
										$ValueAry[]=array('Name'=>$v['Name'.$lang], 'Value'=>$v['LId'], 'Type'=>'level');
									}
								}
							}
							echo manage::box_drop_double('Unit', 'UnitValue', $SelectAry, $ValueAry, 0, '', 1);
							?>
						</div>
					</div>
					<?php
					//应用范围
					$UserAry=$LevelAry=array();
					?>
					<div class="rows clean">
						<label>{/sales.coupon.apply/}</label>
						<div class="input clean">
							<?php
							$SelectAry=array(
								'products'=>array('Name'=>'产品', 'Type'=>'products', 'Table'=>'products'),
								'tags'=>array('Name'=>'标签', 'Type'=>'tags', 'Table'=>'products_tags'),
								'category'=>array('Name'=>'分类', 'Type'=>'category', 'Table'=>'products_category')
							);
							$ValueAry=array();
							if($coupon_row['CateId']){
								$where_ary=array();
								$CateAry=explode('|', substr($coupon_row['CateId'], 1, -1));
								foreach($CateAry as $v){ $where_ary[]=$v; }
								$where_ary=@implode(',', $where_ary);
								if($where_ary){
									$category_row=db::get_all('products_category', 'CateId in('.$where_ary.')', "CateId, Category{$lang}", $c['my_order'].'CateId asc');
									foreach($category_row as $v){
										$ValueAry[]=array('Name'=>$v['Category'.$lang], 'Value'=>$v['CateId'], 'Type'=>'category');
									}
								}
							}
							if($coupon_row['ProId']){
								$where_ary=array();
								$ProAry=explode('|', substr($coupon_row['ProId'], 1, -1));
								foreach($ProAry as $v){ $where_ary[]=$v; }
								$where_ary=@implode(',', $where_ary);
								if($where_ary){
									$products_row=db::get_all('products', 'ProId in('.$where_ary.')', "ProId, Name{$lang}, PicPath_0", $c['my_order'].'ProId desc');
									foreach($products_row as $v){
										$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
										$ValueAry[]=array('Name'=>$v['Name'.$lang], 'Value'=>$v['ProId'], 'Type'=>'products', 'Icon'=>'<em class="icon icon_products pic_box"><img src="'.$img.'" /><span></span></em>');
									}
								}
							}
							if($coupon_row['TagId']){
								$where_ary=array();
								$TagsAry=explode('|', substr($coupon_row['TagId'], 1, -1));
								foreach($TagsAry as $v){ $where_ary[]=$v; }
								$where_ary=@implode(',', $where_ary);
								if($where_ary){
									$tags_row=db::get_all('products_tags', 'TId in('.$where_ary.')', "TId, Name{$lang}", $c['my_order'].'TId desc');
									foreach($tags_row as $v){
										$ValueAry[]=array('Name'=>$v['Name'.$lang], 'Value'=>$v['TId'], 'Type'=>'tags');
									}
								}
							}
							echo manage::box_drop_double('Apply', 'ApplyValue', $SelectAry, $ValueAry, 0, '', 1);
							?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.coupon.use/}{/sales.coupon.times/}</label>
						<?php if (!$coupon_row) { ?>
							<div class="box_explain">{/sales.explain.usenum/}</div>
						<?php } ?>
						<div class="input">
							<?php if(trim($coupon_row['UserId'], '|') || trim($coupon_row['LevelId'], '|') || $coupon_row['CouponWay']){ ?>
								<?=(int)$coupon_row['UseNum']==0?'{/sales.coupon.unlimit/}':(int)$coupon_row['UseNum'];?>
								<input type="hidden" name="UseNum" value="<?=(int)$coupon_row['UseNum']<0?0:(int)$coupon_row['UseNum'];?>" />
							<?php }else{ ?>
								<input name="UseNum" value="<?=(int)$coupon_row['UseNum']<0?0:(int)$coupon_row['UseNum'];?>" type="text" class="box_input" maxlength="10" size="5" />
								<span class="tool_tips_ico" content="{/sales.coupon.use_tips/}"></span>
							<?php } ?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=sales&a=coupon"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>
					</div>
					<input type="hidden" name="CId" value="<?=$CId;?>" />
					<input type="hidden" name="do_action" value="sales.coupon_edit" />
					<input type="hidden" id="back_action" name="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
				</form>
			</div>
		</div>
    <?php
	} ?>
</div>