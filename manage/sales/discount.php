<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'discount'));//检查权限

$discount=db::get_value('config', "GroupId='cart' and Variable='discount'", 'Value');
$value=str::json_data(htmlspecialchars_decode($discount), 'decode');
?>
<div id="sales" class="r_con_wrap discount <?=$c['manage']['cdx_limit'];?>">
    <?=ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
	<script language="javascript">$(function(){sales_obj.discount_init()});</script>
	<div class="center_container">
		<div class="global_container global_form">
			<div class="big_title">
				{/module.sales.discount/}
			</div>
			<form id="discount_form" class="global_form">
				<div class="rows clean">
					<label></label>
					<div class="input">
						<span class="input_checkbox_box <?=(int)$value['IsUsed']?' checked':'';?>">
							<span class="input_checkbox">
								<input type="checkbox" name="IsUsed" value="1" <?=(int)$value['IsUsed']?' checked':'';?>>
							</span>{/global.turn_on/}
						</span>
					</div>
				</div>
				<div class="rows clean">
					<label>{/global.type/}</label>
					<div class="input">
						<div class="discount_type">
							<div class="ty_list <?=$value['Type']==0?'checked':'';?>">
								<input type="radio" name="Type" value="0" <?=$value['Type']==0?'checked="checked"':'';?> />
								{/sales.coupon.discount/}
							</div>
							<div class="ty_list ty_list_1 <?=$value['Type']==1?'checked':'';?>">
								<input type="radio" name="Type" value="1" <?=$value['Type']==1?'checked="checked"':'';?> /> 
								{/sales.coupon.over_money/}
							</div>
							<div class="clear"></div>
						</div>	
					</div>
				</div>
				<div class="rows clean">
					<div class="d_line"></div>
					<div class="input">
						<table border="0" cellspacing="0" cellpadding="3" id="discount_list" class="item_data_table">
							<tbody>
								<?php
								$i=0;
								foreach((array)$value['Data'] as $k=>$v){
								?>
									<tr>
										<td>
											<span class="d_list">
												<?=$i==0?'<span class="d_tit">{/sales.coupon.condition/}</span>':'<div class="d_empty"></div>';?>
												<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input name="UseCondition[]" value="<?=$k?$k:0;?>" type="text" class="box_input" maxlength="10" size="6"></span>
											</span>
											<span class="d_list discount <?=$value['Type']==0?'':'none';?>">
												<?=$i==0?'<span class="d_tit">{/sales.coupon.discount/}</span>':'<div class="d_empty"></div>';?>
												<span class="unit_input"><input name="Discount[]" value="<?=$v[0]?$v[0]:100;?>" type="text" class="box_input" maxlength="3" size="5"><b class="last">%</b></span>
											</span>
											<span class="d_list money <?=$value['Type']==1?'':'none';?>">
												<?=$i==0?'<span class="d_tit">{/sales.coupon.money/}</span>':'<div class="d_empty"></div>';?>
												<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input name="Money[]" value="<?=$v[1]?$v[1]:0;?>" type="text" class="box_input" maxlength="5" size="5"></span>
											</span>
											<?php if($i){?>&nbsp;<a class="d_del fl icon_delete_1 d_del_empty" href="javascript:;"><i></i></a><?php }?>
											<div class="clear"></div>
										</td>
									</tr>
								<?php
									++$i;
								}?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="rows clean">
					<label></label>
					<div class="input"><a href="javascript:;" id="add_discount" class="btn_global btn_add_item">{/global.add/}</a></div>
				</div>
				<div class="d_line"></div>
				<div class="rows clean">
					<label>{/sales.coupon.deadline/}</label>
					<div class="input">
						<input name="DeadLine" value="<?=($value['StartTime'] && $value['EndTime'])?date('Y-m-d H:i:s', $value['StartTime']).'/'.date('Y-m-d H:i:s', $value['EndTime']):'';?>" type="text" class="box_input input_time" size="55" readonly notnull> 
						<font class="fc_red">*</font>
					</div>
				</div>      
				<div class="rows clean">
					<label></label>
					<div class="input">
						<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					</div>
				</div>
				<input type="hidden" name="do_action" value="sales.discount">
			</form>
		</div>
	</div>
</div>