<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'holiday'));//检查权限
if(!in_array('holiday', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}


foreach($c['manage']['lang_pack']['sales']['holiday']['holiday_ary'] as $v){
	$category_ary[$v]=$v;
}
$c['default_path']=$c['root_path']."/static/themes/default/";
?>
<script type="text/javascript">var lang_str_obj={'currency':'<?=$c['manage']['currency_symbol'];?>', 'now_time':'<?=date('Y-m-d H:i', $c['time']);?>'};</script>
<div id="<?=$c['manage']['do']=='products' ? 'sales' : 'holiday'; ?>" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php if($c['manage']['do']!='products'){ ?>
		<div class="inside_container">
			<h1>{/module.sales.holiday/} &nbsp;&nbsp;<span class="box_explain">( {/sales.explain.holiday/} )</span></h1></h1>
		</div>
	<?php } ?>
	<?php
	if($c['manage']['do']=='index'){
		$Name=$_GET['Name'];
		$Category=$_GET['Category'];
		$where='1';//条件
		$page_count=20;//显示数量
		$Category && $where.=" and Category='$Category'";
		$holiday_row=str::str_code(db::get_limit_page('sales_holiday', $where, '*', 'HId asc', (int)$_GET['page'], $page_count));
	?>
		<script type="text/javascript">
		$(document).ready(function(){
			sales_obj.holiday_list_init();
		});
		</script>
		<div class="list_bd list_box">
			<?php
			foreach($holiday_row[0] as $k=>$v){
				$theme=$v['Number'];
			?>
			<div class="item fl<?=$v['IsUsed']?' current':'';?>" hid="<?=$v['HId'];?>">
				<div class="img"><img src="<?="/static/themes/default/holiday/{$theme}/images/cover.jpg";?>" title="{/sales.holiday.click/}<?=$v['Title'];?>" /></div>
				<div class="info">
					<span><?=$v['Title'];?></span>
					<div class="btn fr">
						<?php /*<a class="view" href="?m=sales&a=holiday&d=edit&theme=<?=$theme;?>" title="{/global.preview/}"><img src="/static/manage/images/sales/search.png" align="absmiddle" /></a>*/?>
						<a class="edit" href="?m=sales&a=holiday&d=edit&theme=<?=$theme;?>" title="{/global.edit/}"><img src="/static/manage/images/sales/edit.png" align="absmiddle" /></a>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="clear"></div>
		</div>
		<?=html::turn_page($holiday_row[1], $holiday_row[2], $holiday_row[3], '?'.ly200::query_string('page').'&page=');?>
	<?php
	}elseif($c['manage']['do']=='edit'){
		$theme=$_GET['theme'];
		$holiday_row=str::str_code(db::get_one('sales_holiday', "Number='$theme'"));
		$current_lang = $_GET['lang'] ? trim($_GET['lang']) : $c['manage']['config']['LanguageDefault'];
		$current_lang_ext = '_'.($_GET['lang'] ? trim($_GET['lang']) : $c['manage']['config']['LanguageDefault']);		
		if(!is_file("{$c['default_path']}holiday/$theme/themes{$current_lang_ext}.json")){
			@copy("{$c['default_path']}holiday/$theme/themes.json", "{$c['default_path']}holiday/$theme/themes{$current_lang_ext}.json");
		}
	?>
	<?=ly200::load_static("/static/themes/default/holiday/{$theme}/manage/template.css", '/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js');?>
	<script type="text/javascript">
		var web_template_data=<?php include("{$c['default_path']}holiday/{$theme}/themes{$current_lang_ext}.json");?>;
		$(document).ready(function(){sales_obj.holiday_edit_init();});
	</script>
    <div class="">
		<div class="m_lefter fl">
			<div class="box_explain">{/sales.explain.holiday_1/}</div>
			<?php include("{$c['default_path']}holiday/{$theme}/manage/template.php");?>
		</div>
		<div class="m_righter">
			<div class="global_container">
				<form id="holiday_form" class="global_form">
					<div class="big_title">{/sales.holiday.model_set/} </div>
					<?php if(count($c['manage']['config']['Language'])>1){ ?>
						<div class="rows lang_box clean">
							<label>{/set.config.language_list/}<div class="tab_box"><?=manage::html_tab_button('', $current_lang);?></div></label>
							<?php /*
							<div class="input lang_box">
								<?php foreach($c['manage']['config']['Language'] as $k=>$v){ ?>
									<span class="input_radio_box <?=$v==$current_lang ? 'checked' : '' ; ?>">
										<span href="<?=ly200::query_string('lang').'&lang='.$v; ?>" data-lang="<?=$v; ?>" class="input_radio lang ">
											<input type="radio" name="Lang" <?=$v==$current_lang ? 'checked' : '' ; ?> value="<?=$v; ?>" />
										</span><?=$c['manage']['lang_pack']['language'][$v]; ?>
									</span>&nbsp;&nbsp;&nbsp;
								<?php } ?>
		                        <div class="clear"></div>
		                    </div>*/ ?>
						</div>
	                <?php } ?>
					<div id="set_banner">
						<div class="rows clean" value="images" style="display:none;">
							<label>{/global.pic/}1</label>
							<div class="input">
								<span class="upload_file">
									<div class="no_input" value="title_list" style="display:none;">
										<div class="sec_tit">{/global.title/}</div>
										<input name="TitleList[]" value="" type="text" class="box_input" size="30" maxlength="50">
									</div>
									<div class="no_input" value="url_list" style="display:none;">
										<div class="sec_tit">{/sales.holiday.link/}</div>
										<input name="UrlList[]" value="" type="text" class="box_input" size="30" maxlength="200">
									</div>
									<span class="upload_picture">
										<?=manage::multi_img('', 'ImgPath[]'); ?>
									</span>
								</span>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div id="set_config">
						<div class="rows clean" value="title" style="display:none;">
							<label>{/global.title/}</label>
							<div class="input"><input name="Title" value="" type="text" class="box_input" size="30" maxlength="50"></div>
							<div class="clear"></div>
						</div>
						<div class="rows clean" value="images" style="display:none;">
							<label>{/global.pic/}</label>
							<div class="input upload_file upload_pic">
								<?=manage::multi_img('PicDetail', 'PicPath'); ?>
							</div>
							<div class="clear"></div>
						</div>
						<div class="rows clean" value="url" style="display:none;">
							<label>{/sales.holiday.link/}</label>
							<div class="input"><input name="Url" value="" type="text" class="box_input" size="30" maxlength="200"></div>
							<div class="clear"></div>
						</div>
						<div class="rows clean" value="products" style="display:none;">
							<label>{/sales.holiday.products_list/}</label>
							<div class="input"><input type="button" id="products_btn" class="btn_global" num="0" max="0" value="{/sales.holiday.products_list/}"></div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=sales&a=holiday"><input type="button" class="btn_global btn_cancel" value="{/global.cancel/}"></a>
						</div>
					</div>
					<input type="hidden" name="Theme" id="theme_hide" value="<?=$theme;?>" />
					<input type="hidden" name="Number" id="number_hide" value="0" />
					<input type="hidden" name="ContentsType" id="type_hide" value="0" />
					<input type="hidden" name="HideLang" value="<?=$current_lang; ?>" />
					<input type="hidden" name="do_action" value="sales.holiday_edit">
				</form>
				<div class="blank25"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<?php
	}elseif($c['manage']['do']=='products'){
		$current_lang = $_GET['lang'] ? trim($_GET['lang']) : $c['manage']['config']['LanguageDefault'];
		$current_lang_ext = '_'.($_GET['lang'] ? trim($_GET['lang']) : $c['manage']['config']['LanguageDefault']);		
		
		$theme=$_GET['theme'];
		$number=(int)$_GET['num'];
		$max=(int)$_GET['max'];
		$holiday_row=db::get_value('sales_holiday', "Number='$theme'", 'ProId');
		$holiday_obj=str::json_data($holiday_row, 'decode');
		$proid_ary=$holiday_obj[$number];

		$where='1'.$c['manage']['where']['products'];
		$page_count=30;//显示数量
		$Name=str::str_code($_GET['Keyword']);
		$CateId=(int)$_GET['CateId'];
		$Name && $where.=" and (Name{$c['manage']['web_lang']} like '%$Name%' or concat(Prefix, Number) like '%$Name%')";
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}

		$p_remove_pid=$_POST['remove_pid'] ? $_POST['remove_pid'] : $_GET['remove_pid'];
		$p_remove_pid=str::ary_format($p_remove_pid, 2, '', '|');
		$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";

		if($proid_ary){
			$pro_ary=array();
			$proid_str=@implode(',', $proid_ary);
			$proid_str || $proid_str=0;
			$h_pro_where="ProId in ({$proid_str})";
			$where.=" and ProId not in ({$proid_str})";
			$pro_row=str::str_code(db::get_all('products', $h_pro_where, "ProId, Name{$c['manage']['web_lang']}, Number, Price_0, Price_1, PicPath_0", 'ProId desc'));
			foreach($pro_row as $k => $v) $pro_ary[$v['ProId']]=$v; 
		}

		$row_count=db::get_row_count('products',$where);
		$total_pages=ceil($row_count/$page_count);
		$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
	?>
	<?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
    <script type="text/javascript">var web_template_data=<?php include("{$c['default_path']}holiday/{$theme}/themes{$current_lang_ext}.json");?>; $(document).ready(function(){sales_obj.package_edit_init();});</script>
		<div class="inside_container">
			<h1>{/module.sales.holiday/}</h1>
		</div>
		<div class="sales_content">
			<form id="edit_form" class="global_form" data-return="./?m=sales&a=holiday&d=edit&theme=<?=$theme;?>">
				<div class="date_rows">
					<a href="javascript:;" class="set_add fr">{/global.add/}</a>
					<div class="clear"></div>
				</div>
				<div class="set_sales_list" max="<?=$max;?>">
					<?php
					if($proid_ary){
						$ProIdStr='|'.implode('|', $proid_ary).'|';
						foreach($proid_ary as $v){
							$proid=$v;
							$url=ly200::get_url($pro_ary[$proid], 'products');
							$img=ly200::get_size_img($pro_ary[$proid]['PicPath_0'], '168x168');
							$name=$pro_ary[$proid]['Name'.$c['manage']['web_lang']];
						?>

						<div class="set_sales_item" data-proid="<?=$proid; ?>">
							<div class="edit_box">
								<a href="javascript:;" class="del"></a>
							</div>
							<div class="pic">
								<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
								<span></span>
							</div>
							<div class="content">
								<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
								<div class="number"><?=$pro_ary[$proid]['Number']; ?></div>
								<input type="hidden" name="ProId[]" value="<?=$proid; ?>" />
							</div>
							<div class="clear"></div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
				<input type="hidden" name="Theme" value="<?=$theme;?>" />
				<input type="hidden" name="Number" value="<?=$number;?>" />
				<input type="hidden" name="ProId" id="proid_hide" value="" />
				<input type="hidden" name="ProIdAry" id="packageproid_hide" value="<?=$ProIdStr;?>" />
				<input type="hidden" name="do_action" value="sales.holiday_products_edit" />
				<input type="hidden" name="Type" id="type_hide" value="2" />
				<input type="hidden" name="IsMain" id="is_main" value="0" />
			</form>
			<div class="rows clean fixed_btn_submit">
				<div class="input">
					<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					<a href="./?m=sales&a=holiday&d=edit&theme=<?=$theme;?>"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
				</div>
			</div>
			<div id="fixed_right">
				<div class="global_container fixed_add_seles">
					<div class="top_title">{/global.add/} <a href="javascript:;" class="close"></a></div>
					<div class="list_menu">
						<?=manage::sales_right_search_form('sales', 'holiday', 'products', '<input type="hidden" name="theme" value="'.$theme.'">', '<input type="hidden" name="num" value="'.$number.'">', '<input type="hidden" name="max" value="'.$max.'">'); ?>
					</div>
					<div class="add_sales_list" data-page="1" data-total-pages="<?=$total_pages; ?>">
						<?php
						foreach($products_row[0] as $k=>$v){
							$proid=$v['ProId'];
							$url=ly200::get_url($v, 'products');
							$img=ly200::get_size_img($v['PicPath_0'], '240x240');
							$name=$v['Name'.$c['manage']['web_lang']];
							?>
							<div class="add_sales_item" data-proid="<?=$proid; ?>">
								<div class="img">
									<img src="<?=$img; ?>"<span></span>
									<div class="img_mask"></div>
								</div>
								<div class="name"><?=$name; ?></div>
								<div class="set_sales_item" data-proid="<?=$proid; ?>">
									<div class="edit_box">
										<a href="javascript:;" class="del"></a>
									</div>
									<div class="pic">
										<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
										<span></span>
									</div>
									<div class="content">
										<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
										<div class="number"><?=$v['Number']; ?></div>
										<input type="hidden" name="ProId[]" value="<?=$proid; ?>" />
									</div>
									<div class="clear"></div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	<?php }?>
</div>