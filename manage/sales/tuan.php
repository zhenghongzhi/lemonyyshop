<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'tuan'));//检查权限
if(!in_array('tuan', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

$permit_ary=array(
	'add'	=>	manage::check_permit('sales', 0, array('a'=>'tuan', 'd'=>'add')),
	'edit'	=>	manage::check_permit('sales', 0, array('a'=>'tuan', 'd'=>'edit')),
	'del'	=>	manage::check_permit('sales', 0, array('a'=>'tuan', 'd'=>'del'))
);
?>
<script type="text/javascript">var lang_str_obj={'currency':'<?=$c['manage']['currency_symbol'];?>', 'now_time':'<?=date('Y-m-d H:i', $c['time']);?>'};</script>
<div id="sales" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php
	if($c['manage']['do']=='index'){
		//团购列表
		$status_ary=array(
			0=>manage::language('{/sales.tuan.full/}'),//已满额
			1=>manage::language('{/sales.tuan.normal/}'),//售卖中
			2=>manage::language('{/sales.tuan.not_start/}'),//未开始
			3=>manage::language('{/sales.tuan.expire/}'),//已结束
		);
		?>
		<?=ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.tuan_list_init()});</script>
		<div class="inside_container">
			<h1>{/module.sales.tuan/} &nbsp;&nbsp;<span class="box_explain">( {/sales.explain.tuan_2/} )</span></h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=sales&a=tuan&d=add">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<?php if($permit_ary['edit']){?>
						<li><a class="edit bat_close" href="javascript:;">{/global.edit_bat/}</a></li>
					<?php }?>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/sales.status/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($status_ary, 'Status', '', '', '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="sales" />
						<input type="hidden" name="a" value="tuan" />
					</form>
				</div>
			</div>
			<?php
			$Keyword=str::str_code($_GET['Keyword']);
			$Status=(int)$_GET['Status'];
			$where='1';//条件
			$page_count=10;//显示数量
			$Keyword && $where.=" and ProId in(select ProId from products where Name{$c['manage']['web_lang']} like '%{$Keyword}%' or concat(Prefix, Number) like '%$Keyword%')";
			if($Status){
				if($Status==1){//售卖中
					$where.=" and StartTime<{$c['time']} and {$c['time']}<EndTime";
				}elseif($Status==2){//未开始
					$where.=" and StartTime>{$c['time']}";
				}else{//已结束
					$where.=" and EndTime<{$c['time']}";
				}
			}elseif(isset($_GET['Status']) && $_GET['Status']=='0'){
				$where.=" and BuyerCount>=TotalCount";
			}
			$tuan_row=str::str_code(db::get_limit_page('sales_tuan', $where, '*', $c['my_order'].'TId desc', (int)$_GET['page'], $page_count));
			
			if($tuan_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="36%" nowrap="nowrap">{/sales.package.product_info/}</td>
							<td width="20%" nowrap="nowrap">{/sales.package.duration/}</td>
							<td width="5%" nowrap="nowrap">{/sales.package.tuan_price/}</td>
							<td width="5%" nowrap="nowrap">{/sales.package.buyer_count/}</td>
							<td width="5%" nowrap="nowrap">{/sales.package.total_count/}</td>
							<td width="5%" nowrap="nowrap">{/sales.coupon.status/}</td>
							<td width="10%" nowrap="nowrap">{/global.time/}</td>
							<td width="6%" nowrap="nowrap">{/global.my_order/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						$pro_where='ProId in(0';
						foreach($tuan_row[0] as $v) $pro_where.=",{$v['ProId']}";
						$pro_where.=')';
						$pro_ary=array();
						$pro_row=str::str_code(db::get_all('products', $pro_where, '*', 'ProId desc'));
						foreach($pro_row as $v) $pro_ary[$v['ProId']]=$v;
						
						foreach($tuan_row[0] as $v){
							$img=ly200::get_size_img($pro_ary[$v['ProId']]['PicPath_0'], '168x168');
							$name=$pro_ary[$v['ProId']]['Name'.$c['manage']['web_lang']];
							$tuan_ary=explode('|', substr($v['PackageProId'], 1, -1));
							$price=manage::range_price($pro_ary[$v['ProId']], 1);
							$biref=$pro_ary[$v['ProId']]['BriefDescription'.$c['manage']['web_lang']];
							
							if($v['BuyerCount']>=$v['TotalCount']){
								$s_key=0;//已满额
							}elseif($v['StartTime']<$c['time'] && $c['time']<$v['EndTime']){
								$s_key=1;//售卖中
							}elseif($v['StartTime']>$c['time']){
								$s_key=2;//未开始
							}else{
								$s_key=3;//已结束
							}
						?>
						<tr>
							<?php if($permit_ary['del']){?><td><?=html::btn_checkbox('select', $v['TId']);?></td><?php }?>
							<td class="left">
								<div class="p_row">
									<div class="p_img fl"><img src="<?=$img;?>" alt="<?=$name;?>" price="<?=$price;?>" number="<?=$pro_ary[$v['ProId']]['Number'];?>" biref="<?=$biref;?>" align="absmiddle" height="50" /></div>
									<div class="p_info">
										<div class="p_name"><?=$name;?></div>
										<p class="p_price">{/products.products.price/}: <?=$price;?></p>
									</div>
									<div class="clear"></div>
								</div>
							</td>
							<td><?=date('Y-m-d H:i:s', $v['StartTime']).' <br /> '.date('Y-m-d H:i:s', $v['EndTime']);?></td>
							<td><?=$c['manage']['currency_symbol'].sprintf('%01.2f', $v['Price']);?></td>
							<td><?=$v['BuyerCount'];?></td>
							<td><?=$v['TotalCount'];?></td>
							<td nowrap="nowrap"><?=$status_ary[$s_key];?></td>
							<td><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
							<td nowrap="nowrap"<?=$permit_ary['edit']?' class="myorder_select" data-num="'.$v['MyOrder'].'"':'';?>><?=$c['manage']['my_order'][$v['MyOrder']];?></td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td nowrap="nowrap" class="operation side_by_side">
									<?php if($permit_ary['edit']){?><a href="javascript:;" class="edit" data-url="./?m=sales&a=tuan&d=edit&TId=<?=$v['TId'];?>">{/global.edit/}</a><?php }?>
									<?php if($permit_ary['del']){?>
										<dl>
											<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
											<dd class="drop_down">
												<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=sales.tuan_del&TId=<?=$v['TId'];?>" rel="del">{/global.del/}</a><?php }?>
											</dd>
										</dl>
									<?php }?>
								</td>
							<?php }?>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($tuan_row[1], $tuan_row[2], $tuan_row[3], '?'.ly200::query_string('page').'&page=');?>
				<div id="myorder_select_hide" class="hide"><?=ly200::form_select($c['manage']['my_order'], "MyOrder[]", '');?></div>
				<div id="fixed_right">
					<div class="global_container tuan_edit_form"></div>
				</div>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=sales&a=tuan&d=add');
			}?>
		</div>
	<?php
		unset($tuan_row, $pro_row, $pro_ary);
	}elseif($c['manage']['do']=='add'){
		//团购产品添加
		$where='1';
		$page_count=30;//显示数量
		$Name=str::str_code($_GET['Keyword']);
		$CateId=(int)$_GET['CateId'];
		$Name && $where.=" and (Name{$c['manage']['web_lang']} like '%$Name%' or concat(Prefix, Number) like '%$Name%')";
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}
		$p_remove_pid=$_POST['remove_pid'] ? $_POST['remove_pid'] : $_GET['remove_pid'];
		$p_remove_pid=str::ary_format($p_remove_pid, 2, '', '|');
		$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";
		$where.=$c['manage']['where']['products'];
		$row_count=db::get_row_count('products',$where);
		$total_pages=ceil($row_count/$page_count);
		$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
	?>
    <?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
    <script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();});</script>
    <div class="inside_container">
		<h1>{/module.sales.tuan/}</h1>
	</div>
	<div class="sales_content">
		<form id="edit_form" class="global_form" data-return="./?m=sales&a=tuan">
			<div class="date_rows">
				<a href="javascript:;" class="set_add fr">{/global.add/}</a>
				<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i',time()).'/'.date('Y-m-d H:i',time()); ?>" class="start_time box_input input_time" size="45" />
				<div class="clear"></div>
				<div class="blank12"></div>
				<div class="box_explain">{/sales.explain.tuan/}</div>
			</div>
			<div class="set_sales_list"></div>
			<input type="hidden" name="remove_pid" value="<?=$remove_pid ? $remove_pid : ','; ?>" />
			<input type="hidden" name="PackageProId" id="packageproid_hide" value="" />
			<input type="hidden" name="do_action" value="sales.tuan_add" />
			<input type="hidden" name="IsMain" id="is_main" value="0" />
			<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
		</form>
		<div class="rows clean fixed_btn_submit">
			<div class="input">
				<input type="button" class="btn_global btn_submit" value="{/global.save/}">
				<a href="./?m=sales&a=tuan"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
			</div>
		</div>
		<div id="fixed_right">
			<div class="global_container fixed_add_seles">
				<div class="top_title">{/global.add/} <a href="javascript:;" class="close"></a></div>
				<div class="list_menu">
					<?=manage::sales_right_search_form('sales', 'tuan', 'add'); ?>
				</div>
				<div class="add_sales_list" data-page="1" data-total-pages="<?=$total_pages; ?>">
					<?php
					foreach ($products_row[0] as $k => $v) {
						$proid = $v['ProId'];
						$url = ly200::get_url($v, 'products');
						$img = ly200::get_size_img($v['PicPath_0'], '240x240');
						$name = $v['Name' . $c['manage']['web_lang']];
                        $price = manage::range_price($v, 1);
					?>
						<div class="add_sales_item" data-proid="<?=$proid;?>">
							<div class="img">
								<img src="<?=$img;?>" /><span></span>
								<div class="img_mask"></div>
							</div>
							<div class="name"><?=$name;?></div>
							<div class="set_sales_item" data-proid="<?=$proid;?>">
								<div class="edit_box">
									<a href="javascript:;" class="del"></a>
								</div>
								<div class="pic">
									<a href="<?=$url;?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img;?>" alt="" /></a>
									<span></span>
								</div>
								<div class="content">
									<a href="<?=$url;?>" target="_blank" class="name"><?=$name;?></a>
                                    <div class="price">{/sales.tuan.originalPrice/}: <?=$price;?></div>
									<table>
										<tr>
											<td><span class="tit">{/sales.package.tuan_price/}</span></td>
											<td><span class="tit">{/sales.package.buyer_count/}</span></td>
											<td><span class="tit">{/sales.package.total_count/}</span></td>
										</tr>
										<tr>
											<td>
												<span class="unit_input" parent_null><b class="last"><?=$c['manage']['currency_symbol'];?></b><input type="text" class="box_input" name="Price[]" rel="amount" notnull parent_null="1" size="6" maxlength="255"></span>
											</td>
											<td>
												<input type="text" class="box_input" name="BuyerCount[]" rel="amount" notnull size="10" maxlength="255">
											</td>
											<td>
												<input type="text" class="box_input" name="TotalCount[]" rel="amount" notnull size="10" maxlength="255">
											</td>
										</tr>
									</table>
									<input type="hidden" name="ProId[]" value="<?=$proid;?>" />
								</div>
								<div class="clear"></div>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
    <?php
		unset($products_row);
	}elseif($c['manage']['do']=='batch_edit'){
		//团购产品添加
		$where='1';
		$id_list=str::ary_format($_GET['id_list'], 2, '', '-');
		$id_list && $where.=" and TId in ({$id_list})";
		$tuan_row=db::get_all('sales_tuan',$where,'*',$c['my_order'].'TId desc');
	?>
    <?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
    <script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();});</script>
    <div class="inside_container">
		<h1>{/module.sales.tuan/}</h1>
	</div>
	<div class="sales_content">
		<form id="edit_form" class="global_form" data-return="./?m=sales&a=tuan">
			<div class="date_rows">
				<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i',time()).'/'.date('Y-m-d H:i',time()); ?>" class="start_time box_input input_time" size="45" />
				<div class="clear"></div>
				<div class="blank12"></div>
				<div class="box_explain">{/sales.explain.tuan/}</div>
			</div>
			<div class="set_sales_list">
				<?php foreach((array)$tuan_row as $v){ 
					$proid = $v['ProId'];
					$pro_row = db::get_one('products',"ProId = '{$proid}'");
					$url = ly200::get_url($pro_row,'products');
					$name = $pro_row['Name'.$c['manage']['web_lang']];
					$img = ly200::get_size_img($pro_row['PicPath_0'],'240x240');
					?>
					<div class="set_sales_item" data-proid="<?=$proid; ?>">
						<div class="pic">
							<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
							<span></span>
						</div>
						<div class="content">
							<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
							<table>
								<tr>
									<td><span class="tit">{/sales.package.tuan_price/}</span></td>
									<td><span class="tit">{/sales.package.buyer_count/}</span></td>
									<td><span class="tit">{/sales.package.total_count/}</span></td>
								</tr>
								<tr>
									<td>
										<span class="unit_input" parent_null><b class="last"><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" name="Price[]" value="<?=$v['Price']; ?>" rel="amount" notnull parent_null="1" size="6" maxlength="255"></span>
									</td>
									<td>
										<input type="text" class="box_input" name="BuyerCount[]" value="<?=$v['BuyerCount']; ?>" rel="amount" notnull size="10" maxlength="255">
									</td>
									<td>
										<input type="text" class="box_input" name="TotalCount[]" value="<?=$v['TotalCount']; ?>" rel="amount" notnull size="10" maxlength="255">
									</td>
								</tr>
							</table>
							<input type="hidden" name="TId[]" value="<?=$v['TId']; ?>" />
						</div>
						<div class="clear"></div>
					</div>
				<?php } ?>
			</div>
			<input type="hidden" name="do_action" value="sales.tuan_batch_edit" />
			<input type="hidden" name="IsMain" id="is_main" value="0" />
			<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
		</form>
		<div class="rows clean fixed_btn_submit">
			<div class="input">
				<input type="button" class="btn_global btn_submit" value="{/global.save/}">
				<a href="./?m=sales&a=tuan"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
			</div>
		</div>
	</div>
    <?php
		unset($products_row);
	}else{
		//团购产品编辑
		$TId = (int)$_GET['TId'];
		$tuan_row = str::str_code(db::get_one('sales_tuan', "TId='$TId'"));
		$start_time = $tuan_row['StartTime'];
		$duration_time = ceil($tuan_row['EndTime'] - $start_time) / 3600;
		$ProId = $tuan_row['ProId'];
		$pro_row = str::str_code(db::get_one('products', "ProId='$ProId'"));
		$pro_img = ly200::get_size_img($pro_row['PicPath_0'], '168x168');
		$pro_name = $pro_row['Name' . $c['manage']['web_lang']];
        $pro_price = manage::range_price($pro_row, 1);
	?>
	<script type="text/javascript">$(document).ready(function(){sales_obj.tuan_edit_init();});</script>
	<div class="sales_content tuan_edit_form">
		<div class="edit_bd list_box">
			<form id="tuan_edit_form" name="tuan_edit_form" class="global_form">
				<div class="top_title">{/module.sales.tuan/}<a href="javascript:;" class="close"></a></div>
				<div class="rows_box">
					<div class="rows clean">
						<label></label>
						<div class="input">
							<div class="p_row bor">
								<div class="p_img fl pic_box"><img src="<?=$pro_img;?>" alt="<?=$pro_name;?>" align="absmiddle" /><span></span></div>
								<div class="p_info">
									<div class="p_name color_555"><?=$pro_name;?></div>
									<p class="color_888">#<?=$pro_row['Prefix'].$pro_row['Number'];?></p>
                                    <div class="price"><?=$pro_price;?></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.package.duration/}</label>
						<div class="input"><input name="PromotionTime" value="<?=date('Y-m-d H:i:s',($tuan_row['StartTime']?$tuan_row['StartTime']:$c['time'])).'/'.date('Y-m-d H:i:s', ($tuan_row['EndTime']?$tuan_row['EndTime']:$c['time']));?>" type="text" class="box_input input_time" size="45" readonly></div>
					</div>
					<div class="rows clean">
						<label>{/sales.package.tuan_price/}</label>
						<div class="input">
							<span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input name="Price" value="<?=$tuan_row['Price'];?>" type="text" class="box_input" size="10" maxlength="10" rel="amount" notnull parent_null="1" /></span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.package.buyer_count/}</label>
						<div class="box_explain">{/sales.explain.tuan_0/}</div>
						<div class="input"><input name="BuyerCount" type="text" value="<?=$tuan_row['BuyerCount'];?>" class="box_input" notnull size="5" maxlength="5"></div>
					</div>
					<div class="rows clean">
						<label>{/sales.package.total_count/}</label>
						<div class="box_explain">{/sales.explain.tuan_1/}</div>
						<div class="input"><input name="TotalCount" type="text" value="<?=$tuan_row['TotalCount'];?>" class="box_input" notnull size="5" maxlength="5"></div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<input type="button" class="btn_global btn_cancel" value="{/global.return/}">
						</div>
					</div>
				</div>
				<input type="hidden" id="TId" name="TId" value="<?=$TId;?>" />
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" name="do_action" value="sales.tuan_edit" />
			</form>
		</div>
	</div>
	<?php
    	unset($tuan_row, $pro_row);
	}
	?>
</div>