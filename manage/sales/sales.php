<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'sales'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('sales', 0, array('a'=>'sales', 'd'=>'add')),
	'edit'	=>	manage::check_permit('sales', 0, array('a'=>'sales', 'd'=>'edit')),
	'del'	=>	manage::check_permit('sales', 0, array('a'=>'sales', 'd'=>'del'))
);
?>
<script type="text/javascript">var lang_str_obj={'currency':'<?=$c['manage']['currency_symbol'];?>', 'now_time':'<?=date('Y-m-d H:i', $c['time']);?>'};</script>
<div id="sales" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php
	if($c['manage']['do']=='index'){
		//促销列表
		$status_ary=array(
			1=>manage::language('{/sales.tuan.normal/}'),//售卖中
			2=>manage::language('{/sales.tuan.not_start/}'),//未开始
			3=>manage::language('{/sales.tuan.expire/}'),//已结束
		);
		echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
		?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.sales_list_init();});</script>
		<div class="inside_container">
			<h1>{/module.sales.sales/}</h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=sales&a=sales&d=add">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<?php if($permit_ary['edit']){?>
						<li><a class="edit bat_close" href="javascript:;">{/global.edit_bat/}</a></li>
					<?php }?>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/sales.status/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($status_ary, 'Status', '', '', '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="sales" />
						<input type="hidden" name="a" value="sales" />
					</form>
				</div>
			</div>
			<?php
			$Keyword=str::str_code($_GET['Keyword']);
			$Status=(int)$_GET['Status'];
			$where='IsPromotion=1';//条件
			$page_count=30;//显示数量
			$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(Prefix, Number) like '%$Keyword%')";
			if($Status){
				if($Status==1){//售卖中
					$where.=" and StartTime<{$c['time']} and {$c['time']}<EndTime";
				}elseif($Status==2){//未开始
					$where.=" and StartTime>{$c['time']}";
				}else{//已结束
					$where.=" and EndTime<{$c['time']}";
				}
			}
			$sales_row=str::str_code(db::get_limit_page('products', $where, '*', 'EndTime desc, StartTime desc,'.$c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
			
			if($sales_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="44%" nowrap="nowrap">{/sales.package.product_info/}</td>
							<td width="14%" nowrap="nowrap">{/sales.tuan.rele_type/}</td>
							<td width="24%" nowrap="nowrap">{/sales.package.duration/}</td>
							<td width="14%" nowrap="nowrap">{/sales.coupon.status/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($sales_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							$name=$v['Name'.$c['manage']['web_lang']];
							$price=manage::range_price($v, 1);
							$biref=$v['BriefDescription'.$c['manage']['web_lang']];
							
							if($v['StartTime']<$c['time'] && $c['time']<$v['EndTime']){
								$s_key=1;//售卖中
							}elseif($v['StartTime']>$c['time']){
								$s_key=2;//未开始
							}else{
								$s_key=3;//已结束
							}
						?>
						<tr>
							<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
							<td class="left">
								<div class="p_row">
									<div class="p_img fl"><img src="<?=$img;?>" alt="<?=$name;?>" price="<?=$price;?>" number="<?=$v['Prefix'].$v['Number'];?>" biref="<?=$biref;?>" align="absmiddle" height="50" /></div>
									<div class="p_info"><div class="p_name"><?=$name;?></div></div>
									<div class="clear"></div>
								</div>
							</td>
							<td nowrap="nowrap"><?=$v['PromotionType']==0?'{/products.products.money/}('.$c['manage']['currency_symbol'].$v['PromotionPrice'].')':'{/products.products.discount/}('.$v['PromotionDiscount'].'%)';?></td>
							<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['StartTime']).' <br /> '.date('Y-m-d H:i:s', $v['EndTime']);?></td>
							<td nowrap="nowrap"><?=$status_ary[$s_key];?></td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td nowrap="nowrap" class="operation side_by_side">
									<?php if($permit_ary['edit']){?><a href="javascript:;" class="edit" data-url="./?m=sales&a=sales&d=edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
									<?php if($permit_ary['del']){?>
										<dl>
											<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
											<dd class="drop_down">
												<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=sales.sales_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
											</dd>
										</dl>
									<?php }?>
								</td>
	
							<?php }?>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($sales_row[1], $sales_row[2], $sales_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=sales&a=sales&d=add');
			}?>
		</div>
		<div id="fixed_right">
			<div class="global_container sales_edit_form"></div>
		</div>
	<?php
	}elseif($c['manage']['do']=='add'){
		//促销产品添加
		$where="(IsPromotion=0 or (IsPromotion=1 and (EndTime<{$c['time']} or StartTime>{$c['time']})))";
		$page_count=30;//显示数量
		$Keyword=str::str_code($_GET['Keyword']);
		$CateId=(int)$_GET['CateId'];
		$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(Prefix, Number) like '%$Keyword%')";
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}

		$p_remove_pid=$_POST['remove_pid'] ? $_POST['remove_pid'] : $_GET['remove_pid'];
		$p_remove_pid=str::ary_format($p_remove_pid, 2, '', '|');
		$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";

		$row_count=db::get_row_count('products',$where.$c['manage']['where']['products']);
		$total_pages=ceil($row_count/$page_count);
		$products_row=str::str_code(db::get_limit_page('products', $where.$c['manage']['where']['products'], '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
		?>
		<?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();sales_obj.sales_edit_init();});</script>
        <div class="inside_container">
			<h1>{/module.sales.sales/}</h1>
		</div>
		<div class="sales_content">
			<form id="edit_form" class="global_form" data-return="./?m=sales&a=sales">
				<div class="date_rows">
					<a href="javascript:;" class="set_add fr">{/global.add/}</a>
					<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i',time()).'/'.date('Y-m-d H:i',time()); ?>" class="start_time box_input input_time" size="45" />
					<div class="clear"></div>
					<div class="blank12"></div>
					<div class="box_explain">{/sales.explain.sales/}</div>
				</div>
				<div class="set_sales_list"></div>
				<input type="hidden" name="remove_pid" value="<?=$remove_pid ? $remove_pid : ','; ?>" />
				<input type="hidden" name="PackageProId" id="packageproid_hide" value="" />
				<input type="hidden" name="do_action" value="sales.sales_add" />
				<input type="hidden" name="IsMain" id="is_main" value="0" />
				<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			</form>
			<div class="rows clean fixed_btn_submit">
				<div class="input">
					<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					<a href="./?m=sales&a=sales"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
				</div>
			</div>
			<div id="fixed_right">
				<div class="global_container fixed_add_seles">
					<div class="top_title">{/global.add/} <a href="javascript:;" class="close"></a></div>
					<div class="list_menu">
						<?=manage::sales_right_search_form('sales', 'sales', 'add'); ?>
					</div>
					<div class="add_sales_list" data-page="1" data-total-pages="<?=$total_pages; ?>">
						<?php
						foreach($products_row[0] as $k=>$v){
							$proid=$v['ProId'];
							$url=ly200::get_url($v, 'products');
							$img=ly200::get_size_img($v['PicPath_0'], '240x240');
							$name=$v['Name'.$c['manage']['web_lang']];
							?>
							<div class="add_sales_item" data-proid="<?=$proid; ?>">
								<div class="img">
									<img src="<?=$img; ?>"<span></span>
									<div class="img_mask"></div>
								</div>
								<div class="name"><?=$name; ?></div>
								<div class="set_sales_item" data-proid="<?=$proid; ?>">
									<div class="edit_box">
										<a href="javascript:;" class="del"></a>
									</div>
									<div class="pic">
										<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
										<span></span>
									</div>
									<div class="content">
										<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
										<span class="unit_input table" parent_null>
											<b>
												<span data-promotiontype="1" <?=$v['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>{/products.products.discount/}</span>
												<span data-promotiontype="0" <?=$v['PromotionType']==1 ? 'style="display:none;"' : '' ;?>>{/products.products.money/}</span>
											</b>
											<input name="PromotionDiscount[]" type="text" parent_null="1" rel="amount" data-promotiontype="1" value="<?=$v['PromotionDiscount']?$v['PromotionDiscount']:100;?>" class="box_input" size="5" maxlength="5" <?=$v['PromotionType']==1?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
											<input name="PromotionPrice[]" type="text" parent_null="1" rel="amount" data-promotiontype="0"  value="<?=$v['PromotionPrice']>0 ? $v['PromotionPrice'] : '';?>" class="box_input" size="5" maxlength="10" <?=$v['PromotionType']==0?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
											<b class="last box_select_down">
												<span class="head">
													<span data-promotiontype="1"<?=$v['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>%</span>
													<span data-promotiontype="0"<?=$v['PromotionType']==1 ? 'style="display:none;"' : '' ;?>><?=$c['manage']['currency_symbol']?></span>
													<em></em>
												</span>
												<ul class="list">
													<li><span><?=$c['manage']['currency_symbol']?></span><input type="radio" name="PromotionType[<?=$proid; ?>]" value="0" class="promotion_type" <?=$v['PromotionType']==0?' checked':'';?>> </li>
													<li><span>%</span><input type="radio" name="PromotionType[<?=$proid; ?>]" value="1" class="promotion_type" <?=$v['PromotionType']==1?' checked':'';?>> </li>
												</ul>
											</b>
										</span>		
										<input type="hidden" name="ProId[]" value="<?=$proid; ?>" />
									</div>
									<div class="clear"></div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
    <?php
	}elseif($c['manage']['do']=='batch_edit'){
		//促销产品添加
		$where='1';
		$id_list=str::ary_format($_GET['id_list'], 2, '', '-');
		$id_list && $where.=" and ProId in ({$id_list})";
		$sales_row=db::get_all('products',$where,'*',$c['my_order'].'ProId desc');
		?>
		<?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();sales_obj.sales_edit_init();});</script>
        <div class="inside_container">
			<h1>{/module.sales.sales/}</h1>
		</div>
		<div class="sales_content">
			<form id="edit_form" class="global_form" data-return="./?m=sales&a=sales">
				<div class="date_rows">
					<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i',time()).'/'.date('Y-m-d H:i',time()); ?>" class="start_time box_input input_time" size="45" />
					<div class="clear"></div>
					<div class="blank12"></div>
					<div class="box_explain">{/sales.explain.sales/}</div>
				</div>
				<div class="set_sales_list">
					<?php foreach((array)$sales_row as $v){ 
						$proid = $v['ProId'];
						$url=ly200::get_url($v, 'products');
						$name = $v['Name'.$c['manage']['web_lang']];
						$img = ly200::get_size_img($v['PicPath_0'],'240x240');
						?>
						<div class="set_sales_item" data-proid="<?=$proid; ?>">
							<div class="pic">
								<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
								<span></span>
							</div>
							<div class="content">
								<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
								<span class="unit_input table" parent_null>
									<b>
										<span data-promotiontype="1" <?=$v['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>{/products.products.discount/}</span>
										<span data-promotiontype="0" <?=$v['PromotionType']==1 ? 'style="display:none;"' : '' ;?>>{/products.products.money/}</span>
									</b>
									<input name="PromotionDiscount[]" type="text" parent_null="1" rel="amount" data-promotiontype="1" value="<?=$v['PromotionDiscount']?$v['PromotionDiscount']:100;?>" class="box_input" size="5" maxlength="5" <?=$v['PromotionType']==1?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
									<input name="PromotionPrice[]" type="text" parent_null="1" rel="amount" data-promotiontype="0"  value="<?=$v['PromotionPrice']>0 ? $v['PromotionPrice'] : '';?>" class="box_input" size="5" maxlength="10" <?=$v['PromotionType']==0?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
									<b class="last box_select_down">
										<span class="head">
											<span data-promotiontype="1"<?=$v['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>%</span>
											<span data-promotiontype="0"<?=$v['PromotionType']==1 ? 'style="display:none;"' : '' ;?>><?=$c['manage']['currency_symbol']?></span>
											<em></em>
										</span>
										<ul class="list">
											<li><span><?=$c['manage']['currency_symbol']?></span><input type="radio" name="PromotionType[<?=$proid; ?>]" value="0" class="promotion_type" <?=$v['PromotionType']==0?' checked':'';?>> </li>
											<li><span>%</span><input type="radio" name="PromotionType[<?=$proid; ?>]" value="1" class="promotion_type" <?=$v['PromotionType']==1?' checked':'';?>> </li>
										</ul>
									</b>
								</span>
								<input type="hidden" name="ProId[]" value="<?=$proid; ?>" />
							</div>
							<div class="clear"></div>
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="do_action" value="sales.sales_batch_edit" />
				<input type="hidden" name="Type" id="type_hide" value="3" />
				<input type="hidden" name="IsMain" id="is_main" value="0" />
				<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			</form>
			<div class="rows clean fixed_btn_submit">
				<div class="input input_button">
					<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					<a href="./?m=sales&a=sales"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
				</div>
			</div>
		</div>
    <?php
	}else{
		//促销产品编辑
		$ProId=(int)$_GET['ProId'];
		$products_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
		$pro_img=ly200::get_size_img($products_row['PicPath_0'], '168x168');
		$pro_name=$products_row['Name'.$c['manage']['web_lang']];
		$pro_price=manage::range_price($products_row, 1);
	?>
	<script type="text/javascript">$(document).ready(function(){sales_obj.sales_edit_init();});</script>
	<div class="sales_content sales_edit_form">
		<form id="sales_edit_form" name="sales_edit_form" class="global_form">
			<div class="top_title">{/module.sales.sales/}<a href="javascript:;" class="close"></a></div>
			<div class="rows_box">
				<div class="rows clean">
					<label></label>
					<span class="input">
						<div class="p_row bor">
							<div class="p_img fl pic_box"><img src="<?=$pro_img;?>" alt="<?=$pro_name;?>" align="absmiddle" /><span></span></div>
							<div class="p_info">
								<div class="p_name color_555"><?=$pro_name;?></div>
								<p class="color_888">#<?=$products_row['Prefix'].$products_row['Number'];?></p>
							</div>
							<div class="clear"></div>
						</div>
					</span>
					<div class="clear"></div>
				</div>
				<div class="rows clean">
					<label>{/products.products.promotion/}{/global.time/}</label>
					<span class="input"><input name="PromotionTime" value="<?=date('Y-m-d H:i:s',($products_row['StartTime']?$products_row['StartTime']:$c['time'])).'/'.date('Y-m-d H:i:s', ($products_row['EndTime']?$products_row['EndTime']:$c['time']));?>" type="text" class="box_input input_time" size="45" readonly></span>
					<div class="clear"></div>
				</div>
				<div class="rows clean">
					<label>{/module.sales.sales/}</label>
					<div class="box_explain">{/sales.explain.sales/}</div>
					<span class="input content">
						<span class="unit_input" parent_null>
							<b>
								<span data-promotiontype="1" <?=$products_row['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>{/products.products.discount/}</span>
								<span data-promotiontype="0" <?=$products_row['PromotionType']==1 ? 'style="display:none;"' : '' ;?>>{/products.products.money/}</span>
							</b>
							<input name="PromotionDiscount" type="text" parent_null="1" rel="amount" data-promotiontype="1" value="<?=$products_row['PromotionDiscount']?$products_row['PromotionDiscount']:100;?>" class="box_input" size="5" maxlength="5" <?=$products_row['PromotionType']==1?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
							<input name="PromotionPrice" type="text" parent_null="1" rel="amount" data-promotiontype="0"  value="<?=$products_row['PromotionPrice']>0 ? $products_row['PromotionPrice'] : '';?>" class="box_input" size="5" maxlength="10" <?=$products_row['PromotionType']==0?'notnull="notnull" style="display:inline-block;"':'style="display:none;"';?> />
							<b class="last box_select_down">
								<span class="head">
									<span data-promotiontype="1"<?=$products_row['PromotionType']==0 ? 'style="display:none;"' : '' ;?>>%</span>
									<span data-promotiontype="0"<?=$products_row['PromotionType']==1 ? 'style="display:none;"' : '' ;?>><?=$c['manage']['currency_symbol']?></span>
									<em></em>
								</span>
								<ul class="list">
									<li><span><?=$c['manage']['currency_symbol']?></span><input type="radio" name="PromotionType" value="0" class="promotion_type" <?=$products_row['PromotionType']==0?' checked':'';?>> </li>
									<li><span>%</span><input type="radio" name="PromotionType" value="1" class="promotion_type" <?=$products_row['PromotionType']==1?' checked':'';?>> </li>
								</ul>
							</b>
						</span>
					</span>
				</div>
				<div class="rows clean">
					<label></label>
					<div class="input input_button">
						<input type="button" class="btn_global btn_submit" value="{/global.save/}">
						<input type="button" class="btn_global btn_cancel" value="{/global.return/}">
					</div>
				</div>
			</div>
			<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="do_action" value="sales.sales_edit" />
		</form>
	</div>
	<?php 
		unset($products_row, $pro_row);
	}
	?>
</div>