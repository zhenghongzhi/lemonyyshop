<?php !isset($c) && exit();?>
<?php
manage::check_permit('sales', 1, array('a'=>'seckill'));//检查权限
if(!in_array('seckill', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

$permit_ary=array(
	'add'	=>	manage::check_permit('sales', 0, array('a'=>'seckill', 'd'=>'add')),
	'edit'	=>	manage::check_permit('sales', 0, array('a'=>'seckill', 'd'=>'edit')),
	'del'	=>	manage::check_permit('sales', 0, array('a'=>'seckill', 'd'=>'del'))
);
?>
<script type="text/javascript">var lang_str_obj={'currency':'<?=$c['manage']['currency_symbol'];?>', 'now_time':'<?=date('Y-m-d H:i', $c['time']);?>'};</script>
<div id="sales" class="r_con_wrap <?=$c['manage']['cdx_limit'];?>">
	<?php
	if($c['manage']['do']=='index'){
		//秒杀列表
		$status_ary=array(
			1=>manage::language('{/sales.tuan.normal/}'),//售卖中
			2=>manage::language('{/sales.tuan.sold_out/}'),//售馨
			3=>manage::language('{/sales.tuan.not_start/}'),//未开始
			4=>manage::language('{/sales.tuan.expire/}'),//已结束
		);
		?>
		<?=ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.seckill_list_init();});</script>
		<div class="inside_container">
			<h1>{/module.sales.seckill/}</h1>
		</div>
		<div class="inside_table">
			<?php 
				$PeriodId=(int)$_GET['PeriodId'];
				$time=time();
				$seck_period_row=db::get_all('sales_seckill_period', "EndTime+30*24*3600 > '{$c['time']}'", '*', 'StartTime asc,EndTime asc');
				$period_row=array();
				foreach((array)$seck_period_row as $v){
					$period_row[$v['PId']]=$v;
				}
				if($seck_period_row){
					$cur_row=$seck_period_row[0];
					$period_row[$PeriodId] && $cur_row=$period_row[$PeriodId];
					$PeriodId || $PeriodId=$seck_period_row[0]['PId'];
				?>
				<dl class="period box_drop_down_menu fl">
					<dt class="more"><span><?=date('Y-m-d H:i:s',$cur_row['StartTime']).' ~ '.date('Y-m-d H:i:s',$cur_row['EndTime']); ?></span> <em></em></dt>
					
					<dd class="more_menu drop_down">
						<?php foreach((array)$seck_period_row as $v){ ?>
							<div class="item">
								<div class="opation fr">
									<a href="javascript:;" class="fr del" data-id="<?=$v['PId']; ?>"></a>
									<a href="./?m=sales&a=seckill&d=batch_edit&PId=<?=$v['PId']; ?>" class="fr edit"></a>
								</div>
								<a href="<?='?'.ly200::query_string('PeriodId,Keyword').'&PeriodId='.$v['PId']; ?>"><?=date('Y-m-d H:i:s',$v['StartTime']).' ~ '.date('Y-m-d H:i:s',$v['EndTime']); ?></a>
							</div>
						<?php } ?>
					</dd>
				</dl>
				<div class="clear"></div>
			<?php } ?>
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=sales&a=seckill&d=add">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/sales.status/}</label>
								<div class="input">
									<div class="box_select"><?=ly200::form_select($status_ary, 'Status', '', '', '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="sales" />
						<input type="hidden" name="a" value="seckill" />
					</form>
				</div>
			</div>
			<?php
			$Keyword=str::str_code($_GET['Keyword']);
			$Status=(int)$_GET['Status'];
			$page_count=10; //显示数量
			$where='1'; //条件
			!$Keyword && $PeriodId && $where.=" and PeriodId='{$PeriodId}'";
			$Keyword && $where.=" and ProId in(select ProId from products where Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(Prefix, Number) like '%$Keyword%')";
			if($Status){
				if($Status==1){ //售卖中
					$where.=" and StartTime<{$c['time']} and {$c['time']}<EndTime and RemainderQty>0";
				}elseif($Status==2){ //售馨
					$where.=" and StartTime<{$c['time']} and {$c['time']}<EndTime and RemainderQty=0";
				}elseif($Status==3){ //未开始
					$where.=" and StartTime>{$c['time']}";
				}else{//已结束
					$where.=" and EndTime<{$c['time']}";
				}
			}
			$seckill_row=str::str_code(db::get_limit_page('sales_seckill', $where, '*', $c['my_order'].'SId desc', (int)$_GET['page'], $page_count));
			
			$pro_where='ProId in(0';
			foreach($seckill_row[0] as $v){
				$pro_where.=",{$v['ProId']}";
			}
			$pro_where.=')';
			$pro_ary=array();
			$pro_row=str::str_code(db::get_all('products', $pro_where, '*', 'ProId desc'));
			foreach($pro_row as $v){
				$pro_ary[$v['ProId']]=$v;
			}
			
			if($seckill_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="30%" nowrap="nowrap">{/sales.package.product_info/}</td>
							<td width="20%" nowrap="nowrap">{/sales.package.duration/}</td>
							<td width="8%" nowrap="nowrap">{/sales.package.seckill_price/}</td>
							<td width="5%" nowrap="nowrap">{/products.products.qty/}</td>
							<td width="5%" nowrap="nowrap">{/sales.package.max_qty/}</td>
							<td width="8%" nowrap="nowrap">{/sales.coupon.status/}</td>
							<td width="8%" nowrap="nowrap">{/global.time/}</td>
							<td width="9%" nowrap="nowrap">{/global.my_order/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($seckill_row[0] as $v){
							$img=ly200::get_size_img($pro_ary[$v['ProId']]['PicPath_0'], '168x168');
							$name=$pro_ary[$v['ProId']]['Name'.$c['manage']['web_lang']];
							$seckill_ary=explode('|', substr($v['PackageProId'], 1, -1));
							$price=manage::range_price($pro_ary[$v['ProId']], 1);
							$biref=$pro_ary[$v['ProId']]['BriefDescription'.$c['manage']['web_lang']];
							
							if($v['StartTime']<$c['time'] && $c['time']<$v['EndTime'] && (int)$v['RemainderQty']){
								$s_key=1;//售卖中
							}elseif($v['StartTime']<$c['time'] && $c['time']<$v['EndTime'] && (int)$v['RemainderQty']==0){
								$s_key=2;//售馨
							}elseif($v['StartTime']>$c['time']){
								$s_key=3;//未开始
							}else{
								$s_key=4;//已结束
							}
						?>
						<tr>
							<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['SId']);?></td><?php }?>
							<td class="left">
								<div class="p_row">
									<div class="p_img fl"><img src="<?=$img;?>" alt="<?=$name;?>" price="<?=$price;?>" number="<?=$pro_ary[$v['ProId']]['Number'];?>" biref="<?=$biref;?>" align="absmiddle" height="50" /></div>
									<div class="p_info">
										<div class="p_name"><?=$name;?></div>
										<p class="p_price">{/products.products.price/}: <?=$price;?></p>
									</div>
									<div class="clear"></div>
								</div>
							</td>
							<td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['StartTime']).'<br /> '.date('Y-m-d H:i:s', $v['EndTime']);?></td>
							<td nowrap="nowrap"><?=$c['manage']['currency_symbol'].sprintf('%01.2f', $v['Price']);?></td>
							<td nowrap="nowrap"><?=$v['RemainderQty']==0?'<span class="fc_red">'.$v['RemainderQty'].'</span>':$v['RemainderQty'];?> / <?=$v['Qty'];?></td>
							<td nowrap="nowrap"><?=$v['MaxQty'];?></td>
							<td nowrap="nowrap"><?=$status_ary[$s_key];?></td>
							<td nowrap="nowrap"><?=date('Y-m-d', $v['AccTime']).'<br />'.date('H:i:s', $v['AccTime']);?></td>
							<td nowrap="nowrap"<?=$permit_ary['edit']?' class="myorder_select" data-num="'.$v['MyOrder'].'"':'';?>><?=$c['manage']['my_order'][$v['MyOrder']];?></td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?>
								<td nowrap="nowrap" class="operation side_by_side">
									<?php if($permit_ary['edit']){?><a href="javascript:;" class="edit" data-url="./?m=sales&a=seckill&d=edit&SId=<?=$v['SId'];?>">{/global.edit/}</a><?php }?>
									<?php if($permit_ary['del']){?>
										<dl>
											<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
											<dd class="drop_down">
												<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=sales.seckill_del&SId=<?=$v['SId'];?>" rel="del">{/global.del/}</a><?php }?>
											</dd>
										</dl>
									<?php }?>
								</td>
							<?php }?>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($seckill_row[1], $seckill_row[2], $seckill_row[3], '?'.ly200::query_string('page').'&page=');?>
				<div id="myorder_select_hide" class="hide"><div class="box_select"><?=ly200::form_select($c['manage']['my_order'], "MyOrder[]", '');?></div></div>
				<div id="fixed_right">
					<div class="global_container seckill_edit_form"></div>
				</div>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=sales&a=seckill&d=add');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='add'){
		//秒杀产品添加
		$where='1';
		$page_count=30;//显示数量
		$Keyword=str::str_code($_GET['Keyword']);
		$CateId=(int)$_GET['CateId'];
		$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat(Prefix, Number) like '%$Keyword%')";
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}
		$p_remove_pid=$_POST['remove_pid'] ? $_POST['remove_pid'] : $_GET['remove_pid'];
		$p_remove_pid=str::ary_format($p_remove_pid, 2, '', '|');
		$p_remove_pid && $where.=" and ProId not in ({$p_remove_pid})";
		$where.=$c['manage']['where']['products'];
		$row_count=db::get_row_count('products',$where);
		$total_pages=ceil($row_count/$page_count);
		$products_row=str::str_code(db::get_limit_page('products', $where, '*', $c['my_order'].'ProId desc', (int)$_GET['page'], $page_count));
		?>
		<?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();});</script>
		<div class="inside_container">
			<h1>{/module.sales.seckill/}</h1>
		</div>
		<div class="sales_content">
			<form id="edit_form" class="global_form" data-return="./?m=sales&a=seckill">
				<div class="date_rows">
					<a href="javascript:;" class="set_add fr">{/global.add/}</a>
					<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i',time()).'/'.date('Y-m-d H:i',time()); ?>" class="start_time box_input input_time" size="45" />
					<div class="clear"></div>
					<div class="blank12"></div>
					<div class="box_explain">{/sales.explain.seckill/}</div>
				</div>
				<div class="set_sales_list"></div>
				<input type="hidden" name="remove_pid" value="<?=$remove_pid ? $remove_pid : ','; ?>" />
				<input type="hidden" name="PackageProId" id="packageproid_hide" value="" />
				<input type="hidden" name="do_action" value="sales.seckill_add" />
				<input type="hidden" name="IsMain" id="is_main" value="0" />
				<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			</form>
			<div class="rows clean fixed_btn_submit">
				<div class="input">
					<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					<a href="./?m=sales&a=seckill"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
				</div>
			</div>
			<div id="fixed_right">
				<div class="global_container fixed_add_seles">
					<div class="top_title">{/global.add/} <a href="javascript:;" class="close"></a></div>
					<div class="list_menu">
						<?=manage::sales_right_search_form('sales', 'seckill', 'add'); ?>
					</div>
					<div class="add_sales_list" data-page="1" data-total-pages="<?=$total_pages; ?>">
						<?php
						foreach($products_row[0] as $k=>$v){
							$proid=$v['ProId'];
							$url=ly200::get_url($v, 'products');
							$img=ly200::get_size_img($v['PicPath_0'], '240x240');
							$name=$v['Name'.$c['manage']['web_lang']];
							?>
							<div class="add_sales_item" data-proid="<?=$proid; ?>">
								<div class="img">
									<img src="<?=$img; ?>"<span></span>
									<div class="img_mask"></div>
								</div>
								<div class="name"><?=$name; ?></div>
								<div class="set_sales_item" data-proid="<?=$proid; ?>">
									<div class="edit_box">
										<a href="javascript:;" class="del"></a>
									</div>
									<div class="pic">
										<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
										<span></span>
									</div>
									<div class="content">
										<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
										<table>
											<tr>
												<td><span class="tit">{/sales.package.seckill_price/}</span></td>
												<td><span class="tit">{/products.products.qty/}</span></td>
												<td><span class="tit">{/sales.package.max_qty/}</span></td>
												<?php /*<td><span class="tit">{/sales.package.remainder_qty/}</span></td>*/ ?>
											</tr>
											<tr>
												<td>
													<span class="unit_input" parent_null><b class="last"><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" name="Price[]" rel="amount" notnull parent_null="1" size="6" maxlength="255"></span>
												</td>
												<td>
													<input type="text" class="box_input" name="Qty[]" rel="amount" notnull size="10" maxlength="255">
												</td>
												<td>
													<input type="text" class="box_input" name="MaxQty[]" rel="amount" notnull size="10" maxlength="255">
												</td>
												<?php /*
												<td>
													<span class="unit_input" parent_null><input type="text" class="box_input" name="RemainderQty[]" rel="amount" notnull parent_null="1" size="10" maxlength="255"></span>
												</td>*/ ?>
											</tr>
										</table>
										<input type="hidden" name="ProId[]" value="<?=$proid; ?>" />
									</div>
									<div class="clear"></div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
    <?php
	}elseif($c['manage']['do']=='batch_edit'){
		//秒杀产品添加
		$where='1';
		$PId=(int)$_GET['PId'];
		$seck_period_row=db::get_one('sales_seckill_period', "PId='{$PId}'", '*');
		$PId && $where.=" and PeriodId='{$PId}'";
		$seckill_row=db::get_all('sales_seckill',$where,'*',$c['my_order'].'SId desc');
		?>
		<?=ly200::load_static('/static/js/plugin/drag/drag.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');?>
		<script type="text/javascript">$(document).ready(function(){sales_obj.package_edit_init();});</script>
        <div class="inside_container">
			<h1>{/module.sales.seckill/}</h1>
		</div>
		<div class="sales_content">
			<form id="edit_form" class="global_form" data-return="./?m=sales&a=seckill">
				<div class="date_rows">
					<input name="PromotionTime" type="text" value="<?=date('Y-m-d H:i:s',$seck_period_row['StartTime']).'/'.date('Y-m-d H:i:s',$seck_period_row['EndTime']); ?>" class="start_time box_input input_time" size="45" />
					<div class="clear"></div>
					<div class="blank12"></div>
					<div class="box_explain">{/sales.explain.seckill/}</div>
				</div>
				<div class="set_sales_list">
					<?php foreach((array)$seckill_row as $v){ 
						$proid = $v['ProId'];
						$pro_row = db::get_one('products',"ProId = '{$proid}'");
						$url = ly200::get_url($pro_row,'products');
						$name = $pro_row['Name'.$c['manage']['web_lang']];
						$img = ly200::get_size_img($pro_row['PicPath_0'],'240x240');
						?>
						<div class="set_sales_item" data-proid="<?=$proid; ?>">
							<div class="pic">
								<a href="<?=$url; ?>" target="_blank" title="<?=$name; ?>"><img src="<?=$img; ?>" alt=""></a>
								<span></span>
							</div>
							<div class="content">
								<a href="<?=$url; ?>" target="_blank" class="name"><?=$name; ?></a>
								<table>
									<tr>
										<td><span class="tit">{/sales.package.seckill_price/}</span></td>
										<td><span class="tit">{/products.products.qty/}</span></td>
										<td><span class="tit">{/sales.package.max_qty/}</span></td>
										<?php /*<td><span class="tit">{/sales.package.remainder_qty/}</span></td>*/ ?>
									</tr>
									<tr>
										<td>
											<span class="unit_input" parent_null><b class="last"><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" name="Price[]" value="<?=$v['Price']; ?>" rel="amount" notnull parent_null="1" size="6" maxlength="255"></span>
										</td>
										<td>
											<input type="text" class="box_input" name="Qty[]" value="<?=$v['Qty']; ?>" rel="amount" notnull size="10" maxlength="255">
										</td>
										<td>
											<input type="text" class="box_input" name="MaxQty[]" value="<?=$v['MaxQty']; ?>" rel="amount" notnull size="10" maxlength="255">
										</td>
										<?php /*<td>
											<span class="unit_input" parent_null><input type="text" class="box_input" name="RemainderQty[]" value="<?=$v['RemainderQty']; ?>" rel="amount" notnull parent_null="1" size="10" maxlength="255"></span>*/ ?>
										</td>
									</tr>
								</table>
								<input type="hidden" name="SId[]" value="<?=$v['SId']; ?>">
							</div>
							<div class="clear"></div>
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="do_action" value="sales.seckill_batch_edit" />
				<input type="hidden" name="IsMain" id="is_main" value="0" />
				<input type="hidden" id="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
				<input type="hidden" name="PId" value="<?=$PId; ?>">
			</form>
			<div class="rows clean fixed_btn_submit">
				<div class="input input_button">
					<input type="button" class="btn_global btn_submit" value="{/global.save/}">
					<a href="./?m=sales&a=seckill"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
				</div>
			</div>
		</div>
    <?php
	}else{
		//秒杀产品编辑
		$SId=(int)$_GET['SId'];
		$seckill_row=str::str_code(db::get_one('sales_seckill', "SId='$SId'"));
		$ProId=$seckill_row['ProId'];
		$pro_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
		$pro_img=ly200::get_size_img($pro_row['PicPath_0'], '168x168');
		$pro_name=$pro_row['Name'.$c['manage']['web_lang']];
	?>
	<script type="text/javascript">$(document).ready(function(){sales_obj.seckill_edit_init();});</script>
	<div class="sales_content seckill_edit_form">
		<div class="edit_bd list_box">
			<form id="seckill_edit_form" name="seckill_edit_form" class="global_form">
				<div class="top_title">{/module.sales.seckill/}<a href="javascript:;" class="close"></a></div>
				<div class="rows_box">
					<div class="rows clean">
						<label></label>
						<div class="input">
							<div class="p_row bor">
								<div class="p_img fl pic_box"><img src="<?=$pro_img;?>" alt="<?=$pro_name;?>" align="absmiddle" /><span></span></div>
								<div class="p_info">
									<div class="p_name color_555"><?=$pro_name;?></div>
									<p class="color_888">#<?=$pro_row['Prefix'].$pro_row['Number'];?></p>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					<div class="rows clean">
						<label>{/sales.package.duration/}</label>
						<div class="input">
							<!-- <input name="PromotionTime" value="<?=date('Y-m-d H:i:s',($seckill_row['StartTime']?$seckill_row['StartTime']:$c['time'])).'/'.date('Y-m-d H:i:s', ($seckill_row['EndTime']?$seckill_row['EndTime']:$c['time']));?>" type="text" class="box_input input_time" size="45" readonly> -->
							<?php $seck_period_row=db::get_all('sales_seckill_period', "(EndTime > '{$c['time']}') or PId='{$seckill_row['PeriodId']}'", '*', 'StartTime asc,EndTime asc'); ?>
							<div class="box_select">
								<select name="PeriodId">
									<?php foreach((array)$seck_period_row as $v){ ?>
										<option value="<?=$v['PId']; ?>" <?=$v['PId']==$seckill_row['PeriodId'] ? 'selected="selected"' : ''; ?> ><?=date('Y-m-d H:i:s', $v['StartTime']).' ~ '.date('Y-m-d H:i:s', $v['EndTime']); ?></option>
									<?php } ?>
								</select>
							</div>
							
						</div>
					</div>

					<div class="rows clean">
						<label>{/sales.package.seckill_price/}</label>
						<div class="input">
							<span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input name="Price" value="<?=$seckill_row['Price'];?>" type="text" class="box_input" size="10" maxlength="10" rel="amount" notnull parent_null="1" /></span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/products.products.qty/}</label>
						<div class="box_explain">{/sales.explain.seckill_0/}</div>
						<div class="input"><input name="Qty" type="text" value="<?=$seckill_row['Qty'];?>" class="box_input" notnull size="5" maxlength="5"></div>
					</div>
					<?php /*
					<div class="rows clean">
						<label>{/sales.package.remainder_qty/}</label>
						<div class="input"><input name="RemainderQty" type="text" value="<?=$seckill_row['RemainderQty'];?>" class="box_input" notnull size="5" maxlength="5"></div>
					</div>*/ ?>
					<div class="rows clean">
						<label>{/sales.package.max_qty/}</label>
						<div class="box_explain">{/sales.explain.seckill_1/}</div>
						<div class="input"><input name="MaxQty" type="text" value="<?=$seckill_row['MaxQty'];?>" class="box_input" notnull size="5" maxlength="5"></div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<input type="button" class="btn_global btn_cancel" value="{/global.return/}">
						</div>
					</div>
				</div>
				<input type="hidden" id="SId" name="SId" value="<?=$SId;?>" />
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" name="do_action" value="sales.seckill_edit" />
			</form>
		</div>
	</div>
	<?php 
		unset($seckill_row, $pro_row);
	}
	?>
</div>