<?php !isset($c) && exit();?>
<?php
manage::check_permit('mta', 1);
echo ly200::load_static('/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');

$type_ary=array('enter', 'addtocart', 'placeorder', 'complete');
$menu_ary=array('ratio', 'enter_directly', 'share_platform', 'search_engine', 'other');
?>
<script type="text/javascript">$(document).ready(function(){mta_obj.visits_conversion_init()});</script>
<div id="mta" class="r_con_wrap">
	<div class="inside_container"><h1>{/module.mta.visits_conversion/}</h1></div>
	<div class="inside_table">
		<div class="mta_menu clean">
			<dl class="box_time box_drop_down_menu fl">
				<dt class="more"><span>{/mta.time_ary.2/}</span><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					//foreach(array(0,-1,-7,-30,-89,6=>-99) as $k=>$v){
                    foreach(array(0,-1,-7,-30,6=>-99) as $k=>$v){
                        if($k<2) continue;
					?>
						<a href="javascript:;" class="item color_000<?=$k==2?' current':'';?>" data-value="<?=$v;?>" data-time="<?=date('Y-m-d', $c['time']+86400*$v).'/'.date('Y-m-d', $c['time']);?>">{/mta.time_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<ul class="legend fl">
				<li class="time_1 fl"></li>
				<li class="time_2 fl"></li>
			</ul>
			<dl class="box_terminal box_drop_down_menu fl">
				<dt class="more"><i class="icon_terminal_all"></i><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array('all', 'pc', 'mobile') as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$k;?>"><em class="icon_terminal_<?=$v;?>"></em>{/mta.terminal_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<input type="hidden" name="TimeS" value="" />
			<input type="hidden" name="TimeE" value="" />
		</div>
		<div class="global_container">
			<div class="big_title">{/mta.global.overview/}</div>
            <div class="box_charts" id="conversion_charts"></div>
		</div>
		<div class="box_detail global_container" id="conversion_detail">
			<div class="big_title">{/mta.conversion.channel_details/}</div>
			<table border="0" cellpadding="5" cellspacing="0" class="r_con_table data_table">
				<thead>
					<tr>
						<td width="16.6%" nowrap="nowrap">{/mta.conversion.total_ratio/}</td>
						<td width="16.6%" nowrap="nowrap">{/mta.conversion.access_source/}</td>
						<td width="19.2%" nowrap="nowrap">{/mta.conversion.enter/}</td>
						<td width="18.8%" nowrap="nowrap">{/mta.conversion.addtocart/}</td>
						<td width="18.8%" nowrap="nowrap">{/mta.conversion.placeorder/}</td>
						<td width="10%" nowrap="nowrap">{/mta.conversion.complete/}</td>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($menu_ary as $k=>$v){
					?>
						<tr>
							<td>
								<div><span class="complete_ratio"><span class="<?=$v;?>_operate_total">0</span>%</span></div>
								<div class="compare"><span class="compare_<?=$v;?>_operate_total">0</span>%</div>
							</td>
							<td>{/mta.conversion.<?=$v;?>/}</td>
							<td>
								<div class="contents_left">
									<div class="operate_data">{/mta.conversion.conversation/} (<span class="<?=$v;?>_uv">0</span>)</div>
									<div class="operate_data compare">{/mta.compared/} (<span class="compare_<?=$v;?>_uv">0</span>)</div>
								</div>
								<div class="contents_right">
									<div class="operate_ratio"><span class="<?=$v;?>_operate_addtocart">0</span>%</div><br />
									<div class="operate_ratio compare"><span class="compare_<?=$v;?>_operate_addtocart">0</span>%</div>
								</div>
							</td>
							<td>
								<div class="contents_left">
									<div class="operate_data">{/mta.conversion.conversation/} (<span class="<?=$v;?>_addtocart">0</span>)</div>
									<div class="operate_data compare">{/mta.compared/} (<span class="compare_<?=$v;?>_addtocart">0</span>)</div>
								</div>
								<div class="contents_right">
									<div class="operate_ratio"><span class="<?=$v;?>_operate_placeorder">0</span>%</div><br />
									<div class="operate_ratio compare"><span class="compare_<?=$v;?>_operate_placeorder">0</span>%</div>
								</div>
							</td>
							<td>
								<div class="contents_left">
									<div class="operate_data">{/mta.conversion.conversation/} (<span class="<?=$v;?>_placeorder">0</span>)</div>
									<div class="operate_data compare">{/mta.compared/} (<span class="compare_<?=$v;?>_placeorder">0</span>)</div>
								</div>
								<div class="contents_right">
									<div class="operate_ratio"><span class="<?=$v;?>_operate_complete">0</span>%</div><br />
									<div class="operate_ratio compare"><span class="compare_<?=$v;?>_operate_complete">0</span>%</div>
								</div>
							</td>
							<td>
								<div class="contents_full">
									<div class="operate_data">{/mta.conversion.conversation/} (<span class="<?=$v;?>_complete">0</span>)</div>
									<div class="operate_data compare">{/mta.compared/} (<span class="compare_<?=$v;?>_complete">0</span>)</div>
								</div>
							</td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="pop_form pop_compared">
		<form id="compared_form">
			<div class="t"><h1><span class="h1_compared">{/mta.compared/}</span><span class="h1_custom">{/mta.time_ary.6/}</span></h1><h2 class="hide">×</h2></div>
			<div class="compared_bodyer">
				<span class="unit_input"><b>{/global.time/} 1</b><input type="text" class="box_input input_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/} 2</b><input type="text" class="box_input input_time" name="TimeE" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/}</b><input type="text" class="box_input input_time custon_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
			</div>
			<div class="button">
				<input type="button" value="{/global.view/}" class="btn_global btn_submit" />
				<input type="button" value="{/global.cancel/}" class="btn_global btn_cancel" />
			</div>
		</form>
	</div>
</div>