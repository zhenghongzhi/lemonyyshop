<?php !isset($c) && exit();?>
<?php
manage::check_permit('mta', 1);
echo ly200::load_static('/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
?>
<script type="text/javascript">$(document).ready(function(){mta_obj.orders_init()});</script>
<div id="mta" class="r_con_wrap">
	<div class="inside_container"><h1>{/module.mta.orders/}</h1></div>
	<div class="inside_table">
		<div class="mta_menu clean">
			<dl class="box_time box_drop_down_menu fl">
				<dt class="more"><span>{/mta.time_ary.0/}</span><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array(0,-1,-7,-30,-89,6=>-99) as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$v;?>" data-time="<?=date('Y-m-d', $c['time']+86400*$v).'/'.date('Y-m-d', $c['time']);?>">{/mta.time_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<ul class="legend fl">
				<li class="time_1 fl"></li>
				<li class="time_2 fl"></li>
			</ul>
			<dl class="box_terminal box_drop_down_menu fl">
				<dt class="more"><i class="icon_terminal_all"></i><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array('all', 'pc', 'mobile') as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$k;?>"><em class="icon_terminal_<?=$v;?>"></em>{/mta.terminal_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<input type="hidden" name="TimeS" value="" />
			<input type="hidden" name="TimeE" value="" />
		</div>
		<div class="box_explain">{/mta.explain.orders/}</div>
		<ul class="box_data_list clean">
			<li>
				<div class="item">
					<h1>{/mta.order.order_price/}</h1>
					<h2><span class="order_price">0</span></h2>
                	<h3 class="compare compare_order_price">0</h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.order.customer_price/}</h1>
					<h2><span class="customer_price">0</span></h2>
                	<h3 class="compare compare_customer_price">0</h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.order.payment_rate/}</h1>
					<h2><span class="payment_rate">0</span></h2>
                	<h3 class="compare compare_payment_rate">0</h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.order.order_count/}</h1>
					<h2><span class="order_count">0</span></h2>
                	<h3 class="compare compare_order_count">0</h3>
				</div>
			</li>
		</ul>
		<div class="global_container">
			<div class="big_title">{/mta.global.overview/}</div>
			<div class="box_charts" id="line_charts"></div>
		</div>
		<div class="global_container">
			<div class="box_menu">
				<a href="javascript:;" class="cur">{/mta.order.payment/}</a>
				<a href="javascript:;">{/mta.global.country_distribution/}</a>
				<a href="javascript:;">{/mta.global.equipment_ratio/}</a>
				<a href="javascript:;">{/sales.coupon.coupon/}</a>
			</div>
			<div class="clear"></div>
			<div class="box_list">
				<div class="box_item box_payment" style="display: block;">
					<!-- <div class="big_title">{/mta.order.payment/}</div> -->
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table data_table">
						<thead>
							<tr>
								<td width="20%" nowrap="nowrap">{/mta.order.payment/}</td>
								<td width="20%" nowrap="nowrap">{/mta.order.proportion/}</td>
								<td width="20%" nowrap="nowrap">{/mta.order.amount/}</td>
								<td width="20%" nowrap="nowrap">{/mta.order.order_count/}</td>
								<td width="20%" nowrap="nowrap">{/mta.order.payment_rate/}</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="box_item">
					<!-- <div class="big_title">{/mta.global.country_distribution/}</div> -->
					<div class="box_charts" id="country_charts"></div>
				</div>
				<div class="box_orders_browser box_item">
					<div class="item">
						<div>
							<div class="clear"></div>
							<div class="big_title">{/mta.global.equipment_ratio/}</div>
							<div class="clear"></div>
							<div class="box_charts" id="browser_charts"></div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="item">
						<div class="big_title">{/mta.order.pc_order_count/}</div>
						<div class="box_browser_pc"></div>
					</div>
					<div class="item">
						<div class="big_title">{/mta.order.mobile_order_count/}</div>
						<div class="box_browser_mobile"></div>
					</div>
				</div>
				<div class="box_item box_coupon">
					<!-- <div class="big_title">{/sales.coupon.coupon/}</div> -->
					<div class="coupon_title">
						{/mta.order.order_share/}
						<div class="coupon_ratio compare"><span class="compare_coupon_rate">0</span>%</div>
						<div class="coupon_ratio"><span class="coupon_rate">0</span>%</div>
					</div>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table data_table">
						<thead>
							<tr>
								<td width="40%" nowrap="nowrap">{/mta.order.coupon_number/}</td>
								<td width="30%" nowrap="nowrap">{/mta.order.order_count/}</td>
								<td width="30%" nowrap="nowrap">{/mta.order.coupon_amount/}</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="clean"></div>
		</div>
	</div>
	<div class="pop_form pop_compared">
		<form id="compared_form">
			<div class="t"><h1><span class="h1_compared">{/mta.compared/}</span><span class="h1_custom">{/mta.time_ary.6/}</span></h1><h2 class="hide">×</h2></div>
			<div class="compared_bodyer">
				<span class="unit_input"><b>{/global.time/} 1</b><input type="text" class="box_input input_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/} 2</b><input type="text" class="box_input input_time" name="TimeE" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/}</b><input type="text" class="box_input input_time custon_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
			</div>
			<div class="button">
				<input type="button" value="{/global.view/}" class="btn_global btn_submit" />
				<input type="button" value="{/global.cancel/}" class="btn_global btn_cancel" />
			</div>
		</form>
	</div>
</div>