<?php !isset($c) && exit();?>
<?php
manage::check_permit('mta', 1);
echo ly200::load_static('/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
?>
<script language="javascript">$(document).ready(mta_obj.orders_repurchase_init);</script>
<div id="mta" class="r_con_wrap">
	<div class="inside_container"><h1>{/module.mta.orders_repurchase/}</h1></div>
    <div class="inside_table">
    	<div class="mta_menu clean">
			<dl class="box_cycle box_drop_down_menu fl">
				<dt class="more"><span>{/mta.mta_cycle_ary.1/}</span><em></em></dt>
				<dd class="more_menu drop_down">
                	<?php 
						foreach((array)$c['manage']['lang_pack']['mta']['mta_cycle_ary'] as $k=>$v){
							if(!$k) continue;
					?>
							<a href="javascript:;" class="item color_000<?=$k==1?' current':'';?>" data-value="<?=$k;?>"><?=$v;?></a>
					<?php }?>
				</dd>
			</dl>
			<ul class="legend fl">
				<li class="time_1 fl"></li>
				<li class="time_2 fl"></li>
			</ul>
			<dl class="box_terminal box_drop_down_menu fl">
				<dt class="more"><i class="icon_terminal_all"></i><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array('all', 'pc', 'mobile') as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$k;?>"><em class="icon_terminal_<?=$v;?>"></em>{/mta.terminal_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<input type="hidden" name="TimeS" value="" />
			<input type="hidden" name="TimeE" value="" />
		</div>
        <div class="global_container">
			<div class="big_title">{/mta.global.overview/}</div>
			<div class="box_charts" id="line_charts"></div>
		</div>
    </div>
</div>