<?php !isset($c) && exit();?>
<?php 
manage::check_permit('mta', 1, array('a'=>'search_logs'));//检查权限
$page_count=20;//显示数量
$where=1;
$_GET['Keyword'] && $where.=" and Keyword like '%{$_GET['Keyword']}%'";
$logs_row=str::str_code(db::get_limit_page('search_logs', $where, '*', 'Number desc, LId desc', (int)$_GET['page'], $page_count));
?>
<div id="search_logs" class="r_con_wrap">
    <div class="inside_container">
        <h1>{/module.mta.search_logs/}</h1>
    </div>
    <div class="inside_table">
    <div class="list_menu">
        <ul class="list_menu_button">
            <?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del/}</a></li><?php }?>
        </ul>
        <div class="search_form">
                <form method="get" action="?">
                    <div class="k_input">
                        <input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
                        <input type="button" value="" class="more" />
                    </div>
                    <input type="submit" class="search_btn" value="{/global.search/}" />
                    <input type="hidden" name="m" value="mta" />
                    <input type="hidden" name="a" value="search_logs" />
                </form>
        </div>
    </div>
    <?php
	if($logs_row[0]){
	?>
		<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
			<thead>
				<tr>
					<td width="40%" nowrap="nowrap">{/global.keyword/}</td>
					<td width="10%" nowrap="nowrap">{/search_logs.number/}</td>
					<td width="10%" nowrap="nowrap">{/search_logs.result/}</td>
					<td width="20%" nowrap="nowrap">{/set.country.country/}</td>
					<td></td>
					<td width="20%" class="last" nowrap="nowrap">{/search_logs.time/}</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($logs_row[0] as $v){?>
					<tr>
						<td><?=$v['Keyword']?></td>
						<td><?=$v['Number']?></td>
						<td><?=$v['Result']?></td>
						<td>
							<div class="clean">	
								<?php 
								$country_ary=@explode('|', $v['Country']);
								foreach((array)$country_ary as $v1){
									if(!$v1) continue;
									echo '<span class="other_box">'.$v1.'</span>';
								}
								?>
							</div>
						</td>
						<td></td>
						<td><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
					</tr>
				<?php }?>
			</tbody>
		</table>
    <?php
	}else{
		echo html::no_table_data();
	}?>
    </div>
    <div class="turn_page"><?=html::turn_page($logs_row[1], $logs_row[2], $logs_row[3], '?'.ly200::query_string('page').'&page=');?></div>
</div>