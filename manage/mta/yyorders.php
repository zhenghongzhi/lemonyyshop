<?php !isset($c) && exit();?>
<!--<link rel="stylesheet" href="echarts/css/bootstrap.min.css">-->

<?php
$start = $_GET['start']?$_GET['start']:date("Y-m-d",strtotime("- 1 month +1 day"));
$end = $_GET['end']?$_GET['end']:date("Y-m-d");

$url = "http://119.23.214.213/yy_app/yyshopapi/order_statistics.php?start=".$start."&end=".$end;

$resq = ly200::curl($url);
$resq1 = json_decode($resq);
//echo '<pre>';
foreach ($resq1->data as $key =>$val){
    $data[]=$key;
    $money[]=$val;
}
foreach ($data as $key =>$value) {
    if ($key==0){
        $data1 .= '"'.$value.'"';
    }else{
        $data1 .= ',"'.$value.'"';
    }
}
foreach ($money as $key =>$value) {
    if ($key==0){
        $money1 .= '"'.$value.'"';
    }else{
        $money1 .= ',"'.$value.'"';
    }
}

//echo $data1;
//print_r($resq1);
?>

<script type="text/javascript" src="echarts/js/echarts-all.js" ></script>

<!--<script type="text/javascript" src="echarts/js/min.js" ></script>-->
<script type="text/javascript" src="echarts/js/tagcloud.js" ></script>
<link rel="stylesheet" type="text/css" href="/manage/echarts/css/style.css"/>

<div id="min10"></div>

<div class="r_con_wrap">
    <div class="inside_container"><h1>采购{/module.mta.orders/}</h1></div>
    <div class="inside_table">
        <div class="mta_menu clean">

                <span class="unit_input"><b>开始时间</b><input type="date" class="box_input custon_time" name="start" value="<?=$start; ?>" size="15" maxlength="10"  style="width: 200px;" id="start"></span>
                ~<span class="unit_input"><b>结束时间</b><input type="date" class="box_input custon_time" name="end" value="<?=$end; ?>" size="15" maxlength="10"  style="width: 200px;" id="end"></span>
                <input type="button" value="查看" class="btn_global btn_submit" style="margin-top: 7px;">

        </div>
        <ul class="box_data_list clean">
            <li>
                <div class="item">
                    <h1>{/mta.order.order_price/}</h1>
                    <h2><span class="order_price"><?=$resq1->total; ?></span></h2>

                </div>
            </li>
            <li>
                <div class="item">
                    <h1>{/mta.order.order_count/}</h1>
                    <h2><span class="order_count"><?=$resq1->count; ?></span></h2>
                </div>
            </li>
        </ul>
        <div class="global_container">
            <div class="big_title">{/mta.global.overview/}</div>
            <div class="container">
                <div class="col-sm-12 col-md-12">
                    <div class="row" id="tus">
                        <div class="col-sm-4 col-md-4">
                            <div class="mimd" id="min2" style="height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        function Column0() {
            var myChart = echarts.init(document.getElementById('min10'));
            // 指定图表的配置项和数据
            var option = {
                title : {
                    text: '类型分布',
                    subtext: '',
                    y: '10',
                    x:'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: '10',
                    y: '10',
                    data:['期刊论文','会议论文','学位论文']
                },
                series: [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius: ['0%','50%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '30',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data:[
                            {value:9922, name:'期刊论文'},
                            {value:1033, name:'会议论文'},
                            {value:8001, name:'学位论文'},
                        ]
                    }
                ]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
        }
        function Column2() {
            var myChart = echarts.init(document.getElementById('min2'));
            // 指定图表的配置项和数据
            var option = {
                title: {
                    text: '订单时间分布',
                    subtext: '',
                    y: '10',
                    x:'center'
                },
                grid:{
                    x:60,
                    y:50,
                    x2:40,
                    y2:50,
                },
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    show : true,
                    feature : {
                        saveAsImage : {show: true}
                    }
                },
                xAxis:  {
                    type: 'category',
                    boundaryGap: false,
                    axisLabel:{
                        interval:0,
                        rotate:-30,
                        showMaxLabel: true,
                    },
                    data: [<?=$data1; ?>]
                },
                yAxis: {
                    type: 'value',
                },
                series: [
                    {
                        type:'line',
                        data:[<?=$money1; ?>],
                    }
                ]
            };
            // 使用刚指定的配置项和数据显示图表。
            myChart.setOption(option);
            myChart.resize();
        }//折线图
        Column0();
        Column2();


    });
    $(function() {
        /*3D标签云*/
        tagcloud({
            selector: ".tagcloud",  //元素选择器
            fontsize: 18,       //基本字体大小, 单位px
            radius: 60,         //滚动半径, 单位px
            mspeed: "normal",   //滚动最大速度, 取值: slow, normal(默认), fast
            ispeed: "normal",   //滚动初速度, 取值: slow, normal(默认), fast
            direction: 135,     //初始滚动方向, 取值角度(顺时针360): 0对应top, 90对应left, 135对应right-bottom(默认)...
            keep: false          //鼠标移出组件后是否继续随鼠标滚动, 取值: false, true(默认) 对应 减速至初速度滚动, 随鼠标滚动
        });
    });
    $('.btn_global').click(function(){
        var start = $("#start").val();
        var end = $("#end").val();
        window.location="http://lemonyyshop.com/manage/?m=mta&a=yyorders&start="+start+"&end="+end;
    })

</script>