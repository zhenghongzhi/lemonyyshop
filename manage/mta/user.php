<?php !isset($c) && exit();?>
<?php
manage::check_permit('mta', 1);
echo ly200::load_static('/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');

$tips_ary=array(date('Y', $c['time']), date('m', $c['time']));
$c['manage']['config']['ManageLanguage']=='en' && $tips_ary=array(date('m', $c['time']), date('Y', $c['time']));
?>
<?=ly200::load_static('/static/js/plugin/radialIndicator/radialIndicator.min.js');?>
<script language="javascript">$(document).ready(mta_obj.user_init);</script>
<div id="mta" class="r_con_wrap">
	<div class="">
		<div class="inside_container"><h1>{/module.mta.user/}</h1></div>
        <div class="inside_table">
            <h3 class="title_sub"><?=@date('Y-m', $c['time']);?></h3>
            <ul class="box_data_list data_list clean">
                <li>
                    <div class="item">
                        <h1>{/mta.user.new_member/}</h1>
                        <h2><span class="new_member">0</span></h2>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <h1>{/mta.user.active_member/}</h1>
                        <h2><span class="active_member">0</span></h2>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <h1>{/mta.user.core_member/}</h1>
                        <h2><span class="core_member">0</span></h2>
                    </div>
                </li>
                <li>
                    <div class="item">
                        <h1>{/mta.user.total_member/}</h1>
                        <h2><span class="total_member">0</span></h2>
                    </div>
                </li>
            </ul>
            <div class="blank20"></div>
            <h3 class="title_sub"><?=@date('Y-m', strtotime("-11 month", strtotime(date('Y-m-01', $c['time'])))).' ~ '.date('Y-m', $c['time']);?></h3>
            <div class="global_container box_mart_none">
                <div class="box_charts" id='trend_charts'></div>
            </div>
            <div class="blank9"></div>
            <div class="global_container">
                <div class="box_menu">
                    <a href="javascript:;" class="cur">{/mta.user.country_distribution/}</a>
                    <a href="javascript:;">{/mta.user.gender_statistics/} & {/mta.user.level_statistics/}</a>
                    <a href="javascript:;">{/mta.user.age_statistics/} & {/mta.user.login_statistics/}</a>
                </div>
                <div class="box_list">
                    <div class="box_item" style="display: block;">
                        <div class="box_charts" id='country_charts'></div>
                    </div>
                    <ul class="box_item box_data_list clean">
                        <li>
                            <div class="item box_rate_list" id="gender">
                                <h1>{/mta.user.gender_statistics/}</h1>
                                <div class="rate"></div>
                                <div class="data_list"></div>
                            </div>
                        </li>
                        <li>
                            <div class="item box_rate_list" id="level">
                                <h1>{/mta.user.level_statistics/}</h1>
                                <div class="rate"></div>
                                <div class="data_list"></div>
                            </div>
                        </li>
                    </ul>
                    <ul class="box_item box_data_list clean">
                        <li>
                            <div class="item box_rate_list" id="age">
                                <h1>{/mta.user.age_statistics/}</h1>
                                <div class="rate"></div>
                                <div class="data_list"></div>
                            </div>
                        </li>
                        <li>
                            <div class="item box_rate_list" id="login">
                                <h1>{/mta.user.login_statistics/}</h1>
                                <div class="rate"></div>
                                <div class="data_list"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>