<?php !isset($c) && exit();?>
<?php
manage::check_permit('mta', 1);
echo ly200::load_static('/static/js/plugin/highcharts_new/highcharts.js', '/static/js/plugin/highcharts_new/highcharts-zh_CN.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
?>
<script type="text/javascript">$(document).ready(function(){mta_obj.visits_init()});</script>
<div id="mta" class="r_con_wrap">
	<div class="inside_container"><h1>{/module.mta.visits/}</h1></div>
	<div class="inside_table">
		<div class="mta_menu clean">
			<dl class="box_time box_drop_down_menu fl">
				<dt class="more"><span>{/mta.time_ary.0/}</span><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array(0,-1,-7,-30,-89,6=>-99) as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$v;?>" data-time="<?=date('Y-m-d', $c['time']+86400*$v).'/'.date('Y-m-d', $c['time']);?>">{/mta.time_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<ul class="legend fl">
				<li class="time_1 fl"></li>
				<li class="time_2 fl"></li>
			</ul>
			<dl class="box_terminal box_drop_down_menu fl">
				<dt class="more"><i class="icon_terminal_all"></i><em></em></dt>
				<dd class="more_menu drop_down">
					<?php
					foreach(array('all', 'pc', 'mobile') as $k=>$v){
					?>
						<a href="javascript:;" class="item color_000<?=$k==0?' current':'';?>" data-value="<?=$k;?>"><em class="icon_terminal_<?=$v;?>"></em>{/mta.terminal_ary.<?=$k;?>/}</a>
					<?php }?>
				</dd>
			</dl>
			<input type="hidden" name="TimeS" value="" />
			<input type="hidden" name="TimeE" value="" />
		</div>
		<ul class="box_data_list clean">
			<li>
				<div class="item">
					<h1>{/mta.pv/}(PV)<span class="tool_tips_ico" content="{/mta.explain.visits_0/}">&nbsp;</span></h1>
					<h2><span class="pv">0</span><div class="compare compare_pv_img fr"></div></h2>
					<h3><span class="compare compare_pv">0</span></h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.average_pv/}<span class="tool_tips_ico" content="{/mta.explain.visits_1/}">&nbsp;</span></h1>
					<h2><span class="average_pv">0</span><div class="compare compare_average_pv_img fr"></div></h2>
					<h3><span class="compare compare_average_pv">0</span></h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.conversion.conversation/}<span class="tool_tips_ico" content="{/mta.explain.visits_2/}">&nbsp;</span></h1>
					<h2><span class="session">0</span><div class="compare compare_session_img fr"></div></h2>
					<h3><span class="compare compare_session">0</span></h3>
				</div>
			</li>
			<li>
				<div class="item">
					<h1>{/mta.uv/}(UV)<span class="tool_tips_ico" content="{/mta.explain.visits_3/}">&nbsp;</span></h1>
					<h2><span class="uv">0</span><div class="compare compare_uv_img fr"></div></h2>
					<h3><span class="compare compare_uv">0</span></h3>
				</div>
			</li>
		</ul>
		<div class="global_container">
			<div class="big_title">{/mta.global.overview/}</div>
			<div class="box_charts" id="line_charts"></div>
		</div>
		<div class="global_container">
			<div class="box_menu">
				<a href="javascript:;" class="cur">{/mta.global.details/}</a>
				<a href="javascript:;">{/mta.global.country_distribution/}</a>
				<a href="javascript:;">{/mta.global.traffic_source/}</a>
				<a href="javascript:;">{/mta.global.source_url/}</a>
				<a href="javascript:;">{/mta.global.equipment_ratio/}</a>
			</div>
			<div class="clear"></div>
			<div class="box_list">
				<div class="box_item box_detail" id="visits_detail" style="display: block;">
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table data_table">
						<thead>
							<tr>
								<td width="20%" nowrap="nowrap">{/mta.time_period/}</td>
								<td width="20%" nowrap="nowrap">{/mta.pv/}</td>
								<td width="20%" nowrap="nowrap">{/mta.average_pv/}</td>
								<td width="20%" nowrap="nowrap">{/mta.conversion.conversation/}</td>
								<td width="20%" nowrap="nowrap">{/mta.uv/}</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="box_item">
					<div class="box_charts" id="country_charts"></div>
				</div>
				<div class="box_item mta_parent_top_box">
					<div class="box_detail mr5" id="visits_referrer_detail">
						<table border="0" cellpadding="5" cellspacing="0" class="r_con_table data_table">
							<thead>
								<tr>
									<td width="20%" nowrap="nowrap">{/mta.source/}</td>
									<td width="20%" nowrap="nowrap">{/mta.pv/}</td>
									<td width="20%" nowrap="nowrap">{/mta.average_pv/}</td>
									<td width="20%" nowrap="nowrap">{/mta.conversion.conversation/}</td>
									<td width="20%" nowrap="nowrap">{/mta.uv/}</td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="box_item mta_parent_top_box">
					<div class="box_url_info ml5">
						<table border="0" cellpadding="0" cellspacing="0" class="table_report_list">
							<thead>
								<tr>
									<td width="60%" nowrap></td>
									<td width="20%" nowrap></td>
									<td width="20%" nowrap></td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="no_data">{/error.no_data/}</div>
					</div>
				</div>
				<div class="box_item box_browser">
					<div class="item">
						<div class="big_title">{/mta.global.equipment_ratio/}</div>
						<div class="box_charts" id="browser_charts"></div>
					</div>
					<div class="item">
						<div class="big_title">{/mta.global.pc_browser/}</div>
						<div class="box_browser_pc"></div>
					</div>
					<div class="item">
						<div class="big_title">{/mta.global.mobile_browser/}</div>
						<div class="box_browser_mobile"></div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="pop_form pop_compared">
		<form id="compared_form">
			<div class="t"><h1><span class="h1_compared">{/mta.compared/}</span><span class="h1_custom">{/mta.time_ary.6/}</span></h1><h2 class="hide">×</h2></div>
			<div class="compared_bodyer">
				<span class="unit_input"><b>{/global.time/} 1</b><input type="text" class="box_input input_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/} 2</b><input type="text" class="box_input input_time" name="TimeE" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
				<span class="unit_input"><b>{/global.time/}</b><input type="text" class="box_input input_time custon_time" name="TimeS" value="<?=date('Y-m-d', $c['time']).'/'.date('Y-m-d', $c['time']);?>" size="15" maxlength="10" readonly /></span>
			</div>
			<div class="button">
				<input type="button" value="{/global.view/}" class="btn_global btn_submit" />
				<input type="button" value="{/global.cancel/}" class="btn_global btn_cancel" />
			</div>
		</form>
	</div>
</div>