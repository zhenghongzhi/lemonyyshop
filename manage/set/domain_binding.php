<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'domain_binding'));//检查权限
//判断新用户已经进入域名绑定页面
manage::config_operaction(array('domain_binding'=>1), 'guide_pages');
if (!db::get_row_count('config', 'GroupId="domain_binding" and Variable="domain_list"')) {
	db::insert('config', array(
		'GroupId'	=>	'domain_binding',
		'Variable'	=>	'domain_list',
		'Value'		=>	'',
	));
} else {
	$domain_ary = (array)str::json_data(db::get_value('config', 'GroupId="domain_binding" and Variable="domain_list"', 'Value'), 'decode');
}
?>
<script type="text/javascript">$(document).ready(function(){set_obj.domain_binding_init();});</script>
<div id="domain_binding" class="r_con_wrap">
	<div class="center_container_1000">
		<div class="inside_table global_form">
			<?php 
			$data=array(
				'Action'	=>	'ueeshop_web_get_domain_list',
			);
			$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
			$website_ary=$result['msg'];
			if ($result['ret']==1 && count($website_ary) && $c['manage']['do'] != 'edit') {
				if($c['FunVersion']>=10){
					$count = 0;
					foreach ((array)$website_ary as $k => $v) {
						if($v[1]) {
							$count++;
						}
					}
				}
				?>
				<div class="big_title list_menu">
					<span>{/module.set.domain_binding/}</span>
					<?php if (($c['FunVersion']>=10 && !$count) || $c['FunVersion'] < 10) { ?>
						<a href="./?m=set&a=domain_binding&d=edit&status=1" class="fr add set_add">{/set.domain_binding.new/}</a>
					<?php } ?>
				</div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
		            <thead>
		                <tr>
		                    <td width="33.34%" nowrap="nowrap">{/set.domain_binding.website/}</td>
		                    <td width="33.34%" nowrap="nowrap">{/set.domain_binding.status/}</td>
		                    <td width="33.34%" nowrap="nowrap">{/set.domain_binding.operation/}</td>
		                </tr>
		            </thead>
		            <tbody>
		            	<?php 
							foreach ((array)$website_ary as $k => $v) {
								$verify = 0;
								if (!$v[1]) $_SESSION['Manage']['default_website'] = $k;
							?>
							<tr>
								<td nowrap="nowrap"><a href="//<?=$k;?>" target="_blank" class="color_555"><?=$k;?></a></td>
								<td nowrap="nowrap">
									<?php if($v[1] && (!@in_array($k, (array)$domain_ary) || !@in_array('www.'.$k, (array)$domain_ary) || !@in_array('_acme-challenge.'.$k, (array)$domain_ary))) { 
										$verify = 1;
										?>
										{/set.domain_binding.no_verify/}
									<?php } else { ?>
										{/set.domain_binding.connected/}
									<?php } ?>
								</td>
								<td class="nowrap">
									<?php if($v[1]) { ?>
										<?php if ($verify) { ?>
											<a href="./?m=set&a=domain_binding&d=edit&status=2&domain=<?=$k;?>" class="btn_global btn_add_item">{/set.domain_binding.to_verify/}</a>
											<a href="./?m=set&a=domain_binding&d=edit&status=2&domain=<?=$k;?>" class="btn_global btn_add_item">{/set.domain_binding.how_verify/}</a>
										<?php } ?>
										<a href="javascript:;" class="btn_global btn_add_item del_domain_binding" data-domain="<?=$k; ?>">{/set.domain_binding.relieve/}</a>
									<?php } else { ?>
										-----					
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
		            </tbody>
		        </table>
			<?php
			} else { 
				$status = (int)$_GET['status'];
				$domain = trim(stripslashes($_GET['domain']));
				if ($status > 1 && !$domain) {
					header('Location:./?m=set&a=domain_binding');
				} 
				?>
				<div class="domain_form">
					<?php if($status==1 || !$status){ ?>
						<form id="domain_binding_edit_form">
							<div class="rows clean center">
								<label class="title">{/set.domain_binding.write/}</label>
								<div class="input">
									<input type="text" class="box_input" autocomplete="off" name="DomainBinding" placeholder="{/set.domain_binding.domain_tips/}" value="" notnull="">
								</div>
							</div>
							<div class="rows clean center">
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.submit/}">
									<a href="./?m=set&a=domain_binding"><input type="button" class="btn_global" value="{/global.return/}"></a>
								</div>
							</div>
							<input type="hidden" name="Type" value="add">
							<input type="hidden" name="do_action" value="set.domain_binding_edit">
						</form>
					<?php }elseif($status==2){ ?>
						<div class="rows clean">
							<label class="title">{/set.domain_binding.resolve_domain/}</label>
						</div>
						<div class="rows clean bind_rows">
							<label><?=str_replace(array('%domain%', '%ip%'), array($domain, (gethostbyname(ly200::get_domain(0)) ? gethostbyname(ly200::get_domain(0)) : $_SERVER['SERVER_ADDR'])), $c['manage']['lang_pack']['set']['domain_binding']['domain_to_ip']) ?></label>
							<div class="input">
								<span class="color_aaa">{/set.domain_binding.tips/}</span>
							</div>
						</div>
						<table border="0" cellpadding="5" cellspacing="0" class="r_con_table verification_table">
				            <thead>
				                <tr>
				                    <td width="20%" nowrap="nowrap">{/set.domain_binding.record_type/}</td>
				                    <td width="20%" nowrap="nowrap">{/set.domain_binding.host_record/}</td>
				                    <td width="40%" nowrap="nowrap">{/set.domain_binding.record_value/}</td>
				                    <td width="20%" nowrap="nowrap">{/set.domain_binding.status/}</td>
				                </tr>
				            </thead>
				            <tbody>
								<tr data-status="<?=@in_array($domain, (array)$domain_ary) ? 1 : 0 ; ?>" data-type="A" data-domain="<?=$domain; ?>">
									<td nowrap="nowrap">A</td>
									<td nowrap="nowrap">@</td>
									<td nowrap="nowrap">
										<?=gethostbyname(ly200::get_domain(0)) ? gethostbyname(ly200::get_domain(0)) : $_SERVER['SERVER_ADDR']; ?>
									</td>
									<td nowrap="nowrap" class="status">
										<?php if (@in_array($domain, (array)$domain_ary)) { ?>
											{/set.domain_binding.connected/}
										<?php } else { ?>
											{/set.domain_binding.to_verify/}
										<?php } ?>
									</td>
								</tr>
								<tr data-status="<?=@in_array('www.'.$domain, (array)$domain_ary) ? 1 : 0 ; ?>" data-type="A" data-domain="<?='www.'.$domain; ?>">
									<td nowrap="nowrap">A</td>
									<td nowrap="nowrap">www</td>
									<td nowrap="nowrap">
										<?=gethostbyname(ly200::get_domain(0)) ? gethostbyname(ly200::get_domain(0)) : $_SERVER['SERVER_ADDR']; ?>
									</td>
									<td nowrap="nowrap" class="status">
										<?php if (@in_array('www.'.$domain, (array)$domain_ary)) { ?>
											{/set.domain_binding.connected/}
										<?php } else { ?>
											{/set.domain_binding.to_verify/}
										<?php } ?>
									</td>
								</tr>
								<tr data-status="<?=@in_array('_acme-challenge.'.$domain, (array)$domain_ary) ? 1 : 0 ; ?>" data-type="CNAME" data-domain="<?='_acme-challenge.'.$domain; ?>">
									<td nowrap="nowrap">CNAME</td>
									<td nowrap="nowrap">_acme-challenge</td>
									<td nowrap="nowrap">
										_acme-challenge.<?=$_SESSION['Manage']['default_website']; ?>
									</td>
									<td nowrap="nowrap" class="status">
										<?php if (@in_array('_acme-challenge.'.$domain, (array)$domain_ary)) { ?>
											{/set.domain_binding.connected/}
										<?php } else { ?>
											{/set.domain_binding.to_verify/}
										<?php } ?>
									</td>
								</tr>
				            </tbody>
				        </table>
				        <div class="rows clean center">
				        	<label></label>
							<div class="input input_button">
								<a class="domain_binding_verification_btn" href="javascript:;"><input type="button" class="btn_global btn_submit" value="{/set.domain_binding.completed/}"></a>
								<a href="./?m=set&a=domain_binding"><input type="button" class="btn_global" value="{/global.return/}"></a>
							</div>
						</div>
					<?php }elseif($status==3){ ?>
						<div class="rows clean">
							<label class="title">{/set.domain_binding.in_verify/}</label>
							<div class="input tips">{/set.domain_binding.verify_tips/}</div>
						</div>
						<div class="rows clean center">
							<label></label>
							<div class="input input_button">
								<a href="./?m=set&a=domain_binding"><input type="button" class="btn_global" value="{/global.return/}"></a>
							</div>
						</div>
						<input type="hidden" name="domain_binding_verification" value="<?=$domain;?>">
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>