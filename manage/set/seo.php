<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'seo'));//检查权限
echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js', '/static/js/plugin/jquery-ui/jquery-ui.min.css', '/static/js/plugin/jquery-ui/jquery-ui.min.js', '/static/js/plugin/ckeditor/ckeditor.js');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.config_sitemap_edit();});</script>
<div id="seo" class="r_con_wrap">
	<div class="center_container">
		<?php if($c['manage']['do']=='index'){ ?>
			<form id="set_edit_form" class="global_form">
				<?php /******************************** SEO Start ********************************/?>
				<div class="global_container">
					<?php $seo_row=str::str_code(db::get_one('meta', 'Type="home"')); ?>
					<div class="big_title">
						<a href="./?m=set&a=seo&d=sitemap" class="update_sitemap">{/set.seo.ud_map/}</a>
						<a href="https://www.google.com/search?q=site:<?=ly200::get_domain(0);?>" target="_blank" class="view_seo">{/set.seo.view/}</a>
						<span>{/module.set.seo/}</span>
					</div>
					<div class="sitemap_box">
						<div class="rows clean">
							<label>{/set.seo.website/}</label>
							<div class="input">
								<a href="<?=ly200::get_domain();?>" target="_blank" class="color_555"><?=ly200::get_domain();?></a>
							</div>
						</div>
						<div class="rows clean">
							<label>{/set.config.sitemap/}</label>
							<div class="input">
								<a href="<?=ly200::get_domain().'/sitemap.xml';?>" target="_blank" class="color_555"><?=ly200::get_domain().'/sitemap.xml';?></a>
							</div>
						</div>
						<div class="rows clean date">
							<div class="input">
								<span>{/set.seo.last_ud/}: <?=@date('Y-m-d H:i:s', @filectime($c['root_path'].'sitemap.xml'))?></span>
							</div>
						</div>
					</div>
					<div class="seo_box">
						<div class="rows clean multi_lang" data-name="title">
							<label>{/news.news.seo_title/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_title_notes/}</div>
							<div class="input">
								<?=manage::unit_form_edit($seo_row, 'text', 'SeoTitle', 41, 255);?>
							</div>
						</div>
						<div class="rows clean multi_lang" data-name="description">
							<label>{/news.news.seo_brief/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_description_notes/}</div>
							<div class="input">
								<?=manage::unit_form_edit($seo_row, 'textarea', 'SeoDescription', 41);?>
							</div>
						</div>
						<div class="rows clean multi_lang" data-name="keyword">
							<label>{/news.news.seo_keyword/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_keyword_notes/}</div>
							<dl class="box_basic_more box_seo_basic_more">
								<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>
								<dd class="drop_down">
									<a href="javascript:;" class="item input_checkbox_box btn_open_attr" id="edit_keyword"><span>{/global.edit/}</span></a>
								</dd>
							</dl>
							<?php
							$keys_id_ary=explode(',', $seo_row['SeoKeyword'.$c['manage']['web_lang']]);
							$keys_id_ary=array_filter($keys_id_ary);
							$keys_select_count=0;
							?>
							<div class="rows keys_row clean">
								<div class="input">
									<div class="box_option_list">
										<div class="option_selected">
											<div class="select_list">
												<?php
												foreach((array)$keys_id_ary as $v){
													$Keys=trim($v);
													$keys_select_count+=1;
												?>
													<span class="btn_attr_choice current" data-type="keys"><b><?=$Keys;?></b><input type="checkbox" name="keysCurrent[]" value="<?=$Keys;?>" checked class="option_current" /><input type="hidden" name="keysOption[]" value="<?=$Keys;?>" /><input type="hidden" name="keysName[]" value="<?=$Keys;?>" /><i></i></span>
												<?php }?>
											</div>
											<input type="text" class="box_input" name="_Option" value="" size="30" maxlength="255" />
											<span class="placeholder<?=$keys_select_count>0?' hide':'';?>">{/products.global.placeholder/}</span>
										</div>
										<div class="option_not_yet">
											<div class="select_list" data-type="keys"></div>
										</div>
										<div class="option_button">
											<a href="javascript:;" class="select_all">{/global.select_all/}</a>
											<div class="option_button_menu" style="display:none;">
												<a href="javascript:;" data-type="keys">{/products.products.tag_info/}</a>
											</div>
										</div>
										<input type="hidden" class="option_max_number" value="0" />
									</div>
								</div>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input input_button">
								<input type="button" class="btn_global btn_submit" style="margin-right:15px;" value="{/global.save/}" />
								<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input input_button"></div>
						</div>
					</div>
				</div>
				<?php /******************************** SEO End ********************************/?>
				<input type="hidden" name="Type" value="home" />
				<input type="hidden" name="do_action" value="set.seo_edit" />
			</form>
		<?php
		}elseif($c['manage']['do']=='sitemap'){ ?>
			<form id="watermark_edit_form" class="global_form">
				<div class="global_container">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/set.seo.ud_smap/}</span>
					</a>
					<div class="themes_progress">
						<div class="box_progress">
							<div class="status">{/set.config.water.processing/}</div>
							<div class="progress">
								<div class="num" style="width:10%;"><span>10%</span></div>
							</div>
							<div class="tips">{/set.config.water.wait/}</div>
							<input type="button" class="btn_global" id="btn_progress_continue" value="{/products.sync.progress.continue/}" />
							<input type="button" class="btn_global" id="btn_progress_keep" value="{/set.config.water.proceed/}" />
							<input type="button" class="btn_global" id="btn_progress_cancel" value="{/global.cancel/}" />
							<input type="hidden" name="Start" value="<?=(int)$Start;?>" />
							<input type="hidden" name="WebSiteNum" value="<?=(int)$WebSiteNum;?>" />
							<input type="hidden" name="do_action" value="set.sitemap_create" />
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input input_button">
								<?php /*<input type="button" class="btn_global btn_submit" value="{/global.save/}" />*/?>
								<a href="./?m=set&a=seo&d=sitemap" title="{/set.config.water.proceed/}"><input type="button" class="btn_global btn_proceed" value="{/set.config.water.proceed/}" /></a>
								<a href="./?m=set&a=seo" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
							</div>
						</div>
					</div>
				</div>
			</form>
		<?php } ?>
	</div>
</div>
<div id="fixed_right">
	<div class="global_container fixed_edit_keyword">
		<div class="top_title"><strong>{/products.global.modify_keyword/}</strong><a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_keyword_form">
			<div class="edit_keyword_list"></div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<div class="bg_no_table_data bg_no_table_fixed">
				<div class="content">
					<p class="color_000">{/products.products.no_options_tips/}</p><a href="javascript:;" class="btn_global btn_add_item btn_cancel">{/global.close/}</a>
				</div>
			</div>
			<input type="hidden" name="Type" value="home" />
			<input type="hidden" name="do_action" value="set.seo_keyword_edit" />
		</form>
	</div>
</div>