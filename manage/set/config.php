<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'config'));//检查权限
echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js', '/static/js/plugin/jquery-ui/jquery-ui.min.css', '/static/js/plugin/jquery-ui/jquery-ui.min.js', '/static/js/plugin/ckeditor/ckeditor.js');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.config_edit_init();});</script>
<div id="config" class="r_con_wrap">
	<div class="<?=($c['manage']['do']!='email' || ($c['manage']['do']=='email' && !isset($_GET['v']))) ? 'center_container' : ''; //邮件设置不居中 ?>">
		<?php if($c['manage']['do']=='index'){ ?>
			<form id="edit_form" class="global_form">
				<?php /******************************** 基本设置 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=basis" class="set_edit">{/global.edit/}</a>
						<span>{/module.set.config/}</span>
					</div>
					<div class="rows rows_static first clean">
						<label>{/set.config.site_name/}</label>
						<div class="input"><?=$c['manage']['config']['SiteName'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/set.config.logo/}</label>
						<div class="input">
							<div class="img">
								<img src="<?=$c['manage']['config']['LogoPath'];?>" alt="">
								<span></span>
							</div>
						</div>
					</div>
					<div class="rows rows_static clean">
						<label>{/set.config.copyright/}</label>
						<div class="input">
							<?=htmlspecialchars(htmlspecialchars_decode($c['manage']['config']['CopyRight']["CopyRight{$c['manage']['web_lang']}"]), ENT_QUOTES);?>
						</div>
					</div>
					<div class="rows rows_static clean">
						<label>{/set.config.ico/}</label>
						<div class="input">
							<div class="img">
								<img src="<?=$c['manage']['config']['IcoPath'];?>" alt="">
								<span></span>
							</div>
						</div>
					</div>
					<div class="rows rows_static clean">
						<label>{/set.config.admin_email/}</label>
						<div class="input">
							<?=$c['manage']['config']['AdminEmail'];?>
						</div>
					</div>
					<?php 
					if($c['FunVersion']!=10){
						$AppKey=db::get_value('config', "GroupId='API' and Variable='AppKey'", 'Value');
						if(!substr_count($_SERVER['HTTP_HOST'], '.myueeshop.com')){ //试用的不显示出来 
					?>
						<div class="rows rows_static clean">
							<label>{/module.set.authorization.open/}</label>
							<div class="input">
								<div class="box_explain">{/set.explain.erp/}</div>
								{/set.authorization.access_code/}: <?=$c['Number'];?> <br />
								{/set.authorization.appkey/}: <span class="api_appkey"><?=$AppKey;?></span>&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="btn_global btn_add_item btn_reset_api">{/global.reset/}</a><br />
								API{/set.authorization.url/}: <?=((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')?'https://':'http://').$_SERVER['HTTP_HOST'].'/';?> <br />
							</div>
						</div>
					<?php
						}
					}
					?>
					<div class="rows rows_static clean">
						<label>{/set.config.sitemap/}</label>
						<div class="input"><a href="<?=ly200::get_domain().'/sitemap.xml';?>" target="_blank" class="color_555"><?=ly200::get_domain().'/sitemap.xml';?></a></div>
					</div>
				</div>
				<?php /******************************** 基本设置 End ********************************/?>
				<?php /******************************** SEO Start ********************************?>
				<div class="global_container">
					<?php $seo_row=str::str_code(db::get_one('meta', 'Type="home"')); ?>
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=seo" class="set_edit">{/global.edit/}</a>
						<span>{/products.products.seo_info/}</span>
					</div>
					<div class="rows rows_static first clean">
						<label>{/global.title/}</label>
						<div class="input"><?=$seo_row['SeoTitle'.$c['manage']['web_lang']];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/global.keyword/}</label>
						<div class="input"><?=$seo_row['SeoKeyword'.$c['manage']['web_lang']];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/global.depict/}</label>
						<div class="input"><?=$seo_row['SeoDescription'.$c['manage']['web_lang']];?></div>
					</div>
				</div>
				<?php ******************************** SEO End ********************************/?>
				<?php /******************************** 购物设置 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=shopping_set" class="set_edit">{/global.edit/}</a>
						<span>{/set.config.shopping_info/}</span>
					</div>
					<?php if($c['manage']['config']['LowConsumption']){ ?>
						<div class="rows rows_static first clean">
							<label>{/set.config.low_consumption/}</label>
							<div class="input"><?=$c['manage']['currency_symbol'].$c['manage']['config']['LowPrice']; ?></div>
						</div>
					<?php }?>
					<div class="rows rows_static clean">
						<label>{/set.config.less_stock/}</label>
						<div class="input">{/set.config.less_stock_ary.<?=(int)$c['manage']['config']['LessStock']; ?>/}</div>
					</div>
					<?php if($c['FunVersion'] && $c['FunVersion']!=10){?>
						<div class="rows rows_static clean">
							<label>{/set.config.orders_sms/}</label>
							<div class="input">
								<?=$c['manage']['config']['OrdersSms'];?><br />
								<?php
								for($i=0; $i<2; ++$i){
									if((int)$c['manage']['config']['OrdersSmsStatus'][$i]){
								?>
									{/set.config.orders_sms_ary.<?=$i;?>/}
									<?php }?>
								<?php }?>
							</div>
						</div>
					<?php }?>
					<?php if($c['FunVersion']){ ?>
						<div class="rows rows_static clean">
							<label>{/set.config.tourists_shopping/}</label>
							<div class="input tourists_checkbox">
								<div class="switchery<?=$c['manage']['config']['TouristsShopping']?' checked':'';?>">
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					<div class="rows rows_static clean">
						<label>{/set.config.arrival_info/}</label>
						<div class="input"><?=htmlspecialchars_decode($c['manage']['config']['ArrivalInfo']["ArrivalInfo{$c['manage']['web_lang']}"]); ?></div>
					</div>
				</div>
				<?php /******************************** 购物设置 End ********************************/?>
				<?php /******************************** 会员设置 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<span>{/set.config.user_info/}</span>
					</div>
					<div class="rows rows_static first clean">
						<label>{/set.config.user_level/}</label>
						<div class="input">
							<a href="./?m=set&a=config&d=user_level" class="set_edit">{/global.edit/}</a>
							<?php 
							$level_row=str::str_code(db::get_all('user_level', 1, '*', 'FullPrice desc'));
							foreach((array)$level_row as $v){
								?>
								<div>
									<div class="img s_img"><img src="<?=$v['PicPath'];?>" alt=""><span></span></div>
									<?=$v['Name'.$c['manage']['web_lang']];?>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="rows rows_static clean">
						<label>{/set.config.user_reg_set/}</label>
						<div class="input">
							<a href="./?m=set&a=config&d=user_reg_set" class="set_edit">{/global.edit/}</a>
							<?=$c['manage']['config']['UserVerification']?'{/set.config.user_verification/}':'';?> <br />
							<?php
							$RegSet=db::get_value('config', 'GroupId="user" and Variable="RegSet"', 'Value');
							$reg_ary=str::json_data($RegSet, 'decode');
							$row=str::str_code(db::get_all('user_reg_set', '1', '*', "{$c['my_order']} SetId asc"));
							foreach((array)$c['manage']['user_reg_field'] as $k=>$v){
								$status=$reg_ary[$k];
								if(!($status[0] && $v)) continue;
								?>
								{/user.reg_set.<?=$k;?>/} <br />
							<?php } ?>
							<?php foreach((array)$row as $v){ ?>
								<?=$v['Name'.$c['manage']['web_lang']];?> <br />
							<?php } ?>
						</div>
					</div>
					<?php if($c['FunVersion']>=1 && $c['FunVersion']!=10){?>
						<div class="rows rows_static clean">
							<label>{/set.config.user_verification/}</label>
							<div class="input verification_checkbox">
								<div class="switchery<?=$c['manage']['config']['UserVerification']?' checked':'';?>" data-config="UserVerification">
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
							</div>
						</div>
					<?php }?>
				</div>
				<?php /******************************** 会员设置 End ********************************/?>
				<?php /******************************** 水印设置 Start ********************************/?>
				<?php 
				$set_check=array();
				$set_row=db::get_all('config', 'GroupId="products_show"');
				foreach((array)$set_row as $k=>$v){
					if($v['Variable']=='Config' || $v['Variable']=='review' || $v['Variable']=='favorite' || $v['Variable']=='wholesale'){
						$set_check[$v['Variable']]=str::json_data($v['Value'], 'decode');
					}else{
						$set_check[$v['Variable']]=$v['Value'];
					}
				}
				?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=watermark_set" class="set_edit <?=$c['manage']['cdx_limit'];?>">{/global.edit/}</a>
						<span>{/set.config.watermark_config/}</span>
					</div>
					<div class="rows first rows_static clean">
						<label>{/set.config.water.used/}</label>
						<div class="input water_checkbox">
							<div class="switchery<?=$c['manage']['config']['IsWater']?' checked':'';?> <?=$c['manage']['cdx_limit'];?>" data-config="IsWater">
								<div class="switchery_toggler"></div>
								<div class="switchery_inner">
									<div class="switchery_state_on"></div>
									<div class="switchery_state_off"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="water_box" style="display:<?=$c['manage']['config']['IsWater'] ? 'block' : 'none';?>;">
						<?php if($c['manage']['config']['UpdateWaterStatus']){ ?>
							<div class="rows first rows_static clean">
								<label>{/set.config.water.replace/}</label>
								<div class="input">
										<a href="./?m=set&a=config&d=watermark_set&update=1" class="btn_global btn_add_item">{/set.config.water.update/}</a>
								</div>
							</div>
						<?php } ?>
						<?php if(is_file($c['root_path'].$c['manage']['config']['WatermarkPath'])){ ?>
							<div class="rows rows_static clean">
								<label>{/set.config.watermark/}</label>
								<div class="input"><div class="img"><img src="<?=$c['manage']['config']['WatermarkPath']; ?>" alt=""></div></div>
							</div>
						<?php } ?>
						<div class="rows rows_static clean">
							<label>{/set.config.alpha/}</label>
							<div class="input"><?=$c['manage']['config']['Alpha'];?>%</div>
						</div>
						<div class="rows rows_static clean">
							<label>{/set.config.water_position/}</label>
							<div class="input">{/set.config.water_position_ary.<?=$c['manage']['config']['WaterPosition'];?>/} <br/> <img src="/static/manage/images/set/watermark_position_<?=$c['manage']['config']['WaterPosition']; ?>.png" alt=""></div>
						</div>
					</div>
				</div>
				<?php /******************************** 水印设置 End ********************************/?>
				<?php /******************************** 社会媒体 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=share" class="set_edit">{/global.edit/}</a>
						<span>{/set.config.social/}</span>
					</div>
					<?php 
					$ShareMenuAry=$c['manage']['config']['ShareMenu'];
					foreach((array)$c['share'] as $k => $v){ 
						if(!$ShareMenuAry[$v]) continue;
						?>
						<div class="rows rows_static <?=$k==0 ? 'first' : ''; ?> clean">
							<label>
								<div class="share_div share_<?=strtolower($v); ?>"></div>
								&nbsp;&nbsp;<?=$v; ?>
							</label>
							<div class="input"><?=$ShareMenuAry[$v]; ?></div>
						</div>
					<?php } ?>
				</div>
				<?php /******************************** 社会媒体 End ********************************/?>
				<?php /******************************** 产品评论 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<span>{/products.set.review/}</span>
					</div>
					<div class="config_table_body config_table_body_set">
						<?php
						$products_set_ary=array('range'=>array('customer', $set_check['review']['range']), 'display'=>array('audit', $set_check['review']['display']));
						foreach($products_set_ary as $k=>$v){
						?>
							<div class="table_item">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table review_config_table">
									<tbody>
										<tr>
											<td width="80%" nowrap="nowrap"><span class="color_000">{/set.config.<?=$v[0];?>/}</span></td>
											<td width="30%" nowrap="nowrap"></td>
											<td width="50" nowrap="nowrap" align="center">
												<div class="review_checkbox">
													<div class="switchery<?=$v[1]==1?' checked':'';?>" data-type="<?=$k;?>">
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</div>
											</td>                   
										</tr>
									</tbody>
								</table>
							</div>
						<?php }?>
					</div>
					<?php
					/* #delete#
					<div class="rows rows_static clean">
						<label>{/products.set.sales/}</label>
						<div class="input"><?=$set_check['Config']['sales'] ? '{/global.turn_on/}' : '{/global.close/}'; ?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/products.set.weight/}</label>
						<div class="input"><?=$set_check['Config']['weight'] ? '{/global.turn_on/}' : '{/global.close/}'; ?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/products.set.sku/}</label>
						<div class="input"><?=$set_check['Config']['sku'] ? '{/global.turn_on/}' : '{/global.close/}'; ?></div>
					</div>
					*/
					?>
				</div>
				<?php /******************************** 产品评论 End ********************************/?>
				<?php /******************************** 邮件设置 Start ********************************/?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=email" class="set_edit">{/set.explain.set_smtp/}</a>
						<span>{/set.config.email_config/}</span>
					</div>
					<div class="config_table_body config_table_body_set">
						<?php
						$system_email_ary=array();
						$system_email_row=str::str_code(db::get_all('system_email_tpl', '1'));
						foreach($system_email_row as $v){
							$system_email_ary[$v['Template']]=$v['IsUsed'];
						}
						foreach($c['manage']['email_notice'] as $k=>$v){
							$IsUsed=(int)$system_email_ary[$v];
						?>
							<div class="table_item">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table email_config_table">
									<tbody>
										<tr>
											<td width="80%" nowrap="nowrap"><span class="color_000">{/email.notice.notice_ary.<?=$v;?>/}</span></td>
											<td width="30%" nowrap="nowrap"><a class="edit" href="./?m=set&a=config&d=email&v=<?=$v;?>">{/set.explain.edit_mail/}</a></td>
											<td width="50" nowrap="nowrap" align="center">
												<?php if($v!='validate_mail'){?>
													<div class="used_checkbox">
														<div class="switchery<?=$IsUsed?' checked':'';?>" data-type="<?=$v;?>">
															<div class="switchery_toggler"></div>
															<div class="switchery_inner">
																<div class="switchery_state_on"></div>
																<div class="switchery_state_off"></div>
															</div>
														</div>
													</div>
												<?php }?>
											</td>                   
										</tr>
									</tbody>
								</table>
							</div>
						<?php }?>
					</div>
				</div>
				<?php /******************************** 邮件设置 End ********************************/?>
				<?php /******************************** 语言设置 Start ********************************/?>
				<div class="global_container">
					<script type="text/javascript">$(document).ready(function(){set_obj.config_language_edit();});</script>
					<div class="big_title rows_hd_part">
						<span>{/set.config.language_config/}</span>
					</div>
					<div class="config_table_body config_table_body_set">
						<?php
						$LanguageFlag=str::json_data(htmlspecialchars_decode($c['manage']['config']['LanguageFlag']), 'decode');
						foreach($c['manage']['web_lang_list'] as $v){
						?>
							<div class="table_item">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table email_config_table">
									<tbody>
										<tr>
											<td width="80%" nowrap="nowrap">
												<span class="color_000">{/language.<?=$v;?>/}</span>
												<?=($LanguageFlag[$v] && is_file($c['root_path'].$LanguageFlag[$v]))?'<span class="img img_small"><img src="'.$LanguageFlag[$v].'" class="small_flag" align="absmiddle" /><span></span></span>':'';?>
												<?php if($v==$c['manage']['config']['LanguageDefault']){ ?><span class="desc">{/set.config.default_language/}</span><?php } ?>
											</td>
											<td width="30%" nowrap="nowrap">					
												<a href="./?m=set&a=config&d=language&lang=<?=$v; ?>" class="edit <?=$c['manage']['cdx_limit'];?>">{/global.edit/}</a>
											</td>
											<td width="50" nowrap="nowrap" align="center" class="payment_used">
												<div class="switchery language_switchery <?=in_array($v, $c['manage']['config']['Language'])?'checked':'';?> <?=$c['manage']['cdx_limit'];?>" data-lang="<?=$v; ?>">
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php }?>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<?php if($c['FunVersion']!=100){?>
								<div class="switchery config_switchery<?=$c['manage']['config']['BrowserLanguage']?' checked':'';?> <?=$c['manage']['cdx_limit'];?>" data-config="BrowserLanguage">
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
								{/set.config.browser_language/} &nbsp;&nbsp;&nbsp;
							<?php }?>
							<?php if($c['FunVersion']>0){?>					
								<div class="switchery manage_language_switchery<?=$c['manage']['config']['ManageLanguage']=='en'?' checked':'';?>" data-lang="en">
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
								{/set.config.englist_manage/}
							<?php }?>
						</div>
					</div>
				</div>
				<?php /******************************** 语言设置 End ********************************/?>
				<?php /******************************** 公司信息 Start ********************************/?>
				<?php 
				$print_row=str::str_code(db::get_all('config', "GroupId='print'"));
				foreach((array)$print_row as $v){
					$c['manage']['print_config'][$v['Variable']]=$v['Value'];
				}
				?>
				<div class="global_container">
					<div class="big_title rows_hd_part">
						<a href="./?m=set&a=config&d=company" class="set_edit">{/global.edit/}</a>
						<span>{/set.config.company_info/}</span>
						&nbsp;&nbsp;<em class="box_explain">( {/set.explain.print/} )</em>
					</div>
					<div class="rows rows_static first clean">
						<label>{/set.config.logo/}</label>
						<div class="input">
							<div class="img">
								<img src="<?=$c['manage']['print_config']['LogoPath'];?>" alt="">
								<span></span>
							</div>
						</div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.Company/}</label>
						<div class="input"><?=$c['manage']['print_config']['Compeny'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.Address/}</label>
						<div class="input"><?=$c['manage']['print_config']['Address'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.city/}</label>
						<div class="input"><?=$c['manage']['print_config']['City'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.state/}</label>
						<div class="input"><?=$c['manage']['print_config']['State'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.zipcode/}</label>
						<div class="input"><?=$c['manage']['print_config']['ZipCode'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.Email/}</label>
						<div class="input"><?=$c['manage']['print_config']['Email'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.Telephone/}</label>
						<div class="input"><?=$c['manage']['print_config']['Telephone'];?></div>
					</div>
					<div class="rows rows_static clean">
						<label>{/user.reg_set.Fax/}</label>
						<div class="input"><?=$c['manage']['print_config']['Fax'];?></div>
					</div>
                    <div class="rows rows_static clean">
                        <label>协议</label>
                        <div class="input"><?=company;?></div>
                    </div>
				</div>
				<?php /******************************** 公司信息 End ********************************/?>
				<input type="hidden" name="scrolltop" value="0" />
				<?php if($_SESSION['config_scrolltop']){ //返回的时候滚动条回到原来位置 ?>
					<script>
						$(function(){
							$('#config').animate({scrollTop:<?=$_SESSION['config_scrolltop']; ?> }, 0);
						});
					</script>
				<?php unset($_SESSION['config_scrolltop']); } ?>
			</form>
		<?php
		}elseif($c['manage']['do']=='basis'){
			//基本设置
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_basis_edit();});</script>
			<form id="basis_edit_form" class="global_form">
				<div class="global_container">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/module.set.config/}</span>
					</a>
					<div class="rows clean">
						<label>{/set.config.site_name/}</label>
						<div class="input"><input type="text" class="box_input" name="SiteName" value="<?=$c['manage']['config']['SiteName'];?>" size="50" maxlength="255" notnull /> <font class="fc_red">*</font></div>
					</div>
					<div class="rows clean">
						<label>{/set.config.logo/}</label>
						<div class="input">
							<?=manage::multi_img('LogoDetail', 'LogoPath', $c['manage']['config']['LogoPath']); ?>
						</div>
					</div>
					<div class="rows clean">
						<label>
							{/set.config.copyright/}
							<div class="tab_box"><?=manage::html_tab_button();?></div>
						</label>
						<div class="input">
							<?=manage::form_edit($c['manage']['config']['CopyRight'], 'text', 'CopyRight', 70, 255, 'notnull');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/set.config.ico/}</label>
						<div class="input">
							<?=manage::multi_img('IcoDetail', 'IcoPath', $c['manage']['config']['IcoPath'], '16x16'); ?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/set.config.admin_email/}</label>
						<div class="input">
							<input type="text" class="box_input" name="AdminEmail" value="<?=$c['manage']['config']['AdminEmail'];?>" size="50" maxlength="255" />
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
							<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="set.config_basis_edit">
				</div>
			</form>
		<?php
		}elseif($c['manage']['do']=='user_level'){
			//会员等级
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_user_edit();});</script>
			<div class="global_container">
				<a href="./?m=set&a=config<?=$_GET['p'] ? '&d=user_level' : ''; ?>" class="return_title">
					<span class="return">{/set.config.user_info/}</span> 
					<span class="s_return">/ {/user.level.level/}</span>
					<?php if($_GET['p']){ ?>
						<span class="s_return">/ <?=$_GET['LId'] ? '{/global.edit/}' : '{/global.add/}'; ?></span>
					<?php } ?>
				</a>
				<?php if($_GET['p']){
					$LId=(int)$_GET['LId'];
					$LId && $level=str::str_code(db::get_one('user_level', "LId={$LId}"));
					?>
					<form id="user_level_form" class="global_form">
						<div class="rows clean">
							<label>
								{/user.level.name/}
								<div class="tab_box"><?=manage::html_tab_button();?></div>
							</label>
							<div class="input"><?=manage::form_edit($level, 'text', 'Name', 30, 50, 'notnull');?></div>
						</div>
						<div class="rows clean">
							<label>{/user.level.icon/}</label>
							<div class="input">
								<?=manage::multi_img('PicDetail', 'PicPath', $level['PicPath']); ?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/user.level.discount/}</label>
							<div class="input">
								<span class="unit_input"><input type="text" class="box_input" name="Discount" value="<?=$level['Discount'];?>" size="5" rel="amount" maxlength="255" notnull /><b class="last">%</b></span>
							</div>
							<div class="clear"></div>
						</div>
						<div class="rows clean">
							<label>{/user.level.condition/}</label>
							<div class="input">
								<span class="unit_input"><b>{/user.level.full_price/}: <?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" rel="amount" name="FullPrice" value="<?=$level['FullPrice'];?>" size="5" maxlength="255" notnull /></span>
							</div>
							<div class="clear"></div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input input_button">
								<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
								<a href="./?m=set&a=config&d=user_level" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>								
							</div>
							<div class="clear"></div>
						</div>
						<input type="hidden" name="LId" value="<?=$LId;?>" />
						<input type="hidden" name="do_action" value="set.user_level_edit" />
					</form>	
				<?php }else{ ?>
					<div class="config_table_body <?=$c['manage']['cdx_limit'];?>">
						<div class="blank20"></div>
						<?php 
						$level_row=str::str_code(db::get_all('user_level', 1, '*', 'FullPrice desc'));
						foreach((array)$level_row as $v){
						?>
							<div class="table_item" data-id="<?=$v['PId'];?>">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table">
									<tbody>
										<tr>
											<td width="45%" nowrap="nowrap">
												<span class="name"><?=$v['Name'.$c['manage']['web_lang']];?></span>
												<div class="img img_small"><img src="<?=$v['PicPath'];?>" alt=""><span></span></div>
											</td>
											<td width="25%" nowrap="nowrap">
												{/user.level.discount/}: <?=$v['Discount'];?>% <br />
												{/user.level.condition_price/}: <?=$c['manage']['currency_symbol'].$v['FullPrice']; ?>
											</td>
											<td width="15%" nowrap="nowrap" align="center" class="user_level_used">
												<a href="./?m=set&a=config&d=user_level&p=edit&LId=<?=$v['LId']; ?>" class="edit">{/global.edit/}</a><br />
												<a href="./?do_action=set.user_level_del&LId=<?=$v['LId']; ?>" class="del">{/global.del/}</a>
											</td>	
											<td width="10%" nowrap="nowrap" class="user_level_used" align="center">
												<div class="switchery<?=($v['IsUsed'])?' checked':'';?>" data-id="<?=$v['LId'];?>">
													<input type="checkbox" name="IsUsed" value="1"<?=($v['IsUsed'])?' checked':'';?> />
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>
											</td>				
										</tr>
									</tbody>
								</table>
							</div>
						<?php }?>
					</div>
					<a href="./?m=set&a=config&d=user_level&p=edit" class="add set_add <?=$c['manage']['cdx_limit'];?>">{/global.add/}</a>
				<?php }?>
			</div>
		<?php
		}elseif($c['manage']['do']=='user_reg_set'){
			//注册事项
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_user_edit();});</script>
			<div id="reg_set" class="global_container global_form">
				<a href="./?m=set&a=config<?=$_GET['p'] ? '&d=user_reg_set' : ''; ?>" class="return_title">
					<span class="return">{/set.config.user_info/}</span> 
					<span class="s_return">/ {/user.reg_set.reg_set/}</span>
					<?php if($_GET['p']){?>
						<span class="s_return">/ {/user.reg_set.custom_events/}</span>
					<?php }?>
				</a>
				<?php
				if($c['manage']['page']=='list'){ 
					$row=str::str_code(db::get_all('user_reg_set', '1', '*', "{$c['my_order']} SetId asc"));
				?>
					<div class="blank20"></div>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="25%" nowrap="nowrap">{/user.reg_set.name/}</td>
								<td width="15%" nowrap="nowrap">{/user.reg_set.type/}</td>
								<td width="25%" nowrap="nowrap">{/user.reg_set.option/}</td>
								<td width="15%" nowrap="nowrap">{/global.operation/}</td>
							</tr>
						</thead>
						<tbody>
							<?php foreach((array)$row as $v){?>
							<tr>
								<td><?=$v['Name'.$c['manage']['web_lang']];?></td>
								<td><?=$c['manage']['lang_pack']['user']['reg_set']['type_list'][$v['TypeId']];?></td>
								<td class="line_h_20"><?=str::format($v['Option'.$c['manage']['web_lang']]);?></td>
								<td>
									<a href="javascript:;" data-url="./?m=set&a=config&d=user_reg_set&p=edit&SetId=<?=$v['SetId']; ?>" class="edit">{/global.edit/}</a> <br />
									<a href="./?do_action=set.user_reg_set_del&SetId=<?=$v['SetId']; ?>" class="del">{/global.del/}</a>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
					<a href="javascipt:;" data-url="./?m=set&a=config&d=user_reg_set&p=edit" class="add set_add">{/global.add/}</a>
					<div id="fixed_right">
						<div class="global_container reg_set_edit"></div>
					</div>
				<?php }elseif($c['manage']['page']=='edit'){ ?>
					<div class="reg_set_edit">
						<?php 
						$SetId=(int)$_GET['SetId'];
						$row=str::str_code(db::get_one('user_reg_set', "SetId={$SetId}"));
						$type_id=(int)$row['TypeId'];	
						?>
						<div class="top_title"><?=$SetId?'{/global.edit/}':'{/global.add/}';?>{/user.reg_set.custom_events/} <a href="javascript:;" class="close"></a></div>
						<form id="reg_set_form" class="global_form">
							<div class="rows">
								<label>{/user.reg_set.type/}</label>
								<div class="input">
									<div class="box_select"><?=str_replace('<select', '<select id="type_select" class="box_input"', ly200::form_select($c['manage']['lang_pack']['user']['reg_set']['type_list'], 'TypeId', $type_id));?></div>
								</div>
							</div>
							<div class="rows">
								<label>
									{/user.reg_set.name/}
									<div class="tab_box"><?=manage::html_tab_button();?></div>
								</label>
								<div class="input"><?=manage::form_edit($row, 'text', 'Name', 30, 50, 'notnull');?></div>
							</div>
							<div class="rows row_option" style="display:<?=$type_id==1?'':'none';?>;">
								<label>
									{/user.reg_set.option/}
									<div class="tab_box"><?=manage::html_tab_button();?></div>
									<span class="fc_red">{/user.reg_set.option_tip/}</span>
								</label>
								<div class="input">
									<?=str_replace('<textarea', '<textarea class="box_textarea"', manage::form_edit($row, 'textarea', 'Option'));?>
								</div>
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.save/}">
									<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
								</div>
							</div>
							<input type="hidden" name="SetId" value="<?=$SetId; ?>" />
							<input type="hidden" name="do_action" value="set.user_reg_set_edit" />
						</form>
					</div>
				<?php }else{ ?>
					<div class="blank20"></div>
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
						<thead>
							<tr>
								<td width="70%" nowrap="nowrap">{/user.reg_set.name/}</td>
								<td width="15%" nowrap="nowrap">{/global.used/}</td>
								<td width="15%" nowrap="nowrap">{/global.required/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$RegSet=db::get_value('config', 'GroupId="user" and Variable="RegSet"', 'Value');
							$reg_ary=str::json_data($RegSet, 'decode');
							foreach($c['manage']['user_reg_field'] as $k=>$v){
								if($k=='Email') continue;
								$status=$reg_ary[$k];
							?>
							<tr>
								<td nowrap="nowrap">{/user.reg_set.<?=$k;?>/}<?=!$v?'{/user.reg_set.fixed/}':'';?></td>
								<td nowrap="nowrap">
									<div class="switchery<?=(($status[0] && $v) || !$v)?' checked':'';?><?=!$v?' no_drop':'';?>"<?=$v?" field='{$k}' status='{$status[0]}'":'';?>>
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
								</td>
								<td nowrap="nowrap">
									<?php if($k!='Code'){?>
										<div class="switchery<?=(($status[1] && $v) || !$v)?' checked':'';?><?=(($v && !$status[0]) || !$v)?' no_drop':'';?>"<?=$v?" field='{$k}NotNull' status='{$status[1]}'":'';?>>
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
									<?php }?>
								</td>
							</tr>
							<?php }?>
							<tr>
								<td nowrap="nowrap">{/user.reg_set.custom_events/}</td>
								<td nowrap="nowrap"></td>
								<td nowrap="nowrap">
									<a href="./?m=set&a=config&d=user_reg_set&p=list" class="go"></a>
								</td>
							</tr>
						</tbody>
					</table>
				<?php } ?>
			</div>
		<?php
		}elseif($c['manage']['do']=='shopping_set'){
			//购物设置
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_shopping_edit();});</script>
			<form id="shopping_edit_form" class="global_form">
				<div class="global_container">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/set.config.shopping_info/}</span>
					</a>
					<div class="rows clean">
						<label></label>
						<div class="input min_38">
							{/set.config.low_consumption/}
							<div class="switchery<?=$c['manage']['config']['LowConsumption']?' checked':'';?>">
								<input type="checkbox" name="LowConsumption" value="1"<?=$c['manage']['config']['LowConsumption']?' checked':'';?>>
								<div class="switchery_toggler"></div>
								<div class="switchery_inner">
									<div class="switchery_state_on"></div>
									<div class="switchery_state_off"></div>
								</div>
							</div>
							<span class="low_consumption <?=$c['manage']['config']['LowConsumption']? '':'hide';?>">
								&nbsp;&nbsp;&nbsp;&nbsp;{/products.products.price/}
								<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" name="LowPrice" value="<?=$c['manage']['config']['LowPrice'];?>" size="5" rel="amount" maxlength="255" /></span>
							</span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/set.config.less_stock/}</label>
						<div class="input">
							<span class="input_radio_box <?=$c['manage']['config']['LessStock']==0?'checked':'';?>">
								<span class="input_radio">
									<input type="radio" name="LessStock" value="0" <?=$c['manage']['config']['LessStock']==0?'checked':'';?> />
								</span>
								{/set.config.less_stock_ary.0/}
							</span>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="input_radio_box <?=$c['manage']['config']['LessStock']==1?'checked':'';?>">
								<span class="input_radio">
									<input type="radio" name="LessStock" value="1" <?=$c['manage']['config']['LessStock']==1?'checked':'';?> />
								</span>
								{/set.config.less_stock_ary.1/}
							</span>
						</div>
					</div>
					<?php if($c['FunVersion']>0 && $c['FunVersion']<10){?>
						<div class="rows clean orders_sms_rows">
							<label>{/set.config.orders_sms/}</label>
							<div class="input">
								<input type="text" class="box_input" name="OrdersSms" value="<?=$c['manage']['config']['OrdersSms'];?>" size="16" maxlength="255" />
								&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="input_checkbox_box <?=(int)$c['manage']['config']['OrdersSmsStatus'][0]?'checked':'';?>">
									<span class="input_checkbox">
										<input type="checkbox" name="OrdersSmsStatus[]" value="0" <?=(int)$c['manage']['config']['OrdersSmsStatus'][0]?'checked':'';?> />
									</span>
									{/set.config.orders_sms_ary.0/}
								</span>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="input_checkbox_box <?=(int)$c['manage']['config']['OrdersSmsStatus'][1]?'checked':'';?>">
									<span class="input_checkbox">
										<input type="checkbox" name="OrdersSmsStatus[]" value="1" <?=(int)$c['manage']['config']['OrdersSmsStatus'][1]?'checked':'';?> />
									</span>
									{/set.config.orders_sms_ary.1/}
								</span>
							</div>
						</div>
					<?php }?>
					<div class="rows clean">
						<label>{/set.config.arrival_info/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($c['manage']['config']['ArrivalInfo'], 'editor_simple', 'ArrivalInfo');?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
							<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="set.config_shopping_edit">
				</div>
			</form>
		<?php
		}elseif($c['manage']['do']=='watermark_set'){
			//水印设置
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_watermark_edit();});</script>
			<form id="watermark_edit_form" class="global_form">
				<div class="global_container">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return"><?=$_GET['update']?'{/set.config.water.replace/}':'{/set.config.watermark_config/}'; ?></span>
					</a>
					<?php
					if($_GET['update'] && $c['manage']['config']['UpdateWaterStatus']){
						//判断已经更新到第几页
						if(db::get_row_count('config', 'GroupId="Water" and Variable="Number"')){
							$Start=(int)db::get_value('config', 'GroupId="Water" and Variable="Number"', 'Value');
						}else{
							db::insert('config', array('GroupId'=>'Water', 'Variable'=>'Number', 'Value'=>'0'));
							$Start=0;
						}
						//判断是否在更新颜色图片
						if(db::get_row_count('config', 'GroupId="Water" and Variable="IsColor"')){
							$IsColor=(int)db::get_value('config', 'GroupId="Water" and Variable="IsColor"', 'Value');
						}else{
							db::insert('config', array('GroupId'=>'Water', 'Variable'=>'IsColor', 'Value'=>'0'));
							$IsColor=0;
						}
						$prod_count=db::get_row_count('products', '1');//产品总数
						$page_count=10;//每次分开更新的数量
						$total_pages=ceil($prod_count/$page_count);
						$num=($Start/$total_pages)*50;
						$IsColor && $num+=50;
						$num<10 && $num=10;
					?>
						<div class="themes_progress">
							<div class="box_progress">
								<div class="status">{/set.config.water.processing/}</div>
								<div class="progress">
									<div class="num" style="width:<?=$num;?>%;"><span><?=$num;?>%</span></div>
								</div>
								<div class="tips">{/set.config.water.wait/}</div>
								<input type="button" class="btn_global" id="btn_progress_continue" value="{/products.sync.progress.continue/}" />
								<input type="button" class="btn_global" id="btn_progress_keep" value="{/set.config.water.proceed/}" />
								<input type="button" class="btn_global" id="btn_progress_cancel" value="{/global.cancel/}" />
								<input type="hidden" name="Start" value="<?=(int)$Start;?>" />
								<input type="hidden" name="IsColor" value="<?=(int)$IsColor;?>" />
								<input type="hidden" name="do_action" value="set.config_watermark_update" />
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<?php /*<input type="button" class="btn_global btn_submit" value="{/global.save/}" />*/?>
									<a href="./?m=set&a=config&d=watermark_set&update=1" title="{/set.config.water.proceed/}"><input type="button" class="btn_global btn_proceed" value="{/set.config.water.proceed/}" /></a>
									<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
								</div>
							</div>
						</div>
					<?php }else{ ?>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<span class="input_checkbox_box<?=$c['manage']['config']['IsThumbnail']?' checked':'';?>">
									<span class="input_checkbox">
										<input type="checkbox" name="IsThumbnail" value="1"<?=$c['manage']['config']['IsThumbnail']?' checked':'';?>>
									</span>{/set.config.water.thumbnail/}
								</span>
							</div>
						</div>
						<div class="rows clean">
							<label>{/set.config.watermark_upfile/}</label>
							<div class="input">
								<?=manage::multi_img('WatermarkDetail', 'WatermarkPath', $c['manage']['config']['WatermarkPath']); ?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/set.config.alpha/}</label>
							<div class="input">
								<div class="blank6"></div>
								<div id="slider_box">
									<span class="fl">{/set.config.opacity/}</span>
									<div id="slider" class="fl"></div>
									<span class="fl">{/set.config.opaque/}&nbsp;&nbsp;&nbsp;&nbsp;</span>
									<div id="slider_value" class="fl"><?=$c['manage']['config']['Alpha'];?>%</div>
									<span>{/set.config.alpha_png/}</span>
								</div>
								<input type="hidden" name="Alpha" value="<?=$c['manage']['config']['Alpha'];?>" />
							</div>
						</div>
						<div class="rows clean">
							<label>{/set.config.water_position/}</label>
							<div class="input">
								<?php 
								$position_ary=array(1,5,9);
								foreach((array)$position_ary as $v){ 
								?>
								<div class="watermark_position watermark_position_<?=$v; ?> <?=$c['manage']['config']['WaterPosition']==$v ? 'cur' : ''; ?>" data-position="<?=$v; ?>"></div>
								<?php } ?>
							</div>
							<input type="hidden" name="WaterPosition" value="<?=$c['manage']['config']['WaterPosition'];?>" />
						</div>
						<input type="hidden" name="do_action" value="set.config_watermark_edit" />
						<div class="rows clean">
							<label></label>
							<div class="input input_button">
								<input type="button" class="btn_global btn_submit" value="<?=is_file($c['root_path'].$c['manage']['config']['WatermarkPath']) ? '{/set.config.water.save_update/}' : '{/global.save/}'; ?>" />
								<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel"  value="{/global.return/}" /></a>
							</div>
						</div>
					<?php } ?>
				</div>
			</form>
		<?php
		}elseif($c['manage']['do']=='share'){
			//社交媒体
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_share_edit();});</script>
			<form id="share_edit_form" class="global_form">
				<div class="global_container global_form">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/set.config.social/}</span> 
					</a>
					<div class="blank20"></div>
					<div class="share_box">
						<?php 
						$ShareMenuAry=$c['manage']['config']['ShareMenu'];
						foreach((array)$c['share'] as $v){
							if(!$ShareMenuAry[$v] || $v == 'Google') continue;
							?>
							<table border="0" cellpadding="5" cellspacing="0" class="config_table share_item" data-share="<?=$v;?>">
								<tr>
									<td width="30%" nowrap="nowrap">
										<div class="share_div share_<?=strtolower($v);?>"></div>
										<span class="share_str"><?=$v;?></span>
									</td>
									<td width="55%" nowrap="nowrap">
										<input type="text" class="box_input" name="Share<?=$v;?>" value="<?=$ShareMenuAry[$v];?>" maxlength="255" />
									</td>
									<td width="15%" nowrap="nowrap" align="center">
										<a href="javascript:;" data-share="<?=$v;?>" class="share_del icon_delete_1"></a>
									</td>					
								</tr>
							</table>
						<?php }?>
					</div>
					<div class="share_tpl hide" data-count="<?=count($c['share']); ?>">
						<table border="0" cellpadding="5" cellspacing="0" class="config_table share_item" data-share="">
							<tr>
								<td width="30%" nowrap="nowrap">
									<div class="box_select">
										<select name="tax_code_type">
											<option value="0">{/global.type/}</option>
											<?php foreach((array)$c['share'] as $v){?>
												<option value="<?=$v;?>" class="<?=$ShareMenuAry[$v]?'hide':'';?>"<?=$ShareMenuAry[$v]?' disable':'';?>><?=$v;?></option>
											<?php }?>
										</select>
									</div>
								</td>
								<td width="55%" nowrap="nowrap">
									<input type="text" class="box_input" name="" value="" maxlength="255" />
								</td>
								<td width="15%" nowrap="nowrap" align="center">
									<a href="javascript:;" data-share="" class="share_del icon_delete_1"></a>
								</td>					
							</tr>
						</table>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input">
							<a href="./?m=set&a=config" title="{/global.return/}" class="fr"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
							<input type="button" class="btn_global btn_submit fr" style="margin-right:15px;" value="{/global.save/}" />
							<a href="javascript:;" class="add btn_add_item btn_add_share">{/global.add/}</a>
							<div class="clear"></div>
						</div>
					</div>
					<br />
					<br />
				</div>
				<input type="hidden" name="do_action" value="set.config_share_edit" />
			</form>
		<?php
		}elseif($c['manage']['do']=='company'){ 
			//公司信息
			$config_row=str::str_code(db::get_all('config', "GroupId='print'"));
			foreach($config_row as $v){
				$c['manage']['config'][$v['Variable']]=$v['Value'];
			}
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_company_edit();});</script>
			<form id="company_edit_form" class="global_form">
				<div class="global_container global_form">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/set.config.company_info/}</span>
					</a>
					<div class="rows">
						<label>{/set.config.logo/}</label>
						<div class="input">
							<?=manage::multi_img('LogoDetail', 'LogoPath', $c['manage']['config']['LogoPath']); ?>
						</div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.Company/}</label>
						<div class="input"><input name="Compeny" value="<?=$c['manage']['config']['Compeny'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.Address/}</label>
						<div class="input"><input name="Address" value="<?=$c['manage']['config']['Address'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.city/}</label>
						<div class="input"><input name="City" value="<?=$c['manage']['config']['City'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.state/}</label>
						<div class="input"><input name="State" value="<?=$c['manage']['config']['State'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.zipcode/}</label>
						<div class="input"><input name="ZipCode" value="<?=$c['manage']['config']['ZipCode'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.Email/}</label>
						<div class="input"><input name="Email" value="<?=$c['manage']['config']['Email'];?>" type="text" class="box_input" maxlength="255" size="50"></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.Telephone/}</label>
						<div class="input"><input name="Telephone" value="<?=$c['manage']['config']['Telephone'];?>" type="text" class="box_input" maxlength="255" size="50" notnull></div>
					</div>
					<div class="rows">
						<label>{/user.reg_set.Fax/}</label>
						<div class="input"><input name="Fax" value="<?=$c['manage']['config']['Fax'];?>" type="text" class="box_input" maxlength="20" size="50"></div>
					</div>
                    <div class="rows">
                        <label>协议英文</label>
                        <div class="input"><?=manage::Editor('xieyi',$c['manage']['config']['xieyi']);?></div>
                    </div>
                    <div class="rows">
                        <label>协议波斯语</label>
                        <div class="input"><?=manage::Editor('xieyi_fa',$c['manage']['config']['xieyi_fa']);?></div>
                    </div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
							<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="set.config_company_edit" />
				</div>
			</form>
		<?php
		}elseif($c['manage']['do']=='seo'){
			//SEO
			$seo_row=str::str_code(db::get_one('meta', 'Type="home"'));
			$MId=(int)$seo_row['MId'];
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_seo_edit();});</script>
			<form id="seo_edit_form" class="global_form">
				<div class="global_container global_form">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/products.products.seo_info/}</span>
					</a>
					<div class="rows clean">
						<label>{/global.title/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($seo_row, 'text', 'SeoTitle', 70);?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/global.keyword/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($seo_row, 'textarea', 'SeoKeyword', 70);?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/global.depict/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($seo_row, 'textarea', 'SeoDescription', 70);?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" style="margin-right:15px;" value="{/global.save/}" />
							<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
				</div>
				<input type="hidden" name="do_action" value="set.config_seo_edit" />
				<input type="hidden" name="MId" value="<?=$MId; ?>" />
				<input type="hidden" name="Type" value="home" />
			</form>
		<?php
		}elseif($c['manage']['do']=='email'){ 
			//邮件设置
			$Value=$_GET['v'];
			$email_config_row=str::json_data(db::get_value('config', 'GroupId="email" and Variable="config"', 'Value'), 'decode');
			$email_notice_row=str::json_data(db::get_value('config', 'GroupId="email" and Variable="notice"', 'Value'), 'decode');
			$BottomContent=str::json_data(db::get_value('config', 'GroupId="email" and Variable="bottom"', 'Value'), 'decode');
			if($Value && in_array($Value, $c['manage']['email_notice'])){
				//邮件设置
				$system_email_row=str::str_code(db::get_one('system_email_tpl', "Template='$Value'"));
			?>
				<script type="text/javascript">$(document).ready(function(){set_obj.config_email_templete_edit()})</script>
				<style>#email_edit_form .tpl_tips.<?=$Value;?>{display:block;}</style>
				<div class="center_container_1200">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/set.config.email_config/}</span> 
						<span class="s_return">/ {/email.notice.notice_ary.<?=$Value;?>/}</span>
					</a>
				</div>
				<form id="email_edit_form" class="global_form center_container_1200">
					<div class="left_container">
						<div class="left_container_side">
							<div class="global_container">
								<div class="big_title">{/global.content/}</div>
								<div class="rows">
									<label>{/email.subject/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
									<div class="input"><?=manage::form_edit($system_email_row, 'text', 'Title', 70, 255, 'notnull');?></div>
								</div>
								<div class="rows clean">
									<label>{/email.email_logs.content/} <div class="tab_box"><?=manage::html_tab_button();?></div></label>
									<div class="input"><?=manage::form_edit($system_email_row, 'editor', 'Content');?></div>
								</div>
								<div class="rows clean">
									<label></label>
									<div class="input input_button">
										<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
										<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="right_container">
						<div class="global_container">
							<div class="big_title">{/email.global.variable/}</div>
							<div class="rows clean sys_remark">
								<div class="input">{/email.sys_remark/}</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="template" value="<?=$Value;?>" />
					<input type="hidden" name="do_action" value="set.config_email_templete_edit" />
				</form>
		<?php
			}else{
				//邮箱设置
				$ver = (int)$_GET['ver']; 
			?>
				<script type="text/javascript">$(document).ready(function(){set_obj.config_email_edit()})</script>
				<?php if ($ver) { ?>
					<form id="smtp_ver_form" class="global_form">
						<div class="global_container">
							<a href="javascript:history.back(-1);" class="return_title">
								<span class="return">{/set.email.ver/}</span>
							</a>
							<div class="themes_progress">
								<div class="box_progress">
									<div class="status">{/set.email.vering/}</div>
									<div class="loading"></div>
									<div class="tips">{/set.email.tips/}.</div>
								</div>
								<div class="rows clean">
									<label></label>
									<div class="input input_button">
										<a href="./?m=set&a=config&d=email" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
									</div>
								</div>
							</div>
						</div>
					</form>	
				<?php } else { ?>
					<div class="global_container">
						<form id="email_config_form" class="global_form">
							<a href="javascript:history.back(-1);" class="return_title">
								<span class="return">{/email.mailbox_config/}&nbsp;&nbsp;<span class="box_explain">( {/set.explain.send_from/} )</span></span>
							</a>
							<div class="rows clean">
								<label>{/email.from_email/}</label>
								<div class="input"><input name="FromEmail" value="<?=$email_config_row['FromEmail'];?>" type="text" class="box_input" size="30" maxlength="100" /></div>
							</div>
							<div class="rows clean">
								<label>{/email.from_name/}</label>
								<div class="input"><input name="FromName" value="<?=$email_config_row['FromName'];?>" type="text" class="box_input" size="30" maxlength="100" /></div>
							</div>
							<div class="rows clean module1">
								<label>{/email.smtp/}</label>
								<div class="input"><input name="SmtpHost" value="<?=$email_config_row['SmtpHost'];?>" type="text" class="box_input" size="30" maxlength="100" /></div>
							</div>
							<div class="rows clean module1">
								<label>{/email.port/}</label>
								<div class="input"><input name="SmtpPort" value="<?=$email_config_row['SmtpPort'];?>" type="text" class="box_input" size="5" maxlength="5" /></div>
							</div>
							<div class="rows clean module1">
								<label>{/email.email/}</label>
								<div class="input"><input name="SmtpUserName" value="<?=$email_config_row['SmtpUserName'];?>" type="text" class="box_input" size="30" maxlength="100" /></div>
							</div>
							<div class="rows clean module1">
								<label>{/email.password/}</label>
								<div class="input"><input name="SmtpPassword" value="<?=$email_config_row['SmtpPassword'];?>" type="password" class="box_input" size="30" maxlength="100" /></div>
							</div>
							<div class="rows clean">
								<label>{/email.bottom_content/} <div class="tab_box"><?=manage::html_tab_button();?></div></label>
								<div class="input"><?=manage::form_edit($BottomContent, 'editor', 'BottomContent');?></div>
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
									<a href="./?m=set&a=config" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
								</div>
							</div>
							<input type="hidden" name="do_action" value="set.config_email_edit" />
						</form>
					</div>
				<?php } ?>
		<?php
			}
		}elseif($c['manage']['do']=='language'){
			//语言设置
		?>
			<script type="text/javascript">$(document).ready(function(){set_obj.config_language_edit();});</script>
			<div class="global_container global_form">
				<?php 
				$lang=trim($_GET['lang']);
				$LanguageFlag=str::json_data(htmlspecialchars_decode($c['manage']['config']['LanguageFlag']), 'decode');
				$LanguageCurrency=str::json_data(htmlspecialchars_decode($c['manage']['config']['LanguageCurrency']), 'decode');
				?>
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/set.config.language_config/}</span> 
					<span class="s_return">/ {/language.<?=$lang;?>/}</span>
					<span class="s_return">/ {/global.edit/}</span>
				</a>
				<form id="language_edit_form">
					<div class="rows">
						<label>{/set.country.flag/}</label>
						<div class="input">
							<?=manage::multi_img('FlagDetail', 'FlagPath', $LanguageFlag[$lang]); ?>
						</div>							
					</div>
					<div class="rows">
						<label>{/set.exchange.currency/}</label>
						<div class="input">
							<?php
							$currency_row=db::get_all('currency', 'IsUsed=1', 'CId, Currency, Symbol', $c['my_order'].'CId asc');
							?>
							<div class="box_select">
								<select name="Currency" class="box_input">
									<option value="">{/global.select_index/}</option>
									<?php foreach($currency_row as $v){?>
										<option value="<?=$v['CId'];?>"<?=$LanguageCurrency[$lang]==$v['CId']?' selected':'';?>><?=$v['Currency'];?></option>
									<?php }?>
								</select>
							</div>
						</div>							
					</div>
					<?php if(in_array($lang,$c['manage']['config']['Language']) && $c['manage']['config']['LanguageDefault']!=$lang){ ?>
						<div class="rows">
							<label></label>
							<div class="input">
								<span class="input_checkbox_box <?=$c['manage']['config']['LanguageDefault']==$lang ? 'checked' : ''; ?>">
									<span class="input_checkbox">
										<input type="checkbox" name="LanguageDefault" <?=$c['manage']['config']['LanguageDefault']==$lang ? 'checked="checked"' : ''; ?> value="1">
									</span>{/set.config.default_language/}
								</span>
							</div>
						</div>
					<?php }?>
					<div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=set&a=config"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>
					</div>
					<input type="hidden" name="do_action" value="set.language_edit" />
					<input type="hidden" name="Language" value="<?=$lang; ?>" />
				</form>

			</div>
		<?php }?>
	</div>
</div>