<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'manage_logs'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('set', 0, array('a'=>'manage_logs', 'd'=>'add')),
	'edit'	=>	manage::check_permit('set', 0, array('a'=>'manage_logs', 'd'=>'edit')),
	'del'	=>	manage::check_permit('set', 0, array('a'=>'manage_logs', 'd'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'?'logs':'logs_inside');

$where='1';
$UserId=$_GET['UserId'];
$Keyword=$_GET['Keyword'];
$Module=$_GET['Module'];
$UserId && $where.=" and UserId='$UserId'";
$Module && $where.=" and Module='$Module'";
$Keyword && $where.=" and Log like '%$Keyword%'";
$manage_logs_row=db::get_limit_page('manage_operation_log', $where, '*', 'LId desc', (int)$_GET['page'], 20);
$i=1;
?>
<script type="text/javascript">$(document).ready(function(){set_obj.manage_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<div class="center_container_1000">
        <div class="inside_container">
            <h1>{/module.set.manage_logs/}</h1>
        </div>
        <div class="inside_table">
            <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                <thead>
                    <tr>
                        <td width="1%" nowrap="nowrap">{/global.serial/}</td>
                        <td width="10%" nowrap="nowrap">{/manage.manage_logs.username/}</td>
                        <td width="10%" nowrap="nowrap">{/manage.manage_logs.module/}</td>
                        <td width="25%" nowrap="nowrap">{/manage.manage_logs.log_contents/}</td>
                        <td width="10%" nowrap="nowrap">{/manage.manage_logs.ip/}</td>
                        <td width="10%" nowrap="nowrap">{/global.time/}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($manage_logs_row[0] as $v){
                    ?>
                        <tr>
                            <td nowrap="nowrap"><?=$manage_logs_row[4]+$i++;?></td>
                            <td nowrap="nowrap"><?=$v['UserName'];?></td>
                            <td nowrap="nowrap">{/set.manage_logs.<?=$v['Module'];?>/}</td>
                            <td><?=$v['Log'];?></td>
                            <td nowrap="nowrap"><?=$v['Ip'].'<br />'.ly200::ip($v['Ip']);?></td>
                            <td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
            <?=html::turn_page($manage_logs_row[1], $manage_logs_row[2], $manage_logs_row[3], '?'.ly200::query_string('page').'&page=');?>
        </div>
    </div>
</div>