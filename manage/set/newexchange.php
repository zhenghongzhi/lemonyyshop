<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'newexchange'));//检查权限
$permit_ary=array(
    'add'	=>	manage::check_permit('set', 0, array('a'=>'newexchange', 'd'=>'add')),
    'edit'	=>	manage::check_permit('set', 0, array('a'=>'newexchange', 'd'=>'edit')),
    'del'	=>	manage::check_permit('set', 0, array('a'=>'newexchange', 'd'=>'del'))
);

echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.newexchange_init()});</script>
<div id="newexchange" class="r_con_wrap">
    <div class="center_container">
        <div class="global_container">
            <?php if($c['manage']['do']=='index'){?>
                <div class="big_title">{/module.set.exchange/}&nbsp;&nbsp;<span class="box_explain">( {/set.explain.rate/} )</span></div>
                <div class="blank20"></div>
            <?php }else{?>
                <a href="javascript:history.back(-1);" class="return_title">
                    <span class="return">{/module.set.exchange/}</span>
                    <span class="s_return">  /  <?=$_GET['CId']?'{/global.edit/}':'{/global.add/}';?></span>
                </a>
            <?php }?>
            <?php
            if($c['manage']['do']=='edit'){
                $CId=(int)$_GET['CId'];
                $currency_row=str::str_code(db::get_one('exchange', "id='{$CId}'", '*'));
                ?>
                <?php /***************************** 汇率编辑 Start *****************************/?>
                <form id="newexchange_edit_form" class="global_form">
                    <div class="rows clean">
                        <label>更新时间</label>
                        <div class="input"><input type="date" name="updatetime" value="<?=$currency_row['updatetime'];?>" class="box_input" size="20" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>汇率</label>
                        <div class="input"><input type="text" name="money" value="<?=$currency_row['money'];?>" class="box_input" size="10" maxlength="10" notnull rel="amount" /></div>
                    </div>

                    <div class="rows clean">
                        <label>需要兑换的货币名称</label>
                        <div class="input"><input type="text" name="currency" value="<?=$currency_row['currency'];?>" class="box_input" size="10" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>需要兑换的货币符号</label>
                        <div class="input"><input type="text" name="fuhao" value="<?=$currency_row['fuhao'];?>" class="box_input" size="10" maxlength="10" notnull /> <br /><b class="tips">{/set.exchange.symbol_tops/}</b></div>
                    </div>
                    <div class="rows clean">
                        <label>需要兑换的货币金额</label>
                        <div class="input"><input type="text" name="irrmoney" value="<?=$currency_row['irrmoney'];?>" class="box_input" size="20" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>是否启用</label>
                        <div class="input">
                            <select name="isuse" id="isuse" class="box_input">
                                <option value="1" <?=$currency_row['isuse']==1?'selected':'';?>>是</option>
                                <option value="2" <?=$currency_row['isuse']==2?'selected':'';?>>否</option>
                            </select>
                        </div>
                    </div>
                    <div class="rows clean">
                        <label>排序</label>
                        <div class="input"><input type="text" name="sort" value="<?=$currency_row['sort'];?>" class="box_input" size="10" maxlength="10" notnull rel="amount" /></div>
                    </div>


                    <div class="rows clean">
                        <label></label>
                        <div class="input input_button">
                            <input type="button" class="btn_global btn_submit <?=$c['manage']['cdx_limit']?>" value="{/global.save/}" />
                            <a href="./?m=set&a=newexchange"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
                        </div>
                    </div>
                    <input type="hidden" name="CId" value="<?=$CId; ?>" />
                    <input type="hidden" name="do_action" value="set.newexchange_edit" />
                </form>
                <?php /***************************** 汇率编辑 End *****************************/?>
                <?php
            }else{
                $where='1';
                $currency_row=str::str_code(db::get_all('exchange', $where, '*', 'sort asc'));

                ?>
                <div class="config_table_body">
                    <div class="table_item">
                        <table border="0" cellpadding="5" cellspacing="0" class="config_table">
                            <tbody>
                            <tr>
                                <td width="45%" nowrap="nowrap">
                                    兑换币种
                                </td>
                                <td width="25%" nowrap="nowrap">
                                   汇率
                                </td>
                                <td width="15%" nowrap="nowrap">
                                    操作
                                </td>
<!--                                <td width="10%" nowrap="nowrap" align="center">-->
<!---->
<!--                                </td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    foreach($currency_row as $k=>$v){ ?>
                        <div class="table_item" data-id="<?=$v['CId'];?>">
                            <table border="0" cellpadding="5" cellspacing="0" class="config_table">
                                <tbody>
                                <tr>
                                    <td width="45%" nowrap="nowrap">

                                        <div class="desc">
                                            <?=$v['currency'];?>(<?=$v['fuhao'];?>)
                                        </div>
                                    </td>
                                    <td width="25%" nowrap="nowrap">
                                        <?=(float)$v['irrmoney']*(float)$v['money'];?>IRR=<?=$v['fuhao'].$v['irrmoney'];?>
                                    </td>
                                    <td width="15%" nowrap="nowrap">
                                        <a href="./?m=set&a=newexchange&d=edit&CId=<?=$v['id']; ?>" class="edit <?=$c['manage']['cdx_limit']?>">{/global.edit/}</a>
                                    </td>
<!--                                    <td width="10%" nowrap="nowrap" align="center">-->
<!--                                        --><?php
//                                        if($permit_ary['edit']){
//                                            ?>
<!--                                            <div class="used_checkbox --><?//=$c['manage']['cdx_limit']?><!--">-->
<!--                                                <div class="switchery--><?//=$Used?' checked':'';?><!----><?//=($ManageDefault || $Default)?' no_drop':'';?><!--" data-cid="--><?//=$v['id']; ?><!--">-->
<!--                                                    <div class="switchery_toggler"></div>-->
<!--                                                    <div class="switchery_inner">-->
<!--                                                        <div class="switchery_state_on"></div>-->
<!--                                                        <div class="switchery_state_off"></div>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            --><?php
//                                        }else{
//                                            echo $Used?'{/global.n_y.1/} <br />':'';
//                                        }?>
<!--                                    </td>-->
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php }?>
                </div>
                <?php if($c['manage']['do']=='index' && $c['FunVersion']>=1){?>
                    <a href="./?m=set&a=newexchange&d=edit" class="add set_add <?=$c['manage']['cdx_limit']?>">{/global.add/}</a>
                <?php }?>
            <?php }?>
        </div>
    </div>
</div>