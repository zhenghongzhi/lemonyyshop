<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'country'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('set', 0, array('a'=>'country', 'd'=>'add')),
	'edit'	=>	manage::check_permit('set', 0, array('a'=>'country', 'd'=>'edit')),
	'del'	=>	manage::check_permit('set', 0, array('a'=>'country', 'd'=>'del'))
);
?>
<div id="country" class="r_con_wrap">
	<div class="center_container_1000">
		
		<?php
		if($c['manage']['do']=='index'){
			$currency_ary=array();
			$currency_row=db::get_all('currency', 'IsUsed=1', 'CId, Currency, Symbol', $c['my_order'].'CId asc');
			foreach($currency_row as $v){
				$currency_ary[$v['CId']]=$v['Currency'];
			}
			?>
			<script type="text/javascript">$(document).ready(function(){set_obj.country_init()});</script>
			<div class="inside_table">
				<div class="list_menu">
					<ul class="list_menu_button">
						<?php if($permit_ary['add']){?><li><a class="add" href="./?m=set&a=country&d=edit">{/global.add/}</a></li></ul><?php }?>
					</ul>
					<div class="search_form">
						<form method="get" action="?">
							<div class="k_input">
								<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
								<input type="button" value="" class="more" />
							</div>
							<input type="submit" class="search_btn" value="{/global.search/}" />
							<div class="clear"></div>
							<input type="hidden" name="m" value="set" />
							<input type="hidden" name="a" value="country" />
						</form>
					</div>
				</div>
				<div class="box_explain" style="padding: 0 30px;">{/set.explain.country/}</div>
				<?php 
				$where='1';
				$k=$_GET['k'];
				$Keyword=str::str_code($_GET['Keyword']);
				$Continent=(int)$_GET['Continent'];
				$k!='' && $where.=" and Country like '$k%'";
				$Keyword && $where.=" and Country like '%$Keyword%'";
				$Continent && $where.=" and Continent='$Continent'";
				$country_row=str::str_code(db::get_all('country', $where, '*', 'Country asc'));
				$continent_country_ary=array();
				foreach((array)$country_row as $v){
					$continent_country_ary[$v['Continent']][]=$v;
				}
				foreach((array)$continent_country_ary as $k => $v){
				?>
			        <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
			            <thead>
			            	<tr>
			            		<th colspan="<?=($permit_ary['edit'] || $permit_ary['del']) ? 5 : 4 ; ?>">
			            			<div class="big_title rows_hd_part">
										<span>{/continent.<?=$k;?>/}</span>
									</div>
			            		</th>
			            	</tr>
			                <tr>
			                    <td width="80%" nowrap="nowrap">{/set.country.country/}</td>
			                    <td width="130" nowrap="nowrap">{/set.country.code/}</td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
			                    	<td width="100" nowrap="nowrap">{/global.operation/}</td>
								<?php }?>
			                    <td width="60" nowrap="nowrap">{/global.used/}</td>
			                    <td width="100" nowrap="nowrap">{/set.country.hot/}</td>
			                </tr>
			            </thead>
			            <tbody>
			                <?php
							foreach((array)$v as $k1=>$v1){
								$IsUsed=(int)$v1['IsUsed'];
								$IsHot=(int)$v1['IsHot'];
								$IsDefault=(int)$v1['IsDefault'];
							?>
								<tr cid="<?=$v1['CId'];?>">
									<td nowrap="nowrap" class="img">
										<?=$v1['Country'];?>
										<?=$v1['Acronym']?'('.$v1['Acronym'].')':'';?>
										<?=$v1['CId']<=240?'<div class="icon_flag flag_'.strtolower($v1['Acronym']).'"></div>':'<img src="'.$v1['FlagPath'].'" />';?>
										<?=(int)$IsDefault?'<br /> <span class="gray">{/set.country.default_country/}</span>':'';?>
									</td>
									<td nowrap="nowrap">+<?=$v1['Code'];?></td>
									<?php if($permit_ary['edit'] || $permit_ary['del']){?>
										<td nowrap="nowrap" class="operation">
											<?php if($permit_ary['edit']){?><a class="" href="./?m=set&a=country&d=edit&CId=<?=$v1['CId'];?>">{/global.edit/}</a><?php }?>
											<br />
											<a href="./?m=set&a=country&d=state_list&CId=<?=$v1['CId'];?>" class="open_state gray" >{/set.country.edit_state/}</a>
											<?php if($v1['CId']>240 && $permit_ary['del']){?>
												<br />
												<a class="del gray" href="./?do_action=set.country_del&CId=<?=$v1['CId'];?>">{/global.del/}</a>
											<?php }?>
										</td>
									<?php }?>
									<td class="used_checkbox">
										<?php if($permit_ary['edit']){?>
											<div class="switchery<?=$IsUsed?' checked':'';?><?=$IsDefault?' no_drop':'';?>">
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										<?php
										}else{
											echo $IsUsed?'{/global.n_y.1/}':'';
										}
										?>
									</td>
									<td class="hot_checkbox">
										<?php if($permit_ary['edit']){?>
											<div class="switchery<?=$IsHot?' checked':'';?><?=$IsDefault?' no_drop':'';?>">
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										<?php
										}else{
											echo $IsHot?'{/global.n_y.1/}':'';
										}
										?>
									</td>
								</tr>
			                <?php }?>
			            </tbody>
			        </table>
		        <?php } ?>
			</div>
	    <?php 
		}elseif($c['manage']['do']=='edit'){
			$CId=(int)$_GET['CId'];
			$used_checked=' checked';
			$hot_checked=$state_checked='';
			if($CId){
				$country_row=str::str_code(db::get_one('country', "CId='{$CId}'"));
				$used_checked=$country_row['IsUsed']==1?' checked':'';
				$hot_checked=$country_row['IsHot']==1?' checked':'';
				$default_checked=$country_row['IsUsed']==1&&$country_row['IsHot']==1&&$country_row['IsDefault']==1?' checked':'';
				$state_checked=$country_row['HasState']==1?' checked':'';
			}
			?>
			<script type="text/javascript">$(document).ready(function(){set_obj.country_edit_init()});</script>
			<div class="global_container">
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.set.country/}</span> 
					<span class="s_return">/ <?=$CId?'{/global.edit/}':'{/global.add/}';?></span>
				</a>
				<form id="country_edit_form" class="global_form">
					<div class="rows clean">
						<label>
							{/set.country.country/}
							<div class="tab_box"><?=manage::html_tab_button();?></div>
						</label>
						<div class="input">
							<?php
							$country_data=str::json_data(htmlspecialchars_decode($country_row['CountryData']), 'decode');
							if(!$CId || $country_row['CId']>240){
								$country_ary=array();
								foreach($country_data as $k=>$v){
									$country_ary['Country_'.$k]=$v;
								}
								echo manage::form_edit($country_ary, 'text', 'Country', 25, 100, 'notnull');
							}else{
								foreach($c['manage']['config']['Language'] as $k=>$v){
									echo '<div class="tab_txt tab_txt_'.$v.'" '.($c['manage']['config']['LanguageDefault']==$v ? 'style="display:block;"' : '').' lang="'.$v.'">';
										echo '<div class="country_lang_list">';
											echo $country_data[$v].'<br />';
										echo '</div>';
									echo '</div>';
								}
							}?>
						</div>						
					</div>
					<div class="rows clean">
						<label>{/set.country.country/}{/set.country.acronym/}</label>
						<div class="input"><input type="text" name="Acronym" value="<?=$country_row['Acronym'];?>" class="box_input" size="10" maxlength="2" notnull=""<?=($CId && $country_row['CId']<240)?' readonly':'';?>></div>						
					</div>
					<div class="rows clean">
						<label>{/set.country.continent/}</label>
						<div class="input">
							<?php
							if($CId && $country_row['CId']<240){
								echo $c['manage']['lang_pack']['continent'][$country_row['Continent']];
							}else{?>
								<div class="box_select">
									<select name="Continent" class="box_input">
										<?php foreach($c['continent'] as $k=>$v){?>
											<option value="<?=$k;?>"<?=$country_row['Continent']==$k?' selected':'';?>>{/continent.<?=$k;?>/}</option>
										<?php }?>
									</select>
								</div>
							<?php }?>
						</div>						
					</div>
					<div class="rows clean">
						<label>{/set.country.code/}</label>
						<div class="input">
							<span class="unit_input" parent_null=""><b>+</b><input type="text" name="Code" value="<?=$country_row['Code'];?>" class="box_input" size="5" maxlength="5" parent_null="1" notnull=""<?=($CId && $country_row['CId']<240)?' readonly':'';?>></span>
						</div>						
					</div>
					<div class="rows clean">
		                <label>{/set.country.flag/}</label>
		                <div class="input upload_file upload_flag">
							<?php
							if(!$CId || $country_row['CId']>240){
								if($permit_ary['edit']){
									echo manage::multi_img('FlagDetail', 'FlagPath', $country_row['FlagPath']);
								}else{
									if(is_file($c['root_path'].$country_row['FlagPath'])) echo '<img src="'.$country_row['FlagPath'].'" />';
								}
							}else{
							?>
								<div class="icon_flag flag_<?=strtolower($country_row['Acronym']);?>"></div>
							<?php }?>
		                </div>		                
		            </div>
					<div class="rows clean" style="display:none;"><?php /********************** 暂时隐藏起来 2018/7/26 Sheldon **********************/?>
						<label>{/set.exchange.currency/}</label>
						<div class="input">
							<?php
							$currency_row=db::get_all('currency', 'IsUsed=1', 'CId, Currency, Symbol', $c['my_order'].'CId asc');
							?>
							<div class="box_select">
								<select name="Currency" class="box_input">
									<option value="">{/global.select_index/}</option>
									<?php
									foreach($currency_row as $v){
									?>
										<option value="<?=$v['CId'];?>"<?=$country_row['Currency']==$v['CId']?' selected':'';?>><?=$v['Currency'];?></option>
									<?php }?>
								</select>
							</div>
						</div>						
					</div>
					<div class="rows clean" style="display:<?=$used_checked==''||$hot_checked==''?'none':'block';?>;" id="default">
						<label>{/set.country.default/}{/set.country.country/}</label>
						<div class="input">
							<div class="switchery<?=$default_checked;?>">
								<input type="checkbox" name="IsDefault" value="1"<?=$default_checked;?>>
								<div class="switchery_toggler"></div>
								<div class="switchery_inner">
									<div class="switchery_state_on"></div>
									<div class="switchery_state_off"></div>
								</div>
							</div>
						</div>						
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.submit/}">
							<a href="./?m=set&a=country"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>
					</div>
					<input type="hidden" name="CId" value="<?=$CId;?>" />
					<input type="hidden" name="IsUsed" value="<?=$country_row['IsUsed'];?>" />
					<input type="hidden" name="IsHot" value="<?=$country_row['IsHot'];?>" />
					<input type="hidden" name="do_action" value="set.country_edit" />
				</form>
			</div>
	    <?php 
		}elseif($c['manage']['do']=='state_list'){
			$CId=(int)$_GET['CId'];
			$Country=str::str_code(db::get_one('country', "CId='{$CId}'"));
			!$Country && js::location('./?m=set&a=country');
			$states_row=str::str_code(db::get_all('country_states', "CId='{$CId}'", '*', $c['my_order'].'States asc'));
			echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
			?>
			<script type="text/javascript">$(document).ready(function(){set_obj.country_states_init()});</script>
			<div class="global_container">
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.set.country/}</span>
					<?php if($Country['Continent']){ ?><span class="s_return">/ {/continent.<?=$Country['Continent'];?>/}</span><?php } ?>
					<span class="s_return">/ <?=$Country['Country']; ?></span>
					<span class="s_return">/ {/global.edit/}</span>
				</a>
				<?php if($CId==226){ ?>
					<div class="tips">{/set.country.tips/}</div>
				<?php } ?>
				<div class="country_states" CId="<?=$CId;?>">
					<div class="country_menu_list">
						<dl>
							<?php
							foreach((array)$states_row as $k=>$v){
							?>
							<dt data-id="<?=$v['SId'];?>">
								<i></i>
								<div class="sub" data="&CId=<?=$CId;?>&SId=<?=$v['SId'];?>">
									<h5 class="fl"><strong><?=$v['AcronymCode']?'['.$v['AcronymCode'].'] ':'';?></strong><?=$v['States'];?></h5>
									<?php if($permit_ary['edit']){?>
										<a class="del menu_view fr" href="./?do_action=set.country_states_del&CId=<?=$CId;?>&SId=<?=$v['SId'];?>">{/global.del/}</a>
										<a class="edit menu_view fr" href="javascript:;" data-url="./?m=set&a=country&d=state_edit&CId=<?=$CId ?>&SId=<?=$v['SId'];?>">{/global.edit/}</a>
									<?php }?>
									<di class="clear"></di>
								</div>
							</dt>
							<?php }?>
						</dl>
					</div>
					<div class="clear"></div>
					<div class="blank12"></div>
					<a href="javascript:;" data-url="./?m=set&a=country&d=state_edit&CId=<?=$CId ?>" class="add set_add">{/global.add/}</a>
					<div id="fixed_right">
						<div class="country_states_edit global_container"></div>
					</div>
				</div>
			</div>
		<?php 
		}elseif($c['manage']['do']=='state_edit'){
			$CId=(int)$_GET['CId'];
			$SId=(int)$_GET['SId'];
			($CId && $SId) && $state_row=str::str_code(db::get_one('country_states', "CId='{$CId}' and SId='{$SId}'"));
			?>
			<div class="country_states_edit">
				<form id="country_states_edit_form" class="global_form">
					<div class="top_title"><?=$SId ? '{/global.edit/}' : '{/global.add/}'; ?>{/set.country.state/} <a href="javascript:;" class="close"></a></div>
					<div class="rows">
						<label>{/set.country.state/}</label>
						<div class="input"><input type="text" name="States" value="<?=$state_row['States'];?>" class="box_input" size="25" maxlength="50" notnull=""></div>							
					</div>
					<div class="rows">
						<label>{/set.country.state/}{/set.country.acronym/}</label>
						<div class="input"><input type="text" name="AcronymCode" value="<?=$state_row['AcronymCode'];?>" class="box_input" size="5" maxlength="5" notnull=""></div>							
					</div>
					<div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
					<input type="hidden" name="CId" value="<?=$CId;?>" />
					<input type="hidden" name="SId" value="<?=$SId;?>" />
					<input type="hidden" name="do_action" value="set.country_states_edit">
				</form>
			</div>
	    <?php }?>
	</div>
</div>