<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'shipping'));//检查权限

if(!$c['manage']['do'] || $c['manage']['do']=='index'){//重新指向“快递”页面
	$c['manage']['do']='express';
}
$OvId=(int)$_GET['OvId'];//海外仓ID
$SId=(int)$_GET['SId'];//快递公司ID
$AId=(int)$_GET['AId'];//快递分区ID
$TId=(int)$_GET['TId'];//运费模板ID
$set_row=str::str_code(db::get_one('shipping_config'));

$permit_ary=array(
	'add'			=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'express', 'p'=>'add')),
	'edit'			=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'express', 'p'=>'edit')),
	'del'			=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'express', 'p'=>'del')),
	'insurance_add'	=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'insurance', 'p'=>'add')),
	'insurance_edit'=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'insurance', 'p'=>'edit')),
	'insurance_del'	=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'insurance', 'p'=>'del')),
	'overseas_add'	=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'overseas', 'p'=>'add')),
	'overseas_edit'	=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'overseas', 'p'=>'edit')),
	'overseas_del'	=>	manage::check_permit('set', 0, array('a'=>'shipping', 'd'=>'overseas', 'p'=>'del'))
);
?>
<div id="shipping" class="r_con_wrap">
	<div class="center_container">
		<div class="global_container global_form">
			<?php if($c['manage']['page']=='index'){ ?>
				<div class="big_title">
					<?php $insurance_row=str::str_code(db::get_one('shipping_config', '1', 'IsInsurance')); ?>
					<a href="javascript:;" class="insurance_btn fr" data-url="./?m=set&a=shipping&d=insurance">{/set.explain.insurance/} (<?=$insurance_row['IsInsurance'] ? '{/set.explain.enable/}' : '{/set.explain.unenable/}'; ?>)</a>
					{/shipping.shipping.set/}
					<?php 
					if(in_array('shipping_template', (array)$c['manage']['plugins']['Used'])){
						$shipping_template_row=db::get_all('shipping_template', '1', '*');
						$shipping_template_tid=array();
						foreach((array)$shipping_template_row as $v){
							$shipping_template_tid[$v['TId']]=$v;
						}
						?>
						<div class="inside_table shipping_template_box">
							<dl class="period box_drop_down_menu">
								<dt class="more"><span><?=$shipping_template_tid[$TId] ? $shipping_template_tid[$TId]['Name'] : '选择运费模板'; ?></span><em></em></dt>
								<dd class="more_menu drop_down">
									<?php if($shipping_template_tid[$TId]){ ?>
									<div class="item">
										<a href="<?='?'.ly200::query_string('TId'); ?>">选择运费模板</a>
									</div>
									<?php } ?>
									<?php foreach((array)$shipping_template_row as $v){ ?>
										<div class="item">
											<a href="<?='?'.ly200::query_string('TId').'&TId='.$v['TId']; ?>"><?=$v['Name']; ?></a>
										</div>
									<?php } ?>
									<div class="item">
										<a href="javascript:;" class="template_edit_btn">管理运费模板</a>
									</div>
								</dd>
							</dl>
						</div>
					<?php } ?>
				</div>
				<div class="blank20"></div>
			<?php }else{ ?>
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/shipping.shipping.set/}</span> 
					<?php if($c['manage']['page']=='edit'){ ?>
						<span class="s_return">/ <?=$_GET['SId'] ? '{/global.edit/}' : '{/global.add/}'; ?></span>
					<?php } ?>
					<?php if($c['manage']['page']=='area'){ ?>
						<span class="s_return">/ {/shipping.area.area_list/}</span>
					<?php } ?>

				</a>
			<?php } ?>
			<?php
			if($c['manage']['do']=='express'){
				//快递管理
				echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
				?>
				<script type="text/javascript">$(function(){set_obj.shipping_express_init();});</script>
				<div class="config_table_body">
					<?php
					if($c['manage']['page']=='index'){
						//快递列表
						$api_ary=array();
						$api_row=db::get_all('shipping_api', '1');
						foreach($api_row as $v){ $api_ary[$v['AId']]=$v['Name']; }
						$i=1;
						$weight_area_ary=array('{/shipping.shipping.first_ext_weight/}', '{/shipping.shipping.weightarea/}', '{/shipping.shipping.weightmix/}', '{/shipping.shipping.qty/}', '{/shipping.shipping.special/}');
						$shipping_express_row=str::str_code(db::get_all('shipping', '1'.($TId ? " and TId='{$TId}'" : ''), '*', $c['my_order'].'SId asc'));
						foreach($shipping_express_row as $k=>$v){
						?>
							<div class="table_item" data-id="<?=$v['SId'];?>">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table r_con_table">
									<thead>
										<tr><td nowrap="nowrap" class="myorder"><?php /*<span class="icon_myorder"></span>*/ ?></td></tr>
									</thead>
									<tbody>
										<tr>
											<td width="45%" nowrap="nowrap" class="myorder_left">
												<div class="info"><div class="name"><?=$v['Express'];?></div></div>
												<div class="img img_big"><img src="<?=$v['Logo'];?>" alt="<?=$v['Express'];?>" /><span></span></div>
											</td>
											<td width="25%" nowrap="nowrap">
												<?=$v['IsAPI']>0?str_replace('%API%', $api_ary[$v['IsAPI']], $c['manage']['lang_pack']['shipping']['info']['api']):$weight_area_ary[$v['IsWeightArea']];?>
											</td>
											<td width="<?=$c['manage']['config']['ManageLanguage']=='zh-cn'?'15%':'25%';?>" class="operation">
												<a href="./?m=set&a=shipping&d=express&p=area&SId=<?=$v['SId'];?>" class="set open_area" data-url="" data-name="<?=$v['Express'];?>">{/shipping.shipping.set/}</a><br />
												<?php if($permit_ary['edit'] || $permit_ary['del']){ ?>
													<dl class="fl">
														<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
														<dd class="drop_down">
															<?php if($permit_ary['edit']){?><a class="tip_min_ico edit item" href="./?m=set&a=shipping&d=express&p=edit&SId=<?=$v['SId'];?>">{/global.edit/}</a><?php }?>
															<?php if($permit_ary['del']){?><a class="tip_min_ico del item" href="./?do_action=set.shipping_express_del&SId=<?=$v['SId'];?>">{/global.del/}</a><?php }?>
														</dd>
													</dl>
													<div class="clear"></div>
												<?php } ?>
											</td>
											<td width="10%" nowrap="nowrap" align="center" class="express_used">
												<?php
												if($permit_ary['edit']){
												?>
													<div class="switchery<?=$v['IsUsed']?' checked':'';?>" data-id="<?=$v['SId'];?>">
														<input type="checkbox" name="IsUsed" value="1"<?=$v['IsUsed']?' checked':'';?>>
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												<?php
												}else{
													echo $v['IsUsed']?'{/global.n_y.1/} <br />':'';
												}?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php }?>
					</div>
					<a href="./?m=set&a=shipping&d=express&p=edit" class="add set_add">{/global.add/}</a>
					<br />
					<div id="fixed_right">
						<div class="global_container insurance_edit"></div>
						<?php if(in_array('shipping_template', (array)$c['manage']['plugins']['Used'])){ ?>
							<div class="global_container shipping_template_edit">
								<?php
								$shipping_template_row=db::get_all('shipping_template', '1', '*');
								?>
								<form id="shipping_template_edit_form" class="global_form">
									<div class="top_title">管理运费模板<a href="javascript:;" class="close"></a></div>
									<div class="edit_template_list">
										<div class="rows">
											<label>{/global.name/}</label>
											<div class="input">
												<?php
												foreach($shipping_template_row as $v){
													$pro_count=db::get_row_count('shipping', "TId='{$v['TId']}'");
												?>
													<div class="item clean" data-id="<?=$v['TId'];?>">
														<span class="fl input_radio_box <?=$v['IsDefault']?'checked':'';?>">
															<span class="input_radio">
																<input type="radio" name="IsDefault" value="<?=$v['TId'];?>" <?=$v['IsDefault']?'checked="checked"':'';?>>
															</span>
															{/global.default/}
														</span>
														<input type="text" class="box_input fl" name="Name[<?=$v['TId'];?>]" value="<?=$v["Name"];?>" size="30" maxlength="255" autocomplete="off" notnull />
														<div class="product_count fl"><?=str_replace('%num%', "<span>{$pro_count}</span>", $c['manage']['lang_pack']['shipping']['area']['pro_items']);?></div>
														<?php if($pro_count==0){?>
															<a href="javascript:;" class="template_delete fl icon_delete_1"><i></i></a>
														<?php }?>
														<input type="hidden" name="IsDelete[<?=$v['TId'];?>]" value="0" />
													</div>
												<?php }?>
											</div>
										</div>
									</div>
									<div class="rows clean">
										<label></label>
										<div class="input"><a href="javascript:;" id="add_template" class="btn_global btn_add_item">{/global.add/}</a></div>
									</div>
									<div class="rows clean box_button">
										<div class="input_button">
											<div class="input">
												<input type="button" class="btn_global btn_submit" value="{/global.save/}">
												<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
											</div>
										</div>
									</div>
									<input type="hidden" id="template_max_number" value="0" />
									<input type="hidden" name="do_action" value="set.shipping_template_edit" />
								</form>
							</div>
						<?php } ?>
					</div>
				<?php 
				}elseif($c['manage']['page']=='edit'){
					//快递编辑
					$SId && $shipping_row=str::str_code(db::get_one('shipping', "SId='{$SId}'"));
					$used_checked=' checked';
					$hot_checked=$state_checked='';
					if($shipping_row){
						$used_checked=$shipping_row['IsUsed']==1?' checked':'';
					}
				?>
					<?=ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js');?>
					<script type="text/javascript">$(function(){set_obj.shipping_express_edit_init();});</script>
					<form id="shipping_express_edit_form" class="global_form">
						<div class="rows clean">
							<label>{/shipping.shipping.express/}</label>
							<div class="input"><input type="text" name="Express" value="<?=$shipping_row['Express'];?>" class="box_input" size="25" maxlength="100" notnull=""></div>
						</div>
						<div class="rows clean">
							<label>{/shipping.shipping.logo/}</label>
							<div class="input">
								<?=manage::multi_img('LogoDetail', 'Logo', $shipping_row['Logo']); ?>
								<div class="tips"><?=sprintf(manage::language('{/notes.pic_size_tips/}'), '160*160');?></div>
							</div>
						</div>
						<?php if(in_array('shipping_template', (array)$c['manage']['plugins']['Used'])){ ?>
							<div class="rows clean">
								<label>选择运费模板</label>
								<div class="input">
									<?php $shipping_template_row=db::get_all('shipping_template'); ?>
									<div class="box_select"><?=ly200::form_select($shipping_template_row, 'TId', $shipping_row['TId'], 'Name', 'TId', '{/global.select_index/}');?></div>
								</div>
							</div>
						<?php } ?>
						<div class="blank6"></div>
						<div class="rows hide">
							<label>{/shipping.shipping.brief/}</label>
							<div class="input"><input name="Brief" value="<?=$shipping_row['Brief'];?>" type="text" class="box_input" size="40" maxlength="100" /><span class="tool_tips_ico" content="{/shipping.shipping.brief_notes/}"></span></div>
						</div>
						<div class="rows clean">
							<label>{/shipping.shipping.query/}</label>
							<div class="input"><input name="Query" value="<?=$shipping_row['Query'];?>" type="text" class="box_input" size="40" maxlength="150" /></div>
						</div>
						<?php /*
						<div class="rows clean">
							<label>{/products.myorder/}</label>
							<div class="input">
								<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $shipping_row['MyOrder'], '', '', '', 'class="box_input"');?></div>
							</div>
						</div>
						*/?>
						<?php
						//物流接口
						$only_one_api_ary=array('DHL');//唯一使用的物流接口
						$api_row=db::get_all('shipping_api', 'SId=0', '*', 'AId asc');
						if(count($api_row)){
							//已经使用的接口数据
							$used_api_ary=array();
							$used_api_row=db::get_all('shipping', 'IsAPI>0', 'SId, IsAPI');
							foreach($used_api_row as $v){ $used_api_ary[$v['IsAPI']]=$v['SId']; }
						?>
							<div class="rows api_box">
								<label>{/shipping.shipping.is_api/}</label>
								<div class="box_explain">{/set.explain.docking/}</div>
								<div class="input">
									<span class="input_radio_box input_radio_box_1 choice_btn <?=$shipping_row['IsAPI']==0?'checked':'';?>">
										<span class="input_radio">
											<input type="radio" name="IsAPI" value="0" <?=$shipping_row['IsAPI']==0?' checked':'';?>>
										</span>{/global.no_use/}
									</span>
									<?php
									foreach($api_row as $k=>$v){
										$used_api_row=db::get_one('shipping_api', "Name='{$v['Name']}' and SId='$SId'");
										$IsOnly=in_array($v['Name'], $only_one_api_ary);
										if(!$IsOnly || ($IsOnly && ($used_api_ary[$used_api_row['AId']]==$shipping_row['SId'] || !$used_api_ary[$used_api_row['AId']]))){//正在使用的接口 或者 还没使用的接口
											$ApiName=$v['Name'];
											if($ApiName=='4PX') continue; //隐藏4px 功能 190807
											$ApiName=='4PX' && $ApiName='_4px';
											$ApiAId=$v['AId'];
											$Attribute=$v['Attribute'];
											if($used_api_row){
												$ApiAId=$used_api_row['AId'];
												$Attribute=$used_api_row['Attribute'];
											}
											?>
											<span class="input_radio_box input_radio_box_1 choice_btn <?=$shipping_row['IsAPI']==$ApiAId?'checked':'';?>" data-name="<?=$v['Name'];?>" data-api-name="<?=$ApiName;?>" data-attribute="<?=htmlspecialchars(str::json_data($Attribute));?>">
												<span class="input_radio">
													<input type="radio" name="IsAPI" value="<?=$v['AId'];?>" <?=$shipping_row['IsAPI']==$ApiAId?' checked':'';?>>
												</span><?=$v['Name'];?>
											</span>
									<?php
										}
									}?>
								</div>
							</div>
						<?php }?>
						<div id="method_shipping_box">
							<div class="rows clean">
								<label>{/shipping.shipping.weight_calculation/}</label>
								<div class="input">
									<div class="weightarea_box">
										<?php
										$weightarea_ary=array('{/shipping.shipping.first_ext_weight/}', '{/shipping.shipping.weightarea/}', '{/shipping.shipping.weightmix/}', '{/shipping.shipping.qty/}', '{/shipping.shipping.special/}'); 
										foreach((array)$weightarea_ary as $k=>$v){
										?>
											<div class="item item_<?=$k;?><?=$shipping_row['IsWeightArea']==$k?' cur':'';?>">
												<div class="tool_tips_ico" content="{/shipping.shipping.weight_area_<?=$k;?>/}"></div>
												<input type="radio" name="IsWeightArea" value="<?=$k;?>"<?=$shipping_row['IsWeightArea']==$k?' checked':'';?> />
												<div class="name"><?=$v;?></div>
											</div>
										<?php }?>
									</div>
								</div>
							</div>
							<div class="rows clean" id="WeightBetween">
								<label>{/shipping.shipping.limit/}</label>
								<div class="input">
									<span class="unit_input"><input type="text" name="MinWeight" id="MinWeight" value="<?=(float)$shipping_row['MinWeight'];?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span>&nbsp;&nbsp;~&nbsp;&nbsp;
									<span class="box_unlimited">∞</span>
									<span class="box_max">
										<span class="unit_input"><input type="text" name="MaxWeight" id="MaxWeight" value="<?=(float)$shipping_row['MaxWeight'];?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span>
									</span>
									<span class="tool_tips_ico" content="{/shipping.shipping.limit_notes/}"></span>
								</div>
							</div>
							<div class="rows clean" id="VolumeBetween">
								<label>{/shipping.shipping.volume_limit/}</label>
								<div class="input">
									<span class="unit_input"><input type="text" name="MinVolume" id="MinVolume" value="<?=(float)$shipping_row['MinVolume'];?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.volume_unit/}</b></span>&nbsp;&nbsp;~&nbsp;&nbsp;
									<span class="box_unlimited">∞</span>
									<span class="tool_tips_ico" content="{/shipping.shipping.volume_limit_notes/}"></span>
								</div>
							</div>
							<div class="rows clean" id="ExtWeight">
								<label>{/shipping.shipping.first_weight/}/{/shipping.shipping.ext_weight/}</label>
								<div class="input">
									<span class="unit_input"><b>{/shipping.shipping.first_weight/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstWeight" id="FirstWeight" value="<?=$shipping_row['FirstWeight'];?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span>&nbsp;&nbsp;
									<span class="unit_input"><b>{/shipping.shipping.ext_weight/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtWeight" value="<?=$shipping_row['ExtWeight']?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span>&nbsp;&nbsp;
									<span id="StartWeight_span" style="display:<?=$shipping_row['IsWeightArea']==2?'':'none';?>;">
										<span class="unit_input"><b>{/shipping.shipping.startweight/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="StartWeight" value="<?=$shipping_row['StartWeight']?>" class="box_input" size="4" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span>
									</span>
								</div>
							</div>
							<div class="rows clean" id="ExtWeightArea">
								<label>{/shipping.shipping.ext_weightarea/}</label>
								<div class="input">
									<div id="ExtWeightrow">
										<?php
										$ExtWeightArea=str::json_data(htmlspecialchars_decode($shipping_row['ExtWeightArea']), 'decode');
										!$ExtWeightArea && $ExtWeightArea=array(0);//默认一个0
										foreach($ExtWeightArea as $k=>$v){
										?>
										<div class="row">
											<span class="unit_input"><input type="text" name="ExtWeightArea[]" value="<?=$v?>" class="box_input" size="6" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span><?=$k?'<a class="d_del icon_delete_1" href="javascript:;"><i></i></a>':'';?>
										</div>
										<?php }?>
									</div>
									<div class="blank6"></div>
									<a href="javascript:;" id="addExtWeight" data-unit="{/shipping.shipping.unit/}" class="btn_global btn_add_item">{/global.add/}{/shipping.shipping.node/}</a>
									<div class="blank6"></div>
								</div>
							</div>
							<div class="rows clean" id="WeightArea">
								<label>{/shipping.shipping.weightarea/}</label>
								<div class="input">
									{/shipping.shipping.weightarea_type.0/}
									<div class="switchery<?=$shipping_row['WeightType']?' checked':'';?>">
										<input type="checkbox" name="WeightType" value="1"<?=$shipping_row['WeightType']?' checked':'';?>>
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
									<div class="blank6"></div>
									<div id="Weightrow">
										<?php
										$WeightArea=str::json_data(htmlspecialchars_decode($shipping_row['WeightArea']), 'decode');
										!$WeightArea && $WeightArea=array(0);//默认一个0
										foreach($WeightArea as $k=>$v){
											$readonly=$k==0?true:false;
										?>
										<div class="row">
											<span class="unit_input"><input type="text" name="WeightArea[]" value="<?=$v?>" <?=$readonly?'readonly="readonly"':'';?> class="box_input <?=$readonly?'readonly':'';?>" size="6" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.unit/}</b></span><?=!$readonly?'<a class="d_del icon_delete_1" href="javascript:;"><i></i></a>':'';?>
										</div>
										<?php }?>
									</div>
									<div class="blank6"></div>
									<a href="javascript:;" id="addWeight" data-unit="{/shipping.shipping.unit/}" class="btn_global btn_add_item">{/global.add/}{/shipping.shipping.node/}</a>
									<div class="blank6"></div>
								</div>
							</div>
							<div class="rows clean" id="Quantity">
								<label>{/shipping.shipping.qty/}</label>
								<div class="input">
									<span class="unit_input"><b>{/shipping.shipping.first_qty_0/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstMinQty" id="FirstMinQty" value="<?=$shipping_row['FirstMinQty'];?>" class="box_input" size="4" maxlength="10" rel="amount" /></span>&nbsp;&nbsp;
									<span class="unit_input"><b>{/shipping.shipping.first_qty_1/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstMaxQty" id="FirstMaxQty" value="<?=$shipping_row['FirstMaxQty'];?>" class="box_input" size="4" maxlength="10" rel="amount" /></span>&nbsp;&nbsp;
									<span class="unit_input"><b>{/shipping.shipping.ext_qty/}<div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtQty" id="ExtQty" value="<?=$shipping_row['ExtQty'];?>" class="box_input" size="4" maxlength="10" rel="amount" /></span>
								</div>
							</div>
							<div class="rows clean" id="VolumeArea">
								<label>{/shipping.shipping.volumearea/}</label>
								<div class="input">
									<div class="blank6"></div>
									<div id="Volumerow">
										<?php
										$VolumeArea=str::json_data(htmlspecialchars_decode($shipping_row['VolumeArea']), 'decode');
										!$VolumeArea && $VolumeArea=array(0);//默认一个0
										foreach($VolumeArea as $k=>$v){
											$readonly=$k==0?true:false;
										?>
										<div class="row">
											<span class="unit_input"><input type="text" name="VolumeArea[]" value="<?=$v?>" <?=$readonly?'readonly="readonly"':'';?> class="box_input <?=$readonly?'readonly':'';?>" size="6" maxlength="10" rel="amount" /><b class="last">{/shipping.shipping.volume_unit/}</b></span><?=!$readonly?'<a class="d_del icon_delete_1" href="javascript:;"><i></i></a>':'';?>
										</div>
										<?php }?>
									</div>
									<div class="blank6"></div>
									<a href="javascript:;" id="addVolume" data-unit="{/shipping.shipping.volume_unit/}" class="btn_global btn_add_item">{/global.add/}{/shipping.shipping.node/}</a>
									<div class="blank6"></div>
								</div>
							</div>
						</div>
						<div id="method_api_box" <?=$shipping_row['IsAPI']==0?'style="display:none;"':'';?>>
							<div class="rows clean">
								<label>{/orders.account_info/}</label><div class="input"></div>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input input_button">
								<input type="button" class="btn_global btn_submit" value="{/global.submit/}" />
								<a href="./?m=set&a=shipping&d=express"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
							</div>
						</div>
						<input type="hidden" name="IsUsed" value="<?=$shipping_row['IsUsed'];?>" />
						<input type="hidden" name="SId" value="<?=$SId;?>" />
						<input type="hidden" name="do_action" value="set.shipping_express_edit" />
					</form>
				<?php 
				}elseif($c['manage']['page']=='area'){
					//分区设置
					$shipping_row=str::str_code(db::get_one('shipping', "SId='{$SId}'", 'Express, Logo'));
					!$shipping_row && js::location('./?m=set&a=shipping.express');
					$area_row=str::str_code(db::get_all('shipping_area', "SId='{$SId}' and OvId='{$OvId}'", '*', 'AId asc'));//查询属于此快递的区域
					$shipping_overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));//海外仓
				?>
				 <script type="text/javascript">$(function(){set_obj.shipping_express_area_init();});</script>
				 <div class="shipping_area" SId="<?=$SId;?>">
				 	<div class="blank20"></div>
					<div class="box_multi_tab">
						<div class="multi_tab_row clean"<?=(int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0?' style="display:none;"':'';?>>
							<?php foreach($shipping_overseas_row as $k=>$v){?>
								<a href="javascript:;" class="btn_multi_tab fl<?=$k==0?' current':'';?>" data-id="<?=$v['OvId'];?>" data-url="./?m=set&a=shipping&d=express&p=area&iframe=1&SId=<?=$SId;?>&OvId=<?=$v['OvId'];?>"><?=$v['Name'.$c['manage']['web_lang']];?></a>
							<?php }?>
							<a href="javascript:;" class="add fl">{/global.management/}</a>
						</div>
					</div>
					<div class="clear"></div>
					<div class="shipping_area_list">
						<div class="config_table_body">
							<?php
							foreach($area_row as $k=>$v){
								$Brief=trim($v['Brief']);
							?>
								<div class="table_item" aid="<?=$v['AId'];?>">
									<table border="0" cellpadding="5" cellspacing="0" class="config_table">
										<tbody>
											<tr>
												<td width="80%">
													<div class="name"><?=$v['Name'];?></div><?=$Brief?'<div class="desc">'.$Brief.'</div>':'';?>
												</td>
												<td width="20%" nowrap="nowrap" align="right">
													<?php if($permit_ary['edit']){?><a class="menu_view edit" href="javascript:;" data-url="./?m=set&a=shipping&d=express&p=area_edit&SId=<?=$SId;?>&OvId=<?=$OvId;?>&AId=<?=$v['AId'];?>">{/global.edit/}</a><?php }?>
													<?php if($permit_ary['del']){?><a class="menu_view del" href="./?do_action=set.shipping_area_del&AId=<?=$v['AId'];?>">{/global.del/}</a><?php }?>
												</td>					
											</tr>
										</tbody>
									</table>
								</div>
							<?php }?>
						</div>
						<?php if($permit_ary['add']){?><a class="add set_add" href="javascript:;" data-url="./?m=set&a=shipping&d=express&p=area_edit&SId=<?=$SId?>&OvId=<?=$OvId;?>">{/shipping.area.area_add/}</a><?php }?>
					</div>
					<div id="fixed_right">
						<div class="global_container shipping_area_edit_box">
							<div class="edit_form shipping_area_edit"></div>
						</div>
						<div class="global_container shipping_overseas_edit">
							<?php
							$shipping_overseas_row=db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc');
							?>
							<form id="shipping_overseas_edit_form" class="global_form">
								<div class="top_title">{/shipping.area.manages_ships_from/}<a href="javascript:;" class="close"></a></div>
								<div class="edit_overseas_list">
									<div class="rows">
										<label>{/global.name/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
										<div class="input">
											<?php
											foreach($c['manage']['config']['Language'] as $k=>$lang){
											?>
												<div class="tab_txt tab_txt_<?=$lang;?>" lang="<?=$lang;?>"<?=($c['manage']['config']['LanguageDefault']==$lang?' style="display:block;"':'');?>>
													<?php
													foreach($shipping_overseas_row as $v){
														$pro_count=db::get_row_count('products_selected_attribute', "OvId='{$v['OvId']}' and IsUsed=1 group by ProId", 'SeleteId');
													?>
														<div class="item clean" data-id="<?=$v['OvId'];?>">
															<input type="text" class="box_input fl" name="Name[<?=$v['OvId'];?>][<?=$lang;?>]" value="<?=$v["Name_{$lang}"];?>" size="30" maxlength="255" autocomplete="off" notnull />
															<div class="product_count fl"><?=str_replace('%num%', "<span>{$pro_count}</span>", $c['manage']['lang_pack']['shipping']['area']['pro_items']);?></div>
															<?php if($pro_count==0){?>
																<a href="javascript:;" class="overseas_delete fl icon_delete_1"><i></i></a>
															<?php }?>
															<?php if($k==0){?>
																<input type="hidden" name="IsDelete[<?=$v['OvId'];?>]" value="0" />
															<?php }?>
														</div>
													<?php }?>
												</div>
											<?php }?>
										</div>
									</div>
								</div>
								<div class="rows clean">
									<label></label>
									<div class="input"><a href="javascript:;" id="add_overseas" class="btn_global btn_add_item">{/global.add/}</a></div>
								</div>
								<div class="rows clean box_button">
									<div class="input_button">
										<div class="input">
											<input type="button" class="btn_global btn_submit" value="{/global.save/}">
											<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
										</div>
									</div>
								</div>
								<input type="hidden" id="overseas_max_number" value="0" />
								<input type="hidden" name="do_action" value="set.shipping_overseas_edit" />
							</form>
						</div>
					</div>
				</div>
				<?php
				}elseif($c['manage']['page']=='area_edit'){
					//分区编辑
					$shipping_row=str::str_code(db::get_one('shipping', "SId='{$SId}'"));//所属快递公司
					!$shipping_row && js::location('./?m=set&a=shipping&d=express');
					$AId && $area_row=str::str_code(db::get_one('shipping_area', "AId='{$AId}'"));
					$overseas_row=str::str_code(db::get_one('shipping_overseas', "OvId='$OvId'"));//海外仓
					?>
					<?php /* 20190507
					<div class="shipping_area_edit">
						<form id="shipping_area_edit_form" class="global_form">
							<div class="top_title"><?=$AId?'{/global.edit/}':'{/global.add/}';?>{/shipping.shipping.area/} <a href="javascript:;" class="close"></a></div>
							<div class="rows clean">
								<span class="tit">{/shipping.shipping.express/}:</span>
								<span class="desc"><?=$shipping_row['Express'];?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])){ ?>
									<span class="tit">{/shipping.area.ships_from/}:</span>
									<span class="desc"><?=$overseas_row['Name'.$c['manage']['web_lang']];?></span>
								<?php } ?>
							</div>
							<div class="rows clean">
								<label>{/shipping.area.area/}</label>
								<div class="input"><input type="text" name="Name" value="<?=$area_row['Name'];?>" class="box_input" size="25" maxlength="100" notnull=""></div>
							</div>
							<div class="rows clean">
								<label>{/shipping.shipping.brief/}</label>
								<div class="input">
									<textarea name="Brief" class="box_textarea"><?=$area_row['Brief'];?></textarea>
								</div>
							</div>
							<div class="box_setting">
								<div class="box_multi_tab clean">
									<div class="multi_tab_row clean">
										<a href="javascript:;" class="btn_multi_tab fl current" data-type="country">{/shipping.area.setcountry_to/}</a>
										<a href="javascript:;" class="btn_multi_tab fl" data-type="shipping">{/shipping.area.set_freight/}</a>
									</div>
								</div>
								<div class="box_setting_item" data-type="country" style="display:block;">
									<?php
									$country_row=str::str_code(db::get_all('country', 'IsUsed=1', 'CId, Country, Continent', 'Country asc')); //所有国家
									$express_country_row=str::str_code(db::get_all('shipping_country', "SId='$SId' and AId in(select AId from shipping_area where SId='{$SId}' and OvId='{$OvId}')", '*', 'CId asc')); //从快递公司的国家中筛选出当前分区的国家
									$range_ary=range('A', 'Z');
									$all_country_ary=$continent_country_ary=$area_country_ary=$added_CId=$area_CId=array();
									foreach($country_row as $k=>$v){
										$initial=mb_strcut($v['Country'], 0, 1);//首字母
										$all_country_ary[$initial][]=array('CId'=>$v['CId'], 'Country'=>$v['Country'], 'Continent'=>$v['Continent']); //用一个新数组记录
										$continent_country_ary[$v['Continent']][]=array('CId'=>$v['CId'], 'Country'=>$v['Country'], 'Continent'=>$v['Continent']); //用一个新数组记录
									}
									foreach($express_country_row as $k=>$v){
										$added_CId[]=$v['CId'];
										if($v['AId']==$area_row['AId']){
											foreach($country_row as $k2=>$v2){
												if($v['CId']==$v2['CId']){ //找到对应的国家
													$area_CId[]=$v['CId'];
													$area_country_ary[]=array('CId'=>$v2['CId'], 'Country'=>$v2['Country'], 'Continent'=>$v['Continent']); //用一个新数组记录
													continue;
												}
											}
										}
									}
									?>
									<div class="country_area">
										<?php foreach((array)$continent_country_ary as $k=>$v){ ?>
											<div class="continent_area">
												<div class="continent" continent="<?=$k;?>">
													<span class="input_checkbox_box checked">
														<span class="input_checkbox">
															<input type="checkbox" name="OrdersSmsStatus[]" value="0" checked />
														</span>{/continent.<?=$k;?>/}
													</span>
													<a href="javascript:;" class="down"></a>
												</div>
												<div class="country_item">
													<?php foreach((array)$v as $k1=>$v1){
														$disabled=0; 
														$save= in_array($v1['CId'], $area_CId) ? 1 : 0;
														if(in_array($v1['CId'], $added_CId) && $save==0) $disabled=1;
														?>
														<span class="input_checkbox_box <?=$save ? 'checked' : ''; ?> <?=$disabled ? 'disabled' : ''; ?>">
															<span class="input_checkbox">
																<?php if(!$disabled){ ?>
																	<input type="checkbox" initial="<?=$k;?>" name="CId[]" value="<?=$v1['CId'];?>" <?=$save ? 'checked' : ''; ?> />
																<?php } ?>
															</span><?=$v1['Country'];?>
														</span>
													<?php } ?>
													<div class="clear"></div>
												</div>
											</div>
										<?php } ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="box_setting_item" data-type="shipping">
									<div class="rows clean">
										<label>{/shipping.area.freeshipping/}</label>
										<div class="input visible">
											<div class="free">									
												<div class="switchery<?=$area_row['IsFreeShipping']?' checked':'';?>">
													<input type="checkbox" name="IsFreeShipping" value="1"<?=$area_row['IsFreeShipping']?' checked':'';?>>
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>{/global.used/}&nbsp;
											</div>
											<?php
											$open=0;
											if($area_row['FreeShippingWeight']>0) $open=1;
											?>
											<span class="unit_input" style="display:<?=$area_row['IsFreeShipping']?'inline-block':'none';?>;">
												<input type="text" name="FreeShippingPrice" value="<?=sprintf('%01.2f', $area_row['FreeShippingPrice']);?>" class="box_input<?=$open==0?'':' hide';?>" size="6" maxlength="10" notnull=""<?=$open==0?'':' disabled';?> />
												<input type="text" name="FreeShippingWeight" value="<?=sprintf('%01.3f', $area_row['FreeShippingWeight']);?>" class="box_input<?=$open==1?'':' hide';?>" size="6" maxlength="10" notnull=""<?=$open==1?'':' disabled';?> />
												<b class="last box_select_down">
													<span class="head"><span><?=$open==0?$c['manage']['currency_symbol']:'{/shipping.shipping.unit/}';?></span><em></em></span>
													<ul class="list">
														<li><?=$c['manage']['currency_symbol'];?></li>
														<li>{/shipping.shipping.unit/}</li>
													</ul>
												</b>
											</span>
										</div>
									</div>
									<div class="rows clean">
										<label>{/shipping.area.additional/}</label>
										<div class="input">
											<span class="unit_input"><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input type="text" name="AffixPrice" value="<?=sprintf('%01.2f', $area_row['AffixPrice']);?>" class="box_input" size="6" maxlength="10" notnull="" /></span>
										</div>
									</div>
									<div class="rows clean">
										<?php
										$column='';
										if(!$shipping_row['IsAPI']){
											switch($shipping_row['IsWeightArea']){
												case 0: $column=' {/shipping.shipping.first_weight/}/{/shipping.shipping.ext_weight/}'; break;
												case 1: $column=' {/shipping.shipping.weightarea/}'; break;
												case 2: $column=' {/shipping.shipping.weightmix/}'; break;
												case 3: $column=' {/shipping.shipping.qty/}'; break;
												case 4: $column=' {/shipping.shipping.special/}'; break;
											}
										}?>
										<label>{/shipping.shipping.calculation/}:<?=$column;?></label>
										<?php
										if($shipping_row['IsWeightArea']==0 || $shipping_row['IsWeightArea']==2){
											//首重或混合计算
										?>	
											<div class="input">
												<span class="unit_input"><b>{/shipping.shipping.first_weight/}(<?=$shipping_row['FirstWeight'];?> {/shipping.shipping.unit/}) <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstPrice" value="<?=sprintf('%01.3f', $area_row['FirstPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /></span>
											</div>
										<?php }?>
									</div>
									<?php
									if($shipping_row['IsWeightArea']==3){
										//按数量计算
									?>
										<div class="rows clean">
											<label></label>
											<div class="input">
												<span class="unit_input"><b>{/shipping.shipping.first_weight/}(<?=$shipping_row['FirstMinQty'];?>-<?=$shipping_row['FirstMaxQty'];?>{/global.item/}) <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstQtyPrice" value="<?=sprintf('%01.3f', $area_row['FirstQtyPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /></span>
												<span class="unit_input"><b>{/shipping.shipping.ext_weight/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtQtyPrice" value="<?=sprintf('%01.3f', $area_row['ExtQtyPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /><b class="last">/ <?=$shipping_row['ExtQty'];?>{/global.item/}</b></span>
											</div>
										</div>
									<?php }?>
									<?php 
									if ($shipping_row['IsWeightArea'] == 0 || $shipping_row['IsWeightArea'] == 2) {
										//续重区间
										$ExtWeightArea = str::json_data(htmlspecialchars_decode($shipping_row['ExtWeightArea']), 'decode');
										$ExtWeightAreaPrice = str::json_data(htmlspecialchars_decode($area_row['ExtWeightAreaPrice']), 'decode');
										?>
										<div class="rows clean">
											<label>{/shipping.shipping.ext_weightarea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ($ExtWeightArea as $k => $v) {
														if ((float)$ExtWeightArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v;?> - <?=(float)$ExtWeightArea[$k+1];?> {/shipping.shipping.unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtWeightAreaPrice[]" value="<?=sprintf('%01.3f', $ExtWeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">/ <?=sprintf('%01.3f', $shipping_row['ExtWeight']);?> {/shipping.shipping.unit/}</b></span><div class=""></div></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=@count($ExtWeightArea) ? $v : $shipping_row['FirstWeight'];?> {/shipping.shipping.unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtWeightAreaPrice[]" value="<?=sprintf('%01.3f', $ExtWeightAreaPrice[(@count($ExtWeightArea)?$k:0)]);?>" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">/ <?=sprintf('%01.3f', $shipping_row['ExtWeight']);?> {/shipping.shipping.unit/}</b></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
									<?php 
									if ($shipping_row['IsWeightArea'] == 1 || $shipping_row['IsWeightArea'] == 2 || $shipping_row['IsWeightArea'] == 4) {
										//重量区间
										$WeightArea = str::json_data(htmlspecialchars_decode($shipping_row['WeightArea']), 'decode');
										$WeightAreaPrice = str::json_data(htmlspecialchars_decode($area_row['WeightAreaPrice']), 'decode');
										?>
										<div class="rows clean">
											<label>{/shipping.shipping.weightarea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ($WeightArea as $k => $v) {
														if ((float)$WeightArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v;?> - <?=(float)$WeightArea[$k+1];?> {/shipping.shipping.unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="WeightAreaPrice[]" value="<?=sprintf('%01.3f', $WeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><?=$shipping_row['WeightType']==1 ? '<b class="last">/ {/shipping.shipping.unit/}</b>' : '';?></span></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=$v;?> {/shipping.shipping.unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="WeightAreaPrice[]" value="<?=sprintf('%01.3f', $WeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><?=$shipping_row['WeightType']==1 ? '<b class="last">/ {/shipping.shipping.unit/}</b>' : '';?></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
									<?php
									if ($shipping_row['IsWeightArea'] == 4) {
										//体积区间
										$VolumeArea = str::json_data(htmlspecialchars_decode($shipping_row['VolumeArea']), 'decode');
										$VolumeAreaPrice = str::json_data(htmlspecialchars_decode($area_row['VolumeAreaPrice']), 'decode');
									?>
										<div class="rows clean">
											<label>{/shipping.shipping.volumearea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ((array)$VolumeArea as $k => $v) {
														if ((float)$VolumeArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v?> - <?=(float)$VolumeArea[$k+1]?> {/shipping.shipping.volume_unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="VolumeAreaPrice[]" value="<?=sprintf('%01.2f', $VolumeAreaPrice[$k]);?>" class="box_input" size="6" maxlength="6" rel="amount"><b class="last">/ {/shipping.shipping.volume_unit/}</b></span></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=$v?> {/shipping.shipping.volume_unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="VolumeAreaPrice[]" value="<?=sprintf('%01.2f', $VolumeAreaPrice[$k]);?>" class="box_input" size="6" maxlength="6" rel="amount"><b class="last">/ {/shipping.shipping.volume_unit/}</b></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
								</div>
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.save/}">
									<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
								</div>
							</div>
							<input type="hidden" name="AId" value="<?=$AId;?>" />
							<input type="hidden" name="SId" value="<?=$SId;?>" />
							<input type="hidden" name="OvId" value="<?=$OvId;?>" />
							<input type="hidden" name="do_action" value="set.shipping_express_area_edit" />
						</form>
					</div>*/ ?>
					<div class="shipping_area_edit">
						<form id="shipping_area_edit_form" class="global_form">
							<div class="top_title"><?=$AId?'{/global.edit/}':'{/global.add/}';?>{/shipping.shipping.area/} <a href="javascript:;" class="close"></a></div>
							<div class="rows clean">
								<span class="tit">{/shipping.shipping.express/}:</span>
								<span class="desc"><?=$shipping_row['Express'];?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])){ ?>
									<span class="tit">{/shipping.area.ships_from/}:</span>
									<span class="desc"><?=$overseas_row['Name'.$c['manage']['web_lang']];?></span>
								<?php } ?>
							</div>
							<div class="rows clean">
								<label>{/shipping.area.area/}</label>
								<div class="input"><input type="text" name="Name" value="<?=$area_row['Name'];?>" class="box_input" size="25" maxlength="100" notnull=""></div>
							</div>
							<div class="rows clean">
								<label>{/shipping.shipping.brief/}</label>
								<div class="input">
									<textarea name="Brief" class="box_textarea"><?=$area_row['Brief'];?></textarea>
								</div>
							</div>
							<div class="box_setting">
								<div class="box_multi_tab clean">
									<div class="multi_tab_row clean">
										<a href="javascript:;" class="btn_multi_tab fl current" data-type="country">{/shipping.area.setcountry_to/}</a>
										<a href="javascript:;" class="btn_multi_tab fl" data-type="shipping">{/shipping.area.set_freight/}</a>
									</div>
								</div>
								<div class="box_setting_item" data-type="country" style="display:block;">
									<?php
									$country_row=str::str_code(db::get_all('country', 'IsUsed=1', 'CId, Country, Continent', 'Country asc')); //所有国家
									$express_country_row=str::str_code(db::get_all('shipping_country', "SId='$SId' and AId in(select AId from shipping_area where SId='{$SId}' and OvId='{$OvId}')", '*', 'CId asc')); //从快递公司的国家中筛选出当前分区的国家
									$range_ary=range('A', 'Z');
									$all_country_ary=$continent_country_ary=$added_CId=$area_CId=$states_ary=$cid_states=array();
									foreach($country_row as $k=>$v){
										$initial=mb_strcut($v['Country'], 0, 1);//首字母
										$data=array('CId'=>$v['CId'], 'Country'=>$v['Country'], 'Continent'=>$v['Continent']);
										$all_country_ary[$initial][]=$data; //用一个新数组记录
										$continent_country_ary[$v['Continent']][]=$data; //用一个新数组记录
										if(in_array('provincial_freight', (array)$c['manage']['plugins']['Used']) && $v['CId']==226 && db::get_row_count('country_states', "CId={$v['CId']}")){ //开启省份运费
											$states_ary[]=$v['CId'];
											$country_states_ary[$v['CId']]=$data;

										}
									}
									foreach($express_country_row as $k=>$v){
										$added_CId[]=$v['CId'];
										$cid_states_row=@explode('|', trim($v['states'], '|'));
										foreach((array)$cid_states_row as $v1){
											$cid_states[$v['CId']][]=$v1;
										}
										if($v['AId']==$area_row['AId']){
											$cid_aid_states[$v['CId']]=$cid_states_row;
											foreach($country_row as $k2=>$v2){
												if($v['CId']==$v2['CId']){ //找到对应的国家
													$area_CId[]=$v['CId'];
													continue;
												}
											}
										}
									}
									?>
									<div class="country_area">
										<?php foreach((array)$continent_country_ary as $k=>$v){ ?>
											<div class="continent_area">
												<div class="continent" continent="<?=$k;?>">
													<span class="input_checkbox_box checked">
														<span class="input_checkbox">
															<input type="checkbox" name="OrdersSmsStatus[]" value="0" checked />
														</span>{/continent.<?=$k;?>/}
													</span>
													<a href="javascript:;" class="down"></a>
												</div>
												<div class="country_item">
													<?php foreach((array)$v as $k1=>$v1){
														$disabled=0;
														if(in_array($v1['CId'], (array)$states_ary)){ continue;}
														$save= in_array($v1['CId'], (array)$area_CId) ? 1 : 0;
														if(in_array($v1['CId'], $added_CId) && $save==0) $disabled=1;
														?>
														<span class="input_checkbox_box <?=$save ? 'checked' : ''; ?> <?=$disabled ? 'disabled' : ''; ?>">
															<span class="input_checkbox">
																<?php if(!$disabled){ ?>
																	<input type="checkbox" initial="<?=$k;?>" name="CId[]" value="<?=$v1['CId'];?>" <?=$save ? 'checked' : ''; ?> />
																<?php } ?>
															</span><?=$v1['Country'];?>
														</span>
													<?php } ?>
													<div class="clear"></div>
												</div>
											</div>
										<?php } ?>

										<?php /************************ 省份分区设置 ************************/ ?>
										<?php /*
										<div class="continent_area">
											<div class="continent">
												<span class="input_checkbox_box">
													<span class="input_checkbox">
													</span>有省份的国家
												</span>
												<a href="javascript:;" class="down"></a>
											</div>
											<div class="country_item">
											</div>
										</div>*/ ?>
										<?php foreach((array)$states_ary as $key=>$val){
											$states_row=db::get_all('country_states', "CId={$val}");
											$v=$country_states_ary[$val];													
											?>
											<div class="continent_area">
												<div class="continent">
													<span class="input_checkbox_box checked">
														<span class="input_checkbox">
														</span><?=$v['Country'];?>
													</span>
													<a href="javascript:;" class="down"></a>
												</div>
												<div class="country_item">
													<?php foreach((array)$states_row as $k1=>$v1){ 
														$save=(@in_array($v1['SId'], (array)$cid_aid_states[$v['CId']]) && $AId) ? 1 : 0;
														if(@in_array($v1['SId'], (array)$cid_states[$v['CId']]) && !$save) continue;
														?>
														<span class="input_checkbox_box <?=$save ? 'checked' : ''; ?>">
															<span class="input_checkbox">
																<input type="checkbox" initial="<?=$k;?>" name="CountrySId[<?=$v['CId'];?>][]" value="<?=$v1['SId'];?>" <?=$save ? 'checked' : ''; ?> />
															</span><?=$v1['States']; ?>
														</span>
													<?php } ?>
													<div class="clear"></div>
												</div>
											</div>
										<?php } ?>
										<div class="clear"></div>

										<?php /************************ 省份分区设置 ************************/ ?>

									</div>
									<div class="clear"></div>
								</div>
								<div class="box_setting_item" data-type="shipping">
									<div class="rows clean">
										<label>{/shipping.area.freeshipping/}</label>
										<div class="input visible">
											<div class="free">									
												<div class="switchery<?=$area_row['IsFreeShipping']?' checked':'';?>">
													<input type="checkbox" name="IsFreeShipping" value="1"<?=$area_row['IsFreeShipping']?' checked':'';?>>
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>{/global.used/}&nbsp;
											</div>
											<?php
											$open=0;
											if($area_row['FreeShippingWeight']>0) $open=1;
											?>
											<span class="unit_input" style="display:<?=$area_row['IsFreeShipping']?'inline-block':'none';?>;">
												<input type="text" name="FreeShippingPrice" value="<?=sprintf('%01.2f', $area_row['FreeShippingPrice']);?>" class="box_input<?=$open==0?'':' hide';?>" size="6" maxlength="10" notnull=""<?=$open==0?'':' disabled';?> />
												<input type="text" name="FreeShippingWeight" value="<?=sprintf('%01.3f', $area_row['FreeShippingWeight']);?>" class="box_input<?=$open==1?'':' hide';?>" size="6" maxlength="10" notnull=""<?=$open==1?'':' disabled';?> />
												<b class="last box_select_down">
													<span class="head"><span><?=$open==0?$c['manage']['currency_symbol']:'{/shipping.shipping.unit/}';?></span><em></em></span>
													<ul class="list">
														<li><?=$c['manage']['currency_symbol'];?></li>
														<li>{/shipping.shipping.unit/}</li>
													</ul>
												</b>
											</span>
										</div>
									</div>
									<div class="rows clean">
										<label>{/shipping.area.additional/}</label>
										<div class="input">
											<span class="unit_input"><b><?=$c['manage']['currency_symbol']?><div class="arrow"><em></em><i></i></div></b><input type="text" name="AffixPrice" value="<?=sprintf('%01.2f', $area_row['AffixPrice']);?>" class="box_input" size="6" maxlength="10" notnull="" /></span>
										</div>
									</div>
									<div class="rows clean">
										<label>清关费百分比</label>
										<div class="input">
											<span class="unit_input">
											<input type="text" name="AffixPrice_rate" value="<?=empty($area_row['AffixPrice_rate'])?0:($area_row['AffixPrice_rate']*100);?>" class="box_input" size="6" maxlength="10" notnull="" pattern="^(\d{1,2}(\.\d+)?|100|NA)"/>
											<b>%<div class="arrow"><em></em><i></i></div></b></span>
										</div>
									</div>
									<div class="rows clean">
										<?php
										$column='';
										if(!$shipping_row['IsAPI']){
											switch($shipping_row['IsWeightArea']){
												case 0: $column=' {/shipping.shipping.first_weight/}/{/shipping.shipping.ext_weight/}'; break;
												case 1: $column=' {/shipping.shipping.weightarea/}'; break;
												case 2: $column=' {/shipping.shipping.weightmix/}'; break;
												case 3: $column=' {/shipping.shipping.qty/}'; break;
												case 4: $column=' {/shipping.shipping.special/}'; break;
											}
										}?>
										<label>{/shipping.shipping.calculation/}:<?=$column;?></label>
										<?php
										if($shipping_row['IsWeightArea']==0 || $shipping_row['IsWeightArea']==2){
											//首重或混合计算
										?>	
											<div class="input">
												<span class="unit_input"><b>{/shipping.shipping.first_weight/}(<?=$shipping_row['FirstWeight'];?> {/shipping.shipping.unit/}) <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstPrice" value="<?=sprintf('%01.3f', $area_row['FirstPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /></span>
											</div>
										<?php }?>
									</div>
									<?php
									if($shipping_row['IsWeightArea']==3){
										//按数量计算
									?>
										<div class="rows clean">
											<label></label>
											<div class="input">
												<span class="unit_input"><b>{/shipping.shipping.first_weight/}(<?=$shipping_row['FirstMinQty'];?>-<?=$shipping_row['FirstMaxQty'];?>{/global.item/}) <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="FirstQtyPrice" value="<?=sprintf('%01.3f', $area_row['FirstQtyPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /></span>
												<span class="unit_input"><b>{/shipping.shipping.ext_weight/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtQtyPrice" value="<?=sprintf('%01.3f', $area_row['ExtQtyPrice']);?>" class="box_input" size="6" maxlength="10" rel="amount" /><b class="last">/ <?=$shipping_row['ExtQty'];?>{/global.item/}</b></span>
											</div>
										</div>
									<?php }?>
									<?php 
									if ($shipping_row['IsWeightArea'] == 0 || $shipping_row['IsWeightArea'] == 2) {
										//续重区间
										$ExtWeightArea = str::json_data(htmlspecialchars_decode($shipping_row['ExtWeightArea']), 'decode');
										$ExtWeightAreaPrice = str::json_data(htmlspecialchars_decode($area_row['ExtWeightAreaPrice']), 'decode');
										?>
										<div class="rows clean">
											<label>{/shipping.shipping.ext_weightarea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ($ExtWeightArea as $k => $v) {
														if ((float)$ExtWeightArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v;?> - <?=(float)$ExtWeightArea[$k+1];?> {/shipping.shipping.unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtWeightAreaPrice[]" value="<?=sprintf('%01.3f', $ExtWeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">/ <?=sprintf('%01.3f', $shipping_row['ExtWeight']);?> {/shipping.shipping.unit/}</b></span><div class=""></div></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=@count($ExtWeightArea) ? $v : $shipping_row['FirstWeight'];?> {/shipping.shipping.unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ExtWeightAreaPrice[]" value="<?=sprintf('%01.3f', $ExtWeightAreaPrice[(@count($ExtWeightArea)?$k:0)]);?>" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">/ <?=sprintf('%01.3f', $shipping_row['ExtWeight']);?> {/shipping.shipping.unit/}</b></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
									<?php 
									if ($shipping_row['IsWeightArea'] == 1 || $shipping_row['IsWeightArea'] == 2 || $shipping_row['IsWeightArea'] == 4) {
										//重量区间
										$WeightArea = str::json_data(htmlspecialchars_decode($shipping_row['WeightArea']), 'decode');
										$WeightAreaPrice = str::json_data(htmlspecialchars_decode($area_row['WeightAreaPrice']), 'decode');
										?>
										<div class="rows clean">
											<label>{/shipping.shipping.weightarea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ($WeightArea as $k => $v) {
														if ((float)$WeightArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v;?> - <?=(float)$WeightArea[$k+1];?> {/shipping.shipping.unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="WeightAreaPrice[]" value="<?=sprintf('%01.3f', $WeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><?=$shipping_row['WeightType']==1 ? '<b class="last">/ {/shipping.shipping.unit/}</b>' : '';?></span></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=$v;?> {/shipping.shipping.unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="WeightAreaPrice[]" value="<?=sprintf('%01.3f', $WeightAreaPrice[$k]);?>" class="box_input" size="6" maxlength="10" rel="amount"><?=$shipping_row['WeightType']==1 ? '<b class="last">/ {/shipping.shipping.unit/}</b>' : '';?></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
									<?php
									if ($shipping_row['IsWeightArea'] == 4) {
										//体积区间
										$VolumeArea = str::json_data(htmlspecialchars_decode($shipping_row['VolumeArea']), 'decode');
										$VolumeAreaPrice = str::json_data(htmlspecialchars_decode($area_row['VolumeAreaPrice']), 'decode');
									?>
										<div class="rows clean">
											<label>{/shipping.shipping.volumearea/}</label>
											<div class="input">
												<table cellpadding="2" cellspacing="0" border="0">
													<?php
													foreach ((array)$VolumeArea as $k => $v) {
														if ((float)$VolumeArea[$k+1] <= 0) break;
                                                        if ($k>0) $v += .001;
													?>
														<tr>
															<td><span class="unit_input"><b><?=$v?> - <?=(float)$VolumeArea[$k+1]?> {/shipping.shipping.volume_unit/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="VolumeAreaPrice[]" value="<?=sprintf('%01.2f', $VolumeAreaPrice[$k]);?>" class="box_input" size="6" maxlength="6" rel="amount"><b class="last">/ {/shipping.shipping.volume_unit/}</b></span></td>
														</tr>
													<?php }?>
													<tr>
														<td><span class="unit_input"><b><?=$v?> {/shipping.shipping.volume_unit/} {/shipping.shipping.over/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="VolumeAreaPrice[]" value="<?=sprintf('%01.2f', $VolumeAreaPrice[$k]);?>" class="box_input" size="6" maxlength="6" rel="amount"><b class="last">/ {/shipping.shipping.volume_unit/}</b></span></td>
													</tr>
												</table>
											</div>
										</div>
									<?php }?>
								</div>
							</div>
							<div class="rows clean">
								<label></label>
								<div class="input input_button">
									<input type="button" class="btn_global btn_submit" value="{/global.save/}">
									<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
								</div>
							</div>
							<input type="hidden" name="AId" value="<?=$AId;?>" />
							<input type="hidden" name="SId" value="<?=$SId;?>" />
							<input type="hidden" name="OvId" value="<?=$OvId;?>" />
							<input type="hidden" name="do_action" value="set.shipping_express_area_edit" />
						</form>
					</div>
				<?php
				}elseif($c['manage']['page']=='area_country'){
					//添加删除分区的国家
					$shipping_row=str::str_code(db::get_one('shipping', "SId='{$SId}'", 'SId,Express,Logo')); //所属快递公司
					$area_row=str::str_code(db::get_one('shipping_area', "AId='{$AId}'", 'AId,SId,Name')); //所属分区
					!$area_row && js::location("./?m=set&a=shipping&d=express&p=area&SId=$SId");
					/******************** 分隔线 ********************/
					$country_row=str::str_code(db::get_all('country', 'IsUsed=1', 'CId, Country, Continent', 'Country asc')); //所有国家
					$express_country_row=str::str_code(db::get_all('shipping_country', "SId='$SId' and AId in(select AId from shipping_area where SId='{$SId}' and OvId='{$OvId}')", '*', 'CId asc')); //从快递公司的国家中筛选出当前分区的国家
					$range_ary=range('A', 'Z');
					$all_country_ary=$area_country_ary=$added_CId=$area_CId=array();
					foreach($country_row as $k=>$v){
						$initial=mb_strcut($v['Country'], 0, 1);//首字母
						$all_country_ary[$initial][]=array('CId'=>$v['CId'], 'Country'=>$v['Country'], 'Continent'=>$v['Continent']); //用一个新数组记录
					}
					foreach($express_country_row as $k=>$v){
						$added_CId[]=$v['CId'];
						if($v['AId']==$area_row['AId']){
							foreach($country_row as $k2=>$v2){
								if($v['CId']==$v2['CId']){ //找到对应的国家
									$area_CId[]=$v['CId'];
									$area_country_ary[]=array('CId'=>$v2['CId'], 'Country'=>$v2['Country'], 'Continent'=>$v['Continent']); //用一个新数组记录
									continue;
								}
							}
						}
					}
				?>
					<script type="text/javascript">$(function(){set_obj.shipping_express_area_country_edit_init();});</script>
					<div class="shipping_area_edit">
						<div class="shipping_area_title">{/shipping.area.setcountry_to/} [ <?=$area_row['Name'];?> ]</div>
						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="country_list">
							<tr>
								<td width="45%" valign="top">
									<div class="country_title">{/shipping.area.choice/}</div>
									<div class="continent_list continent_left_list">
										<?php
										foreach($c['continent'] as $k=>$v){
										?>
											<a class="fl" href="javascript:;" continent="<?=$k;?>">{/continent.<?=$k;?>/}</a>
										<?php }?>
									</div>
									<div class="initial_list initial_left_list"> 
										<?php foreach($range_ary as $v){?>
											<a class="fl" href="javascript:;" initial="<?=$v;?>"><?=$v;?></a>
										<?php }?>
										<div class="clear"></div>
									</div>
									<div class="btn_anti left_anti"><input type="checkbox" class="input_anti" /> {/global.anti/}</div>
									<div class="country_box" id="left_country">
										<?php
										$temp=array();
										foreach($all_country_ary as $k=>$v){
											echo '<div class="initial initial_'.$k.'"></div>';
											foreach($v as $vv){
												if(in_array($vv['CId'], $added_CId)) continue;//如果已添加到快递公司，无论是否添加到此分区都跳过
										?>
											<div class="item" continent="<?=$vv['Continent'];?>"><input type="checkbox" class="select_cid" initial="<?=$k;?>" /> <?=$vv['Country'];?><input type="hidden" name="CId[]" value="<?=$vv['CId'];?>" /></div>
										<?php
											}
										}?>
									</div>
								</td>
								<td width="10%" align="center"><a class="btn_cut" href="javascript:;" id="left_arrow_img"></a>&nbsp;&nbsp;<br />&nbsp;&nbsp;<a class="btn_add" href="javascript:;" id="right_arrow_img"></a></td>
								<td width="45%" valign="top">
									<div class="country_title">{/shipping.area.added/}</div>
									<div class="initial_list initial_right_list">
										<?php foreach($range_ary as $v){?>
											<a class="fl" href="javascript:;" initial="<?=$v;?>"><?=$v;?></a>
										<?php }?>
									</div>
									<form id="edit_form">
										<div class="btn_anti right_anti"><input type="checkbox" class="input_anti" /> {/global.anti/}</div>
										<div class="country_box" id="right_country">
											<?php
											$temp=array();
											foreach($all_country_ary as $k=>$v){
												echo '<div class="initial initial_'.$k.'"></div>';
												foreach($v as $vv){
													if(!in_array($vv['CId'], $area_CId)) continue;
											?>
												<div class="item"><input type="checkbox" class="select_cid" initial="<?=$k;?>" /> <?=$vv['Country'];?><input type="hidden" name="CId[]" value="<?=$vv['CId'];?>" /></div>
											<?php
												}
											}?>
										</div>
										<input type="submit" class="btn_ok btn_submit" name="submit_button" value="{/global.submit/}" />
										<input type="hidden" name="AId" value="<?=$AId;?>" />
										<input type="hidden" name="SId" value="<?=$SId;?>" />
										<input type="hidden" name="do_action" value="set.shipping_express_area_country_edit" />
									</form>
								</td>
							</tr>
						</table>
					</div>
				<?php }?>
			<?php
			}elseif($c['manage']['do']=='insurance'){
				//保险管理
				$insurance_row=str::str_code(db::get_one('shipping_insurance'));
				$config_row=str::str_code(db::get_one('shipping_config', '1', 'IsInsurance'));
				?>
				<script type="text/javascript">$(function(){set_obj.shipping_insurance_edit_init()});</script>
				<div class="insurance_edit">
					<form id="insurance_edit_form" class="global_form">
						<div class="top_title">{/module.set.shipping.insurance/} <a href="javascript:;" class="close"></a></div>
						<div class="rows clean">
							<span class="input_checkbox_box <?=$config_row['IsInsurance']?' checked':'';?>">
								<span class="input_checkbox">
									<input type="checkbox" name="IsInsurance" value="1" <?=$config_row['IsInsurance']?' checked':'';?>>
								</span>{/global.used/}
							</span>
						</div>
						<div class="rows clean">
							<label>{/shipping.shipping.insurance/}</label>
							<div class="input">
								<table cellpadding="2" cellspacing="0" border="0" id="InsArea" currency="<?=$c['manage']['currency_symbol']?>" tips="{/shipping.shipping.over/}">
									<tr>
										<td nowrap="nowrap"><div class="tit">{/shipping.info.order_price/}</div></td>
										<td nowrap="nowrap"><div class="tit">{/shipping.info.insurance_price/}</div></td>
									</tr>
									<?php
									$AreaPrice=str::json_data(htmlspecialchars_decode($insurance_row['AreaPrice']), 'decode');
									foreach((array)$AreaPrice as $k=>$v){?>
										<tr>
											<td nowrap="nowrap"><span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="ProPrice[]" value="<?=$v[0];?>" class="box_input" size="3" maxlength="5" rel="amount" notnull parent_null="1" ><b class="last">{/shipping.shipping.over/}</b></span></td>
											<td nowrap="nowrap"><span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input type="text" name="AreaPrice[]" value="<?=$v[1];?>" class="box_input" size="3" maxlength="5" rel="amount" notnull parent_null="1" ></span><a href="javascript:;" class="icon_delete_1"></a></td>
										</tr>
									<?php }?>
								</table>
							</div>
						</div>
						<div class="rows clean">
							<div class="input"><a href="javascript:;" id="addArea" class="btn_global btn_add_item">{/global.add/}</a></div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}">
								<a href="javascript:;" class="btn_global btn_cancel">{/global.cancel/}</a>
							</div>
						</div>
						<input type="hidden" name="do_action" value="set.shipping_insurance_edit" />
					</form>
				</div>
			<?php
			}elseif($c['manage']['do']=='overseas'){
				//海外仓管理
			?>
				<script type="text/javascript">$(function(){set_obj.shipping_overseas_init();});</script>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<td width="6%" nowrap="nowrap">{/global.serial/}</td>
							<td width="84%" nowrap="nowrap">{/global.name/}</td>
							<?php if($permit_ary['overseas_edit'] || $permit_ary['overseas_del']){?><td width="10%" nowrap="nowrap">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$shipping_overseas_row=str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
						foreach($shipping_overseas_row as $k=>$v){
							if((int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==0 && $v['OvId']>1) continue;
						?>
							<tr cid="<?=$v['OvId'];?>" data="<?=htmlspecialchars(str::json_data($v));?>">
								<td nowrap="nowrap"><?=$k+1;?></td>
								<td nowrap="nowrap"><?=$v['Name'.$c['manage']['web_lang']];?></td>
								<?php if($permit_ary['overseas_edit'] || $permit_ary['overseas_del']){?>
									<td nowrap="nowrap">
										<?php if($permit_ary['edit']){?><a class="tip_ico tip_min_ico edit" href="javascript:;" label="{/global.edit/}" data-id="<?=$v['OvId'];?>"><img src="/static/ico/<?=$c['manage']['cdx_path'];?>edit.png" alt="{/global.edit/}" /></a><?php }?>
										<?php if($v['OvId']>1 && $permit_ary['overseas_del']){?><a class="tip_ico tip_min_ico del" href="./?do_action=set.shipping_overseas_del&OvId=<?=$v['OvId'];?>" label="{/global.del/}"><img src="/static/ico/<?=$c['manage']['cdx_path'];?>del.png" alt="{/global.del/}" /></a><?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?php /***************************** 发货地编辑 Start *****************************/?>
				<div class="pop_form box_overseas_edit">
					<form id="edit_form">
						<div class="t"><h1><span></span>{/module.set.shipping.overseas/}</h1><h2>×</h2></div>
						<div class="global_form">
							<div class="rows clean">
								<label>{/global.name/}</label>
								<div class="input"><?=manage::form_edit('', 'text', 'Name', 53, 150, 'notnull');?></div>
							</div>
							<input type="hidden" name="OvId" value="0" />
							<input type="hidden" name="do_action" value="set.shipping_overseas_edit" />
						</div>
						<div class="button"><input type="submit" class="btn_ok" name="submit_button" value="{/global.save/}" /><input type="button" class="btn_cancel" name="submit_button" value="{/global.cancel/}" /></div>
					</form>
				</div>
				<?php /***************************** 发货地编辑 End *****************************/?>
			<?php }?>
		</div>
	</div>
</div>