<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'newexchangelist'));//检查权限

$exchange = db::get_all('exchange','1','*','sort asc');
$exchangeone = $_GET['hlid']?$_GET['hlid']:$exchange[0]['id'];

$exchangelist = db::get_all('exchange_log',"eid = $exchangeone",'*','time desc,addtime desc');

echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.newexchange_init()});</script>
<div id="newexchange" class="r_con_wrap">
    <div class="center_container">
        <div class="global_container">
            <?php if($c['manage']['do']=='index'){?>
                <div class="big_title">{/module.set.exchange/}&nbsp;&nbsp;
                    <select name="" id="currey">
                        <?php foreach ($exchange as $item) { ?>
                            <option value="<?=$item['id']; ?>" <?=$exchangeone==$item['id']?'selected':''; ?>><?=$item['currency']; ?></option>
                        <?php }?>
                    </select>
                    <script>
                        $('#currey').change(function () {
                            var id = $(this).val();
                            window.location.href="?m=set&a=newexchangelist&hlid="+id;
                        })
                    </script>
                </div>
                <div class="blank20"></div>
            <?php }else{?>
                <a href="javascript:history.back(-1);" class="return_title">
                    <span class="return">{/module.set.exchange/}</span>
                    <span class="s_return">  /  <?=$_GET['CId']?'{/global.edit/}':'{/global.add/}';?></span>
                </a>
            <?php }?>
            <?php
            if($c['manage']['do']=='edit'){
                $CId=(int)$_GET['CId'];
                $currency_row=str::str_code(db::get_one('exchange', "id='{$CId}'", '*'));
                ?>
                <?php /***************************** 汇率编辑 Start *****************************/?>
                <form id="newexchange_edit_form" class="global_form">
                    <div class="rows clean">
                        <label>更新时间</label>
                        <div class="input"><input type="date" name="updatetime" value="<?=$currency_row['updatetime'];?>" class="box_input" size="20" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>伊朗币</label>
                        <div class="input"><input type="text" name="irrmoney" value="<?=$currency_row['irrmoney'];?>" class="box_input" size="20" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>需要兑换的货币名称</label>
                        <div class="input"><input type="text" name="currency" value="<?=$currency_row['currency'];?>" class="box_input" size="10" maxlength="10" notnull /></div>
                    </div>
                    <div class="rows clean">
                        <label>需要兑换的货币符号</label>
                        <div class="input"><input type="text" name="fuhao" value="<?=$currency_row['fuhao'];?>" class="box_input" size="10" maxlength="10" notnull /> <br /><b class="tips">{/set.exchange.symbol_tops/}</b></div>
                    </div>
                    <div class="rows clean">
                        <label>汇率</label>
                        <div class="input"><input type="text" name="money" value="<?=$currency_row['money'];?>" class="box_input" size="10" maxlength="10" notnull rel="amount" /></div>
                    </div>
                    <div class="rows clean">
                        <label>是否启用</label>
                        <div class="input">
                            <select name="isuse" id="isuse" class="box_input">
                                <option value="1" <?=$currency_row['isuse']==1?'selected':'';?>>是</option>
                                <option value="2" <?=$currency_row['isuse']==2?'selected':'';?>>否</option>
                            </select>
                        </div>
                    </div>
                    <div class="rows clean">
                        <label>排序</label>
                        <div class="input"><input type="text" name="sort" value="<?=$currency_row['sort'];?>" class="box_input" size="10" maxlength="10" notnull rel="amount" /></div>
                    </div>


                    <div class="rows clean">
                        <label></label>
                        <div class="input input_button">
                            <input type="button" class="btn_global btn_submit <?=$c['manage']['cdx_limit']?>" value="{/global.save/}" />
                            <a href="./?m=set&a=newexchange"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
                        </div>
                    </div>
                    <input type="hidden" name="CId" value="<?=$CId; ?>" />
                    <input type="hidden" name="do_action" value="set.newexchange_edit" />
                </form>
                <?php /***************************** 汇率编辑 End *****************************/?>
                <?php
            }else{
                $where='1';
                $currency_row=str::str_code(db::get_all('exchange', $where, '*', 'sort asc'));

                ?>
                <div class="config_table_body">
                    <div class="table_item">

                        <table border="0" cellpadding="5" cellspacing="0" class="config_table">
                            <tbody>
                            <tr>
                                <td width="33%" nowrap="nowrap">
                                    时间
                                </td>
                                <td width="33%" nowrap="nowrap">
                                   兑换比例
                                </td>
                                <td width="33%" nowrap="nowrap">
                                    添加时间
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php if(count($exchangelist)>0){
                        foreach($exchangelist as $k=>$v){ ?>
                         <div class="table_item" data-id="<?=$v['CId'];?>">
                            <table border="0" cellpadding="5" cellspacing="0" class="config_table">
                                <tbody>
                                <tr>
                                    <td width="33%" nowrap="nowrap">

                                        <div class="desc">
                                            <?=$v['time'];?>
                                        </div>
                                    </td>
                                    <td width="33%" nowrap="nowrap">
                                        <?=(float)$v['irrmoney']*(float)$v['money'];?>IRR:<?=$v['irrmoney'].$v['currency']; ?>
                                    </td>
                                    <td width="33%" nowrap="nowrap">
                                        <?=date("Y-m-d H:i:s",$v['addtime']); ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php }?>
                    <?php }else{ ?>
                        <div class="table_item" data-id="<?=$v['CId'];?>" style="text-align: center;height: 100px; line-height: 100px;">
                            暂无历史数据
                        </div>
                    <?php } ?>


                </div>

            <?php }?>
        </div>
    </div>
</div>