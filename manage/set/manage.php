<?php !isset($c) && exit();?>
<?php
$permit_ary=array(
	'add'	=>	manage::check_permit('set', 0, array('a'=>'manage', 'd'=>'add')),
	'edit'	=>	manage::check_permit('set', 0, array('a'=>'manage', 'd'=>'edit')),
	'del'	=>	manage::check_permit('set', 0, array('a'=>'manage', 'd'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'?'manage':'manage_inside');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.manage_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<div class="center_container_1000">
		<?php
        if($c['manage']['do']=='index'){
            //管理员管理列表
        ?>
            <div class="inside_container">
                <h1>{/module.set.manage/}</h1>
            </div>
            <div class="inside_table">
                <div class="list_menu">
                    <ul class="list_menu_button">
                        <?php if($_SESSION['Manage']['GroupId']==1){?><li><a class="add" href="./?m=set&a=manage&d=edit">{/global.add/}</a></li><?php }?>
                    </ul>
                </div>
                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                        <tr>
                            <td width="1%" nowrap="nowrap">{/global.serial/}</td>
                            <td width="26%" nowrap="nowrap">{/manage.manage.username/}</td>
                            <td width="25%" nowrap="nowrap">{/manage.manage.group/}</td>
                            <td width="20%" nowrap="nowrap">{/manage.manage.last_login_time/}</td>
                            <td width="10%" nowrap="nowrap">{/manage.manage.locked/}</td>
                            <td width="15%" nowrap="nowrap">{/manage.manage.create_time/}</td>
                            <?php if((int)$_SESSION['Manage']['GroupId']!=3){?>
                                <td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$data=array('Action'=>'ueeshop_web_manage_list');
					$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
					if($result['ret']==1){
						foreach($result['msg'] as $k=>$v){
							if($_SESSION['Manage']['GroupId']>1 && $_SESSION['Manage']['UserName']!=$v['UserName']) continue;
							$u=$v['UserName'].'.'.$v['GroupId'].'.'.$v['Locked'];
					?>
                            <tr>
                                <td nowrap="nowrap"><?=$k+1;?></td>
                                <td nowrap="nowrap"><?=$v['UserName'];?></td>
                                <td nowrap="nowrap"><?=$c['manage']['lang_pack']['manage']['manage']['permit_name'][$v['GroupId']];?></td>
                                <td nowrap="nowrap"><?=$v['LastLoginTime']?date('Y-m-d H:i:s', $v['LastLoginTime']):'';?><br /><?=$v['LastLoginIp'];?></td>
                                <td nowrap="nowrap">
                                    <?php
                                    if($_SESSION['Manage']['GroupId']==1 && $_SESSION['Manage']['UserName']!=$v['UserName'] && $permit_ary['edit']){
                                    ?>
                                        <div class="switchery<?=$v['Locked']==0?' checked':'';?>">
                                            <input type="checkbox" name="Locked[]" value="1" data-id="<?=$u;?>"<?=$v['Locked']==0?' checked':'';?> />
                                            <div class="switchery_toggler"></div>
                                            <div class="switchery_inner">
                                                <div class="switchery_state_on"></div>
                                                <div class="switchery_state_off"></div>
                                            </div>
                                        </div>
                                    <?php
                                    }else{
                                        echo $v['Locked']==0?'{/global.n_y.1/}':'';
                                    }?>
                                </td>
                                <td nowrap="nowrap"><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
                                <?php if((int)$_SESSION['Manage']['GroupId']!=3){?>
                                    <td nowrap="nowrap" class="operation side_by_side">
                                        <a href="./?m=set&a=manage&d=edit&u=<?=$u;?>">{/global.edit/}</a>
                                        <?php if($_SESSION['Manage']['UserName']!=$v['UserName']){?>
                                            <dl>
                                                <dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
                                                <dd class="drop_down">
                                                    <a class="del item" href="./?do_action=set.manage_del&u=<?=$v['UserName'];?>" rel="del">{/global.del/}</a>
                                                </dd>
                                            </dl>
                                        <?php }?>
                                    </td>
                                <?php }?>
                            </tr>
					<?php 
						}
					}
					?>
                    </tbody>
                </table>
            </div>
        <?php
        }else{
            //管理员编辑
            $name_ary=array(
                'category'	=>	'{/global.category/}',
                'photo'		=>	'{/set.photo.pic_list/}',
                'page'		=>	'{/page.page.page/}',
                'news'		=>	'{/news.news.news/}',
                'model'		=>	'{/products.attribute/}',
                'business'	=>	'{/module.products.business.module_name/}',
            );
			$data=array();
			if($_GET['u']){
				$data=@explode('.', $_GET['u']);
				if($data[1]!=1){
					$manage_permit=db::get_all('manage_permit', "UserName='{$data[0]}'");
					$manage_permit_ary=array();
					foreach($manage_permit as $v){
						$manage_permit_ary[$v['Module']]=array(
							0=>(int)$v['Permit'],
							1=>@json_decode(stripslashes($v['DetailsPermit']), true)
						);
					}
				}
			}
        ?>
            <div class="global_container">
                <a href="javascript:history.back(-1);" class="return_title">
                    <span class="return">{/module.set.manage/}</span> 
                    <span class="s_return">/ <?=$data[0]?'{/global.edit/}':'{/global.add/}';?></span>
                </a>
                <form id="manage_edit_form" class="global_form">
                    <div class="rows clean">
                        <label>{/manage.manage.username/}</label>
                        <div class="input"><input name="UserName" value="<?=$data[0];?>" type="text" class="box_input" maxlength="30" size="30" <?=$data[0]?'readonly':'';?> notnull></div>
                    </div>
                    <div class="rows clean">
                        <label>{/manage.manage.password/}</label>
                        <div class="input"><input type="password" name="Password" value="" class="box_input" size="30" maxlength="30"<?=$data[0]?'':' notnull';?> /></div>
                    </div>
                    <div class="rows clean">
                        <label>{/manage.manage.confirm_password/}</label>
                        <div class="input"><input type="password" name="ConfirmPassword" value="" class="box_input" size="30" maxlength="30"<?=$data[0]?'':' notnull';?> /></div>
                    </div>
                    <div class="rows clean"<?=$_SESSION['Manage']['GroupId']==1?'':' style="display:none;"';?>>
                        <label>{/manage.manage.group/}</label>
                        <div class="input">
                            <div class="box_select">
                                <select name="GroupId">
                                    <option value="1"<?=$data[1]==1?' selected':'';?>>{/manage.manage.permit_name.1/}</option>
                                    <?php if($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)){?>
                                        <option value="2"<?=$data[1]==2?' selected':'';?>>{/manage.manage.permit_name.2/}</option>
                                        <option value="3"<?=$data[1]==3?' selected':'';?>>{/manage.manage.permit_name.3/}</option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="rows clean">
                        <label></label>
                        <div class="input input_button">
                            <input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=set&a=manage"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
							<?php if($_SESSION['Manage']['GroupId']==1){?>
								<a href="javascript:;" class="btn_cancel btn_permit" style="display:<?=(!$data[0] || $data[1]==1)?'none':'inline-block';?>;">{/manage.manage.permit/}</a>
							<?php }?>
                        </div>
                    </div>
                    <input type="hidden" name="Locked" value="<?=$data[2]==1?1:0;?>" />
                    <input type="hidden" name="Method" value="<?=$data[0]?1:0;?>" />
                    <input type="hidden" name="do_action" value="set.manage_edit" />
                </form>
            </div>
        <?php }?>
	</div>
</div>
<?php
if($c['manage']['do']=='edit'){
?>
<div id="fixed_right">
	<div class="global_container fixed_permit">
		<div class="top_title">{/orders.export.options_config/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="permit_form">
			<?php if($c['FunVersion']>1 || ($c['FunVersion']==1 && $c['NewFunVersion']<=1)){?>
				<div id="PermitBox" style="display:<?=$data[1]==1?'none':'';?>">
					<div class="PermitHead">{/manage.manage.permit/}&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="selected_all" hidefocus="true" />{/global.select_all/}</div>
					<?php
					foreach($c['manage']['permit'] as $k=>$v){
						//一级权限
						if(@in_array($k, $c['manage']['permit_base'])) continue;
						$input="Permit_{$k}";
						$name="{/module.{$k}.module_name/}";
					?>
						<div class="module" style="display:<?=($data[1]==2 || ($data[1]==3 && in_array($k, array('orders', 'user'))))?'block':'none';?>;">
							<input type="checkbox" name="<?=$input;?>" <?=$manage_permit_ary[$k][0]?'checked':'';?> value="1" id="<?=$input;?>" /> <label for="<?=$input;?>"><?=$name;?></label>
							<?php
							if($v){
								//二级权限
								foreach((array)$v as $k2=>$v2){
									$input="Permit_{$k}_{$k2}";
									$name=isset($v2['menu'])?"{/module.{$k}.{$k2}.module_name/}":"{/module.{$k}.{$k2}/}";
							?>
								<dl>
									<dt><input type="checkbox" name="<?=$input;?>" <?=$manage_permit_ary[$k][1][$k2][0]?'checked':'';?> value="1" id="<?=$input;?>" /> <label for="<?=$input;?>"><?=$name;?></label></dt>
									<?php
									if(isset($v2['menu'])){
										//三级权限
									?>
									<dd>
										<?php
										foreach((array)$v2['menu'] as $v3){
											$input="Permit_{$k}_{$k2}_{$v3}";
											$name="{/module.{$k}.{$k2}.{$v3}/}";
											$name_ary[$v3] && $name=$name_ary[$v3];
										?>
										<input type="checkbox" name="<?=$input;?>" <?=$manage_permit_ary[$k][1][$k2][1][$v3][0]?'checked':'';?> value="1" id="<?=$input;?>" /> <label for="<?=$input;?>"><?=$name;?></label>
											<?php if($v2['permit'][$v3]){//三级操作权限?>
												<span>(
												<?php
												foreach((array)$v2['permit'][$v3] as $v4){
													$input="Permit_{$k}_{$k2}_{$v3}_{$v4}";
												?>
													<input type="checkbox" name="<?=$input;?>" <?=$manage_permit_ary[$k][1][$k2][1][$v3][1][$v4][0]?'checked':'';?> value="1" id="<?=$input;?>" /> <label for="<?=$input;?>">{/global.<?=$v4;?>/}</label>
												<?php }?>
												)</span>
											<?php }?>
										<?php }?>
									</dd>
									<?php
									}elseif(!isset($v2['menu']) && isset($v2['permit'])){
										//三级操作权限
									?>
									<dd>
										<span>(
										<?php
										foreach((array)$v2['permit'] as $v3){
											$input="Permit_{$k}_{$k2}_{$v3}";
										?>
										<input type="checkbox" name="<?=$input;?>" <?=$manage_permit_ary[$k][1][$k2][1][$v3][0]?'checked':'';?> value="1" id="<?=$input;?>" /> <label for="<?=$input;?>">{/global.<?=$v3;?>/}</label>
										<?php }?>
										)</span>
									</dd>
									<?php }?>
								</dl>
								<div class="clear"></div>
							<?php
								}
							}?>
						</div>
					<?php }?>
				</div>
			<?php }?>
			<div class="rows clean box_button">
				<div class="input">
					<input type="button" class="btn_global btn_submit" value="{/global.confirm_sure/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
		</form>
	</div>
</div>
<?php }?>