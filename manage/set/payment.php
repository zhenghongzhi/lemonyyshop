<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'payment'));//检查权限

$permit_ary['edit']=manage::check_permit('set', 0, array('a'=>'payment', 'd'=>'edit'));

$payment_plugins=array();
$rows=db::get_all('plugins', "Category='payment' and IsUsed=1", 'ClassName');
foreach($rows as $v){$payment_plugins[]=strtolower($v['ClassName']);}
?>
<div id="payment" class="r_con_wrap">
	<div class="center_container">
		<div class="global_container">
			<?php if($c['manage']['do']=='index'){ ?>
				<div class="big_title">{/module.set.payment/}</div>
				<div class="blank20"></div>
			<?php }else{?>
				<a href="./?m=set&a=payment" class="return_title">
					<span class="return">{/module.set.payment/}</span> 
					<?=$c['manage']['do']=='edit'?'<span class="s_return">/ {/global.edit/}</span>':'';?>
					<?=$c['manage']['do']=='add'?'<span class="s_return">/ {/set.payment.choose/}</span>':'';?>
				</a>
			<?php }?>
			<?php
			if($_GET['PId']){
				$PId=(int)$_GET['PId'];
				$payment_row=str::str_code(db::get_one('payment', "PId='$PId'"));
				!$payment_row && js::location('./?m=set&a=payment');
				echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js', '/static/js/plugin/ckeditor/ckeditor.js');
				?>
				<script type="text/javascript">$(document).ready(function(){set_obj.payment_edit_init();});</script>
				<form id="payment_edit_form" class="global_form">
					<div class="rows clean">
						<label>{/global.name/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input"><?=manage::form_edit($payment_row, 'text', 'Name', 70, 255, 'notnull');?></div>
					</div>
					<div class="rows clean">
						<label>{/global.pic/}</label>
						<div class="input">
							<?php
							if($permit_ary['edit']){
							 	echo manage::multi_img('LogoDetail', 'LogoPath', $payment_row['LogoPath']);
							}else{
								if(is_file($c['root_path'].$payment_row['LogoPath'])) echo '<img src="'.$payment_row['LogoPath'].'" />';
							}?>
						</div>
					</div>
					<?php if(in_array($payment_row['Method'], array('Paypal', 'Excheckout'))){?>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<span class="input_checkbox_box <?=$payment_row['IsCreditCard']?' checked':'';?>">
									<span class="input_checkbox">
										<input type="checkbox" name="IsCreditCard" value="1"<?=$payment_row['IsCreditCard']?' checked':'';?>>
									</span>{/set.payment.credit_card_payment/}
								</span>
							</div>
							<div class="clear"></div>
						</div>
					<?php }?>
					<div class="rows clean">
						<label>{/set.payment.limit/}</label>
						<div class="box_explain">{/set.explain.range/}</div>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input"  name="MinPrice" id="MinPrice" value="<?=(float)$payment_row['MinPrice'];?>" size="5" maxlength="255" rel="amount" /></span>
							&nbsp;&nbsp;~&nbsp;&nbsp;
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input"  name="MaxPrice" id="MaxPrice" value="<?=(float)$payment_row['MaxPrice'];?>" size="5" maxlength="255" rel="amount" /></span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/set.payment.addfee/}</label>
						<div class="input">
							<span class="unit_input"><input type="text" class="box_input" name="AdditionalFee" value="<?=$payment_row['AdditionalFee'];?>" size="5" rel="amount" maxlength="255" /><b class="last">%</b></span>
						</div>
					</div>
					<div class="rows clean">
						<label>{/shipping.area.additional/}</label>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input type="text" class="box_input" name="AffixPrice" value="<?=sprintf('%01.2f', $payment_row['AffixPrice']);?>"  size="5" maxlength="255" rel="amount" /></span>
						</div>
					</div>
					<?php
					$Attribute=$payment_row['Attribute'];
					$new_data=0;
					if($c['NewFunVersion']>=4 && ($payment_row['Method']=='Paypal' || $payment_row['Method']=='Excheckout')){//新用户版本
						$Attribute=$payment_row['NewAttribute'];
						$new_data=1;
					}
					$attr_ary=str::json_data(htmlspecialchars_decode($Attribute), 'decode');
					if($attr_ary){
					?>
						<div class="rows clean">
							<label>{/set.payment.account_info/}</label>							
							<div class="input">
								<?php
                                $i=0;
								foreach((array)$attr_ary as $k=>$v){
								?>
									<span class="unit_input"><b><?=$k;?></b><input type="<?=strtolower($k)=='password' ? 'password' : 'text';?>" class="box_input" name="Value[]" value="<?=$v;?>" size="40"<?=$new_data==1?' notnull':'';?> /><input type="hidden" name="Name[]" value="<?=$k;?>" /></span><?=($new_data && $i==0)?'&nbsp;&nbsp;&nbsp;<a href="http://help.shop.'.($c['FunVersion']?'shopcto':'ueeshop').'.com/i-425.html" target="_blank" style="text-decoration:underline;">{/global.how_to_get/}</a>':'';?>
									<div class="blank9"></div>
								<?php
                                    ++$i;
                                }?>
							</div>
						</div>
					<?php }?>
					<div class="rows">
						<label>{/set.payment.description/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<?php if (!$payment_row['IsOnline']) { ?>
							<div class="box_explain">{/set.explain.account/}</div>
						<?php } ?>
						<div class="input">
							<?=manage::form_edit($payment_row, 'editor_simple', 'Description', 0, 0, 300);?>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input">
							<?php if($permit_ary['edit']){?>
								<input type="button" class="btn_global btn_submit" value="{/global.save/}" />
							<?php }?>
							<a href="./?m=set&a=payment"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" id="PId" name="PId" value="<?=$payment_row['PId'];?>" />
					<input type="hidden" name="do_action" value="set.payment_edit" />
				</form>
			<?php
			}elseif($c['manage']['do']=='add'){
			?>
				<div class="pay_type">
					<a href="./?m=set&a=payment&d=add" <?=$_GET['offline'] ? '' : 'class="cur"'; ?>>{/set.payment.online/}</a>
					<a href="./?m=set&a=payment&d=add&offline=1" <?=$_GET['offline'] ? 'class="cur"' : ''; ?>>{/set.payment.money_transfer/}</a>
				</div>
				<div class="pay_list">
					<?php
					$where='IsGet=0';
					$where.=(int)$_GET['offline'] ? ' and IsOnline=0' : ' and IsOnline=1';
					$payment_row=str::str_code(db::get_all('payment', $where, '*', $c['my_order'].'IsUsed desc, IsOnline desc, PId asc'));
					$i=0;
					foreach((array)$payment_row as $k=>$v){
						$method_path=@strtolower($v['Method']);
						$method_path=='excheckout' && $method_path='paypal_excheckout';
						@substr_count($method_path, 'payssion') && $method_path='payssion_'.substr($method_path, 8);
						if($v['IsOnline'] && !in_array($method_path, $payment_plugins)) continue;

						$pic=@is_file($c['root_path'].$v['LogoPath'])?$v['LogoPath']:'';
						$method=strtolower($v['Method']);
						substr_count($method, 'globebill') && $method='globebill';
						substr_count($method, 'payssion') && $method='payssion';
						$url=$v['Url']?$v['Url']:'javascript:;';
						?>
						<div class="item<?=$i%3==0?' fir':'';?>">
							<div class="pic"><a href="<?=$url; ?>" target="_blank"><img src="<?=$pic;?>" alt=""></a><span></span></div>
							<div class="name"><a href="<?=$url; ?>" target="_blank"><?=($v['Method']=='Excheckout'?'Paypal ':'').str_replace('Payssion', '', $v['Method']);?></a></div>
							<?php if($v['IsOnline']){?>
								<div class="desc">{/set.payment.tips.<?=$method;?>/}</div>
							<?php } ?>
							<a href="./?m=set&a=payment&d=add&PId=<?=$v['PId'];?>" class="get <?=$c['manage']['cdx_limit'];?>">{/set.payment.get/}</a>
						</div>
						<?php if($i%3==2){?>
							<div class="line clear"></div>
						<?php }?>
					<?php
						++$i;
					}?>
					<div class="clear"></div>
				</div>
			<?php
			}else{
				echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
			?>
				<script type="text/javascript">$(document).ready(function(){set_obj.payment_edit_init();});</script>
				<div class="config_table_body">
					<?php
					$payment_row=str::str_code(db::get_all('payment', 'IsGet=1', '*', $c['my_order'].'IsUsed desc, IsOnline desc, PId asc'));
					foreach($payment_row as $k=>$v){
						$method_path=@strtolower($v['Method']);
						$method_path=='excheckout' && $method_path='paypal_excheckout';
						@substr_count($method_path, 'payssion') && $method_path='payssion_'.substr($method_path, 8);
						if($v['IsOnline'] && !in_array($method_path, $payment_plugins)) continue;
	
						$pic=@is_file($c['root_path'].$v['LogoPath'])?$v['LogoPath']:'';
						$method=strtolower($v['Method']);
						substr_count($method, 'globebill') && $method='globebill';
						substr_count($method, 'payssion') && $method='payssion';
						$url=$v['Url']?$v['Url']:'javascript:;';
						?>
						<div class="table_item" data-id="<?=$v['PId'];?>">
							<table border="0" cellpadding="5" cellspacing="0" class="config_table">
								<thead>
									<tr><td nowrap="nowrap" class="myorder"><span class="icon_myorder"></span></td></tr>
								</thead>
								<tbody>
									<tr>
										<td width="45%" nowrap="nowrap" class="myorder_left">
											<div class="info">
												<div class="name"><?=($v['Method']=='Excheckout'?'Paypal ':'').str_replace('Payssion', '', $v['Method']).($c['NewFunVersion']>=5 && ($v['Method']=='Excheckout' || $v['Method']=='Paypal')?' APM':'');?></div>
												<div class="desc"><?=$v['Name'.$c['manage']['web_lang']];?></div>
											</div>
											<div class="img img_pay"><a href="<?=$url; ?>" target="_blank"><img src="<?=$pic;?>" alt=""></a><span></span></div>
										</td>
										<td width="25%" nowrap="nowrap">
											{/set.payment.addfee/}<?=$v['AdditionalFee'];?>% <br /><?=$v['IsOnline']?'{/set.payment.online/}':'{/set.payment.money_transfer/}';?>
										</td>
										<td width="15%" nowrap="nowrap">
											<a href="./?m=set&a=payment&d=edit&PId=<?=$v['PId'];?>" class="edit">{/global.edit/}</a><br />
											<?php if($c['FunVersion']!=10){?><a href="./?do_action=set.payment_del&PId=<?=$v['PId'];?>" class="del">{/global.del/}</a><?php }?>
										</td>
										<td width="10%" nowrap="nowrap" align="center" class="payment_used">
											<?php
											if($permit_ary['edit']){
											?>
												<div class="switchery<?=$v['IsUsed']?' checked':'';?>" data-pid="<?=$v['PId'];?>">
													<input type="checkbox" name="IsUsed" value="1"<?=$v['IsUsed']?' checked':'';?>>
													<div class="switchery_toggler"></div>
													<div class="switchery_inner">
														<div class="switchery_state_on"></div>
														<div class="switchery_state_off"></div>
													</div>
												</div>
											<?php
											}else{
												echo $v['IsUsed']?'{/global.n_y.1/} <br />':'';
											}?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php }?>
				</div>
				<a href="./?m=set&a=payment&d=add" class="add set_add">{/global.add/}</a>
				<br /><br /><br />
			<?php }?>
		</div>
		<div id="statement">
			声明：该展示并不视为我司（广州<?=$c['FunVersion']>=10?'蓝狮':'联雅';?>网络科技有限公司）对展示公司包括但不限于资信、服务能力、经营能力以及其他第三方据以与展示公司开展合作的考量因素等的保证或承诺。任何第三方应自行判断是否与展示公司开展商业往来，并自行承担商业风险。
		</div>
	</div>
</div>