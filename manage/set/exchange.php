<?php !isset($c) && exit();?>
<?php
manage::check_permit('set', 1, array('a'=>'exchange'));//检查权限

$permit_ary=array(
	'add'	=>	manage::check_permit('set', 0, array('a'=>'exchange', 'd'=>'add')),
	'edit'	=>	manage::check_permit('set', 0, array('a'=>'exchange', 'd'=>'edit')),
	'del'	=>	manage::check_permit('set', 0, array('a'=>'exchange', 'd'=>'del'))
);

echo ly200::load_static('/static/js/plugin/operamasks/operamasks-ui.css', '/static/js/plugin/operamasks/operamasks-ui.min.js');
?>
<script type="text/javascript">$(document).ready(function(){set_obj.exchange_init()});</script>
<div id="exchange" class="r_con_wrap">
	<div class="center_container">
		<div class="global_container">
			<?php if($c['manage']['do']=='index'){?>
				<div class="big_title">{/module.set.exchange/}&nbsp;&nbsp;<span class="box_explain">( {/set.explain.rate/} )</span></div>
				<div class="blank20"></div>
			<?php }else{?>
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.set.exchange/}</span> 
					<span class="s_return">  /  <?=$_GET['CId']?'{/global.edit/}':'{/global.add/}';?></span>
				</a>
			<?php }?>
			<?php
			if($c['manage']['do']=='edit'){ 
				$CId=(int)$_GET['CId'];
				$currency_row=str::str_code(db::get_one('currency', "CId='{$CId}'", '*'));
			?>
				<?php /***************************** 汇率编辑 Start *****************************/?>
				<form id="exchange_edit_form" class="global_form">
					<div class="rows clean">
						<label>{/set.exchange.name/}</label>
						<div class="input"><input type="text" name="Currency" value="<?=$currency_row['Currency'];?>" class="box_input <?=$currency_row['IsFixed'] ? 'no_drop' : ''; ?>" <?=$currency_row['IsFixed'] ? 'readonly="readonly"' : ''; ?> size="20" maxlength="10" notnull /></div>
					</div>
                    <div class="rows clean">
                        <label>{/set.exchange.name/}(波斯语)</label>
                        <div class="input"><input type="text" name="Currency" value="<?=$currency_row['faCurrency'];?>" class="box_input <?=$currency_row['IsFixed'] ? 'no_drop' : ''; ?>" <?=$currency_row['IsFixed'] ? 'readonly="readonly"' : ''; ?> size="20" maxlength="10" notnull /></div>
                    </div>
					<div class="rows clean">
						<label>{/set.exchange.symbol/}</label>
						<div class="input"><input type="text" name="Symbol" value="<?=$currency_row['Symbol'];?>" class="box_input <?=$CId==1 ? 'no_drop' : ''; ?>" <?=$CId==1 ? 'readonly="readonly"' : ''; ?> size="10" maxlength="10" notnull /> <br /><b class="tips">{/set.exchange.symbol_tops/}</b></div>
					</div>
					<div class="rows clean">
						<label>{/set.exchange.rate/}(<?=$c['manage']['currency_symbol'];?>)</label>
						<div class="input"><input type="text" name="ExchangeRate" value="<?=$currency_row['ExchangeRate'];?>" class="box_input <?=$CId==1 ? 'no_drop' : ''; ?>" <?=$CId==1 ? 'readonly="readonly"' : ''; ?> size="10" maxlength="10" notnull rel="amount" /></div>
					</div>
					<div class="rows clean">
						<label>{/global.pic/}</label>
						<div class="input">
							<?=manage::multi_img('FlagDetail', 'FlagPath', $currency_row['FlagPath']); ?>
						</div>
					</div>
					<?php if($currency_row['IsUsed']){ ?>
						<div class="rows clean">
							<label>{/set.exchange.rate_default/}</label>
							<div class="input">
								<span class="input_checkbox_box <?=$currency_row['IsDefault'] ? 'checked' : '' ?>">
									<span class="input_checkbox">
										<input type="checkbox" name="IsDefault" <?=$currency_row['IsDefault'] ? 'checked="checked"' : '' ?> value="1">
									</span>{/set.exchange.default/}
								</span> &nbsp;&nbsp;&nbsp;&nbsp;
								<span class="input_checkbox_box <?=$currency_row['ManageDefault'] ? 'checked' : '' ?>">
									<span class="input_checkbox">
										<input type="checkbox" <?=$currency_row['ManageDefault'] ? 'checked="checked"' : '' ?> name="ManageDefault" value="1">
									</span>{/set.exchange.manage_default/}
								</span>
							</div>
						</div>
					<?php } ?>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit <?=$c['manage']['cdx_limit']?>" value="{/global.save/}" />
							<a href="./?m=set&a=exchange"><input type="button" class="btn_global btn_cancel" value="{/global.return/}" /></a>
						</div>
					</div>
					<input type="hidden" name="CId" value="<?=$CId; ?>" />
					<input type="hidden" name="do_action" value="set.exchange_edit" />
				</form>
				<?php /***************************** 汇率编辑 End *****************************/?>
			<?php
			}else{ 
				$where='1';
				if($c['FunVersion']<1){
					$where.=' and Currency="USD"';
					db::update('currency', 'Currency!="USD"', array('IsUsed'=>0, 'IsDefault'=>0));
					db::update('currency', 'Currency="USD"', array('IsUsed'=>1, 'IsDefault'=>1));
				}
				$currency_row=str::str_code(db::get_all('currency', $where, '*', $c['my_order'].'CId asc'));
			?>
				<div class="config_table_body">
					<?php
					foreach($currency_row as $k=>$v){
						$Default=(int)$v['IsDefault'];
						$Used=(int)$v['IsUsed'];
						$ManageDefault=(int)$v['ManageDefault'];
						$v['IsFlagPath']=($v['FlagPath'] && is_file($c['root_path'].$v['FlagPath']))?1:0; //判断国旗图片是否存在
					?>
						<div class="table_item" data-id="<?=$v['CId'];?>">
							<table border="0" cellpadding="5" cellspacing="0" class="config_table">
								<tbody>
									<tr>
										<td width="45%" nowrap="nowrap">
											<div class="img img_small"><img src="<?=$v['FlagPath'];?>" alt=""><span></span></div>
											<span class="name"><?=$v['Currency'].' ('.$v['Symbol'].')';?></span>
											<br />
											<div class="desc">
												<?=($Default?'{/set.exchange.default/}':'').($Default && $ManageDefault?' / ':'').($ManageDefault?'{/set.exchange.manage_default/}':'');?>
											</div>
										</td>
										<td width="25%" nowrap="nowrap">
											$1=<?=$v['Symbol'].$v['ExchangeRate'];?>
										</td>
										<td width="15%" nowrap="nowrap">
											<?php if($c['FunVersion']>=1 && $permit_ary['edit']){?><a href="./?m=set&a=exchange&d=edit&CId=<?=$v['CId']; ?>" class="edit <?=$c['manage']['cdx_limit']?>">{/global.edit/}</a><br /><?php } ?>
											<?php if($v['IsFixed']==0 && $permit_ary['del']){?><a href="./?do_action=set.exchange_del&CId=<?=$v['CId']; ?>" class="del">{/global.del/}</a><?php } ?>
										</td>
										<td width="10%" nowrap="nowrap" align="center">
											<?php
											if($permit_ary['edit']){
											?>
												<div class="used_checkbox <?=$c['manage']['cdx_limit']?>">
													<div class="switchery<?=$Used?' checked':'';?><?=($ManageDefault || $Default)?' no_drop':'';?>" data-cid="<?=$v['CId']; ?>">
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</div>
											<?php
											}else{
												echo $Used?'{/global.n_y.1/} <br />':'';
											}?>
										</td>					
									</tr>
								</tbody>
							</table>
						</div>
					<?php }?>
				</div>
				<?php if($c['manage']['do']=='index' && $c['FunVersion']>=1){?>
					<a href="./?m=set&a=exchange&d=edit" class="add set_add <?=$c['manage']['cdx_limit']?>">{/global.add/}</a>
				<?php }?>
			<?php }?>
		</div>
	</div>
</div>