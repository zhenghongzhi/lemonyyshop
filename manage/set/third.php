<?php !isset($c) && exit();?>
<?php

$permit_ary=array(
	'add'			=>	manage::check_permit('set', 0, array('a'=>'third', 'd'=>'add')),
	'edit'			=>	manage::check_permit('set', 0, array('a'=>'third', 'd'=>'edit')),
	'del'			=>	manage::check_permit('set', 0, array('a'=>'third', 'd'=>'del'))
);
?>
<div id="third" class="r_con_wrap">
    <div class="center_container">
        <div class="global_container">
        	<?php
        	if($c['manage']['do']=='index'){
        		//第三方代码列表
        		//echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
			?>
        		<script type="text/javascript">$(document).ready(function(){set_obj.seo_third_init()});</script>
                <div class="big_title">{/module.set.third/}</div>
                <div class="blank20"></div>
                <div class="myorder_box">
					<div class="config_table_body">
						<?php
						$Keyword=str::str_code($_GET['Keyword']);
						$where='1';//条件
						$Keyword && $where.=" and Title like '%$Keyword%'";
						$third_row=str::str_code(db::get_all('third', $where, '*', $c['my_order'].'TId desc'));
						foreach($third_row as $v){
						?>
							<div class="table_item" data-id="<?=$v['TId'];?>">
								<table border="0" cellpadding="5" cellspacing="0" class="config_table"  data-id="<?=$v['TId'];?>">
									<tr>
										<td width="45%" nowrap="nowrap">
											<div class="name"><?=$v['Title'];?></div>
										</td>
										<td width="25%">
											<?=$c['manage']['lang_pack']['seo']['third']['code_type_ary'][$v['CodeType']];?>
											<?=$v['IsMeta']?'<br /><span class="gary">{/seo.third.is_meta/}</span>':'';?>
										</td>
										<td width="15" nowrap="nowrap">
											<?php if($permit_ary['edit']){?><a class="edit" href="./?m=set&a=third&d=edit&TId=<?=$v['TId'];?>" label="">{/global.edit/}</a><?php } ?>
											<?php if($permit_ary['del']){?><br /><a class="del" href="./?do_action=set.seo_third_del&TId=<?=$v['TId'];?>" rel="del">{/global.del/}</a><?php } ?>
										</td>
										<td width="10%" nowrap="nowrap" align="center">
											<?php if($permit_ary['edit']){?>
												<div class="used_checkbox">
													<div class="switchery<?=$v['IsUsed']?' checked':'';?>" data-tid="<?=$v['TId']; ?>">
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</div>
											<?php
											}else{
												echo $v['IsUsed']?'{/global.n_y.1/} <br />':'';
											}?>
										</td>                   
									</tr>
								</table>
							</div>
						<?php }?>
					</div>
                    <a class="add set_add <?=$c['manage']['cdx_limit'];?>" href="./?m=set&a=third&d=edit">{/global.add/}</a>
                    <br />
                </div>
        	<?php
        	}elseif($c['manage']['do']=='edit'){
        		//第三方代码编辑
        		$TId=(int)$_GET['TId'];
        		$third_row=str::str_code(db::get_one('third', "TId='$TId'"));
        	   ?>
        		<script type="text/javascript">$(document).ready(function(){set_obj.seo_third_edit_init()});</script>
        		<form id="third_edit_form" class="global_form">
                    <a href="javascript:history.back(-1);" class="return_title">
                        <span class="return">{/module.set.third/}</span> 
                        <span class="s_return">/ <?=$TId ? '{/global.edit/}' : '{/global.add/}'; ?></span>
                    </a>
                    <div class="rows">
                        <label>{/global.title/}</label>
                        <div class="input"><input name="Title" value="<?=$third_row['Title'];?>" type="text" class="box_input" size="53" maxlength="100" notnull /></div>                        
                    </div>
                    <div class="rows">
                        <label>{/seo.code/}</label>
                        <div class="input"><textarea name='Code' class="box_textarea" notnull><?=$third_row['Code'];?></textarea></div>                        
                    </div>
                    <div class="rows">
                        <label>{/seo.third.code_type/}:</label>
                        <div class="input"><?=ly200::form_select($c['manage']['lang_pack']['seo']['third']['code_type_ary'], 'CodeType', (int)$third_row['CodeType'], '', '', '', 'class="box_input"');?></div>                        
                    </div>
                    <div class="rows">
                        <label></label>
                        <div class="input">
                            <span class="input_checkbox_box <?=$third_row['IsMeta']==1?'checked':'';?>">
                                <span class="input_checkbox">
                                    <input type="checkbox" name="IsMeta" <?=$third_row['IsMeta']==1?'checked="checked"':'';?> value="1">
                                </span>{/seo.third.is_meta/}
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="input_checkbox_box <?=$third_row['IsBody']==1?'checked':'';?>">
                                <span class="input_checkbox">
                                    <input type="checkbox" name="IsBody" <?=$third_row['IsBody']==1?'checked="checked"':'';?> value="1">
                                </span>{/seo.third.is_body/}
                            </span>
                        </div>                        
                    </div>
                    <div class="rows">
                        <label></label>
                        <div class="input input_button">
                            <input type="button" class="btn_global btn_submit" value="{/global.save/}">
                            <a href="./?m=set&a=third"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
                        </div>                     
                    </div>
        			<input type="hidden" id="TId" name="TId" value="<?=$TId;?>" />
        			<input type="hidden" name="do_action" value="set.seo_third_edit" />
        		</form>
        	<?php } ?>
        </div>
    </div>
</div>