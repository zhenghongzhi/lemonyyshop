<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'review'));//检查权限

$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');

$permit_ary=array(
	'edit'	=>	manage::check_permit('products', 0, array('a'=>'review', 'd'=>'edit')),
	'del'	=>	manage::check_permit('products', 0, array('a'=>'review', 'd'=>'del'))
);
$top_id_name=($c['manage']['do']=='index'?'review':'review_inside');
?>
<div id="<?=$top_id_name; ?>" class="r_con_wrap">
	<?php
	if($c['manage']['do']=='index'){
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.review_init();});</script>
		<div class="inside_container">
			<h1>{/module.products.review/}</h1>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="review" />
					</form>
				</div>
			</div>
			<?php
			$Keyword=$_GET['Keyword'];
			$where='p.ReId=0';//条件
			if(!@in_array('custom_comments', (array)$c['manage']['plugins']['Used'])) $where.=' and p.AppDivision>-1';
			$page_count=30;//显示数量
			$Keyword && $where.=" and p.ProId in(select ProId from products where Name{$c['manage']['web_lang']} like '%$Keyword%')";
			$review_row=str::str_code(db::get_limit_page('products_review p left join orders o on p.OrderId=o.OrderId', $where, '*, o.OId', 'p.RId desc', (int)$_GET['page'], $page_count));
			$w="-1";
			$products_list=array();
			foreach($review_row[0] as $v){$w.=",{$v['ProId']}";}
			$products_row=db::get_all('products', "ProId in($w)", "ProId,Name{$c['manage']['web_lang']}");
			foreach($products_row as $v){$products_list[$v['ProId']]=str::str_code($v);}
			
			if($review_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="25%" nowrap="nowrap">{/products.products.pro_name/}</td>
							<td width="15%" nowrap="nowrap">{/products.review.customer_name/}</td>
							<td>{/products.review.content/}</td>
							<td width="10%" nowrap="nowrap">{/products.review.time/}</td>
							<?php if($review_cfg['display']==1){?><td width="10%" nowrap="nowrap">{/products.review.audit/}</td><?php }?>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($review_row[0] as $v){
							$products_row=$products_list[$v['ProId']];
							$url=$products_row?ly200::get_url($products_row, 'products', $c['manage']['web_lang']):'javascript:;';
							$name=$products_row?$products_row['Name'.$c['manage']['web_lang']]:'N/A';
						?>
							<tr>
								<?php if($permit_ary['del']){?><td><?=html::btn_checkbox('select', $v['RId']);?></td><?php }?>
								<td><a href="<?=$url;?>" target="_blank"><?=(int)$v['IsRead']==0?'<i class="icon_unread_message"></i>':'';?><?=$name;?></a></td>
								<td><?=$v['CustomerName'];?></td>
								<td><span class="star star_s<?=$v['Rating'];?>"></span><br> <?=str::str_code($v['Content'], 'htmlspecialchars_decode');?></td>
								<td><?=date('Y-m-d H:i:s', $v['AccTime']);?></td>
								<?php if($review_cfg['display']==1){?>
									<td class="review_box">
										<div class="switchery<?=(int)$v['Audit']?' checked':'';?>" data-id="<?=$v['RId'];?>">
											<input type="checkbox" name="Audit" value="1" <?=(int)$v['Audit']?' checked':'';?>>
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
									</td>
								<?php }?>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=review&d=reply&RId=<?=$v['RId'];?>">{/global.view/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down"><a class="del item" href="./?do_action=products.review_del&RId=<?=$v['RId'];?>" rel="del">{/global.del/}</a></dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($review_row[1], $review_row[2], $review_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(0);
			}?>
		</div>
	<?php
	}else{
		$RId=(int)$_GET['RId'];
		$review_row=str::str_code(db::get_one('products_review', "RId='$RId'"));
		!$review_row && js::location('./?m=products&a=review');
		(int)$review_row['IsRead']==0 && db::update('products_review', "RId='$RId'", array('IsRead'=>1));
		$products_row=db::get_one('products', "ProId='{$review_row['ProId']}'", '*');
		$rating_ary=explode(',', $review_row['Assess']);
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.review_init();});</script>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.products.review/}</span> 
				<span class="s_return">/ {/global.view/}</span>
			</a>
		</div>
		<div class="center_container_1200">	
			<div class="left_container">
				<div class="left_container_side">
					<div class="global_container">
						<form id="review_edit_form" class="global_form">
							<div class="review_box">
								<div class="star_box">
									<span class="star star_b<?=$review_row['Rating'];?>"></span>
									<?php if($review_row['Agree']){ ?><span class="agree"><?=$review_row['Agree'];?></span><?php } ?>
								</div>
								<div class="msg"><?=$review_row['Content'];?></div>
								<?php 
								$count=0;
								for($i=0; $i<3; ++$i){
									if(!$review_row['PicPath_'.$i] || !is_file($c['root_path'].$review_row['PicPath_'.$i])) continue;
									$count++;
								}
								if($count){
								?>
									<div class="r_img">
										<?php
										for($i=0; $i<3; ++$i){
											if(!$review_row['PicPath_'.$i] || !is_file($c['root_path'].$review_row['PicPath_'.$i])) continue;
										?>
											<div class="list"><a href="<?=$review_row['PicPath_'.$i];?>" target="_blank"><img src="<?=$review_row['PicPath_'.$i];?>" alt=""></a></div>
										<?php }?>
										<div class="clear"></div>
									</div>
								<?php }?>
								<div class="info">
									<?php
									if($review_cfg['display']==1){
									?>
										<div class="fr">
											{/products.review.audit/} &nbsp;&nbsp;
											<div class="switchery review<?=$review_row['Audit']?' checked':'';?>" data-id="<?=$RId;?>">
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										</div>
									<?php
									}
									echo $review_row['CustomerName'];
									echo $review_row['UserId']?'('.str::str_code(db::get_value('user', "UserId='{$review_row['UserId']}'", 'Email')).')':'';
									echo '<span>'.date('Y-m-d H:i:s', $review_row['AccTime']).'</span>';
									echo '<span>'.$review_row['Ip'].' 【'.ly200::ip($review_row['Ip']).'】</span>';
									?>
								</div>
							</div>
							<?php
							$reply_row=str::str_code(db::get_all('products_review', "ProId='{$review_row['ProId']}' and ReId='{$review_row['RId']}'", '*', 'RId asc'));
							$reply_len=count($reply_row);
							if($reply_len){
								foreach($reply_row as $v){
							?>
									<div class="review_box">
										<a href="./?do_action=products.review_del&RId=<?=$v['RId'];?>" rel="del" class="del icon_delete_1 fr"></a>
										<div class="msg"><?=$v['Content'];?></div>
										<div class="info">
											<?php
											if(!($v['CustomerName']=='Manager' && !$v['UserId']) && $review_cfg['display']==1){
											?>
												<div class="fr">
													{/products.review.audit/} &nbsp;&nbsp;
													<div class="switchery<?=$v['Audit']?' checked':'';?>" data-id="<?=$v['RId'];?>">
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</div>
											<?php
											}
											echo ($v['CustomerName']=='Manager' && !$v['UserId'])?'{/manage.manage.manager/}':$v['CustomerName'];
											echo ($v['CustomerName']!='Manager' && $v['UserId'])?'('.str::str_code(db::get_value('user', "UserId='{$v['UserId']}'", 'Email')).')':'';
											echo '<span>'.date('Y-m-d H:i:s', $v['AccTime']).'</span>';
											echo '<span>'.$v['Ip'].' 【'.ly200::ip($v['Ip']).'】</span>';
											?>
										</div>
									</div>
							<?php
								}
							}?>
							<div class="form_remark_log" parent_null>
								<div class="form_box">
									<div class="remark_left"><div><input type="text" class="box_input" name="ReviewComment" notnull parent_null="1" placeholder="{/global.please_enter/} ..."></div></div>
									<input type="button" class="btn_save btn_submit" value="{/products.review.reply/}">
								</div>
							</div>
							<input type="hidden" id="RId" name="RId" value="<?=$RId;?>" />
							<input type="hidden" name="ProId" value="<?=$review_row['ProId']; ?>" />
							<input type="hidden" name="do_action" value="products.review_reply" />
						</form>
					</div>
				</div>
			</div>
			<div class="right_container">
				<div class="global_container">
					<?php
					$order_row=db::get_one('orders', "OrderId='{$review_row['OrderId']}'");
					if($order_row){
						?>
						<div class="o_number"><a href="./?m=orders&a=orders&d=view&OrderId=<?=$review_row['OrderId'];?>" target="_blank">No: #<?=$order_row['OId'];?></a></div>
					<?php }?>
					<div class="product">
						<div class="img"><a href="<?=ly200::get_url($products_row,'products'); ?>" target="_blank"><img src="<?=ly200::get_size_img($products_row['PicPath_0'],'240x240'); ?>" alt=""></a></div>
						<div class="name"><a href="<?=ly200::get_url($products_row,'products'); ?>" target="_blank"><?=$products_row['Name'.$c['manage']['web_lang']]; ?></a></div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	<?php }?>
</div>