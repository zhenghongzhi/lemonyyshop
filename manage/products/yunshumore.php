<?php !isset($c) && exit();?>
<?php
    manage::check_permit('products', 1, array('a' => 'yunshu')); // 检查权限
    $shipping_express_row = str::str_code(db::get_all('shipping', 'IsUsed = 1', '*', $c['my_order'] . 'SId asc'));
?>
<form class="global_form" name="batch_move_form">
    <div class="global_container center_container_1000">
        <div class="rows clean">
            <label>产品分类</label>
            <div class="input">
                <div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
            </div>
        </div>
        <div class="rows clean">
            <label>物流</label>
            <div class="input">
                <div class="box_select">
                    <select name="SId" id="SId">
                        <?php foreach ($shipping_express_row as $key => $value){ ?>
                            <option value="<?=$value['SId']; ?>"><?=$value['Express']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="rows clean">
            <label></label>
            <div class="input">
<!--                <input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />-->
                <a class="btn_global btn_submit" id="zhzsubmit">{/global.submit/}</a>
                <a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
            </div>
        </div>
    </div>
    <input type="hidden" name="do_action" value="products.products_batch_move" />
</form>
<script>
    $('#zhzsubmit').click(function(){
        var CateId = $("select[name=CateId]").val();
        var SId = $("#SId").val();
        if (!CateId){
            alert('请选择分类');
            return false;
        }
        $.post('?do_action=products.moreshipping', {'CateId':CateId,'SId':SId}, function(data){
            alert(data.msg);
            window.location.reload();
        }, 'json');

    })

</script>