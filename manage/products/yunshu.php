<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a' => 'yunshu')); // 检查权限
if ($c['manage']['do'] == 'yunshu') {
    $ProId = (int) $_GET['ProId'];
    $shipping_express_row = str::str_code(db::get_all('shipping', '1', '*', $c['my_order'] . 'SId asc'));
    $checkrows = str::str_code(db::get_all('product_shipping', "products_id='".$ProId."'", '*'));
    foreach ($shipping_express_row as $k => &$v)
    {
        $ischeck=false;
        foreach ($checkrows as $ck=>$cv)
        {
            if($v["SId"]==$cv["shipping_id"])
            {
                $ischeck=true;
                break;
            }
        }
        $v['IsUsed']=$ischeck;
    }
}
echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
$permit_ary['edit'] =1;
//$permit_ary = array(
//    'edit' => manage::check_permit('products', 0, array(
//        'a' => 'shipping',
//        'd' => 'express',
//        'p' => 'edit'
//    ))
//
//);

?>
<style type="text/css">
#main .home_righter .new {
	margin-left: 0px;
}
</style>

<div id="righter" class="righter home_righter new"
	style="width: 1336px;">
	<div id="shipping" class="r_con_wrap" style="height: 358px;">
		<div class="center_container">
			<div class="global_container global_form">
				<div class="big_title">
					<a href="javascript:history.back(-1);" class="return_title"> <span
						class="return">产品管理</span> <span class="s_return">/ 运输方式</span>
					</a>
					<input type="hidden" id="pid" value=<?=$ProId; ?>>
				</div>
				<div class="blank20"></div>
				<div class="config_table_body">
				<?php
    foreach ($shipping_express_row as $k => &$v) {
        
        ?>
							<div class="table_item" data-id="<?=$v['SId'];?>">
						<table border="0" cellpadding="5" cellspacing="0"
							class="config_table r_con_table">
							<thead>
								<tr>
									<td nowrap="nowrap" class="myorder"><?php /*<span class="icon_myorder"></span>*/ ?></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="55%" nowrap="nowrap" class="myorder_left">
										<div class="info">
											<div class="name"><?=$v['Express'];?></div>
										</div>
										<div class="img img_big">
											<img src="<?=$v['Logo'];?>" alt="<?=$v['Express'];?>" /><span></span>
										</div>
									</td>
									<td width="25%" nowrap="nowrap" class="myorder_left"></td>

									<td width="10%" nowrap="nowrap" align="center"
										class="express_used">
											
												<?php
                                            if ($permit_ary['edit']) {
                                        ?>
													<div class="switchery<?=$v['IsUsed']?' checked':'';?>"
											data-id="<?=$v['SId'];?>">
											<input type="checkbox" name="IsUsed" value="1"
												<?=$v['IsUsed']?' checked':'';?>>
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
												<?php
                                            } else {
                                                echo $v['IsUsed'] ? '{/global.n_y.1/} <br />' : '';
                                            }
                                             ?>
											</td>
								</tr>
							</tbody>
						</table>
					</div>
						<?php }?>
				
				</div>
				<br>
				<div id="fixed_right">
					<div class="global_container insurance_edit"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$(function(){
	$('.express_used .switchery').click(function(){
		var $sid=$(this).attr('data-id'),
		$used;
	if($(this).hasClass('checked')){
		$(this).removeClass('checked');
		$used=0;
	}else{
		$(this).addClass('checked');
		$used=1;
	}
	var pid=$("#pid").val();
	    $.post('?do_action=products.shipping_products_used', {'SId':$sid,'IsUsed':$used,'Pid':pid}, function(){}, 'json');
	});
});
</script>
