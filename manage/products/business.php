<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'business'));//检查权限
if(!in_array('business', $c['manage']['plugins']['Used'])){//检查应用状态
	manage::no_permit(1);
}

$out=0;
$open_ary=array();
foreach((array)$c['manage']['permit']['products']['business']['menu'] as $k=>$v){
	if(!manage::check_permit('products', 0, array('a'=>'business', 'd'=>$v))){
		if($v=='business' && $c['manage']['do']=='index') $out=1;
		continue;
	}else{
		$v=='business' && $v='index';
		$open_ary[]=$v;
	}
}
if($out) js::location('?m=products&a=business&d='.$open_ary[0]);//当第一个选项没有权限打开，就跳转能打开的第一个页面

$cate_ary=str::str_code(db::get_all('business_category', '1'));//获取类别列表
$category_ary=array();
foreach((array)$cate_ary as $v){
	$category_ary[$v['CateId']]=$v;
}
$category_count=count($category_ary);
unset($cate_ary);

$CateId=(int)$_GET['CateId'];
$mycateid=(int)$_GET['mycateid'];
if($CateId){
	$category_one=str::str_code(db::get_one('business_category', "CateId='$CateId'"));
	$UId=$category_one['UId'];
	if($c['manage']['do']=='index'){
		$column=$category_one['Category'];
	}
}

$permit_ary=array(
	'add'		=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'business', 'p'=>'add')),
	'edit'		=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'business', 'p'=>'edit')),
	'del'		=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'business', 'p'=>'del')),
	'cate_add'	=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'category', 'p'=>'add')),
	'cate_edit'	=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'category', 'p'=>'edit')),
	'cate_del'	=>	manage::check_permit('products', 0, array('a'=>'business', 'd'=>'category', 'p'=>'del'))
);
$top_id_name=(($c['manage']['do']=='index' || $c['manage']['do']=='category')?'business':'business_inside');
echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');

$cate = db::get_all("business_cate","uid = '0,'");

?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php if($c['manage']['do']=='index' || $c['manage']['do']=='category'){ ?>
		<div class="inside_container">
			<h1>{/module.products.business.module_name/}</h1>
			<ul class="inside_menu">
				<?php if(manage::check_permit('products', 0, array('a'=>'business', 'd'=>'business'))){?>
					<li><a href="./?m=products&a=business"<?=($c['manage']['do']=='index' || $c['manage']['do']=='edit')?' class="current"':'';?>>{/module.products.business.module_name/}<?=$column?" ($column)":'';?></a></li>
				<?php }?>
				<?php if(manage::check_permit('products', 0, array('a'=>'business', 'd'=>'category'))){?>
					<li><a href="./?m=products&a=business&d=category"<?=($c['manage']['do']=='category' || $c['manage']['do']=='category_edit')?' class="current"':'';?>>{/global.category/}</a></li>
				<?php }?>
			</ul>
		</div>
	<?php }?>
	<?php
	if($c['manage']['do']=='index'){
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.business_init()});</script>
		<div class="inside_table clean">
			<div class="list_menu">
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'business_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
								</div>
							</div>
                            <div class="rows item clean">
                                <label>供应商分类</label>
                                <div class="input">
                                    <div class="box_select">
                                        <select name="mycateid" id="">
                                            <option value="0">请选择</option>
                                            <? foreach ($cate as $key=>$value){ ?>
                                                <option value="<?=$value['id']; ?>" <?=$mycateid==$value['id']?'selected':''; ?>><?=$value['name']; ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="business" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=products&a=business&d=edit">{/global.add/}</a></li><?php }?>
                    <?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
                    <li><a class="upload" href="./?m=products&a=business&d=upload">导入</a></li>
                    <li><a class="gongyingshang">更新供应商信息</a></li>
				</ul>
			</div>
            <script>
                var zong = 0;
                function digui(page){
                    $.ajax({
                        type: "GET",
                        url: "./?do_action=products.businessapi",
                        data: {"page":page},
                        dataType: "json",
                        success: function(data){
                            alert(data.msg);
                        }
                    });
                }
                $('.gongyingshang').click(function () {
                    if(confirm('确定更新供应商信息吗？')){
                        $.ajax({
                            type: "GET",
                            url: "./?do_action=products.businessapi",
                            data: 1,
                            dataType: "json",
                            success: function(data){
                                alert(data.msg);
                            }
                        });
                    }
                })
            </script>
			<?php
			$Keyword=str::str_code($_GET['Keyword']);
			$where='1';//条件
			$page_count=20;//显示数量
			$Keyword && $where.=" and Name like '%$Keyword%'";
            $CateId && $where.=" and CateId='$CateId'";
            $mycateid && $where.=" and mycateid='$mycateid'";
			$business_row=str::str_code(db::get_limit_page('business', $where, '*', $c['my_order'].'BId desc', (int)$_GET['page'], $page_count));
			if($business_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
                            <td width="16%" nowrap="nowrap">{/global.title/}</td>
                            <td width="16%" nowrap="nowrap">编码</td>
							<td width="16%" nowrap="nowrap">{/products.global.subjection/}</td>
<!--                            <td width="33%" nowrap="nowrap">{/business.business.url/}</td>-->
                            <td width="16%" nowrap="nowrap">来源</td>
                            <td width="16%" nowrap="nowrap">分类</td>
                            <td width="16%" nowrap="nowrap">产品数量 上架/下架</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach((array)$business_row[0] as $v){
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['BId']);?></td><?php }?>
                                <td><?=$v['Name'];?></td>
                                <td><?=$v['memberNo'];?></td>
								<td class="category_select" cateid="<?=$v['CateId'];?>">
									<?php
									$UId=$category_ary[$v['CateId']]['UId'];
									if($UId){
										$key_ary=@explode(',',$UId);
										array_shift($key_ary);
										array_pop($key_ary);
										foreach((array)$key_ary as $k2=>$v2){
											echo $category_ary[$v2]['Category'].'->';
										}
									}
									echo $category_ary[$v['CateId']]['Category'];
									?>
								</td>
                                <td>
                                    <? if ($v['type']==1){
                                        echo '蚂蚁搬家';
                                    }elseif($v['type']==2){
                                        echo '系统添加数据';
                                    }else{
                                        echo '导入数据';
                                    } ?>
                                </td>
                                <td>
                                    <?php
                                        if ($v['mycateid']){
                                            $k = db::get_one('business_cate',"id in(0".$v['mycateid']."0)","group_concat(name) as name");
                                            echo $k['name'];
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?
                                        $shangjiacount = db::get_one("business_product","is_show = 1 and pid = ".$v['BId'],'count(*) as count');
                                        $xiajiacount = db::get_one("business_product","is_show = 2 and pid = ".$v['BId'],'count(*) as count');

                                    ?>
                                    <a href=""><? echo $shangjiacount['count'].'/'.$xiajiacount['count']; ?></a>
                                </td>
<!--								<td nowrap="nowrap">--><?//=$v['Url']?'<a href="'.$v['Url'].'" target="_blank">'.$v['Url'].'</a>':'';?><!--</td>-->
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=business&d=edit&BId=<?=$v['BId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.business_del&BId=<?=$v['BId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
								<td></td>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($business_row[1], $business_row[2], $business_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=products&a=business&d=edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='edit'){
		//供应商编辑页
		$BId=(int)$_GET['BId'];
		if($BId){
			$business_one=db::get_one('business', "BId='$BId'");
		}
        //一级
        $one = db::get_all("business_cate","is_del = 1 and uid = '0,'");

	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.business_edit_init()});</script>
		<div class="center_container">
			<div class="global_container">
				<form id="business_edit_form" class="global_form wrap_content">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/business.business.business/}</span> 
						<span class="s_return">/ <?=$BId?'{/global.edit/}':'{/global.add/}';?></span>
					</a>
                    <div class="rows clean">
                        <label>会员号</label>
                        <div class="input"><input type='text' name='memberNo' value='<?=$business_one['memberNo'];?>' class='box_input' size='53' <?=$BId?'readonly':''; ?>  notnull></div>
                    </div>
                    <div class="rows clean">
                        <label>logo</label>
                        <div class="input">
<!--                            --><?//=manage::multi_img('logoDetail', 'logoPath', $business_one['logoPath']); ?>
                            <?=manage::multi_img('LogoDetail', 'logoPath', $business_one['logoPath']); ?>
                        </div>
                    </div>
					<div class="rows clean">
						<label>{/products.name/}</label>
						<div class="input"><input type='text' name='Name' value='<?=$business_one['Name'];?>' class='box_input' size='53'  notnull></div>						
					</div>
                    <div class="rows clean">
                        <label>{/products.name/}(英文版)</label>
                        <div class="input"><input type='text' name='enname' value='<?=$business_one['enname'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>所属产品分类</label>
                        <div class="input">
                            <?php
                            $arr = explode(',',$business_one['mycateid']);
                            foreach ($one as $val){ ?>
                                <input type="checkbox" name="mycateid[]" value="<?=$val['id']; ?>" id="<?=$val['id']; ?>" <?=in_array($val['id'], $arr)?"checked":"";  ?>>
                                <label for="<?=$val['id']; ?>"><?=$val['name']; ?></label>
                            <? } ?>

                        </div>
                    </div>
					<div class="rows clean">
						<label>{/business.business.url/}</label>
						<div class="input"><input type='text' name='Url' value='<?=$business_one['Url'];?>' class='box_input' size='53' ></div>
					</div>
					<?php if($BId){?>
						<div class="rows clean">
							<label>{/manage.manage.create_time/}</label>
							<div class="input"><?=date('Y-m-d H:i:s', $business_one['AccTime']);?></div>							
						</div>
					<?php }?>
					<div class="rows clean myorder_rows">
						<label>{/products.myorder/}</label>
						<div class="input">
							<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $business_one['MyOrder'], '', '', '', 'class="box_input"');?></div>
						</div>
					</div>
<!--					<div class="rows clean">-->
<!--						<label>{/business.business.classify/}</label>-->
<!--						<div class="input">-->
<!--							<div class="box_select">--><?//=category::ouput_Category_to_Select('CateId', $business_one['CateId'], 'business_category', 'UId="0,"', 1, 'notnull class="box_input"', '{/global.select_index/}');?><!--</div>-->
<!--						</div>-->
<!--					</div>-->
					<div class="rows clean">
						<label>{/business.business.address/}</label>
						<div class="input"><input type='text' name='Address' value='<?=$business_one['Address'];?>' class='box_input' size='53' notnull></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.remark/}</label>
						<div class="input"><input type='text' name='Remark' value='<?=$business_one['Remark'];?>' class='box_input' size='53'></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.qualification/}</label>
						<div class="input">
							<?=manage::multi_img('ImgDetail', 'ImgPath', $business_one['ImgPath']); ?>				
						</div>
					</div>
					<div class="rows clean">
						<label>{/business.business.voucher/}</label>
						<div class="input">
							<?=manage::multi_img('PicDetail', 'PicPath', $business_one['PicPath']); ?>
						</div>					
					</div>
					<div class="rows clean">
						<label>{/business.business.entity/}</label>
						<div class="input"><input type='text' name='Entity' value='<?=$business_one['Entity'];?>' class='box_input' size='50'></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.contacts/}</label>
						<div class="input"><input type='text' name='Contacts' value='<?=$business_one['Contacts'];?>' class='box_input' size='50'></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.phone/}</label>
						<div class="input"><input type='text' name='Phone' value='<?=$business_one['Phone'];?>' class='box_input' size='50' maxlength='30'></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.telephone/}</label>
						<div class="input"><input type='text' name='Telephone' value='<?=$business_one['Telephone'];?>' class='box_input' size='50'></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.fax/}</label>
						<div class="input"><input type='text' name='Fax' value='<?=$business_one['Fax'];?>' class='box_input' size='50' ></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.qq/}</label>
						<div class="input"><input type='text' name='QQ' value='<?=$business_one['QQ'];?>' class='box_input' size='50' rel="amount"></div>						
					</div>
                    <div class="rows clean">
                        <label>标签</label>
                        <div class="input">
                            <select name="laabel" id="">
                                <option value="0">请选择</option>
                                <option value="合作伙伴" <?=$business_one['laabel']=='合作伙伴'?"selected":""; ?>>合作伙伴</option>
                                <option value="平台推荐" <?=$business_one['laabel']=='平台推荐'?"selected":""; ?>>平台推荐</option>
                                <option value="信誉保障" <?=$business_one['laabel']=='信誉保障'?"selected":""; ?>>信誉保障</option>
                            </select>
                        </div>
                    </div>

                    <div class="rows clean">
                        <label>城市</label>
                        <div class="input"><input type='text' name='city' value='<?=$business_one['city'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>开户银行</label>
                        <div class="input"><input type='text' name='openBank' value='<?=$business_one['openBank'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>开户支行</label>
                        <div class="input"><input type='text' name='openBranch' value='<?=$business_one['openBranch'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>开户名称</label>
                        <div class="input"><input type='text' name='openName' value='<?=$business_one['openName'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>银行账号</label>
                        <div class="input"><input type='text' name='bankNo' value='<?=$business_one['bankNo'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>产品关键字</label>
                        <div class="input"><input type='text' name='companyDesc' value='<?=$business_one['companyDesc'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>产品关键字（英文）</label>
                        <div class="input"><input type='text' name='encompanyDesc' value='<?=$business_one['encompanyDesc'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>产品关键字（波斯）</label>
                        <div class="input"><input type='text' name='facompanyDesc' value='<?=$business_one['facompanyDesc'];?>' class='box_input' size='53'></div>
                    </div>
                    <div class="rows clean">
                        <label>产品</label>
<!--                        <div class="input">--><?//=manage::form_edit($products_description_row, 'editor', 'product');?><!--</div>-->
                        <div class="input"><?=manage::Editor('product',$business_one['product']);?></div>
                    </div>
<!--                    <div class="rows clean">-->
<!--                        <label>排序</label>-->
<!--                        <div class="input"><input type='text' name='sort' value='--><?//=$business_one['sort'];?><!--' class='box_input' size='53'></div>-->
<!--                    </div>-->
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=products&a=business" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>			
					</div>
                    <input type="hidden" name="type" value="<?=$BId?$business_one['type']:2;?>">
					<input type="hidden" name="BId" value="<?=$BId;?>" />
					<input type="hidden" name="do_action" value="products.business_edit" />
				</form>
			</div>
		</div>
	<?php
	}elseif($c['manage']['do']=='category'){
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.business_category_init()});</script>
		<div class="inside_table clean">
			<div class="list_menu">
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'business_category', 'UId="0,"', 'Dept<2', '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="business" />
						<input type="hidden" name="d" value="category" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if($permit_ary['cate_add']){?><li><a class="add" href="./?m=products&a=business&d=category_edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['cate_del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
			</div>
			<?php
			$where='1';//条件
			$page_count=50;//显示数量
			if($CateId){
				$where.=" and UId='{$UId}{$CateId},'";
			}
			$Keyword=str::str_code($_GET['Keyword']);
			$Keyword && $where.=" and Category like '%$Keyword%'";
			$category_row=str::str_code(db::get_limit_page('business_category', $where, '*', $c['my_order'].'CateId asc', (int)$_GET['page'], $page_count));
			
			if($category_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['cate_del']){?><td width="6%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="43%" nowrap="nowrap">{/products.products_category.category_name/}</td>
							<td width="43%" nowrap="nowrap">{/products.global.subjection/}</td>
							<?php if($permit_ary['cate_edit'] || $permit_ary['cate_del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($category_row[0] as $v){
						?>
							<tr>
								<?php if($permit_ary['cate_del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['CateId']);?></td><?php }?>
								<td><?=$v['Category'];?></td>
								<td class="category_select" cateid="<?=$v['CateId'];?>">
									<?php
									$UId=$category_ary[$v['CateId']]['UId'];
									if($UId){
										if($UId=='0,'){
											echo '--';
										}else{
											$key_ary=@explode(',',$UId);
											array_shift($key_ary);
											array_pop($key_ary);
											foreach((array)$key_ary as $k2=>$v2){
												echo $category_ary[$v2]['Category'];
											}
										}
									}
									?>
								</td>
								<?php if($permit_ary['cate_edit'] || $permit_ary['cate_del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['cate_edit']){?><a href="./?m=products&a=business&d=category_edit&CateId=<?=$v['CateId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['cate_del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['cate_del']){?><a class="del item" href="./?do_action=products.business_category_del&CateId=<?=$v['CateId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
								<td></td>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($category_row[1], $category_row[2], $category_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=products&a=business&d=category_edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='category_edit'){
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.business_category_edit_init()});</script>
		<div class="center_container">
			<div class="global_container">
				<form id="category_edit_form" class="global_form wrap_content">
					<a href="javascript:history.back(-1);" class="return_title">
						<span class="return">{/business.business.classify/}</span> 
						<span class="s_return">/ <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
					</a>
					<div class="rows clean">
						<label>{/business.business.classify/}</label>
						<div class="input"><input name="Category" value="<?=$category_one['Category'];?>" type="text" class="box_input" maxlength="100" size="50" notnull> <font class="fc_red">*</font></div>						
					</div>
					<div class="rows clean">
						<label>{/business.business.children/}</label>
						<div class="input">
							<div class="box_select"><?=category::ouput_Category_to_Select('UnderTheCateId', category::get_CateId_by_UId($category_one['UId']), 'business_category', 'UId="0,"', "CateId!='{$category_one['CateId']}' and Dept<2", 'class="box_input"', '{/global.select_index/}');?></div>		
						</div>				
					</div>
					<div class="rows clean myorder_rows">
						<label>{/products.myorder/}</label>
						<div class="input">
							<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder', $category_one['MyOrder'], '', '', '', 'class="box_input"');?></div>
						</div>
					</div>
					<div class="rows clean">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}">
							<a href="./?m=products&a=business&d=category" title="{/global.return/}"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
						</div>			
					</div>
					<input type="hidden" name="CateId" value="<?=$CateId;?>" />
					<input type="hidden" name="do_action" value="products.business_category_edit" />
				</form>
			</div>
		</div>
	<?php }elseif($c['manage']['do']=='upload'){
        echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
    ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.bupload_init()});</script>
        <div class="center_container_1200 center_container">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">供应商管理</span>
                <span class="s_return">/ {/products.global.upload/}</span>
            </a>
            <form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <div class="global_container">
                    <div id="form_container">
                        <div class="explode_box">
                            <div class="box_item">
                                <div class="tit">{/global.upload_table/}</div>
                                <div class="desc">{/global.submit_table/}</div>
                                <div class="input upload_file">
                                    <input name="ExcelFile" value="" type="text" class="box_input" id="excel_path" size="50" maxlength="100" readonly notnull />
                                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                    <div class="row fileupload-buttonbar">
										<span class="btn_file btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>{/global.file_upload/}</span>
											<input type="file" name="Filedata" multiple>
										</span>
                                        <div class="fileupload-progress fade"><div class="progress-extended"></div></div>
                                        <div class="clear"></div>
                                        <div class="photo_multi_img template-box files"></div>
                                        <div class="photo_multi_img" id="PicDetail"></div>
                                    </div>
                                    <script id="template-upload" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										<div class="template-upload fade">
											<div class="clear"></div>
											<div class="items">
												<p class="name">{%=file.name%}</p>
												<strong class="error text-danger"></strong>
											</div>
											<div class="items">
												<p class="size">Processing...</p>
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
											</div>
											<div class="items">
												{% if (!i) { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
									</script>
                                    <script id="template-download" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										{% if (file.thumbnailUrl) { %}
											<div class="pic template-download fade hide">
												<div>
													<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
													<a href="{%=file.url%}" class="zoom" target="_blank"></a>
													{% if (file.deleteUrl) { %}
														<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } %}
													<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
												</div>
												<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
											</div>
										{% } else { %}
											<div class="template-download fade hide">
												<div class="clear"></div>
												<div class="items">
													<p class="name">
														{% if (file.url) { %}
															<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
														{% } else { %}
															<span>{%=file.name%}</span>
														{% } %}
													</p>
													{% if (file.error) { %}
														<div><span class="label label-danger">Error</span> {%=file.error%}</div>
													{% } %}
												</div>
												<div class="items">
													<span class="size">{%=o.formatFileSize(file.size)%}</span>
												</div>
												<div class="items">
													{% if (file.deleteUrl) { %}
														<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
															<i class="glyphicon glyphicon-trash"></i>
															<span>{/global.del/}</span>
														</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } else { %}
														<button class="btn_file btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>{/global.cancel/}</span>
														</button>
													{% } %}
												</div>
												<div class="clear"></div>
											</div>
										{% } %}
									{% } %}
									</script>
                                </div>
                                <div class="rows clean">
                                    <label></label>
                                    <div class="input input_button">
                                        <input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
                                        <a href="./?m=products&a=business_cate" class="btn_global btn_cancel">{/global.return/}</a>
                                        <input type="button" class="btn_global btn_picture" value="" style="display:none;" />
                                        <input type="hidden" name="do_action" value="products.bupload" />
                                        <input type="hidden" name="Number" value="0" />
                                        <input type="hidden" name="Current" value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="progress_container">
                        <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                            <thead>
                            <tr>
                                <td width="50%" nowrap="nowrap">分类名</td>
                                <td width="50%" nowrap="nowrap">状态</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="progress_loading"></div>
                    </div>
                </div>
            </form>
        </div>
    <?php } ?>
</div>