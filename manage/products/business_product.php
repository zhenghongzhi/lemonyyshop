<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'business_product'));//检查权限
$Keyword=$_GET['Keyword'];
$CateId=(int)$_GET['CateId'];
$cid=(int)$_GET['cid'];
$is_show=(int)$_GET['is_show'];
$pid=(int)$_GET['pid'];
//产品
//供应商


//产品分类
$business_cate = db::get_all("business_cate","is_del = 1 and uid = '0,'",'id,name,uid');
foreach ($business_cate as $key=>$item){
    $erji = db::get_all("business_cate","is_del = 1 and uid = '".$item['uid'].$item['id'].",'",'id,name,uid');
    foreach ($erji as $k => $it){
        $sanji = db::get_all("business_cate","is_del = 1 and uid = '".$it['uid'].$it['id'].",'",'id,name,uid');
        $erji[$k]['son'] = $sanji;
    }
    $business_cate[$key]['son'] = $erji;
}

$permit_ary=array(
    'add'	=>	manage::check_permit('products', 0, array('a'=>'business_product', 'd'=>'add')),
    'edit'	=>	manage::check_permit('products', 0, array('a'=>'business_product', 'd'=>'edit')),
    'del'	=>	manage::check_permit('products', 0, array('a'=>'business_product', 'd'=>'del'))
);

$top_id_name=($c['manage']['do']=='index'?'category':'category_inside');

echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');

?>
<link rel="stylesheet" type="text/css" href="jquery-ui-multiselect-widget-master/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-multiselect-widget-master/demos/assets/style.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-multiselect-widget-master/demos/assets/prettify.css" />
<script type="text/javascript" src="http://lib.sinaapp.com/js/jquery-ui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="jquery-ui-multiselect-widget-master/src/jquery.multiselect.js"></script>
<script type="text/javascript" src="jquery-ui-multiselect-widget-master/i18n/jquery.multiselect.zh-cn.js"></script>
<script type="text/javascript" src="jquery-ui-multiselect-widget-master/demos/assets/prettify.js"></script>

<script type="text/javascript" src="jquery-ui-multiselect-widget-master/src/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="jquery-ui-multiselect-widget-master/i18n/jquery.multiselect.filter.zh-cn.js"></script>
<style>
    .ui-multiselect-menu{
        background: #ddd !important;
    }
</style>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
    <? if ($c['manage']['do']=='index'){
        $where = 1;
        if($Keyword){
            $where .= " and bp.name like '%".$Keyword."%' and bp.name like '%".$Keyword."%'";
        }
        $page_count = 20;
        $cid && $where.=" and bp.cid='$cid'";
        $is_show && $where.=" and bp.is_show='$is_show'";
        $pid && $where.=" and bp.pid='$pid'";
        $product_row = db::get_limit_page('business_product as bp left join business_cate as bc on bp.cid = bc.id left join business as b on b.BId = bp.pid', $where, 'bp.PicPath_0,bp.id,bp.name,bp.sku,bp.msku,bp.money,bp.startqty,bp.is_show,bc.name as cname,b.Name', 'bp.id desc', (int)$_GET['page'], $page_count);
        ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.gysproducts_init();});</script>
        <div class="inside_container">

        </div>
        <div class="inside_table">
            <div class="list_menu">
                <div class="search_form">
                    <form method="get" action="?">
                        <div class="k_input">
                            <input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
                            <input type="button" value="" class="more" />
                        </div>
                        <input type="submit" class="search_btn" value="{/global.search/}" />
                        <div class="ext drop_down">
                            <div class="rows item clean">
                                <label>产品分类</label>
                                <div class="input">
                                    <div class="box_select">
                                        <select name="cid" id="listcid">
                                            <option value="0">请选择</option>
                                            <?php foreach ($business_cate as $value) { ?>
                                                <option value="<?=$value['id']; ?>" <?=$value['id']==$cid?'selected':''; ?>>&nbsp;┝<?=$value['name']; ?></option>
                                                <?php if ($value['son']){
                                                    foreach ($value['son'] as $val){ ?>
                                                        <option value="<?=$val['id']; ?>" <?=$val['id']==$cid?'selected':''; ?>>&nbsp;｜┝<?=$val['name']; ?></option>
                                                        <?php if ($val['son']){
                                                            foreach ($val['son'] as $v){ ?>
                                                                <option value="<?=$v['id']; ?>" <?=$v['id']==$cid?'selected':''; ?>>&nbsp;｜┝<?=$v['name']; ?></option>
                                                            <?php }
                                                        }?>
                                                    <? }
                                                } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="rows item clean">
                                <label>上下架</label>
                                <div class="input">
                                    <div class="box_select">
                                        <select name="is_show" id="">
                                            <option value="0">请选择</option>
                                            <option value="1" <?=$is_show==1?'selected':''; ?>>上架</option>
                                            <option value="2" <?=$is_show==2?'selected':''; ?>>下架</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="rows item clean">
                                <label>供应商</label>
                                <div class="input">
                                    <div class="box_select">
                                        <select name="pid" id="listpid" notnull="">
                                            <option value="0">请选择</option>
                                            <?php foreach ($business as $value) { ?>
                                                <option value="<?=$value['BId']; ?>" <?=$value['BId']==$pid?'selected':''; ?>><?=$value['Name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <input type="hidden" name="m" value="products" />
                        <input type="hidden" name="a" value="business_product" />
                    </form>
                </div>
                <ul class="list_menu_button">
                    <li><a class="add" href="./?m=products&a=business_product&d=edit">添加</a></li>
                    <li><a class="sold_out" href="javascript:;">下架</a></li>
                    <li><a class="sold_in" href="javascript:;">上架</a></li>
                    <li><a class="upload" href="./?m=products&a=business_product&d=upload">导入</a></li>
                    <li><a class="zhzdel" href="javascript:;">删除</a></li>
                </ul>
                <div class="search_form">
                    <form method="get" action="?">
                        <input type="hidden" name="m" value="products">
                        <input type="hidden" name="a" value="business_product">
                    </form>
                </div>
            </div>
            <? if($product_row[0]){ ?>
                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                    <tr>
                        <td width="1%" nowrap="nowrap">
                            <?=html::btn_checkbox('select_all');?>
                        </td>
                        <td width="12.5%" nowrap="nowrap">图片</td>
                        <td width="12.5%" nowrap="nowrap">供应商</td>
                        <td width="12.5%" nowrap="nowrap">名称 / 分类</td>
                        <td width="12.5%" nowrap="nowrap">编号</td>
                        <td width="12.5%" nowrap="nowrap">商家编号</td>
                        <td width="12.5%" nowrap="nowrap">参考价格</td>
                        <td width="12.5%" nowrap="nowrap">起定量</td>
                        <td width="12.5%" nowrap="nowrap">状态</td>
                        <td width="115" nowrap="nowrap" class="operation">操作</td>
                    </tr>
                    </thead>
                    <tbody data-listidx="0">
                        <? foreach ($product_row[0] as $key=>$item){
                                $img=ly200::get_size_img($item['PicPath_0'], end($c['manage']['resize_ary']['products']));

                            ?>
                            <tr data-id="<?=$item['id'];?>">
                                <td width="1%" nowrap="nowrap">
                                    <?=html::btn_checkbox('select', $item['id']);?>
                                </td>
                                <td width="12.5%" nowrap="nowrap" class="img"><img src="/static/themes/default/images/global/loading_oth.gif" data-src="<?=$img;?>" class="loading_img" /></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['Name']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['name'].'/'.$item['cname']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['sku']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['msku']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['money']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['startqty']; ?></td>
                                <td width="12.5%" nowrap="nowrap"><?=$item['is_show']==1?'上架':'下架'; ?></td>
                                <td width="115" nowrap="nowrap" class="operation">
                                    <a class="item" href="./?m=products&a=business_product&d=edit&ProId=<?=$item['id'];?>">{/global.edit/}</a>
                                </td>
                            </tr>
                        <? } ?>
                    </tbody>
                </table>
                <?=html::turn_page($product_row[1], $product_row[2], $product_row[3], '?'.ly200::query_string('page').'&page=');?>
            <? }else{
                echo html::no_table_data(($Keyword?0:1), './?m=products&a=business_product&d=edit');
            } ?>
        </div>
        <script>
            $("#listcid").multiselect({
                multiple: false,
                noneSelectedText: "请选择",
                selectedList: 1 } );
            $("#listcid").multiselect().multiselectfilter();
            $("#listpid").multiselect({
                multiple: false,
                noneSelectedText: "请选择",
                selectedList: 1 } );
            $("#listpid").multiselect().multiselectfilter();
        </script>
    <? }elseif($c['manage']['do']=='edit'){


    //$business = db::get_all("business","1","BId,Name",$c['my_order'].'BId desc');

            $pro_menu_ary=array('pic_info', 'classify_info', 'basic_info', 'sales_info', 'attr_info', 'attr_price_info', 'tag_info', 'seo_info', 'freight_info', 'platform_info');

            $ProId = $_GET['ProId'];
            $cate = db::get_one("business_product","id='{$ProId}'");
            echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
        ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.products_edit_init()});</script>
        <div class="center_container_1200">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">{/module.products.products/}</span>
                <span class="s_return">/ <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
            </a>
        </div>
        <form id="edit_form" class="global_form center_container_1200">
            <div class="left_container">
                <div class="left_container_side">
                    <div class="global_container">
                        <div class="big_title">{/products.products.basic_info/}</div>
                        <?php /***************************** 产品图片 Start *****************************/?>
                        <div class="global_container" data-name="<?=$pro_menu_ary[0];?>">
                            <div class="big_title">{/products.products.<?=$pro_menu_ary[0];?>/}</div>
                            <div class="rows clean">
                                <div class="input">
                                    <div class="tips"><?=str_replace(array('%count%', '%format%', '%size%'), array('12', 'jpg', '640*640'), $c['manage']['lang_pack']['notes']['new_pic_tips']);?></div>
                                    <div class="multi_img upload_file_multi pro_multi_img" id="PicDetail">

                                        <?php
                                        $j = 0;
                                        for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
                                            echo manage::multi_img_item('PicPath[]', $cate["PicPath_{$i}"], $i ,ly200::get_size_img($cate["PicPath_{$i}"], '240x240'));
                                            ++$j;
                                        }?>
                                        <?php if ($j < $c['prod_image']['count'] ){?>
                                            <dl class="img" num="<?=$j;?>">
                                                <dt class="upload_box preview_pic">
                                                    <input type="button" id="PicUpload_<?=$j;?>" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" />
                                                    <input type="hidden" name="PicPath[]" value="" data-value="" save="" />
                                                </dt>
                                            </dl>
                                        <?php }?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php /***************************** 产品图片 End *****************************/?>
                        <div class="rows clean">
                            <label>产品分类</label>
                            <div class="input">
                                <div class="box_select">
                                    <select name="cid" id="addcid" notnull="">
                                        <option value="0">请选择</option>
                                        <?php foreach ($business_cate as $value) { ?>
                                            <option value="<?=$value['id']; ?>" <?=$value['id']==$cate['cid']?'selected':''; ?>>&nbsp;┝<?=$value['name']; ?></option>
                                            <?php if ($value['son']){
                                                foreach ($value['son'] as $val){ ?>
                                                    <option value="<?=$val['id']; ?>" <?=$val['id']==$cate['cid']?'selected':''; ?>>&nbsp;｜┝<?=$val['name']; ?></option>
                                                    <?php if ($val['son']){
                                                        foreach ($val['son'] as $v){ ?>
                                                            <option value="<?=$v['id']; ?>" <?=$v['id']==$cate['cid']?'selected':''; ?>>&nbsp;｜┝<?=$v['name']; ?></option>
                                                        <?php }
                                                     }?>
                                                <? }
                                            } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="pid" value="<?=$cate['pid']?>">
<!--                        <div class="rows clean">-->
<!--                            <label>供应商</label>-->
<!--                            <div class="input">-->
<!--                                <div class="box_select">-->
<!--                                    <select name="pid" id="addpid" notnull="">-->
<!--                                        <option value="0">请选择</option>-->
<!--                                        --><?php //foreach ($business as $value) { ?>
<!--                                            <option value="--><?//=$value['BId']; ?><!--" --><?//=$value['BId']==$cate['pid']?'selected':''; ?><!-->--><?//=$value['Name']; ?><!--</option>-->
<!--                                        --><?php //} ?>
<!--                                    </select>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="rows clean">
                            <label>{/products.name/}</label>
                            <div class="input">
                                <input type="text" name="name" value="<?=$cate['name']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>{/products.name/}(英文)</label>
                            <div class="input">
                                <input type="text" name="name_en" value="<?=$cate['name_en']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>sku</label>
                            <div class="input">
                                <input type="text" name="sku" value="<?=$cate['sku']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>商家产品sku</label>
                            <div class="input">
                                <input type="text" name="msku" value="<?=$cate['msku']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>参考价格</label>
                            <div class="input">
                                <input type="text" name="money" value="<?=$cate['money']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>起定量</label>
                            <div class="input">
                                <input type="text" name="startqty" value="<?=$cate['startqty']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>交货时间</label>
                            <div class="input">
                                <input type="text" name="deliverytime" value="<?=$cate['deliverytime']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>材料</label>
                            <div class="input">
                                <input type="text" name="material" value="<?=$cate['material']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>规格属性</label>
                            <div class="input">
                                <input type="text" name="spec" value="<?=$cate['spec']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>关键字中文</label>
                            <div class="input">
                                <input type="text" name="seo" value="<?=$cate['seo']; ?>" class="box_input" size="53">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>关键字英文</label>
                            <div class="input">
                                <input type="text" name="seo_en" value="<?=$cate['seo_en']; ?>" class="box_input" size="53">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>关键字波斯语</label>
                            <div class="input">
                                <input type="text" name="seo_fa" value="<?=$cate['seo_fa']; ?>" class="box_input" size="53">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>详情</label>
                            <div class="input"><?=manage::Editor('content',$cate['content']);?></div>
                        </div>
                        <div class="rows clean">
                            <label>备注</label>
                            <div class="input">
                                <input type="text" name="remake" value="<?=$cate['remake']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <div class="center_container_1200">
                                <div class="input">
                                    <input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}" />
                                    <a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=business_product';?>" class="btn_global btn_cancel">{/global.return/}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="ProId" value="<?=$ProId;?>" />
            <input type="hidden" name="do_action" value="products.business_product_edit" />
        </form>
        <script>
            $("#addcid").multiselect({
                multiple: false,
                noneSelectedText: "请选择",
                selectedList: 1 } );
            $("#addcid").multiselect().multiselectfilter();
            $("#addpid").multiselect({
                multiple: false,
                noneSelectedText: "请选择",
                selectedList: 1 } );
            $("#addpid").multiselect().multiselectfilter();
        </script>
    <? }elseif($c['manage']['do']=='upload'){
        echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
    ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.bpupload_init()});</script>
        <div class="center_container_1200 center_container">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">产品管理</span>
                <span class="s_return">/ {/products.global.upload/}</span>
            </a>
            <form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <div class="global_container">
                    <div id="form_container">
                        <div class="explode_box">
                            <div class="box_item">
                                <div class="tit">{/global.upload_table/}</div>
                                <div class="desc">{/global.submit_table/}</div>
                                <div class="input upload_file">
                                    <input name="ExcelFile" value="" type="text" class="box_input" id="excel_path" size="50" maxlength="100" readonly notnull />
                                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                    <div class="row fileupload-buttonbar">
										<span class="btn_file btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>{/global.file_upload/}</span>
											<input type="file" name="Filedata" multiple>
										</span>
                                        <div class="fileupload-progress fade"><div class="progress-extended"></div></div>
                                        <div class="clear"></div>
                                        <div class="photo_multi_img template-box files"></div>
                                        <div class="photo_multi_img" id="PicDetail"></div>
                                    </div>
                                    <script id="template-upload" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										<div class="template-upload fade">
											<div class="clear"></div>
											<div class="items">
												<p class="name">{%=file.name%}</p>
												<strong class="error text-danger"></strong>
											</div>
											<div class="items">
												<p class="size">Processing...</p>
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
											</div>
											<div class="items">
												{% if (!i) { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
									</script>
                                    <script id="template-download" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										{% if (file.thumbnailUrl) { %}
											<div class="pic template-download fade hide">
												<div>
													<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
													<a href="{%=file.url%}" class="zoom" target="_blank"></a>
													{% if (file.deleteUrl) { %}
														<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } %}
													<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
												</div>
												<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
											</div>
										{% } else { %}
											<div class="template-download fade hide">
												<div class="clear"></div>
												<div class="items">
													<p class="name">
														{% if (file.url) { %}
															<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
														{% } else { %}
															<span>{%=file.name%}</span>
														{% } %}
													</p>
													{% if (file.error) { %}
														<div><span class="label label-danger">Error</span> {%=file.error%}</div>
													{% } %}
												</div>
												<div class="items">
													<span class="size">{%=o.formatFileSize(file.size)%}</span>
												</div>
												<div class="items">
													{% if (file.deleteUrl) { %}
														<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
															<i class="glyphicon glyphicon-trash"></i>
															<span>{/global.del/}</span>
														</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } else { %}
														<button class="btn_file btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>{/global.cancel/}</span>
														</button>
													{% } %}
												</div>
												<div class="clear"></div>
											</div>
										{% } %}
									{% } %}
									</script>
                                </div>
                                <div class="rows clean">
                                    <label></label>
                                    <div class="input input_button">
                                        <input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
                                        <a href="./?m=products&a=business_cate" class="btn_global btn_cancel">{/global.return/}</a>
                                        <input type="button" class="btn_global btn_picture" value="" style="display:none;" />
                                        <input type="hidden" name="do_action" value="products.zhzbpupload" />
                                        <input type="hidden" name="Number" value="0" />
                                        <input type="hidden" name="Current" value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="progress_container">
                        <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                            <thead>
                            <tr>
                                <td width="50%" nowrap="nowrap">分类名</td>
                                <td width="50%" nowrap="nowrap">状态</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="progress_loading"></div>
                    </div>
                </div>
            </form>
        </div>
    <? } ?>
</div>
