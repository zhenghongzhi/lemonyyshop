<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'sync'));//检查权限
@extract($_GET, EXTR_PREFIX_ALL, 'g');
$g_callback==1 && include('authorization.tips.php');//授权返回处理、提示页面
if(!$c['manage']['do'] || $c['manage']['do']=='index'){
	$c['manage']['do']='aliexpress';
}

if($c['FunVersion']<2 && !substr_count($c['manage']['do'], 'aliexpress')){
	manage::no_permit(1);
}
if($c['manage']['do']=='aliexpress' || $c['manage']['do']=='aliexpress_products_edit'){
    //速卖通
	$Account=$_SESSION['Manage']['Aliexpress']['Token']['Account'];
	(!$Account || !db::get_row_count('authorization', "Platform='aliexpress' and  Account='{$Account}'")) && $Account=aliexpress::set_default_authorization();
	
	$category_data=$grouplist_data=array();
	if($Account){
		$category_row=db::get_all('products_aliexpress_category', "isleaf=1 and AliexpressCateId in (select categoryId from products_aliexpress where Account='$Account' group by categoryId)", 'AliexpressCateId,AliexpressUId,AliexpressCategory', 'AliexpressUId asc');
		foreach($category_row as $v){$category_data[$v['AliexpressCateId']]=$v;}
		
		$group_row=db::get_all('products_aliexpress_grouplist', "AliexpressGroupId in (select GroupId from products_aliexpress where Account='$Account' group by GroupId)");
		foreach($group_row as $v){$grouplist_data[$v['AliexpressGroupId']]=$v;}
	}
		
	$Keyword=str::str_code($_GET['Keyword']);
	$categoryId=(int)$_GET['categoryId'];
	$GroupId=(int)$_GET['GroupId'];
	$category_select_html=ly200::form_select($category_data, 'categoryId', $categoryId, 'AliexpressCategory', 'AliexpressCateId', '{/global.select_index/}');

	$shop_row=db::get_all('authorization', "Platform='aliexpress'", '*');
}elseif($c['manage']['do']=='amazon' || $c['manage']['do']=='amazon_products_edit'){
    //亚马逊
	$Account=$_SESSION['Manage']['Amazon']['Account'];
	
	$Keyword=str::str_code($_GET['Keyword']);
	$shop_row=db::get_all('authorization', "Platform='amazon'", '*');
	
	if(!$Account || !db::get_row_count('authorization', "Platform='amazon' and  Account='{$Account}'")){
		$_SESSION['Manage']['Amazon']=$shop_row[0];
		$Account=$shop_row[0]['Account'];
	}
}elseif($c['manage']['do']=='wish' || $c['manage']['do']=='wish_products_edit'){
    //Wish
	$Account=$_SESSION['Manage']['Wish']['Account'];
	
	$Keyword=str::str_code($_GET['Keyword']);
	$shop_row=db::get_all('authorization', "Platform='wish'", '*');
	
	if(!$Account || !db::get_row_count('authorization', "Platform='wish' and Account='{$Account}'")){
		$_SESSION['Manage']['Wish']=$shop_row[0];
		$Account=$shop_row[0]['Account'];
	}
}elseif($c['manage']['do']=='shopify' || $c['manage']['do']=='shopify_products_edit'){
    //Shopify
	$Account=$_SESSION['Manage']['Shopify']['Account'];
	
	$Keyword=str::str_code($_GET['Keyword']);
	$shop_row=db::get_all('authorization', "Platform='shopify'", '*');
	
	if(!$Account || !db::get_row_count('authorization', "Platform='shopify' and Account='{$Account}'")){
		$_SESSION['Manage']['Shopify']=$shop_row[0];
		$Account=$shop_row[0]['Account'];
	}
}

$permit_ary=array(
	'add'		=>	manage::check_permit('products', 0, array('a'=>'sync', 'd'=>'add')),
	'edit'		=>	manage::check_permit('products', 0, array('a'=>'sync', 'd'=>'edit')),
	'del'		=>	manage::check_permit('products', 0, array('a'=>'sync', 'd'=>'del')),
	'sync'		=>	manage::check_permit('products', 0, array('a'=>'sync', 'd'=>'sync'))
);
?>
<div id="products_sync" class="r_con_wrap r_con_sync">
	<div class="inside_container">
		<h1>{/module.products.sync/}</h1>
		<ul class="inside_menu">
			<?php
			$out=0;
			$open_ary=array();
			foreach($c['manage']['sync_ary'] as $k=>$v){
				if(($c['FunVersion']<2 && $k!='aliexpress') || !in_array($k, $c['manage']['plugins']['Used'])){
					if($k=='aliexpress' && $c['manage']['do']=='aliexpress') $out=1;
					continue;
				}else{
					$open_ary[]=$k;
				}
				echo '<li><a href="./?m=products&a=sync&d='.$k.'"'.(substr_count($c['manage']['do'], $k)?' class="current"':'').'>{/module.set.authorization.'.$k.'/}</a></li>';
			}
			if($out) js::location('?m=products&a=sync&d='.$open_ary[0]);//当第一个选项没有权限打开，就跳转能打开的第一个页面
			?>
		</ul>
	</div>
	<?php if($c['manage']['do']=='aliexpress' || $c['manage']['do']=='amazon' || $c['manage']['do']=='wish' || $c['manage']['do']=='shopify'){?>
		<div class="inside_table inside_menu_table">
			<div class="list_menu">
				<div class="account_list_box fl">
					<dl class="account_list box_drop_down_menu fl">
						<?php if(count($shop_row)){?>
							<dt class="more">
								<span>
									<?php 
									foreach((array)$shop_row as $v){
										echo $Account==$v['Account'] ? $v['Name'] : '';
									}?>
								</span> 
								<em></em>
							</dt>
							<dd class="more_menu drop_down">
								<?php foreach((array)$shop_row as $v){?>
									<div class="item" aid="<?=$v['AId'];?>" account="<?=($c['manage']['do']=='amazon' || $c['manage']['do']=='wish' || $c['manage']['do']=='shopify')?str::str_code($v['Token']):$v['Account'];?>">
										<div class="opation fr">
											<a href="./?do_action=products.authorization_del&AId=<?=$v['AId'];?>" class="fr del" label="{/global.del/}"></a>
											<?php if($c['manage']['do']=='aliexpress'){?>
												<a href="javascript:;" class="fr refresh" label="{/set.authorization.refresh/}"></a>
											<?php }?>
											<a href="javascript:;" class="fr edit" label="{/global.edit/}"></a>
										</div>
										<a href="javascript:;" class="change_account<?=$Account==$v['Account']?' cur':'';?>" data-id="<?=$v['AId'];?>"><?=$v['Name'];?></a>
									</div>
								<?php }?>
								<div class="item">
									<a href="javascript:;" class="add_store">{/products.sync.add_store/}</a>
								</div>
							</dd>
						<?php }else{?>
							<dt><a href="javascript:;" class="add_store">{/products.sync.add_store/}</a></dt>
						<?php }?>
					</dl>
					<div class="clear"></div>
				</div>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<?php if($c['manage']['do']=='aliexpress'){?>
							<div class="ext drop_down">
								<div class="rows item">
									<label>{/products.classify/}</label>
									<span class="input">
										<div class="box_select"><?=$category_select_html;?></div>
									</span>
									<div class="clear"></div>
								</div>
							</div>
						<?php }?>
						<div class="clear"></div>
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="sync" />
						<input type="hidden" name="d" value="<?=$c['manage']['do'];?>" />
					</form>
				</div>
				<ul class="list_menu_button">
					<?php if(@count($shop_row)){?>
						<li><a class="<?=$c['manage']['do'];?>_product_list_sync" href="javascript:;">{/products.sync.sync_products/}</a></li>
						<li><a class="<?=$c['manage']['do'];?>_product_list_post copy" href="javascript:;">{/products.sync.copy_to_local/}</a></li>
					<?php } ?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
				</ul>
			</div>
		</div>
    <?php }?>
	<?=ly200::load_static('/static/manage/js/sync.js');?>
	<?php if($c['manage']['do']=='aliexpress'){?>
		<script type="text/javascript">$(document).ready(function(){sync_obj.sync_init();sync_obj.aliexpress_init();});</script>
        <div class="inside_table">
        	<?php 
        	//产品列表				
			$where="Account='$Account'";//条件
			$page_count=50;//显示数量
			$Keyword && $where.=" and Subject like '%$Keyword%'";
			$categoryId && $where.=" and categoryId='$categoryId'";
			$GroupId && $where.=" and GroupId='$GroupId'";

			$products_row=str::str_code(db::get_limit_page('products_aliexpress', $where, '*', 'ProId desc', (int)$_GET['page'], $page_count));// 'productId desc'
        	if($products_row[0]){ ?>
	        	<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="10%" nowrap="nowrap">{/products.picture/}</td>
							<td width="50%" nowrap="nowrap">{/products.name/}/{/products.product/} ID</td>
							<td width="20%" nowrap="nowrap">{/products.classify/}/{/products.sync.group/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php

						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							$name=$v['Subject'];
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
								<td class="img"><a class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
								<td><?=$name;?><br /><a href="https://www.aliexpress.com/item/info/<?=$v['productId'];?>.html" target="_blank"><?=$v['productId'];?></a></td>
								<td><?=$category_data[$v['categoryId']]['AliexpressCategory'];?><br /><span class="fc_grey"><?=$grouplist_data[$v['GroupId']]['AliexpressGroupName'];?></span></td>
								<td nowrap="nowrap"><?=$v['currencyCode'];?> <span><?=sprintf('%01.2f', $v['productPrice']);?></span></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=sync&d=aliexpress_products_edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.aliexpress_products_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php }else{ echo html::no_table_data(($shop_row ? 0 : 1), 'javascript:;'); } ?>
		</div>
        <div id="fixed_right">
	        <div class="global_form box_aliexpress_sync global_container">
	        	<div class="top_title">{/products.sync.sync_products/} <a href="javascript:;" class="close"></a></div>
	            <form id="aliexpress_sync_form">
	                <div class="rows">
	                    <label>{/products.sync.pro_group/}:</label>
	                    <div class="input">
	                        <?php 
								$group_row=str::str_code(db::get_all('products_aliexpress_grouplist', "Account='{$Account}'"));
								foreach($group_row as $v){
									if($v['UpperAliexpressGroupId']){
										$group_data[$v['UpperAliexpressGroupId']]['childGroup'][]=$v;
									}else{
										$group_data[$v['AliexpressGroupId']]=$v;
									}
								}
								$groupData='';
								foreach((array)$group_data as $v){
									if(@count((array)$v['childGroup'])){
										$groupData.="<optgroup label=\"{$v['AliexpressGroupName']}\">";
										foreach((array)$v['childGroup'] as $val){
											$groupData.="<option value=\"{$val['AliexpressGroupId']}\">{$val['AliexpressGroupName']}</option>";
										}
										$groupData.="</optgroup>";
									}else{
										$groupData.="<option value=\"{$v['AliexpressGroupId']}\">{$v['AliexpressGroupName']}</option>";
									}
								}
							?>
							<div class="box_select">
		                    	<select name="GroupId">
		                        	<option value="">{/products.sync.all_products/}</option>
		                            <?=$groupData;?>
		                        </select>
							</div>
	                    </div>
	                </div>
	                <div class="rows">
	                    <label>{/products.sync.pro_status/}:</label>
	                    <div class="input">
	                    	<div class="box_select">
		                    	<select name="productStatusType">
		                        <?php foreach($c['manage']['sync_ary']['aliexpress'] as $v){?>
		                        	<option value="<?=$v;?>">{/products.sync.status.<?=$v;?>/}</option>
		                        <?php }?>
		                        </select>
		                    </div>
	                    </div>
	                </div>
	                <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/products.sync.start/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	            </form>
	        </div>
			<div class="copy_products_box global_form global_container">
				<div class="top_title">{/products.sync.copy_to_local/} <a href="javascript:;" class="close"></a></div>
				<form id="copy_edit_form">
					<div class="rows">
						<label>{/products.products_category.category/}</label>
						<div class="input">
							<div class="box_select">
								<?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?>
							</div>
						</div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<input type="button" class="btn_global btn_submit" value="{/global.confirm/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
				</form>
			</div>
			<?php /***************************** 添加店铺弹出框 Start *****************************/?>
			<div class="store_add global_form global_container">
				<div class="top_title">{/set.authorization.add_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
				<form id="store_add" name="store_add">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input">
							<input type="text" class='box_input' value="" name="Name" maxlength="50" notnull />&nbsp;&nbsp;&nbsp;<?php if($c['FunVersion']<10){?><a href="http://help.shop.ueeshop.com/i-332.html" target="_blank" style="text-decoration:underline;">{/products.sync.hot_to_add/}</a><?php }?>
							<br />
							<label>{/set.authorization.shop_name_tips/}</label>
						</div>
                    </div>
                    <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/set.authorization.authorization/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="d" value="<?=$c['manage']['do'];?>" />
	            </form>
            </div>
            <?php /***************************** 添加店铺弹出框 End *****************************/?>
            <?php /***************************** 编辑店铺弹出框 Start *****************************/?>
	        <div class="global_form box_authorization_edit global_container">
	        	<div class="top_title">{/set.authorization.edit_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
	            <form id="authorization_mod" name="authorization_mod">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="Name" maxlength="50" notnull /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/global.save/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="AId" value="" />
	                <input type="hidden" name="d" value="" />
	                <input type="hidden" name="do_action" value="" />
	            </form>
	        </div>
	        <?php /***************************** 编辑店铺弹出框 End *****************************/?>

        </div>
	<?php
	}elseif($c['manage']['do']=='aliexpress_products_edit'){
		$unitAry=array(
			100000000	=>	array('cn'=>'袋', 'en'=>'bag/bags'),
			100000001	=>	array('cn'=>'桶', 'en'=>'barrel/barrels'),
			100000002	=>	array('cn'=>'蒲式耳', 'en'=>'bushel/bushels'),
			100078580	=>	array('cn'=>'箱', 'en'=>'carton'),
			100078581	=>	array('cn'=>'厘米', 'en'=>'centimeter'),
			100000003	=>	array('cn'=>'立方米', 'en'=>'cubic meter'),
			100000004	=>	array('cn'=>'打', 'en'=>'dozen'),
			100078584	=>	array('cn'=>'英尺', 'en'=>'feet'),
			100000005	=>	array('cn'=>'加仑', 'en'=>'gallon'),
			100000006	=>	array('cn'=>'克', 'en'=>'gram'),
			100078587	=>	array('cn'=>'英寸', 'en'=>'inch'),
			100000007	=>	array('cn'=>'千克', 'en'=>'kilogram'),
			100078589	=>	array('cn'=>'千升', 'en'=>'kiloliter'),
			100000008	=>	array('cn'=>'千米', 'en'=>'kilometer'),
			100078559	=>	array('cn'=>'升', 'en'=>'liter/liters'),
			100000009	=>	array('cn'=>'英吨', 'en'=>'long ton'),
			100000010	=>	array('cn'=>'米', 'en'=>'meter'),
			100000011	=>	array('cn'=>'公吨', 'en'=>'metric ton'),
			100078560	=>	array('cn'=>'毫克', 'en'=>'milligram'),
			100078596	=>	array('cn'=>'毫升', 'en'=>'milliliter'),
			100078597	=>	array('cn'=>'毫米', 'en'=>'millimeter'),
			100000012	=>	array('cn'=>'盎司', 'en'=>'ounce'),
			100000014	=>	array('cn'=>'包', 'en'=>'pack/packs'),
			100000013	=>	array('cn'=>'双', 'en'=>'pair'),
			100000015	=>	array('cn'=>'件/个', 'en'=>'piece/pieces'),
			100000016	=>	array('cn'=>'磅', 'en'=>'pound'),
			100078603	=>	array('cn'=>'夸脱', 'en'=>'quart'),
			100000017	=>	array('cn'=>'套', 'en'=>'set/sets'),
			100000018	=>	array('cn'=>'美吨', 'en'=>'short ton'),
			100078606	=>	array('cn'=>'平方英尺', 'en'=>'square feet'),
			100078607	=>	array('cn'=>'平方英寸', 'en'=>'square inch'),
			100000019	=>	array('cn'=>'平方米', 'en'=>'square meter'),
			100078609	=>	array('cn'=>'平方码', 'en'=>'square yard'),
			100000020	=>	array('cn'=>'吨', 'en'=>'ton'),
			100078558	=>	array('cn'=>'码', 'en'=>'yard/yards')
		);

		//产品编辑
		$ProId=(int)$_GET['ProId'];
		$property_data=$property_ary=$sku_data=$attribute_data=$sku_property_data=$sku_selected_data=$attributeAry=$group_data=array();
		$products_row=str::str_code(db::get_one('products_aliexpress', "ProId='$ProId'"));
		$productId=(int)$products_row['productId'];
		!$products_row['ProId'] && js::location('./?m=products&a=sync');
		$products_description_row=str::str_code(db::get_one('products_aliexpress_description', "productId='$productId'"));
		$sku_data=str::json_data(str::str_code($products_row['aeopAeProductSKUs'], 'htmlspecialchars_decode'), 'decode');
		
		$AliexpressLang=@substr($c['manage']['config']['ManageLanguage'],0,2);
	?>
		<?=ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');?>
        <script type="text/javascript">$(document).ready(function(){sync_obj.aliexpress_edit_init();});</script>
        <div class="center_container" style="margin-top: 20px;">
			<form id="edit_form" class="global_form">
				<div class="global_container">
					<?php /***************************** 基本信息 Start *****************************/?>
					<h3 class="big_title">{/products.sync.base/}</h3>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.shopName/}</label>
						<div class="input">
							<div class="box_select">
								<?=ly200::form_select($shop_row, 'Account', $products_row['Account'], 'Name', 'Account');?>
							</div>
						</div>
					</div>
					<div class="rows">
						<label>{/products.classify/}</label>
						<div class="input">
							<div class="box_select">
								<?=ly200::form_select($category_data, 'categoryId', $products_row['categoryId'], 'AliexpressCategory');?>
							</div>
		                    <div class="clear"></div>
		                    <div class="category_list"></div>
		                </div>
						<div class="blank12"></div>
					</div>
					<?php /***************************** 基本信息 End *****************************/?>
				</div>
				<div class="global_container">
					<?php /***************************** 属性信息 Start *****************************/?>
					<h3 class="big_title">{/products.sync.attr/}</h3>
					<div class="rows">
						<label>{/products.sync.productAttr/}</label>
						<div class="input">
		                	<div class="property-table">
		                    	<div class="property-form"></div>
		                        <div class="custom-property hide"></div>
		                    </div>
		                </div>
						<div class="blank12"></div>
					</div>
					<?php /***************************** 属性信息 End *****************************/?>
				</div>
				<div class="global_container">
				<?php /***************************** 产品信息 Start *****************************/?>
					<h3 class="big_title">{/products.sync.goods/}</h3>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.name/}</label>
						<div class="input"><input name="Subject" value="<?=$products_row['Subject'];?>" type="text" class="box_input" size="100" maxlength="128" notnull /></div>					
					</div>
					<div class="rows">
						<label>{/products.sync.pro_group/}</label>
						<div id="GroupList" class="input" data-value="<?=$products_row['GroupId'];?>">
							<div class="box_select"><select name="GroupId"></select></div>
							 &nbsp;&nbsp; <a href="javascript:;" class="group sync green">{/global.synchronize/}</a></div>					
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.pro_picture/}</label>
						<div class="input">
							<span class="multi_img upload_file_multi" id="PicDetail">
								<?php
								for($i=0; $i<6; ++$i){
									echo manage::multi_img_item('PicPath[]', $products_row["PicPath_{$i}"], $i, ly200::get_size_img($products_row["PicPath_{$i}"], '240x240'));
								}
								?>
							</span>
							<div class="tips"><?=sprintf(manage::language('{/notes.pic_size_tips/}'), '800*800');?></div>
							<div class="clear"></div>
						</div>
						<div class="blank30"></div>
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.unit/}</label>
						<div class="input">
							<div class="box_select" parent_null>
			                    <select name="productUnit" data-unit="<?=$unitAry[$products_row['productUnit']][($AliexpressLang=='en'?'en':'cn')];?>" notnull parent_null="1">
			                        <?php 
			                            foreach($unitAry as $k=>$v){
											$groupSelected=$k==$products_row['productUnit']?' selected':'';
											$dataUnit=@reset(explode('/', $AliexpressLang=='en'?$v['en']:$v['cn']));
											echo "<option value=\"{$k}\"{$groupSelected} data-unit=\"{$dataUnit}\">{$v['cn']} ({$v['en']})</option>";
			                            }
			                        ?>
			                    </select>
		                    </div>
		                </div>
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.salesMethod/}</label>
						<div class="input">
		                    <label><input type="radio" name="packageType" value="0"<?=!(int)$products_row['packageType']?' checked':'';?> /> {/products.sync.salesByPiece/}</label>
		                    <label><input type="radio" name="packageType" value="1"<?=(int)$products_row['packageType']?' checked':'';?> /> {/products.sync.salesByPack/}</label> &nbsp; 
		                    <span class="salesByPack <?=(int)$products_row['packageType']?'show':'hide';?>">{/products.sync.salesByPack/} <input name="lotNum" value="<?=$products_row['lotNum'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /> <span class="pUnit"></span></span>
		                </div>
					</div>
		            <div id="sku" class="attribute" attrid="">
		            	
		                
		                
		                <div class="rows hide" id="attribute_ext_box">
		                    <label></label>
		                    <div class="input">
		                        <table border="0" cellpadding="5" cellspacing="0" id="attribute_ext" class="relation_box">
		                            <thead>
		                                <tr>
		                                    <td width="35%">{/products.attribute/}</td>
		                                    <td width="11%">{/products.sync.productPrice/}</td>
		                                    <td width="12%">{/products.products.stock/}</td>
		                                    <td width="15%">{/products.sync.skucode/}</td>
		                                </tr>
		                            </thead>
		                        </table>
		                        <div id="attribute_tmp" class="hide">
		                            <table class="column">
		                                <tbody id="AttrId_XXX">
		                                    <tr>
		                                        <td class="title">Column</td>
		                                        <td class="title"><a href="javascript:;" class="synchronize_btn" data-num="0">{/global.synchronize/}</a></td>
		                                        <td class="title"><a href="javascript:;" class="synchronize_btn" data-num="1">{/global.synchronize/}</a></td>
		                                        <td class="title"><a href="javascript:;" class="synchronize_btn" data-num="2">{/global.synchronize/}</a></td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                            <table>
		                                <tbody class="contents">
		                                    <tr id="VId_XXX" attr_txt="">
		                                        <td>Name</td>
		                                        <td><input type="text" name="skuPrice[XXX]" value="p_v" class="box_input input_w" size="5" maxlength="6" rel="amount" notnull /></td>
		                                        <td><input type="text" name="skuStock[XXX]" value="s_v" class="box_input input_w" size="5" maxlength="6" rel="digital" notnull /></td>
		                                        <td><input type="text" name="skuCode[XXX]" value="u_v" class="box_input input_w sku_input" size="10" maxlength="30" /></td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                        <div id="custom_tmp" class="hide">
		                            <table>
		                            	<tbody class="contents_all">
		                                	<tr id="CustomAll_XXX">
		                                    	<td>Name</td>
		                                        <td class="spacing"><input type="text" name="CustomName[XXX]" value="c_n" class="box_input input_w sku_input" size="10" maxlength="20"></td>
		                                        <td class="spacing">Content</td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                            <table>
		                            	<tbody class="contents_input">
		                                	<tr id="CustomInput_XXX">
		                                    	<td>Name</td>
		                                        <td class="spacing"><input type="text" name="CustomName[XXX]" value="c_n" class="box_input input_w sku_input" size="10" maxlength="20"></td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                            <table>
		                            	<tbody class="contents_image">
		                                	<tr id="CustomImage_XXX">
		                                    	<td>Name</td>
		                                        <td class="spacing">Content</td>
		                                    </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
		                
		                
		                
		                
		                <div class="rows form-post-list">
		                    <label><font class="fc_red">*</font> {/products.sync.productPrice/}</label>
		                    <div class="input"><?=$products_row['currencyCode'];?> <input name="productPrice" value="<?=$products_row['productPrice'];?>" type="text" class="box_input" size="20" maxlength="6" rel="amount" /> / <span class="pUnit"></span></div>
		                </div>
		                <div class="rows">
		                    <label>{/products.products.wholesale_price/}</label>
		                    <div class="input">
		                        <label><input type="checkbox" name="IsWholeSale" value="1"<?=((int)$products_row['bulkOrder'] && (int)$products_row['bulkDiscount'])?' checked':'';?> /> {/products.sync.support/}</label>
		                        <div class="clear"></div>
		                        <div class="WholeSaleBox<?=((int)$products_row['bulkOrder'] && (int)$products_row['bulkDiscount'])?'':' hide';?>">
		                            {/products.sync.wholesale_before/}<input type="text" name="bulkOrder" value="<?=$products_row['bulkOrder']?$products_row['bulkOrder']:''?>" class="box_input" size="5" maxlength="6" rel="digital" /> <span class="pUnit"></span>
		                            {/products.sync.wholesale_middle/}<input type="text" name="bulkDiscount" value="<?=$products_row['bulkDiscount']?$products_row['bulkDiscount']:''?>" class="box_input" size="5" maxlength="2" rel="digital" />
		                            {/products.sync.wholesale_after/}
		                        </div>
		                    </div>
		                </div>
		                <div class="rows form-post-list">
		                    <label><font class="fc_red">*</font> {/products.products.stock/}</label>
		                    <div class="input"><input name="ipmSkuStock" value="<?=$sku_data[0]['ipmSkuStock'];?>" type="text" class="box_input" size="20" maxlength="6" rel="digital" /></div>	                   
		                </div>
		                <div class="rows form-post-list">
		                    <label>{/products.sync.skucode/}</label>
		                    <div class="input"><input name="ipmSkuCode" value="<?=$sku_data[0]['skuCode'];?>" type="text" class="box_input" size="20" maxlength="30" /></div>	                   
		                </div>
		            </div>
					<div class="rows">
						<label>{/products.sync.reduceStrategy/}</label>
						<div class="input">
		                    <label><input type="radio" name="reduceStrategy" value="place_order_withhold"<?=$products_row['reduceStrategy']=='place_order_withhold'?' checked':'';?> /> {/products.sync.placeReduce/}</label>
		                    <label><input type="radio" name="reduceStrategy" value="payment_success_deduct"<?=($products_row['reduceStrategy']=='payment_success_deduct'||$products_row['reduceStrategy']=='')?' checked':'';?> /> {/products.sync.paymentReduce/}</label>
		                </div>					
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.delivery/}</label>
						<div class="input"><input name="deliveryTime" value="<?=$products_row['deliveryTime'];?>" type="text" class="box_input" size="5" maxlength="1" rel="digital" notnull /> {/products.sync.date/} {/products.sync.deliveryTips/}</div>					
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.validPeriod/}</label>
						<div class="input">
		                	<label><input type="radio" name="wsValidNum" value="14"<?=$products_row['wsValidNum']==14?' checked':'';?> /> 14 {/products.sync.date/}</label>
		                	<label><input type="radio" name="wsValidNum" value="30"<?=($products_row['wsValidNum']==30||!$products_row['wsValidNum'])?' checked':'';?> /> 30 {/products.sync.date/}</label>
		                </div>
						<div class="blank30"></div>
					</div>
					<?php /***************************** 产品信息 End *****************************/?>
				</div>
				<div class="global_container">			
					<?php /***************************** 文字描述 Start *****************************/?>
					<h3 class="big_title">{/products.sync.descInfo/}</h3>
					<div class="rows">
						<label>{/products.sync.pcDetail/}</label>
						<div class="input"><?=manage::Editor("Detail", $products_description_row["Detail"]);?></div>					
					</div>
					<div class="rows">
						<label>{/products.sync.mobileDetail/}</label>
						<div class="input">
		                	<div class="turn_on_mobile_detail"><input type="checkbox" name="IsMobileDetail" value="1"<?=(int)$products_description_row["mobileDetail"]?' checked="checked"':'';?> /> {/global.turn_on/} &nbsp;&nbsp; <a href="javascript:;" class="sync green">{/global.key_sync/}</a></div>
							<div class="mobile_detail <?=$products_description_row["mobileDetail"]==''?'hide':'';?>"><?=manage::Editor("mobileDetail", $products_description_row["mobileDetail"]);?></div>
						</div>
						<div class="blank12"></div>
					</div>
					<?php /***************************** 文字描述 End *****************************/?>
				</div>
				<div class="global_container">
					<?php /***************************** 包装信息 Start *****************************/?>
					<h3 class="big_title">{/products.sync.packageInfo/}</h3>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.packageWeight/}</label>
						<div class="input">
							<input name="grossWeight" value="<?=$products_row['grossWeight'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" notnull /> {/products.sync.WeightUnit/}/<span class="pUnit"></span> &nbsp;&nbsp; 
		                    <input type="checkbox" name="isPackSell" value="1" /> {/products.sync.customWeight/}
		                    <div class="clear"></div>
		                    <div class="custom_weight hide">
		                        <div><?=sprintf($c['manage']['lang_pack']['products']['sync']['baseUnit'], ($products_row['baseUnit']?$products_row['baseUnit']:''));?></div>
		                        <div><?=sprintf($c['manage']['lang_pack']['products']['sync']['addUnit'], ($products_row['addUnit']?$products_row['addUnit']:''), ($products_row['addWeight']>0?$products_row['addWeight']:''));?></div>
		                    </div>
		                </div>
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.packageSize/}</label>
						<div class="input">
							<input name="packageLength" value="<?=$products_row['packageLength'];?>" type="text" class="box_input" size="5" maxlength="3" rel="digital" notnull /> &nbsp; X &nbsp; 
		                    <input name="packageWidth" value="<?=$products_row['packageWidth'];?>" type="text" class="box_input" size="5" maxlength="3" rel="digital" notnull /> &nbsp; X &nbsp; 
		                    <input name="packageHeight" value="<?=$products_row['packageHeight'];?>" type="text" class="box_input" size="5" maxlength="3" rel="digital" notnull /> {/products.sync.sizeTips/}
		                </div>
						<div class="blank12"></div>
					</div>
					<?php /***************************** 包装信息 End *****************************/?>
				</div>
				<div class="global_container">
					<?php /***************************** 模板信息 Start *****************************/?>
					<h3 class="big_title">{/products.sync.templateInfo/}</h3>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.freightTemp/}</label>
						<div id="freightTemp" class="input" data-value="<?=$products_row['freightTemplateId'];?>"> &nbsp;&nbsp; <a href="javascript:;" class="freight sync green">{/global.synchronize/}</a></div>
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.serviceTemp/}</label>
						<div id="serviceTemp" class="input" data-value="<?=$products_row['promiseTemplateId'];?>"> &nbsp;&nbsp; <a href="javascript:;" class="service sync green">{/global.synchronize/}</a></div>
						<div class="blank30"></div>
					</div>
					<?php /*?><div class="rows">
						<label>{/products.sync.sizechartTemp/}</label>
						<span class="input">

		                </span>
						<div class="clear"></div>
					</div><?php */?>
					<?php /***************************** 模板信息 End *****************************/?>
				</div>
	            
				<div class="rows">
					<label></label>
					<div class="input">
						<input type="button" class="btn_global btn_submit" value="{/global.submit/}">
						<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=snyc';?>"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
					</div>
					<div class="clear"></div>
				</div>
				<input type="hidden" id="all_attr" value="" />
				<input type="hidden" id="check_attr" value="" />
				<input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data($combinatin_ary));?>" />
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" id="productId" name="productId" value="<?=$productId;?>" />
				<input type="hidden" name="do_action" value="products.sync_aliexpress_products_edit" />
				<input type="hidden" id="back_action" name="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			</form>
		</div>
	<?php }elseif($c['manage']['do']=='amazon'){?>
		<script type="text/javascript">$(document).ready(function(){sync_obj.sync_init();sync_obj.amazon_init();});</script>
        
        <div class="inside_table">
			<?php 
			//产品列表				
			$where="MerchantId='$Account'";//条件
			$page_count=50;//显示数量
			$Keyword && $where.=" and `item-name` like '%$Keyword%'";

			$products_row=str::str_code(db::get_limit_page('products_amazon', $where, '*', 'ProId desc', (int)$_GET['page'], $page_count));// 'productId desc'
			if($products_row[0]){ ?>
	        	<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="12%" nowrap="nowrap">{/products.picture/}</td>
							<td width="35%" nowrap="nowrap">{/products.name/} / {/products.sync.amazon.asin/}</td>
							<td width="15%" nowrap="nowrap">{/products.sync.amazon.sku/} / {/products.products.number/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price/}</td>
							<td width="8%" nowrap="nowrap">{/products.products.stock/}</td>
							<td width="10%" nowrap="nowrap">{/set.authorization.marketplace/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['image-url'], end($c['manage']['resize_ary']['products']));
							!$img && $img='/static/manage/images/frame/no-image-sm.gif';
							$name=$v['item-name'];
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
								<td class="img"><a class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
								<td><?=$name;?><br /><a href="<?=$c['manage']['sync_ary']['amazon'][$v['Marketplace']][1];?>dp/<?=$v['asin1'];?>" target="_blank"><?=$v['asin1'];?></a></td>
								<td><?=$v['seller-sku'];?><br /><span class="fc_grey"><?=$v['listing-id'];?></span></td>
								<td nowrap="nowrap"><?=$v['currencyCode'];?> <span><?=sprintf('%01.2f', $v['price']);?></span></td>
								<td nowrap="nowrap"><?=$v['quantity'];?></td>
								<td>{/set.authorization.<?=$v['Marketplace'];?>/}</td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=sync&d=amazon_products_edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.amazon_products_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php }else{ echo html::no_table_data(($shop_row ? 0 : 1), 'javascript:;'); } ?>
		</div>
		<div id="fixed_right">
			<div class="copy_products_box global_form global_container">
				<div class="top_title">{/products.sync.copy_to_local/} <a href="javascript:;" class="close"></a></div>
				<form id="copy_edit_form">
					<div class="rows">
						<label>{/products.products_category.category/}</label>
						<div class="input tab_box">
							<div class="box_select">
								<?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?>
							</div>
						</div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<input type="button" class="btn_global btn_submit" value="{/global.confirm/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
				</form>
			</div>
			<?php /***************************** 添加店铺弹出框 Start *****************************/?>
			<div class="store_add global_form global_container">
				<div class="top_title">{/set.authorization.add_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
				<form id="store_add" name="store_add">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" placeholder="{/set.authorization.shop_name/}" value="" name="Name" maxlength="50" notnull /></div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.account_site/}</label>
                        <div class="input">
                        	<div class="box_select" parent_null>
	                        	<select name="MarkectPlace" notnull parent_null="1">
	                            	<option value="">{/global.select_index/}</option>
	                            	<?php foreach($c['manage']['sync_ary']['amazon'] as $k=>$v){?>
	                                	<option value="<?=$k;?>" data-url="<?=$v[0];?>">{/set.authorization.<?=$k;?>/}</option>
	                                <?php }?>
	                            </select>&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <a href="javascript:;" class="blue">{/set.authorization.go/}</a>
                        </div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.merchant_id/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="MerchantId" size="40" maxlength="20" placeholder="{/set.authorization.seller_id/}" notnull />&nbsp;&nbsp;&nbsp;<?php if($c['FunVersion']<10){?><a href="http://help.shop.ueeshop.com/i-333.html" target="_blank" style="text-decoration:underline;">{/global.how_to_get/}</a><?php }?></div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.aws_access_key_id/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="AWSAccessKeyId" size="40" maxlength="30" placeholder="{/set.authorization.access_key_tips/}" notnull /></div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.secret_key/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="SecretKey" size="40" maxlength="50" placeholder="{/set.authorization.secret_key_tips/}" notnull /></div>                        
                    </div>
	                <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/set.authorization.authorization/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="d" value="amazon" />
	                <input type="hidden" name="do_action" value="products.authorization_add" />
	            </form>
            </div>
            <?php /***************************** 添加店铺弹出框 End *****************************/?>
            <?php /***************************** 编辑店铺弹出框 Start *****************************/?>
	        <div class="global_form box_authorization_edit global_container">
	        	<div class="top_title">{/set.authorization.edit_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
	            <form id="authorization_mod" name="authorization_mod">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" value="" name="Name" maxlength="50" notnull /></div>          
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.account_site/}</label>
                        <div class="input">
                        	<div class="box_select" parent_null>
	                        	<select name="MarkectPlace" notnull parent_null="1">
	                            	<option value="">{/global.select_index/}</option>
	                            	<?php foreach($c['manage']['sync_ary']['amazon'] as $k=>$v){?>
	                                	<option value="<?=$k;?>" data-url="<?=$v[0];?>">{/set.authorization.<?=$k;?>/}</option>
	                                <?php }?>
	                            </select>
                        	</div>
                            <a href="javascript:;" class="amazon_url blue">{/set.authorization.go/}</a>
                        </div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.merchant_id/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="MerchantId" size="40" maxlength="20" readonly /></div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.aws_access_key_id/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="AWSAccessKeyId" size="40" maxlength="30" readonly /></div>                        
                    </div>
                    <div class="rows">
                        <label>{/set.authorization.secret_key/}</label>
                        <div class="input"><input type="text" class='box_input' value="" name="SecretKey" size="40" maxlength="50" readonly /></div>                        
                    </div>
                    <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="AId" value="" notnull />
	                <input type="hidden" name="d" value="" />
	                <input type="hidden" name="do_action" value="" />
	                <input type="hidden" name="method" value="" />
	            </form>
	        </div>
	        <?php /***************************** 编辑店铺弹出框 End *****************************/?>
        </div>
	<?php
	}elseif($c['manage']['do']=='amazon_products_edit'){
		//产品编辑
		$ProId=(int)$_GET['ProId'];
		$products_row=str::str_code(db::get_one('products_amazon', "ProId='$ProId'"));
		$productId=$products_row['product-id'];
		!$products_row['ProId'] && js::back();

		$feature_data=str::json_data(str::str_code($products_row['Feature'], 'htmlspecialchars_decode'), 'decode');
		$listprice_data=str::json_data(str::str_code($products_row['ListPrice'], 'htmlspecialchars_decode'), 'decode');
		$item_data=str::json_data(str::str_code($products_row['ItemDimensions'], 'htmlspecialchars_decode'), 'decode');
		$pakeage_data=str::json_data(str::str_code($products_row['PackageDimensions'], 'htmlspecialchars_decode'), 'decode');
		
		$img=ly200::get_size_img($products_row['image-url'], '240x240');
		$defaultCurrency=db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Currency');
	?>
		<?=ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');?>
        <script type="text/javascript">$(document).ready(function(){sync_obj.amazon_edit_init();});</script>
        <div class="center_container" style="margin-top: 20px;">
			<form id="edit_form" class="global_form global_container">
				<?php /***************************** 基本信息 Start *****************************/?>
				<h3 class="big_title">{/products.sync.shop_info/}</h3>
				<div class="rows">
					<label><font class="fc_red">*</font> {/products.sync.shopName/}</label>
					<div class="input">
						<div class="box_select"><?=ly200::form_select($shop_row, 'Account', $products_row['Account'], 'Name', 'Account');?></div>
					</div>						
				</div>
				<?php /***************************** 基本信息 End *****************************/?>

				<?php /***************************** 产品信息 Start *****************************/?>
				<h3 class="big_title">{/products.sync.goods/}</h3>
				<div class="rows">
					<label><font class="fc_red">*</font> {/products.name/}</label>
					<div class="input"><input name="item-name" value="<?=$products_row['item-name'];?>" type="text" class="box_input" size="100" maxlength="128" notnull /></div>					
				</div>
				<div class="rows">
					<label>{/products.sync.amazon.asin/}</label>
					<div class="input"><input name="seller-sku" value="<?=$products_row['asin1'];?>" type="text" class="box_input" size="53" maxlength="50" readonly /></div>					
				</div>
				<div class="rows">
					<label>{/products.sync.amazon.sku/}</label>
					<div class="input"><input name="seller-sku" value="<?=$products_row['seller-sku'];?>" type="text" class="box_input" size="53" maxlength="50" readonly /></div>					
				</div>
	            <div class="rows">
	                <label>{/products.products.number/}</label>
	                <div class="input"><input name="listing-id" value="<?=$products_row['listing-id'];?>" type="text" class="box_input" size="53" maxlength="50" readonly /></div>	                
	            </div>
	            <div class="rows">
	                <label><font class="fc_red">*</font> {/products.products.price/}</label>
	                <div class="input">
	                	<input type="text" name="price" value="<?=sprintf('%01.2f', $products_row['price']);?>" class="box_input" size="10" maxlength="10" rel="amount" notnull />&nbsp;&nbsp;
	                    <span class="unit_input"><b>{/products.products.price_ary.0/} <?=$listprice_data['CurrencyCode']?><div class="arrow"><em></em><i></i></div></b><input name="ListPrice[Amount]" value="<?=sprintf('%01.2f', $listprice_data['Amount']);?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /></span>
	                    <input type="hidden" name="ListPrice[CurrencyCode]" value="<?=$listprice_data['CurrencyCode']?$listprice_data['CurrencyCode']:$defaultCurrency;?>" />
	                </div>	                
	            </div>
	            <div class="rows">
	                <label><font class="fc_red">*</font> {/products.products.qty/}</label>
	                <div class="input"><input name="quantity" value="<?=(int)$products_row['quantity'];?>" type="text" class="box_input" size="10" maxlength="10" rel="digital" notnull /> ({/products.products.stock/})</div>	                
	            </div>
				<div class="rows">
					<label><font class="fc_red">*</font> {/products.sync.picture/}</label>
	                <div class="input">
	                	<?=manage::multi_img('ImageDetail', 'ImagePath', $img); ?>
	                </div>					
				</div>
				<?php /***************************** 产品信息 End *****************************/?>

				<?php /***************************** 包装信息 Start *****************************/?>
				<h3 class="big_title">{/products.sync.packageInfo/}</h3>
	            <div class="rows">
	                <label>{/products.products.cubage/}</label>
	                <div class="input">
	                    <span class="unit_input"><b>{/products.products.long/}<div class="arrow"><em></em><i></i></div></b><input name="ItemDimensions[Length][0]" value="<?=$item_data['Length'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$item_data['Length'][1];?></b></span>&nbsp;&nbsp;&nbsp;
	                    <input type="hidden" name="ItemDimensions[Length][1]" value="<?=$item_data['Length'][1]?$item_data['Length'][1]:'inches';?>" />
	                    <span class="unit_input"><b>{/products.products.width/}<div class="arrow"><em></em><i></i></div></b><input name="ItemDimensions[Width][0]" value="<?=$item_data['Width'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$item_data['Width'][1];?></b></span>&nbsp;&nbsp;&nbsp;
	                    <input type="hidden" name="ItemDimensions[Width][1]" value="<?=$item_data['Width'][1]?$item_data['Width'][1]:'inches';?>" />
	                    <span class="unit_input"><b>{/products.products.height/}<div class="arrow"><em></em><i></i></div></b><input name="ItemDimensions[Height][0]" value="<?=$item_data['Height'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$item_data['Height'][1];?></b></span>
	                    <input type="hidden" name="ItemDimensions[Height][1]" value="<?=$item_data['Height'][1]?$item_data['Height'][1]:'inches';?>" />
	                </div>	                
	            </div>
	            <div class="rows">
	                <label>{/products.sync.packageWeight/}</label>
	                <div class="input">
	                    <span class="unit_input"><b>{/products.products.weight/}<div class="arrow"><em></em><i></i></div></b><input name="PackageDimensions[Weight][0]" value="<?=$pakeage_data['Weight'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$pakeage_data['Weight'][1];?></b></span>
	                    <input type="hidden" name="PackageDimensions[Weight][1]" value="<?=$pakeage_data['Weight'][1]?$pakeage_data['Weight'][1]:'pounds';?>" />
	                </div>	                
	            </div>
	            <div class="rows">
	                <label>{/products.sync.packageSize/}</label>
	                <div class="input">
	                    <span class="unit_input"><b>{/products.products.long/}<div class="arrow"><em></em><i></i></div></b><input name="PackageDimensions[Length][0]" value="<?=$pakeage_data['Length'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$pakeage_data['Length'][1];?></b></span>&nbsp;&nbsp;&nbsp;
	                    <input type="hidden" name="PackageDimensions[Length][1]" value="<?=$pakeage_data['Length'][1]?$pakeage_data['Length'][1]:'inches';?>" />
	                    <span class="unit_input"><b>{/products.products.width/}<div class="arrow"><em></em><i></i></div></b><input name="PackageDimensions[Width][0]" value="<?=$pakeage_data['Width'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$pakeage_data['Width'][1];?></b></span>&nbsp;&nbsp;&nbsp;
	                    <input type="hidden" name="PackageDimensions[Width][1]" value="<?=$pakeage_data['Width'][1]?$pakeage_data['Width'][1]:'inches';?>" />
	                    <span class="unit_input"><b>{/products.products.height/}<div class="arrow"><em></em><i></i></div></b><input name="PackageDimensions[Height][0]" value="<?=$pakeage_data['Height'][0];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last"><?=$pakeage_data['Height'][1];?></b></span>
	                    <input type="hidden" name="PackageDimensions[Height][1]" value="<?=$pakeage_data['Height'][1]?$pakeage_data['Height'][1]:'inches';?>" />
	                </div>	                
	            </div>
				<?php /***************************** 包装信息 End *****************************/?>
							
				<?php /***************************** 文字描述 Start *****************************/?>
				<h3 class="big_title">{/products.sync.descInfo/}</h3>
	            <?php for($i=0; $i<5; $i++){?>
	            <div class="rows">
	                <label><?=!$i?'{/products.sync.amazon.feature/}':'';?></label>
	                <div class="input"><input name="Feature[]" value="<?=$feature_data[$i];?>" title="<?=$feature_data[$i];?>" type="text" class="box_input" size="100" maxlength="200" /></div>	                
	            </div>
	            <?php }?>
	            
				<div class="rows">
					<label>{/products.products.description/}</label>
					<div class="input"><?=manage::Editor("item-description", $products_row["item-description"]);?></div>					
				</div>
				<?php /***************************** 文字描述 End *****************************/?>

				<div class="rows">
					<label></label>
					<div class="input">
						<input type="button" class="btn_global btn_submit" value="{/global.save/}">
						<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=snyc&d=amazon';?>"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
					</div>					
				</div>
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" name="do_action" value="products.sync_amazon_products_edit" />
				<input type="hidden" id="back_action" name="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" /><?php /*?><?php */?>
			</form>
		</div>
	<?php }elseif($c['manage']['do']=='wish'){?>
		<script type="text/javascript">$(document).ready(function(){sync_obj.sync_init();sync_obj.wish_init();});</script>
		<input type="hidden" name="Start" value="0" />
        <div class="inside_table">
			<?php 
			//产品列表				
			$where="ClientId='$Account'";//条件
			$page_count=50;//显示数量
			$Keyword && $where.=" and name like '%$Keyword%'";
			$products_row=str::str_code(db::get_limit_page('products_wish', $where, '*', 'ProId desc', (int)$_GET['page'], $page_count));
			if($products_row[0]){
			?>
	        	<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="12%" nowrap="nowrap">{/products.picture/}</td>
							<td width="35%" nowrap="nowrap">{/products.name/}</td>
							<td width="15%" nowrap="nowrap">{/products.sync.amazon.sku/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price/}</td>
							<td width="8%" nowrap="nowrap">{/products.products.stock/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							!$img && $img='/static/manage/images/frame/no-image-sm.gif';
							$name=$v['Name'];
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
								<td class="img"><a class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
								<td><?=$name;?></td>
								<td><?=$v['parent_sku'];?></td>
								<td nowrap="nowrap"><?=$v['currencyCode'];?> <span><?=sprintf('%01.2f', $v['price']);?></span></td>
								<td nowrap="nowrap"><?=$v['inventory'];?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=sync&d=wish_products_edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.wish_products_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php }else{ echo html::no_table_data(($shop_row ? 0 : 1), 'javascript:;'); } ?>
		</div>
		<div id="fixed_right">
			<div class="copy_products_box global_form global_container">
				<div class="top_title">{/products.sync.copy_to_local/} <a href="javascript:;" class="close"></a></div>
				<form id="copy_edit_form">
					<div class="rows">
						<label>{/products.products_category.category/}</label>
						<div class="input tab_box">
							<div class="box_select">
								<?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?>
							</div>
						</div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<input type="button" class="btn_global btn_submit" value="{/global.confirm/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
				</form>
			</div>
			<?php /***************************** 添加店铺弹出框 Start *****************************/?>
			<div class="store_add global_form global_container">
				<div class="top_title">{/set.authorization.add_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
				<form id="store_add" name="store_add">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" placeholder="{/set.authorization.shop_name/}" value="" name="Name" maxlength="50" notnull /></div>
                    </div>
                    <div class="rows">
                        <label>Client Id</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_id" size="40" maxlength="40" placeholder="Your app's client ID" notnull />&nbsp;&nbsp;&nbsp;<?php if($c['FunVersion']<10){?><a href="http://help.shop.ueeshop.com/i-405.html" target="_blank" style="text-decoration:underline;">{/global.how_to_get/}</a><?php }?></div>
                    </div>
					<div class="rows">
                        <label>Client Secret</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_secret" size="40" maxlength="40" placeholder="Your app's client secret" notnull /></div>
                    </div>
					<div class="rows">
                        <label>Redirect URI</label>
                        <div class="input"><?=ly200::get_domain();?>/plugins/api/wish/<br />Your app's redirect uri that you specified when you created the app.<input type="hidden" value="<?=ly200::get_domain();?>/plugins/api/wish/" name="redirect_uri" /></div>
                    </div>
	                <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/set.authorization.authorization/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="d" value="<?=$c['manage']['do'];?>" />
	            </form>
            </div>
            <?php /***************************** 添加店铺弹出框 End *****************************/?>
            <?php /***************************** 编辑店铺弹出框 Start *****************************/?>
	        <div class="global_form box_authorization_edit global_container">
	        	<div class="top_title">{/set.authorization.edit_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
	            <form id="authorization_mod" name="authorization_mod">
					<div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" placeholder="{/set.authorization.shop_name/}" value="" name="Name" maxlength="50" notnull /></div>
                    </div>
                    <div class="rows">
                        <label>Client Id</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_id" size="40" maxlength="40" placeholder="Your app's client ID" readonly /></div>
                    </div>
					<div class="rows">
                        <label>Client Secret</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_secret" size="40" maxlength="40" placeholder="Your app's client secret" readonly /></div>
                    </div>
					<div class="rows">
                        <label>Redirect URI</label>
                        <div class="input"><?=ly200::get_domain();?>/plugins/api/wish/<br />Your app's redirect uri that you specified when you created the app.<input type="hidden" value="<?=ly200::get_domain();?>/plugins/api/wish/" name="redirect_uri" /></div>
                    </div>
                    <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="AId" value="" notnull />
	                <input type="hidden" name="d" value="" />
	                <input type="hidden" name="do_action" value="" />
	                <input type="hidden" name="method" value="" />
	            </form>
	        </div>
	        <?php /***************************** 编辑店铺弹出框 End *****************************/?>
        </div>
	<?php }elseif($c['manage']['do']=='shopify'){?>
		<script type="text/javascript">$(document).ready(function(){sync_obj.sync_init();sync_obj.shopify_init();});</script>
        <div class="inside_table">
			<?php 
			//产品列表				
			$where="Account='$Account'";//条件
			$page_count=50;//显示数量
			$Keyword && $where.=" and title like '%$Keyword%'";
			$products_row=str::str_code(db::get_limit_page('products_shopify', $where, '*', 'ProId desc', (int)$_GET['page'], $page_count));
			if($products_row[0]){
			?>
	        	<div class="clear"></div>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="12%" nowrap="nowrap">{/products.picture/}</td>
							<td width="35%" nowrap="nowrap">{/products.name/}</td>
							<td width="15%" nowrap="nowrap">{/products.sync.amazon.sku/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price/}</td>
							<td width="8%" nowrap="nowrap">{/products.products.stock/}</td>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							!$img && $img='/static/manage/images/frame/no-image-sm.gif';
							$name=$v['title'];
						?>
							<tr>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
								<td class="img"><a class="pic_box"><img src="<?=$img;?>" /><span></span></a></td>
								<td><?=$name;?></td>
								<td><?=$v['sku'];?></td>
								<td nowrap="nowrap"><?=$v['currencyCode'];?> <span><?=sprintf('%01.2f', $v['price']);?></span></td>
								<td nowrap="nowrap"><?=$v['inventory_quantity'];?></td>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=sync&d=shopify_products_edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.shopify_products_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page=');?>
			<?php
			}else{
				echo html::no_table_data(($shop_row?0:1), 'javascript:;');
			}?>
		</div>
		<div id="fixed_right">
			<div class="copy_products_box global_form global_container">
				<div class="top_title">{/products.sync.copy_to_local/} <a href="javascript:;" class="close"></a></div>
				<form id="copy_edit_form">
					<div class="rows">
						<label>{/products.products_category.category/}</label>
						<div class="input">
                            <a href="javascript:;" class="btn_global btn_item_sec btn_add" id="btn_expand">{/products.products.expand/}</a>
                            <div class="classify">
                                <div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>
                            </div>
                            <ul class="expand_list"></ul>
						</div>
					</div>
					<div class="rows">
						<label></label>
						<div class="input">
							<input type="button" class="btn_global btn_submit" value="{/global.confirm/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
				</form>
			</div>
			<?php /***************************** 添加店铺弹出框 Start *****************************/?>
			<div class="store_add global_form global_container">
				<div class="top_title">{/set.authorization.add_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
				<form id="store_add" name="store_add">
                    <div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" placeholder="{/set.authorization.shop_name/}" value="" name="Name" maxlength="50" notnull /></div>
                    </div>
                    <div class="rows">
                        <label>API key</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_username" size="40" maxlength="100" placeholder="The API key that you generated" notnull />&nbsp;&nbsp;&nbsp;<?php if($c['FunVersion']<10){?><a href="http://help.shop.ueeshop.com/i-424.html" target="_blank" style="text-decoration:underline;">{/global.how_to_get/}</a><?php }?></div>
                    </div>
					<div class="rows">
                        <label>password</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_password" size="40" maxlength="100" placeholder="The API password" notnull /></div>
                    </div>
					<div class="rows">
                        <label>shop</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_shop" size="40" maxlength="100" placeholder="The name that you entered for your development store" notnull /></div>
                    </div>
	                <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="submit" class="btn_global btn_submit" value="{/set.authorization.authorization/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="d" value="<?=$c['manage']['do'];?>" />
	                <input type="hidden" name="do_action" value="products.authorization_add" />
	            </form>
            </div>
            <?php /***************************** 添加店铺弹出框 End *****************************/?>
            <?php /***************************** 编辑店铺弹出框 Start *****************************/?>
	        <div class="global_form box_authorization_edit global_container">
	        	<div class="top_title">{/set.authorization.edit_store/} ({/module.set.authorization.<?=$c['manage']['do'];?>/}) <a href="javascript:;" class="close"></a></div>
	            <form id="authorization_mod" name="authorization_mod">
					<div class="rows">
                        <label>{/set.authorization.shop_name/}</label>
                        <div class="input"><input type="text" class="box_input" placeholder="{/set.authorization.shop_name/}" value="" name="Name" maxlength="50" notnull /></div>
                    </div>
                    <div class="rows">
                        <label>API key</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_username" size="40" maxlength="100" placeholder="The API key that you generated" readonly /></div>
                    </div>
					<div class="rows">
                        <label>password</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_password" size="40" maxlength="100" placeholder="The API password" readonly /></div>
                    </div>
					<div class="rows">
                        <label>shop</label>
                        <div class="input"><input type="text" class='box_input' value="" name="client_shop" size="40" maxlength="100" placeholder="The name that you entered for your development store" notnull /></div>
                    </div>
                    <div class="rows">
						<label></label>
						<div class="input input_button">
							<input type="button" class="btn_global btn_submit" value="{/global.save/}" name="submit_button">
							<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}">
						</div>
					</div>
	                <input type="hidden" name="AId" value="" notnull />
	                <input type="hidden" name="d" value="" />
	                <input type="hidden" name="do_action" value="" />
	            </form>
	        </div>
	        <?php /***************************** 编辑店铺弹出框 End *****************************/?>
        </div>
	<?php
	}elseif($c['manage']['do']=='shopify_products_edit'){
		//Shopify产品编辑
		$ProId		= (int)$_GET['ProId'];
		$prod_row	= str::str_code(db::get_one('products_shopify', "ProId='$ProId'"));
		$productId	= $prod_row['product_id'];
		!$prod_row && js::back();
		$pic_ary=array();
		for($i=0; $i<10; ++$i){
			if($prod_row["PicPath_$i"] && is_file($c['root_path'].$prod_row["PicPath_$i"])){
				$pic_ary[]=$prod_row["PicPath_$i"];
			}
		}
		
		echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
	?>
        <script type="text/javascript">$(document).ready(function(){sync_obj.shopify_edit_init();});</script>
        <div class="center_container" style="margin-top:20px;">
			<form id="edit_form" class="global_form">
				<?php /***************************** 基本信息 Start *****************************/?>
				<div class="global_container">
					<div class="big_title">{/products.sync.shop_info/}</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.shopName/}</label>
						<div class="input">
							<div class="box_select"><?=ly200::form_select($shop_row, 'Account', $prod_row['Account'], 'Name', 'Account');?></div>
						</div>						
					</div>
				</div>
				<?php /***************************** 基本信息 End *****************************/?>

				<?php /***************************** 产品信息 Start *****************************/?>
				<div class="global_container">
					<div class="big_title">{/products.sync.goods/}</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.name/}</label>
						<div class="input"><input name="title" value="<?=$prod_row['title'];?>" type="text" class="box_input" size="100" maxlength="128" notnull /></div>					
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.products.sku/}</label>
						<div class="input"><input name="sku" value="<?=$prod_row['sku'];?>" type="text" class="box_input" size="100" maxlength="255" notnull /></div>					
					</div>
                    <div class="rows">
						<label>{/products.sync.shopify.productType/}</label>
						<div class="input"><input name="product_type" value="<?=$prod_row['product_type'];?>" type="text" class="box_input" size="100" maxlength="255" /></div>					
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.products.price/}</label>
						<div class="input">
							<span class="unit_input"><b><?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input name="price" value="<?=sprintf('%01.2f', $prod_row['price']);?>" type="text" class="box_input" size="5" maxlength="255" rel="amount" /></span>&nbsp;&nbsp;
							<span class="unit_input"><b>{/products.products.price_ary.0/} <?=$c['manage']['currency_symbol'];?><div class="arrow"><em></em><i></i></div></b><input name="compare_at_price" value="<?=sprintf('%01.2f', $prod_row['compare_at_price']);?>" type="text" class="box_input" size="5" maxlength="255" rel="amount" /></span>
						</div>	                
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.products.qty/}</label>
						<div class="input"><input name="inventory_quantity" value="<?=(int)$prod_row['inventory_quantity'];?>" type="text" class="box_input" size="10" maxlength="10" rel="digital" notnull /> ({/products.products.stock/})</div>	                
					</div>
					<div class="rows">
						<label><font class="fc_red">*</font> {/products.sync.pro_picture/}</label>
						<div class="input">
							<span class="multi_img upload_file_multi" id="PicDetail">
							<?php
							for($i=0; $i<12; ++$i){
								echo manage::multi_img_item('PicPath[]', $prod_row["PicPath_{$i}"], $i, ly200::get_size_img($prod_row["PicPath_{$i}"], '240x240'));
							}?>
							</span>
							<div class="tips"><?=sprintf(manage::language('{/notes.pic_size_tips/}'), '800*800');?></div>
							<div class="clear"></div>
						</div>
						<div class="blank30"></div>
					</div>
				</div>
				<?php /***************************** 产品信息 End *****************************/?>

				<?php /***************************** 包装信息 Start *****************************/?>
				<div class="global_container">
					<div class="big_title">{/products.sync.packageInfo/}</div>
					<div class="rows">
						<label>{/products.products.weight/}</label>
						<div class="input">
							<span class="unit_input"><input name="weight" value="<?=$prod_row['weight'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /><b class="last">{/products.products.weight_unit/}</b></span>
						</div>	                
					</div>
				</div>
				<?php /***************************** 包装信息 End *****************************/?>
							
				<?php /***************************** 文字描述 Start *****************************/?>
				<div class="global_container">
					<div class="big_title">{/products.sync.descInfo/}</div>
					<div class="rows">
						<label>{/products.products.description/}</label>
						<div class="input"><?=manage::Editor('body_html', $prod_row['body_html']);?></div>					
					</div>
				</div>
				<?php /***************************** 文字描述 End *****************************/?>

				<div class="rows">
					<label></label>
					<div class="input">
						<input type="button" class="btn_global btn_submit" value="{/global.save/}">
						<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=snyc&d=shopify';?>"><input type="button" class="btn_global btn_cancel" value="{/global.return/}"></a>
					</div>					
				</div>
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" name="do_action" value="products.sync_shopify_products_edit" />
				<input type="hidden" id="back_action" name="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			</form>
		</div>
	<?php }else{?>
    
    <?php }?>
    <div class="pop_form sync_progress">
        <div class="tips_contents">
			<div class="box_progress">
				<div class="status">{/set.config.water.processing/}</div>
				<div class="progress">
					<div class="num" style="width:10%;"><span>10%</span></div>
				</div>
				<div class="tips">{/products.sync.progress.tips/}</div>
				<input type="button" class="btn_global" id="btn_progress_continue" value="{/products.sync.progress.continue/}" />
				<input type="button" class="btn_global" id="btn_progress_keep" value="{/set.config.water.proceed/}" />
				<input type="button" class="btn_global" id="btn_progress_cancel" value="{/global.cancel/}" />
				<input type="hidden" name="Start" value="1" />
				<input type="hidden" name="TaskId" value="" />
				<input type="hidden" name="do_action" value="products.<?=$c['manage']['do'];?>_products_sync_status" />
			</div>
		</div>
    </div>
</div>