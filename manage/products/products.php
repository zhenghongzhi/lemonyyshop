<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'products'));//检查权限

$permit_ary=array(
    'yunshu'		=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'yunshu')),
	'add'		=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'add')),
	'edit'		=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'edit')),
	'copy'		=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'copy')),
	'del'		=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'del')),
	'export'	=>	manage::check_permit('products', 0, array('a'=>'products', 'd'=>'export'))
);

if($c['manage']['do']=='index'){
	//产品列表页面
	$no_sort_url='?'.ly200::get_query_string(ly200::query_string('sort'));
	$prod_show=array();
	$cfg_row=str::str_code(db::get_all('config', 'GroupId="products_show"'));
	foreach($cfg_row as $v){
		$prod_show[$v['Variable']]=$v['Value'];
	}
	$used_row=str::json_data(htmlspecialchars_decode($prod_show['Config']), 'decode');
	/*产品后台排序 start*/
	if(db::get_row_count('config', 'GroupId="products" and Variable="sort"')){
		$sort = (int)db::get_value('config', 'GroupId="products" and Variable="sort"', 'Value');
	}else{
		db::insert('config', array(
				'GroupId'	=>	'products',
				'Variable'	=>	'sort',
				'Value'		=>	(int)$used_row['manage_myorder'],
			));
		$sort = (int)$used_row['manage_myorder'];
	}
	if(isset($_GET['sort'])){
		$sort =  (int)$_GET['sort'];
		db::update('config', 'GroupId="products" and Variable="sort"',array(
				'Value'		=>	$sort,
			));
	}
	/*产品后台排序 end*/

	//获取类别列表
	$cate_ary=str::str_code(db::get_all('products_category', '1', '*'));
	$category_ary=array();
	foreach((array)$cate_ary as $v){
		$category_ary[$v['CateId']]=$v;
	}
	$category_count=count($category_ary);
	unset($cate_ary);
	
	//获取所属供应商
//	$business_row=db::get_all('business', 1, 'BId, Name', 'BId desc');
//	$business_ary=array();
//	foreach($business_row as $v){
//		$business_ary[$v['BId']]=$v;
//	}
//	unset($business_row);
	
	//产品列表
	$Keyword=$_GET['Keyword'];
	$CateId=(int)$_GET['CateId'];
	$Other=(int)$_GET['Other'];
	$Status=(int)$_GET['Status'];
	$Status==0 && $Status=1;//默认仅显示上架产品
	
	$where='1';//条件
	$page_count=50;//显示数量
	//$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%' or ProId in(select ProId from products_selected_attribute_combination where SKU like '%$Keyword%'))";
	$Keyword && $where.=" and (Name{$c['manage']['web_lang']} like '%$Keyword%' or concat_ws('', Prefix, Number) like '%$Keyword%' or SKU like '%$Keyword%')";
	if($CateId){
        //add by ueeshop start
//		$where.=" and (CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($CateId)."%') or CateId='{$CateId}')";
        $where.=" and ((CateId in(select CateId from products_category where UId like '".category::get_UId_by_CateId($CateId)."%') or CateId='{$CateId}') or find_in_set($CateId,ExtCateId))";
        //add by ueeshop end
        $category_one=str::str_code(db::get_one('products_category', "CateId='$CateId'"));
		$UId=$category_one['UId'];
		$UId!='0,' && $TopCateId=category::get_top_CateId_by_UId($UId);
	}
	if($Status){
		switch($Status){
			case 1: $where.=$c['manage']['where']['products']; break;
			case 2: $where.=" and ((SoldStatus=2 and Stock<=0) or (SoldOut=1 and IsSoldOut=0) or (SoldOut=1 and IsSoldOut=1 and (SStartTime>{$c['time']} or SEndTime<{$c['time']})))"; break;
		}
	}
	if($Other){
		switch($Other){
			case 1: $where.=' and IsNew=1'; break;
			case 2: $where.=' and IsHot=1'; break;
			case 3: $where.=' and IsBestDeals=1'; break;
			case 4: $where.=' and IsIndex=1'; break;
			case 5: $where.=' and IsSoldOut=1'; break;
			case 6: $where.=" and Stock<={$c['manage']['warning_stock']}"; break;
			case 7: $where.=" and Stock<MOQ && Stock<=0"; break;
			case 8: $where.=" and FacebookId!=''"; break;
		}
	}
	$products_row=str::str_code(db::get_limit_page('products', $where, '*', ($sort ? $c['my_order'] : '').'ProId desc', (int)$_GET['page'], $page_count));

	$column_row=db::get_value('config', "GroupId='custom_column' and Variable='Products'", 'Value');
	$custom_ary=str::json_data($column_row, 'decode');
	$column_fixed_ary=array('picture', 'name', 'classify', 'products.price', 'products.other');
	$column_ary=array('picture', 'name', 'products.business', 'classify', 'products.price', 'products.other', 'products.weight', 'products.cubage', 'products.stock', 'products.edit_time');
}

$pro_menu_ary=array('pic_info', 'classify_info', 'basic_info', 'sales_info', 'attr_info', 'attr_price_info', 'tag_info', 'seo_info', 'freight_info', 'platform_info');
$top_id_name=($c['manage']['do']=='index'?'products':'products_inside');

echo ly200::load_static('/static/js/plugin/facebook/store.css', '/static/js/plugin/facebook/store.js');
?>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php if($c['manage']['do']=='index'){?>
		<script type="text/javascript">$(document).ready(function(){products_obj.products_init();});</script>
		<div class="inside_container">
			<h1>{/module.products.products/}</h1>
			<ul class="inside_menu">
				<li><a href="./?m=products&a=products&Status=1"<?=$Status==1?' class="current"':'';?>>{/products.products.sold_in/}</a></li>
				<li><a href="./?m=products&a=products&Status=2"<?=$Status==2?' class="current"':'';?>>{/products.products.sold_out/}</a></li>
			</ul>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=products&a=products&d=edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<?php if($permit_ary['edit']){?>
						<?php if($Status!=1){?><li><a class="sold_in" href="javascript:;">{/products.products.sold_in/}</a></li><?php }?>
						<?php if($Status!=2){?><li><a class="sold_out" href="javascript:;">{/products.products.sold_out/}</a></li><?php }?>
						<li><a class="edit bat_close" href="javascript:;">{/products.products.batch_edit/}</a></li>
					<?php }?>
					<?php if(in_array('facebook_ads_extension', $c['manage']['plugins']['Used'])){?>
						<li>
							<a class="facebook" href="javascript:;">{/plugins.global.other.facebook_store/}</a>
							<a class="facebook_release" href="javascript:;">{/global.release/}</a>
							<a class="facebook_delete" href="javascript:;">{/global.del/}</a>
							<em class="facebook_border"></em>
						</li>
					<?php }?>
					<li>
						<a class="more" href="javascript:;">{/global.more/}<em></em></a>
						<div class="more_menu drop_down">
							<?php if($permit_ary['export']){?>
								<a class="explode item" href="./?m=products&a=products&d=explode">{/global.explode/}</a>
							<?php }?>
							<?php if($permit_ary['edit'] && in_array('upload', $c['manage']['plugins']['Used'])){?>
								<a class="explode item" href="./?m=products&a=products&d=upload">{/products.global.upload/}</a>
							<?php }?>
							<a class="item" href="?m=products&a=products&d=batch_edit&type=0">{/products.batch.batch_price/}</a>
                            <a class="item" href="?m=products&a=products&d=batch_edit&type=1">{/products.batch.batch_move_category/}</a>
                            <a class="item" href="?m=products&a=yunshumore&d=yunshumore">批量修改物流</a>
						</div>
					</li>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows">
								<div class="box_explain">{/products.explain.category/}</div>
							</div>
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', $CateId, 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
								</div>
							</div>
							<div class="rows item clean">
								<label>{/products.products.other/}</label>
								<div class="input">
									<div class="box_select">
										<select name="Other">
											<option value="0">{/global.select_index/}</option>
											<option value="1"<?=$Other==1?' selected':'';?>>{/products.products.is_new/}</option>
											<option value="2"<?=$Other==2?' selected':'';?>>{/products.products.is_hot/}</option>
											<option value="3"<?=$Other==3?' selected':'';?>>{/products.products.is_best_deals/}</option>
											<option value="4"<?=$Other==4?' selected':'';?>>{/products.products.is_index/}</option>
											<option value="5"<?=$Other==5?' selected':'';?>>{/products.products.sold_in_time/}</option>
											<option value="6"<?=$Other==6?' selected':'';?>>{/products.products.warn_stock/}</option>
											<option value="7"<?=$Other==7?' selected':'';?>>{/products.products.not_stock/}</option>
											<?php if(in_array('facebook_ads_extension', $c['manage']['plugins']['Used'])){?>
												<option value="8"<?=$Other==8?' selected':'';?>>{/plugins.global.other.facebook_store/}</option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="products" />
						<input type="hidden" name="Status" value="<?=$_GET['Status'];?>" />
					</form>
				</div>
			</div>
			<?php
			if($products_row[0]){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="3%" nowrap="nowrap">{/products.picture/}</td>
							<td width="50%" nowrap="nowrap">{/products.name/} / {/products.classify/}</td>
							<td width="20%" nowrap="nowrap">{/products.products.number/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price/}</td>
							<td width="10%" nowrap="nowrap">{/products.products.price_rate/}</td>
							<td width="8%" nowrap="nowrap">{/products.products.stock/}</td>
							<td width="12%" nowrap="nowrap">{/products.products.edit_time/}</td>
							<td width="10%" nowrap="nowrap">
								<a href="<?=$no_sort_url.'&sort='.($sort ? 0 : 1); ?>" class="sort_box <?=$sort ? 'asc' : 'desc'; ?>">
									{/global.my_order/}
								</a>
							</td>
							<?php if($permit_ary['edit'] || $permit_ary['copy'] || $permit_ary['del']|| $permit_ary['yunshu'])
							{?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php 
							}?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($products_row[0] as $v){
							$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
							$name=htmlspecialchars_decode($v['Name'.$c['manage']['web_lang']]);
							$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
						?>
							<tr data-id="<?=$v['ProId'];?>">
								<?php if($permit_ary['edit'] || $permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['ProId']);?></td><?php }?>
								<td class="img"><a href="<?=$url;?>" target="_blank" class="pic_box"><img src="/static/themes/default/images/global/loading_oth.gif" data-src="<?=$img;?>" class="loading_img" /><span></span></a></td>
								<td class="info">
									<a class="name" href="<?=$url;?>" target="_blank"><?=$name;?></a>
									<div class="classify color_888<?php /*<?=$permit_ary['edit']?' category_select':'';*/?>" cateid="<?=$v['CateId'];?>">
										<?php
										$UId=$category_ary[$v['CateId']]['UId'];
										if($UId){
											$key_ary=@explode(',',$UId);
											array_shift($key_ary);
											array_pop($key_ary);
											foreach((array)$key_ary as $k2=>$v2){
												echo $category_ary[$v2]['Category'.$c['manage']['web_lang']].' / ';
											}
										}
										echo $category_ary[$v['CateId']]['Category'.$c['manage']['web_lang']];
										?>
									</div>
									<div class="clean">
										<?php
										echo $v['IsNew']?'<div class="other_box fl">{/products.products.is_new/}</div>':'';
										echo $v['IsHot']?'<div class="other_box fl">{/products.products.is_hot/}</div>':'';
										echo $v['IsBestDeals']?'<div class="other_box fl">{/products.products.is_best_deals/}</div>':'';
										echo $v['IsIndex']?'<div class="other_box fl">{/products.products.is_index/}</div>':'';
										echo $v['FacebookId']?'<div class="other_box fl" data-name="facebook">{/plugins.global.other.facebook_store/}</div>':'';
										?>
									</div>
								</td>
								<td nowrap="nowrap">#<?=$v['Prefix'].$v['Number'];?></td>
								<td nowrap="nowrap"<?=$permit_ary['edit']?' class="price_input"':'';?>>
									<span class="Price_1" price="<?=sprintf('%01.2f', $v['Price_1']);?>"><?=$c['manage']['currency_symbol'];?><span><em class="ajax_upload_btn"><?=sprintf('%01.2f', $v['Price_1']);?></em></span></span>
								</td>
								<td nowrap="nowrap"<?=$permit_ary['edit']?' class="price_input"':'';?>>
									<?=($v["price_rate"]*100)."%";?>
								</td>
								<td nowrap="nowrap" <?=$v['Stock']<=$c['manage']['warning_stock']?' class="fc_red"':'';?>><?=$v['Stock'];?></td>
								<td nowrap="nowrap" class="time_data"><?=$v['EditTime']?date('Y-m-d', $v['EditTime']):'N/A';?></td>
								<td nowrap="nowrap">
									<div <?=$permit_ary['edit']?' class="myorder_select" data-num="'.$v['MyOrder'].'"':'';?>><em class="ajax_upload_btn"><?=$c['manage']['my_order'][$v['MyOrder']];?></em></div>
								</td>
								<?php if($permit_ary['edit'] || $permit_ary['copy'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=products&d=edit&ProId=<?=$v['ProId'];?>">{/global.edit/}</a><?php }?>
										
										<br>
										<?php if($permit_ary['yunshu']){?><a  href="./?m=products&a=yunshu&d=yunshu&ProId=<?=$v['ProId'];?>">{/global.yunshu/}</a><?php }?>
										<?php if($permit_ary['copy'] || $permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down">
													<?php if($permit_ary['copy']){?><a class="copy item" href="./?do_action=products.products_copy&ProId=<?=$v['ProId'];?>">{/global.copy/}</a><?php }?>
													<?php if($permit_ary['del']){?><a class="del item" href="./?do_action=products.products_del&ProId=<?=$v['ProId'];?>" rel="del">{/global.del/}</a><?php }?>
												</dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
				<?=str_replace('条', '个产品', html::turn_page($products_row[1], $products_row[2], $products_row[3], '?'.ly200::query_string('page').'&page='));?>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=products&a=products&d=edit');
			}?>
		</div>
		<?php /*<div id="category_select_hide" class="hide"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>*/?>
		<div id="myorder_select_hide" class="hide"><div class="box_select"><?=ly200::form_select($c['manage']['my_order'], "MyOrder[]", '');?></div></div>
	<?php
	}elseif($c['manage']['do']=='explode'){
		//产品导出页面
		$pagenum_ary = array(50,100,150,200);
	?>
        <script type="text/javascript">$(document).ready(function(){products_obj.explode_init()});</script>
	<div class="center_container">	
		<form id="explode_edit_form" class="global_form">
			<div class="global_container">
            	<a href="javascript:history.back(-1);" class="return_title">
                    <span class="return">{/module.products.products/}</span> 
                    <span class="s_return">/ {/products.global.export/}</span>
                </a>
				<div class="rows clean">
					<label>{/products.classify/}</label>
					<div class="input">
						<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>
					</div>
				</div>
				<div class="rows clean">
					<label>{/products.explode.expage/}</label>
					<div class="input">
						<div class="box_select">
							<select name="PageNum">
								<?php foreach((array)$pagenum_ary as $v){?>
									<option value="<?=$v;?>"<?=$v==200?' selected':'';?>><?=$v;?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="rows clean">
					<label>{/global.my_order/}</label>
					<div class="input">
						<div class="box_select">
							<select name="MyOrder">
								<?php for($i=0; $i<3; ++$i){?>
									<option value="<?=$i;?>">{/products.explode.myorder.<?=$i;?>/}</option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="rows clean">
					<label>{/set.config.language_list/}</label>
					<div class="input">
						<div class="box_select">
							<select name="Language">
								<?php foreach($c['manage']['web_lang_list'] as $v){?>
									<option value="<?=$v;?>">{/language.<?=$v;?>/}</option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="rows clean">
					<label></label>
					<div class="input input_button">
						<input type="submit" class="btn_global btn_submit" value="{/global.explode/}" />
						<a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
					</div>
				</div>
				<div class="rows clean">
					<label>{/products.upload.export_progress/}</label>
					<div id="explode_progress"></div>
				</div>
				<input type="hidden" name="do_action" value="products.products_explode" />
				<input type="hidden" name="Number" value="0" />
			</div>
		</form>
    </div>
	<?php
    }elseif($c['manage']['do']=='batch_edit'){
		//产品批量修改页面
		if($_GET['proid_list']){
			//选择产品
			echo ly200::load_static('/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js');
	?>
			<script type="text/javascript">$(document).ready(function(){products_obj.batch_edit_init()});</script>
			<div id="batch">
				<a href="javascript:history.back(-1);" class="return_title">
					<span class="return">{/module.products.products/}</span>
					<span class="s_return">/ {/products.products.batch_edit/}</span>
				</a>
				<form name="batch_form" id="batch_form" class="clean">
					<table border="0" cellpadding="5" cellspacing="0" class="r_con_table r_con_form">
						<thead>
							<tr>
								<td width="8%" nowrap="nowrap">{/products.picture/}</td>
								<td width="15%" nowrap="nowrap">{/products.name/}</td>
								<td width="20%" nowrap="nowrap">{/products.products.price/}</td>
								<td width="25%" nowrap="nowrap">{/products.products.sale_status/}</td>
								<td width="20%" nowrap="nowrap">{/products.global.other_attr/}</td>
								<td width="12%" nowrap="nowrap">{/global.my_order/}</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$cfg_row=str::str_code(db::get_all('config', 'GroupId in("business", "products_show")'));
							foreach($cfg_row as $v){
								$cfg_ary[$v['GroupId']][$v['Variable']]=$v['Value'];
							}
							$used_row=str::json_data(htmlspecialchars_decode($cfg_ary['products_show']['Config']), 'decode');

							$proid_list=str_replace('-', ',', trim($_GET['proid_list']));
							$where="ProId in($proid_list)";
							$sort = (int)db::get_value('config', 'GroupId="products" and Variable="sort"', 'Value');
							$products_row=db::get_all('products', $where, '*', ($sort ? $c['my_order'] : '').'ProId desc');
							foreach($products_row as $v){
								$img=ly200::get_size_img($v['PicPath_0'], end($c['manage']['resize_ary']['products']));
								$name=htmlspecialchars_decode($v['Name'.$c['manage']['web_lang']]);
								$url=ly200::get_url($v, 'products', $c['manage']['web_lang']);
							?>
								<tr class="rows">
									<td class="img"><a href="<?=$url;?>" target="_blank" class="pic_box"><img src="/static/themes/default/images/global/loading_oth.gif" data-src="<?=$img;?>" class="loading_img" /><span></span></a></td>
									<td><a href="<?=$url;?>" target="_blank"><?=$name;?></a></td>
									<td nowrap="nowrap" class="input">
										<span class="unit_input"><b>{/products.products.price_ary.1/}<?=$c['manage']['currency_symbol']?></b><input name="Price_1[]" value="<?=$v['Price_1'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" notnull /></span>
										<div class="blank6"></div>
										<span class="unit_input"><b>{/products.products.price_ary.0/}<?=$c['manage']['currency_symbol']?></b><input name="Price_0[]" value="<?=$v['Price_0'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" <?=(int)$cfg_ary['products_show']['price']?'notnull':'';?> /></span>
										<div class="blank6"></div>
										<?php /*
										<span class="unit_input"><b>{/products.products.price_ary.2/}<?=$c['manage']['currency_symbol']?></b><input name="PurchasePrice[]" value="<?=$v['PurchasePrice'];?>" type="text" class="box_input" size="5" maxlength="10" rel="amount" /></span>
										<div class="blank6"></div>
										*/?>
									</td>
									<td nowrap="nowrap" class="input">
										<div class="switchery<?=$v['SoldOut']==1?' checked':'';?>">
											<input type="checkbox" name="SoldOut[<?=$v['ProId'];?>]" value="1" class="SoldOutInput"<?=$v['SoldOut']==1?' checked':'';?> />
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
										{/products.products.sold_out/}<span class="tool_tips_ico" content="{/products.products.soldOut_notes/}"></span>
										<div class="blank12"></div>
										<div id="sold_out_div" style="display:<?=$v['SoldOut']==1?'':'none';?>;">
											<div class="switchery<?=$v['IsSoldOut']==1?' checked':'';?>">
												<input type="checkbox" name="IsSoldOut[<?=$v['ProId'];?>]" value="1" class="IsSoldOutInput"<?=$v['IsSoldOut']==1?' checked':'';?> />
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
											{/products.products.sold_in_time/}<span class="tool_tips_ico" content="{/products.products.soldIn_notes/}"></span>
											<div class="blank6"></div>
											<span class="sold_in_time" style="display:<?=$v['IsSoldOut']==1?'':'none';?>;">{/products.products.sold_in_daytime/}: <input name="SoldOutTime[]" value="<?=date('Y-m-d H:i:s',($v['SStartTime']?$v['SStartTime']:$c['time'])).'/'.date('Y-m-d H:i:s', ($v['SEndTime']?$v['SEndTime']:$c['time']));?>" type="text" class="box_input input_time" size="45" readonly></span>
										</div>
									</td>
									<td class="other">
										<div class="switchery<?=$v['IsFreeShipping']?' checked':'';?>">
											<input type="checkbox" value="1" name="IsFreeShipping[<?=$v['ProId'];?>]"<?=$v['IsFreeShipping']?' checked':'';?> />
											<div class="switchery_toggler"></div>
											<div class="switchery_inner">
												<div class="switchery_state_on"></div>
												<div class="switchery_state_off"></div>
											</div>
										</div>
										{/products.products.free_shipping/}<span class="tool_tips_ico" content="{/products.products.free_shipping_notes/}"></span>
										<div class="blank12"></div>
										<span class="input_checkbox_box<?=$v['IsIndex']?' checked':'';?>">
											<span class="input_checkbox">
												<input type="checkbox" name="IsIndex[<?=$v['ProId'];?>]" value="1"<?=$v['IsIndex']?' checked':'';?> />
											</span>{/products.products.is_index/}
										</span>
										<span class="tool_tips_ico" content="{/products.products.index_notes/}"></span>&nbsp;&nbsp;&nbsp;&nbsp;
										<span class="input_checkbox_box<?=$v['IsNew']?' checked':'';?>">
											<span class="input_checkbox">
												<input type="checkbox" name="IsNew[<?=$v['ProId'];?>]" value="1"<?=$v['IsNew']?' checked':'';?> />
											</span>{/products.products.is_new/}
										</span>
										<span class="tool_tips_ico" content="{/products.products.new_notes/}"></span>
										<div class="blank12"></div>
										<span class="input_checkbox_box<?=$v['IsHot']?' checked':'';?>">
											<span class="input_checkbox">
												<input type="checkbox" name="IsHot[<?=$v['ProId'];?>]" value="1"<?=$v['IsHot']?' checked':'';?> />
											</span>{/products.products.is_hot/}
										</span>
										<span class="tool_tips_ico" content="{/products.products.hot_notes/}"></span>&nbsp;&nbsp;&nbsp;&nbsp;
										<span class="input_checkbox_box<?=$v['IsBestDeals']?' checked':'';?>">
											<span class="input_checkbox">
												<input type="checkbox" name="IsBestDeals[<?=$v['ProId'];?>]" value="1"<?=$v['IsBestDeals']?' checked':'';?> />
											</span>{/products.products.is_best_deals/}
										</span>
										<span class="tool_tips_ico" content="{/products.products.best_deals_notes/}"></span>
									</td>
									<td nowrap="nowrap">
										<div class="box_select"><?=ly200::form_select($c['manage']['my_order'], 'MyOrder[]', $v['MyOrder']);?></div>
									</td>
									<input type="hidden" name="ProId[]" value="<?=$v['ProId'];?>" />
									<input type="hidden" name="SoldStatus[<?=$v['ProId'];?>]" value="<?=$v['SoldStatus'];?>" />
								</tr>
							<?php }?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6"><input type="submit" class="btn_global btn_submit" value="{/global.submit/}" /></td>
							</tr>
						</tfoot>
						<input type="hidden" name="do_action" value="products.products_batch_price" />
					</table>
				</form>
			</div>
        <?php
        }else{
			//全选产品
		?>
			<script type="text/javascript">$(document).ready(function(){products_obj.batch_edit_init()});</script>
			<div class="inside_container">
				<h1>{/module.products.products/}</h1>
				<ul class="inside_menu unusual">
					<li><a href="?m=products&a=products&d=batch_edit&type=0"<?=$_GET['type']==0?' class="current"':''?>>{/products.batch.batch_price/}</a></li>
					<li><a href="?m=products&a=products&d=batch_edit&type=1"<?=$_GET['type']==1?' class="current"':''?>>{/products.batch.batch_move_category/}</a></li>
					<!-- <li><a href="?m=products&a=products&d=batch_edit&type=2"<?=$_GET['type']==2?' class="current"':''?>>{/products.batch.batch_watermark/}</a></li> -->
				</ul>
			</div>
			<?php
			if($_GET['type']==0){
				//批量修改价格
			?>
				<form id="batch_edit_form" class="global_form" name="batch_price_form">
					<div class="global_container center_container_1000">
						<div class="rows clean">
							<label>{/products.classify/}</label>
							<div class="input">
								<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
							</div>
						</div>
						<div class="rows clean">
							<label>{/global.type/}</label>
							<div class="input type">
								<span class="btn_choice current">{/products.batch.type_price/}<input type="radio" name="Type" value="0" checked /></span>
								<span class="btn_choice">{/products.batch.type_rate/}<input type="radio" name="Type" value="1" /></span>
							</div>
						</div>
						<div class="choice_box">
							<div class="rows clean">
								<label>{/products.batch.type_price/}<span class="tool_tips_ico" content="{/products.batch.type_price_tips/}"></span></label>
								<span class="input">
									<span class="unit_input"><b>{/products.products.price_ary.1/} <?=$c['manage']['currency_symbol'];?></b><input type="text" class="box_input" name="Price" value="0.00" size="5" maxlength="10" rel="amount" data-key="-" /></span>
								</span>
							</div>
							<div class="rows clean" style="display:none;">
								<label>{/products.batch.type_rate/}<span class="tool_tips_ico" content="{/products.batch.type_rate_tips/}"></span></label>
								<span class="input">
									<span class="unit_input"><b>{/products.products.price_ary.1/}</b><input type="text" class="box_input" name="Rate" value="0" size="5" maxlength="10" rel="amount" data-key="-" /><b class="last">%</b></span>
								</span>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
								<a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
							</div>
						</div>
					</div>
					<input type="hidden" name="do_action" value="products.products_batch_price" />
				</form>
			<?php
			}elseif($_GET['type']==1){
				//批量移动产品
			?>
				<form id="batch_edit_form" class="global_form" name="batch_move_form">
					<div class="global_container center_container_1000">
						<div class="rows clean">
							<label>{/products.batch.source/}</label>
							<div class="input">
								<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.batch.target/}</label>
							<div class="input">
								<div class="box_select"><?=category::ouput_Category_to_Select('CateIdTo', '', 'products_category', 'UId="0,"', 1, '', '{/global.select_index/}');?></div>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
								<a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
							</div>
						</div>
					</div>
					<input type="hidden" name="do_action" value="products.products_batch_move" />
				</form>
        	<?php
			}else{
				//批量更新水印
			?>
				<form id="batch_edit_form" class="global_form" name="batch_watermark_form">
					<div class="global_container center_container_1000">
						<div class="rows clean">
							<label>{/products.classify/}</label>
							<div class="input">
								<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>
							</div>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
								<a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
								<input type="hidden" name="Number" value="0" />
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.upload.progress/}</label>
							<div id="explode_progress"></div>
						</div>
					</div>
					<input type="hidden" name="do_action" value="products.products_batch_watermark" />
				</form>
        <?php
			}
		}
		?>
	<?php
	}elseif($c['manage']['do']=='upload'){
		//产品批量上传
		echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
	?>
		<!--[if (gte IE 8)&(lt IE 10)]><script src="/static/js/plugin/file_upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
		<script type="text/javascript">$(document).ready(function(){products_obj.upload_init()});</script>
        <div class="center_container_1200 center_container">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">{/module.products.products/}</span> 
                <span class="s_return">/ {/products.global.upload/}</span>
            </a>
            <form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <div class="global_container">
					<div id="form_container">
						<div class="explode_box">
							<div class="box_item">
								<div class="tit"><em>1</em>{/global.down_table/}</div>
								<div class="desc"><?=str_replace('%url%', './?do_action=products.upload_new_excel_download', $c['manage']['lang_pack']['global']['click_here']); ?></div>
							</div>
							<div class="box_item">
								<div class="tit"><em>2</em>{/global.upload_table/}</div>
								<div class="desc">{/global.submit_table/}</div>
								<div class="input upload_file">
									<input name="ExcelFile" value="" type="text" class="box_input" id="excel_path" size="50" maxlength="100" readonly notnull />
									<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
									<div class="row fileupload-buttonbar">
										<span class="btn_file btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>{/global.file_upload/}</span>
											<input type="file" name="Filedata" multiple>
										</span>
										<div class="fileupload-progress fade"><div class="progress-extended"></div></div>
										<div class="clear"></div>
										<div class="photo_multi_img template-box files"></div>
										<div class="photo_multi_img" id="PicDetail"></div>
									</div>
									<script id="template-upload" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										<div class="template-upload fade">
											<div class="clear"></div>
											<div class="items">
												<p class="name">{%=file.name%}</p>
												<strong class="error text-danger"></strong>
											</div>
											<div class="items">
												<p class="size">Processing...</p>
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
											</div>
											<div class="items">
												{% if (!i) { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
									</script>
									<script id="template-download" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										{% if (file.thumbnailUrl) { %}
											<div class="pic template-download fade hide">
												<div>
													<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
													<a href="{%=file.url%}" class="zoom" target="_blank"></a>
													{% if (file.deleteUrl) { %}
														<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } %}
													<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
												</div>
												<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
											</div>
										{% } else { %}
											<div class="template-download fade hide">
												<div class="clear"></div>
												<div class="items">
													<p class="name">
														{% if (file.url) { %}
															<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
														{% } else { %}
															<span>{%=file.name%}</span>
														{% } %}
													</p>
													{% if (file.error) { %}
														<div><span class="label label-danger">Error</span> {%=file.error%}</div>
													{% } %}
												</div>
												<div class="items">
													<span class="size">{%=o.formatFileSize(file.size)%}</span>
												</div>
												<div class="items">
													{% if (file.deleteUrl) { %}
														<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
															<i class="glyphicon glyphicon-trash"></i>
															<span>{/global.del/}</span>
														</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } else { %}
														<button class="btn_file btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>{/global.cancel/}</span>
														</button>
													{% } %}
												</div>
												<div class="clear"></div>
											</div>
										{% } %}
									{% } %}
									</script>
								</div>
								<div class="rows clean">
									<label>{/set.config.language_list/}</label>
									<div class="input">
										<div class="box_select">
											<select name="Language">
												<?php foreach($c['manage']['web_lang_list'] as $v){?>
													<option value="<?=$v;?>">{/language.<?=$v;?>/}</option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
								<div class="rows clean">
									<label></label>
									<div class="input input_button">
										<input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
										<a href="./?m=products&a=products" class="btn_global btn_cancel">{/global.return/}</a>
										<input type="button" class="btn_global btn_picture" value="" style="display:none;" />
										<input type="hidden" name="do_action" value="products.upload" />
										<input type="hidden" name="Number" value="0" />
										<input type="hidden" name="Current" value="0" />
									</div>
								</div>
							</div>
							<div class="box_item">
								<div class="tit2">{/products.upload.explanation/}</div>
								<?php 
									$explanation_row = @explode('%br%', $c['manage']['lang_pack']['products']['upload']['explanation_txt']);
								?>
								<ul>
									<?php foreach((array)$explanation_row as $v){ ?>
										<li><?=trim($v); ?></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<div id="progress_container">
						<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
							<thead>
								<tr>
									<td width="5%" nowrap="nowrap">{/global.serial/}</td>
									<td width="40%" nowrap="nowrap">{/products.products.pro_name/}</td>
									<td width="15%" nowrap="nowrap">{/products.products.number/}</td>
									<td width="5%" nowrap="nowrap">{/products.pic_upfile_oth/}</td>
									<td width="35%" nowrap="nowrap">{/global.status/}</td>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div id="progress_loading"></div>
					</div>
				</div>
			</form>
        </div>
	</div>
	<?php
	}else{
		//产品编辑
		$ProId=(int)$_GET['ProId'];
		$products_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
		$products_seo_row=str::str_code(db::get_one('products_seo', "ProId='$ProId'"));
		$products_description_row=str::str_code(db::get_one('products_description', "ProId='$ProId'"));

		$cfg_row=str::str_code(db::get_all('config', 'GroupId in("business", "products", "products_show")'));
		foreach($cfg_row as $v){
			$cfg_ary[$v['GroupId']][$v['Variable']]=$v['Value'];
		}
		$used_row=str::json_data(htmlspecialchars_decode($cfg_ary['products_show']['Config']), 'decode');

		if(!$ProId && $cfg_ary['products_show']['favorite']){//自定义收藏数范围
			$range_ary=str::json_data(htmlspecialchars_decode($cfg_ary['products_show']['favorite']), 'decode');
			if(count($range_ary)==2 && (int)$range_ary[0]>0 && (int)$range_ary[1]>0) $products_row['FavoriteCount']=rand((int)$range_ary[0], (int)$range_ary[1]);
		}
		
		$OpenParameter=array();
		if($products_row['OpenParameter']){
			$OpenParameter=explode('|', $products_row['OpenParameter']);
		}
		
		//产品单位
		$Unit='';
		$IsUnitShow=0;
		if($products_row['Unit']){
			$Unit=$products_row['Unit'];
			$IsUnitShow=1;
		}elseif($used_row['item']){//是否开启产品自定义单位
			$Unit=$cfg_ary['products_show']['item'];
			$IsUnitShow=1;
		}
		$unit_list=str::json_data(htmlspecialchars_decode($cfg_ary['products']['Unit']), 'decode');

		//添加产品
		if($ProId==0){
			if(!isset($_SESSION['AddProduct']) || !$_SESSION['AddProduct']){
				$_AddProId=1;//临时添加ID
				$_SESSION['AddProduct'][$_AddProId]=array();
			}else{
				$_SESSION['AddProduct'];
				end($_SESSION['AddProduct']);//最后一个临时添加ID
				$add_pro_ary=each($_SESSION['AddProduct']);
				$_AddProId=$add_pro_ary['key']+1;
				$_SESSION['AddProduct'][$_AddProId]=array();
			}
		}
		$VideoFirst=(int)db::get_value('products_development', "ProId='{$ProId}'", 'VideoFirst');
		echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
	?>
		<script type="text/javascript">$(document).ready(function(){products_obj.products_edit_init()});</script>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.products.products/}</span> 
				<span class="s_return">/ <?=$ProId?'{/global.edit/}':'{/global.add/}';?></span>
			</a>
		</div>
		<form id="edit_form" class="global_form center_container_1200">
			<div class="left_container">
				<div class="left_container_side">
					<?php /***************************** 产品图片 Start *****************************/?>
					<div class="global_container" data-name="<?=$pro_menu_ary[0];?>">
						<div class="big_title">{/products.products.<?=$pro_menu_ary[0];?>/}</div>
						<div class="rows clean">
							<div class="input">
								<div class="tips"><?=str_replace(array('%count%', '%format%', '%size%'), array('12', 'jpg', '640*640'), $c['manage']['lang_pack']['notes']['new_pic_tips']);?></div>
								<div class="multi_img upload_file_multi pro_multi_img" id="PicDetail">
									<?php if ($VideoFirst) {?>
										<dl class="video">
											<dt class="upload_box">
												<input type="button" id="VideoUpload" class="btn_ok upload_btn <?=$products_row['VideoUrl']?'is_video' : ''; ?>" name="submit_button" value="{/global.upload_pic/}" tips="" />
												<input type="hidden" name="VideoUrl" value="" data-value="" save="" />
											</dt>
										</dl>
									<?php }?>
									<?php
									$j = 0;
									for ($i = 0; $i < $c['prod_image']['count']; ++$i) {
										echo manage::multi_img_item('PicPath[]', $products_row["PicPath_{$i}"], $i ,ly200::get_size_img($products_row["PicPath_{$i}"], '240x240'));
										++$j;
									}?>
									<?php if ($j < $c['prod_image']['count'] ){?>
										<dl class="img" num="<?=$j;?>">
											<dt class="upload_box preview_pic">
												<input type="button" id="PicUpload_<?=$j;?>" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" />
												<input type="hidden" name="PicPath[]" value="" data-value="" save="" />
											</dt>
										</dl>
									<?php }?>
									<?php if (!$VideoFirst){?>
										<dl class="video">
											<dt class="upload_box">
												<input type="button" id="VideoUpload" class="btn_ok upload_btn <?=$products_row['VideoUrl']?'is_video' : ''; ?>" name="submit_button" value="{/global.upload_pic/}" tips="" />
												<input type="hidden" name="VideoUrl" value="" data-value="" save="" />
											</dt>
										</dl>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
					<?php /***************************** 产品图片 End *****************************/?>
					<?php /***************************** 所属分类 Start *****************************/?>
					<div class="global_container" data-name="<?=$pro_menu_ary[1];?>">
						<div class="big_title">{/products.products.<?=$pro_menu_ary[1];?>/}</div>
						<div class="big_notes color_aaa">{/products.products.classify_notes/}</div>
						<div class="rows clean">
							<div class="input">
								<a href="javascript:;" class="btn_global btn_item_sec btn_add" id="btn_expand">{/products.products.expand/}</a>
								<div class="classify">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', $products_row['CateId'], 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}');?></div>
								</div>
								<ul class="expand_list">
									<?php
									if($products_row['ExtCateId']){
										$ext_ary=explode(',',substr($products_row['ExtCateId'], 1, -1));
										foreach((array)$ext_ary as $v){
											echo '<li><div class="box_select">'.category::ouput_Category_to_Select('ExtCateId[]', $v, 'products_category', 'UId="0,"', 1, 'notnull', '{/global.select_index/}').'</div><a href="javascript:;" class="close icon_delete_1"><i></i></a></li>';
										}
									}?>
								</ul>
							</div>
						</div>
					</div>
					<?php /***************************** 所属分类 End *****************************/?>
					<?php /***************************** 基础信息 Start *****************************/?>
					
					<div class="global_container" data-name="<?=$pro_menu_ary[2];?>">
					
						<div class="big_title">{/products.products.<?=$pro_menu_ary[2];?>/}</div>
				<div class="rows clean translation multi_lang">
					<label><strong>定金比例</strong></label>
					<div class="input">
						<input type="text" class="box_input" name="price_rate" value="<?=$products_row["price_rate"]*100?>" size="8" maxlength="15" pattern="^(\d{1,2}(\.\d+)?|100|NA)">%
					</div>
				</div>
				<dl class="box_basic_more">
							<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>
							<dd class="drop_down">
								<a href="javascript:;" class="item input_checkbox_box btn_open_unit<?=in_array('Unit', $OpenParameter)?' checked':'';?>">
									<em class="input_checkbox"><input type="checkbox" name="OpenParameter[]" value="Unit"<?=in_array('Unit', $OpenParameter)?' checked':'';?> /></em>
									<span>{/products.unit/}</span>
								</a>
								<?php /*
								<a href="javascript:;" class="item input_checkbox_box btn_open_sku<?=in_array('SKU', $OpenParameter)?' checked':'';?>">
									<em class="input_checkbox"><input type="checkbox" name="OpenParameter[]" value="SKU"<?=in_array('SKU', $OpenParameter)?' checked':'';?> /></em>
									<span>SKU</span>
								</a>
								*/?>
								<a href="javascript:;" class="item input_checkbox_box btn_open_attr add_attr" id="add_general_attribute"><em></em><span>{/products.global.add_attribute/}</span></a>
								<a href="javascript:;" class="item input_checkbox_box btn_open_attr edit_attr" id="edit_general_attribute"><em></em><span>{/products.global.modify_attribute/}</span></a>
							</dd>
						</dl>
						<div class="rows clean translation multi_lang">
							<label>{/products.name/}</label>
							<div class="input">
								<?=manage::unit_form_edit($products_row, 'text', 'Name', 53, 255, 'notnull');?>
							</div>
						</div>
						<?php
						$IsNumShow=1;
						$Number=$products_row['Number'];
						$Prefix=$products_row['Prefix'];
						if($cfg_ary['products_show']['myorder'] && $cfg_ary['products_show']['NumberSort'] && !$ProId){//开启产品编号自动排序
							$max_num=(int)db::get_max('products', "Prefix='{$cfg_ary['products_show']['myorder']}' and Number!=''", 'ProId');
							$Number=$max_num+1;
							$Prefix=$cfg_ary['products_show']['myorder'];
							$IsNumShow=1;
						}
						if($Prefix) $IsNumShow=1;
						?>
						<div class="rows clean">
							<label>{/products.products.number/}</label>
							<div class="input"><?php if($IsNumShow){?><input type="text" class="box_input" name="Prefix" value="<?=$Prefix;?>" size="8" maxlength="15" /> - <?php }?><input type="text" class="box_input" name="Number" value="<?=$Number;?>" size="53" maxlength="50" notnull /></div>
						</div>
						<div class="rows" id="prod_info_unit" style="display:<?=(in_array('Unit', $OpenParameter) && $IsUnitShow)?'':'none';?>;">
							<label>{/products.unit/}</label>
							<span class="input input_unit">
								<?=manage::option_to_select('Unit', 'UnitValue', $Unit, $unit_list, '', 1, 1);?>
							</span>
							<div class="clear"></div>
						</div>
						<?php
						if($c['FunVersion']>=1 && in_array('business', $c['manage']['plugins']['Used'])){
//							$business_cate_row=db::get_all('business_category', 1, 'CateId,UId,Category', $c['my_order'].' CateId asc');
//							$business_cate_ary=array();
//							foreach($business_cate_row as $v){
//								$business_cate_ary[$v['UId']][]=$v;
//							}
//							$business_row=db::get_all('business', 1, 'BId,Name,memberNo', 'BId desc');
//							$business_ary=array();
                            if ($products_row['Business']){
                                $businessone = db::get_one("business","BId = ".$products_row['Business'], 'BId,Name,memberNo', 'BId desc');
                            }
						?>
							<div class="rows clean">
								<label>{/products.products.business/}</label>
								<div class="box_explain">{/products.explain.business/}</div>
								<div class="input">

                                    <div class="box_select">
                                        <input type="text" id="Business" style="width: 404px;height: 36px;" onkeyup="business_search(this)" value="<?=$businessone['memberNo'].$businessone['Name'] ?>">
                                        <input type="hidden" name="Business" id="Business1" style="width: 404px;height: 36px;" value="<?=$products_row['Business']; ?>">
<!--										<select name='Business'>-->
<!--											<option value=''>{/global.select_index/}</option>-->
<!--											--><?php
//											foreach((array)$business_cate_ary['0,'] as $v){
//											?>
<!--											<optgroup label="--><?//=$v['Category'];?><!--">-->
<!--												--><?php
//												foreach((array)$business_ary[$v['CateId']] as $v2){
//													echo "<option value='{$v2['BId']}'".($products_row['Business']==$v2['BId']?' selected':'').">{$v2['Name']}</option>";
//												}?>
<!--											</optgroup>-->
<!--												--><?php //foreach((array)$business_cate_ary['0,'.$v['CateId'].','] as $v2){?>
<!--												<optgroup label="--><?//=$v2['Category'];?><!--">-->
<!--													--><?php
//													foreach((array)$business_ary[$v2['CateId']] as $v3){
//														echo "<option value='{$v3['BId']}'".($products_row['Business']==$v3['BId']?' selected':'').">{$v3['Name']}</option>";
//													}?>
<!--												</optgroup>-->
<!--												--><?php //}?>
<!--											--><?php //}?>
<!--										</select>-->
									</div>

                                    <script>
                                        function business_search(that){

                                            var val = $(that).val();
                                            $.ajax({
                                                type:'GET',
                                                url: './?do_action=products.business_search&keyword='+ val,
                                                dataType:'json',
                                                data: 1,
                                                success: function(redata) {
                                                    if(redata.ret == 1){
                                                        $('#zz').show();
                                                        var k = redata.msg;
                                                        console.log(k)
                                                        var str = "<ul>";
                                                        for (var i = 0; i <k.length; i++) {
                                                            str += '<li onClick="msgval('+k[i]['BId']+',this)">'+k[i]['memberNo']+k[i]['Name']+'</li>';
                                                        }
                                                        str += "</ul>";
                                                        $('#kkk').html(str);
                                                    }
                                                }
                                            });
                                        }
                                        function msgval(val,that){
                                            console.log(val);
                                            $('#zz').hide();
                                            $('#Business1').val(val);
                                            $('#Business').val($(that).html());

                                        }
                                    </script>
                                    <style>
                                        #zz{
                                            display: none;
                                            border:1px solid #000;
                                            /*width: 366px;*/
                                        }
                                        #zz ul li{
                                            height: 30px;
                                            line-height: 30px;
                                            border-bottom: 1px solid #dfdfdf;
                                        }
                                    </style>
                                    <div id="zz">
                                        <ul id="kkk">
<!--                                            <li>123123</li>-->
<!--                                            <li>123123</li>-->
                                        </ul>
                                    </div>
								</div>
							</div>
						<?php }?>

						<div class="box_general_attribute"></div>
						<div class="clear"></div>
						<div class="rows clean translation multi_lang">
							<label>{/products.products.briefdescription/}</label>
							<div class="input">
								<?=manage::unit_form_edit($products_row, 'textarea', 'BriefDescription', 85);?>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<?php /***************************** 基础信息 End *****************************/?>
					
					<?php /***************************** 详细介绍 Start *****************************/?>
					<div class="global_container" data-name="Description">
						<dl class="box_basic_more">
							<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>
							<dd class="drop_down">
								<a href="javascript:;" class="item input_checkbox_box btn_open_attr add_attr" id="add_editor_attribute"><em></em><span>{/global.add/}</span></a>
								<a href="javascript:;" class="item input_checkbox_box btn_open_attr edit_attr" id="edit_editor_attribute"><em></em><span>{/global.edit/}</span></a>
								<a href="javascript:;" class="item input_checkbox_box btn_open_attr myorder_attr" id="myorder_editor_attribute"><em></em><span>{/global.my_order/}</span></a>
							</dd>
						</dl>
						<div class="rows clean translation description_box">
							<div class="head clean">
								<a href="javascript:;" class="head_btn head_btn_left end"><i></i></a>
								<div class="head_hide">
									<ul>
										<li><span>{/products.products.description/}</span></li>
									</ul>
								</div>
								<a href="javascript:;" class="head_btn head_btn_right"><i></i></a>
								<div class="tab_box"><?=manage::html_tab_button();?></div>
							</div>
							<div class="input">
								<ul>
									<li><?=manage::form_edit($products_description_row, 'editor', 'Description');?></li>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<?php /***************************** 详细介绍 End *****************************/?>
					
					<?php /***************************** 销售信息 Start *****************************/?>
					<div class="global_container" data-name="<?=$pro_menu_ary[3];?>">
						<div class="big_title">{/products.products.<?=$pro_menu_ary[3];?>/}</div>
						<div class="rows clean price_box">
							<label>{/products.products.price_ary.1/}</label>
							<div class="input">
								<span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol'];?></b><input type="text" class="box_input" name="Price_1" value="<?=$products_row['Price_1'];?>" size="5" maxlength="255" rel="amount" notnull parent_null="1" /></span>
							</div>
						</div>
						<div class="rows clean price_box">
							<label>{/products.products.price_ary.0/}</label>
							<div class="input">
								<span class="unit_input" parent_null><b><?=$c['manage']['currency_symbol'];?></b><input type="text" class="box_input" name="Price_0" value="<?=$products_row['Price_0'];?>" size="5" maxlength="255" rel="amount"<?=(int)$cfg_ary['products_show']['price']?' notnull':'';?> parent_null="1" /></span>
							</div>
						</div>
						<div class="clear"></div>
						<?php
						if(in_array('wholesale', $c['manage']['plugins']['Used'])){//开启了批发价APP
						?>
							<div class="d_line"></div>
							<div class="rows clean">
								<label>{/products.products.wholesale_price/}</label>
								<div class="input">
									<table border="0" cellspacing="0" cellpadding="3" id="wholesale_list" class="item_data_table" data-unit="<?=$Unit;?>">
										<tbody>
											<?php
											$i=0;
											$wholesale_price=str::json_data(htmlspecialchars_decode($products_row['Wholesale']), 'decode');
											foreach((array)$wholesale_price as $k=>$v){
												$price=$products_row['Price_1']>0?sprintf('%01.5f', $v/$products_row['Price_1']):0;
												$Devide=(float)substr($price, 0, -2);
											?>
												<tr>
													<td>
														<span class="d_list">
															<?=$i==0?'<span class="d_tit color_000">{/products.products.qty/}</span>':'<span class="d_empty"></span>';?>
															<span class="unit_input"><input name="Qty[]" value="<?=$k;?>" type="text" class="box_input" maxlength="10" size="5"><?=$Unit?'<b class="last">'.$Unit.'</b>':'';?></span>
														</span>
														<span class="d_list money">
															<?=$i==0?'<span class="d_tit color_000">{/products.products.price/}</span>':'<span class="d_empty"></span>';?>
															<span class="unit_input"><b><?=$c['manage']['currency_symbol']?></b><input name="Price[]" value="<?=sprintf('%01.2f', $v);?>" type="text" class="box_input" maxlength="10" size="5"></span>
														</span>
														&nbsp;<a class="d_del fl icon_delete_1<?=$i>0?' d_del_empty':'';?>" href="javascript:;"><i></i></a>
														<div class="clear"></div>
													</td>
												</tr>
											<?php
												++$i;
											}?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="rows clean">
								<div class="input"><a href="javascript:;" id="add_wholesale" class="btn_global btn_add_item">{/global.add/}</a></div>
							</div>
							<div class="rows clean twice_box twice_first">
								<label>{/products.products.moq/}</label>
								<div class="input">
									<span class="unit_input"><input type="text" class="box_input" name="MOQ" value="<?=(int)$products_row['MOQ'];?>" size="10" maxlength="255" rel="amount" /><?=$Unit?'<b class="last">'.$Unit.'</b>':'';?></span>
								</div>
							</div>
							<div class="rows clean twice_box">
								<label>{/products.products.maxoq/}</label>
								<div class="input">
									<span class="unit_input"><input type="text" class="box_input" name="MaxOQ" value="<?=(int)$products_row['MaxOQ'];?>" size="10" maxlength="10" rel="amount" /><?=$Unit?'<b class="last">'.$Unit.'</b>':'';?></span>
								</div>
							</div>
							<div class="clear"></div>
							<div class="d_line"></div>
						<?php }?>
						<div class="rows clean">
							<label>{/global.status/}</label>
							<div class="input">
								<div class="box_select sold_status fl">
									<select name="SoldOut">
										<option value="0"<?=$products_row['SoldOut']==0?' selected':'';?>>{/products.products.sold_in/}</option>
										<option value="1"<?=$products_row['SoldOut']==1?' selected':'';?>>{/products.products.sold_out/}</option>
										<?php /*<option value="2"<?=$products_row['SoldOut']==2?' selected':'';?>>{/products.products.stock_out/}</option>*/?>
									</select>
								</div>
								<div id="sold_out_div" class="sold_status_box fl hide" style="display:<?=$products_row['SoldOut']==1?'block':'none';?>;">
									<div class="switchery<?=$products_row['IsSoldOut']?' checked':'';?>">
										<input type="checkbox" name="IsSoldOut" value="1"<?=$products_row['IsSoldOut']?' checked':'';?> />
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
									<span class="sold_in_title">{/products.products.sold_in_time/}</span>
									<span class="sold_in_time" style="display:<?=$products_row['IsSoldOut']?'':'none';?>;"><input type="text" class="box_input input_time" name="SoldOutTime" value="<?=date('Y-m-d H:i:s',($products_row['SStartTime']?$products_row['SStartTime']:$c['time'])).'/'.date('Y-m-d H:i:s', ($products_row['SEndTime']?$products_row['SEndTime']:$c['time']));?>" size="45" readonly /></span>
								</div>
								<div id="arrival_notice_div" class="sold_status_box fl hide" style="display:none;">
									<div class="switchery<?=$products_row['StockOut']?' checked':'';?>">
										<input type="checkbox" name="StockOut" value="1"<?=$products_row['StockOut']?' checked':'';?> />
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
									<span class="sold_in_title">{/products.products.arrival_notice/}</span>
								</div>
							</div>
						</div>
					</div>
					<?php /***************************** 销售信息 End *****************************/?>
					
					<?php /***************************** 规格参数 Start *****************************/?>
					<div class="global_container" data-name="<?=$pro_menu_ary[4];?>">
						<div class="big_title">{/products.products.<?=$pro_menu_ary[4];?>/}</div>
						<div class="box_explain">{/products.explain.attr_order/}</div>
						<div class="box_cart_attribute"></div>
						<div class="rows clean attr_add">
							<div class="input">
                            	<a href="javascript:;" class="btn_global btn_item_sec btn_add fl" id="add_attribute" data-cart="1">{/products.global.add_attribute/}</a>
								<input type="button" class="btn_global btn_item_sec btn_edit fl" id="edit_attribute" value="{/products.global.modify_attribute/}" data-cart="1" />
							</div>
						</div>
					</div>
					<?php /***************************** 规格参数 End *****************************/?>
					
					<?php /***************************** 规格设置 Start *****************************/?>
					<?php
					$combination_status=' style="display:'.($products_row['IsCombination']?'table-cell':'none').';"';
					?>
					<div class="global_container" data-name="<?=$pro_menu_ary[5];?>">
						<div class="big_title">{/products.products.<?=$pro_menu_ary[5];?>/}</div>
						<div class="open_attr_price">
							<div class="switchery">
								<input type="checkbox" value="1" name="IsCombination" />
								<div class="switchery_toggler"></div>
								<div class="switchery_inner">
									<div class="switchery_state_on"></div>
									<div class="switchery_state_off"></div>
								</div>
							</div>
							<span>{/products.products.is_open_attr_price/}</span>
						</div>
						<div class="rows clean">
							<label></label>
							<div class="input">
								<div class="box_select sold_status fl">
									<select name="SoldStatus">
										<option value="0"<?=$products_row['SoldStatus']==0?' selected':'';?>>{/products.products.stock_ary.0/}</option>
										<option value="1"<?=$products_row['SoldStatus']==1?' selected':'';?>>{/products.products.stock_ary.1/}</option>
										<option value="2"<?=$products_row['SoldStatus']==2?' selected':'';?>>{/products.products.stock_ary.2/}</option>
									</select>
								</div>
							</div>
						</div>
						<div id="attribute_unit_box">
							<div class="rows clean twice_box twice_first">
								<label>{/products.products.sku/}</label>
								<div class="input"><input type="text" class="box_input" name="SKU" value="<?=$products_row['SKU'];?>" size="39" maxlength="50" /></div>
							</div>
							<div class="rows clean twice_box">
								<label>{/products.products.weight/}</label>
								<div class="input">
									<span class="unit_input"><input type="text" class="box_input" name="Weight" value="<?=$products_row['Weight'];?>" size="39" maxlength="11" rel="amount" /><b class="last">{/products.products.weight_unit/}</b></span>
									<?php
									/*if((int)db::get_value('config', 'GroupId="products" and Variable="IsPacking"', 'value')){
									?>
										<div class="blank9"></div>
										<?php
										$packing_start='<span class="price_input"><input name="PackingStart" value="'.(int)$products_row['PackingStart'].'" type="text" class="form_input" size="5" maxlength="10" rel="amount" /><b class="last">'.$c['manage']['lang_pack']['global']['item'].'</b></span>';
										echo str_replace('%input%', $packing_start, $c['manage']['lang_pack']['products']['products']['packing_start']).'<div class="blank9"></div>';
										$packing_qty='<span class="price_input"><input name="PackingQty" value="'.(int)$products_row['PackingQty'].'" type="text" class="form_input" size="5" maxlength="10" rel="amount" /><b class="last">'.$c['manage']['lang_pack']['global']['item'].'</b></span>';
										$packing_weight='<span class="price_input"><input name="PackingWeight" value="'.(float)$products_row['PackingWeight'].'" type="text" class="form_input" size="5" maxlength="10" rel="amount" /><b class="last">'.$c['manage']['lang_pack']['shipping']['shipping']['unit'].'</b></span>';
										echo str_replace(array('%qty%', '%weight%'), array($packing_qty, $packing_weight), $c['manage']['lang_pack']['products']['products']['packing_weight']);
										?>
									<?php }*/?>
								</div>
							</div>
							<div class="clear"></div>
							<div class="rows clean twice_box twice_first">
								<label>{/products.products.stock/}</label>
								<div class="input">
									<span class="unit_input" parent_null><input type="text" class="box_input" name="Stock" value="<?=(int)$products_row['Stock'];?>" size="39" maxlength="255" notnull rel="amount" parent_null="1" /><?=$Unit?'<b class="last">'.$Unit.'</b>':'';?></span>
								</div>
							</div>
							<div class="rows clean twice_box box_combination">
								<label>{/products.products.is_combination/}</label>
								<div class="input">
									<div class="switchery<?=$products_row['IsOpenAttrPrice']?' checked':'';?>">
										<input type="checkbox" value="1" name="IsOpenAttrPrice"<?=$products_row['IsOpenAttrPrice']?' checked':'';?> />
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="rows clean" id="attribute_ext_box"<?=$products_row['IsOpenAttrPrice']?' style="display:block;"':'';?>>
							<div class="input box_multi_tab">
								<div class="multi_tab_row<?=(int)@in_array('overseas', (array)$c['manage']['plugins']['Used'])==1?' show':' hide';?>"></div>
								<div id="attribute_ext">
									<table border="0" cellpadding="5" cellspacing="0" class="relation_box">
										<thead>
											<tr>
												<td width="35%" class="color_555">{/products.attribute/}</td>
												<td width="20%" class="color_555"<?=$combination_status;?>>{/products.products.sku/}</td>
												<td width="5%" class="color_555"<?=$combination_status;?>>{/products.products.mark_up/}</td>
												<td width="10%" class="color_555"><?=$products_row['IsCombination']?'{/products.products.price/}':'{/products.products.mark_up/}';?>(<?=$c['manage']['currency_symbol'];?>)</td>
												<td width="10%" class="color_555"<?=$combination_status;?>>{/products.products.stock/}</td>
												<td width="10%" class="color_555"<?=$combination_status;?>>{/products.products.weight/}(kg)</td>
												<?php /*<td width="10%" class="color_555"<?=$combination_status;?> nowrap>{/global.batch/}</td>*/?>
											</tr>
										</thead>
									</table>
								</div>
								<div id="attribute_tmp" class="hide">
									<table class="column">
										<tbody>
											<tr>
												<td class="title attr_name">{/products.global.batch_edit/}</td>
												<td class="title"<?=$combination_status;?>><a href="javascript:;" class="synchronize_btn" data-num="0">{/global.synchronize/}</a></td>
												<td class="title"<?=$combination_status;?>>
													<div class="switchery btn_increase_all">
														<input type="checkbox" value="1" />
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</td>
												<td class="title"><a href="javascript:;" class="synchronize_btn" data-num="2">{/global.synchronize/}</a></td>
												<td class="title"<?=$combination_status;?>><a href="javascript:;" class="synchronize_btn" data-num="3">{/global.synchronize/}</a></td>
												<td class="title"<?=$combination_status;?>><a href="javascript:;" class="synchronize_btn" data-num="4">{/global.synchronize/}</a></td>
												<?php /*<td class="title"<?=$combination_status;?>></td>*/?>
											</tr>
										</tbody>
									</table>
									<table>
										<tbody class="contents">
											<tr id="VId_XXX" attr_txt="">
												<td class="attr_name">Name</td>
												<td<?=$combination_status;?>><input type="text" class="box_input" name="AttrSKU[XXX]" value="%SKUV%" size="16" maxlength="255" /></td>
												<td<?=$combination_status;?>>
													<div class="switchery">
														<input type="checkbox" value="1" name="AttrIsIncrease[XXX]" />
														<div class="switchery_toggler"></div>
														<div class="switchery_inner">
															<div class="switchery_state_on"></div>
															<div class="switchery_state_off"></div>
														</div>
													</div>
												</td>
												<td><input type="text" class="box_input" name="AttrPrice[XXX]" value="%PV%" size="6" maxlength="10" rel="amount" /></td>
												<td<?=$combination_status;?>><input type="text" class="box_input" name="AttrStock[XXX]" value="%SV%" size="4" maxlength="10" rel="amount" /></td>
												<td<?=$combination_status;?>><input type="text" class="box_input" name="AttrWeight[XXX]" value="%WV%" size="5" maxlength="10" rel="amount" /></td>
												<?php /*<td<?=$combination_status;?> class="batch_box"><a href="javascript:;" class="btn_batch color_888">{/global.operation/}</a><div class="batch_edit" style="display:none;"></div></td>*/?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php /***************************** 规格设置 End *****************************/?>
				</div>
			</div>
			<div class="right_container">
				<?php /***************************** 标签 Start *****************************/?>
				<div class="global_container" data-name="<?=$pro_menu_ary[6];?>">
					<div class="big_title no_pointer">{/products.products.<?=$pro_menu_ary[6];?>/}</div>
					<div class="box_explain">{/products.explain.tags/}</div>
					<?php
					$fixed_tags_ary=array('IsIndex'=>'is_index', 'IsNew'=>'is_new', 'IsHot'=>'is_hot', 'IsBestDeals'=>'is_best_deals');
					$tags_id_ary=explode('|', $products_row['Tags']);
					$tags_id_ary=array_filter($tags_id_ary);
					$tags_row=db::get_all('products_tags', 1);
					$tags_select_count=0;
					?>
					<div class="rows tags_row clean">
						<div class="input">
							<div class="box_option_list">
								<div class="option_selected">
									<div class="select_list">
										<?php
										foreach($fixed_tags_ary as $k=>$v){//固有标签
											if($products_row[$k]==0) continue;
											$tags_select_count+=1;
										?>
											<span class="btn_attr_choice current" data-type="tags" data-fixed="1"><b>{/products.products.<?=$v;?>/}</b><input type="checkbox" name="tagsCurrent[]" value="<?=$k;?>" class="option_current" checked /><input type="hidden" name="tagsOption[]" value="<?=$k;?>" /><input type="hidden" name="tagsName[]" value="{/products.products.<?=$v;?>/}" /><i></i></span>
										<?php }?>
										<?php
										foreach((array)$tags_row as $k=>$v){//自定义标签
											if(!in_array($v['TId'], $tags_id_ary)) continue;
											$tags_select_count+=1;
										?>
											<span class="btn_attr_choice current" data-type="tags"><b><?=$v['Name'.$c['manage']['web_lang']];?></b><input type="checkbox" name="tagsCurrent[]" value="<?=$v['TId'];?>" checked class="option_current" /><input type="hidden" name="tagsOption[]" value="<?=$v['TId'];?>" /><input type="hidden" name="tagsName[]" value="<?=$v['Name'.$c['manage']['web_lang']];?>" /><i></i></span>
										<?php }?>
									</div>
									<input type="text" class="box_input" name="_Option" value="" size="30" maxlength="255" />
									<span class="placeholder<?=$tags_select_count>0?' hide':'';?>">{/products.global.placeholder/}</span>
								</div>
								<div class="option_not_yet">
									<div class="select_list" data-type="tags">
										<?php
										foreach($fixed_tags_ary as $k=>$v){//固有标签
											if($products_row[$k]==1) continue;
										?>
											<span class="btn_attr_choice" data-type="tags" data-fixed="1"><b>{/products.products.<?=$v;?>/}</b><input type="checkbox" name="tagsCurrent[]" value="<?=$k;?>" class="option_current" /><input type="hidden" name="tagsOption[]" value="<?=$k;?>" /><input type="hidden" name="tagsName[]" value="{/products.products.<?=$v;?>/}" /><i style="display:none;"></i></span>
										<?php }?>
										<?php
										foreach((array)$tags_row as $k=>$v){//自定义标签
											if(in_array($v['TId'], $tags_id_ary)) continue;
										?>
											<span class="btn_attr_choice" data-type="tags"><b><?=$v['Name'.$c['manage']['web_lang']];?></b><input type="checkbox" name="tagsCurrent[]" value="<?=$v['TId'];?>" class="option_current" /><input type="hidden" name="tagsOption[]" value="<?=$v['TId'];?>" /><input type="hidden" name="tagsName[]" value="<?=$v['Name'.$c['manage']['web_lang']];?>" /><i></i></span>
										<?php }?>
									</div>
								</div>
								<div class="option_button">
									<a href="javascript:;" class="select_all">{/global.select_all/}</a>
									<div class="option_button_menu" style="display:none;">
										<a href="javascript:;" data-type="tags">{/products.products.tag_info/}</a>
									</div>
								</div>
								<input type="hidden" class="option_max_number" value="0" />
							</div>
						</div>
					</div>
				</div>
				<?php /***************************** 标签 End *****************************/?>
				<div class="global_container">
					<?php /***************************** SEO Start *****************************/?>
					<div class="big_title">{/products.products.<?=$pro_menu_ary[7];?>/}</div>
					<a href="javascript:;" class="btn_edit" id="edit_seo_list" data-save="0">{/global.mod/}</a>
					<?php
					$isSeoShow=($products_seo_row['SeoTitle'.$c['manage']['web_lang']] || $products_seo_row['SeoKeyword'.$c['manage']['web_lang']] || $products_seo_row['SeoDescription'.$c['manage']['web_lang']] || $products_row['PageUrl'])?1:0;
					?>
					<div class="seo_info_box"<?=$isSeoShow?' style="display:block;"':'';?>>
						<div class="title clean color_000"<?=$products_seo_row['SeoTitle'.$c['manage']['web_lang']]?'':' style="display:none;"';?>><?=$products_seo_row['SeoTitle'.$c['manage']['web_lang']];?></div>
						<div class="keyword clean color_555"<?=$products_seo_row['SeoKeyword'.$c['manage']['web_lang']]?'':' style="display:none;"';?>><?=$products_seo_row['SeoKeyword'.$c['manage']['web_lang']];?></div>
						<div class="description clean color_555"<?=$products_seo_row['SeoDescription'.$c['manage']['web_lang']]?'':' style="display:none;"';?>><?=$products_seo_row['SeoDescription'.$c['manage']['web_lang']];?></div>
						<div class="url clean color_888"<?=$products_row['PageUrl']?'':' style="display:none;"';?>><?=ly200::get_domain().'/'.$products_row['PageUrl']."-p{$ProId}.html";?></div>
						<div class="blank20"></div>
					</div>
					<div class="seo_box">
						<div class="rows clean multi_lang" data-name="title">
							<label>{/news.news.seo_title/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_title_notes/}</div>
							<div class="input">
								<?=manage::unit_form_edit($products_seo_row, 'text', 'SeoTitle', 41, 255);?>
							</div>
						</div>
						<div class="rows clean multi_lang" data-name="description">
							<label>{/news.news.seo_brief/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_description_notes/}</div>
							<div class="input">
								<?=manage::unit_form_edit($products_seo_row, 'textarea', 'SeoDescription', 41);?>
							</div>
						</div>
						<div class="rows clean multi_lang" data-name="keyword">
							<label>{/news.news.seo_keyword/}</label>
							<div class="big_notes color_aaa">{/products.products.seo_keyword_notes/}</div>
							<dl class="box_basic_more box_seo_basic_more">
								<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>
								<dd class="drop_down">
									<a href="javascript:;" class="item input_checkbox_box btn_open_attr" id="edit_keyword"><span>{/global.edit/}</span></a>
								</dd>
							</dl>
							<?php
							$keys_id_ary=explode(',', $products_seo_row['SeoKeyword'.$c['manage']['web_lang']]);
							$keys_id_ary=array_filter($keys_id_ary);
							$keys_select_count=0;
							?>
							<div class="rows keys_row clean">
								<div class="input">
									<div class="box_option_list">
										<div class="option_selected">
											<div class="select_list">
												<?php
												foreach((array)$keys_id_ary as $v){
													$Keys=trim($v);
													$keys_select_count+=1;
												?>
													<span class="btn_attr_choice current" data-type="keys"><b><?=$Keys;?></b><input type="checkbox" name="keysCurrent[]" value="<?=$Keys;?>" checked class="option_current" /><input type="hidden" name="keysOption[]" value="<?=$Keys;?>" /><input type="hidden" name="keysName[]" value="<?=$Keys;?>" /><i></i></span>
												<?php }?>
											</div>
											<input type="text" class="box_input" name="_Option" value="" size="30" maxlength="255" />
											<span class="placeholder<?=$keys_select_count>0?' hide':'';?>">{/products.global.placeholder/}</span>
										</div>
										<div class="option_not_yet">
											<div class="select_list" data-type="keys"></div>
										</div>
										<div class="option_button">
											<a href="javascript:;" class="select_all">{/global.select_all/}</a>
											<div class="option_button_menu" style="display:none;">
												<a href="javascript:;" data-type="keys">{/products.products.tag_info/}</a>
											</div>
										</div>
										<input type="hidden" class="option_max_number" value="0" />
									</div>
								</div>
							</div>
						</div>
						<div class="rows custom_row clean" data-name="url">
							<label>{/page.page.custom_url/}</label>
							<div class="big_notes color_aaa"><?=str_replace('%url%', ly200::get_domain(), $c['manage']['lang_pack']['products']['products']['page_url_notes']);?></div>
							<div class="input">
								<textarea name='PageUrl' class="box_textarea" data-content="<?=ly200::get_domain().'/{/products.products.page_url_content/}.html';?>" data-url="<?=ly200::get_domain().'/%url%'.($ProId ? "-p{$ProId}.html" : '.html');?>" data-domain="<?=ly200::get_domain().'/';?>"><?=$products_row['PageUrl']?ly200::get_domain().'/'.$products_row['PageUrl'].($ProId ? "-p{$ProId}.html" : '.html'):ly200::get_domain().ly200::get_url($products_row, 'products', $c['manage']['web_lang']);?></textarea>
							</div>
						</div>
					</div>
					<?php /***************************** SEO End *****************************/?>
					<?php /***************************** 物流 Start *****************************/?>
					<?php
					$Cubage=explode(',', $products_row['Cubage']);
					?>
					<div class="big_title border_t">{/products.products.<?=$pro_menu_ary[8];?>/}<i></i></div>
					<div class="freight_box" data-name="<?=$pro_menu_ary[8];?>">
						<?php if(in_array('shipping_template', (array)$c['manage']['plugins']['Used'])){?>
							<div class="rows clean">
								<label>{/products.products.shipping_template/}</label>
								<div class="input">
									<?php $shipping_template_row=db::get_all('shipping_template');?>
									<div class="box_select"><?=ly200::form_select($shipping_template_row, 'TId', $shipping_row['TId'], 'Name', 'TId', '');?></div>
								</div>
							</div>
						<?php }?>
						<div class="rows clean">
							<label>{/products.products.cubage/}</label>
							<div class="input">
								<span class="unit_input cubage_input"><b>{/products.products.L_W_H.0/}</b><input type="text" class="box_input" name="Cubage[0]" value="<?=(float)$Cubage[0];?>" size="3" maxlength="10" rel="amount" /><b class="last">{/products.products.cubage_unit/}</b></span>
								<span class="unit_input cubage_input"><b>{/products.products.L_W_H.1/}</b><input type="text" class="box_input" name="Cubage[1]" value="<?=(float)$Cubage[1];?>" size="3" maxlength="10" rel="amount" /><b class="last">{/products.products.cubage_unit/}</b></span>
								<span class="unit_input cubage_input"><b>{/products.products.L_W_H.2/}</b><input type="text" class="box_input" name="Cubage[2]" value="<?=(float)$Cubage[2];?>" size="3" maxlength="10" rel="amount" /><b class="last">{/products.products.cubage_unit/}</b></span>
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.products.free_shipping/}</label>
							<div class="input">
								<div class="switchery<?=$products_row['IsFreeShipping']?' checked':'';?>">
									<input type="checkbox" value="1" name="IsFreeShipping"<?=$products_row['IsFreeShipping']?' checked':'';?> />
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
								<div class="box_volume_weight" style="display:<?=$products_row['IsFreeShipping']?'none':'inline-block';?>;">
									<span class="input_checkbox_box<?=$products_row['IsVolumeWeight']?' checked':'';?>">
										<span class="input_checkbox">
											<input type="checkbox" name="IsVolumeWeight" value="1"<?=$products_row['IsVolumeWeight']?' checked':'';?> />
										</span>{/products.products.volume_weight/}
									</span>
								</div>
							</div>
						</div>
					</div>
					<?php /***************************** 物流 End *****************************/?>
					<?php /***************************** 平台导流 Start *****************************/?>
					<?php
					if(in_array('platform', $c['manage']['plugins']['Used'])){
						$platform_ary=array('amazon'=>array('us','au','br','ca','fr','de','in','it','jp','mx','nl','es','tr','uk','fa'), 'aliexpress', 'wish', 'ebay', 'alibaba');
						$platform=str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
						$lang_count=count($c['manage']['config']['Language']);
					?>
						<div class="big_title border_t">
							{/products.products.<?=$pro_menu_ary[9];?>/}<i></i>
						</div>
						<div class="platform_box" data-name="<?=$pro_menu_ary[9];?>">
							<ul class="platform_list">
								<?php
								foreach($platform_ary as $k=>$v){
									if(is_array($v)){
										$set_ary=array($k);
										foreach((array)$v as $v1){
											$set_ary[]=$k.'_'.$v1;
										}
										foreach((array)$set_ary as $v1){
											?>
											<li class="clean rows multi_lang<?=!$platform[$v1]?' hide':'';?><?=$lang_count==1?' only':'';?>" data-type="platform_<?=$v1;?>">
												<label class="icon icon_platform_<?=$k;?>">{/products.products.platform_name.<?=$k;?>.<?=$v1; ?>/}</label>
												<div class="input">
													<a class="del" href="javascript:;" data-type="<?=$v1;?>" data-top="<?=$k; ?>">{/global.del/}</a>
													<?php foreach((array)$c['manage']['config']['Language'] as $val){?>
														<div class="lang_txt lang_txt_<?=$val;?>"<?=$c['manage']['config']['LanguageDefault']==$val?' style="display:block;" data-default="1"':'';?>>
															<span class="unit_input unit_textarea"><?=$lang_count>1?"<b>{/language.{$val}/}</b>":'';?><textarea name="Platform_<?=$v1;?>_Url_<?=$val;?>[]" class="box_textarea"><?=$platform[$v1][0]['Url_'.$val];?></textarea></span>
														</div>
													<?php }?>
												</div>
											</li>
										<?php
										}
									}else{
									?>
										<li class="clean rows multi_lang<?=!$platform[$v]?' hide':'';?><?=$lang_count==1?' only':'';?>" data-type="platform_<?=$v;?>">
											<label class="icon icon_platform_<?=$v;?>">{/products.products.platform_name.<?=$v;?>/}</label>
											<div class="input">
												<a class="del" href="javascript:;" data-type="<?=$v;?>">{/global.del/}</a>
												<?php foreach((array)$c['manage']['config']['Language'] as $val){?>
													<div class="lang_txt lang_txt_<?=$val;?>"<?=$c['manage']['config']['LanguageDefault']==$val?' style="display:block;" data-default="1"':'';?>>
														<span class="unit_input unit_textarea"><?=$lang_count>1?"<b>{/language.{$val}/}</b>":'';?><textarea name="Platform_<?=$v;?>_Url_<?=$val;?>[]" class="box_textarea"><?=$platform[$v][0]['Url_'.$val];?></textarea></span>
													</div>
												<?php }?>
											</div>
										</li>
								<?php }} ?>
								<li class="clean platform_default" data-type="platform_default">
									<div class="box_select fl">
										<select name="PlatformType">
											<option value="-1">{/products.model.model/}</option>
											<?php
											foreach($platform_ary as $k=>$v){
												if(is_array($v)){
													$cou=$platform[$k] ? 0 : 1;
													foreach((array)$v as $v1){
														if($platform[$k.'_'.$v1]) continue;
														$cou++;
													}
													?>
													<option data-sec="1" value="<?=$k;?>"<?=!$cou?' class="hide" disabled':'';?>>{/products.products.platform_name.<?=$k;?>.<?=$k;?>/}</option>
												<?php }else{ ?>
													<option value="<?=$v;?>"<?=$platform[$v]?' class="hide" disabled':'';?>>{/products.products.platform_name.<?=$v;?>/}</option>
												<?php } ?>
											<?php }?>
										</select>
									</div>
									<?php 
									foreach($platform_ary as $k=>$v){
										if(is_array($v)){
										?>
										<div class="box_select fl PlatformTypeSec" data-type="<?=$k; ?>">
											<select name="PlatformTypeSec">
												<option value="<?=$k; ?>"<?=$platform[$k]?' class="hide" disabled':'';?>>{/products.products.platform_name.<?=$k;?>.<?=$k;?>/}</option>
												<?php
													foreach((array)$v as $v1){
													?>
													<option value="<?=$k.'_'.$v1;?>"<?=$platform[$k.'_'.$v1]?' class="hide" disabled':'';?>>{/products.products.platform_name.<?=$k;?>.<?=$k.'_'.$v1;?>/}</option>
													<?php } ?>
											</select>
										</div>
										<?php } ?>
									<?php }?>
									<div class="clear"></div>
									<div class="rows multi_lang">
										<div class="input"><?=manage::unit_form_edit('', 'textarea', 'Platform_XX_Url');?></div>
									</div>
								</li>
							</ul>
							<div class="clear"></div>
							<div class="platform_button">
								<a href="javascript:;" class="btn_global btn_add_item btn_platform fl">{/global.add/}</a>
								<div class="clear"></div>
							</div>
						</div>
						<div class="box_explain">{/products.explain.platform/}</div>
					<?php }?>
					<?php /***************************** 平台导流 End *****************************/?>
				</div>
			</div>
			
			<div class="rows">
				<div class="center_container_1200">
					<div class="input">
						<input type="submit" class="btn_global btn_submit btn_disabled" name="submit_button" value="{/global.save/}" />
						<input type="button" class="btn_global btn_drafts" value="{/products.products.save_drafts/}" style="display:none;" />
						<?php if(count($c['manage']['config']['Language'])>1){?>
							<input type="button" class="btn_global btn_translation" value="{/global.translation/}">
						<?php }?>
						<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=products';?>" class="btn_global btn_cancel">{/global.return/}</a>
					</div>
				</div>
			</div>
			<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" id="AddProId" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="do_action" value="products.products_edit" />
			<input type="hidden" id="back_action" name="back_action" value="<?=$_SERVER['HTTP_REFERER'];?>" />
			<input type="hidden" id="IsOpenOverseas" value="<?=(int)@in_array('overseas', (array)$c['manage']['plugins']['Used']);?>" />
			<input type="hidden" id="IsCombination" value="<?=(int)$products_row['IsCombination'];?>" />
			<input type="hidden" id="save_drafts" name="SaveDrafts" value="0" />
			<input type="hidden" id="attr_max_number" value="0" />
			<input type="hidden" id="option_max_number" value="0" />
			<input type="hidden" id="tags_max_number" value="0" />
		</form>
		<?php include('static/inc/translation.php');?>
	<?php }?>
</div>

<?php
//编辑页面
if($c['manage']['do']=='edit'){
	$VideoFirst=(int)db::get_value('products_development', "ProId='{$ProId}'", 'VideoFirst');
?>
<div id="fixed_right">
	<div class="global_container fixed_video">
		<div class="top_title">{/products.global.edit_video/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="video_form">
			<div class="rows clean" data-type="name">
				<label>{/products.products.video_link/}</label>
				<div class="box_explain">{/products.explain.video/}</div>
				<div class="input">
					<textarea name='VideoUrl' class="box_textarea"><?=$products_row['VideoUrl'];?></textarea>
				</div>
			</div>
			<div class="rows clean">
				<label></label>
				<div class="input">
					<span class="input_checkbox_box <?=$VideoFirst? 'checked' : ''; ?>">
						<span class="input_checkbox">
							<input type="checkbox" name="VideoFirst" value="1" <?=$VideoFirst? 'checked=""' : ''; ?>>
						</span>{/products.video_first/}
					</span>
				</div>
			</div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="do_action" value="products.products_video_edit" />
		</form>
	</div>
	<div class="global_container fixed_general_attribute">
		<div class="top_title">{/products.global.add_attribute/} <a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="add_general_attribute_form">
			<div class="rows clean" data-type="name">
				<label>{/global.name/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
				<div class="input" id="check_general_attribute_name">
					<?=manage::form_edit('', 'text', 'Name', 50, 255, 'autocomplete="off" notnull');?>
				</div>
			</div>
			<div class="rows clean type_box">
				<label>{/global.type/}</label>
				<div class="box_explain">{/products.explain.attr_desc/}</div>
				<div class="input">
					<span class="input_radio_box">
						<span class="input_radio">
							<input type="radio" name="Type" value="0" />
						</span>{/products.model.common_attr/}
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="input_radio_box">
						<span class="input_radio">
							<input type="radio" name="Type" value="1" />
						</span>{/products.option/}
					</span>
					<span class="input_radio_box hide">
						<span class="input_radio">
							<input type="radio" name="Type" value="2" />
						</span>{/products.tab/}
					</span>
				</div>
			</div>
			<div class="rows clean" data-type="editor" style="display:none;">
				<label>{/global.editor/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
				<div class="input box_attr_input">
					<?=manage::form_edit('', 'editor', 'Editor');?>
				</div>
			</div>
			<div class="rows clean screen_box">
				<label></label>
				<div class="input">
					<span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="IsScreen" value="1" /></span>{/products.notes.screen_notes/}</span>
				</div>
			</div>
			<div class="rows clean global_box">
				<label></label>
				<div class="input">
					<span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="IsGlobal" value="1" /></span>{/products.notes.global_notes/}</span>
				</div>
			</div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="CateId" value="" />
			<input type="hidden" name="AttrId" value="" />
			<input type="hidden" name="CartAttr" value="0" />
			<input type="hidden" name="do_action" value="products.products_attribute_edit" />
		</form>
	</div>
	<div class="global_container fixed_edit_attribute">
		<div class="top_title"><strong>{/products.global.edit_attribute/}</strong><a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_attribute_form">
			<div class="edit_tips color_aaa">{/products.notes.edit_attribute/}</div>
			<div class="edit_attr_list"></div>
			<div class="edit_editor_list"></div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="CateId" value="" />
			<input type="hidden" name="AttrId" value="" />
			<input type="hidden" name="CartAttr" value="1" />
			<input type="hidden" name="Type" value="0" />
			<input type="hidden" name="do_action" value="products.products_attribute_name_edit" />
		</form>
	</div>
	<div class="global_container fixed_edit_attribute_option_edit">
		<div class="top_title"><strong>{/products.global.edit_attr_option/}</strong><a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_attribute_option_edit_form">
			<div class="edit_tips color_aaa">{/products.notes.edit_attribute_option/}</div>
			<div class="edit_attr_list"></div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<div class="bg_no_table_data bg_no_table_fixed">
				<div class="content">
					<p class="color_000">{/products.products.no_options_tips/}</p><a href="javascript:;" class="btn_global btn_add_item btn_cancel">{/global.close/}</a>
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="AttrId" value="" />
			<input type="hidden" name="do_action" value="products.products_attribute_option_edit" />
		</form>
	</div>
	<div class="global_container fixed_associate_pic">
		<div class="top_title"><strong>{/products.products.associated_picture/}</strong><a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_picture_form">
			<div class="edit_tips color_aaa"><?=str_replace(array('%format%', '%size%'), array('.jpg .png .gif', '640*640'), $c['manage']['lang_pack']['notes']['new_pic_tips_1']);?></div>
			<div class="switchery_pictures">
				<div class="switchery">
					<input type="checkbox" value="1" name="IsColor" />
					<div class="switchery_toggler"></div>
					<div class="switchery_inner"><div class="switchery_state_on"></div><div class="switchery_state_off"></div></div>
				</div>
				<span class="color_000">{/products.products.associated_picture_notes/}</span>
			</div>
			<div class="edit_picture_list"></div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<div class="bg_no_table_data bg_no_table_fixed">
				<div class="content">
					<p class="color_000">{/products.products.no_options_tips/}</p><a href="javascript:;" class="btn_global btn_add_item btn_cancel">{/global.close/}</a>
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="CateId" value="" />
			<input type="hidden" name="AttrId" value="" />
			<input type="hidden" name="AttrTitle" value="" />
			<input type="hidden" name="do_action" value="products.products_associate_pic" />
		</form>
	</div>
	<div class="global_container fixed_edit_keyword">
		<div class="top_title"><strong>{/products.global.modify_keyword/}</strong><a href="javascript:;" class="close"></a></div>
		<form class="global_form" id="edit_keyword_form">
			<div class="edit_keyword_list"></div>
			<div class="rows clean box_button">
				<div class="input">
					<input type="submit" class="btn_global btn_submit" value="{/global.save/}" />
					<input type="button" class="btn_global btn_cancel" value="{/global.cancel/}" />
				</div>
			</div>
			<div class="bg_no_table_data bg_no_table_fixed">
				<div class="content">
					<p class="color_000">{/products.products.no_options_tips/}</p><a href="javascript:;" class="btn_global btn_add_item btn_cancel">{/global.close/}</a>
				</div>
			</div>
			<input type="hidden" name="ProId" value="<?=$ProId;?>" />
			<input type="hidden" name="AddProId" value="<?=$_AddProId;?>" />
			<input type="hidden" name="do_action" value="products.products_keyword_edit" />
		</form>
	</div>
</div>

<?php }?>