<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'category'));//检查权限

$Keyword=$_GET['Keyword'];
$CateId=(int)$_GET['CateId'];

//获取类别列表
$cate_ary=str::str_code(db::get_all('products_category', '1'));
$category_ary=array();
foreach((array)$cate_ary as $v){
	$category_ary[$v['CateId']]=$v;
}
$category_count=count($category_ary);
unset($cate_ary);

if($CateId){
	$column='<a href="./?m=products&a=category">{/products.products_category.all_category/}</a>->';
	$category_row=db::get_one('products_category', "CateId='$CateId'");
	$category_description_row=str::str_code(db::get_one('products_category_description', "CateId='$CateId'"));
	$UId=$category_ary[$CateId]['UId'];
	if($UId){
		$key_ary=@explode(',',$UId);
		array_shift($key_ary);
		array_pop($key_ary);
		foreach((array)$key_ary as $k=>$v){
			$column.='<a href="./?m=products&a=category&CateId='.$v.'">'.$category_ary[$v]['Category'.$c['manage']['web_lang']].'</a>->';
		}
	}
	$column.=$category_ary[$CateId]['Category'.$c['manage']['web_lang']];
}else{
	$column='{/products.products_category.all_category/}';
}

$permit_ary=array(
	'add'	=>	manage::check_permit('products', 0, array('a'=>'category', 'd'=>'add')),
	'edit'	=>	manage::check_permit('products', 0, array('a'=>'category', 'd'=>'edit')),
	'del'	=>	manage::check_permit('products', 0, array('a'=>'category', 'd'=>'del'))
);

$top_id_name=($c['manage']['do']=='index'?'category':'category_inside');

echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');
?>
<script type="text/javascript">$(function(){products_obj.category_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
	<?php
	if($c['manage']['do']=='index'){
		//产品分类列表
	?>
		<div class="inside_container">
			<?php
			if($CateId){
				$column='<a href="javascript:history.back(-1);" class="return_title"><span class="return">{/products.products_category.category/}</span>';
				$category_row=db::get_one('products_category', "CateId='$CateId'");
				$category_description_row=str::str_code(db::get_one('products_category_description', "CateId='$CateId'"));
				$UId=$category_ary[$CateId]['UId'];
				if($UId){
					$key_ary=@explode(',',$UId);
					array_shift($key_ary);
					array_pop($key_ary);
					foreach((array)$key_ary as $k=>$v){
						$column.='<span class="s_return">/ '.$category_ary[$v]['Category'.$c['manage']['web_lang']].'</span>';
					}
				}
				$column.='<span class="s_return">/ '.$category_ary[$CateId]['Category'.$c['manage']['web_lang']].'</span>';
				$column.='</a>';
			}else{
				$column='<h1>{/products.products_category.category/}</h1>';
			}
			echo $column;
			?>
		</div>
		<div class="inside_table">
			<div class="list_menu">
				<ul class="list_menu_button">
					<?php if($permit_ary['add']){?><li><a class="add" href="./?m=products&a=category&d=edit">{/global.add/}</a></li><?php }?>
					<?php if($permit_ary['del']){?><li><a class="del" href="javascript:;">{/global.del_bat/}</a></li><?php }?>
					<li><div class="box_explain">{/products.explain.cate_order/}</div></li>
				</ul>
				<div class="search_form">
					<form method="get" action="?">
						<div class="k_input">
							<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}" />
							<input type="button" value="" class="more" />
						</div>
						<input type="submit" class="search_btn" value="{/global.search/}" />
						<div class="ext drop_down">
							<div class="rows item clean">
								<label>{/products.classify/}</label>
								<div class="input">
									<div class="box_select"><?=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', 'Dept<3', '', '{/global.select_index/}');?></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="m" value="products" />
						<input type="hidden" name="a" value="category" />
					</form>
				</div>
			</div>
			<?php
			$where='1';//条件
			$Keyword && $where.=" and Category{$c['manage']['web_lang']} like '%$Keyword%'";
			if($CateId){
				$UId=category::get_UId_by_CateId($CateId);
				$where.=" and UId='{$UId}'";
			}else{
				$where.=' and UId="0,"';
			}
			$category_row=str::str_code(db::get_all('products_category', $where, '*', $c['my_order'].'CateId asc'));
			
			if($category_row){
			?>
				<table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
					<thead>
						<tr>
							<?php if($permit_ary['edit']){?><td width="1%" nowrap="nowrap" class="myorder"></td><?php }?>
							<?php if($permit_ary['del']){?><td width="1%" nowrap="nowrap"><?=html::btn_checkbox('select_all');?></td><?php }?>
							<td width="4%" nowrap="nowrap">ID</td>
							<td width="83%" nowrap="nowrap">{/products.products_category.category_name/}</td>
							<?php if(!$UId){?><td width="4%" nowrap="nowrap">{/products.products.is_index/}</td><?php }?>
							<?php /*********** 暂时先隐藏起来
							<td width="4%" nowrap="nowrap">{/products.products.sold_out/}</td>
							*/?>
							<?php if($permit_ary['edit'] || $permit_ary['del']){?><td width="115" nowrap="nowrap" class="operation">{/global.operation/}</td><?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=1;
						foreach($category_row as $v){
						?>
							<tr data-id="<?=$v['CateId'];?>">
								<?php if($permit_ary['edit']){?><td nowrap="nowrap" align="center" class="myorder move_myorder" data="move_myorder"><i class="icon_myorder"></i></td><?php }?>
								<?php if($permit_ary['del']){?><td nowrap="nowrap"><?=html::btn_checkbox('select', $v['CateId']);?></td><?php }?>
								<td nowrap="nowrap"><?=$v['CateId'];?></td>
								<td><a href="<?=$v['SubCateCount']?"./?m=products&a=category&CateId={$v['CateId']}":'javascript:;';?>" <?=$v['SubCateCount']?'':'class="no_count"';?>  title="<?=$v['Category'.$c['manage']['web_lang']];?>"><?=$v['Category'.$c['manage']['web_lang']];?></a> <?=$v['SubCateCount']?"({$v['SubCateCount']})":''?></td>
								<?php if(!$UId){?>
									<td>
										<?php if($v['Dept']==1){?>
											<div class="switchery<?=(int)$v['IsIndex']?' checked':'';?>">
												<input type="checkbox" name="IsIndex" value="1" data-id="<?=$v['CateId'];?>"<?=(int)$v['IsIndex']?' checked':'';?>>
												<div class="switchery_toggler"></div>
												<div class="switchery_inner">
													<div class="switchery_state_on"></div>
													<div class="switchery_state_off"></div>
												</div>
											</div>
										<?php }?>
									</td>
								<?php }?>
								<?php /*********** 暂时先隐藏起来
								<td>
									<div class="switchery<?=(int)$v['IsSoldOut']?' checked':'';?>">
										<input type="checkbox" name="IsSoldOut" value="1" data-id="<?=$v['CateId'];?>"<?=(int)$v['IsSoldOut']?' checked':'';?>>
										<div class="switchery_toggler"></div>
										<div class="switchery_inner">
											<div class="switchery_state_on"></div>
											<div class="switchery_state_off"></div>
										</div>
									</div>
								</td>
								*/?>
								<?php if($permit_ary['edit'] || $permit_ary['del']){?>
									<td nowrap="nowrap" class="operation side_by_side">
										<?php if($permit_ary['edit']){?><a href="./?m=products&a=category&d=edit&CateId=<?=$v['CateId'];?>">{/global.edit/}</a><?php }?>
										<?php if($permit_ary['del']){?>
											<dl>
												<dt><a href="javascript:;">{/global.more/}<i></i></a></dt>
												<dd class="drop_down"><a class="del item" href="./?do_action=products.category_del&CateId=<?=$v['CateId'];?>" rel="del">{/global.del/}</a></dd>
											</dl>
										<?php }?>
									</td>
								<?php }?>
							</tr>
						<?php }?>
					</tbody>
				</table>
			<?php
			}else{//没有数据
				echo html::no_table_data(($Keyword?0:1), './?m=products&a=category&d=edit');
			}?>
		</div>
	<?php
	}elseif($c['manage']['do']=='edit'){
		//产品分类编辑
		$edit_ok=0;
		if(($CateId && manage::check_permit('products', 0, array('a'=>'category', 'd'=>'edit'))) || (!$CateId && manage::check_permit('products', 0, array('a'=>'category', 'd'=>'add')))) $edit_ok=1;//修改权限
		$category_description_row=db::get_one('products_category_description', "CateId='{$category_row['CateId']}'");
		echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js');
	?>
		<div class="center_container_1200">
			<a href="javascript:history.back(-1);" class="return_title">
				<span class="return">{/module.products.category/}</span> 
				<span class="s_return">/ <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
			</a>
		</div>
		<form id="edit_form" class="global_form center_container_1200">
			<div class="left_container">
				<div class="left_container_side">
					<div class="global_container">
						<div class="big_title">{/products.products.basic_info/}</div>
						<div class="rows clean">
							<label>{/products.title/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
							<div class="input">
								<?=manage::form_edit($category_row, 'text', 'Category', 53, 255, 'notnull');?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.products_category.children/}</label>
							<div class="input">
								<?php
								$now_dept=$category_row['Dept']+3-(db::get_max('products_category', "UId like '{$category_row['UId']}{$category_row['CateId']},%'", 'Dept'));
								$ext_where="CateId!='{$category_row['CateId']}' and Dept<".($category_row['SubCateCount']?$now_dept:3);
								echo '<div class="box_select">'.category::ouput_Category_to_Select('UnderTheCateId', category::get_CateId_by_UId($category_row['UId']), 'products_category', "UId='0,' and $ext_where", $ext_where, '', '{/global.select_index/}').'</div>';
								?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.picture/}</label>
							<div class="input">
								<?=manage::multi_img('PicDetail', 'PicPath', $category_row['PicPath']); ?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/global.brief/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
							<div class="input">
								<?=manage::form_edit($category_row, 'textarea', 'BriefDescription');?>
							</div>
						</div>
						<div class="rows clean">
							<label>{/products.products.description/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
							<div class="input">
								<?=manage::form_edit($category_description_row, 'editor', 'Description');?>
							</div>
						</div>
						<div class="rows clean">
							<div class="center_container_1200">
								<div class="input">
									<input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}" />
									<a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=category';?>" class="btn_global btn_cancel">{/global.return/}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="right_container">
				<div class="global_container">
					<div class="big_title">SEO</div>
					<div class="rows clean">
						<label>{/news.news.seo_title/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($category_row, 'text', 'SeoTitle', 49, 255, '');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/news.news.seo_keyword/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($category_row, 'textarea', 'SeoKeyword');?>
						</div>
					</div>
					<div class="rows clean">
						<label>{/news.news.seo_brief/}<div class="tab_box"><?=manage::html_tab_button();?></div></label>
						<div class="input">
							<?=manage::form_edit($category_row, 'textarea', 'SeoDescription');?>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="CateId" value="<?=$CateId;?>" />
			<input type="hidden" name="do_action" value="products.category_edit" />
		</form>
	<?php }?>
</div>