<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'logistics_line'));//检查权限

$Keyword=$_GET['Keyword'];
$CateId=(int)$_GET['CateId'];


$permit_ary=array(
    'add'	=>	manage::check_permit('products', 0, array('a'=>'logistics_line', 'd'=>'add')),
    'edit'	=>	manage::check_permit('products', 0, array('a'=>'logistics_line', 'd'=>'edit')),
    'del'	=>	manage::check_permit('products', 0, array('a'=>'logistics_line', 'd'=>'del'))
);

$top_id_name=($c['manage']['do']=='index'?'category':'category_inside');



//echo $where;die;
$line_arr = db::get_all('logistics_line','1','*','sort asc');

//print_r($cate_arr);die;
echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');

?>
<script type="text/javascript">$(function(){products_obj.logistics_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
    <?php
    if($c['manage']['do']=='index'){
        ?>
        <div class="inside_container">
            <?php

            $column='<h1>物流列表</h1>';

            echo $column;
            ?>
        </div>
        <div class="inside_table">
            <div class="list_menu">
                <ul class="list_menu_button">
                    <li><a class="add" href="./?m=products&a=logistics_line&d=edit">添加</a></li>
                </ul>

                <div class="search_form">
                    <form method="get" action="?">
                        <input type="hidden" name="m" value="products">
                        <input type="hidden" name="a" value="logistics_line">
                    </form>
                </div>
            </div>
            <? if($line_arr){ ?>

                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                    <tr>
                        <td width="1%" nowrap="nowrap">

                        </td>
                        <td width="4%" nowrap="nowrap">ID</td>
                        <td width="60%" nowrap="nowrap">路线</td>
                        <td width="27%" nowrap="nowrap">货物类型</td>
                        <td width="27%" nowrap="nowrap">时效</td>
                        <td width="27%" nowrap="nowrap">排序</td>
                        <td width="27%" nowrap="nowrap">是否显示</td>
                        <td width="115" nowrap="nowrap" class="operation">操作</td>
                    </tr>
                    </thead>
                    <tbody data-listidx="0">
                    <?php foreach ($line_arr as $item) {?>
                        <tr>
                            <td nowrap="nowrap">
                            </td>
                            <td nowrap="nowrap"><?=$item['id']; ?></td>
                            <td><?=$item['sfdgj'];?>--<?=$item['mddgj'];?><?=$item['name']; ?></td>
                            <td><?=$item['protype']; ?></td>
                            <td><?=$item['prescription']; ?><?=$item['prescriptiondw']; ?></td>
                            <td>
                                <?=$item['sort']; ?>
                            </td>
                            <td>
                                <?=$item['is_show']==1?'是':'否'; ?>
                            </td>
                            <td nowrap="nowrap" class="operation side_by_side">
                                <a href="./?m=products&a=logistics_line&d=edit&CateId=<?=$item['id']; ?>">修改</a>																					<dl>
<!--                                    <dt><a href="javascript:;">更多<i></i></a></dt>-->
<!--                                    <dd class="drop_down"><a class="del item" href="./?do_action=products.logistics_line_del&CateId=--><?//=$item['id']; ?><!--" rel="del">删除</a></dd>-->
                                </dl>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <? }else{
                echo html::no_table_data(($Keyword?0:1), './?m=products&a=logistics_line&d=edit');
            } ?>
        </div>
    <?php
    }elseif($c['manage']['do']=='edit'){
    //一级

    echo ly200::load_static('/static/js/plugin/ckeditor/ckeditor.js', '/static/js/plugin/daterangepicker/daterangepicker.css', '/static/js/plugin/daterangepicker/moment.min.js', '/static/js/plugin/daterangepicker/daterangepicker.js', '/static/js/plugin/dragsort/dragsort-0.5.1.min.js');

    //产品分类编辑
    $edit_ok=0;
    if ($CateId){
        $cate = db::get_one('logistics_line','id = '.$CateId);
    }
    if(($CateId && manage::check_permit('products', 0, array('a'=>'logistics_line', 'd'=>'edit'))) || (!$CateId && manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'add')))) $edit_ok=1;//修改权限
    ?>
        <style>
            .w150{
                width: 150px;
            }
        </style>
        <div class="center_container_1200">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">路线</span>
                <span class="s_return">/ <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
            </a>
        </div>
        <form id="edit_form" class="global_form center_container_1200">
            <div class="left_container">
                <div class="left_container_side">
                    <div class="global_container">
                        <div class="big_title">{/products.products.basic_info/}</div>
                        <div class="rows clean">
                            <label>路线名（中文）</label>
                            <div class="input">
                                <input type="text" name="name" value="<?=$cate['name']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>路线名(英文)</label>
                            <div class="input">
                                <input type="text" name="name_en" value="<?=$cate['name_en']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>路线名(波斯语)</label>
                            <div class="input">
                                <input type="text" name="name_fa" value="<?=$cate['name_fa']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>始发地国家</label>
                            <div class="input">
                                中文：<input type="text" name="sfdgj" value="<?=$cate['sfdgj']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                英文：<input type="text" name="sfdgj_en" value="<?=$cate['sfdgj_en']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                波斯语：<input type="text" name="sfdgj_fa" value="<?=$cate['sfdgj_fa']; ?>" class="box_input w150" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>始发地城市</label>
                            <div class="input">
                                中文：<input type="text" name="sfdcs" value="<?=$cate['sfdcs']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                英文：<input type="text" name="sfdcs_en" value="<?=$cate['sfdcs_en']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                波斯语：<input type="text" name="sfdcs_fa" value="<?=$cate['sfdcs_fa']; ?>" class="box_input w150" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>目的地国家</label>
                            <div class="input">
                                中文：<input type="text" name="mddgj" value="<?=$cate['mddgj']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                英文：<input type="text" name="mddgj_en" value="<?=$cate['mddgj_en']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                波斯语：<input type="text" name="mddgj_fa" value="<?=$cate['mddgj_fa']; ?>" class="box_input w150" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>目的地城市</label>
                            <div class="input">
                                中文：<input type="text" name="mddcs" value="<?=$cate['mddcs']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                英文：<input type="text" name="mddcs_en" value="<?=$cate['mddcs_en']; ?>" class="box_input w150" size="53" maxlength="255" notnull="" >
                                波斯语：<input type="text" name="mddcs_fa" value="<?=$cate['mddcs_fa']; ?>" class="box_input w150" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>配送方式</label>
                            <div class="input">
                                <select name="psfs" id="" class="box_input">
                                    <option value="1" <?=$cate['psfs']=='1'?'selected':''; ?>>直达</option>
                                    <option value="2" <?=$cate['psfs']=='2'?'selected':''; ?>>海运</option>
                                </select>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>货物类型（中文）</label>
                            <div class="input">
                                <input type="text" name="protype" value="<?=$cate['protype']; ?>" class="box_input" size="53" maxlength="255" >
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>货物类型（英文）</label>
                            <div class="input">
                                <input type="text" name="protype_en" value="<?=$cate['protype_en']; ?>" class="box_input" size="53" maxlength="255" >
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>货物类型（波斯）</label>
                            <div class="input">
                                <input type="text" name="protype_fa" value="<?=$cate['protype_fa']; ?>" class="box_input" size="53" maxlength="255" >
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>时效</label>
                            <div class="input">
                                <input type="text" name="prescription" value="<?=$cate['prescription']; ?>" class="box_input" size="53" maxlength="255" >
                                <select name="prescriptiondw" id="" class="box_input">
                                    <option value="days" <?=$cate['prescriptiondw']=='days'?'selected':''; ?>>天</option>
                                    <option value="weeks" <?=$cate['prescriptiondw']=='weeks'?'selected':''; ?>>周</option>
                                    <option value="month" <?=$cate['prescriptiondw']=='month'?'selected':''; ?>>月</option>
                                </select>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>箱子单价</label>
                            <div class="input">
                                <input type="text" name="xsdanjia" value="<?=$cate['xsdanjia']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>体积单价</label>
                            <div class="input">
                                <input type="text" name="tjdanjia" value="<?=$cate['tjdanjia']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>体积 最低起送值</label>
                            <div class="input">
                                <input type="text" name="tjqs" value="<?=$cate['tjqs']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>重量单价</label>
                            <div class="input">
                                <input type="text" name="zldanjia" value="<?=$cate['zldanjia']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>重量 最低起送值</label>
                            <div class="input">
                                <input type="text" name="zlqs" value="<?=$cate['zlqs']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>货币值比例(%)</label>
                            <div class="input">
                                <input type="text" name="hzb" value="<?=$cate['hzb']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>详情（中文）</label>
                            <div class="input"><?=manage::Editor('content',$cate['content']);?></div>
                        </div>
                        <div class="rows clean">
                            <label>详情（英文）</label>
                            <div class="input"><?=manage::Editor('content_en',$cate['content_en']);?></div>
                        </div>
                        <div class="rows clean">
                            <label>详情（波斯）</label>
                            <div class="input"><?=manage::Editor('content_fa',$cate['content_fa']);?></div>
                        </div>
                        <div class="rows clean">
                            <label>排序</label>
                            <div class="input">
                                <input type="text" name="sort" value="<?=$cate['sort']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>是否显示</label>
                            <div class="input">
                                <select name="is_show" id="" class="box_input">
                                    <option value="1" <?=$cate['is_show']==1?'selected':''; ?>>是</option>
                                    <option value="2" <?=$cate['is_show']==2?'selected':''; ?>>否</option>
                                </select>
                            </div>
                        </div>
                        <div class="rows clean">
                            <div class="center_container_1200">
                                <div class="input">
                                    <input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}" />
                                    <a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=logistics_line';?>" class="btn_global btn_cancel">{/global.return/}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="CateId" value="<?=$CateId;?>" />
            <input type="hidden" name="do_action" value="products.logistics_line_edit" />
        </form>
    <?php }elseif($c['manage']['do']=='upload'){
    echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
    ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.bcupload_init()});</script>
        <div class="center_container_1200 center_container">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">分类管理</span>
                <span class="s_return">/ {/products.global.upload/}</span>
            </a>
            <form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <div class="global_container">
                    <div id="form_container">
                        <div class="explode_box">
                            <div class="box_item">
                                <div class="tit">{/global.upload_table/}</div>
                                <div class="desc">{/global.submit_table/}</div>
                                <div class="input upload_file">
                                    <input name="ExcelFile" value="" type="text" class="box_input" id="excel_path" size="50" maxlength="100" readonly notnull />
                                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                    <div class="row fileupload-buttonbar">
										<span class="btn_file btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>{/global.file_upload/}</span>
											<input type="file" name="Filedata" multiple>
										</span>
                                        <div class="fileupload-progress fade"><div class="progress-extended"></div></div>
                                        <div class="clear"></div>
                                        <div class="photo_multi_img template-box files"></div>
                                        <div class="photo_multi_img" id="PicDetail"></div>
                                    </div>
                                    <script id="template-upload" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										<div class="template-upload fade">
											<div class="clear"></div>
											<div class="items">
												<p class="name">{%=file.name%}</p>
												<strong class="error text-danger"></strong>
											</div>
											<div class="items">
												<p class="size">Processing...</p>
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
											</div>
											<div class="items">
												{% if (!i) { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
									</script>
                                    <script id="template-download" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										{% if (file.thumbnailUrl) { %}
											<div class="pic template-download fade hide">
												<div>
													<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
													<a href="{%=file.url%}" class="zoom" target="_blank"></a>
													{% if (file.deleteUrl) { %}
														<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } %}
													<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
												</div>
												<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
											</div>
										{% } else { %}
											<div class="template-download fade hide">
												<div class="clear"></div>
												<div class="items">
													<p class="name">
														{% if (file.url) { %}
															<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
														{% } else { %}
															<span>{%=file.name%}</span>
														{% } %}
													</p>
													{% if (file.error) { %}
														<div><span class="label label-danger">Error</span> {%=file.error%}</div>
													{% } %}
												</div>
												<div class="items">
													<span class="size">{%=o.formatFileSize(file.size)%}</span>
												</div>
												<div class="items">
													{% if (file.deleteUrl) { %}
														<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
															<i class="glyphicon glyphicon-trash"></i>
															<span>{/global.del/}</span>
														</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } else { %}
														<button class="btn_file btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>{/global.cancel/}</span>
														</button>
													{% } %}
												</div>
												<div class="clear"></div>
											</div>
										{% } %}
									{% } %}
									</script>
                                </div>
                                <div class="rows clean">
                                    <label></label>
                                    <div class="input input_button">
                                        <input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
                                        <a href="./?m=products&a=logistics_line" class="btn_global btn_cancel">{/global.return/}</a>
                                        <input type="button" class="btn_global btn_picture" value="" style="display:none;" />
                                        <input type="hidden" name="do_action" value="products.zhzupload" />
                                        <input type="hidden" name="Number" value="0" />
                                        <input type="hidden" name="Current" value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="progress_container">
                        <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                            <thead>
                            <tr>
                                <td width="50%" nowrap="nowrap">分类名</td>
                                <td width="50%" nowrap="nowrap">状态</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="progress_loading"></div>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div>