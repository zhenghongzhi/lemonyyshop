<?php !isset($c) && exit();?>
<?php
manage::check_permit('products', 1, array('a'=>'business_cate'));//检查权限

$Keyword=$_GET['Keyword'];
$CateId=(int)$_GET['CateId'];

//获取类别列表
$cate_ary=str::str_code(db::get_all('business_cate', 'is_del = 1'));
$category_ary=array();
foreach((array)$cate_ary as $v){
    $category_ary[$v['id']]=$v;
}
$category_count=count($category_ary);
unset($cate_ary);



$permit_ary=array(
    'add'	=>	manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'add')),
    'edit'	=>	manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'edit')),
    'del'	=>	manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'del'))
);

$top_id_name=($c['manage']['do']=='index'?'category':'category_inside');

//分类获取
$where = "1";
if ($CateId){
    $category_row = db::get_one("business_cate","id=".$CateId);
    $UId=$category_row['uid'].$category_row['id'].',';
    $where .= " and uid = '".$UId."'";
}else{
    $where .= " and uid = '0,'";
}


//echo $where;die;
$cate_arr = db::get_all('business_cate',$where,'*','sort asc');

//分类目录
$catelog_arr = db::get_all('business_catelog','is_del = 1','*','sort asc');

//print_r($cate_arr);die;

echo ly200::load_static('/static/js/plugin/dragsort/dragsort-0.5.1.min.js');


?>
<script type="text/javascript">$(function(){products_obj.cate_init()});</script>
<div id="<?=$top_id_name;?>" class="r_con_wrap">
    <?php
    if($c['manage']['do']=='index'){
        //产品分类列表
        ?>
        <div class="inside_container">
            <?php
            if($CateId){
                $column='<a href="javascript:history.back(-1);" class="return_title"><span class="return">供应商产品分类</span>';
                $category_row=db::get_one('business_cate', "id='$CateId'");
                $UId=$category_ary[$CateId]['uid'];

                if($UId){
                    $key_ary=@explode(',',$UId);
                    array_shift($key_ary);
                    array_pop($key_ary);
                    foreach((array)$key_ary as $k=>$v){
                        $column.='<span class="s_return">/ '.$category_ary[$v]['name'].'</span>';
                    }
                }
                $column.='<span class="s_return">/ '.$category_ary[$CateId]['name'].'</span>';
                $column.='</a>';
            }else{
                $column='<h1>{/products.products_category.category/}</h1>';
            }
            echo $column;
            ?>
        </div>
        <div class="inside_table">
            <div class="list_menu">
                <ul class="list_menu_button">
                    <li><a class="add" href="./?m=products&a=business_cate&d=edit">添加</a></li>
                    <li><a class="upload" href="./?m=products&a=business_cate&d=upload">导入</a></li>
                </ul>

                <div class="search_form">
                    <form method="get" action="?">
                        <input type="hidden" name="m" value="products">
                        <input type="hidden" name="a" value="business_cate">
                    </form>
                </div>
            </div>
            <? if($cate_arr){ ?>

                <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                    <thead>
                    <tr>
                        <td width="1%" nowrap="nowrap">

                        </td>
                        <td width="4%" nowrap="nowrap">ID</td>
                        <td width="60%" nowrap="nowrap">分类名称</td>
                        <td width="27%" nowrap="nowrap">编码</td>
                        <td width="27%" nowrap="nowrap">是否显示</td>
                        <td width="115" nowrap="nowrap" class="operation">操作</td>
                    </tr>
                    </thead>
                    <tbody data-listidx="0">
                    <?php foreach ($cate_arr as $item) {
                        $aa = db::get_one("business_cate","uid = '".$item['uid'].$item['id'].",'","count(*) as count");
                        ?>
                        <tr>
                            <td nowrap="nowrap">
                            </td>
                            <td nowrap="nowrap"><?=$item['id']; ?></td>
                            <td><a href="./?m=products&a=business_cate&CateId=<?=$item['id']; ?>" title="<?=$item['name']; ?>"><?=$item['name']; ?>(<?=$aa["count"]; ?>)</a></td>
                            <td>
                                <?=$item['number']; ?>
                            </td>
                            <td>
                                <?=$item['is_del']==1?'是':'否'; ?>
                            </td>
                            <td nowrap="nowrap" class="operation side_by_side">
                                <a href="./?m=products&a=business_cate&d=edit&CateId=<?=$item['id']; ?>">修改</a>																					<dl>
                                    <dt><a href="javascript:;">更多<i></i></a></dt>
                                    <dd class="drop_down"><a class="del item" href="./?do_action=products.business_cate_del&CateId=<?=$item['id']; ?>" rel="del">删除</a></dd>
                                </dl>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <? }else{
                echo html::no_table_data(($Keyword?0:1), './?m=products&a=business_cate&d=edit');
            } ?>
        </div>
        <?php
    }elseif($c['manage']['do']=='edit'){
        //一级
        $one = db::get_all("business_cate","is_del = 1 and uid = '0,'");
        foreach ($one as $key=>$item){
            $erji = db::get_all("business_cate","is_del = 1 and uid = '".$item['uid'].$item['id'].",'");
            $one[$key]['son'] = $erji;
        }

        //产品分类编辑
        $edit_ok=0;
        if ($CateId){
            $cate = db::get_one('business_cate','id = '.$CateId);
            $aa = category::get_CateId_by_UId($cate['uid']);
            //            echo $aa;die;
        }
        if(($CateId && manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'edit'))) || (!$CateId && manage::check_permit('products', 0, array('a'=>'business_cate', 'd'=>'add')))) $edit_ok=1;//修改权限
        ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.business_edit_init()});</script>
        <div class="center_container_1200">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">{/module.products.category/}</span>
                <span class="s_return">/ <?=$CateId?'{/global.edit/}':'{/global.add/}';?></span>
            </a>
        </div>
        <form id="edit_form" class="global_form center_container_1200">
            <div class="left_container">
                <div class="left_container_side">
                    <div class="global_container">
                        <div class="big_title">{/products.products.basic_info/}</div>
                        <div class="rows clean">
                            <label>{/products.name/}</label>
                            <div class="input">
                                <input type="text" name="name" value="<?=$cate['name']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>{/products.name/}(英文)</label>
                            <div class="input">
                                <input type="text" name="name_en" value="<?=$cate['name_en']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>{/products.name/}(波斯语)</label>
                            <div class="input">
                                <input type="text" name="name_fa" value="<?=$cate['name_fa']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>

                        <div class="rows clean">
                            <label>隶属栏目</label>
                            <div class="input">
                                <div class="box_select">
                                    <select name="cid" id="">
                                        <option value="0">请选择</option>
                                        <?php foreach ($one as $value) { ?>
                                            <option value="<?=$value['id']; ?>" <?=$value['id']==$aa?'selected':''; ?>>&nbsp;┝<?=$value['name']; ?></option>
                                            <?php if ($value['son']){
                                                foreach ($value['son'] as $val){ ?>
                                                    <option value="<?=$val['id']; ?>" <?=$val['id']==$aa?'selected':''; ?>>&nbsp;｜┝<?=$val['name']; ?></option>
                                                <? }
                                            } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>分类目录</label>
                            <div class="input">
                                <div class="box_select">
                                    <select name="catelogid" id="">
                                        <option value="0">请选择</option>
                                        <?php foreach ($catelog_arr as $value) { ?>
                                            <option value="<?=$value['id']; ?>" <?=$value['id']==$cate['catelogid']?'selected':''; ?>><?=$value['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>图片</label>
                            <div class="input">
                                <?=manage::multi_img('img', 'img', $cate['img']); ?>
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>编码</label>
                            <div class="input">
                                <input type="text" name="number" value="<?=$cate['number']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>颜色</label>
                            <div class="input">
                                <input type="text" name="color" value="<?=$cate['color']; ?>" class="box_input" size="53" maxlength="255">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>排序</label>
                            <div class="input">
                                <input type="text" name="sort" value="<?=$cate['sort']; ?>" class="box_input" size="53" maxlength="255" notnull="">
                            </div>
                        </div>
                        <div class="rows clean">
                            <label>是否显示</label>
                            <div class="input">
                                <select name="is_del" id="" class="box_input">
                                    <option value="1" <?=$cate['is_del']==1?'selected':''; ?>>是</option>
                                    <option value="2" <?=$cate['is_del']==2?'selected':''; ?>>否</option>
                                </select>
                            </div>
                        </div>
                        <div class="rows clean">
                            <div class="center_container_1200">
                                <div class="input">
                                    <input type="submit" class="btn_global btn_submit" name="submit_button" value="{/global.save/}" />
                                    <a href="<?=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'./?m=products&a=business_cate';?>" class="btn_global btn_cancel">{/global.return/}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="CateId" value="<?=$CateId;?>" />
            <input type="hidden" name="do_action" value="products.business_cate_edit" />
        </form>
    <?php }elseif($c['manage']['do']=='upload'){
        echo ly200::load_static('/static/js/plugin/file_upload/js/vendor/jquery.ui.widget.js','/static/js/plugin/file_upload/js/external/tmpl.js','/static/js/plugin/file_upload/js/external/load-image.js','/static/js/plugin/file_upload/js/external/canvas-to-blob.js','/static/js/plugin/file_upload/js/external/jquery.blueimp-gallery.js','/static/js/plugin/file_upload/js/jquery.iframe-transport.js','/static/js/plugin/file_upload/js/jquery.fileupload.js','/static/js/plugin/file_upload/js/jquery.fileupload-process.js','/static/js/plugin/file_upload/js/jquery.fileupload-image.js','/static/js/plugin/file_upload/js/jquery.fileupload-audio.js','/static/js/plugin/file_upload/js/jquery.fileupload-video.js','/static/js/plugin/file_upload/js/jquery.fileupload-validate.js','/static/js/plugin/file_upload/js/jquery.fileupload-ui.js');
        ?>
        <script type="text/javascript">$(document).ready(function(){products_obj.bcupload_init()});</script>
        <div class="center_container_1200 center_container">
            <a href="javascript:history.back(-1);" class="return_title">
                <span class="return">分类管理</span>
                <span class="s_return">/ {/products.global.upload/}</span>
            </a>
            <form id="upload_edit_form" class="global_form" name="upload_form" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                <div class="global_container">
                    <div id="form_container">
                        <div class="explode_box">
                            <div class="box_item">
                                <div class="tit">{/global.upload_table/}</div>
                                <div class="desc">{/global.submit_table/}</div>
                                <div class="input upload_file">
                                    <input name="ExcelFile" value="" type="text" class="box_input" id="excel_path" size="50" maxlength="100" readonly notnull />
                                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                    <div class="row fileupload-buttonbar">
										<span class="btn_file btn-success fileinput-button">
											<i class="glyphicon glyphicon-plus"></i>
											<span>{/global.file_upload/}</span>
											<input type="file" name="Filedata" multiple>
										</span>
                                        <div class="fileupload-progress fade"><div class="progress-extended"></div></div>
                                        <div class="clear"></div>
                                        <div class="photo_multi_img template-box files"></div>
                                        <div class="photo_multi_img" id="PicDetail"></div>
                                    </div>
                                    <script id="template-upload" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										<div class="template-upload fade">
											<div class="clear"></div>
											<div class="items">
												<p class="name">{%=file.name%}</p>
												<strong class="error text-danger"></strong>
											</div>
											<div class="items">
												<p class="size">Processing...</p>
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
											</div>
											<div class="items">
												{% if (!i) { %}
													<button class="btn_file btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>{/global.cancel/}</span>
													</button>
												{% } %}
											</div>
											<div class="clear"></div>
										</div>
									{% } %}
									</script>
                                    <script id="template-download" type="text/x-tmpl">
									{% for (var i=0, file; file=o.files[i]; i++) { %}
										{% if (file.thumbnailUrl) { %}
											<div class="pic template-download fade hide">
												<div>
													<a href="javascript:;" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" /><em></em></a>
													<a href="{%=file.url%}" class="zoom" target="_blank"></a>
													{% if (file.deleteUrl) { %}
														<button class="btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>{/global.del/}</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } %}
													<input type="hidden" name="PicPath[]" value="{%=file.url%}" disabled />
												</div>
												<input type="text" maxlength="30" class="form_input" value="{%=file.name%}" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" disabled notnull />
											</div>
										{% } else { %}
											<div class="template-download fade hide">
												<div class="clear"></div>
												<div class="items">
													<p class="name">
														{% if (file.url) { %}
															<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
														{% } else { %}
															<span>{%=file.name%}</span>
														{% } %}
													</p>
													{% if (file.error) { %}
														<div><span class="label label-danger">Error</span> {%=file.error%}</div>
													{% } %}
												</div>
												<div class="items">
													<span class="size">{%=o.formatFileSize(file.size)%}</span>
												</div>
												<div class="items">
													{% if (file.deleteUrl) { %}
														<button class="btn_file btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
															<i class="glyphicon glyphicon-trash"></i>
															<span>{/global.del/}</span>
														</button>
														<input type="checkbox" name="delete" value="1" class="toggle" style="display:none;">
													{% } else { %}
														<button class="btn_file btn-warning cancel">
															<i class="glyphicon glyphicon-ban-circle"></i>
															<span>{/global.cancel/}</span>
														</button>
													{% } %}
												</div>
												<div class="clear"></div>
											</div>
										{% } %}
									{% } %}
									</script>
                                </div>
                                <div class="rows clean">
                                    <label></label>
                                    <div class="input input_button">
                                        <input type="submit" class="btn_global btn_submit" value="{/global.submit/}" />
                                        <a href="./?m=products&a=business_cate" class="btn_global btn_cancel">{/global.return/}</a>
                                        <input type="button" class="btn_global btn_picture" value="" style="display:none;" />
                                        <input type="hidden" name="do_action" value="products.zhzupload" />
                                        <input type="hidden" name="Number" value="0" />
                                        <input type="hidden" name="Current" value="0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="progress_container">
                        <table border="0" cellpadding="5" cellspacing="0" class="r_con_table">
                            <thead>
                            <tr>
                                <td width="50%" nowrap="nowrap">分类名</td>
                                <td width="50%" nowrap="nowrap">状态</td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div id="progress_loading"></div>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div>