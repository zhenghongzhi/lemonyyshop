<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class html{
	/**
	 * 后台 首页设置的显示内容
	 *
	 * @param: $config[object]	首页设置的数据
	 * @return string
	 */
	public static function config_edit_form($config){
		global $c;
		/*
		'Effects'		//特效方式
		'HeaderContent'	//头部内容
		'IndexContent'	//首页内容
		'TopMenu'		//顶部栏目
		'ContactMenu'	//联系我们
		'ShareMenu'		//分享栏目
		'IndexTitle'	//首页标题
		*/
		$ShareMenuAry=$c['manage']['config']['ShareMenu'];
		$ContactMenuAry=$c['manage']['config']['ContactMenu'];
		$TopMenuAry=$c['manage']['config']['TopMenu'];
		$html='';
		/*
		if((int)$config['Effects']){ //特效方式
			$html.='
				<div class="rows" style="display:none;">
					<label>{/themes.products_list.effects_order/}</label>
					<div class="input effects_list">
						<select name="Effects">
			';
				for($i=0; $i<7; ++$i){
					$html.='<option value="'.$i.'" number="'.$i.'"'.($c['manage']['config']['Effects']==$i?' selected':'').'>{/themes.products_list.effects_ary.'.$i.'/}</option>';
				}
			$html.='
						</select>
					</div>					
				</div>
			';
		}
		if($config['HeaderContent'] || $config['IndexContent']){
			$html.='
				<div class="rows">
					<label>{/global.other/}<div class="tab_box">'.manage::html_tab_button('border').'</div></label>
					<div class="input">';
					foreach($c['manage']['config']['Language'] as $k=>$v){
						$html.='<div class="tab_txt tab_txt_'.$v.'">';
							for($i=0;$i<$config['HeaderContent'];++$i){ //头部内容
								$num = $config['HeaderContent']>1 ? '_'.$i : '';
								$html.='<span class="unit_input lang_input"><b>{/set.config.header_content/}</b><input type="text" name="HeaderContent'.$num.'_'.$v.'" value="'.htmlspecialchars(htmlspecialchars_decode($c['manage']['config']['HeaderContent']["HeaderContent{$num}_{$v}"]), ENT_QUOTES).'" class="box_input" size="35" /></span>';
							}
							for($i=0;$i<$config['IndexContent'];++$i){ //首页内容
								$num = $config['IndexContent']>1 ? '_'.$i : '';
								$html.='<div class="rows"><label>{/set.config.index_content/}'.($i+1).'</label><div class="input"><textarea class="box_textarea" name="IndexContent'.$num.'_'.$v.'">'.htmlspecialchars(htmlspecialchars_decode($c['manage']['config']['IndexContent']["IndexContent{$num}_{$v}"]), ENT_QUOTES).'</textarea></div></div>';
							}
						$html.='</div>';
					}
			$html.='
					</div>
				</div>
			';
		}
		if((int)$config['TopMenu']){ //顶部栏目
			$html.='
				<div class="rows">
					<label>{/set.config.topmenu/}<div class="tab_box">'.manage::html_tab_button('border').'</div></label>
					<div class="input">';
					foreach($c['manage']['config']['Language'] as $k=>$v){
					$html.='<div class="tab_txt tab_txt_'.$v.'">';
					for($i=0; $i<4; ++$i){
						$html.='
							<div class="help_item">
								<span class="unit_input not_input"><b>{/global.name/}</b><input type="text" name="TopName_'.$v.'[]" value="'.$TopMenuAry[$i]["TopName_{$v}"].'" class="box_input input_name" size="25" /></span>
								<span class="unit_input not_input long"><b>{/themes.nav.url/}</b><input type="text" name="TopUrl[]" value="'.$TopMenuAry[$i]['TopUrl'].'" class="box_input input_url" size="45" '.($k?' disabled':'').' /></span>
								<b>{/themes.nav.target/}</b>
								<div class="switchery'.($TopMenuAry[$i]['TopNewTarget']?' checked':'').'">
									<input type="checkbox" name="TopNewTarget[]" value="1"'.($TopMenuAry[$i]['TopNewTarget']?' checked':'').($k?' disabled':'').'>
									<div class="switchery_toggler"></div>
									<div class="switchery_inner">
										<div class="switchery_state_on"></div>
										<div class="switchery_state_off"></div>
									</div>
								</div>
							</div>';
						}
					$html.='</div>';
					}
			$html.='</div>					
				</div>
			';
		}*/
		if((int)$config['ContactMenu']){ //联系我们
			$html.='
				<div class="rows">
					<label>{/set.config.contactmenu/}</label>
					<div class="input">
			';
						for($i=0; $i<4; ++$i){
							$html.='<span class="unit_input" '.(($i==0 || $i==2) ? 'style="display:none;"' : '').'><b>{/set.config.contactmenu_ary.'.$i.'/}</b><input type="text" name="ContactMenu[]" value="'.$ContactMenuAry[$i].'" class="box_input" size="35" /></span><div class="blank6"></div>';
						}
			$html.='
					</div>
				</div>
			';
		}
		/*
		if((int)$config['ShareMenu']){ //分享栏目
			$html.='
				<div class="rows">
					<label>{/set.config.sharemenu/}</label>
					<div class="input">';
						foreach($c['share'] as $v){
							$html.='<span class="unit_input lang_input"><b>'.$v.'</b><input type="text" name="Share'.$v.'" value="'.$ShareMenuAry[$v].'" class="box_input" size="35" /></span><div class="blank6"></div>';
						}
			$html.='
					</div>					
				</div>
			';
		}
		if((int)$config['IndexTitle']){ //首页标题
			$html.='
				<div class="rows">
					<label>{/module.account.index/}{/global.title/}</label>
					<div class="input"><input name="IndexTitle" value="'.$c['manage']['config']['IndexTitle'].'" type="text" class="box_input" size="50" /></div>
				</div>
			';
		}*/
		return $html;
	}
	
	/**
	 * 后台 勾选按钮
	 *
	 * @param: $rating[int]	评论平均数
	 * @return string
	 */
	public static function btn_checkbox($name, $value='', $class=''){
		$html='';
		
		$html.='<div class="btn_checkbox '.$class.'">';
			$html.='<em class="button"></em>';
			$html.='<input type="checkbox" name="'.$name.'" value="'.$value.'" />';
		$html.='</div>';
		return $html;
	}
	
	/**
	 * 后台 翻页按钮
	 *
	 * @param: $rating[int]	评论平均数
	 * @return string
	 */
	public static function turn_page($row_count, $page, $total_pages, $page_string, $base_page=3){	//翻页
		global $c;
		if(!$row_count){return;}
		$start=$page-$base_page>0?$page-$base_page:1;
		$end=$page+$base_page>=$total_pages?$total_pages:$page+$base_page;
		($total_pages-$page)<$base_page && $start=$start-($base_page-($total_pages-$page));
		$page<=$base_page && $end=$end+($base_page-$page+1);
		$start<1 && $start=1;
		$end>=$total_pages && $end=$total_pages;
		$pre=$page-1>0?$page-1:1;
		
		$html='';
		$html.='<div class="turn_page">';
			$html.='<div class="total_count">'.str_replace('%num%', $row_count, $c['manage']['lang_pack']['notes']['page_tips']).'</div>';
			$html.='<ul class="page">';
				if($page<=1){
					$html.='<li class="page_first"><span class="page_item page_noclick"><em class="icon_page_prev"></em></span></li>';
				}else{
					$html.='<li class="page_first"><a href="'.$page_string.$pre.'" class="page_item page_button"><em class="icon_page_prev"></em>'.$pre_page.'</a></li>';
				}
				$start>1 && $html.='<li><a href="'.$page_string.'1'.'" class="page_item">1</a></li><li><span class="page_omitted">...</span></li>';
				for($i=$start; $i<=$end; $i++){
					$html.=$page!=$i?'<li><a href="'.$page_string.$i.'" class="page_item">'.$i.'</a></li>':'<li><span class="page_item page_item_current">'.$i.'</span></li>';
				}
				$end<$total_pages && $html.='<li><span class="page_omitted">...</span></li><li><a href="'.$page_string.$total_pages.'" class="page_item">'.$total_pages.'</a></li>';
				$next=$page+1>$total_pages?$total_pages:$page+1;
				if($page+1>$total_pages){
					$html.='<li class="page_last"><span class="page_item page_noclick"><em class="icon_page_next"></em></span></li>';
				}else{
					$page>=$total_pages && $page--;
					$html.='<li class="page_last"><a href="'.$page_string.$next.'" class="page_item page_button"><em class="icon_page_next"></em></a></li>';
				}
			$html.='</ul>';
			/*$html.='<div class="turn">';
				$html.='跳至';
			$html.='</div>';*/
		$html.='</div>';
		return $html;
	}
	
	/**
	 * 后台 多功能选项按钮
	 *
	 * @param: $name[string]	名称
	 * @param: $value[string]	数值
	 * @param: $type[string]	类型
	 * @param: $isCurrent[int]	是否为勾选
	 * @param: $isFixed[int]	是否为固定选项
	 * @param: $isDelete[int]	是否能删除选项
	 * @param: $icon_txt[html]	图标文本
	 * @return string
	 */
	public static function box_option_button($name, $value, $type, $isCurrent=0, $isFixed=0, $isDelete=0, $icon_txt=''){
		$html='';
		$html.=	'<span class="btn_attr_choice'.($isCurrent?' current':'').'" data-type="'.$type.'"'.($isFixed?' data-fixed="1"':'').'>';
		$html.=		($icon_txt?$icon_txt:'');
		$html.=		'<b>'.$name.'</b>';
		$html.=		'<input type="checkbox" name="'.$type.'Current[]" value="'.$value.'" class="option_current"'.($isCurrent?' checked':'').' />';
		$html.=		'<input type="hidden" name="'.$type.'Option[]" value="'.$value.'" />';
		$html.=		'<input type="hidden" name="'.$type.'Name[]" value="'.$name.'" />';
		$html.=		'<i'.($isDelete==0?' style="display:none;"':'').'></i>';
		$html.='</span>';
		return $html;
	}
	
	/**
	 * 后台 内页表格空白提示
	 *
	 * @param: $isAdd[int]		是否显示添加按钮
	 * @param: $addUrl[string]	添加按钮地址
	 * @return string
	 */
	public static function no_table_data($isAdd=0, $addUrl='javascript:;'){
		global $c;
		$html='';
		$html.=	'<div class="bg_no_table_data">';
		$html.=		'<div class="content">';
		$html.=			'<p class="color_000">'.$c['manage']['lang_pack']['error']['no_table_data'].'</p>';
		$html.=			($isAdd==1?'<a href="'.$addUrl.'" class="btn_global btn_add_item">'.$c['manage']['lang_pack']['error']['add_now'].'</a>':'');
		$html.=		'</div>';
		$html.=	'</div>';
		return $html;
	}
	
	/**
	 * PC端 评论星星
	 *
	 * @param: $rating[int]	评论平均数
	 * @return string
	 */
	public static function review_star($rating, $big=0){
		global $c;
		$html='';
		if($c['themes_type']==1){
			$big || $class='s_review_star';
		}else{
			$class='review_star_2';
			$big || $class.=' s_review_star_2';
		}
		$html.='<span class="review_star '.$class.'">';
		for($i=1; $i<6; ++$i){
			if($i<=$rating){
				$html.='<span class="star_1"></span>';
			}else{
				$html.='<span class="star_0"></span>';
			}
		}
		$html.='</span>';
		return $html;
	}
	
	/**
	 * PC端 第三方视频
	 *
	 * @param: $url[string]	视频地址
	 * @return string
	 */
	public static function video_show($url, $auto=0){
		global $c;
		$html=$Type=$ID='';
		$type_ary=array(
			'youtube'	=> array(0=>'https://www.youtube.com/watch?v=', 1=>'https://youtu.be/', 2=>'https://www.youtube.com/embed/'),
			'vimeo'		=> array(0=>'https://vimeo.com/channels/staffpicks/', 1=>'https://vimeo.com/', 2=>'https://player.vimeo.com/video/'),
		);
		foreach($type_ary as $k=>$v){
			foreach($v as $v2){
				$path=strstr($url, $v2);
				if($path){
					$Type=$k;//视频类型
					$ID=str_replace($v2, '', $path);//视频编号
					break;
				}
			}
		}
		if($Type=='youtube'){
			$html='<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'.$ID.'?rel=0&amp;controls=0&amp;showinfo=0'.($auto?'&amp;autoplay=1':'').'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		}elseif($Type=='vimeo'){
			$html='<iframe src="https://player.vimeo.com/video/'.$ID.'?title=0&byline=0&portrait=0'.($auto?'&autoplay=1':'').'" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}
		return $html;
	}
	
	/**
	 * 移动端 评论星星
	 *
	 * @param: $rating[int]	评论平均数
	 * @return string
	 */
	public static function mobile_review_star($rating, $class=''){
		$html='';
		
		$html.='<span class="review_star '.$class.'">';
		for($i=1; $i<6; ++$i){
			if($i<=$rating){
				$html.='<span class="star_1"></span>';
			}else{
				$html.='<span class="star_0"></span>';
			}
		}
		$html.='</span>';
		return $html;
	}
	
	/**
	 * 移动端 面包屑
	 *
	 * @param: $location[string]	面包屑后面接上的内容
	 * @return string
	 */
	public static function mobile_crumb($location){
		$html='';
		
		$html.='<div class="crumb clean">';
			$html.='<a href="/"><span class="icon_crumb_home"></span></a>'.$location;
		$html.='</div>';
		return $html;
	}
	
	/**
	 * 移动端 购物车的引导向标
	 *
	 * @param: $number[int]	引导数值  0:Checkout 1:Payment 2:Complete
	 * @return string
	 */
	public static function mobile_cart_step($number){
		global $c;
		$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');
		$style_data=str::json_data($StyleData, 'decode');
		$html='';
		
		$html="<style type=\"text/css\">\r\n";
			$html.=".cart_step>div.current{background-color:".$style_data['FontColor'].";}\r\n";
			$html.=".cart_step>div.current>i{border-color:transparent transparent transparent ".$style_data['FontColor'].";}\r\n";
		$html.='</style>';
		
		$html.='<div class="cart_step clean">';
			for($i=0; $i<3; ++$i){
				$html.='<div class="step_'.$i.($number==$i?' current':'').'">'.($i>0?'<em></em>':'').$c['lang_pack']['mobile']['step_ary'][$i].($i<2?'<i></i>':'').'</div>';
			}
		$html.='</div>';
		return $html;
	}

	/**
	 * 单页a标签
	 *
	 * @param: $row[array]	单页纪录
	 * @return string
	 */
	public static function article_a($row, $content=''){
		global $c;
		$url=ly200::get_url($row, 'article');
		$name=$row['Title'.$c['lang']];
		$nofollow=$row['Url'] ? 'rel="nofollow" target="_blank"' : '';
		$html="<a href=\"{$url}\" {$nofollow} title=\"{$name}\">{$content}{$name}</a>";
		return $html;
	}

	/**
	 * Logo
	 *
	 * @param: $row[array]	单页纪录
	 * @return string
	 */
	public static function website_h1($type='logo', $text='', $attr='', $logo='', $url=''){
		global $c;
		$logo_path=$logo?$logo:$c['config']['global']['LogoPath']; //logo 图片
		$url=$url?$url:'/'; //logo 地址
		$html='';
		$IsH1=0;
		$m=$_GET['m'];
		$a=$_GET['a'];
		if($m=='products' || $m=='article' || ($m=='blog' && $a=='detail')) $IsH1=1; //产品 单页 博客详细页面H1不放logo
		if($type=='logo'){
			$IsH1 || $html.='<h1 '.$attr.' >';
			$html.='<a href="'.$url.'"><img src="'.$logo_path.'" alt="'.$c['config']['global']['SiteName'].'" /></a>';
			$IsH1 || $html.='</h1>';
		}else{
			$IsH1 && $html.='<h1 '.$attr.' >';
			$html.=$text;
			$IsH1 && $html.='</h1>';
		}
		return $html;
	}
}
?>