<?php
//============================================================+
// File name   : tcpdf_config.php
// Begin       : 2004-06-11
// Last Update : 2013-05-16
//
// Description : Example of alternative configuration file for TCPDF.
// Author      : Nicola Asuni - Tecnick.com LTD - www.tecnick.com - info@tecnick.com
// License     : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// -------------------------------------------------------------------
// Copyright (C) 2004-2013  Nicola Asuni - Tecnick.com LTD
//
// This file is part of TCPDF software library.
//
// TCPDF is free software: you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// TCPDF is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with TCPDF.  If not, see <http://www.gnu.org/licenses/>.
//
// See LICENSE.TXT file for more information.
//============================================================+

/**
 * Example of alternative configuration file for TCPDF.
 * @author Nicola Asuni
 * @package com.tecnick.tcpdf
 * @version 4.9.005
 * @since 2004-10-27
 */

/**
 * Define the following constant to ignore the default configuration file.
 */
define ('K_TCPDF_EXTERNAL_CONFIG', true);

/**
 * Installation path (/var/www/tcpdf/).
 * By default it is automatically calculated but you can also set it as a fixed string to improve performances.
 */
//define ('K_PATH_MAIN', '');

/**
 * URL path to tcpdf installation folder (http://localhost/tcpdf/).
 * By default it is automatically set but you can also set it as a fixed string to improve performances.
 */
//define ('K_PATH_URL', '');

/**
 * Path for PDF fonts.
 * By default it is automatically set but you can also set it as a fixed string to improve performances.
 */
//define ('K_PATH_FONTS', K_PATH_MAIN.'fonts/');

/**
 * Default images directory.
 * By default it is automatically set but you can also set it as a fixed string to improve performances.
 */
define ('K_PATH_IMAGES', dirname(__FILE__).'/../images/');

/**
 * Deafult image logo used be the default Header() method.
 * Please set here your own logo or an empty string to disable it.
 * 页眉的LOGO图，图片路径对应上面的常量K_PATH_IMAGES
 */
define ('PDF_HEADER_LOGO', 'logo.jpg');

/**
 * Header logo image width in user units.
 * 页眉的LOGO图的宽度，这个不是30px，单位是下面设置的常量PDF_UNIT
 */
define ('PDF_HEADER_LOGO_WIDTH', 30);

/**
 * Cache directory for temporary files (full path).
 * 临时文件的缓存目录（完整路径）。
 */
define ('K_PATH_CACHE', sys_get_temp_dir().'/');

/**
 * Generic name for a blank image.
 * 空白图像的通用名称。
 */
define ('K_BLANK_IMAGE', '_blank.png');

/**
 * Page format.
 * 页面格式，默认设置是A4
 */
define ('PDF_PAGE_FORMAT', 'A4');

/**
 * Page orientation (P=portrait, L=landscape).
 * 页面方向纵向，横向
 */
define ('PDF_PAGE_ORIENTATION', 'P');

/**
 * Document creator.
 * 文档创建者
 */
define ('PDF_CREATOR', 'Ueeshop');

/**
 * Document author.
 * 文档作者
 */
define ('PDF_AUTHOR', 'Ueeshop');

/**
 * Header title.
 * 页眉标题
 */
define ('PDF_HEADER_TITLE', 'Ueeshop Example');

/**
 * Header description string.
 * 页眉字符串
 */
define ('PDF_HEADER_STRING', "by Ueeshop - www.ueeshop.com");

/**
 * Document unit of measure [pt=point, mm=millimeter, cm=centimeter, in=inch].
 * 单位
 */
define ('PDF_UNIT', 'mm');

/**
 * Header margin.
 * 页眉间距
 */
define ('PDF_MARGIN_HEADER', 5);

/**
 * Footer margin.
 * 页脚间距
 */
define ('PDF_MARGIN_FOOTER', 10);

/**
 * Top margin.
 * 上间距
 */
define ('PDF_MARGIN_TOP', 10);

/**
 * Bottom margin.
 * 下间距
 */
define ('PDF_MARGIN_BOTTOM', 15);

/**
 * Left margin.
 * 左间距
 */
define ('PDF_MARGIN_LEFT', 10);

/**
 * Right margin.
 * 右间距
 */
define ('PDF_MARGIN_RIGHT', 10);

/**
 * Default main font name.
 * 默认主体字体，目前仅stsongstdlight字体支持中文，换其他字体可能会导致乱码，后期发现其他字体支持中文请添加备注
 */
define ('PDF_FONT_NAME_MAIN', 'stsongstdlight');

/**
 * Default main font size.
 * 默认主体字体大小
 */
define ('PDF_FONT_SIZE_MAIN', 10);

/**
 * Default data font name.
 * 默认数据字体
 */
define ('PDF_FONT_NAME_DATA', 'stsongstdlight');

/**
 * Default data font size.
 * 默认数据字体大小
 */
define ('PDF_FONT_SIZE_DATA', 8);

/**
 * Default monospaced font name.
 * 默认等宽字体，目前仅stsongstdlight字体支持中文，换其他字体可能会导致乱码，后期发现其他字体支持中文请添加备注
 */
define ('PDF_FONT_MONOSPACED', 'stsongstdlight');

/**
 * Ratio used to adjust the conversion of pixels to user units.
 * PDF图像比例
 */
define ('PDF_IMAGE_SCALE_RATIO', 1.25);

/**
 * Magnification factor for titles.
 * 头部放大
 */
define('HEAD_MAGNIFICATION', 1.1);

/**
 * Height of cell respect font height.
 * 单元格高度，依赖字体高度
 */
define('K_CELL_HEIGHT_RATIO', 1.25);

/**
 * Title magnification respect main font size.
 * 标题放大率，依赖字体大小
 */
define('K_TITLE_MAGNIFICATION', 1.3);

/**
 * Reduction factor for small font.
 * 小字体缩小系数
 */
define('K_SMALL_RATIO', 2/3);

/**
 * Set to true to enable the special procedure used to avoid the overlappind of symbols on Thai language.
 * 设置为true以启用用于避免泰语符号重叠的特殊过程。
 */
define('K_THAI_TOPCHARS', true);

/**
 * If true allows to call TCPDF methods using HTML syntax
 * IMPORTANT: For security reason, disable this feature if you are printing user HTML content.
 * 如果为true则允许使用HTML语法调用TCPDF方法
 */
define('K_TCPDF_CALLS_IN_HTML', true);

/**
 * If true and PHP version is greater than 5, then the Error() method throw new exception instead of terminating the execution.
 * 如果为true且PHP版本大于5，则Error（）方法抛出新异常而不是终止执行。
 */
define('K_TCPDF_THROW_EXCEPTION_ERROR', false);

//============================================================+
// END OF FILE
//============================================================+
