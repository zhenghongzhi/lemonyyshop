<?php
require_once('tcpdf.php');
class CUS_PDF extends TCPDF
{
	function Header() //设定页眉
	{
		$this->SetFont('stsongstdlight','',10);
		$this->Write(10,'IPv6协议一致性测试报告','',false,'C');

		$this->Ln(20);
	}

	function Footer() //设定页脚
	{
		$this->SetY(-15);
		$this->SetFont('stsongstdlight','',10);
		$this->Cell(0,10,'申请时间：' .date('Y-m-d', time()) ,0,0,'R');
	}
}
