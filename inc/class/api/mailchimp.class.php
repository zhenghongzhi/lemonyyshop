<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class mailchimp_api{
	public static function authorization_complete(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		if($p_Data){
			$Data=str::json_data(stripslashes($p_Data), 'decode');
			if($Data['AccessToken'] && $Data['dc'] && $Data['Email']){
				$MailChimpOauthAry=array(
					'access_token'	=>	$Data['AccessToken'],
					'dc'			=>	$Data['dc'],
					'email'			=>	$Data['Email'],
					'api_endpoint'	=>	$Data['ApiEndpoint'],
				);
				$MailChimpOauthData=str::json_data($MailChimpOauthAry);
				$data=array(
					'Value'=>addslashes($MailChimpOauthData)
				);
				if(!db::get_row_count('config', 'GroupId="MailChimp" and Variable="MailChimpOauth"')){
					$data['GroupId']='MailChimp';
					$data['Variable']='MailChimpOauth';
					db::insert('config', $data);
				}else{
					db::update('config', 'GroupId="MailChimp" and Variable="MailChimpOauth"', $data);
				}
				ly200::e_json('', 1);
			}
		}
		ly200::e_json('', 0);
	}
}
?>