<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class stationgroup_api{
	public static function verification(){
		global $c;
		ly200::e_json('', 1);
	}
	
	public static function overview(){
		global $c;
		$_POST=str::str_code($_POST, 'addslashes');
		include($c['root_path'].'manage/static/do_action/account.php');
		$c['cache_timeout']=3600*2;//更新接口缓存文件间隔(s)
		$c['manage']['config']['ManageLanguage']='zh-cn';
		$c['manage']['web_lang']='_'.db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');	//网站的默认语言版本
		$c['manage']['lang_pack']=@include($c['root_path']."manage/static/lang/{$c['manage']['config']['ManageLanguage']}.php");//后台语言包
		$c['manage']['currency_symbol']=db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Symbol');	//后台默认的币种符号
		$c['manage']['resize_ary']=array('products'=>array('default', '640x640', '500x500', '240x240'));
		$data=account_module::get_account_data_class();
		foreach((array)$data['products']['sales'] as $k => $v){
			$data['products']['sales'][$k]['picture']=((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')?'https://':'http://').$_SERVER['HTTP_HOST'].'/'.$v['picture'];
		}
		$data_post=array(
			'Action'	=>	'ueeshop_analytics_get_visits_data',
			'Compare'	=>	0,
			'Terminal'	=>	0,
			'TimeE'		=>	'',
			'TimeS'		=>	$_POST['TimeS'],
		);
		$result=ly200::api($data_post, $c['ApiKey'], $c['api_url']);
		if($result['msg']['browser_charts']){
			$data['browser_charts']=$result['msg']['browser_charts'];
			$data['browser_all_charts']=$result['msg']['browser_all_charts'];	
		}
		ly200::e_json($data, 1);
	}
	
	public static function traffic_statistics(){
		global $c;
		$_POST=str::str_code($_POST, 'addslashes');
		$data=array();
		//visits
		$data_visits=array(
			'Action'	=>	'ueeshop_analytics_get_visits_data',
			'Compare'	=>	0,
			'Terminal'	=>	0,
			'TimeE'		=>	'',
			'TimeS'		=>	$_POST['TimeS'],
		);
		$result_visits=ly200::api($data_visits, $c['ApiKey'], $c['api_url']);
		$data['visits']=$result_visits['msg'];
		//referrer
		$data_referrer=array(
			'Action'	=>	'ueeshop_analytics_get_visits_referrer_data',
			'Compare'	=>	0,
			'Terminal'	=>	0,
			'TimeE'		=>	'',
			'TimeS'		=>	$_POST['TimeS'],
			'lang'		=>	'zh-cn',
		);
		$result_referrer=ly200::api($data_referrer, $c['ApiKey'], $c['api_url']);
		$data['referrer']=$result_referrer['msg'];
		//country
		$data_country=array(
			'Action'	=>	'ueeshop_analytics_get_visits_country_data',
			'Compare'	=>	0,
			'Terminal'	=>	0,
			'TimeE'		=>	'',
			'TimeS'		=>	$_POST['TimeS'],
		);
		$result_country=ly200::api($data_country, $c['ApiKey'], $c['api_url']);
		$data['country']=$result_country['msg'];
		ly200::e_json($data, 1);
	}
	
	public static function traffic_conversion(){
		global $c;
		$_POST=str::str_code($_POST, 'addslashes');
		$TimeS=$_POST['TimeS'];
		$_POST=array(
			'Action'	=>	'ueeshop_analytics_get_visits_conversion_data',
			'TimeS'		=>	$TimeS,
			'TimeE'		=>	'',
			'Terminal'	=>	0,
			'Compare'	=>	0,
		);
		include($c['root_path'].'manage/static/do_action/mta.php');
		$data=mta_module::get_visits_conversion_data();
	}
	
	public static function order_statistics(){
		global $c;
		$_POST=str::str_code($_POST, 'addslashes');
		$TimeS=$_POST['TimeS'];
		unset($_POST);
		$_POST=array(
			'Action'	=>	'ueeshop_analytics_get_order_visits_data',
			'TimeS'		=>	$TimeS,
			'TimeE'		=>	'',
			'Terminal'	=>	0,
			'Compare'	=>	0,
			'lang'		=>	'zh-cn',
		);
		include($c['root_path'].'manage/static/do_action/mta.php');
		$c['manage']['currency_symbol']=db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Symbol');	//后台默认的币种符号
		$c['manage']['config']['ManageLanguage']='zh-cn';
		$c['manage']['lang_pack']=@include($c['root_path']."manage/static/lang/{$c['manage']['config']['ManageLanguage']}.php");//后台语言包
		$data=mta_module::get_orders_data();
	}
}
?>