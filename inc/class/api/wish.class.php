<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class wish_api{
	/*************************************************************************************************************
	同步产品
	*************************************************************************************************************/
	public static function sync_products_goods(){//产品信息同步
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$table='products_wish';
		$attr_table='products_wish_attribute';
		
		$GoodsInfo=str::str_code(str::str_code(str::json_data(@gzuncompress(base64_decode($p_Goods['info'])), 'decode'), 'stripslashes'), 'addslashes');
		$AttrInfo=str::str_code(str::str_code(str::json_data(@gzuncompress(base64_decode($p_Goods['attr'])), 'decode'), 'stripslashes'), 'addslashes');
		if($GoodsInfo['product_id']){//Wish产品ID存在
			$where="product_id='{$GoodsInfo['product_id']}'";
			if($ProId=db::get_value($table, $where, 'ProId')){
				db::update($table, $where, $GoodsInfo);
			}else{
				//获取产品图片开始
				$resize_ary=array('default', '640x640', '500x500', '240x240');
				$config_row=str::str_code(db::get_all('config', "GroupId='global' and (Variable='IsWater' or Variable='IsThumbnail')"));
				$cfg=$ImgPath=array();
				foreach($config_row as $v){$cfg[$v['Variable']]=$v['Value'];}
				
				$ImagePath=array($GoodsInfo['main_image']);//主图
				if($GoodsInfo['extra_images']){//次图
					$extra_images=@explode('|', $GoodsInfo['extra_images']);
					foreach((array)$extra_images as $v){
						$ImagePath[]=$v;
					}
				}
				
				foreach((array)$ImagePath as $k=>$v){
					if($v){
						$ImgPath[]=wish::save_wish_images($v);
					}
				}
				if($cfg['IsWater']){//更新水印图片
					foreach((array)$ImgPath as $v1){
						$water_ary=array($v1);
						$ext_name=file::get_ext_name($v1);
						@copy($c['root_path'].$v1.".default.{$ext_name}", $c['root_path'].$v1);//覆盖大图
						if($cfg['IsThumbnail']){//缩略图加水印
							img::img_add_watermark($v1);
							$water_ary=array();
						}
						foreach($resize_ary as $v2){
							if($v1=='default') continue;
							$size_w_h=explode('x', $v2);
							$resize_path=img::resize($v, $size_w_h[0], $size_w_h[1]);
						}
						foreach((array)$water_ary as $v2){
							img::img_add_watermark($v2);
						}
					}
				}
				foreach((array)$ImgPath as $v1){
					$ext_name=file::get_ext_name($v1);
					foreach($resize_ary as $v2){
						if(!is_file($c['root_path'].$v1.".{$v2}.{$ext_name}")){
							$size_w_h=explode('x', $v2);
							$resize_path=img::resize($v1, $size_w_h[0], $size_w_h[1]);
						}
					}
					if(!is_file($c['root_path'].$v1.".default.{$ext_name}")){
						@copy($c['root_path'].$v1, $c['root_path'].$v1.".default.{$ext_name}");
					}
				}
				$GoodsInfo['PicPath_0']=$ImgPath[0];
				$GoodsInfo['PicPath_1']=$ImgPath[1];
				$GoodsInfo['PicPath_2']=$ImgPath[2];
				$GoodsInfo['PicPath_3']=$ImgPath[3];
				$GoodsInfo['PicPath_4']=$ImgPath[4];
				$GoodsInfo['PicPath_5']=$ImgPath[5];
				$GoodsInfo['PicPath_6']=$ImgPath[6];
				$GoodsInfo['PicPath_7']=$ImgPath[7];
				$GoodsInfo['PicPath_8']=$ImgPath[8];
				$GoodsInfo['PicPath_9']=$ImgPath[9];
				//获取产品图片结束
				
				db::insert($table, $GoodsInfo);
				$ProId=db::get_insert_id();
			}
			//产品属性记录
			$AttrInfo['ProId']=$ProId;
			db::get_row_count($attr_table, $where, 'AttributeId')?db::update($attr_table, $where, $AttrInfo):db::insert($attr_table, $AttrInfo);
			//更新任务完成度
			db::update('products_sync_task', "Platform='wish' and TaskId='{$p_TaskId}'", array('TaskStatus'=>1, 'CompletionRate'=>(int)$p_CompletionRate));
		}
		ly200::e_json('', 1);
	}

	/*************************************************************************************************************
	同步产品任务完成
	*************************************************************************************************************/
	public static function sync_products_complete(){//产品信息同步完成
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		db::update('products_sync_task', "Platform='{$p_ApiName}' and TaskId='{$p_TaskId}'", array(
				'TaskStatus'	=>	$p_TaskStatus,
				'CompletionRate'=>	$p_CompletionRate,
				'ReturnData'	=>	$p_ConditionInfo
			)
		);
	}
}
?>