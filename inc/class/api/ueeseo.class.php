<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class ueeseo_api{
	public static function verification(){
		global $c;
		$result=array(
			'WebType'	=>	0,
			'ApiKey'	=>	str::str_crypt($c['ApiKey'])
		);
		db::insert_update('config', "GroupId='global' and Variable='UeeSeo'", array('GroupId'=>'global', 'Variable'=>'UeeSeo', 'Value'=>1));
		ly200::e_json($result, 1);
	}
	
	public static function article_add(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$p_Contents=str::str_content_crypt(str::str_code($p_Contents, 'stripslashes'));
		if(!$p_Title || !$p_Contents) ly200::e_json('Title or Contents is empty!');
		$data=array(
			'Title' 			=>	$p_Title,
			'PicPath'			=>	$p_Images,
			'SeoTitle' 			=>	$p_Title,
			'SeoKeyword' 		=>	$p_Keywords?$p_Keywords:$p_Title,
			'SeoDescription' 	=>	$p_Title,
			'AccTime'			=>	$c['time'],
		);
		db::insert('blog', $data);
		$AId=db::get_insert_id();
		$bolg_row=db::get_one('blog', "AId='{$AId}'");
		$data=array(
			'AId'		=>	$AId,
			'Content'	=>	addslashes(str_replace('{keyword_url}', '/', $p_Contents)),
		);
		db::insert('blog_content', $data);
		$data=array(
			'Id'	=> $AId,
			'Url'	=> ly200::get_url($bolg_row, 'blog'),
		);
		ly200::e_json($data, 1);
	}
	
	public static function links_add(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$links_row=str::json_data(db::get_value('config', "GroupId='swap_chain' and Variable='ueeseo'", 'Value'), 'decode');
		$links_row[(int)$p_ListId]=array(
			'Keyword'	=>	$p_Keywords,
			'Url'		=>	$p_Url,
		);
		$links_row=addslashes(str::json_data(str::str_code($links_row, 'stripslashes')));
		manage::config_operaction(array('ueeseo'=>$links_row), 'swap_chain');
		ly200::e_json('', 1);
	}
	
	public static function links_del(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$links_row=str::json_data(db::get_value('config', "GroupId='swap_chain' and Variable='ueeseo'", 'Value'), 'decode');
		unset($links_row[(int)$p_ListId]);
		$links_row=str::json_data($links_row);
		manage::config_operaction(array('ueeseo'=>$links_row), 'swap_chain');
		ly200::e_json('', 1);
	}


	public static function get_orders_inquery_statistics_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		if($p_Type=='total'){
			$TotalOrders=db::get_row_count('orders');
			ly200::e_json(array('count'=>$TotalOrders), 1);
		}

		$time_s=where::time($p_TimeS);
		$w="OrderTime between {$time_s[0]} and {$time_s[1]}";
		
		$time_step=86400;
		$time_format=array('%m/%d', 'm/d');
		$orders_row=db::get_all('orders', $w.' group by t', "count(OrderId) as total,FROM_UNIXTIME(OrderTime, '{$time_format[0]}') as t", 't asc');
		$orders_ary=array();
		foreach($orders_row as $v){$orders_ary[$v['t']]=$v['total'];}
		
		
		for($i=$time_s[0]; $i<$time_s[1]; $i+=$time_step){
			$key=date($time_format[1], $i);
			$series[]=(int)$orders_ary[$key];
		}
		ly200::e_json($series, 1);
	}

	public static function get_orders_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$page_count=(int)$p_page_count;
		!$page_count && $page_count=10;
		$status=array(
			1	=>	'等待支付',//Awaiting Payment
			2	=>	'等待确认支付',//Awaiting Confirm Payment
			3	=>	'支付出错',//Payment Wrong
			4	=>	'等待发货',//Awaiting Shipping
			5	=>	'已发货',//Shipment Shipped
			6	=>	'已完成',//Received
			7	=>	'已取消',//Cancelled
		);
		
		$time_s=where::time($p_TimeS);
		$w="OrderTime between {$time_s[0]} and {$time_s[1]}";
		$p_Keywords!='' && $w.=" and (OId like '%$p_Keywords%' or Email like '%$p_Keywords%' or ShippingFirstName like '%$p_Keywords%' or ShippingLastName like '%$p_Keywords%' or ShippingCountry like '%$p_Keywords%')";
		$row=db::get_limit_page('orders', $w, '*', 'OrderTime desc', (int)$p_page, $page_count);
		
		$all_currency_ary=array();
		$ManageSymbol='';//db::get_value('currency', 'IsUsed=1 and ManageDefault=1', 'Symbol');
		$currency_row=db::get_all('currency', '1', 'Currency, Symbol, Rate');
		foreach($currency_row as $k=>$v){
			$all_currency_ary[$v['Currency']]=$v;
			($v['IsUsed']==1 && $v['ManageDefault']==1) && $ManageSymbol=$v['Symbol'];
		}
		
		$data=array(
			'orders'		=>	array(),
			'total'			=>	$row[1],
			'page'			=>	$row[2],
			'total_pages'	=>	$row[3],
		);
		foreach($row[0] as $v){
			$isFee=($v['OrderStatus']>=4 && $v['OrderStatus']!=7)?1:0;
			$total_price=orders::orders_price($v, $isFee, 1);
			
			$v['ManageCurrency'] && $Symbol=$all_currency_ary[$v['ManageCurrency']]['Symbol'];
			!$Symbol && $Symbol=$ManageSymbol;
			$data['orders'][]=array(
				'OId'			=>	$v['OId'],
				'Client'		=>	(int)$v['Source'],
				'Email'			=>	$v['Email'],
				'Name'			=>	$v['ShippingFirstName'].' '.$v['ShippingLastName'],
				'Country'		=>	$v['ShippingCountry'],
				'TotalPrice'	=>	$Symbol.sprintf('%01.2f', $total_price),
				'OrderStatus'	=>	$status[$v['OrderStatus']],
				'OrderTime'		=>	@date('Y-m-d H:i:s', $v['OrderTime']),
				'PaymentMethod'	=>	$v['PaymentMethod'],
				'PayTime'		=>	$v['PayTime']?@date('Y-m-d H:i:s', $v['PayTime']):'N/A'
			);
		}
		
		ly200::e_json($data, 1);
	}
	
	public static function get_nav_data(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		
		$c['lang']='_'.db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');
		$allcate_row=str::str_code(db::get_all('products_category', ' IsSoldOut=0', "CateId,UId,Category{$c['lang']},SubCateCount",  $c['my_order'].'CateId asc'));
		$allcate_ary=array();
		foreach((array)$allcate_row as $k=>$v){
			$allcate_ary[$v['UId']][]=$v;
			$cateid_cate_ary[$v['CateId']]=$v;
		}

		$nav_row=db::get_value('config', "GroupId='themes' and Variable='NavData'", 'Value');
		$nav_data=str::json_data($nav_row, 'decode');
		$ProductsListStatus=0;
		foreach((array)$nav_data as $k=>$v){
			($nav['Nav']==3 && $nav['Url']=='/products/') && $ProductsListStatus=1;
		}
		
		//导航栏
		$data=array();
		foreach((array)$nav_data as $k=>$v){
			$nav=ly200::nav_style($v, 1);
			unset($nav['Target']);
			if($ProductsListStatus==1 && $nav['Nav']==3 && $nav['UId']!='0,') continue;//有产品列表导航，过滤分类导航
			if(!$nav['Name'] || $nav['Nav']=='' || in_array($nav['Nav'], array(4,8,9,10,11,12))) continue;
			$data[$k]=$nav;

			if((($nav['Nav']==3 && $nav['UId'] && count($allcate_ary[$nav['UId']])) || ($nav['Nav']==1 && db::get_row_count('article',"CateId='{$nav['Page']}'")))){//只有产品和单页有下拉
				$data[$k]['Select']=1;
				if($nav['Nav']==3){
					$data[$k]['Id']=category::get_CateId_by_UId($nav['UId']);
					$data[$k]['data']=$allcate_ary[$nav['UId']];
					!count((array)$allcate_ary[$nav['UId']]) && $value['Select']=0;
					foreach((array)$allcate_ary[$nav['UId']] as $key=>$pro){
						$pro['Url']=ly200::get_url($pro, 'products_category');
						$data[$k]['data'][$key]=array(
							'Id'	=>	$pro['CateId'],
							'Name'	=>	$pro['Category'.$c['lang']],
							'Url'	=>	$pro['Url']
						);
					}
				}else{
					$article_nav_row=db::get_all('article', "CateId='{$nav['Page']}'", "AId,Title{$c['lang']},PageUrl,Url", $c['my_order'].'AId desc');
					$data[$k]['Url']='';
					!count($article_nav_row) && $value['Select']=0;
					foreach((array)$article_nav_row as $key=>$art){
						$data[$k]['data'][$key]=array(
							'Id'	=>	$art['AId'],
							'Name'	=>	$art['Title'.$c['lang']],
							'Url'	=>	ly200::get_url($art, 'article')
						);
					}
				}
			}
		}

		ly200::e_json($data, 1);
	}
	
	public static function get_tkd(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Nav=(int)$p_Nav;
		$Id=(int)$p_Id;
		!in_array($Nav, array(0,1,3,5,6,7)) && ly200::e_json('请勿非法操作');
		
		$table='meta';
		$where='';
		$Nav==0 && $where='Type="home"';
		$Nav==5 && $where='Type="tuan"';
		$Nav==6 && $where='Type="seckill"';
		$Nav==7 && $where='Type="blog"';
		if($Nav==1){
			$table='article';
			$where="AId='{$Id}'";
		}elseif($Nav==3){
			$table=$Id?'products_category':'meta';
			$where=$Id?"CateId='{$Id}'":'Type="products"';
		}
		$lang='_'.db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');
		$row=db::get_one($table, $where, "SeoTitle{$lang},SeoKeyword{$lang},SeoDescription{$lang}");
		$SeoAry=array(
			'Nav'			=>	$Nav,
			'Id'			=>	$Id,
			'SeoTitle'		=>	$row['SeoTitle'.$lang],
			'SeoKeyword'	=>	$row['SeoKeyword'.$lang],
			'SeoDescription'=>	$row['SeoDescription'.$lang]
		);
		
		ly200::e_json($SeoAry, 1);
	}
	
	public static function set_tkd(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$Nav=(int)$p_Nav;
		$Id=(int)$p_Id;
		!in_array($Nav, array(0,1,3,5,6,7)) && ly200::e_json('请勿非法操作');
		
		$lang='_'.db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');
		$SeoAry=array(
			'SeoTitle'.$lang		=>	$p_SeoTitle,
			'SeoKeyword'.$lang		=>	$p_SeoKeyword,
			'SeoDescription'.$lang	=>	$p_SeoDescription
		);
		if(in_array($Nav, array(0,5,6,7)) || ($Nav==3 && !$Id)){
			$navAry=array(0=>'home',3=>'products',5=>'tuan',6=>'seckill',7=>'blog');
			$SeoAry['Type']=$navAry[$Nav];
			db::insert_update('meta', "Type='{$navAry[$Nav]}'", $SeoAry);
		}elseif($Nav==1){
			db::update('article', "AId='{$Id}'", $SeoAry);
		}elseif($Nav==3){
			db::update('products_category', "CateId='{$Id}'", $SeoAry);
		}
		
		ly200::e_json($SeoAry, 1);
	}
	
	public static function web_page_count(){
		global $c;
		$data['page_count']=1;
		//产品分类数量
		$data['page_count']+=db::get_row_count('products_category');
		//产品数量
		$data['page_count']+=db::get_row_count('products');
		//文章数量
		$data['page_count']+=db::get_row_count('blog');
		
		ly200::e_json($data, 1);
	}

	public static function set_seo_hidden(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if(db::get_row_count('config', 'GroupId="global" and Variable="UeeSeoIsHidden"')){
			db::update('config', 'GroupId="global" and Variable="UeeSeoIsHidden"', array('Value'=>(int)$p_Status));
		}else{
			db::insert('config', array(
				'GroupId'	=>	'global',
				'Variable'	=>	'UeeSeoIsHidden',
				'Value'		=>	(int)$p_Status,
			));
		}
		ly200::e_json('', 1);
	}
}
?>