<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class wish{
    /******************************************************** access_token 设置(start) *****************************************************************/
	public static function set_default_authorization($AId=0){//设置默认速卖通账号
		global $c;
		
		$Account=$_SESSION['Manage']['Wish']['Account'];
		$w="Platform='wish'";
		if(!(int)$AId && db::get_row_count('authorization', "{$w} and Account='{$Account}'")) return $Account;
		
		(int)$AId && $w.=" and AId='$AId'";
		$row=db::get_one('authorization', $w, '*', 'AId asc');
		if(!$row['AId']) return;
		$_SESSION['Manage']['Wish']['AuthorizationId']=$row['AId'];
		$_SESSION['Manage']['Wish']['Account']=$row['Account'];
		return $row['Account'];
	}
	/******************************************************** access_token 设置(end) *****************************************************************/
    
    
	public static function save_wish_images($WishImage){//将Wish图片保存到本地
		global $c;
		if(!$WishImage) return;//图片为空

		$resize_ary=array('default', '640x640', '500x500', '240x240');
		$save_dir='/u_file/'.date('ym/').'products/'.date('d/');
		file::mk_dir($save_dir);
		
		$WishName=file::get_base_name($WishImage);//图片名称
		$UeeshopImagesUrl=db::get_value('products_wish_images', "WishName='{$WishName}'", 'UeeshopImagesUrl');//检查此图片是否曾保存到本地
		if(@is_file($c['root_path'].$UeeshopImagesUrl)){//此图片已下载过
			$ext_name=file::get_ext_name($UeeshopImagesUrl);
			$PicPath=$save_dir.str::rand_code().'.'.$ext_name;
			@copy($c['root_path'].$UeeshopImagesUrl.".default.{$ext_name}", $c['root_path'].$PicPath);//复制原图
		}else{//下载图片
			$PicPath=file::write_file($save_dir, str::rand_code().'.jpg', ly200::curl($WishImage));
			@copy($c['root_path'].$PicPath, $c['root_path'].$PicPath.".default.jpg");//保留原图
			db::insert('products_wish_images', array(
					'WishName'			=>	$WishName,
					'WishImageUrl'		=>	$WishImage,
					'UeeName'			=>	@str_replace('/', '-', trim($PicPath, '/')),
					'UeeshopImagesUrl'	=>	$PicPath
				)
			);
		}
		
		return $PicPath;
	}
	
	public static function remove_wish_images($Images){//删除Wish图片记录表
		global $c;
		$UeeName=@str_replace('/', '-', trim($Images, '/'));
		db::delete('products_wish_images', "UeeName='{$UeeName}'");
	}
}
?>