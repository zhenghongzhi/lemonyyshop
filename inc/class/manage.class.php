<?php
/*
Powered by ly200.com		http://www.ly200.com
广州联雅网络科技有限公司		020-83226791
*/

class manage{
	public static function language($html){
		global $c;
		$replace=array();
		preg_match_all("/{\/(.*)\/}/isU", $html, $lang_ary);
		foreach($lang_ary[1] as $v){
			$replace[0][]="{/$v/}";
			$replace[1][]=manage::get_language($v);
		}
		return str_replace($replace[0], $replace[1], $html);
	}
	
	public static function get_language($language){
		global $c;
		return @eval('return $c[\'manage\'][\'lang_pack\'][\''.str_replace('.', '\'][\'', $language).'\'];');
	}
	
	public static function check_upfile($file){
		!substr_count($file, '/u_file') && $file='';
		return $file;
	}
	
	public static function check_tmp($file){
		!substr_count($file, '/tmp') && $file='';
		return $file;
	}
	
	public static function config_operaction($cfg, $global){
		global $c;
		foreach($cfg as $key=>$value){
			$where="GroupId='$global' and Variable='$key'";
			if(db::get_row_count('config', $where)){
				db::update('config', $where, array('Value'=>$value));
			}else{
				db::insert('config', array(
						'GroupId'	=>	$global,
						'Variable'	=>	$key,
						'Value'		=>	$value
					)
				);
			}
		}
	}
	
	public static function operation_log($Logs){
		global $c;
		if($_SESSION['Manage']['UserId']==-1){return;}
		$data='';
		if($_GET){
			$get_data=@array_filter($_GET);
			foreach($get_data as $k=>$v){
				substr_count(strtolower($k), 'password') && $get_data[$k]='<font color=red>removed</font>';
			}
			$data.='GET='.addslashes(str::json_data(str::str_code($get_data, 'stripslashes')));
		}
		if($_POST){
			$post_data=@array_filter($_POST);
			foreach($post_data as $k=>$v){
				substr_count(strtolower($k), 'password') && $post_data[$k]='<font color=red>removed</font>';
			}
			$data.=($data?"\n":'').'POST='.addslashes(str::json_data(str::str_code($post_data, 'stripslashes')));
		}
		$do_action_ary=@explode('.', isset($_POST['do_action'])?$_POST['do_action']:$_GET['do_action']);
		db::insert('manage_operation_log', array(
				'UserId'	=>	$_SESSION['Manage']['UserId'],
				'UserName'	=>	addslashes($_SESSION['Manage']['UserName']),
				'Module'	=>	array_shift($do_action_ary),
				'Ip'		=>	ly200::get_ip(),
				'Log'		=>	addslashes($Logs),
				'Data'		=>	$data,
				'AccTime'	=>	$c['time']
			)
		);
	}
	
	public static function custom_operation_log($Logs, $Ary){
		global $c;
		if($_SESSION['Manage']['UserId']==-1){return;}
		$data='';
		if($Ary){
			$Ary=@array_filter($Ary);
			$data.='DATA='.addslashes(str::json_data(str::str_code($Ary, 'stripslashes')));
		}
		$do_action_ary=@explode('.', isset($_POST['do_action'])?$_POST['do_action']:$_GET['do_action']);
		db::insert('manage_operation_log', array(
				'UserId'	=>	$_SESSION['Manage']['UserId'],
				'UserName'	=>	addslashes($_SESSION['Manage']['UserName']),
				'Module'	=>	array_shift($do_action_ary),
				'Ip'		=>	ly200::get_ip(),
				'Log'		=>	addslashes($Logs),
				'Data'		=>	$data,
				'AccTime'	=>	$c['time']
			)
		);
	}

	public static function ueeshop_web_get_service_data(){
		global $c;
		if(!db::get_row_count('config', 'GroupId="global" and Variable="UpdateVersion"')){
			db::insert('config', array('GroupId'=>'global', 'Variable'=>'UpdateVersion', 'Value'=>0));
			$UpdateVersion=0;
		}else{
			$UpdateVersion=(int)db::get_value('config', 'GroupId="global" and Variable="UpdateVersion"', 'Value');
		}
		if(!$UpdateVersion){
			include($c['root_path'].'/tmp/notice/UpdateVersion_'.$c['manage']['config']['ManageLanguage'].'.html');
		}else{
			if(!file::check_cache($c['root_path'].'/tmp/cache/service_data_'.$c['manage']['config']['ManageLanguage'].'.html', 0)){
				$analytics_row=ly200::ueeshop_web_get_data();//加载统计json
				$data=array(
					'trial'		=>	(int)$analytics_row['IsCustomer'],
					'spread'	=>	$analytics_row['Spread'],
					'server'	=>	$analytics_row['Server'],
					'expired'	=>	$analytics_row['ExpiredTime'],
					'backup'	=>	$analytics_row['BackupTime'],
					'service'	=>	str_replace('】【', '】<br/>【', $analytics_row['Service']),
				);
				/* //测试数据
				$data=array(
					'trial'		=>	1,
					'spread'	=>	'Ueeseo推广平台正在测试中，即将上线，敬请期待！',
					'server'	=>	'正式服务器（香港）',
					'expired'	=>	'2019-01-26',
					'backup'	=>	'2018-02-07 00:00:00',
					'service'	=>	array(
						'Contacts'	=>	'Angal',
						'Email'	=>	'<a href=\'mailto:800031052@qq.com\'>800031052@qq.com</a>',
						'QQ'	=>	'<a href=\"http://crm2.qq.com/page/portalpage/wpa.php?uin=800031052&aty=0&a=0&curl=&ty=1\" target=\"_blank\">800031052</a>',
						'WorkTime'	=>	'【周一至周五9:00至18:00】<br />【周六9:00至17:00】',
						'Telephone'	=>	'020-83226791',
						'Wechat'	=>	'13922455822',
						'Complain'	=>	'13922455822',
						),
				);*/
				ob_start();
					$html='';
					if($data['expired']){
						$html='
						<li class="menu_agent menu_down">
							<a href="javascript:;" class="user_service" title="'.$c['manage']['lang_pack']['account']['customer_service'].'"></a>
							<div class="message_info">
								<div class="agent_box drop_down">
									<div class="agent_menu">
										<a href="javascript:;" class="cur">售后</a>
										<a href="javascript:;">消息</a>
									</div>
									<div class="msg_box agent_list">';
										//<div class="pic"><img src="" alt=""></div>
										if($data['service']['Contacts']){
											$html.='
											<div class="msg">
												<div class="name">'.$data['service']['Contacts'].'</div>
												<div class="email">'.$data['service']['Email'].'</div>
											</div>';
										}
										$html.='
										<div class="clear"></div>
										<ul>
											<li>
												<div class="tit">'.$c['manage']['lang_pack']['global']['support'][0].'</div>
												<div class="desc">'.$data['service']['QQ'].'</div>
											</li>
											<li>
												<div class="tit">'.$c['manage']['lang_pack']['global']['support'][2].'</div>
												<div class="desc">'.$data['service']['Telephone'].'</div>
											</li>
											<li>
												<div class="tit">'.$c['manage']['lang_pack']['global']['support'][3].'</div>
												<div class="desc">'.$data['service']['Wechat'].'</div>
											</li>
											<li>
												<div class="tit">'.$c['manage']['lang_pack']['global']['support'][4].'</div>
												<div class="desc">'.$data['service']['Complain'].'</div>
											</li>
											<li>
												<div class="tit">'.$c['manage']['lang_pack']['global']['support'][5].'</div>
												<div class="desc">'.$data['expired'].'</div>
											</li>
										</ul>
										<div class="work_time">'.$data['service']['WorkTime'].'</div>
									</div>
									<div class="logs_box agent_list" style="display: none;">
										<ul>
											<li><a rel="nofollow" title="">万圣节节日模板上线啦！</a></li>
											<li><a rel="nofollow" title="">UeeShop 4.0 全新升级</a></li>
											<li><a rel="nofollow" title="">UeeShop3月大规模更新</a></li>
											<li><a rel="nofollow" title="">UeeShop携手Facebook的第一场...</a></li>
											<li><a rel="nofollow" title="">UeeShop 2018年1月功能更新优化</a></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
						';
					}
					echo $html;
				$cache_contents=ob_get_contents();
				ob_end_clean();
				file::write_file(ly200::get_cache_path('', 0), 'service_data_'.$c['manage']['config']['ManageLanguage'].'.html', $cache_contents);
			}
			include($c['root_path'].'/tmp/cache/service_data_'.$c['manage']['config']['ManageLanguage'].'.html');
		}
	}

	public static function email_log($Email, $Subject, $Body){
		global $c;
		if($_SESSION['Manage']['UserId']==-1){return;}
		$time=$c['time'];
		foreach((array)$Email as $k=>$v){
			db::insert('email_log', array(
					'Email'		=>	$v,
					'Subject'	=>	addslashes($Subject),
					'Body'		=>	addslashes($Body),
					'AccTime'	=>	$time
				)
			);
		}
	}
	
	public static function google_translation($text, $target='', $source=''){ //google翻译接口
		global $c;
		if($target==''){return $text;}
		$source=='' && $source=$c['manage']['language_default'];
		$data=array(
			'ApiName'	=>	'google',
			'Action'	=>	'translation',
			'Text'		=>	$text,
			'Source'	=>	$source,
			'Target'	=>	$target
		);
		return ly200::api($data, $c['ApiKey'], $c['sync_url']);
	}
	
	public static function time_between($StartTime, $EndTime){ //输出时间范围的格式
		if(date('H:i:s', $StartTime)=='00:00:00' && date('H:i:s', $EndTime)=='00:00:00'){
			$format='Y-m-d';
			$separator=' ~ ';
		}elseif(date('s', $StartTime)=='00' && date('s', $EndTime)=='00'){
			$format='Y-m-d H:i';
			$separator='<br>~<br>';
		}else{
			$format='Y-m-d H:i:s';
			$separator='<br>~<br>';
		}
		return date($format, $StartTime).$separator.date($format, $EndTime);
	}
	
	public static function iconv_price($price, $method=0, $currency=''){
		global $c;
		$currency_row=array();
		$currency!='' && $currency_row=db::get_one('currency', "Currency='{$currency}'");//设置固定货币
		!$currency_row && $currency_row=$_SESSION['Manage']['Currency'];//未设置默认货币，选择网站默认货币
		$rate=(float)$currency_row['Rate'];
		$Symbol=$currency_row['Symbol'];
		if($method==0){
			return $Symbol.sprintf('%01.2f', $price*$rate);
		}elseif($method==1){
			return $Symbol;
		}else{
			return sprintf('%01.2f', $price*$rate);
		}
	}
	
	public static function range_price($row, $method=0){
		global $c;
		$CurPrice=$row['Price_1'];
		$is_wholesale=($row['Wholesale'] && $row['Wholesale']!='[]');
		if($is_wholesale){
			$wholesale_price=str::json_data(htmlspecialchars_decode($row['Wholesale']), 'decode');
			foreach($wholesale_price as $k=>$v){
				if($row['MOQ']<$k) break;
				$CurPrice=(float)$v;
			}
			$maxPrice=reset($wholesale_price);
			$minPrice=end($wholesale_price);
		}
		if($row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime']){
			if($row['PromotionType']){
				$CurPrice=$row['Price_1']*($row['PromotionDiscount']/100);
			}else $CurPrice=$row['PromotionPrice'];
		}
		if($is_wholesale){
			$CurPrice>$maxPrice && $maxPrice=$CurPrice;
			$CurPrice<$minPrice && $minPrice=$CurPrice;
		}
		if($is_wholesale && !$method){
			return $c['manage']['currency_symbol'].cart::iconv_price($minPrice, 2).' - '.cart::iconv_price($maxPrice, 2);
		}elseif($is_wholesale && $method){
			return manage::iconv_price($minPrice);
		}elseif(!$is_wholesale || $method){
			return manage::iconv_price($CurPrice);
		}
	}
	
	/**
	 * 价格格式显示(固定汇率)
	 *
	 * @param: $price[float]		价格
	 * @param: $method[int]			0 符号+价格，1 符号，2 价格
	 * @param: $currency[string]	货币缩写
	 * @param: $symbol[string]		货币符号
	 * @param: $rate[float]			汇率
	 * @param: $show[int]			0 不已货币格式显示，1 货币格式显示
	 * @return string
	 */
	public static function rate_price($price, $method=0, $currency='', $symbol='', $rate=0, $show=1){
		$return='';
		if($method==0){
			$return=(float)substr(sprintf('%01.3f', $price*$rate), 0, -1);
			$show && $return=cart::currency_format($return, 0, $currency);
			$return=$symbol.$return;
		}elseif($method==1){
			$return=$symbol;
		}else{
			$return=(float)substr(sprintf('%01.3f', $price*$rate), 0, -1);
			$show && $return=cart::currency_format($return, 0, $currency);
		}
		return $return;
	}
	
	/*
	 * 多语言版本表单输出
	 * 类型：0=>'text', 1=>'textarea', 2=>'editor', 3=>'editor_simple'
	 * 添加字段配置：请查看$c['manage']['field_ext']
	 * 参数：(数据(object)，类型(string)，字段名(string)，内容长度(integer)，内容上限(integer)，额外内容(string))
	 * 输出：html
	 */
	public static function form_edit($row, $type='text', $name, $size=0, $max=0, $attr=''){
		global $c;
		$result='';
		foreach($c['manage']['config']['Language'] as $k=>$v){
			if(substr($name, -2, 2)=='[]'){
				$field_name=substr($name, 0, -2).'_'.$v;
				$value=isset($row[$field_name])?$row[$field_name]:'';
				$field_name.='[]';
			}else{
				$field_name=$name.'_'.$v;
				$value=isset($row[$field_name])?$row[$field_name]:'';
			}
			$result.='<div class="tab_txt tab_txt_'.$v.'" '.($c['manage']['config']['LanguageDefault']==$v?'style="display:block;"':'').' lang="'.$v.'">';
			if($type=='text'){
				$value=htmlspecialchars(htmlspecialchars_decode($value), ENT_QUOTES);
				$result.='<input type="text" name="'.$field_name.'" value="'.$value.'" class="box_input" size="'.$size.'" '.($max ? 'maxlength="'.$max.'" ':'').$attr.' />';
				$attr=='notnull' && $result.=' <font class="fc_red">*</font>';
			}elseif($type=='textarea'){
				$result.='<textarea name="'.$field_name.'" class="box_textarea" '.$attr.'>'.$value.'</textarea>';
			}elseif($type=='editor'){
				$result.='<div class="fl"></div>'.manage::Editor($field_name, $value);
			}else{
				$result.='<div class="fl"></div>'.manage::Editor_Simple($field_name, $value, $size, $attr);
			}
			$result.='</div>';
		}
		return $result;
	}
	
	public static function unit_form_edit($row, $type='text', $name, $size=0, $max=0, $attr=''){
		global $c;
		$result='';
		$lang_count=count($c['manage']['config']['Language']);
		$lang_count==1 && $size=$size+7;//单一个语言
		array_unshift($c['manage']['config']['Language'], $c['manage']['config']['LanguageDefault']);
		$c['manage']['config']['Language']=array_unique($c['manage']['config']['Language']);
		foreach($c['manage']['config']['Language'] as $k=>$v){
			if(substr($name, -2, 2)=='[]'){
				$field_name=substr($name, 0, -2).'_'.$v;
				$value=isset($row[$field_name])?$row[$field_name]:'';
				$field_name.='[]';
			}else{
				$field_name=$name.'_'.$v;
				$value=isset($row[$field_name])?$row[$field_name]:'';
			}
			$isDefault=($c['manage']['config']['LanguageDefault']==$v?1:0);
			$result.='<div class="lang_txt lang_txt_'.$v.'"'.($isDefault?' style="display:block;"':'').($isDefault?' data-default="1"':'').' lang="'.$v.'">';
			if($type=='text'){
				$value=htmlspecialchars(htmlspecialchars_decode($value), ENT_QUOTES);
				$result.='<span class="unit_input"'.($attr=='notnull'?' parent_null':'').'>'.($lang_count>1 ? '<b>{/language.'.$v.'/}</b>' : '').'<input type="text" class="box_input" name="'.$field_name.'" value="'.$value.'" size="'.$size.'" '.($max ? 'maxlength="'.$max.'" ':'').($attr=='notnull'?' notnull parent_null="1"':$attr).' /></span>';
				$attr=='notnull' && $result.=' <font class="fc_red">*</font>';
			}elseif($type=='textarea'){
				$value=htmlspecialchars(htmlspecialchars_decode($value), ENT_QUOTES);
				$result.='<span class="unit_input unit_textarea" parent_null>'.($lang_count>1 ? '<b>{/language.'.$v.'/}</b>' : '').'<textarea name="'.$field_name.'" cols="'.$size.'" class="box_textarea"'.($attr=='notnull'?' notnull parent_null="1"':$attr).'>'.$value.'</textarea></span>';
			}
			$result.='</div>';
		}
		return $result;
	}

	/*
	 * 更新数据库多语言版本操作
	 * 参数：(表名(string)，执行条件(string)，字段(array))
	 * 输出：null
	 */
	public static function database_language_operation($table, $where, $input_field){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$column_ary=db::get_table_fields($table, 1); //获取数据表的所有字段名称
		$data=array();
		foreach($c['manage']['config']['Language'] as $k=>$v){
			foreach($input_field as $k2=>$v2){
				$field_name=$k2.'_'.$v;
				$data[$field_name]=${'p_'.$field_name};
				if(!in_array($field_name, $column_ary)){
					$f=$c['manage']['field_ext'][$v2];
					db::query("alter table {$table} add {$field_name} {$f} after {$k2}_en");
				}
			}
		}
		db::update($table, $where, $data);
	}
	
	/*
	 * 开启语言版，增加对应语言版的字段
	 * 参数：(表名(string)，执行条件(string)，字段(array))
	 * 输出：null
	 */
	public static function turn_on_language_database_operation($p_LanguageDefault, $p_Language){
		global $c;
		$langs=$c['manage']['config']['Language'];
		$diff=@array_diff($p_Language, $langs);
		$default=@in_array($p_LanguageDefault, $diff)?$c['manage']['config']['LanguageDefault']:$p_LanguageDefault;
		if(!count($diff)) return;
		$tables=$c['manage']['table_lang_field'];//有多语言字段的表
		foreach($tables as $tb=>$field){
			$column_ary=db::get_table_fields($tb, 1); //获取数据表的所有字段名称
			$update_sql='';
			foreach($diff as $k=>$v){
				foreach($field as $k2=>$v2){
					$field_name=$k2.'_'.$v;
					if(!in_array($field_name, $column_ary)){
						$update_sql.=($update_sql!=''?',':'')."`$field_name`=`{$k2}_{$default}`";
						$f=$c['manage']['field_ext'][$v2];
						db::query("alter table {$tb} add {$field_name} {$f} after {$k2}_en");
					}elseif($tb=='products_attribute' &&  $k2=='Value'){
						$update_sql.=($update_sql!=''?',':'')."`$field_name`=`{$k2}_{$default}`";
					}
				}
			}
			$update_sql!='' && db::query("UPDATE $tb SET $update_sql");
		}
	}
	
	/**
	 * Ckeditor编辑器生成
	 *
	 * @param: $name[string]	提交字段名
	 * @param: $content[string]	内容
	 * @param: $imgbank[boolen]	是否加载图片银行
	 * @return html
	 */
	public static function Editor($name, $content='', $imgbank=true){
		global $c;
		$html='';
		if($name){
			$html .= "<textarea id='{$name}' name='{$name}'>".htmlspecialchars_decode($content)."</textarea>";
			$html .= '<script type="text/javascript">';
			$html .= "CKEDITOR.replace('{$name}', {'language':'".$c['manage']['config']['ManageLanguage']."'});";
			$html .= '</script>';
		}
		return $html;
	}
	
	public static function Editor_Simple($name, $content='', $width='', $height=300){
		global $c;
		$html='';
		if($name){
			$html .= "<textarea id='{$name}' name='{$name}'>".htmlspecialchars_decode($content)."</textarea>";
			$html .= '<script type="text/javascript">';
			$html .= "CKEDITOR.replace('{$name}', {'toolbar':'simple', 'height':'".$height."', 'width':'".($width?$width:'100%')."', 'language':'".$c['manage']['config']['ManageLanguage']."'});";
			$html .= '</script>';
		}
		return $html;
	}
	
	public static function turn_page($row_count, $page, $total_pages, $query_string){	//翻页
		if(!$row_count){return;}
		$str="<div class='page'><div class='cur'>$page/$total_pages</div>";
		if($total_pages>1){
			$step=array(1, $total_pages>=200?2:0, $total_pages>=500?5:0, $total_pages>=1000?10:0, $total_pages>=2000?20:0, $total_pages>=5000?50:0);
			$str.='<ul>';
			for($i=max($step); $i<=$total_pages; $i+=max($step)){
				$str.="<li><a href='$query_string$i'>$i/$total_pages</a></li>";
			}
			$str.='</ul>';
		}
		$str.='</div>';
		$pre=$page>1?$page-1:1;
		$next=$page+1>$total_pages?$total_pages:$page+1;
		$str.="<a href='$query_string$pre' class='page_item pre'></a>";
		$str.="<a href='$query_string$next' class='page_item next'></a>";
		return $str;
	}
	
	public static function update_permit($UserName){
		global $c;
		if(!$UserName){return;}
		$details_permit=array();
		foreach($c['manage']['permit'] as $k=>$v){
			$DetailsPermit='';
			//一级权限
			if(@in_array($k, $c['manage']['permit_base'])) continue;
			$permit=(int)$_POST['Permit_'.$k];
			if($v){
				//二级权限
				foreach((array)$v as $k2=>$v2){
					$details_permit[$k][$k2][0]=(int)$_POST['Permit_'.$k.'_'.$k2];
					if(isset($v2['menu'])){
						//三级权限
						foreach((array)$v2['menu'] as $v3){
							$details_permit[$k][$k2][1][$v3][0]=(int)$_POST['Permit_'.$k.'_'.$k2.'_'.$v3];
							if($v2['permit'][$v3]){//三级操作权限
								foreach((array)$v2['permit'][$v3] as $v4){
									$details_permit[$k][$k2][1][$v3][1][$v4][0]=(int)$_POST['Permit_'.$k.'_'.$k2.'_'.$v3.'_'.$v4];
								}
							}
						}
					}elseif(!isset($v2['menu']) && isset($v2['permit'])){
						//三级操作权限
						foreach((array)$v2['permit'] as $v3){
							$details_permit[$k][$k2][1][$v3][0]=(int)$_POST['Permit_'.$k.'_'.$k2.'_'.$v3];
						}
					}
				}
			}
			$details_permit[$k] && $DetailsPermit=addslashes(str::json_data(str::str_code($details_permit[$k], 'stripslashes')));
			if((int)db::get_row_count('manage_permit', "UserName='$UserName' and Module='$k'")){
				db::update('manage_permit', "UserName='$UserName' and Module='$k'", array('Permit'=>$permit, 'DetailsPermit'=>$DetailsPermit));
			}else{
				db::insert('manage_permit', array(
						'UserName'		=>	$UserName,
						'Module'		=>	$k,
						'Permit'		=>	$permit,
						'DetailsPermit'	=>	$DetailsPermit
					)
				);
			}
		}
	}
	
	public static function check_permit($module, $type=0, $details=array()){//权限检测，$details(a=>$_GET['a'], d=>$_GET['d'], p=>$_GET['p'])
		global $c;
		$data=$_SESSION['Manage']['Permit'][$module];
		$len=count($details);
		if((int)$_SESSION['Manage']['GroupId']!=1 && !in_array($module, $c['manage']['permit_base'])){
			if(!$len && !(int)$data[0]){//判断一级
				return manage::no_permit($type);//无权限
			}elseif($details['a'] && $len==1 && !(int)$data[1][$details['a']][0]){//判断二级
				return manage::no_permit($type);
			}elseif($details['d'] && $len==2 && !(int)$data[1][$details['a']][1][$details['d']][0]){//判断三级
				return manage::no_permit($type);
			}elseif($details['p'] && $len==3 && !(int)$data[1][$details['a']][1][$details['d']][1][$details['p']][0]){//判断四级
				return manage::no_permit($type);
			}
		}
		
		unset($data);
		return true;
	}
	
	public static function no_permit($type){//权限检测
		global $c;
		if($type==0){
			return false;
		}else{
			exit($c['manage']['lang_pack']['manage']['manage']['no_permit']);
		}
	}
	
	public static function html_tab_button($class='', $lang=''){
		global $c;
		$default=$c['manage']['config']['LanguageDefault'];
		$lang && $default=$lang;
		$html='';
		$html.='<dl class="tab_box_row'.(count($c['manage']['config']['Language'])==1?' hide':'').($class?" {$class}":'').'">';
			$html.="<dt><span>{$c['manage']['lang_pack']['language'][$default]}</span><i></i></dt>";
			$html.='<dd class="drop_down">';
			foreach($c['manage']['config']['Language'] as $k=>$v){
				$html.='<a class="tab_box_btn item"'.($default==$v ? ''/* style="display:none;"*/:'').' data-lang="'.$v.'"><span>'.$c['manage']['lang_pack']['language'][$v].'</span><i></i></a>';
			}
			$html.='</dd>';
			$html.='<dd class="tips">'.$c['manage']['lang_pack']['error']['supplement_lang'].'</dd>';
		$html.='</dl>';
		return $html;
	}
	
	public static function config_edit($config){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		if((int)$config['Effects']){ //特效方式
			$data['Effects']=$p_Effects;
		}
		if((int)$config['HeaderContent']){ //首页内容
			$HeaderContentAry=array();
			foreach($c['manage']['config']['Language'] as $k=>$v){
				for($i=0;$i<$config['HeaderContent'];++$i){
					$num = (int)$config['HeaderContent']>1 ? '_'.$i : '';
					$HeaderContentAry['HeaderContent'.$num.'_'.$v]=${'p_HeaderContent'.$num.'_'.$v};
				}
			}
			$HeaderContentData=addslashes(str::json_data(str::str_code($HeaderContentAry, 'stripslashes')));
			$data['HeaderContent']=$HeaderContentData;
		}
		if((int)$config['IndexContent']){ //首页内容
			$IndexContentAry=array();
			foreach($c['manage']['config']['Language'] as $k=>$v){
				for($i=0;$i<$config['IndexContent'];++$i){
					$num = (int)$config['IndexContent']>1 ? '_'.$i : '';
					$IndexContentAry['IndexContent'.$num.'_'.$v]=${'p_IndexContent'.$num.'_'.$v};
				}
			}
			$IndexContentData=addslashes(str::json_data(str::str_code($IndexContentAry, 'stripslashes')));
			$data['IndexContent']=$IndexContentData;
		}
		if((int)$config['TopMenu']){ //顶部栏目
			$TopMenuAry=array();
			foreach((array)$p_TopUrl as $k=>$v){
				foreach($c['manage']['config']['Language'] as $k2=>$v2){
					$TopMenuAry[$k]["TopName_{$v2}"]=${'p_TopName_'.$v2}[$k];
				}
				$TopMenuAry[$k]['TopUrl']=$v;
				$TopMenuAry[$k]['TopNewTarget']=$p_TopNewTarget[$k];
			}
			$TopMenuData=addslashes(str::json_data(str::str_code($TopMenuAry, 'stripslashes')));
			$data['TopMenu']=$TopMenuData;
		}
		if((int)$config['ContactMenu']){ //联系我们
			$ContactMenuData=addslashes(str::json_data(str::str_code($p_ContactMenu, 'stripslashes')));
			$data['ContactMenu']=$ContactMenuData;
		}
		if((int)$config['ShareMenu']){ //顶部栏目
			$ShareMenu=array();
			foreach($c['share'] as $v){
				$ShareMenu[$v]=${'p_Share'.$v};
			}
			$ShareMenuData=addslashes(str::json_data(str::str_code($ShareMenu, 'stripslashes')));
			$data['ShareMenu']=$ShareMenuData;
		}
		if((int)$config['IndexTitle']){ //首页标题
			$data['IndexTitle']=$p_IndexTitle;
		}
		return $data;
	}
	
	public static function option_to_select($select_name, $input_name, $value, $data_ary=array(), $attr, $is_edit=0, $is_delete=0){	//输出成select表单
		global $c;
		$is_edit && $edit=' edit_box';
		$html='';
		$html.='<dl class="down_select_box'.$edit.'">';
			if($is_edit){
				$html.='<dt><input type="text" class="box_input" name="'.$input_name.'" placeholder="'.$c['manage']['lang_pack']['global']['select_write'].'" value="'.$value.'"'.$attr.' /><input type="hidden" name="'.$select_name.'" value="'.$value.'" /></dt>';
			}else{
				$html.='<dt><div class="box_select"><span>'.$c['manage']['lang_pack']['global']['select_index'].'</span><input type="hidden" name="'.$select_name.'" value="'.$value.'" /></div></dt>';
			}
			$html.='<dd class="drop_down"><div class="hscroll">';
				foreach((array)$data_ary as $k=>$v){
					$html.='<div class="select select_1 item" value="'.$k.'">'.$v.($is_delete==1?'<i></i>':'').'</div>';
				}
			$html.='</div></dd>';
		$html.='</dl>';
		return $html;
	}
	
	/**
	 * 下拉 文本框 混合
	 *
	 * @param:	$SelectName[string]	下拉名称	
	 * @param:	$InputName[string]	文本名称
	 * @param:	$DataAry[array]		产品属性
	 * @param:	$Value[object]		数值 (单选:[Select]=>下拉值,[Input]=>文本值,[Type]=>类型值 多选:[Name]=>名称,[Value]=>数值,[Type]=>类型)
	 * @param:	$Type[int]			产品类型 (0:下拉+文本，1:下拉)
	 * @param:	$InputAttr[string]	文本额外参数
	 * @param:	$isCheckbox[int]	是否为多选
	 * @param:	$placeholder[string]默认形式的文字提示
	 * @return	object
	 */
	public static function box_drop_double($SelectName, $InputName, $DataAry=array(), $Value=array(), $Type=0, $InputAttr, $isCheckbox=0, $isMore=0, $placeholder=''){
		global $c;
		$is_edit=0;
		$Type==0 && $is_edit=1;//带有文本
		$Value=(array)$Value;
		$isCheckbox==1 && $is_edit=0;
		$JsonData=htmlspecialchars(str::json_data($DataAry));
		$moreAry=array('value'=>'', 'type'=>'', 'table'=>'', 'top'=>0, 'all'=>0, 'check-all'=>'', 'start'=>0);
		if($isMore==1 && count($DataAry)>0){//开启默认页面显示加载更多
			$ary=current($DataAry);
			if($ary['Type']=='products'){
				$moreAry=array('value'=>'products', 'type'=>'products', 'table'=>'products', 'top'=>0, 'all'=>1, 'check-all'=>0, 'start'=>1);
			}
		}
		$html='';
		$html.='<dl class="box_drop_double'.($is_edit?' edit_box':'').'"'.($isCheckbox?' data-checkbox="1"':'').'>';
			if($isCheckbox){//多选
				$html.='<dt class="box_checkbox_list">';
					$html.='<div class="select_list">';
						if($Value){
							foreach((array)$Value as $k=>$v){
								$html.=html::box_option_button($v['Name'], $v['Value'], $v['Type'], 1, 0, 1, $v['Icon']);
							}
						}
					$html.='</div>';
					$html.='<input type="text" class="box_input check_input" name="'.$InputName.'" value="" size="30" maxlength="255" autocomplete="off" />';
					$html.='<input type="hidden" name="'.$SelectName.'" value="" class="hidden_value" /><input type="hidden" name="'.$SelectName.'Type" value="" class="hisdden_type" />';
				$html.='</dt>';
			}else{
				if($is_edit){//带有文本框
					$html.='<dt><input type="text" class="box_input" name="'.$InputName.'" placeholder="'.($placeholder ? $placeholder : $c['manage']['lang_pack']['global']['select_write']).'" value="'.$Value['Input'].'" autocomplete="off"'.$InputAttr.' /><input type="hidden" name="'.$SelectName.'" value="'.$Value['Select'].'" class="hidden_value" /><input type="hidden" name="'.$SelectName.'Type" value="'.$Value['Type'].'" class="hidden_type" /></dt>';
				}else{//仅下拉
					$html.='<dt><div class="box_select"><span>'.$c['manage']['lang_pack']['global']['select'].'</span><input type="hidden" name="'.$SelectName.'" value="'.$Value['Select'].'" class="hidden_value" /><input type="hidden" name="'.$SelectName.'Type" value="'.$Value['Type'].'" class="hidden_type" /></div></dt>';
				}
			}
			$html.='<dd class="drop_down">';
				$html.='<div class="drop_menu">';
					$html.='<a href="javascript:;" class="btn_back" data-value="" data-type="" data-table="" data-top="0" data-all="0" style="display:none;">'.$c['manage']['lang_pack']['global']['return'].'</a>';
					$html.='<div class="drop_skin" style="display:none;"></div>';
					$html.='<div class="drop_list" data="'.$JsonData.'"></div>';
					$html.='<a href="javascript:;" class="btn_load_more" data-value="'.$moreAry['value'].'" data-type="'.$moreAry['type'].'" data-table="'.$moreAry['table'].'" data-top="'.$moreAry['top'].'" data-all="'.$moreAry['all'].'" data-check-all="'.$moreAry['check-all'].'" data-start="'.$moreAry['start'].'" style="display:'.($isMore==1 && $moreAry?'block':'none').';">'.$c['manage']['lang_pack']['global']['load_more'].'</a>';
				$html.='</div>';
			$html.='</dd>';
		$html.='</dl>';
		return $html;
	}
	
	public static function box_drop_double_option($Name, $Value, $Type, $Table, $Checkbox=0, $Next=0, $IconTxt=''){
		$Html='';
		$Html.='<div class="item'.($Type=='add'?' drop_add':'').($Next?' children':'').'" data-name="'.htmlspecialchars($Name).'" data-value="'.$Value.'" data-type="'.$Type.'" data-table="'.$Table.'">';
		if($Checkbox==1){
			$Html.='<span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="_DoubleOption[]" value="'.$Value.'" /></span></span><span class="item_name">'.($IconTxt?$IconTxt:'').$Name.'</span>';
		}else{
			$Html.='<span>'.($IconTxt?$IconTxt:'').$Name.'</span>';
		}
		$Html.=($Next?'<em></em>':'');
		$Html.='</div>';
		return $Html;
	}

	/**
	 * 下拉 文本框 混合
	 *
	 * @param:	$box_id[string]			图片框ID
	 * @param:	$input_name[string]		保存图片的字段	
	 * @param:	$picpath[string]		图片路径
	 * @param:	$num[int]				用于多图片上传
	 */

	public static function multi_img($box_id, $input_name, $picpath='', $img_size=''){
		global $c;
		$isFile=is_file($c['root_path'].$picpath)?1:0;
		$Html='';
		if($img_size){
			$Html.='<div class="tips">'.str_replace('%s', $img_size, $c['manage']['lang_pack']['notes']['pic_size_tips']).'</div>';
		}
		$Html.='<div class="multi_img upload_file_multi" id="'.$box_id.'">';
			$Html.='<dl class="img '.($isFile ? 'isfile' : '').'" num="0">';
				$Html.='<dt class="upload_box preview_pic">';
					$Html.='<input type="button" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" tips="" />';
					$Html.='<input type="hidden" name="'.$input_name.'" value="'.$picpath.'" data-value="" save="'.$isFile.'" />';
				$Html.='</dt>';
				$Html.='<dd class="pic_btn">';
					$Html.='<a href="javascript:;" class="edit"><i class="icon_edit_white"></i></a>';
					$Html.='<a href="'.($isFile ? $picpath : 'javascript:;').'" class="zoom" target="_blank"><i class="icon_search_white"></i></a>';
					$Html.='<a href="javascript:;" class="del" rel="del"><i class="icon_del_white"></i></a>';
				$Html.='</dd>';
			$Html.='</dl>';
		$Html.='</div>';
		return $Html;
	}

	public static function multi_img_item($input_name, $picpath='', $num ,$s_picpath=''){
		global $c;
		$isFile=is_file($c['root_path'].$picpath)?1:0;
		$Html.='<dl class="img '.($isFile ? 'isfile' : '').'" num="'.$num.'">';
			$Html.='<dt class="upload_box preview_pic">';
				$Html.='<input type="button" class="btn_ok upload_btn" name="submit_button" value="{/global.upload_pic/}" tips="" />';
				$Html.='<input type="hidden" name="'.$input_name.'" value="'.$picpath.'" data-value="'.$s_picpath.'" save="'.$isFile.'" />';
			$Html.='</dt>';
			$Html.='<dd class="pic_btn">';
				$Html.='<a href="javascript:;" class="edit"><i class="icon_edit_white"></i></a>';
				$Html.='<a href="'.($isFile ? $picpath : 'javascript:;').'" class="zoom" target="_blank"><i class="icon_search_white"></i></a>';
				$Html.='<a href="javascript:;" class="del" rel="del"><i class="icon_del_white"></i></a>';
			$Html.='</dd>';
		$Html.='</dl>';
		return $Html;
	}

	public static function sales_right_search_form($m, $a, $d, $ext='', $ext1='', $ext2=''){
		$Html='<div class="search_form">';
			$Html.='<form method="get" action="?" style="border-radius: 4px;">';
				$Html.='<div class="k_input">';
					$Html.='<input type="text" name="Keyword" value="" class="form_input" size="15" autocomplete="off" placeholder="{/global.placeholder/}">';
					$Html.='<input type="button" value="" class="more">';
				$Html.='</div>';
				$Html.='<input type="submit" class="search_btn" value="{/global.search/}">';
				$Html.='<div class="ext drop_down" style="display: none;">';
					$Html.='<div class="rows item">';
						$Html.='<label>{/products.products_category.category/}</label>';
						$Html.='<span class="input">';
							$Html.='<div class="box_select">';
								$Html.=category::ouput_Category_to_Select('CateId', '', 'products_category', 'UId="0,"', '1', '','{/global.select_index/}');
							$Html.='</div>';
						$Html.='</span>';
						$Html.='<div class="clear"></div>';
					$Html.='</div>';
				$Html.='</div>';
				$Html.='<div class="clear"></div>';
				$Html.='<input type="hidden" name="m" value="'.$m.'">';
				$Html.='<input type="hidden" name="a" value="'.$a.'">';
				$Html.='<input type="hidden" name="d" value="'.$d.'">';
				$Html.=$ext;
				$Html.=$ext1;
				$Html.=$ext2;
			$Html.='</form>';
		$Html.='</div>';
		return $Html;
	}

	/**
	 * 自动生成回收表的字段
	 *
	 * @param:	$table				要删除数据的表
	 * @param:	$recycle_table		保存删除数据的表	
	 */
	public static function auto_add_recycle_column($table, $recycle_table){
		global $c;
		$column_ary=db::get_table_fields($table, 0);
		$recycle_column_ary=db::get_table_fields($recycle_table, 1);
		foreach((array)$column_ary as $v){
			if(!in_array($v['Field'], $recycle_column_ary)){
				$v['Null'] == 'NO' && $Null = 'NOT NULL';
				$Default = $v['Default'] == 'NULL' ? 'DEFAULT NULL' : "DEFAULT '{$v['Default']}'";
				db::query("ALTER TABLE `{$recycle_table}` ADD `{$v['Field']}` {$v['Type']} {$Null} {$Default} {$v['Extra']};");
			}
		}
	}

}
?>