<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

/**
 * 亚翔供应链相关类
 */
class asiafly
{
	/**
	 * curl请求
	 * @param  [String]  $url     [请求地址]
	 * @param  integer $method    [请求方式： 0 == GET, 1 == POST]
	 * @param  array   $form_data [POST请求]
	 * @return [array]            [返回响应数据]
	 */
	public static function curl($url, $method = 0, $form_data = array()){
		$curl=@curl_init(); 
		@curl_setopt($curl, CURLOPT_URL, $url);
		@curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		@curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		@curl_setopt($curl, CURLOPT_HEADER, FALSE);
		@curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		@curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/json"));
		@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		if ($method && $form_data) {
			@curl_setopt($curl, CURLOPT_POST, true);
			@curl_setopt($curl, CURLOPT_POSTFIELDS, $form_data);
		}
		$result=@curl_exec($curl);
		$info=@curl_getinfo($curl);
		@curl_close($curl);
		$res=str::json_data($result,'decode');
		return $res;
	}

	/**
	 * [运费试算]
	 * @param  integer $OrderId    [订单ID]
	 * @param  integer $exprectDay [期望的期限]
	 * @param  integer $ProductType [产品类型]
	 * @return [array]             [获取亚翔供应链物流列表]
	 */
	public static function getExpressList($OrderId, $exprectDay, $ProductType){
		global $c;
		// 获取订单信息
		$orders_row = db::get_one('orders', "OrderId='{$OrderId}'", '*');
		$toCountry = db::get_value('country', "CId='{$orders_row['ShippingCId']}'", 'Acronym');
		$form_data = array(
			'toCountry'		=>	$toCountry,
			'weight'		=>	(float)$orders_row['TotalWeight'],
			'wrapType'		=>	"$ProductType", // 1：普货，2：带电，3：敏感
			'exprectDay'	=>	$exprectDay,
			'count'			=>	3
		);
		$form_json = str::json_data($form_data);
		$return = self::curl('http://122.112.168.32:729/api/Customer/CustomerAPI/FreightTrial', 1, $form_json);

		if (empty($return)) {
			// 接口连接错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$c['manage']['lang_pack']['orders']['asiafly']['asiafly_error'],
			);
		} else if (!$return['IsSucceed']) {
			// 接口请求错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$return['Message'],
			);
		} else if ($return['IsSucceed']) {
			$Entity = str::json_data($return['Entity'], 'decode');
			if ($Entity['Code'] == '0000') {
				// 成功
				$data = array(
					'IsSucceed'		=>	1,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
					'Data'			=>	str::json_data($Entity['Obj_Json'], 'decode'),
					'Version'		=>	$Entity['Version'],
				);
			} else {
				// 接口处理错误
				$data = array(
					'IsSucceed'		=>	2,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
				);
			}
		}
		return $data;
	}

	/**
	 * [根据提供的参数，获取标签PDF文件链接，客户单号]
	 * @param  [String] $OrderId     [订单号]
	 * @param  [String] $ProductCode [物流产品代码]
	 * @param  [String] $ProductName [物流产品名]
	 * @return [array]  [获取订单信息提交接口返回数据]
	 */
	public static function getTable($OrderId, $ProductCode, $ProductName){
		// 获取订单数据
		$orders_row = db::get_one('orders', "OrderId='{$OrderId}'", '*');
		// 获取亚翔商户数据
		$AsiaflyData = db::get_value('config', "GroupId='plugins' and Variable='Asiafly'", 'Value');
		$account = str::json_data($AsiaflyData, 'decode');
		
		// 获取国家码
		$Acronym = db::get_value('country', "CId='{$orders_row['ShippingCId']}'", 'Acronym'); 

		// 获取省份
		$State = '';
		if ($orders_row['ShippingState']) {
			$State = $orders_row['ShippingState'];
		} else if($orders_row['ShippingSId']) {
			$State = db::get_value('country_states', "CId='{$orders_row['ShippingCId']}' and SId='{$orders_row['ShippingSId']}'", 'States');
		}

		// 订单总价
		$productsPrice = orders::orders_product_price($orders_row, 1);

		// 获取订单产品列表
		$orders_products_list_row = db::get_limit('orders_products_list', "OrderId='{$OrderId}'", '*', "LId desc", 0, 10);

		$form_data = array(
			'Order'			=>	array(
				'CustomerNo'	=>	$orders_row['OId'], // 客户单号
				'ProductId'		=>	trim($ProductCode), // 物流产品代码
				'ProductName'	=>	trim($ProductName), // 物流产品名
				'FrCompanyName'	=>	'', // 发件人公司名
				'FrName'		=>	'', // 发件人名
				'FrAddress'		=>	'', // 发件人地址
				'FrCountry'		=>	'', // 发件人国家
				'FrProvince'	=>	'', // 发件人省份/州
				'FrCity'		=>	'', // 发件人城市
				'FrTel'			=>	'', // 发件人电话
				'FrFax'			=>	'', // 发件人传真
				'FrZipcode'		=>	'', // 发件人邮编
				'FrEmail'		=>	'', // 发人电子邮件
				'ToCompanyName'	=>	'', // 收件人公司名
				'ToName'		=>	trim($orders_row['ShippingFirstName'].' '.$orders_row['ShippingLastName']), // 收件人名
				'ToAddress'		=>	trim($orders_row['ShippingAddressLine1'].' '.$orders_row['ShippingAddressLine2']), // 收件人地址
				'ToCountry'		=>	$Acronym, // 收件人国家
				'ToProvince'	=>	trim($State), // 收件人省份/州
				'ToCity'		=>	trim($orders_row['ShippingCity']), // 收件人城市
				'ToTel'			=>	trim($orders_row['ShippingCountryCode'].'-'.$orders_row['ShippingPhoneNumber']), // 收件人电话
				'ToFax'			=>	'', // 收件人传真
				'ToZipcode'		=>	trim($orders_row['ShippingZipCode']), // 收件人邮编
				'ToEmail'		=>	trim(urlencode($orders_row['Email'])), // 收件人电子邮件
				'ToCardType'	=>	'', // 收件人证件类型,1：身份证，2：护照，3：其他。
				'ToCardNo'		=>	'', // 收件人证件号
				'ToCardTerm'	=>	'', // 收件人证件有效期
				'CustomerWeight'=>	(float)$orders_row['TotalWeight'], // 收件人证件有效期
				'Package'		=>	1, // 件数
				'Value'			=>	(float)$productsPrice, // 总价值
				'Currency'		=>	$orders_row['Currency'], // 币制
				'MainGoods'		=>	'', // 主要货物品名
				'WrapType'		=>	'1', // 包装类型：1：包裹，2：信件，3：其他
				'AddService'	=>	'', // 附加服务代码
				'InsureAmount'	=>	cart::iconv_price($orders_row['ShippingInsurancePrice'], 2, $orders_row['Currency'], 0), // 投保金额
				'InsureCurrency'=>	$orders_row['Currency'], // 保险币制
				'BuyerId'		=>	'', // 买家id
				'TradeId'		=>	'', // 交易id
				'Remark'		=>	'', // 备注
			),
			'OrderGoodsList'	=>	array(),
			'SignMsg'			=>	'',
			'CustomerCode'		=>	$account['merchantCode']
		);

		foreach((array)$orders_products_list_row as $k=>$v){
			$form_data['OrderGoodsList'][$k]['Sku'] = $v['SKU']; // 商品SKU
			$form_data['OrderGoodsList'][$k]['GoodsName'] = $v['Name']; // 商品品名
			$form_data['OrderGoodsList'][$k]['DeclPrice'] = cart::iconv_price($v['Price'] + $v['PropertyPrice'], 2, $orders_row['Currency'], 0); // 商品申报单价
			$form_data['OrderGoodsList'][$k]['DeclCurrency'] = $orders_row['Currency']; // 商品申报币制
			$form_data['OrderGoodsList'][$k]['DeclCount'] = (int)$v['Qty']; // 商品申报数量
			$form_data['OrderGoodsList'][$k]['DeclUnit'] = ''; // 商品申报单位
			$form_data['OrderGoodsList'][$k]['GoodsNameCn'] = ''; // 商品中文品名
			$form_data['OrderGoodsList'][$k]['SaleUrl'] = ''; // 销售url
			$form_data['OrderGoodsList'][$k]['Hscode'] = ''; // 海关hscode
		}
		// 签名
		
		$SignMsg = self::json_data($form_data['Order']).self::json_data($form_data['OrderGoodsList']).$form_data['CustomerCode'].$account['privateKey'];

		$form_data['SignMsg'] = MD5($SignMsg);
		$form_json = self::json_data($form_data);
		// POST提交数据
		$return = self::curl('http://122.112.168.32:729/api/Customer/CustomerAPI/CreateOrder', 1, $form_json);
		$data = array();
		if(empty($return)){
			// 接口连接错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$c['manage']['lang_pack']['orders']['asiafly']['asiafly_error'],
			);
		} else if (!$return['IsSucceed']) {
			// 接口请求错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$return['Message'],
			);
		} else if ($return['IsSucceed']) {
			$Entity = str::json_data($return['Entity'], 'decode');
			if ($Entity['Code'] == '0000') {
				// 成功
				$data = array(
					'IsSucceed'		=>	1,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
					'Data'			=>	str::json_data($Entity['Obj_Json'], 'decode')
				);
			} else {
				// 接口处理错误
				$data = array(
					'IsSucceed'		=>	2,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
				);
			}
		}
		return $data;
	}

	/**
	 * [从远程服务器上下载文件]
	 * @param  [String] $filepath  [远程服务器文件路径]
	 * @param  [String] $BusinessNo  [亚翔客户单号]
	 * @return 无，文件存在直接下载
	 */
	public static function httpCopy($filepath, $BusinessNo){
		global $c;

        $file = $c['root_path'].'/u_file/'.date('ym/').'asiafly/'.$BusinessNo.'.pdf';
		file::mk_dir('/u_file/'.date('ym/').'asiafly/');
		
		$url = str_replace(" ","%20", $filepath);
		
		if(function_exists('curl_init')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$temp = curl_exec($ch);
			if(@file_put_contents($file, $temp) && !curl_error($ch)) {
				return $file;
			} else {
				return false;
			}
		} else {
			$opts = array(
				"http"=>array(
				"method"=>"GET",
				"header"=>"",
				"timeout"=>60)
			);
			$context = stream_context_create($opts);
			if(@copy($url, $file, $context)) {
				//$http_response_header
				return $file;
			} else {
				return false;
			}
		}
	}

	public static function down_file($filepath, $save_name=''){//下载文件
		!is_file($filepath) && exit();
		$save_name=='' && $save_name=basename($filepath);
		$file_size=filesize($filepath);
		$file_handle=fopen($filepath, 'r');
		ob_clean();//清理缓存，可避免下载文件后，出现乱码的情况
		header("Content-type: application/octet-stream; name=\"$save_name\"\n");
		header("Accept-Ranges: bytes\n");
		header("Content-Length: $file_size\n");
		header("Content-Disposition: attachment; filename=\"$save_name\"\n\n");
		while(!feof($file_handle)){
			echo fread($file_handle, 1024*100);
		}
		fclose($file_handle);
	}

	/**
	 * [解决低版本PHP环境使用str::json_data转json字符串的时候，中文变成unicode编码的问题]
	 * @param  [array/String] $data   [需要编码的字符串]
	 * @param  string $action [需要执行的动作，默认数组转json]
	 * @return [String/array]         [返回转换的结果]
	 */
	public static function json_data($data, $action='encode'){	//json数据编码
		if($action=='encode'){
			if(!function_exists('unidecode')){
				function unidecode($match){
					return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
				}
			}
			return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'unidecode', json_encode($data));
		}else{
			return (array)json_decode($data, true);
		}
	}

	/**
	 * [获取物流轨迹]
	 * @return [array] [返回物流轨迹查询日志]
	 */
	public static function getTrack($BusinessNo){
		global $c;
		// 获取亚翔商户数据
		$AsiaflyData = db::get_value('config', "GroupId='plugins' and Variable='Asiafly'", 'Value');
		$account = str::json_data($AsiaflyData, 'decode');

		$form_data = array(
			'CustomerNo'		=>	'',
			'BusinessNo'		=>	$BusinessNo, // 'bn000000008116'
			'CustomerCode'		=>	$account['merchantCode'],
			'SignMsg'			=>	'',
		);
		$form_data['SignMsg'] = md5($form_data['CustomerNo'].$form_data['BusinessNo'].$form_data['CustomerCode'].$account['privateKey']);
		$form_json = str::json_data($form_data);
		$return = self::curl('http://122.112.168.32:729/api/Customer/CustomerAPI/QueryOrderRoute', 1, $form_json);
		$data = array();
		if(empty($return)){
			// 接口连接错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$c['manage']['lang_pack']['orders']['asiafly']['asiafly_error'],
			);
		} else if (!$return['IsSucceed']) {
			// 接口请求错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$return['Message'],
			);
		} else if ($return['IsSucceed']) {
			$Entity = str::json_data($return['Entity'], 'decode');
			if ($Entity['Code'] == '0000') {
				// 成功
				$data = array(
					'IsSucceed'		=>	1,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
					'Data'			=>	str::json_data($Entity['Obj_Json'], 'decode')
				);
			} else {
				// 接口处理错误
				$data = array(
					'IsSucceed'		=>	2,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
				);
			}
		}
		return $data;
	}
	
	/**
	 * [获取物流产品信息接口]
	 * @return 
	 */
	public static function get_products_list()
	{
		// 获取亚翔商户数据
		$AsiaflyData = db::get_value('config', "GroupId='plugins' and Variable='Asiafly'", 'Value');
		$account = str::json_data($AsiaflyData, 'decode');

		$form_data = array(
			'CustomerCode'	=>	$account['merchantCode'],
			'SignMsg'		=>	'',
		);

		$form_data['SignMsg'] = md5($form_data['CustomerCode'].$account['privateKey']);
		$form_json = str::json_data($form_data);
		$return = self::curl('http://122.112.168.32:729/api/Customer/CustomerAPI/GetProductList', 1, $form_json);

		$data = array();
		$data = array();
		if(empty($return)){
			// 接口连接错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$c['manage']['lang_pack']['orders']['asiafly']['asiafly_error'],
			);
		} else if (!$return['IsSucceed']) {
			// 接口请求错误
			$data = array(
				'IsSucceed'		=>	0,
				'Message'		=>	$return['Message'],
			);
		} else if ($return['IsSucceed']) {
			$Entity = str::json_data($return['Entity'], 'decode');
			if ($Entity['Code'] == '0000') {
				// 成功
				$data = array(
					'IsSucceed'		=>	1,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
					'Data'			=>	str::json_data($Entity['Obj_Json'], 'decode')
				);
			} else {
				// 接口处理错误
				$data = array(
					'IsSucceed'		=>	2,
					'Message'		=>	$return['Message'],
					'Code'			=>	$Entity['Code'],
					'Msg'			=>	$Entity['Msg'],
					'Count'			=>	$Entity['Obj_Count'],
				);
			}
		}
		return $data;
	}

	/**
	 * [返回输入数组中某个单一列的值 array_column 低PHP版本的实现]
	 * @param  [array] $input     [输入数组]
	 * @param  [String] $columnKey [需要获取的列名]
	 * @param  [type] $indexKey  [可选。作为返回数组的索引/键的列。]
	 * @return [array]            [返回列值]
	 */
	public static function i_array_column($input, $columnKey, $indexKey=null){
	    if(!function_exists('array_column')){ 
	        $columnKeyIsNumber  = (is_numeric($columnKey))?true:false; 
	        $indexKeyIsNull            = (is_null($indexKey))?true :false; 
	        $indexKeyIsNumber     = (is_numeric($indexKey))?true:false; 
	        $result                         = array(); 
	        foreach((array)$input as $key=>$row){ 
	            if($columnKeyIsNumber){ 
	                $tmp= array_slice($row, $columnKey, 1); 
	                $tmp= (is_array($tmp) && !empty($tmp))?current($tmp):null; 
	            }else{ 
	                $tmp= isset($row[$columnKey])?$row[$columnKey]:null; 
	            } 
	            if(!$indexKeyIsNull){ 
	                if($indexKeyIsNumber){ 
	                  $key = array_slice($row, $indexKey, 1); 
	                  $key = (is_array($key) && !empty($key))?current($key):null; 
	                  $key = is_null($key)?0:$key; 
	                }else{ 
	                  $key = isset($row[$indexKey])?$row[$indexKey]:0; 
	                } 
	            } 
	            $result[$key] = $tmp; 
	        } 
	        return $result; 
	    }else{
	        return array_column($input, $columnKey, $indexKey);
	    }
	}


}
?>