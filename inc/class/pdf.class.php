<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class pdf{

	/**
     * 生成pdf
     * @param  $info    array   
     *   array(
	 *          'title'=>'文档标题' , 
	 *          'subject'=>'文档主题' , 
	 *          'keywords'=>'文档关键字' , 
	 *          'content'=>'文档正文内容' , 
	 *          'path'=>'文档保存路径',
	 *          'file_name'=>'文档保存文档名称' , 
	 *      );
     * @return null  
     */
	public static function createPDF($info = array())
	{
		global $c;

		// 引入常量配置文件
		require_once($c['root_path'].'/inc/class/pdf/config/tcpdf_config.php');

		// 引入TCPDF主体方法文件
		require_once($c['root_path'].'/inc/class/pdf/cus_pdf.php');

		/**
		 * 创建TCPDF对象
		 * @param PDF_PAGE_ORIENTATION ：文档方向
		 * @param PDF_UNIT : 文档单位
		 * @param PDF_PAGE_FORMAT ： 文档页面大小
		 * @param 是否使用unicode
		 * @param 字符编码
		 * @var Object TCPDF
		 */
		$pdf = new CUS_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8');


		// 设置文档创建者
		$pdf->SetCreator(PDF_CREATOR);

		// 设置文档作者
		$pdf->SetAuthor(PDF_AUTHOR);

		// 设置文档标题
		$pdf->SetTitle($info['title']);

		// 设置文档主题
		$pdf->SetSubject($info['subject']);

		// 设置文档关键字
		$pdf->SetKeywords($info['keywords']);


		/**
		 * 设置文档页眉
		 * @param PDF_HEADER_LOGO LOGO图
		 * @param PDF_HEADER_LOGO_WIDTH LOGO图宽度
		 * @param PDF_HEADER_TITLE 页眉标题
		 * @param PDF_HEADER_STRING 页眉字符串
		 * @param 文本的rgb颜色
		 * @param 页眉下划线的颜色
		 */
		
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));
		 
	
		// 删除默认页眉/页脚，使用false就是关闭
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter($info['Footer'] ? true : false);


		// 设置页眉和页脚字体
		
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		 
	
		// 设置等宽字体
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// 设置文档间距
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM);
		
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		

		// 设置自动分页
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// 设置图像比例因子
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		$lang = Array();

		// PAGE META DESCRIPTORS --------------------------------------

		$lang['a_meta_charset'] = 'UTF-8';
		$lang['a_meta_dir'] = 'ltr';
		$lang['a_meta_language'] = 'cn';

		// TRANSLATIONS --------------------------------------
		$lang['w_page'] = '页面';
		$pdf->setLanguageArray($lang);

		// ---------------------------------------------------------

		// 设置默认字体子集模式
		$pdf->setFontSubsetting(true);

		// 设置字体，字体大小
		$pdf->SetFont('stsongstdlight', '', 14);

		// 添加页面
		$pdf->AddPage();

		// 设置文字阴影效果
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0, 'depth_h'=>0, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		// 如果打印html文档且包含了css文件，就需要使用writeHTML方法
		$pdf->writeHTML($info['content'], true, false, true, false, '');

		// ---------------------------------------------------------
		// 创建$info['path']文件夹
		$info['path'] && file::mk_dir($info['path']);

		/**
		 * @param 文件名称或者路径
		 * @param I输出到浏览器 F输出到指定路径
		 */
		$pdf->Output(($info['path'] ? $c['root_path'] . $info['path'] : '') . $info['file_name'], 'F');
	}

	/**
	 * 生成Account Application Form.pdf文件
	 * @return 无
	 */
	public static function dhl_account_app_form_pdf()
	{
		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$DHLAccountOpenStep2Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep2"', 'Value');
		$DHLAccountOpenStep2 = str::json_data($DHLAccountOpenStep2Data, 'decode');

		$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
				.lowercase {
					text-transform: lowercase;
				}
				.uppercase {
					text-transform: uppercase;
				}
				.capitalize {
					text-transform: capitalize;
				}
				.first{border:1px solid #000;}
				.first th{font-size:11px;background-color:#999;}
				.first td{font-size:11px;}
				.border_bot{border-bottom:1px solid #000;}
			</style>

			<table cellpadding="0" cellspacing="0" style="border:none;">
				<tr>
					<td width="75%" style="font-size:14px;font-weight:bold;">Section I</td>
					<td width="25%" rowspan="2" align="right"><img src="/inc/class/pdf/images/dhl_logo.png"></td>
				</tr>
				<tr>
					<td style="font-size:14px;font-weight:bold;">Account Applicatio n Form / 账户申请表</td>
				</tr>
			</table>

			<table class="first" cellpadding="0" cellspacing="0">
			    <tr>
			        <th colspan="20">1. Customer Information</th>
			    </tr>
			    <tr>
			    	<th class="border_bot" colspan="20">信息</th>
			    </tr>
			    <tr>
					<td colspan="20">Company Name</td>
			    </tr>
			    <tr>
					<td colspan="2">公司名称</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['CompanyName'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Business Registration No./Company Registration No. </td>
			    </tr>
			    <tr>
					<td colspan="4">营业执照/经营许可证</td>
					<td class="border_bot" colspan="16">' . $DHLAccountOpenStep2['BusinessNumber'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Date of Incorporation</td>
			    </tr>
			    <tr>
					<td colspan="2">注册日期</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['RegisterDate'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Place of Incorporation</td>
			    </tr>
			    <tr>
					<td colspan="2">注册地</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['RefisterAddress'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Company Address</td>
			    </tr>
			    <tr>
					<td colspan="2">公司地址</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['CompanyAddress'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">House No.</td>
					<td colspan="10">Street</td>
			    </tr>
			    <tr>
					<td colspan="2">门牌号</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['HouseNo'] . '</td>
					<td colspan="1">街道</td>
					<td class="border_bot" colspan="9">' . $DHLAccountOpenStep2['Street'] . '</td>
			    </tr>
			    <tr>
					<td colspan="6">Postal Code.</td>
					<td colspan="6">City</td>
					<td colspan="8">Region (State)</td>
			    </tr>
			    <tr>
					<td>邮编</td>
					<td class="border_bot" colspan="5">' . $DHLAccountOpenStep2['ZipCode'] . '</td>
					<td>城市</td>
					<td class="border_bot" colspan="5">' . $DHLAccountOpenStep2['City'] . '</td>
					<td colspan="2">地区 (省份)</td>
					<td class="border_bot" colspan="6">' . $DHLAccountOpenStep2['State'] . '</td>
			    </tr>
			    <tr>
					<td colspan="10">Country</td>
					<td colspan="10">Telephone No</td>
			    </tr>
			    <tr>
					<td>国家</td>
					<td class="border_bot" colspan="9">' . $DHLAccountOpenStep2['Country'] . '</td>
					<td colspan="2">电话号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['Telephone'] . '</td>
			    </tr>
			     <tr>
					<td colspan="10">Website (If Applicable)</td>
					<td colspan="10">Fax Number</td>
			    </tr>
			    <tr>
					<td colspan="2">网址 (选填)</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['Website'] . '</td>
					<td colspan="2">传真号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['FaxNo'] . '</td>
			    </tr>
			    <tr>
					<td colspan="10">Name of Contact Person</td>
					<td colspan="10">Email Address(Name of Contact Person)</td>
			    </tr>
			    <tr>
					<td colspan="2">联系人姓名</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['ContactName'] . '</td>
					<td colspan="4">联系人电子邮件地址</td>
					<td class="border_bot" colspan="6">' . $DHLAccountOpenStep2['ContactEmail'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Mobile Number of Contact Person</td>
			    </tr>
			    <tr>
					<td colspan="3">联系人手机号码</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['ContactMobile'] . '</td>
			    </tr>
			    <tr>
					<td colspan="10">(Implementation) Name of Contact Person</td>
					<td colspan="10">Email Address((Implementation) Name of Contact Person)</td>
			    </tr>
			    <tr>
					<td colspan="4">系统对接联系人姓名</td>
					<td class="border_bot" colspan="6">' . $DHLAccountOpenStep2['SystemContactName'] . '</td>
					<td colspan="5">系统对接联系人电子邮件地址</td>
					<td class="border_bot" colspan="5">' . $DHLAccountOpenStep2['SystemContactEmail'] . '</td>
			    </tr>
				<tr>
					<td colspan="20">(Implementation) Mobile Number of Contact Person</td>
			    </tr>
			    <tr>
					<td colspan="4">系统对接联系人手机号码</td>
					<td class="border_bot" colspan="16">' . $DHLAccountOpenStep2['SystemContactMobile'] . '</td>
			    </tr>
				<tr>
			        <th colspan="20">1.1 Billing/Invoicing Address</th>
			    </tr>
			    <tr>
			    	<th class="border_bot" colspan="20">账单/发票地址</th>
			    </tr>
				<tr>
					<td colspan="20">Registered Company Name</td>
			    </tr>
			    <tr>
					<td colspan="3">注册公司名称</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['BillCompanyName'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Billing/Invoicing Address</td>
			    </tr>
			    <tr>
					<td colspan="3">账单/发票地址</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['BillCompanyAddress'] . '</td>
			    </tr>
				<tr>
					<td colspan="20">City/Zip Code</td>
			    </tr>
			    <tr>
					<td colspan="2">城市/邮编</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['BillZipCode'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">Telephone Number</td>
					<td colspan="10">Fax Number</td>
			    </tr>
			    <tr>
					<td colspan="2">电话号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['BillTelephone'] . '</td>
					<td colspan="2">传真号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['BillFaxNo'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Name of Authorized Signatory Person</td>
			    </tr>
			    <tr>
					<td colspan="3">授权签字人姓名</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['BillAuthorizedName'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">Name of Contact Person</td>
					<td colspan="10">Email Address</td>
			    </tr>
			    <tr>
					<td colspan="2">联系人姓名</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['BillContactName'] . '</td>
					<td colspan="4">联系人电子邮件地址</td>
					<td class="border_bot" colspan="6">' . $DHLAccountOpenStep2['BillContactEmail'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Mobile Number of Contact Person</td>
			    </tr>
			    <tr>
					<td colspan="3">联系人手机号码</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['BillContactMobile'] . '</td>
			    </tr>
				<tr>
					<td colspan="20">*Attach annexure with details for more than 1 billing/invoicing address</td>
			    </tr>
			    <tr>
					<td colspan="20">*一个以上开票地址时请附加详细信息</td>
			    </tr>
			    <tr>
			        <th colspan="20">1.2 Pickup Addres</th>
			    </tr>
			    <tr>
			    	<th class="border_bot" colspan="20">取件地址</th>
			    </tr>
			    <tr>
					<td colspan="20">Pickup Address</td>
			    </tr>
			    <tr>
					<td colspan="2">取件地址</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['PickupAddress'] . '</td>
			    </tr>
				<tr>
					<td colspan="20">City/Zip Code</td>
			    </tr>
			    <tr>
					<td colspan="2">城市/邮编</td>
					<td class="border_bot" colspan="18">' . $DHLAccountOpenStep2['PickupZipCode'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">Telephone Number</td>
					<td colspan="10">Fax Number</td>
			    </tr>
			    <tr>
					<td colspan="2">电话号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['PickupTelephone'] . '</td>
					<td colspan="2">传真号码</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['PickupFaxNo'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Name of Authorized Signatory Person</td>
			    </tr>
			    <tr>
					<td colspan="3">授权签字人姓名</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['PickupAuthorizedName'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">Name of Contact Person</td>
					<td colspan="10">Email Address</td>
			    </tr>
			    <tr>
					<td colspan="2">联系人姓名</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['PickupContactName'] . '</td>
					<td colspan="4">联系人电子邮件地址</td>
					<td class="border_bot" colspan="6">' . $DHLAccountOpenStep2['PickupContactEmail'] . '</td>
			    </tr>
			    <tr>
					<td colspan="20">Mobile Number of Contact Person</td>
			    </tr>
			    <tr>
					<td colspan="3">联系人手机号码</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['PickupContactMobile'] . '</td>
			    </tr>
				<tr>
					<td colspan="20">*Attach annexure with details for more than 1 pick-up address</td>
			    </tr>
			    <tr>
					<td colspan="20">*一個以上取件位址時請附加詳細資訊</td>
			    </tr>
				<tr>
			        <th colspan="20">2. Credit Verification Details (authorized signatory confirms all information provided is valid and true to best of
			knowledge)</th>
			    </tr>
			    <tr>
			    	<th class="border_bot" colspan="20">信用验证信息（授权签字人确认所提供的所有信息真实有效）</th>
			    </tr>
				<tr>
					<td colspan="20">Annual Estimated Revenue</td>
			    </tr>
			    <tr>
					<td colspan="3">预计年付运费</td>
					<td class="border_bot" colspan="17">' . $DHLAccountOpenStep2['AnnualEstRevenue'] . '</td>
			    </tr>
				<tr>
					<td colspan="6">Owner/Directors Name</td>
					<td colspan="14">Address</td>
			    </tr>
			    <tr>
					<td colspan="3">经营者/董事姓名</td>
					<td class="border_bot" colspan="3">' . $DHLAccountOpenStep2['OwnerName'] . '</td>
					<td colspan="1">地址</td>
					<td class="border_bot" colspan="13">' . $DHLAccountOpenStep2['OwnerAddress'] . '</td>
			    </tr>
				<tr>
					<td colspan="10">Contact number</td>
					<td colspan="10">E-mail ID</td>
			    </tr>
			    <tr>
					<td colspan="2">联系电话</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['OwnerTelephone'] . '</td>
					<td colspan="3">电子邮件地址</td>
					<td class="border_bot" colspan="7">' . $DHLAccountOpenStep2['OwnerEmail'] . '</td>
			    </tr>
			    <tr>
					<td colspan="6">Finance Contact Name</td>
					<td colspan="14">Address</td>
			    </tr>
			    <tr>
					<td colspan="3">财务联系人姓名</td>
					<td class="border_bot" colspan="3">' . $DHLAccountOpenStep2['FinanceContactName'] . '</td>
					<td colspan="1">地址</td>
					<td class="border_bot" colspan="13">' . $DHLAccountOpenStep2['FinanceContactAddress'] . '</td>
			    </tr>
			    <tr>
					<td colspan="10">Contact number</td>
					<td colspan="14">E-mail ID</td>
			    </tr>
			    <tr>
					<td colspan="2">联系电话</td>
					<td class="border_bot" colspan="8">' . $DHLAccountOpenStep2['FinanceContactTelephone'] . '</td>
					<td colspan="3">电子邮件地址</td>
					<td class="border_bot" colspan="7">' . $DHLAccountOpenStep2['FinanceContactEmail'] . '</td>
			    </tr>
			    <tr>
					<td colspan="6">Office Premise</td>
					<td colspan="6">' . ($DHLAccountOpenStep2['OfficePremise'] == 0 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . 'Owned</td>
					<td colspan="8">' . ($DHLAccountOpenStep2['OfficePremise'] == 1 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . 'Rented</td>
			    </tr>
			    <tr>
					<td colspan="6">办公场所</td>
					<td colspan="6">自有</td>
					<td colspan="8">租赁</td>
			    </tr>
			';

			// 上海，深圳都需要填写增值税发票信息，委托付款信息
			if ($DHLAccountOpenStep1['AccountAddress'] > 0) {
				$html .= '
					<tr>
				        <th colspan="20">3. 增值税发票信息征询表</th>
				    </tr>
				    <tr>
				    	<th class="border_bot" colspan="20">尊敬的客户：请完整填列下表（*为必填项），并确保相关税务信息与贵司税务登记证以及向税局备案信息完全一致，感谢您的支持与配合！</th>
				    </tr>
				    <tr>
						<td colspan="10">公司名稱 （税务登记证或者营业执照的公司名称）</td>
						<td class="border_bot" colspan="10">' . $DHLAccountOpenStep2['VATCompany'] . '</td>
				    </tr>
				    <tr>
						<td colspan="6">*请选择所属纳税人类型</td>
						<td colspan="6">' . ($DHLAccountOpenStep2['TaxpayerType'] == 0 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">'). '</td>
						<td colspan="8">' . ($DHLAccountOpenStep2['TaxpayerType'] == 1 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">'). '</td>
				    </tr>
				    <tr>
						<td colspan="6">*请填写所属纳税人类型的代码</td>
						<td colspan="6">增值税一般纳税人</td>
						<td colspan="8">非增值税一般纳税人</td>
				    </tr>
					<tr>
						<td colspan="20" style="color:#f00;">如果您属于“1.增值税一般纳税人”，请务必完整填列下表全部信息，并提供贵公司一般纳税人认定通知书复印件。</td>
				    </tr>

				    <tr>
						<td colspan="20" style="color:#f00;">如果您属于“2.非增值税一般纳税人”，请填列下表带*项信息。</td>
				    </tr>
					<tr>
						<td colspan="3">统一社会信用代码</td>
						<td colspan="17" class="border_bot">' . $DHLAccountOpenStep2['SocialCreditCode'] . '</td>
				    </tr>
				    <tr>
						<td colspan="4">公司地址(营业执照注册)</td>
						<td colspan="16" class="border_bot">' . $DHLAccountOpenStep2['VATCompanyAddress'] . '</td>
				    </tr>
				    <tr>
						<td colspan="4">公司电话(营业执照注册)</td>
						<td colspan="16" class="border_bot">' . $DHLAccountOpenStep2['VATCompanyTelephone'] . '</td>
				    </tr>
					<tr>
						<td colspan="5">开户银行名称(税局备案信息)</td>
						<td colspan="15" class="border_bot">' . $DHLAccountOpenStep2['VATBankName'] . '</td>
				    </tr>
				    <tr>
						<td colspan="5">开户银行账号(税局备案信息)</td>
						<td colspan="15" class="border_bot">' . $DHLAccountOpenStep2['VATBankAccount'] . '</td>
				    </tr>
				    <tr>
						<td colspan="7">*请选择上线后所需我司开具的发票类型</td>
						<td colspan="3">' . ($DHLAccountOpenStep2['VATInvoiceType'] == 0 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . '</td>
						<td colspan="7">' . ($DHLAccountOpenStep2['VATInvoiceType'] == 1 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . '</td>
						<td colspan="3">' . ($DHLAccountOpenStep2['VATInvoiceType'] == 2 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . '</td>
				    </tr>
				    <tr>
						<td colspan="7">*请填写所需发票类型代码</td>
						<td colspan="3">增值税普通发票</td>
						<td colspan="7">增值税专用发票（只有增值税一般纳税人方可选择此项）</td>
						<td colspan="3">无需税务发票</td>
				    </tr>
				    <tr>
						<td colspan="3">税务发票事宜联系人</td>
						<td colspan="17" class="border_bot">' . $DHLAccountOpenStep2['VATInvoiceContactName'] . '</td>
				    </tr>
				    <tr>
						<td colspan="4">税务发票事宜联系人电话</td>
						<td colspan="16" class="border_bot">' . $DHLAccountOpenStep2['VATInvoiceContactTelephone'] . '</td>
				    </tr>
				    <tr>
						<td colspan="4">税务发票事宜联系人邮箱</td>
						<td colspan="16" class="border_bot">' . $DHLAccountOpenStep2['VATInvoiceContactEmail'] . '</td>
				    </tr>
					<tr>
				        <th colspan="20">4. Payment Authorization</th>
				    </tr>
				    <tr>
				        <th colspan="20">委托付款</th>
				    </tr>

				    <tr>
						<td colspan="7">Payment from a third party or not</td>
						<td colspan="4">' . ($DHLAccountOpenStep2['IsPaymentAuth'] == 0 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . 'Yes</td>
						<td colspan="4">' . ($DHLAccountOpenStep2['IsPaymentAuth'] == 1 ? '<img src="/inc/class/pdf/images/icon_choice_selected.png">' : '<img src="/inc/class/pdf/images/icon_choice.png">') . 'No</td>
				    </tr>
				    <tr>
						<td colspan="7">是否由第三方付款</td>
						<td colspan="4">是</td>
						<td colspan="4">否</td>
				    </tr>
					<tr>
						<td colspan="20">If Yes, please fill below</td>
				    </tr>
				    <tr>
						<td colspan="20">如果是，请填入</td>
				    </tr>
				    <tr>
						<td colspan="20">3rd Party Company Name</td>
				    </tr>
				     <tr>
						<td colspan="3">第三方名称</td>
						<td colspan="17" class="border_bot">' . $DHLAccountOpenStep2['ThirdCompanyName'] . '</td>
				    </tr>
				    <tr>
						<td colspan="20">3rd Party Bank Account Number</td>
				    </tr>
				     <tr>
						<td colspan="3">第三方银行账号</td>
						<td colspan="17" class="border_bot">' . $DHLAccountOpenStep2['ThirdBankAccount'] . '</td>
				    </tr>
				';
			}		
			$html .= '</table>';
			$html .= '<br><br>
			<hr>
			<p></p>
			<table>
				<tr>
					<td align="right">' . ($DHLAccountOpenStep1['AccountAddress'] > 0 ? '申请时间' : '申請時間') .'：'. date('Y-m-d', time()) . '</td>
				</tr>
			</table>';

		// 生成Account Application Form.pdf文件
		$info = array(
		    'title'			=>	'账户申请表', 
		    'subject'		=>	'Account Application Form', 
		    'keywords'		=>	'Account,Application,Form', 
		    'content'		=>	$html,
		    'path'			=>	'/u_file/file/dhl/',
		    'file_name'		=>	'Account Application Form.pdf',
		    'Footer'		=>	0,
		);
		self::createPDF($info);
	}

	/**
	 * 生成Shipper's Checklist for Hidden Dangerous Goods.pdf文件
	 * @return 无
	 */
	public static function shipper_dangerous_goods_pdf()
	{
		$DHLAccountOpenStep5Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep5"', 'Value');
		$DHLAccountOpenStep5 = str::json_data($DHLAccountOpenStep5Data, 'decode');

		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		// 香港版与上海深圳之前不同的，唯有文字。所以，做个语言包
		$lang = $DHLAccountOpenStep1['AccountAddress'] == 0 ? 0 : 1;

		$c['lang_pack'] = array(
					'declaration_goods'		=>	array('貨物申報', '货物申报'),
					'dangerous_goods'		=>	array('托運人隱含危險物品清單', '托运人隐含危险物品清单'),
					'protect_safety'		=>	array('為保護飛機及其乘客的安全，請不要錯誤申報危險物品', '为保护飞机及其乘客的安全，请不要错误申报危险物品'),
					'cargo_airline'			=>	array('填写此表并将其提交给货运代理或航空公司', '填写此表并将其提交给货运代理或航空公司'),
					'shipper_name'			=>	array('托運人名稱', '托运人名称'),
					'company_name'			=>	array('公司名稱', '公司名称'),
					'contact_number'		=>	array('聯繫人號碼', '联系人号码'),
					'goods_description'		=>	array('貨物說明', '货物说明'),
					'shipment_reference'	=>	array('貨運參考號', '货运参考号'),
					'dangerous_item_0'		=>	array('我在此聲明，我交付給DHL電子商務/全球貨運的上述貨運參考號的托運貨物不含有此處所列的任何一種產品', '我在此声明，我交付给DHL电子商务/全球货运的上述货运参考号的托运货物不含有此处所列的任何一种产品'),
					'dangerous_item_1'		=>	array('含鋰電池的電子產品、移動電話、充電寶、照相設備、含鋰電池的手電筒、散裝的鋰電池、原型鋰電池、充電電池、濕電池、鹼性電池、設備所帶的電池、揚聲器、太陽能充電器、超電容、有缺陷的鋰電池（包括散裝、包裝的或設備中帶的電池）', '含锂电池的电子产品、移动电话、充电宝、照相设备、含锂电池的手电筒、散装的锂电池、原型锂电池、充电电池、湿电池、碱性电池、设备所带的电池、扬声器、太阳能充电器、超电容、有缺陷的锂电池（包括散装、包装的或设备中带的电池）'),
					'dangerous_item_2'		=>	array('帶可燃潤滑油的機動車、摩托車或電動車輛零件、發動機、割草機、發電機、化油器、燃油箱、燃油、燃油控制器、滅火器、氣囊、輪船或飛機配件、氣壓計、可燃液體或燃氣發動機驅動的無線控制的玩具車（汽車/飛機/船）', '带可燃润滑油的机动车、摩托车或电动车辆零件、发动机、割草机、发电机、化油器、燃油箱、燃油、燃油控制器、灭火器、气囊、轮船或飞机配件、气压计、可燃液体或燃气发动机驱动的无线控制的玩具车（汽车/飞机/船）'),
					'dangerous_item_3'		=>	array('含可燃氣體或液體的壓力罐裝的個人護理產品、香水或身體噴霧器、指甲油、家用化學品、清潔劑、氣溶膠、漂白劑、下水道清潔劑、殺蟲劑、石蠟、帶可燃液體或溶劑的工具箱、膠水、噴漆、潤滑油、打火機', '含可燃气体或液体的压力罐装的个人护理产品、香水或身体喷雾器、指甲油、家用化学品、清洁剂、气溶胶、漂白剂、下水道清洁剂、杀虫剂、石蜡、带可燃液体或溶剂的工具箱、胶水、喷漆、润滑油、打火机'),
					'dangerous_item_4'		=>	array('農藥或工業用化學品、有強烈氣味的化學品或揮發性化學品、金屬粉末、顏料、可燃固體、氧化劑、有毒或腐蝕性物質、殺蟲劑、壓縮氣體、可燃液體、樹脂、墨水、油漆、密封劑、粘合劑、水銀', '农药或工业用化学品、有强烈气味的化学品或挥发性化学品、金属粉末、颜料、可燃固体、氧化剂、有毒或腐蚀性物质、杀虫剂、压缩气体、可燃液体、树脂、墨水、油漆、密封剂、粘合剂、水银'),
					'dangerous_item_5'		=>	array('醫療設備、放射性物質、醫藥原材料、疫苗或病毒樣本、壓縮氣體、低溫液化氣、乾冰、血壓計、溫度計', '医疗设备、放射性物质、医药原材料、疫苗或病毒样本、压缩气体、低温液化气、干冰、血压计、温度计'),
					'dangerous_item_6'		=>	array('帶濕電池或含水銀的電器、含可燃氣體的冰箱或空調、真空管、磁偏轉電子射線管、工業用或高強度磁鐵、釹磁鐵、煙霧探測器', '带湿电池或含水银的电器、含可燃气体的冰箱或空调、真空管、磁偏转电子射线管、工业用或高强度磁铁、钕磁铁、烟雾探测器'),
					'dangerous_item_7'		=>	array('鞭炮、火藥、照明彈、信號彈、射釘槍、鑽探和採礦設備、探險裝備', '鞭炮、火药、照明弹、信号弹、射钉枪、钻探和采矿设备、探险装备'),
					'dangerous_tips_0'		=>	array('注意！如果您的產品符合以上描述或者您不確定您的產品是否屬於國際航空運輸協會危險品條例*定義的危險品分類，請諮詢DHL電子商務公司的員工。', '注意！如果您的产品符合以上描述或者您不确定您的产品是否属于国际航空运输协会危险品条例*定义的危险品分类，请咨询DHL电子商务公司的员工。'),
					'dangerous_tips_1'		=>	array('國際航空運輸協會危險品網站', '国际航空运输协会危险品网站'),
					'apply_time'			=>	array('申請時間', '申请时间'),
		);

		$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
				h1{font-size:16px;font-weight:bold;text-align:center;text-decoration: underline;line-height:100%;text-align:center;}
				h2{font-size:12px;font-weight:bold;line-height:100%;text-align:center;}
				h3{font-size:12px;font-weight:bold;line-height:100%;text-align:center;color:#f00;}
				p{font-size:11px;}
				td{font-size:11px;}
				.border_bot{border-bottom:1px solid #000;}
			</style>
			<table cellpadding="0" cellspacing="0" style="border:none;">
				<tr>
					<td width="75%" style="font-size:14px;font-weight:bold;">Section II</td>
					<td width="25%" rowspan="2" align="right"><img src="/inc/class/pdf/images/dhl_logo.png"></td>
				</tr>
				<tr>
					<td style="font-size:14px;font-weight:bold;">Declaration of Goods / ' . $c['lang_pack']['declaration_goods'][$lang] . '</td>
				</tr>
			</table>
			<h1>Shipper\'s Checklist for HiddenDangerous Goods</h1>
			<h1>' . $c['lang_pack']['dangerous_goods'][$lang] . '</h1>
			<h2>To protect the safety of the aircraft and its passengers, do not misdeclare dangerous goods.</h2>
			<h2>' . $c['lang_pack']['protect_safety'][$lang] . '</h2>
			<h3>Please complete this form and submit it to your cargo agent or airline.</h3>
			<h3>' . $c['lang_pack']['cargo_airline'][$lang] . '</h3>
			<table>
				<tr>
					<td colspan="10">Shipper\'s Name: </td>
				</tr>
				<tr>
					<td colspan="1">' . $c['lang_pack']['shipper_name'][$lang] . ':</td>
					<td colspan="9" class="border_bot">' . $DHLAccountOpenStep5['ShipperName'] . '</td>
				</tr>
				<tr>
					<td colspan="10">Company Name:</td>
				</tr>
				<tr>
					<td colspan="1">' . $c['lang_pack']['company_name'][$lang] . ':</td>
					<td colspan="9" class="border_bot">' . $DHLAccountOpenStep5['CompanyName'] . '</td>
				</tr>
				<tr>
					<td colspan="10">Contact Number:</td>
				</tr>
				<tr>
					<td colspan="1">' . $c['lang_pack']['contact_number'][$lang] . ':</td>
					<td colspan="9" class="border_bot">' . $DHLAccountOpenStep5['ShipperTelephone'] . '</td>
				</tr>
				<tr>
					<td colspan="10">Goods Description:</td>
				</tr>
				<tr>
					<td colspan="1">' . $c['lang_pack']['goods_description'][$lang] . ':</td>
					<td colspan="9" class="border_bot">' . $DHLAccountOpenStep5['CargoDescription'] . '</td>
				</tr>
				<tr>
					<td colspan="10">Shipment Reference Number:</td>
				</tr>
				<tr>
					<td colspan="1">' . $c['lang_pack']['shipment_reference'][$lang] . ':</td>
					<td colspan="9" class="border_bot">' . $DHLAccountOpenStep5['FreightReference'] . '</td>
				</tr>
				<tr>
					<td colspan="10"></td>
				</tr>
			</table>
			<table style="border:1px solid #000">
				<tr>
					<td class="border_bot">
						<p>I hereby declare that my consignments handed over to DHL eCommerce/Global Forwarding as per above mentioned shipment reference number do NOT contain any of these products stated herein:<br>' . $c['lang_pack']['dangerous_item_0'][$lang] . '：</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Electronic products with lithium batteries, mobile phones, Powerbank chargers, photographic equipment, torch/flashlight with lithium batteries, loose lithium batteries, prototype lithium batteries, rechargeable batteries, wet cell batteries, alkaline batteries, batteries contained in equipment, speaker, solar charger, ultra capacitor, Defective lithium batteries (either loose or packed with or contained in equipment)<br>' . $c['lang_pack']['dangerous_item_1'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Motor vehicles, motorcycles or electric vehicles parts with flammable lubricants, engines, lawn mowers, generator, carburettor, fuel tank, fuel, fuel controllers, fire extinguishers, air bag, ships or aircraft parts, barometer, radio controlled toy vehicles (cars/airplanes/boats) powered by flammable liquid or gas engines<br>' . $c['lang_pack']['dangerous_item_2'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Personal care products in pressurized canisters with flammable gas or liquid, perfumes or body sprays, nail polish, household chemicals, cleaning agents, aerosols, bleach, drain cleaners, pesticides, wax, toolbox with flammable liquid or solvents, glue, spray paint, lubricants, lighter<br>' . $c['lang_pack']['dangerous_item_3'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Agricultural or industrial chemicals, chemicals with strong odour or volatile chemicals, metal powder, pigment, flammable solids, oxidizers, toxic or corrosive substances, pesticides, compressed gases, flammable liquids, resins, inks, paints, sealants, adhesives, mercury<br>' . $c['lang_pack']['dangerous_item_4'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Medical equipment, radioactive substances, pharmaceuticals raw materials, vaccines or virus samples, compressed gases, cryogenic liquefied gases, dry ice, sphygmomanometer, thermometer<br>' . $c['lang_pack']['dangerous_item_5'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td class="border_bot">
						<p>Electrical appliances with wet batteries or mercury contents, refrigerators or air conditioners with flammable gas, vacuum tube, magnetic tubes, industrial or high strength magnets, neodymium magnets, smoke detectors<br>' . $c['lang_pack']['dangerous_item_6'][$lang] . '</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>Fireworks, ammunition, flares, signal flares, nail gun, drilling and mining equipment, adventure equipment<br>' . $c['lang_pack']['dangerous_item_7'][$lang] . '</p>
					</td>
				</tr>
			</table>
			<p></p>
			<p>CAUTION! PLEASE CONSULT DHL eCommerce STAFF BEFORE SHIPPING IF YOU DO HAVE PRODUCTS THAT FALL INTO ABOVE DESCRIPTION OR YOU ARE UNSURE WHETHER YOUR PRODUCTS DO FALL INTO ANY OF THE DEFINED CATEGORIES IN IATA DANGEROUS GOODS REGULATIONS* AS A DANGEROUS GOODS. ' . $c['lang_pack']['dangerous_tips_0'][$lang] . '<br>* IATA DANGEROUS GOODS WEBSITE: <a href="www.iata.org/whatwedo/cargo/dgr/Pages/fag.aspx">www.iata.org/whatwedo/cargo/dgr/Pages/fag.aspx</a><br>*' . $c['lang_pack']['dangerous_tips_1'][$lang] . ': <a href="www.iata.org/whatwedo/cargo/dgr/Pages/fag.aspx">www.iata.org/whatwedo/cargo/dgr/Pages/fag.aspx</a></p>
			<hr>
			<p></p>
			<p></p>
			<table>
				<tr>
					<td align="right">' . $c['lang_pack']['apply_time'][$lang] . ': ' . date('Y-m-d', time()) . '</td>
				</tr>
			</table>
		';

		// Shippers Checklist for Hidden Dangerous Goods.pdf文件
		$info = array(
		    'title'			=>	'貨物申報', 
		    'subject'		=>	'Shippers Checklist for Hidden Dangerous Goods', 
		    'keywords'		=>	'Shippers,Checklist,Dangerous', 
		    'content'		=>	$html,
		    'path'			=>	'/u_file/file/dhl/',
		    'file_name'		=>	'Shipper\'s Checklist for Hidden Dangerous Goods.pdf',
		);
		self::createPDF($info);
	}

	/**
	 * 生成Personal Information Collection Statement for Customers.pdf文件
	 * @return 无
	 */
	public static function personal_information_pdf ()
	{
		$DHLAccountOpenStep6Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep6"', 'Value');
		$DHLAccountOpenStep6 = str::json_data($DHLAccountOpenStep6Data, 'decode');
		// 根据不同地点，显示不同的
		$DHLAccountOpenStep1Data = db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountOpenStep1"', 'Value');
		$DHLAccountOpenStep1 = str::json_data($DHLAccountOpenStep1Data, 'decode');

		$lang = $DHLAccountOpenStep1['AccountAddress'] ? 1 : 0;

		$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
				h2{font-size:14px;font-weight:bold;}
				h1{font-size:16px;font-weight:bold;text-align:center;text-decoration: underline;}
				p{font-size:11px;}
				td{font-size:11px;}
			</style>
			<h2>Section III</h2>
			<h1>Personal Information Collection Statement for Customers</h1>
		';

		$html .= $lang ? '<p>DHL Global Forwarding (China) Co., Ltd.(“DHL”) is committed to protecting and respecting your privacy in accordance with applicable law of the People’s Republic of China (the “Law”) and DHL’s Personal Data Protection Policy (the “Policy”) at <span style="color:#3385ff">https://www.logistics.dhl/hk-en/home/footer/local-privacy-notice.html</span></p>' : '<p>DHL eCommerce (Hong Kong) Limited ("DHL") is committed to protecting and respecting your privacy in accordance with the Personal Data (Privacy) Ordinance of Hong Kong (the "Ordinance") and DHL\'s Personal Data Protection Policy (the "Policy") at <span style="color:#3385ff">https://www.logistics.dhl/hk-en/home/footer/local-privacy-notice.html</span></p>';

		$html .= '<br>
			<p>The purpose of this statement is to bring to your attention DHL\'s practices and policies relating to the collection, use, processing, disclosure and retention of your personal data. Such personal data is sourced from information provided by you in past dealings with DHL and/or may be provided by you through your use of DHL\'s services or DHL\'s websites or that may otherwise be collected by us.</p>
			<br>
			<p>DHL collects, uses, processes and retains personal data for its legitimate functions:</p>
			<br>
			<p>effecting delivery of shipments locally or abroad;</p>
			<p>providing, monitoring and improving DHL\'s services including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services;</p>
			<p>managing and administering your account(s) with DHL;</p>
			<p>advertising products and services provided by DHL including shipment processing, delivery and distribution, data and record management, e-commerce related logistics services, other logistics and value added services and other services which are ancillary to the aforesaid core products and business of DHL including insurance services of DHL\'s business partners; </p>
			<p>advertising products and services for environmental conservation and charitable purposes; and </p>
			<p>such other purposes as may be required for the running of DHL\'s services and businesses or by applicable laws or regulations, including the communication of the same to customs authorities.</p>
			<br>
			<p>Without this personal data, DHL may be faced with delays or be unable to provide you with the contracted services which you require.</p>
			<br>';

		$html .= $lang ? '<p>Personal data collected by DHL may be transferred to, and stored or processed by a third party and/or at a destination outside the People’s Republic of China (either within the Deutsche Post DHL group of companies or by third parties) in accordance with the Law and the Policy. By submitting your personal data and/or using DHL’s services, products or websites, you agree to this transfer, storing or processing.</p>' : '<p>Personal data collected by DHL may be transferred to, and stored or processed by a third party and/or at a destination outside Hong Kong (either within the Deutsche Post DHL group of companies or by third parties) in accordance with the Ordinance and the Policy. By submitting your personal data and/or using DHL\'s services, products or websites, you agree to this transfer, storing or processing.</p>';

		$html .= '<p>DHL has also implemented reasonable and practical security measures designed to protect against loss, misuse and/or alteration of your personal data under its control.</p>';

		$html .= $lang ? '<p>You have the right to seek access to and correction of your personal data held by DHL. In addition, you may request us to delete your personal data that is no longer required for the relevant purposes which you have given consent. Such rights may be exercised by you at any time. If you wish to exercise such rights, or if you have any question regarding your personal data or this statement, please contact our Data Protection Officer in writing at 2/F EDGE. 30-34 Kwai Wing Road, Kwai Chung New Territories, Hong Kong. DHL reserves the right to charge you a reasonable fee for complying with a data access request as permitted by the Law.' : '<p>You have the right to seek access to and correction of your personal data held by DHL. In addition, you may request us to delete your personal data that is no longer required for the relevant purposes which you have given consent. Such rights may be exercised by you at any time. If you wish to exercise such rights, or if you have any question regarding your personal data or this statement, please contact our Data Protection Officer in writing at 2/F EDGE. 30-34 Kwai Wing Road, Kwai Chung New Territories, Hong Kong. DHL reserves the right to charge you a reasonable fee for complying with a data access request as permitted by the Ordinance.</p>';


		$html .= '<br><br>
			<hr>
			<p></p>
			<table>
				<tr>
					<td align="right">' . ($lang ? '申请时间' : '申請時間') .'：'. date('Y-m-d', time()) . '</td>
				</tr>
			</table>';

		// Personal Information Collection Statement for Customers.pdf文件
		$info = array(
		    'title'			=>	'个人资料', 
		    'subject'		=>	'Personal Information Collection Statement for Customers', 
		    'keywords'		=>	'Personal,Information,Collection,Customers', 
		    'content'		=>	$html,
		    'path'			=>	'/u_file/file/dhl/',
		    'file_name'		=>	'Personal Information Collection Statement for Customers.pdf',
		);
		self::createPDF($info);

	}

	/**
	 * 生成Rate Card.pdf文件
	 * $address 取值： HKG/SHA/SZX
	 * pdf::RateCard_pdf('HKG', 'Rate Card 0');
	 * pdf::RateCard_pdf('SHA', 'Rate Card 1');
	 * pdf::RateCard_pdf('SZX', 'Rate Card 2');
	 * @return 无
	 */
	public static function RateCard_pdf ($address)
	{
		$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
				h2{font-size:14px;font-weight:bold;}
				h1{font-size:16px;font-weight:bold;text-align:center;}
				p{font-size:11px;}
				td{font-size:11px;}
			</style>
			<h2>Section IV</h2>
			<h1>Rate Card</h1>
		';
		$html .= '<p style="text-align: center;">';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_01.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_02.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_03.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_04.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_05.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_06.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_07.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_08.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_09.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_10.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_11.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_12.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_13.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_14.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_15.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_16.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_17.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_18.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_19.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_20.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_21.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_22.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_23.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_24.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_25.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_26.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_27.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_28.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_29.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_30.png"><br/>';
			$html .= '<img src="/inc/class/pdf/images/' . $address . '_31.png"><br/>';
		$html .= '</p>';
		$html .= '<br><br>
			<hr>
			<p></p>
			<table>
				<tr>
					<td align="right">' . ($address != 'HKG' ? '申请时间' : '申請時間') .'：'. date('Y-m-d', time()) . '</td>
				</tr>
			</table>';

		$info = array(
		    'title'			=>	'Rate Card', 
		    'subject'		=>	'Rate Card', 
		    'keywords'		=>	'Rate Card', 
		    'content'		=>	$html,
		    'path'			=>	'/u_file/file/dhl/',
		    'file_name'		=>	'Rate Card.pdf',
		    'Footer'		=>	0,
		);
		self::createPDF($info);

	}

}
?>