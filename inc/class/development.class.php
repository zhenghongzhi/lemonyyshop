<?php
/*
 * Powered by ly200.com		http://www.ly200.com
 * 广州联雅网络科技有限公司		020-83226791
 */

class development
{
    /**
	 * 前台加载文件
	 *
	 * @param object $path	文件路径
     * @param object $exit	终止PHP执行
	 */
    public static function load_file($path, $exit = 0)
    {
        global $c, $m, $a, $d;
        $pass = 0;
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        if ($dir && is_dir($c['root_path'] . $dir)) {
            if ($path && is_file($c['root_path'] . $path)) {
                include($c['root_path'] . $path);
                $pass = 1;
            }
        }
        if ($pass == 1 && $exit == 1) {
            exit;
        }
    }

    /**
     * 前台加载 css/js 文件
     *
     * @param object $path	文件路径
     * @param object $exit	终止PHP执行
     */
    public static function load_link_file($path, $exit = 0)
    {
        global $c, $m, $a, $d;
        $pass = 0;
        if ($path && is_file($c['root_path'] . $path)) {
            echo ly200::load_static($path);
            $pass = 1;
        }
        if ($pass == 1 && $exit == 1) {
            exit;
        }
    }
    
    /**
	 * 前台do_action加载
	 *
	 * @param object $do_action	Data数据
	 */
    public static function web_do_action($do_action)
    {
		global $c, $m, $a, $d;
		$_ = @explode('.', $do_action);
        $_do_action_file = "{$c['static_path']}/do_action/development/{$_[0]}.php";
        if (@is_file($_do_action_file)) {
            include($_do_action_file);
            if (method_exists($_[0] . '_module', $_[1])) {
                eval("{$_[0]}_module::{$_[1]}();");
                exit;
            }
        }
	}
    
    
}
?>