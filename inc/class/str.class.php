<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class str{
	public static function str_code($data, $fun='htmlspecialchars'){	//文本编码
		if(!is_array($data)){
			return $fun($data);
		}
		$new_data=array();
		foreach((array)$data as $k=>$v){
			if(is_array($v)){
				$new_data[$k]=str::str_code($v, $fun);
			}else{
				$new_data[$k]=$fun($data[$k]);
			}
		}
		return $new_data;
	}
	
	public static function str_color($str='', $key=0, $return_type=0){
		$key>15 && $key=$key%15;
		if($return_type==0){
			return "<font class='fc_$key'>$str</font>";
		}else{
			return "fc_$key";
		}
	}
	
	public static function get_time($t, $return=0, $type=1){
		global $c;
		$format=array('Y-m-d', 'Y-m-01', 'Y-01-01');
		$type_ary=array('day', 'month', 'year');
		$st=strtotime(date($format[$type], $c['time']));
		$time=strtotime("$t {$type_ary[$type]}", $st);
		return $return==0?$time:date($format[$type], $time);
	}
	
	public static function iconver($data, $source='UTF-8', $target='GBK'){
		global $c;
		$chs=new iconver();
		if(!is_array($data)){
			return $chs->Convert($data, $source, $target);
		}
		$new_data=array();
		foreach((array)$data as $k=>$v){
			if(is_array($v)){
				$new_data[$k]=str::iconver($v, $source, $target);
			}else{
				$new_data[$k]=$chs->Convert($data[$k], $source, $target);
			}
		}
		return $new_data;
	}
	
	public static function str_sprintf($str, $vars, $char='%'){	//替换多个参数的sprintf
    	if(is_array($vars)){
			foreach($vars as $k=>$v){
				$str=str_replace($char.$k, $v, $str);
			}
		}
		return $str;
	}
	
	public static function rand_code($length=10){	//随机命名
		global $c;
		return substr(md5($c['time']+mt_rand(100000, 999999)), mt_rand(0, 32-$length), $length);	
	}
	
	public static function json_data($data, $action='encode'){	//json数据编码
		if($action=='encode'){
			if(version_compare(PHP_VERSION, '5.4.0', '>=')){
				return json_encode($data, JSON_UNESCAPED_UNICODE);
			}else{
            	return json_encode($data);
			}
		}else{
			return (array)json_decode($data, true);
		}
	}
	
	public static function format($str){	//格式化文本
		$str=htmlspecialchars($str);
		$str=str_replace('  ', '&nbsp;&nbsp;', $str);
		$str=nl2br($str);	
		return $str;
	}
	
	public static function cut_str($str, $length, $start=0){	//剪切字符串
		$str_0=array('&amp;', '&quot;', '&lt;', '&gt;', '&ldquo;', '&rdquo;');
		$str_1=array('&', '"', '<', '>', '“', '”');
		$str=str_replace($str_0, $str_1, $str);
		$len=strlen($str);
		if($len<=$length){return str_replace($str_1, $str_0, $str);}
		$substr='';
		$n=$m=0;
		for($i=0; $i<$len; $i++){
			$x=substr($str, $i, 1);
			$a=base_convert(ord($x), 10, 2);
			$a=substr('00000000'.$a, -8);
			if($n<$start){
				if(substr($a, 0, 3)==110){
					$i+=1;
				}elseif(substr($a, 0, 4)==1110){
					$i+=2;
				}
				$n++;
			}else{
				if(substr($a, 0, 1)==0){
					$substr.=substr($str, $i, 1);
				}elseif(substr($a, 0, 3)==110){
					$substr.=substr($str, $i, 2);
					$i+=1;
				}elseif(substr($a, 0, 4)==1110){
					$substr.=substr($str, $i, 3);
					$i+=2;
				}else{
					$substr.='';
				}
				if(++$m>=$length){break;}
			}
		}
		return str_replace($str_1, $str_0, $substr);
	}

	public static function str_echo($str, $length, $start=0, $replace=''){	//输出固定长度内容
		$result=@mb_substr(trim($str), $start, $length, "UTF-8");
		strlen($str)>$length && $result.=$replace;
		return $result;
	}
	
	public static function dump($str, $type=''){//原样输出
		echo '<pre>';
		if ($type){
			var_dump($str);
		}else{
			print_r($str);
		}
		echo '</pre>';
	}
	
	public static function keywords_filter($ary=array()){//过滤敏感词
		global $c;
		if((int)$c['Replica']) return;//Replica website do not filter
		
		@include_once($c['root_path'].'/manage/static/inc/filter.library.php');
		@include_once($c['root_path'].'/inc/un_filter_keywords.php');
		$filter_keywords_ary=$FilterKeyArr['Keyword'];
		$un_filter_keywords_ary=(array)@str::str_code($un_filter_keywords, 'strtolower');
		($ary && !@is_array($ary)) && $ary=(array)$ary;
		if((int)count($ary)){
			$filter_ary=$ary;
		}else{
			$filter_ary=$_POST;
			unset($filter_ary['do_action'], $filter_ary['PicPath'], $filter_ary['FilePath'], $filter_ary['UId'], $filter_ary['ColorPath'], $filter_ary['Number']);
		}
		$str=' '.@implode(' -- ', $filter_ary).' ';
		$key='';
		$in=0;
		foreach($filter_keywords_ary as $v){
			if(@count($un_filter_keywords_ary) && @in_array(strtolower(trim($v)), $un_filter_keywords_ary)){continue;}
			if(@substr_count(strtolower(stripslashes($str)), strtolower($v))){
				/*
				$in=1;
				$key=$v;
				break;
				*/
				$key.=($in?' ,':'').$v;
				$in++;
			}
		}
		unset($filter_ary);
		$in && ly200::e_json('带有敏感词：'.$key);
	}
	
	public static function clear_html($content) {//手机版清除格式
		$content = preg_replace("/<!--[^>]*-->/i", "", $content);//注释内容  
		$content = preg_replace("/style=.+?['|\"]/i",'',$content);//去除样式  
		$content = preg_replace("/class=.+?['|\"]/i",'',$content);//去除样式  
		$content = preg_replace("/id=.+?['|\"]/i",'',$content);//去除样式     
		$content = preg_replace("/lang=.+?['|\"]/i",'',$content);//去除样式      
		$content = preg_replace("/width=.+?['|\"]/i",'',$content);//去除样式   
		$content = preg_replace("/height=.+?['|\"]/i",'',$content);//去除样式   
		//$content = preg_replace("/border=.+?['|\"]/i",'',$content);//去除样式   
		$content = preg_replace("/face=.+?['|\"]/i",'',$content);//去除样式
		$content = preg_replace("/&nbsp;/i", " ", $content);
		return $content;
	}
	
	public static function ary_unique($ary){//去除二维数组的重复项 
		foreach($ary as $v){
			$temp[$v[0]]=$v[1];
		}
		$temp=array_unique($temp);//去掉重复的字符串,也就是重复的一维数组
		foreach($temp as $k=>$v){
			$temp_new[]=array($k, $v);
		}
		return $temp_new;
	}
	
	public static function ary_del_min($ary, $min=0, $sort=0){	//把数据中小于$min的数据删除
		if(!is_array($ary)) return false;
		foreach($ary as $k=>$v){
			if((float)$v<$min){
				unset($ary[$k]);
			}
		}
		$ary=array_unique($ary);//删除重复，空值，0
		$sort?rsort($ary):sort($ary);//(从大到小)(从小到大)排序，防止故意乱填
		return $ary;
	}
	
	public static function ary_format($ary, $return=0, $unset='', $explode_char=',', $implode_char=','){	//$return，0：字符串，1：数组，2：in查询语句，3：or查询语句，4：返回第一个值
		!is_array($ary) && $ary=explode($explode_char, $ary);
		//$ary=array_filter($ary, self::ary_format_ext($v));
		$ary=array_filter($ary, array(self, 'ary_format_ext'));
		if($unset){	//从数组中删除这些值
			$unset=str::ary_format($unset, 1, '', $explode_char, $implode_char);
			foreach($ary as $k=>$v){
				if(in_array($v, $unset)){
					unset($ary[$k]);
				}
			}
		}
		if($return==0){	
			return $ary?($implode_char.implode($implode_char, $ary).$implode_char):'';
		}elseif($return==1){
			return $ary;
		}elseif($return==2 || $return==3){
			if(!$ary){return '"0"';}
			if($return==2){
				$is_numeric=true;
				foreach($ary as $v){
					if(!is_numeric($v)){
						$is_numeric=false;
						break;
					}
				}
				return ($is_numeric?'':"'").implode($is_numeric?',':"','", $ary).($is_numeric?'':"'");
			}else{
				return implode(' or ', $ary);
			}
		}elseif($return==4){
			return array_shift($ary);
		}
	}
	
	public static function ary_format_ext($v){
		return ($v!='' || $v===0)?true:false;
	}
	
	public static function attr_decode($str){	//格式化产品属性内容里面的双引号
		//$str=str_replace(array('{"', '":"', '","', '"}'), array('{\"', '\":\"', '\",\"', '\"}'), $str);
		//$str=str_replace('"', '\\"', $str);
		//$str=str_replace('\\\"', '"', $str);
		$str=str::str_code($str, 'stripslashes');
		return $str;
	}
	
	public static function unescape($str){ 	//escape转译解码
		$ret='';
		$len=strlen($str);
		for($i=0; $i<$len; $i++){
			if($str[$i]=='%' && $str[$i+1]=='u'){
				$val=hexdec(substr($str, $i+2, 4));
				if($val<0x7f){
					$ret.=chr($val);
				}elseif($val<0x800){
					$ret.=chr(0xc0|($val>>6)).chr(0x80|($val&0x3f));
				}else{
					$ret.=chr(0xe0|($val>>12)).chr(0x80|(($val>>6)&0x3f)).chr(0x80|($val&0x3f));
				}
				$i+=5;
			}elseif($str[$i]=='%'){
				$ret.=urldecode(substr($str, $i, 3));
				$i+=2;
			}else{
				$ret.=$str[$i];
			}
		}
		return $ret; 
	}
	
	public static function str_str($str, $array){	//查找字符串在另一字符串中的第一次出现
		foreach((array)$array as $v){
			if(strstr($str, $v)!==false){
				return true;
			}
		}
		return false;
	}

	public static function referrer_filter($referrer){
		$share_platform=array('facebook', 'twitter', 'plus.google.com', 'pinterest', 'linkedin', 'digg', 'reddit', 'blogger', 'vk.com', 'youtube', 'instagram');
		$search_engine=array('google', 'bing', 'yahoo', 'yandex', 'baidu', 'naver', 'ask', 'haosou', 'sogou', 'youdao');
		
		//0:搜索引擎	1:分享平台 99:直接输入 100:其他
		if(!$referrer) return 99;
		foreach($share_platform as $v){if(substr_count($referrer, $v)) return 1;}
		foreach($search_engine as $v){if(substr_count($referrer, $v)) return 0;}
		return 100;
	}
	
	/********************************设置Cookie登录(start)********************************/
	public static function GetCookie($type=''){//获取Cookie
		global $c;
		$name=ly200::password($c['ApiKey']).'_'.$c['db_cfg']['database'];
		if($type){
			return $_COOKIE[$type][$name];
		}else{
			return $_COOKIE[$name];
		}
	}

	public static function SetTheCookie($type='', $value='', $expire=31536000, $path='/', $domain=''){//设置Cookie
		global $c;
		$name=ly200::password($c['ApiKey']).'_'.$c['db_cfg']['database'];//Cookie名称
		$type && $name="{$type}[{$name}]";
		$expire=$expire!=0?$expire+=$c['time']:$expire;//Cookie过期时间
		@setcookie($name, $value, $expire, $path, $domain);
	}

	public static function PwdCode($pwd){
		global $c;
		return ly200::password($_SERVER["HTTP_USER_AGENT"].$pwd.$c['ApiKey']);//将密码加密保存到Cookie
	}
	
	public static function DIST_share_url($UserId){
		global $c;
		//return urlencode(str::str_crypt(trim($UserId)."\t".ly200::password($c['Number'].$c['ApiKey'])));
		return urlencode(str::str_crypt(trim($UserId)));
	}
	
	public static function str_crypt($str, $action='encrypt', $key='www-ly200-com'){//字符串加密
		if(!$str){return;}
		$action!='encrypt' && $str=base64_decode($str);
		$keylen=strlen($key);
		$strlen=strlen($str);
		$new_str='';
		for($i=0; $i<$strlen; $i++){
			$k=$i%$keylen;
			$new_str.=$str[$i]^$key[$k];
		}
		return $action!='decrypt'?base64_encode($new_str):$new_str;
	}
	/********************************设置Cookie登录(end)********************************/
	public static function ary_filter($data, $key, $value=''){	//返回数组指定下标包含指定值的新数组
		if($value==''){return $data;};
		!is_array($key) && $key=explode('|', $key);
		!is_array($value) && $value=explode('|', $value);
		foreach($data as $k=>$v){
			$unset=false;
			foreach($key as $k1=>$v1){
				if(is_array($v[$v1])){
					$data[$k]=ary::ary_filter($v[$v1], $key, $value);
				}elseif(!in_array($v[$v1], explode(',', $value[$k1]))){
					$unset=true;
					break;
				}
			}
			if($unset){unset($data[$k]);}
		}
		return $data;
	}

	public static function str_content_crypt($str, $action='decrypt'){//内容加密解密  默认解密
		if(!$str){return;}
		if($action=='decrypt'){
			$new_str=@gzuncompress(base64_decode($str));	//接收解密
		}else{
			$new_str=@base64_encode(gzcompress($str));	//加密传递
		}
		return $new_str;
	}

	public static function jsformat($str){
		$str = trim($str);
		$str = str_replace('\\s\\s', '\\s', $str);
		$str = str_replace(chr(10), '', $str);
		$str = str_replace(chr(13), '', $str);
		$str = str_replace(array('\\', '"'), array('\\\\', '\"'), $str);
		return $str;
	}

	public static function random_user(){ //随机生成用户名
		$name_ary = array(
			'A'	=>	array('Allen' ,'Ava' ,'Andy' ,'Armstrong' ,'Arnold' ,'Adams' ,'Alston' ,'Albert' ,'Ashley' ,'Alison' ,'Adkins' ,'Anthony' ,'Amos' ,'Andrew' ,'Archer' ,'Augustine' ,'Abbott' ,'Abel' ,'Abraham' ,'Adair' ,'Aldrich' ,'Angel' ,'Abernathy' ,'Abrams' ,'Acker' ,'Ackerman' ,'Adamson' ,'Adcock' ,'Adler' ,'Alonso' ,'Ali' ,'Alonzo' ,'Angle' ,'Alger' ,'Archibald' ,'Armand' ,'August' ,'Abner' ,'Adrian' ,'Ahern' ,'Alfred' ,'Amy' ,'Abbey' ,'Abell' ,'Abercrombie' ,'Abernethy' ,'Able' ,'Abrahams' ,'Abrahamson' ,'Abram' ,'Abramson' ,'Acheson' ,'Acton' ,'Acuff' ,'Addington' ,'Addy' ,'Alfonso' ,'Allan' ,'Alton' ,'Annabelle' ,'Algernon' ,'Alva' ,'Alvin' ,'Alvis' ,'Andre' ,'Angelo' ,'Augus' ,'Ansel' ,'Antony' ,'Antoine' ,'Antonio' ,'Aries' ,'Arlen' ,'Arno' ,'Arvin' ,'Asa' ,'Ashbur' ,'Atwood' ,'Aaron' ,'Adam' ,'Adolph' ,'Adonis' ,'Alan' ,'Abigail' ,'Angela' ,'Anna' ,'Amanda' ,'Ann' ,'Alice' ,'Andrea' ,'Anne' ,'Annie' ,'Anita' ,'Amber' ,'April' ,'Audrey' ,'Annette' ,'Ana' ,'Alma' ,'Agnes' ,'Arlene' ,'Ada' ,'Angie' ,'Amelia' ,'Alberta' ,'Antoinette' ,'Angelica' ,'Adrienne' ,'Alexandra' ,'Angelina' ,'Antonia' ,'Alyssa' ,'Aalto' ,'Abadam' ,'Abbado' ,'Abbas' ,'Abbe' ,'Abbet' ,'Abbs' ,'Abby' ,'Abdey' ,'Abdie' ,'Abdul' ,'Abelard' ,'Abelson' ,'Abercromby' ,'Aberdeen' ,'Ableson' ,'Ablett' ,'Ablewhite' ,'Ablitt' ,'Ablott' ,'Abrikosor' ,'Absalom' ,'Absolom' ,'Absolon' ,'Aby' ,'Ace' ,'Achard' ,'Achebe' ,'Achilles' ,'Ackary' ,'Ackerly' ,'Ackers' ,'Ackery' ,'Acket' ,'Acketts' ,'Ackland' ,'Ackroyd' ,'Acland'),
			'B'	=>	array('Bennett' ,'Bishop' ,'Bradley' ,'Baker' ,'Bryant' ,'Bryson' ,'Baird' ,'Baldwin' ,'Barnett' ,'Barry' ,'Barton' ,'Beck' ,'Benjamin' ,'Benson' ,'Berg' ,'Bernard' ,'Bruce' ,'Ballard' ,'Bryan' ,'Barlow' ,'Baron' ,'Bartley' ,'Benedict' ,'Brandon' ,'Beverly' ,'Bain' ,'Bentley' ,'Bancroft' ,'Bart' ,'Basil' ,'Ben' ,'Bertram' ,'Bill' ,'Brian' ,'Billy' ,'Baber' ,'Bader' ,'Baily' ,'Bainbridge' ,'Beenle' ,'Barbie' ,'Bubles' ,'Bard' ,'Barret' ,'Bartholomew' ,'Beacher' ,'Beau' ,'Berger' ,'Bernie' ,'Bert' ,'Berton' ,'Bevis' ,'Bing' ,'Blair' ,'Blithe' ,'Bob' ,'Booth' ,'Borg' ,'Boris' ,'Bowen' ,'Boyce' ,'Boyd' ,'Brady' ,'Brook' ,'Bruno' ,'Buck' ,'Burgess' ,'Burke' ,'Burnell' ,'Burton' ,'Byron' ,'Barbara' ,'Betty' ,'Brenda' ,'Bonnie' ,'Beatrice' ,'Bernice' ,'Brittany' ,'Beth' ,'Bessie' ,'Brandy' ,'Billie' ,'Becky' ,'Bobbie' ,'Belinda' ,'Blanche' ,'Beulah' ,'Bridget' ,'Blanca' ,'Brooke' ,'Bernadette' ,'Betsy' ,'Baal' ,'Babbie' ,'Babette' ,'Babs' ,'Babur' ,'Bacchus' ,'Bachelor' ,'Bagot' ,'Baillie' ,'Balaam' ,'Baldie' ,'Baldrick' ,'Balfour' ,'Babbette' ,'Babsi' ,'Bailee' ,'Balbina' ,'Baljinder' ,'Balvina' ,'Bambi' ,'Barbaro' ,'Barbra' ,'Barra' ,'Baseerat' ,'Baylee' ,'Beatriz' ,'Beaulah' ,'Bebe' ,'Becca' ,'Becci' ,'Becka' ,'Beena' ,'Begona' ,'Bekki' ,'Bell' ,'Bella' ,'Bellamy' ,'Belle' ,'Belva' ,'Benedicte' ,'Benediz' ,'Benita' ,'Berenice' ,'Berkeley' ,'Bernadine' ,'Bernardine' ,'Berneice' ,'Berniece' ,'Bernita' ,'Berta' ,'Bertha' ,'Bertie' ,'Beryl' ,'Beshaun' ,'Bethel' ,'Bettie' ,'Bettina' ,'Bettyann'),
			'C'	=>	array('Carl' ,'Carlos' ,'Campbell' ,'Carroll' ,'Cole' ,'Coleman' ,'Caiden' ,'Charles' ,'Cain' ,'Caldwell' ,'Carlson' ,'Carver' ,'Clay' ,'Clayton' ,'Collier' ,'Chloe' ,'Chase' ,'Cecil' ,'Christopher' ,'Clifford' ,'Cornelius' ,'Christy' ,'Christie' ,'Calvert' ,'Carmichael' ,'Cartwright' ,'Cary' ,'Cassidy' ,'Castle' ,'Chadwick' ,'Chamberlain' ,'Chappell' ,'Clinton' ,'Connor' ,'Colton' ,'Clyde' ,'Carman' ,'Celestine' ,'Charley' ,'Charlie' ,'Columbus' ,'Cory' ,'Carson' ,'Christal' ,'Colorfully' ,'Caesar' ,'Calvin' ,'Carey' ,'Carr' ,'Carter' ,'Cash' ,'Cedric' ,'Chad' ,'Channing' ,'Chapman' ,'Chasel' ,'Chester' ,'Christ' ,'Clare' ,'Clarence' ,'Clark' ,'Claude' ,'Clement' ,'Cleveland' ,'Cliff' ,'Colbert' ,'Colby' ,'Colin' ,'Conrad' ,'Cornell' ,'Craig' ,'Curitis' ,'Cyril' ,'Carol' ,'Cynthia' ,'Carolyn' ,'Christine' ,'Catherine' ,'Cheryl' ,'Christina' ,'Crystal' ,'Connie' ,'Carmen' ,'Cindy' ,'Carrie' ,'Charlotte' ,'Clara' ,'Cathy' ,'Carla' ,'Colleen' ,'Constance' ,'Claudia' ,'Courtney' ,'Caroline' ,'Cassandra' ,'Carole' ,'Claire' ,'Cora' ,'Cecilia' ,'Candace' ,'Candice' ,'Cell' ,'Chelsea' ,'Cristina' ,'Cecelia' ,'Camille' ,'Camilla' ,'Catharine' ,'Cecily' ,'Chanel' ,'Chauncey' ,'Chuck' ,'Cicely' ,'Cinderella' ,'Claudette' ,'Claudine' ,'Cleopatra' ,'Colette' ,'Collen' ,'Candance' ,'Cybelle' ,'Cholena' ,'Carlita' ,'Carlina' ,'Conan' ,'Caton' ,'Cleon' ,'Cowan' ,'Camey' ,'Carling' ,'Carmine' ,'Carmelle' ,'Cydney' ,'Coryana' ,'Corrinne' ,'Cisca' ,'Chynna' ,'Chit' ,'Cheyli' ,'Cheree' ,'Charysse' ,'Chardonnay' ,'Chapin' ,'Cerys' ,'Cequoyah' ,'Celicia' ,'Catlyn' ,'Cathriona' ,'Carys' ,'Cammie'),
			'D'	=>	array('Davis' ,'Dick' ,'Duncan' ,'Dunn' ,'Daniel' ,'David' ,'Dean' ,'Dennis' ,'Douglas' ,'Duke' ,'Dalton' ,'Davidson' ,'Dillon' ,'Donovan' ,'Dorsey' ,'Doyle' ,'Drake' ,'Dudley' ,'Duffy' ,'Duran' ,'Dyer' ,'Dempsey' ,'Derrick' ,'Daly' ,'Darby' ,'Davies' ,'Denny' ,'Dewey' ,'Doherty' ,'Donnelly' ,'Douglass' ,'Drummond' ,'Duff' ,'Dunbar' ,'Dunham' ,'Dan' ,'Diana' ,'Don' ,'Delia' ,'Damon' ,'Dane' ,'Darwin' ,'Deane' ,'Desmond' ,'Domingo' ,'Dreamy' ,'Darcy' ,'Darnell' ,'Darren' ,'Dave' ,'Devin' ,'Dominic' ,'Donahue' ,'Donald' ,'Drew' ,'Dwight' ,'Dylan' ,'Dorothy' ,'Donna' ,'Deborah' ,'Debra' ,'Diane' ,'Doris' ,'Denise' ,'Dawn' ,'Debbie' ,'Danielle' ,'Dolores' ,'Delores' ,'Dora' ,'Deanna' ,'Dianne' ,'Daisy' ,'Della' ,'Dianna' ,'Doreen' ,'Desiree' ,'Darla' ,'Dixie' ,'Danny' ,'Dante' ,'Daphne' ,'Darcey' ,'Davina' ,'Debby' ,'Deirdre' ,'Delilah' ,'Derek' ,'Dinah' ,'Dione' ,'Dirk' ,'Dolly' ,'Dorothea' ,'Diego' ,'Dobias' ,'Dmitry' ,'Dirceu' ,'Diogo' ,'Dimitri' ,'Dimigy' ,'Dillan' ,'Diallo' ,'Dexter' ,'Devonte' ,'Devic' ,'Devendra' ,'Devante' ,'Deshawn' ,'Deshaun' ,'Derwin' ,'Derron' ,'Derrell' ,'Dermot' ,'Derico' ,'Derick' ,'Dereck' ,'Derald' ,'Deontrae' ,'Deonte' ,'Deondre' ,'Denzel' ,'Denton' ,'Denicio' ,'Demontay' ,'Demetrius' ,'DeMarcus' ,'Delon' ,'Delbert' ,'Delano' ,'Dejohn' ,'Dejan' ,'Deekay' ,'Dedric' ,'Declan' ,'Decarlos' ,'Debelen' ,'Deaux' ,'Deangelo' ,'Deandre' ,'Deakin' ,'Dazz' ,'Daylyn' ,'Davy' ,'Davonte' ,'Davon' ,'Davion' ,'Daunte' ,'Dathan' ,'Dashul' ,'Dashawn'),
			'E'	=>	array('Emma' ,'Ellis' ,'Edith' ,'Elliott' ,'Ethan' ,'Eaton' ,'Everett' ,'Edgar' ,'Elliot' ,'Early' ,'Eddy' ,'Edmond' ,'Egan' ,'Elias' ,'Ellsworth' ,'Elmore' ,'Emery' ,'Engle' ,'Ennis' ,'Ernst' ,'Ervin' ,'Erwin' ,'Elmer' ,'Elton' ,'Emmanuel' ,'Enoch' ,'Ernest' ,'Eugene' ,'Evan' ,'Evelyn' ,'Elisa' ,'Earle' ,'Eddie' ,'Elbert' ,'Elwood' ,'Emanuel' ,'Emmett' ,'Emory' ,'Erlinda' ,'Earl' ,'Ed' ,'Eden' ,'Edmund' ,'Edison' ,'Edward' ,'Edwiin' ,'Egbert' ,'Elijah' ,'Elroy' ,'Elvis' ,'Eric' ,'Everley' ,'Elizabeth' ,'Emily' ,'Edna' ,'Ethel' ,'Ellen' ,'Elaine' ,'Esther' ,'Eva' ,'Eleanor' ,'Erin' ,'Erica' ,'Elsie' ,'Eileen' ,'Ella' ,'Erika' ,'Eunice' ,'Erma' ,'Ernestine' ,'Elena' ,'Estelle' ,'Eloise' ,'Elvira' ,'Essie' ,'Elsa' ,'Ebony' ,'Eda' ,'Edwin' ,'Effie' ,'Eleanora' ,'Eleanore' ,'Elfreda' ,'Elin' ,'Elinor' ,'Elisabeth' ,'Elise' ,'Elisha' ,'Elissa' ,'Ellie' ,'Elma' ,'Elmo' ,'Eloisa' ,'Else' ,'Elva' ,'Elvin' ,'Emeline' ,'Emerald' ,'Emile' ,'Emilia' ,'Emmie' ,'Emmitt' ,'Ena' ,'Enid' ,'Ernie' ,'Errol' ,'Eve' ,'Ebone' ,'Eboni' ,'Edwige' ,'Edwina' ,'Edyth' ,'Eira' ,'Ekaterina' ,'Elaina' ,'Elba' ,'Elda' ,'Eleni' ,'Eleonore' ,'Eleri' ,'Elexis' ,'Eliana' ,'Eliane' ,'Elina' ,'Elke' ,'Elora' ,'Elvia' ,'Elvina' ,'Elysa' ,'Elyse' ,'Elyssa' ,'Emely' ,'Emilee' ,'Emileigh' ,'Emilie' ,'Emmalee' ,'Emmanuelle' ,'Emmy' ,'Emogene' ,'Enola' ,'Enriqueta' ,'Enya' ,'Eranthe' ,'Ericka' ,'Erlene' ,'Erna' ,'Eryn' ,'Esin' ,'Esmee' ,'Esmeralda'),
			'F'	=>	array( 'Ford' ,'Franklin' ,'Fisher' ,'Foster' ,'Freeman' ,'Frederica' ,'Fitch' ,'Fitzgerald' ,'Francis' ,'Frank' ,'Farley' ,'Farrell' ,'Finley' ,'Fleming' ,'Fletcher' ,'Flynn' ,'Fowler' ,'Franco' ,'Frazier' ,'Frost' ,'Fuller' ,'Fulton' ,'Floyd' ,'Frederick' ,'Felix' ,'Farrar' ,'Faust' ,'Felton' ,'Field' ,'Finn' ,'Flint' ,'Forbes' ,'Fraser' ,'Frey' ,'Fritz' ,'Funk' ,'Forrest' ,'Fabian' ,'Flora' ,'Freda' ,'Faith' ,'Felice' ,'Fernando' ,'Fairy' ,'Flower' ,'Ferdinand' ,'Frederic' ,'Florence' ,'Felicia' ,'Faye' ,'Fannie' ,'Farrah' ,'Fernanda' ,'Flavia' ,'Francesca' ,'Freddie' ,'Freddy' ,'Frieda' ,'Fabiana' ,'Fabiola' ,'Faline' ,'Fallon' ,'Farah' ,'Farisa' ,'Fatima' ,'Fayetta' ,'Febe' ,'Felecia' ,'Felicity' ,'Femke' ,'Ffion' ,'Filipa' ,'Fiona' ,'Florine' ,'Francine' ,'Freya' ,'Frida' ,'Fritzi' ,'Fabrice' ,'Fabrizio' ,'Facundo' ,'Fahad' ,'Fahim' ,'Faizal' ,'Fajar' ,'Farhaad' ,'Federico' ,'Feicien' ,'Felipe' ,'Filimon' ,'Filipe' ,'Flavio' ,'Flemming' ,'Flex' ,'Florentino' ,'Florian' ,'Follis' ,'Fortunato' ,'Francisco' ,'Francois' ,'Frans' ,'Franz' ,'Fredrick' ,'Fredy' ,'Freestone' ,'Frery' ,'Fanny' ,'Fermin' ,'Finlay' ,'Fadwa' ,'Fawne' ,'Faye' ,'Filomena' ,'Fleur' ,'Floor' ,'Flory' ,'Francisca' ,'Franka' ,'Frosina' ,'Fremont' ,'Fontane'),
			'G'	=>	array('Gordon' ,'Gibson' ,'Graham' ,'Grant' ,'George' ,'Gilbert' ,'Giles' ,'Griffith' ,'Guy' ,'Gregory' ,'Gallagher' ,'Galloway' ,'Garrett' ,'Garrison' ,'Greer' ,'Guzman' ,'Grayson' ,'Gabriel' ,'Gary' ,'Gavin' ,'Grover' ,'Grace' ,'Gage' ,'Garnett' ,'Godfrey' ,'Godwin' ,'Gore' ,'Granger' ,'Gregg' ,'Gunter' ,'Gabriella' ,'Gerald' ,'Gloria' ,'Gayle' ,'Garfield' ,'Garth' ,'Gerard' ,'Gerry' ,'Gillian' ,'Glinda' ,'Greenle' ,'Gale' ,'Geoffrey' ,'Geoff' ,'Glenn' ,'Godfery' ,'Greg' ,'Gregary' ,'Gustave' ,'Gladys' ,'Geraldine' ,'Gertrude' ,'Gina' ,'Georgia' ,'Glenda' ,'Gwendolyn' ,'Geneva' ,'Genevieve' ,'Ginger' ,'Gretchen' ,'Gwen' ,'Galen' ,'Gaye' ,'Gemma' ,'Georgie' ,'Germaine' ,'Giovanna' ,'Goldie' ,'Greta' ,'Gwyneth' ,'Garrick' ,'Gabriela' ,'Gabrielle' ,'Gaby' ,'Gaia' ,'Galilea' ,'Genesis' ,'Genica' ,'Genna' ,'Georgette' ,'Georgina' ,'Gerri' ,'Gia' ,'Giada' ,'Gianna' ,'Gigi' ,'Gilda' ,'Gilmore' ,'Giorgia' ,'Gisela' ,'Giselle' ,'Gisselle' ,'Giulietta' ,'Glory' ,'Glynis' ,'Goldia' ,'Gracie' ,'Graciela' ,'Gretel' ,'Griselda' ,'Gaelen' ,'Gaetan' ,'Gaige' ,'Galo' ,'Gambit' ,'Gareth' ,'Garett' ,'Garic' ,'Garon' ,'Garry' ,'Garza' ,'Gaston' ,'Gaven' ,'Gavyn' ,'Genaro' ,'Geraldo' ,'Gerardo' ,'Gerasimos' ,'Gerd' ,'German' ,'Gezi' ,'Ghenadie' ,'Giam' ,'Giancarlo' ,'Gianfranco' ,'Gideon' ,'Gijs' ,'Gilberto' ,'Gino' ,'Giovani' ,'Giovanni' ,'Giovanny' ,'Gligorea' ,'Gman' ,'Goncalo' ,'Gonzalo' ,'Gradus' ,'Grady' ,'Grandberry' ,'Graydon' ,'Greydon' ,'Greyson' ,'Grondall' ,'Guero' ,'Guilherme' ,'Guillermo' ,'Gunnar' ,'Gunner' ,'Gurneev' ,'Guru'),
			'H'	=>	array('Harris' ,'Henry' ,'Howard' ,'Hamilton' ,'Harrison' ,'Hayes' ,'Hale' ,'Haley' ,'Hardy' ,'Hayden' ,'Herman' ,'Hogan' ,'Humphrey' ,'Hancock' ,'Hanson' ,'Harding' ,'Henson' ,'Hoffman' ,'Holland' ,'Holt' ,'Houston' ,'Harlan' ,'Harley' ,'Harry' ,'Herbert' ,'Hyman' ,'Hannah' ,'Hamlin' ,'Hammer' ,'Hand' ,'Harden' ,'Hartley' ,'Hastings' ,'Hawthorne' ,'Haywood' ,'Healy' ,'Heller' ,'Henley' ,'Hilliard' ,'Hilton' ,'Holbrook' ,'Holley' ,'Hailey' ,'Harold' ,'Halley' ,'Hans' ,'Homer' ,'Hosea' ,'Hamiltion' ,'Harriet'),
			'I'	=>	array('Isabella' ,'Isla' ,'Ingram' ,'Isaac' ,'Inman' ,'Irvin' ,'Irving' ,'Irwin' ,'Israel' ,'Ivory' ,'Ivy' ,'Icey' ,'Ian' ,'Ingemar' ,'Ira' ,'Isidore' ,'Ivan' ,'Ives' ,'Irene' ,'Ida' ,'Irma' ,'Isabel' ,'Iris' ,'Inez' ,'Ike' ,'Ileana' ,'Ilse' ,'Imogene' ,'Ines' ,'Ingeborg' ,'Inger' ,'Ingrid' ,'Iona' ,'Isadora' ,'Isaiah' ,'Isis' ,'Ismael' ,'Ishtar' ,'Iesha' ,'Ilene' ,'Iliana' ,'Ilona' ,'Imelda' ,'Ina' ,'India' ,'Inka' ,'Inmaculada' ,'Iola' ,'Irine' ,'Irit'),
			'J'	=>	array('Jackson' ,'Johnson' ,'James' ,'Jeanne' ,'Joyce' ,'Jarvis' ,'Jefferson' ,'Jacob' ,'Jeffrey' ,'John' ,'Julian' ,'Joy' ,'Jacques' ,'Jameson' ,'Jarrett' ,'Jeffery' ,'Jewell' ,'Jordon' ,'Jace' ,'Jessie' ,'Jason' ,'Jay' ,'Jerry' ,'Jim' ,'Jonas' ,'Joshua' ,'Julius' ,'Justin' ,'Judy' ,'June' ,'Jeannie' ,'Jose' ,'Joe' ,'Jayne' ,'Jesus' ,'Jone' ,'Johnny' ,'Jase' ,'Jodie' ,'Janice' ,'Jack' ,'Jared' ,'Jeff' ,'Jeremy' ,'Jerome' ,'Jonathan' ,'Joseph' ,'Jennifer' ,'Jessica' ,'Janet' ,'Julie' ,'Joan' ,'Judith' ,'Jane' ,'Jacqueline' ,'Julia' ,'Josephine' ,'Juanita' ,'Joanne ' ,'Jill' ,'Joann' ,'Jeanette' ,'Jo' ,'Jennie' ,'Jenny' ,'Joanna' ,'Jodi' ,'Janie' ,'Juana' ,'Jeannette' ,'Jacquelyn' ,'Johnnie' ,'Jasmine' ,'Jana' ,'Jenna' ,'Josefina' ,'Johanna' ,'Jaime' ,'Juan' ,'Jacquetta' ,'Jake' ,'Janetta' ,'Janey' ,'Jaqueline' ,'Jarred' ,'Jarrod' ,'Jed' ,'Jeffry' ,'Jenifer' ,'Jerrold' ,'Jewel' ,'Jillian' ,'Jimmie' ,'Jimmy' ,'Jocelyn' ,'Joel' ,'Joie' ,'Jonah' ,'Josiah' ,'Josie' ,'JJ' ,'Jaana' ,'Jacalyn' ,'Jacinda' ,'Jackeline' ,'Jacklyn' ,'Jaclyn' ,'Jacobi' ,'Jadzia' ,'Jaida' ,'Jailyn' ,'Jakayla' ,'Jalyn' ,'Jalynn' ,'Janae' ,'Janaya' ,'Janeen' ,'Janelle' ,'Janessa' ,'Janette' ,'Janine' ,'Janneke' ,'Jasmina' ,'Jasmyn' ,'Jaycee' ,'Jayda' ,'Jayde' ,'Jaye' ,'Jayla' ,'Jaylah' ,'Jayna' ,'Jazlyn' ,'Jazmin' ,'Jazmine' ,'Jazmyn' ,'Jazmyne' ,'Jeanna' ,'Jeannine' ,'Jelena' ,'Jena' ,'Jenelle' ,'Jenessa' ,'Jennessa' ,'Jeri' ,'Jerri' ,'Jerrica' ,'Jerrie' ,'Jerusha' ,'Jesseka' ,'Jessenia'),
			'K'	=>	array('King' ,'Kevin' ,'Kelley' ,'Knight' ,'Khaleesi' ,'Kent' ,'Kerr' ,'Kirk' ,'Keith' ,'Kane' ,'Kemp' ,'Key' ,'Kirby' ,'Klein' ,'Knox' ,'Kyle' ,'Kay' ,'Kearney' ,'Keen' ,'Kendrick' ,'Kenney' ,'Kenny' ,'Kern' ,'Kimbrough' ,'Kincaid' ,'Kinsey' ,'Kirkland' ,'Karl' ,'Kaye' ,'Ken' ,'Kennedy' ,'Kenneth' ,'Kerwin' ,'Karen' ,'Kathleen' ,'Katherine' ,'Kathy' ,'Kathryn' ,'Katie' ,'Kristen' ,'Kristin' ,'Kristina' ,'Katrina' ,'Kayla' ,'Kristine' ,'Kristy' ,'Kelli' ,'Kara' ,'Krista' ,'Kendra' ,'Krystal' ,'Kari' ,'Kerry' ,'Kate' ,'Kellie' ,'Kristie' ,'Kaley' ,'Karan' ,'Karin' ,'Karla' ,'Karol' ,'Katharine' ,'Kathie' ,'Katy' ,'Keely' ,'Kelvin' ,'Kendal' ,'Kenna' ,'Kenton' ,'Kenyatta' ,'Kermit' ,'Kimberley' ,'Kimberly' ,'Kirsten' ,'Kit' ,'Kittie' ,'Kitty' ,'Kennard' ,'Kaitlyn' ,'Kiara' ,'Kaci' ,'Kacie' ,'Kaela' ,'Kaelyn' ,'Kaia' ,'Kail' ,'Kaila' ,'Kailee' ,'Kailey' ,'Kailyn' ,'Kaitlan' ,'Kaitleen' ,'Kaitlin' ,'Kaitlynn' ,'Kaiya' ,'Kajal' ,'Kala' ,'Kaleigh' ,'Kaliyah' ,'Kallie' ,'Kalyn' ,'Kamryn' ,'Kandi' ,'Kandie' ,'Karel' ,'Karima' ,'Karina' ,'Karis' ,'Karissa' ,'Karlee' ,'Karlene' ,'Karley' ,'Karli' ,'Karlie' ,'Karly' ,'Karma' ,'Karolyn' ,'Karyn' ,'Kasandra' ,'Kasi' ,'Kasia' ,'Kassandra' ,'Kassidy' ,'Katarina' ,'Katelin' ,'Katelyn' ,'Katelynn' ,'Katerina' ,'Katia' ,'Katina' ,'Katja' ,'Katrien' ,'Katya' ,'Kaya' ,'Kaylah' ,'Kaylee' ,'Kayleigh' ,'Kayley' ,'Kayli' ,'Kaylie' ,'Kaylyn' ,'Kaylynn' ,'Keanna' ,'Keara' ,'Keila' ,'Keisha' ,'Kelsi' ,'Kelsie' ,'Kenia' ,'Kerenza'),
			'L'	=>	array('Lewis' ,'Lawrence' ,'Lilith' ,'Larissa' ,'Lambert' ,'Leonard' ,'Lester' ,'Lora' ,'Lang' ,'Lara' ,'Larson' ,'Leon' ,'Lloyd' ,'Lucas' ,'Lance' ,'Louis' ,'Luther' ,'Lyle' ,'Lacey' ,'Lacy' ,'Ladd' ,'Laird' ,'Lange' ,'Langston' ,'Larkin' ,'Latham' ,'Lawler' ,'Lay' ,'Layne' ,'Layton' ,'Libby' ,'Lilly' ,'Lincoln' ,'Linn' ,'Landon' ,'Liam' ,'Lorenzo' ,'Larry' ,'Leo' ,'Levi' ,'Lucy' ,'Lillie' ,'Lamont' ,'Laurence' ,'Leland' ,'Lenard' ,'Leroy' ,'Luis' ,'Leif' ,'Len' ,'Lennon' ,'Leopold' ,'Les' ,'Lionel' ,'Lucien' ,'Lyndon' ,'Linda' ,'Lisa' ,'Laura' ,'Lori' ,'Louise' ,'Lois' ,'Lillian' ,'Lucille' ,'Lauren' ,'Lorraine' ,'Loretta' ,'Laurie' ,'Lydia' ,'Lena' ,'Leah' ,'Leona' ,'Lindsey' ,'Lindsay' ,'Lynda' ,'Luz' ,'Lula' ,'Lola' ,'Latoya' ,'Lynne' ,'Leticia' ,'Lynette' ,'Laverne' ,'Lorena' ,'Lila' ,'Lana' ,'Lorene' ,'Lucia' ,'Lela' ,'Lanny' ,'Latonia' ,'Laurel' ,'Lauretta' ,'Laurinda' ,'Lavinia' ,'Lean' ,'Leda' ,'Leila' ,'Leilani' ,'Lemuel' ,'Lennie' ,'Lenny' ,'Lenora' ,'Lenore' ,'Leonie' ,'Leonora' ,'Leonore' ,'Letitia' ,'Lettie' ,'Letty' ,'Lili' ,'Lily' ,'Lina' ,'Lindy' ,'Linsey' ,'Ladonna' ,'Lashay' ,'Lachelle' ,'Lacie' ,'Laila' ,'Laine' ,'Lainey' ,'Lakeisha' ,'LaKeydra' ,'Lakita' ,'Lal' ,'Laney' ,'Lanita' ,'LaQuisha' ,'Laquita' ,'Larisa' ,'Latifah' ,'Latika' ,'Latina' ,'Latisha' ,'Latricia' ,'Lauran' ,'Laureen' ,'Lauryn' ,'Lavina' ,'Lavon' ,'Lavonne' ,'Lawanda' ,'Layla' ,'Layna' ,'Leann' ,'Leala' ,'Leandra' ,'Leanna' ,'Leanne'),
			'M'	=>	array('Martin' ,'Moore' ,'Mary' ,'Marshall' ,'Murphy' ,'Murray' ,'Mason' ,'Mitchell' ,'Morris' ,'Moon' ,'Marsh' ,'Maxwell' ,'Michael' ,'Miles' ,'Morton' ,'Moses' ,'May' ,'MacDonald' ,'Mack' ,'Maddox' ,'Mann' ,'Mathews' ,'Maynard' ,'Magee' ,'Malcolm' ,'Marcus' ,'Mark' ,'Marvin' ,'Matthew' ,'Mahoney' ,'Major' ,'Mallory' ,'Malloy' ,'Maloney' ,'Manley' ,'Mansfield' ,'Manuel' ,'Marin' ,'Marquis' ,'Mayfield' ,'Maria' ,'Matt' ,'Maurice' ,'Max' ,'Mike' ,'Milo' ,'Melinda' ,'Mercedes' ,'Macy' ,'Malcom' ,'Marcellus' ,'Marlin' ,'Marvel' ,'Mathew' ,'Mya' ,'Magical' ,'Mandel' ,'Marico' ,'Marlon' ,'Maximilian' ,'Merlin' ,'Michell' ,'Mick' ,'Montague' ,'Mortimer' ,'Myron' ,'Madison' ,'Michelle' ,'Melissa' ,'Martha' ,'Marie' ,'Mildred' ,'Marilyn' ,'Marjorie' ,'Monica' ,'Marion' ,'Melanie' ,'Maureen' ,'Marcia' ,'Minnie' ,'Marlene' ,'Marian' ,'Maxine' ,'Mabel' ,'Marsha' ,'Margie' ,'Miriam' ,'Misty' ,'Mae' ,'Margarita' ,'Marguerite' ,'Molly' ,'Madeline' ,'Monique' ,'Maggie' ,'Maryann' ,'Melody' ,'Mamie' ,'Marianne' ,'Myra' ,'Marcella' ,'Mona' ,'Meghan' ,'Mindy' ,'Mandy' ,'Mirana' ,'Marta' ,'Mac' ,'Madeleine' ,'Madge' ,'Madonna' ,'Magda' ,'Magdalen' ,'Magnolia' ,'Maisie' ,'Malvina' ,'Margaret' ,'Marge' ,'Margery' ,'Margot' ,'Mariana' ,'Marietta' ,'Marina' ,'Marjory' ,'Marnie' ,'Mathilda' ,'Matilda' ,'Maud' ,'Maude' ,'Maura' ,'Mavis' ,'Michaela' ,'Macarena' ,'Machelle' ,'Maci' ,'Madai' ,'Madalena' ,'Madalyn' ,'Madalynn' ,'Maddison' ,'Madelyn' ,'Madelyne' ,'Madelynn' ,'Madilyn' ,'Madisen' ,'Madisyn' ,'Madyson' ,'Maegan' ,'Maeve' ,'Mafalda'),
			'N'	=>	array('Nelson' ,'Noah' ,'Natasha' ,'Newman' ,'Norman' ,'Norton' ,'Natalie' ,'Nash' ,'Neal' ,'Newton' ,'Nixon' ,'Noble' ,'Nolan' ,'Norris' ,'Neil' ,'Nicholas' ,'Naylor' ,'Neely' ,'Neville' ,'Newell' ,'Norwood' ,'Nathan' ,'Nathaniel' ,'Nick' ,'Napoleon' ,'Nestor' ,'Nicolas' ,'Normand' ,'Nancy' ,'Nat' ,'Nigel' ,'Nicole' ,'Norma' ,'Nellie' ,'Nora' ,'Nina' ,'Naomi' ,'Nadine' ,'Nettie' ,'Nana' ,'Nanette' ,'Nannie' ,'Natalia' ,'Nathalie' ,'Nathanael' ,'Ned' ,'Nelly' ,'Nicola' ,'Nicolette' ,'Nicolle' ,'Nita' ,'Nola' ,'Nona' ,'Norah' ,'Noreen' ,'Norene' ,'Nabila' ,'Nadia' ,'Nafeeza' ,'Nailah' ,'Nalani' ,'Nallely' ,'Nanci' ,'Nancie' ,'Nani' ,'Nannette' ,'Narcisa' ,'Narelle' ,'Nasrin' ,'Nataly' ,'Natalya' ,'Nathalia' ,'Nathaly' ,'Naya' ,'Nayeli' ,'Nayely' ,'Neha' ,'Neli' ,'Nerissa' ,'Nesha' ,'Nessa' ,'Neta' ,'Neva' ,'Nevaeh' ,'Nia' ,'Nichelle' ,'Nicki' ,'Nickole' ,'Nicky' ,'Nidia' ,'Nieves' ,'Niki' ,'Nikka' ,'Nikki' ,'Nikola' ,'Ninon' ,'Nisa' ,'Nitsa' ,'Noelia' ,'Noemi' ,'Noga' ,'Nokomis' ,'Noriko' ,'Nour' ,'Nubia' ,'Nuria' ,'Nya' ,'Nyah' ,'Nyasia' ,'Nyla' ,'Nyree' ,'Nadav' ,'Nadir' ,'Nafis' ,'Nahoom' ,'Naku' ,'Namit' ,'Nanda' ,'Naquay' ,'Narain' ,'Narong' ,'Nasir' ,'Nassef' ,'Nate' ,'Nathanial' ,'Nathen' ,'Neath' ,'Nehemiah' ,'Nery' ,'Nickolas' ,'Nikhil' ,'Nikolas' ,'Niles' ,'Nils' ,'Nokovic' ,'Nollie' ,'Norberto' ,'Noriel' ,'Nuen' ,'Nunez' ,'Nuno' ,'Nyle' ,'Nacho' ,'Najae' ,'Nando' ,'Nassim' ,'Neandro' ,'Neelix' ,'Negro' ,'Nemanja'),
			'O'	=>	array('Olivia' ,'Oliver' ,'Owen' ,'Osborn' ,'Olga' ,'Odom' ,'Ogden' ,'Otto' ,'Oakes' ,'Oakley' ,'Orlando' ,'Oscar' ,'Omar' ,'Orville' ,'Osmond' ,'Oswald' ,'Otis' ,'Opal' ,'Octavia' ,'Odette' ,'Olympia' ,'Ophelia' ,'Orval' ,'Odalys' ,'Oksana' ,'Olimpia' ,'Oliviana' ,'Omaira' ,'Orietta' ,'Orlina' ,'Obed' ,'Octavio' ,'Oden' ,'Ody' ,'Oggy' ,'Oleg' ,'Oluwatosin' ,'Omari' ,'Omarian' ,'Omarion' ,'Ompaa' ,'Osa' ,'Osamah' ,'Osvaldo' ,'Oswaldo' ,'Ottis' ,'Olina' ,'Oprah' ,'Obidinma' ,'Olee'),
			'P'	=>	array('Parker' ,'Payne' ,'Perry' ,'Porter' ,'Palmer' ,'Peterson' ,'Phillips' ,'Powell' ,'Page' ,'Patrick' ,'Paul' ,'Parrish' ,'Patton' ,'Pearson' ,'Petty' ,'Phelps' ,'Pollard' ,'Pope' ,'Potter' ,'Prescott' ,'Padgett' ,'Paige' ,'Parr' ,'Patten' ,'Paz' ,'Pearce' ,'Phipps' ,'Pierre' ,'Pierson' ,'Pike' ,'Piper' ,'Platt' ,'Pollock' ,'Pete' ,'Peter' ,'Philip' ,'Pearl' ,'Patty' ,'Patti' ,'Penney' ,'Percy' ,'Phillip' ,'Polly' ,'Paddy' ,'Phil' ,'Primo' ,'Patricia' ,'Pamela' ,'Phyllis' ,'Paula' ,'Peggy' ,'Pauline' ,'Patsy' ,'Penny' ,'Priscilla' ,'Pam' ,'Paulette' ,'Pandora' ,'Pansy' ,'Pattie' ,'Paulina' ,'Peggie' ,'Penelope' ,'Pennie' ,'Phillis' ,'Philomena' ,'Phoebe' ,'Porsche' ,'Pacey' ,'Pada' ,'Pallavi' ,'Paloma' ,'Paola' ,'Patia' ,'Patrice' ,'Perla' ,'Perri' ,'Petra' ,'Phylicia' ,'Phylis' ,'Piia' ,'Pilar' ,'Pocahontas' ,'Pollyanna' ,'Pooja' ,'Portia' ,'Precious' ,'Primrose' ,'Princess' ,'Priscila' ,'Priya' ,'Pablo' ,'Papo' ,'Pascal' ,'Paulo' ,'Pavee' ,'Pedro' ,'Peejay' ,'Peng' ,'Pepper'),
			'Q'	=>	array('Quinton' ,'Quentin' ,'Quennel' ,'Quintion' ,'Queenie' ,'Quintin'),
			'R'	=>	array('Rose' ,'Rupert' ,'Russell' ,'Reed' ,'Ross' ,'Randolph' ,'Raymond' ,'Regan' ,'Richard' ,'Roy' ,'Rosa' ,'Roxanne' ,'Ramsey' ,'Randall' ,'Reese' ,'Reeves' ,'Reid' ,'Reilly' ,'Rhodes' ,'Rivers' ,'Rodgers' ,'Rollins' ,'Roman' ,'Robert' ,'Rock' ,'Ruth' ,'Ralph' ,'Raines' ,'Rankin' ,'Ransom' ,'Redding' ,'Redmond' ,'Rhea' ,'Ring' ,'Ritchie' ,'Roe' ,'Rachel' ,'Rex' ,'Robin' ,'Roderick' ,'Rodney' ,'Ruby' ,'Roger' ,'Rae' ,'Raleigh' ,'Randell' ,'Raphael' ,'Raven' ,'Rey' ,'Richie' ,'Rick' ,'Rodger' ,'Rolf' ,'Rolland' ,'Romeo' ,'Raja' ,'Ralap' ,'Reg' ,'Reginald' ,'Reuben' ,'Rod' ,'Ron' ,'Ronald' ,'Rory' ,'Rudolf' ,'Rebecca' ,'Rita' ,'Rhonda' ,'Regina' ,'Roberta' ,'Rosemary' ,'Ramona' ,'Rosie' ,'Rosalie' ,'Rosemarie' ,'Rochelle' ,'Raquel' ,'Randy' ,'Randal' ,'Raye' ,'Reba' ,'Rebekah' ,'Reggie' ,'Rena' ,'Renata' ,'Rhoda' ,'Ricarda' ,'Robbie' ,'Roma' ,'Romaine' ,'Rachal' ,'Rachelle' ,'Raegan' ,'Rafaela' ,'Raija' ,'Raina' ,'Raiza' ,'Raman' ,'Rani' ,'Rania' ,'Rashida' ,'Raya' ,'Rayanna' ,'Raychelle' ,'Raylene' ,'Rayna' ,'Rayne' ,'Reanna' ,'Rebeca' ,'Rebekka' ,'Reet' ,'Reina' ,'Renie' ,'Reno' ,'Reta' ,'Reyna' ,'Rhianna' ,'Rhiannon' ,'Rikki' ,'Ris' ,'Rizpah' ,'Rizza' ,'Robynne' ,'Romina' ,'Ronda' ,'Rosalyn' ,'Rosanna' ,'Rosanne' ,'Rosetta' ,'Rosina' ,'Rosita' ,'Rowena' ,'Roxana' ,'Roxane' ,'Roxanna' ,'Roz' ,'Rute' ,'Ruxandra' ,'Ryann' ,'Ryanna' ,'Ryleigh' ,'Radou' ,'Radu' ,'Raffaele' ,'Rahul' ,'Raje' ,'Ramdas' ,'Ramesh' ,'Ramiles' ,'Ramiro'),
			'S'	=>	array('Sophie' ,'Scott' ,'Spencer' ,'Sophia' ,'Stewart' ,'Samantha' ,'Sampson' ,'Simon' ,'Solomon' ,'Stanley' ,'Steve' ,'Sanford' ,'Sawyer' ,'Sellers' ,'Sexton' ,'Shelton' ,'Shepard' ,'Shepherd' ,'Sheppard' ,'Sherman' ,'Sofia' ,'Samuel' ,'Stanford' ,'Steward' ,'Shirley' ,'Silvia' ,'Sage' ,'Sanderson' ,'Scales' ,'Sewell' ,'Seymour' ,'Sheehan' ,'Sheffield' ,'Sheldon' ,'Sheridan' ,'Sherwood' ,'Shipley' ,'Simms' ,'Sinclair' ,'Sam' ,'Steven' ,'Sharon' ,'Sylvia' ,'Stacy' ,'Stella' ,'Shelly' ,'Stephen' ,'Saul' ,'Scarlett' ,'Seth' ,'Shane' ,'Silas' ,'Sara' ,'Snowy' ,'Silverdew' ,'Star' ,'Sweety' ,'Saxon' ,'Sean' ,'Sebastian' ,'Sid' ,'Silvester' ,'Stan' ,'Sandra' ,'Sarah' ,'Stephanie' ,'Sherry' ,'Sheila' ,'Sally' ,'Sue' ,'Stacey' ,'Sonia' ,'Sherri' ,'Sheryl' ,'Sabrina' ,'Sonya' ,'Susie' ,'Shelia' ,'Sheri' ,'Sadie' ,'Sonja' ,'Shari' ,'Shawna' ,'Sabina' ,'Sabine' ,'Sallie' ,'Salome' ,'Sammie' ,'Sammy' ,'Scarlet' ,'Selena' ,'Selene' ,'Selina' ,'Selma' ,'Shera' ,'Shona' ,'Sibyl' ,'Sigrid' ,'Saba' ,'Sable' ,'Sachi' ,'Sade' ,'Sadia' ,'Saffron' ,'Saige' ,'Salma' ,'Samara' ,'Samia' ,'Samira' ,'Sana' ,'Sanchia' ,'Sandrine' ,'Sanne' ,'Sapphire' ,'Sarahi' ,'Sarai' ,'Saretta' ,'Sarina' ,'Sarita' ,'Saskia' ,'Savana' ,'Savanah' ,'Savanna' ,'Savannah' ,'Seanna' ,'Sela' ,'Serene' ,'Serenity' ,'Shaina' ,'Shaira' ,'Shakira' ,'Shakyra' ,'Shalana' ,'Shalimar' ,'Shalyse' ,'Shameka' ,'Shamika' ,'Shana' ,'Shanae' ,'Shanda' ,'Shandi' ,'Shandra' ,'Shani' ,'Shania' ,'Shanice' ,'Shaniya' ,'Shantel' ,'Shantell' ,'Shara' ,'Sharyn'),
			'T'	=>	array('Tami' ,'Todd' ,'Tyler' ,'Tanner' ,'Tate' ,'Terrell' ,'Townsend' ,'Travis' ,'Tobias' ,'Talbot' ,'Tatum' ,'Teague' ,'Temple' ,'Thorne' ,'Thorpe' ,'Thurman' ,'Thurston' ,'Titus' ,'Tobin' ,'Trent' ,'Tripp' ,'Tyson' ,'Theodore' ,'Tiffany' ,'Timothy' ,'Tom' ,'Troy' ,'Truman' ,'Tandy' ,'Tomas' ,'Tankard' ,'Tab' ,'Ted' ,'Ternence' ,'Theobald' ,'Thomas' ,'Tim' ,'Toby' ,'Tony' ,'Tyrone' ,'Teresa' ,'Theresa' ,'Tammy' ,'Tina' ,'Thelma' ,'Tara' ,'Terri' ,'Tonya' ,'Tamara' ,'Tanya' ,'Tracey' ,'Toni' ,'Traci' ,'Teri' ,'Tricia' ,'Tasha' ,'Tabitha' ,'Tammie' ,'Tania' ,'Teddy' ,'Terrance' ,'Terrence' ,'Tess' ,'Tessa' ,'Tessie' ,'Thad' ,'Thaddeus' ,'Thalia' ,'Thea' ,'Theodora' ,'Theresia' ,'Thomasina' ,'Tilda' ,'Tillie' ,'Torrie' ,'Trevor' ,'Tristan' ,'Trudy' ,'Tabatha' ,'Tabea' ,'Tahnee' ,'Taina' ,'Taisha' ,'Taleen' ,'Talia' ,'Talina' ,'Talisha' ,'Talitha' ,'Taliyah' ,'Tallulah' ,'Tamah' ,'Tamatha' ,'Tameka' ,'Tamia' ,'Tamika' ,'Tamsen' ,'Tamsin' ,'Tamzin' ,'Tana' ,'Tangi' ,'Tanisha' ,'Taniya' ,'Tanja' ,'Taryn' ,'Tatiana' ,'Tatianna' ,'Tatjana' ,'Tatyana' ,'Tawanna' ,'Tawny' ,'Taya' ,'Tayla' ,'Taysia' ,'Tegan' ,'Telma' ,'Tennille' ,'Tera' ,'Teyana' ,'Therese' ,'Tia' ,'Tiana' ,'Tianna' ,'Tiara' ,'Tierney' ,'Tierra' ,'Tiffani' ,'Tirzah' ,'Tomara' ,'Tonia' ,'Tori' ,'Toya' ,'Tracie' ,'Trang' ,'Trina' ,'Trish' ,'Trixie' ,'Tryna' ,'Twila' ,'Tyla' ,'Tyra' ,'Taber' ,'Tad' ,'Taj' ,'Tajon' ,'Tamaitikoha' ,'Tamaris' ,'Tambor' ,'Tampe' ,'Tapasvi' ,'Tarek'),
			'U'	=>	array('Ulysses' ,'Upton' ,'Uriah' ,'Una' ,'Ursula'),
			'V'	=>	array('Vance' ,'Vera' ,'Vernon' ,'Vinson' ,'Victor' ,'Virgil' ,'Victoria' ,'Viola' ,'Vicente' ,'Van' ,'Verne' ,'Vic' ,'Vito' ,'Vivian' ,'Virginia' ,'Valerie' ,'Veronica' ,'Vanessa' ,'Vicki' ,'Vickie' ,'Velma' ,'Violet' ,'Verna' ,'Vicky' ,'Valeria' ,'Valery' ,'Venus' ,'Verena' ,'Vesta' ,'Vida' ,'Valencia' ,'Valentina' ,'Valorie' ,'Vanda' ,'Vanesa' ,'Vania' ,'Varsha' ,'Veena' ,'Veer' ,'Vena' ,'Verity' ,'Veronique' ,'Vesna' ,'Vien' ,'Vijaya' ,'Vikki' ,'Vilma' ,'Viorica' ,'Viviana' ,'Vadim'),
			'W'	=>	array('Walker' ,'Wilson' ,'Ward' ,'Webb' ,'Warren' ,'Washington' ,'Watkins' ,'West' ,'Wheeler' ,'Williamson' ,'Willis' ,'Wallace' ,'Wade' ,'Walter' ,'Warner' ,'Webster' ,'William' ,'Waller' ,'Walton' ,'Ware' ,'Watts' ,'Weber' ,'Whitehead' ,'Wilder' ,'Wilkinson' ,'Witt' ,'Wolfe' ,'Wilbur' ,'Winston' ,'Winifred' ,'Waite' ,'Walden' ,'Waldron' ,'Washburn' ,'Watt' ,'Webber' ,'Weldon' ,'Wesley' ,'Westbrook' ,'Weston' ,'Whitfield' ,'Whitlock' ,'Whitmore' ,'Whittaker' ,'Willard' ,'Willoughby' ,'Winslow' ,'Wayne' ,'Wendell' ,'Woodrow'),
			'X'	=>	array('Xavier' ,'Xenia'),
			'Y'	=>	array('Young' ,'York' ,'Yates' ,'Yolanda' ,'Yvette' ,'Yilia' ,'Yale' ,'Yehudi' ,'Yves' ,'Yvonne' ,'Yolande'),
			'X'	=>	array('Zachary' ,'Zack' ,'Zoey' ,'Zebulon' ,'Zachariah' ,'Zandra' ,'Zena' ,'Zenia' ,'Zenobia' ,'Zola' ,'Zoe' ,'Zara' ,'Zona' ,'Zora')
		);
		$str='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$key = @substr($str, rand(0,strlen($str)-1), 1);
		$frist = mt_rand(0, count($name_ary[$key])-1);
		$username = $name_ary[$key][$frist];
		return $username;
	}

}
?>