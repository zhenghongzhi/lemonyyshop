<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class sftp{

	public static function put($config, $source, $path)
	{
		global $c;
		
		set_include_path(get_include_path() . PATH_SEPARATOR . $c['root_path'] . '/inc/class/sftp');
		var_dump();
		require_once('Net/SFTP.php');
		require_once('Crypt/RSA.php');

		$sftp = new Net_SFTP($config['host']);
		$Key = new Crypt_RSA();
		// Next load the private key using file_gets_contents to retrieve the key
		$Key->loadKey(file_get_contents($config['key_path']));
		if (!$sftp->login($config['user'], $Key)) {
			exit('Login Failed');
		}

		// puts a three-byte file named filename.remote on the SFTP server
		// $sftp->put('filename.remote', 'xxx');
		// puts an x-byte file named filename.remote on the SFTP server,
		// where x is the size of filename.local
		$sftp->put($path, $source, NET_SFTP_LOCAL_FILE);
	}
}
?>