<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
// $dd=ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, Narrow'));
class ly200{
	public static function ad($Id){//广告图样式
		global $c;
		$themes=$c['manage']?$c['manage']['web_themes']:$c['theme'];
		$lang=substr($c['lang'], 1);
		$ad_row=db::get_one('ad', "Themes='$themes' and Number=$Id");
		$width=$ad_row['Width']?$ad_row['Width'].'px':'auto';
		$height=$ad_row['Height']?$ad_row['Height'].'px':'auto';
		$ad_contents="<div style='overflow:hidden;'>";
		if($ad_row['AdType']==0){
			$ad_ary=array();
			for($i=0; $i<$ad_row['PicCount']; ++$i){
				$ad_ary['Name'][$i]=str::str_code(str::json_data($ad_row['Name_'.$i], 'decode'));
				$ad_ary['Brief'][$i]=str::str_code(str::json_data($ad_row['Brief_'.$i], 'decode'));
				$ad_ary['Url'][$i]=str::str_code(str::json_data($ad_row['Url_'.$i], 'decode'));
				$ad_ary['PicPath'][$i]=str::str_code(str::json_data($ad_row['PicPath_'.$i], 'decode'));
			}
			if($ad_row['PicCount']==1){
				$ad_contents="<div>";
				$ad_ary['Url'][0][$lang] && $ad_contents.="<a href='{$ad_ary['Url'][0][$lang]}' target='_blank'>";
				if(is_file($c['root_path'].$ad_ary['PicPath'][0][$lang])) $ad_contents.="<img src='{$ad_ary['PicPath'][0][$lang]}' alt='{$ad_ary['Name'][0][$lang]}'>";
				$ad_ary['Url'][0][$lang] && $ad_contents.='</a>';
			}else{
				if($ad_row['ShowType']==1 || $ad_row['ShowType']==2 || $ad_row['ShowType']==3){
					$effect_ary=array('', 'fade', 'top', 'left');
					$interTime=5000;//自动运行间隔
					$ad_contents.=ly200::load_static('/static/js/plugin/banner/jQuery.blockUI.js', '/static/js/plugin/banner/jquery.SuperSlide.js');
					$ad_contents.="<style type='text/css'>
										.slideBox_{$Id}{overflow:hidden; position:relative;}
										.slideBox_{$Id} .hd{height:15px; overflow:hidden; position:absolute; bottom:15px; z-index:1;}
										.slideBox_{$Id} .hd ul{overflow:hidden; zoom:1; float:left;}
										.slideBox_{$Id} .hd ul li{float:left; margin-left:5px; width:10px; height:10px; -webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; background:#f1f1f1; cursor:pointer;}
										.slideBox_{$Id} .hd ul li:first-child{margin-left:0;}
										.slideBox_{$Id} .hd ul li.on{ background:#f00; color:#fff;}
										.slideBox_{$Id} .bd{position:relative; height:100%; z-index:0;}
										.slideBox_{$Id} .bd ul li a{display:block; background-position:center top; background-repeat:no-repeat;}
									</style>";
					$ad_contents.='<div id="slideBox_'.$Id.'" class="slideBox_'.$Id.'">';
					$ad_contents.='<a href="javascript:;" rel="nofollow" class="prev"></a><a href="javascript:;" rel="nofollow" class="next"></a>';
					$hd='<div class="hd"><ul>';
					$bd='<div class="bd"><ul>';
					for($i=0; $i<$ad_row['PicCount']; $i++){
						$b='target="_blank"';
						$p=$ad_ary['PicPath'][$i][$lang];
						$u=$ad_ary['Url'][$i][$lang];
						$n=$ad_ary['Name'][$i][$lang];
						if(!is_file($c['root_path'].$p)){continue;}
						if(!$u){ $u='javascript:;'; $b=''; }
						$hd.="<li></li>";
						$bd.="<li><a href='$u' $b><img src='$p' alt='$n' /></a></li>";
					}
					$hd.='</ul></div>';
					$bd.='</ul></div>';
					$ad_contents.=$hd.$bd;
					$ad_contents.='</div><script type="text/javascript">jQuery(document).ready(function(){jQuery(".slideBox_'.$Id.'").slide({mainCell:".bd ul",effect:"'.$effect_ary[$ad_row['ShowType']].'",autoPlay:true,interTime:'.$interTime.($width=='auto'?', showType:"bg"':'').'});});</script>';
				}else{
					$ad_contents.=ly200::load_static('/static/js/plugin/banner/swf_obj.js');
					$xmlData='<list>';
					for($i=0; $i<$ad_row['PicCount']; $i++){
						$p=$ad_ary['PicPath'][$i][$lang];
						$u=$ad_ary['Url'][$i][$lang];
						is_file($c['root_path'].$p) && $xmlData.="<item><img>$p</img><url>$u</url></item>";
					}
					$xmlData.='</list>';
					$ad_contents.="<div id='swfContents_{$Id}'></div>
								<script type='text/javascript'>
									var xmlData='$xmlData';
									var flashvars={xmlData:xmlData};
									var params={menu:false, wmode:'transparent'};
									var attributes={};
									swfobject.embedSWF('/static/images/swf/ad.swf', 'swfContents_{$Id}', '{$ad_row['Width']}', '{$ad_row['Height']}', '9', 'expressInstall.swf', flashvars, params, attributes);
								</script>";
				}
			}
		}else{
			$ad_contents='<div>'.$ad_row['Contents'];
		}
		$ad_contents.='</div>';
		return $ad_contents;
	}

	public static function ad_custom($Id, $AId, $MThemesHome){//自定义广告图样式
		global $c;
		$lang=substr($c['lang'], 1);
		$ad_row=db::get_one('ad', " Number='$Id'".($c['theme'] ? " and Themes='{$c['theme']}'" : '').($AId?" and AId='$AId'":'').($MThemesHome?" and MThemesHome='$MThemesHome'":''));
		$result=array('Lang'=>$lang, 'Count'=>$ad_row['PicCount']);
		for($i=0; $i<$ad_row['PicCount']; ++$i){
			$result['Name'][$i]=str::str_code(str::json_data($ad_row['Name_'.$i], 'decode'));
			$result['Brief'][$i]=str::str_code(str::json_data($ad_row['Brief_'.$i], 'decode'));
			$result['Url'][$i]=str::str_code(str::json_data($ad_row['Url_'.$i], 'decode'));
			$result['PicPath'][$i]=str::str_code(str::json_data($ad_row['PicPath_'.$i], 'decode'));
		}
		return $result;
	}

	public static function web_settings($position, $mobileHomeTpl=0){//网站设置
		global $c;
		$where="Themes='{$c['theme']}'";
		$mobileHomeTpl && $where="Themes='{$mobileHomeTpl}'";
		$lang=substr($c['lang'], 1);
		$row=db::get_one('web_settings',$where." and Position='{$position}'",'Config,Data');
		$Data=str::json_data(htmlspecialchars_decode($row['Data']), 'decode');
		$config=str::json_data($row['Config'], 'decode');
		$PicCount=(int)$config['PicCount'];
		$PicCount || $PicCount=1;
		$result=array();
		foreach((array)$Data as $k => $v){
			if($PicCount>1){
				for($i=0;$i<$PicCount;$i++){
					$result[$k][$i]=$Data[$k][$i][$lang];
				}
			}else{
				$result[$k]=$Data[$k][0][$lang];
			}
		}
		return $result;
	}

	public static function get_cache_path($theme='', $root=1){	//生成缓存文件路径【$theme：风格，$root：1 绝对路径，0 相对路径】
		global $c;
		return ($root?$c['root_path']:'').$c['tmp_dir'].'cache/'.$theme.'/'.substr($c['lang'], 1).'/';
	}

	public static function get_url($row, $field='products_category', $lang=''){	//生成页面地址
		global $c;
		!$lang && $lang=$c['lang'];
		$ary=@explode('_', $field);
		$length=count($ary);
		if($ary[0]=='article' && $length==1){
			$path=ly200::str_to_url($row['Title'.$lang]);
			$url='/art/'.$path.'-a'.sprintf('%04d', $row['AId']).'.html';
			$url=$row['PageUrl']?'/art/'.$row['PageUrl'].'.html':$url;
			$url=$row['Url']?$row['Url']:$url;
		}elseif($ary[0]=='help' && $length==1){
			$path=ly200::str_to_url($row['Title'.$lang]);
			$url='/help/'.$path.'-h'.sprintf('%04d', $row['AId']).'.html';
			$url=$row['PageUrl']?'/help/'.$row['PageUrl'].'.html':$url;
			$url=$row['Url']?$row['Url']:$url;
		}elseif($ary[0]=='info' && $length==1){
			$path=ly200::str_to_url($row['Title'.$lang]);
			$path=$row['PageUrl']?$row['PageUrl']:$path;
			$url='/info/'.$path.'-i'.sprintf('%04d', $row['InfoId']).'.html';
			$url=$row['Url']?$row['Url']:$url;
		}elseif($ary[0]=='info' && $ary[1]=='category'){
			$path=ly200::str_to_url($row['Category'.$lang]);
			$url='/info/'.$path.'-c'.sprintf('%04d', $row['CateId']);
		}elseif($ary[0]=='products' && $length==1){
			//产品详细页
			$path=ly200::str_to_url($row['Name'.$lang]);
			$path=$row['PageUrl']?$row['PageUrl']:$path;
			if((int)$_GET['ueeshop_store']==1){//店铺模式
				$url='/'.$path.'-sp'.sprintf('%04d', $row['ProId']).'.html';
			}else{
				$url='/'.$path.'-p'.sprintf('%04d', $row['ProId']).'.html';
			}
		}elseif($ary[0]=='products' && $ary[1]=='category'){
			//产品分类列表页
			$path=ly200::str_to_url($row['Category'.$lang]);
			$url='/c/'.$path.'-'.sprintf('%04d', $row['CateId']);
		}elseif($ary[0]=='seckill'){
			//秒杀详细页
			$path=ly200::str_to_url($row['Name'.$lang]);
			$url='/'.$path.'-p'.sprintf('%04d', $row['ProId']).'-s'.sprintf('%04d', $row['SId']).'.html';
		}elseif($ary[0]=='tuan'){
			//团购详细页
			$path=ly200::str_to_url($row['Name'.$lang]);
			$url='/'.$path.'-p'.sprintf('%04d', $row['ProId']).'-t'.sprintf('%04d', $row['TId']).'.html';
		}elseif($ary[0]=='review'){
			$url='/review_p'.sprintf('%04d', $row['ProId']).'/';
		}elseif($ary[0]=='write' && $ary[1]=='review'){
			$url='/review-write/'.sprintf('%04d', $row['ProId']).'.html';
		}elseif($ary[0]=='blog' && $length==1){
			$path=ly200::str_to_url($row['Title']);
			$url='/blog/'.$path.'-b'.sprintf('%04d', $row['AId']).'.html';
		}elseif($ary[0]=='blog' && $ary[1]=='category'){
			$path=ly200::str_to_url($row['Category_en']);
			$url='/blog/c/'.$path.'-'.sprintf('%04d', $row['CateId']);
		}elseif($ary[0]=='blog' && $ary[1]=='date'){
			$url='/blog/t/'.$row;
		}else{
			$url_ary=array(
				'page'				=>	"/?a=article&AId={$row['AId']}",
				'info'				=>	"/?a=info&InfoId={$row['InfoId']}",
				'products_category'	=>	"/?a=products&CateId={$row['CateId']}",
				'products'			=>	"/?a=goods&ProId={$row['ProId']}",
				'blog'				=>	"/?m=blog&p=detail&AId={$row['AId']}",
			);
			$url=$url_ary[$field];
		}
		if((int)$_GET['ueeshop_store']==1){//店铺模式
			$url='/store'.$url;
		}
		return $url;
	}

	public static function str_to_url($str){	//字符串转换成合法的url路径
		$url=strtolower(trim($str));
		$url=str_replace(array(' ', '/'), '-', $url);
		$url=str_replace(array('`','~','!','@','#','$','%','^','&','*','(',')','_','=','+','[','{',']','}',';',':','\'','"','\\','|','<',',','.','>','?',"\r","\n","\t"), '', $url);
		$url=preg_replace('/[^\x00-\x7F]+/', '', $url);	//去掉中文
		$url=preg_replace('/-{2,}/', '-', $url);
		!eregi('^[a-z0-9]', $url) && $url='';
		return $url;
	}

	public static function get_narrow_url($query_string, $narrow_ary=array(), $id, $type){//生成高级筛选地址
		$page_string=ly200::get_url_dir($_SERVER['REQUEST_URI'], $ext_name='.html');
		$query_string='/'.trim($page_string, '/').'?'.ly200::get_query_string($query_string);
		$type==0 && $id='s'.trim($id,'s');//普通属性（文本框），加上s为前缀作为区分
		if($narrow_ary){
			$key=array_search($id, $narrow_ary);
			if($key===false){
				$narrow_ary[]=$id;
			}else{
				unset($narrow_ary[$key]);
			}
			$result=$query_string.(count($narrow_ary)?'&Narrow='.implode('+', $narrow_ary):'');
		}else{
			$result=$query_string.'&Narrow='.$id;
		}
		return $result;
	}

	public static function get_narrow_pro_count($narrow_ary=array(), $id, $CateId=0, $type){//计算高级筛选产品数量
		global $c;
		$arr=array();
		$v_ary=array();
		$type==0 && $id='s'.$id;//普通属性（文本框），加上s为前缀作为区分
		$key=array_search($id, $narrow_ary);
		if($key===false){ $narrow_ary[]=$id; }
		foreach((array)$narrow_ary as $k=>$v){ $arr[]=$v; }
		sort($arr); //从小到大排序
		$narrow_where='';
		if($CateId){
			$UId=category::get_UId_by_CateId($CateId);
			$narrow_where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
		}
		if($arr){
			$attrid_ary=array();
			foreach($arr as $v){
				if(!strstr($v, 's')){//普通属性（选项）
					$attrid_ary['Option'][]=$v;
				}else{//普通属性（文本框）
					$attrid_ary['Text'][]=$v;
				}
			}
			// 2019-04-28 把条件 $attrid_ary['Option'] 改成 $type
			if($type){
				//普通属性（选项）
				//整合属性信息
				$attr_ary=$value_ary=array();
				$attr_str=implode(',', $attrid_ary['Option']);
				!$attr_str && $attr_str=-1;
				$value_row=db::get_all('products_selected_attribute', 'IsUsed=1 and (VId in ('.$attr_str.'))', 'ProId, VId', 'VId asc');
				if(ly200::is_mobile_client(1)==1){//移动端
					$narrow_ary=array(0);
					foreach((array)$value_row as $v){
						if(!in_array($v['VId'], $value_ary[$v['ProId']][$v['AttrId']])){
							$value_ary[$v['ProId']][$v['AttrId']][]=$v['VId'];
						}
						if(!in_array($v['VId'], $attr_ary[$v['AttrId']])){
							$attr_ary[$v['AttrId']][]=$v['VId'];
						}
					}
					$attr_len=count($attr_ary);
					foreach((array)$value_ary as $k=>$v){
						$count=0;
						foreach($attr_ary as $k2=>$v2){
							foreach($v2 as $k3=>$v3){
								if(in_array($v3, $v[$k2])){//拥有其中一个选项，就可以跳出循环，直接记录
									$count+=1;
									break;
								}
							}
						}
						if($count>=$attr_len){
							$narrow_ary[]=$k;
						}
					}
				}else{//PC端
					$narrow_ary=array(0);
					foreach((array)$value_row as $v){
						if(!$value_ary[$v['ProId']]){
							$value_ary[$v['ProId']]=$v['VId'];
						}else{
							$value_ary[$v['ProId']].=','.$v['VId'];
						}
					}
					$attr_str='';
					foreach((array)$arr as $k=>$v){ $attr_str.=($k?',':'').$v; }
					foreach((array)$value_ary as $k=>$v){
						if($attr_str==$v){
							$narrow_ary[]=$k;
						}
					}
				}
			}
			// 2019-04-28 把条件 $attrid_ary['Text'] 改成 !$type
			if(!$type){
				//普通属性（文本框）
				//整合属性信息
				$attr_ary['Text']=str_replace('s', '', $attrid_ary['Text']);
				$attr_str=implode(',', $attr_ary['Text']);
				!$attr_str && $attr_str=-1;
				$select_where_ary='';
				$value_row=db::get_all('products_selected_attribute', 'IsUsed=1 and (SeleteId in ('.$attr_str.'))', "AttrId, Value{$c['lang']}", 'AttrId asc');
				foreach($value_row as $v){
					$value=addslashes($v['Value'.$c['lang']]);
					$select_where_ary[]="(AttrId='{$v['AttrId']}' and Value{$c['lang']}='{$value}')";
				}
				$select_where=implode(' or ', $select_where_ary);
				//查询
				if(ly200::is_mobile_client(1)==1){
					//移动端
					$narrow_ary=array(0);
					$value_row=db::get_all('products_selected_attribute', "IsUsed=1 and ($select_where) Group by ProId", 'ProId');
					foreach((array)$value_row as $v){//拥有其中一个选项
						$narrow_ary[]=$v['ProId'];
					}
				}else{
					//PC端
					$narrow_ary=array(0);
					$value_ary=array();
					$value_row=db::get_all('products_selected_attribute', "IsUsed=1 and ($select_where)", "ProId, AttrId, Value{$c['lang']}", 'AttrId asc');
					foreach((array)$value_row as $v){
						$value=$v['Value'.$c['lang']];
						$value_ary[$v['ProId']][]="(AttrId='{$v['AttrId']}' and Value{$c['lang']}='{$value}')";
					}
					foreach($value_ary as $k=>$v){
						$v=implode(' or ', $v);
						if($select_where==$v){//同时拥有
							$narrow_ary[]=$k;
						}
					}

				}
			}
			count($narrow_ary)>0 && $narrow_where.=' and ProId in ('.implode(',', $narrow_ary).')';
		}
		unset($v_ary, $arr);
		$result=(int)db::get_row_count('products', '1 '.$c['where']['products'].$narrow_where);
		return $result;
	}

	public static function get_web_position($row, $table, $lang='_en', $char=' &gt; ', $length=0){	//面包屑地址
		global $c;
		$str='';
		$lang=$c['lang']?$c['lang']:$lang;
		$UId=trim($row['UId'], ',');
		$name=$row['Category'.$lang];
		$length && $name=str::str_echo($name, $length, 0, '..');
		if($UId=='0'){
			$str.=$char."<a href='".ly200::get_url($row, $table)."'>{$name}</a>";
		}
		if($UId){
			$all_row=str::str_code(db::get_all($table, "CateId in($UId)", '*', 'Dept asc'));
			$i=0;
			foreach((array)$all_row as $v){
				$ext_name=$v['Category'.$lang];
				$length && $ext_name=str::str_echo($ext_name, $length, 0, '..');
				$str.=($i==0?$char:'')."<a href='".ly200::get_url($v, $table)."'>{$ext_name}</a>".$char;
				$i+=1;
			}
			$str.="<a href='".ly200::get_url($row, $table)."'>{$name}</a>";
		}
		return $str;
	}

	public static function seo_meta($row='', $spare_row=''){	//前台页面输出标题标签
		global $c;
		$lang=$c['lang'];
		$SeoTitle=htmlspecialchars_decode($row['SeoTitle'.$lang]?$row['SeoTitle'.$lang]:$spare_row['SeoTitle']);
		$SeoKeywords=htmlspecialchars_decode($row['SeoKeyword'.$lang]?$row['SeoKeyword'.$lang]:$spare_row['SeoKeyword']);
		$SeoDescription=htmlspecialchars_decode($row['SeoDescription'.$lang]?$row['SeoDescription'.$lang]:$spare_row['SeoDescription']);
		if(!$SeoTitle && !$SeoKeywords && !$SeoDescription){
			$home_row=str::str_code(db::get_one('meta', 'Type="home"'));
			$SeoTitle=htmlspecialchars_decode($home_row['SeoTitle'.$lang]?$home_row['SeoTitle'.$lang]:$c['config']['global']['SiteName']);
			$SeoKeywords=htmlspecialchars_decode($home_row['SeoKeyword'.$lang]?$home_row['SeoKeyword'.$lang]:$c['config']['global']['SiteName']);
			$SeoDescription=htmlspecialchars_decode($home_row['SeoDescription'.$lang]?$home_row['SeoDescription'.$lang]:$c['config']['global']['SiteName']);
		}
		$web_url=ly200::get_domain().$_SERVER['REQUEST_URI'];
		$str='';
		if($_SERVER['HTTP_X_FROM']){  //临时域名(使用代理),禁止收录
			$str.='<meta name="robots" content="noindex,nofollow,noarchive" />';
		}
		$where='IsUsed=1 and IsMeta=1 and IsBody=0';
		$where.=(ly200::is_mobile_client(1)?' and CodeType in(0,2)':' and CodeType in(0,1)');
		$third_row=db::get_all('third', $where, '*', $c['my_order'].'TId desc');	//自定义Meta代码
		if(!$_GET['client']=='website' && !$_GET['client']=='mobile'){  //可视化后台编辑不加载第三方代码
			foreach((array)$third_row as $v) $str.=$v['Code'];
		}
		return "{$str}<link rel=\"canonical\" href=\"{$web_url}\" />\r\n<link rel='shortcut icon' href='{$c['config']['global']['IcoPath']}' />\r\n<meta name=\"keywords\" content=\"$SeoKeywords\" />\r\n<meta name=\"description\" content=\"$SeoDescription\" />\r\n<title>$SeoTitle</title>\r\n{$copyCode}";
	}

	public static function get_size_img($filepath, $size){//输入缩略图
		global $c;
		$result=$filepath;
		$ext_name=file::get_ext_name($filepath);
		if(is_file($c['root_path']."{$filepath}.{$size}.{$ext_name}")){
			$result="{$filepath}.{$size}.{$ext_name}";
		}
		return $result;
	}

	public static function set_products_history($row, $CurPrice, $OldPrice){//生成产品浏览历史记录
		global $c;
		$time=$c['time']+3600*24*365;
		$max=15;//最大储存数
		ksort($_COOKIE['history']);
		$history_num=count($_COOKIE['history']);
		if($history_num==0){
			setcookie('history[0][ProId]', $row['ProId'], $time);
			setcookie('history[0][Name]', $row['Name'.$c['lang']], $time);
			setcookie('history[0][PicPath]', $row['PicPath_0'], $time);
			setcookie('history[0][Price]', $CurPrice, $time);
			setcookie('history[0][OldPrice]', $OldPrice, $time);
		}else{
			$i=0;
			foreach($_COOKIE['history'] as $k=>$v){
				if($history_num==$max && $i==0){//达成最大值，删除第一个
					setcookie("history[$k][ProId]", '');
					setcookie("history[$k][Name]", '');
					setcookie("history[$k][PicPath]", '');
					setcookie("history[$k][Price]", '');
					setcookie("history[$k][OldPrice]", '');
				}
				if($v['ProId']==$row['ProId']){//删除相同
					setcookie("history[$k][ProId]", '');
					setcookie("history[$k][Name]", '');
					setcookie("history[$k][PicPath]", '');
					setcookie("history[$k][Price]", '');
					setcookie("history[$k][OldPrice]", '');
				}
				if($i==($history_num-1)) $num=$k+1;
				++$i;
			}
			setcookie("history[$num][ProId]", $row['ProId'], $time);
			setcookie("history[$num][Name]", $row['Name'.$c['lang']], $time);
			setcookie("history[$num][PicPath]", $row['PicPath_0'], $time);
			setcookie("history[$num][Price]", $CurPrice, $time);
			setcookie("history[$num][OldPrice]", $OldPrice, $time);
		}
	}

	public static function get_products_package($ProId, $Type=0){//组合产品
		global $c;
		$id_where='0';
		$pid_ary=$prod_ary=array();
		$row=db::get_all('sales_package', "ProId='$ProId' and Type=$Type");
		$row=array_merge($row, (array)db::get_all('sales_package', "ReverseAssociate=1 and PackageProId like '%|$ProId|%' and Type=$Type", '*', 'PId desc'));
		if(!$row) return false;
		foreach((array)$row as $v){
			$v['ReverseAssociate']==1 && $v['PackageProId']=str_replace("|{$ProId}|", "|{$v['ProId']}|", $v['PackageProId']);
			$pid_ary=@array_filter(@explode('|', $v['PackageProId']));
			if(!count($pid_ary)){
				return false;
			}
			$id_where.=','.@implode(',', $pid_ary);
		}
		$pro_row=db::get_all('products', "ProId in($id_where) and (SoldStatus<2 && (SoldOut!=1 or (SoldOut=1 and IsSoldOut=1 and {$c['time']} between SStartTime and SEndTime)) or (SoldStatus=2 and Stock>0))", "ProId, Name{$c['lang']}, CateId, IsPromotion, PromotionType, PromotionPrice, PromotionDiscount, StartTime, EndTime, Price_0, Price_1, PicPath_0, Wholesale, AttrId, Attr, ExtAttr, MOQ, Stock, SoldOut, IsSoldOut, SStartTime, SEndTime, IsCombination, IsOpenAttrPrice, SoldStatus, PageUrl", 'ProId asc');
		foreach((array)$pro_row as $v){
			$prod_ary[$v['ProId']]=$v;
		}
		return array('data'=>$row, 'pro'=>$prod_ary);
	}

	public static function get_table_data_to_ary($table, $w, $key, $return_field='', $query_field=''){	//从数据表中获取数据，生成一个以指定字段为下标的数组
		global $c;
		$data=array();
		$row=str::str_code(db::get_all($table, $w, $query_field?$query_field:($return_field?"{$key},{$return_field}":'*')));
		foreach($row as $v){
			$data[$v[$key]]=$return_field?$v[$return_field]:$v;
		}
		return $data;
	}

	public static function get_ip($istry=0){	//浏览者IP
		if($istry){
			$ip=$_SERVER['HTTP_X_FORWARDER_FOR'];
		}elseif($_SERVER['HTTP_X_FORWARDED_FOR']){
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}elseif($_SERVER['HTTP_CLIENT_IP']){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return preg_match('/^[\d]([\d\.]){5,13}[\d]$/', $ip)?$ip:'';
	}

	public static function get_server_ip(){
		if($_SERVER['SERVER_ADDR']){
			$server_ip = $_SERVER['SERVER_ADDR'];
		}else if(function_exists('getenv') && getenv('SERVER_NAME')){
			$server_ip = getenv('SERVER_ADDR');
		}else if($_SERVER['SERVER_NAME']){
			$server_ip = gethostbyname($_SERVER['SERVER_NAME']);
		}else if($_SERVER['HTTP_HOST']){
			$host = preg_replace('/(\:{1}\d+)$/', '', $_SERVER['HTTP_HOST']);
			$server_ip = gethostbyname($host);
		}else{
			$server_ip = '';
		}
		return preg_match('/^[\d]([\d\.]){5,13}[\d]$/', $server_ip)?$server_ip:'';
	}

	/*
	public static function ip($ip, $group=''){	//IP地址转换为地理地址
		if(!$ip){return;}
		$iploca=new ip;
		@$iploca->init();
		@$iploca->getiplocation($ip);
		$area=array();
		$area['country']=str_replace(array('未知', 'CZ88.NET'), '', $iploca->get('country'));
		$area['area']=str_replace(array('未知', 'CZ88.NET'), '', $iploca->get('area'));
		$area['country']=='' && $area['country']='未知';
		$area['area']=='' && $area['area']='未知';
		if($group=='country'){
			return str::iconver($area['country'], 'GBK', 'UTF-8');
		}elseif($group=='area'){
			return str::iconver($area['area'], 'GBK', 'UTF-8');
		}
		return str::iconver(implode($area), 'GBK', 'UTF-8');
	}
	 */

	public static function ip($ip, $group=''){	//IP地址转换为地理地址
		if(!$ip){return;}
		$iploca=new ip;
		$ip_ary=@$iploca->find($ip);
		if($ip_ary!='N/A'){
			$country=$ip_ary[0];
			$state=$ip_ary[1];
			$city=$ip_ary[2];
			$area=$ip_ary[3];
			if($country=='中国' && $group!='country'){
				return $state.$city.$area;
			}else{
				return $country;
			}
		}else{
			return '未知';
		}
	}

	public static function password($password){	//密码加密
		$password=md5($password);
		$password=substr($password, 0, 5).substr($password, 10, 20).substr($password, -5).'www.ly200.com';
		return md5($password.$password);
	}

	public static function check_upfile($file){
		global $c;
		$basepath=implode('/', array_slice(explode('/', $file), 0, 3)).'/';
		if(@is_file($c['root_path'].$file) && substr_count($file, $basepath)){
			return $file;
		}
		return '';
	}

	public static function form_select($data, $name, $selected_value='', $field='', $key='', $index=0, $attr=''){	//生成下拉表单
		$select="<select name='$name' $attr>".($index?"<option value=''>$index</option>":'');
		foreach($data as $k=>$v){
			$value=$key!=''?$v[$key]:$k;
			$selected=($selected_value!='' && $value==$selected_value)?'selected':'';
			$text=$field!=''?$v[$field]:$v;
			$select.="<option value='{$value}' $selected>{$text}</option>";
		}
		$select.='</select>';
		return $select;
	}

	public static function e_json($msg='', $ret=0, $exit=1){
		is_bool($ret) && $ret=$ret?1:0;
		echo str::json_data(array(
				'msg'	=>	$msg,
				'ret'	=>	$ret
			)
		);
		$exit && exit;
	}

	public static function load_static(){
		global $c;
		$static='';
		$refresh='?v='.$c['ProjectVersion'].'0'; //.time();
		$args=func_get_args();
		foreach($args as $v){
			if(is_array($v)){
				$attr=$v[1];
				$v=$v[0];
			}
			if(!$v || @!is_file($c['root_path'].$v)){continue;}
			$ext_name=file::get_ext_name($v);

			if($ext_name=='css'){
				$static.="<link href='{$v}$refresh' rel='stylesheet' type='text/css' $attr />\r\n";
			}elseif($ext_name=='js'){
				if(ly200::isCrawler() || ly200::checkrobot()) $async = 'async';
				$static.="<script {$async} type='text/javascript' src='{$v}$refresh' $attr></script>\r\n";
			}
		}
		return $static;
	}

	public static function load_cdn_contents($html=''){
		global $c;
		echo $html;
		exit;
	}

	public static function query_string($un=''){	//组织url参数	'a', 'b'
		!is_array($un) && $un=explode(',', str_replace(' ','',$un));//$un=array($un);
		if($_SERVER['QUERY_STRING']){
			$q=@explode('&', $_SERVER['QUERY_STRING']);
			$v='';
			for($i=0; $i<count($q); $i++){
				$t=@explode('=', $q[$i]);
				if(in_array($t[0], $un)){continue;}
				$v.=$t[0].'='.$t[1].'&';
			}
			$v=substr($v, 0, -1);
			$v=='=' && $v='';
			return $v;
		}else{
			return '';
		}
	}

	public static function get_domain($protocol=1, $istry=0){	//获取网站域名	$istry: 0 返回HTTP_HOST， 1 返回临时域名
		if($c['FunVersion']>=10){
			//查看是否有绑定域名
			$domain_ary = (array)str::json_data(db::get_value('config', 'GroupId="domain_binding" and Variable="domain_list"', 'Value'), 'decode');
			$certificate=0;
			foreach((array)$domain_ary as $v){
				if(substr_count($v, '_acme-')) $certificate=1;
				if(substr_count($v, 'www.') || substr_count($v, '_acme-')) continue; //过滤www和别名
				$_SESSION['Manage']['WebDomain']=($certificate?'https://':'http://').$v;
			}
		}
		if($_SESSION['Manage']['WebDomain']){
			//后台临时域名，前台域名SESSION
			return $protocol==1?$_SESSION['Manage']['WebDomain']:str_replace(array('https://', 'http://'), '', $_SESSION['Manage']['WebDomain']);
		}
		return ($protocol==1?((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on')?'https://':'http://'):'').(($istry==1 && $_SERVER['HTTP_X_FROM'])?$_SERVER['HTTP_X_FROM']:$_SERVER['HTTP_HOST']).(($_SERVER['SERVER_PORT']!=80 && $_SERVER['SERVER_PORT']!=443)?':'.$_SERVER['SERVER_PORT']:'');
	}

	public static function turn_page_html($row_count, $page, $total_pages, $query_string, $pre_page='<<', $next_page='>>', $base_page=3, $link_ext_str='.html', $html=1){	//翻页
		if(!$row_count){return;}
		if($html==1){
			$page_string=ly200::get_url_dir($_SERVER['REQUEST_URI'], $link_ext_str);
			$page_string=str_replace('//', '/', $page_string);
			if($query_string!='' && $query_string!='?'){
				$query_string='?'.ly200::get_query_string($query_string);
			}
		}else{
			$page_string=$query_string;
			$link_ext_str=$query_string='';
		}
		$i_start=$page-$base_page>0?$page-$base_page:1;
		$i_end=$page+$base_page>=$total_pages?$total_pages:$page+$base_page;
		($total_pages-$page)<$base_page && $i_start=$i_start-($base_page-($total_pages-$page));
		$page<=$base_page && $i_end=$i_end+($base_page-$page+1);
		$i_start<1 && $i_start=1;
		$i_end>=$total_pages && $i_end=$total_pages;
		$turn_page_str='<ul>';
		$pre=$page-1>0?$page-1:1;
		$turn_page_str.=($page<=1)?"<li><font class='page_noclick'><em class='icon_page_prev'></em>$pre_page</font></li>":"<li><a href='{$page_string}{$pre}{$link_ext_str}{$query_string}' class='page_button'><em class='icon_page_prev'></em>$pre_page</a></li>";
		$i_start>1 && $turn_page_str.="<li><a href='{$page_string}1{$link_ext_str}{$query_string}' class='page_item'>1</a></li><li><font class='page_item'>...</font></li>";
		for($i=$i_start; $i<=$i_end; $i++){
			$turn_page_str.=$page!=$i?"<li><a href='{$page_string}{$i}{$link_ext_str}{$query_string}' class='page_item'>$i</a></li>":"<li><font class='page_item_current'>$i</font></li>";
		}
		$i_end<$total_pages && $turn_page_str.="<li><font class='page_item'>...</font></li><li><a href='{$page_string}{$total_pages}{$link_ext_str}{$query_string}' class='page_item'>$total_pages</a></li>";
		$next=$page+1>$total_pages?$total_pages:$page+1;
		if($page+1>$total_pages){
			$turn_page_str.="<li class='page_last'><font class='page_noclick'>$next_page<em class='icon_page_next'></em></font></li>";
		}else{
			$page>=$total_pages && $page--;
			$turn_page_str.="<li class='page_last'><a href='{$page_string}{$next}{$link_ext_str}{$query_string}' class='page_button'>$next_page<em class='icon_page_next'></em></a></li>";
		}
		$turn_page_str.='</ul>';
		return $turn_page_str;
	}

	public static function turn_page_small_html($row_count, $page, $total_pages, $query_string, $link_ext_str='.html', $html=1){	//翻页
		if(!$row_count){return;}
		if($html==1){
			$page_string=ly200::get_url_dir($_SERVER['REQUEST_URI'], $link_ext_str);
			$page_string=str_replace('//', '/', $page_string);
			if($query_string!='' && $query_string!='?'){
				$query_string='?'.ly200::get_query_string($query_string);
			}
		}else{
			$page_string=$query_string;
			$link_ext_str=$query_string='';
		}
		$str="<div class='page'><div class='cur'>$page/$total_pages</div>";
		if($total_pages>1){
			$step=array(1, $total_pages>=200?2:0, $total_pages>=500?5:0, $total_pages>=1000?10:0, $total_pages>=2000?20:0, $total_pages>=5000?50:0);
			$str.='<ul>';
			for($i=max($step); $i<=$total_pages; $i+=max($step)){
				$str.="<li><a href='{$page_string}{$i}{$link_ext_str}{$query_string}'>$i/$total_pages</a></li>";
			}
			$str.='</ul>';
		}
		$str.='</div>';
		$pre=$page>1?$page-1:1;
		$next=$page+1>$total_pages?$total_pages:$page+1;
		$str.="<a href='{$page_string}{$pre}{$link_ext_str}{$query_string}' class='page_item pre'></a>";
		$str.="<a href='{$page_string}{$next}{$link_ext_str}{$query_string}' class='page_item next'></a>";
		return $str;
	}

	public static function turn_page_mobile_html($row_count, $page, $total_pages, $query_string, $base_page=3, $link_ext_str='.html', $html=1){	//翻页
		if(!$row_count){return;}
		if($html==1){
			$page_string=ly200::get_url_dir($_SERVER['REQUEST_URI'], $link_ext_str);
			$page_string=str_replace('//', '/', $page_string);
			if($query_string!='' && $query_string!='?'){
				$query_string='?'.ly200::get_query_string($query_string);
			}
		}else{
			$page_string=$query_string;
			$link_ext_str=$query_string='';
		}
		$i_start=$page-$base_page>0?$page-$base_page:1;
		$i_end=$page+$base_page>=$total_pages?$total_pages:$page+$base_page;
		($total_pages-$page)<$base_page && $i_start=$i_start-($base_page-($total_pages-$page));
		$page<=$base_page && $i_end=$i_end+($base_page-$page+1);
		$i_start<1 && $i_start=1;
		$i_end>=$total_pages && $i_end=$total_pages;
		$turn_page_str='';
		$pre=$page-1>0?$page-1:1;
		$turn_page_str.="<a href='{$page_string}{$pre}{$link_ext_str}{$query_string}' class='btn prev'>&nbsp;</a>";
		$i_start>1 && $turn_page_str.="<a href='{$page_string}1{$link_ext_str}{$query_string}'>1</a><font>...</font>";
		for($i=$i_start; $i<=$i_end; $i++){
			$turn_page_str.=$page!=$i?"<a href='{$page_string}{$i}{$link_ext_str}{$query_string}'>$i</a>":"<font class='cur'>$i</font>";
		}
		$i_end<$total_pages && $turn_page_str.="<font>...</font><a href='{$page_string}{$total_pages}{$link_ext_str}{$query_string}'>$total_pages</a>";
		$next=$page+1>$total_pages?$total_pages:$page+1;
		$turn_page_str.="<a href='{$page_string}{$next}{$link_ext_str}{$query_string}' class='btn next'>&nbsp;</a>";
		return $turn_page_str;
	}

	public static function get_url_dir($REQUEST_URI, $ext_name){	//获取伪静态目录
		$url_ary=@explode('/', trim($REQUEST_URI, '/'));
		$url='/';
		foreach($url_ary as $k=>$v){
			if($k==count($url_ary)-1){
				$p=@explode('?', $v);
				$v=$p[0];
			}
			if(substr_count($v, $ext_name)){break;}
			$url.=$v.'/';
		}
		return $url;
	}

	public static function get_query_string($str){	//重新组织伪静态参数
		$query_ary=@explode('&', trim($str, '?'));
		$query_string='';
		foreach($query_ary as $k=>$v){
			$v=trim($v);
			if($v=='' || $v=='='){continue;}
			$query_string.="&{$v}";
		}
		return $query_string;
	}

	public static function turn_page($row_count, $page, $total_pages, $query_string, $pre_page='<<', $next_page='>>', $base_page=3, $link_ext_str=''){	//翻页
		if(!$row_count){return;}
		$i_start=$page-$base_page>0?$page-$base_page:1;
		$i_end=$page+$base_page>=$total_pages?$total_pages:$page+$base_page;
		($total_pages-$page)<$base_page && $i_start=$i_start-($base_page-($total_pages-$page));
		$page<=$base_page && $i_end=$i_end+($base_page-$page+1);
		$i_start<1 && $i_start=1;
		$i_end>=$total_pages && $i_end=$total_pages;
		$turn_page_str='';
		$pre=$page-1>0?$page-1:1;
		$turn_page_str.=($page<=1)?"<font class='page_noclick'>$pre_page</font>&nbsp;":"<a href='{$query_string}{$pre}{$link_ext_str}' class='page_button'>$pre_page</a>&nbsp;";
		for($i=$i_start; $i<=$i_end; $i++){
			$turn_page_str.=$page!=$i?"<a href='{$query_string}{$i}{$link_ext_str}' class='page_item'>$i</a>&nbsp;":"<font class='page_item_current'>$i</font>&nbsp;";
		}
		$i_end<$total_pages && $turn_page_str.="<font class='page_item'>...</font>&nbsp;<a href='{$query_string}{$total_pages}{$link_ext_str}' class='page_item'>$total_pages</a>&nbsp;";
		$next=$page+1>$total_pages?$total_pages:$page+1;
		if($page+1>$total_pages){
			$turn_page_str.="<font class='page_noclick'>$next_page</font>";
		}else{
			$page>=$total_pages && $page--;
			$turn_page_str.="<a href='{$query_string}{$next}{$link_ext_str}' class='page_button'>$next_page</a>";
		}
		return $turn_page_str;
	}

	public static function turn_page_mobile($row_count, $page, $total_pages, $query_string, $pre_page='<<', $next_page='>>', $base_page=3, $link_ext_str=''){	//翻页
		if(!$row_count){return;}
		$i_start=$page-$base_page>0?$page-$base_page:1;
		$i_end=$page+$base_page>=$total_pages?$total_pages:$page+$base_page;
		($total_pages-$page)<$base_page && $i_start=$i_start-($base_page-($total_pages-$page));
		$page<=$base_page && $i_end=$i_end+($base_page-$page+1);
		$i_start<1 && $i_start=1;
		$i_end>=$total_pages && $i_end=$total_pages;
		$turn_page_str='';
		$pre=$page-1>0?$page-1:1;
		$turn_page_str.=$page<=1?"<font class='page_noclick'>$pre_page</font>":"<a href='$query_string$pre$link_ext_str' class='page_button'>$pre_page</a>";
		$turn_page_str.="&nbsp;&nbsp;<span class='fc_red'>{$page}</span> / {$total_pages}&nbsp;&nbsp;";
		$next=$page+1>$total_pages?$total_pages:$page+1;
		$turn_page_str.=$page>=$total_pages?"<font class='page_noclick'>$next_page</font>":"<a href='$query_string$next$link_ext_str' class='page_button'>$next_page</a>";
		return $turn_page_str;
	}

	public static function set_custom_style(){	//设置自定义样式
		global $c;
		$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');
		$style_data=str::json_data($StyleData, 'decode');
		$style_result="<style type=\"text/css\">\r\n";
		foreach($style_data as $k=>$v){
			if($k=='PriceColor') continue;
			$isHover=substr_count($k, 'Hover')?1:0;
			if(substr_count($k, 'Bg')){
				$style_result.=".{$k}".($isHover?':hover':'')."{background-color:{$v};}\r\n";
				if($k=='DiscountBgColor') $style_result.=".DiscountBorderColor{border-color:{$v};}\r\n";
				if($k=='AddtoCartBgColor') $style_result.=".".str_replace('Bg', 'Border', $k)."{border-color:{$v};}\r\n";
			}elseif(substr_count($k, 'Border')){
				$style_result.=".{$k}".($isHover?':hover':'')."{border-color:{$v};}\r\n";
				if($k=='GoodBorderHoverColor') $style_result.=".GoodBorderColor.selected{border-color:{$v};}\r\n.GoodBorderBottomHoverColor{border-bottom-color:{$v};}\r\n";
			}elseif(substr_count($k, 'Font')){
				$style_result.=".{$k},a.{$k},a.{$k}:hover{color:{$v};}\r\n";
				$style_result.=".".str_replace('Font', 'FontBg', $k)."{background-color:{$v};}\r\n";
				$style_result.=".".str_replace('Font', 'FontBorder', $k)."{border-color:{$v};}\r\n";
				$style_result.=".".str_replace('Font', 'FontBorderHover', $k).":hover, a.".str_replace('Font', 'FontBorderHover', $k).":hover{border-color:{$v}!important;}\r\n";
				$style_result.=".".str_replace('Font', 'FontBgHover', $k).":hover{background-color:{$v}!important;}\r\n";
				$style_result.=".".str_replace('Font', 'FontHover', $k).":hover{color:{$v}!important;}\r\n";
			}else{
				$style_result.=".{$k}{color:{$v};}\r\n";
			}
		}
		$style_result.='</style>';
		return $style_result;
	}

	public static function nav_style($row, $down=0){ //导航、底部栏目设置，$down是否有下拉设置
		global $c, $nav_ary;
		$Id=0;
		if($row['Custom']){//自定义
			$url=$row['Url'];
			$name=$row['Name'.$c['lang']];
		}else{
			$url=$c['nav_cfg'][$row['Nav']]['url'];
			$name=$c['nav_cfg'][$row['Nav']]['name'.$c['lang']];
			if($row['Nav']==1){//单页
				$name=str::str_code(db::get_value('article_category', "CateId='{$row['Page']}'", "Category{$c['lang']}"));
				$page_row=str::str_code(db::get_one('article', "CateId='{$row['Page']}'", "AId, CateId, Title{$c['lang']}, PageUrl, Url", $c['my_order']."AId desc"));
				if($page_row){
					$Id=$page_row['AId'];
					$url=ly200::get_url($page_row, 'article');
				}
			}elseif($row['Nav']==2){//文章
				$info_row=str::str_code(db::get_one('info_category', "CateId='{$row['Info']}'", "CateId, Category{$c['lang']}"));
				if($info_row){
					$Id=$info_row['CateId'];
					$name=$info_row['Category'.$c['lang']];
					$url=ly200::get_url($info_row, 'info_category');
				}
			}elseif($row['Nav']==3){//产品
				$down && $row['Down']=1;
				if((int)$row['Cate']){
					$Id=$row['Cate'];
					$prod_row=str::str_code(db::get_one('products_category', "CateId='{$row['Cate']}'", "CateId, UId, Dept, SubCateCount, Category{$c['lang']}"));
					if($prod_row){
						if($down && $row['Down'] && $prod_row['Dept']<3){//产品下拉
							$select=1;
							$uid="{$prod_row['UId']}{$prod_row['CateId']},";
						}
						$name=$prod_row['Category'.$c['lang']];
						$url=ly200::get_url($prod_row, 'products_category');
					}
				}else{
					if($down && $row['Down']){//产品下拉
						$select=1;
						$uid='0,';
					}
				}
			}
		}
		$target=$row['NewTarget']?' target="_blank"':'';
		return array('Name'=>$name, 'Url'=>$url, 'Target'=>$target, 'Select'=>$select, 'UId'=>$uid, 'Nav'=>$row['Nav'], 'Page'=>(int)$row['Page'], 'Id'=>$Id);
	}

	public static function out_put_third_code(){	//输出第三方代码
		global $c;
		if($_GET['client']=='website' || $_GET['client']=='mobile') return;  //可视化后台编辑不加载第三方
		$Number=str::str_crypt($c['Number']);
		$str="<script type='text/javascript' src='{$c['analytics']}?Number={$Number}'></script>";
		($_GET['m']=='user' && $_GET['a']=='register') && $str='';//注册页面去掉统计代码，Google把注册页面统计代码当木马了
		$where='IsUsed=1 and IsMeta=0 and IsBody=0';
		$where.=(ly200::is_mobile_client(1)?' and CodeType in(0,2)':' and CodeType in(0,1)');
		$third_row=db::get_all('third', $where, '*', $c['my_order'].'TId desc');
		foreach((array)$third_row as $v) $str.=$v['Code'];
		return $str!=''?'<div align="center">'.$str.'</div>':'';
	}

	public static function partners($type='',$vam=''){	//底部合作伙伴
		global $c;
		$result='<div class="partners_box">';
		$partner_row=db::get_all('partners', "IsUsed=1", '*', $c['my_order']." PId asc");
		foreach($partner_row as $v){
			$name=$v['Name'.$c['lang']];
			$type && $result.="<div class='partners_item'>";
			$v['Url']!='' && $result.="<a href=\"{$v['Url']}\" title=\"{$name}\" class=\"themes_bor\" target=\"_blank\">";
			if(is_file($c['root_path'].$v['PicPath'])){
				$result.="<img src=\"{$v['PicPath']}\" alt=\"{$name}\" />";
			}else{
				$result.=$name;
			}
			$v['Url']!='' && $result.="</a>";
			$vam && $result.="<em></em>";
			$type && $result.="</div>";
		}
		$result.='</div>';
		return $result;
	}

	public static function powered_by($type=0){	//技术支持 0:隐藏、1:带链接、2:不带链接
		global $c;
		if((int)$c['UeeshopAgentId'] || $type==0){	//代理商开通网站不加技术支持
			return '';
		}elseif($c['config']['global']['IsVgcart']){
			return '<a href="https://www.shopcto.com/" target="_blank" rel="nofollow">POWERED BY SHOPCTO</a>';
		}elseif($type==2){
			if($c['FunVersion']>=10){
				return 'POWERED BY SHOPCTO';
			}else{
				return 'POWERED BY UEESHOP';
			}
		}else{
			if($c['FunVersion']>=10){
				return '<a href="https://www.shopcto.com/" target="_blank" rel="nofollow">POWERED BY SHOPCTO</a>';
			}else{
				return '<a href="http://www.ueeshop.com" target="_blank" rel="nofollow">POWERED BY UEESHOP</a>';
			}
		}
	}

	public static function sendmail($toEmail, $Msubject, $Mbody, $massive_email=0){ //发送邮件
		global $c;
		$toEmail=(array)$toEmail;
		foreach($toEmail as $k=>$v){
			if(!preg_match('/^\w+[a-zA-Z0-9-.+_]+@[a-zA-Z0-9-.+_]+\.\w*$/i', $v)){
				unset($toEmail[$k]);
			}
		}
		if(!$toEmail) return array('msg'=>'邮箱格式不对');
		$used_ary=($c['manage']?$c['manage']['plugins']['Used']:$c['plugins']['Used']);
		if($massive_email && in_array('massive_email', (array)$used_ary)){
			//邮件群发
			mailchimp::get_template_folder('Customize');
			$folder_id=db::get_value('mailchimp_template_folder', 'Type="customize"', 'FolderId');
			$TemplateId=mailchimp::create_template($Msubject, $Mbody, $folder_id);
			mailchimp::get_user_list();
			$list_id=db::get_value('mailchimp_lists', 'Type="0"', 'ListId');
			foreach((array)$toEmail as $k=>$v){
				mailchimp::get_members($v);
			}
			return mailchimp::send_mail($list_id, $TemplateId, $toEmail);
		}else{
			//邮件单发
			$config_row=str::json_data(db::get_value('config', 'GroupId="email" and Variable="Config"', 'Value'), 'decode');
			$data=array(
				'Action'		=>	'send_mail',
				'From'			=>	$config_row['FromEmail']?$config_row['FromEmail']:'noreply@ueeshop.com',
				'FromName'		=>	$config_row['FromName']?$config_row['FromName']:'noreply',
				'To'			=>	$toEmail,
				'Subject'		=>	$Msubject,
				'Body'			=>	$Mbody
			);
			if($config_row['SmtpHost'] && $config_row['SmtpPort'] && $config_row['SmtpUserName'] && $config_row['SmtpPassword']){
				$data['SmtpHost']=$config_row['SmtpHost'];
				$data['SmtpPort']=$config_row['SmtpPort'];
				$data['SmtpUserName']=$config_row['SmtpUserName'];
				$data['SmtpPassword']=$config_row['SmtpPassword'];
			}
			return ly200::api($data, $c['ApiKey'], $c['api_url']);
		}
	}

	public static function smtpconnect($Host, $Username, $Password, $Port=25){ //发送邮件
		include_once('phpmailer/phpmailer.php');
		$mail=new PHPMailer();
		$mail->IsSMTP();
		$mail->Port=$Port;
		$mail->SMTPSecure=$mail->Port==465?'ssl':'';
		$mail->Host=$Host;
		$mail->Username=$Username;
		$mail->Password=$Password;
		$mail->SMTPDebug=0;
		$mail->Subject='=?utf-8?B?'.base64_encode('title').'?=';
		$mail->IsHTML(true);
		$result=$mail->SmtpConnectStatus()?1:0;
		return $result;
	}

	public static function sendsms($mobilephone, $sms, $TemplateId=1){ //发送短信
		global $c;
		/*$data=array(
			'Action'		=>	'send_sms',
			'MobilePhone'	=>	$mobilephone,
			'SmsContents'	=>	$sms,
			'Sign'			=>	'Ueeshop'
		);*/
		$data=array(
			'Action'		=>	'send_sms',
			'Application'	=>	0,	//ueeshop固定为0
			'TemplateId'	=>	$TemplateId,	//ueeshop：短信0，订单1
			'MobilePhone'	=>	$mobilephone,
			'SmsContents'	=>	$sms	//数组形式：短信模板为：【%s，订单号：%s】，此处应该为数组，即 array('您的网站有新的订单', '16120609460686')
		);
		return ly200::api($data, $c['ApiKey'], $c['api_url']);
	}

	public static function api($data, $key, $url){	//返回结果如果是数组，则表示成功，非数组，则是错误的提示语
		global $c;
        date_default_timezone_set("PRC");
		$data['Domain']=$_SERVER['HTTP_HOST'];
		(int)$c['UeeshopAgentId'] && $data['AgentId']=(int)$c['UeeshopAgentId'];
		(int)$c['UeeshopQcloudUserId'] && $data['QcloudUserId']=(int)$c['UeeshopQcloudUserId'];
		$c['manage']['config']['ManageLanguage'] && $data['lang']=$c['manage']['config']['ManageLanguage'];
		$data['Number']=$c['Number'];
		$data['timestamp']=$c['time'];
		$data=str::str_code($data, 'trim');
		$data['sign']=ly200::sign($data, $key);
		$curl_opt=array(
			CURLOPT_CONNECTTIMEOUT	=>	10,
			CURLOPT_TIMEOUT			=>	10
		);
		for($i=0;$i<5;$i++){//防止连接OA超时出现502
			$result=ly200::curl($url, $data, '', $curl_opt);
			if($result) break;//返回结果不为空则退出
		}
		if(!$result){
			$return['msg']='connection error';
			return $return;
		}else{
			$json_data=str::json_data($result, 'decode');
			if($json_data['ret']==1){
				return $json_data;
			}else{
				$return['msg']=$json_data['msg']?$json_data['msg']:$result;
				return $return;
			}
		}
	}

	public static function sign($data, $key){	//生成签名
		$str='';
		$data=str::str_code($data, 'trim');
		ksort($data);
		foreach($data as $k=>$v){
			if($k=='sign' || $v===''){continue;}
			$str.="$k=$v&";
		}
		return md5($str.'key='.$key);
	}

	public static function appkey($ApiName=''){
		global $c;
		$open_api=array('dianxiaomi', 'spdcat', 'openapi', 'ueeseo');//
		$appkey=$c['ApiKey'];
		@in_array($ApiName, $open_api) && $appkey=db::get_value('config', "GroupId='API' and Variable='AppKey'", 'Value');
		return $appkey;
	}

	public static function curl($url='', $post='', $referer='', $curl_opt=array(), $return_cookie=false){
		$options=array(
			CURLOPT_URL				=>	$url,
			CURLOPT_RETURNTRANSFER	=>	true,
			CURLOPT_CONNECTTIMEOUT	=>	60,
			CURLOPT_TIMEOUT			=>	60,
			CURLOPT_POST			=>	$post?true:false,
			CURLOPT_SSL_VERIFYPEER	=>	false,
			CURLOPT_REFERER			=>	$referer
		);
		$post && $options[CURLOPT_POSTFIELDS]=is_array($post)?http_build_query($post):$post;
		$return_cookie && $options[CURLOPT_HEADER]=true;
		foreach((array)$curl_opt as $k=>$v){
			$options[$k]=$v;
		}
		$ch=curl_init();
		curl_setopt_array($ch, $options);
		$result=curl_exec($ch);
		$handle=curl_getinfo($ch);
		//@substr_count($url, 'gw.api.alibaba.com') && ly200::debug($handle);
		//ly200::debug($handle);
		if($handle['http_code']!=200){return;}
		if($return_cookie){
			$raw_header=substr($result, 0, $handle['header_size']);
			$cookies=array();
			if(preg_match_all('/Set-Cookie:(?:\s*)([^=]*?)=([^\;]*?);/i', $raw_header, $cookie_match)){
				for($i=0; $i<count($cookie_match[0]); $i++){
					$cookies[$cookie_match[1][$i]]=$cookie_match[2][$i];
				}
			}
			curl_close($ch);
			return array(substr($result, -$handle['download_content_length']), $cookies);
		}
		curl_close($ch);
		return $result;
	}

	public static function getyy_balance($YY_code)
	{
	    $return_str="";
	    if ($YY_code)
	    {
	        $result=ly200::curl("http://47.106.88.138:8080/api/customerByNumber?number=".$YY_code);
	        if ($result)
	        {
	            $result_arr=json_decode($result, true);
	            if (isset($result_arr["currency"]) && isset($result_arr["balance"]))
	            {
	                if ($result_arr["balance"]){
                        $return_str=$result_arr["currency"].": ".sprintf("%.2f",$result_arr["balance"]);
                    }
	            }
	        }
	    }
	    return $return_str;
	}
	
	public static function debug($data){
		if(is_array($data)){
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}elseif(is_bool($data)){
			var_dump($data);
		}else{
			echo $data;
		}
	}

	public static function ueeshop_web_get_data(){
		global $c;
		$save_dir=$c['tmp_dir'].'manage/';
		$filename='analytics.json';
		$data=array('Action'=>'ueeshop_web_get_data');

		if(!file::check_cache($c['root_path'].$save_dir.$filename, $isThemes=0)){//文件是否存在、是否到更新时间
			$result=ly200::api($data, $c['ApiKey'], $c['api_url']);
			if($result['ret']==1){
				$contents=str::json_data(str::str_code($result['msg'], 'stripslashes'));
				file::write_file($save_dir, $filename, $contents);
			}
		}

		//加载统计json
		$data_object=@file_get_contents($c['root_path'].$save_dir.$filename);
		return str::json_data($data_object, 'decode');
	}

	public static function ueeshop_agent_info(){
		global $c;
		$data=array(
			'Action'	=>	'ueeshop_agent_info'
		);
		return ly200::api($data, $c['ApiKey'], $c['api_url']);
	}

	public static function check_user_id(){ //检测保持登录会员ID
		global $c;
		if(!(int)$_SESSION['User']['UserId']){
			@list($autologin_UserId, $autologin_Password)=@explode("\t", str::str_crypt(str::GetCookie('User'), 'decrypt'));
			if($autologin_UserId!='' && @strlen($autologin_Password)==32){
				$userinfo=db::get_one('user', "UserId='$autologin_UserId'");
				if(str::PwdCode($userinfo['Password'])==$autologin_Password){//登录成功
					if(($c['FunVersion']>=1 && $c['config']['global']['UserStatus'] && $userinfo['Status']==1) || !$c['config']['global']['UserStatus']){//审核通过
						$_SESSION['User']=$userinfo;
						$UserId=$userinfo['UserId'];
						$ip=ly200::get_ip();
						db::update('user', "UserId='{$UserId}'", array('LastLoginTime'=>$c['time'], 'LastLoginIp'=>$ip, 'LoginTimes'=>$userinfo['LoginTimes']+1));
						cart::login_update_cart();
						unset($_SESSION['Cart']['ShippingAddress']); //清空非会员收货地址信息
						user::operation_log($UserId, '会员登录(Cookie登录)', 1);
					}else{//审核不通过
						unset($_SESSION['User']);
						str::SetTheCookie('User');
					}
				}else{//密码不正确
					unset($_SESSION['User']);
					str::SetTheCookie('User');
				}
			}
		}
	}

	public static function set_session_id(){ //生成非会员ID
		global $c;
		$session_id='';
		if(!(int)$_SESSION['User']['UserId']){
			$session_id=substr(md5(md5(session_id())), 0, 10);
			$time=$c['time']+3600*24*31; //一个月期限
			if($_COOKIE['session_id']){
				$session_id=$_COOKIE['session_id'];
			}else{
				setcookie('session_id', $session_id, $time);
			}
		}
		return $session_id;
	}

	public static function is_mobile_client($type=0){	//是否用手机访问($type 1：电脑允许打开手机版，0：只允许手机打开手机版)
		global $c;
		$_SESSION['Ueeshop']['IsMobileClient']=0;
		// if((int)$c['FunVersion']==0 && $c['NewFunVersion']>=1) return $_SESSION['Ueeshop']['IsMobileClient'];//未开启手机版功能
		if($type && ((int)substr_count(ly200::get_domain(), '://m.') || preg_match('/^m\.(.*)/', $_SERVER['HTTP_HOST'], $host_match))){
			$_SESSION['Ueeshop']['IsMobileClient']=1;
		}else{
			if(@stripos($_SERVER['HTTP_USER_AGENT'], 'ipad')){//iPad
				$_SESSION['Ueeshop']['IsMobileClient']=0;
			}else{//其他移动端
				$phone_client_agent_array=array('240x320','acer','acoon','acs-','abacho','ahong','airness','alcatel','amoi','android','anywhereyougo.com','applewebkit/525','applewebkit/532','asus','audio','au-mic','avantogo','becker','benq','bilbo','bird','blackberry','blazer','bleu','cdm-','compal','coolpad','danger','dbtel','dopod','elaine','eric','etouch','fly ','fly_','fly-','go.web','goodaccess','gradiente','grundig','haier','hedy','hitachi','htc','huawei','hutchison','inno','ipaq','ipod','jbrowser','kddi','kgt','kwc','lenovo','lg ','lg2','lg3','lg4','lg5','lg7','lg8','lg9','lg-','lge-','lge9','longcos','maemo','mercator','meridian','micromax','midp','mini','mitsu','mmm','mmp','mobi','mot-','moto','nec-','netfront','newgen','nexian','nf-browser','nintendo','nitro','nokia','nook','novarra','obigo','palm','panasonic','pantech','philips','phone','pg-','playstation','pocket','pt-','qc-','qtek','rover','sagem','sama','samu','sanyo','samsung','sch-','scooter','sec-','sendo','sgh-','sharp','siemens','sie-','softbank','sony','spice','sprint','spv','symbian','tablet','talkabout','tcl-','teleca','telit','tianyu','tim-','toshiba','tsm','up.browser','utec','utstar','verykool','virgin','vk-','voda','voxtel','vx','wap','wellco','wig browser','wii','windows ce','wireless','xda','xde','zte','mobile');
				foreach($phone_client_agent_array as $v){
					if(@stripos($_SERVER['HTTP_USER_AGENT'], $v)){
						$_SESSION['Ueeshop']['IsMobileClient']=1;
						break;
					}
				}
				unset($phone_client_agent_array);
			}
		}
		if((int)$_GET['ueeshop_store']==1){//店铺模式
			$_SESSION['Ueeshop']['IsMobileClient']=1;
		}
		return $_SESSION['Ueeshop']['IsMobileClient'];
	}

	public static function lock_china_ip(){
		global $c;
		if(ly200::isCrawler() || ly200::checkrobot()){return false;}//蜘蛛爬虫
		if((int)$_SESSION['Manage']['UserName'] || $_SESSION['tryDomain']==1){return false;}//已登录后台，则跳过检测
		if((int)$c['config']['global']['IsIP'] && $_SESSION['Ueeshop']['Ip']!='' && $_SESSION['Ueeshop']['Ip']==ly200::get_ip() && (int)$_SESSION['Ueeshop']['LockChinaIp']){return true;}
		$_SESSION['Ueeshop']['Ip']=ly200::get_ip();
		$_SESSION['Ueeshop']['LockChinaIp']=0;
		if((int)$c['config']['global']['IsIP']){
			$ChinaProvince="中国/北京/浙江/天津/安徽/上海/福建/重庆/江西/山东/河南/内蒙古/湖北/新疆维吾尔/湖南/宁夏回族/广东/西藏/海南/广西壮族/四川/河北/贵州/山西/云南/辽宁/陕西/吉林/甘肃/黑龙江/青海/江苏";
			$IpArea=ly200::ip(ly200::get_ip());
			if(substr_count($_SERVER['PHP_SELF'], '/manage/')==0 && substr_count($ChinaProvince, substr($IpArea, 0, 6))>0){
				$_SESSION['Ueeshop']['LockChinaIp']=1;
				return true;
			}
		}
		return false;
	}

	public static function lock_china_browser(){
		global $c;
		if(ly200::isCrawler() || ly200::checkrobot()){return false;}//蜘蛛爬虫
		if((int)$_SESSION['Manage']['UserName'] || $_SESSION['tryDomain']==1){return false;}//已登录后台，则跳过检测

		if((int)$c['config']['global']['IsChineseBrowser'] && preg_match("/zh-cn/i", strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5)))){//简体中文版浏览器
			return true;
		}
		return false;
	}

	public static function lock_close_website(){
		global $c;
		if((int)$_SESSION['Manage']['UserName'] || $_SESSION['tryDomain']==1){return false;}//已登录后台，则跳过检测

		if((int)$c['config']['global']['IsCloseWeb']){//关闭网站
			return true;
		}
		return false;
	}

	public static function subdomain_list(){	//返回网站的二级域名列表
		global $c;
		$keys=array_keys($c['lang_name']);
		$pre_domain=array('www', 'm');
		return array_merge($pre_domain, $keys);
	}

	public static function get_http_browser(){ //返回当前浏览器的版本号
		$agent=$_SERVER['HTTP_USER_AGENT'];
		$browser_ary=array();
		$agent_ary=explode(' ', $agent);
		foreach($agent_ary as $k=>$v){
			if(strpos($v, '/')){
				$v=explode('/', $v);
				$browser_ary[$v[0]]=$v[1];
			}
		}
		unset($agent_ary);

		$return=array('browser'=>'unknown', 'version'=>'0');
		if(strpos($agent, 'MSIE')!==false){//ie11判断
			$return['browser']='ie';
			if(strpos($agent, 'rv:11.0')) $return['version']='11';
			elseif(strpos($agent, 'MSIE 10.0')) $return['version']='10';
			elseif(strpos($agent, 'MSIE 9.0')) $return['version']='9';
			elseif(strpos($agent, 'MSIE 8.0')) $return['version']='8';
			elseif(strpos($agent, 'MSIE 7.0')) $return['version']='7';
			elseif(strpos($agent, 'MSIE 6.0')) $return['version']='6';
		}elseif(strpos($agent, 'Firefox')!==false){
			$return=array('browser'=>'firefox', 'version'=>$browser_ary['Firefox']);
		}elseif(strpos($agent, 'Chrome')!==false){
			$return=array('browser'=>'chrome', 'version'=>$browser_ary['Chrome']);
		}elseif(strpos($agent, 'Opera')!==false){
			$return=array('browser'=>'opera', 'version'=>$browser_ary['Opera']);
		}elseif(strpos($agent, 'Chrome')==false && strpos($agent, 'Safari')!==false){
			$return=array('browser'=>'safari', 'version'=>$browser_ary['Safari']);
		}
		return $return;
	}

	/**********************产品效果(start)************************/
	/**
	 * 不同风格模板的首页产品框效果
	 *
	 * @param: $effects[int]		效果类型
	 * @param: $num[int]			当前产品序号
	 * @param: $row[array]			产品数据
	 * @param: $content[string]		产品详细资料
	 * @param: $length[int]			每一行显示的数量
	 * @param: $notClear[int]		需不需要自动换行  1:不换行 0:换行
	 * @return float
	 */
	public static function product_effects($effects, $num, $row, $content, $length=4, $notClear=0){
		global $c;
		$is_promition=($row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
		$url=ly200::get_url($row, 'products');
		$img=ly200::get_size_img($row['PicPath_0'], '240x240');
		$imgTo=ly200::get_size_img($row['PicPath_1'], '240x240');
		$name=$row['Name'.$c['lang']];
		$price_ary=cart::range_price_ext($row);
		$price_0 = $price_ary[1];
		$share_data=htmlspecialchars(str::json_data(array('title'=>$name, 'url'=>ly200::get_domain().$url)));

		$html='<div class="prod_box prod_box_'.$effects.' fl'.($num%$length==0?' first':'').'">';
			$html.='
				<div class="prod_box_pic">
					<a class="pic_box" href="'.$url.'" title="'.$name.'">
						<img'.($effects==6?' class="thumb"':'').' src="'.$img.'" /><span></span>'.($effects==4?'<span class="icon_eyes"></span>':'').(($effects==6 && $imgTo)?'<em class="thumb_hover"><img src="'.$imgTo.'" /><span></span></em>':'').'
					</a>
					'.($is_promition?'<em class="icon_discount DiscountBgColor"><b>'.@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100)).'</b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em>':'').'
					<em class="icon_seckill DiscountBgColor">'.$c['lang_pack']['products']['sale'].'</em>
				</div>
			';
			$html.='<div class="prod_box_info">';
				$html.='<div class="prod_box_inner">'.$content.'</div>';
				if($effects==1){
					$html.='<div class="prod_box_view">
						<div class="prod_box_button">
							<div class="addtocart fr"><a href="javascript:;" rel="nofollow" class="add_cart" data="'.$row['ProId'].'">'.$c['lang_pack']['products']['addToCart'].'</a></div>
							'.($c['config']['products_show']['Config']['favorite']?'<div class="wishlist fl"><a href="javascript:;" rel="nofollow" class="add_favorite" data="'.$row['ProId'].'"></a></div>':'').'
							<div class="compare fl"><a href="javascript:;" rel="nofollow" class="share_this" data="'.$share_data.'"></a></div>
						</div>
					</div>';
				}elseif($effects==2){
					$html.='<div class="add_cart_box"><div class="add_cart_bg ProListBgColor"></div><a href="javascript:;" rel="nofollow" class="add_cart" data="'.$row['ProId'].'">'.$c['lang_pack']['products']['addToCart'].'</a></div>';
				}elseif($effects==3){
					$html.='<div class="button_group">
						<div class="addtocart fl"><a href="javascript:;" rel="nofollow" class="add_cart ProListBgColor" data="'.$row['ProId'].'">'.$c['lang_pack']['products']['addToCart'].'</a></div>
						'.($c['config']['products_show']['Config']['favorite']?'<div class="wishlist fr"><a href="javascript:;" rel="nofollow" class="add_favorite" data="'.$row['ProId'].'"></a></div>':'').'
					</div>';
				}elseif($effects==4){
					$html.='<div class="prod_action">
						'.($c['config']['products_show']['Config']['favorite']?'<div class="wishlist fl"><a href="javascript:;" rel="nofollow" class="add_favorite" data="'.$row['ProId'].'"></a></div>':'').'
						<div class="addtocart fl"><a href="javascript:;" rel="nofollow" class="add_cart" data="'.$row['ProId'].'">'.$c['lang_pack']['products']['addToCart'].'</a></div>
						<div class="compare fr"><a href="javascript:;" rel="nofollow" class="share_this" data="'.$share_data.'"></a></div>
					</div>';
				}
			$html.='</div>';
		$html.='</div>';
		if(!$notClear && (($num+1)%$length)==0){ $html.='<div class="clear"></div>'; }
		return $html;
	}
	/**********************产品效果(end)************************/

	public static function system_email_tpl($txt, $data=array()){//系统邮件模板,$data传入的参数
		if($txt==''){return '';}
		global $c;
		//全局变量
		if($c['config']['global']['LogoPath']){
			$LogoPath=$c['config']['global']['LogoPath'];
		}else{
			$LogoPath=db::get_value('config', "GroupId='global' and Variable='LogoPath'", 'Value');
		}
		$EmailStr	= $data['EmailStr'];
		$PasswordStr= $data['PasswordStr'];
		$orders_row	= (array)$data['orders_row'];
		$LogoPath	= '<img src="'.ly200::get_domain().$LogoPath.'" style="max-width:350px;" border="0" />';
		$Domain		= ly200::get_domain(0);
		$FullDomain	= ly200::get_domain();
		$time		= date('m/d/Y H:i:s', $c['time']);
		$Email		= $orders_row?$orders_row['Email']:($EmailStr?$EmailStr:$_SESSION['User']['Email']);
		$Password	= $PasswordStr?$PasswordStr:'********';
		$UserName	= $orders_row?($orders_row['ShippingFirstName'].' '.$orders_row['ShippingLastName']):($_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']);
		$UserName	= trim($UserName);
		$UserName	= htmlspecialchars($UserName?$UserName:$Email);
		$Global_ary	= array('{Logo}', '{Domain}', '{FullDomain}', '{Time}', '{UserName}', '{Email}', '{Password}');
		$Global_val	= array($LogoPath, $Domain, $FullDomain, $time, $UserName, $Email, $Password);
		$txt		= str_replace($Global_ary, $Global_val, $txt);
		//会员注册验证邮箱模板
		$backUrl=$data['backUrl'];
		if($backUrl){
			$txt=str_replace('{VerUrl}', $backUrl, $txt);
		}
		//会员忘记密码模板
		$ForgotUrl=$data['ForgotUrl'];
		if($ForgotUrl){
			$txt=str_replace('{ForgotUrl}', $ForgotUrl, $txt);
		}
		//订单模板
		$isFee=(int)$data['isFee'];
		if($orders_row['OId']){
			//订单全局变量
			$OrderNum=$OId=$orders_row['OId'];
			$total_price=orders::orders_price($orders_row, $isFee);
			//订单详情
			if(strstr($txt, '{OrderDetail}')){
				ob_start();
				$default_lang=$c['manage']?$c['manage']['config']['LanguageDefault']:$c['config']['global']['LanguageDefault'];
				$c['lang_pack_email']=include($c['root_path'].'/static/static/inc/mail/lang/'.($c['lang']?substr($c['lang'], 1):$default_lang).'.php');//加载语言包
				include($c['root_path'].'/static/static/inc/mail/order_detail.php');
				$OrderDetail=ob_get_contents();
				ob_end_clean();
			}else{
				$OrderDetail='';
			}
			$OrderUrl	= ly200::get_domain().'/account/orders/view'.$orders_row['OId'].'.html';
			$OrderStatus= $c['orders']['status'][$orders_row['OrderStatus']];
			$OrderPrice	= $orders_row['Currency'].' '.cart::iconv_price(0, 1, $orders_row['Currency']).$total_price;
			$orders_ary	= array('{OrderNum}', '{OrderDetail}', '{OrderUrl}', '{OrderStatus}', '{OrderPrice}');
			$orders_val	= array($OrderNum, $OrderDetail, $OrderUrl, $OrderStatus, $OrderPrice);
			$txt		= str_replace($orders_ary, $orders_val, $txt);
			unset($orders_ary, $orders_val, $OrderDetail);
			//订单全局变量 结束
			//创建订单
			$OrderPaymentUrl	= ly200::get_domain()."/cart/complete/{$orders_row['OId']}.html";
			$OrderPaymentName	= $orders_row['PaymentMethod'];
			$orders_ary			= array('{OrderPaymentUrl}', '{OrderPaymentName}');
			$orders_val			= array($OrderPaymentUrl, $OrderPaymentName);
			$txt				= str_replace($orders_ary, $orders_val, $txt);
			unset($orders_ary, $orders_val);
			//订单发货模板
			$shipping_cfg	= $data['shipping_cfg'];
			$shipping_row	= $data['shipping_row'];
			$ShippingName	= (int)$orders_row['ShippingMethodSId']?$shipping_cfg['Express']:($orders_row['ShippingMethodType']=='air'?$shipping_cfg['AirName']:$shipping_cfg['OceanName']);
			$ShippingBrief	= $shipping_row['Brief'];;
			$TrackingNumber	= ($data['trackingNumberStr']?$data['trackingNumberStr']:$orders_row['TrackingNumber']);
			$ShippingTime	= @date('m/d-Y', $data['ShippingTimeStr']?$data['ShippingTimeStr']:$orders_row['ShippingTime']);
			$QueryUrl		= $shipping_cfg['Query'];
			$orders_ary		= array('{ShippingName}', '{ShippingBrief}', '{TrackingNumber}', '{ShippingTime}', '{QueryUrl}');
			$orders_val		= array($ShippingName, $ShippingBrief, $TrackingNumber, $ShippingTime, $QueryUrl);
			$txt			= str_replace($orders_ary, $orders_val, $txt);
			unset($orders_ary, $orders_val);
		}
		return $txt;
	}

	public static function search_logs($count){
		global $c;
		$keyword=strip_tags($_GET['Keyword']);
		if($_SESSION['Search']['Keyword']==$keyword || !trim($keyword)) return;
		$keyword_row=db::get_one('search_logs',"Keyword='$keyword'");
		$selt_ip=ly200::get_ip();
		$selt_country=ly200::ip($selt_ip,'country');
		if($keyword_row){
			$country_ary=explode('|',$keyword_row['Country']);
			$country_ary=array_filter($country_ary);
			if(!in_array($selt_country,$country_ary)) $country_ary[]=$selt_country;
			$country_ary='|'.implode('|',$country_ary).'|';
			db::update('search_logs',"Keyword='$keyword'",array(
				'Result'	=>	(int)$count,
				'Number'	=>	$keyword_row['Number']+1,
				'Country'	=>	$country_ary,
				'AccTime'	=>	$c['time']
			));
		}else{
			db::insert('search_logs',array(
				'Keyword'	=>	$keyword,
				'Result'	=>	(int)$count,
				'Number'	=>	1,
				'Country'	=>	"|$selt_country|",
				'AccTime'	=>	$c['time']
			));
		}
		$_SESSION['Search']['Keyword']=$keyword;
	}

	/**
	 * 产品所属的属性选项
	 *
	 * @param: $CateId[int]		当前产品分类ID
	 * @return float
	 */
	public static function product_attribute_attr_id($CateId){
		global $c;
		$ParentId=0;
		$category_row=$row=str::str_code(db::get_one('products_category', "CateId='$CateId'", 'UId, AttrId, Dept'));
		if($category_row){
			for($i=0; $i<$category_row['Dept']; ++$i){
				$ParentId=(int)$row['AttrId'];
				if($ParentId==0){//还没有属性ID的记录
					$UpCateId=category::get_FCateId_by_UId($row['UId']);
					$row=str::str_code(db::get_one('products_category', "CateId='$UpCateId'", 'UId, AttrId'));
				}else{//已有记录
					break;
				}
			}
		}
		return $ParentId;
	}

	/**
	 * 产品选项卡的属性部分
	 *
	 * @param: $ProId[int]		当前产品ID
	 * @param: $ParentId[int]	当前产品所属的属性分类ID
	 * @param: $IsManage[int]	当前位置是否在后台
	 * @return float
	 */
	public static function product_attribute_tab($ProId, $CateId, $IsManage=0)
    {
        global $c;
        //语言
		$ManageLanguage=explode(',', db::get_value('config', "GroupId='global' and Variable='Language'", 'Value'));
		if ($c['NewFunVersion'] >= 6) {
			//全局属性选项卡
			$global_category_desc_ary = array();
	        $new_global_category_desc_ary = array();
	        $myorder_ary = array();
			$desc_attr_row = str::str_code(db::get_all('products_attribute', '(CateId="0" or CateId="") and DescriptionAttr=1', '*', $c['my_order'].'AttrId asc'));
			foreach ((array)$desc_attr_row as $k => $v) {
				$global_category_desc_ary['Num:' . $k] = $v; //旧的全局数据
				$new_global_category_desc_ary[$v['AttrId']] = $v; //新的全局数据
				$myorder_ary[] = $v['AttrId']; //排序
			}
			//分类属性选项卡
			$category_desc_ary = array();
			$desc_attr_row = str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%' and DescriptionAttr=1", '*', $c['my_order'].'AttrId asc'));
			foreach ((array)$desc_attr_row as $k => $v) {
				$category_desc_ary[$v['AttrId']] = $v;
				$myorder_ary[] = $v['AttrId']; //排序
			}
			//产品选项卡
			$pro_desc_ary = array();
			if ($ProId > 0) {
				$pro_tab_ary = array();
				$pro_tab_row = db::get_all('products_tab', "ProId='{$ProId}'", '*', 'TId asc');
				foreach ((array)$pro_tab_row as $k => $v) {
					if ($v['IsUsed'] == 1) {
						if ($v['AttrId']) {
							$pro_tab_ary[$v['AttrId']] = $v;
						} else {
							$pro_tab_ary['Num:' . $k] = $v;
						}
					}
				}
				foreach ((array)$pro_tab_ary as $k => $v) {
					$isProName = 0;
	                $isProDesc = 0;
					foreach ((array)$ManageLanguage as $lang) {
						if (trim($v["Name_$lang"])) {
							$isProName += 1;
						}
						if (trim($v["Description_$lang"])) {
							$isProDesc += 1;
						}
					}
					$row = (!strstr($k, 'Num:') ? $category_desc_ary[$k] : $global_category_desc_ary[$k]);
					//!$row && $row = $new_global_category_desc_ary[$k]; //新的全局数据
					if ($isProName > 0) {
						foreach ((array)$ManageLanguage as $lang) {
							$pro_desc_ary[$k]["Name_$lang"] = $pro_tab_ary[$k]["Name_$lang"];
						}
					} else {
						foreach ((array)$ManageLanguage as $lang) {
							$pro_desc_ary[$k]["Name_$lang"] = $row["Name_$lang"];
						}
					}
					if ($isProDesc > 0) {
						foreach ((array)$ManageLanguage as $lang) {
							$pro_desc_ary[$k]["Description_$lang"] = $pro_tab_ary[$k]["Description_$lang"];
						}
					} else {
						foreach ((array)$ManageLanguage as $lang) {
							$pro_desc_ary[$k]["Description_$lang"] = $row["Description_$lang"];
						}
					}
					if ($IsManage && !strstr($k, 'Num:') && $row['CateId'] != '0' && !db::get_row_count('products_attribute', "AttrId='{$k}' and CateId like '%|{$CateId}|%' and DescriptionAttr=1")) {
						//全局选项卡的无需判断
						unset($pro_desc_ary[$k]);
					}
				}
			} else {
				$pro_desc_ary = $category_desc_ary;
			}
	        //“所有产品可用”选项卡
	        if ($new_global_category_desc_ary) {
	            foreach ((array)$new_global_category_desc_ary as $k => $v) {
	                foreach ((array)$ManageLanguage as $lang) {
	                    $pro_desc_ary[$k]["Name_$lang"] = $v["Name_$lang"];
	                    $pro_desc_ary[$k]["Description_$lang"] = $v["Description_$lang"];
	                }
	            }
	        }
	        //输出的排序
			$i = 0;
			$count = count($pro_desc_ary);
			$result_ary = array();
			foreach ($myorder_ary as $v) {
				if ($pro_desc_ary[$v]) {
					$result_ary[$v] = $pro_desc_ary[$v];
				}
			}
			foreach ($pro_desc_ary as $k => $v) {
				if (!$myorder_ary[$k]) {
					$result_ary[$k] = $v;
				}
			}
		} else {
			//全局属性选项卡
			$global_category_desc_ary=$new_global_category_desc_ary=$myorder_ary=array();
			$desc_attr_row=str::str_code(db::get_all('products_attribute', '(CateId="0" or CateId="") and DescriptionAttr=1', '*', $c['my_order'].'AttrId asc'));
			foreach((array)$desc_attr_row as $k=>$v){
				$global_category_desc_ary['Num:'.$k]=$v;//旧的全局数据
				$new_global_category_desc_ary[$v['AttrId']]=$v;//新的全局数据
				$myorder_ary[]=$v['AttrId'];//排序
			}
			//分类属性选项卡
			$category_desc_ary=array();
			$desc_attr_row=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%' and DescriptionAttr=1", '*', $c['my_order'].'AttrId asc'));
			foreach((array)$desc_attr_row as $k=>$v){
				$category_desc_ary[$v['AttrId']]=$v;
				$myorder_ary[]=$v['AttrId'];//排序
			}
			//产品选项卡
			$pro_desc_ary=array();
			if($ProId>0){
				$pro_tab_ary=array();
				$pro_tab_row=db::get_all('products_tab', "ProId='{$ProId}'", '*', 'TId asc');
				foreach((array)$pro_tab_row as $k=>$v){
					if($v['IsUsed']==1){
						if($v['AttrId']){
							$pro_tab_ary[$v['AttrId']]=$v;
						}else{
							$pro_tab_ary['Num:'.$k]=$v;
						}
					}
				}
				foreach((array)$pro_tab_ary as $k=>$v){
					$isProName=$isProDesc=0;
					foreach((array)$ManageLanguage as $lang){
						if(trim($v["Name_$lang"])){
							$isProName+=1;
						}
						if(trim($v["Description_$lang"])){
							$isProDesc+=1;
						}
					}
					$row=(!strstr($k, 'Num:')?$category_desc_ary[$k]:$global_category_desc_ary[$k]);
					!$row && $row=$new_global_category_desc_ary[$k];//新的全局数据
					if($isProName>0){
						foreach((array)$ManageLanguage as $lang){
							$pro_desc_ary[$k]["Name_$lang"]=$pro_tab_ary[$k]["Name_$lang"];
						}
					}else{
						foreach((array)$ManageLanguage as $lang){
							$pro_desc_ary[$k]["Name_$lang"]=$row["Name_$lang"];
						}
					}
					if($isProDesc>0){
						foreach((array)$ManageLanguage as $lang){
							$pro_desc_ary[$k]["Description_$lang"]=$pro_tab_ary[$k]["Description_$lang"];
						}
					}else{
						foreach((array)$ManageLanguage as $lang){
							$pro_desc_ary[$k]["Description_$lang"]=$row["Description_$lang"];
						}
					}
					if($IsManage && !strstr($k, 'Num:') && $row['CateId']!='0' && !db::get_row_count('products_attribute', "AttrId='{$k}' and CateId like '%|{$CateId}|%' and DescriptionAttr=1")){
						//全局选项卡的无需判断
						unset($pro_desc_ary[$k]);
					}
				}
			}else{
				$pro_desc_ary=$category_desc_ary;
			}
			$i=0;
			$count=count($pro_desc_ary);
			$result_ary=array();
			foreach($myorder_ary as $v){
				if($pro_desc_ary[$v]){
					$result_ary[$v]=$pro_desc_ary[$v];
				}
			}
			foreach($pro_desc_ary as $k=>$v){
				if(!$myorder_ary[$k]){
					$result_ary[$k]=$v;
				}
			}
		}
		return $result_ary;
	}

	/**
	 * 把ID号拿出来并自动排成字符串格式
	 *
	 * @param: $row[object]	导入的数据
	 * @param: $id[String]	ID号名称
	 * @param: $TwoD[int]	二级循环
	 * @return float
	 */
	public static function get_id_by_row($row, $id, $TwoD=0){
		global $c;
		$id_list=array();
		foreach((array)$row as $k=>$v){
			if($TwoD==1){
				foreach((array)$v as $k1=>$v1){
					$id_list[]=$v1[$id];
				}
			}else{
				$v[$id] && $id_list[]=$v[$id];
			}
		}
		$id_list=str::ary_format($id_list, 2);
		return $id_list;
	}

	/**
	 * 产品所属的属性选项
	 *
	 * @param: $products_row	产品记录
	 * @return array()
	 */
	public static function get_pro_info($products_row){
		global $c;
		/*
		$count_row=(array)str::json_data(str::str_code(db::get_value('config', 'GroupId="products" and Variable="batch_edit"', 'Value'), 'htmlspecialchars_decode'), 'decode');
		$ishot=db::get_row_count('products', "ProId='{$products_row['ProId']}' and (IsIndex=1 or IsNew=1 or IsHot=1 or IsBestDeals=1)");
		$type_ary=array('Sales', 'FavoriteCount', 'DefaultReviewRating', 'DefaultReviewTotalRating');
		//0=>Sales (销量) , 1=>FavoriteCount (收藏数), 2=>DefaultReviewRating (评论平均分), 3=>DefaultReviewTotalRating (评论总人数)
		$return=array();
		if($products_row){
			$return[0]=(int)$products_row['Sales']; //销量
			$return[1]=(int)$products_row['FavoriteCount']; //收藏数
			$return[2]=ceil($products_row['Rating']); //评论平均分
			!ceil($products_row['Rating']) && $return[2]=in_array('batch_edit', (array)$c['plugins']['Used'])?$count_row['DefaultReviewRating']:0;
			$return[3]=(int)$products_row['TotalRating']; //评论总人数
			if(in_array('batch_edit', (array)$c['plugins']['Used'])){
				$id_befor=(int)substr($products_row['ProId'], 0, 2);
				$id_after=(int)substr($products_row['ProId'], -2);
				$num = (int)@substr((($id_befor+$id_after)/3.1415926), -2)/2;//生成随机数
				$num1 = (int)@substr((($id_befor+$id_after)/3.1415), -2)/2;//生成随机数
				foreach((array)$return as $k=>$v){
					if($k==2) continue;
					if($k==1){
						$return[$k]=(int)($v+$num1+$count_row[$type_ary[$k]]);
					}else{
						if($ishot){
							$return[$k]=(int)($v+$num+50+$count_row[$type_ary[$k]]);
						}else{
							$return[$k]=(int)($v+$num+$count_row[$type_ary[$k]]);
						}
					}
				}
				$return[1] = $return[1]+$return[3];
			}
		}
		return $return;*/
		// $type_ary=array('Sales', 'FavoriteCount', 'DefaultReviewRating', 'DefaultReviewTotalRating');
		//0=>Sales (销量) , 1=>FavoriteCount (收藏数), 2=>DefaultReviewRating (评论平均分), 3=>DefaultReviewTotalRating (评论总人数)
		$row=db::get_one('products_development', "ProId='{$products_row['ProId']}'");
		$return=array();
		if($products_row){
			$return[0]=(int)$products_row['Sales']; //销量
			$return[1]=(int)$products_row['FavoriteCount']; //收藏数
			$return[2]=ceil($products_row['Rating']); //评论平均分
			$return[3]=(int)$products_row['TotalRating']; //评论总人数
			if(@in_array('custom_comments', (array)$c['plugins']['Used']) && $row['IsVirtual']){
				$return[0]=(int)($products_row['Sales'] > $row['Sales'] ? $products_row['Sales'] : $row['Sales']); //销量
				$return[1]=(int)($products_row['FavoriteCount'] > $row['FavoriteCount'] ? $products_row['FavoriteCount'] : $row['FavoriteCount']); //收藏数
				$return[2]=(int)(ceil($products_row['Rating']) > $row['DefaultReviewRating'] ? ceil($products_row['Rating']) : $row['DefaultReviewRating']); //评论平均分
				$return[3]=(int)($products_row['TotalRating'] > $row['DefaultReviewTotalRating'] ? $products_row['TotalRating'] : $row['DefaultReviewTotalRating']); //评论总人数
			}
		}
		return $return;
	}

	/**********************过滤搜索引擎蜘蛛爬虫(start)************************/
	public static function isCrawler(){
		$agent=@strtolower($_SERVER['HTTP_USER_AGENT']);
		if(!empty($agent)) {
			$spiderSite=array("TencentTraveler", "Baiduspider+", "BaiduGame", "Googlebot", "msnbot", "Sosospider+", "Sogou web spider", "ia_archiver", "Yahoo! Slurp", "YoudaoBot", "Yahoo Slurp", "MSNBot", "Java (Often spam bot)", "BaiDuSpider", "Voila", "Yandex bot", "BSpider", "twiceler", "Sogou Spider", "Speedy Spider", "Google AdSense", "Heritrix", "Python-urllib", "Alexa (IA Archiver)", "Ask", "Exabot", "Custo", "OutfoxBot/YodaoBot", "yacy", "SurveyBot", "legs", "lwp-trivial", "Nutch", "StackRambler", "The web archive (IA Archiver)", "Perl tool", "MJ12bot", "Netcraft", "MSIECrawler", "WGet tools", "larbin", "Fish search", "Google Page Speed Insights", "Chrome-Lighthouse");
			foreach($spiderSite as $val){
				$str=@strtolower($val);
				if(@strpos($agent, $str)!==false){return true;}
			}
		}else{
			return false;
		}
	}

	public static function checkrobot($useragent=''){//Discuz判断蜘蛛爬虫方法
		static $kw_spiders = array('bot', 'crawl', 'spider' ,'slurp', 'sohu-search', 'lycos', 'robozilla');
		static $kw_browsers = array('msie', 'netscape', 'opera', 'konqueror', 'mozilla');

		$useragent = @strtolower(empty($useragent) ? $_SERVER['HTTP_USER_AGENT'] : $useragent);
		if(@strpos($useragent, 'http://')===false && ly200::dstrpos($useragent, $kw_browsers)) return false;
		if(ly200::dstrpos($useragent, $kw_spiders)) return true;
		return false;
	}

	public static function dstrpos($string, $arr, $returnvalue=false) {
		if(empty($string)) return false;
		foreach((array)$arr as $v){
			if(strpos($string, $v)!== false){
				$return = $returnvalue ? $v : true;
				return $return;
			}
		}
		return false;
	}
	/**********************过滤搜索引擎蜘蛛爬虫(end)************************/

	/**********************可视化编辑(start)************************/
	public static function visual_ad($data){  //广告图
		global $c;
		if($data){  //有传入数据为广告图类型
			$html='<div>';
			$data['Link'] && $html.="<a href='{$data['Link']}' target='_blank'>";
			$html.="<img src='{$data['Pic']}' alt='{$data['Title']}' plugins_mod='Pic'>";
			$data['Link'] && $html.='</a>';
			$html.='</div>';
		}else{  //没传入数据为Banner类型
			/**
			 * Banner轮播插件
			 *
			 * Type: Banner图片类型  0.宽高不固定,图片自动缩 1.固定宽高,图片等比例缩 2.宽高固定,图片不缩
			 * ShowType: 显示方式  0渐显 1.上滚动 2.做滚动
			 */
			if(!$c['web_pack']['banner'][0]) return;
			if(!method_exists('themes_set', 'themes_banner_init')) exit('请技术先配置广告图函数');
			$lang=substr($c['lang'], 1);
			$param=$c['web_pack']['banner'][0];
			$width=$param[0]['Banner']['width'];
			!$width && $width=1920;
			$height=$param[0]['Banner']['height'];
			$config=themes_set::themes_banner_init();
			$font=array();  //需要载入的字体库
			$html='<div plugins="banner-0" effect="0-1">';
				$html.='<div id="banner_edit" data-type="'.$config['Type'].'" data-slidetype="'.$param['Layout'].'" data-width="'.$width.'" data-height="'.$height.'">';
					$html.='<div class="banner_box">';
						$tab='<div class="banner_tab">';
						for($i=0;$i<count($param);$i++){
							if(!is_file($c['root_path'].$param[$i]['Banner']['url']) && !substr_count($param[$i]['Banner']['url'], $c['cdn'])) continue;
							if(!in_array($param[$i]['Title']['font-family'], $font)) $font[]=$param[$i]['Title']['font-family'];
							if(!in_array($param[$i]['SubTitle']['font-family'], $font)) $font[]=$param[$i]['SubTitle']['font-family'];
							$js_json=array('banner'=>$param[$i]['Banner'], 'title'=>$param[$i]['Title'], 'brief'=>$param[$i]['SubTitle']);
							unset(  //去除不是样式的参数
								$js_json['banner']['url'],
								$js_json['banner']['link'],
								$js_json['banner']['width'],
								$js_json['banner']['height'],
								$js_json['title']['content'],
								$js_json['title']['oldcontent'],
								$js_json['brief']['content']
							);
							$tab.='<a href="javascript:;" '.($i?'':'class="on"').'></a>';
							$alt=$param[$i]['Title']['content'];
							if($param[$i]['Title']['oldcontent']) $alt=$param[$i]['Title']['oldcontent'];  //旧数据导过来后没编辑过banner图用回旧的alt
							$html.='<div class="banner_list" data-data="'.htmlspecialchars(str::json_data($js_json)).'">';
								if($param[$i]['Banner']['link']) $html.='<a href="'.$param[$i]['Banner']['link'].'" title="'.$alt.'">';
									$html.='<div class="banner_position">';
										$html.='<img src="'.$param[$i]['Banner']['url'].'" alt="'.$alt.'" />';
										$html.='<div class="banner_title">'.$param[$i]['Title']['content'].'</div>';
										$html.='<div class="banner_brief">'.$param[$i]['SubTitle']['content'].'</div>';
									$html.='</div>';
								if($param[$i]['Banner']['link']) $html.='</a>';
							$html.='</div>';
						}
						$tab.='</div>';
					$html.='</div>';
					$html.=$tab;
					$html.='<a class="banner_prev" href="javascript:;"></a>';
					$html.='<a class="banner_next" href="javascript:;"></a>';
					$html.='<div class="banner_loading"></div>';
				$html.='</div>';
			$html.='</div>';
			$txt_font_fiter_ary=array(  //系统默认,不需要单独加载
				'Arial',
				'Verdana',
				'微软雅黑'
			);
			$font=array_filter($font);
			foreach((array)$font as $v){  //载入字体库
				if(in_array($v, $txt_font_fiter_ary)) continue;
				$html.="<link href='{$c['cdn']}static/font/$v/font.css' rel='stylesheet' type='text/css' />\r\n";
			}
			$html.=ly200::load_static('/static/js/plugin/banner/zbanner.js');
			$html.='<script>$(function(){zbanner_init();})</script>';
		}
		return $html;
	}
	/**********************可视化编辑(end)************************/
	public static function official_website_landing(){
		global $c;
		if(substr_count($_SERVER['HTTP_REFERER'], $c['config']['global']['tryDomain']) && $c['NewFunVersion'] >= 5){
			$_SESSION['tryDomain'] = 1;
		}
	}
	public static function get_order_property($property){
		$attr=array();
		$attr=unserialize($property);
		!$attr && $attr=unserialize(htmlspecialchars_decode($property));
		!$attr && $attr=str::json_data($property, 'decode');
		!$attr && $attr=str::json_data(htmlspecialchars_decode($property), 'decode');
		return $attr;
	}

	//add by zhz start
    public static function zhzcurl($url){
        $token = "258d9ee0-7d2d-11ea-b62d-2feeaf422946";
        $headers = array('X-Token:'.$token);
        $ch = curl_init ();
        curl_setopt ($ch, CURLOPT_URL, $url );
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ($ch, CURLOPT_HEADER, 0 );
        curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 5); // 定义超时5秒钟
        $result = curl_exec($ch);
        curl_close ( $ch );
        return $result;
    }
    //add by zhz end
}
?>