<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class shopify{
	/******************************************************** access_token 设置(start) *****************************************************************/
	public static function set_default_authorization($AId=0){//设置默认速卖通账号
		global $c;
		
		$Account=$_SESSION['Manage']['Shopify']['Account'];
		$w="Platform='shopify'";
		if(!(int)$AId && db::get_row_count('authorization', "{$w} and Account='{$Account}'")) return $Account;
		
		(int)$AId && $w.=" and AId='$AId'";
		$row=db::get_one('authorization', $w, '*', 'AId asc');
		if(!$row['AId']) return;
		$_SESSION['Manage']['Shopify']['AuthorizationId']=$row['AId'];
		$_SESSION['Manage']['Shopify']['Account']=$row['Account'];
		return $row['Account'];
	}
	/******************************************************** access_token 设置(end) *****************************************************************/
	
	
	public static function save_shopify_images($ShopifyImage){//将Shopify图片保存到本地
		global $c;
		if(!$ShopifyImage) return;//图片为空

		$resize_ary=array('default', '640x640', '500x500', '240x240');
		$save_dir='/u_file/'.date('ym/').'products/'.date('d/');
		file::mk_dir($save_dir);
		
		$ShopifyName=file::get_base_name($ShopifyImage);//图片名称
		$UeeshopImagesUrl=db::get_value('products_shopify_images', "ShopifyImageUrl='{$ShopifyImage}'", 'UeeshopImagesUrl');//检查此图片是否曾保存到本地
		if(@is_file($c['root_path'].$UeeshopImagesUrl)){//此图片已下载过
			$ext_name=file::get_ext_name($UeeshopImagesUrl);
			$PicPath=$save_dir.str::rand_code().'.'.$ext_name;
			@copy($c['root_path'].$UeeshopImagesUrl.".default.{$ext_name}", $c['root_path'].$PicPath);//复制原图
		}else{//下载图片
			$curl=@curl_init(); 
			@curl_setopt($curl, CURLOPT_URL, $ShopifyImage);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			@curl_setopt($curl, CURLOPT_TIMEOUT, 300);
			@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
			@curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
			@curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			@curl_setopt($curl, CURLOPT_REFERER, 'http://'.$_SERVER['HTTP_HOST']);
			@curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11');
			$ShopifyImagePath=@curl_exec($curl);
			@curl_close($curl);
			$ext_name=file::get_ext_name($ShopifyImage);
			$PicPath=file::write_file($save_dir, str::rand_code().'.'.$ext_name, $ShopifyImagePath);
			@copy($c['root_path'].$PicPath, $c['root_path'].$PicPath.".default.{$ext_name}");//保留原图
			db::insert('products_shopify_images', array(
					'ShopifyName'		=>	$ShopifyName,
					'ShopifyImageUrl'	=>	$ShopifyImage,
					'UeeName'			=>	@str_replace('/', '-', trim($PicPath, '/')),
					'UeeshopImagesUrl'	=>	$PicPath
				)
			);
		}
		
		return $PicPath;
	}
	
	public static function remove_shopify_images($Images){//删除Shopify图片记录表
		global $c;
		$UeeName=@str_replace('/', '-', trim($Images, '/'));
		db::delete('products_shopify_images', "UeeName='{$UeeName}'");
	}
}
?>