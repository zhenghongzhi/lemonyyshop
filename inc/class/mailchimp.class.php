<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class mailchimp{

	/**
	*	最主要的方法，
	*	$url 		请求地址
	*	$method 	用GET获取信息，用POST创建信息
	*	$form_data 	POST创建信息需要用到的数据
	*/
	public static function get_info($url, $method=0, $form_data=array()){ //$method为1，POST
		global $c;
		$MailChimpOauthData=db::get_value('config', 'GroupId="MailChimp" and Variable="MailChimpOauth"', 'Value');
		$MailChimpOauthAry=str::json_data($MailChimpOauthData, 'decode');
		if(!empty($MailChimpOauthAry)){
			$dc=$MailChimpOauthAry['dc'];
			$apikey=$MailChimpOauthAry['access_token'].'-'.$MailChimpOauthAry['dc'];
			$newurl = "https://{$dc}.api.mailchimp.com/3.0".$url.(!$method?'?offset=0&count=2000':'');
			$apikey_ary=explode('-', $apikey);
			$curl=@curl_init(); 
			@curl_setopt($curl, CURLOPT_URL, $newurl);
			@curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
			@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
			@curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			@curl_setopt($curl, CURLOPT_HEADER, TRUE);
			@curl_setopt($curl, CURLOPT_TIMEOUT, 60);
			@curl_setopt($curl, CURLOPT_USERPWD, "anystring:{$apikey}");
			@curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/json"));
			@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			!empty($form_data) && @curl_setopt($curl, CURLOPT_POSTFIELDS,$form_data);
			$method==1 && @curl_setopt($curl, CURLOPT_POST, true);
			$method == 2 && @curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
			$result=@curl_exec($curl);
			$info=@curl_getinfo($curl);
			@curl_close($curl);
			$rep_ary=explode("\r\n\r\n", $result);
		}else{
			$rep_ary=array();
		}
		$header_res=in_array('HTTP/1.1 100 Continue', explode("\r\n", $rep_ary[0]))?explode("\r\n", $rep_ary[1]):explode("\r\n", $rep_ary[0]);
		$res=in_array('HTTP/1.1 100 Continue', explode("\r\n", $rep_ary[0]))?str::json_data($rep_ary[2],'decode'):str::json_data($rep_ary[1],'decode');
		$data=array(
			'header'	=>	$header_res,
			'response'	=>	$res,
			'error'		=>	in_array('HTTP/1.1 200 OK',$header_res)?0:1,
			'detail'	=>	in_array('HTTP/1.1 200 OK',$header_res)?'':$res['detail']
		);
		return $data;
	}

	/**
	*	查询文件夹是否存在，不存在就新建
	*	@param:		$name 文件夹名称
	*	@return:	无
	*/
	public static function get_template_folder($name, $type='customize'){
		global $c;
		if(!db::get_row_count('mailchimp_template_folder', "Type='{$type}'")){//如果没有模板文件夹ID，就创建
			self::create_template_folder($name, $type);
		}else{
			$folder_id=db::get_value('mailchimp_template_folder', "Type='{$type}'", 'FolderId');
			$get_info=self::get_info('/template-folders/'.$folder_id);
			if($get_info['error']){
				self::create_template_folder($name, $type, 1);
			}
		}
	}

	/**
	*	创建文件夹
	*	@param:		$name 文件夹名称
	*	@param:		$type 文件夹类型
	*	@param:		$update 修改还是添加，默认添加
	*	@return:	无
	*/
	public static function create_template_folder($name, $type='customize', $update=0){
		global $c;
		$form_data=array('name'=>$name);
		$form_json=str::json_data($form_data);
		$res=self::get_info('/template-folders', 1, $form_json);
		if(!$res['error'] && $res['response']['id']){
			if($update){
				$data=array(
					'FolderId'	=>	$res['response']['id'],
				);
				db::update('mailchimp_template_folder', "Type='{$type}'", $data);
			}else{
				$data=array(
					'FolderId'	=>	$res['response']['id'],
					'Type'		=>	$type,
					'Name'		=>	$name,
					'AccTime'	=>	$c['time']
				);
				db::insert('mailchimp_template_folder', $data);
			}
			$folder_id=$data['FolderId'];
			$LogData=str::json_data($data);
			self::mailchimp_log('创建模板文件夹', $LogData);
		}
	}

		/**
	*	创建模板
	*	@param:		$tpl_name 模板名称
	*	@param:		$content 模板内容
	*	@param:		$folder_id 模板文件夹ID
	*	@param:		$tpl_img 模板图
	*	@return:	$TemplateId 返回模板ID
	*/
	public static function create_template($tpl_name,$content,$folder_id='',$tpl_img=''){
		global $c;
		$html='<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Document</title></head><body>%content%</body></html>';
		$form_data=array(
			'name'		=>	$tpl_name,
			'folder_id'	=>	$folder_id,
			'html'		=>	str_replace('%content%', $content, $html),
		);
		$form_json=str::json_data($form_data);
		$res=self::get_info('/templates', 1, $form_json);
		if(!$res['error']){
			$data=array(
				'TemplateId'	=>	$res['response']['id'],
				'FolderId'		=>	$folder_id,
				'Name'			=>	addslashes($tpl_name),
				'PicPath'		=>	$tpl_img,
				'AccTime'		=>	$c['time'],
			);
			db::insert('mailchimp_template',$data);
			$LogData=str::json_data($data);
			self::mailchimp_log('创建模板', $LogData);
		}
		return $res['response']['id'];
	}

	/**
	*	查询列表ID是否存在，不存在就新建
	*	@return:	无
	*/
	public static function get_user_list(){
		global $c;
		if(!db::get_row_count('mailchimp_lists', 'Type="0" and ListId IS NOT NULL')){
			$res = self::get_info('/lists');
			$ListId = '';
			if (!$res['error']){
				$ListId = $res['response']['lists'][0]['id'];
				$data=array(
					'ListId'	=>	$ListId,
					'Name'		=>	$res['response']['lists'][0]['name'],
					'Type'		=>	0,
					'AccTime'	=>	$c['time'],
				);
				db::insert('mailchimp_lists',$data);
			}
			if (!$ListId) {
				self::create_user_list('All members');
			}
		}else{
			$list_id=db::get_value('mailchimp_lists', 'Type="0"', 'ListId');
			$get_info=self::get_info('/lists/'.$list_id);
			if($get_info['error']){
				self::create_user_list('All members', 1);
			}
		}
	}

	/**
	*	创建联系人列表
	*	@param:		$subject 列表名称
	*	@param:		$update 修改还是添加，默认添加
	*	@return:	无
	*/
	public static function create_user_list($subject='', $update=0){
		global $c;
		//网站基本设置
		$res=array();
		db::delete('mailchimp_lists', 'ListId IS NULL');
		$config_row=db::get_all('config', "GroupId='global' or (GroupId='email' and Variable='notice') or GroupId='print'");
		foreach($config_row as $v){
			if("{$v['GroupId']}|{$v['Variable']}"=='global|Language'){
				$c['config'][$v['GroupId']][$v['Variable']]=explode(',', $v['Value']);
			}else{
				$c['config'][$v['GroupId']][$v['Variable']]=$v['Value'];
			}
		}
		$form_data=array(
			'name'		=>	addslashes($c['config']['global']['SiteName']).'-'.$subject,
			'contact'	=>	array(
								'company'	=>	$c['config']['print']['Compeny'] ? $c['config']['print']['Compeny'] : 'Guangzhou Lianya Network Technology Co., Ltd.',
								'address1'	=>	$c['config']['print']['Address'] ? $c['config']['print']['Address'] : '10th Floor, Chuangju Business Building, No. 185 Yuexiu South Road, Yuexiu District, Guangzhou',
								'city'		=>	$c['config']['print']['City'] ? $c['config']['print']['City'] : 'Guangzhou',
								'state'		=>	$c['config']['print']['State'] ? $c['config']['print']['State'] : 'Guangdong Province',
								'zip'		=>	$c['config']['print']['ZipCode'] ? $c['config']['print']['ZipCode'] : '510000',
								'country'	=>	'CN',
								'phone'		=>	$c['config']['print']['Telephone'] ? $c['config']['print']['Telephone'] : "+86 020-83226791",
							),
			'permission_reminder'=>	'You\'re receiving this email because you signed up for updates about '.$c['config']['global']['SiteName'].' newest hats.',
			'campaign_defaults'	=>	array(
										'from_name'	=>	$c['config']['global']['SiteName'],
										'from_email'=>	$c['config']['global']['AdminEmail'],
										'subject'	=>	$subject,
										'language'	=>	'en',
									),
			'email_type_option'	=>	TRUE,
		);
		$form_json=str::json_data($form_data);
		$res=self::get_info('/lists', 1, $form_json);
		if(!$res['error']){
			if($update){
				$data=array(
					'ListId'	=>	$res['response']['id'],
				);
				db::update('mailchimp_lists', 'Type="0"', $data);
			}else{
				$data=array(
					'ListId'	=>	$res['response']['id'],
					'Name'		=>	$res['response']['name'],
					'Type'		=>	0,
					'AccTime'	=>	$c['time'],
				);
				db::insert('mailchimp_lists',$data);
			}
			$LogData=str::json_data($data);
			self::mailchimp_log('创建联系人列表', $LogData);
		}else if($update){
			// 有列表存在，且不是现在后台的列表
			$res=self::get_info('/lists');
			if($res['response']['lists'][0]['id']){
				$data=array(
					'ListId'	=>	$res['response']['lists'][0]['id'],
				);
				db::update('mailchimp_lists', 'Type="0"', $data);
				self::update_user_list();
			}
		}
		return $res;
	}

	/**
	 * 修改联系人列表的信息
	 * @return 无
	 */
	public static function update_user_list()
	{
		$list_id=db::get_value('mailchimp_lists', 'Type="0"', 'ListId');
		if ($list_id) {
			$config_row=db::get_all('config', "GroupId='global' or (GroupId='email' and Variable='notice') or GroupId='print'");
			foreach($config_row as $v){
				if("{$v['GroupId']}|{$v['Variable']}"=='global|Language'){
					$c['config'][$v['GroupId']][$v['Variable']]=explode(',', $v['Value']);
				}else{
					$c['config'][$v['GroupId']][$v['Variable']]=$v['Value'];
				}
			}
			$form_data=array(
				'name'		=>	addslashes($c['config']['global']['SiteName']).'-'.$subject,
				'contact'	=>	array(
									'company'	=>	$c['config']['print']['Compeny'] ? $c['config']['print']['Compeny'] : 'Guangzhou Lianya Network Technology Co., Ltd.',
									'address1'	=>	$c['config']['print']['Address'] ? $c['config']['print']['Address'] : '10th Floor, Chuangju Business Building, No. 185 Yuexiu South Road, Yuexiu District, Guangzhou',
									'city'		=>	$c['config']['print']['City'] ? $c['config']['print']['City'] : 'Guangzhou',
									'state'		=>	$c['config']['print']['State'] ? $c['config']['print']['State'] : 'Guangdong Province',
									'zip'		=>	$c['config']['print']['ZipCode'] ? $c['config']['print']['ZipCode'] : '510000',
									'country'	=>	'CN',
									'phone'		=>	$c['config']['print']['Telephone'] ? $c['config']['print']['Telephone'] : "+86 020-83226791",
								),
				'permission_reminder'=>	'You\'re receiving this email because you signed up for updates about '.$c['config']['global']['SiteName'].' newest hats.',
				'campaign_defaults'	=>	array(
											'from_name'	=>	$c['config']['global']['SiteName'],
											'from_email'=>	$c['config']['global']['AdminEmail'],
											'subject'	=>	'All members',
											'language'	=>	'en',
										),
				'email_type_option'	=>	TRUE,
			);
			$form_json=str::json_data($form_data);
			$res=self::get_info('/lists/' . $list_id, 2, $form_json);
			self::mailchimp_log('修改联系人列表的信息', $form_json);
		}
	}

	/**
	*	查询联系人ID是否存在，不存在就新建
	*	@param:		$Email 联系人邮箱
	*	@return:	无
	*/
	public static function get_members($Email){
		global $c;
		if(!db::get_row_count('mailchimp_members', "Email='{$Email}'")){
			self::create_members($Email);
		}else{
			$list_id=db::get_value('mailchimp_lists', 'Type="0"', 'ListId');
			$members_id=db::get_value('mailchimp_members', "Email='{$Email}'", 'MembersId');
			$get_info=self::get_info('/lists/'.$list_id.'/members/'.$members_id);
			if($get_info['error']){
				self::create_members($Email);
			}
		}
	}

	/**
	*	创建联系人
	*	@param:		$Email 联系人邮箱
	*	@return:	无
	*/
	public static function create_members($Email){
		global $c;
		$lists_row=db::get_one('mailchimp_lists', '1');
		$list_id=$lists_row['ListId'];
		$form_data=array(
			'email_address'	=>	$Email,
			'status'		=>	'subscribed',
		);
		$form_json=str::json_data($form_data);
		$res=self::get_info('/lists/'.$list_id.'/members', 1, $form_json);
		if(!$res['error'] && $res['response']['id']){
			$data=array(
				'ListId'	=>	$list_id, 
				'MembersId'	=>	$res['response']['id'],
				'Email'		=>	$Email,
				'Status'	=>	'subscribed',
				'AccTime'	=>	$c['time']
			);
			db::insert('mailchimp_members' ,$data);
			$LogData=str::json_data($data);
			self::mailchimp_log('创建联系人', $LogData);
		}
	}

	/**
	*	发送邮件
	*	@param:		$list_id 联系人列表ID
	*	@param:		$template_id 模板ID
	*	@param:		$email_ary 联系人邮箱列表
	*	@return:	$res 
	*/
	public static function send_mail($list_id, $template_id, $email_ary){
		global $c;
		$res['error']=1;
		$campaigns_id=self::get_campaign_id($list_id,$template_id,$email_ary);
		if($campaigns_id){
			$res=self::get_info('/campaigns/'.$campaigns_id.'/actions/send',1);
		}
		if(in_array('HTTP/1.1 204 No Content',$res['header']) || !$res['error']){
			$tips='Send mail success';
		}else if(!$list_id){
			$tips='List Id is empty';
		}else if(!$template_id){
			$tips='Template Id is empty';
		}else if(!$campaigns_id){
			$tips='Create ad campaign failed';
		}else{
			$tips='Send mail fail';
		}
		if(!$campaigns_id){
			$res['detail']='请填写正确的Account和ApiKey。';
		}

		$log_data=array(
			'Email'			=>	$email_ary,
			'result'		=>	$res,
			'list_id'		=>	$list_id,
			'template_id'	=>	$template_id,
			'campaigns_id'	=>	$campaigns_id,
			'tips'			=>	$tips,
		);
		$LogData=str::json_data($log_data);
		self::mailchimp_log('发送邮件', $LogData);

		if(in_array('HTTP/1.1 204 No Content',$res['header']) || !$res['error']){
			return true;
		}else{
			return $res;
		}
	}

	/**
	*	创建广告系列
	*	@param:		$list_id 联系人列表ID
	*	@param:		$template_id 模板ID
	*	@param:		$email_ary 联系人邮箱列表
	*	@return:	$campaigns_id 广告系列ID
	*/
	public static function get_campaign_id($list_id,$template_id,$email_ary=array()){
		global $c;
		$config_row=str::json_data(db::get_value('config', 'GroupId="email" and Variable="config"', 'Value'), 'decode');
		$template_row=db::get_one('mailchimp_template',"TemplateId='{$template_id}'");
		$Subject=$template_row['Name'];
		if(!$Subject){
			$fileType=mb_detect_encoding($Subject,array('UTF-8', 'GBK', 'LATIN1', 'BIG5')) ;
			if($fileType!='UTF-8'){
				$Subject = mb_convert_encoding($Subject ,'utf-8' , $fileType);
			}
		}
		$MailChimpOauthData=db::get_value('config', 'GroupId="MailChimp" and Variable="MailChimpOauth"', 'Value');
		$MailChimpOauthAry=str::json_data($MailChimpOauthData, 'decode');
		$form_data=array(
			'recipients'	=>	array(
									'list_id'	=>	$list_id,
								),
			'type'			=>	'regular',
			'settings'		=>	array(
									'subject_line'	=>	$Subject,
									'reply_to'		=>	$MailChimpOauthAry['email'], //临时jhsmktg@public.guangzhou.gd.cn
									'from_name'		=>	$config_row['FromName']?$config_row['FromName']:'noreply',
									'template_id'	=>	(int)$template_id,
									'auto_footer'	=>	false
								),
			'tracking'		=>	array(
									'opens'			=>	false,
								)
		);

		if(!empty($email_ary)){
			$form_data['recipients']['segment_opts']['match']='any';
			foreach((array)$email_ary as $k=>$v){
				$form_data['recipients']['segment_opts']['conditions'][$k]['condition_type']='EmailAddress';
				$form_data['recipients']['segment_opts']['conditions'][$k]['op']='is';
				$form_data['recipients']['segment_opts']['conditions'][$k]['field']='EMAIL';
				$form_data['recipients']['segment_opts']['conditions'][$k]['value']=$v;
			}
		}
		
		$form_json=str::json_data($form_data);
		$res=self::get_info('/campaigns', 1, $form_json);
		$campaigns_id=0;
		if(!$res['error']){
			$campaigns_id=$res['response']['id'];
			self::mailchimp_log('创建新的广告系列', $form_json);
		}
		return $campaigns_id;
	}


	/**
	*	保存mailchimp日志
	*	@param:		$Log 操作的描述
	*	@param:		$Data 操作的详细
	*	@param:		$UserId 操作管理员ID
	*	@param:		$UserName 操作管理员名称
	*	@return:	无
	*/
	public static function mailchimp_log($Log, $Data, $UserId=0, $UserName=''){//订单日志
		global $c;
		if($_SESSION['Manage']['UserId']==-1){return;}
		$UserId=$UserId?$UserId:(int)$_SESSION['Manage']['UserId'];
		$UserName=$UserName?$UserName:$_SESSION['Manage']['UserName'];
		is_array($Data) && $Data=str::json_data($Data);
		db::insert('mailchimp_log', array(
				'UserId'		=>	$UserId,
				'UserName'		=>	addslashes($UserName),
				'Ip'			=>	ly200::get_ip(),
				'Log'			=>	addslashes($Log),
				'Data'			=>	addslashes($Data),
				'AccTime'		=>	$c['time']
			)
		);
	}
	
	/**
	*	oauth授权
	*	@param:		$client_id
	*	@return：	获得授权链接地址
	*/
	public static function mailchimp_get_oauth_url(){
		global $c;
		$MailChimpClient=db::get_value('config', 'GroupId="MailChimp" and Variable="MailChimpClient"', 'Value');
		$MailChimpClientAry=str::json_data($MailChimpClient, 'decode');
		$client_id=$MailChimpClientAry['MailChimpClientId'];
		$redirect_uri=urlencode('https://sync.ly200.com/plugin/mailchimp/oauth.php?Number='.$c['Number']);
		$url="https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id={$client_id}&redirect_uri={$redirect_uri}";
		self::mailchimp_log('请求授权', $url);
		return $url;
	}

	/**
	*	oauth授权请求同步网站
	*	@param:		$Email 用于判断账号是属于谁请求的
	*	@return：	获得$metadata
	*/
	public static function request_authorization_data(){
		global $c;
		$url = mailchimp::mailchimp_get_oauth_url();
		$data=array(
			'ApiKey'		=>	'ueeshop_sync',
			'Action'		=>	'authorization',
			'ApiName'		=>	'mailchimp',
			'Number'		=>	$c['Number'],
			'notify_url'	=>	ly200::get_domain().'/gateway/',
			'timestamp'		=>	$c['time'],
			'OauthUrl'		=>	$url,
		);
		$data['sign']=ly200::sign($data, $c['ApiKey']);
		$result=str::json_data(ly200::curl($c['sync_url'], $data), 'decode');
	}
}
?>