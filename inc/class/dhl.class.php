<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

class dhl{

	/**
	 * [DHL公用CURL方法]
	 * @param  string  $url    请求地址
	 * @param  integer $method 请求方式，[0：get, 1: post]
	 * @param  string|array   $form_data   请求体(参数)，仅post用到
	 * @return array   $res   响应数组
	 */
	public static function curl($url, $method=0, $form_data)
	{
		$curl=@curl_init(); 
		@curl_setopt($curl, CURLOPT_URL, $url);
		@curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		@curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		@curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		@curl_setopt($curl, CURLOPT_HEADER, FALSE);
		@curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		@curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/json"));
		@curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		@curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		if ($method && $form_data) {
			@curl_setopt($curl, CURLOPT_POST, true);
			@curl_setopt($curl, CURLOPT_POSTFIELDS, $form_data);
		}
		$result=@curl_exec($curl);
		$info=@curl_getinfo($curl);
		@curl_close($curl);
		$res=str::json_data($result,'decode');

		return $res;
	}
	
	/**
	 * 获取Token
	 * @return Token
	 */
	public static function getToken()
	{
		global $c;

		// 获取账号信息
		$DHLAccountInfo = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountInfo"', 'Value'), 'htmlspecialchars_decode'), 'decode');

		// 获取旧Token信息，判断是否已过期
		$DHLAccountToken = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountToken"', 'Value'), 'htmlspecialchars_decode'), 'decode');

		// 初始化
		$token = $DHLAccountToken['token'];

		// 存在账号信息才能请求，且Token并不存在或者已过期，重新请求
		if ($DHLAccountInfo['ClientId'] && $DHLAccountInfo['Password'] && (!$token || $DHLAccountToken['expires'] < $c['time'])) {
			$url = 'https://api.dhlecommerce.dhl.com/rest/v1/OAuth/AccessToken?clientId=' . $DHLAccountInfo['ClientId'] . '&password=' . $DHLAccountInfo['Password'] . '&returnFormat=json';
			$res = self::curl($url);
			$data = array(
				'response'	=>	$res,
				'error'		=>	$res['accessTokenResponse']['responseStatus']['code'] == 100000 ? 0 : 1,
				'detail'	=>	$res['accessTokenResponse']['responseStatus']['messageDetails'],
			);

			if (!$data['error']) {
				$token_ary = array(
					'token'		=>	$res['accessTokenResponse']['token'],
					'expires'	=>	$res['accessTokenResponse']['expires_in_seconds'] + $c['time'],
				);
				$tokenData=addslashes(str::json_data($token_ary));
				manage::config_operaction(array('DHLAccountToken' => $tokenData), 'plugins');
				$token = $res['accessTokenResponse']['token'];
			}
		}
		return $token;
	}

	/**
	 * 获取标签的PNG图路径
	 * @param  $info = array(
	 *                 		
	 *                 )
	 */
	public static function getLabel($orders_row, $info=array())
	{
		global $c;

		$result = array();
		// 支付成功才开始执行获取标签的动作
		if($orders_row['OrderStatus'] >= 4){
			// 获取Token
			$token = self::getToken();

			// 获取账号信息
			$DHLAccountInfo = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountInfo"', 'Value'), 'htmlspecialchars_decode'), 'decode');
			
			// 获取国家编码
			$Acronym = db::get_value('country', "CId='{$orders_row['ShippingCId']}'", 'Acronym');

			// 获取省份/州/地区信息
			$StateCode = '';
			$orders_row['ShippingSId'] && $StateCode = db::get_value('country_states', "CId='{$orders_row['ShippingSId']}'", 'States');

			// 包裹总价值
			$productsPrice = orders::orders_product_price($orders_row, 1);
			$data = array(
				'labelRequest'	=>	array(
					'hdr'	=>	array(
						'accessToken'		=>	$token, // token
						'messageDateTime'	=>	date('c', $c['time']), // 请求日期和时间
						"messageLanguage"	=>	'en', // 语言：en/zh_CH(英文、中文)
						"messageType"		=>	'LABEL', // 必须为“LABEL”
						"messageVersion"	=>	'1.4' // 默认为"1.4"
					),
					'bd'	=>	array(
						'pickupAccountId'	=>	$DHLAccountInfo['pickupAccountId'], // DHL Pickup Account
						'soldToAccountId'	=>	$DHLAccountInfo['soldToAccountId'], // DHL Soldto Account
						'pickupDateTime'	=>	date('c', $c['time']), // 提货日期和时间
						'shipperAddress'	=>	array(
							// 发件人地址详情
							'address1'			=>	$info['shipperAddress']['address1'], // 发货地址栏位
							'city'				=>	$info['shipperAddress']['city'], // 发货地址城市
							'country'			=>	$info['shipperAddress']['country'], // 发货地址国家
							'name'				=>	$info['shipperAddress']['name'], // 发货联系人姓名
						),
						'shipmentItems'		=>	array(
							array(	
								// 发货包裹信息详情
								'consigneeAddress'	=>	array(
									// 收件人地址详情
									'address1'			=>	trim($orders_row['ShippingAddressLine1'].$orders_row['ShippingAddressLine2']), // 收件人地址栏位 1
									'city'				=>	trim($orders_row['ShippingCity']), // 收件人地址城市
									'country'			=>	trim($Acronym), // 收件人地址国家
									'name'				=>	trim($orders_row['ShippingFirstName'].' '.$orders_row['ShippingLastName']), // 收件联系人姓名
									'postCode'			=>	trim($orders_row['ShippingZipCode']), // 收件人地址邮编
									'state'				=>	trim($orders_row['ShippingSId'] == 226 ? $StateCode : $orders_row['ShippingState']), // 收件人地址州 中美专线为必填项，且只接受 State 2 字代码
								),
								'shipmentID'		=>trim($c['Number'].$orders_row['OId']), // 发货号码（物流商流水号）由客户自定义，必须以客户唯一前缀为首。
								'packageDesc'		=>	'', // 包裹品类概括描述：
								'totalWeight'		=>	$orders_row['TotalWeight'] * 1000, // 包裹总重量
								'totalWeightUOM'	=>	"G", // 包裹总重量单位：默认为“G”
								'contentIndicator'	=>	$info['shipmentItems']['contentIndicator'], // 包裹属性标识。如客户未开通带电，则默认 null。如客户开通带电，则 00 -普货，04 - 带电。
								'totalValue'		=>	(float)$productsPrice, // 包裹总价值
								'currency'			=>	$orders_row['Currency'], // 包裹总价值币种
								'dimensionUOM'		=>	'CM', // 包裹长度单位
								'incoterm'			=>	'DDU', // 国际贸易简制（DDU - 平邮、挂号、中英、中澳）（DDP - 中美、中以）
								'insuranceValue'	=>	(float)cart::iconv_price($orders_row['ShippingInsurancePrice'], 2, $orders_row['Currency'], 0), // 包裹保险金额
								'productCode'		=>	$info['shipmentItems']['productCode'], // 包裹产品编码
								'remarks'			=>	$orders_row['Remarks'] ? $orders_row['Remarks'] : 'remarks', // 包裹备注（会出现在报关标签中的 Remarks 中）
								'shipmentContents'	=>	array(),
							)
						),
						'label'	=>	array(
							'format'	=>	'PDF', // 包裹标签输出格式（PNG、PDF）	
							'layout'	=>	'1x1', // 默认为"1x1"
							'pageSize'	=>	'400x400' // 包裹标签大小。（400x400 或 400x600）
						)
					)
				),
			);

			// 查询订单产品信息
			$products_list_row = db::get_all('orders_products_list', "OrderId='{$orders_row['OrderId']}'", 'ProId, Price, PropertyPrice, Discount, Qty, SKU', 'LId asc');
			// 包裹品类概括描述：
			$packageDescAry = array();
			$language = db::get_value('config', "GroupId='global' and Variable='LanguageDefault'", 'Value');
			foreach((array)$products_list_row as $k => $v){
				$row = db::get_one('products', "ProId='{$v['ProId']}'", 'Prefix, Number, CateId, Weight, Name_'.$language);
				$column_products_ary = db::get_table_fields('products_category', 1);
				
				$Name = 'Category_'.$language;
				
				$Category = db::get_value('products_category', "CateId='{$row['CateId']}'", $Name);
				if ($Category && !in_array($Category, $packageDescAry)) {
					$packageDescAry[] = $Category;
				}

				// shipmentContents
				$Price = ($v['Price'] + $v['PropertyPrice']) * ($v['Discount'] < 100 ? $v['Discount'] / 100 : 1);
				$data['labelRequest']['bd']['shipmentItems'][0]['shipmentContents'][$k] = array(
					// 包裹物品详细描述
					'contentIndicator'	=>	$info['shipmentContents']['contentIndicator'][$v['ProId']], // 产品属性标识。如客户未开通带电，则默认 null。如客户开通带电，则 00 -普货，04 - 带电。
					'countryOfOrigin'	=>	'CN', // 产品原产国，默认中国。
					'description'		=>	$info['shipmentContents']['description'][$v['ProId']], // 产品描述。
					'descriptionExport'	=>	$info['shipmentContents']['descriptionExport'][$v['ProId']], // 产品中文出口描述。
					'weightUOM'			=>	'G', // 产品净重单位。
					'itemQuantity'		=>	(int)$v['Qty'], // 产品数量。
					'itemValue'			=>	(float)cart::iconv_price($Price, 2, $orders_row['Currency'], 0), // 产品单价。
					'skuNumber'			=>	$v['SKU'] ? $v['SKU'] : trim($row['Prefix'].$row['Number']), // 产品 SKU 编码。
				);
			}
			$packageDesc = implode('/', $packageDescAry);
			$data['labelRequest']['bd']['shipmentItems'][0]['packageDesc'] = $packageDesc;
			// 包裹产品内容
			$data = self::json_data($data);
			$res = self::curl('https://api.dhlecommerce.dhl.com/rest/v2/Label', 1, $data);

			$result=array(
				'response'	=>	$res,
				'error'		=>	$res['labelResponse']['bd']['responseStatus']['code'] == 200 ? 0 : 1,
				'detail'	=>	$res['labelResponse']['bd']['responseStatus']['message'],
			);
		}
		
		return $result;
	}

	/**
	 * /**
     * [将Base64PDF转换为本地PDF并保存]
     * @param $base64File [要保存的Base64]
     * @return string
     */
	public static function base64FileResolve($base64File, $base_name)
	{
		global $c;

		$file = base64_decode($base64File);
		
		$save_name = $base_name. str::rand_code() . '.pdf';
		file::mk_dir($base_name);
		$file_length = file_put_contents($c['root_path'].$save_name, $file);//返回的是字节数
		// 输出文件
		return $file_length ? $save_name : '';
	}


	/**
	 * 重新获取标签PDF图
	 * @param  $info array(
	 *         		
	 * 			)
	 */
	public static function reacquireLabel($info=array())
	{
		global $c;
		$token = self::getToken();

		$data = array(
			'labelReprintRequest'	=>	array(
				'hdr'	=>	array(
					'messageType'		=>	'LABELREPRINT',
					'messageDateTime'	=>	date('c', $info['messageDateTime']),
					'accessToken'		=>	$token,
					'messageVersion'	=>	'1.0',
					'messageLanguage'	=>	$info['language']
				),
				'bd'	=>	array(
					'pickupAccountId'	=>	$info['pickupAccountId'],
					'soldToAccountId'	=>	$info['soldToAccountId'],
					'shipmentItems'		=>	array(
						array(
							'shipmentID'	=>	$info['shipmentItems']['shipmentID'],
						),
					)
				)
			)
		);

		$data = str::json_data($data);

		$res = self::curl('https://api.dhlecommerce.dhl.com/rest/v2.Label.Reprint/', 1, $data);
		$data=array(
			'response'	=>	$res,
			'error'		=>	$res['labelReprintResponse']['bd']['responseStatus']['code'] == 200 ? 0 : 1,
			'detail'	=>	$res['labelReprintResponse']['bd']['responseStatus']['message'],
		);
		$ImgPath = array();
		if (!$data['error']) {
			// 重新获取标签成功
			$labels_ary = $res['labelReprintResponse']['bd']['shipmentItems'];
			foreach ((array)$labels_ary as $k => $v) {
				$ImgPath[] = dhl::base64FileResolve($v['content']);
			}
		}
		return $ImgPath;
	}

	/**
	 * 获取追踪信息查询
	 * @param $OrderId 订单号
	 */
	public static function trackingInformationQuery($OrderId)
	{
		$token = self::getToken();

		$trackingReferenceNumber = array();
		$label_row = db::get_all('orders_label_list', "OrderId='{$OrderId}' and ProductName='DHL'", 'BusinessNo');
		foreach((array)$label_row as $k=>$v){
			if($v['BusinessNo'] && !in_array($v['BusinessNo'], $trackingReferenceNumber)){
				$trackingReferenceNumber[] = $v['BusinessNo'];
			}
		}
		$data = array(
			'trackItemRequest'	=>	array(
				'trackingReferenceNumber'	=>	$trackingReferenceNumber,
				'messageLanguage'	=>	'en',
				'messageVersion'	=>	'1.1',
				'token'				=>	$token
			)
		);

		$data = str::json_data($data);
		
		$res = self::curl('https://api.dhlecommerce.dhl.com/rest/v2/Tracking', 1, $data);
		$data=array(
			'response'	=>	$res,
			'error'		=>	$res['trackItemResponse']['responseCode'] == 0 ? 0 : 1,
			'detail'	=>	$res['trackItemResponse']['responseText'],
		);

		return $data;
	}

	/**
	 * [提交订单数据]
	 * @param [int] $OrderId [订单ID]
	 * @param [array] $shipmentID [提交运单号]
	 */
	public static function Closeout($OrderId, $shipmentID)
	{
		global $c;
		$token = self::getToken();
		// 获取账号信息
		$DHLAccountInfo = str::json_data(str::str_code(db::get_value('config', 'GroupId="plugins" and Variable="DHLAccountInfo"', 'Value'), 'htmlspecialchars_decode'), 'decode');
			

		$data = array(
			'closeOutRequest'	=>	array(
				'hdr'	=>	array(
					'messageType'		=>	'CLOSEOUT',
					'accessToken'		=>	$token, // $token
					'messageDateTime'	=>	date('c', $c['time']), // date('c', $c['time'])
					'messageVersion'	=>	'1.2',
					'messageLanguage'	=>	'en'
				),
				'bd'	=>	array(
					'customerAccountId'	=>	null,
					'soldToAccountId'	=>	$DHLAccountInfo['soldToAccountId'], // $DHLAccountInfo['soldToAccountId']
					'pickupAccountId'	=>	$DHLAccountInfo['soldToAccountId'], // $DHLAccountInfo['pickupAccountId']
					'handoverMethod'	=>	1,
					'shipmentItems'		=>	array(),
				),
			)
		);

		foreach((array)$shipmentID as $k=>$v){
			$data['closeOutRequest']['bd']['shipmentItems'][$k]['shipmentID'] = $v;
		}

		$data = str::json_data($data);
		$res = self::curl('https://api.dhlecommerce.dhl.com/rest/v2/Order/Shipment/CloseOut', 1, $data);
		$LogData = self::json_data($res);
		self::label_log((int)$_SESSION['Manage']['UserId'], $_SESSION['Manage']['UserName'], $OrderId, 'DHL提交订单数据', $data.$LogData);
	}

	/**
	 * [解决低版本PHP环境使用str::json_data转json字符串的时候，中文变成unicode编码的问题]
	 * @param  [array/String] $data   [需要编码的字符串]
	 * @param  string $action [需要执行的动作，默认数组转json]
	 * @return [String/array]         [返回转换的结果]
	 */
	public static function json_data($data, $action='encode'){	//json数据编码
		if($action=='encode'){
			if(!function_exists('unidecode')){
				function unidecode($match){
					return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
				}
			}
			return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'unidecode', json_encode($data));
		}else{
			return (array)json_decode($data, true);
		}
	}

	/**
	 * [记录日志]
	 * @param  [type] $UserId   [后台管理员ID]
	 * @param  [type] $UserName [后台管理员用户名]
	 * @param  [type] $OrderId  [订单ID]
	 * @param  [type] $Log      [日志名称]
	 * @param  [type] $Data     [日志数据]
	 * @return void 无         
	 */
	public static function label_log($UserId, $UserName, $OrderId, $Log, $Data){//订单日志
		global $c;
		db::insert('orders_label_log', array(
				'UserId'		=>	$UserId,
				'UserName'		=>	addslashes($UserName),
				'OrderId'		=>	$OrderId,
				'Ip'			=>	ly200::get_ip(),
				'Log'			=>	addslashes(stripslashes($Log)),
				'Data'			=>	addslashes($Data),
				'AccTime'		=>	$c['time']
			)
		);
	}
	
	
}
?>