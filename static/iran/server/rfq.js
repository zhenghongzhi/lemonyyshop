$(document).ready(function () {
  $('.flip').click(function () {
    $('.cont-flip').toggleClass('flipped');
    return false;
  });

  $("#send_button").click(function () {
    ValidateForm();

  });
});



function ValidateForm() {
  var inputErrors = []
  var formInvalid = false;
  var name = $('#1').val()
  var product = $('#3').val()
  var phone = $('#2').val()

  if (name === '') {
    formInvalid = true;
    inputErrors.push('Name');
  }
  if (product === '') {
    formInvalid = true;
    inputErrors.push('Product Name');
  }
  if (phone === '') {
    formInvalid = true;
    inputErrors.push('phone Number');
  }
  if (formInvalid) {

    invalidData();

  } else {
      $.ajax({
          url: '/?do_action=action.usermessage',
          type: 'POST',
          dataType: 'json',
          data: {name:name,product:product,phone:phone,source:'index'},
          success: function(data){
              if (data.ret == 1) {

                  validData();
              }
          },
          error: function(){
              invalidData();
          }
      })


  }

  function invalidData() {
    Swal.fire({
      position: 'center',
      icon: 'error',
      title: 'Oops...',
      text: 'maybe your ' + inputErrors + ' is wrong!   ',
      showConfirmButton: false,
      timer:'10000' /* mSec*/ 
    })
  }

  function validData() {
    Swal.fire({
      
      position: 'center',
      icon: 'success',
      title: 'submitted successful! ',
      text: 'we will contact you soon!!',
      showConfirmButton: false,
      timer:'10000'
    })
  }



}







