<?php !isset($c) && exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
echo ly200::seo_meta();
include("{$c['static_path']}inc/static.php");
echo ly200::load_static("/static/themes/{$c['theme']}/css/index.css");
?>
</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['default_path']}/inc/header.php");?>
<?php
if(!file::check_cache('index.html')){
	ob_start();
?>
<div id="banner">
	<div class="wide">
		<dl class="banner">
			<dt>
            	<?php /*?><?=ly200::ad(1);?><?php */?>
                <?=ly200::visual_ad();?>
            </dt>
			<dd plugins="index-0" effect="0-1">
				<ul>
					<?php
					for($i=0; $i<3; ++$i){
						$url=$c['web_pack']['index'][0][$i]['Link'];
					?>
					<li<?=$i==1?' class="middle"':'';?> plugins_pos="<?=$i?>">
						<?php if($url){?><a plugins_mod="Link" href="<?=$url;?>" target="_blank"><?php }?>
							<div class="img"><img plugins_mod="Pic" src="<?=$c['web_pack']['index'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['index'][0][$i]['Name'];?>" /></div>
							<h2 class="FontColor"  plugins_mod="Title"><?=$c['web_pack']['index'][0][$i]['Title'];?></h2>
							<span  plugins_mod="SubTitle"><?=$c['web_pack']['index'][0][$i]['SubTitle'];?></span>
						<?php if($url){?></a><?php }?>
					</li>
					<?php }?>
				</ul>
			</dd>
		</dl>
	</div>
</div>
<div id="main" class="wide">
    <div class="pro_left fl">
		<?php if((int)$c['config']['global']['RecentOrders']) include("{$c['static_path']}/inc/order_live.php");?>
        <?php include("{$c['default_path']}/inc/what_hot.php");?>
		<?php include("{$c['default_path']}/inc/special_offer.php");?>
        <?php include("{$c['default_path']}/inc/popular_search.php");?>
		<div class="blank20"></div>
		<div class="ad" plugins="index-6" effect="0-1" plugins_pos="0"><?=ly200::visual_ad($c['web_pack']['index'][6][0]);?></div>
		<div class="blank20"></div>
		<div class="ad" plugins="index-7" effect="0-1" plugins_pos="0"><?=ly200::visual_ad($c['web_pack']['index'][7][0]);?></div>
    </div>
    <div class="pro_right fr">
        <div class="prod_list" plugins="index-1" effect="0-1">
			<div class="title">
				<h3 class="fl" plugins_pos="0" plugins_mod="Title"><?=$c['web_pack']['index'][1][0]['Title'];?></h3>
				<a href="/Best-Deals/" class="fr"><?=$c['lang_pack']['more'];?> >></a>
			</div>
			<div class="blank15"></div>
			<?php
			$products_list_row=str::str_code(db::get_limit('products', '1'.where::equal(array('IsBestDeals'=>'1', 'IsIndex'=>'1')).$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 8));
			foreach((array)$products_list_row as $k=>$v){
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$url=ly200::get_url($v, 'products');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$price_0=$price_ary[1];
				$pro_info=ly200::get_pro_info($v);
				$RatingCount=$pro_info[3];
				$Rating=$pro_info[2];
				$content='
					<div class="pro_name"><a href="'.$url.'" title="'.$name.'">'.str::str_echo($name, 45, 0, '..').'</a></div>
					<div class="pro_price">
						<em class="currency_data PriceColor">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data PriceColor" data="'.$price_ary[0].'" keyid="'.$v['ProId'].'">'.cart::iconv_price($price_ary[0], 2).'</span>'.($price_ary[2]?'<del><em class="currency_data">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data" data="'.$price_0.'">'.cart::iconv_price($price_0, 2).'</span></del>':'').'
					</div>
					<div class="free_shipping">'.($v['IsFreeShipping']?$c['lang_pack']['free_shipping']:'').'</div>
					<div class="pro_view">'.html::review_star($Rating).'<a class="review_count" href="'.$url.'#review_box">('.$RatingCount.')</a><span class="favorite add_favorite" data="'.$v['ProId'].'"><i class="icon_heart"></i>('.$pro_info[1].')</span>
					</div>
				';
				echo ly200::product_effects($c['config']['global']['Effects'], $k, $v, $content);
			}
			?>
			<div class="clear"></div>
		</div>
		<div class="ad" plugins="index-2" effect="0-1" plugins_pos="0"><?=ly200::visual_ad($c['web_pack']['index'][2][0]);?></div>
		<div class="prod_list" plugins="index-3" effect="0-1">
			<div class="title">
				<h3 class="fl" plugins_pos="0" plugins_mod="Title"><?=$c['web_pack']['index'][3][0]['Title'];?></h3>
				<a href="/Hot-Sales/" class="fr"><?=$c['lang_pack']['more'];?> >></a>
			</div>
			<div class="blank15"></div>
			<?php
			$products_list_row=str::str_code(db::get_limit('products', '1'.where::equal(array('IsHot'=>'1', 'IsIndex'=>'1')).$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 8));
			foreach((array)$products_list_row as $k=>$v){
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$url=ly200::get_url($v, 'products');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$price_0=$price_ary[1];
				$pro_info=ly200::get_pro_info($v);
				$RatingCount=$pro_info[3];
				$Rating=$pro_info[2];
				$content='
					<div class="pro_name"><a href="'.$url.'" title="'.$name.'">'.str::str_echo($name, 45, 0, '..').'</a></div>
					<div class="pro_price">
						<em class="currency_data PriceColor">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data PriceColor" data="'.$price_ary[0].'" keyid="'.$v['ProId'].'">'.cart::iconv_price($price_ary[0], 2).'</span>'.($price_ary[2]?'<del><em class="currency_data">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data" data="'.$price_0.'">'.cart::iconv_price($price_0, 2).'</span></del>':'').'
					</div>
					<div class="free_shipping">'.($v['IsFreeShipping']?$c['lang_pack']['free_shipping']:'').'</div>
					<div class="pro_view">'.
						html::review_star($Rating).'<a class="review_count" href="'.$url.'#review_box">('.$RatingCount.')</a>'.
						'<span class="favorite add_favorite" data="'.$v['ProId'].'"><i class="icon_heart"></i>('.$pro_info[1].')</span>
					</div>
				';
				echo ly200::product_effects($c['config']['global']['Effects'], $k, $v, $content);
			}
			?>
			<div class="clear"></div>
		</div>
        <div class="ad" plugins="index-4" effect="0-1" plugins_pos="0"><?=ly200::visual_ad($c['web_pack']['index'][4][0]);?></div>
		<div class="prod_list" plugins="index-5" effect="0-1" plugins_pos="0">
			<div class="title">
				<h3 class="fl" plugins_mod="Title"><?=$c['web_pack']['index'][5][0]['Title'];?></h3>
				<a href="/New-Arrivals/" class="fr"><?=$c['lang_pack']['more'];?> >></a>
			</div>
			<div class="blank15"></div>
			<?php
			$products_list_row=str::str_code(db::get_limit('products', '1'.where::equal(array('IsNew'=>'1', 'IsIndex'=>'1')).$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 8));
			foreach((array)$products_list_row as $k=>$v){
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$url=ly200::get_url($v, 'products');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$price_0=$price_ary[1];
				$pro_info=ly200::get_pro_info($v);
				$RatingCount=$pro_info[3];
				$Rating=$pro_info[2];
				$content='
					<div class="pro_name"><a href="'.$url.'" title="'.$name.'">'.str::str_echo($name, 45, 0, '..').'</a></div>
					<div class="pro_price">
						<em class="currency_data PriceColor">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data PriceColor" data="'.$price_ary[0].'" keyid="'.$v['ProId'].'">'.cart::iconv_price($price_ary[0], 2).'</span>'.($price_ary[2]?'<del><em class="currency_data">'.$_SESSION['Currency']['Symbol'].'</em><span class="price_data" data="'.$price_0.'">'.cart::iconv_price($price_0, 2).'</span></del>':'').'
					</div>
					<div class="free_shipping">'.($v['IsFreeShipping']?$c['lang_pack']['free_shipping']:'').'</div>
					<div class="pro_view">'.
						html::review_star($Rating).'<a class="review_count" href="'.$url.'#review_box">('.$RatingCount.')</a>'.
						'<span class="favorite add_favorite" data="'.$v['ProId'].'"><i class="icon_heart"></i>('.$pro_info[1].')</span>
					</div>
				';
				echo ly200::product_effects($c['config']['global']['Effects'], $k, $v, $content);
			}
			?>
			<div class="clear"></div>
		</div>
    </div>
	<div class="clear"></div>
</div>
<?php
	if($c['plugin_app']->trigger('blog', '__config', 'get_blog')=='enable'){//博客
		$c['plugin_app']->trigger('blog', 'get_blog');
	}
	if($c['plugin_app']->trigger('gallery', '__config', 'gallery_index')=='enable'){//app插件是否存在
		$c['plugin_app']->trigger('gallery', 'gallery_index','');
	}
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'index.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'index.html');
?>
<?php include("{$c['default_path']}inc/footer.php");?>
<?php
//首页特效跟随主色调
$cfg_module_style=db::get_value('config_module', 'IsDefault=1', 'StyleData');
$cfg_module_style=str::json_data($cfg_module_style, 'decode');
?>
<style>
.prod_box.hover_1 .prod_box_pic{ border:1px solid <?=$cfg_module_style['FontColor']?>;}
.prod_box.hover_1 .prod_box_info{ background:<?=$cfg_module_style['FontColor']?>;}
.prod_box .prod_box_view{ background:<?=$cfg_module_style['FontColor']?>;}
.prod_box.hover_1 .prod_box_view{ border-top:1px solid #fff;}
.prod_box .prod_box_button{ border-top:1px #fff solid;}
.prod_box .prod_box_button>div{ border-right:1px solid #fff;}
</style>
</body>
</html>