<?php !isset($c) && exit();?>
<?php
$lang=trim($c['lang'], '_');
$numAry=array(
	1=>'width:368px;height:217px;top:51px;left:16px;',		//广告1
	2=>'width:368px;height:145px;top:283px;left:16px;',		//广告2
	3=>'width:400px;height:53px;top:440px;left:0px;',		//标题1
	4=>'width:368px;height:535px;top:516px;left:16px;',		//产品1
	5=>'width:368px;height:443px;top:1129px;left:16px;',	//广告3
	6=>'width:400px;height:53px;top:1590px;left:0px;',		//标题2
	7=>'width:368px;height:460px;top:1665px;left:16px;',	//产品2
	8=>'width:368px;height:251px;top:2149px;left:16px;',	//广告4
	9=>'width:400px;height:53px;top:2420px;left:0px;',		//标题3
	10=>'width:368px;height:815px;top:2495px;left:16px;'	//产品3
);
$data_ary=array();
$set_row=db::get_all('web_settings', 'Themes="store"');
foreach($set_row as $v){
	$ad_ary=str::str_code(str::json_data($v['Data'], 'decode'));
	$ad_config=str::json_data($v['Config'], 'decode');
	if(in_array($v['PositionStyle'], $numAry)){
		$key=array_search($v['PositionStyle'], $numAry);
		$data_ary[$key]=array(
			'Data'	=>	$ad_ary,
			'Config'=>	$ad_config
		);
	}
}
?>
<div class="wrapper">
	<div class="banner clean" id="banner_box">
		<ul>
            <?php
			$row=$data_ary[1]['Data'];
            for($i=$sum=0; $i<$data_ary[1]['Config']['PicCount']; ++$i){
				if(!is_file($c['root_path'].$row['PicPath'][$i][$lang])) continue;
				$url=$row['Url'][$i][$lang];
            ?>
            	<li><a href="<?=$url?$url:'javascript:;';?>"<?=$url?' target="_blank"':'';?>><img src="<?=$row['PicPath'][$i][$lang];?>" alt="<?=$row['Name'][$i][$lang];?>" /></a></li>
            <?php
				++$sum;
			}?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="n_ban">
	    <?php
		$row=$data_ary[2]['Data'];
		if(is_file($c['root_path'].$row['PicPath'][0][$lang])){
			echo '<div class="ban fl"><a href="'.($row['Url'][0][$lang]?$row['Url'][0][$lang]:'javascript:;').'"><img src="'.$row['PicPath'][0][$lang].'" alt="'.$row['Name'][0][$lang].'" /></a></div>';
		}
		if(is_file($c['root_path'].$row['PicPath'][1][$lang])){
			echo '<div class="ban fr"><a href="'.($row['Url'][1][$lang]?$row['Url'][1][$lang]:'javascript:;').'"><img src="'.$row['PicPath'][1][$lang].'" alt="'.$row['Name'][1][$lang].'" /></a></div> ';
		}?>
		<div class="clear"></div>
    </div>
	<div class="products_list">
		<?php
		$row=$data_ary[3]['Data'];
		?>
    	<h2 class="cht"><?=$row['Name'][0][$lang]?str::format($row['Name'][0][$lang]):$c['lang_pack']['mobile']['new_arrival'];?></h2>
        <div class="container">
        	<?php
			$pro_data_ary=array();
			$pro_ary=$data_ary[4]['Data'];
			if($pro_ary['Products']){
				$pro_row=str::str_code(db::get_all('products', 'ProId in('.implode(',', $pro_ary['Products']).')', '*', 'ProId desc'));
				foreach($pro_row as $v){
					$pro_data_ary[$v['ProId']]=$v;
				}
				foreach((array)$pro_ary['Products'] as $k=>$v){
					$proid=$v;
					$row=$pro_data_ary[$proid];
					$url=ly200::get_url($row, 'products');
					$img=ly200::get_size_img($row['PicPath_0'], '500x500');
					$name=$row['Name'.$c['lang']];
					$price_ary=cart::range_price_ext($row);
					$is_promition=($row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
					$price_0=$price_ary[1];
					$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
					if($row['PromotionType']) $promotion_discount=100-$row['PromotionDiscount'];
					$rating=($row['IsDefaultReview'] && $row['DefaultReviewRating'])?(int)$row['DefaultReviewRating']:(int)$row['Rating'];
					$total_rating=($row['IsDefaultReview'] && $row['DefaultReviewTotalRating'])?$row['DefaultReviewTotalRating']:$row['TotalRating'];
			?>
					<div class="item<?=$k%2==0?' fl':' fr';?>">
						<div class="img pic_box">
							<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
							<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
							<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
						</div>
						<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
						<div class="price">
							<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$row['ProId'];?>"><?=cart::iconv_price($price_ary[0],2);?></span></span>
							<?php if($price_ary[2]){?>
								<del><span class="price_data"><?=cart::iconv_price($price_0);?></span></del>
							<?php }?>
						</div>
					</div>
				<?php if($k%2==1){?><div class="clear"></div><?php }?>
            <?php
				}
			}?>
            <div class="clear"></div>
        </div>
        <div class="more"><a href="/New-Arrivals/"><?=$c['lang_pack']['more'];?> + </a></div>
    </div> 
	<?php
	$row=$data_ary[5]['Data'];
	if(is_file($c['root_path'].$row['PicPath'][0][$lang])){
		echo '<div class="ban"><a href="'.($row['Url'][0][$lang]?$row['Url'][0][$lang]:'javascript:;').'"><img src="'.$row['PicPath'][0][$lang].'" alt="'.$row['Name'][0][$lang].'" /></a></div>';
	}
	if(is_file($c['root_path'].$row['PicPath'][1][$lang])){
		echo '<div class="ban"><a href="'.($row['Url'][1][$lang]?$row['Url'][1][$lang]:'javascript:;').'"><img src="'.$row['PicPath'][1][$lang].'" alt="'.$row['Name'][1][$lang].'" /></a></div>';
	}?>
	<div class="swipe_products swipe_products_0">
		<?php
		$row=$data_ary[6]['Data'];
		?>
		<h2 class="cht"><?=$row['Name'][0][$lang]?str::format($row['Name'][0][$lang]):$c['lang_pack']['mobile']['hot_sale'];?></h2>
        <div class="container" id="swipe_products_0">
        	<ul>
				<?php
				$pro_data_ary=array();
				$pro_ary=$data_ary[7]['Data'];
				if($pro_ary['Products']){
					$pro_row=str::str_code(db::get_all('products', 'ProId in('.implode(',', $pro_ary['Products']).')', '*', 'ProId desc'));
					foreach($pro_row as $v){
						$pro_data_ary[$v['ProId']]=$v;
					}
					foreach((array)$pro_ary['Products'] as $k=>$v){
						$proid=$v;
						$row=$pro_data_ary[$proid];
						$url=ly200::get_url($row, 'products');
						$img=$row['PicPath_0'];//ly200::get_size_img($row['PicPath_0'], '640x640');
						$name=$row['Name'.$c['lang']];
						$price_ary=cart::range_price_ext($row);
						$is_promition=($row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
						$price_0=$price_ary[1];
						$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
						if($row['PromotionType']) $promotion_discount=100-$row['PromotionDiscount'];
						$rating=($row['IsDefaultReview'] && $row['DefaultReviewRating'])?(int)$row['DefaultReviewRating']:(int)$row['Rating'];
						$total_rating=($row['IsDefaultReview'] && $row['DefaultReviewTotalRating'])?$row['DefaultReviewTotalRating']:$row['TotalRating'];
				?>
						<li class="item fl">
							<div class="img pic_box">
								<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
							</div>
							<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
							<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
							<div class="price">
								<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$row['ProId'];?>"><?=cart::iconv_price($price_ary[0],2);?></span></span>
								<?php if($price_ary[2]){?>
									<del><span class="price_data"><?=cart::iconv_price($price_0);?></span></del>
								<?php }?>
								<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><?=$promotion_discount;?>% OFF</em><?php }?>
								<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
							</div>
						</li>
				<?php
					}
				}?>
            </ul>
        </div>
        <a href="javascript:;" rel="nofollow" class="prev"></a>
        <a href="javascript:;" rel="nofollow" class="next"></a>
	</div>
	<?php
	$row=$data_ary[8]['Data'];
	if(is_file($c['root_path'].$row['PicPath'][0][$lang])){
		echo '<div class="ban"><a href="'.($row['Url'][0][$lang]?$row['Url'][0][$lang]:'javascript:;').'"><img src="'.$row['PicPath'][0][$lang].'" alt="'.$row['Name'][0][$lang].'" /></a></div>';
	}?>
    <div class="products_list">
    	<?php
		$row=$data_ary[9]['Data'];
		?>
    	<h2 class="cht"><?=$row['Name'][0][$lang]?str::format($row['Name'][0][$lang]):$c['lang_pack']['mobile']['best_deals'];?></h2>
        <div class="container">
        	<?php
        	$pro_data_ary=array();
			$pro_ary=$data_ary[10]['Data'];
			if($pro_ary['Products']){
				$pro_row=str::str_code(db::get_all('products', 'ProId in('.implode(',', $pro_ary['Products']).')', '*', 'ProId desc'));
				foreach($pro_row as $v){
					$pro_data_ary[$v['ProId']]=$v;
				}
				foreach((array)$pro_ary['Products'] as $k=>$v){
					$proid=$v;
					$row=$pro_data_ary[$proid];
					$url=ly200::get_url($row, 'products');
					$img=ly200::get_size_img($row['PicPath_0'], '500x500');
					$name=$row['Name'.$c['lang']];
					$price_ary=cart::range_price_ext($row);
					$is_promition=($row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
					$price_0=$price_ary[1];
					$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
					if($row['PromotionType']) $promotion_discount=100-$row['PromotionDiscount'];
					$rating=($row['IsDefaultReview'] && $row['DefaultReviewRating'])?(int)$row['DefaultReviewRating']:(int)$row['Rating'];
					$total_rating=($row['IsDefaultReview'] && $row['DefaultReviewTotalRating'])?$row['DefaultReviewTotalRating']:$row['TotalRating'];
			?>
					<div class="item<?=$k%2==0?' fl':' fr';?>">
						<div class="img pic_box">
							<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
							<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
							<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
						</div>
						<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
						<div class="price">
							<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$row['ProId'];?>"><?=cart::iconv_price($price_ary[0],2);?></span></span>
							<?php if($price_ary[2]){?>
								<del><span class="price_data"><?=cart::iconv_price($price_0);?></span></del>
							<?php }?>
						</div>
					</div>
				<?php if($k%2==1){?><div class="clear"></div><?php }?>
            <?php
				}
			}?>
            <div class="clear"></div>
        </div>
    </div> 
</div>