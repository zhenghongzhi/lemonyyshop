/*
 * 广州联雅网络
 */

$(function(){
	var ospan=$('.banner .btn span');
	new Swipe(document.getElementById('banner_box'), {
		speed:500,
		auto:5000,
		callback: function(){
			ospan.removeClass("on").eq(this.index).addClass("on");
		}
	});

	$('#swipe_products_0').height($('#swipe_products_0 .item').eq(0).innerHeight());
	var swipe_products_0 = new Swipe(document.getElementById('swipe_products_0'), {
		speed:500,
		auto:100000,
		callback: function(){
			$('#swipe_products_0').animate({height:$('#swipe_products_0 .item').eq(this.index).innerHeight()},'100');
		}
	});
	$('.swipe_products_0 .prev').click(function(){
		swipe_products_0.prev();
	});
	$('.swipe_products_0 .next').click(function(){
		swipe_products_0.next();
	});
});
