<?php !isset($c) && exit();?>
<?php
$products_list_row=str::str_code(db::get_limit('products', 'IsBestDeals=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 10));
?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
            for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
            	<li plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="little clean" plugins="mindex-0" effect="0-1">
    	<?php
        for($i=0; $i<3; ++$i){
			$url=$c['web_pack']['mindex'][0][$i]['Link']?$c['web_pack']['mindex'][0][$i]['Link']:'javascript:;';
		?>
			<div class="item fl ui_border" plugins_pos="<?=$i?>">
				<div class="c">
					<div class="img"><a href="<?=$url;?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][0][$i]['Title'];?>" /></a></div>
					<div class="name"><a href="<?=$url;?>" plugins_mod="Title"><?=$c['web_pack']['mindex'][0][$i]['Title'];?></a></div>
				</div>
			</div>
        <?php }?>
    </div>
    <div class="banr2 clean" plugins="mindex-1" effect="0-1" plugins_pos="0"><a href="<?=$c['web_pack']['mindex'][1][0]['Link']?$c['web_pack']['mindex'][1][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][0]['Title'];?>" /></a></div>
    <div class="home_list on">
    	<h2 class="cht"><?=$c['lang_pack']['mobile']['best_deals'];?></h2>
        <div class="container">
        	<?php
			$len=count($products_list_row);
			foreach($products_list_row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$price_0 = $price_ary[1];
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
				//$rating=($v['IsDefaultReview'] && $v['DefaultReviewRating'])?(int)$v['DefaultReviewRating']:ceil($v['Rating']);
				//$total_rating=($v['IsDefaultReview'] && $v['DefaultReviewTotalRating'])?$v['DefaultReviewTotalRating']:$v['TotalRating'];
                $pro_info=ly200::get_pro_info($v);
                $rating=$pro_info[2];
                $total_rating=$pro_info[3];
				echo $k%2==0?'<div class="box clean">':'';
			?>
				<div class="item fl ui_border">
					<div class="c">
						<div class="img pic_box">
							<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
							<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
							<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
						</div>
						<div class="proname"><a href="<?=$url;?>"><?=$name;?></a></div>
						<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?><span>(<?=$total_rating;?>)</span></div><?php }?>
						<div class="price">
							<span><?=cart::iconv_price(0, 1);?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span> <?=$_SESSION['Currency']['Currency'];?>
						</div>
					</div>
				</div>
            <?php
				echo (($k+1)%2==0 || $k==$len-1)?'</div>':'';
			}?>
        </div>
    </div>
    <div class="banr2 clean" plugins="mindex-2" effect="0-1" plugins_pos="0"><a href="<?=$c['web_pack']['mindex'][2][0]['Link']?$c['web_pack']['mindex'][2][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][2][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][2][0]['Title'];?>" /></a></div>
</div>