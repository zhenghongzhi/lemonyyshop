<?php !isset($c) && exit();?>
<?php
$category_row=str::str_code(db::get_all('products_category', 'UId="0," and IsSoldOut=0', "CateId, UId, Category{$c['lang']}, PicPath", $c['my_order'].'CateId asc'));
?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, '09');
            for($i=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
            ?>
            <li><a href="<?=$url?$url:'javascript:;';?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
    </div>
    <div class="home_menu">
    	<?php foreach((array)$category_row as $k=>$v){?>
    		<div class="row"><a href="<?=$c['mobile_url'].ly200::get_url($v, 'products_category')?>" class="links"><?=$v['Category'.$c['lang']]?></a></div>
        <?php }?>
    </div>
    <div class="home_pro_touch" id="touch0" plugins="mindex-0" effect="0-1" plugins_pos="0">
    	<div class="title" plugins_mod="Title"><?=$c['web_pack']['mindex'][0][0]['Title'];?></div>
    	<div class="list clean">
        	<?php
			$products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 6));
			foreach($products_list_row as $v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$price_0 = $price_ary[1];
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
			?>
				<div class="item fl">
					<div class="img pic_box">
						<a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a>
						<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
						<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
					</div>
					<div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
					<div class="price">
						<span><?=cart::iconv_price(0, 1);?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2)?></span></span> <?=$_SESSION['Currency']['Currency'];?>
					</div>
				</div>
            <?php }?>
        </div>
    </div>
</div>