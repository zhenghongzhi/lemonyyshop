<?php !isset($c) && exit();?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, 16);
            for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
            	<li><a href="<?=$url?$url:'javascript:;';?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
	<div class="products_list p_list swipe_products_0" plugins="mindex-0" effect="0-1" plugins_pos="0">
		<?php //$indcon=ly200::web_settings(4, 16); ?>
		<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][0][0]['Title'];?></h2>
		<div class="top_title_tips" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][0][0]['SubTitle'];?></div>
	    <div class="ban container"><a href="<?=$c['web_pack']['mindex'][0][0]['Link']?$c['web_pack']['mindex'][0][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][0][0]['Title']?>" /></a></div> 
        <div class="container" id="swipe_products_0">
        	<ul>
            <?php
				$products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 10));
				$products_list_ary=array_chunk((array)$products_list_row,2);
	            foreach((array)$products_list_ary as $k=>$v){
	            ?>
	            	<li>
	            		<?php foreach((array)$v as $k1=>$v1){
	            			$url=ly200::get_url($v1, 'products');
							$img=ly200::get_size_img($v1['PicPath_0'], '500x500');
							$name=$v1['Name'.$c['lang']];
							$price_ary=cart::range_price_ext($v1);
							$is_promition=($v1['IsPromotion'] && $v1['StartTime']<$c['time'] && $c['time']<$v1['EndTime'])?1:0;
							$price_0=$price_ary[1];
							$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
							if($v1['PromotionType']) $promotion_discount=100-$v1['PromotionDiscount'];
							$rating=($v1['IsDefaultReview'] && $v1['DefaultReviewRating'])?(int)$v1['DefaultReviewRating']:(int)$v1['Rating'];
							$total_rating=($v1['IsDefaultReview'] && $v1['DefaultReviewTotalRating'])?$v1['DefaultReviewTotalRating']:$v1['TotalRating'];
	            		?>
	            			<div class="item fl <?=$k1%2==1?'last':'';?>">
								<div class="img pic_box">
									<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
									<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor">- <?=$promotion_discount;?>%</em><?php }?>
									<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
								</div>
								<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
								<div class="price">
									<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v1['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span>
									<?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?>
								</div>
								<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
							</div>
	            		<?php }?>
	            	</li>
	            <?php }?>
	        </ul>
	        <div class="btn">
	        	<?php foreach((array)$products_list_ary as $k=>$v){?>
	            	<span class="<?=$k==0?'on':'';?>"></span>
	            <?php }?>
	        </div>
        </div>  
        <div class="products_blank"></div>      
	</div>

	<div class="middle_ad" plugins="mindex-1" effect="0-1">
		<?php
		//$ad_ary=ly200::ad_custom(3, 0, 16);
		for($i=$m=0;$i<2;$i++){
		?>
	    	<div class="mid_ban <?=$m==0?'fir':'';?>" plugins_pos="<?=$i?>">
	    		<a href="<?=$c['web_pack']['mindex'][1][$i]['Link']?$c['web_pack']['mindex'][1][$i]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][$i]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][$i]['Title'];?>" /></a>
	    	</div>
		<?php 
			$m++;
			}
		?>
	</div>
    <div class="products_list p_list" plugins="mindex-2" effect="0-1" plugins_pos="0">
    	<?php //$indcon=ly200::web_settings(5, 16); ?>
		<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][2][0]['Title'];?></h2>
		<div class="top_category_list">
			<?php 
				$index_category_row=str::str_code(db::get_limit('products_category','IsIndex=1 and IsSoldOut=0','*',$c['my_order'].'CateId asc',0,3));
				foreach((array)$index_category_row as $k=>$v){
			?>
				<a href="javascript:;" class="top_category_item" title="<?=$v['Category'.$c['lang']];?>" data-cateid="<?=$v['CateId'];?>"><?=$v['Category'.$c['lang']];?></a>
			<?php }?>
		</div>
        <div class="container" id="index_products_list"></div>
    </div> 
	<?php if($c['FunVersion']>1 && in_array('blog', $c['plugins']['Used']) && db::get_row_count('blog')){ ?>
		<div class="index_blog" plugins="mindex-3" effect="0-1" plugins_pos="0">
			<?php //$indcon=ly200::web_settings(6, 16); ?>
			<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title'];?></h2>
	    	<div class="top_title_tips2" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][3][0]['SubTitle'];?></div>
	    	<div class="blog_list b_list">
	    		<?php 
	    			$blog_row=str::str_code(db::get_limit('blog','IsHot=1','*',$c['my_order'].'AId desc',0,2));
	    			foreach((array)$blog_row as $k=>$v){
	    				$url=ly200::get_url($v,'blog');
	    				$title=$v['Title'];
	    				$pic=$v['PicPath'];
	    				$brief=$v['BriefDescription'];
	    		?>
		    	<div class="blog_item">
		    		<?php if(is_file($c['root_path'].$pic)){?>
		    			<a href="<?=$url;?>" class="blog_img"><img src="<?=$pic;?>"></a>
		    		<?php }?>
		    		<div class="blog_content">
		    			<a href="<?=$url;?>" title="<?=$title;?>" class="blog_title"><?=$title;?></a>
		    			<div class="blog_brief"><?=$brief;?></div>
		    			<div class="blog_time">Published on <span><?=date('M,d Y',$v['AccTime']);?></span></div>
		    		</div>
		    	</div>
		    	<?php }?>
	    	</div>
		</div>
	<?php } ?>	
</div>