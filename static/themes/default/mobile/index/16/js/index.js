/*
 * 广州联雅网络
 */

$(function(){
	var ospan=$('.banner .btn span');
	new Swipe(document.getElementById('banner_box'), {
		speed:500,
		auto:5000,
		callback: function(){
			ospan.removeClass("on").eq(this.index).addClass("on");
		}
	});
	
	var ospan1=$('#swipe_products_0 .btn span');
	new Swipe(document.getElementById('swipe_products_0'), {
		speed:500,
		auto:100000,
		callback: function(){
			ospan1.removeClass("on").eq(this.index).addClass("on");
		}
	});

	function get_mid(item){
		return parseInt($(item).length/2);
	}

	$('.top_category_list').find('.top_category_item').click(function(){
		var CateId=parseInt($(this).attr('data-cateid'));
		$(this).addClass('on').siblings('.top_category_item').removeClass('on');
		$.post('/ajax/index_products_list.html',{'CateId':CateId},function(result){
			$('#index_products_list').html(result);
		},'html');
	});

	$('.top_category_list').find('.top_category_item').eq(0).click();
});
