<?php !isset($c) && exit();?>
<!-- redesign styles -->
<!--<link rel="stylesheet" type="text/css" href="/static/iran/server/mainServicesStyle.css">-->
<!--<link rel="stylesheet" type="text/css" href="/static/iran/server/rfqStyle.css">-->
<!--<link rel="stylesheet" type="text/css" href="/static/iran/server/hotsalesStyle.css">-->
<!--link rel="stylesheet" type="text/css" href="/static/iran/server/article.css">-->
<!--<link rel="stylesheet" type="text/css" href="/static/iran/server/footerNewStyle.css">-->
<link rel="stylesheet" type="text/css" href="/static/iran/server/Redesign_Files/article.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/Redesign_Files/footerNewStyle.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/Redesign_Files/hotsalesStyle.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/Redesign_Files/mainServicesStyle.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/Redesign_Files/rfqStyle.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/blog.css">
<link rel="stylesheet" href="/static/iran/server/Redesign_Files/flickity.css">
<link rel="stylesheet" type="text/css" href="/static/iran/server/alarm.css">

<!-- redesign styles -->
<!-- redesign scripts -->
<script type="text/javascript" src="/static/iran/server/rfq.js"></script>
<script type="text/javascript" src="/static/iran/server/sweetalert.min.js"></script>
<script src='/static/iran/server/Redesign_Files/flickity.pkgd.js'></script>
<!-- redesign scripts -->

<style type="text/css">
    .FontColor,
    a.FontColor,
    a.FontColor:hover {
        color: #028abe;
    }

    .FontBgColor {
        background-color: #028abe;
    }

    .FontBorderColor {
        border-color: #028abe;
    }

    .FontBorderHoverColor:hover,
    a.FontBorderHoverColor:hover {
        border-color: #028abe !important;
    }

    .FontBgHoverColor:hover {
        background-color: #028abe !important;
    }

    .FontHoverColor:hover {
        color: #028abe !important;
    }

    .AddtoCartBgColor {
        background-color: #AF0D64;
    }

    .AddtoCartBorderColor {
        border-color: #AF0D64;
    }

    .BuyNowBgColor {
        background-color: #EA215C;
    }

    .ReviewBgColor {
        background-color: #EA215C;
    }

    .DiscountBgColor {
        background-color: #EA215C;
    }

    .DiscountBorderColor {
        border-color: #EA215C;
    }

    .ProListBgColor {
        background-color: #005AB0;
    }

    .ProListHoverBgColor:hover {
        background-color: #357CBE;
    }

    .GoodBorderColor {
        border-color: #005AB0;
    }

    .GoodBorderHoverColor:hover {
        border-color: #357CBE;
    }

    .GoodBorderColor.selected {
        border-color: #357CBE;
    }

    .GoodBorderBottomHoverColor {
        border-bottom-color: #357CBE;
    }
</style>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, 10);
            for($i=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
            ?>
            <li><a href="<?=$url?$url:'javascript:;';?>" title="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
    </div>
    <div class="blank"></div>


    <div class="ad_box" plugins="mindex-0" effect="0-1">
    	<?php
		//$ad_ary=ly200::ad_custom(2, 0, 10);
		for($i=0; $i<2; ++$i){
			//if(!is_file($c['root_path'].$c['web_pack']['mindex'][0][$i]['Pic'])) continue;
			$url=$c['web_pack']['mindex'][0][$i]['Link'];
		?>
        <div class="item <?=$i?'fr':'fl'?>" plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;'?>" title="<?=$c['web_pack']['mindex'][0][$i]['Title'];?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][$i]['Pic']?>" alt="<?=$c['web_pack']['mindex'][0][$i]['Title'];?>" /></a></div>
        <?php }?>
        <div class="clear"></div>
    </div>
    <div class="blank"></div>
    <!-- main services -->
    <!-- onclick="window.location.href='https://www.google.com/'" -->
<!--    <div class="g_title titleService" plugins_mod="Title">-->
<!--        --><?//=$c['lang_pack']['zhziwant']; ?>
<!--    </div>-->
    <div style="background-image: url(/static/iran/server/img/9-3_banner_bg.png);background-repeat: no-repeat;background-size: cover;background-position: top;">
        <div class="myarticle">
            <h2><?=$c['lang_pack']['zhziwant']; ?></h2>
        </div>

<!--        <div class="mainServicesContainer">-->
<!--            <div class="mainIcon">-->
<!--                <div class="feature-box servicesItem "style="margin: auto; width: 96%;" onclick="window.location.href='/products/rfqcate1/'">-->
<!--                    <div class="img thirdImg"><img plugins_mod="Pic" src="/static/iran/server/img/10_sell-logo.png"></div>-->
<!--                    <div class="status-tag critical-low tag-in-box">-->
<!--                        <i class="highlight critical-low" style="--iteration-count: infinite;"></i>-->
<!--                        <p class="status-tag__txt bac-l-stack-xs">--><?//=$c['lang_pack']['zhzfindproduct']; ?><!--</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="mainServicesContainer">
            <div class="mainIcon">
                <div class="feature-box servicesItem global_menu">
                    <div class="img secondImg"><img plugins_mod="Pic" src="/static/iran/server/img/15_marketing_logo.png"></div>
                    <div class="status-tag critical-low tag-in-box">
                        <i class="highlight critical-low" style="--iteration-count: infinite;"></i>
                        <p class="status-tag__txt bac-l-stack-xs"><?=$c['lang_pack']['zhzmarketplace']; ?></p>
                    </div>
                </div>
                <div class="feature-box servicesItem" style="margin-left:2%;" onclick="window.location.href='/products/logline/'">

                    <div class="img firstImg"><img plugins_mod="Pic" src="/static/iran/server/img/17_pay.png"></div>
                    <div class="status-tag critical-low tag-in-box">
                        <i class="highlight critical-low" style="--iteration-count: infinite;"></i>
                        <p class="status-tag__txt bac-l-stack-xs"><?=$c['lang_pack']['zhzfindcargo']; ?></p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- end main services -->
    <!-- RFQ form -->

    <div class="myarticle g_title titleService">
        <h2 style="font-size: 1.375rem !important;"><?=$c['lang_pack']['zhztelluswhatproductyouinteresting']; ?></h3>
    </div>
    <? if($c['lang']=='_fa'){ ?>
        <img style="" src="/static/iran/server/img/test_fa.png" alt="">
    <? }else{ ?>
        <img style="" src="/static/iran/server/img/test.png" alt="">
    <? } ?>

    <div class="rfqContainer">
        <div class="cont-contactBtn">
            <div class="cont-flip ">
                <div class="contact-form">
                    <input id="1" class="gutter" type="text" placeholder="<?=$c['lang_pack']['zhzname']; ?>">
                    <input id="2" class="text" type="text" placeholder="<?=$c['lang_pack']['zhzphonenumber']; ?>">
                    <input id="3" class="text-p" type="text" placeholder="<?=$c['lang_pack']['zhzproductnamecategory']; ?>">
                    <input id="send_button" type="submit" value="<?=$c['lang_pack']['zhzSend']; ?>">
                </div>
            </div>
        </div>
    </div>
    <!-- end RFQ form -->
<!--    <div class="g_title titleService" plugins_mod="Title">-->
<!--        <h3>--><?//=$c['lang_pack']['zhztelluswhatproductyouinteresting']; ?><!--</h3>-->
<!--    </div>-->
<!--    --><?// if($c['lang']=='_fa'){ ?>
<!--        <img style="" src="/static/iran/server/img/test_fa.png" alt="">-->
<!--    --><?// }else{ ?>
<!--        <img style="" src="/static/iran/server/img/test.png" alt="">-->
<!--    --><?// } ?>
<!---->
<!--    <div class="rfqContainer">-->
<!--        <div class="cont-contactBtn">-->
<!--            <div class="cont-flip ">-->
<!---->
<!--                <div class="contact-form">-->
<!---->
<!--                    <input id="1" class="gutter" type="text" placeholder="--><?//=$c['lang_pack']['zhzname']; ?><!--">-->
<!--                    <input id="2" class="text" type="text" placeholder="--><?//=$c['lang_pack']['zhzphonenumber']; ?><!--">-->
<!--                    <input id="3" class="text-p" type="text" placeholder="--><?//=$c['lang_pack']['zhzproductnamecategory']; ?><!--">-->
<!--                    <input id="send_button" type="submit" value="--><?//=$c['lang_pack']['zhzSend']; ?><!--">-->
<!---->
<!--                </div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <!-- end RFQ form -->
    <!-- Blog -->

    <div class="myarticle">
        <div class="g_title" plugins_mod="Title">Read Our Lates News</div>
<!--        <h2>Read Our Lates News</h2>-->
    </div>
    <div class="blog-store">

        <div class="blog-slide">
            <div class="blog js-flickity" data-flickity-options='{ "wrapAround": true }'>
                <div class="blog-cell"
                     style="background-image: linear-gradient(rgba(0, 0, 0, 0.1),rgba(0, 0, 0, 0.1)) , url(https://blog.lemonyyshop.com/upload/photos/2020/08/GJPTEzRJQCdj1a1oGFfl_06_97451785714f9e0efed6c542c55a7ab5_image.jpg);"
                     tap="window.location.href='https://blog.lemonyyshop.com/news/اصطلاحات-و-قوانین-تجارت-بین-الملل-8'" url="https://blog.lemonyyshop.com/news/اصطلاحات-و-قوانین-تجارت-بین-الملل-8">
                        <div class="blog-content">
                            <div class="blog-border">

                                <div class="blog-title">International Trade Terms and Conditions International</div>
                                <div class="blog-author">
                                    <span id="datetime">Thu, 06 Aug 2020 19:28:12</span>
                                </div>

                                <!-- <div class="blog-sum">you will underestand more about Factory Delivery , Free Delivery
                                    at the Origin , The Cost of Transportation ,Transportation’s Paid Cost and Insurance
                                </div> -->
                            </div>
                            <!-- <div class="blog-see"
                                onclick="window.location.href='https://blog.lemonyyshop.com/news/اصطلاحات-و-قوانین-تجارت-بین-الملل-8'">
                                Read More</div> -->
                        </div>
                </div>
                <div class="blog-cell"
                     style="background-image:  linear-gradient(
                                rgba(0, 0, 0, 0.1),
                                rgba(0, 0, 0, 0.1)
                              ) ,  url(https://blog.lemonyyshop.com/upload/photos/2020/08/NnuExQfgH3V1jsGHTat8_06_3a73e76333c92552b0c3d0991784f8c1_image.jpg);"
                     tap="window.location.href='https://blog.lemonyyshop.com/news/در-مذاکره-با-بازرگانان-چینی-چه-نکاتی-به-دردمان-می-خورد-7'" url="https://blog.lemonyyshop.com/news/در-مذاکره-با-بازرگانان-چینی-چه-نکاتی-به-دردمان-می-خورد-7">
                    <div class="blog-content">
                        <div class="blog-border">
                            <div class="blog-title">What is the point of use in negotiating with Chinese businessmen
                            </div>
                            <div class="blog-author">
                                <span id="datetime">Thu, 06 Aug 2020 19:14:32</span>
                            </div>

                            <!-- <div class="blog-sum">we refer to the most important behaviors and practices of
                                negotiating with Chinese businessmen
                                read it for having successful business
                            </div> -->
                        </div>
                        <!-- <div class="blog-see blog-blue" onclick="window.location.href='https://blog.lemonyyshop.com/news/در-مذاکره-با-بازرگانان-چینی-چه-نکاتی-به-دردمان-می-خورد-7'">Read More</div> -->
                    </div>
                </div>
                <div class="blog-cell"
                     style="background-image: linear-gradient(
                                rgba(0, 0, 0, 0.1),
                                rgba(0, 0, 0, 0.1)
                              ) , url(https://blog.lemonyyshop.com/upload/photos/2020/08/qPE1dB4DjqpAxdMSfetC_06_82677ca6178691736ec369cf55c2ac68_image.png);"
                     tap="window.location.href='https://blog.lemonyyshop.com/news/۴-نکته-اساسی-برای-تجارت-ایمن-با-چین-6'" url="https://blog.lemonyyshop.com/news/۴-نکته-اساسی-برای-تجارت-ایمن-با-چین-6">
                    <div class="blog-content">
                        <div class="blog-border">
                            <div class="blog-title">4 essential tips for safe trade with China</div>
                            <div class="blog-author">
                                <span id="datetime">Thu, 06 Aug 2020 18:46:55</span>
                            </div>
                            <!-- <div class="blog-sum">how can we make business relationships based on trust ,for
                                answering to your questions read this article </div>-->
                        </div>
                        <!-- <div class="blog-see blog-pink" onclick="window.location.href='https://blog.lemonyyshop.com/news/۴-نکته-اساسی-برای-تجارت-ایمن-با-چین-6'">Read More</div> -->
                    </div>
                </div>
                <div class="blog-cell"
                     style="background-image: linear-gradient(
                                rgba(0, 0, 0, 0.1),
                                rgba(0, 0, 0, 0.1)
                              ) , url(https://blog.lemonyyshop.com/upload/photos/2020/08/9MigRIZkbyZ7I2RNWtif_05_182f198f224310487ecf4eb642bf8362_image.jpg);"
                     tap="window.location.href='https://blog.lemonyyshop.com/news/حقوق-تجارت-بین-الملل-چیست-و-چه-تفاوتی-با-حقوق-بین-الملل-دارد-4'" url="https://blog.lemonyyshop.com/news/حقوق-تجارت-بین-الملل-چیست-و-چه-تفاوتی-با-حقوق-بین-الملل-دارد-4">
                    <div class="blog-content">
                        <div class="blog-border">
                            <div class="b
                            </div> -->
                        </div>log-title">what is the law of international trade and what difference does
                        it make with international law</div>
                    <div class="blog-author">
                        <span id="datetime">Wed, 05 Aug 2020 19:36:42</span>
                    </div>
                    <!-- <div class="blog-sum">find more about diffrent goverment's law and
                        the diffrents between International law and international trade law

                <!-- <div class="blog-see blog-yellow" onclick="window.location.href='https://blog.lemonyyshop.com/news/حقوق-تجارت-بین-الملل-چیست-و-چه-تفاوتی-با-حقوق-بین-الملل-دارد-4'">Read More</div> -->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end Blog -->
    <script>
        $('.blog-cell').tap(function(){
            var url = $(this).attr('url');
            window.location.href = url;
        })
    </script>
    <?php
    $SeckillEndTime=db::get_value('sales_seckill_period', "{$c['time']}>StartTime and {$c['time']}<EndTime", 'EndTime', 'EndTime asc'); 
    if($SeckillEndTime>$c['time']){
    ?>
    <div class="sekill_box" plugins="mindex-1" effect="0-1" plugins_pos="0">
    	<div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][1][0]['Title']?></div>
        <div id="timer" class="timer" data-time="<?=date('Y/m/d H:i:s', $SeckillEndTime);?>">
        	<span class="hours">
                <i>0</i>
                <i>0</i>
            </span>
            <em></em>
			<span class="minutes">
                <i>0</i>
                <i>0</i>
            </span>
            <em></em>
            <span class="second">
                <i>0</i>
                <i>0</i>
            </span>
        </div>
        <div id="seckill_pro" class="con">
        	<ul>
           		<li>
					<?php
					$where="1 and s.StartTime<={$c['time']} and s.EndTime>{$c['time']}";
					$in_sales_pro=str::str_code(db::get_limit('sales_seckill s left join products p on s.ProId=p.ProId', $where.' and s.RemainderQty>0', "s.*, p.Name{$c['lang']}, p.PicPath_0, p.Price_0, p.Price_1, p.TotalRating, p.IsHot, p.Unit", 's.Price asc, if(s.MyOrder>0, if(s.MyOrder=999, 1000001, s.MyOrder), 1000000) asc, s.SId desc', 0, 6));
					foreach((array)$in_sales_pro as $k=>$v){
						$url=ly200::get_url($v, 'products');
						$img=ly200::get_size_img($v['PicPath_0'], '500x500');
                        $price_ary=cart::range_price_ext($v);
                        $price_0 = $price_ary[1];
						$name=$v['Name'.$c['lang']];
						$progress=ceil((1-$v['RemainderQty']/$v['Qty'])*100);
						echo ($k && $k%2==0)?'<div class="clear"></div></li><li>':'';
					?>
						<div class="item <?=$k%2==0?'fl':'fr'?>">
							<div class="img pic_box"><a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a></div>
							<div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
							<div class="price"><?=cart::iconv_price($v['Price'])?><?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?></div>
							<div class="progress"><span><?=$progress;?>% Claimed</span><i style="width:<?=$progress;?>%;"></i></div>
						</div>
					<?php }?>
                </li>
            </ul>
			<?php
			if($in_sales_pro){
				$num=count($in_sales_pro)/2;
				$num=ceil($num);
			?>
				<div class="tab">
					<?php for($i=0; $i<$num; ++$i){?>
						<a href="javascript:;"<?=$i?'':' class="cur"';?>></a>
					<?php }?>
				</div>
			<?php }?>
        </div>
    </div>
    <?php }?>
    <?php $url=$c['web_pack']['mindex'][2][0]['Link'];?>
    <div class="blank"></div>
    <? if($c['web_pack']['mindex'][2][0]['Title']){ ?>
        <div class="mid_ad" plugins="mindex-2" effect="0-1" plugins_pos="0">
            <div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][2][0]['Title']?></div>
            <div class="img"><a href="<?=$url?$url:'javascript:;'?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][2][0]['Pic'];?>" /></a></div>
            <div class="brief" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][2][0]['SubTitle']?></div>
        </div>
        <div class="blank"></div>
    <? } ?>
    <div class="hot_box" plugins="mindex-3" effect="0-1" plugins_pos="0">
    	<div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title']?></div>
        <div class="con">
            <?php
            $products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 20));
            foreach($products_list_row as $k => $v){
                $url=ly200::get_url($v, 'products');
                $img=ly200::get_size_img($v['PicPath_0'], '500x500');
                $name=$v['Name'.$c['lang']];
                $price_ary=cart::range_price_ext($v);
                $price_0 = $price_ary[1];
                $is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
                $promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
                if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
                ?>
                <div class="item" <?=$k%2==0?'style="margin-left:0;"':''?>>
                    <div class="img pic_box">
                        <a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a>
                        <?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
                        <em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
                    </div>
                    <div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
                    <div class="price">
                        <span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0],2);?></span></span>
                    </div>
                </div>
            <?php }?>
            <div class="clear"></div>
        </div>


    <div class="blank"></div>
</div>