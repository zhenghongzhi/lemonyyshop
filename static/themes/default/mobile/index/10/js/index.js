/*
 * 广州联雅网络
 */
(function ($){
	$(function (){
		new Swipe(document.getElementById('banner_box'), {
			speed:500,
			auto:5000,
			callback: function(){}
		});
		new Swipe(document.getElementById('seckill_pro'), {
			speed:500,
			auto:100000,
			callback: function(){
				$('#seckill_pro .tab a').eq(this.index).addClass('cur').siblings().removeClass();
			}
		});
		//秒杀
        var filtertime=function(num,n){	//向前补零
			return (Array(n).join(0) + num).slice(-n);
        }
		setInterval(function(){
			var nowtime=new Date();
			var date=$('#timer').data('time');
			var endtime=new Date(date);
			var time=endtime-nowtime;
			var day = parseInt(time / 1000 / 60 / 60 / 24);
			var hour = parseInt(time / 1000 / 60 / 60 % 24);
			var minutes = parseInt(time / 1000 / 60 % 60);
			var second = parseInt(time / 1000 % 60);
			hour=day*24+hour;
			if(hour>99) hour=99;
			hour=filtertime(hour,2);
			minutes=filtertime(minutes,2);
			second=filtertime(second,2);
			$('#timer .hours i:eq(0)').text(hour.substr(0,1));
			$('#timer .hours i:eq(1)').text(hour.substr(1,1));
			$('#timer .minutes i:eq(0)').text(minutes.substr(0,1));
			$('#timer .minutes i:eq(1)').text(minutes.substr(1,1));
			$('#timer .second i:eq(0)').text(second.substr(0,1));
			$('#timer .second i:eq(1)').text(second.substr(1,1));
		},1000);
	});
})(jQuery);