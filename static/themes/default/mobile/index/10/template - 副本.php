<?php !isset($c) && exit();?>
<link rel="stylesheet" type="text/css" href="/static/iran/server/mainServicesStyle.css">
<style type="text/css">
    .FontColor,
    a.FontColor,
    a.FontColor:hover {
        color: #028abe;
    }

    .FontBgColor {
        background-color: #028abe;
    }

    .FontBorderColor {
        border-color: #028abe;
    }

    .FontBorderHoverColor:hover,
    a.FontBorderHoverColor:hover {
        border-color: #028abe !important;
    }

    .FontBgHoverColor:hover {
        background-color: #028abe !important;
    }

    .FontHoverColor:hover {
        color: #028abe !important;
    }

    .AddtoCartBgColor {
        background-color: #AF0D64;
    }

    .AddtoCartBorderColor {
        border-color: #AF0D64;
    }

    .BuyNowBgColor {
        background-color: #EA215C;
    }

    .ReviewBgColor {
        background-color: #EA215C;
    }

    .DiscountBgColor {
        background-color: #EA215C;
    }

    .DiscountBorderColor {
        border-color: #EA215C;
    }

    .ProListBgColor {
        background-color: #005AB0;
    }

    .ProListHoverBgColor:hover {
        background-color: #357CBE;
    }

    .GoodBorderColor {
        border-color: #005AB0;
    }

    .GoodBorderHoverColor:hover {
        border-color: #357CBE;
    }

    .GoodBorderColor.selected {
        border-color: #357CBE;
    }

    .GoodBorderBottomHoverColor {
        border-bottom-color: #357CBE;
    }
</style>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, 10);
            for($i=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
            ?>
            <li><a href="<?=$url?$url:'javascript:;';?>" title="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
    </div>
    <div class="blank"></div>
    <div class="g_title titleService" plugins_mod="Title">Services</div>
    <div class="mainServicesContainer">
        <div class="mainIcon">
            <div class="feature-box servicesItem" onclick="window.location.href='/products/rfqcate1/'">
                <div class="img thirdImg"><img plugins_mod="Pic" src="/static/iran/server/img/3_img.png"></div>
                <div class="txt-bg">
                    <p class="text-xl">Procurement service</p>
                </div>
            </div>
            <div class="feature-box servicesItem global_menu">
                <div class="img secondImg"><img plugins_mod="Pic" src="/static/iran/server/img/2_img.png"></div>
                <div  class=" txt-bg">
                    <p class="text-xl">Market Place</p>
                </div>
            </div>
            <div class="feature-box servicesItem"  onclick="alert('Coming soon....')">
                <div class="img firstImg"><img plugins_mod="Pic" src="/static/iran/server/img/1_pay.png"></div>
                <div class="txt-bg">
                    <p class="text-xl">Logistics & Payment</p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="ad_box" plugins="mindex-0" effect="0-1">
    	<?php
		//$ad_ary=ly200::ad_custom(2, 0, 10);
		for($i=0; $i<2; ++$i){
			//if(!is_file($c['root_path'].$c['web_pack']['mindex'][0][$i]['Pic'])) continue;
			$url=$c['web_pack']['mindex'][0][$i]['Link'];
		?>
        <div class="item <?=$i?'fr':'fl'?>" plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;'?>" title="<?=$c['web_pack']['mindex'][0][$i]['Title'];?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][$i]['Pic']?>" alt="<?=$c['web_pack']['mindex'][0][$i]['Title'];?>" /></a></div>
        <?php }?>
        <div class="clear"></div>
    </div>
    <div class="blank"></div>


    <?php
    $SeckillEndTime=db::get_value('sales_seckill_period', "{$c['time']}>StartTime and {$c['time']}<EndTime", 'EndTime', 'EndTime asc'); 
    if($SeckillEndTime>$c['time']){
    ?>
    <div class="sekill_box" plugins="mindex-1" effect="0-1" plugins_pos="0">
    	<div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][1][0]['Title']?></div>
        <div id="timer" class="timer" data-time="<?=date('Y/m/d H:i:s', $SeckillEndTime);?>">
        	<span class="hours">
                <i>0</i>
                <i>0</i>
            </span>
            <em></em>
			<span class="minutes">
                <i>0</i>
                <i>0</i>
            </span>
            <em></em>
            <span class="second">
                <i>0</i>
                <i>0</i>
            </span>
        </div>
        <div id="seckill_pro" class="con">
        	<ul>
           		<li>
					<?php
					$where="1 and s.StartTime<={$c['time']} and s.EndTime>{$c['time']}";
					$in_sales_pro=str::str_code(db::get_limit('sales_seckill s left join products p on s.ProId=p.ProId', $where.' and s.RemainderQty>0', "s.*, p.Name{$c['lang']}, p.PicPath_0, p.Price_0, p.Price_1, p.TotalRating, p.IsHot, p.Unit", 's.Price asc, if(s.MyOrder>0, if(s.MyOrder=999, 1000001, s.MyOrder), 1000000) asc, s.SId desc', 0, 6));
					foreach((array)$in_sales_pro as $k=>$v){
						$url=ly200::get_url($v, 'products');
						$img=ly200::get_size_img($v['PicPath_0'], '500x500');
                        $price_ary=cart::range_price_ext($v);
                        $price_0 = $price_ary[1];
						$name=$v['Name'.$c['lang']];
						$progress=ceil((1-$v['RemainderQty']/$v['Qty'])*100);
						echo ($k && $k%2==0)?'<div class="clear"></div></li><li>':'';
					?>
						<div class="item <?=$k%2==0?'fl':'fr'?>">
							<div class="img pic_box"><a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a></div>
							<div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
							<div class="price"><?=cart::iconv_price($v['Price'])?><?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?></div>
							<div class="progress"><span><?=$progress;?>% Claimed</span><i style="width:<?=$progress;?>%;"></i></div>
						</div>
					<?php }?>
                </li>
            </ul>
			<?php
			if($in_sales_pro){
				$num=count($in_sales_pro)/2;
				$num=ceil($num);
			?>
				<div class="tab">
					<?php for($i=0; $i<$num; ++$i){?>
						<a href="javascript:;"<?=$i?'':' class="cur"';?>></a>
					<?php }?>
				</div>
			<?php }?>
        </div>
    </div>
    <?php }?>
    <?php $url=$c['web_pack']['mindex'][2][0]['Link'];?>
    <div class="blank"></div>
    <div class="mid_ad" plugins="mindex-2" effect="0-1" plugins_pos="0">
    	<div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][2][0]['Title']?></div>
        <div class="img"><a href="<?=$url?$url:'javascript:;'?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][2][0]['Pic'];?>" /></a></div>
        <div class="brief" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][2][0]['SubTitle']?></div>
    </div>
    <div class="blank"></div>
    <div class="hot_box" plugins="mindex-3" effect="0-1" plugins_pos="0">
    	<div class="g_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title']?></div>
        <div class="con">
        	<?php
			$products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 6));
			foreach($products_list_row as $k => $v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
                $price_0 = $price_ary[1];
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
			?>
            <div class="item" <?=$k%2==0?'style="margin-left:0;"':''?>>
            	<div class="img pic_box">
                	<a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a>
					<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
                    <em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
                </div>
                <div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
                <div class="price">
                    <span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0],2);?></span></span>
                </div>
            </div>
            <?php }?>
            <div class="clear"></div>
        </div>
    </div>
    <div class="blank"></div>
</div>