<?php !isset($c) && exit();?>
<div class="wrapper homebg">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
            <li plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            <span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="htitle" plugins="mindex-0" effect="0-1" plugins_pos="0"><span plugins_mod="Title"><?=$c['web_pack']['mindex'][0][0]['Title'];?></span></div>
    <div class="home_box" plugins="mindex-1" effect="0-1" plugins_pos="0">
    	<div class="big_pro">
        	<div class="img"><a href="<?=$c['web_pack']['mindex'][1][0]['Link'];?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][0]['Title'];?>" /></a></div>
            <div class="proname"><a href="<?=$c['web_pack']['mindex'][1][0]['Link'];?>" plugins_mod="Title"><?=$c['web_pack']['mindex'][1][0]['Title'];?></a></div>
        </div>
    </div>
    <?php
	$products_list_row=str::str_code(db::get_limit('products', 'IsBestDeals=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 4));
	$len=count($products_list_row);
	foreach($products_list_row as $k=>$v){
		$url=ly200::get_url($v, 'products');
		$img=ly200::get_size_img($v['PicPath_0'], '500x500');
		$name=$v['Name'.$c['lang']];
		$price_ary=cart::range_price_ext($v);
		$price_0 = $price_ary[1];
		$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
		$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
		if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
	?>
        <?php if($k%2==0){?><div class="home_box clean"><?php }?>
		<div class="small_pro item fl">
			<div class="c">
				<div class="img pic_box">
					<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
					<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
					<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
				</div>
				<div class="proname"><a href="<?=$url;?>"><?=$name;?></a></div>
				<div class="price">
					<span><?=cart::iconv_price(0, 1);?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span> <?=$_SESSION['Currency']['Currency'];?></span>
				</div>
			</div>
		</div>
        <?php if(($k+1)%2==0 || $k==$len-1){?></div><?php }?>
    <?php }?>
    <div class="htitle" plugins="mindex-2" effect="0-1" plugins_pos="0"><span plugins_mod="Title"><?=$c['web_pack']['mindex'][2][0]['Title'];?></span></div>
	<?php
	$ad_ary=ly200::ad_custom(3, 0, '05');
	?>
    <div class="home_box" plugins="mindex-3" effect="0-1" plugins_pos="0">
    	<div class="big_pro">
        	<div class="img"><a href="<?=$c['web_pack']['mindex'][3][0]['Link'];?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][3][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][3][0]['Title'];?>" /></a></div>
            <div class="proname"><a href="<?=$c['web_pack']['mindex'][3][0]['Link'];?>" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title'];?></a></div>
        </div>
    </div>
    <?php
	$products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 4));
    $len=count($products_list_row);
	foreach($products_list_row as $k=>$v){
		$url=ly200::get_url($v, 'products');
		$img=ly200::get_size_img($v['PicPath_0'], '500x500');
		$name=$v['Name'.$c['lang']];
		$price_ary=cart::range_price_ext($v);
		$price_0 = $price_ary[1];
		$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
		$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
		if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
	?>
        <?php if($k%2==0){?><div class="home_box clean"><?php }?>
		<div class="small_pro item fl">
			<div class="c">
				<div class="img pic_box">
					<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
					<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
					<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
				</div>
				<div class="proname"><a href="<?=$url;?>"><?=$name;?></a></div>
				<div class="price">
					<span><?=cart::iconv_price(0, 1);?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span> <?=$_SESSION['Currency']['Currency'];?>
				</div>
			</div>
		</div>
        <?php if(($k+1)%2==0 || $k==$len-1){?></div><?php }?>
    <?php }?>
</div>