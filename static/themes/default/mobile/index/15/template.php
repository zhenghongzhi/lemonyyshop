<?php !isset($c) && exit();?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, 15);
            for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
            	<li><a href="<?=$url?$url:'javascript:;';?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="middle_ad">
	    <?php
		//$ad_ary=ly200::ad_custom(2, 0, 15);
		$j=0;
		for($i=0;$i<3;$i++){
		?>
	    	<div class="ban <?=$j==0 ? 'fir' : ''; ?>" plugins="mindex-<?=$i?>" effect="0-1" plugins_pos="0"><a href="<?=$c['web_pack']['mindex'][$i][0]['Link']?$c['web_pack']['mindex'][$i][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][$i][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][$i][0]['Title'];?>" /></a></div> 
		<?php 
			$j++;
		}
		?>
	</div>
	<div class="swipe_products p_list swipe_products_0" plugins="mindex-3" effect="0-1" plugins_pos="0">
		<?php //$indcon=ly200::web_settings(4, 15); ?>
		<div class="top_title_tips" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][3][0]['SubTitle']?str::format($c['web_pack']['mindex'][3][0]['SubTitle']) : 'The latest featured products';?></div>
		<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title']?str::format($c['web_pack']['mindex'][3][0]['Title']) : 'FEATURED PRODUCTS';?></h2>
        <div class="container" id="swipe_products_0">
        	<ul>
        	<?php
        	$products_list_row=str::str_code(db::get_limit('products', 'IsHot=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 15));
			$len=count($products_list_row);
			foreach($products_list_row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$price_0=$price_ary[1];
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
				$rating=($v['IsDefaultReview'] && $v['DefaultReviewRating'])?(int)$v['DefaultReviewRating']:(int)$v['Rating'];
				$total_rating=($v['IsDefaultReview'] && $v['DefaultReviewTotalRating'])?$v['DefaultReviewTotalRating']:$v['TotalRating'];
				$rating || $rating=5;
			  	?>
				<li class="item fl">
					<div class="img pic_box">
						<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
						<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
						<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
					</div>
					<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
					<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
					<div class="price">
						<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span>
						<?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?>
					</div>
				</li>
            <?php } ?>
            </ul>
        </div>        
	</div>
	<div class="middle_ad2" plugins="mindex-4" effect="0-1" plugins_pos="0">
	    <a href="<?=$c['web_pack']['mindex'][4][0]['Link']?$c['web_pack']['mindex'][4][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][4][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][4][0]['Title'];?>" /></a>
	</div>
	<div class="swipe_products p_list swipe_products_1" plugins="mindex-5" effect="0-1" plugins_pos="0">
		<?php //$indcon=ly200::web_settings(5, 15); ?>
		<div class="top_title_tips" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][5][0]['SubTitle']?str::format($c['web_pack']['mindex'][5][0]['SubTitle']):'Best selling product';?></div>
		<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][5][0]['Title']?str::format($c['web_pack']['mindex'][5][0]['Title']):'Best Sellers';?></h2>
        <div class="container" id="swipe_products_1">
        	<ul>
        	<?php
        	$products_list_row=str::str_code(db::get_limit('products', 'IsBestDeals=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 15));
			$len=count($products_list_row);
			foreach($products_list_row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$price_0=$price_ary[1];
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
				$rating=($v['IsDefaultReview'] && $v['DefaultReviewRating'])?(int)$v['DefaultReviewRating']:(int)$v['Rating'];
				$total_rating=($v['IsDefaultReview'] && $v['DefaultReviewTotalRating'])?$v['DefaultReviewTotalRating']:$v['TotalRating'];
				$rating || $rating=5;
			  	?>
				<li class="item fl">
					<div class="img pic_box">
						<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
						<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
						<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
					</div>
					<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
					<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
					<div class="price">
						<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span>
						<?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?>
					</div>
				</li>
            <?php } ?>
            </ul>
        </div>        
	</div>
	<div class="ind_bg"></div>
    <div class="products_list p_list" plugins="mindex-6" effect="0-1" plugins_pos="0">
    	<?php //$indcon=ly200::web_settings(6, 15); ?>
		<div class="top_title_tips" plugins_mod="SubTitle"><?=$c['web_pack']['mindex'][6][0]['SubTitle']?str::format($c['web_pack']['mindex'][6][0]['SubTitle']):'Recently launched products';?></div>
		<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][6][0]['Title']?str::format($c['web_pack']['mindex'][6][0]['Title']):'New Product';?></h2>
        <div class="container">
        	<?php
        	$products_list_row=str::str_code(db::get_limit('products', 'IsNew=1 and IsIndex=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 6));
			$len=count($products_list_row);
			foreach($products_list_row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$img=ly200::get_size_img($v['PicPath_0'], '500x500');
				$name=$v['Name'.$c['lang']];
				$price_ary=cart::range_price_ext($v);
				$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
				$price_0=$price_ary[1];
				$promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
				if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
				$rating=($v['IsDefaultReview'] && $v['DefaultReviewRating'])?(int)$v['DefaultReviewRating']:(int)$v['Rating'];
				$total_rating=($v['IsDefaultReview'] && $v['DefaultReviewTotalRating'])?$v['DefaultReviewTotalRating']:$v['TotalRating'];
				$rating || $rating=5;
			  	?>
				<div class="item fl">
					<div class="img pic_box">
						<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
						<?php if($is_promition && $promotion_discount){?><em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em><em class="icon_discount_foot DiscountBorderColor"></em><?php }?>
						<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
					</div>
					<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
					<div class="price">
						<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></span>
						<?php if($price_ary[2]){?><del><?=cart::iconv_price($price_0);?></del><?php }?>
					</div>
					<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
				</div>
				<?php if($k%2==1){ ?><div class="clear"></div><?php } ?>
            <?php } ?>
        </div>
    </div> 
</div>
