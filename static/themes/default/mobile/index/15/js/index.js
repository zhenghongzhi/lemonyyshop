/*
 * 广州联雅网络
 */

$(function(){
	var ospan=$('.banner .btn span');
	new Swipe(document.getElementById('banner_box'), {
		speed:500,
		auto:5000,
		callback: function(){
			ospan.removeClass("on").eq(this.index).addClass("on");
		}
	});
	var swipe_products_0 = new Swipe(document.getElementById('swipe_products_0'), {
		speed:500,
		auto:100000
	});
	swipe_products_0.slide(get_mid('#swipe_products_0 .item'), 0);
	var swipe_products_1 = new Swipe(document.getElementById('swipe_products_1'), {
		speed:500,
		auto:100000
	});
	swipe_products_1.slide(get_mid('#swipe_products_1 .item'), 0);

	function get_mid(item){
		return parseInt($(item).length/2);
	}
});
