<?php !isset($c) && exit();?>
<?php
$article_category_row=str::str_code(db::get_limit('article_category', 'CateId not in(1)', "CateId, Category{$c['lang']}", 'CateId asc', 0, 2));
$article_row=str::str_code(db::get_all('article', 'CateId not in(1)', "AId, CateId, Title{$c['lang']}"));
$article_ary=array();
foreach($article_row as $v){
	$article_ary[$v['CateId']][]=$v;
}
?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
            for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
           	<li plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="banner clean" plugins="mindex-0" effect="0-1" plugins_pos="0"><a href="<?=$c['web_pack']['mindex'][0][0]['Link']?$c['web_pack']['mindex'][0][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][0][0]['Title'];?>" /></a></div>
	<div plugins="mindex-1" effect="0-1">    
        <div class="banner clean">
            <div class="element" plugins_pos="0"><a href="<?=$c['web_pack']['mindex'][1][0]['Link']?$c['web_pack']['mindex'][1][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][0]['Title'];?>" /></a></div>
            <div class="element" plugins_pos="1"><a href="<?=$c['web_pack']['mindex'][1][1]['Link']?$c['web_pack']['mindex'][1][1]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][1]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][1]['Title'];?>" /></a></div>
        </div>
        <div class="banner clean">
            <div class="element" plugins_pos="2"><a href="<?=$c['web_pack']['mindex'][1][2]['Link']?$c['web_pack']['mindex'][1][2]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][2]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][2]['Title'];?>" /></a></div>
            <div class="element" plugins_pos="3"><a href="<?=$c['web_pack']['mindex'][1][3]['Link']?$c['web_pack']['mindex'][1][3]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][3]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][3]['Title'];?>" /></a></div>
        </div>
    </div>
    <?php foreach((array)$article_category_row as $k=>$v){?>
		<section class="h-container<?=$k==0?' on':'';?>">
			<h2 class="clean"><?=$v['Category'.$c['lang']];?></h2>
			<div class="list clean" style="display:<?=$k==0?'block':'none';?>;">
				<?php foreach((array)$article_ary[$v['CateId']] as $k2=>$v2){?>
					<div class="item fl"><a href="<?=$c['mobile_url'].ly200::get_url($v2, 'article');?>"><?=$v2['Title'.$c['lang']];?></a></div>
					<?php if(($k2+1)%2==0 || $k2==count($article_ary[$v['CateId']])-1){?><p class="line"></p><?php }?>
				<?php }?>
			</div>
		</section>
    <?php }?>
</div>