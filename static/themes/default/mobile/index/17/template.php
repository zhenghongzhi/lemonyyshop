<?php !isset($c) && exit();?>
<div class="wrapper">
	<div class="banner clean" id="banner_box" plugins="mbanner-0" effect="0-1">
    	<ul>
            <?php
			//$ad_ary=ly200::ad_custom(1, 0, 17);
            for($i=$sum=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
				$sum++;
            ?>
            	<li><a href="<?=$url?$url:'javascript:;';?>"><img src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
        <div class="btn">
        	<?php for($i=0; $i<$sum; ++$i){?>
            	<span class="<?=$i==0?'on':'';?>"></span>
            <?php }?>
        </div>
    </div>
    <div class="middle_ad" plugins="mindex-0" effect="0-1" plugins_pos="0">
	    <div class="ban container"><a href="<?=$c['web_pack']['mindex'][0][0]['Link']?$c['web_pack']['mindex'][0][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][0][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][0][0]['Title'];?>" /></a></div> 
	</div>
	<div class="middle_ad2" plugins="mindex-1" effect="0-1">
		<?php
		//$ad_ary=ly200::ad_custom(3, 0, 17);
		for($i=$m=0;$i<2;$i++){
		?>
	    	<div class="mid_ban container" plugins_pos="<?=$i?>">
	    		<a href="<?=$c['web_pack']['mindex'][1][$i]['Link']?$c['web_pack']['mindex'][1][$i]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][1][$i]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][1][$i]['Title'];?>" /></a>
	    	</div>
		<?php 
			$m++;
			}
		?>
	</div>
	<div class="products_list p_list index_products_0">
		<div class="index_category_0">
			<?php 
			$index_category_row=str::str_code(db::get_limit('products_category','IsIndex=1 and IsSoldOut=0','CateId,Category'.$c['lang'],$c['my_order'].'CateId asc',0,10));
			foreach((array)$index_category_row as $k=>$v){
			?>
			<a href="javascript:;" class="cate_item" data-cateid="<?=$v['CateId'];?>"><?=$v['Category'.$c['lang']];?></a>
			<?php }?>
		</div>
		<div id="index_products_list" class="container"></div>
	</div>
	<?php 
		$seckill_used=db::get_value('plugins','ClassName="seckill"','IsUsed');
    	$seckill_list_row=str::str_code(db::get_limit('sales_seckill s left join products p on s.ProId=p.ProId', "s.RemainderQty>0 and {$c['time']} between s.StartTime and s.EndTime", "s.*,p.Name{$c['lang']}, p.PicPath_0, p.Price_0, p.Price_1,p.IsDefaultReview,p.DefaultReviewRating,p.Rating,p.DefaultReviewTotalRating,p.TotalRating", 'if(s.MyOrder>0, if(s.MyOrder=999, 1000001, s.MyOrder), 1000000) asc, s.SId desc', 0, 10));
		$len=count($seckill_list_row);
		
		if($seckill_used && $len){
	?>
	<div class="weekly_hot_sales" plugins="mindex-2" effect="0-1" plugins_pos="0">
	    <div class="weekly_img"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][2][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][2][0]['Title'];?>" /></div> 
		<div class="hot_sales_container">
			<?php //$indcon=ly200::web_settings(6, 17); ?>
			<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][2][0]['Title']?str::format($c['web_pack']['mindex'][2][0]['Title']):$c['lang_pack']['new_arrival'];?></h2>
			<div class="swipe_products swipe_products_0">
				<div class="container" id="swipe_products_0">
		        	<ul>
		        	<?php
					foreach($seckill_list_row as $k=>$v){
						$url=ly200::get_url($v, 'seckill');
						$price_ary=cart::range_price_ext($v);
                        $price_0 = $price_ary[1];
						$img=ly200::get_size_img($v['PicPath_0'], '500x500');
						$name=$v['Name'.$c['lang']];
						$rating=($v['IsDefaultReview'] && $v['DefaultReviewRating'])?(int)$v['DefaultReviewRating']:(int)$v['Rating'];
						$total_rating=($v['IsDefaultReview'] && $v['DefaultReviewTotalRating'])?$v['DefaultReviewTotalRating']:$v['TotalRating'];
					  	?>
						<li class="item fl">
							<div class="img pic_box">
								<a href="<?=$url;?>"><img src="<?=$img;?>" /><span></span></a>
							</div>
							<a href="<?=$url;?>" class="proname" title="<?=$name;?>"><?=$name;?></a>
							<?php if($rating){?><div class="star"><?=html::mobile_review_star($rating);?></div><?php }?>
							<div class="price">
								<span><?=cart::iconv_price(0,1); ?><span class="price_data" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($v['Price'],2);?></span></span>
								<?php if($price_ary[2]){?>
									<del><span class="price_data"><?=cart::iconv_price($price_0);?></span></del>
								<?php }?>
							</div>
							<?php
								$m=(int)@date('m', $v['EndTime'])-1;
								$d=date("Y, $m, j, G, i, s", $v['EndTime']);
							?>
								<div class="time"><span id="flashsale_<?=$v['ProId'];?>" endTime="<?=date('Y/m/d H:i:s', $v['EndTime']);?>" proId="<?=$v['ProId'];?>"></span></div>
						</li>
		            <?php } ?>
		            </ul>
		        </div>
		        <a href="javascript:;" rel="nofollow" class="prev"></a>
		        <a href="javascript:;" rel="nofollow" class="next"></a>
			</div>
		</div>
	</div>
	<?php }?>
	<?php if($c['FunVersion']>1 && in_array('blog', $c['plugins']['Used']) && db::get_row_count('blog')){ ?>
		<div class="index_blog" plugins="mindex-3" effect="0-1" plugins_pos="0">
			<?php //$indcon=ly200::web_settings(7, 17); ?>
			<h2 class="top_title" plugins_mod="Title"><?=$c['web_pack']['mindex'][3][0]['Title'] ? str::format($c['web_pack']['mindex'][3][0]['Title']) : 'Blog Update';?></h2>
	    	<div class="blog_list b_list">
	    		<?php 
	    			$blog_row=str::str_code(db::get_limit('blog','IsHot=1','*',$c['my_order'].'AId desc',0,2));
	    			foreach((array)$blog_row as $k=>$v){
	    				$url=ly200::get_url($v,'blog');
	    				$title=$v['Title'];
	    				$pic=$v['PicPath'];
	    				$brief=$v['BriefDescription'];
	    		?>
		    	<div class="blog_item">
		    		<?php if(is_file($c['root_path'].$pic)){?>
		    			<a href="<?=$url;?>" class="blog_img fl"><img src="<?=$pic;?>"></a>
		    		<?php }?>
		    		<div class="blog_content fr">
		    			<div class="blog_time"><span><?=date('M,d',$v['AccTime']);?></span></div>
		    			<a href="<?=$url;?>" title="<?=$title;?>" class="blog_title"><?=$title;?></a>
		    			<div class="blog_brief"><?=$brief;?></div>
		    		</div>
		    		<div class="clear"></div>
		    	</div>
		    	<?php }?>
	    	</div>
		</div>
	<?php } ?>
	<div class="bottom_ad" plugins="mindex-4" effect="0-1" plugins_pos="0">
	    <div class="bot_ban"><a href="<?=$c['web_pack']['mindex'][4][0]['Link']?$c['web_pack']['mindex'][4][0]['Link']:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mindex'][4][0]['Pic'];?>" alt="<?=$c['web_pack']['mindex'][4][0]['Title'];?>" /></a></div> 
	</div>
</div>
