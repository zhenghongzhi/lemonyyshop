/*
 * 广州联雅网络
 */

$(function(){
	var ospan=$('.banner .btn span');
	new Swipe(document.getElementById('banner_box'), {
		speed:500,
		auto:5000,
		callback: function(){
			ospan.removeClass("on").eq(this.index).addClass("on");
		}
	});
	
	var swipe_products_0 = new Swipe(document.getElementById('swipe_products_0'), {
		speed:500,
		auto:100000,
		callback: function(){
			$('#swipe_products_0').animate({height:$('#swipe_products_0 .item').eq(this.index).innerHeight()},'100');
		}
	});
	$('.swipe_products_0 .prev').click(function(){
		swipe_products_0.prev();
	});
	$('.swipe_products_0 .next').click(function(){
		swipe_products_0.next();
	});

	function get_mid(item){
		return parseInt($(item).length/2);
	}

	$('.index_category_0').find('.cate_item').click(function(){
		var CateId=parseInt($(this).attr('data-cateid'));
		$(this).addClass('on').siblings('.cate_item').removeClass('on');
		$.post('/ajax/index_products_list.html',{'CateId':CateId},function(result){
			$('html').seckillPrice();
			$('#index_products_list').html(result);
		},'html');
	});

	$('.index_category_0').find('.cate_item').eq(0).click();

	var seckill_timer=new Array();
	for(i in seckill_timer){
		clearInterval(seckill_timer[i]);//清除计时器，防止时间乱跳
	}
	$("#swipe_products_0 ul .time").each(function(){
		var obj=$(this).find("span"),
			time=new Date(),
			proid=obj.attr("proId");
		obj.genTimer({
			beginTime: ueeshop_config.date,
			targetTime: obj.attr("endTime"),
			unitWord:{hours:"<span>Hours</span>", minutes:"<span>Mins</span>", seconds:"<span>Secs</span>"},
			type:'all_tags',
			callback: function(e){
				$('#flashsale_'+proid).html(e);
			}
		});
	});
});
