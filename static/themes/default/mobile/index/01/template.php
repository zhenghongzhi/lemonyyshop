<?php !isset($c) && exit();?>
<?php
$category_row=str::str_code(db::get_all('products_category', 'UId="0," and IsSoldOut=0', "CateId, UId, Category{$c['lang']}, PicPath", $c['my_order'].'CateId asc'));
?>
<div class="wrapper">
	<div class="banner" id="banner_box" plugins="mbanner-0" effect="0-1">
        <ul>
            <?php
            for($i=0; $i<5; ++$i){
				if(!is_file($c['root_path'].$c['web_pack']['mbanner'][0][$i]['Pic'])) continue;
				$url=$c['web_pack']['mbanner'][0][$i]['Link'];
            ?>
            <li plugins_pos="<?=$i?>"><a href="<?=$url?$url:'javascript:;';?>"><img plugins_mod="Pic" src="<?=$c['web_pack']['mbanner'][0][$i]['Pic'];?>" alt="<?=$c['web_pack']['mbanner'][0][$i]['Title'];?>" /></a></li>
            <?php }?>
        </ul>
    </div>
    <div class="home_list">
    	<?php foreach((array)$category_row as $k=>$v){?>
			<div class="item clean">
				<div class="img fl pic_box">
					<?php if($v['PicPath'] && is_file($c['root_path'].$v['PicPath'])){?>
					<img src="<?=$v['PicPath'];?>" /><span></span>
					<?php }?>
				</div>
				<div class="name fl"><a href="<?=ly200::get_url($v, 'products_category');?>"><?=$v['Category'.$c['lang']];?></a></div>
			</div>
        <?php }?>
    </div>
</div>