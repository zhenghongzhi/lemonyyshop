<?php !isset($c) && exit();?>
<?php
$login_module_ary=array('checkout', 'buynow', 'quick', 'ajax_get_coupon_info', 'placeorder', 'complete', 'payment', 'success');	//需要登录的模块列表
$un_login_module_ary=array('list', 'additem', 'add_success', 'modify', 'remove', 'address', 'set_no_login_address', 'get_default_country', 'get_shipping_methods', 'get_excheckout_country', 'excheckout', 'offline_payment');	//不需要登录的模块列表

if((int)$_SESSION['User']['UserId']){	//已登录
	$module_ary=array_merge($un_login_module_ary, $login_module_ary);
	$cart_where="UserId='{$_SESSION['User']['UserId']}'";
}else{	//未登录
	if($c['orders']['mode']==0){	//必须登录方可下订单
		@in_array($a, $login_module_ary) && js::location("{$c['mobile_url']}/cart/?&jump_url=".urlencode($_SERVER['REQUEST_URI']));	//访问需要登录的模块但用户并未登录  .'?'.ly200::query_string()
		$module_ary=$un_login_module_ary;
	}else{	//未登录也可以下订单
		$module_ary=@array_merge($un_login_module_ary, $login_module_ary);
	}

	$cart_where="SessionId='{$c['session_id']}'";
}

($a=='' || !in_array($a, $module_ary)) && $a=$module_ary[0];

$do_action=$a;
if($do_action && method_exists(cart, $do_action)){
	eval("cart::{$do_action}();");
}

//快捷支付模板
if($a=='excheckout'){
	$d_ary=array('SetExpressCheckout', 'ReviewOrder', 'APIError', 'checkout', 'DoExpressCheckoutPayment', 'GetExpressCheckoutDetails', 'cancel');
	$d=$_GET['d']?$_GET['d']:$_POST['d'];
	!in_array($d, $d_ary) && $d=$d_ary[0];
	
	include("{$c['root_path']}static/themes/default/gateway/paypal_excheckout/{$d}.php");
	exit();
}

$cutArr=str::json_data(db::get_value('config', "GroupId='cart' and Variable='discount'", 'Value'), 'decode');
$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');//模板风格色调的数据
$style_data=str::json_data($StyleData, 'decode');

if($a=='list' || $a=='checkout' || $a=='buynow'){//自动更新产品信息
	cart::open_update_cart();
}

$file_name=$a;
$a=='buynow' && $file_name='checkout';//BuyNow页面归纳到Checkout页面
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="noindex,nofollow" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection" />
<?php
echo ly200::seo_meta();
include("{$c['mobile']['theme_path']}inc/resource.php");
//echo ly200::load_static('/static/themes/default/css/user.css', "{$c['mobile']['tpl_dir']}js/cart.js", "{$c['mobile']['tpl_dir']}js/user.js");
echo ly200::load_static("{$c['mobile']['tpl_dir']}js/cart.js", "{$c['mobile']['tpl_dir']}js/user.js");

//第三方统计
$facebook_pixel = in_array('facebook_pixel', $c['plugins']['Used'])?1:0;
$google_pixel = in_array('google_pixel', $c['plugins']['Used'])?1:0;
if($facebook_pixel || $google_pixel){
	if ($a == 'payment' || $a == 'complete') {
		if ($facebook_pixel && (int)$order_row['CutPay'] == 0) {
            //Facebook Pixel
            //When a person enters the checkout flow prior to completing the checkout flow.
            $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            fbq('track', 'InitiateCheckout', {
                content_type: 'product',
                content_ids: ['0'],
                value: '<?=$total_price;?>',
                currency: '<?=$order_row['Currency'];?>'
            });
            </script>
            <!-- End Facebook Pixel Code -->
<?php
		}
	}
    if ($a == 'checkout' || $a == 'buynow' || $a == 'quick') {
		if ($facebook_pixel) {
		    //When a person enters the checkout flow prior to completing the checkout flow.
		    $iconv_total_price = cart::cart_total_price(($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 1);
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            $.fn.fbq_checkout=function(){
                fbq('track', 'InitiateCheckout', {
                    content_ids: ['0'],
                    value:'<?=$iconv_total_price;?>',
                    currency:'<?=$_SESSION['Currency']['Currency'];?>'
                });
            }
            </script>
            <!-- End Facebook Pixel Code -->
<?php
		}
    } elseif ($a == 'complete') {
		if ($facebook_pixel) {
            //Facebook Pixel
		    //When payment information is added in the checkout flow.
            $OId = trim($_GET['OId']);
            $order_row = db::get_one('orders', "OId='$OId'");	
            $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
            $Number_ary = array();
            $order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'p.Prefix,p.Number', 'o.LId asc');
            foreach ($order_list_row as $k => $v) {
                $v['Number'] != '' && $Number_ary[$k] = $v['Prefix'] . $v['Number'];
            }
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            fbq('track', 'AddPaymentInfo', {
                content_name: 'Shipping Cart',//关键词
                content_type: 'product',//产品类型为产品
                content_ids: ['<?=@implode("','", $Number_ary);?>'],//产品ID
                value:'<?=$total_price;?>',
                currency:'<?=$order_row['Currency'];?>'
            });
            </script>
            <!-- End Facebook Pixel Code -->
<?php
		}
	} elseif ($a == 'success') {
		//When a purchase is made or checkout flow is completed.
		$OId = trim($_GET['OId']);
		$order_row = db::get_one('orders', "OId='$OId'");	
		if ((int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
            //仅有付款成功才执行
			$total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
			$Number_ary = array();
			$order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'p.Prefix,p.Number,o.*', 'o.LId asc');
			foreach ($order_list_row as $k => $v) {
				$v['Number'] != '' && $Number_ary[$k] = addslashes(htmlspecialchars_decode($v['Prefix'] . $v['Number']));
			}
			if ($facebook_pixel) {
?>
                <!-- Facebook Pixel Code -->
                <script type="text/javascript">
                fbq('track', 'Purchase', {
                    content_type: 'product',//产品类型为产品
                    content_ids: ['<?=@implode("','", $Number_ary);?>'],//产品ID
                    value: <?=$total_price;?>,//订单总金额
                    currency: '<?=$order_row['Currency'];?>'//货币类型
                });
                </script>
                <!-- End Facebook Pixel Code -->
<?php
			}
			if ($google_pixel) {
?>
                <script type="text/javascript">
                gtag('event', 'purchase', {
                    "transaction_id": "<?=$order_row['OId'];?>",
                    "affiliation": "<?=addslashes($c['config']['global']['SiteName']);?>",
                    "value": <?=$total_price;?>,
                    "currency": "<?=$order_row['Currency'];?>",
                    "tax": 0,
                    "shipping": <?=cart::iconv_price($order_row['ShippingPrice'], 2, $_SESSION['Currency']['Currency'], 0);?>,
                    "items": [
                        <?php
                        foreach ((array)$order_list_row as $k => $v) {
                        echo $k?',':'';
                        ?>
                            {
                                "id": "<?=$Number_ary[$k];?>",
                                "name": "<?=addslashes($v['Name']);?>",
                                "list_position": <?=$k+1;?>,
                                "quantity": <?=$v['Qty'];?>,
                                "price": '<?=cart::iconv_price($v['Price']+$v['PropertyPrice'], 2, $_SESSION['Currency']['Currency'], 0);?>'
                            }
                        <?php }?>
                    ]
                });
                </script>
<?php
			}
		}
	}
}

//统计结尾
if (($a == 'payment' || $a == 'complete') && (int)$order_row['CutPay'] == 0) {
    db::update('orders', "OId='{$order_row['OId']}'", array('CutPay'=>1));
}
if ($a == 'success' && (int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
    db::update('orders', "OId='{$order_row['OId']}'", array('CutSuccess'=>1));
}
?>
</head>

<body  class="lang<?=$c['lang'];?>">
<?php
include("{$c['mobile']['theme_path']}inc/header.php");
$file_name!='checkout' && $file_name!='quick' && include("{$c['mobile']['theme_path']}header/{$c['mobile']['HeaderTpl']}/header.php");
?>
<div class="wrapper">
	<?php
	if($file_name=='address'){
		include("{$c['theme_path']}user/module/{$file_name}.php");
	}elseif($file_name=='payment'){//付款页面
		include("{$c['default_path']}cart/module/{$file_name}.php");
	}else{
		include("module/{$file_name}.php");
	}
	echo ly200::out_put_third_code();
	?>
</div>
<?php
//Analytics统计
if ($a == 'checkout' || $a == 'buynow') {
    //Checkout页面
    $analytics = 4;
} elseif ($a == 'quick') {
    //快捷支付
    $analytics = 3;
} elseif (($a == 'payment' || $a == 'complete') && (int)$order_row['CutPay'] == 0) {
    //Place an Order 生成订单
    $analytics = 5;
} elseif ($a == 'success' && (int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
    //付款成功页面
    $analytics = 6;
}
if ($analytics > 0) {
    echo '<script type="text/javascript">$(function(){ if($.isFunction(analytics_click_statistics)){ analytics_click_statistics('.$analytics.') } });</script>';
}
?>
</body>
</html>