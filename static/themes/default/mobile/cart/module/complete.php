<?php !isset($c) && exit(); ?>
<?php
$OId = trim($_GET['OId']);
//add by jay start
$d_ary = array('view', 'balance');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];
//add by jay end

!$OId && js::location('/cart/', '', '.top');
$order_row = db::get_one('orders', "OId='$OId'");
if (!$_GET['utm_nooverride']) {//除了刚下单，都要例行检查
    //订单不存在
    (!$order_row) && js::location('/', '', '.top');
    //会员订单，未登录不允许查看
    ((int)$order_row['UserId'] && !(int)$_SESSION['User']['UserId']) && js::location("/account/?JumpUrl=" . urlencode("/cart/complete/{$OId}.html"), '', '.top');
    //当前会员非订单会员
    ((int)$order_row['UserId'] && (int)$order_row['UserId'] != (int)$_SESSION['User']['UserId']) && js::location("/account/", '', '.top');
    //会员订单发货后状态在会员中心查询
    ((int)$_SESSION['User']['UserId'] && (int)$order_row['OrderStatus'] > 4) && js::location("/account/orders/view{$OId}.html", '', '.top');
    //订单总金额低于等于0
    $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
    if ($total_price <= 0 && (int)$order_row['OrderStatus'] < 4) {
        if ((int)$order_row['OrderStatus'] == 1) {//更改为“待确认”状态
            $UserName = (int)$_SESSION['User']['UserId'] ? $_SESSION['User']['FirstName'] . ' ' . $_SESSION['User']['LastName'] : 'Tourist';
            $Log = 'Update order status from ' . $c['orders']['status'][1] . ' to ' . $c['orders']['status'][2];
            db::update('orders', "OId='$OId'", array('OrderStatus' => 2));
            orders::orders_log($order_row['UserId'], $UserName, $order_row['OrderId'], 2, $Log);
        }
        js::location("/cart/success/{$OId}.html");
    }

    $payment_row = db::get_one('payment', "PId='{$order_row['PId']}'");
    !$payment_row && js::location("/cart/success/{$OId}.html");//支付方式已关闭

    if ($payment_row['IsOnline'] == 1 && (int)$order_row['OrderStatus'] < 4) {//支付方式为在线付款，并且状态为未付款
        include("{$c['default_path']}cart/module/payment.php");
        exit();
    }
} else {
    $payment_row = db::get_one('payment', "PId='{$order_row['PId']}' and IsUsed=1");
    !$payment_row && js::location("/cart/success/{$OId}.html");//支付方式已关闭
}
$payOffline = ($payment_row['IsOnline'] != 1 && $order_row['OrderStatus'] != 1 && $order_row['OrderStatus'] != 5 && $order_row['OrderStatus'] != 3) ? 0 : 1;
?>
<style type="text/css">
    .checkout_shipping .list li.current .icon > i, .checkout_payment .payment_row.current .icon > i {
        background-color: <?=$style_data['FontColor'];?>;
        border-color: <?=$style_data['FontColor'];?>;
    }
</style>
<script type="text/javascript">$(function () {
        cart_obj.complete()
    });</script>
<div id="cart">
    <?= html::mobile_cart_step(1); ?>

    <?php
    if ($d == 'view') {
        ?>

        <?php
        /************************************************** Paypal Checkout Start **************************************************/
    if ($order_row['OrderStatus'] == 1 && $order_row['PId'] == 2) {
        //相关费用
        $Symbol = db::get_value('currency', "Currency='{$order_row['Currency']}'", 'Symbol');//货币符号
        $ProductPrice = orders::orders_product_price($order_row, 1);//产品总价
        $UserDiscount = ($order_row['UserDiscount'] > 0 && $order_row['UserDiscount'] < 100) ? $order_row['UserDiscount'] : 100;
        $_price = $ProductPrice;
        $_price -= $ProductPrice * (1 - (100 - $order_row['Discount']) / 100);//折扣
        $_price -= $ProductPrice * (1 - $UserDiscount / 100);//会员折扣
        $_price -= $ProductPrice * $order_row['CouponDiscount'];//优惠券折扣
        $DiscountPrice = $ProductPrice - $_price;
        $SaleDiscountPrice = cart::iconv_price($order_row['DiscountPrice'], 2, $order_row['Currency'], 0);
        //快递选择
        $IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
        $shipping_mothed_ary = orders::orders_confirm_shipping_method($order_row);
        if (!$shipping_mothed_ary['info'][1]) {//购物车没有默认海外仓追加隐藏选项
            $shipping_mothed_ary['info'][1] = array();
        }
        $oversea_count = count($shipping_mothed_ary['info']);
        if ((int)$c['config']['global']['Overseas'] == 0 || count($c['config']['Overseas']) < 2 || count($oversea_id_ary) < 2) {//关闭海外仓功能 或者 仅有一个海外仓选项
            echo '<style type="text/css">.checkout_shipping .shipping:first-child .title{display:none;}</style>';
        }
        ?>
        <script type="text/javascript">
            var address_perfect = 0;
            var address_perfect_aid = 0;
            $(document).ready(function () {
                cart_obj.cart_checkout();
                $('.checkout_shipping .shipping').each(function () {
                    $(this).find('.shipping_method_list li:eq(0)').click(); //默认点击第一个选项
                });
                $('.checkout_payment .payment_row').eq(0).click();
            });
        </script>
        <div class="success">
            <div class="success_info">
                <div class="order_info">
                    <div class="checkout_error_tips hide" data-country="<?= $order_row['ShippingCountry']; ?>"
                         data-email="<?= $c['config']['global']['AdminEmail']; ?>"></div>
                    <div class="checkout_box checkout_address ui_border_b">
                        <div class="box_title"><?= $c['lang_pack']['cart']['address']; ?></div>
                        <div class="box_content">
                            <div class="address_default item clearfix">
                                <p class="clearfix">
                                    <strong><?= $order_row['ShippingFirstName'] . ' ' . $order_row['ShippingLastName']; ?></strong>
                                </p>
                                <p class="address_line"><?= $order_row['ShippingAddressLine1'] . ' ' . ($order_row['ShippingAddressLine2'] ? $order_row['ShippingAddressLine2'] . ' ' : ''); ?></p>
                                <p><?= $order_row['ShippingCity'] . ' ' . ($order_row['ShippingStateName'] ? $order_row['ShippingStateName'] : $order_row['ShippingState']) . ' ' . $order_row['ShippingCountry'] . ' (' . $order_row['ShippingZipCode'] . ')'; ?></p>
                                <?= $order_row['ShippingPhoneNumber'] ? '<p>' . $order_row['ShippingCountryCode'] . ' ' . $order_row['ShippingPhoneNumber'] . '</p>' : ''; ?>
                            </div>
                            <div id="addressInfo" style="display:none;"></div>
                        </div>
                    </div>
                    <?php if ($order_row['ShippingPhoneNumber'] == '') { ?>
                        <div class="checkout_divide ui_border_b"></div>
                        <div class="checkout_box checkout_phone ui_border_b">
                            <div class="box_title"><?= $c['lang_pack']['cart']['shipPhone']; ?></div>
                            <div class="box_content">
                                <div class="phone_input clearfix"><input type="text" name="phoneCode"
                                                                         class="box_input"
                                                                         placeholder="<?= $c['lang_pack']['cart']['phoneTips']; ?>"
                                                                         autocomplete="off" notnull/></div>
                                <p class="phone_error"></p>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="checkout_divide ui_border_b"></div>
                    <div class="checkout_box checkout_shipping ui_border_b">
                        <div class="box_title<?= $oversea_count > 1 ? ' ui_border_b' : ''; ?>"><?= $c['lang_pack']['cart']['shipmethod']; ?></div>
                        <div class="box_content">
                            <?php foreach ((array)$shipping_mothed_ary['info'] as $OvId => $v) { ?>
                                <div class="shipping<?= ($NotDefualtOvId && (int)$OvId == 1) ? ' hide' : ''; ?>"
                                     data-id="<?= $OvId; ?>">
                                    <div class="title">
                                        <strong><?= $c['config']['Overseas'][$OvId]['Name' . $c['lang']]; ?></strong>
                                        <div class="shipping_info"><span class="name"></span><span
                                                    class="price"></span></div>
                                        <i class="icon_shipping_title"></i>
                                    </div>
                                    <div class="list">
                                        <ul class="shipping_method_list clearfix"></ul>
                                        <?php if ($IsInsurance) { ?>
                                            <div class="checkbox_wrapper insurance">
                                                <div class="checkbox_input">
                                                    <input type="checkbox" name="_shipping_method_insurance"
                                                           class="checkbox_check shipping_insurance"
                                                           id="shipping_insurance_<?= $v; ?>" value="1" checked/>
                                                </div>
                                                <label class="checkbox_label" for="shipping_insurance_<?= $v; ?>">
                                                    <span><?= $c['lang_pack']['cart']['add_insur']; ?></span>
                                                    <span class="price"><?= $_SESSION['Currency']['Symbol']; ?>
                                                        <em></em></span>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="checkout_divide ui_border_b hide"></div>
                    <div class="checkout_box checkout_payment ui_border_b hide">
                        <div class="box_title"><?= $c['lang_pack']['cart']['paymethod']; ?></div>
                        <div class="box_content">
                            <?php
                            $payment_row = db::get_one('payment', 'IsUsed=1 and PId=2');
                            $payment_name = $payment_row['Name' . $c['lang']] ? $payment_row['Name' . $c['lang']] : $payment_row['Name_en'];
                            ?>
                            <div class="payment_row clean"
                                 min="<?= cart::iconv_price($payment_row['MinPrice'], 2, '', 0); ?>"
                                 max="<?= cart::iconv_price($payment_row['MaxPrice'], 2, '', 0); ?>"
                                 pid="<?= $payment_row['PId']; ?>" method="<?= $payment_row['Method']; ?>"
                                 style="display:block;">
                                <div class="img fl"><img src="<?= $payment_row['LogoPath']; ?>"
                                                         alt="<?= $payment_name; ?>"/></div>
                                <div class="name fl"><?= $payment_name; ?></div>
                                <div class="payment_contents fl" fee="<?= $payment_row['AdditionalFee']; ?>"
                                     affix="<?= cart::iconv_price($payment_row['AffixPrice'], 2, '', 0); ?>"></div>
                            </div>
                        </div>
                    </div>
                    <div class="checkout_divide ui_border_b"></div>
                    <div class="checkout_box">
                        <div class="box_title"><?= $c['lang_pack']['mobile']['summary']; ?></div>
                        <div class="box_content">
                            <div class="cart_item_list">
                                <?php
                                $total_price = $s_total_price = $quantity = $OvId = 0;
                                $cart_attr = $cart_attr_data = $cart_attr_value = array();
                                $order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, o.SKU as OrderSKU, p.Prefix, p.Number, p.SKU, p.PicPath_0, p.Business, p.CateId, p.IsFreeShipping, p.TId', 'o.LId asc');
                                foreach ((array)$order_list_row as $v) {
                                    if ($v['BuyType'] == 4) {
                                        //组合促销
                                        $package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
                                        if (!$package_row) continue;
                                        $attr = array();
                                        $v['Property'] != '' && $attr = str::json_data($v['Property'], 'decode');
                                        $products_row = str::str_code(db::get_all('products', "SoldOut=0 and ProId='{$package_row['ProId']}'"));
                                        $pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
                                        $pro_where == '' && $pro_where = 0;
                                        $products_row = array_merge($products_row, str::str_code(db::get_all('products', "SoldOut=0 and ProId in($pro_where)")));
                                        $data_ary = str::json_data($package_row['Data'], 'decode');
                                    } else {
                                        //普通产品
                                        $attr = array();
                                        $v['Property'] != '' && $attr = str::json_data($v['Property'], 'decode');
                                        $attr_len = count($attr);
                                        $oversea_len = db::get_row_count('products_selected_attribute', "ProId='{$v['ProId']}' and AttrId=0 and VId=0 and OvId>1 and IsUsed=1");
                                        (int)$c['config']['global']['Overseas'] == 0 && $attr_len == 1 && $attr['Overseas'] && $attr_len -= 1;
                                        $img = ly200::get_size_img($v['PicPath'], '240x240');
                                        $url = ly200::get_url($v, 'products');
                                    }
                                    $price = $v['Price'] + $v['PropertyPrice'];
                                    $v['Discount'] < 100 && $price *= $v['Discount'] / 100;
                                    $s_total_price += $price * $v['Qty'];
                                    $total_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
                                    $quantity += $v['Qty'];
                                    $OvId = $v['OvId'];
                                    ?>
                                    <div cid="<?= $v['CId']; ?>" ovid="<?= $v['OvId']; ?>"
                                         class="item clean ui_border_b">
                                        <div class="img fl"><a href="<?= $url; ?>"><img src="<?= $img; ?>"
                                                                                        alt="<?= $v['Name' . $c['lang']]; ?>"/></a>
                                        </div>
                                        <div class="info">
                                            <div class="name"><a href="<?= $url; ?>"><?= $v['Name']; ?></a></div>
                                            <?php
                                            if (count($attr)) {
                                                foreach ($attr as $k => $z) {
                                                    if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                    echo '<div class="rows">' . ($k == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $k) . ': &nbsp;' . $z . '</div>';
                                                }
                                            }
                                            if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                echo '<div class="rows">' . $c['lang_pack']['products']['shipsFrom'] . ': &nbsp;' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</div>';
                                            } ?>
                                            <?php if ($v['Remark']) { ?>
                                                <div class="rows"><?= $c['lang_pack']['mobile']['notice'] . ': &nbsp;' . $v['Remark']; ?></div><?php } ?>
                                            <div class="i_shipping"></div>
                                            <div class="clear"></div>
                                            <div class="price"><?= cart::iconv_price($price); ?><span
                                                        class="quantity">x<?= $v['Qty']; ?></span></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if ($c['FunVersion'] >= 1) { ?>
                                <div class="checkout_coupon">
                                    <div class="clean code_input">
                                        <label class="input_box fl">
                                            <span class="input_box_label"><?= $c['lang_pack']['mobile']['coupon_code']; ?></span>
                                            <input type="text" name="couponCode" class="box_input"
                                                   placeholder="<?= $c['lang_pack']['mobile']['coupon_code']; ?>"/>
                                        </label>
                                        <div class="btn_global btn_submit fr btn_coupon_disabled"
                                             id="coupon_apply"><?= $c['lang_pack']['mobile']['apply']; ?></div>
                                        <div class="clear"></div>
                                        <p class="error"></p>
                                    </div>
                                    <div class="code_valid clean" id="code_valid" style="display:none;">
                                        <div class="coupon_code_box">
                                            <em class="icon fl"></em>
                                            <span class="coupon_code_text fl"></span>
                                            <input type="button" value="<?= $c['lang_pack']['mobile']['remove']; ?>"
                                                   class="coupon_code_remove fr" id="removeCoupon"/>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            $UserPrice = 0;
                            if ($order_row['UserDiscount'] > 0) { //会员折扣
                                $UserPrice = $ProductPrice - $ProductPrice * ($order_row['UserDiscount'] / 100);
                            } ?>
                            <form id="PlaceOrderFrom" method="post" action="?" amountPrice="<?= $ProductPrice; ?>"
                                  userPrice="<?= $UserPrice; ?>">
                                <div class="checkout_summary ui_border_t">
                                    <div class="clean"><!-- subtotal -->
                                        <div class="key"><?= $c['lang_pack']['mobile']['subtotal']; ?></div>
                                        <div class="value"><?= $Symbol; ?><span
                                                    id="ot_subtotal"><?= cart::currency_format($ProductPrice, 0, $order_row['Currency']); ?></span>
                                        </div>
                                    </div>
                                    <?php
                                    if ($order_row['UserDiscount'] > 0) { //会员折扣
                                        ?>
                                        <div class="clean" id="memberSavings"><!-- Member Savings -->
                                            <div class="key"><?= $c['lang_pack']['cart']['user_save']; ?></div>
                                            <div class="value"><?= $Symbol; ?><span
                                                        id="ot_user"><?= cart::currency_format($UserPrice, 0, $order_row['Currency']); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="clean" id="shipping_charges" style="display:none;">
                                        <!-- Shipping Charges -->
                                        <div class="key"><?= $c['lang_pack']['mobile']['ship']; ?></div>
                                        <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                                    id="ot_shipping">0</span></div>
                                    </div>
                                    <div class="clean" id="shipping_and_insurance">
                                        <!-- Shipping Insurance combine -->
                                        <div class="key"><?= $c['lang_pack']['mobile']['ship_and_ins']; ?></div>
                                        <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                                    id="ot_combine_shippnig_insurance">0</span></div>
                                    </div>
                                    <div class="clean" style="display:none;" id="couponSavings">
                                        <!-- Coupon Savings -->
                                        <div class="key"><?= $c['lang_pack']['mobile']['coupon_save']; ?></div>
                                        <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                                    id="ot_coupon">0</span></div>
                                    </div>
                                    <?php
                                    if ($order_row['Discount'] > 0) { //优惠折扣 折扣形式
                                        $DiscountPrice = $ProductPrice - $ProductPrice * ((100 - $order_row['Discount']) / 100);
                                        ?>
                                        <div class="clean" id="subtotalDiscount"><!-- subtotal discount -->
                                            <div class="key"><?= $c['lang_pack']['cart']['save']; ?></div>
                                            <div class="value"><?= $Symbol; ?>
                                                <span><?= cart::currency_format($DiscountPrice, 0, $order_row['Currency']); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($order_row['DiscountPrice'] > 0) { //优惠折扣 金额形式
                                        $DiscountPrice = $order_row['DiscountPrice'];
                                        ?>
                                        <div class="clean" id="subtotalDiscount"><!-- subtotal discount -->
                                            <div class="key"><?= $c['lang_pack']['cart']['save']; ?></div>
                                            <div class="value"><?= $Symbol; ?><span
                                                        id="ot_subtotal_discount"><?= cart::currency_format($order_row['DiscountPrice'], 0, $order_row['Currency']); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="clean" id="serviceCharge"><!-- Service Charge -->
                                        <div class="key"><?= $c['lang_pack']['cart']['fee']; ?></div>
                                        <div class="value"><?= $Symbol; ?><span id="ot_fee">0</span></div>
                                    </div>
                                    <div class="clean ui_border_t" id="total"><!-- Total -->
                                        <div class="key"><?= $c['lang_pack']['mobile']['grand_total']; ?></div>
                                        <div class="value"><?= $Symbol; ?><span id="ot_total"></span></div>
                                    </div>
                                </div>
                                <div class="checkout_button">
                                    <div class="btn_global btn BuyNowBgColor btn_confirm"
                                         id="excheckoutFormSubmit"><?= $c['lang_pack']['mobile']['confirm']; ?></div>
                                    <div style="display: none" class="btn_global btn btn_cancel"><?= $c['lang_pack']['cart']['cancel']; ?></div>
                                </div>
                                <input type="hidden" name="OId" value="<?= $OId; ?>"/>
                                <input type="hidden" name="ProductPrice"
                                       value="<?= sprintf('%01.2f', $ProductPrice); ?>"/>
                                <input type="hidden" name="DiscountPrice"
                                       value="<?= sprintf('%01.2f', $DiscountPrice); ?>"/>
                                <input type="hidden" name="order_coupon_code"
                                       value="<?= $_SESSION['Cart']['Coupon']; ?>" cutprice="0"/>
                                <input type="hidden" name="order_discount_price"
                                       value="<?= sprintf('%01.2f', $DiscountPrice); ?>"/>
                                <input type="hidden" name="order_shipping_address_aid" value="-1"/>
                                <input type="hidden" name="order_shipping_states_sid"
                                       value="<?= (int)$order_row['ShippingSId']; ?>"/>
                                <input type="hidden" name="order_shipping_address_cid"
                                       value="<?= $order_row['ShippingCId']; ?>"/>
                                <input type="hidden" name="order_shipping_method_sid" value="[]"/>
                                <input type="hidden" name="order_shipping_method_type" value="[]"/>
                                <input type="hidden" name="order_shipping_price" value="[]"/>
                                <input type="hidden" name="order_shipping_insurance" value="[]" price="[]"/>
                                <input type="hidden" name="order_shipping_oversea"
                                       value="<?= $oversea_id_hidden ? implode(',', $oversea_id_hidden) : ''; ?>"/>
                                <input type="hidden" name="order_payment_method_pid" value="-1"/>
                                <input type="hidden" name="order_products_attribute_error"
                                       value="<?= (int)$products_attribute_error; ?>"/>
                                <input type="hidden" name="shipping_method_where"
                                       value="&ProId=<?= $cart_row[0]['ProId']; ?>&Qty=<?= $cart_row[0]['Qty']; ?>&Type=shipping_cost"
                                       attr="<?= str::str_code(str::json_data($Attr)); ?>"/>
                                <input type="hidden" name="shipping_template"
                                       value="<?= @in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0; ?>"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blank12"></div>
    <?php
    /************************************************** Paypal Checkout End **************************************************/
    }else{
    ?>
        <div class="complete_box">
            <div class="complete_tips"><?= $c['lang_pack']['mobile']['received_txt']; ?></div>
            <?php
            if ((int)$payOffline && $payment_row['PId'] != 9) {    //线下支付，并且非货到付款
                $currency_row = db::get_all('currency', "IsUsed='1'");
                ?>
                <div class="pay_info">
                    <div class="title"><?= $c['lang_pack']['mobile']['order_summary']; ?></div>
                    <div class="rows">
                        <strong><?= $c['lang_pack']['mobile']['order_num']; ?>:</strong>
                        <span>#<?= $OId; ?></span>
                    </div>
                    <div class="rows">
                        <strong><?= $c['lang_pack']['mobile']['total_amonut']; ?>:</strong>
                        <span><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price(orders::orders_price($order_row, 1, 1), 2); ?></span>
                    </div>
                    <div class="rows">
                        <strong><?= $c['lang_pack']['mobile']['num_of_item']; ?>:</strong>
                        <span><?= (int)db::get_sum('orders_products_list', "OrderId='{$order_row['OrderId']}'", 'Qty'); ?></span>
                    </div>
                    <!--                        add by jay start-->
                    <div class="rows">
                        <strong><?= $c['lang_pack']['cart']['deposit']; ?>:</strong>
                        <span><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price($order_row['DepositPrice'], 2); ?></span>
                    </div>
                    <div class="rows">
                        <strong><?= $c['lang_pack']['mobile']['order_status']; ?>:</strong>
                        <span>
                            <?=
                            //                            $c['orders']['status'][$order_row['OrderStatus']];
                            $c['lang_pack']['user']['OrderStatusAry'][$order_row['OrderStatus']];
                            ?>
                        </span>
                    </div>
                    <!--                        add by jay end-->
                    <div class="rows">
                        <strong><?= $c['lang_pack']['mobile']['pay_method']; ?>:</strong>
                        <span><?= $order_row['PaymentMethod']; ?></span>
                    </div>
<!--                    add by jay start -->
                    <a href="/account/orders/"
                       class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
<!--                    add by jay end -->
                </div>
                <div class="payment_info"><?= $payment_row['Description' . $c['lang']]; ?></div>
                <form method="post" action="?" class="pay_form" id="pay_form" style="display: none">
                    <?php if ($payment_row['Method'] == 'WesternUnion') { ?>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['first_name']; ?>:</div>
                            <div class="input clean"><input type="text" class="box_input" name="FirstName" notnull/>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['last_name']; ?>:</div>
                            <div class="input clean"><input type="text" class="box_input" name="LastName" notnull/>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['currency']; ?>:</div>
                            <div class="input clean">
                                <div class="box_select">
                                    <select class="addr_select" name="Currency" notnull>
                                        <?php
                                        $currency_row = db::get_all('currency', "IsUsed='1'");
                                        foreach ((array)$currency_row as $v) {
                                            ?>
                                            <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['sent_money']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="SentMoney"
                                                                    maxlength="8" notnull/></span>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['mtcn_no']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="MTCNNumber"
                                                                    placeholder="<?= $c['lang_pack']['mobile']['10_digits']; ?>"
                                                                    maxlength="10" format="Length|10" notnull/></span>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['country']; ?>:</div>
                            <div class="input clean">
                                <div class="box_select">
                                    <select class="addr_select" name="Country" notnull>
                                        <option value=""><?= $c['lang_pack']['mobile']['plz_country']; ?>---
                                        </option>
                                        <?php
                                        $country_row = str::str_code(db::get_all('country', "IsUsed=1", '*', 'IsHot desc, Country asc'));
                                        foreach ($country_row as $v) {
                                            $Country = $v['Country'];
                                            if ($c['lang'] != '_en') {
                                                $country_data = str::json_data(htmlspecialchars_decode($v['CountryData']), 'decode');
                                                $Country = $country_data[substr($c['lang'], 1)];
                                            }
                                            ?>
                                            <option
                                            value="<?= $v['CId']; ?>"><?= $Country; ?></option><?php /*?> <?=$v['IsDefault']==1?'selected':'';?><?php */
                                            ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['contents']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><textarea name="Contents"
                                                                       class="box_input box_textarea"></textarea></span>
                            </div>
                        </div>
                    <?php } elseif ($payment_row['Method'] == 'MoneyGram' || $payment_row['Method'] == 'TT' || $payment_row['Method'] == 'BankTransfer') { ?>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['send_name']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span fl whalf"><input type="text" class="box_input"
                                                                             placeholder="First Name" name="FirstName"
                                                                             notnull/></span>
                                <span class="input_span fr whalf"><input type="text" class="box_input"
                                                                         placeholder="Last Name" name="LastName"
                                                                         notnull/></span>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['currency']; ?>:</div>
                            <div class="input clean">
                                <div class="box_select">
                                    <select class="addr_select" name="Currency" notnull>
                                        <?php
                                        $currency_row = db::get_all('currency', "IsUsed='1'");
                                        foreach ((array)$currency_row as $v) {
                                            ?>
                                            <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['sent_money']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="SentMoney"
                                                                    maxlength="8" notnull/></span>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['cart']['bankSlipCode']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="MTCNNumber"
                                                                    placeholder="<?= $c['lang_pack']['cart']['bankSlipCode']; ?>"
                                                                    maxlength="50" notnull/></span>
                            </div>
                        </div>
                        <div class="rows">
                            <div class="field"><?= $c['lang_pack']['mobile']['contents']; ?>:</div>
                            <div class="input clean">
                                    <span class="input_span"><textarea name="Contents"
                                                                       class="box_input box_textarea"></textarea></span>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="pay_button">
                            <span class="btn_global btn btn_pay BuyNowBgColor"
                                  id="paybtn"><?= $c['lang_pack']['mobile']['submit']; ?></span>
                        <a class="btn_global btn btn_view_order"
                           href="/account/orders/view<?= $OId ?>.html"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
                    </div>
                    <input type="hidden" name="PaymentMethod" value="<?= $payment_row['Method']; ?>"/>
                    <input type="hidden" name="OId" value="<?= $order_row['OId']; ?>"/>
                </form>
                <div class="blank15"></div>
                <?php
            } else {
                if ((int)$_GET['ueeshop_store'] == 0) {
                ?>
                <a href="/account/orders/"
                   class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
                <?php
                }
            } ?>
        </div>
    <?php } ?>

    <?php
    } elseif ($d == 'balance'){
    $data = array(
        'OrderStatus' => 5
    );
    db::update('orders', "OId='$OId'", $data);
    $order_row = db::get_one('orders', "OId='$OId'");
    ?>
        <div class="complete_box">
            <div class="complete_tips"><?= $c['lang_pack']['mobile']['received_txt']; ?></div>
            <?php
            $currency_row = db::get_all('currency', "IsUsed='1'");
            ?>
            <div class="pay_info">
                <div class="title"><?= $c['lang_pack']['mobile']['order_summary']; ?></div>
                <div class="rows">
                    <strong><?= $c['lang_pack']['mobile']['order_num']; ?>:</strong>
                    <span>#<?= $OId; ?></span>
                </div>
                <div class="rows">
                    <strong><?= $c['lang_pack']['mobile']['total_amonut']; ?>:</strong>
                    <span><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price(orders::orders_price($order_row, 1, 1), 2); ?></span>
                </div>
                <div class="rows">
                    <strong><?= $c['lang_pack']['mobile']['num_of_item']; ?>:</strong>
                    <span><?= (int)db::get_sum('orders_products_list', "OrderId='{$order_row['OrderId']}'", 'Qty'); ?></span>
                </div>
                <!--                        add by jay start-->
                <div class="rows">
                    <strong><?= $c['lang_pack']['cart']['deposit']; ?>:</strong>
                    <span><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price($order_row['DepositPrice'], 2); ?></span>
                </div>
                <div class="rows">
                    <strong><?= $c['lang_pack']['user']['_balance']; ?>:</strong>
                    <span>
                            <?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price(cart::getBalance(
                                orders::orders_price($order_row, 1, 1),
                                $order_row['DepositPrice']
                            ), 2); ?>
                        </span>
                </div>
                <div class="rows">
                    <strong><?= $c['lang_pack']['mobile']['order_status']; ?>:</strong>
                    <span>
                            <?=
                            //                            $c['orders']['status'][$order_row['OrderStatus']];
                            $c['lang_pack']['user']['OrderStatusAry'][$order_row['OrderStatus']];
                            ?>
                        </span>
                </div>
                <!--                        add by jay end-->
                <div class="rows">
                    <strong><?= $c['lang_pack']['mobile']['pay_method']; ?>:</strong>
                    <span><?= $order_row['PaymentMethod']; ?></span>
                </div>

                <!--                    add by jay start -->
                <a style="display: none" href="/account/orders/"
                   class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
                <!--                    add by jay end -->
            </div>
            <div class="payment_info"><?= $payment_row['Description' . $c['lang']]; ?></div>
            <form method="post" action="?" class="pay_form" id="pay_form" style="display: none">
                <?php if ($payment_row['Method'] == 'WesternUnion') { ?>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['first_name']; ?>:</div>
                        <div class="input clean"><input type="text" class="box_input" name="FirstName" notnull/>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['last_name']; ?>:</div>
                        <div class="input clean"><input type="text" class="box_input" name="LastName" notnull/>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['currency']; ?>:</div>
                        <div class="input clean">
                            <div class="box_select">
                                <select class="addr_select" name="Currency" notnull>
                                    <?php
                                    $currency_row = db::get_all('currency', "IsUsed='1'");
                                    foreach ((array)$currency_row as $v) {
                                        ?>
                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['sent_money']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="SentMoney"
                                                                    maxlength="8" notnull/></span>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['mtcn_no']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="MTCNNumber"
                                                                    placeholder="<?= $c['lang_pack']['mobile']['10_digits']; ?>"
                                                                    maxlength="10" format="Length|10" notnull/></span>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['country']; ?>:</div>
                        <div class="input clean">
                            <div class="box_select">
                                <select class="addr_select" name="Country" notnull>
                                    <option value=""><?= $c['lang_pack']['mobile']['plz_country']; ?>---
                                    </option>
                                    <?php
                                    $country_row = str::str_code(db::get_all('country', "IsUsed=1", '*', 'IsHot desc, Country asc'));
                                    foreach ($country_row as $v) {
                                        $Country = $v['Country'];
                                        if ($c['lang'] != '_en') {
                                            $country_data = str::json_data(htmlspecialchars_decode($v['CountryData']), 'decode');
                                            $Country = $country_data[substr($c['lang'], 1)];
                                        }
                                        ?>
                                        <option
                                        value="<?= $v['CId']; ?>"><?= $Country; ?></option><?php /*?> <?=$v['IsDefault']==1?'selected':'';?><?php */
                                        ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['contents']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><textarea name="Contents"
                                                                       class="box_input box_textarea"></textarea></span>
                        </div>
                    </div>
                <?php } elseif ($payment_row['Method'] == 'MoneyGram' || $payment_row['Method'] == 'TT' || $payment_row['Method'] == 'BankTransfer') { ?>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['send_name']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span fl whalf"><input type="text" class="box_input"
                                                                             placeholder="First Name" name="FirstName"
                                                                             notnull/></span>
                            <span class="input_span fr whalf"><input type="text" class="box_input"
                                                                     placeholder="Last Name" name="LastName"
                                                                     notnull/></span>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['currency']; ?>:</div>
                        <div class="input clean">
                            <div class="box_select">
                                <select class="addr_select" name="Currency" notnull>
                                    <?php
                                    $currency_row = db::get_all('currency', "IsUsed='1'");
                                    foreach ((array)$currency_row as $v) {
                                        ?>
                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['sent_money']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="SentMoney"
                                                                    maxlength="8" notnull/></span>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['cart']['bankSlipCode']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><input type="text" class="box_input" name="MTCNNumber"
                                                                    placeholder="<?= $c['lang_pack']['cart']['bankSlipCode']; ?>"
                                                                    maxlength="50" notnull/></span>
                        </div>
                    </div>
                    <div class="rows">
                        <div class="field"><?= $c['lang_pack']['mobile']['contents']; ?>:</div>
                        <div class="input clean">
                                    <span class="input_span"><textarea name="Contents"
                                                                       class="box_input box_textarea"></textarea></span>
                        </div>
                    </div>
                <?php } ?>
                <div class="pay_button">
                            <span class="btn_global btn btn_pay BuyNowBgColor"
                                  id="paybtn"><?= $c['lang_pack']['mobile']['submit']; ?></span>
                    <a style="display: none" class="btn_global btn btn_view_order"
                       href="/account/orders/view<?= $OId ?>.html"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
                </div>
                <input type="hidden" name="PaymentMethod" value="<?= $payment_row['Method']; ?>"/>
                <input type="hidden" name="OId" value="<?= $order_row['OId']; ?>"/>
            </form>
            <div class="blank15"></div>
            <?php

            if ((int)$_GET['ueeshop_store'] == 0) {
            ?>
            <a href="/account/orders/"
               class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
            <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>