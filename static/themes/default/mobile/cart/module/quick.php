<?php !isset($c) && exit();?>
<?php
//购物车产品
$where='c.'.$c['where']['cart'];
if($_GET['CId']) $where.=' and c.CId in('.str_replace('.', ',', $_GET['CId']).')';
$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', $where, "c.*, c.Attr as CartAttr, p.Name{$c['lang']}, p.Prefix, p.Number, p.AttrId, p.Attr, p.IsCombination, p.IsOpenAttrPrice", 'c.CId desc');
!count($cart_row) && js::location('/cart/', '', '.top');

//产品总价、总数量
$total_price=db::get_sum('shopping_cart', $c['where']['cart'].($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), '(Price+PropertyPrice)*Discount/100*Qty');
$iconv_total_price=cart::cart_total_price(($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 1);

//检查产品资料是否完整
$oversea_id_ary=array();
$products_attribute_error=0;
foreach((array)$cart_row as $v){
	!in_array($v['OvId'], $oversea_id_ary) && $oversea_id_ary[]=$v['OvId'];
	if($v['BuyType']!=4 && $v['CartAttr']!='[]' && $v['CartAttr']!='{}' && $v['CartAttr']!='{"Overseas":"Ov:1"}'){
		if(!(int)$v['IsCombination'] && !(int)$v['IsOpenAttrPrice']) continue;//多规格模式和属性价格，都没有开启
		$IsError=0;
		$prod_selected_ary=$ext_ary=array();
		$AttrAry=@str::json_data(str::attr_decode($v['CartAttr']), 'decode');
		$prod_selected_row=db::get_all('products_selected_attribute', "ProId='{$v['ProId']}' and VId>0 and IsUsed=1");
		foreach((array)$prod_selected_row as $v2){
			$prod_selected_ary[]=$v2['VId'];
		}
		if((int)$v['IsCombination'] && db::get_row_count('products_selected_attribute_combination', "ProId='{$v['ProId']}'")){//开启规格组合
			$OvId=1;
			foreach($AttrAry as $k2=>$v2){
				if($k2=='Overseas'){//发货地
					$OvId=str_replace('Ov:', '', $v2);
					(int)$c['config']['global']['Overseas']==0 && $OvId!=1 && $OvId=0;//关闭海外仓功能，发货地不是China，不能购买
				}else{
					!in_array($v2, $prod_selected_ary) && $IsError=1;
					$ext_ary[]=$v2;
				}
			}
			sort($ext_ary); //从小到大排序
			$Combination='|'.implode('|', $ext_ary).'|';
			$row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='{$Combination}' and OvId='{$OvId}'"));
		}else{
			foreach((array)$AttrAry as $k2=>$v2){
				if($k2=='Overseas') continue;
				$row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='|{$v2}|' and OvId=1"));
				$row && $PropertyPrice+=(float)$row['Price'];//固定是加价
			}
			if($v['BuyType']==3 && !$AttrAry) $row=1;//放过组合购买
		}
		if(!$row || $IsError>0) $products_attribute_error=1;//检查此产品是否有选择购物车属性
	}
}
sort($oversea_id_ary); //排列正序
$OvId_where='a.OvId in('.implode(',', $oversea_id_ary).')';

if((int)$_SESSION['User']['UserId']){ //会员收货地址信息
	//收货地址
	$address_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', 'a.'.$c['where']['cart']." and a.IsBillingAddress=0".($_SESSION['Cart']['ShippingAddressAId']?" and a.AId='{$_SESSION['Cart']['ShippingAddressAId']}'":''), 'a.*, c.Country, s.States as StateName', 'a.AccTime desc, a.AId desc'));
}elseif($_SESSION['Cart']['ShippingAddress']){ //非会员收货地址信息
	$address_ary=$_SESSION['Cart']['ShippingAddress'];
	$country_val=str::str_code(db::get_value('country', "CId='{$address_ary['CId']}'", 'Country'));
	$states_val=str::str_code(db::get_value('country_states', "SId='{$address_ary['SId']}'", 'States'));
	if($country_val || $states_val){
		$address_ary['Country']=$country_val;
		$address_ary['StateName']=$states_val;
	}
	$address_row=$address_ary;
	unset($address_ary);
}
//付款方式
$payment_row=db::get_all('payment', "IsUsed=1 and PId!=2");

$IsInsurance=str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));

$total_weight=db::get_sum('shopping_cart', $c['where']['cart'].($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 'Weight*Qty');
$total_quantity=db::get_sum('shopping_cart', $c['where']['cart'].($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 'Qty');

//会员优惠价 与 全场满减价 比较
$AfterPrice_0=$AfterPrice_1=0;
$user_discount=0;
if((int)$_SESSION['User']['UserId'] && (int)$_SESSION['User']['Level']){
	$user_discount=(float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
	$user_discount=($user_discount>0 && $user_discount<100)?$user_discount:100;
	$AfterPrice_0=$iconv_total_price-($iconv_total_price*($user_discount/100));
}
if($cutArr['IsUsed']==1 && $c['time']>=$cutArr['StartTime'] && $c['time']<=$cutArr['EndTime']){
	foreach((array)$cutArr['Data'] as $k=>$v){
		if($total_price<$k) break;
		$AfterPrice_1=($cutArr['Type']==1?cart::iconv_price($v[1], 2, '', 0):($iconv_total_price*(100-$v[0])/100));
	}
}
if($AfterPrice_0==$AfterPrice_1){//当会员优惠价和全场满减价一致，默认只保留会员优惠价
	$AfterPrice_1=0;
}

$country_row=str::str_code(db::get_all('country', "IsUsed='1'", 'CId, Country, Acronym, FlagPath, IsDefault', 'IsHot desc, Country asc'));
foreach($country_row as $v){
	if($v['IsDefault']){ $CId=$v['CId']; break; }
}
$shipto_country_ary=array();
$shipping_country_row=db::get_all('shipping_area a left join shipping_country c on a.AId=c.AId', $OvId_where.' Group By c.CId', 'c.CId');
foreach($shipping_country_row as $v){ $shipto_country_ary[]=$v['CId']; }

if((int)$c['config']['global']['Overseas']==0 || count($c['config']['Overseas'])<2 || count($oversea_id_ary)<2){//关闭海外仓功能 或者 仅有一个海外仓选项
?>
	<style>#shippingObj .txt .oversea:first-child .shipping_title{display:none;}</style>
<?php
}
$oversea_id_hidden=$oversea_id_ary;
$NotDefualtOvId=0;
if(!in_array(1, $oversea_id_ary)){//购物车没有默认海外仓追加隐藏选项
	$oversea_id_ary[]=1;
	$NotDefualtOvId=1;
}
sort($oversea_id_ary); //排列正序
$oversea_count=count($oversea_id_ary);
?>
<script type="text/javascript">
var address_perfect=0;
$(function(){
	cart_obj.cart_checkout();
	<?php
	if($c['NewFunVersion']>=4){//新用户版本 和 Paypal支付
		$IsCreditCard=(int)db::get_value('payment', 'IsUsed=1 and Method="Excheckout"', 'IsCreditCard');//是否开启信用卡支付
		//生成临时订单号
		while(1){
			$OId=date('ymdHis', $c['time']).mt_rand(10,99);
			if(!db::get_row_count('orders', "OId='$OId'")){
				break;
			}
		}
	?>
		cart_obj.paypal_checkout_init(<?=$IsCreditCard;?>);
		cart_obj.cart_init.paypal_data.OId='<?=$OId;?>';
	<?php
		foreach($cart_row as $k=>$v){
			echo "
				cart_obj.cart_init.paypal_data['name_{$k}']='".addslashes($v['Name'.$c['lang']])."';
				cart_obj.cart_init.paypal_data['quantity_{$k}']='{$v['Qty']}';
				cart_obj.cart_init.paypal_data['price_{$k}']='".($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1)."';
				cart_obj.cart_init.paypal_data['currency_{$k}']='{$_SESSION['Currency']['Currency']}';
			";
		}
	}?>
});
</script>
<style type="text/css">
.checkout_shipping .shipping:hover .icon_shipping_title, .checkout_shipping .current .icon_shipping_title, .checkout_shipping .icon_shipping_title{background-color:<?=$style_data['BuyNowBgColor'];?>;}
</style>
<div id="cart">
    <div class="cart_head ui_border_b"><?=html::website_h1('logo', '', '', $logo);?></div>
    <div class="cart_checkout">
		<form id="PlaceOrderFrom" name="paypal_excheckout" method="post" action="/payment/paypal_excheckout/do_payment/?utm_nooverride=1" amountPrice="<?=$iconv_total_price;?>" userPrice="<?=(($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0>$AfterPrice_1))?$AfterPrice_0:0;?>">
			<div class="checkout_box checkout_address ui_border_b">
				<div class="box_title"><?=$c['lang_pack']['mobile']['select_country'];?></div>
				<div class="box_content">
					<div class="box_select" id="select_country">
						<select name="CId">
							<option value=""><?=$c['lang_pack']['mobile']['plz_country'];?></option>
							<?php
							foreach($country_row as $v){
								if(!in_array($v['CId'], $shipto_country_ary)) continue;//所有快递方式都没有的国家，给过滤掉
							?>
								<option value="<?=$v['CId'];?>"<?=$v['IsDefault']?' selected':'';?>><?=$v['Country'];?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<?php if(@in_array('shipping_template', (array)$c['plugins']['Used'])){ ?>
                <div class="checkout_divide"></div>
		    	<div class="checkout_box">
		            <div class="box_title"><?=$c['lang_pack']['mobile']['summary'];?></div>
		            <div class="box_content">
						<div class="cart_item_list">
							<?php
							$ptotal=0;
							$cart_attr=$cart_attr_data=array();
							foreach($cart_row as $v){
								$attr=array();
								$v['Property']!='' && $attr=str::json_data(str::attr_decode($v['Property']), 'decode');
								!$attr && $attr=str::json_data(htmlspecialchars_decode($v['Property']), 'decode');
								$price=$v['Price']+$v['PropertyPrice'];
								$v['Discount']<100 && $price*=$v['Discount']/100;
								$img=ly200::get_size_img($v['PicPath'], '240x240');
								$url=ly200::get_url($v, 'products');
								$total_weight+=($v['Weight']*$v['Qty']);
							?>
							<div cid="<?=$v['CId'] ?>" ovid="<?=$v['OvId'] ?>" class="item clean ui_border_b">
								<div class="img fl"><a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$v['Name'.$c['lang']];?>"></a></div>
								<div class="info">
									<div class="name"><a href="<?=$url;?>"><?=$v['Name'.$c['lang']];?></a></div>
									<?php
									if(count($attr)){
										foreach($attr as $k=>$z){
											if($k=='Overseas' && ((int)$c['config']['global']['Overseas']==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
											echo '<div class="rows">'.($k=='Overseas'?$c['lang_pack']['products']['shipsFrom']:$k).': &nbsp;'.$z.'</div>';
										}
									}
									if((int)$c['config']['global']['Overseas']==1 && $v['OvId']==1){
										echo '<div class="rows">'.$c['lang_pack']['products']['shipsFrom'].': &nbsp;'.$c['config']['Overseas'][$v['OvId']]['Name'.$c['lang']].'</div>';
									}?>
									<?php if($v['Remark']){?><div class="rows"><?=$c['lang_pack']['mobile']['notice'].': &nbsp;'.$v['Remark'];?></div><?php }?>
									<div class="i_shipping">
										
									</div>
									<div class="clear"></div>
									<div class="price"><?=cart::iconv_price($price);?><span class="quantity">x<?=$v['Qty'];?></span></div>
								</div>
							</div>
							<?php }?>
						</div>
		            </div>
		        </div>
	        <?php } ?>
	        <div class="checkout_divide"></div>
			<div class="checkout_box checkout_shipping">
				<div class="box_title<?=$oversea_count>1?' ui_border_b':'';?>"><?=$c['lang_pack']['cart']['shipmethod'];?></div>
				<div class="box_content">
					<?php foreach($oversea_id_ary as $k=>$v){?>
						<div class="shipping<?=($NotDefualtOvId && (int)$v==1)?' hide':'';?>" data-id="<?=$v;?>">
							<div class="title">
								<strong><?=/*$c['lang_pack']['products']['shipsFrom'].' '.*/$c['config']['Overseas'][$v]['Name'.$c['lang']];?></strong>
								<div class="shipping_info"><span class="error"><?=$c['lang_pack']['cart']['notDeliveries'];?></span><span class="name"></span><span class="price"></span></div>
								<i class="icon_shipping_title"></i>
							</div>
							<div class="list">
								<ul class="shipping_method_list clearfix"></ul>
								<?php if($IsInsurance){?>
									<div class="insurance">
										<input type="checkbox" name="_shipping_method_insurance" class="shipping_insurance" value="1" checked />
										<label for="shipping_insurance"><?=$c['lang_pack']['cart']['add_insur'];?></label>
										<span class="price"><?=$_SESSION['Currency']['Symbol'];?><em></em></span>
									</div>
								<?php }?>
							</div>
						</div>
					<?php }?>
				</div>
			</div>
            <div class="checkout_box">
                <div class="box_title"><?=$c['lang_pack']['mobile']['summary'];?></div>
                <div class="box_content">
                    <?php if($c['FunVersion']>=1){?>
                        <div class="checkout_coupon">
                            <div class="clean code_input">
                                <label class="input_box fl">
                                    <span class="input_box_label"><?=$c['lang_pack']['mobile']['coupon_code'];?></span>
                                    <input type="text" name="couponCode" class="box_input" placeholder="<?=$c['lang_pack']['mobile']['coupon_code'];?>" />
                                </label>
                                <div class="btn_global btn_submit fr btn_coupon_disabled" id="coupon_apply"><?=$c['lang_pack']['submit'];?></div>
                                <div class="clear"></div>
                                <p class="error"></p>
                            </div>
                            <div class="code_valid clean" id="code_valid" style="display:none;">
                                <div class="coupon_code_box">
                                    <em class="icon fl"></em>
                                    <span class="coupon_code_text fl"></span>
                                    <input type="button" value="<?=$c['lang_pack']['mobile']['remove'];?>" class="coupon_code_remove fr" id="removeCoupon" />
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="checkout_summary ui_border_t">
                        <div class="clean"><!-- subtotal -->
                            <div class="key"><?=$c['lang_pack']['mobile']['subtotal'].' ('.str_replace('%num%', $total_quantity, $c['lang_pack']['cart'][($total_quantity>1?'itemsCount':'itemCount')]).')';?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_subtotal"><?=cart::currency_format($iconv_total_price, 0, $_SESSION['Currency']['Currency']);?></span></div>
                        </div>
                        <?php if(($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0>$AfterPrice_1)){?>
                            <div class="clean" id="memberSavings"><!-- Member Savings -->
                                <div class="key">(-) <?=$c['lang_pack']['cart']['user_save'];?></div>
                                <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_user"><?=cart::currency_format($AfterPrice_0, 0, $_SESSION['Currency']['Currency']);?></span></div>
                            </div>
                        <?php }?>
                        <div class="clean" id="shipping_charges" style="display:none;"><!-- Shipping Charges -->
                            <div class="key">(+) <?=$c['lang_pack']['mobile']['ship_charge'];?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_shipping">0</span></div>
                        </div>
                        <div class="clean" id="shipping_and_insurance"><!-- Shipping Insurance combine -->
                            <div class="key">(+) <?=$c['lang_pack']['mobile']['ship_ins_txt'];?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_combine_shippnig_insurance">0</span></div>
                        </div>
                        <div class="clean" style="display:none;" id="couponSavings"><!-- Coupon Savings -->
                            <div class="key">(-) <?=$c['lang_pack']['mobile']['coupon_save'];?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_coupon">0</span></div>
                        </div>
                        <?php
                        $cutprice=0;
                        if(($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1>$AfterPrice_0)){
                        ?>
                            <div class="clean" id="subtotalDiscount"><!-- subtotal discount -->
                                <div class="key">(-) <?=$c['lang_pack']['cart']['save'];?>:</div>
                                <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_subtotal_discount"><?=cart::currency_format($AfterPrice_1, 0, $_SESSION['Currency']['Currency']);?></span></div>
                            </div>
                        <?php }?>
                        <?php $pay_row=db::get_one('payment', 'IsUsed=1 and Method="Excheckout"', '*'); ?>
                        <div class="clean" style="display:<?=$pay_row['AdditionalFee']>0 || $pay_row['AffixPrice']>0 ? 'block' : 'none'; ?>;" id="serviceCharge"><!-- Service Charge -->
                            <div class="key">(+) <?=$c['lang_pack']['cart']['fee'];?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_fee" fee="<?=(float)$pay_row['AdditionalFee']; ?>" affix="<?=cart::iconv_price($pay_row['AffixPrice'], 2, '', 0); ?>">0</span></div>
                        </div>
                        <div class="clean" id="total"><!-- Total -->
                            <div class="key"><?=$c['lang_pack']['mobile']['grand_total'];?>:</div>
                            <div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_total"></span></div>
                        </div>
                    </div>
                    <div class="checkout_button">
                        <div class="btn_global btn BuyNowBgColor" id="paypal_checkout"><?=$c['lang_pack']['mobile']['checkout'];?></div>
                    </div>
                    <input type="hidden" name="order_coupon_code" value="<?=$_SESSION['Cart']['Coupon'];?>" cutprice="0" />
                    <input type="hidden" name="order_discount_price" value="<?=($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1>$AfterPrice_0)?$AfterPrice_1:0;?>" />
                    <input type="hidden" name="order_products_attribute_error" value="<?=(int)$products_attribute_error;?>" />
                    <!-- 提交数据 Start -->
                    <input type="hidden" name="ShippingMethodType" value="[]" />
                    <input type="hidden" name="ShippingPrice" value="[]" />
                    <input type="hidden" name="ShippingInsurancePrice" value="[]" />
                    <input type="hidden" name="ShippingExpress" value="[]" />
                    <input type="hidden" name="ShippingInsurance" value="[]" />
                    <input type="hidden" name="SId" value="[]" />
                    <!-- 提交数据 End -->
                    <!-- 防止JSON格式报错 Start -->
                    <input type="hidden" name="order_shipping_method_sid" value="[]" />
                    <input type="hidden" name="order_shipping_method_type" value="[]" />
                    <input type="hidden" name="order_shipping_price" value="[]" />
                    <input type="hidden" name="order_shipping_insurance" value="[]" price="[]" />
                    <input type="hidden" name="order_shipping_oversea" value="<?=$oversea_id_ary?implode(',', $oversea_id_ary):'';?>" />
                    <!-- 防止JSON格式报错 End -->
                    <input type="hidden" name="shipping_method_where" value="&ProId=<?=$cart_row[0]['ProId'];?>&Qty=<?=$cart_row[0]['Qty'];?>&Type=shipping_cost" attr="<?=str::str_code(str::json_data($Attr));?>" />
                    <input type="hidden" name="CartCId" value="<?=$_GET['CId']?str_replace('.', ',', $_GET['CId']):'';?>" />
                    <input type="hidden" name="order_cid" value="<?=$_GET['CId']?str_replace('.', ',', $_GET['CId']):'';?>" />
                    <input type="hidden" name="NewFunVersion" value="<?=$c['NewFunVersion'];?>" />
                    <input type="hidden" name="_OId" value="<?=$OId;?>" />
                </div>
            </div>
        </form> 
    </div>
</div>

