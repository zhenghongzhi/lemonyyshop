<?php !isset($c) && exit(); ?>
<?php
$yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');
$balance = ly200::getyy_balance($yy_code['yy_code']);
//头部Logo
$logo_path = str::str_code(db::get_value('config', "GroupId='print' and Variable='LogoPath'", 'Value'));
$logo = $logo_path ? $logo_path : $c['config']['global']['LogoPath'];

//BuyNow数据
if ($_GET['Data']) {
    $Data = array();
    $data_ary = explode('&', rawurldecode(base64_decode($_GET['Data']))); //Buy Now参数，已通过加密
    foreach ($data_ary as $v) {
        $arr = explode('=', $v);
        $Data[$arr[0]] = $arr[1];
    }
    $CId = (int)$Data['CId'];
}

//Checkout数据
if ($_GET['CId']) {
    $CId = $_GET['CId'];
}

$CIdStr = $cCIdStr = '';
if ($CId) {
    $CIdStr = ' and CId in(' . str_replace('.', ',', $CId) . ')';
    $cCIdStr = ' and c.CId in(' . str_replace('.', ',', $CId) . ')';
}

//购物车产品
$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', "c.{$c['where']['cart']}{$cCIdStr}", "c.*, c.Attr as CartAttr, p.Name{$c['lang']}, p.Prefix, p.Number, p.AttrId, p.Attr, p.IsCombination, p.IsOpenAttrPrice, p.CateId, p.price_rate", 'c.CId desc');
!count($cart_row) && js::location("/cart/");
//产品总金额
$total_price = db::get_sum('shopping_cart', $c['where']['cart'] . $CIdStr, '(Price+PropertyPrice)*Discount/100*Qty');
$iconv_total_price = cart::cart_total_price($cCIdStr, 1);
//产品总重量（自身+包装）
$total_weight = cart::cart_product_weight($cCIdStr, 2);

//检查产品资料是否完整
$oversea_id_ary = array();
$products_attribute_error = 0;
foreach ((array)$cart_row as $v) {
    !in_array($v['OvId'], $oversea_id_ary) && $oversea_id_ary[] = $v['OvId'];
    if ($v['BuyType'] != 4 && $v['CartAttr'] != '[]' && $v['CartAttr'] != '{}' && $v['CartAttr'] != '{"Overseas":"Ov:1"}') {
        if (!(int)$v['IsCombination'] && !(int)$v['IsOpenAttrPrice']) continue;//多规格模式和属性价格，都没有开启
        $IsError = 0;
        $prod_selected_ary = $ext_ary = array();
        $AttrAry = @str::json_data(str::attr_decode($v['CartAttr']), 'decode');
        $prod_selected_row = db::get_all('products_selected_attribute', "ProId='{$v['ProId']}' and VId>0 and IsUsed=1");
        foreach ((array)$prod_selected_row as $v2) {
            $prod_selected_ary[] = $v2['VId'];
        }
        if ((int)$v['IsCombination'] && db::get_row_count('products_selected_attribute_combination', "ProId='{$v['ProId']}'")) {//开启规格组合
            $OvId = 1;
            foreach ($AttrAry as $k2 => $v2) {
                if ($k2 == 'Overseas') {//发货地
                    $OvId = str_replace('Ov:', '', $v2);
                    (int)$c['config']['global']['Overseas'] == 0 && $OvId != 1 && $OvId = 0;//关闭海外仓功能，发货地不是China，不能购买
                } else {
                    !in_array($v2, $prod_selected_ary) && $IsError = 1;
                    $ext_ary[] = $v2;
                }
            }
            sort($ext_ary); //从小到大排序
            $Combination = '|' . implode('|', $ext_ary) . '|';
            $row = str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='{$Combination}' and OvId='{$OvId}'"));
        } else {
            foreach ((array)$AttrAry as $k2 => $v2) {
                if ($k2 == 'Overseas') continue;
                $row = str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='|{$v2}|' and OvId=1"));
                $row && $PropertyPrice += (float)$row['Price'];//固定是加价
            }
            if ($v['BuyType'] == 3 && !$AttrAry) $row = 1;//放过组合购买
        }
        if (!$row || $IsError > 0) $products_attribute_error = 1;//检查此产品是否有选择购物车属性
    }
}

//店铺模式，只允许非会员购买
if ((int)$_GET['ueeshop_store'] == 1) {
    $_SESSION['User'] = '';
    unset($_SESSION['User']);
}

//收货地址
if ((int)$_SESSION['User']['UserId']) {
    //会员收货地址信息
    $address_row = str::str_code(db::get_all('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', 'a.' . $c['where']['cart'] . " and a.IsBillingAddress=0" . ($_SESSION['Cart']['ShippingAddressAId'] ? " and a.AId='{$_SESSION['Cart']['ShippingAddressAId']}'" : ''), 'a.*, c.Country, s.States as StateName', 'a.AccTime desc, a.AId desc'));
    $CountryId = $address_row[0]['CId'];
    $IsStates = (int)db::get_row_count('country_states', "CId='{$address_row[0]['CId']}'", 'SId');
} elseif ($_SESSION['Cart']['ShippingAddress']) {
    //非会员收货地址信息
    $temporary_address_ary = $_SESSION['Cart']['ShippingAddress'];
    $country_val = str::str_code(db::get_value('country', "CId='{$address_ary['CId']}'", 'Country'));
    $states_val = str::str_code(db::get_value('country_states', "SId='{$address_ary['SId']}'", 'States'));
    if ($country_val || $states_val) {
        $temporary_address_ary['Country'] = $country_val;
        $temporary_address_ary['StateName'] = $states_val;
    }
    $CountryId = $temporary_address_ary['CId'];
}

//付款方式
$payment_row = db::get_all('payment', "IsUsed=1 and PId!=2", '*', $c['my_order'] . 'IsOnline desc,PId asc');

//是否开启保险费
$IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));

$total_weight = $total_quantity = 0;
$total_quantity = db::get_sum('shopping_cart', $c['where']['cart'] . ($CId ? ' and CId in(' . str_replace('.', ',', $CId) . ')' : ''), 'Qty');

//检测优惠
$DiscountData = cart::check_list_discounts($total_price, $iconv_total_price);

//会员优惠价 与 全场满减价 比较
$AfterPrice_0 = $AfterPrice_1 = 0;
$user_discount = 0;
if ((int)$_SESSION['User']['UserId'] && (int)$_SESSION['User']['Level']) {
    $user_discount = (float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
    $user_discount = ($user_discount > 0 && $user_discount < 100) ? $user_discount : 100;
    $AfterPrice_0 = $iconv_total_price - ($iconv_total_price * ($user_discount / 100));
}
if ($cutArr['IsUsed'] == 1 && $c['time'] >= $cutArr['StartTime'] && $c['time'] <= $cutArr['EndTime']) {
    foreach ((array)$cutArr['Data'] as $k => $v) {
        if ($total_price < $k) break;
        $AfterPrice_1 = ($cutArr['Type'] == 1 ? cart::iconv_price($v[1], 2, '', 0) : ($iconv_total_price * (100 - $v[0]) / 100));
    }
}
if ($AfterPrice_0 == $AfterPrice_1) {//当会员优惠价和全场满减价一致，默认只保留会员优惠价
    $AfterPrice_1 = 0;
}

//所有产品属性
$attribute_cart_ary = $vid_data_ary = array();
$CateIdStr = implode(' or ', $CateId_ary);
$attribute_row = str::str_code(db::get_all('products_attribute', '1' . ($CateIdStr ? " and $CateIdStr" : ''), "AttrId, Type, Name{$c['lang']}, CartAttr, ColorAttr"));
foreach ($attribute_row as $v) {
    $attribute_ary[$v['AttrId']] = array(0 => $v['Type'], 1 => $v["Name{$c['lang']}"]);
}
$AttrIdStr = implode(',', array_keys($attribute_ary));
$value_row = str::str_code(db::get_all('products_attribute_value', '1' . ($AttrIdStr ? " and AttrId in($AttrIdStr)" : ''), '*', $c['my_order'] . 'VId asc')); //属性选项
foreach ($value_row as $v) {
    $vid_data_ary[$v['AttrId']][$v['VId']] = $v["Value{$c['lang']}"];
}

//add by jay start
// file_put_contents("t1.txt","o_ary".var_export($cart_row,true),FILE_APPEND);
$isSub = 0;
$o_id_ary = array();
//$_get_only=array();
foreach ((array)$cart_row as $k=>$v) {
//     if (isset($o_id_ary[$v['ProId']]))
//     {
//         continue;
//     }
    $ship_arr=array();
    $o_ary = get_shipping_methods_by_proId($address_row[0]["CId"], $address_row[0]["AId"], $v['ProId'],$v['Qty']);
    //file_put_contents("checkout.txt", "o_ary=>" . var_export($o_ary, true) . PHP_EOL . PHP_EOL);
    $p_s_all = db::get_all("product_shipping", "products_id='" . $v["ProId"] . "'");
    //file_put_contents("checkout.txt", "p_s_all=>" . var_export($p_s_all, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
    foreach ($o_ary[1] as $oir) {
        foreach ($p_s_all as $ps_item) {
            if ($oir["SId"] == $ps_item["shipping_id"]) {
                $o_id_ary[$v['ProId']][] = $oir;
                $ship_arr[]= $oir;
                break;
            }
        }
    }
    $o_id_ary[$v['ProId']][$k]=$ship_arr;
}


// file_put_contents("t1.txt","p_s_all".var_export($p_s_all,true),FILE_APPEND);
// get_shipping_methods_by_proId($address_row[0]["CId"],$address_row[0]["AId"],$proid);
function get_shipping_methods_by_proId($CId, $AId, $ProId,$Qty,$Attr="")
{
    $_POST = array('CId' => $CId,
        'AId' => $AId,
        'ProId' => $ProId,
        'Qty'=>$Qty,
        'Attr'=>$Attr
    );
    $info = get_shipping_methods();
    //file_put_contents("checkout.txt", var_export($info, true));
    return $info;

}

function get_shipping_methods()
{    //get shipping methods
    global $c;
    @extract($_POST, EXTR_PREFIX_ALL, 'p');
    //file_put_contents("checkout.txt", var_export($_POST, true), FILE_APPEND);
    $p_Type="shipping_cost";
    $p_CId = (int)$p_CId;
    $p_AId = (int)$p_AId;
    $p_ProId = (int)$p_ProId;
    $p_Qty = (int)$p_Qty;
    $info = array();
    $IsFreeShipping = $ProductPrice = 0;
    $shipping_template = @in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0;
    $provincial_freight = @in_array('provincial_freight', (array)$c['plugins']['Used']) ? 1 : 0; //开启省份运费
    $pro_info_ary = array();
    if ($p_Type == 'order') {
        $order_row = db::get_one('orders', "OId='{$p_OId}'");
        $sProdInfo = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, p.IsFreeShipping, p.TId');
        foreach ($sProdInfo as $v) {
            $price = ($v['Price'] + $v['PropertyPrice']) * $v['Qty'] * ($v['Discount'] < 100 ? $v['Discount'] / 100 : 1);
            $ProductPrice += $price;
            if ($shipping_template) {
                if (!$pro_info_ary[$v['CId']]) {
                    $pro_info_ary[$v['CId']] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0, 'OvId' => $v['OvId']);
                }
                $pro_info_ary[$v['CId']]['tWeight'] = ($v['Weight'] * $v['Qty']);
                $pro_info_ary[$v['CId']]['tVolume'] = ($v['Volume'] * $v['Qty']);
                $pro_info_ary[$v['CId']]['tQty'] = $v['Qty'];
                $pro_info_ary[$v['CId']]['Price'] = $price;
                $pro_info_ary[$v['CId']]['TId'] = $v['TId'];
                if ((int)$v['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$v['CId']]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$v['CId']]['Weight'] = ($v['Weight'] * $v['Qty']);
                    $pro_info_ary[$v['CId']]['Volume'] = ($v['Volume'] * $v['Qty']);
                    $pro_info_ary[$v['CId']]['Qty'] = $v['Qty'];
                }
            } else {
                if (!$pro_info_ary[$v['OvId']]) {
                    $pro_info_ary[$v['OvId']] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0);
                }
                $pro_info_ary[$v['OvId']]['tWeight'] += ($v['Weight'] * $v['Qty']);
                $pro_info_ary[$v['OvId']]['tVolume'] += ($v['Volume'] * $v['Qty']);
                $pro_info_ary[$v['OvId']]['tQty'] += $v['Qty'];
                $pro_info_ary[$v['OvId']]['Price'] += $price;
                if ((int)$v['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$v['OvId']]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$v['OvId']]['Weight'] += ($v['Weight'] * $v['Qty']);
                    $pro_info_ary[$v['OvId']]['Volume'] += ($v['Volume'] * $v['Qty']);
                    $pro_info_ary[$v['OvId']]['Qty'] += $v['Qty'];
                }
            }
        }
    } elseif ($p_Type == 'shipping_cost') {
        if ($p_Attr) {//产品属性
            $Attr = str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
            ksort($Attr);
        }
        $proInfo = db::get_one('products', "ProId='$p_ProId'");
        $Weight = $proInfo['Weight'];//产品默认重量
        $volume_ary = $proInfo['Cubage'] ? @explode(',', $proInfo['Cubage']) : array(0, 0, 0);
        $Volume = $volume_ary[0] * $volume_ary[1] * $volume_ary[2];
        $Volume = sprintf('%.f', $Volume);//防止数额太大，程序自动转成科学计数法
        if ((int)$proInfo['IsVolumeWeight']) {//体积重
            $VolumeWeight = ($Volume * 1000000) / 5000;//先把立方米转成立方厘米，再除以5000
            $VolumeWeight > $Weight && $Weight = $VolumeWeight;
        }
        $StartFrom = (int)$proInfo['MOQ'] > 0 ? (int)$proInfo['MOQ'] : 1;    //起订量
        $p_Qty < $StartFrom && $p_Qty = $StartFrom;    //小于起订量
        $CurPrice = cart::products_add_to_cart_price($proInfo, $p_Qty);
        $PropertyPrice = 0;
        //产品属性
        $AttrData = cart::get_product_attribute(array(
            'Type' => 0,//不用获取产品属性名称
            'ProId' => $p_ProId,
            'Price' => $CurPrice,
            'Attr' => $Attr,
            'IsCombination' => $proInfo['IsCombination'],
            'IsAttrPrice' => $proInfo['IsOpenAttrPrice'],
            'Weight' => $Weight
        ));
        $CurPrice = $AttrData['Price'];        //产品单价
        $PropertyPrice = $AttrData['PropertyPrice'];//产品属性价格
        $combinatin_ary = $AttrData['Combinatin'];    //产品属性的数据
        $OvId = $AttrData['OvId'];        //发货地ID
        $Weight = $AttrData['Weight'];        //产品重量
        $Attr = $AttrData['Attr'];        //产品属性数据
        //秒杀产品
        if ($p_proType == 2) {
            $seckill_row = str::str_code(db::get_one('sales_seckill', "SId='$p_SId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
            if ($seckill_row) {
                $CurPrice = $seckill_row['Price'];
            }
        }
        //产品数据
        if ($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime'] < $c['time'] && $c['time'] < $proInfo['EndTime']) {
            $CurPrice = $CurPrice * ($proInfo['PromotionDiscount'] / 100);
        }
        $total_price = ($CurPrice + $PropertyPrice) * $p_Qty;
        $IsFreeShipping = (int)$proInfo['IsFreeShipping'];
        $total_volume = $Volume * $p_Qty;
        $total_weight = $Weight * $p_Qty;
        $s_total_weight = $total_weight;
        $s_total_volume = $total_volume;
        //产品的海外仓数据
        $pro_info_ary[$OvId]['Weight'] = $pro_info_ary[$OvId]['tWeight'] = $total_weight;
        $pro_info_ary[$OvId]['Volume'] = $pro_info_ary[$OvId]['tVolume'] = $total_volume;
        $pro_info_ary[$OvId]['Price'] = $total_price;
        $pro_info_ary[$OvId]['Qty'] = $p_Qty;
        $pro_info_ary[$OvId]['TId'] = (int)$proInfo['TId'];
        $ProductPrice += $total_price;
        if ($IsFreeShipping == 1) {
            $pro_info_ary[$OvId]['Weight'] = $pro_info_ary[$OvId]['Volume'] = 0;
        }
    } else {
        $CIdStr = '';
        if ($p_order_cid) {
            $in_where = str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
            $CIdStr = " and CId in({$in_where})";
        }
//        file_put_contents("t.txt", "p_order_cid" . var_export($p_order_cid, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        $cartInfo = db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.' . $c['where']['cart'] . $CIdStr, 'p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr', 's.CId desc');
        $IsFreeShipping = 0;
        $total_weight = $total_volume = 0;
//        file_put_contents("t.txt", "cartInfo" . var_export($cartInfo, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        foreach ($cartInfo as $val) {
            $CartId = (int)$val['CId'];
            $OvId = 1;
            $CartAttr = str::json_data(stripslashes($val['CartAttr']), 'decode');
            if (count($CartAttr)) { //include attribute values
                foreach ((array)$CartAttr as $k => $v) {
                    if ($k == 'Overseas') { //发货地
                        $OvId = str_replace('Ov:', '', $v);
                        !$OvId && $OvId = 1;//丢失发货地，自动默认China
                    }
                }
            }
            $price = ($val['Price'] + $val['PropertyPrice']) * $val['Qty'] * ($val['Discount'] < 100 ? $val['Discount'] / 100 : 1);
            if ($shipping_template) {
                if (!$pro_info_ary[$CartId]) {
                    $pro_info_ary[$CartId] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0, 'OvId' => $OvId);
                }
                $pro_info_ary[$CartId]['tWeight'] = ($val['CartWeight'] * $val['Qty']);
                $pro_info_ary[$CartId]['tVolume'] = ($val['Volume'] * $val['Qty']);
                $pro_info_ary[$CartId]['tQty'] = $val['Qty'];
                $pro_info_ary[$CartId]['Price'] = $price;
                $pro_info_ary[$CartId]['TId'] = (int)$val['TId'];
                if ((int)$val['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$CartId]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$CartId]['Weight'] = ($val['CartWeight'] * $val['Qty']);
                    $pro_info_ary[$CartId]['Volume'] = ($val['Volume'] * $val['Qty']);
                    $pro_info_ary[$CartId]['Qty'] = $val['Qty'];
                }
            } else {
                if (!$pro_info_ary[$OvId]) {
                    $pro_info_ary[$OvId] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0);
                }
                $pro_info_ary[$OvId]['tWeight'] += ($val['CartWeight'] * $val['Qty']);
                $pro_info_ary[$OvId]['tVolume'] += ($val['Volume'] * $val['Qty']);
                $pro_info_ary[$OvId]['tQty'] += $val['Qty'];
                $pro_info_ary[$OvId]['Price'] += $price;
                if ((int)$val['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$OvId]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$OvId]['Weight'] += ($val['CartWeight'] * $val['Qty']);
                    $pro_info_ary[$OvId]['Volume'] += ($val['Volume'] * $val['Qty']);
                    $pro_info_ary[$OvId]['Qty'] += $val['Qty'];
                }
            }
            $total_weight += ($val['CartWeight'] * $val['Qty']);
            $total_volume += ($val['Volume'] * $val['Qty']);
            $ProductPrice += $price;
        }
        /*
         //产品包装重量  //没有多少客户用 4.0先注释
         $cartProAry=cart::cart_product_weight($CIdStr, 1);
         foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
         foreach((array)$v as $k2=>$v2){//$k2是ProId
         $pro_info_ary[$k]['tWeight']+=$v2;
         }
         }
         foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
         foreach((array)$v as $k2=>$v2){//$k2是ProId
         $pro_info_ary[$k]['Weight']+=$v2;
         }
         }*/
        $total_weight = $pro_info_ary[$OvId]['Weight'];
        $total_volume = $pro_info_ary[$OvId]['tVolume'];
    }
//    file_put_contents("t.txt", 'pro_info_ary' . var_export($pro_info_ary, true), FILE_APPEND);
    ksort($pro_info_ary); //排列正序
    $shipping_cfg = db::get_one('shipping_config', 'Id="1"');
    $weight = @ceil($total_weight);
    if ($_SESSION['User']['UserId']) {
        if ($p_AId) {
            $address_aid = $p_AId;
        } else {
            $address_aid = (int)$_SESSION['Cart']['ShippingAddressAId'];
        }
        if ($address_aid > 0) $StatesSId = (int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
    } else {
        $StatesSId = (int)$_SESSION['Cart']['ShippingAddress']['Province'];
    }
    $StatesSId = $p_StatesSId ? (int)$p_StatesSId : $StatesSId;
    $provincial_freight && $p_CId == 226 && $StatesSId && $area_where = " and states like '%|{$StatesSId}|%'";
    $IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
    $row = db::get_all('shipping_area a left join shipping s on a.SId=s.SId', "a.AId in(select AId from shipping_country where CId='{$p_CId}' {$area_where})", 's.Express, s.Logo, s.IsWeightArea, s.WeightArea, s.ExtWeightArea, s.VolumeArea, s.IsUsed, s.IsAPI, s.FirstWeight, s.ExtWeight, s.StartWeight, s.MinWeight, s.MaxWeight, s.MinVolume, s.MaxVolume, s.FirstMinQty, s.FirstMaxQty, s.ExtQty, s.WeightType, s.TId, a.*', 'if(s.MyOrder>0, s.MyOrder, 100000) asc, a.SId asc, a.AId asc');

    //添加获取清关费费率 所有id
    $qg_rows=array();
    foreach ((array)$row as $value) {
        $r=array();
        $r["SId"]=$value["SId"];
        $r["AffixPrice_rate"]=$value["AffixPrice_rate"]?$value["AffixPrice_rate"]:0;
        $qg_rows[]=$r;
    }
    //-------------------------------------


    $shipping_row = db::get_all('shipping', '1', 'SId, TId');
    $row_ary = $shipping_tid_ary = array();
    foreach ((array)$row as $v) {
        !$row_ary[$v['SId']] && $row_ary[$v['SId']] = array('info' => $v, 'overseas' => array());
        $row_ary[$v['SId']]['overseas'][$v['OvId']] = $v;
    }
    $shipping_template && $DefaultTId = db::get_value('shipping_template', 'IsDefault=1', 'TId');
    foreach ((array)$shipping_row as $v) {
        if ($shipping_template) $shipping_tid_ary[$v['TId']][] = $v['SId'];
    }
    unset($row);
    foreach ((array)$row_ary as $key => $val) {
        $row = $val['info'];
        $isOvId = 0;
        foreach ((array)$pro_info_ary as $k => $v) {
            if ($shipping_template && $p_Type != 'shipping_cost') {
                $val['overseas'][$v['OvId']] && $isOvId += 1;
            } else {
                $val['overseas'][$k] && $isOvId += 1;
            }
        }//循环产品数据
        if ($isOvId == 0 && $pro_info_ary['1']) {
            $info[1][] = array('SId' => '', 'Name' => '', 'Brief' => '', 'IsAPI' => '', 'type' => '', 'ShippingPrice' => '-1');
            continue;
        }
        /************************************** 新增折后之后计算运费 开始**********************************/
        $_total_price = orders::orders_discount_total_price($ProductPrice, $_SESSION['Cart']['Coupon'], $_SESSION['User']['UserId'], $p_order_cid, $p_ProId);
        $pro_info_ary = orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
        /************************************** 新增折后之后计算运费 结束**********************************/
        //循环产品数据 Start
        foreach ((array)$pro_info_ary as $k => $v) {
            $overseas = $val['overseas'][$k];
            $shipping_template && $p_Type != 'shipping_cost' && $overseas = $val['overseas'][$v['OvId']];
            if ($shipping_template && (!in_array($key, (array)$shipping_tid_ary[$v['TId']]) || ($DefaultTId && !$shipping_tid_ary[$v['TId']] && !in_array($key, (array)$shipping_tid_ary[$DefaultTId])))) continue;
            $open = 0;//默认不通过
            if (in_array($row['IsWeightArea'], array(0, 1, 2)) && ((float)$row['MaxWeight'] ? ($v['tWeight'] >= $row['MinWeight'] && $v['tWeight'] <= $row['MaxWeight']) : ($v['tWeight'] >= $row['MinWeight']))) {//重量限制
                $open = 1;
            } elseif ($row['IsWeightArea'] == 4 && ($v['tWeight'] >= $row['MinWeight'] || $v['tVolume'] >= $row['MinVolume'])) {//重量限制+体积限制
                $open = 1;
            } elseif ($row['IsWeightArea'] == 3) {//按数量计算，直接不限制
                $open = 1;
            }
            if ($overseas && (int)$row['IsUsed'] == 1 && $open == 1) {
                $sv = array(
                    'SId' => $row['SId'],
                    'Name' => $overseas['Express'],
                    'Logo' => ($overseas['Logo'] && is_file($c['root_path'] . $overseas['Logo'])) ? $overseas['Logo'] : '',
                    'Brief' => $overseas['Brief'],
                    'IsAPI' => $overseas['IsAPI'],
                    'type' => '',
                    'weight' => $v['Weight']
                );
                if ($IsFreeShipping || ($v['IsFreeShipping'] == 1 && $v['Weight'] == 0) || ((int)$c['config']['products_show']['Config']['freeshipping'] && $v['Weight'] == 0) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingPrice'] > 0 && $v['Price'] >= $overseas['FreeShippingPrice']) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingWeight'] > 0 && $v['Weight'] < $overseas['FreeShippingWeight']) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingPrice'] == 0 && $overseas['FreeShippingWeight'] == 0)) {
                    $shipping_price = 0;
                } else {
                    $shipping_price = 0;
                    if ($overseas['IsWeightArea'] == 1 || ($overseas['IsWeightArea'] == 2 && $v['Weight'] >= $overseas['StartWeight'])) {
                        //重量区间 重量混合
                        $WeightArea = str::json_data($overseas['WeightArea'], 'decode');
                        $WeightAreaPrice = str::json_data($overseas['WeightAreaPrice'], 'decode');
                        $areaCount = count($WeightArea) - 1;
                        foreach ((array)$WeightArea as $k2 => $v2) {
                            if ($k2 <= $areaCount && (($WeightArea[$k2 + 1] && $v['Weight'] < $WeightArea[$k2 + 1]) || (!$WeightArea[$k2 + 1] && $v['Weight'] >= $v2))) {
                                if ($overseas['WeightType'] == 1) {//按每KG计算
                                    $shipping_price = $WeightAreaPrice[$k2] * $v['Weight'];
                                } else {//按整价计算
                                    $shipping_price = $WeightAreaPrice[$k2];
                                }
                                break;
                            }
                        }
                        //$v['Weight']>$WeightArea[$areaCount] && $shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
                    } elseif ($overseas['IsWeightArea'] == 3) {
                        //按数量
                        $shipping_price = $overseas['FirstQtyPrice'];//先收取首重费用
                        $ExtQtyValue = $v['Qty'] > $overseas['FirstMaxQty'] ? $v['Qty'] - $overseas['FirstMaxQty'] : 0;//超出的数量
                        if ($ExtQtyValue) {//续重
                            $shipping_price += (float)(@ceil($ExtQtyValue / $overseas['ExtQty']) * $overseas['ExtQtyPrice']);
                        }
                    } elseif ($overseas['IsWeightArea'] == 4) {
                        //重量体积混合计算
                        $weight_shipping_price = $volume_shipping_price = 0;
                        if ($v['Weight'] >= $overseas['MinWeight']) {//重量
                            $WeightArea = str::json_data($overseas['WeightArea'], 'decode');
                            $WeightAreaPrice = str::json_data($overseas['WeightAreaPrice'], 'decode');
                            $areaCount = count($WeightArea) - 1;
                            foreach ((array)$WeightArea as $k2 => $v2) {
                                if ($k2 <= $areaCount && (($WeightArea[$k2 + 1] && $v['Weight'] < $WeightArea[$k2 + 1]) || (!$WeightArea[$k2 + 1] && $v['Weight'] >= $v2))) {
                                    if ($overseas['WeightType'] == 1) {//按每KG计算
                                        $weight_shipping_price = $WeightAreaPrice[$k2] * $v['Weight'];
                                    } else {//按整价计算
                                        $weight_shipping_price = $WeightAreaPrice[$k2];
                                    }
                                    break;
                                }
                            }
                            $overseas['WeightType'] == 1 && $v['Weight'] > $WeightArea[$areaCount] && $weight_shipping_price = $WeightAreaPrice[$areaCount] * $v['Weight'];
                        }
                        if ($v['Volume'] >= $overseas['MinVolume']) {//体积
                            $VolumeArea = str::json_data($overseas['VolumeArea'], 'decode');
                            $VolumeAreaPrice = str::json_data($overseas['VolumeAreaPrice'], 'decode');
                            $areaCount = count($VolumeArea) - 1;
                            foreach ((array)$VolumeArea as $k2 => $v2) {
                                if ($k2 <= $areaCount && (($VolumeArea[$k2 + 1] && $v['Volume'] < $VolumeArea[$k2 + 1]) || (!$VolumeArea[$k2 + 1] && $v['Volume'] >= $v2))) {
                                    $volume_shipping_price = $VolumeAreaPrice[$k2] * $v['Volume'];
                                    break;
                                }
                            }
                            $v['Volume'] > $VolumeArea[$areaCount] && $volume_shipping_price = $VolumeAreaPrice[$areaCount] * $v['Volume'];
                        }
                        $shipping_price = max($weight_shipping_price, $volume_shipping_price);
                    } else {
                        //首重续重
                        $ExtWeightArea = str::json_data($overseas['ExtWeightArea'], 'decode');
                        $ExtWeightAreaPrice = str::json_data($overseas['ExtWeightAreaPrice'], 'decode');
                        $areaCount = count($ExtWeightArea) - 1;
                        $ExtWeightValue = $v['Weight'] > $overseas['FirstWeight'] ? $v['Weight'] - $overseas['FirstWeight'] : 0;//超出的重量
                        if ($areaCount > 0) {
                            $shipping_price = $overseas['FirstPrice'];//先收取首重费用
                            foreach ((array)$ExtWeightArea as $k2 => $v2) {
                                if ($v['Weight'] > $v2 && $ExtWeightArea[$k2 + 1]) {
                                    $ext = $v['Weight'] > $ExtWeightArea[$k2 + 1] ? ($ExtWeightArea[$k2 + 1] - $v2) : ($v['Weight'] - $v2);
                                    $shipping_price += (float)(@ceil(sprintf('%01.4f', $ext / $overseas['ExtWeight'])) * $ExtWeightAreaPrice[$k2]);
                                } elseif ($v['Weight'] > $v2 && !$ExtWeightArea[$k2 + 1]) {//达到以上费用
                                    $ext = $v['Weight'] - $v2;
                                    $shipping_price += (float)(@ceil(sprintf('%01.4f', $ext / $overseas['ExtWeight'])) * $ExtWeightAreaPrice[$k2]);
                                }
                            }
                        } else {
                            $ExtWeightCount = sprintf('%01.4f', $ExtWeightValue / $overseas['ExtWeight']);
                            $ExtWeightCountdecimal = floor($ExtWeightCount);
                            if (strstr($ExtWeightCount, '.') != false && ($ExtWeightCount - $ExtWeightCountdecimal) > 0) {
                                $ExtWeightCount = @ceil($ExtWeightCount);
                            }
                            $shipping_price = (float)($ExtWeightCount * $ExtWeightAreaPrice[0] + $overseas['FirstPrice']);
                        }
                    }
                    if ($overseas['AffixPrice']) {//附加费用
                        $shipping_price += $overseas['AffixPrice'];
                    }
                }
                $sv['ShippingPrice'] = cart::iconv_price($shipping_price, 2, '', 0);
                $sv['InsurancePrice'] = cart::get_insurance_price_by_price($v['Price']);
                $info[$k][] = $sv;
            }
        }
        //循环产品数据 End
    }
    if ($info) {
        $info_ary = array();
        foreach ((array)$info as $k => $v) {
            $sort_ary = $price_ary = array();
            foreach ((array)$v as $k2 => $v2) { //
                $price_ary[$v2['ShippingPrice']][] = $k2;
            }
            ksort($price_ary);
            foreach ((array)$price_ary as $k2 => $v2) {
                foreach ((array)$v2 as $k3 => $v3) {
                    $info_ary[$k][] = $info[$k][$v3];
                }
            }
        }

        //添加清关费到返回值
        foreach ((array)$info_ary as $k=>&$v) {
            foreach ((array)$v as $k2=>&$v2) { //
                foreach ($qg_rows as $q_k=>$q_v)
                {
                    if ($v2["SId"]==$q_v['SId'])
                    {
                        $info_ary[$k][$k2]["AffixPrice_rate"]=$q_v["AffixPrice_rate"];
                        break;
                    }
                }
            }
        }
        //-------------

        return $info_ary;
        ly200::e_json(array('Symbol' => cart::iconv_price(0, 1), 'info' => $info_ary, 'IsInsurance' => $IsInsurance, 'total_weight' => $total_weight, 'shipping_template' => (int)$shipping_template), 1);
    } else {
        return false;
    }
}

//add by jay end

if ((int)$c['config']['global']['Overseas'] == 0 || count($c['config']['Overseas']) < 2 || count($oversea_id_ary) < 2) {//关闭海外仓功能 或者 仅有一个海外仓选项
    ?>
    <style type="text/css">.checkout_shipping .shipping:first-child .title {
            display: none;
        }</style>
    <?php
}
$oversea_id_hidden = $oversea_id_ary;
$NotDefualtOvId = 0;
if (!in_array(1, $oversea_id_ary)) {//购物车没有默认海外仓追加隐藏选项
    $oversea_id_ary[] = 1;
    $NotDefualtOvId = 1;
}
sort($oversea_id_ary); //排列正序
$oversea_count = count($oversea_id_ary);
?>
    <script type="text/javascript">
        var address_perfect =<?=(int)$_SESSION['User']['UserId'] && (!$address_row || ($address_row && (!$address_row[0]['FirstName'] || !$address_row[0]['LastName'] || !$address_row[0]['AddressLine1'] || !$address_row[0]['City'] || !$address_row[0]['CId'] || ($IsStates > 0 && !$address_row[0]['SId']) || !$address_row[0]['ZipCode'] || !$address_row[0]['PhoneNumber']))) ? 1 : 0;?>; //会员的收货地址信息是否缺失
        var address_perfect_aid =<?=$address_row ? (int)$address_row[0]['AId'] : 0;?>;
        if (!$('html').loginOrVisitors()) { //跳转到登录页面
            window.top.location.href = '/account/login.html?&jumpUrl=' + decodeURIComponent('<?=$_SERVER['REQUEST_URI'];?>');
        }
        $(function () {
            // cart_obj.cart_checkout();
            cart_obj.cart_checkout_v2();
            <?php
            if ($_SESSION['Cart']['ShippingAddress']) {
                echo 'cart_obj.cart_init.checkout_no_login(' . str::json_data($_SESSION['Cart']['ShippingAddress']) . ');';
            }
            if ($c['NewFunVersion'] >= 4) {
                //新用户版本 和 Paypal支付
                echo 'cart_obj.paypal_init();';
            }?>
        });
    </script>
    <style type="text/css">
        .checkout_shipping .shipping:hover .icon_shipping_title, .checkout_shipping .current .icon_shipping_title, .checkout_shipping .icon_shipping_title {
            background-color: <?=$style_data['BuyNowBgColor'];?>;
        }

        .checkout_shipping .list li.current .icon > i, .checkout_payment .payment_row.current .icon > i {
            background-color: <?=$style_data['FontColor'];?>;
            border-color: <?=$style_data['FontColor'];?>;
        }
    </style>
    <div id="cart">
        <div class="cart_head ui_border_b"><?= html::website_h1('logo', '', '', $logo); ?></div>
        <div class="cart_checkout">
            <div class="checkout_error_tips hide" data-country="<?= $address_row[0]['Country']; ?>"
                 data-email="<?= $c['config']['global']['AdminEmail']; ?>"></div>
            <?php if ((int)$_SESSION['User']['UserId'] == 0) { ?>
                <div class="checkout_box checkout_customer ui_border_b">
                    <div class="box_title"><?= $c['lang_pack']['cart']['customerInfo']; ?></div>
                    <div class="box_content">
                        <div class="checkout_login"><?= $c['lang_pack']['cart']['already']; ?> <a
                                    href="/account/sign-up.html"
                                    class="SignInButton btn_signin"><?= $c['lang_pack']['cart']['login']; ?></a></div>
                        <label class="input_box">
                            <span class="input_box_label"><?= $c['lang_pack']['mobile']['enter_email']; ?></span>
                            <input type="text" class="input_box_txt elmbBlur" name="Email"
                                   placeholder="<?= $c['lang_pack']['mobile']['enter_email']; ?>" maxlength="200"/>
                        </label>
                        <p class="error"></p>
                        <input type="hidden" name="jumpUrl" value="<?= $_SERVER['REQUEST_URI']; ?>"/>
                    </div>
                </div>
                <div class="checkout_divide ui_border_b"></div>
            <?php } ?>
            <div class="checkout_box checkout_address ui_border_b">
                <div class="box_title"><?= $c['lang_pack']['cart']['address']; ?></div>
                <div class="box_content">
                    <?php if ((int)$_SESSION['User']['UserId']) { ?>
                        <div class="address_button clearfix"><a
                                    href="<?= (int)$_SESSION['User']['UserId'] ? '/account/address/?Shipping=1' : 'javascript:;'; ?>"
                                    class="btn_global btn_address_more"
                                    id="moreAddress"><?= $c['lang_pack']['cart']['add']; ?> / <?= $c['lang_pack']['cart']['edit']; ?>
                                / <?= $c['lang_pack']['more']; ?></a></div>
                    <?php } ?>
                    <div class="address_default item clearfix">
                        <?php if ($address_row) { ?>
                            <p class="clearfix">
                                <strong><?= $address_row[0]['FirstName'] . ' ' . $address_row[0]['LastName']; ?></strong>
                            </p>
                            <p class="address_line"><?= $address_row[0]['AddressLine1'] . ' ' . ($address_row[0]['AddressLine2'] ? $address_row[0]['AddressLine2'] . ' ' : ''); ?></p>
                            <p><?= $address_row[0]['City'] . ' ' . ($address_row[0]['StateName'] ? $address_row[0]['StateName'] : $address_row[0]['State']) . ' ' . $address_row[0]['Country'] . ' (' . $address_row[0]['ZipCode'] . ')'; ?></p>
                            <p>+<?= $address_row[0]['CountryCode'] . ' ' . $address_row[0]['PhoneNumber']; ?></p>
                        <?php } ?>
                    </div>
                    <?php if (!(int)$_SESSION['User']['UserId']) { ?>
                        <div id="ShipAddrFrom"><?php include('include/shippingAddress.php'); ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="checkout_divide ui_border_b"></div>

            <div class="checkout_box checkout_payment ui_border_b">
                <div class="box_title"><?= $c['lang_pack']['cart']['paymethod']; ?></div>
                <div class="box_content">
                    <?php
                    foreach ((array)$payment_row as $v) {
                        $name = $v['Name' . $c['lang']] ? $v['Name' . $c['lang']] : $v['Name_en'];
                        ?>
                        <div class="payment_row clean" min="<?=cart::iconv_price($v['MinPrice'], 2, '', 0); ?>"
                             max="<?= cart::iconv_price($v['MaxPrice'], 2, '', 0); ?>" pid="<?= $v['PId']; ?>"
                             method="<?= $v['Method']; ?>">
                            <em class="icon"><i></i></em>
                            <div class="img pic_box"><img src="<?= $v['LogoPath']; ?>"
                                                          alt="<?= $name; ?>"/><span></span>
                            </div>
                            <div class="payment_contents" fee="<?= $v['AdditionalFee']; ?>"
                                 affix="<?= cart::iconv_price($v['AffixPrice'], 2, '', 0); ?>">
                                <?php if ($v['Description' . $c['lang']]) { ?>
                                        <div class="desc"><?= $v['Description' . $c['lang']] ?></div><?php } ?>
                            </div>
                            <?php if($v['PId'] == 11){ ?>
                            <br>
                            <!--<span>--><?//=$c['lang_pack']['user']['balance']; ?><!----><?//=$balance; ?><!--</span>-->
                                <span style="font-weight: bold;"><?=$c['lang_pack']['user']['yourbalance']; ?>&nbsp;&nbsp;<?=$balance; ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="checkout_divide ui_border_b"></div>
            <div class="checkout_box">
                <div class="box_title"><?= $c['lang_pack']['mobile']['summary']; ?></div>
                <div class="box_content">
                    <div class="cart_item_list">
                        <?php
                        //add by jay start
                        $ptotal = 0;
                        $total_price=$s_total_price=$quantity=0;
                        $deposit_ot_total_price=0;
                        //add by jay end
                        $cart_attr = $cart_attr_data = array();
                        foreach ($cart_row as $cr_key=>$item) {
                            $pro_id = 0;

                            if ($item['BuyType'] == 4) {
                                //组合促销
                                $package_row = str::str_code(db::get_one('sales_package', "PId='{$item['KeyId']}'"));
                                !$package_row && $error_ary["{$item['ProId']}_{$item['CId']}"] = 1;
                                $products_row = str::str_code(db::get_all('products', "ProId='{$package_row['ProId']}'"));
                                $pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
                                $pro_where == '' && $pro_where = 0;
                                $products_row = array_merge($products_row, str::str_code(db::get_all('products', "ProId in($pro_where)")));
                                $data_ary = str::json_data(htmlspecialchars_decode($package_row['Data']), 'decode');
                                $item['Package'] = $package_row;
                                $item['ProRow'] = $products_row;
                                $item['Data'] = $data_ary;
                            }
                            $attr = array();
                            $item['Property'] != '' && $attr = str::json_data($item['Property'], 'decode');
                            $price = $item['Price'] + $item['PropertyPrice'];
                            $item['Discount'] < 100 && $price *= $item['Discount'] / 100;
                            $img = ly200::get_size_img($item['PicPath'], '240x240');
                            $url = ly200::get_url($item, 'products');
                            $total_weight += $item['Weight'] * $item['Qty'];
                            //add by jay start
                            $s_total_price += $price*$item['Qty'];
                            $total_price += cart::iconv_price($price, 2, '', 0)*$item['Qty'];
                            $deposit_ot_total_price += $price * $item['Qty'] * $item["price_rate"];
                            $quantity+=$item['Qty'];
                            //add by jay end

                            $foreach_ary = array();
                            if ($item['BuyType'] == 4) {
                                //组合促销
                                $foreach_ary = $item['ProRow'];
                            } else {
                                //普通产品
                                $foreach_ary[0] = $item;
                            }
                            $last_one = count($foreach_ary) - 1;
                            foreach ((array)$foreach_ary as $k => $v) {
                                $pro_id = $v['ProId'];
                                $attr_show = '';
                                if ($item['BuyType'] == 4) {
                                    //组合促销
                                    $img = ly200::get_size_img($v['PicPath_0'], '240x240');
                                    $url = ly200::get_url($v, 'products');
                                    $v['Qty'] = 1;
                                    if ($k == 0) echo '<strong class="sales_title">[ ' . $c['lang_pack']['cart']['package'] . ' ] ' . $item['Package']['Name'] . '</strong>'; //组合促销标题
                                }
                                ?>
                                <div cid="<?= $item['CId']; ?>" ovid="<?= $v['OvId']; ?>"
                                     class="item clean<?= $item['BuyType'] == 4 && $k > 0 ? ' sales_item' : ''; ?>">
                                    <div class="img fl">
                                        <a href="<?= $url; ?>"><img src="<?= $img; ?>"
                                                                    alt="<?= $v['Name' . $c['lang']]; ?>"></a>
                                        <?php if ($item['BuyType'] != 4 || ($item['BuyType'] == 4 && $k == 0)) { ?>
                                            <div class="quantity"><?= $v['Qty']; ?></div>
                                        <?php } ?>
                                    </div>
                                    <div class="info">
                                        <div class="name"><a href="<?= $url; ?>"><?= $v['Name' . $c['lang']]; ?></a>
                                        </div>
                                        <?php
                                        if ($item['BuyType'] == 4) {
                                            //组合促销
                                            if ($item['Data'][$v['ProId']]) {
                                                $OvId = 1;
                                                $j = 0;
                                                foreach ((array)$item['Data'][$v['ProId']] as $k3 => $v3) {
                                                    if ($k3 == 'Overseas') {
                                                        //发货地
                                                        $OvId = str_replace('Ov:', '', $v3);
                                                        if ((int)$c['config']['global']['Overseas'] == 0 || $OvId == 1) continue; //发货地是中国，不显示
                                                        $attr_show .= ($j > 0 ? ' , ' : '') . $c['config']['Overseas'][$OvId]['Name' . $c['lang']];
                                                    } else {
                                                        $attr_show .= ($j > 0 ? ' , ' : '') . $vid_data_ary[$k3][$v3];
                                                    }
                                                    ++$j;
                                                }
                                                if ((int)$c['config']['global']['Overseas'] == 1 && $OvId == 1) {
                                                    $attr_show .= ($attr_show ? ' , ' : '') . $c['config']['Overseas'][$OvId]['Name' . $c['lang']];
                                                }
                                            }
                                        } else {
                                            //普通产品
                                            if (count($attr)) {
                                                $i = 0;
                                                foreach ($attr as $k => $z) {
                                                    if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                    $attr_show .= ($i > 0 ? ' , ' : '') . $z;
                                                    ++$i;
                                                }
                                            }
                                            if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                $attr_show .= ($attr_show ? ' , ' : '') . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']];
                                            }
                                        }
                                        if ($attr_show) {
                                            echo '<div class="rows">' . $attr_show . '</div>';
                                        } ?>
                                        <?php
                                        if ($item['BuyType'] != 4 || ($item['BuyType'] == 4 && $k == 0)) {
                                            ?>
                                            <div class="i_shipping clean"></div>
                                            <div class="price"><?= $_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price($price); ?></div>
                                        <?php } ?>
                                        <?php
                                        if ($item['BuyType'] != 4) {
                                            ?>
                                            <div class="remark"><input type="text" name="Remark[]"
                                                                       value="<?= $v['Remark']; ?>" maxlength="200"
                                                                       placeholder="<?= $c['lang_pack']['cart']['remark']; ?>"
                                                                       cid="<?= $item['CId']; ?>"
                                                                       proid="<?= $v['ProId']; ?>"/></div>
                                        <?php } ?>
                                    </div>
                                </div>


                                <?php
                            }
                            ?>

                            <!--add by jay start-->
                            <div class="checkout_box checkout_shipping ui_border_b">
                                <div class="box_title<?= $oversea_count > 1 ? ' ui_border_b' : ''; ?>"><?= $c['lang_pack']['cart']['shipmethod']; ?></div>
                                <div class="box_content" >

                                    <?php foreach ($oversea_id_ary as $k => $v) { ?>
                                        <div class="shipping"
                                             cid="<?= $item['CId']; ?>"
                                             proid="<?= $pro_id; ?>"
                                             data-id="<?= $v; ?>"
                                             pro-price="<?= $price; ?>"
                                             pro-qty="<?= $item['Qty']; ?>">
                                            <div class="title">
                                                <strong><?= $c['config']['Overseas'][$v]['Name' . $c['lang']]; ?></strong>
                                                <div class="shipping_info"><span
                                                            class="error"><?= $c['lang_pack']['cart']['notDeliveries']; ?></span><span
                                                            class="name"></span><span class="price"></span></div>
                                                <i class="icon_shipping_title"></i>
                                            </div>
                                            <div class="list" style="display: block;">
                                                <ul class="shipping_method_list clearfix">

                                                    <?php
                                                    if (!isset($o_id_ary[$item['ProId']])){
                                                        $isSub++;
                                                    }

//                                                    file_put_contents("checkout.txt", '$o_id_ary=>' . var_export($o_id_ary, true), FILE_APPEND);
                                                    foreach ($o_id_ary as $ke => $s_valu) {
                                                        if ($ke != $item['ProId']) {
                                                            continue;
                                                        }
                                                        $real_ship_arr=array();
                                                        foreach ($s_valu as $kk=>$vv)
                                                        {
                                                            if ($cr_key==$kk)
                                                            {
                                                                $real_ship_arr=$vv;
                                                            }
                                                        }
                                                        $index = 0;
                                                        foreach ($real_ship_arr as $s_k => $val) {
                                                            ?>

                                                            <li name="<?=$val['Name']?>">
                                                                <em class="icon"><i></i></em>
                                                                <span class="name">
                                                                <input type="radio" name="_shipping_method<?=$index ?>"
                                                                       value="<?= $val['SId'] ?>"
                                                                       price="<?= $val['ShippingPrice'] ?>"
                                                                       insurance="<?= $val['InsurancePrice'] ?>"
                                                                       ShippingType="<?= $val['type'] ?>"
                                                                       cid="<?= $val['CId'] ?>"
                                                                       affixprice="<?= $val['AffixPrice_rate'] ?>"
                                                                       method="<?= $val['Name'] ?>"/>
                                                                <label><?= $val['Name'] ?></label>
                                                                    <?php
                                                                    if ($val['IsAPI']) {
                                                                        ?>
                                                                        <span class="price waiting"></span>
                                                                        <?php
                                                                    } else {
                                                                        if ($val['ShippingPrice'] > 0) {
                                                                            ?>
                                                                            <span class="price"><?= cart::iconv_price($val["ShippingPrice"]); ?></span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <span class="price free_shipping"><?= $c['lang_pack']['products']['Free'] ?></span>
                                                                            <?php

                                                                        }
                                                                    }
                                                                    ?>
                                                            </span>
                                                                <span class="brief"
                                                                      title="<?= $val['Brief'] ?>"><?= $val['Brief'] ?></span>
                                                                <div class="clear"></div>
                                                            </li>
                                                            <?php
                                                            $index++;
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                                <?php if ($IsInsurance) { ?>
                                                    <div class="checkbox_wrapper insurance">
                                                        <div class="checkbox_input">
                                                            <input type="checkbox" name="_shipping_method_insurance"
                                                                   class="checkbox_check shipping_insurance"
                                                                   id="shipping_insurance_<?= $v; ?>" value="1"
                                                                   checked/>
                                                        </div>
                                                        <label class="checkbox_label"
                                                               for="shipping_insurance_<?= $v; ?>">
                                                            <span><?= $c['lang_pack']['cart']['add_insur']; ?></span>
                                                            <span class="price"><?= $_SESSION['Currency']['Symbol']; ?>
                                                                <em></em></span>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="checkout_divide ui_border_b"></div>


                            <!--add by jay end-->
                            <?php
                        } ?>
                    </div>
                    <?php if ($c['FunVersion'] >= 1) { ?>
                        <div class="checkout_coupon">
                            <div class="clean code_input">
                                <label class="input_box fl">
                                    <span class="input_box_label"><?= $c['lang_pack']['mobile']['coupon_code']; ?></span>
                                    <input type="text" name="couponCode" class="box_input"
                                           placeholder="<?= $c['lang_pack']['mobile']['coupon_code']; ?>"/>
                                </label>
                                <div class="btn_global btn_submit fr btn_coupon_disabled"
                                     id="coupon_apply"><?= $c['lang_pack']['mobile']['apply']; ?></div>
                                <div class="clear"></div>
                                <p class="error"></p>
                            </div>
                            <div class="code_valid clean" id="code_valid" style="display:none;">
                                <div class="coupon_code_box">
                                    <em class="icon fl"></em>
                                    <span class="coupon_code_text fl"></span>
                                    <input type="button" value="<?= $c['lang_pack']['mobile']['remove']; ?>"
                                           class="coupon_code_remove fr" id="removeCoupon"/>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <form id="PlaceOrderFrom" method="post" action="?" amountPrice="<?= $iconv_total_price; ?>"
                          userPrice="<?= (($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0 > $AfterPrice_1)) ? $AfterPrice_0 : 0; ?>">
                        <div class="checkout_summary ui_border_t">
                            <div class="clean"><!-- subtotal -->
                                <div class="key"><?= $c['lang_pack']['mobile']['subtotal']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                            id="ot_subtotal"><?= cart::currency_format($iconv_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                                </div>
                            </div>
                            <?php if (($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0 > $AfterPrice_1)) { ?>
                                <div class="clean" id="memberSavings"><!-- Member Savings -->
                                    <div class="key"><?= $c['lang_pack']['cart']['user_save']; ?></div>
                                    <div class="value">- <?= $_SESSION['Currency']['Symbol']; ?><span
                                                id="ot_user"><?= cart::currency_format($AfterPrice_0, 0, $_SESSION['Currency']['Currency']); ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="clean" id="shipping_charges" ><!-- Shipping Charges -->
                                <div class="key"><?= $c['lang_pack']['cart']['shipcharge']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                            id="ot_shipping">0</span>
                                </div>
                            </div>
                            <div class="clean" id="shipping_and_insurance" style="display:none;"><!-- Shipping Insurance combine -->
                                <div class="key"><?= $c['lang_pack']['mobile']['ship_and_ins']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span
                                            id="ot_combine_shippnig_insurance">0</span></div>
                            </div>
                            <div class="clean" style="display:none;" id="couponSavings"><!-- Coupon Savings -->
                                <div class="key"><?= $c['lang_pack']['mobile']['coupon_save']; ?></div>
                                <div class="value">- <?= $_SESSION['Currency']['Symbol']; ?><span
                                            id="ot_coupon">0</span>
                                </div>
                            </div>
                            <div class="clean" style="display:block;" id="depositCharge"><!-- Deposit Charge -->
                                <div class="key"><?= $c['lang_pack']['cart']['deposit']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?>
                                    <span id="ot_deposit"><?=cart::currency_format($deposit_ot_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                                </div>
                            </div>
                            <?php
                            $cutprice = 0;
                            if (($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1 > $AfterPrice_0)) {
                                ?>
                                <div class="clean" id="subtotalDiscount"><!-- subtotal discount -->
                                    <div class="key"><?= $c['lang_pack']['cart']['save']; ?></div>
                                    <div class="value">- <?= $_SESSION['Currency']['Symbol']; ?><span
                                                id="ot_subtotal_discount"><?= cart::currency_format($AfterPrice_1, 0, $_SESSION['Currency']['Currency']); ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="clean" style="display:none;" id="serviceCharge"><!-- Service Charge -->
                                <div class="key"><?= $c['lang_pack']['cart']['fee']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span id="ot_fee">0</span>
                                </div>
                            </div>
                            <div class="clean ui_border_t" style="padding-top: 40px;" id="total"><!-- Total -->
                                <div class="key"><?= $c['lang_pack']['mobile']['total']; ?></div>
                                <div class="value"><?= $_SESSION['Currency']['Symbol']; ?><span id="ot_total">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="checkout_button">
                            <div class="btn_global btn BuyNowBgColor btn_disabled"
                                 id="cart_checkout">
                                <?= $c['lang_pack']['mobile']['place_order']; ?>
                                <br>
                                <?= $c['lang_pack']['cart']['deposit']; ?>: <span id="ot_deposit"><?=cart::currency_format($deposit_ot_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                            </div>

                            <?php if ($c['NewFunVersion'] >= 4) {//新用户版本 和 Paypal支付?>
                                <div id="paypal_button_container">

                                </div>
                            <?php } ?>
                            <a href="<?= (int)$_GET['ueeshop_store'] == 0 ? '/cart/' : 'javascript:history.back();' ?>"
                               class="btn_back"><em></em>Return to Cart</a>
                        </div>
                        <input type="hidden" name="order_coupon_code" value="<?= $_SESSION['Cart']['Coupon']; ?>"
                               cutprice="0"/>
                        <input type="hidden" name="order_discount_price"
                               value="<?= ($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1 > $AfterPrice_0) ? $AfterPrice_1 : 0; ?>"/>
                        <input type="hidden" name="order_shipping_address_aid"
                               value="<?= $address_row ? $address_row[0]['AId'] : 0; ?>"/>
                        <input type="hidden" name="order_shipping_address_cid"
                               value="<?= $CountryId ? $CountryId : -1; ?>"/>
                        <input type="hidden" name="order_shipping_method_sid" value="[]"/>
                        <input type="hidden" name="order_shipping_method_type" value="[]"/>
                        <input type="hidden" name="order_shipping_price" value="[]"/>
                        <input type="hidden" name="order_shipping_insurance" value="[]" price="[]"/>
                        <input type="hidden" name="order_shipping_oversea"
                               value="<?= $oversea_id_hidden ? implode(',', $oversea_id_hidden) : ''; ?>"/>
                        <input type="hidden" name="order_payment_method_pid" value="-1"/>
                        <input type="hidden" name="order_products_attribute_error"
                               value="<?= (int)$products_attribute_error; ?>"/>
                        <input type="hidden" name="shipping_method_where"
                               value="&ProId=<?= $cart_row[0]['ProId']; ?>&Qty=<?= $cart_row[0]['Qty']; ?>&Type=shipping_cost"
                               attr="<?= str::str_code(str::json_data($Attr)); ?>"/>
                        <?php if ($CId) { ?><input type="hidden" name="order_cid" value="<?= $CId; ?>" /><?php } ?>
                        <input type="hidden" name="shipping_template"
                               value="<?= @in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0; ?>"/>
                        <!--add by jay start-->
                        <input type="hidden" name="order_shipping_info" value=""/>
                        <input type="hidden" name="order_affix_price" value=""/>
                        <input type="hidden" name="order_deposit_price" value="<?=$deposit_ot_total_price?>" />
                        <!--add by jay end-->

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="payment_ready">
        <div class="load">
            <div class="load_payment">
                <div class="load_image"></div>
                <div class="load_loader"></div>
            </div>
        </div>
        <div class="info">
            <p><?= $c['lang_pack']['cart']['payment_tip_0']; ?></p>
            <p><?= $c['lang_pack']['cart']['payment_tip_1']; ?></p>
        </div>
    </div>
<script>

</script>
<?php
if (!$isSub){
    echo '<script>$("#cart_checkout").removeClass("btn_disabled")</script>';
}
/**
 * <div class="checkout_box checkout_shipping ui_border_b">
 * <div class="box_title<?=$oversea_count>1?' ui_border_b':'';?>"><?=$c['lang_pack']['cart']['shipmethod'];?></div>
 * <div class="box_content">
 * <?php foreach($oversea_id_ary as $k=>$v){?>
 * <div class="shipping<?=($NotDefualtOvId && (int)$v==1)?' hide':'';?>" data-id="<?=$v;?>">
 * <div class="title">
 * <strong><?=$c['config']['Overseas'][$v]['Name'.$c['lang']];?></strong>
 * <div class="shipping_info"><span class="error"><?=$c['lang_pack']['cart']['notDeliveries'];?></span><span class="name"></span><span class="price"></span></div>
 * <i class="icon_shipping_title"></i>
 * </div>
 * <div class="list">
 * <ul class="shipping_method_list clearfix"></ul>
 * <?php if($IsInsurance){?>
 * <div class="checkbox_wrapper insurance">
 * <div class="checkbox_input">
 * <input type="checkbox" name="_shipping_method_insurance" class="checkbox_check shipping_insurance" id="shipping_insurance_<?=$v;?>" value="1" checked />
 * </div>
 * <label class="checkbox_label" for="shipping_insurance_<?=$v;?>">
 * <span><?=$c['lang_pack']['cart']['add_insur'];?></span>
 * <span class="price"><?=$_SESSION['Currency']['Symbol'];?><em></em></span>
 * </label>
 * </div>
 * <?php }?>
 * </div>
 * </div>
 * <?php }?>
 * </div>
 * </div>
 * <div class="checkout_divide ui_border_b"></div>
 */
?>