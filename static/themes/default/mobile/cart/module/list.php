<?php !isset($c) && exit();?>
<?php
$cart_row = db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', 'c.' . $c['where']['cart'], "c.*, p.Name{$c['lang']}, p.CateId, p.PreFix, p.Number, p.AttrId, p.Attr, p.PageUrl, p.MOQ, p.Stock, p.SoldOut, p.IsSoldOut, p.SStartTime, p.SEndTime", 'c.CId desc');
if (count($cart_row)) {
    $cart_attr = $cart_attr_data = $cart_attr_value = array();
    if ((int)$_SESSION['User']['UserId']) {
        //会员收藏夹
        $favorite_row = db::get_all('user_favorite', $c['where']['cart'], 'ProId');
        $favorite_pro = array();
        foreach ($favorite_row as $k => $v) {
            $favorite_pro[] = $v['ProId'];
        }
    }
    
    //检查产品资料是否完整
    $error_ary = array();
    $products_attribute_error = 0;
    foreach ((array)$cart_row as $v) {
        if (!$v['Name'.$c['lang']] && !$v['Number'] && (float)$v['Price'] == 0) {
            //产品资料丢失
            $error_ary["{$v['ProId']}_{$v['CId']}"] = 1;
        }
    }
    
    //购物车列表产品
    $CateId_ary=$attribute_cart_ary=$vid_data_ary=$error_ary=array();
    $ProIdStr='0';
    foreach((array)$cart_row as $v){//检查产品资料是否完整
        if($v['ProId'] && !$v['Name'.$c['lang']] && !$v['Number']){//产品资料丢失
            $error_ary["{$v['ProId']}_{$v['CId']}"]=1;
        }
        if($v['BuyType']==4){//组合促销
            $package_row=str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
            if(!$package_row) continue;
            $ProIdStr.=",{$package_row['ProId']}";
            $pro_where=str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
            $products_row=str::str_code(db::get_all('products', "SoldOut!=1 and ProId in($pro_where)", 'CateId'));
            foreach((array)$products_row as $v2){
                $CateId_ary[$v2['CateId']]="CateId like '%|{$v2['CateId']}|%'";//记录产品分类
            }
        }
        $ProIdStr.=",{$v['ProId']}";
        $CateId_ary[$v['CateId']]="CateId like '%|{$v['CateId']}|%'";//记录产品分类
    }
    $CateIdStr=implode(' or ', $CateId_ary);
    $attribute_row=str::str_code(db::get_all('products_attribute', '1'.($CateIdStr?" and $CateIdStr":''), "AttrId, Type, Name{$c['lang']}, ParentId, CartAttr, ColorAttr"));//属性
    foreach($attribute_row as $v){
        $attribute_ary[$v['AttrId']]=array(0=>$v['Type'], 1=>$v["Name{$c['lang']}"]);
    }
    $AttrIdStr=implode(',', array_keys($attribute_ary));
    $value_row=str::str_code(db::get_all('products_attribute_value', '1'.($AttrIdStr?" and AttrId in($AttrIdStr)":''), '*', $c['my_order'].'VId asc'));//属性选项
    foreach($value_row as $v){
        $vid_data_ary[$v['AttrId']][$v['VId']]=$v["Value{$c['lang']}"];
    }
    
    $cart_pro_ary = array();
    $cart_qty = 0;
    $total_price = 0;
    $s_total_price = 0;
    $quantity = 0;
    foreach ($cart_row as $v) {
        if ($v['BuyType'] == 4) {
            //组合促销
            $package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
            !$package_row && $error_ary["{$v['ProId']}_{$v['CId']}"] = 1;
            $products_row = str::str_code(db::get_all('products', "ProId='{$package_row['ProId']}'"));
            $pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
            $pro_where == '' && $pro_where = 0;
            $products_row = array_merge($products_row, str::str_code(db::get_all('products', "ProId in($pro_where)")));
            $data_ary = str::json_data(htmlspecialchars_decode($package_row['Data']), 'decode');
            $v['Package'] = $package_row;
            $v['ProRow'] = $products_row;
            $v['Data'] = $data_ary;
        }
        $attr = array();
        $v['Property'] != '' && $attr = str::json_data($v['Property'], 'decode');
        $v['Attr'] = $attr;
        $price = $v['Price'] + $v['PropertyPrice'];
        $v['Discount'] < 100 && $price *= $v['Discount'] / 100;
        $v['ProPrice'] = $price;
        $v['Img'] = ly200::get_size_img($v['PicPath'], '240x240');
        $v['Url'] = ly200::get_url($v, 'products');
        if (!$error_ary["{$v['ProId']}_{$v['CId']}"]) {
            $total_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
            $s_total_price += $price * $v['Qty'];
            $cart_qty += 1;
            $quantity += $v['Qty'];
        }
        $cart_pro_ary[] = $v;
    }
    
    //会员优惠价 与 全场满减价 比较
    $cutprice = $AfterPrice_0 = $AfterPrice_1 = 0;
    $user_discount = 100;
    if ((int)$_SESSION['User']['UserId'] && (int)$_SESSION['User']['Level']) {
        $user_discount = (float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
        $user_discount = ($user_discount > 0 && $user_discount < 100) ? $user_discount : 100;
        $AfterPrice_0 = $total_price - ($total_price * ($user_discount / 100));
    }
    if ($cutArr['IsUsed'] == 1 && $c['time'] >= $cutArr['StartTime'] && $c['time'] <= $cutArr['EndTime']){
        foreach ((array)$cutArr['Data'] as $k => $v) {
            if ($total_price < cart::iconv_price($k, 2, '', 0)) break;
            $AfterPrice_1 = ($cutArr['Type'] == 1 ? cart::iconv_price($v[1], 2, '', 0) : ($total_price * (100 - $v[0]) / 100));
        }
    }
    if ($AfterPrice_0 == $AfterPrice_1) {
        //当会员优惠价和全场满减价一致，默认只保留会员优惠价
        $AfterPrice_1 = 0;
    }
    (($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0 > $AfterPrice_1)) && $cutprice = $AfterPrice_0; //会员优惠价
    (($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1 > $AfterPrice_0)) && $cutprice = $AfterPrice_1; //全场满减价
    $final_total_price = $total_price - $cutprice;
    
    $IsPaypalCheckout = 0;
    if ($a && (int)db::get_row_count('payment', 'Method="Excheckout" and IsUsed=1')) {
        if ($c['NewFunVersion'] >= 4) {
            //新版Paypal checkout
            $IsPaypalCheckout = 2;
        } else {
            //旧版Paypal checkout
            $IsPaypalCheckout = 1;
        }
    }
}
?>
<script type="text/javascript">
$(function(){
    // cart_obj.cart_list();
    cart_obj.zhzcart_list();
	<?php
	if ($c['NewFunVersion'] >= 4) {
        //新版Paypal checkout
		echo 'cart_obj.paypal_checkout_init("#paypal_button_container");';
        echo 'cart_obj.paypal_checkout_init("#paypal_button_container_to");';
	}?>
});
</script>
<div id="cart">
    <div class="cart_crumb ui_border_b clean"><?=$c['lang_pack']['mobile']['shopping_cart'];?></div>
    <?php
    if (count($cart_row)) {
        //购物车列表
    ?>
        <div class="cart_top_info ui_border_b clean">
            <div class="cart_total_price"><strong><?=$c['lang_pack']['mobile']['total'];?> :</strong><span><?=$_SESSION['Currency']['Currency'].' '.cart::iconv_price(0, 1).cart::currency_format($final_total_price, 0, $_SESSION['Currency']['Currency']);?></span></div>
            <div class="cart_btn">
                <?php
                if ($IsPaypalCheckout == 2) {
                    echo '<div id="paypal_button_container" class="btn"></div>';
                } elseif ($IsPaypalCheckout == 1) {
                    echo '<button class="btn paypal_checkout_button"></button>';
                }?>
                <div class="btn checkout BuyNowBgColor" data-name="<?=$c['lang_pack']['mobile']['checkout'];?>"><?=$c['lang_pack']['mobile']['check_out'];?></div>
            </div>
        </div>
		<div class="cart_list cart_main_list">
			<?php
			foreach ($cart_pro_ary as $row) {
                $foreach_ary = array();
                if ($row['BuyType'] == 4) {
                    //组合促销
                    $foreach_ary = $row['ProRow'];
                } else {
                    //普通产品
                    $foreach_ary[0] = $row;
                }
                $last_one = count($foreach_ary) - 1;
                foreach ((array)$foreach_ary as $k => $v) {
                    if ($row['BuyType'] == 4) {
                        //组合促销
                        $v['Img'] = ly200::get_size_img($v['PicPath_0'], '240x240');
                        $v['Url'] = ly200::get_url($v, 'products');
                        $v['ProPrice'] = $row['ProPrice'];
                        if ($k == 0) echo '<strong class="sales_title">[ '.$c['lang_pack']['cart']['package'].' ] '.$row['Package']['Name'].'</strong>'; //组合促销标题
                    }
			?>
                    <div class="item clean<?=($row['BuyType'] != 4 || ($row['BuyType'] == 4 && $k == $last_one) ? ' ui_border_b' : '') . ($error_ary["{$v['ProId']}_{$row['CId']}"] && $row['BuyType']!=1 ? ' null' : '') . ($row['BuyType'] == 4 && $k > 0 ? ' sales_item' : '');?>" cid="<?=$row['CId'];?>">
                        <div class="check fl">
                            <em class="btn_checkbox<?=$error_ary["{$v['ProId']}_{$row['CId']}"] ? ' style="display:none;"' : ' current';?> FontBgColor"></em>
                            <input type="checkbox" name="select" value="<?=$row['CId'];?>" class="va_m"<?=$error_ary["{$v['ProId']}_{$row['CId']}"] ? '' : ' checked';?> />
                        </div>
                        <div class="lefter fl">
                            <div class="img"><?php if(is_file($c['root_path'] . $v['Img'])){?><a href="<?=$v['Url'];?>"><img src="<?=$v['Img'];?>" alt="<?=$v['Name' . $c['lang']];?>" /></a><?php }?></div>
                            <?php if ($row['BuyType'] != 4) {?>
                                <div class="button">
                                    <a href="javascript:;" class="btn_save_for_later">save for later</a><a href="javascript:;" class="del" data-url="/cart/remove_c<?=$row['CId'];?>.html"><?=$c['lang_pack']['mobile']['remove'];?></a>
                                </div>
                            <?php }?>
                        </div>
                        <div class="info fr">
                            <?php
                            if ($error_ary["{$v['ProId']}_{$row['CId']}"]) {
                            ?>
                                <div class="rows clean">
                                    <div class="error fl"><?=$c['lang_pack']['mobile']['product_error'];?></div>
                                    <div class="del fr" url="/cart/remove_c<?=$row['CId'];?>.html">X</div>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="name"><a href="<?=$v['Url'];?>"><?=$v['Name' . $c['lang']];?></a></div>
                                <div class="rows clean later_price"><?=$_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price($v['ProPrice']);?></div>
                                <?php
                                if ($row['BuyType'] == 4) {
                                    //组合促销
                                    if($row['Data'][$v['ProId']]){
                                        $OvId = 1;
                                        foreach ((array)$row['Data'][$v['ProId']] as $k3 => $v3) {
                                            if($k3 == 'Overseas'){
                                                //发货地
                                                $OvId = str_replace('Ov:', '', $v3);
                                                if ((int)$c['config']['global']['Overseas'] == 0 || $OvId == 1) continue; //发货地是中国，不显示
                                                echo '<div class="rows clean attr">' . $c['lang_pack']['products']['shipsFrom'] . ': &nbsp;' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</div>';
                                            }else{
                                                echo '<div class="rows clean attr">' . $attribute_ary[$k3][1] . ': &nbsp;' . $vid_data_ary[$k3][$v3] . '</div>';
                                            }
                                        }
                                        if ((int)$c['config']['global']['Overseas'] == 1 && $OvId == 1) {
                                            echo '<div class="rows clean attr">' . $c['lang_pack']['products']['shipsFrom'] . ': &nbsp;' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</div>';
                                        }
                                    }
                                } else {
                                    //普通产品
                                    if (count($v['Attr'])) {
                                        foreach ($v['Attr'] as $y => $z) {
                                            if ($y == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                            echo '<div class="rows clean attr">' . ($y == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $y) . ': &nbsp;' . $z . '</div>';
                                        }
                                    }
                                    if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                        echo '<div class="rows clean attr">' . $c['lang_pack']['products']['shipsFrom'] . ': &nbsp;' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</div>';
                                    }
                                }?>
                                <?php
                                if ($row['BuyType'] != 4 || ($row['BuyType'] == 4 && $k == 0)) {
                                ?>
                                    <div class="rows clean price_qty">
                                        <div class="price fl" data-price="<?=$v['Price'];?>" data-discount="<?=$v['Discount'];?>"><?=cart::iconv_price($v['ProPrice']);?></div>
                                        <div class="qty_box fr">
                                            <div class="cut fl">-</div>
                                            <div class="qty fl"><input type="number" name="Qty[]" data-start="<?=$v['StartFrom'];?>" data-cid="<?=$row['CId'];?>" data-proid="<?=$v['ProId'];?>" value="<?=$row['BuyType'] == 4 ? 1 : $v['Qty'];?>" class="fl"<?=$row['BuyType'] == 4 ? ' disabled' : '';?> /></div>
                                            <div class="add fl">+</div>
                                            <input type="hidden" name="Qty[]" data-start="<?=$v['StartFrom'];?>" data-cid="<?=$row['CId'];?>" data-proid="<?=$v['ProId'];?>" value="<?=$v['Qty'];?>" />
                                        </div>
                                    </div>
                                    <div class="rows clean button_box">
                                        <a href="javascript:;" class="btn btn_move_to_cart">Move to Cart</a><a href="javascript:;" class="btn del" data-url="/cart/remove_c<?=$row['CId'];?>.html"><?=$c['lang_pack']['mobile']['remove'];?></a>
                                    </div>
                                <?php }?>
                            <?php }?>
                            <?php
                            if ($row['BuyType'] != 4 || ($row['BuyType'] == 4 && $k == 0)) {
                            ?>
                                <input type="hidden" name="S_Qty[]" value="<?=$v['Qty'];?>" />
                                <input type="hidden" name="CId[]" value="<?=$row['CId'];?>" />
                                <input type="hidden" name="ProId[]" value="<?=$v['ProId'];?>" />
                            <?php }?>
                        </div>
                        <?php if ($row['BuyType'] == 4 && $k == $last_one) {?>
                            <div class="clear"></div>
                            <div class="button">
                                <a href="javascript:;" class="btn_save_for_later">save for later</a><a href="javascript:;" class="del" data-url="/cart/remove_c<?=$row['CId'];?>.html"><?=$c['lang_pack']['mobile']['remove'];?></a>
                            </div>
                        <?php }?>
                    </div>
			<?php
                }
            }?>
		</div>
		<div class="cart_total ui_border_b">
			<div class="clean savings"<?=$AfterPrice_0 ? ' userPrice="' . $AfterPrice_0 . '" userRatio="' . $user_discount . '"':' userPrice="0" userRatio="100"';?><?=$cutprice ? '' : ' style="display:none;"';?>>
				<span class="title"><?=$c['lang_pack']['mobile']['total_savings'];?></span>
				<span class="cutprice_p">- <?=$_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price(0, 1) . cart::currency_format($cutprice, 0, $_SESSION['Currency']['Currency']);?></span>
			</div>
			<div class="clean total">
				<span class="title"><?=$c['lang_pack']['mobile']['total'];?></span>
				<span class="p"><?=$_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price(0, 1) . cart::currency_format($final_total_price, 0, $_SESSION['Currency']['Currency']);?></span>
			</div>
		</div>
        <input type="hidden" name="CartProductPrice" value="<?=cart::iconv_price($total_price, 2, '', 0);?>" />
		<?php 
		if ($cutArr['IsUsed'] == 1 && $c['time'] >= $cutArr['StartTime'] && $c['time'] <= $cutArr['EndTime']) {
			foreach ((array)$cutArr['Data'] as $k => $v) {
				if ($s_total_price < $k) {
					$show_price = $k;
					$show_coupon = $cutArr['Type'] == 1 ? ($c['lang'] == '_en' ? cart::iconv_price($v[1]) . ' discount' : '-' . cart::iconv_price($v[1])) : (100 - $v[0]) . '% off';
					break;
				}
			}
		}
		$fullcoupon = cart::iconv_price($show_price - $s_total_price);
		?>
		<div class="fullcoupon" style="display:<?=$show_price?'block':'none';?>;"><?=str_replace(array('%price%','%off%'), array($fullcoupon, $show_coupon), $c['lang_pack']['cart']['fulldiscount']);?></div>
        <div class="cart_btn">
            <?php
            if ($IsPaypalCheckout == 2) {
                echo '<div id="paypal_button_container_to" class="btn"></div>';
            } elseif ($IsPaypalCheckout == 1) {
                echo '<button class="btn paypal_checkout_button"></button>';
            }?>
            <div class="btn checkout BuyNowBgColor" data-name="<?=$c['lang_pack']['mobile']['checkout'];?>"><?=$c['lang_pack']['mobile']['check_out'];?></div>
        </div>
        <div class="cart_list cart_later_list">
            <div class="later_title ui_border_tb"><?=$c['lang_pack']['mobile']['later_title'];?></div>
            <div class="later_list"></div>
        </div>
	<?php
	}else{
        //购物车为空
	?>
		<div class="nocart"><?=$c['lang_pack']['mobile']['cart_empty'];?></div>
		<div class="cart_btn"><a href="/" class="btn_global btn_continue"><?=$c['lang_pack']['mobile']['continue_shop'];?></a></div>
	<?php }?>
</div>