<?php !isset($c) && exit();?>
<?php
$login_module_ary=array('index', 'order', 'favorite', 'coupon', 'address', 'setting', 'password','inbox', 'rfp', 'rfq','balance','pruchaseorder','orderpro','logistics','rfq_cate','review'); //需要登录的模块列表
$un_login_module_ary=array('login', 'binding', 'forgot','rfq_cate'); //不需要登录的模块列表 'login',
$app_module_ary=array('distribution', 'apply');//app模块列表
$login_module_ary=array_merge($login_module_ary, $app_module_ary);

if((int)$_SESSION['User']['UserId']){ //已登录
	$module_ary=$login_module_ary;
	$module_ary[]='address';
}else{ //未登录
//    print_r($_GET);die;
    if ($_GET['a'] == 'pruchaseorder' && $_GET['d'] == 'view'){
        in_array($a, $login_module_ary) && js::location("/account/login.html?&jumpUrl=".urlencode('http://lemonyyshop.com/account/pruchaseorder/view'.$_GET['OId'].'.html'));
        $module_ary=$un_login_module_ary; //重置模块列表
    }else{
        in_array($a, $login_module_ary) && js::location("/account/login.html?&jumpUrl=".urlencode($_GET['JumpUrl']));
        $module_ary=$un_login_module_ary; //重置模块列表
    }
}
!in_array($a, $module_ary) && $a=$module_ary[0];
if((int)$_SESSION['User']['UserId']){
	$user_row=str::str_code(db::get_one('user', $c['where']['user']));
}

?>
<!DOCTYPE HTML>
<html lang="us">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex,nofollow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection" />
<?php
echo ly200::seo_meta();
include("{$c['mobile']['theme_path']}inc/resource.php");
echo ly200::load_static(/*'/static/themes/default/css/user.css', */"{$c['mobile']['tpl_dir']}css/user.css","{$c['mobile']['tpl_dir']}js/user.js");
?>
<script type="text/javascript">$(function(){user_obj.user_index()});</script>
</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['mobile']['theme_path']}inc/header.php");?>
<?php if($a!='index') include("{$c['mobile']['theme_path']}user/module/include/header.php");?>
<div class="wrapper">
	<?php if($a!='index' && $a!='login'){?>
		<div id="u_title">
			<div class="u_title_box">
            <!--   add by zhz start            -->
                <?php if ($a == 'order' && !$d){ ?>
                    <style>
                        #u_title .u_title_box{padding-left: 0rem;}
                        .u_xuanze li{width:50%;float:left;height:44px;text-align:center;font-size: 0.86rem;font-weight: bold;
                            line-height: 52px;}
                        .u_xuanze li img{width: 0.86rem;vertical-align: middle;margin-right: 5px;}
                        .u_xuanze li a{color: #000;}
                        .u_xuanze .dq{color:#ffe058;}
                        .u_xuanze .fdq{background: #f5f5f5;}
                    </style>
                    <ul class="u_xuanze">
                        <li class="dq"><img src="/static/themes/default/mobile/images/orderlistdq1.png" alt=""><?= $c['lang_pack']['user']['zhz_lemonyyorders']; ?></li>
                        <li class="fdq"><img src="/static/themes/default/mobile/images/orderlist2.png" alt=""><a
                                    href="/account/pruchaseorder/"><?= $c['lang_pack']['user']['zhz_pruchaseorders']; ?></a></li>
                        <div style="clear: both;"></div>
                    </ul>
                <?php }elseif($a=='pruchaseorder' && !$d){ ?>
                    <style>
                        #u_title .u_title_box{padding-left: 0rem;}
                        .u_xuanze li{width:50%;float:left;height:44px;text-align:center;font-size: 0.86rem;font-weight: bold;
                            line-height: 52px;}
                        .u_xuanze li img{width: 0.86rem;vertical-align: middle;margin-right: 5px;}
                        .u_xuanze li a{color: #000;}
                        .u_xuanze .dq{color:#ffe058;}
                        .u_xuanze .fdq{background: #f5f5f5;}
                    </style>
                    <ul class="u_xuanze">
                        <li class="fdq"><img src="/static/themes/default/mobile/images/orderlist1.png" alt=""><a
                                    href="/account/orders/"><?= $c['lang_pack']['user']['zhz_lemonyyorders']; ?></a></li>
                        <li class="dq"><img src="/static/themes/default/mobile/images/orderlistdq2.png" alt=""><?=$c['lang_pack']['user']['zhz_pruchaseorders']; ?></li>
                        <div style="clear: both;"></div>
                    </ul>
                <?php }else{
                    /* add by zhz start */
                    $user_title['pruchaseorder'] = 'My Order';
                    $user_title['orderpro'] = 'Order Number';
                    $user_title['logistics'] = 'Tracking details for Container';
                    /* add by zhz end */
                    ?>
                    <h1><?=$user_title[$a];?></h1>
                <?php } ?>
            </div>
		</div>
	<?php }?>
	<?php
    $a=str_replace('p-', '', $a);
    if(in_array($a, $app_module_ary)){//app模块
        include("{$c['default_path']}/app.php");
    }else{//基础模块
        include("{$c['mobile']['theme_path']}user/module/{$a}.php");
    }
    ?>
</div>
</body>
</html>