<?php !isset($c) && exit();?>
<?php
$user_title=array(
	'index'			=>	$c['lang_pack']['mobile']['my_account'],
	'order'			=>	$c['lang_pack']['my_orders'],
	'favorite'		=>	$c['lang_pack']['my_fav'],
	'coupon'		=>	$c['lang_pack']['my_coupon'],
	'address'		=>	$c['lang_pack']['my_address_book'],
	'inbox'			=>	$c['lang_pack']['user']['inboxTitle'],
	'setting'		=>	$c['lang_pack']['user']['settingTitle'],
	'distribution'	=>	$c['lang_pack']['plugins']['DIST_make_money'],
	'password'		=>	$c['lang_pack']['user']['password'],
	'rfq'		    =>	$c['lang_pack']['user']['my_rfq'],
	'rfp'		    =>	$c['lang_pack']['user']['new_rfq'],
	'balance'		=>	$c['lang_pack']['user']['my_balance']
);
if($a=='distribution'){
	$d_module=array(
		'index'		=>	$c['lang_pack']['plugins']['DIST_make_money'],
		'withdraw'	=>	$c['lang_pack']['plugins']['DIST_withdraw'],
		'view'		=>	$c['lang_pack']['plugins']['DIST_comm_detail'],
		'record'	=>	$c['lang_pack']['plugins']['DIST_record'],
		'distor'	=>	$c['lang_pack']['plugins']['DIST_make_money'],
	);
	$user_title[$a]=$d_module[$_GET['d']];
}
?>
<div id="u_header"<?=$a!='login'?' class="fixed"':'';?>>
	<a class="back" href="javascript:history.back(-1);"></a>
    <a class="menu global_menu" href="javascript:;"></a>
    <a class="cart" href="/cart/"><?=(int)$c['shopping_cart']['TotalQty']?'<i class="FontBgColor"></i>':''?></a>
    <div class="title"><?=$user_title[$a];?></div>
</div>