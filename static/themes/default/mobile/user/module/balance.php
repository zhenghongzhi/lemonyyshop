<?php !isset($c) && exit(); ?>
<?php

//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach ($currency_row as $k => $v) {
    $all_currency_ary[$v['Currency']] = $v;
}

$level_row = db::get_one('user_level', "IsUsed=1 and LId = '{$user_row['Level']}'");
$next_level_row = db::get_one('user_level', "IsUsed=1 and FullPrice > '{$level_row['FullPrice']}'", '*', 'FullPrice asc');
$next_level_row || $next_level_row = $level_row;
$_UserName = substr($c['lang'], 1) == 'jp' ? $user_row['LastName'] . ' ' . $user_row['FirstName'] : $user_row['FirstName'] . ' ' . $user_row['LastName'];

$yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');
$balance = ly200::getyy_balance($yy_code['yy_code']);
?>

<?php
if ($yy_code['yy_code']) {
    ?>
    <script type="text/javascript" language="javascript">
        function resetHeight() {
            var ifm = document.getElementById("iframeBlance");
            var subWeb = document.frames ? document.frames["iframeBlance"].document : ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }
        function changeFrameHeight(that){
            //电脑屏幕高度-iframe上面其他组件的高度
            //例:我这里iframe上面还有其他一些div组件，一共的高度是120，则减去120
            $(that).height(document.documentElement.clientHeight - 100);

        }
    </script>
    <div style="width: 100%; height: auto;">
        <iframe id='iframeBlance'
                src='http://47.106.88.138:8082/#/api/customer/info?customerNumber=<?=$yy_code['yy_code'] ?>'
                width="100%"
                frameBorder='0'
                scrolling='yes'
                onLoad="changeFrameHeight(this)"/>
    </div>

    <?php
} else {
    ?>
    <div class="no_data"><div class="content_blank"><?=$c['lang_pack']['mobile']['no_data'];?></div></div>
    <?php
}
?>
