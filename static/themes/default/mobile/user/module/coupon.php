<?php !isset($c) && exit();?>
<?php
$g_page=(int)$_GET['page'];
$page_count=10;
$row=str::str_code(db::get_limit_page('sales_coupon_relation r left join sales_coupon c on r.CId=c.CId', "{$c['time']} < c.EndTime and {$c['time']} > c.StartTime and r.UserId={$_SESSION['User']['UserId']}", 'c.*,r.IsExpired', 'c.UseCondition asc,c.Discount asc', $g_page, $page_count));
$query_string=ly200::get_query_string(ly200::query_string('m, a, p, page'));
?>
<script type="text/javascript">$(function (){user_obj.user_order()});</script>
<div id="user">
    <div class="user_coupon">
    	<?php
		if($row[0]){
			foreach($row[0] as $k=>$v){
                $BeUseTimes=db::get_row_count('sales_coupon_log', "CId={$v['CId']} and UserId={$_SESSION['User']['UserId']}");
		?>
			<div class="item clean ui_border_b">
            	<div class="clean">
					<div class="fl cpnum"><?=$c['lang_pack']['mobile']['coupon_code'];?>: <?=$v['CouponNumber'];?> (<span class="fcr"><?=$BeUseTimes?$c['lang_pack']['user']['alreadyUse'].($v['UseNum']>0?" ({$BeUseTimes}/{$v['UseNum']})":" ({$BeUseTimes})"):$c['lang_pack']['user']['effective'];?></span>)</div>
                </div>
                <div class="cpnum"><?=$v['CouponType']?"{$c['lang_pack']['mobile']['redu_of']}".cart::iconv_price($v['Money']):(100-$v['Discount']).'% off';?></div>
				<div class="fl cpnum"><?=date('d/m/Y', $v['StartTime']).' - '.date('d/m/Y', $v['EndTime']);?></div>
                <div class="fr cpdate"><?=$c['lang_pack']['mobile']['con_of_use']?>: <?=$c['lang_pack']['mobile']['full']?> <?=cart::iconv_price($v['UseCondition']);?></div>
			</div>
			<?php }?>
        <?php }else{?>
        	<div class="content_blank"><?=$c['lang_pack']['mobile']['you_no_coupon']?></div>
        <?php }?>
    </div>
</div>