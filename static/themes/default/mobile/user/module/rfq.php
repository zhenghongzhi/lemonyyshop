<?php !isset($c) && exit(); ?>
<?php
$d_ary = array('list', 'view', 'contact');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];

$user_id = $_SESSION['User']['UserId'];
?>

<?php
echo ly200::load_static("{$c['mobile']['tpl_dir']}css/rfq.css", "{$c['mobile']['tpl_dir']}js/rfq.js");
?>
<style>
    a:link, a:visited {
        color: #444444;
        text-decoration: none;
    }

    a:hover {
        color: #444444;
    }

    .float_bottom {
        width: 100%;
        position: fixed;
        bottom: 0;
        z-index: 999;
    }

    #rfqlist {
        margin-bottom: 52px;
    }

    #rfqlist .prod_box .info {
        margin-right: 50px;
    }

    .float_bottom .add_new {
        width: 90%;
        height: 40px;
        line-height: 40px;
        color: white;
        border: 1px solid #dbdbdb;
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        cursor: pointer;
        display: block;
        margin: 0 auto;
        text-align: center;
        margin-bottom: 12px;
        background-color: #e9af24;
        font-size: 14px;
        z-index: 999;
    }

    .float_bottom .add_new:hover {
        background-color: #e9af24;
        z-index: 999;
    }

    .user_order .prod_list .plist .fr {
        float: right;
        text-align: center;
        position: relative;
        margin-top: 1.8rem;
    }

    .user_order .prod_list .plist .img img{
        object-fit: cover;
        display: block;
    }

    .user_order .prod_list .plist .fr span {
        float: right;
        margin-right: 16px;
    }

    .user_order .prod_list .plist .fr em {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #999;
        border-style: solid;
        float: right;
        display: block;
        position: relative;
        z-index: 10;
    }

    .user_order .prod_list .plist .fr em > i {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #fff;
        border-style: solid;
        display: block;
        position: absolute;
        top: -.4rem;
        right: .1rem;
        z-index: 11;
    }

    #rfqlist .prod_box{
        padding-left: 0;
        padding-right: 16px;
    }

    .user_order .prod_list .title{
        width: 100%;
        height: 2.0rem;
        line-height: 2.0rem;
        background-color: #eeeeee;
    }


    div>.line{
        width: 100%;
        margin-left: 8px;
        margin-top: 10px;
        height: 1px;
        background-color: #eeeeee;
    }

    .user_order .prod_list .plist .info{
        width: auto;
    }

    .user_order .prod_list .plist .info .name{
        margin-top: 8px;
    }

    .user_order .prod_list .plist .info .attr{
        margin-top: 8px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<div id="user">
    <?php
    if ($d == 'list') {
        $rfq_row = str::str_code(db::get_all('purchase_request', "user_id = $user_id", '*', 'created_at desc'));
        ?>
        <div id="rfqlist" class="user_order" data-number="0" data-page="1" data-total="<?= $rfq_row[3]; ?>">
            <?php

            if ($rfq_row) {
                foreach ($rfq_row as $k => $v) {
                    $url = "/account/rfq/view/{$v['OId']}.html";
                    $pr_id = $v['id'];
                    $img = str::str_code(db::get_one('purchase_img', "pr_id = $pr_id", 'img_url'));
                    $name = $v['product_name'];
                    $description = $v['description'];
                    ?>
                    <div class="prod_list">
                        <a href="<?= $url; ?>">
                            <div class="prod_box clean">
                                <div class="item clean" data-url="<?= $url ?>">
                                    <div class="title clean ui_border_b">
                                        <span class="oid"><?= $c['lang_pack']['mobile']['no.'] . $v['OId']; ?></span>
                                        <span class="status"><?= $c['lang_pack']['user']['PurchaseStatusAry'][$v['status']]; ?></span>
                                    </div>
                                    <div class="plist package clean" style="margin-left: 0.5rem;">
                                        <div class="img fl">
                                            <!-- edit by zhz start-->
                                                <img src="<?= $img['img_url']?$img['img_url']:'/static/themes/default/images/caigou.png' ?>">
                                            <!-- edit by zhz end  -->
                                        </div>
                                        <div class="info">
                                            <div class="name"><?= $name; ?></div>
                                            <div class="attr clean"><?= $description ?></div>
                                        </div>
                                        <div class="fr">
                                            <em>

                                                <i></i>
                                            </em>
                                            <span>
                                            <?= $c['lang_pack']['user']['view_more'] ?>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="divide_5px"></div>
        </div>

        <div class="float_bottom">
            <a class="add_new" href="/account/rfp/"><?= $c['lang_pack']['add_new']; ?></a>
        </div>
        <script>
            $(function () {
                $('html').unbind();
            });
        </script>
        <?php
    } elseif ($d == 'view') {
        $OId = $_GET['OId'];
        $purchase_row = str::str_code(db::get_one('purchase_request', "OId='$OId' and user_id='{$_SESSION['User']['UserId']}'"));
        !$purchase_row && js::location('/account/rfq/');
        ?>

        <div class="item clean" style="margin: 10px; border: #bbbbbb 1px solid;">
            <div class="title clean">
                <br>
                <span class="oid"
                      style="font-size: 18px; margin: 10px 10px;"><?= $c['lang_pack']['mobile']['no.'] . $purchase_row['OId']; ?></span>
                <hr>
            </div>
            <div id="wrap_rfq" class="package clean">
                <form id="rfq_from" class="frm_rfq">
                    <div class="row">
                        <input name="productName" id="productName" class="lib_txt lib_input input-half" type="text"
                               value="<?= $purchase_row['product_name'] ?>" readonly/>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <input id="quantity" name="quantity" class="lib_txt input-half" type="text"
                               value="<?= $purchase_row['quantity'] ?>" readonly/>
                        <select id="quantityUtil" name="quantityUtil" disabled>
                            <option value="<?= $purchase_row['unit'] ?>"><?= $purchase_row['unit'] ?></option>
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <textarea name="description" id="description" style="text-align:left;"
                                  class="lib_txt lib_input box_textarea" rows="3" maxlength="500"
                                  readonly><?= $purchase_row['description'] ?>
                        </textarea>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <input name="productURL" id="productURL" class="lib_txt lib_input" type="text"
                               value="<?= $purchase_row['product_url'] ?>" readonly/>
                    </div>
                    <div class="row">
                        <input name="referQuotation" id="referQuotation" class="lib_txt" type="text"
                               value="<?= $purchase_row['refer_price'] ?>" readonly/>
                        <span style="font-size: 16px; margin: 6px;"><?= $c['lang_pack']['products']['RMB']; ?></span>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <label for="productImage"><?= $c['lang_pack']['products']['productImage']; ?></label>
                        <div id="productImage">
                            <?php
                            $pr_id = $purchase_row['id'];
                            $img_row = str::str_code(db::get_all('purchase_img', "pr_id = $pr_id", '*'));
                            if ($img_row) {
                                foreach ($img_row as $k => $v) {
                                    ?>
                                    <div class="img fl"><img src="<?= $v['img_url']; ?>"</div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <input name="factoryInformation" id="factoryInformation" class="lib_txt lib_input" type="text"
                               value="<?= $purchase_row['factory_info'] ?>" readonly/>
                    </div>
                    <div class="clear"></div>
                </form>
                <br>
                <div style="padding: 20px; font-size: 18px; color: black;">
                    <a style="text-decoration:underline; right: 20px;"
                       href='/account/rfq/contact/<?= $OId ?>.html'><?= $c['lang_pack']['user']['view_contact']; ?></a>
                </div>
            </div>
            <br>
        </div>

        <?php
    }
    elseif ($d == 'contact') {
    $OId = $_GET['OId'];
    $purchase_row = str::str_code(db::get_one('purchase_request', "OId='$OId' and user_id='{$_SESSION['User']['UserId']}'"));
    !$purchase_row && js::location('/account/rfq/contact/' . $OId . '.html');
    $row = str::str_code(db::get_one('request_message', "UserId='{$_SESSION['User']['UserId']}' and Module='purchase' and Subject='$OId'"));
    if ($row) {
        if ($row['IsReply']) db::update('request_message', "MId='{$row['MId']}'", array('IsReply' => 0));
        $reply_row = str::str_code(db::get_all('request_message_reply', "MId='{$row['MId']}'"));
    }
    $back_url = 'javascript:history.back(-1);';
    if ($_SESSION['Ueeshop']['RequestReturnUrl']) {
        $back_url = $_SESSION['Ueeshop']['RequestReturnUrl'];
    }
    unset($_SESSION['Ueeshop']['RequestReturnUrl']);
    ?>
    <script type="text/javascript">$(function () {
            //user_obj.purchase_message_init();
            /* edit by zhz start */
            var height = $('.wrapper').height();
            $("html,body").scrollTop(height);
            /* edit by zhz end  */
        })</script>
    <div class="blank20"></div>
    <div class="item clean" style="margin: 10px; border: #bbbbbb 1px solid;">
        <div class="title clean">
            <br>
            <span class="oid"
                  style="font-size: 18px; margin: 10px 10px;"><?= $c['lang_pack']['mobile']['no.'] . $purchase_row['OId']; ?></span>
            <hr>
        </div>
        <div id="user" data-user-id="<?= $_SESSION['User']['UserId']; ?>">
            <?php
            if (empty($reply_row)){
                ?>
                <div class="no_data">
                    <div class="content_blank"><?= $c['lang_pack']['mobile']['no_data']; ?></div>
                </div>
                <?php
            }else{
            ?>
            <div class="inbox_tab_con">
                <div class="tab_con inbox">
                    <?php
                    foreach ((array)$reply_row as $k => $v){
                    ?>
                    <?php if (empty($v['UserId'])){
                    ?>
                    <div class="dialogue_box dialogue_box_left clean" style="width: 85vw;">
                        <?php
                        }else{
                        ?>
                        <div class="dialogue_box dialogue_box_right clean" style="width: 85vw;">
                            <?php
                            } ?>
                            <div class="time"><?= date('Y-m-d H:i', $v['AccTime']) ?></div>
                            <div class="message"><?= nl2br($v['Content']) ?></div>
                            <?php if ($v['PicPath']) { ?>
                                <div class="picture">
                                    <a href="<?= $v['PicPath'] ?>" target="_blank" class="pic_box"><img
                                                src="<?= $v['PicPath'] ?>"><span></span></a>
                                </div>
                            <?php } ?>
                            <span></span>
                        </div>
                        <div class="clear"></div>
                        <?php
                        }
                        ?>
                        <div id="View"></div>
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="inbox_form">
                    <form id="reply_form" method="post" action="/" enctype="multipart/form-data">
                        <div class="upload_box">
                            <input class="upload_file" id="upload_file" type="file" name="PicPath"
                                   onchange="loadImg(this);"
                                   accept="image/gif,image/jpeg,image/png">
                            <div id="pic_show" class="pic_box"></div>
                        </div>
                        <div class="input_box">
                            <input type="text" name="Content" value="" size="50" notnull/>
                        </div>
                        <input type="submit" class="submit_btn" name="submit_button"
                               value="<?= $c['lang_pack']['user']['send']; ?>"/>
                        <input type="hidden" name="Subject" value="<?= $OId ?>"/>
                        <input type="hidden" name="MId" value="<?= $row['MId'] ?>"/>
                        <input type="hidden" name="UserId" value="<?= $_SESSION['User']['UserId'] ?>"/>
                        <input type="hidden" name="OId" value="<?= $OId ?>"/>
                        <!--                        <input type="hidden" name="JumpUrl" value="/account/rfq/contact/-->
                        <? //=$OId?><!--.html" />-->
                        <!--                        <input type="hidden" name="BackUrl" value="-->
                        <? //=$_SERVER['HTTP_REFERER'];?><!--" />-->
                        <input type="hidden" name="do_action" value="user.reply_rfq_msg"/>
                    </form>
                </div>
            </div>
            <div class="blank15"></div>
        </div>
        <script>
            document.getElementById('View').scrollIntoView();

            function loadImg(obj) {
                //获取文件
                var file = obj.files[0];
                //创建读取文件的对象
                var reader = new FileReader();
                //为文件读取成功设置事件
                reader.onload = function (e) {
                    var imgFile = e.target.result;
                    $(obj).next('#pic_show').html('<img src="' + imgFile + '"/><span></span>');
                };
                //正式读取文件
                reader.readAsDataURL(file);
            }
        </script>
        <?php } ?>
    </div>