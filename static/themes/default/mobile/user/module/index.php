<?php !isset($c) && exit();?>
<?php
$level_row=db::get_one('user_level', "IsUsed=1 and LId='{$user_row['Level']}'");
$awaiting_order=db::get_one('orders', "UserId='{$_SESSION['User']['UserId']}' and OrderStatus in (3)", '*', 'OrderId desc');
$awaiting_order && $awaiting_order_products=db::get_limit('orders_products_list', "OrderId='{$awaiting_order['OrderId']}'", '*', 'LId desc', 0, 5);
$shopping_cart_count=db::get_row_count('shopping_cart', $c['where']['user']);
//会员站内信未读数量统计
$msg_where="(UserId like '%|{$_SESSION['User']['UserId']}|%' or UserId='-1') and Type=1";	//兼容3.0系统
$msg_where.=" or (UserId='{$_SESSION['User']['UserId']}' and Module='others' and IsReply=1)";
$user_msg_row=str::str_code(db::get_all('user_message', $msg_where, '*', 'MId desc'));
$user_msg_len=0;
foreach((array)$user_msg_row as $k=>$v){
	$is_read=0;
	if($v['UserId']==$_SESSION['User']['UserId']){
		$user_msg_len+=1;
	}else{	//兼容3.0系统,3.0系统UserId有多个
		if($v['IsRead']){
			$userid_ary=@array_flip(explode('|', $v['UserId']));
			$isread_ary=@explode('|', $v['IsRead']);
			$is_read=$isread_ary[$userid_ary[$_SESSION['User']['UserId']]];
			!$is_read && $user_msg_len+=1;
		}
	}
}
$user_order_msg=db::get_row_count('user_message', "UserId='{$_SESSION['User']['UserId']}' and Module='orders' and IsReply=1");
$user_favorite_msg=db::get_row_count('user_favorite', "UserId='{$_SESSION['User']['UserId']}'");
$user_coupon_msg=db::get_row_count('sales_coupon_relation r left join sales_coupon c on r.CId=c.CId', "{$c['time']}<c.EndTime and {$c['time']}>c.StartTime and r.UserId={$_SESSION['User']['UserId']} and r.IsExpired=0");

//add by jay start
$user_rfq_msg=db::get_row_count('request_message',"UserId='{$_SESSION['User']['UserId']}' and Module='purchase' and IsReply=1");
$yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');
$balance = ly200::getyy_balance($yy_code['yy_code']);
//add by jay end
//add by zhz start
$exchange = db::get_all('exchange','isuse=1','*',"sort asc");
$exchangecount = count($exchange)-1;
//print_r($exchange);die;
//add by zhz end
?>
<div id="user">
	<div class="index_user_container">
    	<div class="user_index_menu">
        	<a class="setting" href="/account/setting/"></a>
            <a class="global_menu" href="javascript:;"></a>
            <div class="clear"></div>
        </div>
        <div class="user_data">
            <div class="user_logo fl" data-url="/account/"></div>
            <div class="user_info fl">
            	<div class="email"><?=$user_row['Email'];?></div>
				<?php if($level_row){?>
					<div class="level"><img src="<?=$level_row['PicPath'];?>" alt=""><?=$level_row['Name'.$c['lang']];?></div>
               		<?php if($level_row['Discount']>0 && $level_row['Discount']<100){?>
						<div class="discount"><?=str_replace('%discount%',(100-$level_row['Discount']).'%',$c['lang_pack']['mobile']['discount']);?></div>
					<?php }?>
				<?php }?>
                <!-- add for yy number start -->
                <div class="yy" style="color: #0654ba; margin: 6px; font-size: 16px;">
                    <?php
                        if ($yy_code['yy_code']){
                            echo $yy_code['yy_code'];
                        }
                    ?>
                </div>
                <!-- add for yy number end -->
            </div>
            <div class="clear"></div>
        </div>
        <a href="/cart/" class="user_cart">
        	<div class="title"><?=$c['lang_pack']['shoppingCart'];?></div>
            <div class="count"><?=$shopping_cart_count.' '.$c['lang_pack']['cart']['items'];?></div>
        </a>
        <style>
            .zhzhuilv{
                padding: 1rem 0;
                border-radius: 0.3rem;
                background: #fff;
                background-size: 1.5rem auto;
                margin-bottom: 0.6rem;
            }
            .zhzhuilvbot{
                height: 3rem;
                line-height: 1rem;
                margin: 0 0.5rem;
                border-top: 1px solid #F0F0F0;
                position: relative;
            }
            .zhzhuilvbot i{
                position: absolute;
                background-color: #fff;
                top: 10px;
                right: 0px;
            }
            .zhzhuilvbot i:before{
                border: 0.5rem solid transparent;
                border-left: 0.5rem solid #efeff4;
                width: 0;
                height: 0;
                position: absolute;
                top: 0;
                right: 0;
                content: '';
            }
            .zhzhuilvbot i:after{
                border: 0.5rem solid transparent;
                border-left: 0.5rem solid #efeff4;
                width: 0;
                height: 0;
                position: absolute;
                top: 0;
                right: 0;
                content: '';
            }
            .zhzhuilvcenter{
                float: left;
                width: 40%;
                padding-top: 30px;
                font-size: 0.6rem;
                height: 100px;
            }
            .zhzhuilvleft,.zhzhuilvright{
                padding-top: 30px;
                font-size: 1rem;
                width: 30%;
                float: left;
                height: 100px;
                text-align: center;
            }
            .zhzhuilvleft{

            }
            .zhzhuilvcenter{
                text-align: center;
            }
            .zhzdate{
                font-size: 0.8rem;
                display: inline-block;
                background-color: #f0f0f0;
                padding: 5px 8px;
                margin-top: 10px;
            }
            .clear{
                clear: both;
            }
            .zhzdisplay{
                display: none;
            }
            .zhzcolor{
                color: #000000;
                font-size: 1.2rem;
            }
        </style>
        <?php foreach ($exchange as $key=>$item) { ?>
            <div class="zhzhuilv <?=$key!=0?'zhzdisplay':''; ?>">
                <div class="zhzhuilvcontent">
                    <div class="zhzhuilvleft">
                        IRR
                        <br>
                        <br>
                        <span class="zhzcolor"><?=round($item['irrmoney']*$item['money']);?></span>
                    </div>
                    <div class="zhzhuilvcenter">
                        <img src="/static/themes/default/mobile/images/zhuan.png" width="70"><br>
<!--                        --><?//=round($item['irrmoney']);?><!--IRR=--><?//=round($item['money']);?><!----><?//=$item['currency']?>
                        <br>
                        <p class="zhzdate">
                            <?=$item['updatetime']?>
                        </p>
                    </div>
                    <div class="zhzhuilvright">
                        <?=$item['currency']?>
                        <br>
                        <br>
                        <span class="zhzcolor"><?=round($item['irrmoney']);?></span>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="zhzhuilvbot" count="<?=$key+1; ?>" zong="<?=$exchangecount+1; ?>">
                    <p>Click View IRR：<?=$key==$exchangecount?$exchange[0]['currency']:$exchange[$key+1]['currency']; ?> exchange rate<i></i></p>
                    <p><?=$c['lang_pack']['zhzhuilvtishi'];?></p>
                </div>
            </div>
        <?php } ?>
        <script>

            $('.zhzhuilvbot').click(function () {
                var zong = $(this).attr('zong');
                var count = $(this).attr('count');
                $(".zhzhuilv").hide();
                if (zong == count){
                    count = 0;
                }
                $('.zhzhuilv').eq(count).show();
            })
        </script>
        <?php if($awaiting_order){?>
        <div class="user_awaiting">
        	<div class="title"><?=$c['lang_pack']['user']['OrderStatusAry'][1];?></div>
            <div class="item">
            	<ul>
                	<?php foreach((array)$awaiting_order_products as $k => $v){?>
                    <li<?=$k?'':' style="margin-left:0;"'?>><a href="<?=ly200::get_url($v, 'products');?>"><img src="<?=$v['PicPath'];?>" alt="<?=$v['Name'];?>" /></a></li>
                    <?php }?>
                </ul>
            </div>
            <div class="para">
            	<a class="more" href="/account/orders/view<?=$awaiting_order['OId'];?>.html"><?=$c['lang_pack']['viewDetail'];?></a>
<!--                add by jay start-->
<!--                <a class="payment" href="--><?//=($c['NewFunVersion']>=4 && ($awaiting_order['PId']==1 || $awaiting_order['PId']==2))?"/account/orders/view{$awaiting_order['OId']}.html":"/cart/complete/{$awaiting_order['OId']}.html";?><!--">--><?//=$c['lang_pack']['cart']['paynow'];?><!--</a>-->
                <a class="payment" href="/cart/complete/<?=$awaiting_order['OId']?>.html?d=balance""><?=$c['lang_pack']['user']['pay_balance'];?></a>
<!--               add by jay end-->
                <div class="total"><span><?=cart::iconv_price(0, 1, $awaiting_order['Currency']);?></span><?=cart::currency_format(orders::orders_price($awaiting_order, 0), 0, $awaiting_order['Currency']);?></div>
                <div class="clear"></div>
            </div>
        </div>
        <?php }?>
        <div class="user_menu">
        	<ul>
                <!--   add by jay for balance start   -->
                <style>
                    .user_menu .zhz{
                        background-position: left 8px;
                    }
                    body.lang_fa .user_menu li{
                        background-position: right 8px;
                    }
                </style>
                <li class="balance bg_white zhz" style="height: 100px;">
                    <a href="/account/balance/" style="height: 50%;border-bottom: 1px solid #dddde2;">
                        <?=$c['lang_pack']['my_balance'];?>
                        <?=$balance?"<span>{$balance}</span>":''?>
                        <i></i>
                    </a>
                    <a href="/art/payment-a0043.html" style="height: 50%;">
                        <span><?=$c['lang_pack']['user']['recharge']?></span>
                        <i style="top: 4.2rem;"></i>
                    </a>
                </li>
                <li class="rfq bg_white">
                    <a href="/account/rfq/">
                        <?=$c['lang_pack']['my_rfq'];?>
                        <?=$user_rfq_msg?"<span>{$user_rfq_msg}</span>":''?>
                        <i></i>
                    </a>
                </li>
                <!--   add by jay for balance end   -->
            	<li class="orders"><a href="/account/orders/"><?=$c['lang_pack']['my_orders'];?><?=$user_order_msg?"<span>{$user_order_msg}</span>":''?><i></i></a></li>
                <li class="favorite"><a href="/account/favorite/"><?=$c['lang_pack']['my_fav'];?><?=$user_favorite_msg?"<span>{$user_favorite_msg}</span>":''?><i></i></a></li>
                <li class="coupon"><a href="/account/coupon/"><?=$c['lang_pack']['my_coupon'];?><?=$user_coupon_msg?"<span>{$user_coupon_msg}</span>":''?><i></i></a></li>
                <li class="address"><a href="/account/address/"><?=$c['lang_pack']['my_address_book'];?><i></i></a></li>
                <li class="inbox"><a href="/account/inbox/"><?=$c['lang_pack']['user']['inboxTitle'];?><?=$user_msg_len?"<span>{$user_msg_len}</span>":''?><i></i></a></li>
				<?php if($c['plugin_app']->trigger('distribution', '__config', 'DIST_share')=='enable'){//分销APP是否存在?>
                	<li class="distribution"><a href="/account/p-distribution/index/"><?=$c['lang_pack']['plugins']['DIST_make_money'];?><i></i></a></li>
                <?php }?>
                <li class="password"><a href="/account/password/"><?=$c['lang_pack']['user']['password'];?><i></i></a></li>
            </ul>
        </div>
        <div class="user_button"><a href="/account/logout.html"><?=$c['lang_pack']['sign_out'];?></a></div>
        <div class="user_index_bottom_blank"></div>
    </div>
</div>