<?php !isset($c) && exit();?>
<?php
echo ly200::load_static(
        "{$c['mobile']['tpl_dir']}css/rfq.css",
        "{$c['mobile']['tpl_dir']}css/purchase/animate.css",
        "{$c['mobile']['tpl_dir']}css/purchase/global.css",
        "{$c['mobile']['tpl_dir']}css/purchase/loading.css",
        "{$c['mobile']['tpl_dir']}js/purchase/loading.js",
        "{$c['mobile']['tpl_dir']}js/purchase/lrz.bundle.js",
        "{$c['mobile']['tpl_dir']}js/rfq.js"
);

if ($_GET['spid']){
    $pro = db::get_one("business_product","id = ".$_GET['spid']);
}
?>
<script type="text/javascript">$(function (){user_obj.user_purchase()});</script>

<div id="wrap_rfq">

    <form id="rfq_from" class="frm_rfq" method="post" name="frm_rfq"  enctype="multipart/form-data">
        <h3 class="title"><?=$c['lang_pack']['products']['rfqTitle'];?></h3>
        <div class="clear"></div>
        <div class="row">
            <label style="font-size: 14px;">
                <?=$c['lang_pack']['products']['rfqComplete'];?>
            </label>
            <label style="font-size: 12px; margin-top: 8px;">
                <?=$c['lang_pack']['products']['rfqTips'];?>
            </label>
        </div>

        <div id="error_register_box" class="error_note_box"></div>
        <div class="clear"></div>

        <div class="row">
            <label for="quantity">
                <?=$c['lang_pack']['products']['quantity'];?>
                <span class="fc_red">*</span>
            </label>
            <input id="quantity" name="quantity" class="lib_txt input-half" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" notnull/>
            <select id="quantityUtil" name="quantityUtil" notnull>
                <option value="pieces"><?=$c['lang_pack']['products']['pieces'];?></option>
                <option value="hq_container_40"><?=$c['lang_pack']['products']['hq_container_40'];?></option>
                <option value="bags"><?=$c['lang_pack']['products']['bags'];?></option>
                <option value="boxes"><?=$c['lang_pack']['products']['boxes'];?></option>
                <option value="foot"><?=$c['lang_pack']['products']['foot'];?></option>
                <option value="liter"><?=$c['lang_pack']['products']['liter'];?></option>
                <option value="meter"><?=$c['lang_pack']['products']['meter'];?></option>
                <option value="mille"><?=$c['lang_pack']['products']['mille'];?></option>
                <option value="pack"><?=$c['lang_pack']['products']['pack'];?></option>
                <option value="pairs"><?=$c['lang_pack']['products']['pairs'];?></option>
                <option value="tons"><?=$c['lang_pack']['products']['tons'];?></option>
                <option value="square_feet"><?=$c['lang_pack']['products']['square_feet'];?></option>
                <option value="square_inch"><?=$c['lang_pack']['products']['square_inch'];?></option>
                <option value="square_meters"><?=$c['lang_pack']['products']['square_meters'];?></option>
                <option value="square_mile"><?=$c['lang_pack']['products']['square_mile'];?></option>
                <option value="square_yard"><?=$c['lang_pack']['products']['square_yard'];?></option>
                <option value="unit"><?=$c['lang_pack']['products']['unit'];?></option>
                <option value="yard"><?=$c['lang_pack']['products']['yard'];?></option>
            </select>

            <div class="error_info fl">
                <img src="/static/ico/error.png" alt="">
                <p class="error"></p>
            </div>

        </div>
        <div class="clear"></div>
        <div class="row">
            <label for="productName"><?=$c['lang_pack']['products']['productName'];?><span class="fc_red">*</span></label>
            <input name="productName" id="productName" class="lib_txt lib_input" type="text" size="30" maxlength="20" notnull value="<?=$pro['name']; ?>"/>
            <div class="error_info fl">
                <img src="/static/ico/error.png" alt="">
                <p class="error"></p>
            </div>
        </div>
        <div class="clear"></div>

        <div class="row">
            <label for="description"><?=$c['lang_pack']['products']['description'];?><span class="fc_red">*</span></label>
            <textarea name="description" id="description" class="lib_txt lib_input box_textarea" rows="3" maxlength="500" notnull></textarea>
            <div class="error_info fl">
                <img src="/static/ico/error.png" alt="">
                <p class="error"></p>
            </div>
        </div>
        <div class="clear"></div>
        <div class="row">
            <label for="productURL"><?=$c['lang_pack']['products']['productURL'];?></label>
            <input name="productURL" id="productURL" class="lib_txt lib_input" type="text" value="<?=$_GET['spid']?'http://lemonyyshop.com/supplierproduct/?sp='.$_GET['spid']:''; ?>" />
        </div>
        <div class="row">
            <label for="referQuotation"><?=$c['lang_pack']['products']['referQuotation'];?></label>
            <input style="width: 79%" name="referQuotation" id="referQuotation" class="lib_txt" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
            <span class="lib_txt" style="font-size: 16px; padding: 9px 7px; width: 14%;"><?=$c['lang_pack']['products']['RMB'];?></span>
        </div>
        <div class="clear"></div>
        <div class="row">
            <label for="productImage"><?=$c['lang_pack']['products']['productImage'];?></label>
            <label><?=$c['lang_pack']['products']['productImageTip'];?></label>
            <div id="productImage">
                <div class="input upload_box">
                    <input class="upload_file" type="file" name="picPath_0" onchange="loadImgPreVView(this);" accept="image/gif,image/jpeg,image/png">
                    <div id="pic_show" class="pic_box"></div>
                    <div class="hint_text"><?=$c['lang_pack']['upload_attachments']?></div>
                    <div class="btn_delete hidden" onclick="removeBox(this);"></div>
                </div>


            </div>

        </div>
        <div class="clear"></div>
        <div class="row">
            <label for="factoryInformation"><?=$c['lang_pack']['products']['factoryInformation'];?></label>
            <input name="factoryInformation" id="factoryInformation" class="lib_txt lib_input" type="text" />
        </div>
        <div class="clear"></div>
        <div class="row"><button class="subbtn wrap_rfq form_button_bg btn_submit" type="submit"><?=$c['lang_pack']['products']['submitRFQ'];?></button></div>
        <input type="hidden" id="fileCount" name="fileCount" />
        <input type="hidden" id="source" name="1" />
        <input type="hidden" name="jumpUrl" value="/account/rfq/" />
        <input type="hidden" name="spid" value="<?=$_GET['spid']; ?>">
        <input type="hidden" name="BackUrl" value="<?=$_SERVER['HTTP_REFERER'];?>" />
        <input type="hidden" name="do_action" value="user.purchease_submit" />
    </form>

</div>