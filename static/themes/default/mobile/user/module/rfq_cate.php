<?php !isset($c) && exit(); ?>
<?php
$d_ary = array('list', 'view', 'contact');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];

$user_id = $_SESSION['User']['UserId'];
?>

<?php
echo ly200::load_static("{$c['mobile']['tpl_dir']}css/rfq.css", "{$c['mobile']['tpl_dir']}js/rfq.js");
?>
<style>
    a:link, a:visited {
        color: #444444;
        text-decoration: none;
    }

    a:hover {
        color: #444444;
    }

    .float_bottom {
        width: 100%;
        position: fixed;
        bottom: 0;
        z-index: 999;
    }

    #rfqlist {
        margin-bottom: 52px;
    }

    #rfqlist .prod_box .info {
        margin-right: 50px;
    }

    .float_bottom .add_new {
        width: 90%;
        height: 40px;
        line-height: 40px;
        color: white;
        border: 1px solid #dbdbdb;
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        cursor: pointer;
        display: block;
        margin: 0 auto;
        text-align: center;
        margin-bottom: 12px;
        background-color: #e9af24;
        font-size: 14px;
        z-index: 999;
    }

    .float_bottom .add_new:hover {
        background-color: #e9af24;
        z-index: 999;
    }

    .user_order .prod_list .plist .fr {
        float: right;
        text-align: center;
        position: relative;
        margin-top: 1.8rem;
    }

    .user_order .prod_list .plist .img img{
        object-fit: cover;
        display: block;
    }

    .user_order .prod_list .plist .fr span {
        float: right;
        margin-right: 16px;
    }

    .user_order .prod_list .plist .fr em {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #999;
        border-style: solid;
        float: right;
        display: block;
        position: relative;
        z-index: 10;
    }

    .user_order .prod_list .plist .fr em > i {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #fff;
        border-style: solid;
        display: block;
        position: absolute;
        top: -.4rem;
        right: .1rem;
        z-index: 11;
    }

    #rfqlist .prod_box{
        padding-left: 0;
        padding-right: 16px;
    }

    .user_order .prod_list .title{
        width: 100%;
        height: 2.0rem;
        line-height: 2.0rem;
        background-color: #eeeeee;
    }


    div>.line{
        width: 100%;
        margin-left: 8px;
        margin-top: 10px;
        height: 1px;
        background-color: #eeeeee;
    }

    .user_order .prod_list .plist .info{
        width: auto;
    }

    .user_order .prod_list .plist .info .name{
        margin-top: 8px;
    }

    .user_order .prod_list .plist .info .attr{
        margin-top: 8px;
    }

    .rfqsearch{
        background: #f5f5f4;
        padding: 5px;
    }
    .rfqsearinput{
        height: 30px;
        width: 75%;
        margin-right:10px;
        border-radius: 10px;
    }
    .rfqsubmit{
        color: #646464;
    }
    .prod_list{
        padding: 5px 10px;
        font-size: 0.8rem;
        border-bottom: 1px solid #ccc;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<div id="user">
    <?php
    if ($d == 'list') {
        $pid = (int)$_GET['pid'];
        if($pid == 0) {
            $list = db::get_all('business_cate', "uid = '0,'");
        }else{
            $one = db::get_one("business_cate","id = ".$pid);
            $list = db::get_all('business_cate',"uid = '".$one['uid'].$one['id'].",'");
        }

        ?>
        <div class="rfqsearch">

            <div class="clear"></div>
            <form action="/supplier/" style="position: relative;">
                <input type="text" name="Keyword" class="rfqsearinput">
                <input type="submit" value="" class="rfqsubmit" style="width: 2.5rem;height: 2rem;background: url(/static/themes/default/mobile/images/icon_search_submit.png) no-repeat center/1.5rem;position: absolute;top: 0px;">
                <a href="/products/rfq/" class="rfp" style="position: absolute;top:0px;right:0px;width:2.5rem;height: 2rem;display: block;background: url(/static/themes/default/mobile/footer/02/images/menu_rfp.png) no-repeat center/1.5rem;"></a>
            </form>
        </div>
        <div id="rfqlist" class="user_order">
            <?php
            if ($list) {
                foreach ($list as $k => $v) {
                    if ($pid){
                        $url = '/supplier/?pid='.$v['id'];
                    }else{
                        $url = '/account/rfqcate/?pid='.$v['id'];
                    }
                    $count = db::get_one("business_cate","uid = '".$v['uid'].$v['id'].",'","count(*) as count");
                    ?>
                    <div class="prod_list">
                        <a href="<?=$url; ?>">
                            <div class="prod_box clean">
                                <div class="item clean">
                                    <?=$v['name'.$c['lang']];?><? if(!$pid){ ?>(<?=$count['count']; ?>) <? } ?>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="divide_5px"></div>
        </div>


        <script>
            $(function () {
                $('html').unbind();
            });
        </script>
        <?php
    } ?>
    </div>