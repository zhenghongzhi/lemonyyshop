<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/3
 * Time: 15:03
 */
?>
<?php !isset($c) && exit(); ?>
<?php
    $d_ary = array('list', 'view');
    $d = $_GET['d'];
    !in_array($d, $d_ary) && $d = $d_ary[0];
    //货币汇率
    $all_currency_ary = array();
    $currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
    foreach ($currency_row as $k => $v) {
        $all_currency_ary[$v['Currency']] = $v;
    }
    $id= $_GET['id'];
    $url = "http://119.23.214.213/yy_app/yyshopapi/logistics.php?orderid=".$id."&kehuid=".$user_row['yy_code'];

//    $url = "http://119.23.214.213/yy_app/yyshopapi/logistics.php?orderid=626&kehuid=YY-401";
//    $url = "http://119.23.214.213/yy_app/yyshopapi/logistics.php?orderid=7048&kehuid=YY-488";

    $resq = ly200::curl($url);
    $resq1 = json_decode($resq);
    $log = $resq1->list;
//    echo "<pre>";
//    print_r($log);die;
$count = count($log);
?>
<style>
    body{
        background: #f5f5f5;
    }
    .content{
        border-top: 1px solid #e0e0e0;
        margin-top: 10px;
    }
    .content1{
        position: relative;
    }
    .jindutiao{
        width: 85%;
        /*margin: 0px auto;*/
        height: 1.625rem;
        border-radius: 50px;
        background: #ededed;
        margin-top: 5.75rem;
        position: relative;
    }
    .jindutiao1{
        position: absolute;
        left: 0px;
        top: 0;
        height: 1.625rem;
        width: 70%;
        border-radius: 50px;
        background: #008dbd;
    }
    .date{
        position: absolute;
        text-align: center;
        font-weight: bold;
    }

    .load{
        left: -28px;
        top: 26px;
    }
    .sailing{
        left: 50px;
        top: -50px;
    }
    .delivery{
        top: 26px;
        left: 100px;
    }
    .arrival{
        top: -50px;
        right: 80px;
    }
    .dispatched{
        top: 26px;
        right: -30px;
    }
    .baifenbi{
        position: absolute;
        right: 40px;
        top: 7px;
        font-weight: bold;
    }
    .guihao{
        width: 100%;
        text-align: center;
        position: absolute;
        top: -78px;
        font-weight: bold;
    }
    .footercontent{
        margin-top: 4rem;
    }
    .xuanxiang{
        width: 50%;
        float: left;
        height: 3.5rem;
        background: #FFF;
        font-weight: bold;
        text-align: center;
        line-height: 3.5rem;
        color: #999999;
        border-bottom: 3px solid #f5f5f5;
    }
    .xuanxiangdq{
        color: #000;
        border-bottom: 3px solid #ffd800;
    }
    ul{
        border: 1px solid #c9c8ce;
    }
    ul li{
        float: left;
        text-align: center;
        font-size: 0.75rem;
        padding: 1.6rem 0rem;
        height: 24px;
        border-right: 1px solid #e0e0e0;
        border-bottom:1px solid #e0e0e0;
    }
    .jiben1 li{
        width: 16.04%;
    }
    .jiben1 li:nth-child(5n){
        width: 16.24%;
    }
    .zhuantai1 li{
        width: 20.17%;
    }
    .bold{
        font-weight: bold;
        background: #c9c8ce;
    }

    .zt{
        width: 21.47%;
    }
    .zhuantai1{
        display: none;
    }
    .clear{
        clear: both;
    }
    p { word-wrap:break-word; }
    .content3{
        font-size: 34px;
        text-align: center;
    }
</style>
<div class="content">
    <? if ($count > 0 ){ ?>
    <? foreach ($log as $item) { ?>
        <div class="content1">
            <div class="guihao">
                <?=$item->type1; ?>
            </div>
            <div style="width: 80%;margin: 0 auto;">
                <? if ($item->qstime){
                    $baifenbi = '100%';
                }else{
                    $time = time();
                    $addtime = $item->time1;
                    $tian = floor((time() - $addtime)/(3600*24));
                    if ($tian >= 15) {
                        $tian = 15;
                    }
                    $baifenbi = $tian/18*100;
                    $baifenbi = round($baifenbi).'%';
                    ?>

                <? } ?>
                <div class="jindutiao">
                    <div class="jindutiao1" style="width: <?=$baifenbi; ?>;"></div>
                    <? if ($item->addtime){?>
                        <div class="load date">
                            <img src="/static/themes/default/mobile/images/sanjiao.png" ><br>
                            <?=$c['lang_pack']['user']['zhz_loadingdate']; ?><br>
                            <?=date("Y-m-d", ($item->addtime));?>
                        </div>
                    <? } ?>
                    <? if ($item->time1){?>
                        <div class="sailing date">
                            <?=date("Y-m-d", ($item->time1));?><br>
                            <?=$c['lang_pack']['user']['zhz_sailingdate']; ?><br>
                            <img src="/static/themes/default/mobile/images/daosanjiao.png" >
                        </div>
                    <? } ?>
                    <? if ($item->time1){?>
                        <div class="delivery date">
                            <img src="/static/themes/default/mobile/images/sanjiao.png" ><br>
                            <?=$c['lang_pack']['user']['zhz_arrivaldate']; ?><br>
                            <?=date("Y-m-d", ($item->time1 + 18 * 24 * 3600));?>
                        </div>
                    <? } ?>
                    <? if ($item->qstime){?>
                        <div class="dispatched date">
                            <img src="/static/themes/default/mobile/images/sanjiao.png" ><br>
                            <?=$c['lang_pack']['user']['zhz_dispatched']; ?><br>
                            <?=date("Y-m-d", ($item->qstime));?>
                        </div>
                    <? } ?>

                </div>
                <div class="baifenbi">
                    <?=$baifenbi; ?>
                </div>
            </div>

<!--            <div class="arrival date">-->
<!--                --><?//=$c['lang_pack']['user']['zhz_deliverydate']; ?><!--<br>-->
<!--                <img src="/static/themes/default/mobile/images/daosanjiao.png" >-->
<!--            </div>-->


        </div>
        <div class="footercontent">
            <div class="jiben xuanxiang xuanxiangdq">
                <?=$c['lang_pack']['user']['zhz_basicinformation']; ?>
            </div>
            <div class="zhuangtai xuanxiang">
                <?=$c['lang_pack']['user']['zhz_statusinformation']; ?>
            </div>
            <div class="clear"></div>
            <ul class="jiben1">
                <li class="bold" style="width: 18%;"><?= $c['lang_pack']['user']['zhz_casenumber']; ?></li>
                <li class="bold jc"><?= $c['lang_pack']['user']['zhz_piece']; ?></li>
                <li class="bold jc"><?= $c['lang_pack']['user']['zhz_volume']; ?></li>
                <li class="bold jc"><?= $c['lang_pack']['user']['zhz_weight']; ?></li>
                <li class="bold jc"><?= $c['lang_pack']['user']['zhz_status']; ?></li>
                <li class="bold jc"><?= $c['lang_pack']['user']['zhz_storagetime']; ?></li>

                <div class="clear"></div>
                <? foreach ($item->son as $it) { ?>
                    <li class="" style="width: 18%;"><?=$it->p1; ?></li>
                    <li class="jc"><?=$it->p2; ?></li>
                    <li class="jc"><?=$it->p13; ?></li>
<!--                    <li class="jc">--><?//=$it->p13; ?><!--</li>-->
                    <li class="jc"><?=$it->p14; ?></li>
                    <li class="jc"><p><? if($it->p4 == 1) { echo "Warehousing"; }if($it->p4 == 2) { echo "containerized"; }if($it->p4 == 3) { echo "arrived port"; }if($it->p4 == 4) { echo "out port"; } ?></p></li>
                    <li class="jc"><?=date("Y-m-d", $it->p5); ?></li>
                    <div class="clear"></div>
                <? } ?>
            </ul>
            <ul class="zhuantai1">
                <li class="bold" style="width: 18%;"><?= $c['lang_pack']['user']['zhz_casenumber']; ?></li>
                <li class="bold zt"><?= $c['lang_pack']['user']['zhz_entrytime']; ?></li>
                <li class="bold zt"><?= $c['lang_pack']['user']['zhz_exittime']; ?></li>
                <li class="bold zt"><?= $c['lang_pack']['user']['zhz_arrivaltime']; ?></li>
                <li class="bold zt"><?= $c['lang_pack']['user']['zhz_submissiontime']; ?></li>
                <div class="clear"></div>
                <? foreach ($item->son as $it) { ?>
                    <li class="" style="width: 18%;"><?=$it->p1; ?></li>
                    <li class="zt"><?=date("Y-m-d", $it->p5); ?></li>
<!--                    <li class="zt">--><?//=date("Y-m-d", $it->p15); ?><!--</li>-->
                    <li class="zt"><?=date("Y-m-d", $it->p10); ?></li>
                    <li class="zt">
                        <?=$it->p7; ?><br>
                        <?=date("Y-m-d", ($it->p16 + 3600*24*18)); ?>
                    </li>
                    <li class="zt"><? if($it->p11 == 1){ echo $c['lang_pack']['user']['zhz_nosign']; }else{echo date("Y-m-d", $it->p12);} ?></li>
                    <div class="clear"></div>
                <? } ?>
            </ul>
        </div>
        <div style="height: 30px;background: #fff;"></div>
    <? } ?>

    <? }else{ ?>
        <div class="content3">
        <?=$c['lang_pack']['user']['zhz_nodate']; ?>
        </div>

    <? } ?>
</div>
<script>
    $('.jiben').click(function () {
        $(this).addClass('xuanxiangdq');
        $(this).siblings('.zhuangtai').removeClass('xuanxiangdq');
        $(this).siblings('.jiben1').show();
        $(this).siblings('.zhuantai1').hide();
    })
    $('.zhuangtai').click(function () {
        $(this).addClass('xuanxiangdq');
        $(this).siblings('.jiben').removeClass('xuanxiangdq');
        $(this).siblings('.zhuantai1').show();
        $(this).siblings('.jiben1').hide();
    })
</script>
