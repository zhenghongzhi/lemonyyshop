<?php !isset($c) && exit();?>
<script type="text/javascript">$(function(){user_obj.inbox_init();})</script>
<div id="user" data-user-id="<?=$_SESSION['User']['UserId'];?>">
	<div class="inbox_tab_con">
        <div class="tab_con inbox"></div>
	</div>
	<div class="inbox_detail_pop">
        <div class="box">
            <div class="title"></div>
            <div class="date"></div>
            <div class="con"></div>
            <div class="img"></div>
        </div>
    </div>
    <div class="no_data"><div class="content_blank"><?=$c['lang_pack']['mobile']['no_data'];?></div></div>
	<div class="inbox_form">
		<form method="post" action="/" enctype="multipart/form-data">
			<div class="upload_box">
				<input class="upload_file" id="upload_file" type="file" name="PicPath" onchange="loadImg(this);" accept="image/gif,image/jpeg,image/png">
				<div id="pic_show" class="pic_box"></div>
			</div>
			<div class="input_box">
				<input type="text" name="Content" value="" size="50" notnull />
			</div>
			<input type="submit" class="submit_btn" name="submit_button" value="<?=$c['lang_pack']['user']['send'];?>" />
			<input type="hidden" name="do_action" value="user.write_inbox" />        
		</form>
	</div>
</div>
<script>
function loadImg(obj){
    //获取文件  
    var file = obj.files[0];  
    //创建读取文件的对象  
    var reader = new FileReader();  
    //为文件读取成功设置事件
    reader.onload=function(e) {    
        var imgFile = e.target.result;
		$(obj).next('#pic_show').html('<img src="'+imgFile+'"/><span></span>');
    }; 
    //正式读取文件  
    reader.readAsDataURL(file);
}
</script>
