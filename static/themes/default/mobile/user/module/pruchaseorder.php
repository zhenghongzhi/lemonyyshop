<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/12/31
 * Time: 11:53
 */
?>
<?php !isset($c) && exit(); ?>
<?php
$d_ary = array('list', 'view','agreement','flowing');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];
//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach ($currency_row as $k => $v) {
    $all_currency_ary[$v['Currency']] = $v;
}


?>
<? if($d == 'list'){
    if ($user_row['yy_code']){

        $url = "http://119.23.214.213/yy_app/yyshopapi/orderlist.php?userid=".$user_row['yy_code']."&search=".$_GET['search'];
        $orderlist = ly200::curl($url);
        $orderlist = json_decode($orderlist);
        $list = $orderlist->list;
        $count = $orderlist->count;
    }else{
        $count = 0;
    } ?>

    <div id="user">
    <?php if ($count > 0){ ?>
        <style>
            .search{
                width: 70%;
                height: 2rem;
                border-radius: 50px;
                background: #f5f5f5;
                border: 0px;
                margin-left: 10px;
            }
            .searchimg{
                vertical-align: middle;
            }
            .user_order .prod_list .plist .info {
                width: 56%;
                padding-left: .5rem;
            }
            .caigout{
                height: 4.5rem !important;
            }
            .bill{
                display: block;
                width:80px;
                height: 30px;
                border:1px solid #0cb083;
                text-align: center;
                line-height: 30px;
                position: absolute;
                right: 5px;


            }
        </style>
        <div id="orderlist" class="user_order" data-number="0" data-page="1" data-total="3">
            <form action="" method="get" id="form">
                <input type="text" name="search" class="search">
                <img src="/static/themes/default/mobile/images/header_icon_1_0.png" alt="" class="searchimg">
            </form>
            <script>
                $('.searchimg').click(function () {
                    $('#form').submit();
                })
            </script>
            <?php foreach ($list as $item) { ?>
                <a href="/account/pruchaseorder/view<?=$item->id; ?>.html">
                    <div class="item clean">
                        <div class="title clean ui_border_b">
                            <span class="oid">No.<?=$item->order_id; ?></span>
                            <em><i></i></em>
                            <span class="status">
                                <? if($item->iscai != 2){ ?>
                                <?=$item->status==1?$c['lang_pack']['user']['zhz_orderstauts']['chulizhong']:$c['lang_pack']['user']['zhz_orderstauts']['wancheng']; ?>
                                <? }else{ ?>
                                    <?=$c['lang_pack']['user']['zhz_orderstatus'][$item->isqueding]; ?>
                               <? } ?>
                            </span>
                        </div>
                        <? if($item->iscai != 2){ ?>
    
                            <div class="prod_list">
                                <div class="prod_box clean">
                                    <div class="plist clean first">
                                        <div class="img fl">
                                            <? if ($item->images){ ?>
                                                <img src="http://119.23.214.213/yy_app/<?=$item->images; ?>">
                                            <? }else{ ?>
                                                <img src="/static/themes/default/mobile/images/orderlist.png">
                                            <? } ?>
                                        </div>
                                        <div class="info" style="height: 83px;">
                                            <div class="name" style="height: 40px;color: #000;">
                                                <?=$item->son->subject; ?>
                                            </div>
                                            <div class="number"><?=$item->son->type1; ?></div>
                                            <div class="attr clean"></div>
                                        </div>
                                        <div class="value">
                                            <div class="price">
                                            </div>
                                            <div class="qty">x<?=$item->type2; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="total ui_border_tb">
                                <?=$item->count; ?> items&nbsp;&nbsp;&nbsp;Amount: CNY <?=$item->type1; ?>
<!--                                <a href="/account/pruchaseorder/flowing--><?//=$item->order_id; ?><!--.html">view bill</a>-->
                            </div>
                        <? }else{ ?>
                            <div class="prod_list">
                                <div class="prod_box clean">
                                    <? foreach ($item->son as $it) { ?>
                                        <div class="plist clean first">
                                        <div class="img fl">
                                            <? if ($it->images){ ?>
                                                <img src="http://119.23.214.213/yy_app/<?=$it->images; ?>">
                                            <? }else{ ?>
                                                <img src="/static/themes/default/mobile/images/orderlist.png">
                                            <? } ?>
                                        </div>
                                        <div class="info" style="height: 83px;">
                                            <div class="name" style="height: 40px;color: #000;">
                                                <a href="/account/pruchaseorder/view<?=$item->id; ?>.html"><?=$c['lang']=='_en'?$it->subject_en:$it->subject_fa ?></a>
                                            </div>
                                            <div class="number"><?=$it->type2; ?></div>
                                            <div class="attr clean"></div>
                                        </div>
                                        <div class="value">
                                            <div class="price">
                                                ￥<?=$it->type5; ?>
                                            </div>
                                            <div class="qty">x<?=$it->type4; ?></div>
                                        </div>
                                    </div>
                                    <? } ?>

                                </div>
                            </div>
                            <div class="total ui_border_tb caigout">
                                <?=$item->count; ?> items&nbsp;&nbsp;&nbsp;Amount: CNY <?=$item->type1; ?>
                                <a href="/account/pruchaseorder/flowing<?=$item->order_id; ?>.html" class="bill">view bill</a>
                            </div>
                        <? } ?>
<!--                        <div class="total ui_border_tb caigout">-->
<!--                            --><?//=$item->count; ?><!-- items&nbsp;&nbsp;&nbsp;Amount: CNY --><?//=$item->type1; ?>
<!--                            <a href="/account/pruchaseorder/flowing--><?//=$item->order_id; ?><!--.html">view bill</a>-->
<!--                        </div>-->
                    </div>
                </a>
            <? } ?>
            <div class="divide_8px"></div>
        </div>
    <?php }else{ ?>
        <div class="user_order" style="font-size: 1.5rem; text-align: center;">

            <?=$c['lang_pack']['user']['zhz_nodate'];?>
        </div>
    <?php } ?>
</div>
<? }elseif($d == 'view'){
    $OId = $_GET['OId'];
    $url = "http://119.23.214.213/yy_app/yyshopapi/orderview.php?orderid=".$OId;
    $order = ly200::curl($url);
    $order = json_decode($order);
    $orderview = $order->list;
    $product = $order->product;
    $yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');


    $caiwuurl = "http://47.106.88.138:8080/api/customerByNumber?number=".$yy_code['yy_code'];
    $caiwuurlres = ly200::curl($caiwuurl);
    $caiwuurlres = json_decode($caiwuurlres);
    $caiwuurl1 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$orderview->order_id;
    $caiwuurl1res = ly200::curl($caiwuurl1);
    $caiwuurl1res = json_decode($caiwuurl1res);
//    print_r($caiwuurl1res);die;


//    $caiwuurl1 = "http://47.106.88.138:8080/api/lemonyy1/queryDepositAndBalanceByOrderNumber?number=".$orderview->order_id;
//    $caiwu1 = ly200::curl($caiwuurl1);
//    $caiwu1 = json_decode($caiwu1);

//    print_r($orderview);die;
    ?>
    <style>
        body{
            /*padding-bottom: 100px;*/
        }
        .clear{
            clear: both;
        }
        .detail_box .txt .rows>strong{
            width: 45%;
        }
        .detail_box .txt .rows>span{
            width: 55%;
        }
        .ui_border_b em>i{
            border-width: .4rem 0 .4rem .4rem;
            border-color: transparent transparent transparent #fff;
            border-style: solid;
            display: block;
            position: absolute;
            top: -.4rem;
            right: .1rem;
            z-index: 11;
        }
        .ui_border_b .rows{
            position: relative;
        }
        .ui_border_b i {
            position: absolute;
            right: 0;
            top: -0.25rem;
            background-color: #fff;
        }
        .ui_border_b i:before {
            border-left-color: #bbb;
            right: -2px;
            border: 0.5rem solid transparent;
            border-left: 0.5rem solid #efeff4;
            width: 0;
            height: 0;
            position: absolute;
            top: 0;
            content: '';
        }
        a{
            color: #000000;
        }
        .ui_border_b i:after{
            border: 0.5rem solid transparent;
            border-left: 0.5rem solid #efeff4;
            width: 0;
            height: 0;
            position: absolute;
            top: 0;
            right: 0;
            content: '';
        }
        .zhzfooter{
            position: fixed;
            bottom: 0px;
            width: 100%;
        }
        .footerbutton{
            width: 50%;
            text-align: center;
            float: left;
            height: 4rem;
            line-height: 4rem;
            font-size: 1rem;
            color:#fff;
        }
        .contact{
            background: #fcd340;
        }
        .comfirm{
            background: #1c54a5;
        }
        .comfirmed{
            background: #0bb183;
        }
        .zhzpro{
            margin: 10px;
        }
        .zhz_left{
            width: 70%;
            float: left;
        }
        .zhzimgage{
            width: 20%;
            float: left;
            margin-right: 10px;
        }
        .zhztext{
            float: left;
            width: 70%;
        }
        .zhz_right{
            width: 30%;
            float: right;
            text-align: right;
        }
        .zz{
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            background: rgba(0,0,0,0.7);
            display: none;
            z-index: 99999;
            align-items: center
        }
        .smallimg{
            align-items: center
        }
        table{
            width: 100%;
        }
        table tr td{
            width: 33%;
            text-align: center;
            font-size: .875rem;
            height: 30px;
            line-height: 30px;
        }
    </style>
    <div class="divide_8px ui_border_b"></div>
    <div class="order_detail">
        <div class="detail_box ui_border_b">
            <div class="txt">
            </div>
        </div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_status']; ?></strong>
                    <span>
                        <? if($orderview->iscai != 2){ ?>
                            <?=$item->status==1?$c['lang_pack']['user']['zhz_orderstauts']['chulizhong']:$c['lang_pack']['user']['zhz_orderstauts']['wancheng']; ?>
                        <? }else{ ?>
                            <?=$c['lang_pack']['user']['zhz_orderstatus'][$orderview->isqueding]; ?>
                        <? } ?>

                    </span>
                </div>
            </div>
        </div>
        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <style>
                    .ui_border_b i:after{
                        border-left: 0.5rem solid #ccc;
                    }
                </style>

                <a href="/account/logistics/<?=$orderview->id; ?>.html">
                    <div class="rows clean">
                        <strong><?= $c['lang_pack']['user']['zhz_logisticsprogress']; ?></strong>
                        <i></i>
                    </div>
                </a>

            </div>
        </div>
        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_supplier']; ?></strong>
                    <span><?=$orderview->subject?></span>
                </div>
            </div>

        </div>

        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <? foreach ($product as $item) { ?>
                <div class="zhzpro">
                    <div class="zhz_left">
                        <div class="zhzimgage">
                            <? if ($item->images){ ?>
                                <a class="bigimg" url1="http://119.23.214.213/yy_app/<?=$item->images; ?>"><img src="http://119.23.214.213/yy_app/<?=$item->images; ?>" ></a>
                            <? }else{ ?>
                                <img src="/static/themes/default/mobile/images/orderlist.png" >
                            <? } ?>
                        </div>
                        <div class="zhztext">
                            <div class="info" >
                                <div class="name" style="color: #000;">
                                    <?=$c['lang']=='_en'?$item->subject_en:$item->subject_fa ?>
                                </div>
                                <div class="number"><?=$item->type2; ?></div>
                                <div class="attr clean"></div>
                            </div>
                        </div>
                    </div>
                    <div class="zhz_right">
                        <div class="value">
                            <div class="price">
                                ￥<?=$item->type5; ?>&nbsp;x&nbsp;<?=$item->type4; ?>
                            </div>
<!--                            <div class="qty"></div>-->
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            <? } ?>
        </div>
        <div class="divide_8px"></div>

        <div class="detail_box ui_border_b">
            <div class="txt">
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_total']; ?></strong>
                    <span>￥<?=number_format($orderview->type1+$orderview->wuliu,2);?></span>
                </div>
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_deposit']; ?></strong>
                    <span>￥<?=$orderview->dingjin?number_format($orderview->dingjin,2):0;?></span>
                </div>
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_balance']; ?></strong>
                    <span>￥<?=number_format($orderview->type1-$orderview->dingjin,2); ?></span>
                </div>
<!--                <div class="rows clean">-->
<!--                    <strong>--><?//= $c['lang_pack']['user']['zhz_customerpaid']; ?><!--</strong>-->
<!--                    <span>--><?//=$caiwu1->deposit+$caiwu1->balance;?><!--</span>-->
<!--                </div>-->
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_logisticscost']; ?></strong>
                    <span>￥<?=$orderview->wuliu?number_format($orderview->wuliu,2):number_format(0,2);?></span>
                </div>
            </div>
        </div>
        <div class="divide_8px"></div>


        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <div class="rows clean">
                    <strong><?= $c['lang_pack']['user']['zhz_orderno']; ?></strong>
                    <span><?=$orderview->order_id?></span>
                </div>
            </div>
        </div>
        <? if($orderview->iscai == 2 && $orderview->isqueding != 2){ ?>
        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <a href="/account/pruchaseorder/agreement.html">
                    <div class="rows clean">
                        <strong style="width: 34%;"><?= $c['lang_pack']['user']['zhz_agreement']; ?></strong><input type="checkbox" id="xieyi" value="1" style="width: 15px;height: 15px;" checked><span style="text-align: left;width: 59%;"><?= $c['lang_pack']['user']['zhz_ihavereadandagree']; ?></span>
                        <i></i>
                    </div>
                </a>

            </div>
        </div>
        <? } ?>
        <div class="divide_8px"></div>
        <div class="detail_box ui_border_b">
            <div class="txt">
                <table>
                    <tr>
                        <td><?= $c['lang_pack']['user']['zhz_accountbalance']; ?></td>
                        <td><?= $c['lang_pack']['user']['zhz_paid']; ?></td>
                        <td><?= $c['lang_pack']['user']['zhz_unpaid']; ?></td>
                    </tr>
                    <tr>
                        <td>￥<?=number_format($caiwuurlres->balance,2); ?></td>
                        <td>￥<?=number_format($caiwuurl1res->deposit+$caiwuurl1res->balance,2); ?></td>
                        <td>￥<?=number_format($orderview->type1+$orderview->wuliu-$caiwuurl1res->deposit-$caiwuurl1res->balance,2);?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="divide_8px"></div>
        <? if($orderview->iscai != 2){ ?>
            <div class="detail_box ui_border_b">
                <div class="txt">
                    <div class="rows clean">
                        <strong><?= $c['lang_pack']['user']['zhz_originaldocument']; ?></strong>
                        <span>
                            <? if ($orderview->images){ ?>
                                <a href="http://119.23.214.213/yy_app/<?=$orderview->images; ?>" target="_blank"><img src="http://119.23.214.213/yy_app/<?=$orderview->images; ?>" width="60" alt="Factory direct multi-function full throw spiral tattoo machine Korean tattoo eyebrow tattoo machine"></a>
                            <? }else{ ?>
                                <img src="/static/themes/default/mobile/images/orderlist.png" width="60" alt="Factory direct multi-function full throw spiral tattoo machine Korean tattoo eyebrow tattoo machine">
                            <? } ?>
                        </span>
                    </div>
                </div>
            </div>
        <? } ?>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>
        <div class="divide_8px"></div>




        <div class="zhzfooter">
            <? if($orderview->iscai == 2 && $orderview->isqueding == 1){ ?>
                <div class="footerbutton comfirm"><?=$c['lang_pack']['user']['zhz_confirm']; ?></div>
            <? }else{ ?>
                <div class="footerbutton comfirmed"><?=$c['lang_pack']['user']['zhz_orderstatus'][$orderview->isqueding]; ?></div>
            <? } ?>
            <div class="footerbutton contact zhzshare"><?=$c['lang_pack']['contactUs']; ?></div>
        </div>
        <div class="zz">
            <img src="" alt="" class="smallimg">
        </div>
    </div>
    <script>
        $('.bigimg').on('click',function(){
            $('.zz').css('display','flex');
            var url1 = $(this).attr('url1');
            $('.smallimg').attr('src',url1);
        })
        $('.zz').on('click',function(){
            $('.zz').hide();
        })
        $('.comfirm').on('click',function(){
            var str ="<?=$c['lang_pack']['user']['zhz_conirmation']; ?>"
            var a = confirm(str);
            if (a){
               var b = $("#xieyi").prop('checked');
                if (b){
                    var oid = <?=$OId; ?>;
                    $.ajax({
                        url: 'http://119.23.214.213/yy_app/yyshopapi/orderque.php?orderid='+oid,
                        type: 'GET',
                        dataType: 'json',
                        data: 1,
                        success: function(data){
                            console.log(data);
                            alert(data.msg);
                            window.location.reload();
                        },
                        error: function(msg){

                        }
                    })
                }else{
                    alert('请阅读协议');
                }
            }
        });

        $('.zhzshare').on('click', function() {
            // href="https://api.whatsapp.com/send?phone=8614739070501&text=May I help you?"
            var content="";
            content+="<?=$orderview->order_id?>";
            //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
            content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
            //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
            location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
        })
    </script>
<? }elseif($d == 'agreement'){ ?>
    <div class="detail_content editor_txt">
        <? if($c['lang'] == '_fa'){ ?>
            <?=str::str_code($c['config']['print']['xieyi_fa'], 'htmlspecialchars_decode');?>

        <? }else{ ?>

            <?=str::str_code($c['config']['print']['xiei'], 'htmlspecialchars_decode');?>
        <? }?>
    </div>
<? }elseif($d == 'flowing'){ ?>
    <?php
        $order_id = $_GET['order'];
        $url = "http://47.106.88.138:8080/api/accountByNumber?number=".$order_id;
        $order = ly200::curl($url);
        $order = json_decode($order);
//        print_r($order->list);die;
    ?>
    <style>
        html{
            font: ;
        }
        body{
            background: #f2f2f2;
        }
        .content{
            padding: 0rem .625rem;
            margin-top: 1.125rem;
        }
        .ulcontent{
            background: #fff;
            padding: 1.3rem 0rem;
            border-radius: 10px;
        }
        ul{
            /*background: #fff;*/

            border: 1px solid #c9c8ce;
        }
        ul li{
            float: left;
            width: 25%;
            text-align: center;
            font-size: 0.75rem;
            padding: 1rem 0rem;
            height: 2rem;
            /*line-height: 3rem;*/
            /*border-right: 1px solid #e0e0e0;*/
            border-bottom:1px solid #e0e0e0;
        }
        /*ul li:nth-child(4n-1){*/
            /*width: 22.8%;*/
        /*}*/
        ul li:nth-child(4n){
            border-right: 0px;
        }
        .bold{
            font-weight: bold;
            background: #c9c8ce;
        }
        .clear{
            clear: both;
        }
        .zzz{
            height: 30px;
        }
    </style>

    <div class="content">
        <div class="ulcontent">
            <ul>
                <li class="bold"><?=$c['lang_pack']['user']['zhz_paymenttype']; ?></li>
                <li class="bold"><?=$c['lang_pack']['user']['zhz_money']; ?></li>
                <li class="bold"><?=$c['lang_pack']['user']['zhz_date']; ?></li>
                <li class="bold"><?=$c['lang_pack']['user']['zhz_remarks']; ?></li>
            <? if ($order->list){ ?>
                <? foreach ($order->list as $key => $value){ ?>
                    <li class="zzz"><?=$value->expendType; ?></li>
                    <li class="zzz"><?=$value->expend; ?></li>
                    <li class="zzz"><?=$value->creationTime; ?></li>
                    <li class="zzz"><?=$value->remark; ?></li>
                <? } ?>
            <? }else{ ?>
               <p style="font-size: 2rem; text-align: center;padding: 20px;">NO DATE</p>
            <? } ?>
                <div class="clear"></div>
            </ul>
        </div>



    </div>
<? } ?>