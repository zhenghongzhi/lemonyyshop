<?php
$ShareMenuAry=$c['config']['global']['ShareMenu'];
?>
<footer>
	<div id="prolist_mask_footer"></div>
	<div class="newsletter_box">
		<div class="pic"></div>
		<h3><?=$c['lang_pack']['mobile']['email_sign_up'];?></h3>
		<h4><?=$c['lang_pack']['newsletterTips'];?></h4>
		<div class="newsletter_main">
			<form id="newsletter_form">
				<input type="text" name="Email" value="" placeholder="<?=$c['lang_pack']['emailAddress'];?>" class="form_text" format="Email" notnull />
				<input type="submit" value="<?=$c['lang_pack']['submit'];?>" class="form_button FontBgColor" />
			</form>
		</div>
	</div>
	<div class="follow_us_list">
		<div class="title"><?=$c['lang_pack']['followUs'];?></div>
		<ul>
			<?php
			foreach($c['follow'] as $v){
				if(!$ShareMenuAry[$v]) continue;
			?>
				<li><a rel="nofollow" class="icon_follow_<?=strtolower($v);?>" href="<?=$ShareMenuAry[$v];?>" target="_blank" title="<?=$v;?>"><?=$v;?></a></li>
			<?php }?>
		</ul>
	</div>
	<ul class="footer_list">
		<?php 
        $help_category_row=str::str_code(db::get_limit('article_category', 'UId="0," and IsHelp=0', "CateId, Category{$c['lang']}", $c['my_order'].'CateId asc', 0, 5));
        foreach((array)$help_category_row as $v){
        ?>
			<li>
				<a href="javascript:;" class="list_close help_click"><span class="title"><?=$v['Category'.$c['lang']];?></span><em></em><i></i></a>
				<ul class="help_list clean">
					<?php 
					$help_row=str::str_code(db::get_limit('article', "CateId='{$v['CateId']}'", "AId, Title{$c['lang']}, PageUrl, Url", $c['my_order'].'AId desc', 0, 5));
					foreach((array)$help_row as $vv){
					?>
					<li><a href="<?=ly200::get_url($vv, 'article');?>" title="<?=$vv['Title'.$c['lang']];?>"><?=$vv['Title'.$c['lang']];?></a></li>
					<?php }?>
				</ul>
			</li>
        <?php }?>
	</ul>
	<section class="font_col border_col copyright"><?=$c['config']['global']['CopyRight']['CopyRight'.$c['lang']]?$c['config']['global']['CopyRight']['CopyRight'.$c['lang']].'&nbsp;&nbsp;&nbsp;':'';?><?=$c['powered_by']!=''?$c['powered_by']:'';?></section>
</footer>