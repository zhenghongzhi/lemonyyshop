<footer>
	<div class="wrap">
		<?php
		if($m=='index' || ($m=='products' && $a=='blog')){//仅是首页才显示
		?>
			<div class="icon" plugins="mglobal-0" effect="0-1">
<!--				--><?php
//				//$ad_ary=ly200::ad_custom(4, 0, 10);
//				for($i=0; $i<4; ++$i){
//					//if(!is_file($c['root_path'].$c['web_pack']['mglobal'][0][$i]['Pic'])) continue;
//					$url=$c['web_pack']['mglobal'][0][$i]['Url'];
//				?>
<!--				<div class="item" --><?//=$i?'':'style="margin-left:0;"'?><!-- plugins_pos="--><?//=$i?><!--">-->
<!--					<div class="img"><a href="--><?//=$url?$url:'javascript:;'?><!--"><img plugins_mod="Pic" src="--><?//=$c['web_pack']['mglobal'][0][$i]['Pic']?><!--" /></a></div>-->
<!--					<div plugins_mod="Title" class="title">--><?//=$c['web_pack']['mglobal'][0][$i]['Title']?><!--</div>-->
<!--				</div>-->
<!--				--><?php //}?>
<!--				<div class="clear"></div>-->
			</div>
			<ul class="footer_list">
				<?php 
				$help_category_row=str::str_code(db::get_limit('article_category', 'UId="0," and IsHelp=0', "CateId, Category{$c['lang']}", $c['my_order'].'CateId asc', 0, 5));
				foreach((array)$help_category_row as $v){
				?>
					<li>
						<a href="javascript:;" class="list_close help_click"><?=$v['Category'.$c['lang']];?></a>
						<ul class="help_list clean">
							<?php 
							$help_row=str::str_code(db::get_limit('article', "CateId='{$v['CateId']}'", "AId, Title{$c['lang']}, PageUrl, Url", $c['my_order'].'AId desc', 0, 5));
							foreach((array)$help_row as $vv){
							?>
							<li><a href="<?=ly200::get_url($vv, 'article');?>" title="<?=$vv['Title'.$c['lang']];?>"><?=$vv['Title'.$c['lang']];?></a></li>
							<?php }?>
						</ul>
					</li>
				<?php }?>
			</ul>
			<div class="newsletter_box">
				<h3><?=$c['lang_pack']['mobile']['email_sign_up'];?></h3>
				<div class="newsletter_main">
					<form id="newsletter_form">
						<input type="text" name="Email" value="" placeholder="<?=$c['lang_pack']['newsletterTips'];?>..." class="form_text" format="Email" notnull />
						<input type="submit" value="<?=$c['lang_pack']['ok'];?>" class="form_button FontBgColor" />
					</form>
				</div>
			</div>
		<?php }?>
        <div class="follow_us_list">
            <ul>
                <?php
				$ShareMenuAry=$c['config']['global']['ShareMenu'];
                foreach($c['follow'] as $v){
                    if(!$ShareMenuAry[$v]) continue;
                ?>
                    <li><a rel="nofollow" class="icon_follow_<?=strtolower($v);?>" href="<?=$ShareMenuAry[$v];?>" target="_blank" title="<?=$v;?>"><?=$v;?></a></li>
                <?php }?>
            </ul>
        </div>
        <div class="clear"></div>
<!--        <div class="newsletter_box">-->
<!--            <h3>--><?//=$c['lang_pack']['zhzContactInformation']; ?><!--</h3>-->
<!---->
<!--            <ul class="social-icons">-->
<!--  -->
<!--                <li><a class="facebook" href="mailto:cs@lemonyy.net"><img src="/static/iran/server/img/1_info_icon.png"-->
<!--                                                                          alt=""></a></li>-->
<!---->
<!--                <li><a class="twitter" href="tel:009821-88679854"><img src="/static/iran/server/img/2_info_icon.png" alt=""></a>-->
<!--                </li>-->
<!--  -->
<!--                <li><a class="dribbble" href="weixin://dl/chat?{lemonyyshop}"><img src="/static/iran/server/img/3_info_icon.png"-->
<!--                                                                                   alt=""></a></li>-->
<!---->
<!--                <li><a class="linkedin" href="#"><img src="/static/iran/server/img/4_info_icon.png" alt=""></a></li>-->
<!---->
<!--                <li><a class="dribbble" href="https://api.whatsapp.com/send?phone=8614739070501"><img-->
<!--                                src="/static/iran/server/img/5_info_icon.png" alt=""></a></li>-->
<!---->
<!--                <li><a class="linkedin" href="#"><img src="/static/iran/server/img/6_info_icon.png" alt=""></a></li>-->
<!---->
<!--                <li><a class="mine" href="#"><img src="/static/iran/server/img/7_info_icon.png" alt=""></a></li>-->
<!---->
<!--                <li><a class="twitter" href="#"><img src="/static/iran/server/img/8_info_icon.png" alt=""></a></li>-->
<!---->
<!--            </ul>-->
<!--            <div class="copyright">--><?//=$c['config']['global']['CopyRight']['CopyRight'.$c['lang']]?$c['config']['global']['CopyRight']['CopyRight'.$c['lang']].'<br />':'';?><!----><?//=$c['powered_by']!=''?$c['powered_by']:'';?><!--</div>-->
<!--        </div>-->
    </div>
    <div class="menu">
        <a class="home" href="/"><?=$c['lang_pack']['home']?></a>
<!--        <a class="products" href="javascript:;">--><?//=$c['lang_pack']['category']?><!--</a>-->
        <a class="zhz_products" href="/account/orders/"><?=$c['lang_pack']['my_orders']?></a>
<!--        <a class="rfp" href="/account/rfq/">--><?//=$c['lang_pack']['RFP']?><!--</a>-->
        <a class="rfp" href="/products/rfqcate1/"><?=$c['lang_pack']['RFP']?></a>
        <a class="cart" href="/cart/"><?=$c['lang_pack']['cartStr']?></a>widget prod_info_moq
        <a class="account" href="/account/"><?=$c['lang_pack']['me']?></a>
    </div>
</footer>