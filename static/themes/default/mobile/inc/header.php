<?php !isset($c) && exit();?>
<?php
//body第三方代码
$third_where='IsUsed=1 and IsMeta=0 and IsBody=1';
$third_where.=(ly200::is_mobile_client(1)?' and CodeType in(0,2)':' and CodeType in(0,1)');
$third_row=db::get_all('third', $third_where, '*', 'TId desc');
foreach((array)$third_row as $v) echo $v['Code'];

echo ly200::set_custom_style();
$MobileHeadData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'MobileHeadData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'MobileHeadData');
$mobile_head_data=str::json_data($MobileHeadData, 'decode');
$FbData=$c['config']['Platform']['Facebook']['SignIn']['Data'];

$isStore=0;
if($c['mobile']['HomeTpl']=='store'){//店铺模式
	$isStore=1;
}
?>
<style>
	.MobileHeadBgColor{background-color:<?=$mobile_head_data['BgColor'];?>;}
</style>
<script type="text/javascript">
var ueeshop_config={
	"domain":"<?=ly200::get_domain();?>",
	"date":"<?=date('Y/m/d H:i:s', $c['time']);?>",
	"lang":"<?=substr($c['lang'], 1);?>",
	"currency":"<?=$_SESSION['Currency']['Currency'];?>",
	"currency_symbols":"<?=$_SESSION['Currency']['Symbol'];?>",
	"currency_rate":"<?=$_SESSION['Currency']['Rate'];?>",
	"FbAppId":"<?=$FbData?$FbData['appId']:'';?>",
	"FbPixelOpen":"<?=in_array('facebook_pixel', $c['plugins']['Used'])?1:0;?>",
	"UserId":"<?=(int)$_SESSION['User']['UserId'];?>",
	"TouristsShopping":"<?=(int)$c['config']['global']['TouristsShopping'];?>",
	"PaypalENV":"<?=$c['paypal']=='live'?'production':'sandbox';?>",
	"PaypalLoaclPayment":"<?=$c['config']['paypal']['LocalPayment'];?>",
	"StoreUrl":"<?=(int)$_GET['ueeshop_store']==1?'/store':'';?>",
	"IsMobile":1
}
</script>
<?php if($c['config']['global']['IsCopy']){?>
	<script type="text/javascript">
		document.oncontextmenu=new Function("event.returnValue=false;");
		document.onselectstart=new Function("event.returnValue=false;");
		document.oncontextmenu=function(e){return false;}
	</script>
	<style>
	html, img{-moz-user-select:none; -webkit-user-select:none;}
	</style>
<?php }?>
<div class="left_fixed_side">
	<div class="pop_up nav_side">
	    <a class="close" href="javascript:;"><em></em></a>
	    <div class="pop_up_container nav_container clean">
			<?php
			//普通手机版
			if($c['mobile']['HomeTpl']!='store'){ //店铺模式不显示会员
				if((int)$_SESSION['User']['UserId']){
					$_UserName=substr($c['lang'], 1)=='jp'?$_SESSION['User']['LastName'].' '.$_SESSION['User']['FirstName']:$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName'];
					?>
					<div class="user clean">
						<div class="user_logo" data-url="/account/"></div>
						<a rel="nofollow" href="/account/" class="FontColor"><?=($_SESSION['User']['FirstName'] || $_SESSION['User']['LastName'])?$_UserName:$_SESSION['User']['Email'];?></a>
					</div>
				<?php }else{ ?>
					<div class="user clean">
						<div class="user_logo" data-url="/account/sign-up.html"></div>
						<a rel="nofollow" href="/account/sign-up.html" class="FontColor"><?=$c['lang_pack']['sign_in'];?></a> <?=$c['lang_pack']['or'];?> <a rel="nofollow" href="/account/sign-up.html" class="FontColor"><?=$c['lang_pack']['join_free'];?></a>
					</div>
			<?php } 
			}
			$navcate_row=str::str_code(db::get_all('products_category', 'IsSoldOut=0', "CateId,UId,Category{$c['lang']},PicPath",  $c['my_order'].'CateId asc'));
			$navcate_ary=array();
			foreach($navcate_row as $k=>$v){
			    $navcate_ary[$v['UId']][]=$v;
			}
			$CName='Category'.$c['lang'];
			?>
			<nav class="menu_list">
				<div class="menu_container">
					<?php
					$nav_row=db::get_value('config', "GroupId='themes' and Variable='NavData'", 'Value');
					$nav_data=str::json_data($nav_row, 'decode');
					foreach((array)$nav_data as $k=>$v){
						$nav=ly200::nav_style($v, 1);
						if($c['mobile']['HomeTpl']=='store'){ //店铺模式
							if($k==0){ echo '<div class="ui_border_b item"><a href="/store/">'.$c['lang_pack']['home'].'</a></div>';}
							if(!($nav['Name'] && (strstr($nav['Url'], '/c/') || $nav['Url']=='/products/'))) continue; // facebook店铺
							if($nav['Url']=='/products/') $nav['Url']='/store/products/';
						}else{
							if(!$nav['Name'] || ($nav['Url']=='/holiday.html' && (int)db::get_value('sales_holiday', 'IsUsed=1', 'Number')<21) || $nav['Url']=='/sitemap.html') continue; //临时取消节目模板 网站地图
						}
						$son=0;
						if(($nav['Nav']==3 && $nav['UId'] && count($navcate_ary[$nav['UId']])) || ($nav['Nav']==1 && db::get_row_count('article',"CateId='{$nav['Page']}'"))) $son=1; //又下级
						?>
						<div class="ui_border_b item <?=$son ? 'son' : ''; ?>">
							<a href="<?=$nav['Url'];?>"<?=$isStore?' data-store="1"':'';?> rel="nofollow"><?=$nav['Name'];?></a><div class="icon nav_icon"><em><i></i></em></div>
							<?php if($son){ ?>
								<div class="pop_up category_side">
								    <div class="pop_up_container nav_container clean">
								        <a class="fl close category_close" href="javascript:;"><em><i></i></em></a>
								        <div class="fl category_title"><?=$nav['Name'];?></div>
								        <div class="clear"></div>
								        <div class="menu_list">
											<div class="menu_container">
												<?php
												if($nav['Nav']==3){
													foreach((array)$navcate_ary[$nav['UId']] as $k2=>$v2){
														$ary=$navcate_ary["{$v2['UId']}{$v2['CateId']},"];
														?>
														<div class="ui_border_b item <?=$ary?' son':'';?>">
															<a href="<?=ly200::get_url($v2, 'products_category');?>" title="<?=$v2[$CName];?>"<?=$isStore?' data-store="1"':'';?>><?=$v2[$CName];?></a>
															<div class="icon nav_icon"><em><i></i></em></div>
															<?php if($ary){ ?>
																<div class="pop_up category_side">
																    <div class="pop_up_container nav_container clean">
																        <a class="fl close category_close" href="javascript:;"><em><i></i></em></a>
																        <div class="fl category_title"><?=$v2[$CName];?></div>
																        <div class="clear"></div>
																        <div class="menu_list">
																			<div class="menu_container">
																				<?php
																				foreach((array)$ary as $k3=>$v3){
																					$ary=$navcate_ary["{$v3['UId']}{$v3['CateId']},"];
																					?>
																					<div class="ui_border_b item<?=$ary?' son':'';?>">
																						<a href="<?=ly200::get_url($v3, 'products_category');?>" title="<?=$v3[$CName];?>"<?=$isStore?' data-store="1"':'';?>><?=$v3[$CName];?></a><div class="icon nav_icon"><em><i></i></em></div>
																						<?php if($ary){ ?>
																							<div class="pop_up category_side">
																							    <div class="pop_up_container nav_container clean">
																							        <a class="fl close category_close" href="javascript:;"><em><i></i></em></a>
																							        <div class="fl category_title"><?=$v3[$CName];?></div>
																							        <div class="clear"></div>
																							        <div class="menu_list">
																										<div class="menu_container">
																											<?php
																											foreach((array)$ary as $k4=>$v4){
																												$ary=$navcate_ary["{$v4['UId']}{$v4['CateId']},"];
																												?>
																												<div class="ui_border_b item<?=$ary?' son':'';?>">
																													<a href="<?=ly200::get_url($v4, 'products_category');?>" title="<?=$v4[$CName];?>"<?=$isStore?' data-store="1"':'';?>><?=$v4[$CName];?></a><div class="icon nav_icon"><em><i></i></em></div>
																												</div>
																											<?php }?>
																										</div>
																							        </div>
																							    </div>
																							</div>
																						<?php } ?>
																					</div>
																				<?php }?>
																			</div>
																        </div>
																    </div>
																</div>
															<?php } ?>
														</div>
													<?php }?>
												<?php }else{ 
													$article_nav_row=db::get_all('article', "CateId='{$nav['Page']}'", "AId,Title{$c['lang']},PageUrl,Url", $c['my_order'].'AId desc');
	                       							foreach((array)$article_nav_row as $v1){
	                       							?>
	                       								<div class="ui_border_b item">
															<a href="<?=ly200::get_url($v1, 'article');?>" title="<?=$v1['Title'.$c['lang']];?>"><?=$v1['Title'.$c['lang']];?></a>
														</div>
													<?php } ?>
												<?php } ?>
											</div>
								        </div>
								    </div>
								</div>
							<?php } ?>
						</div>
					<?php }?>
				</div>
				<div class="menu_divide"></div>
				<div class="menu_container">
					<?php
					$cur_lang=substr($c['lang'], 1);
					$web_lang=array_shift(explode('.', $_SERVER['HTTP_HOST']));
					$web_lang=='zh-tw' && $web_lang='zh_tw';
					if(in_array($web_lang, $c['config']['global']['Language']) || reset(explode('.', $_SERVER['HTTP_HOST']))=='www'){
						$dir=str_replace(array_shift(explode('.', $_SERVER['HTTP_HOST'])).'.', '', $_SERVER['HTTP_HOST']);						
					}else{
						$dir=$_SERVER['HTTP_HOST'];						
					}
					$currency_row=db::get_all('currency', 'IsUsed=1');
					?>
					<div class="ui_border_b item son language_item">
						<a href="javascript:;" rel="nofollow"><?=$c['lang_name'][$cur_lang];?></a>
						<div class="icon"><em><i></i></em></div>
					</div>
					<div class="ui_border_b item son currency_item">
						<a href="javascript:;" rel="nofollow"><?=$_SESSION['Currency']['Currency'].(is_file($c['root_path'].$_SESSION['Currency']['FlagPath'])?'<img src="'.$_SESSION['Currency']['FlagPath'].'" alt="'.$_SESSION['Currency']['Currency'].'" />':'');?></a>
						<div class="icon"><em><i></i></em></div>
					</div>
				</div>
			</nav>
	    </div>
	</div>

	<div class="pop_up language_side">
	    <div class="pop_up_container nav_container clean">
	        <a class="fl close category_close" href="javascript:;"><em><i></i></em></a>
	        <div class="fl category_title"><?=$c['lang_name'][$cur_lang];?></div>
	        <div class="clear"></div>
	        <div class="menu_list">
				<div class="menu_container">
					<?php
					//语言
					foreach($c['config']['global']['Language'] as $v){
						if($v==$cur_lang) continue;
						$v_lang=$v;
						$v_lang=='zh_tw' && $v_lang='zh-tw';
						$dir_url='//'.($v==$c['config']['global']['LanguageDefault']?'':$v_lang.'.').$dir.($_SERVER['REQUEST_URI']!='/'?$_SERVER['REQUEST_URI']:'');
						echo '<div class="ui_border_b item"><a href="'.$dir_url.'">'.$c['lang_name'][$v].'</a></div>';
					}?>
				</div>
	        </div>
	    </div>
	</div>

	<div class="pop_up currency_side">
	    <div class="pop_up_container nav_container clean">
	        <a class="fl close category_close" href="javascript:;"><em><i></i></em></a>
	        <div class="fl category_title"><?=$_SESSION['Currency']['Currency'];?></div>
	        <div class="clear"></div>
	        <div class="menu_list">
				<div class="menu_container">
					<?php
					//货币
					foreach((array)$currency_row as $v){
						echo '<div class="ui_border_b item"><a href="javascript:;" class="currency_item" data="'.$v['Currency'].'">'.(is_file($c['root_path'].$v['FlagPath'])?'<img src="'.$v['FlagPath'].'" alt="'.$v['Currency'].'" />':'').$v['Currency'].'</a></div>';
					}?>
				</div>
	        </div>
	    </div>
	</div>

	<div class="pop_up search_side">
		<a class="close" href="javascript:;"><em><i></i></em></a>
	    <div class="pop_up_container nav_container clean">
			<div class="search clean ui_border_b">
<!--                <form action="--><?//=$c['mobile_url'];?><!--/search/" method="get">-->
                <form action="<?=$c['mobile_url'];?>/supplier/" method="get">
	                <input type="search" value="" name="Keyword" placeholder="<?=$c['config']['global']['SearchTips']["SearchTips{$c['lang']}"];?>" class="text fl" />
					<?php /*<input type="reset" value="" class="reset" />*/?>
					<input type="submit" value="" class="sub" />
	            </form>
	        </div>
	        <div class="clear"></div>
	        <div class="menu_list">
				<div class="search_container">
					<div class="search_title"><?=$c['lang_pack']['mobile']['search_history'];?></div>
					<div class="search_list">
						<?php
						$search_history_row=db::get_limit('search_logs', '1', '*', 'AccTime desc', 0, 5);
						foreach((array)$search_history_row as $k=>$v){
						?>
<!--                            <a href="/search/?Keyword=--><?//=$v['Keyword'];?><!--">--><?//=$v['Keyword'];?><!--</a>-->
                            <a href="/supplier/?Keyword=<?=$v['Keyword'];?>"><?=$v['Keyword'];?></a>
						<?php }?>
					</div>
					<div class="search_title"><?=$c['lang_pack']['cart']['sLikeProd'];?></div>
					<div class="search_list">
						<?php
						$search_like_row=str::str_code(db::get_limit('search_logs', '1', '*', 'Number+0 desc', 0, 5));
						foreach((array)$search_like_row as $k=>$v){
						?>
<!--                            <a href="/search/?Keyword=--><?//=$v['Keyword'];?><!--">--><?//=$v['Keyword'];?><!--</a>-->
                            <a href="/supplier/?Keyword=<?=$v['Keyword'];?>"><?=$v['Keyword'];?></a>
						<?php }?>
					</div>
				</div>
	        </div>
	    </div>
	</div>
</div>