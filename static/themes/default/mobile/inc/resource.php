<?php !isset($c) && exit();?>
<?php
echo ly200::load_static(
	"{$c['mobile']['tpl_dir']}css/global.css",
	"{$c['mobile']['tpl_dir']}css/style.css",
	"{$c['mobile']['tpl_dir']}js/jquery-min.js",
	'/static/js/global.js',
	"{$c['mobile']['tpl_dir']}js/rye-touch.js",
	"{$c['mobile']['tpl_dir']}js/global.js",
	"{$c['mobile']['tpl_dir']}lang/{$c['lang']}/css/style.css",
	'/static/js/lang/'.substr($c['lang'], 1).'.js',
	'/static/css/visual.css'
);
if(in_array('facebook_pixel', $c['plugins']['Used'])){
?>
	<!-- Facebook Pixel Code -->
	<script type="text/javascript">
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '<?=$c['config']['Platform']['Facebook']['Pixel']['Data']['PixelID'];?>');
	fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?=$c['config']['Platform']['Facebook']['Pixel']['Data']['PixelID'];?>&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
<?php
}
if(in_array('google_pixel', $c['plugins']['Used'])){
?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?=$c['config']['Platform']['Google']['Pixel']['Data']['TrackingID']?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '<?=$c['config']['Platform']['Google']['Pixel']['Data']['TrackingID']?>');
    </script>
<?php
}
$notice_mid_row = db::get_one('billing','Device=1 and Position=1');
if((int)$notice_mid_row['IsUsed'] && !$_SESSION['m_middle_banner']){
	$_SESSION['m_middle_banner']=1;
	$notice_img_ary=str::json_data($notice_mid_row['PicPath'], 'decode');
?>
	<div id="middle_banner">
		<a href="<?=$notice_mid_row['Url'] ? $notice_mid_row['Url'] : 'javascript:;'; ?>">
			<div class="close"></div>
			<img src="<?=$notice_img_ary[substr($c['lang'], 1)];?>" alt="">
		</a>
	</div>
<?php } ?>

<?php 
$notice_full_row = db::get_one('billing','Device=1 and Position=2');
if((int)$notice_full_row['IsUsed'] && !$_SESSION['m_full_banner']){
	$_SESSION['m_full_banner']=1;
	$notice_img_ary=str::json_data($notice_full_row['PicPath'], 'decode');
?>
	<div id="full_banner">
		<a href="javascript:;" class="close">5s</a>
		<img src="<?=$notice_img_ary[substr($c['lang'], 1)];?>" alt="">
	</div>
<?php } ?>

<?php
$HeaderTpl=array(
	'01' =>	'01',
	'02' =>	'01',
	'03' =>	'01',
	'04' =>	'01',
	'05' =>	'01',
	'06' =>	'01',
	'07' =>	'01',
	'08' =>	'01',
	'09' =>	'01',
	'10' =>	'01',
	'11' =>	'01',
	'12' =>	'01',
	'13' =>	'01',
	'14' =>	'01',
	'15' =>	'01',
	'16' =>	'01',
	'17' =>	'01',
	'store'=>'store',
);
$FooterTpl=array(
	'01' => '01',
	'02' => '01',
	'03' => '01',
	'04' => '01',
	'05' => '01',
	'06' => '01',
	'07' => '01',
	'08' => '01',
	'09' => '01',
	'10' => '02',
	'11' => '03',
	'12' => '04',
	'13' => '05',
	'14' => '04',
	'15' => '06',
	'16' =>	'07',
	'17' =>	'08',
	'store'=>'store',
);
$c['mobile']['HeaderTpl']=$HeaderTpl[$c['mobile']['HomeTpl']];
$c['mobile']['FooterTpl']=$FooterTpl[$c['mobile']['HomeTpl']];
?>
<?=ly200::load_static("{$c['mobile']['tpl_dir']}header/{$c['mobile']['HeaderTpl']}/css/header.css","{$c['mobile']['tpl_dir']}footer/{$c['mobile']['FooterTpl']}/css/footer.css");?>