<?php !isset($c) && exit();?>
<?php
$chat_row=str::str_code(db::get_all('chat', '`Type` IN (1,2,5)', '*', 'CId asc')); //浮动在线客服，只显示QQ,Skype,Email,Whatsapp
?>
<div id="float_chat">
    <div class="float_list">
        <?php 
        /*
        if((int)in_array('facebook_messenger', $c['plugins']['Used'])){
            $messenger_url='https://m.me/'.$c['config']['Platform']['Facebook']['PageId']['Data']['page_id'];
        ?>
            <a href="<?=$messenger_url;?>" class="btn_global FontBgColor message_us" target="_blank"><img src="/static/ico/mobile-message-us-blue.png" alt="Message Us" /></a>
        <?php }*/?>
        <?php
        if(in_array('facebook_messenger', $c['plugins']['Used'])){
            $messenger_url='https://m.me/'.$c['config']['Platform']['Facebook']['PageId']['Data']['page_id'];
            $messenger_data=str::str_code(db::get_value('config', 'GroupId="facebook" and Variable="facebook_messenger"', 'Value'));
            $messenger_data=str::json_data(htmlspecialchars_decode($messenger_data), 'decode');
            if($messenger_data){
                //新
                echo '<div class="fb-customerchat" page_id="'.$messenger_data['page_id'].'" theme_color="'.$messenger_data['customization']['themeColorCode'].'" logged_in_greeting="'.$messenger_data['customization']['greetingTextCode'].'" logged_out_greeting="'.$messenger_data['customization']['greetingTextCode'].'"></div>';
            }else{
                //旧
                echo '<a href="'.$messenger_url.'" class="btn_global FontBgColor message_us" target="_blank"><img src="/static/ico/mobile-message-us-blue.png" alt="Message Us" /></a>';
            }
            if($messenger_data){
        ?>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=<?=$messenger_data['facebook_jssdk_version']?$messenger_data['facebook_jssdk_version']:'v3.2';?>"></script>
                <script type="text/javascript">
                (function(d, s, id){
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {return;}
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                </script>
        <?php
            }
        }?>
        <?php if($chat_row){?><a href="javascript:;" class="btn_global btn_chat global_btn FontBgColor">Chat</a><?php }?>
        <a href="javascript:;" class="btn_global btn_top">Top</a>
    </div>
    <?php if($chat_row){?>
        <dl class="inner_chat">
            <?php /*<dt class="chat_hd"><?=$c['lang_pack']['mobile']['chat'];?></dt>*/?>
            <h3 style="height: 2.5rem;line-height: 2.5rem;text-align: center;font-size: 1.2rem; color: #333;font-weight: bold;"><?=$c['lang_pack']['online']; ?></h3>
            <dd class="chat_bd">
                <?php 
                foreach((array)$chat_row as $v){
                    $name=$v['Name'];
                    $url=sprintf($c['chat']['link'][$v['Type']], $v['Account']);
                    //$c['chat']['type'][$v['Type']]=='WhatsApp' && $name=$v['Account'];
                ?>
                <a class="item <?=strtolower($c['chat']['type'][$v['Type']]);?>" href="<?=$url?$url:'javascript:;';?>" title="<?=$name;?>"><?=$name;?></a><div class="blank6"></div>
                <?php }?>
            </dd>
            <dd class="chat_close"></dd>
        </dl>
    <?php }?>
</div>
<?php
//交换链接
if($c['plugin_app']->trigger('swap_chain', '__config', 'get_urls_list')=='enable'){
	$c['plugin_app']->trigger('swap_chain', 'get_urls_list');
}
?>
<?=ly200::out_put_third_code();?>