<?php !isset($c) && exit();?>
<style type="text/css">
#header_fix{overflow:visible;}
<?php if($c['mobile']['HeadFixed']){?>
	.header_top{width:100%; max-width:100%; position:fixed; top:0; left:0; z-index:1101;}
<?php }?>
</style>
<header>
    <div class="header_top clean <?=$mobile_head_data['BgColor'] == 'FontBgColor' ? 'FontBgColor' : 'MobileHeadBgColor';?>">
		<div class="head_bg_col clean">
        	<div class="head_menu fl">
            	<div class="i1"><a href="javascript:;" class="global_menu"><img src="<?=$c['mobile']['tpl_dir'];?>images/header_icon<?=$mobile_head_data['ImageType']==1 ? '' : '_'.$mobile_head_data['ImageType']; ?>_0.png" alt="" /></a></div>
            </div>
            <div class="logo fl pic_box">
            	<?=html::website_h1(); ?>
            </div>
			<aside class="head_menu fr">
				<div class="fr i3"><a href="/cart/"><img src="<?=$c['mobile']['tpl_dir'];?>images/header_icon<?=$mobile_head_data['ImageType']==1 ? '' : '_'.$mobile_head_data['ImageType']; ?><?=$HeadIcon;?>_2.png" alt="" /><span class="cart_count"><?=$c['shopping_cart']['TotalQty'];?></span></a></div>
				<div class="fr i2"><a href="javascript:;" class="global_search"><img src="<?=$c['mobile']['tpl_dir'];?>images/header_icon<?=$mobile_head_data['ImageType']==1 ? '' : '_'.$mobile_head_data['ImageType']; ?><?=$HeadIcon;?>_1.png" alt="" /></a></div>
			</aside>
		</div>
    </div>
	<?php if($c['mobile']['HeadFixed']){?>
        <div class="header_fill"></div>
    <?php }?>
</header>