/*
 * 广州联雅网络
 */

(function ($){	
	//产品列表页效果
	var t=window.location.pathname,
		i=window.location.search,
   		n=window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	
	$('.dropdown_toggle').on('click', function(){
		/*
		var $this=$(this),
			$menu=$('.dropdown_menu');
		if($menu.is(':visible')){
			$this.removeClass('current');
			$menu.hide();
			$('#prolist_mask, #prolist_mask_footer').hide();
			$('html').removeClass('html_overflow');
		}else{
			$this.addClass('current');
			$menu.show();
			$('#prolist_mask, #prolist_mask_footer').show();
			$('html').addClass('html_overflow');
		}
		*/
		global_obj.div_mask();
		$('.sort_by_side').css('visibility', 'visible').addClass('show');
		$('html').addClass('html_overflow');
		var h=$(window).outerHeight()-$('.sort_by_side .close').outerHeight(true);
		$('.sort_by_side .menu_list').css({'height':h, 'max-height':h});
		
		$('#div_mask').on('click', function(){
			global_obj.div_mask(1);
			$('.sort_by_side').css('visibility', 'hidden').removeClass('show');
			$('html').removeClass('html_overflow');
		});
	});
	
	$('.dropdown_modal').on('click', function(){
		global_obj.div_mask();
		$('.modal_side').css('visibility', 'visible').addClass('show');
		$('html').addClass('html_overflow');
		var h=$(window).outerHeight()-$('.modal_side .close').outerHeight(true)-$('.modal_side .menu_button').outerHeight(true);
		$('.modal_side .menu_list').css({'height':h, 'max-height':h});
		
		$('#div_mask').on('click', function(){
			global_obj.div_mask(1);
			$('.modal_side').css('visibility', 'hidden').removeClass('show');
			$('html').removeClass('html_overflow');
		});
	});

	$('.modal_side .menu_list .son').each(function(){
		if($(this).find('span').size()==0){
			$(this).hide();
		}
	});

	$('.attr_son span').on('click', function(){
		var e=$(this).attr('id'),
			x=$(this).children('strong').text(),
			o=$(this).parents('.item').find('.current_list');
		'' === i && (i='?');
		if(-1 === i.indexOf('Narrow')){
			i += '&Narrow=';
		}
        if(-1 === i.indexOf(e)){
			'' === i ? i = '': i += '+', i += e;
			o.append('<span data-id="'+e+'">'+x+'</span>');
			$(this).siblings('span').each(function(index, elem){
				var s_e = $(elem).attr('id');
				if(i.indexOf(s_e) != -1){
					// 同属性存在其他已选择属性值
					var s_t=i.indexOf(s_e),
		            	s_n=i.indexOf('+', s_t);
					-1 === s_n && (s_n=i.length);
		            var s_a=i.substring(s_t - 1, s_n);
					-1 !== s_a.indexOf('=') && (s_a=s_a.replace('=', ''));
		            i=i.replace(s_a, '');
					o.find('span[data-id="'+s_e+'"]').remove();
					$(elem).removeClass('current FontBgColor');
				}
			});
		}else{
            var t=i.indexOf(e),
            	n=i.indexOf('+', t);
			-1 === n && (n=i.length);
            var a=i.substring(t - 1, n);
			-1 !== a.indexOf('=') && (a=a.replace('=', ''));
            i=i.replace(a, '');
			o.find('span[data-id="'+e+'"]').remove();
        }
		$(this).toggleClass('current FontBgColor');
	});
	$('.refine_search').on('click', function(){
        if(i){
			//-1 === i.indexOf('+') ? (i=i.replace('&Narrow=', '')) : (i=i.replace('Narrow=+', 'Narrow='));
			i.indexOf('+') && (i=i.replace('Narrow=+', 'Narrow='));
            window.location.replace(t+i);
        }else{
        	window.location.replace(t);
		}
    }),
    $('.clear_all').on('click', function(){
        i=i.replace(/\&Narrow\=(.*)+/, ''),
        window.location.replace(t+i);
    })
	$('.img a img').load(function(){
		$(this).parent().parent('.img').css('background-image', 'none');
	});
	
	//列表显示方式切换
	var view_show={
		value:{
			'Default':($('#prolist').attr('data-default') ? 'gallery' : 'list'), //默认显示方式 (list | gallery)
			'isStore':($('#prolist').attr('data-store')==1?1:0)
		},
		init:function(){
			//默认事件
			$('.dropdown_'+view_show.value.Default).hide().siblings().show();
			$('#prolist>.prolist_'+view_show.value.Default).show().siblings().hide();
			view_show.ajax_product_list({"page":0, "Data":$("#prolist").attr("data-pro"), "IsSearch":$("#prolist").attr("data-search")}, 1);
			//点击切换事件
			$('.dropdown_list, .dropdown_gallery').on('tap', function(){
				$(this).hide().siblings().show();
				$('#prolist>.prolist_'+$(this).attr('data-type')).show().siblings().hide();
				view_show.value.Default=$(this).attr('data-type');
				$('html').seckillPrice();
			});
		},
		list:function(data){
			$html='';
			$html+='<div class="pro_item item pro_list_item ui_border_b">';
			$html+='	<div class="img pic_box fl">';
			$html+='		<a href="'+data.Url+'" title="'+data.Name+'"><img src="'+data.Img+'"><span></span></a>';
			$html+='	</div>';
			$html+='	<div class="desc">';
			$html+='		<div class="name"><a href="'+data.Url+'" title="'+data.Name+'">'+data.Name+'</a></div>';
			$html+='		<div class="price clean">';
			if(data.OldPrice>0){ //市场价
			$html+='			<del class="old_price">'+ueeshop_config.currency+' '+ueeshop_config.currency_symbols+'<span class="price_data" data="'+data.OldPriceData+'">'+data.OldPrice+'</span></del>';
			}
			$html+='			<div class="cur_price fl">'+ueeshop_config.currency+' '+ueeshop_config.currency_symbols+'<span class="price_data" keyid="'+data.ProId+'">'+data.CurPrice+'</span></div>';
			if(data.IsPromition && data.PromotionDiscount){ //促销折扣
			$html+='			<div class="discount fl DiscountBgColor icon_discount"><span>'+data.PromotionDiscount+'</span><em></em></div>';
			}
			$html+='			<div class="discount fl DiscountBgColor icon_seckill"><span>'+lang_obj.manage.sales.seckill+'</span><em></em></div>';
			$html+='		</div>';
			if(data.IsFreeShipping==1){ //免运费
			$html+='		<div class="free_shipping">'+lang_obj.products.free_shipping+'</div>';
			}
			$html+='		<div class="view clean">';
			if(data.Star){ //评分
			$html+='			<div class="star fl">'+data.Star+'</div>';
			}
			// if(data.Sales){ //销量
			// $html+='			<div class="sold fr">Sold: '+data.Sales+'</div>';
			// }
			$html+='		</div>';
			$html+='	</div>';
			$html+='</div>';
			return $html;
		},
		gallery:function(data){
			$html='';
			$html+='<div class="pro_item item'+(view_show.value.isStore?' pro_store_item':'')+'">';
			$html+='	<div class="img pic_box">';
			$html+='		<a href="'+data.Url+'" title="'+data.Name+'"><img src="'+data.Img+'"><span></span></a>';
			$html+='	</div>';
			$html+='	<div class="name"><a href="'+data.Url+'" title="'+data.Name+'">'+data.Name+'</a></div>';
			$html+='	<div class="price">';
			if(data.IsPromition && data.PromotionDiscount){ //促销折扣
			$html+='		<div class="discount fr DiscountBgColor icon_discount"><span>'+data.PromotionDiscount+'</span><em></em></div>';
			}
			$html+='		<div class="discount fr DiscountBgColor icon_seckill"><span>'+lang_obj.manage.sales.seckill+'</span><em></em></div>';
			$html+='		<div class="cur_price">'+ueeshop_config.currency+' '+ueeshop_config.currency_symbols+'<span class="price_data" keyid="'+data.ProId+'">'+data.CurPrice+'</span></div>';
			if(data.OldPrice>0){ //市场价
			$html+='		<del class="old_price">'+ueeshop_config.currency+' '+ueeshop_config.currency_symbols+'<span class="price_data" data="'+data.OldPriceData+'">'+data.OldPrice+'</span></del>';
			}
			$html+='	</div>';
			$html+='	<div class="star">'+(data.Star?data.Star:'')+'</div>'; //评分
			$html+='</div>';
			return $html;
		},
		ajax_product_init:function(){
			$('#prolist .btn_view').off().on('tap, click', function(){
				var $Num=parseInt($("#prolist").attr('data-number'));
				if($Num==0){
					$('#prolist .btn_view').remove();
					$("#prolist").attr('data-number', '1');
					var page=parseInt($("#prolist").attr("data-page"));
					if(!isNaN(page)){
						var Total=parseInt($("#prolist").attr('data-total'));
						if(page>=Total){
							return false;
						}
						$("#prolist").attr('data-page', page+1);
						view_show.ajax_product_list({"page":page+1, "Data":$("#prolist").attr("data-pro"), "IsSearch":$("#prolist").attr("data-search")}, 0);
					}
				}
			});
			
			if($("#prolist .content_more").length){
				setTimeout(function(){
					$("#prolist .content_more").fadeOut();
				}, 2000);
			}
			$('html').seckillPrice();
		},
		ajax_product_list:function(data, clear){
			var $Obj=$('#prolist>.prolist_'+view_show.value.Default),
				$html='';
			clear && $Obj.html('');
			$Obj.loading();
			$(".loading_msg").css({"top":0, "position":"initial", "width":"auto", "height":'4rem', "background-position":"center"});
			setTimeout(function(){
				$.ajax({
					url:'/ajax/products_list.html'+(view_show.value.isStore?'?ueeshop_store=1':''),
					async:false,
					type:'post',
					data:data,
					dataType:'json',
					success:function(result){
						if(result.ret==1){
							$('#prolist>div').each(function(){
								$html='';
								//产品内容
								if($(this).attr('data-type')=='list'){
									for(k in result.msg.items){
										$html+=view_show.list(result.msg.items[k]);
									}
								}else{
									for(k in result.msg.items){
										k=parseInt(k);
										$html+=view_show.gallery(result.msg.items[k]);
										//(k+1)%2==0 && ($html+='<div class="clear"></div>');
									}
								}
								//更多按钮
								if(result.msg.status==1){
									$html+='<div class="content_blank">'+
                                        lang_obj.mobile.no_data+
                                        '<br><br><br>'+
                                        '<a style="text-decoration: underline;" href="/account/rfp/">'+lang_obj.mobile.rfqTitle+'</a>'+
                                        '</div>';
								}else if(result.msg.status==2){ //总共只有一页 或者 最后一页
									$html+='<div class="content_more">'+lang_obj.mobile.no_data+'</div>';
								}else{
									$html+='<div class="btn_global btn_view"><button class="btn_global FontBgColor">'+lang_obj.mobile.load_more+'</button></div>';
								}
								$(this).append($html).unloading();
							});
							$('#prolist').attr('data-number', '0');
							view_show.ajax_product_init();
						}
					}
				});
			}, 300);
		}
	};
	if(view_show.value.isStore){ //店铺模式
		view_show.value.Default='gallery';
	}
	view_show.init();
	
	/*
	旧代码的记录
	function ajax_product_init(){
		$('#prolist .btn_view').off().on('tap', function(){
			var $Num=parseInt($("#prolist").attr('data-number'));
			if($Num==0){
				$(this).remove();
				$("#prolist").attr('data-number', '1');
				var page=parseInt($("#prolist").attr("data-page"));
				if(!isNaN(page)){
					var Total=parseInt($("#prolist").attr('data-total'));
					if(page>=Total){
						return false;
					}
					$("#prolist").attr('data-page', page+1);
					ajax_product_list({"page":page+1, "Data":$("#prolist").attr("data-pro"), "IsSearch":$("#prolist").attr("data-search")}, 0);
				}
			}
		});
		
		if($("#prolist .content_more").length){
			setTimeout(function(){
				$("#prolist .content_more").fadeOut();
			}, 2000);
		}
	}
	
	function ajax_product_list(data, clear){
		clear && $("#prolist").html('');
		$Obj.loading();
		$(".loading_msg").css({"top":0, "position":"initial", "width":"auto", "height":'4rem', "background-position":"center"});
		setTimeout(function(){
			$.ajax({
				url:"/ajax/products_list.html",//"/?m=ajax&a=products_list",
				async:false,
				type:'post',
				data:data,
				dataType:'html',
				success:function(result){
					if(result){
						$("#prolist").append(result).unloading();
						$('#prolist').attr('data-number', '0');
						ajax_product_init();
					}
				}
			});
		}, 300);
	}
	*/
})(jQuery);