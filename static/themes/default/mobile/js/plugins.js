/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var plugins_obj={
	/******************* 分销 Start *******************/
	distribution_init:function(){
		$('.share_third>a').click(function(){ //分享地址
			$(this).shareThis($(this).attr('data'), $('.share_third').attr('data-title'), $('.share_third').attr('data-url'));
		});
		if($('#distribution_orders').size() || $('#distribution_withdraw').size() || $('#distribution_distor').size()){
			var $this=$('#distribution_orders').size()?'#distribution_orders':($('#distribution_withdraw').size()?'#distribution_withdraw':'#distribution_distor'),
				ajax_url=$('#distribution_orders').size()?'/ajax/user_distribution_orders.html':($('#distribution_withdraw').size()?'/ajax/user_distribution_withdraw.html':'/ajax/user_distribution_distor.html');
				page=1,
				lock=0;
				loading=0;
			var loading_data=function(obj){
				if(!loading){
					loading=1;
					var obj=$(obj);
					obj.loading();
					$(".loading_msg").css({"top":0, "position":"initial", "width":"auto", "height":'4rem', "background-position":"center"});
					setTimeout(function(){
						$.ajax({
							url:ajax_url,
							async:false,
							type:'post',
							data:{page:page},
							dataType:'html',
							success:function(result){
								if(result){
									loading=0;
									page++;
									obj.append(result).unloading();
									if($('.content_more').size() || $('.content_blank').size()) lock=1;
								}
							}
						});
					}, 300);
				}
			}
			loading_data($this);
			$(window).scroll(function(){
				var $winTop		= $(window).scrollTop(),
					$winH		= $(window).height(),
					$listTop	= $($this).offset().top,
					$listHeight	= $($this).outerHeight();
				if($winTop+$winH>$listTop+$listHeight && !loading && !lock){
					loading_data($this);
				}
			});
		}
		$('#distribution_withdraw_form input[type=button]').click(function(){
			var form =$('#distribution_withdraw_form');
			if(global_obj.check_form(form.find('*[notnull]'), form.find('*[format]'))){return false;}
			$.post('?',form.serialize(),function(data){
				if(data.ret==1){
					window.location.href='/account/p-distribution/record/';
				}else{
					$('html').tips_box(data.msg, 'error');	
				}
			},'json');
		});
	}
	/******************* 分销 End *******************/
};