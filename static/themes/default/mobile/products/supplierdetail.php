<?php !isset($c) && exit();?>
<?php
    $id = $_GET['sp'];
    $business = db::get_one("business","BId = '$id'");

    $product = db::get_all("business_product","is_show = 1 and is_del = 1 and pid = '$id'");

//    print_r($product);die;
//    echo 1;die;
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
    <title><?=$c['lang_pack']['shield'];?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection" />
    <link rel="canonical" href="<?=ly200::get_domain();?>/products/"/>
    <?php
    include("{$c['mobile']['theme_path']}inc/resource.php");
    //echo ly200::load_static("{$c['mobile']['tpl_dir']}products/{$c['mobile']['ListTpl']}/css/style.css");
    ?>
    <link href="/static/themes/default/mobile/css/suggestionProduct.css" rel="stylesheet" type="text/css">

</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['mobile']['theme_path']}inc/header.php");?>
<div id="u_header"<?=$a!='login'?' class="fixed"':'';?>>
    <a class="back" href="javascript:history.back(-1);"></a>
    <a class="menu global_menu" href="javascript:;"></a>
    <a class="cart" href="/cart/"><?=(int)$c['shopping_cart']['TotalQty']?'<i class="FontBgColor"></i>':''?></a>
    <div class="title"><?=$user_title[$a];?></div>
</div>
<div id="user">
    <!-- start -->
    <style>
        body {
            background: #f8f8f8;
        }

        .clear {
            clear: both;
        }

        .yunfeicontent {
            margin-top: 60px;
            background: #fff;
            height: 210px;
            padding: 15px 10px;
        }

        .zhzcontent {
            margin-top: 50px;
        }

        .yunfeititle {
            font-size: 16px;
        }

        .yunfei {
            font-size: 18px;
            color: #0fc20f;
        }

        .yunfeiname {
            font-weight: bold;
            font-size: 18px;
            color: #000;
            margin-top: 15px;
        }

        .yunfeidec p,
        .yunfeidec span {
            margin: 8px 0px;
            font-size: 14px;
        }

        .jisuan {
            margin-top: 10px;
            background: #fff;
            padding: 15px 10px;
            font-size: 18px;
            font-weight: blod;
        }

        .jslist {
            margin-top: 20px;
            padding-bottom: 20px;
            border-bottom: 1px dashed #f0f0f0;
        }

        .jslistleft {
            float: left;
            width: 42%;
            margin-top: 8px;
        }

        .jslistright {
            float: left;
            width: 58%;
        }

        .but {
            display: inline-block;
            width: 30px;
            height: 31px;
            line-height: 30px;
            text-align: center;
            border: 1px solid #f0f0f0;
        }

        .buttonjian {
            margin-right: -5px;
        }

        .bottonjia {
            margin-left: -5px;
        }

        .shuzhi {
            width: 120px;
            border: 1px solid #f0f0f0;
            height: 33px;
        }

        .displaynone {
            display: none;
        }

        .shuoming {
            margin-top: 10px;
            background: #fff;
            padding: 15px 10px;
            margin-bottom: 150px;
        }

        .cbottom {
            position: fixed;
            bottom: 0px;
            width: 100%;
        }

        .ccombuttom {
            display: inline-block;
            width: 50%;
            height: 50px;
            line-height: 50px;
            text-align: center;
            float: left;
            font-size: 1rem;
        }

        .cbottom a {
            color: #ffffff;
        }

        .cnow {
            /*background: #e49535;*/
            background-image: linear-gradient(#ffd388, #d7780f);
        }

        .srfq {
            background-image: linear-gradient(#13c4bb, #0689c1);
        }

        .zhzfooter {
            text-align: center;
            height: 50px;
            line-height: 50px;
            font-size: 1rem;
            background-color: #FF5959;
        }
    </style>
    <div class="zhzcontent">
        <div id="breaking">Supplier Details</div>
        <div id="newsTicker">
            <p></p>
        </div>
        <div class="card_sup">

            <div class="thumbnail_sup">
                <? if ($business['logoPath']){ ?>
                    <img src="<?=$business['logoPath']; ?>" alt="" style="width: 100px;" class="logo">
                <? }else{ ?>
                    <p><?=mb_substr($business['Name'], 0, 1); ?></p>
                <? } ?>
            </div>
            <div class="right">
                <h1><?=$business['Name']; ?></h1>
                <div class="separator"></div>
                <? if($business['city']){ ?>
                    <p><?=$business['city']; ?></p>
                    <br>
                <? } ?>
                <? if($business['Address']){ ?>
                    <p><?=$business['Address']; ?></p>
                    <br>
                <? } ?>
                <? if($business['Phone']){ ?>
                    <p><?=$business['Phone']; ?></p>
                    <br>
                <? } ?>
                <? if($business['companyDesc']){ ?>
                    <p><?=$business['companyDesc']; ?></p>
                    <br>
                <? } ?>

            </div>
            <h5>Big</h5>
            <h6 style="top: 196px;">Suppliers</h6>
            <div class="fab"><img style="width: 50%;margin: -10% auto;" src="/static/themes/default/mobile/images/phone+icon-1320183329252923405.png"
                                  alt=""></div>

        </div>
        <div class="mainSuggContainer">
            <? foreach ($product as $key => $value){
                $img=ly200::get_size_img($value['PicPath_0'], '500x500');
                ?>
                <div class="itemProduct img pic_box">

                    <img class="ImgProduct" src="<?=$img?$img:'/static/themes/default/mobile/images/timg.jpg'; ?>" alt="">
                    <p class="txtProducts"><?=$value['name']; ?></p> <br>
                    <p class="detailProducts"></p>
                    <div class="ReadMoreDetail"><a href="/supplierproduct/?sp=<?=$value['id']; ?>">read more</a></div>
                </div>
            <? } ?>
        </div>

    </div>





    <div class="cbottom">
        <a class="ccombuttom cnow zhzshare"><?=$c['lang_pack']['zhzcontactnow']; ?></a>
        <a href="/account/rfp/" class="ccombuttom srfq"><?=$c['lang_pack']['zhzsendrfq']; ?></a>
    </div>
    <script src="/static/themes/default/mobile/js/script.js"></script>
    <script src="/static/themes/default/mobile/js/productScript.js"></script>
    <!-- end -->
</div>
<div class="wrapper">
    <div id="u_title">
        <div class="u_title_box">
            <!--   add by zhz start            -->
            <h1>Supplier Details</h1>
        </div>
    </div>
    <div class="divide_8px ui_border_b"></div>
    <style>
        body {
            background: #f8f8f8;
        }

        .clear {
            clear: both;
        }

        .yunfeicontent {
            margin-top: 60px;
            background: #fff;
            height: 210px;
            padding: 15px 10px;
        }

        .zhzcontent {
            margin-top: 50px;
        }

        .yunfeititle {
            font-size: 16px;
        }

        .yunfei {
            font-size: 18px;
            color: #0fc20f;
        }

        .yunfeiname {
            font-weight: bold;
            font-size: 18px;
            color: #000;
            margin-top: 15px;
        }

        .yunfeidec p,
        .yunfeidec span {
            margin: 8px 0px;
            font-size: 14px;
        }

        .jisuan {
            margin-top: 10px;
            background: #fff;
            padding: 15px 10px;
            font-size: 18px;
            font-weight: blod;
        }

        .jslist {
            margin-top: 20px;
            padding-bottom: 20px;
            border-bottom: 1px dashed #f0f0f0;
        }

        .jslistleft {
            float: left;
            width: 42%;
            margin-top: 8px;
        }

        .jslistright {
            float: left;
            width: 58%;
        }

        .but {
            display: inline-block;
            width: 30px;
            height: 31px;
            line-height: 30px;
            text-align: center;
            border: 1px solid #f0f0f0;
        }

        .buttonjian {
            margin-right: -5px;
        }

        .bottonjia {
            margin-left: -5px;
        }

        .shuzhi {
            width: 120px;
            border: 1px solid #f0f0f0;
            height: 33px;
        }

        .displaynone {
            display: none;
        }

        .shuoming {
            margin-top: 10px;
            background: #fff;
            padding: 15px 10px;
            margin-bottom: 150px;
        }

        .cbottom {
            position: fixed;
            bottom: 0px;
            width: 100%;
        }

        .ccombuttom {
            display: inline-block;
            width: 50%;
            height: 50px;
            line-height: 50px;
            text-align: center;
            float: left;
            font-size: 1rem;
        }

        .cbottom a {
            color: #ffffff;
        }

        .cnow {
            /*background: #e49535;*/
            background-image: linear-gradient(#ffd388, #d7780f);
        }

        .srfq {
            background-image: linear-gradient(#13c4bb, #0689c1);
        }

        .zhzfooter {
            text-align: center;
            height: 50px;
            line-height: 50px;
            font-size: 1rem;
            background-color: #FF5959;
        }
    </style>
    <div class="comname">
        <? if ($business['logoPath']){ ?>
            <img src="<?=$business['logoPath']; ?>" alt="" style="width: 100px;" class="logo">
        <? }else{ ?>
            <div class="logo">
                <div class="logo1 zhzlogo1" style="background: #f5f5f5;">
                    <span style="font: 500 4rem 'Comic Sans MS';
                                        color: white;
                                       text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de;
                                        "><?=mb_substr($business['Name'], 0, 1); ?></span>
                </div>
            </div>
        <? } ?>
        <div style="color: #666666;margin-top: 5px;font-size: 1rem;">
            <span ><?=$business['Name']; ?></span>
        </div>
        <div class="clear"></div>
    </div>
    <div class="divide_8px ui_border_b"></div>
    <div class="comcontent">
        <h3>企业信息</h3>
        <ul>
            <? if($business['city']){ ?>
                <li><span class="cleft">城&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市</span><span class="cright"><?=$business['city']; ?></span></li><div class="clear"></div>
            <? } ?>
            <? if($business['Address']){ ?>
                <li><span class="cleft">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址</span><span class="cright"><?=$business['Address']; ?></span></li><div class="clear"></div>
            <? } ?>
            <? if($business['Phone']){ ?>
                <li><span class="cleft">电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话</span><span class="cright"><?=$business['Phone']; ?></span></li><div class="clear"></div>
            <? } ?>
            <? if($business['companyDesc']){ ?>
                <li><span class="cleft">主营产品</span><span class="cright"><?=$business['companyDesc']; ?></span></li><div class="clear"></div>
            <? } ?>
        </ul>
    </div>
    <div class="divide_8px ui_border_b"></div>
    <div class="divide_8px ui_border_b"></div>
    <div class="divide_8px ui_border_b"></div>
    <div class="comfooter">
        <div class="prolist_gallery" data-type="gallery">
            <? foreach ($product as $key => $value){
                $img=ly200::get_size_img($value['PicPath_0'], '500x500');
                ?>

                <div class="pro_item item">
                    <div class="img pic_box">
                        <a href="/supplierproduct/?sp=<?=$value['id']; ?>" title="<?=$value['name']; ?>">
                            <img src="<?=$img; ?>">
                            <span></span>
                        </a>
                    </div>
                    <div class="name">
                        <a href="/supplierproduct/?sp=<?=$value['id']; ?>" title="<?=$value['name']; ?>">4<?=$value['name']; ?></a>
                    </div>
                </div>
            <? } ?>
            <div class="clear"></div>
        </div>
        <?=$business['product']; ?>
    </div>

    <div class="cbottom">
        <a class="ccombuttom cnow zhzshare"><?=$c['lang_pack']['zhzcontactnow']; ?></a>
        <a href="/account/rfp/" class="ccombuttom srfq"><?=$c['lang_pack']['zhzsendrfq']; ?></a>
    </div>
</div>
<script>
    $('.zhzshare').on('click', function() {
        // href="https://api.whatsapp.com/send?phone=8614739070501&text=May I help you?"
        var content="";
        content+="<?=$business['Name']; ?>";
        //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
        content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
        //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
        location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
    })
</script>
</body>
</html>