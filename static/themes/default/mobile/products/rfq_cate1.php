<?php !isset($c) && exit(); ?>
<?php

$d_ary = array('list', 'view', 'contact');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];
$user_id = $_SESSION['User']['UserId'];
?>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection">
    <link rel="canonical" href="http://lemonyyshop.com/account/rfqcate/">
    <link rel="shortcut icon" href="/u_file/1912/photo/5d61fce285.png">
    <meta name="keywords" content="LemonYY China trade starts here">
    <meta name="description" content="LemonYY China trade starts here">
    <title>LemonYY China trade starts here</title>
    <link href="/static/themes/default/mobile/css/global.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" src="/static/themes/default/mobile/js/jquery-min.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/js/global.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/rye-touch.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/global.js?v=4.01320"></script>
    <link href="/static/themes/default/mobile/lang/_en/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/js/lang/en.js?v=4.01320"></script>
    <link href="/static/css/visual.css?v=4.01320" rel="stylesheet" type="text/css">
    <!-- Facebook Pixel Code -->
    <script type="text/javascript">
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '');
        fbq('track', "PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id="></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '');
    </script>


    <link href="/static/themes/default/mobile/header/01/css/header.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/footer/02/css/footer.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/user.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/themes/default/mobile/js/user.js?v=4.01320"></script>
    <script type="text/javascript">$(function(){user_obj.user_index()});</script>
</head>
<?php
echo ly200::load_static("{$c['mobile']['tpl_dir']}css/rfq.css","{$c['mobile']['tpl_dir']}js/rfq.js");
?>

<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<? include("{$c['mobile']['theme_path']}inc/header.php"); ?>
<div id="u_header" class="fixed">
    <a class="back" href="javascript:history.back(-1);"></a>
    <a class="menu global_menu" href="javascript:;"></a>
    <a class="cart" href="/cart/"><i class="FontBgColor"></i></a>
    <div class="title"></div>
</div>
<div id="user">
    <?php
    if ($d == 'list') {
        $pid = $_GET['pid'];
        //目录
        $catelog = db::get_all('business_catelog','is_del = 1',"*","sort asc");
//        print_r($catelog);
        if ($pid){
            $pid = $pid;
        }else{
            $pid = $catelog[0]['id'];
        }

        $cate = db::get_all("business_cate",'is_del = 1 and catelogid = '.$pid,'*','sort asc');
        foreach ($cate as $item) {
            $uid = $item['uid'].$item['id'].',';
            $son = db::get_all("business_cate","is_del = 1 and uid = '".$uid."'","*","sort asc");
            $arr[$item['id']] = $son;
        }
        ?>
        <style>
            .clear{
                clear: both;
            }
            .zhzcontent{
                margin-top: 50px;
            }
            .zhzcateleft{
                float: left;
                width: 30%;
                height: 100%;
                background: #f8f8f8;
                overflow-x: hidden;
                overflow-y: scroll;

            }
            .zhzcateright{
                float: left;
                width: 70%;
                height: 100%;
                overflow-x: hidden;
                overflow-y: scroll;
            }
            .zhzcateright h1{
                font-size: 1rem;
                display: block;
                height: 40px;
                line-height: 40px;
                text-align: left;
                margin-top: 20px;
                font-weight: bold;
                margin-left: 10px;
            }
            .zhzcateleft ul li{
                padding: 15px 10px;
                background: #f8f8f8;
                color: #555;
                font-size: 1rem;
            }
            .zhzcateleft ul li a{
                color: #555;
            }
            .zhzprocate ul li{
                box-sizing:border-box;
                padding: 20px 3px;
                float: left;
                width: 30%;
                height: 140px;
                text-align: center;
            }
            .zhzprocate ul li p{
                display: -webkit-box;
                -webkit-box-orient: vertical;
                -webkit-line-clamp: 2;
                overflow: hidden;
                font-size: 0.8rem;
                color:#555;
            }
            .dq{
                background: #fff !important;
            }
            .dq a{
                color: red !important;
            }
        </style>
        <div class="zhzcontent">
            <div class="zhzcateleft">
                <ul>
                    <? foreach ($catelog as $item) { ?>
                        <li class="<?=$item['id']==$pid?"dq":""; ?>"><a href="/products/rfqcate1/<?=$item['id']; ?>/"><?=$item['name'.$c['lang']]; ?></a></li>
                    <? } ?>
                </ul>
            </div>
            <div class="zhzcateright">
                <? foreach ($cate as $item){ ?>
                    <h1><?=$item['name'.$c['lang']]; ?></h1>
                    <div class="zhzprocate">
                        <ul>
                            <? foreach ($arr[$item['id']] as $it) { ?>
                                <li>
                                    <a href="/supplier/?pid=<?=$it['id'];?>">
                                        <img src="<?=$it['img']; ?>" alt="">
                                        <p><?=$it['name'.$c['lang']]; ?></p>
                                    </a>
                                </li>
                            <? }?>
                            <div class="clear"></div>
                        </ul>
                    <? } ?>
                </div>
            </div>
        </div>
        <?php
    } ?>
</div>