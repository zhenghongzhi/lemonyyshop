<?php !isset($c) && exit();?>
<?php
$ProId=(int)$_GET['sp'];

//$products_row=str::str_code(db::get_one('business_product', "id='$ProId'"));
$row=str::str_code(db::get_one('business_product as bp left join business as b on b.BId = bp.pid', "bp.id='$ProId'","bp.*,b.Name"));
//print_r($pro_row);die;
if(!$row){
    @header('HTTP/1.1 404');
    exit;
}
//返回按钮
$page_back_url = 'javascript:history.back();';
//if ($_SERVER['HTTP_REFERER']) {
//    if (stripos($_SERVER['HTTP_REFERER'], ly200::get_domain()) !== false) {
//        $page_back_url = ly200::get_url($category_row);
//    }
//}
//print_r($row);die;
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection" />

    <!--<meta property="og:type" content="article">-->
    <!--<meta property="og:title" content="--><?//=$Name;?><!--">-->
    <!--<meta property="og:description" content="share">-->
    <!--<meta property="og:image" content="http://lopscoop.com/static/html/share/lucklymeta.jpg">-->
    <!--<meta property="og:url" content="http://********:8020/whatsappshop-one/one.html">-->

    <?=ly200::seo_meta($products_seo_row, $spare_ary);?>
    <?=cart::products_pins($products_row, $CurPrice); ?>
    <?php include("{$c['mobile']['theme_path']}inc/resource.php");?>
    <?=ly200::load_static("{$c['mobile']['tpl_dir']}css/goods.css", "{$c['mobile']['tpl_dir']}js/cart.js");?>
    <style>
        .detail_desc table{border-collapse:collapse; width:100%;}
        .detail_desc table td{border:1px solid #ccc;}
    </style>
</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['mobile']['theme_path']}inc/header.php");?>
<?php include("{$c['mobile']['theme_path']}header/{$c['mobile']['HeaderTpl']}/header.php");?>
<style>
    .zhzpro{
        float: none;
        margin: 10px;
        border: 1px solid #ccc;
        padding: 8px;
    }
    .zhzprocontent{
        margin-top: 15px;
    }
    .cbottom{
        position: fixed;
        bottom: 0px;
        width: 100% ;
    }
    .ccombuttom{
        display: inline-block;
        width: 50%;
        height: 50px;
        line-height: 50px;
        text-align: center;
        float: left;
        font-size: 1rem;
    }
    .cbottom a{
        color: #ffffff;
    }
    .cnow{
        /*background: #e49535;*/
        background-image: linear-gradient(#ffd388, #d7780f);
    }
    .srfq{
        background-image: linear-gradient(#13c4bb, #0689c1);
    }
</style>
<div class="wrapper">
    <div class="detail_pic clean">
        <div class="detail_pic_container">
            <div class="goods_pic 111" style="height: 375px;">
                <ul class="clean" style="width: 4875px; display: block; height: 375px;">
                    <?php
			            for($i=0; $i<$c['prod_image']['count']; $i++){
                            $pic=$row['PicPath_'.$i];
                            if(!is_file($c['root_path'].$pic)) continue;
			                ?>
                            <li class="fl"><img src="<?=ly200::get_size_img($pic, '500x500');?>"></li>
                    <?php } ?>
                </ul>
                <div class="trigger clean">
                    <?php
                    for($i=0; $i<$c['prod_image']['count']; $i++){
                        $pic=$row['PicPath_'.$i];
                        if(!is_file($c['root_path'].$pic)) continue;
                        ?>
                        <div class="item<?=$i==0?' FontBgColor':' off';?>"><?=$i;?></div>
                    <?php }?>
                </div>
                <script>
                    console.log(999)
                    if ($('body').hasClass('lang_fa')) {
                        var li_len = $(".goods_pic .trigger .item").length;
                        var boxW= $(window).width();
                        console.log(li_len);
                        console.log(boxW);
                        $(".goods_pic .trigger .item").eq(li_len - 1).addClass('FontBgColor').removeClass('off').siblings('.item').addClass('off').removeClass('FontBgColor');
                        $(".goods_pic ul.clean").css("transform",'translate3d(' + -((li_len - 1) * boxW) + 'px,0,0)') ;
                        // $(".goods_pic ul.clean").style.msTransform = $(".goods_pic ul.clean").style.OTransform = 'translateX(' + -(li_len * boxW) + 'px)';

                    };
                </script>
            </div>
            <!-- 抛物线的div -->
            <div class="big_pic" style="display:none;"><img src="/u_file/2004/products/08/7e8349290d.jpg.240x240.jpg" class="normal" alt=""></div>
        </div>
        <div class="detail_pic_toolbar clean">
            <a rel="nofollow" class="gh_menu fl gh_menu_back" href="<?=$page_back_url; ?>"><i></i></a>
<!--            <a rel="nofollow" class="gh_menu fr gh_menu_share" href="javascript:;"><i></i></a>-->
        </div>
    </div>
    <div class="goods_info clean">
        <div class="prod_info_name"><?=$row['name']; ?></div>
    </div>
    <div class="prod_info_divide"></div>
    <div class="clean prod_info_price zhzpro">
        <div class="box_price price_1 last_price">
            <div class="price cur_price">参考价格:<?=$row['money'];?></div>
            <? if ($row['startqty']){ ?>
                <div class="zhzprocontent">
                    最低起定量：<?=$row['startqty'];?>
                </div>
            <? } ?>
            <? if ($row['deliverytime']){ ?>
                <div class="zhzprocontent">
                    交货时间：<?=$row['deliverytime'];?> Days
                </div>
            <? } ?>
            <? if ($row['Name']){ ?>
                <div class="zhzprocontent">
                    供应商名称：<?=$row['Name'];?>
                </div>
            <? } ?>
            <? if ($row['spec']){ ?>
                <div class="zhzprocontent">
                    规格属性：<?=$row['spec'];?>
                </div>
            <? } ?>
            <? if ($row['material']){ ?>
                <div class="zhzprocontent">
                    材料：<?=$row['material'];?>
                </div>
            <? } ?>
        </div>
    </div>
    <div class="prod_info_divide"></div>
    <div class="prod_detail_box prod_description">
        <div class="detail_title">
            <strong>Description</strong>
        </div>
        <div class="detail_content editor_txt">
            <?=str::str_code($row['content'], 'htmlspecialchars_decode');?>
<!--           --><?//=$row['content']; ?>
        </div>
    </div>
</div>

<div class="cbottom">
    <a class="ccombuttom cnow zhzshare">Contact Now</a>
    <a href="/account/rfp/?spid=<?=$ProId; ?>" class="ccombuttom srfq">Send RFQ</a>
</div>



<?php
//include("{$c['mobile']['theme_path']}footer/{$c['mobile']['FooterTpl']}/footer.php");
include("{$c['mobile']['theme_path']}inc/footer.php");
echo ly200::load_static('/static/js/plugin/lightbox/js/lightbox.min.js');
?>
<script>
    //切换图片
    function goods_pic() {
        //切换图片
        var goods_pic = $('.goods_pic'),
            olist = $('.goods_pic ul'),
            oitem = $('li', olist),
            pic = $('img', oitem),
            oitemLen = oitem.length,
            boxW = $(window).width(),
            small_pic = goods_pic.find('.trigger .item');

        olist.css({'width': boxW * oitemLen, 'display': 'block'});
        oitem.css('width', boxW);

        if (oitemLen > 1) {
            //********** 拨动切换 **************
            var startX = 0,
                endX = 0,
                disX = 0,//偏移量
                basicX = boxW * 0.15,//偏移量小于此值时还原
                i = 0,//当前索引
                startML = 0,
                str = '',
                startPos = {},
                MovePos = {},
                isScrolling = 0;
            if ($('body').hasClass('lang_fa')) {
                i = oitemLen - 1;
            }
            ;

            function move(e) {
                var touch = e.originalEvent.touches;
                goods_pic.css('background-image', 'none');
                if (touch.length > 1 || e.originalEvent.scale && e.originalEvent.scale !== 1) return;
                MovePos = {x: touch[0].pageX - startPos.x, y: touch[0].pageY - startPos.y};
                isScrolling = Math.abs(MovePos.x) < Math.abs(MovePos.y) ? 1 : 0;
                if (isScrolling == 0) { //左右
                    e.preventDefault();
                    disX = touch[0].pageX - startX;
                    startML = -i * boxW;
                    this.style.MozTransform = this.style.webkitTransform = 'translate3d(' + (startML + disX) + 'px,0,0)';
                    this.style.msTransform = this.style.OTransform = 'translateX(' + (startML + disX) + 'px)';
                } else { //上下
                    olist.unbind("touchmove", move);
                    olist.unbind("touchend", end);
                }
            }

            function end(e) {
                var _x = Math.abs(disX);
                if (_x >= basicX) {
                    if (disX > 0) {//右移
                        i--;
                        if (i < 0) {
                            i = 0;
                        }
                    } else {//左移
                        i++;
                        if (i >= oitemLen) {
                            i = oitemLen - 1;
                        }
                    }
                }
                ooo = i;

                small_pic.eq(ooo).addClass('FontBgColor').removeClass('off').siblings('.item').addClass('off').removeClass('FontBgColor');
                this.style.MozTransitionDuration = this.style.webkitTransitionDuration = '0.3s';
                this.style.MozTransform = this.style.webkitTransform = 'translate3d(' + -(ooo * boxW) + 'px,0,0)';
                this.style.msTransform = this.style.OTransform = 'translateX(' + -(ooo * boxW) + 'px)';
                var img_h = $(this).find('li:eq(' + ooo + ') img').outerHeight();
                // $('.goods_pic, .goods_pic ul').css({'height':img_h});
                startX = disX = _x = 0;
            }

            olist.get(0).ontouchstart = function (e) {
                startX = e.touches[0].pageX;
                this.style.MozTransitionDuration = this.style.webkitTransitionDuration = 0;
                startPos = {x: e.touches[0].pageX, y: e.touches[0].pageY, time: +new Date};
                isScrolling = 0;
                olist.bind("touchmove", move);
                olist.bind("touchend", end);
            };
            //*********** 拨动切换 结束 ***********
        }
    }

    $('.zhzshare').on('click', function() {
        // href="https://api.whatsapp.com/send?phone=8619924351059&text=May I help you?"
        var content="";
        content+="<?=$business['Name']; ?>";
        //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
        content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
        //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
        location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
    })

    goods_pic();
</script>
</body>
</html>