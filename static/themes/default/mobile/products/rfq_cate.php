<?php !isset($c) && exit(); ?>
<?php
$d_ary = array('list', 'view', 'contact');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];

$user_id = $_SESSION['User']['UserId'];
?>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection">
    <link rel="canonical" href="http://lemonyyshop.com/account/rfqcate/">
    <link rel="shortcut icon" href="/u_file/1912/photo/5d61fce285.png">
    <meta name="keywords" content="LemonYY China trade starts here">
    <meta name="description" content="LemonYY China trade starts here">
    <title>LemonYY China trade starts here</title>
    <link href="/static/themes/default/mobile/css/global.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" src="/static/themes/default/mobile/js/jquery-min.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/js/global.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/rye-touch.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/global.js?v=4.01320"></script>
    <link href="/static/themes/default/mobile/lang/_en/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/js/lang/en.js?v=4.01320"></script>
    <link href="/static/css/visual.css?v=4.01320" rel="stylesheet" type="text/css">
    <!-- Facebook Pixel Code -->
    <script type="text/javascript">
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '');
        fbq('track', "PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id="></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '');
    </script>


    <link href="/static/themes/default/mobile/header/01/css/header.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/footer/02/css/footer.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/user.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/themes/default/mobile/js/user.js?v=4.01320"></script>
    <script type="text/javascript">$(function(){user_obj.user_index()});</script>
</head>
<?php
echo ly200::load_static("{$c['mobile']['tpl_dir']}css/rfq.css","{$c['mobile']['tpl_dir']}js/rfq.js");
?>
<style>
    a:link, a:visited {
        color: #444444;
        text-decoration: none;
    }

    a:hover {
        color: #444444;
    }

    .float_bottom {
        width: 100%;
        position: fixed;
        bottom: 0;
        z-index: 999;
    }

    #rfqlist {
        margin-bottom: 52px;
    }

    #rfqlist .prod_box .info {
        margin-right: 50px;
    }

    .float_bottom .add_new {
        width: 90%;
        height: 40px;
        line-height: 40px;
        color: white;
        border: 1px solid #dbdbdb;
        border-radius: 3px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        cursor: pointer;
        display: block;
        margin: 0 auto;
        text-align: center;
        margin-bottom: 12px;
        background-color: #e9af24;
        font-size: 14px;
        z-index: 999;
    }

    .float_bottom .add_new:hover {
        background-color: #e9af24;
        z-index: 999;
    }

    .user_order .prod_list .plist .fr {
        float: right;
        text-align: center;
        position: relative;
        margin-top: 1.8rem;
    }

    .user_order .prod_list .plist .img img{
        object-fit: cover;
        display: block;
    }

    .user_order .prod_list .plist .fr span {
        float: right;
        margin-right: 16px;
    }

    .user_order .prod_list .plist .fr em {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #999;
        border-style: solid;
        float: right;
        display: block;
        position: relative;
        z-index: 10;
    }

    .user_order .prod_list .plist .fr em > i {
        border-width: .4rem 0 .4rem .4rem;
        border-color: transparent transparent transparent #fff;
        border-style: solid;
        display: block;
        position: absolute;
        top: -.4rem;
        right: .1rem;
        z-index: 11;
    }

    #rfqlist .prod_box{
        padding-left: 0;
        padding-right: 16px;
    }

    .user_order .prod_list .title{
        width: 100%;
        height: 2.0rem;
        line-height: 2.0rem;
        background-color: #eeeeee;
    }


    div>.line{
        width: 100%;
        margin-left: 8px;
        margin-top: 10px;
        height: 1px;
        background-color: #eeeeee;
    }

    .user_order .prod_list .plist .info{
        width: auto;
    }

    .user_order .prod_list .plist .info .name{
        margin-top: 8px;
    }

    .user_order .prod_list .plist .info .attr{
        margin-top: 8px;
    }

    .rfqsearch{
        margin-top: 50px;
        background: #f5f5f4;
        padding: 5px;
    }
    .rfqsearinput{
        height: 30px;
        width: 75%;
        margin-right:10px;
        border-radius: 10px;
    }
    .rfqsubmit{
        color: #646464;
        margin: 0;
        padding: 0;
        border: 0;
        font: inherit;
        vertical-align: baseline;
        -webkit-tap-highlight-color: transparent;
    }
    .prod_list{
        padding: 5px 10px;
        font-size: 0.8rem;
        border-bottom: 1px solid #ccc;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<? include("{$c['mobile']['theme_path']}inc/header.php"); ?>
<div id="u_header" class="fixed">
    <a class="back" href="javascript:history.back(-1);"></a>
    <a class="menu global_menu" href="javascript:;"></a>
    <a class="cart" href="/cart/"><i class="FontBgColor"></i></a>
    <div class="title"></div>
</div>
<div id="user">
    <?php
    if ($d == 'list') {
        $pid = (int)$_GET['pid'];
        if($pid == 0) {
            $list = db::get_all('business_cate', "uid = '0,'",'*','sort asc');
        }else{
            $one = db::get_one("business_cate","id = ".$pid);
            $list = db::get_all('business_cate',"uid = '".$one['uid'].$one['id'].",'",'*','sort asc');
        }

        ?>
        <div class="rfqsearch">

            <div class="clear"></div>
            <form action="/supplier/" style="position: relative;">
                <input type="text" name="Keyword" class="rfqsearinput">
                <input type="submit" value="" class="rfqsubmit" style="width: 2.5rem;height: 2rem;background: url(/static/themes/default/mobile/images/icon_search_submit.png) no-repeat center/1.5rem;position: absolute;top: 0px;">
                <a href="/account/rfq/" class="rfp" style="position: absolute;top:0px;right:0px;width:2.5rem;height: 2rem;display: block;background: url(/static/themes/default/mobile/footer/02/images/menu_rfp.png) no-repeat center/1.5rem;"></a>
            </form>
        </div>
        <div id="rfqlist" class="user_order">
            <?php
            if ($list) {
                foreach ($list as $k => $v) {
                    if ($pid){
                        $url = '/supplier/?pid='.$v['id'];
                    }else{
                        $url = '/products/rfqcate/?pid='.$v['id'];
                    }
                    $count = db::get_one("business_cate","uid = '".$v['uid'].$v['id'].",'","count(*) as count");
                    ?>
                    <div class="prod_list">
                        <a href="<?=$url; ?>">
                            <div class="prod_box clean">
                                <div class="item clean">
                                    <p <? if(!$pid){?>style="color:#<?=$v['color']; ?>"<? } ?>><?=$v['name'.$c['lang']];?><? if(!$pid){ ?>(<?=$count['count']; ?>) <? } ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="divide_5px"></div>
        </div>

        <script>
            $(function () {
                $('html').unbind();
            });
        </script>
        <?php
    } ?>
</div>