<?php !isset($c) && exit(); ?>
<?php

$d_ary = array('list', 'view', 'contact','receive');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];
$user_id = $_SESSION['User']['UserId'];
?>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection">
    <link rel="canonical" href="http://lemonyyshop.com/account/rfqcate/">
    <link rel="shortcut icon" href="/u_file/1912/photo/5d61fce285.png">
    <meta name="keywords" content="LemonYY China trade starts here">
    <meta name="description" content="LemonYY China trade starts here">
    <title><?=$c['lang_pack']['zhzgetcoupon']; ?></title>
    <link href="/static/themes/default/mobile/css/global.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
<!--    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>-->
    <script type="text/javascript" src="/static/themes/default/mobile/js/jquery-min.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/js/global.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/rye-touch.js?v=4.01320"></script>
    <script type="text/javascript" src="/static/themes/default/mobile/js/global.js?v=4.01320"></script>
    <link href="/static/themes/default/mobile/lang/_en/css/style.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/js/lang/en.js?v=4.01320"></script>
    <link href="/static/css/visual.css?v=4.01320" rel="stylesheet" type="text/css">
    <!-- Facebook Pixel Code -->
    <script type="text/javascript">
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '');
        fbq('track', "PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<!--    <script async="" src="https://www.googletagmanager.com/gtag/js?id="></script>-->
<!--    <script>-->
<!--        window.dataLayer = window.dataLayer || [];-->
<!--        function gtag(){dataLayer.push(arguments);}-->
<!--        gtag('js', new Date());-->
<!--        gtag('config', '');-->
<!--    </script>-->


    <link href="/static/themes/default/mobile/header/01/css/header.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/footer/02/css/footer.css?v=4.01320" rel="stylesheet" type="text/css">
    <link href="/static/themes/default/mobile/css/user.css?v=4.01320" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/static/themes/default/mobile/js/user.js?v=4.01320"></script>
    <script type="text/javascript">$(function(){user_obj.user_index()});</script>
</head>
<?php
echo ly200::load_static("{$c['mobile']['tpl_dir']}css/rfq.css","{$c['mobile']['tpl_dir']}js/rfq.js");
?>

<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<? include("{$c['mobile']['theme_path']}inc/header.php"); ?>
<div id="u_header" class="fixed">
    <a class="back" href="javascript:history.back(-1);"></a>
    <a class="menu global_menu" href="javascript:;"></a>
    <a class="cart" href="/cart/"><i class="FontBgColor"></i></a>
    <div class="title"></div>
</div>
<div id="user">
    <?php
    if ($d == 'list') {
            $exchange = db::get_one('exchange','isuse=1 and currency = "CNY"','*',"sort asc");
            $line_arr = db::get_all('logistics_line','is_show = 1',"*","sort asc");

        ?>
        <style>
            .clear {
                clear: both;
            }

            .zhzcontent {
                margin-top: 50px;
            }

            body {
                background: #f8f8f8;
            }

            h2 {
                text-align: center;
                font-size: 26px;
                font-weight: bold;
                padding-top: 10px;
            }

            .linelist {
                margin: 6% 2% 6% 2%;
                /*border: 1px solid #000;*/
                background: #fff;
                border-radius: 5px;
                padding: 15px 0px;
                box-shadow: 0 4px 7px rgba(0, 0, 0, 0.12), 0 6px 5px rgba(0, 0, 0, 0.24);
            }

            .listleft {
                float: left;
                width: 65%;
                /*background: red;*/
                height: 75px;
                padding-left: 9px;
                border-right: 1px dashed #f0f0f0;
            }

            .listright {
                float: right;
                width: 30%;
                /*background: yellow;*/
                text-align: center;
                height: 75px;
                padding-top: 8px;
            }

            .listtitle {
                font-weight: bold;
                font-size: 18px;
                margin-bottom: 7px;
                color: #000;
            }

            .listdec {
                font-size: 12px;
                color: #555;
            }

            .listdec1 {
                font-size: 14px;
                color: #555;
                margin-bottom: 7px;
            }

            .listjia {
                font-size: 18px;
                color: #0fc20f;
            }
        </style>
        <style type="text/css">

            #scroll_div {
                height:30px;
                overflow: hidden;
                white-space: nowrap;
                width:100%;
                background-color: #fff;
                color: #999999;
                margin: 1rem 0rem;
                text-align: center;
                margin-bottom: 5px;
            }
            #scroll_begin,#scroll_end {
                display: inline;
            }
        </style>
        <link rel='stylesheet' href='/static/themes/default/mobile/css/bootstrap.min.css'>
        <script src='/static/themes/default/mobile/js/bootstrap.min.js'></script>
        <? if($c['lang'] == '_fa'){ ?>
            <link href="/static/themes/default/mobile/css/cardMe_fa.css" rel="stylesheet" type="text/css">

            <div class="zhzcontent">
                <img style="width: 80%;margin: 3% 10%;" src="/static/themes/default/mobile/images/Untitled-1_fa.png" alt="">

                <div id="scroll_div" class="fl">
                    <div id="scroll_begin">
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>e：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </div>
                    <div id="scroll_end"></div>
                </div>
                <div class="section">
                    <div class="tpd-plan">
                        <div class="tp-flight-plan">
                            <div class="container-fluid">
                                <? foreach ($line_arr as $key => $value){ ?>
                                    <div style="margin-bottom: 4rem;" class="crop depart">
                                    <div class="context collapsed" data-toggle="collapse" data-target="#demo<?=$key;?>">
                                        <a role="button" tabindex="0" class="tog-cal itin-det-btn">
                                            <i><img style="width: 1.5rem;" src="/static/themes/default/mobile/images/drop-down_fa.png" alt=""></i>
                                        </a>
                                        <div class="item it-1">
                                            <label class="trip-type depart">حرکت</label>
                                            <div class="airline-image">
                                                <div class="df-text"><?=$value['prescription']; ?> <?=$c['lang_pack'][$line['prescriptiondw']]; ?></div>
                                                <span class="img-wrapper">
												<svg class="anime-airplane">
													<use xlink:href="#airplane"></use>
												</svg>
												<span class="top-label"><?=$value['psfs'] == 1?'direct':'Sea Shipping'; ?></span>
											</span>
                                            </div>

                                            <div class="port-seg">
                                                <div class="flight-seg origin">
                                                    <div class="time"><?=$value['sfdgj'.$c['lang']]?></div>
                                                    <div class="port"><br></div>
                                                    <div class="name"><?=$value['sfdcs'.$c['lang']]?></div>
                                                </div>
                                                <div class="flight-seg destination">
                                                    <div class="time"><?=$value['mddgj'.$c['lang']]?></div>
                                                    <div class="port"><br></div>
                                                    <div class="name"><?=$value['mddcs'.$c['lang']]?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item it-2">
                                            <div class="dr-row">
                                                <span class="al-name"><?=$value['name'.$c['lang']]?></span>
                                                <!-- <img class="airline-logo" src="./3142246.png" /> -->
                                            </div>
                                            <div class="take-tim">
                                                <?=$c['lang_pack']['zhzunitprice']; ?>
                                                <? if($value['tjdanjia'] != 0){ ?>
                                                    <span  style="color: green;font-size: 2vh;font-weight: 700; float: left;">¥<?=$value['tjdanjia']; ?>/CBM</span>
                                                <? } ?>
                                                <? if($value['zldanjia'] != 0){ ?>
                                                    <span  style="color: green;font-size: 2vh;font-weight: 700; float: left;">¥<?=$value['zldanjia']; ?>/kg</span>
                                                <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="demo<?=$key;?>" class="fly-wrap collapse">
                                        <div class="fly-det">
                                            <div class="f-item">
                                                <div class="airway-title">
                                                    <? if($value['psfs'] == 1){ ?>
                                                        <img class="airline-logo" style="width: 7rem;float: right; opacity: 0.5;" src="/static/themes/default/mobile/images/express_fa.png" /> <span class="tx" style="    color: red;"><?=$value['name'.$c['lang']]; ?> <?=$c['lang_pack']['zhzservices']; ?></span>
                                                    <? }else{ ?>
                                                        <img class="airline-logo" style="width: 7rem;float: right; opacity: 0.5;" src="/static/themes/default/mobile/images/economy_fa.png" /> <span class="tx" style="    color: red;"><?=$value['name'.$c['lang']]; ?> <?=$c['lang_pack']['zhzservices']; ?></span>
                                                    <? } ?>
<!--                                                    <img class="airline-logo" style="width: 7rem;float: right; opacity: 0.5;" src="/static/themes/default/mobile/images/express_fa.png" /> <span class="tx" style="    color: red;">سرویس پیشتاز</span>-->
                                                </div>
                                                <div class="root-de">

                                                    <div class="directs">
                                                        <div class="itin-time">
                                                            <div class="itin-lines"></div>
                                                        </div>

                                                        <div class="hour-sm">
                                                            <div class="hour-time-sm"><?=$value['sfdcs'.$c['lang']]?></div>

                                                            <div class="hour-time-sm"><?=$value['mddcs'.$c['lang']]?></div>
                                                        </div>
                                                    </div>

                                                    <div class="itin-target">
                                                        <div class="tar-label"><?=$item['protype'.$c['lang']]; ?></div>
                                                        <? if($value['tjdanjia'] != 0){ ?>
                                                            <div class="tar-label">¥<?=$value['tjdanjia']; ?>/CBM</div>
                                                        <? } ?>
                                                        <? if($value['zldanjia'] != 0){ ?>
                                                            <div class="tar-label">¥<?=$value['zldanjia']; ?>/kg</div>
                                                        <? } ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="arrival-info">

                                            <span style="    float: right;" class="sub-span duration-info">
                                                <strong><?=$c['lang_pack']['zhzduration']; ?> :</strong>
                                                <?=$value['prescription']; ?><?=$c['lang_pack'][$line['prescriptiondw']]; ?>
                                            </span>
                                            <span><button onclick="window.location.href='/products/logline/view<?=$value['id']; ?>/'" style="margin-left: 5%;
											height: 2.5rem;
											font-size: 2.3vh;">انتخاب سرویس پیشتاز</button></span>
                                        </div>
                                    </div>

                                </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" display="none">
                        <symbol id="airplane" viewBox="243.5 245.183 25 21.633">
                            <g>
                                <path d="M251.966,266.816h1.242l6.11-8.784l5.711,0.2c2.995-0.102,3.472-2.027,3.472-2.308
								  c0-0.281-0.63-2.184-3.472-2.157l-5.711,0.2l-6.11-8.785h-1.242l1.67,8.983l-6.535,0.229l-2.281-3.28h-0.561v3.566
								  c-0.437,0.257-0.738,0.724-0.757,1.266c-0.02,0.583,0.288,1.101,0.757,1.376v3.563h0.561l2.281-3.279l6.535,0.229L251.966,266.816z
								  " />
                            </g>
                        </symbol>
                    </svg>
                </div>
            </div>
        <? }else{ ?>
            <link href="/static/themes/default/mobile/css/cardMe.css" rel="stylesheet" type="text/css">

            <div class="zhzcontent">
                <img style="width: 80%;margin: 3% 10%;" src="/static/themes/default/mobile/images/Untitled-1.png" alt="">
                <div id="scroll_div" class="fl">
                    <div id="scroll_begin">
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </div>
                    <div id="scroll_end"></div>
                </div>
                <div class="section">
                    <div class="tpd-plan">
                        <div class="tp-flight-plan">
                            <div class="container-fluid">
                                <? foreach ($line_arr as $key => $value){ ?>
                                    <div style="margin-bottom: 4rem;" class="crop depart">
                                    <div class="context collapsed" data-toggle="collapse" data-target="#demo<?=$key;?>">
                                        <a role="button" tabindex="0" class="tog-cal itin-det-btn">
                                            <i><img style="width: 1.5rem;" src="/static/themes/default/mobile/images/drop-down.png" alt=""></i>
                                        </a>
                                        <div class="item it-1">
                                            <label class="trip-type depart">Departure</label>
                                            <div class="airline-image">
                                                <div class="df-text"><?=$value['prescription']; ?> <?=$c['lang_pack'][$line['prescriptiondw']]; ?></div>
                                                <span class="img-wrapper">
                                                    <svg class="anime-airplane">
                                                        <use xlink:href="#airplane"></use>
                                                    </svg>
                                                    <span class="top-label"><?=$value['psfs'] == 1?'direct':'Sea Shipping'; ?></span>
                                                </span>
                                            </div>

                                            <div class="port-seg">
                                                <div class="flight-seg origin">
                                                    <div class="time"><?=$value['sfdgj'.$c['lang']]?></div>
                                                    <div class="port"><br></div>
                                                    <div class="name"><?=$value['sfdcs'.$c['lang']]?></div>
                                                </div>
                                                <div class="flight-seg destination">
                                                    <div class="time"><?=$value['mddgj'.$c['lang']]?></div>
                                                    <div class="port"><br></div>
                                                    <div class="name"><?=$value['mddcs'.$c['lang']]?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item it-2">
                                            <div class="dr-row">
                                                <span class="al-name"><?=$value['name'.$c['lang']]?></span>
                                            </div>
                                            <div class="take-tim"><?=$c['lang_pack']['zhzunitprice']; ?>
                                                <? if($value['tjdanjia'] != 0){ ?>
                                                    <span style="color: green;font-size: 2vh;font-weight: 700;">¥<?=$value['tjdanjia']; ?>/CBM</span>
                                                <? } ?>
                                                <? if($value['zldanjia'] != 0){ ?>
                                                    <span style="color: green;font-size: 2vh;font-weight: 700;">¥<?=$value['zldanjia']; ?>/kg</span>
                                                <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="demo<?=$key;?>" class="fly-wrap collapse">
                                        <div class="fly-det">
                                            <div class="f-item">
                                                <div class="airway-title">
                                                    <? if($value['psfs'] == 1){ ?>
                                                        <img class="airline-logo" style="width: 7rem;float: right; opacity: 0.5;" src="/static/themes/default/mobile/images/express.png" /> <span class="tx" style="    color: red;"><?=$value['name'.$c['lang']]; ?> <?=$c['lang_pack']['zhzservices']; ?></span>
                                                    <? }else{ ?>
                                                        <img class="airline-logo" style="width: 7rem;float: right; opacity: 0.5;" src="/static/themes/default/mobile/images/economy.png" /> <span class="tx" style="    color: red;"><?=$value['name'.$c['lang']]; ?> <?=$c['lang_pack']['zhzservices']; ?></span>
                                                    <? } ?>
                                                </div>
                                                <div class="root-de">
                                                    <div class="directs">
                                                        <div class="itin-time">
                                                            <div class="itin-lines"></div>
                                                        </div>

                                                        <div class="hour-sm">
                                                            <div class="hour-time-sm"><?=$value['sfdcs'.$c['lang']]?></div>

                                                            <div class="hour-time-sm"><?=$value['mddcs'.$c['lang']]?></div>
                                                        </div>
                                                    </div>

                                                    <div class="itin-target">
                                                        <div class="tar-label"><?=$item['protype'.$c['lang']]; ?></div>
                                                        <? if($value['tjdanjia'] != 0){ ?>
                                                            <div class="tar-label">¥<?=$value['tjdanjia']; ?>/CBM</div>
                                                        <? } ?>
                                                        <? if($value['zldanjia'] != 0){ ?>
                                                            <div class="tar-label">¥<?=$value['zldanjia']; ?>/kg</div>
                                                        <? } ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="arrival-info">
                                            <span class="sub-span duration-info">
                                                <strong><?=$c['lang_pack']['zhzduration']; ?>:</strong>
                                                <?=$value['prescription']; ?><?=$c['lang_pack'][$line['prescriptiondw']]; ?>
                                            </span>

                                            <span><button onclick="window.location.href='/products/logline/view<?=$value['id']; ?>/'" style="margin-left: 5%;
                                                height: 2.5rem;font-size: 2.3vh;">select Express</button></span>
                                        </div>
                                    </div>

                                </div>

                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" display="none">
                        <symbol id="airplane" viewBox="243.5 245.183 25 21.633">
                            <g>
                                <path d="M251.966,266.816h1.242l6.11-8.784l5.711,0.2c2.995-0.102,3.472-2.027,3.472-2.308
                                      c0-0.281-0.63-2.184-3.472-2.157l-5.711,0.2l-6.11-8.785h-1.242l1.67,8.983l-6.535,0.229l-2.281-3.28h-0.561v3.566
                                      c-0.437,0.257-0.738,0.724-0.757,1.266c-0.02,0.583,0.288,1.101,0.757,1.376v3.563h0.561l2.281-3.279l6.535,0.229L251.966,266.816z
                                      " />
                            </g>
                        </symbol>
                    </svg>
                </div>
            </div>
        <? } ?>

        <script>
            //文字横向滚动
            function ScrollImgLeft(){
                var speed=50;//初始化速度 也就是字体的整体滚动速度
                var MyMar = null;//初始化一个变量为空 用来存放获取到的文本内容
                var scroll_begin = document.getElementById("scroll_begin");//获取滚动的开头id
                var scroll_end = document.getElementById("scroll_end");//获取滚动的结束id
                var scroll_div = document.getElementById("scroll_div");//获取整体的开头id
                scroll_end.innerHTML=scroll_begin.innerHTML;//滚动的是html内部的内容,原生知识!
                //定义一个方法
                function Marquee(){
                    if(scroll_end.offsetWidth-scroll_div.scrollLeft<=0)
                        scroll_div.scrollLeft-=scroll_begin.offsetWidth;
                    else
                        scroll_div.scrollLeft++;
                }
                MyMar=setInterval(Marquee,speed);//给上面的方法设置时间  setInterval
                //鼠标点击这条公告栏的时候,清除上面的方法,让公告栏暂停
                scroll_div.onmouseover = function(){
                    clearInterval(MyMar);
                }
                //鼠标点击其他地方的时候,公告栏继续运动
                scroll_div.onmouseout = function(){
                    MyMar = setInterval(Marquee,speed);
                }
            }
            ScrollImgLeft();
        </script>
        <?php
    }elseif($d == 'view'){
            $line = db::get_one("logistics_line","id = ".$_GET['id']);
            $exchange = db::get_one('exchange','isuse=1 and currency = "CNY"','*',"sort asc");
        ?>
    <style type="text/css">

        #scroll_div {
            height:30px;
            overflow: hidden;
            white-space: nowrap;
            width:100%;
            background-color: #fff;
            color: #999999;
            text-align: center;
        }
        #scroll_begin,#scroll_end {
            display: inline;
        }
    </style>
        <? if($c['lang']=='_fa'){ ?>
            <style>
                .gate{
                    font-size: 20px;
                }
                .gate + label{
                    padding: 12px 15px;
                }
                .txt-gate{
                    font-size: 20px;
                }
                body{
                    background: #f8f8f8;
                }
                .clear{
                    clear: both;
                }
                .yunfeicontent{
                    margin-top: 60px;
                    background: #fff;
                    height: 210px;
                    padding: 15px 10px;
                }
                .zhzcontent {
                    margin-top: 50px;
                }
                .yunfeititle{
                    font-size: 16px;
                }
                .yunfei{
                    font-size: 18px;
                    color: #0fc20f;
                }
                .yunfeiname{
                    font-weight: bold;
                    font-size: 18px;
                    color:#000;
                    margin-top: 15px;
                }
                .yunfeidec p,.yunfeidec span{
                    margin: 8px 0px;
                    font-size: 14px;
                }
                .jisuan{
                    margin-top: 10px;
                    background: #fff;
                    padding: 15px 10px;
                    font-size: 18px;
                    font-weight: blod;
                }
                .jslist{
                    margin-top: 20px;
                    padding-bottom: 20px;
                    /* border-bottom: 1px dashed #f0f0f0; */
                }
                .jslistleft{
                    float: left;
                    font-size: 0.8rem;
                    font-weight: 700;
                    width: 42%;
                    margin-top: 8px;
                    color: #6b6869;
                }
                .jslistright{
                    float: left;
                    width: 58%;
                }
                .but{
                    display: inline-block;
                    width: 30px;
                    height: 31px;
                    line-height: 30px;
                    text-align: center;
                    border:1px solid #f0f0f0;
                }
                .buttonjian{
                    margin-right: -5px;
                    color: white;
                    background-color: #fd5e53;
                    border: 0 solid;
                    border-radius: 40% 0 0 40%;
                    box-shadow: -3px 3px 5px rgba(0, 0, 0, 0.3);
                }
                .bottonjia{
                    color: white;
                    background-color: #21bf73;
                    margin-left: -5px;
                    box-shadow: -3px 3px 5px rgba(0, 0, 0, 0.3);
                    border-radius: 0 40% 40% 0;
                    border: 0 solid;
                }
                .shuzhi{
                    width: 40%;
                    border: 0 solid #ecebeb;
                    height: 31px;
                    text-align: center;
                    color: #6b6869;
                    background-color: #f9fcfb;
                    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
                }
                .buttom-txt{
                    margin-top: 5px;
                    font-size: 0.7rem;
                    color: #6b6869;
                    font-weight: 600;
                }
                .displaynone{
                    display: none;
                }
                .shuoming{
                    margin-top: 10px;
                    background: #fff;
                    padding: 15px 10px;
                    margin-bottom: 150px;
                }
                .cbottom{
                    position: fixed;
                    bottom: 0px;
                    width: 100% ;
                }
                .ccombuttom{
                    display: inline-block;
                    width: 50%;
                    height: 50px;
                    line-height: 50px;
                    text-align: center;
                    float: left;
                    font-size: 1rem;
                }
                .cbottom a{
                    color: #ffffff;
                }
                .cnow{
                    background: #ec9b3b;
                    /* background-image: linear-gradient(#ffd388, #d7780f); */
                }
                .srfq{
                    background-color: #00818a;
                    /* background-image: linear-gradient(#13c4bb, #0689c1); */
                }
                .zhzfooter{
                    text-align: center;
                    height: 50px;
                    line-height: 50px;
                    font-size: 1rem;
                    background-image: linear-gradient(#ee0979,#ff6a00);
                }
            </style>
            <link rel='stylesheet' href='/static/themes/default/mobile/css/flexboxgrid.min.css'>
            <link href="/static/themes/default/mobile/css/iconsAndmaterials.css" rel="stylesheet">
            <link rel="stylesheet" href="/static/themes/default/mobile/css/normalize.min.css">
            <link href="/static/themes/default/mobile/css/detail_fa.css" rel="stylesheet" type="text/css">
            <div class="con-main">
            <div class="boardingPass">
                <header class="boardingPass-header">
                    <h1 class="boardingPass-airline"><?=$line['name'.$c['lang']]; ?></h1>
                </header>
                <main class="boardingPass-main">
                    <div class="row boardingPass-main-row">
                        <section class="boardingPass-departur text-depart col-xs">
				            <span class="section-label">
					            <?=$line['sfdcs'.$c['lang']]; ?>
                            </span>
                            <span class="boardingPass-departur-IATA"><?=$line['sfdgj'.$c['lang']]; ?></span>
                        </section>

                        <section class="boardingPass-transport boardingPass-icon col-xs">
                            <i class="boardingPass-transport-icon material-icons">airplanemode_active</i>
                        </section>

                        <section class="boardingPass-arrival text-depart col-xs">
                            <span class="section-label"><?=$line['mddcs'.$c['lang']]; ?></span>
                            <span class="boardingPass-arrival-IATA"><?=$line['mddgj'.$c['lang']]; ?></span>
                        </section>
                    </div>

                    <hr class="hr--invisible" />
                    <div id="scroll_div" class="fl">
                        <div id="scroll_begin">
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                        <div id="scroll_end"></div>
                    </div>
                    <div class="row">
                        <section  class="boardingPass-passenger right-text  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhzduration']; ?></span>
                            <span><?=$line['prescription']?><?=$c['lang_pack'][$line['prescriptiondw']]; ?><br>
                                <?=$line['protype'.$c['lang']]; ?></span>
                        </section>


                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons right-icon">event</i>
                        </section>
                    </div>

                    <hr />

                    <div class="row">
                        <section  class="boardingPass-passenger right-text  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhzdetail']; ?></span>
                            <span id="mingxi"></span>

                        </section>
                        <section  class="boardingPass-icon col-xs">
                            <i  class="material-icons right-icon">account_circle</i>
                        </section>
                    </div>
                    <hr>
                    <div class="row">
                        <section class="boardingPass-passenger right-text  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhztotalcost']; ?></span>
                            <span id="zongyunfei">
					</span>
                        </section>
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons right-icon"><img style="width: 1.8rem;" src="/static/themes/default/mobile/images/2948684-200.png" alt=""></i>
                        </section>
                    </div>
                    <hr>

                    <div class="row">


                        <section class="boardingPass-passenger right-text  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhzguaranteedvalue']; ?></span>
                            <span id="huozhi1">
					            </span>
                        </section>
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons right-icon"><img style="width: 1.8rem;" src="/static/themes/default/mobile/images/3073205-200.png" alt=""></i>
                        </section>
                    </div>




                </main>
                <div class="jisuan">
                    <div class="jslist <?=$line['xsdanjia'] == 0?'displaynone':''; ?>">
                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,1)">-</span>
                            <input type="number" class="shuzhi" value="1" money="<?=$line['xsdanjia']; ?>" id="xiangzi">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span>
                            <p class="buttom-txt">کمترین مقدار 1 جعبه</p>
                        </div>
                        <div class="jslistleft r-side">
                            <?=$c['lang_pack']['zhznumberofbox']; ?> (box)
                        </div>
                        <div class="clear"></div>
                    </div>
                    <hr>
                    <div class="jslist <?=$line['tjdanjia'] == 0?'displaynone':''; ?>">

                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,<?=$line['tjqs']; ?>)">-</span>
                            <input type="number" class="shuzhi" value="<?=$line['tjqs']?$line['tjqs']:0; ?>" money="<?=$line['tjdanjia']; ?>" minsz="<?=$line['tjqs']; ?>" id="tiji">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span><br>
                            <p class="buttom-txt">کمترین مقدار  CBM <?=$line['tjqs']; ?></p>
                        </div>
                        <div class="jslistleft r-side">
                            <?=$c['lang_pack']['zhzvolume']; ?>（CBM）
                        </div>
                        <div class="clear"></div>
                    </div>
                    <hr class="<?=$line['tjdanjia'] == 0?'displaynone':''; ?>">
                    <div class="jslist <?=$line['zldanjia'] == 0?'displaynone':''; ?>">

                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,<?=$line['zlqs']; ?>)">-</span>
                            <input type="number" class="shuzhi" value="<?=$line['zlqs']?$line['zlqs']:0; ?>" money="<?=$line['zldanjia']; ?>" minsz="<?=$line['zlqs']; ?>" id="zhongliang">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span><br>
                            <p class="buttom-txt">کمترین مقدار <?=$line['zlqs']; ?> کیلو</p>
                        </div>
                        <div class="jslistleft r-side">
                            <?=$c['lang_pack']['zhzweight']; ?> (kg)
                        </div>

                    </div>
                    <div class="clear"></div>
                    <hr class="<?=$line['zldanjia'] == 0?'displaynone':''; ?>">
                    <div class="jslist ">

                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1)">-</span>
                            <input type="number" class="shuzhi" value="0" money="<?=$line['hzb']; ?>" id="huozhi">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span>
                            <p class="buttom-txt">کمترین مقدار لحاظ نشده</p>
                        </div>
                        <div class="jslistleft r-side">
                            مقدار تضمین（¥）
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <input type="hidden" value="" id="mingxi1">


                <div class="container-Rules r-side">
                    <h1><?=$c['lang_pack']['zhzillustrate']; ?></h1>
                    <hr>
                    <?=$line['content'.$c['lang']] ?>
                </div>


                <div class="cbottom">
                    <div class="zhzfooter">
                        <a href="/products/logline/receive<?=$line['id']; ?>/"> <p class="but-txt"><?=$c['lang_pack']['zhzgetcoupon']; ?></p> </a>
                    </div>
                    <a class="ccombuttom cnow zhzshare"><p class="but-txt"><?=$c['lang_pack']['zhzcontactnow']; ?></p> </a>
                    <a class="ccombuttom srfq"> <p  class="but-txt"><?=$c['lang_pack']['zhzshare']; ?></p> </a>
                    <div class="clear"></div>
                </div>
            </div>

        <? }else{ ?>
            <style>
                .gate{
                    font-size: 20px;
                }
                .gate + label{
                    padding: 12px 15px;
                }
                .txt-gate{
                    font-size: 20px;
                }
                body{
                    background: #f8f8f8;
                }
                .clear{
                    clear: both;
                }
                .yunfeicontent{
                    margin-top: 60px;
                    background: #fff;
                    height: 210px;
                    padding: 15px 10px;
                }
                .zhzcontent {
                    margin-top: 50px;
                }
                .yunfeititle{
                    font-size: 16px;
                }
                .yunfei{
                    font-size: 18px;
                    color: #0fc20f;
                }
                .yunfeiname{
                    font-weight: bold;
                    font-size: 18px;
                    color:#000;
                    margin-top: 15px;
                }
                .yunfeidec p,.yunfeidec span{
                    margin: 8px 0px;
                    font-size: 14px;
                }
                .jisuan{
                    margin-top: 10px;
                    background: #fff;
                    padding: 15px 10px;
                    font-size: 18px;
                    font-weight: blod;
                }
                .jslist{
                    margin-top: 20px;
                    padding-bottom: 20px;
                    /* border-bottom: 1px dashed #f0f0f0; */
                }
                .jslistleft{
                    float: left;
                    font-size: 0.8rem;
                    font-weight: 700;
                    width: 42%;
                    margin-top: 8px;
                    color: #6b6869;
                }
                .jslistright{
                    float: left;
                    width: 58%;
                }
                .but{
                    display: inline-block;
                    width: 30px;
                    height: 31px;
                    line-height: 30px;
                    text-align: center;
                    border:1px solid #f0f0f0;
                }
                .buttonjian{
                    margin-right: -5px;
                    color: white;
                    background-color: #fd5e53;
                    border: 0 solid;
                    border-radius: 40% 0 0 40%;
                    box-shadow: -3px 3px 5px rgba(0, 0, 0, 0.3);
                }
                .bottonjia{
                    color: white;
                    background-color: #21bf73;
                    margin-left: -5px;
                    box-shadow: -3px 3px 5px rgba(0, 0, 0, 0.3);
                    border-radius: 0 40% 40% 0;
                    border: 0 solid;
                }
                .shuzhi{
                    width: 40%;
                    border: 0 solid #ecebeb;
                    height: 31px;
                    text-align: center;
                    color: #6b6869;
                    background-color: #f9fcfb;
                    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
                }
                .buttom-txt{
                    margin-top: 5px;
                    font-size: 0.7rem;
                    color: #6b6869;
                    font-weight: 600;
                }
                .displaynone{
                    display: none;
                }
                .shuoming{
                    margin-top: 10px;
                    background: #fff;
                    padding: 15px 10px;
                    margin-bottom: 150px;
                }
                .cbottom{
                    position: fixed;
                    bottom: 0px;
                    width: 100% ;
                }
                .ccombuttom{
                    display: inline-block;
                    width: 50%;
                    height: 50px;
                    line-height: 50px;
                    text-align: center;
                    float: left;
                    font-size: 1rem;
                }
                .cbottom a{
                    color: #ffffff;
                }
                .cnow{
                    background: #ec9b3b;
                    /* background-image: linear-gradient(#ffd388, #d7780f); */
                }
                .srfq{
                    background-color: #00818a;
                    /* background-image: linear-gradient(#13c4bb, #0689c1); */
                }
                .zhzfooter{
                    text-align: center;
                    height: 50px;
                    line-height: 50px;
                    font-size: 1rem;
                    background-image: linear-gradient(#ee0979,#ff6a00);
                }
            </style>
            <link rel='stylesheet' href='/static/themes/default/mobile/css/flexboxgrid.min.css'>
            <link href="/static/themes/default/mobile/css/iconsAndmaterials.css" rel="stylesheet">
            <link rel="stylesheet" href="/static/themes/default/mobile/css/normalize.min.css">
            <link href="/static/themes/default/mobile/css/detail.css" rel="stylesheet" type="text/css">
            <div class="con-main">
            <div class="boardingPass">
                <header class="boardingPass-header">
                    <h1 class="boardingPass-airline"><?=$line['name'.$c['lang']]; ?></h1>
                </header>

                <main class="boardingPass-main">
                    <div class="row boardingPass-main-row">
                        <section class="boardingPass-departur text-depart col-xs">
					<span class="section-label">
						<?=$line['sfdcs'.$c['lang']]; ?></span>
                            <span class="boardingPass-departur-IATA"><?=$line['sfdgj'.$c['lang']]; ?></span>
                        </section>

                        <section class="boardingPass-transport boardingPass-icon col-xs">
                            <i class="boardingPass-transport-icon material-icons">airplanemode_active</i>
                        </section>

                        <section class="boardingPass-arrival text-depart col-xs">
                            <span class="section-label"><?=$line['mddcs'.$c['lang']]; ?></span>
                            <span class="boardingPass-arrival-IATA"><?=$line['mddgj'.$c['lang']]; ?></span>
                        </section>
                    </div>

                    <hr class="hr--invisible" />
                    <div id="scroll_div" class="fl">
                        <div id="scroll_begin">
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class="pad_right"><?=$c['lang_pack']['zhzcurrentexchangerate']; ?>：IRR<?=round($exchange['irrmoney']*$exchange['money']);?> : <?=$exchange['currency']?><?=round($exchange['irrmoney']);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                        <div id="scroll_end"></div>
                    </div>
                    <div class="row">
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons">event</i>
                        </section>

                        <section class="boardingPass-date col-xs">
                            <span class="section-label"><?=$c['lang_pack']['zhzduration']; ?></span>
                            <span><?=$line['prescription']; ?> <?=$c['lang_pack'][$line['prescriptiondw']]; ?></span>
                        </section>

                        <section class="boardingPass-departurTime col-xs">
                            <span class="section-label"></span>
                            <span><?=$line['protype'.$c['lang']]; ?></span>
                        </section>


                    </div>

                    <hr />

                    <div class="row">
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons">account_circle</i>
                        </section>

                        <section class="boardingPass-passenger  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhzdetail']; ?></span>
                            <span id="mingxi"></span>
                        </section>

                    </div>
                    <hr>
                    <div class="row">
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons"><img style="width: 1.8rem;" src="/static/themes/default/mobile/images/2948684-200.png" alt=""></i>
                        </section>

                        <section class="boardingPass-passenger  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhztotalcost']; ?></span>
                            ¥
                            <span id="zongyunfei"></span>
                        </section>
                    </div>
                    <hr>
                    <div class="row">
                        <section class="boardingPass-icon col-xs">
                            <i class="material-icons"><img style="width: 1.8rem;" src="/static/themes/default/mobile/images/3073205-200.png" alt=""></i>
                        </section>

                        <section class="boardingPass-passenger  col-xs detail-size">
                            <span class="section-label"><?=$c['lang_pack']['zhzguaranteedvalue']; ?></span>
                            <span id="huozhi1">
						</span>
                        </section>
                    </div>




                </main>

                <div class="jisuan">
                    <div class="jslist <?=$line['xsdanjia'] == 0?'displaynone':''; ?>">
                        <div class="jslistleft">
                            <?=$c['lang_pack']['zhznumberofbox']; ?>（box）
                        </div>
                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,1)">-</span>
                            <input type="number" class="shuzhi" value="1" money="<?=$line['xsdanjia']; ?>" id="xiangzi">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span>
                            <p class="buttom-txt"><?=$c['lang_pack']['zhzminimum']; ?> 1 box</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <hr>
                    <div class="jslist <?=$line['tjdanjia'] == 0?'displaynone':''; ?>">
                        <div class="jslistleft">
                            <?=$c['lang_pack']['zhzvolume']; ?>（CBM）
                        </div>
                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,<?=$line['tjqs']; ?>)">-</span>
                            <input type="number" class="shuzhi" value="<?=$line['tjqs']?$line['tjqs']:0; ?>" money="<?=$line['tjdanjia']; ?>" minsz="<?=$line['tjqs']; ?>" id="tiji">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span><br>
                            <p class="buttom-txt"><?=$c['lang_pack']['zhzminimum']; ?>  <?=$line['tjqs']; ?>CBM</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <hr class="<?=$line['tjdanjia'] == 0?'displaynone':''; ?>">
                    <div class="jslist <?=$line['zldanjia'] == 0?'displaynone':''; ?>">
                        <div class="jslistleft">
                            <?=$c['lang_pack']['zhzweight']; ?>（kg）
                        </div>
                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1,<?=$line['zlqs']; ?>)">-</span>
                            <input type="number" class="shuzhi" value="<?=$line['zlqs']?$line['zlqs']:0; ?>" money="<?=$line['zldanjia']; ?>" minsz="<?=$line['zlqs']; ?>" id="zhongliang">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span><br>
                            <p class="buttom-txt"><?=$c['lang_pack']['zhzminimum']; ?>  <?=$line['zlqs']; ?>kg</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <hr class="<?=$line['zldanjia'] == 0?'displaynone':''; ?>">
                    <div class="jslist ">
                        <div class="jslistleft">
                            <?=$c['lang_pack']['zhzguaranteedvalue']; ?>（¥）
                        </div>
                        <div class="jslistright">
                            <span class="but buttonjian" onclick="jisuan(this,-1)">-</span>
                            <input type="number" class="shuzhi" value="0" money="<?=$line['hzb']; ?>" id="huozhi">
                            <span class="but bottonjia" onclick="jisuan(this,1)">+</span>
                            <p class="buttom-txt">minimum not identified</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <input type="hidden" value="" id="mingxi1">


                <div class="container-Rules">
                    <h1><?=$c['lang_pack']['zhzillustrate']; ?></h1>
                    <hr>
                    <?=$line['content'.$c['lang']] ?>
                </div>


                <div class="cbottom">
                    <div class="zhzfooter">
                        <a href="/products/logline/receive<?=$line['id']; ?>/"> <p class="but-txt"><?=$c['lang_pack']['zhzgetcoupon']; ?></p> </a>
                    </div>
                    <a class="ccombuttom cnow zhzshare"><p class="but-txt"><?=$c['lang_pack']['zhzcontactnow']; ?></p> </a>
                    <a class="ccombuttom srfq"> <p  class="but-txt"><?=$c['lang_pack']['zhzshare']; ?></p> </a>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <? } ?>
        <script>
            //文字横向滚动
            function ScrollImgLeft(){
                var speed=50;//初始化速度 也就是字体的整体滚动速度
                var MyMar = null;//初始化一个变量为空 用来存放获取到的文本内容
                var scroll_begin = document.getElementById("scroll_begin");//获取滚动的开头id
                var scroll_end = document.getElementById("scroll_end");//获取滚动的结束id
                var scroll_div = document.getElementById("scroll_div");//获取整体的开头id
                scroll_end.innerHTML=scroll_begin.innerHTML;//滚动的是html内部的内容,原生知识!
                //定义一个方法
                function Marquee(){
                    if(scroll_end.offsetWidth-scroll_div.scrollLeft<=0)
                        scroll_div.scrollLeft-=scroll_begin.offsetWidth;
                    else
                        scroll_div.scrollLeft++;
                }
                MyMar=setInterval(Marquee,speed);//给上面的方法设置时间  setInterval
                //鼠标点击这条公告栏的时候,清除上面的方法,让公告栏暂停
                scroll_div.onmouseover = function(){
                    clearInterval(MyMar);
                }
                //鼠标点击其他地方的时候,公告栏继续运动
                scroll_div.onmouseout = function(){
                    MyMar = setInterval(Marquee,speed);
                }
            }
            ScrollImgLeft();
        </script>

        <script>
            function jisuan(that,num,min) {
                var shuzhi = $(that).siblings('.shuzhi').val();
                shuzhi = Number(shuzhi) + Number(num);
                if (shuzhi < min){
                    shuzhi = min;
                }
                $(that).siblings('.shuzhi').val(shuzhi);
                alljs();
            }
            function alljs() {
                var xiangzi = $("#xiangzi").attr('money');
                var xiangzival = $("#xiangzi").val();

                var tiji = $('#tiji').attr("money");
                var tijimin = $('#tiji').attr("minsz");
                var tijival = $('#tiji').val();


                var zhongliang = $('#zhongliang').attr("money");
                var zhongliangmin = $('#zhongliang').attr("minsz");
                var zhongliangval = $('#zhongliang').val();

                var huozhi = $('#huozhi').attr("money");
                var huozhival = $('#huozhi').val();
                var money = 0;
                var str ='';
                var str1 ='';
                var str2 ='';
                var arr = new Array();
                if (xiangzi != 0 && xiangzival != 0) {
                    str += xiangzival + ' * '+ xiangzi + " (RMB per box) = " + xiangzival * xiangzi +"<br>";
                    str1 = xiangzival + ' * '+ xiangzi;
                    money += (xiangzi*xiangzival);
                    arr.push(str1);
                }
                if (tiji != 0 && tijival != 0) {
                    if (Number(tijival) < Number(tijimin)){
                        tijival = tijimin;
                    }
                    str += tijival + 'CBM * '+ tiji + " (RMB per CBM) = " + tijival * tiji + "<br>";
                    str1 = tijival + 'CBM * '+ tiji;
                    money += (tiji*tijival);
                    arr.push(str1);
                }
                if (zhongliang != 0 && zhongliangval != 0) {
                    if (Number(zhongliangval) < Number(zhongliangmin)){
                        zhongliangval = zhongliangmin;
                    }
                    str += zhongliangval + 'kg * '+ zhongliang + " (RMB per kg) = " + zhongliangval * zhongliang + "<br>";
                    str1 = zhongliangval + 'kg * '+ zhongliang;
                    money += (zhongliang*zhongliangval);

                    arr.push(str1);
                }
                if (huozhi != 0 && huozhival != 0) {
                    str2 += huozhival + ' * '+ huozhi  + "% = " + huozhival * huozhi / 100 +"<br>";
                    str1 = huozhival + ' * '+ huozhi + "%";
                    money += (huozhi*huozhival/100);
                    arr.push(str1);
                }
                var arr1 = arr.join('+');

                $('#mingxi').html(str);
                $('#huozhi1').html(str2);

                $('#mingxi1').val(arr1);
                $('#zongyunfei').html(money.toFixed(2));
            }

            $('.shuzhi').keyup(function () {
                alljs();
            })
            $('.zhzshare').on('click', function() {
                // href="https://api.whatsapp.com/send?phone=8614739070501&text=May I help you?"
                var content="";
                var mingxi1 = '<?=$c['lang_pack']['zhzgetcoupon']; ?>';
                content+=mingxi1;
                //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
                content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
                //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
                location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
            })
            $('.srfq').on('click', function() {
                // href="https://api.whatsapp.com/send?phone=8614739070501&text=May I help you?"
                var content="";
                var mingxi1 = '<?=$c['lang_pack']['zhzgetcoupon']; ?>';
                content+=mingxi1;
                //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
                content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
                //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
                location="https://api.whatsapp.com/send?text="+ encodeURIComponent(content);
            })
            alljs();
        </script>
   <? }elseif($d == 'receive'){
//            print_r($_SERVER);die;
            $line = db::get_one("logistics_line","id = ".$_GET['id']);
        ?>
        <script type="text/javascript" src="/static/iran/server/sweetalert.min.js"></script>

        <style>
            .clear{
                clear: both;
            }
            .zhzcontent{
                margin-top: 70px;
                background: #fff;
                padding-bottom: 20px;
            }
            body{
                background: #f8f8f8;
            }
            .zhztitle{
                text-align: center;
                font-size: 24px;
                font-weight: bold;
                padding-top: 20px;

            }
            .zhzform{
                margin-top: 20px;
                padding: 0px 20px;
                text-align: center;
                font-size: 18px;
            }
            .zhzform input{
                border:1px solid #f0f0f0;
                width: 150px;
                height: 30px;
                line-height: 30px;
            }
            table{
                width: 100%;
            }
            table tr td{
                height: 50px;
                line-height: 50px;
            }
            #send_button{
                background-color: rgb(252, 161, 62);
                border: none;
                color: #fff;
                height: 45px;
                width: 100%;
                max-width: 160px;
                text-align: center;
            }
        </style>
            <? if($c['lang'] == '_fa'){ ?>
            <style>
                .newRow {
                    max-width: 100%;
                    margin: 0 auto;
                    padding: 60px 30px;
                    /* background: #f7f7f7; */
                    /* background-image: url('./index_files/img/bg1.png'); */
                    position: relative;
                    z-index: 1;
                    text-align: center;
                }
                .newRow:before {
                    position: absolute;
                    content: "";
                    display: block;
                    top: 0;
                    left: -5000px;
                    height: 100%;
                    width: 15000px;
                    z-index: -1;
                    background: inherit;
                }
                .newRow:first-child {
                    padding: 40px 30px;
                }
                .newRow:nth-child(2), .newRow:nth-child(8), .newRow:nth-child(10) {
                    background-image: url('./index_files/img/bg1.png');
                    background-repeat: no-repeat;
                    background-size: auto;
                    background-position: top;

                }
                .newRow:nth-child(3), .newRow:nth-child(7) {
                    background: #377D6A;
                }
                .newRow:nth-child(4), .newRow:nth-child(6) {
                    background: #7AB893;
                }
                .newRow:nth-child(5) {
                    background: #B2E3AF;
                }
                .newRow span {
                    position: relative;
                    display: inline-block;
                    margin: 16px 10px;
                }
                .txt-gate{
                    color: white;
                    font-size: 20px;
                    font-family: 'system-ui';
                }
                .gate {
                    display: inline-block;
                    width: 215px;
                    font-size: 20px;
                    padding: 10px 0 10px 15px;
                    font-family: "Open Sans", sans;
                    font-weight: 400;
                    color: #979595;
                    background: #efefef;
                    border: 0;
                    border-radius: 3px;
                    outline: 0;
                    text-indent: 65px;
                    transition: all .3s ease-in-out;
                }
                .gate::-webkit-input-placeholder {
                    color: #efefef;
                    text-indent: 0;
                    font-weight: 300;
                }
                .gate + label {
                    display: inline-block;
                    position: absolute;
                    top: 0;
                    right: 0;
                    padding: 12px 15px;
                    text-shadow: 0 1px 0 rgba(19, 74, 70, 0.4);
                    background: #7AB893;
                    transition: all .4s ease-in-out;
                    border-top-left-radius: 3px;
                    border-bottom-left-radius: 3px;
                    transform-origin: right top;
                    z-index: 99;
                }
                .gate + label:before, .gate + label:after {
                    content: "";
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    border-radius: 3px;
                    background: #fa1616;
                    transform-origin: left bottom;
                    transition: all .4s ease-in-out;
                    pointer-events: none;
                    z-index: -1;
                }
                .gate + label:before {
                    background: rgba(3, 36, 41, 0.2);
                    z-index: -2;
                    right: 20%;
                }

                span:nth-child(2) .gate {
                    text-indent: 85px;
                }

                span:nth-child(2) .gate:focus,
                span:nth-child(2) .gate:active {
                    text-indent: 0;
                }

                .gate:focus,
                .gate:active {
                    color: #fa1616;
                    text-indent: 0;
                    background: #fff;
                    border-top-right-radius: 3px;
                    border-bottom-right-radius: 3px;
                }
                .gate:focus::-webkit-input-placeholder,
                .gate:active::-webkit-input-placeholder {
                    color: #aaa;
                }
                .gate:focus + label,
                .gate:active + label {
                    transform: rotate(-66deg);
                    border-radius: 3px;
                }
                .gate:focus + label:before,
                .gate:active + label:before {
                    transform: rotate(10deg);
                }

                .cpn-btn{
                    display: flex;
                    margin: 14px auto 0 auto;
                    height: 2.2rem;
                    width: 60%;     background-color: #21bf73;     border: 2px solid #339637;
                }
                .cpn-txt{
                    margin: auto;
                    font-size: 2.3vh;
                    color: white;
                    font-family: 'system-ui';
                }
            </style>
            <div class="zhzcontent">
                <div class="zhztitle">
                    <img style="width: 80%;" src="/static/themes/default/mobile/images/3_fa.png" alt=""></div>

                <!-- new form -->

                <div class="newRow">

                    <span>
                      <input class="gate" id="name" type="text" placeholder="لطفا نام خود را وارد کنید" /><label class="txt-gate"  for="name"><?=$c['lang_pack']['zhzname']; ?></label>
                    </span>
                    <span>
                      <input class="gate" id="phone" type="text" placeholder="لطفا شماره همراه خود را وارد کنید" /><label class="txt-gate" for="phone"><?=$c['lang_pack']['user']['phone']; ?></label>
                    </span>
                    <input type="hidden" value="<?=$line['sfdgj']; ?>-<?=$line['mddgj']; ?><?=$line['name']; ?>" name="product" id="product">

                    <button class="cpn-btn" id="send_button"><a class="cpn-txt"><?=$c['lang_pack']['zhzgetcouponnow']; ?></a>

                   </button>
                  </div>



                <!-- end new form -->


                <div style="margin-top: 20px;font-size: 16px; text-align: center;">
                    <p><img src="/static/themes/default/mobile/images/whatsapp.jpg" alt="" width="50" style="vertical-align: middle">8614739070501</p>
                </div>
            </div>
        <? }else{ ?>
            <style>
                .newRow {
                    max-width: 100%;
                    margin: 0 auto;
                    padding: 60px 30px;
                    /* background: #f7f7f7; */
                    /* background-image: url('./index_files/img/bg1.png'); */
                    position: relative;
                    z-index: 1;
                    text-align: center;
                }
                .newRow:before {
                    position: absolute;
                    content: "";
                    display: block;
                    top: 0;
                    left: -5000px;
                    height: 100%;
                    width: 15000px;
                    z-index: -1;
                    background: inherit;
                }
                .newRow:first-child {
                    padding: 40px 30px;
                }
                .newRow:nth-child(2), .newRow:nth-child(8), .newRow:nth-child(10) {
                    background-image: url('/static/themes/default/mobile/images/bg1.png');
                    background-repeat: no-repeat;
                    background-size: auto;
                    background-position: top;

                }
                .newRow:nth-child(3), .newRow:nth-child(7) {
                    background: #377D6A;
                }
                .newRow:nth-child(4), .newRow:nth-child(6) {
                    background: #7AB893;
                }
                .newRow:nth-child(5) {
                    background: #B2E3AF;
                }
                .newRow span {
                    position: relative;
                    display: inline-block;
                    margin: 16px 10px;
                }
                .txt-gate{
                    color: white;
                    font-size: 20px;
                }
                .gate {
                    display: inline-block;
                    width: 215px;
                    font-size: 20px;
                    font-size: 20px;
                    padding: 10px 0 10px 15px;
                    font-family: "Open Sans", sans;
                    font-weight: 400;
                    color: #979595;
                    background: #efefef;
                    border: 0;
                    border-radius: 3px;
                    outline: 0;
                    text-indent: 65px;
                    transition: all .3s ease-in-out;
                }
                .gate::-webkit-input-placeholder {
                    color: #efefef;
                    text-indent: 0;
                    font-weight: 300;
                }
                .gate + label {
                    display: inline-block;
                    position: absolute;
                    top: 0;
                    left: 0;
                    padding: 12px 15px;
                    text-shadow: 0 1px 0 rgba(19, 74, 70, 0.4);
                    background: #7AB893;
                    transition: all .4s ease-in-out;
                    border-top-left-radius: 3px;
                    border-bottom-left-radius: 3px;
                    transform-origin: left bottom;
                    z-index: 99;
                }
                .gate + label:before, .gate + label:after {
                    content: "";
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    border-radius: 3px;
                    background: #fa1616;
                    transform-origin: left bottom;
                    transition: all .4s ease-in-out;
                    pointer-events: none;
                    z-index: -1;
                }
                .gate + label:before {
                    background: rgba(3, 36, 41, 0.2);
                    z-index: -2;
                    right: 20%;
                }

                span:nth-child(2) .gate {
                    text-indent: 85px;
                }

                span:nth-child(2) .gate:focus,
                span:nth-child(2) .gate:active {
                    text-indent: 0;
                }

                .gate:focus,
                .gate:active {
                    color: #fa1616;
                    text-indent: 0;
                    background: #fff;
                    border-top-right-radius: 3px;
                    border-bottom-right-radius: 3px;
                }
                .gate:focus::-webkit-input-placeholder,
                .gate:active::-webkit-input-placeholder {
                    color: #aaa;
                }
                .gate:focus + label,
                .gate:active + label {
                    transform: rotate(-66deg);
                    border-radius: 3px;
                }
                .gate:focus + label:before,
                .gate:active + label:before {
                    transform: rotate(10deg);
                }

                .cpn-btn{
                    display: flex;
                    margin: 14px auto 0 auto;
                    height: 2.2rem;
                    width: 60%;
                    background-color: #21bf73;
                    border: 2px solid #339637;
                }
                .cpn-txt{
                    margin: auto;
                    font-size: 2.3vh;
                    color: white;
                }
            </style>
            <div class="zhzcontent">
                <div class="zhztitle">
                    <img style="width: 80%;" src="/static/themes/default/mobile/images/3.png" alt="">
                </div>
                <div class="newRow">
                    <span>
                        <input class="gate" id="name" type="text" placeholder="write your Name please" /><label class="txt-gate" for="name"><?=$c['lang_pack']['zhzname']; ?></label>
                    </span>
                    <span>
                        <input class="gate" id="phone" type="text" placeholder="write your phone number please" /><label class="txt-gate" for="phone"><?=$c['lang_pack']['user']['phone']; ?></label>
                    </span>
                    <input type="hidden" value="<?=$line['name']; ?>" name="product" id="product">

                    <button class="cpn-btn" id="send_button"><a class="cpn-txt"><?=$c['lang_pack']['zhzgetcouponnow']; ?></a></button>
                </div>

                <div style="margin-top: 20px;font-size: 16px; text-align: center;">
                    <p><img src="/static/themes/default/mobile/images/whatsapp.jpg" alt="" width="50" style="vertical-align: middle">8614739070501</p>
                </div>
            </div>
       <? } ?>

        <script>
            var inputErrors = [];
            $("#send_button").click(function () {
                var formInvalid = false;
                var name = $('#name').val()
                var phone = $('#phone').val()
                var product = $('#product').val()
                if (name === '') {
                    formInvalid = true;
                    inputErrors.push('Name');
                }
                if (phone === '') {
                    formInvalid = true;
                    inputErrors.push('phone Number');
                }
                if (formInvalid) {
                    invalidData();
                }else{
                    $.ajax({
                        url: '/?do_action=action.usermessage',
                        type: 'POST',
                        dataType: 'json',
                        data: {name:name,phone:phone,source:'receive',product:product},
                        success: function(data){
                            if (data.ret == 1) {
                                validData();
                                setTimeout(function () {
                                    tiaozhuan();
                                },2000)
                            }
                        },
                        error: function(){
                            invalidData();
                        }
                    })
                }

            });
            function  tiaozhuan() {
                window.location.href="http://<?=$_SERVER['HTTP_HOST']; ?>//products/logline/view<?=$_GET['id']; ?>/";
            }
            function validData() {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'submitted successful! ',
                    text: 'we will contact you soon!!',
                    showConfirmButton: false,
                    timer:'10000'
                })
            }
            function invalidData() {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Oops...',
                    text: 'maybe your ' + inputErrors + ' is wrong!   ',
                    showConfirmButton: false,
                    timer:'10000' /* mSec*/
                })
            }
        </script>
    <? } ?>
</div>