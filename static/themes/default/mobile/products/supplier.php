<?php !isset($c) && exit();?>
<?php
$no_sort_url='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, Sort'));

$Column='';
//查询
$query_string=ly200::get_query_string(ly200::query_string('m, a, CateId, page'));
$page_count=10;
$page=(int)$_GET['page'];
$CateId=(int)$_GET['CateId'];
$Keyword=$_GET['Keyword'];
$Narrow=str::str_code($_GET['Narrow'], 'urlencode');
$Ext=(int)$_GET['Ext'];
$Sort=($_GET['Sort'] && $c['products_sort'][$_GET['Sort']])?$_GET['Sort']:'1a';

//产品筛选
$where=1;
if($CateId){
    $UId=category::get_UId_by_CateId($CateId);
    $where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
    $category_row=db::get_one('products_category', "CateId='$CateId'");
    if(!$category_row){//分类不存在
        @header('HTTP/1.1 404');
        exit;
    }

    $Column=ly200::get_web_position($category_row, 'products_category', '', '<em><i></i></em>', 12);
    //SEO
    if($category_row['UId']!='0,'){//非大类
        if($category_row['SubCateCount']){
            $subcate_row=db::get_limit('products_category', "UId like '{$category_row['UId']}{$CateId},%'", 'Category'.$c['lang'], $c['my_order'].'CateId asc', 0, 20);
            foreach($subcate_row as $v) $subcateStr.=','.$v['Category'.$c['lang']];
        }
        $spare_ary=array(
            'SeoTitle'		=>	$category_row['Category'.$c['lang']],
            'SeoKeyword'	=>	$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']].$subcateStr,
            'SeoDescription'=>	$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']].$subcateStr
        );
    }else{//大类
        $subcateStr='';
        $subcate_row=db::get_limit('products_category', "UId like '0,{$CateId},%'", 'Category'.$c['lang'], $c['my_order'].'CateId asc', 0, 20);
        foreach($subcate_row as $v) $subcateStr.=','.$v['Category'.$c['lang']];
        $spare_ary=array(
            'SeoTitle'		=>	$category_row['Category'.$c['lang']],
            'SeoKeyword'	=>	$category_row['Category'.$c['lang']].$subcateStr,
            'SeoDescription'=>	$category_row['Category'.$c['lang']].$subcateStr
        );
    }
}else{
    $Column='<em><i></i></em><a href="javascript:;">'.$c['lang_pack']['mobile']['pro_list'].'</a>';
}
if($Keyword){
    $Column='<em><i></i></em><a href="javascript:;">'.str_replace('xxx', $Keyword, $c['lang_pack']['mobile']['key_count']).'</a>';
}
if($Ext){
    $Ext=($Ext<1 || $Ext>4)?1:$Ext;
    $where.=$c['where']['products_ext'][$Ext];
    switch($Ext){
        case 1: $Column='<em><i></i></em><a href="javascript">'.$c['nav_cfg'][8]['name'.$c['lang']].'</a>'; break;
        case 2: $Column='<em><i></i></em><a href="javascript">'.$c['nav_cfg'][9]['name'.$c['lang']].'</a>'; break;
        case 3: $Column='<em><i></i></em><a href="javascript">'.$c['nav_cfg'][10]['name'.$c['lang']].'</a>'; break;
        case 4: $Column='<em><i></i></em><a href="javascript">'.$c['nav_cfg'][11]['name'.$c['lang']].'</a>'; break;
    }
}

$screenAry=where::products('', $Narrow);
$where.=$screenAry[0];//条件
$Narrow_ary=$screenAry[3];//筛选属性
$OrderBy=$screenAry[4];//条件排序
$products_list_row=str::str_code(db::get_limit_page('products', $where.$c['where']['products'], '*', $OrderBy.$c['products_sort'][$Sort].$c['my_order'].'ProId desc', $page, $page_count));

//记录搜索痕迹
ly200::search_logs($products_list_row[1]);

(!$page || $page>$products_list_row[3]) && $page=1;
$Column=sprintf($Column, ($page_count*($page-1)+1), $page_count*($page-1)+count($products_list_row[0]), $products_list_row[1]);

//分类属性
//$AttrId=ly200::product_attribute_attr_id($CateId);
if($category_row['UId']!='0,'){
    $TopCateId=category::get_top_CateId_by_UId($category_row['UId']);
    $SecCateId=category::get_FCateId_by_UId($category_row['UId']);
    $TopCategory_row=str::str_code(db::get_one('products_category', "CateId='$TopCateId'"));
}
$UId_ary=@explode(',', $category_row['UId']);

//该分类的普通属性
if($CateId>0){
    $attr_ary=$all_value_ary=$vid_name_ary=$attrid=$value_ary=array();
    $whereValue=" and (CateId like '%|{$CateId}|%'";
    if(in_array('screening', $c['plugins']['Used'])){
        $all_attribute_row=db::get_all('products_category', "UId like '{$category_row['UId']}{$CateId}%'", 'CateId');
        foreach($all_attribute_row as $v){
            $whereValue.=" or CateId like '%|{$v['CateId']}|%'";
        }
    }
    $whereValue.=')';
    $where='1'.$whereValue;
    $where.=where::equal(array('CartAttr'=>'0', 'DescriptionAttr'=>'0', 'ScreenAttr'=>'1'));
    $products_attr=str::str_code(db::get_all('products_attribute', $where, "AttrId, Type, Name{$c['lang']}", $c['my_order'].'AttrId asc'));
    foreach((array)$products_attr as $v){
        $attr_ary[$v['AttrId']]=$v;
        if($v['Type']==1){//选项
            $attrid['Option'][]=$v['AttrId'];
        }else{//文本框
            $attrid['Text'][]=$v['AttrId'];
        }
    }
    if($attrid['Option']){
        //普通属性（选项）
        $attrid_list=@implode(',', $attrid['Option']);
        !$attrid_list && $attrid_list=-1;
        $value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
        foreach($value_row as $v){
            $all_value_ary[$v['AttrId']][$v['VId']]=$v;
            $vid_name_ary[$v['VId']]=$v['Value'.$c['lang']];
        }
    }
    if($attrid['Text']){
        //普通属性（文本框）
        $attrid_list=@implode(',', $attrid['Text']);
        !$attrid_list && $attrid_list=-1;
        $value_row=str::str_code(db::get_all('products_selected_attribute', "IsUsed=1 and Value{$c['lang']}!='' and AttrId in ($attrid_list)", "SeleteId, AttrId, VId, Value{$c['lang']}", 'SeleteId asc'));//属性选项，SeleteId以最小值为准
        foreach($value_row as $v){
            if(!$value_ary[$v['AttrId']][$v['Value'.$c['lang']]]){//没记录
                $value_ary[$v['AttrId']][$v['Value'.$c['lang']]]=$v['SeleteId'];
                $all_value_ary[$v['AttrId']][$v['SeleteId']]=$v;
                $vid_name_ary['s'.$v['SeleteId']]=$v['Value'.$c['lang']];
            }
        }
    }
    unset($value_ary);
}

if($_GET['Keyword']){
    $businesswhere = "name like '%".$_GET['Keyword']."%' or enname like '%".$_GET['Keyword']."%' or companyDesc like '%".$_GET['Keyword']."%' or encompanyDesc like '%".$_GET['Keyword']."%' or facompanyDesc like '%".$_GET['Keyword']."%'";
    $businessprowhere = "name like '%".$_GET['Keyword']."%' or name_en like '%".$_GET['Keyword']."%' or seo like '%".$_GET['Keyword']."%' or seo_en like '%".$_GET['Keyword']."%' or seo_fa like '%".$_GET['Keyword']."%'";
    $proall = db::get_one("business_product",$businessprowhere,"group_concat(pid) as pid");
    $cateall = db::get_one("business_cate","uid like '".$cate['uid'].$cate['id'].",%'","group_concat(id) as id");
//    //查找3级目录
//
//    $sanjiid = db::get_one("business_cate","name like '%".$_GET['Keyword']."%' or name_en like '%".$_GET['Keyword']."%' or name_fa like '%".$_GET['Keyword']."%'",'group_concat(id) as id');
//    $sanjipro = db::get_one("business_product","cid in (".$sanjiid['id'].")","group_concat(pid) as pid");

    if ($proall['pid']){
        $proall['pid'] = rtrim($proall['pid'], ',');
        $wherea = 1;
        if ($proall['pid']){
            $wherea .= ' and BId in ('.$proall['pid'].')';
        }
//        if($sanjipro['pid']){
//            $wherea .= ' and BId in ('.$sanjipro['pid'].')';
//        }
//        print_r($proall);die;
        //供应商信息
        $business = db::get_all('business',$wherea,'*',$c['my_order'].'BId desc');
    }

    $url = "/search/?Keyword=".$_GET['Keyword'];
}elseif($_GET['pid']){
    $cate = db::get_one("business_cate","id = ".$_GET['pid']);
    $cateall = db::get_one("business_cate","uid like '".$cate['uid'].$cate['id'].",%'","group_concat(id) as id");
    if ($cateall['id']){
        $wher = $cateall['id'].",".$cate['id'];
    }else{
        $wher = $cate['id'];
    }
    $businessid = db::get_one("business_product","cid in (".$wher.")","group_concat(pid) as pid");

    if ($businessid['pid']){
        $business = db::get_all('business',"BId in (".$businessid['pid'].")",'*',$c['my_order'].'BId desc');
    }
    $url = "/search/?pid=".$_GET['pid'];
}



?>
<!DOCTYPE HTML>
<html lang="us">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection" />
    <link rel="canonical" href="<?=ly200::get_domain();?>/products/"/>
    <link rel='stylesheet' href='/static/themes/default/mobile/css/contactinfo.css'>

    <?php
    echo ly200::seo_meta($category_row, $spare_ary);
    include("{$c['mobile']['theme_path']}inc/resource.php");
    //echo ly200::load_static("{$c['mobile']['tpl_dir']}products/{$c['mobile']['ListTpl']}/css/style.css");
    ?>
    <?php
    if(in_array('facebook_pixel', $c['plugins']['Used']) && $_GET['Keyword']){
        //Facebook Pixel
        ?>
        <!-- Facebook Pixel Code -->
        <script type="text/javascript">
            <!-- When a search is made, such as a product query. -->
            fbq('track', 'Search', {
                search_string: '<?=str::str_code(stripslashes($_GET['Keyword']));?>',//搜索关键词
                content_category: 'Product Search',//分类
                content_ids: ['0'],
                value: '0.00',//数值
                currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
            });
        </script>
        <!-- End Facebook Pixel Code -->
    <?php }?>
</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['mobile']['theme_path']}inc/header.php");?>
<?php include("{$c['mobile']['theme_path']}header/{$c['mobile']['HeaderTpl']}/header.php");?>
<div class="wrapper">
<!--    --><?//=html::website_h1('h1', ($category_row['Category'.$c['lang']] ? $category_row['Category'.$c['lang']] : $c['lang_pack']['related_category']), 'class="hideden_h1"'); ?>
    <style>
        .pro_item{
            padding: 0 2.5vw;
        }
        .pro_list_item {
            width: 94vw;
            overflow: hidden;
            margin: 0 3vw;
            padding: 1rem 0;
        }
        .pro_list_item .price {
            height: 4.5rem;
        }
        .pro_list_item .name{
            height: 2rem;
            line-height: 1rem;
            overflow: hidden;
            margin-top: 0.5rem;
            padding: 0;
        }
        .pro_list_item .name a{
            font-size: 1rem;
            color: #333333;
        }
        .pro_item .price .cur_price{
            font-size: 0.8rem;
            color: #333333;
        }
    </style>
    <style>
        .zhzprod_sort li{
            margin-top: 0.1%;
            float: left;
            text-align: center;
            font-size: 1rem;
            height: 2.8rem;
            line-height: 2.8rem;
        }
        .active{
            align-items: center;
            background-color: #f8f8f8!important;
            color: #f0134d!important;
            border-bottom: 3px solid #f0134d;
            display: block;
            width: 50%;
            font-size: 1.1rem!important;
        }
        .not-active{
            background-color: #f8f8f8!important;
            color: rgb(170, 167, 167);
            border-bottom: 3px solid rgb(170, 167, 167);
            display: block;
            width: 50%;
            align-items: center;
        }
        .zhzprod_sort li a{
            color: rgb(170, 167, 167);

        }
        .clear{
            clear: both;
        }
    </style>
    <ul class="zhzprod_sort">
        <li class="active">
            <?=$c['lang_pack']['zhzgongyingshang']; ?>
        </li>
        <li class="not-active">
            <a href="<?=$url; ?>"><?=$c['lang_pack']['zhzproduct']; ?></a>
        </li>
        <div class="clear"></div>
    </ul>
    <div id="filter" class="clean ui_border_b">
<!--        <ul class="prod_sort">-->
<!--            <li class="dropdown fl"><a href="javascript:;" class="dropdown_toggle">Sort By<i class="icon_sort"></i><em></em></a></li>-->
<!--            <li class="dropdown fr">-->
<!--                <a href="javascript:;" class="dropdown_list" data-type="list"><i class="icon_view_list"></i><em></em></a>-->
<!--                <a href="javascript:;" class="dropdown_gallery" data-type="gallery" style="display: none;"><i class="icon_view_gallery"></i><em></em></a>-->
<!--            </li>-->
<!--        </ul>-->
    </div>
    <style>
        .zhzlogo{
            width: 100%;
            height: 100%;
            background: #fff;
            display: table;

        }
        .price{
            display: grid;
        }
        .zhzlogo1{
            width: auto;
            height: auto;
            display:table-cell;
            vertical-align:middle;
            box-shadow: 1px 3px 5px rgba(0,0,0,0.19), 1px 4px 3px rgba(0,0,0,0.13);
            border-radius: 10%;
        }
        .pro_list_item .name a{
            height: 1rem;
            color: #ffd933!important;
            text-shadow: 0px 0px 1.5px #000, 2px 1px 2px #000;
        }
        .more-new{
            background-color: #e61c5d;
            padding: 4% 10%;
            border-radius: 100px;
            cursor: pointer;
            color: white;
        }
    </style>
    <div id="pro_box" class="clean">
        <div class="over">
            <div class="prolist_list" data-type="list" style="display: block;">
                <?php foreach ($business as $key=>$value){ ?>
                <div class="pro_item item pro_list_item ui_border_b">
                    <div class="img pic_box fl">
                        <a href="/supplierd/?sp=<?=$value['BId']; ?>" title="<?=$value['Name']?>">
                            <? if($value['logoPath']){ ?>
                                <a href="/supplierd/?sp=<?=$value['BId']; ?>" title="<?=$value['Name']?>"><img src="<?=$value['logoPath']; ?>"></a>
                            <? }else{ ?>
                                <div class="zhzlogo">
                                    <div class="zhzlogo1" style="background: #f5f5f5;" >
                                        <a href="/supplierd/?sp=<?=$value['BId']; ?>" style="font: 500 4rem 'Comic Sans MS';
                                        color: white;
                                       text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de;
                                        "><?=mb_substr($value['Name'], 0, 1); ?></a>
                                    </div>
                                </div>
                            <? } ?>
                            <? if($value['laabel']){ ?>
                            <span style="position: absolute;top: 0px; right: 0px;color: #333;"><?=$value['laabel']; ?></span>
                            <? } ?>
                        </a>
                    </div>
                    <div class="desc">
                        <div class="name">
                            <a class="name-new" href="/supplierd/?sp=<?=$value['BId']; ?>" title="<?=$value['Name']?>"><?=$value['Name']?></a>
                        </div>
                        <div class="price clean">
                            <div class="cur_price fl" style="margin-bottom: 8px;">地址:<span class="price_data"><?=$value['Address']; ?></span></div>
                            <div class="cur_price fl" style="color: #0a0a0a;height: 10px;">主要产品:<span class="price_data" style="text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box; -webkit-line-clamp: 2; line-clamp: 2;-webkit-box-orient: vertical;"><?=$value['companyDesc']; ?></span></div>
                            <br>
                            <div class="clear"></div>
                        </div>
                        <div class="view clean">
                            <p style="text-align: right;font-size: 0.8rem;"><a class="more-new" href="/supplierd/?sp=<?=$value['BId']; ?>">more</a></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php include("{$c['mobile']['theme_path']}footer/{$c['mobile']['FooterTpl']}/footer.php");?>
<?php include("{$c['mobile']['theme_path']}inc/footer.php");?>
<?=ly200::load_static("{$c['mobile']['tpl_dir']}js/products.js"); ?>
</body>
</html>