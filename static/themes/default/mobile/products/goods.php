<?php !isset($c) && exit();?>
<?php
$ProId=(int)$_GET['ProId'];
$ColorId=(int)$_GET['ColorId'];
$products_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
if(!$products_row){
    @header('HTTP/1.1 404');
    exit;
}

$Name=$products_row['Name'.$c['lang']];
$Price_0=(float)$products_row['Price_0'];
$Price_1=(float)$products_row['Price_1'];
$MOQ=(int)$products_row['MOQ'];
$Max=(int)$products_row['Stock']; //最大购买上限
$products_row['MaxOQ']>0 && $products_row['MaxOQ']<$Max && $Max=$products_row['MaxOQ'];//最大购买量
$CateId=(int)$products_row['CateId'];
$CateId && $category_row=str::str_code(db::get_one('products_category', "CateId='$CateId'"));
$products_description_row=str::str_code(db::get_one('products_description', "ProId='$ProId'"));
$review_cfg=str::json_data(db::get_value('config', "GroupId='products_show' and Variable='review'", 'Value'), 'decode');//评论显示设置
if ($products_row['BriefDescription' . $c['lang']]) {
    $BriefDescription = $products_row['BriefDescription' . $c['lang']];
}else{
    $BriefDescription = $Name;
}
//echo '<pre>';
//print_r($_SERVER);die;
//产品分类
if($category_row['UId']!='0,'){
    $TopCateId=category::get_top_CateId_by_UId($category_row['UId']);
    $TopCategory_row=str::str_code(db::get_one('products_category', "CateId='$TopCateId'"));
}
$UId_ary=@explode(',', $category_row['UId']);

//返回按钮
$page_back_url = 'javascript:history.back();';
if ($_SERVER['HTTP_REFERER']) {
    if (stripos($_SERVER['HTTP_REFERER'], ly200::get_domain()) !== false) {
        $page_back_url = ly200::get_url($category_row);
    }
}

//面包屑
$Column=ly200::get_web_position($category_row, 'products_category', '', '<em><i></i></em>', 12);

//产品售卖状态
$is_stockout=(($products_row['SoldStatus']!=1 && ($products_row['Stock']<$products_row['MOQ'] || $products_row['Stock']<1)) || ($products_row['SoldOut']==1 && $products_row['IsSoldOut']==0) || ($products_row['IsSoldOut'] && ($products_row['SStartTime']>$c['time'] || $c['time']>$products_row['SEndTime'])));

//产品评论
$pro_info=ly200::get_pro_info($products_row);
$Sales=$pro_info[0];
$FavoriteCount=$pro_info[1];
$Rating=$pro_info[2];
$TotalRating=$pro_info[3];

//产品属性
$IsCombination=(int)$products_row['IsCombination']; //是否开启规格组合
$attr_ary=$color_attr_ary=$selected_ary=$color_picpath_ary=array();
$isHaveOversea=count($c['config']['Overseas']); //是否开启海外仓
if((int)$c['config']['global']['Overseas']==0){ //关闭海外仓功能
    $isHaveOversea=1;
}
if($CateId || $isHaveOversea){
    $products_attr=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%' and DescriptionAttr=0", "AttrId, Name{$c['lang']}, CartAttr, ColorAttr, Type", $c['my_order'].'AttrId asc'));
    foreach((array)$products_attr as $v){
        if($v['CartAttr']){ //购物车属性
            $attr_ary['Cart'][$v['AttrId']]=$v;
        }else{ //普通属性
            $attr_ary['Common'][$v['AttrId']]=$v;
        }
        (int)$v['ColorAttr'] && $color_attr_ary[]=$v['AttrId'];
    }
    $products_selected_attr=str::str_code(db::get_all('products_selected_attr', "ProId='{$ProId}'"));
    if($products_selected_attr){
        $products_attr_status_ary=$color_attr_ary=array();
        foreach((array)$products_selected_attr as $v){
            $products_attr_status_ary[$v['AttrId']]=(int)$v['IsUsed'];
            (int)$v['IsColor'] && $color_attr_ary[]=$v['AttrId'];
        }
    }
    $selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='{$ProId}' and IsUsed=1", '*', 'SeleteId asc'));
    foreach($selected_row as $v){
        $selected_ary['Id'][$v['AttrId']][]=$v['VId']; //记录勾选属性ID
        $selected_ary['MyOrder'][$v['VId']]=$v['MyOrder']; //记录勾选属性排序
        $v['AttrId']>0 && $v['VId']==0 && $v['OvId']<2 && $selected_ary['Value'][$v['AttrId']]=$v['Value'.$c['lang']]; //文本框内容
        $v['AttrId']==0 && $v['VId']==0 && $v['OvId']>0 && $selected_ary['Overseas'][]=$v['OvId']; //记录勾选属性ID 发货地
    }
    $color_row=str::str_code(db::get_all('products_color', "ProId='{$ProId}'", 'VId, PicPath_0'));
    foreach((array)$color_row as $k=>$v){//统计产品颜色图片
        if(!$v['PicPath_0']) continue;
        if(is_file($c['root_path'].$v['PicPath_0'])){
            $color_picpath_ary[$v['VId']]=$v['PicPath_0'];
        }
    }
}
$isHaveAttr = (int)($attr_ary['Cart'] && $products_row['AttrId'] == ($TopCategory_row ? $TopCategory_row['AttrId'] : $category_row['AttrId'])); //是否有规格属性
if ($isHaveAttr || $isHaveOversea) {
    //初始化
    $combinatin_ary = $all_value_ary = $attrid = $ext_ary = array();
    foreach ($attr_ary['Cart'] as $v) {
        $attrid[] = $v['AttrId'];
    }
    $attrid_list = implode(',', $attrid);
    !$attrid_list && $attrid_list = '0';
    $value_row = str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
    foreach ($value_row as $v) {
        $all_value_ary[$v['AttrId']][$v['VId']] = $v;
    }
    //属性组合数据
    $combinatin_row = str::str_code(db::get_all('products_selected_attribute_combination', "ProId='{$ProId}'", '*', 'CId asc'));
    foreach ($combinatin_row as $v) {
        $combinatin_ary[$v['Combination']][$v['OvId']] = array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
        $key = str_replace('|', '_', substr($v['Combination'], 1, -1));
        $v['OvId'] < 1 && $v['OvId'] = 1;
        $IsCombination == 1 && $key .= ($key ? '_' : '') . 'Ov:' . $v['OvId'];
        $ext_ary[$key] = array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
    }
    unset($attrid);
}

//产品价格和折扣
$CurPrice=$products_row['Price_1'];
$is_wholesale=(in_array('wholesale', $c['plugins']['Used']) && $products_row['Wholesale'] && $products_row['Wholesale']!='[]');
if($is_wholesale){
    $wholesale_price=str::json_data(htmlspecialchars_decode($products_row['Wholesale']), 'decode');
    foreach((array)$wholesale_price as $k=>$v){
        if($MOQ<$k) break;
        $CurPrice=(float)$v;
    }
    $maxPrice=reset($wholesale_price);
    $minPrice=end($wholesale_price);
}
$discount=($Price_1-$CurPrice)/((float)$Price_1?$Price_1:1)*100;

//产品促销
$is_promotion=((int)$products_row['IsPromotion'] && $products_row['StartTime']<$c['time'] && $c['time']<$products_row['EndTime']);
if($is_promotion && !$products_row['PromotionType']){//现金类型
    $CurPrice=$products_row['PromotionPrice'];
}

//秒杀
$sec_where="ProId='$ProId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime";
$SId=(int)$_GET['SId'];
$SId && $sec_where="SId='{$SId}' and ".$sec_where;
$sales_row=str::str_code(db::get_one('sales_seckill', $sec_where));
if($sales_row){
    $is_promotion=0;//秒杀优先于促销
    $IsSeckill=1;
    $CurPrice=$sales_row['Price'];
    $discount=($Price_1-$CurPrice)/((float)$Price_1?$Price_1:1)*100;
    $SId=$sales_row['SId'];
    $SMax=($sales_row['MaxQty'] && $sales_row['RemainderQty'] && $sales_row['RemainderQty']>=$sales_row['MaxQty']?$sales_row['MaxQty']:$sales_row['RemainderQty']); //最大购买上限
    $SMax<=$Max && $Max=$SMax;
}

//团购
$TId=(int)$_GET['TId'];
if($TId){
    $tuan_row=str::str_code(db::get_one('sales_tuan', "TId='{$TId}' and ProId='$ProId' and BuyerCount<TotalCount and {$c['time']} between StartTime and EndTime"));
    if($tuan_row){
        $is_promotion=0;//团购优先于促销
        $IsTuan=1;
        $CurPrice=$tuan_row['Price'];
        $discount=($Price_1-$CurPrice)/((float)$Price_1?$Price_1:1)*100;
        //$Max=$tuan_row['TotalCount']-$tuan_row['BuyerCount']; //最大购买上限
        $Max=1;//最大购买上限
        $Column='<em><i></i></em><a href="'.$c['nav_cfg'][5]['url'].'">'.$c['nav_cfg'][5]['name'.$c['lang']].'</a>';
    }
}

//最后拍板
if(!$IsSeckill && !$IsTuan && $is_wholesale){
    $CurPrice>$maxPrice && $maxPrice=$CurPrice;
    $CurPrice<$minPrice && $minPrice=$CurPrice;
}
$discount=sprintf('%01.0f', $discount);
$oldPrice=(($SId && $IsSeckill) || ($TId && $IsTuan) || $is_promotion)?$Price_1:$Price_0;
$ItemPrice=$CurPrice;
$CurPrice=($is_promotion && $products_row['PromotionType']?$CurPrice*($products_row['PromotionDiscount']/100):$CurPrice);
$save_discount=@intval(sprintf('%01.2f', ($oldPrice-$CurPrice)/$oldPrice*100));
$save_discount=$save_discount<1?1:$save_discount;
$Max=$Max<1?1:$Max;

//默认国家参数
if($_SESSION['User']['UserId']){
    $country_default_row=str::str_code(db::get_one('orders o left join country c on o.ShippingCId=c.CId', "UserId='{$_SESSION['User']['UserId']}'", 'o.ShippingCId, c.Country, c.Acronym, c.CountryData', 'OrderId desc'));//会员订单信息
    if(!$country_default_row['ShippingCId'] || !$country_default_row['CountryData']){//获取会员地址信息
        $address_where=($_SESSION['Cart']['ShippingAddressAId']?"a.AId={$_SESSION['Cart']['ShippingAddressAId']}":"a.UserId='{$_SESSION['User']['UserId']}' and a.IsBillingAddress=0");//默认？第一个？
        $country_default_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId', $address_where, 'a.CId, c.Country, c.Acronym, c.CountryData', 'a.AccTime desc, a.AId desc'));
    }else{
        $country_default_row['CId']=$country_default_row['ShippingCId'];
    }
}else{//默认国家
    $country_default_row=str::str_code(db::get_one('country', 'IsUsed=1', 'CId, Country, Acronym, CountryData', 'IsDefault desc, Country asc'));
}
if($country_default_row['CountryData']){
    $country_default_data=str::json_data(htmlspecialchars_decode($country_default_row['CountryData']), 'decode');
    $country_default_row['Country']=$country_default_data[substr($c['lang'], 1)];
}

//组合产品
in_array('package', $c['plugins']['Used']) && $group_promotion_ary[0]=ly200::get_products_package($ProId);//组合购买
in_array('promotion', $c['plugins']['Used']) && $group_promotion_ary[1]=ly200::get_products_package($ProId, 1);//组合促销
if(!array_filter($group_promotion_ary)) unset($group_promotion_ary);

//SEO
$products_seo_row=str::str_code(db::get_one('products_seo', "ProId='$ProId'"));
$spare_ary=array(
    'SeoTitle'		=>	$Name.','.$category_row['Category'.$c['lang']],
    'SeoKeyword'	=>	$Name.','.$category_row['Category'.$c['lang']],
    'SeoDescription'=>	$Name.','.$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']]
);
if($IsTuan){
    $products_type=1;
}elseif($IsSeckill){
    $products_type=2;
}else{
    $products_type=0;
}

//产品选项卡
$tab_row=array();
$tab_ary=ly200::product_attribute_tab($ProId, $CateId);
foreach($tab_ary as $k=>$v){
    if(!$v["Name{$c['lang']}"]) continue;
    $tab_row[$k]['TabName']=$v["Name{$c['lang']}"];
    $tab_row[$k]['Tab']=$v["Description{$c['lang']}"];
}

ly200::set_products_history($products_row, $CurPrice, $oldPrice);
$view_num=count($_SESSION['Ueeshop']['ViewHistory']);
if($view_num==0){
    $_SESSION['Ueeshop']['ViewHistory']=array($products_row['ProId']);
    db::query("update products set View=View+1 where ProId='{$products_row['ProId']}'");
}else{
    if(!in_array($products_row['ProId'], $_SESSION['Ueeshop']['ViewHistory'])){
        $_SESSION['Ueeshop']['ViewHistory'][]=$products_row['ProId'];
        db::query("update products set View=View+1 where ProId='{$products_row['ProId']}'");
    }
}

//产品单位
if($products_row['Unit']){//产品自身设置单位
    $Unit=$products_row['Unit'];
}elseif($c['config']['products_show']['Config']['item'] && $c['config']['products_show']['item']){//产品统一设置单位
    $Unit=$c['config']['products_show']['item'];
}else{
    $Unit=$c['lang_pack']['products']['units'];
}
$OpenParameter=array();
if($products_row['OpenParameter']){
    $OpenParameter=explode('|', $products_row['OpenParameter']);
}
in_array('Unit', $OpenParameter) || $Unit='';

//收藏列表
$_SESSION['User']['UserId'] && $user_favorite_row=db::get_all('user_favorite',"UserId='{$_SESSION['User']['UserId']}'",'ProId');
$user_favorite_ary=array();
foreach((array)$user_favorite_row as $v){
    $user_favorite_ary[]=$v['ProId'];
}

//快捷支付
$is_paypal_checkout=(int)db::get_row_count('payment', "Method='Excheckout' and IsUsed=1");
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection" />

    <!--<meta property="og:type" content="article">-->
    <!--<meta property="og:title" content="--><?//=$Name;?><!--">-->
    <!--<meta property="og:description" content="share">-->
    <!--<meta property="og:image" content="http://lopscoop.com/static/html/share/lucklymeta.jpg">-->
    <!--<meta property="og:url" content="http://********:8020/whatsappshop-one/one.html">-->

    <?=ly200::seo_meta($products_seo_row, $spare_ary);?>
    <?=cart::products_pins($products_row, $CurPrice); ?>
    <?php include("{$c['mobile']['theme_path']}inc/resource.php");?>
    <?=ly200::load_static("{$c['mobile']['tpl_dir']}css/goods.css", "{$c['mobile']['tpl_dir']}js/goods.js", "{$c['mobile']['tpl_dir']}js/cart.js");?>
    <style>
        .detail_desc table{border-collapse:collapse; width:100%;}
        .detail_desc table td{border:1px solid #ccc;}
    </style>
    <?php
    if(in_array('facebook_pixel', $c['plugins']['Used'])){
        //Facebook Pixel
        ?>
        <!-- Facebook Pixel Code -->
        <script type="text/javascript">
            <!-- When a page viewed such as landing on a product detail page. -->
            fbq('track', 'ViewContent', {
                content_type: 'product',//产品类型为产品
                content_ids: ['<?=$products_row['SKU']?addslashes(htmlspecialchars_decode($products_row['SKU'])):addslashes(htmlspecialchars_decode($products_row['Prefix'])).addslashes(htmlspecialchars_decode($products_row['Number']));?>'],//产品ID
                content_name: '<?=addslashes($Name);?>',//产品名称
                value: <?=cart::iconv_price($CurPrice, 2, '', 0);?>,//产品价格
                currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
            });

            <!-- When some adds a product to a shopping cart. -->
            $.fn.fbq_addtocart=function(val){
                fbq('track', 'AddToCart', {
                    content_type: 'product',//产品类型为产品
                    content_ids: ['<?=$products_row['SKU']?addslashes(htmlspecialchars_decode($products_row['SKU'])):addslashes(htmlspecialchars_decode($products_row['Prefix'])).addslashes(htmlspecialchars_decode($products_row['Number']));?>'],//产品ID
                    content_name: '<?=addslashes($Name);?>',//产品名称
                    value: val,//数值
                    currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
                });
            }
        </script>
        <!-- End Facebook Pixel Code -->
    <?php }?>
    <script type="text/javascript">
        $(document).ready(function(){
            <?php
            if ($c['NewFunVersion'] >= 4 && $is_paypal_checkout) {
                //新版Paypal checkout
                echo 'cart_obj.paypal_checkout_init("#paypal_button_container");';
            }?>
        });
    </script>
</head>

<body class="lang<?=$c['lang'];?>">
<?php include("{$c['mobile']['theme_path']}inc/header.php");?>
<?php include("{$c['mobile']['theme_path']}header/{$c['mobile']['HeaderTpl']}/header.php");?>
<div class="wrapper">
    <div class="detail_pic clean">
        <div class="detail_pic_container"></div>
        <div class="detail_pic_toolbar clean">
            <a rel="nofollow" class="gh_menu fl gh_menu_back" href="<?=$page_back_url;?>"><i></i></a>
            <a rel="nofollow" class="gh_menu fr gh_menu_share" href="javascript:;"><i></i></a>
        </div>
    </div>
    <?php
    if ((int)$IsTuan) {
        //团购盒子
        ?>
        <div class="clean prod_info_tuan">
            <div class="item"><i class="icon_time"></i><?=str_replace('%time%', '<span class="flashsale_time" endTime="' . date('Y/m/d H:i:s', $tuan_row['EndTime']) . '"></span>', $c['lang_pack']['saleEnd']);?></div>
            <div class="item"><i class="icon_bought"></i><?=$tuan_row['BuyerCount'];?><br /><?=$c['lang_pack']['bought'];?></div>
            <?php if ($TotalRating) {?>
                <div class="item"><?=html::mobile_review_star($Rating);?><br /><?=$TotalRating;?><br /><?=$c['lang_pack']['ratings'];?></div>
            <?php }?>
        </div>
    <?php }?>
    <div class="goods_info clean">
        <?php if ($TotalRating) {?>
            <div class="prod_info_star">
                <?=html::mobile_review_star($Rating);?><span class="review_nums"><?=$TotalRating;?> reviews</span><span class="sold fr"><?=$Sales;?> sold</span>
            </div>
        <?php }?>
        <div class="prod_info_name"><?=$Name;?></div>
        <?php /*if ($products_row['BriefDescription' . $c['lang']]) {?>
			<div class="prod_info_brief"><?=str::format(htmlspecialchars_decode($products_row['BriefDescription' . $c['lang']]));?></div>
		<?php }*/?>
        <?php
        if ((int)$IsSeckill) {
            //秒杀盒子
            $time = $sales_row['EndTime']-$c['time'];
            $progress = ceil((1 - $sales_row['RemainderQty'] / $sales_row['Qty']) * 100);
            ?>
            <div class="clean prod_info_seckill">
                <div class="title"><?=$c['lang_pack']['flashSale'];?></div>
                <div class="clear"></div>
                <div class="time"><i class="icon_time"></i><?=str_replace('%time%', '<span class="flashsale_time" endTime="' . date('Y/m/d H:i:s', $sales_row['EndTime']) . '"></span>', $c['lang_pack']['dealsEnd']);?></div>
                <div class="progress_count"><?=($sales_row['Qty'] - $sales_row['RemainderQty']) . ' ' . $c['lang_pack']['sold'];?></div>
                <div class="progress"><div class="progress_current" style="width:<?=$progress;?>%;"></div></div>
            </div>
        <?php } else {?>
            <div class="prod_info_line"></div>
        <?php }?>
        <div class="clean prod_info_price">
            <div class="box_price price_1 last_price">
                <div class="price cur_price"><?=$_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price($CurPrice);?></div>
                <?php
                if ($is_promotion) {
                    $time = $products_row['EndTime'] - $c['time'];
                    $promotion_discount = @round(sprintf('%01.2f', ($Price_1 - $ItemPrice) / $Price_1 * 100));
                    if ($products_row['PromotionType']) $promotion_discount = 100 - $products_row['PromotionDiscount'];
                    $month = ceil($time / 86400);
                    if ($month < 31) echo '<div class="onlydays">(' . sprintf($c['lang_pack']['mobile']['only_days'], $month) . ')</div>';
                }
                if ($CurPrice > $oldPrice && $oldPrice > 0) {
                    echo '<div class="save_price">' . $c['lang_pack']['products']['save'] . ' <span class="save_p">' . cart::iconv_price($oldPrice - $CurPrice) . '</span><span class="save_style">(' . $save_discount . '% Off)</span></div>';
                }?>
            </div>
            <?php
            if ($oldPrice > $CurPrice && $oldPrice > 0) {
                //市场价
                echo '<div class="box_price price_0"><del>' . $_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price($oldPrice) . '</del></div>';
            }?>
        </div>
        <?php if ((int)$_GET['ueeshop_store'] == 0) {?>
            <div class="clean prod_info_favorite">
                <a rel="nofollow" class="global_trans add_favorite <?=in_array($ProId, $user_favorite_ary) ? 'is_in' : ''; ?>" href="javascript:;" data="<?=$ProId;?>"><i></i></a>
                <p><?=$FavoriteCount;?></p>
            </div>
        <?php }?>
        <div class="clear"></div>
        <?php
        if (!$IsSeckill && $is_wholesale) {
            $wholesale_ary = array();
            $wholesale_count = count($wholesale_price);
            foreach ($wholesale_price as $k => $v) {
                $wholesale_ary[] = array('Unit'=>$k, 'Price'=>$v);
            }
            ?>
            <div class="list_wholesale clean">
                <dl class="wholesale_list" data="<?=$products_row['Wholesale'];?>">
                    <?php foreach ($wholesale_ary as $k => $v) {?>
                        <dd class="item fl clean ui_border_l" data-num="<?=$v['Unit'];?>">
                            <div class="wunits"><?=str_replace('%count%', ($k + 1 == $wholesale_count ? $v['Unit'] . ' +' : $v['Unit'] . ' - ' . ($wholesale_ary[$k + 1]['Unit'] - 1)), $c['lang_pack']['products']['pcs']);?></div>
                            <div class="wprice" data-price="<?=$v['Price'];?>" data-discount="<?=1-($v['Price']/$Price_1);?>"><?=$_SESSION['Currency']['Currency'].' '.cart::iconv_price($v['Price']);?></div>
                        </dd>
                    <?php }?>
                </dl>
            </div>
        <?php }?>
        <?php
        if ($attr_ary['Cart'] || $isHaveOversea) {
            //属性显示
            $AttrName = '';
            if ($attr_ary['Cart']) {
                //规格属性
                $attr_name_ary = array();
                foreach ((array)$attr_ary['Cart'] as $k => $v) {
                    if (!$selected_ary['Id'][$v['AttrId']]) continue; //踢走
                    $attr_value_count = 0;
                    foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
                        if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
                        $attr_value_count++;
                    }
                    if (!$attr_value_count) continue;
                    $attr_name_ary[] = $v['Name'.$c['lang']];
                }
                if ($attr_name_ary) {
                    $AttrName .= implode(', ', $attr_name_ary);
                }
            }
            if ($isHaveOversea) {
                //海外仓
                $oversea_ary = array();
                foreach($c['config']['Overseas'] as $k => $v) {
                    if (!$selected_ary['Overseas'] && $v['OvId'] > 1) continue; //踢走
                    if ($selected_ary['Overseas'] && !in_array($v['OvId'], $selected_ary['Overseas'])) continue; //踢走
                    $oversea_ary[] = $v['OvId'];
                }
                if (count($oversea_ary) > 1) {
                    $AttrName .= ($AttrName ? ', ' :'') . $c['lang_pack']['products']['shipsFrom'];
                }
            }
            if ($AttrName) {
                ?>
                <div class="clean prod_info_attribute">
                    <div class="item">
                        <div class="attr_name"><?=$c['lang_pack']['user']['pleaseSelect'];?><span><?=$AttrName;?></span></div>
                        <div class="attr_option">----</div>
                    </div>
                </div>
                <?php
            }
        }?>
        <div class="clean rows prod_info_qty" data="<?=htmlspecialchars('{"min":' . $MOQ . ',"max":' . $Max . ',"count":' . $MOQ . '}');?>">
            <div class="title"><?=strtolower($c['lang_pack']['mobile']['QTY']);?> :</div>
            <div class="txt">
                <div class="cut"></div>
                <!--  <div class="qty"><input type="number" name="Qty" value="<?=$products_row['MOQ'] ? $products_row['MOQ'] : 1;?>" id="quantity" data-stock="<?=$Max;?>" /></div> -->
                <div class="qty"><input type="number" name="Qty[]" id="quantity" class="quantity" value="<?=$products_row['MOQ'] ? $products_row['MOQ'] : 1;?>" data-stock="<?=$Max;?>" /></div>
                <div class="add"></div>

            </div>
            <div class="stock"><?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">' . $Max . '</b>', $Unit), $c['lang_pack']['products']['available']);?></div>
        </div>
<!--        <a class="btn_global add_btn" target="_blank"  href="https://api.whatsapp.com/send?phone=8619924351059&amp;text=May I help you?" style="display: inline-block;background-color:#fcd340;color: #fff;margin-top: 1.25rem;height:12vw;line-height: 12vw;font-size:5vw">--><?//=$c['lang_pack']['contactUs']; ?><!--</a>-->
        <a class="btn_global add_btn zhzshare"  style="display: inline-block;background-color:#fcd340;color: #fff;margin-top: 1.25rem;height:12vw;line-height: 12vw;font-size:5vw;cursor:pointer;"><?=$c['lang_pack']['contactUs']; ?></a>

        <a class="clean prod_info_action<?=(int)$_GET['ueeshop_store'] == 1 ? ' prod_info_store' : '';?>">
            <?php
            $pre_fix = 's_';
            if (!$AttrName) {
                $pre_fix = '';
            }
            if (!$is_stockout) {
                //正常售卖
                if ($is_paypal_checkout) {
                    //Paypal快捷支付
                    if ($c['NewFunVersion'] < 4 || ($AttrName && $c['NewFunVersion'] >= 4)) {
                        //旧版Paypal checkout
                        echo '<div class="btn_buynow"><input type="button" value="" class="btn_global add_btn paypal_checkout_button" id="' . $pre_fix . 'paypal_checkout_button" /></div>';
                    } else {
                        //新版Paypal checkout
                        echo '<div class="btn_replace">' . cart::btn_paypal_replace('btn_global add_btn') . '</div>';
                        echo '<div class="btn_paypal"><div id="' . $pre_fix . 'paypal_button_container" class="add_btn"></div></div>';
                    }
                } else {
                    //立即购买
                    echo '<input type="button" value="' . $c['lang_pack']['products']['buyNow'] . '" class="btn_global add_btn buynow BuyNowBgColor" id="' . $pre_fix . 'zhzbuynow_button" />';
                }
                if ((int)$_GET['ueeshop_store'] == 0) {
                    //添加购物车
                    echo '<input type="submit" value="' . $c['lang_pack']['products']['addToCart'] . '" class="btn_global add_btn addtocart" id="' . $pre_fix . 'zhzaddtocart_button" />';
                }
            } else {
                //脱销
                echo '<input type="button" value="' . $c['lang_pack']['products']['soldOut'] . '" class="btn_global add_btn soldout" />';
            }?>
        </div>
        <div class="clean prod_info_platform">
            <?php
            $platform = str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
            if (count($platform)) echo '<div class="platform_title ui_border_t"><strong>' . $c['lang_pack']['products']['platform_tips'] . '</strong></div>';
            foreach ((array)$platform as $k => $v) {
                if (!$v[0]['Url' . $c['lang']]) continue;
                if (count($v) > 1) {
                    ?>
                    <div class="btn_global add_btn platform_btn btn_<?=$k;?>">
                        <i></i><?=$c['lang_pack']['products'][$k];?><em></em>
                        <div class="platform_ab">
                            <?php foreach ((array)$v as $v1) {?>
                                <a href="<?=$v1['Url' . $c['lang']];?>" target="_blank"><?=$v1['Name' . $c['lang']];?></a>
                            <?php }?>
                        </div>
                    </div>
                    <?php
                } else {
                    $icon = @explode('_', $k);
                    echo '<a href="' . $v[0]['Url' . $c['lang']] . '" target="_blank" class="btn_global add_btn platform_btn btn_' . $icon[0] . '"><i></i>' . $c['lang_pack']['products'][$k] . '</a>';
                }
            }?>
        </div>
    </div>
    <div id="prod_form_box">
        <form id="goods_form" action="?" method="post">
            <a href="javascript:;" class="close">Close</a>
            <div class="clean prod_info_scroll">
                <div class="clean prod_info_information">
                    <div class="pic pic_box"><img src="<?=ly200::get_size_img($products_row['PicPath_0'], '240x240');?>" /><span></span></div>
                    <div class="pro_price ui_border_b"><?=$_SESSION['Currency']['Currency'] . ' ' . cart::iconv_price($CurPrice);?></div>
                </div>
                <div class="clean prod_info_option">
                    <?php
                    if ($isHaveAttr || $isHaveOversea) {
                        foreach ((array)$attr_ary['Cart'] as $k => $v) {
                            if (!$selected_ary['Id'][$v['AttrId']]) continue; //踢走
                            $attr_value_count = 0;
                            foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
                                if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
                                $attr_value_count++;
                            }
                            if (!$attr_value_count) continue;
                            $new_value_ary = array(); //重新定义，属性选项排序
                            foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
                                if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
                                $MyOrder = (int)$selected_ary['MyOrder'][$k2];
                                if ($MyOrder) {
                                    $new_value_ary[$v['AttrId']][$MyOrder] = array('VId'=>$k2, 'Data'=>$v2);
                                } else {
                                    $new_value_ary[$v['AttrId']][] =array('VId'=>$k2, 'Data'=>$v2);
                                }
                                ksort($new_value_ary[$v['AttrId']]);
                            }
                            ?>
                            <div class="clean rows attr_show none" name="<?=$v['Name' . $c['lang']];?>">
                                <div class="title"><?=$v['Name' . $c['lang']];?> :</div>
                                <div class="txt">
                                    <!-- by zhz start-->
                                    <style>
                                        .prod_info_option .attr_show span{
                                            margin-bottom: 0rem;
                                        }
                                        <? if($c['lang'] == '_fa') { ?>
                                            .zhznum{
                                                float: left;
                                            }
                                        <? }else{ ?>

                                            .zhznum{
                                                float: right;
                                            }
                                        <? } ?>
                                    </style>
                                    <!-- by zhz end-->
                                    <?php
                                    foreach ((array)$new_value_ary[$v['AttrId']] as $k2 => $v2) {
                                        if (!in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])) continue; //踢走
                                        $value = $combinatin_ary["|{$v2['VId']}|"][1];
                                        $price = (float)$value[0];
                                        $qty = (int)$value[1];
                                        $weight = (float)$value[2];
                                        $sku = $value[3];
                                        $increase = (int)$value[4];
                                        ?>

                                        <span value="<?=$v2['VId'];?>" data="<?=htmlspecialchars('{"Price":' . $price . ',"Qty":' . $qty . ',"Weight":' . $weight . ',"SKU":' . $sku . ',"IsIncrease":' . $increase . '}');?>" class="<?=((int)$products_row['SoldStatus'] == 0 && $IsCombination && $value && $qty < 1) ? ' out_stock' : '';?><?=$color_picpath_ary[$v2['VId']] ? ' pic_color' : '';?>" title="<?=htmlspecialchars($v2['Data']['Value' . $c['lang']]);?>">
                                            <?php
                                            if ($color_picpath_ary[$v2['VId']]) {
                                                echo '<a class="attr_pic"><img src="' . $color_picpath_ary[$v2['VId']] . '" alt="' . $v2['Data']['Value' . $c['lang']] . '" /></a>';
                                            } else {
                                                echo '<b>' . $v2['Data']['Value' . $c['lang']] . '</b>';
                                            }?>
                                        </span>
                                        <!-- by zhz start-->
                                        <div class="clean rows prod_info_qty none zhznum" data="<?=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?>">
                                            <div class="txt">
                                                <div class="cut"></div>
                                                <div class="qty"><input type="number" name="Qty[]" value="0"  class="quantity zhzquantity" data-stock="<?=$Max;?>" /></div>
                                                <div class="add"></div>
                                            </div>
                                            <div class="stock"><?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?></div>
                                        </div>
                                        <br>
                                        <i><?=cart::iconv_price($CurPrice+$price);?></i>
                                        <br>
                                        <input type="hidden" name="zhzid[][<?=$v['AttrId'];?>]" id="zhzattr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="<?=$v2['VId'];?>" class="attr_value<?=in_array($v['AttrId'], $color_attr_ary) ? ' colorid' : '';?>" />
                                        <!-- by zhz end-->
                                    <?php }?>
                                    <input type="hidden" name="id[<?=$v['AttrId'];?>]" id="attr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="" class="attr_value<?=in_array($v['AttrId'], $color_attr_ary) ? ' colorid' : '';?>" />
                                    <!-- by zhz start-->
                                    <input type="hidden" name="Qty1" value="" id="quantity" data-stock="<?=$Max;?>" />
                                    <!-- by zhz end-->
                                </div>
                            </div>
                            <?php
                        }
                        if ($isHaveOversea) {
                            //发货地
                            ?>
                            <div class="clean rows attr_show none" name="<?=$c['lang_pack']['products']['shipsFrom'];?>" style="display:<?=((int)$c['config']['global']['Overseas']==1 && count($selected_ary['Overseas'])>1 && $IsCombination==1)?'block':'none';?>;">
                                <div class="title"><?=$c['lang_pack']['products']['shipsFrom'];?>:</div>
                                <div class="txt">
                                    <?php
                                    foreach($c['config']['Overseas'] as $k=>$v){
                                        $Ovid='Ov:'.$v['OvId'];
                                        if(!$selected_ary['Overseas'] && $v['OvId']>1) continue; //踢走
                                        if($selected_ary['Overseas'] && !in_array($v['OvId'], $selected_ary['Overseas'])) continue; //踢走
                                        $value=$combinatin_ary['||'][$v['OvId']];
                                        $price=(float)$value[0];
                                        $qty=(int)$value[1];
                                        $weight=(float)$value[2];
                                        $sku=$value[3];
                                        $increase=(int)$value[4];
                                        ?>
                                        <span value="<?=$Ovid;?>" data="<?=htmlspecialchars('{"Price":'.$price.',"Qty":'.$qty.',"Weight":'.$weight.',"SKU":'.$sku.',"IsIncrease":'.$increase.'}');?>" class="<?=((int)$products_row['SoldStatus']==0 && $IsCombination && $value && $qty<1)?' out_stock':'';?>" title="<?=htmlspecialchars($v['Name'.$c['lang']]);?>"><b><?=$v['Name'.$c['lang']];?></b><em>X</em></span>
                                    <?php }?>
                                    <input type="hidden" name="id[Overseas]" id="attr_Overseas" attr="Overseas" value="" class="attr_value" />
                                </div>
                            </div>
                            <?php
                        }
                    }?>
                    <? if (!$AttrName) { ?>
                        <div class="qty"><input type="number" name="Qty[]" 111 value="<?=$products_row['MOQ']?$products_row['MOQ']:1;?>" id="zzquantity" data-stock="<?=$Max;?>" /></div>
                        <input type="hidden" id="isguige" value="1">
                    <? } ?>
<!--                    <div class="clean rows prod_info_qty none" data="--><?//=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?><!--">-->
<!--                        <div class="title">--><?//=strtolower($c['lang_pack']['mobile']['QTY']);?><!-- :</div>-->
<!--                        <div class="txt">-->
<!--                            <div class="cut"></div>-->
<!--                            <div class="qty"><input type="number" name="Qty" value="--><?//=$products_row['MOQ']?$products_row['MOQ']:1;?><!--" id="quantity" data-stock="--><?//=$Max;?><!--" /></div>-->
<!--                            <div class="add"></div>-->
<!--                        </div>-->
<!--                        <div class="stock">--><?//=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?><!--</div>-->
<!--                    </div>-->
                    <div class="clean prod_info_action">
                        <?php
                        if (!$is_stockout) {
                            //正常售卖
                            if ($is_paypal_checkout) {
                                //Paypal快捷支付
                                if ($c['NewFunVersion'] >= 4) {
                                    //新版Paypal checkout
                                    echo '<div class="btn_replace">' . cart::btn_paypal_replace('btn_global add_btn') . '</div>';
                                    echo '<div class="btn_paypal"><div id="paypal_button_container" class="add_btn"></div></div>';
                                } else {
                                    //旧版Paypal checkout
                                    echo '<div class="btn_buynow"><input type="button" value="" class="btn_global add_btn paypal_checkout_button" id="paypal_checkout_button" /></div>';
                                }
                            } else {
                                //立即购买
                                echo '<input type="button" value="' . $c['lang_pack']['products']['buyNow'] . '" class="btn_global add_btn buynow BuyNowBgColor" id="zhzbuynow_button" />';
                            }
                            if ((int)$_GET['ueeshop_store'] == 0) {
                                //添加购物车
                                echo '<input type="submit" value="' . $c['lang_pack']['products']['addToCart'] . '" class="btn_global add_btn addtocart" id="zhzaddtocart_button" />';
                            }
                        } else {
                            //脱销
                            echo '<input type="button" value="' . $c['lang_pack']['products']['soldOut'] . '" class="btn_global add_btn soldout" />';
                        }?>
                    </div>
                </div>
            </div>
            <input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
            <input type="hidden" id="ItemPrice" name="ItemPrice" value="<?=$ItemPrice;?>" initial="<?=$ItemPrice;?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=$is_promotion && !$products_row['PromotionType']?$products_row['PromotionPrice']:'';?>" discount="<?=$is_promotion && $products_row['PromotionType']?$products_row['PromotionDiscount']:'';?>" old="<?=$oldPrice;?>" />
            <input type="hidden" name="Attr" id="attr_hide" value="{}" />
            <input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data($ext_ary));?>" />
            <input type="hidden" name="products_type" value="<?=$products_type;?>" />
            <input type="hidden" name="SId" value="<?=(int)$SId;?>"<?=((int)$IsSeckill && (int)$SId)?' stock="'.$Max.'"':'';?> />
            <input type="hidden" name="TId" value="<?=(int)$TId;?>" />
            <input type="hidden" id="CId" value="<?=(int)$country_default_row['CId'];?>" />
            <input type="hidden" id="CountryName" value="<?=$country_default_row['Country'];?>" />
            <input type="hidden" id="CountryAcronym" value="<?=$country_default_row['Acronym'];?>" />
            <input type="hidden" id="ShippingId" value="0" />
            <input type="hidden" id="attrStock" value="<?=(int)$products_row['SoldStatus'];?>" />
            <input type="hidden" id="IsCombination" value="<?=$IsCombination;?>" />
            <input type="hidden" id="IsOpenAttrPrice" value="<?=(int)$products_row['IsOpenAttrPrice'];?>" />
            <input type="hidden" id="IsDefaultSelected" value="<?=(int)$c['config']['products_show']['Config']['selected'];?>" />
        </form>
    </div>
    <div class="prod_info_divide"></div>
    <div class="detail_list">
        <?php if((int)$c['FunVersion'] && $group_promotion_ary){?>
            <div class="list list_sale clean ui_border_b">
                <a href="javascript:;" id="detail_sale">
                    <div class="sale_info"><?=$c['lang_pack']['products']['sales_group'];?></div>
                    <div class="sale_info_to"><?=$c['lang_pack']['products']['more_sales'];?></div>
                    <em></em>
                </a>
            </div>
        <?php }?>
        <?php if(in_array('freight', $c['plugins']['Used'])){?>
            <div class="list clean">
                <a href="javascript:;" id="detail_shipping">
                    <div class="shipping_cost_detail">
                        <span class="shipping_cost_price FontColor"></span>
                        <span class="shipping_cost_to"><?=$c['lang_pack']['products']['to'];?></span>
                        <span id="shipping_cost_button" class="shipping_cost_button"></span>
                    </div>
                    <div class="shipping_cost_info"><?=$c['lang_pack']['products']['shipEstimated'];?>:<span class="delivery_day"></span></div>
                    <div class="shipping_cost_error"><?=$c['lang_pack']['products']['shipError'];?></div>
                    <em></em>
                </a>
            </div>
        <?php }?>
    </div>
    <div class="prod_info_divide"></div>
    <div class="prod_info_detail">
        <div class="prod_detail_box prod_description">
            <div class="detail_title">
                <strong><?=$c['lang_pack']['products']['description'];?></strong>
            </div>
            <div class="detail_content editor_txt">
                <?php
                if($attr_ary['Common']){
                    $all_value_ary=$attrid=array();
                    foreach($attr_ary['Common'] as $v){ $attrid[]=$v['AttrId']; }
                    $attrid_list=implode(',', $attrid);
                    !$attrid_list && $attrid_list=0;
                    $value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
                    foreach($value_row as $v){ $all_value_ary[$v['AttrId']][$v['VId']]=$v; }
                    ?>
                    <div class="item_specifics">
                        <div class="title"><?=$c['lang_pack']['products']['specifics'];?></div>
                        <?php
                        $item=0;
                        foreach((array)$attr_ary['Common'] as $k=>$v){
                            if(!$v || !$v['Name'.$c['lang']] || $products_attr_status_ary[$v['AttrId']]==2 || ($v['Type']==1 && !$selected_ary['Id'][$v['AttrId']]) || ($v['Type']==0 && !$selected_ary['Value'][$v['AttrId']])) continue;
                            ?>
                            <span>
                                <strong><?=$v['Name'.$c['lang']];?>:</strong>
                                <?php
                                if($v['Type']==1 && is_array($all_value_ary[$v['AttrId']])){
                                    $i=0;
                                    foreach($all_value_ary[$v['AttrId']] as $k2=>$v2){
                                        if(in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])){
                                            echo ($i?', ':'').$v2['Value'.$c['lang']];
                                            ++$i;
                                        }
                                    }
                                }else echo stripslashes($selected_ary['Value'][$v['AttrId']]);
                                ?>
                            </span>
                            <?php
                            ++$item;
                        }?>
                    </div>
                <?php }?>
                <?php if(!$item){?>
                    <script type="text/javascript">$('.item_specifics').remove();</script>
                <?php }?>
                <?=str::str_code($products_description_row['Description'.$c['lang']], 'htmlspecialchars_decode');?>
            </div>
        </div>
        <?php
        if ($tab_row && count($tab_row) > 0) echo '<div class="prod_info_divide"></div>';
        $i = 0;
        foreach ((array)$tab_row as $k => $v) {
            //选项卡
            ?>
            <div class="prod_detail_box prod_description<?=$i > 0 ? ' ui_border_t' : '';?>">
                <div class="detail_title">
                    <strong><?=$v['TabName'];?></strong>
                    <em></em>
                </div>
                <div class="detail_content editor_txt close"><?=str::str_code(stripslashes($v['Tab']), 'htmlspecialchars_decode');?></div>
            </div>
            <?php
            ++$i;
        }?>
        <?php
        if ((int)$_GET['ueeshop_store'] == 0 && $TotalRating > 0) {
            //评论
            ?>
            <div class="prod_info_divide"></div>
            <div class="prod_detail_box goods_review">
                <?php /*
                <div class="goods_review_title">
                    <div class="title"><?=$c['lang_pack']['reviews'];?></div>
                    <?=html::mobile_review_star($Rating);?><br />
                    <?php if($TotalRating){?>
                        <div class="num">
                            <span class="review_nums">(<?=str_replace('%TotalRating%', $TotalRating, $c['lang_pack']['products']['basedOn']);?>)</span>
                        </div>
                    <?php }?>
                    <a href="<?=ly200::get_url($products_row, 'write_review');?>" class="btn_write_review FontBorderColor FontColor" rel="nofollow"><?=$c['lang_pack']['products']['writeReview'];?></a>
                </div>
                */?>
                <div class="detail_title">
                    <strong><?=$c['lang_pack']['reviews'];?></strong>
                    <div class="star"><?=html::mobile_review_star($Rating);?><span><?=$Rating;?></span></div>
                    <?php if($TotalRating){?>
                        <div class="review_nums"><?=str_replace('%count%', $TotalRating, $c['lang_pack']['mobile']['review_count']);?></div>
                    <?php }?>
                    <a href="<?=ly200::get_url($products_row, 'write_review');?>" class="btn_write_review" rel="nofollow"><?=$c['lang_pack']['products']['writeReview'];?></a>
                </div>
                <?php include('review_box.php');?>
            </div>
        <?php }?>
        <?php
        //You May Also Like
        if($TopCateId){
            $UId="0,{$TopCateId},";
            $cateid=$TopCateId;
        }else{
            $UId=category::get_UId_by_CateId($CateId);
            $cateid=$CateId;
        }
        $row=str::str_code(db::get_limit('products', "1 and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$cateid}' or ".category::get_search_where_by_ExtCateId($cateid, 'products_category').')'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 6));
        $len=count($row);
        if($len>0){
            ?>
            <div class="prod_info_divide"></div>
            <div class="prod_detail_box goods_recently">
                <div class="detail_title"><?=$c['lang_pack']['cart']['sLikeProd'];?></div>
                <div class="list">
                    <?php
                    foreach((array)$row as $k=>$v){
                        $is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
                        $url=ly200::get_url($v, 'products');
                        $img=ly200::get_size_img($v['PicPath_0'], '240x240');
                        $name=$v['Name'.$c['lang']];
                        $price_ary=cart::range_price_ext($v);
                        $price_0=$v["Price_{$is_promition}"];
                        $pro_info=ly200::get_pro_info($v);
                        $rating=$pro_info[2];
                        $total_rating=$pro_info[3];
                        ?>
                        <div class="item fl">
                            <div class="pic pic_box"><a href="<?=$url;?>" title="<?=$name;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a></div>
                            <div class="name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
                            <div class="price clean">
                                <div class="now_price"><?=$_SESSION['Currency']['Currency'].' '.cart::iconv_price($price_ary[0]);?></div>
                                <?php if ($price_0 > 0){?>
                                    <del class="old_price"><?=$_SESSION['Currency']['Currency'].' '.cart::iconv_price($price_0);?></del>
                                <?php }?>
                            </div>
                            <div class="star fl"><?=($rating?html::mobile_review_star($rating).'<span class="total">('.$total_rating.')</span>':'');?></div>
                        </div>
                    <?php }?>
                </div>
            </div>
        <?php }?>
    </div>
</div>
<?php
include("{$c['mobile']['theme_path']}footer/{$c['mobile']['FooterTpl']}/footer.php");
include("{$c['mobile']['theme_path']}inc/footer.php");
echo ly200::load_static('/static/js/plugin/lightbox/js/lightbox.min.js');
?>

<div id="tips_cart">
    <p><?=$c['lang_pack']['mobile']['cart_add'];?></p><p><?=str_replace('%num%', '<span class="tips_cart_count"></span>', $c['lang_pack']['mobile']['cart_items']);?></p><p><?=$c['lang_pack']['cart']['total'];?>: <span class="tips_cart_total"></span><p class="consumption"><?=str_replace('%price%', '<span class="FontColor"></span>', $c['lang_pack']['mobile']['consumption']);?></p></p>
    <div class="blank5"></div>
    <a href="/cart/" class="btn_global btn_check"><?=$c['lang_pack']['mobile']['pro_to_check'];?></a>
    <a href="javascript:;" class="btn_global btn_return"><?=$c['lang_pack']['mobile']['re_to_shop'];?></a>
</div>

<?php
if((int)$c['FunVersion'] && $group_promotion_ary){//组合产品
    include("{$c['mobile']['theme_path']}products/combination.php");
}
if(in_array('freight', $c['plugins']['Used'])){
    ?>
    <section id="detail_shipping_layer" class="prod_layer">
        <nav class="layer_head ui_border_b">
            <a class="layer_back" href="javascript:;"><em><i></i></em></a>
            <div class="layer_title"><?=$c['lang_pack']['products']['shippingCost'];?></div>
        </nav>
        <div class="layer_body">
            <div class="shipping_info">
                <div class="shipping_info_weight"><b><?=$c['lang_pack']['cart']['weight'];?>: </b><span>0.000</span>KG</div>
            </div>
            <form class="shipping_cost_form" name="shipping_cost_form" target="_blank" method="POST" action="">
                <div class="shipping_cost_country clean ui_border_tb">
                    <a href="javascript:;" id="shipping_country">
                        <span class="country_left"><?=$c['lang_pack']['user']['shipTo'];?></span>
                        <span class="country_right">
							<em></em><i></i>
							<span class="title"><span class="title_wrap"><?=$country_default_row['Country'];?></span></span>
							<span id="shipping_flag" class="icon_flag flag_<?=strtolower($country_default_row['Acronym']);?>"></span>
							<select name="CId"></select>
						</span>
                    </a>
                </div>
                <div class="prod_info_divide"></div>
                <div class="shipping_method clean ui_border_t">
                    <div class="title"><?=$c['lang_pack']['mobile']['choose_ship'];?></div>
                    <ul id="shipping_method_list"></ul>
                </div>
                <input type="hidden" name="ShippingSId" value="0" />
                <input type="hidden" name="ShippingMethodType" value="" />
                <input type="hidden" name="ShippingPrice" value="0" />
                <input type="hidden" name="ShippingExpress" value="" />
                <input type="hidden" name="ShippingBrief" value="" />
            </form>
        </div>
    </section>
<?php }?>
<?php
//分享按钮
$sahre_data = str::json_data(db::get_value('config', "GroupId='global' and Variable='Share'", 'Value'), 'decode');
//print_r($sahre_data);
?>
<div id="detail_share_layer" class="prod_layer">
    <div class="clean share_toolbox" data-title="<?=$Name;?>" data-url="<?=ly200::get_domain().$_SERVER['REQUEST_URI'];?>">
        <div class="share_list">
            <a href="javascript:;" class="close">Close</a>
            <!--            --><?php
            //            foreach ($sahre_data as $k => $v) {
            //                if ($v == 'googleplus') continue; //Google Plus 已下线
            //            ?>
            <!--                <div class="share_item"><btn_global add_btn addtocarta href="javascript:;" rel="nofollow" class="share_s_btn share_s_--><?//=$v;?><!--" data="--><?//=$v;?><!--">--><?//=$v;?><!--</a></div>-->
            <!--            --><?php //}?>
            <div class="share_item"><span rel="nofollow" class="zhzshare"><img src="/static/themes/default/mobile/images/plugins/share/timg.jpg" alt="" width="50">whatsapp</span></div>
        </div>
    </div>
    <script>
        //function shareWa(str){
        //    // content是我们自己定义的一些需要分享的内容;
        //    var content="";
        //    content+="<?//=$Name; ?>//";
        //    content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
        //    content+="\n"+"http://<?//=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>//"+"\n";
        //    //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
        //    location="https://api.whatsapp.com/send?phone=8619924351059&text="+ encodeURIComponent(content);
        //}
        $('.zhzshare').on('click', function() {
            // href="https://api.whatsapp.com/send?phone=8619924351059&text=May I help you?"
            var content="";
            content+="<?=$Name; ?>";
            //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
            content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
            //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
            location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
        })
        //$(document).on("click", ".zhzshare", function() {
        //    var content="";
        //    content+="<?//=$Name; ?>//";
        //    content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
        //    content+="\n"+"http://<?//=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>//"+"\n";
        //    //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
        //    location="https://api.whatsapp.com/send?phone=8619924351059&text="+ encodeURIComponent(content);
        //});
    </script>
</div>
</body>
</html>