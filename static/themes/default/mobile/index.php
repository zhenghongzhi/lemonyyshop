<?php !isset($c) && exit();?>
<!DOCTYPE HTML>
<html lang="<?=substr($c['lang'], 1);?>">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta content="telephone=no" name="format-detection" />
<?php
echo ly200::seo_meta();
include("{$c['mobile']['theme_path']}inc/resource.php");
echo ly200::load_static("{$c['mobile']['tpl_dir']}js/swipe.js", "{$c['mobile']['tpl_dir']}index/{$c['mobile']['HomeTpl']}/css/style.css", "{$c['mobile']['tpl_dir']}index/{$c['mobile']['HomeTpl']}/js/index.js");
?>
</head>
<body class="lang<?=$c['lang'];?>">
<?php
include("{$c['mobile']['theme_path']}inc/header.php");
include("{$c['mobile']['theme_path']}header/{$c['mobile']['HeaderTpl']}/header.php");
include("{$c['mobile']['theme_path']}index/{$c['mobile']['HomeTpl']}/template.php");
include("{$c['mobile']['theme_path']}footer/{$c['mobile']['FooterTpl']}/footer.php");
include("{$c['mobile']['theme_path']}inc/footer.php");
?>
</body>
</html>