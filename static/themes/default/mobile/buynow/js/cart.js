/*
 * Powered by ueeshop.com		http://www.ueeshop.com
 * 广州联雅网络科技有限公司		020-83226791
 */

(function($, _w){
	_w.cart_obj={
		cart_init:{
			paypal_create:{ "OId":"" },
			paypal_data:{"total":"0.00", "currency":"USD", "subtotal":"0.00", "tax":"0.00", "shipping":"0.00", "handling_fee":"0.00", "shipping_discount":"0.00", "insurance":"0.00"},
			paypal_result:{ "OId":"", "CUSTOM":"" },
			
			get_state_from_country:function(CId){
				//收货地址 国家和省份的显示
				$.ajax({
					url:"/account/",
					async:false,
					type:"POST",
					data:{"CId": CId, do_action:'user.select_country'},
					dataType:"json",
					success:function (data){
						if(data.ret==1){
							d=data.msg.contents;
							if(d==-1){
								$('#zoneId').css({'display':'none'}).find('select').attr('disabled', 'disabled').removeAttr('notnull');
								$('#state').css({'display':'block'}).find('input').removeAttr('disabled');
							}else{
								$('#zoneId').css({'display':'block'}).find('select').removeAttr('disabled').attr('notnull', '');
								$('#state').css({'display':'none'}).find('input').attr('disabled', 'disabled');
								str='';
								var vselect='';
								var vli='';
								for(i=0;i<d.length;i++){
									vselect+='<option value="'+d[i]['SId']+'">'+d[i]['States']+'</option>';
									vli+='<li class="group-option active-result">'+d[i]['States']+'</li>';
								}
								$('#zoneId select').html(vselect);
							}
							$('#countryCode').val('+'+data.msg.code);
							if(data.msg.cid==30){
								$('#taxCode').css({'display':'block'}).find('select, input').removeAttr('disabled');
								$('#taxCode').find('input').attr('notnull', 'notnull');
								$('#tariffCode').css({'display':'none'}).find('select, input').attr('disabled', 'disabled');
								$('#tariffCode').find('input').removeAttr('notnull');
							}else if(data.msg.cid==211){
								$('#tariffCode').css({'display':'block'}).find('select, input').removeAttr('disabled');
								$('#tariffCode').find('input').attr('notnull', 'notnull');
								$('#taxCode').css({'display':'none'}).find('select, input').attr('disabled', 'disabled');
								$('#taxCode').find('input').removeAttr('notnull');
							}else{
								$('#taxCode').css({'display':'none'}).find('select, input').attr('disabled', 'disabled');
								$('#tariffCode').css({'display':'none'}).find('select, input').attr('disabled', 'disabled');
								$('#taxCode, #tariffCode').find('input').removeAttr('notnull');
							}
							return true;
						}
					}
				});
			},
			
			set_default_address:function(AId, NotUser){
				$.ajax({
					url:"/",
					async:false,
					type:'post',
					data:{'do_action':'user.get_addressbook', 'AId':AId, 'NotUser':NotUser},
					dataType:'json',
					success:function(data){
						if(data.ret==1){
							$('input[name=edit_address_id]').val(data.msg.address.AId);
							$('input[name=FirstName]').val(data.msg.address.FirstName);
							$('input[name=LastName]').val(data.msg.address.LastName);
							$('input[name=AddressLine1]').val(data.msg.address.AddressLine1);
							$('input[name=AddressLine2]').val(data.msg.address.AddressLine2);
							$('input[name=City]').val(data.msg.address.City);
							
							$('select[name=country_id]').find('option[value='+data.msg.address.CId+']').eq(0).attr('selected', 'selected');
							cart_obj.cart_init.get_state_from_country(data.msg.address.CId);
							if(data.msg.address.CId==30 || data.msg.address.CId==211){
								$('select[name=tax_code_type]').find('option[value='+data.msg.address.CodeOption+']').attr('selected', 'selected');
								$('input[name=tax_code_value]').attr('maxlength', (data.msg.address.CodeOption==1?11:14)).val(data.msg.address.TaxCode);
							}
							
							if(data.msg.country.HasState==1){
								$('select[name=Province]').find('option[value='+data.msg.address.Province+']').attr('selected', 'selected');
							}else{
								$('input[name=State]').val(data.msg.address.State);
							}
							
							$('input[name=ZipCode]').val(data.msg.address.ZipCode);
							$('input[name=CountryCode]').val('+'+data.msg.address.CountryCode);
							$('input[name=PhoneNumber]').val(data.msg.address.PhoneNumber);
							
						}else if(data.ret==2){
							$('input[name=edit_address_id], input[name=FirstName], input[name=LastName], input[name=AddressLine1], input[name=AddressLine2], input[name=City], input[name=tax_code_value], input[name=State], input[name=ZipCode], input[name=CountryCode], input[name=PhoneNumber]').val('');
		
							$('select[name=country_id]').find('option[value='+data.msg.country.CId+']').eq(0).attr('selected', 'selected');
							cart_obj.cart_init.get_state_from_country(data.msg.country.CId);
						}else{
							$('html').tips_box(data.msg.error, 'error');
						}
						
						$('#ShipAddrFrom .input_box_txt').each(function(){
							if($.trim($(this).val())!=''){
								$(this).parent().addClass('filled');
							}else{
								$(this).parent().removeClass('filled');
							}
						});
					}
				});
			},
			
			checkout_no_login:function(json){
				$.post('/?do_action=cart.set_no_login_address', json?json:$('.ship_address_form').serialize(), function(data){
					if(data.ret==1){
						$('#PlaceOrderFrom').attr('nologin', data.msg.info);
						var Html='';
						Html+=	'<p class="clearfix"><strong>'+data.msg.v.FirstName+' '+data.msg.v.LastName+'</strong></p>';
						Html+=	'<p class="address_line">'+data.msg.v.AddressLine1+' '+(data.msg.v.AddressLine2?data.msg.v.AddressLine2+'':'')+'</p>';
						Html+=	'<p>'+data.msg.v.City+', '+(data.msg.v.StateName?data.msg.v.StateName:data.msg.v.State)+' '+data.msg.v.Country+' ('+data.msg.v.ZipCode+')</p>';
						Html+=	'<p>'+data.msg.v.CountryCode+' '+data.msg.v.PhoneNumber+'</p>';
						
						$('.address_default').html(Html).show(500);
						$('.address_button').show(500);
						// $('#ShipAddrFrom').slideUp(500);
						$('input[name=order_shipping_address_aid]').val(0);
						$('input[name=order_shipping_address_cid]').val(data.msg.v.CId);
						cart_obj.get_shipping_method_from_country(data.msg.v.CId);
					}
				}, 'json');
			},
			
			show_shipping_info:function(OvId){
				//下单页面 快递信息的显示
				var $Obj=$('.checkout_shipping .shipping[data-id='+OvId+']'),
					$shipObj=$Obj.find('.title .shipping_info'),
					$radioObj=$Obj.find('input:radio:checked'),
					$Type=($Obj.find('.shipping_insurance').is(':checked')?1:0),
					$Price=parseFloat($radioObj.attr('price')),
					$Insurance=parseFloat($radioObj.attr('insurance')),
					$sPrice=$Price+($Type==1?$Insurance:0);
				if($radioObj.length){ //快递信息存在
					$shipObj.find('.error').css('display', 'none');
					$shipObj.find('.name').text($radioObj.next('label').text());
					if($sPrice==0){
						$shipObj.find('.price').text(lang_obj.products.free_shipping).addClass('free_shipping');
					}else{
						$shipObj.find('.price').text(ueeshop_config.currency_symbols+$('html').currencyFormat($sPrice, ueeshop_config.currency)).removeClass('free_shipping');
					}
				}else{ //不存在
					$shipObj.find('.error').css('display', 'inline-block');
					$shipObj.find('.name').text('');
					$shipObj.find('.price').text('').removeClass('free_shipping');
				}
				
			},
			
			show_shipping_insurance:function(v){
				//下单页面 快递保险费的显示
				if(v==1) $('#ShippingInsuranceCombine').show().prev().hide();
				else $('#ShippingInsuranceCombine').hide().prev().show();
			},
			
			create_order:function($obj, $btnTxt){
				var $Result		= 1,
					Email		= $('input[name=Email]'),
					EmailVal	= $.trim(Email.val()),
					addrId		= $('input[name=order_shipping_address_aid]'),
					countryId	= $('input[name=order_shipping_address_cid]'),
					ShipId		= $('input[name=order_shipping_method_sid]'),
					PayId		= $('input[name=order_payment_method_pid]');
				
				if(Email.length){ //检查邮箱地址
					if(EmailVal=='' || (EmailVal && /^\w+[a-zA-Z0-9-.+_]+@[a-zA-Z0-9-.+_]+\.\w*$/.test(Email.val())==false)){
						Email.addClass('null').next('p.error').text(lang_obj.user.reg_error.EmailFormat).show();
						$('body, html').animate({scrollTop:Email.offset().top-20}, 500);
						$obj.removeClass('processing').text($btnTxt);
						$Result=0;
					}
				}
				
				if($Result==1 && $('#PlaceOrderFrom input[name=order_products_attribute_error]').val()==1){//检查是否存在错误产品
					$('body, html').animate({scrollTop:$('.cart_item_list').offset().top}, 500);
					$('html').tips_box(lang_obj.cart.attribute_error, 'error');
					$obj.removeClass('processing').text($btnTxt);
					$Result=0;
				}
				if($Result==0){ //检查收货地址 && (addrId.val()==-1 || countryId.val()==-1)
					$('body, html').animate({scrollTop:$('.checkout_address').offset().top}, 500);
					$('html').tips_box(lang_obj.cart.address_error, 'error');
					$obj.removeClass('processing').text($btnTxt);
					$Result=0;
				}
				
				if($Result==1 && PayId.val()==-1){ //检查运费方式
					$('body, html').animate({scrollTop:$('.checkout_payment').offset().top}, 500);
					$('html').tips_box(lang_obj.cart.payment_error, 'error');
					$obj.removeClass('processing').text($btnTxt);
					$Result=0;
				}
				
				return $Result;
			}
		},
		
		cart_list:function(){
			$('.qty_box .cut, .qty_box .add').on('tap', function(){
				var value	= $(this).hasClass('add')?1:-1,
					obj		= $(this).siblings('.qty').find('input'),
					qty		= Math.abs(parseInt(obj.val())),
					CId		= obj.attr('data-cid'),
					ProId	= obj.attr('data-proid'),
					start	= obj.attr('data-start'),
					s_qty	= $(obj).parent().parent().siblings('input[name="S_Qty[]"]').val();
				if(!qty || qty<0 || qty<start){
					$('html').tips_box(lang_obj.products.warning_number, 'error');
				}
				qty=qty?qty:1;
				qty+=value;
				qty=qty>0?qty:1;
				qty<start && (qty=start);
				if(s_qty==qty) return false;
				var query_string='&Qty='+qty+'&CId='+CId+'&ProId='+ProId;
				var cid_str='&CIdAry=';
				if($('.cart_list input[name=select]:checked').length){//部分已选
					cid_str+='0';
					$('.cart_list input[name=select]:checked').each(function(index, element){
						cid_str+=','+$(element).val();
					});
				}
				cart_obj.modify_cart_result(obj, query_string+cid_str, 1);
			});
			$('.qty_box .qty input').on('keyup paste', function(){
				p=/[^\d]/g;
				$(this).val($(this).val().replace(p, ''));
			}).on('blur', function(){
				var obj		= $(this),
					qty 	= Math.abs(parseInt(obj.val())),
					CId 	= obj.attr('data-cid'),
					ProId	= obj.attr('data-proid'),
					start	= obj.attr('data-start'),
					s_qty	= $(obj).parent().parent().siblings('input[name="S_Qty[]"]').val();
				if(!qty || qty<0 || qty<start){
					$('html').tips_box(lang_obj.products.warning_number, 'error');
				}
				qty=qty?qty:1;
				qty=qty>0?qty:1;
				qty<start && (qty=start);
				if(s_qty==qty) return false;
				var query_string='&Qty='+qty+'&CId='+CId+'&ProId='+ProId;
				var cid_str='&CIdAry=';
				if($('.cart_list input[name=select]:checked').length){//部分已选
					cid_str+='0';
					$('.cart_list input[name=select]:checked').each(function(index, element){
						cid_str+=','+$(element).val();
					});
				}
				cart_obj.modify_cart_result(obj, query_string+cid_str, 0);
			});
			$('.cart_list .item .del').on('tap', function(){ //购物车产品删除
				var url=$(this).attr('url');
				$('html').tips_box(lang_obj.cart.del_confirm, 'confirm', function(){
					$.get(url, function(data){
						if(data){
							window.location.reload();
						}
					});
				});
				return false;
			});
			$('.cart_list .check').on('tap', function(){ //购物车产品勾选
				if($(this).find('input[name=select]:checked').length){
					$(this).find('input[name=select]')[0].checked=false;
					$(this).find('.btn_checkbox').removeClass('current');
				}else{
					$(this).find('input[name=select]')[0].checked=true;
					$(this).find('.btn_checkbox').addClass('current');
				}
				cart_obj.select_cart_result();
				return false;
			});
			$('.cart_list input[name=select]').on('click', function(){ //购物车产品勾选
				$(this).parent('.check').click();
				return false;
			});
			$('.cart_list input[name=select]').each(function(){ //重新默认全部勾选
				if($(this).is(':checked')===false) $(this).get(0).checked='checked';
			});
			$('.cart_btn .checkout').on('tap', function(){ //Checkout
				/*if($('.cart_list .item.null').length){//检查是否存在错误产品
					$('html').tips_box(lang_obj.cart.attribute_error, 'error');
					return false;
				}*/
				var $this=$(this), Data=new Object;
				$('#cart .cart_list input[name=Remark\\[\\]]').each(function(){
					Data[$(this).attr('data-cid')]=$(this).val();
				});
				
				$this.addClass('processing').text(lang_obj.cart.processing_str+'...');
				var $checked_len=$('.cart_list input[name=select]:checked').length,
					$checkout_len=$('.cart_list input[name=select]').length,
					$query='?';
				if($checked_len){//部分已选
					if($checked_len!=$checkout_len){ //部分已选，不是全选
						var $CId='0';
						$('.cart_list input[name=select]:checked').each(function(index, element){
							$CId+='.'+$(element).val();
						});
						$query+='CId='+$CId;
					}
					$.post('/?do_action=cart.check_low_consumption&t='+Math.random(), '', function(data){ //最低消费金额判断
						if(data.ret==1){ //符合
							setTimeout(function(){
								$checkoutUrl='/cart/checkout.html'+$query;
								$this.removeClass('processing').text($this.attr('data-name'));
								if($(this).loginOrVisitors()){
									$.post('/?do_action=cart.checkout_submit&t='+Math.random(), Data, function(data){
										if(data.ret==1){
											window.location.href=$checkoutUrl;
											return false;
										};
									}, 'json');
								}else{
									window.top.location.href='/account/login.html?&jumpUrl='+decodeURIComponent($checkoutUrl);
									//window.location.href='/account/';
								}
							}, 500);
						}else{ //不符合
							var tips=(lang_obj.cart.consumption).replace('%low_price%', ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.low_price, ueeshop_config.currency)).replace('%difference%', ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.difference, ueeshop_config.currency));
							$('html').tips_box(tips, 'error');
							$this.removeClass('processing').text($this.attr('data-name'));
						}
					}, 'json');
				}else{
					$('html').tips_box(lang_obj.cart.checked_error, 'error');
					$this.removeClass('processing').text($this.attr('data-name'));
				}
				return false;
			});
			$('.cart_btn .paypal_checkout_button').on('tap', function(){ //Paypal快捷支付
				var $this=$(this);
				/*if($('.cart_list .item.null').length){ //检查是否存在错误产品
					$('html').tips_box(lang_obj.cart.attribute_error, 'error');
					return false;
				}*/
				$this.addClass('processing').text(lang_obj.cart.processing_str+'...');
				var $checked_len=$('.cart_list input[name=select]:checked').length,
					$checkout_len=$('.cart_list input[name=select]').length,
					$query='?';
				if($('.cart_list input[name=select]:checked').length){//部分已选
					if($checked_len!=$checkout_len){ //部分已选，不是全选
						var $CId='0';
						$('.cart_list input[name=select]:checked').each(function(index, element){
							$CId+='.'+$(element).val();
						});
						$query+='CId='+$CId;
					}
					$.post('/?do_action=cart.check_low_consumption&t='+Math.random(), {'CId':$CId}, function(data){ //最低消费金额判断
						if(data.ret==1){ //符合
							$quickUrl='/cart/quick.html'+$query;
							setTimeout(function(){
								$this.removeClass('processing').text('');
								if($(this).loginOrVisitors()){
									window.top.location.href=$quickUrl;
								}else{
									window.top.location.href='/account/login.html?&jumpUrl='+decodeURIComponent($quickUrl);
								}
							}, 500);
						}else{ //不符合
							var tips=(lang_obj.cart.consumption).replace('%low_price%', ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.low_price, ueeshop_config.currency)).replace('%difference%', ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.difference, ueeshop_config.currency));
							$('html').tips_box(tips, 'error');
							$this.removeClass('processing').text('');
						}
					}, 'json');
				}else{
					$('html').tips_box(lang_obj.cart.checked_error, 'error');
					$this.removeClass('processing').text('');
				}
				return false;
			});
		},
		
		//结算
		cart_checkout:function(){
			var CountryId=$('input[name=order_shipping_address_cid]').val()?$('input[name=order_shipping_address_cid]').val():$('form[name=paypal_excheckout]').find('option:selected').val();
			cart_obj.get_shipping_method_from_country(CountryId);
			var cart_price = cart_obj.cart_price_init();
			$('#ot_fee').text($('html').currencyFormat(cart_price.feePrice.toFixed(2), ueeshop_config.currency));
			$('#ot_total').text($('html').currencyFormat((cart_price.totalAmount+cart_price.free_Price).toFixed(2), ueeshop_config.currency));
			
			
			$('.checkout_address').delegate('.btn_address_more', 'click', function(){
				if($(this).attr('href')=='javascript:;'){
					if($('.ship_address_form input[name=typeAddr]').val()==0){ //会员状态
						var $AId=$(this).parents('.item').find('input').val();
						cart_obj.cart_init.set_default_address($AId);
					}else{ //非会员状态
						$('input[name=order_shipping_address_aid]').val(-1);
						cart_obj.cart_init.set_default_address(0, 1);
					}
					$('.address_default, .address_button').hide();
					$('#ShipAddrFrom').slideDown(500);
					$('#ShipAddrFrom input[notnull], #ShipAddrFrom select[notnull]').removeClass('null');
					$('#ShipAddrFrom p.error').hide();
					return false;
				}
			});
			
			if(address_perfect==0){ //有收货地址信息
				// $('#ShipAddrFrom').hide();
			}
			if(address_perfect==1){ //非会员 或者 缺失收货地址
				var $AId=0;
				$('input[name=order_shipping_address_aid]').val(-1);
				$('.address_default, .address_button').hide();
				$('#ShipAddrFrom').slideDown(500);
				$('#cancel_address').hide();
				address_perfect_aid>0 && ($AId=address_perfect_aid);
				cart_obj.cart_init.set_default_address($AId); //大于0就是缺失收货地址 等于0就是非会员
				var CId=$('#country').find('option:selected').val();
				cart_obj.get_shipping_method_from_country(CId);
			}
			
			$('#country').change(function(e){
				var CId = $(this).find(':selected').val();
				// user_obj.get_state_from_country(CId);
				cart_obj.cart_init.get_state_from_country(CId);
            });
            $('#country').change();
			
			//收货地址的编辑
			var address_rq_mark=true;
			$('.ship_address_form').submit(function(){ return false; });
			$('#save_address').on('click', function(){
				if(address_rq_mark && !$('#save_address').hasClass('disabled')){
					var $notnull=$('.ship_address_form input[notnull], .ship_address_form select[notnull]'),
						$TypeAddr=parseInt($('.ship_address_form input[name=typeAddr]').val())==1?1:0,
						$errorObj=new Object;
					$('#save_address').addClass('disabled');
					address_rq_mark=false;
					setTimeout(function(){
						var status=0;
						$notnull.each(function(){
							$errorObj=($(this).attr('name')=='PhoneNumber'?$(this).parent().parent().next('p.error'):$(this).parent().next('p.error'));
							if($.trim($(this).val())==''){
								$(this).addClass('null');
								$errorObj.text(lang_obj.user.address_tips.PleaseEnter.replace('%field%', $(this).attr('placeholder'))).show();
								status++;
								if(status==1){
									$('body,html').animate({scrollTop:$(this).offset().top-20}, 500);
								}
							}else{
								$(this).removeClass('null');
								$errorObj.hide();
							}
						});
						$('.ship_address_form input[format][notnull]').each(function(){
							$errorObj=$(this).parent().next('p.error');
							$format=$(this).attr('format').split('|');
							if($format[0]=='Length' && $.trim($(this).val()).length!=parseInt($format[1])){
								$(this).addClass('null');
								$errorObj.text(lang_obj.format.length.replace('%num%', $format[1])).show();
								status++;
								if(status==1){
									$('body,html').animate({scrollTop:$(this).offset().top-20}, 500);
								}
							}else{
								$(this).removeClass('null');
								$errorObj.hide();
							}
						});
						if(status){ //检查表单
							address_rq_mark=true;
							$('#save_address').removeClass('disabled');
							return false;
						}
						if($TypeAddr==1){
							cart_obj.cart_init.checkout_no_login();
							$('.address_default').show(500);
						}else{
							$.post('/account/', $('.ship_address_form').serialize()+'&do_action=user.addressbook_mod', function(data){
								if(data.ret==1){
									window.top.location.reload();
								}
							}, 'json');
						}
						address_rq_mark=true;
						$('#save_address').removeClass('disabled');
					}, 100);
				}
				return false;
			});
			
			//快递方式
			$('.checkout_shipping .shipping .title').click(function(){
				var $Obj=$(this).parent();
				if($Obj.hasClass('current')){ //隐藏
					$Obj.removeClass('current');
					$Obj.find('.list').slideUp();
				}else{ //展开
					$Obj.addClass('current');
					$(this).next('.list').slideDown();
				}
			});
			if($('.checkout_shipping .shipping').size()>0){ //有快递方式信息
				$('.checkout_shipping .shipping:eq(0) .title').click(); //默认点击第一个
			}
			
			//选择快递方式
			$('.shipping_method_list').delegate('li', 'click', function(){ 
				var obj=$(this).find('input:radio'),
					OvId=obj.parents('.list').parent().attr('data-id'),
					SId=obj.val(),
					type=obj.attr('ShippingType'),
					price=obj.attr('price'),
					insurance=obj.attr('insurance'),
					inputCId=$('#PlaceOrderFrom input[name=order_shipping_address_cid]'),
					inputSId=$('#PlaceOrderFrom input[name=order_shipping_method_sid]'),
					inputType=$('#PlaceOrderFrom input[name=order_shipping_method_type]'),
					inputPrice=$('#PlaceOrderFrom input[name=order_shipping_price]'),
					inputSId_ary={},inputType_ary={}, inputPrice_ary={};
				
				inputSId.val()!='[]' && (inputSId_ary=$.evalJSON(inputSId.val()));
				inputType.val()!='[]' && (inputType_ary=$.evalJSON(inputType.val()));
				inputPrice.val()!='[]' && (inputPrice_ary=$.evalJSON(inputPrice.val()));
				
				obj.parent().parent().siblings().removeClass('current').find('input').removeAttr('checked');
				obj.attr('checked', 'checked');
				$(this).addClass('current');
				cart_obj.set_shipping_method(OvId, SId, price, type, insurance);
			});
			
			//选择快递保险费
			$('.shipping_insurance').click(function(){
				var $This=$(this),
					$Type=$This.is(':checked')?1:0;
				
				//运费计算
				var inputPrice=$('#PlaceOrderFrom input[name=order_shipping_price]'),
					inputPrice_ary={},
					shipPrice=0;
				inputPrice.val()!='[]' && (inputPrice_ary=$.evalJSON(inputPrice.val()));
				for(k in inputPrice_ary){
					shipPrice+=parseFloat(inputPrice_ary[k]);
				}
				
				//保险费计算
				var OvId=$This.parents('.shipping').attr('data-id'),
					insurance=$Type==1?parseFloat($This.parents('.shipping').find('input:radio:checked').attr('insurance')):0;
					inputInsurance=$('#PlaceOrderFrom input[name=order_shipping_insurance]'),
					inputInsurance_ary={}, inputInsurancePrice_ary={},
					insurancePrice=0, insuranceShow=0;
				if(isNaN(insurance)) insurance=0;
				
				inputInsurance.val()!='[]' && (inputInsurance_ary=$.evalJSON(inputInsurance.val()));
				inputInsurance_ary['OvId_'+OvId]=$Type;
				inputInsurance.val($.toJSON(inputInsurance_ary));
				$('input[name=ShippingInsurance]').length && $('input[name=ShippingInsurance]').val($.toJSON(inputInsurance_ary));
				
				inputInsurance.attr('price')!='[]' && (inputInsurancePrice_ary=$.evalJSON(inputInsurance.attr('price')));
				inputInsurancePrice_ary['OvId_'+OvId]=insurance;
				inputInsurance.attr('price', $.toJSON(inputInsurancePrice_ary));
				$('input[name=ShippingInsurancePrice]').length && $('input[name=ShippingInsurancePrice]').val($.toJSON(inputInsurancePrice_ary));
				
				for(k in inputInsurance_ary){
					if(inputInsurance_ary[k]==1){
						insurancePrice+=parseFloat(inputInsurancePrice_ary[k]);
						insuranceShow=1;
					}
				};
				cart_obj.cart_init.show_shipping_insurance(insuranceShow);
				cart_obj.cart_init.show_shipping_info(OvId);
				
				//价格显示
				var cart_price=cart_obj.cart_price_init();
				$('#ot_fee').text($('html').currencyFormat(cart_price.feePrice.toFixed(2), ueeshop_config.currency));
				$('#shipping_charges span').text($('html').currencyFormat(cart_price.price.toFixed(2), ueeshop_config.currency));
				$('#shipping_and_insurance span').text($('html').currencyFormat(cart_price.shippingPrice.toFixed(2), ueeshop_config.currency));
				$('#ot_total').text($('html').currencyFormat((cart_price.totalAmount+cart_price.feePrice).toFixed(2), ueeshop_config.currency));
			});
			
			//付款方式
			$('.payment_row').on('click tap', function(e){
				e.stopPropagation(); //阻止JavaScript事件冒泡传递
				$(this).addClass('current').siblings().removeClass('current');
				$('#PlaceOrderFrom input[name=order_payment_method_pid]').val($(this).attr('pid'));
				
				var $Method=$(this).attr('method');
				//Paypal按钮 (仅限于新用户版本)
				if($('#paypal_button_container').length){
					if($Method=='Paypal'){ //显示
						$('#paypal_button_container').show();
						$('#cart_checkout').hide();
					}else{ //隐藏
						$('#paypal_button_container').hide();
						$('#cart_checkout').show();
					}
				}
				
				var fee=parseFloat($(this).find('.payment_contents').attr('fee'));
				var affix=parseFloat($(this).find('.payment_contents').attr('affix'));
				
				if(isNaN(fee)) fee=0;
				if(isNaN(affix)) affix=0;
				
				var amount=parseFloat($('#PlaceOrderFrom').attr('amountPrice'));	//产品总价
				var userPrice=parseFloat($('#PlaceOrderFrom').attr('userPrice'));	//会员优惠
				var discountPrice=parseFloat($('input[name=order_discount_price]').val());	//满额减价
				var cutprice=parseFloat($('input[name=order_coupon_code]').attr('cutprice'));	//折扣
				//var price=parseFloat($('input[name=order_shipping_price]').val());	//运费
				//var insurance=parseFloat($('input[name=order_shipping_insurance]').attr('price'));	//运费保险
				
				//运费计算
				var inputPrice=$('#PlaceOrderFrom input[name=order_shipping_price]'),
					inputPrice_ary={},
					shipPrice=0;
				inputPrice.val()!='[]' && (inputPrice_ary=$.evalJSON(inputPrice.val()));
				for(k in inputPrice_ary){
					shipPrice+=parseFloat(inputPrice_ary[k]);
				}
				//保险费计算
				var inputInsurance=$('#PlaceOrderFrom input[name=order_shipping_insurance]'),
					inputInsurance_ary={}, inputInsurancePrice_ary={},
					insurancePrice=0;
				inputInsurance.val()!='[]' && (inputInsurance_ary=$.evalJSON(inputInsurance.val()));
				inputInsurance.attr('price')!='[]' && (inputInsurancePrice_ary=$.evalJSON(inputInsurance.attr('price')));
				for(k in inputInsurance_ary){
					if(inputInsurance_ary[k]==1){
						insurancePrice+=parseFloat(inputInsurancePrice_ary[k]);
					}
				};
				
				var totalAmount=amount-userPrice+shipPrice+insurancePrice-cutprice-discountPrice;	//最终价格
				var feePrice=totalAmount*(fee/100)+affix;	//付款手续费
				if(feePrice<0) feePrice=0;
				
				$('#ot_fee').text($('html').currencyFormat(feePrice.toFixed(2), ueeshop_config.currency)).attr({'fee':fee, 'affix':affix});
				$('#ot_total').text($('html').currencyFormat((totalAmount*(1+fee/100)+affix).toFixed(2), ueeshop_config.currency));
				
				if(fee>0 || affix>0){
					$('#serviceCharge').show();
				}else{
					$('#serviceCharge').hide();
				}
            });
			if($('.checkout_payment .payment_row').size()>0){ //有付款方式信息
				$('.checkout_payment .payment_row:eq(0)').show().click(); //默认点击第一个
			}
			
			/******************************** 优惠券 Start ********************************/
			var coupon_ajax_mark=true,
				couponCode=$('input[name=order_coupon_code]').val();
			
			if(couponCode!='') ajax_get_coupon_info(couponCode);
			
			$('#coupon_apply').on('tap', function(){
				var code=$('input[name=couponCode]').val();
				if(code && coupon_ajax_mark){
					ajax_get_coupon_info(code);
				}else{
					$('input[name=couponCode]').addClass('null');
					setTimeout(function(){
						$('input[name=couponCode]').removeClass('null');
					}, 2000);
				}
			});
			
			$('#removeCoupon').on('tap', function (){
				$('#couponSavings, #code_valid').hide();
				$('.code_input').slideDown(200);
				$('#code_valid strong').text('');
				$('input[name=order_coupon_code]').val('').attr('cutprice', '0.00');
				$.post('/?do_action=cart.remove_coupon');
				var cart_price=cart_obj.cart_price_init();
				$('#ot_fee').text($('html').currencyFormat(cart_price.feePrice.toFixed(2), ueeshop_config.currency));
				$('#couponSavings .value span').text($('html').currencyFormat(cart_price.cutprice.toFixed(2), ueeshop_config.currency));
				$('#ot_total').text($('html').currencyFormat((cart_price.totalAmount+cart_price.feePrice).toFixed(2), ueeshop_config.currency));
				coupon_ajax_mark=true;
			});
			
			function ajax_get_coupon_info(code){
				coupon_ajax_mark=false;
				var price=parseFloat($('#PlaceOrderFrom').attr('amountPrice'));
				var userprice=parseFloat($('#PlaceOrderFrom').attr('userprice'));
				var order_discount_price=parseFloat($('input[name=order_discount_price]').val());
				var order_cid=$('input[name=CartCId]').val();
				$.post('/?do_action=cart.ajax_get_coupon_info', {coupon:code, price:price, order_discount_price:order_discount_price, userprice:userprice, order_cid:(order_cid?order_cid:'')}, function(data){
					if(data.msg.status==1){
						var cutprice=parseFloat(data.msg.cutprice)*ueeshop_config.currency_rate;
						$('input[name=couponCode]').val('');
						$('.code_input').hide(0);
						$('#couponSavings').slideDown(200);
						$('input[name=order_coupon_code]').val(data.msg.coupon).attr('cutprice', cutprice);
						var cart_price=cart_obj.cart_price_init();
						$('#couponSavings .value span').text(cart_price.cutprice.toFixed(2));
						$('#code_valid').slideDown(200);
						$('#code_valid strong').eq(0).text(data.msg.coupon);
						$('#code_valid strong').eq(1).text(ueeshop_config.currency + $('html').currencyFormat(cart_price.cutprice.toFixed(2), ueeshop_config.currency));
						$('#code_valid strong').eq(2).text(data.msg.end);
						$('#ot_fee').text($('html').currencyFormat(cart_price.feePrice.toFixed(2), ueeshop_config.currency));
						$('#ot_total').text($('html').currencyFormat((cart_price.totalAmount+cart_price.feePrice).toFixed(2), ueeshop_config.currency));
						coupon_ajax_mark=true;
					}else{
						$('html').tips_box((lang_obj.cart.coupon_tips_to).replace('%coupon%', code), 'error');
						coupon_ajax_mark=true;
					}
				}, 'json');
			}
			/******************************** 优惠券 End ********************************/
			
			/******************* Paypal Checkout Start *******************/
			$('#select_country select[name=CId]').on('change', function(){
				cart_obj.get_shipping_method_from_country($(this).val());//运费
			});
			
			//提交
			$('#paypal_checkout').on('click', function(){
				var obj=$('#PlaceOrderFrom');
				$(this).attr('disabled', 'disabled').blur();
				if(obj.find('input[name=order_products_attribute_error]').val()==1){//检查是否存在错误产品
					$('html').tips_box(lang_obj.cart.attribute_error, 'error');
					setTimeout(function(){window.location.href='/cart/';},2000);
					return false;
				}
				if(parseInt(obj.find('input[name=SId]').val())<1 && obj.find('input[name=ShippingMethodType]').val()==''){
					$('html').tips_box(lang_obj.cart.shipping_method_tips, 'error');
					$(this).removeAttr('disabled');
					return false;
				}
				//快捷支付统计
				analytics_click_statistics(1);//暂时统计为添加购物车事件
				obj.submit();
			});
			/******************* Paypal Checkout End *******************/
			
			//提交
			$('#cart_checkout').on('click touchstart', function(e){
				e.preventDefault();

				var $notnull=$('.ship_address_form input[notnull], .ship_address_form select[notnull]'),
					$TypeAddr=parseInt($('.ship_address_form input[name=typeAddr]').val())==1?1:0,
					$errorObj=new Object;
				$('#save_address').addClass('disabled');
				address_rq_mark=false;
				var status=0;
				$notnull.each(function(){
					$errorObj=($(this).attr('name')=='PhoneNumber'?$(this).parent().parent().next('p.error'):$(this).parent().next('p.error'));
					if($.trim($(this).val())==''){
						$(this).addClass('null');
						$errorObj.text(lang_obj.user.address_tips.PleaseEnter.replace('%field%', $(this).attr('placeholder'))).show();
						status++;
						if(status==1){
							$('body,html').animate({scrollTop:$(this).offset().top-20}, 500);
						}
					}else{
						$(this).removeClass('null');
						$errorObj.hide();
					}
				});
				$('.ship_address_form input[format][notnull]').each(function(){
					$errorObj=$(this).parent().next('p.error');
					$format=$(this).attr('format').split('|');
					if($format[0]=='Length' && $.trim($(this).val()).length!=parseInt($format[1])){
						$(this).addClass('null');
						$errorObj.text(lang_obj.format.length.replace('%num%', $format[1])).show();
						status++;
						if(status==1){
							$('body,html').animate({scrollTop:$(this).offset().top-20}, 500);
						}
					}else{
						$(this).removeClass('null');
						$errorObj.hide();
					}
				});
				if(status){ //检查表单
					address_rq_mark=true;
					return false;
				}

				if ($(this).hasClass('processing') || address_rq_mark){
					return false;
				}
				var btnTxt=$(this).text();
				$(this).addClass('processing').text(lang_obj.cart.processing_str+'...');				
				//检查表单
				$result=cart_obj.cart_init.create_order($(this), btnTxt);
				if($result==0){
					return false;
				}				
				var EmailVal=$.trim($('input[name=Email]').val());
				var Attr='&';
				if($('.ship_address_form input[name=typeAddr]').val()==1){
					Attr+=$('.ship_address_form').serialize();
				}				
				var Remark='';
				if($('.itemFrom input[name=Remark\\[\\]]').length){
					$('.itemFrom input[name=Remark\\[\\]]').each(function(){
						Remark+='&Remark_'+$(this).attr('proid')+'_'+$(this).attr('cid')+'='+$(this).val();
					});
				}
				
				$.post('/?do_action=buynow.buynow_placeorder', $('#PlaceOrderFrom').serialize()+Attr, function(data){
					if(data.ret==1){//成功
						window.location.href='/order_complete.html';
					}else if(data.ret==-1){//地址错误
						$('html').tips_box(lang_obj.cart.address_error, 'error');
					}else if(data.ret==-2){//送货方式错误
						$('html').tips_box(lang_obj.cart.shipping_error, 'error');
					}else if(data.ret==-3){//支付方式错误
						$('html').tips_box(lang_obj.cart.payment_error, 'error');
						$('#paymentObj .box_select').css('border', '.0625rem #900 solid');
						$('body, html').animate({scrollTop:$('#paymentObj').offset().top}, 500);
						setTimeout(function(){ //3秒后自动清除
							$('#paymentObj .box_select').removeAttr('style');
						}, 3000);
					}else if(data.ret==-4){
						$('html').tips_box(lang_obj.cart.product_error, 'error');
					}else if(data.ret==-5){
						$('html').tips_box(lang_obj.cart.low_error+': '+data.msg, 'error');
					}else if(data.ret==-6){
						var arr=data.msg.split(',');
						for(i in arr){
							if(!$('.cart_item_list .item[cid='+arr[i]+'] .stock_error').length){
								$('.cart_item_list .item[cid='+arr[i]+'] .cart_attr_list').append('<p class="error stock_error">'+lang_obj.cart.prod_stock_error+'</p>');
							}
						}
						$('html').tips_box(lang_obj.cart.stock_error, 'error');
					}
					$('#cart_checkout').removeClass('processing').text(btnTxt);
				}, 'json');
			});
		},
		
		paypal_init:function(IsCreditCard){
			$("#paypal_button_container").loading();
			$(".loading_msg").css("top", -18);
			$.getScript("//www.paypalobjects.com/api/checkout.js", function(){
				$("#paypal_button_container").unloading();
				paypal.Button.render({
					env: 'sandbox',//sandbox | production
					commit: true,
					style: { layout:(IsCreditCard?'vertical':'horizontal'), size:'responsive', shape:'rect' },
					payment: function(){
						if($('.success_container').length){//支付失败页面
							var $OId=$('input[name=OId]').val(); 
							var CREATE_URL='/?do_action=cart.paypal_payment_create_log&OId='+$OId;
							return paypal.request.post(CREATE_URL).then(function(data){
								return data.id;
							});
						}else{//下单页面
							if($('#PlaceOrderFrom input[name=order_products_attribute_error]').val()==1){//检查是否存在错误产品
								$('html').tips_box(lang_obj.cart.attribute_error, 'error');
								$(this).removeClass('processing').text(btnTxt);
								setTimeout(function(){window.location.href='/cart/';},2000);
								return false;
							}
							var Attr='';
							if($('#PlaceOrderFrom').attr('nologin')){
								Attr=$('#PlaceOrderFrom').attr('nologin');
							}
							if($('#address_from:visible').length){
								$('html').tips_box('Please save your shipping address data, then your order submission!', 'error');
								$(this).removeClass('processing').text(btnTxt);
								return false;
							}
							var CREATE_URL='/?do_action=cart.paypal_payment_create_log',
								CREATE_DATA={"data":$('#PlaceOrderFrom').serialize()+Attr};
							return paypal.request.post(CREATE_URL, CREATE_DATA).then(function(data){
								if(data.ret){
									if(data.ret==-1){//地址错误
										$('html').tips_box(lang_obj.cart.address_error, 'error');
									}else if(data.ret==-2){//送货方式错误
										$('html').tips_box(lang_obj.cart.shipping_error, 'error');
									}else if(data.ret==-3){//支付方式错误
										$('html').tips_box(lang_obj.cart.payment_error, 'error');
										$('#paymentObj .box_select').css('border', '.0625rem #900 solid');
										$('body, html').animate({scrollTop:$('#paymentObj').offset().top}, 500);
										setTimeout(function(){ //3秒后自动清除
											$('#paymentObj .box_select').removeAttr('style');
										}, 3000);
									}else if(data.ret==-4){
										$('html').tips_box(lang_obj.cart.product_error, 'error');
									}else if(data.ret==-5){
										$('html').tips_box(lang_obj.cart.low_error+': '+data.msg, 'error');
									}else if(data.ret==-6){
										var arr=data.msg.split(',');
										for(i in arr){
											if(!$('.cart_item_list .item[cid='+arr[i]+'] .stock_error').length){
												$('.cart_item_list .item[cid='+arr[i]+'] .cart_attr_list').append('<p class="error stock_error">'+lang_obj.cart.prod_stock_error+'</p>');
											}
										}
										$('html').tips_box(lang_obj.cart.stock_error, 'error');
									}
									return false;
								}else{
									//Place an Order 生成订单 统计
									analytics_click_statistics(5);
									parseInt(ueeshop_config.FbPixelOpen)==1 && $('html').fbq_checkout();
									cart_obj.cart_init.paypal_result.OId=data.OId;
									return data.id;
								}
							});
						}
					},
					onAuthorize: function(data, actions) {
						var EXECUTE_URL='/?do_action=cart.paypal_payment_execute_log&OId='+cart_obj.cart_init.paypal_result.OId;
						var data={
							paymentID: data.paymentID,
							payerID: data.payerID
						};
						return paypal.request.post(EXECUTE_URL, data).then(function(res){
							window.top.location='/cart/success/'+res.OId+'.html';
						});
					}
				}, '#paypal_button_container');
			});
		},
		
		paypal_checkout_init:function(IsCreditCard){
			cart_obj.cart_init.paypal_data.currency=ueeshop_config.currency;
			$("#paypal_payment_container").loading();
			$(".loading_msg").css("top", -18);
			$.getScript("//www.paypalobjects.com/api/checkout.js", function(){
				$("#paypal_payment_container").unloading();
				paypal.Button.render({
					env: 'sandbox',//sandbox | production
					commit: true,
					style: { layout:(IsCreditCard?'vertical':'horizontal'), size:'responsive', shape:'rect', color:'gold' },
					payment: function(){
						if($('#PlaceOrderFrom input[name=order_products_attribute_error]').val()==1){//检查是否存在错误产品
							$('html').tips_box(lang_obj.cart.attribute_error, 'error');
							$(this).removeClass('processing').text(btnTxt);
							setTimeout(function(){window.location.href='/cart/';},2000);
							return false;
						}
						var Attr='';
						if($('#PlaceOrderFrom').attr('nologin')){
							Attr=$('#PlaceOrderFrom').attr('nologin');
						}
						if($('#address_from:visible').length){
							$('html').tips_box('Please save your shipping address data, then your order submission!', 'error');
							$(this).removeClass('processing').text(btnTxt);
							return false;
						}
						$.post('/?do_action=cart.paypal_checkout_payment_log&'+$('#PlaceOrderFrom').serialize()+Attr, '', function(result){ //提交前的数据记录
							if(result.ret==1){
								cart_obj.cart_init.paypal_result.OId=result.msg.OId;
								cart_obj.cart_init.paypal_result.CUSTOM=result.msg.CUSTOM;
							}
						}, 'json');
						var CREATE_URL='/?do_action=cart.paypal_checkout_create_log';
						return paypal.request.post(CREATE_URL, cart_obj.cart_init.paypal_data).then(function(data){
							return data.id;
						});
					},
					onAuthorize: function(data, actions) {
						var EXECUTE_URL='/?do_action=cart.paypal_checkout_complete_log';
						var data={
							paymentID: data.paymentID,
							payerID: data.payerID,
							OId: cart_obj.cart_init.paypal_result.OId,
							CUSTOM: cart_obj.cart_init.paypal_result.CUSTOM
						};
						return paypal.request.post(EXECUTE_URL, data).then(function(res){
							if(res.ret==1){
								window.top.location='/cart/success/'+res.OId+'.html';
							}else{
								alert(res.msg);
							}
						});
					}
				}, '#paypal_payment_container');
			});
		},
		
		modify_cart_result:function(obj, query_string, is_tips){
			if(cart_obj.update_cart_mark){
				cart_obj.update_cart_mark=false;
				$.post('/?do_action=cart.modify&t='+Math.random(), query_string, function(data){
					if(data.ret==1){
						cart_obj.update_cart_mark=true;
						//var price=$(obj).parents('.item').find('.price');
						if(is_tips && data.msg.qty==obj.val()){ //提示
							$('html').tips_box(lang_obj.products.warning_number, 'error');
						}
						obj.val(data.msg.qty);
						obj.parent().parent().siblings('input[name="S_Qty[]"]').val(data.msg.qty);
						//price.html(ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.price, ueeshop_config.currency));
						for(k in data.msg.price){
							$(".cart_list .item[cid="+k+"] .price").html(ueeshop_config.currency_symbols+$('html').currencyFormat(data.msg.price[k], ueeshop_config.currency));
						}
						
						var userRatio=parseInt($('.cart_total .savings').attr('userRatio')); //会员优惠折扣比率
						var userPrice=parseFloat(data.msg.total_price)-(parseFloat(data.msg.total_price)*(userRatio/100));
						var discountPrice=parseFloat(data.msg.cutprice); //满额减价
						var cutprice=0;
						userPrice=parseFloat(userPrice);
						if(userPrice || discountPrice){
							if(discountPrice>userPrice) cutprice=discountPrice;
							else cutprice=userPrice;
						}
						$('.cutprice_p').text('-'+ueeshop_config.currency_symbols+$('html').currencyFormat(cutprice, ueeshop_config.currency));
						if(cutprice){ //控制全场满减的显示
							$('.cart_total .savings').show();
						}else{
							$('.cart_total .savings').hide();
						}
						var punit=data.msg.total_count>1?'itemsCount':'itemCount';
						//$('.cart_total .total b').text(data.msg.total_count);
						$('.cart_total .total strong').html('('+lang_obj.cart[punit].replace('%num%', '<b>'+data.msg.total_count+'</b>')+')');
						$('.cart_total .total .p').html(ueeshop_config.currency_symbols+$('html').currencyFormat(parseFloat(data.msg.total_price)-cutprice, ueeshop_config.currency));
					}
				}, 'json');
			}
		},
		
		select_cart_result:function(){
			var $CId='0';
			$('.cart_list input[name=select]:checked').each(function(){
				$CId+=','+$(this).parents('.item').find("input[name=CId\\[\\]]").val();
			});
			$.post('/?do_action=cart.select&t='+Math.random(), 'CId='+$CId, function(data){
				if(data.ret==1){
					var total=parseFloat(data.msg.total_price);
					var userRatio=parseInt($('.cart_total .savings').attr('userRatio')); //会员优惠折扣比率
					var userPrice=total-(total*(userRatio/100));
					var discountPrice=parseFloat(data.msg.cutprice);
					var cutprice=0;
					if(userPrice && discountPrice){
						if(discountPrice>userPrice) cutprice=discountPrice;
						else cutprice=userPrice;
					}
					$('.cutprice_p').text('-'+ueeshop_config.currency_symbols+$('html').currencyFormat(cutprice, ueeshop_config.currency));
					if(cutprice){ //控制全场满减的显示
						$('.cart_total .savings').show();
					}else{
						$('.cart_total .savings').hide();
					}
					$('.cart_total .total b').text(data.msg.total_count);
					$('.cart_total .total .p').html(ueeshop_config.currency_symbols+$('html').currencyFormat(total-cutprice, ueeshop_config.currency));
				}
			}, 'json');
		},
		
		update_cart_mark:true,
		
		cart_price_init:function(){//返回价格
			//运费计算
			var inputPrice=$('#PlaceOrderFrom input[name=order_shipping_price]'),
				inputPrice_ary={},
				shipPrice=0;
			inputPrice.val()!='[]' && (inputPrice_ary=$.evalJSON(inputPrice.val()));
			for(k in inputPrice_ary){
				shipPrice+=parseFloat(inputPrice_ary[k]);
			}
			//保险费计算
			var inputInsurance=$('#PlaceOrderFrom input[name=order_shipping_insurance]'),
				inputInsurance_ary={}, inputInsurancePrice_ary={},
				insurancePrice=0;
			inputInsurance.val()!='[]' && (inputInsurance_ary=$.evalJSON(inputInsurance.val()));
			inputInsurance.attr('price')!='[]' && (inputInsurancePrice_ary=$.evalJSON(inputInsurance.attr('price')));
			for(k in inputInsurance_ary){
				if(inputInsurance_ary[k]==1){
					insurancePrice+=parseFloat(inputInsurancePrice_ary[k]);
				}
			};
			//总价计算
			var amount=parseFloat($('#PlaceOrderFrom').attr('amountPrice')); //产品总价
			var userPrice=parseFloat($('#PlaceOrderFrom').attr('userPrice')); //会员优惠
			var discountPrice=parseFloat($('input[name=order_discount_price]').val()); //满额减价
			var cutprice=parseFloat($('input[name=order_coupon_code]').attr('cutprice')); //折扣
			var shippingPrice=shipPrice+insurancePrice;	//运费+保险费
			var fee=parseFloat($('#ot_fee').attr('fee'));
			var affix=parseFloat($('#ot_fee').attr('affix'));
			if(isNaN(fee)) fee=0;
			if(isNaN(affix)) affix=0;
			var totalAmount=amount-userPrice+shippingPrice-cutprice-discountPrice; //最终价格
			var feePrice=totalAmount*(fee/100)+affix; //付款手续费
			if(feePrice<0) feePrice=0;
			//记录快捷支付数据
			if($('#PlaceOrderFrom input[name=NewFunVersion]').val()>=4){ //新用户版本
				if(ueeshop_config.currency=='TWD' || ueeshop_config.currency=='JPY'){ //取整
					cart_obj.cart_init.paypal_data.total=parseInt((totalAmount+feePrice));
					cart_obj.cart_init.paypal_data.subtotal=parseInt(amount);
					cart_obj.cart_init.paypal_data.insurance=parseInt(insurancePrice);
					cart_obj.cart_init.paypal_data.shipping=parseInt(shipPrice);
					cart_obj.cart_init.paypal_data.shipping_discount='-'+parseInt((userPrice+discountPrice+cutprice));
					cart_obj.cart_init.paypal_data.handling_fee=parseInt(feePrice);
					cart_obj.cart_init.paypal_data.tax=0;
				}else{
					cart_obj.cart_init.paypal_data.total=(totalAmount+feePrice).toFixed(2);
					cart_obj.cart_init.paypal_data.subtotal=amount.toFixed(2);
					cart_obj.cart_init.paypal_data.insurance=insurancePrice.toFixed(2);
					cart_obj.cart_init.paypal_data.shipping=shipPrice.toFixed(2);
					cart_obj.cart_init.paypal_data.shipping_discount='-'+(userPrice+discountPrice+cutprice).toFixed(2);
					cart_obj.cart_init.paypal_data.handling_fee=feePrice.toFixed(2);
				}
			}
			//返回数据
			return {totalAmount:totalAmount, amount:amount, userPrice:userPrice, discountPrice:discountPrice, cutprice:cutprice, price:shipPrice, insurance:insurancePrice, shippingPrice:shippingPrice, feePrice:feePrice};
		},
		
		complete:function(){ //线下付款
			var pay_form	= $('#pay_form'),
				rq_mark		= true,
				notnull		= $('*[notnull]', pay_form);
			$('#paybtn').on('tap', function(){
				if(rq_mark){
					notnull.removeClass('null');
					setTimeout(function(){
						var status=0;
						notnull.each(function(index, element){
							if($(element).val()==''){
								$(element).addClass('null');
								status=1;
							}else{
								$(element).removeClass('null');
							}
						});
						
						var reg={'Length':/^.*/};
						var tips={'Length':lang_obj.format.length};
						pay_form.find('*[format]').each(function(){
							var o=$(this);
							var s=o.attr('format').split('|');
							if((s[0]=='Length' && $.trim(o.val()).length!=parseInt(s[1])) || (s[0]!='Length' && reg[s[0]].test($.trim(o.val()))===false)){
								$('html').tips_box(tips[s[0]].replace('%num%', s[1]), 'error');
								o.addClass('null');
								status=1;
							}else{
								o.removeClass('null');
							}
						});
						
						if(status){
							return false;
						}
						//通过验证，提交数据
						rq_mark=false;
						$.post('/?do_action=cart.offline_payment', pay_form.serialize(), function(data){
							if(data.ret==1){
								window.location.href=window.location.href;
							}else{
								rq_mark=true;
							}
						}, 'json');
					}, 10);
				}
			});
		},
		
		get_shipping_method_from_country:function(CId){	//选择快递方式
			if(!CId){
				cart_obj.set_shipping_method(1, -1, 0, '', 0);
				return false;
			}
			var dataVal="CId="+CId;
			
			if($('input[name=order_cid]').val()) dataVal+='&order_cid='+$('input[name=order_cid]').val();
			//if($('input[name=CartCId]').val()) dataVal+='&order_cid='+$('input[name=CartCId]').val();
			
			$('#submitCart').hide();
			
			$.post('/?do_action=cart.get_shipping_methods', dataVal, function(data){
				if(data.ret==1){
					var rowObj, rowStr, j=0;
					for(OvId in data.msg.info){
						if(!$('.checkout_shipping .shipping[data-id='+OvId+']').length) continue;//没有这个海外仓选项
						rowStr='';
						j=1;
						for(i=0; i<data.msg.info[OvId].length; i++){
							rowObj=data.msg.info[OvId][i];
							if(parseFloat(rowObj.ShippingPrice)<0) continue;
							rowStr+='<li name="'+rowObj.Name.toUpperCase()+'"'+(++j%2==0?' class="odd"':'')+'>';
							rowStr+=	'<span class="name">';
							rowStr+=		'<input type="radio" name="_shipping_method['+OvId+']" value="'+rowObj.SId+'" price="'+ rowObj.ShippingPrice+'" insurance="'+rowObj.InsurancePrice+'" ShippingType="'+rowObj.type+'" cid="'+CId+'" />';
							/*if(rowObj.Logo){
								rowStr+='<img src="'+rowObj.Logo+'" alt="'+rowObj.Name+'" />';
							}*/
							rowStr+=		'<label>'+rowObj.Name+'</label>';
							
							if(rowObj.IsAPI>0){
								rowStr+='<span class="price waiting"></span>';
							}else{
								if(rowObj.ShippingPrice>0){
									rowStr+='<span class="price">'+ueeshop_config.currency_symbols+$('html').currencyFormat(rowObj.ShippingPrice, ueeshop_config.currency)+'</span>';
								}else{
									rowStr+='<span class="price free_shipping">'+lang_obj.products.free_shipping+'</span>';
								}
							}
							
							rowStr+=	'</span>';
							rowStr+=	'<span class="brief" title="'+rowObj.Brief+'">'+rowObj.Brief+'</span>';
							rowStr+=	'<div class="clear"></div>';
							rowStr+='</li>';
							
							if(rowObj.IsAPI>0){ //使用API接口
								var $AId=$('input[name=order_shipping_address_aid]').val(),
									$CId=$('input[name=order_shipping_address_cid]').val();
								$('.checkout_shipping .shipping[data-id='+OvId+'] .shipping_method_list li[name="'+rowObj.Name.toUpperCase()+'"] input').attr('disabled', true);
								$.post('/?do_action=cart.ajax_get_api_info', 'OvId='+OvId+'&AId='+$AId+'&CId='+$CId+'&Name='+rowObj.Name.toUpperCase()+'&IsAPI='+rowObj.IsAPI+'&'+dataVal, function(data){
									var $apiObj=$('.checkout_shipping .shipping[data-id='+data.msg.OvId+'] .shipping_method_list li[name="'+data.msg.Name+'"]');
									if(data.ret==1){
										$apiObj.find('.price').removeClass('waiting').text(data.msg.Price>0 ? ueeshop_config.currency_symbols+$('html').currencyFormat(parseFloat(data.msg.Price)*ueeshop_config.currency_rate, ueeshop_config.currency) : lang_obj.products.free_shipping).parent().find('input').attr({'price':parseFloat(data.msg.Price)*ueeshop_config.currency_rate, 'disabled':false});
									}else{
										$Price=$apiObj.find('input').attr('price');
										$apiObj.find('.price').removeClass('waiting').text($Price>0 ? ueeshop_config.currency_symbols+$('html').currencyFormat($Price, ueeshop_config.currency) : lang_obj.products.free_shipping);
									}
									if($('input[name="order_shipping_api\\['+data.msg.IsAPI+'\\]"]').length){
										if(data.msg.Price>0) $('input[name="order_shipping_api\\['+data.msg.IsAPI+'\\]"]').val(data.msg.Price);
										else $('input[name="order_shipping_api\\['+data.msg.IsAPI+'\\]"]').remove();
									}else{
										data.msg.Price>0 && $('#PlaceOrderFrom').append('<input type="hidden" name="order_shipping_api['+data.msg.IsAPI+']" value="'+data.msg.Price+'" />');
									}
								}, 'json');
							}
						}
						$('.checkout_shipping .shipping[data-id='+OvId+'] .shipping_method_list').html(rowStr);
						if(rowStr==''){
							$('.checkout_shipping .shipping[data-id='+OvId+'] .shipping_method_list').html(''); //清空内容
							cart_obj.set_shipping_method(OvId, -1, 0, '', 0);
						}else{
							$('.checkout_shipping .shipping[data-id='+OvId+'] .shipping_method_list li:eq(0)').click(); //默认点击第一个选项
						}
					}
					$('.checkout_shipping .shipping').not('.hide').each(function(){ //检查海外仓的快递数据是否存在
						OvId=$(this).attr('data-id');
						if(!data.msg.info[OvId]){ //这个海外仓没有相应的快递数据
							$('.checkout_shipping .shipping[data-id='+OvId+'] .shipping_method_list').html(''); //清空内容
							cart_obj.set_shipping_method(OvId, -1, 0, '', 0);
						}
					});
				}else{
					$('.shipping_method_list').html(''); //清空内容
					cart_obj.set_shipping_method(1, -1, 0, '', 0);
				}
				$('#submitCart').show();
			}, 'json');
		},

		set_shipping_method:function(OvId, SId, price, type, insurance, express){ //选择运费
			if(SId==-1){
				$('#shippingObj .oversea[data-id="'+OvId+'"] .shipping_list').html('<span class="no_delivery">Sorry! No delivery!</span>');
			}
			
			//运费记录
			var inputExpress=$('input[name=ShippingExpress]'),
				inputSId=$('input[name=order_shipping_method_sid]'),
				inputType=$('input[name=order_shipping_method_type]'),
				inputPrice=$('input[name=order_shipping_price]'),
				inputInsurance=$('input[name=order_shipping_insurance]'),
				inputExpress_ary={}, inputSId_ary={}, inputType_ary={}, inputPrice_ary={}, inputInsurance_ary={}, inputInsurancePrice_ary={};
			
			inputSId.val()!='[]' && (inputSId_ary=$.evalJSON(inputSId.val()));
			inputSId_ary['OvId_'+OvId]=SId;
			inputSId.val($.toJSON(inputSId_ary));
			$('input[name=SId]').length && $('input[name=SId]').val($.toJSON(inputSId_ary));
	
			inputType.val()!='[]' && (inputType_ary=$.evalJSON(inputType.val()));
			inputType_ary['OvId_'+OvId]=type;
			inputType.val($.toJSON(inputType_ary));
			$('input[name=ShippingMethodType]').length && $('input[name=ShippingMethodType]').val($.toJSON(inputType_ary));
			
			inputPrice.val()!='[]' && (inputPrice_ary=$.evalJSON(inputPrice.val()));
			inputPrice_ary['OvId_'+OvId]=price;
			inputPrice.val($.toJSON(inputPrice_ary));
			$('input[name=ShippingPrice]').length && $('input[name=ShippingPrice]').val($.toJSON(inputPrice_ary));
			
			if(inputExpress.length){
				inputExpress.val()!='[]' && (inputExpress_ary=$.evalJSON(inputExpress.val()));
				inputExpress_ary['OvId_'+OvId]=express;
				inputExpress.val($.toJSON(inputExpress_ary));
			}
			
			var shipPice=0; //运费
			for(k in inputPrice_ary){
				shipPice+=parseFloat(inputPrice_ary[k]);
			}
			
			//保险费记录
			var obj=$('.checkout_shipping .shipping[data-id='+OvId+']'),
				v=obj.find('.shipping_insurance').is(':checked')?1:0;
			obj.find('.insurance .price em').text($('html').currencyFormat(insurance, ueeshop_config.currency));
			insurance=parseFloat(insurance);
			insurance=v==1?insurance:0;
			
			inputInsurance.val()!='[]' && (inputInsurance_ary=$.evalJSON(inputInsurance.val()));
			inputInsurance_ary['OvId_'+OvId]=v;
			inputInsurance.val($.toJSON(inputInsurance_ary));
			$('input[name=ShippingInsurance]').length && $('input[name=ShippingInsurance]').val($.toJSON(inputInsurance_ary));
			
			inputInsurance.attr('price')!='[]' && (inputInsurancePrice_ary=$.evalJSON(inputInsurance.attr('price')));
			inputInsurancePrice_ary['OvId_'+OvId]=insurance;
			inputInsurance.attr('price', $.toJSON(inputInsurancePrice_ary));
			$('input[name=ShippingInsurancePrice]').length && $('input[name=ShippingInsurancePrice]').val($.toJSON(inputInsurancePrice_ary));
			
			var insurancePrice=0, insuranceShow=0;
			for(k in inputInsurance_ary){
				if(inputInsurance_ary[k]==1){
					insurancePrice+=parseFloat(inputInsurancePrice_ary[k]);
					insuranceShow=1;
				}
			};
			cart_obj.cart_init.show_shipping_insurance(insuranceShow);
			cart_obj.cart_init.show_shipping_info(OvId);
			
			var cart_price=cart_obj.cart_price_init();
			
			$('#ot_fee').text($('html').currencyFormat(cart_price.feePrice.toFixed(2), ueeshop_config.currency));
			$('#shipping_charges span').text($('html').currencyFormat(cart_price.price.toFixed(2), ueeshop_config.currency));
			$('#shipping_and_insurance span').text($('html').currencyFormat(cart_price.shippingPrice.toFixed(2), ueeshop_config.currency));
			$('#ot_total').text($('html').currencyFormat((cart_price.totalAmount+cart_price.feePrice).toFixed(2), ueeshop_config.currency));
			cart_obj.show_shipping_price(v);
			
			var minPrice=maxPrice=0;
			totalAmount=parseFloat((cart_price.totalAmount-cart_price.feePrice).toFixed(2));
			$('.payment_row').each(function(){
				minPrice=parseFloat($(this).attr('min'));
				maxPrice=parseFloat($(this).attr('max'));
				if(maxPrice?(totalAmount>=minPrice && totalAmount<=maxPrice):(totalAmount>=minPrice)){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		},

		show_shipping_price:function(v){
			if(v){//有保险
				$('#shipping_charges').hide(0);
				$('#shipping_and_insurance').show(0);
			}else{
				$('#shipping_charges').show(0);
				$('#shipping_and_insurance').hide(0);
			}
		}
	};
})(jQuery, window);
