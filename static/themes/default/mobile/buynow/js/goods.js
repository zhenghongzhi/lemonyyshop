/*
 * 广州联雅网络
 */

jQuery(function($){
	
	$.fn.extend({
		//总价格整理
		get_price:function(count){
			var p=parseFloat($("#ItemPrice").val()),//目前单价
				old=parseFloat($("#ItemPrice").attr('old')),//目前市场价
				curP=parseFloat($("#ItemPrice").attr("initial")),//产品会员价
				isSales=parseFloat($("#ItemPrice").attr("sales")),//是否开启产品促销
				salesP=parseFloat($("#ItemPrice").attr("salesPrice")),//产品促销现金
				disCount=parseInt($("#ItemPrice").attr("discount")),//产品促销折扣
				secKill=parseInt($("#goods_form input[name=SId]").val()),//产品秒杀
				isTuan=parseInt($("input[name=TId]").val()),//产品团购
				attr_hide=$.evalJSON($("#attr_hide").val()),//属性
				attr_len=$("#attr_hide").val().split(",").length,
				ext_attr=$.evalJSON($("#ext_attr").val()),//扩展属性
				IsCombination=parseInt($("#IsCombination").val()),//是否开启规格组合
				IsAttrPrice=parseInt($("#IsOpenAttrPrice").val()),//是否开启属性价格功能
				price=0,
				wholesalePrice=0,
				wholesaleDiscount=0,
				_value=0,
				ary=new Array,
				i, s='';
			count=parseInt(count);
			if($(".wholesale_list").length) var wholesale_attr=$.evalJSON($(".wholesale_list").attr("data"));
			
			if(wholesale_attr){//加入批发价
				for(k in wholesale_attr){
					if(count<parseInt(k)){
						break;
					}else{
						wholesalePrice=p=parseFloat(wholesale_attr[k]);
						wholesaleDiscount=parseFloat($('.wholesale_list dd[data-num='+k+'] .wprice').attr('data-discount'));
					}
				}
				if(!wholesalePrice || curP<wholesalePrice) p=curP;
			}
			$("#ItemPrice").val(p.toFixed(2));
			
			$('.wholesale_list dd').each(function(){
				_value=$(this).find('.wprice').attr('data-price');
				$(this).find('.wprice').text(ueeshop_config.currency_symbols+parseFloat(_value).toFixed(2));
			});
			
			if(IsAttrPrice==1 && attr_len && ext_attr && ext_attr!='[]'){//加入属性价格
				if(IsCombination==1){//规格组合
					i=0;
					for(k in attr_hide){
						ary[i]=attr_hide[k];
						i+=1;
					}
					//ary.sort(function(a,b){ return a-b });
					ary.sort(function(a, b){
						if(a.indexOf('Ov:')!=-1){
							a=99999999;
						}
						if(b.indexOf('Ov:')!=-1){
							b=99999999;
						}
						return a - b;
					});
					s=ary.join('_');
					if(ext_attr[s] && attr_len==$('.attr_show').length){
						if(parseInt(ext_attr[s][4])){ //加价
							price=parseFloat(ext_attr[s][0]);
						}else{ //单价
							if(secKill==0 && isTuan==0){ //不能是秒杀产品 或者 团购产品
								p=parseFloat(ext_attr[s][0]);
								wholesaleDiscount && (p*=1-wholesaleDiscount);
								$('.prod_info_wholesale .pw_column').each(function(){
									_value=$(this).find('.pw_td:eq(1)').attr('data-discount');
									$discount=((1-_value)*100).toFixed(3);
									$discount=$discount.lastIndexOf('.')>0 && $discount.length-$discount.lastIndexOf('.')-1>1?$discount.substring(0, $discount.lastIndexOf('.')+2):parseFloat($discount).toFixed(1);
									$(this).find('.pw_td:eq(1)').text($discount+'% Off');
								});
							}
						}
					}
				}else{
					var ext_value='';
					for(k in attr_hide){//循环已勾选的属性参数
						ext_value=ext_attr[attr_hide[k]];
						if(ext_value) price+=parseFloat(ext_value[0]);//固定是加价
					}
				}
			}
			
			if(isSales && salesP && p>salesP){//批发价和促销现金相冲突的情况下，以最低价优先
				p=salesP;
			}
			
			cPrice=(p+price).toFixed(2);
			cOld=(old+price).toFixed(2);
			if(isSales && disCount){//促销折扣(会员价+属性价格)
				cPrice=cPrice*(disCount/100);
			}
			
			var cur_price=(cPrice*parseFloat(ueeshop_config.currency_rate)).toFixed(2),
				del_price=(cOld*parseFloat(ueeshop_config.currency_rate)).toFixed(2);
			if(!isNaN(cPrice)) $('.cur_price').html('<span>'+ueeshop_config.currency+' '+ueeshop_config.currency_symbols+'</span>'+$('html').currencyFormat(cur_price, ueeshop_config.currency));
			$('.prod_info_price .price_0 del').html(ueeshop_config.currency_symbols+$('html').currencyFormat(del_price, ueeshop_config.currency));
			if($('.price_0').length && $('.last_price .save_price').length){
				var dis=(cOld-cPrice)/cOld*100;
				if(dis>0){
					$('.last_price .save_price').show();
					$('.last_price .save_p').text(ueeshop_config.currency_symbols+$('html').currencyFormat(((cOld-cPrice)*parseFloat(ueeshop_config.currency_rate)).toFixed(2), ueeshop_config.currency));
					$('.last_price .save_style').text('('+parseInt((dis>0 && dis<1)?1:dis)+'% Off)');
				}else{
					$('.last_price .save_price').hide();
				}
			}
		},
		
		//购买数量增减
		set_amount:function(e){
			var t=this,
				n=t.find("#quantity");
				
			t.on("blur", "#quantity", function(){
				e=$.evalJSON($(".prod_info_qty").attr("data"));
				if(!e) e=$.extend({min:1,max:99999,count:1});
				var num=parseInt($(this).val(), 10);
				if(!/^\d+$/.test($(this).val())){
					//$('html').tips_box('Quantity entered must be a number!', 'error');
					$(this).val(e.count);
				}
				if(!/^[^\d]+$/.test($(this).val())){
					$(this).val(num).focus();
				}
				if($(this).val()==""){
					return e.count;
				}else{
					var Max=parseInt($('#quantity').attr('data-stock'));
					if(isNaN(num) || e.min>num || num>Max || num>e.max){
						if(num<e.min){ //低过起订量
							e.count=e.min;
							e.count>Max && (e.count=Max); //防止 起订量 > 最大购买数量
							$('html').tips_box(lang_obj.products.warning_MOQ, 'error');
						}else if(num>Max){ //高过最大购买数量
							e.count=Max;
							$('html').tips_box(lang_obj.products.warning_Max.replace('%num%', Max), 'error');
						}else if(num>e.max){ //高过库存
							e.count=e.max;
							$('html').tips_box(lang_obj.products.warning_stock.replace('%num%', e.max), 'error');
						}else{ //不是数字
							$('html').tips_box(lang_obj.products.warning_number, 'error');
						}
					}else{
						e.count=num;
						return void 0;
					}
					n.val(e.count);
					t.get_price(e.count);
					return !1;
				}
			}).on("keyup", "#quantity", function(){
				t.get_price($(this).val());
			}).on("click", ".add, .cut", function(){
				e=$.evalJSON($(".prod_info_qty").attr("data"));
				if(!e) e=$.extend({min:1,max:99999,count:1});
				var num=parseInt(n.val(), 10);
				var value=$(this).hasClass('add')?1:-1;
				var Max=parseInt($('#quantity').attr('data-stock'));
				num = num?num:1;
				num += value;
				if(num<e.min){ //低过起订量
					num=e.min;
					num>Max && (num=Max); //防止 起订量 > 最大购买数量
					$('html').tips_box(lang_obj.products.warning_MOQ, 'error');
				}else if(num>Max){ //高过最大购买数量
					num=Max;
					$('html').tips_box(lang_obj.products.warning_Max.replace('%num%', Max), 'error');
				}else if(num>e.max){ //高过库存
					num=e.max;
					$('html').tips_box(lang_obj.products.warning_stock.replace('%num%', e.max), 'error');
				}
				n.val(num);
				t.get_price(num);
			});
		},
		
		//检查当前属性库存的情况
		check_stock:function(){
			var attr_len=$('.attr_show').length,
				ext_attr=$.evalJSON($("#ext_attr").val()),//扩展属性
				$attrStock=parseInt($("#attrStock").val()),
				tagName=this.get(0).tagName.toLowerCase();
			
			if(attr_len && $attrStock){ //开启了0是库存为空的设定
				var ext_ary=new Object, ary=new Object, cur, stock_ary=new Object;
				for(k in ext_attr){
					ary=k.split('_');
					for(k2 in ary){
						if(!stock_ary[ary[k2]]) stock_ary[ary[k2]]=0;
					}
					if(ext_attr[k][1]>0){
						for(k2 in ary){
							if(ary.length!=attr_len) continue;
							stock_ary[ary[k2]]+=1;
						}
					}
				}
				for(k in stock_ary){
					if(stock_ary[k]<1){
						if(this.find('span[value="'+k+'"]').length) this.find('span[value="'+k+'"]').addClass('out_stock out_stock_fixed');
					}else{
						if(this.find('span[value="'+k+'"]').length) this.find('span[value="'+k+'"]').removeClass('out_stock out_stock_fixed');
					}
				}
			}
		}
	});
	
	set_amount=$('.prod_info_qty').set_amount();
	
	//检查当前所有属性库存的情况
	if($('.attr_show').length && parseInt($('#IsCombination').val())==1){
		$('.attr_show').check_stock();
	}
	
	//属性事件
	var num, attr_id,
		attr_ary		= new Object,
		goods_form		= $('#goods_form'),
		attr_select		= $(".attr_select", goods_form),
		cart_tips		= $('#cart_tips'),
		cart_arrt_tips	= $('#cart_arrt_tips'),
		attr_hide		= $("#attr_hide"),
		attr_len		= $(".gr .select").length,
		ext_attr		= $.evalJSON($("#ext_attr").val()),//扩展属性
		$attrStock		= parseInt($("#attrStock").val());
	
	var o={
		goods_form: $('#goods_form'),
		prod_info_name: $('.prod_info_name'),
		attr_show: $('.attr_show'),
		prod_info_qty: $('.prod_info_qty'),
		quantity: $('#quantity'),
		prod_info_actions: $('.prod_info_actions'),
		cart_tips: $('#cart_tips'),
		cart_arrt_tips: $('#cart_arrt_tips'),
		attr_hide: $("#attr_hide"),
		ext_attr: $("#ext_attr"),
		attrStock: $("#attrStock"),
		is_combination: $('#IsCombination'),
		is_attr_price: $('#IsOpenAttrPrice'),
		is_default_selected: $('#IsDefaultSelected'),
	};
	
	var VId, attr_id, attr_ary=new Object,
		attr_len=o.attr_show.length,
		ext_attr=$.evalJSON(o.ext_attr.val()),//扩展属性
		$attrStock=parseInt(o.attrStock.val()),
		//$sku_box=$(".prod_info_sku"),//SKU显示
		$defaultStock=parseInt(o.quantity.attr('data-stock')),//产品默认库存
		$IsCombination=parseInt(o.is_combination.val());//是否开启规格组合
		$IsAttrPrice=parseInt(o.is_attr_price.val());//是否开启属性价格功能
		attrSelected=parseInt(o.is_default_selected.val());//默认选择
	
	o.attr_show.on("click ontouchstart", "span", function(e){//增加ipad触屏事件
		e.preventDefault();
		if($(this).hasClass("out_stock")){return false;}
		var $this=$(this),
			$obj=$this.parents(".attr_show").find("input");
		
		if($this.hasClass("selected")){//取消操作
			$this.removeClass("selected");
			VId='';
		}else{//勾选操作
			$this.parent().find('span').removeClass('form_select_tips');
			$this.addClass("selected").siblings().removeClass("selected");
			VId=$(this).attr("value");
		}
		$obj.val(VId);
		attr_id=$obj.attr("attr");
		if(attr_hide.val() && attr_hide.val()!='[]'){
			attr_ary=$.evalJSON(attr_hide.val());
		}
		if(VId){
			attr_ary[attr_id]=VId;
		}else{//选择默认选项，清除对应ID
			delete attr_ary[attr_id];
		}
		attr_hide.val($.toJSON(attr_ary));
		
		//库存显示
		var i=stock=0,
			ary=new Array,
			cur_attr='';
		for(k in attr_ary){
			ary[i]=attr_ary[k];
			++i;
		}
		//ary.sort(function(a,b){ return a-b });
		ary.sort(function(a, b){
			if(a.indexOf('Ov:')!=-1){
				a=99999999;
			}
			if(b.indexOf('Ov:')!=-1){
				b=99999999;
			}
			return a - b;
		});
		cur_attr=ary.join('_');
		if(cur_attr=='Ov:1'){
			stock=$defaultStock;
			$('#inventory_number').text(stock);
		}else if(cur_attr && ext_attr[cur_attr]){
			stock=(!$attrStock && ext_attr[cur_attr][1]<1)?$defaultStock:ext_attr[cur_attr][1];
			parseInt($('#goods_form input[name=SId]').val()) && parseInt($('#goods_form input[name=SId]').attr('stock'))<=stock && (stock=$('#goods_form input[name=SId]').attr('stock'));
			$('#inventory_number').text(stock);
		}else{
			//$('#inventory_number').text(0);//还原库存
			stock=$defaultStock;
			$('#inventory_number').text($defaultStock);
		}
		if($IsCombination==0 || $IsAttrPrice==0){//关闭规格组合 or 关闭属性价格功能，固定显示默认库存
			stock=$defaultStock;
			$('#inventory_number').text($defaultStock);
		}
		
		if($attrStock && $IsCombination==1 && $IsAttrPrice==1){
			if(attr_hide.val()=='[]' || attr_hide.val()=='{}'){//组合属性都属于默认选项
				$('.attr_show').check_stock(); //检查当前所有属性库存的情况
				$('#inventory_number').text($defaultStock);//还原库存
				stock=$defaultStock;
			}else if(ext_attr && ext_attr!='[]'){//判断组合属性库存状态
				var select_ary=new Array, i=-1, ext_ary=new Object, ary=new Object, cur, no_stock_ary=new Object;
				for(k in attr_ary){
					select_ary[++i]=attr_ary[k];
				}
				if(select_ary.length == attr_len-1){ //勾选数 比 属性总数 少一个
					var no_attrid=0, attrid=0, _select_ary, key;
					$('.attr_show').each(function(){
						attrid=$(this).find('.attr_value').attr('attr');
						if(!attr_ary[attrid]){
							no_attrid=attrid; //没有勾选的属性ID
						}
					});
					$('#attr_'+no_attrid).siblings('span').each(function(){
						value=$(this).attr('value');
						_select_ary=new Array;
						for(k in select_ary){
							_select_ary[k]=select_ary[k];
						}
						_select_ary[select_ary.length]=value;
						//_select_ary.sort(function(a, b){ return a - b });
						_select_ary.sort(function(a, b){
							if(a.indexOf('Ov:')!=-1){
								a=99999999;
							}
							if(b.indexOf('Ov:')!=-1){
								b=99999999;
							}
							return a - b;
						});
						key=_select_ary.join('_');
						if(ext_attr[key][1]==0){
							if($('span[value="'+value+'"]').length) $('span[value="'+value+'"]').addClass('out_stock');
						}else{
							if($('span[value="'+value+'"]').length) $('span[value="'+value+'"]').removeClass('out_stock');
						}
						if(VId==''){ //取消操作
							$('.attr_show').each(function(){
								if($(this).find('.attr_value').attr('attr')!=attr_id){
									$(this).find('span.out_stock').not('.out_stock_fixed').removeClass('out_stock');
								}
							});
						}
					});
				}else if(select_ary.length == attr_len && attr_len!=1){ //勾选数 跟 属性总数 一致
					for(k in ext_attr){
						ary=k.split('_');
						for(k2 in ary){
							if(!no_stock_ary[ary[k2]]) no_stock_ary[ary[k2]]=0;
						}
						cur=0;
						for(k2 in select_ary){
							if(global_obj.in_array(select_ary[k2], ary) && ary.length==attr_len){ //找出包含自身的关联项数据，不一致的属性数量数据也排除掉
								++cur;
							}
						}
						if(cur && cur>=(select_ary.length-1) && select_ary.length==attr_len){ //“数值里已有的选项数量”跟“已勾选的选项数量”一致
							if(ext_attr[k][1]==0){
								for(k2 in ary){
									if(global_obj.in_array(ary[k2], select_ary)) continue;
									if(!no_stock_ary[ary[k2]]){
										no_stock_ary[ary[k2]]=1;
									}else{
										no_stock_ary[ary[k2]]+=1;
									}
								}
							}
						}
					}
					for(k in no_stock_ary){
						if(!global_obj.in_array(k, select_ary) && no_stock_ary[k]>0){
							if($('span[value="'+k+'"]').length) $('span[value="'+k+'"]').addClass('out_stock');
						}else{
							if($('span[value="'+k+'"]').length) $('span[value="'+k+'"]').removeClass('out_stock');
						}
					}
				}else{ //勾选数 大于 1
					$('.attr_show').each(function(){
						$(this).find('span.out_stock').not('.out_stock_fixed').removeClass('out_stock');
					});
				}
			}
		}
		
		qty_data=$.evalJSON(o.prod_info_qty.attr("data"));
		if(parseFloat(qty_data.max)!=stock){//更新属性库存
			if(!stock || stock<1) stock=$defaultStock;
			qty_data.max=stock;
			o.prod_info_qty.attr("data", $.toJSON(qty_data));
		}
		var Max=parseInt($("#quantity").attr('data-stock'));
		if(parseInt(o.quantity.val())>Max){ //最大购买量
			$('html').tips_box(lang_obj.products.warning_MOQ.replace('%num%', Max), 'error');
			o.quantity.val(Max);
		}else if(parseInt(o.quantity.val())>parseInt(qty_data.max)){ //库存
			$('html').tips_box(lang_obj.products.warning_stock.replace('%num%', qty_data.max), 'error');
			o.quantity.val(qty_data.max);
		}
		o.quantity.get_price(o.quantity.val());
		
		if($(this).parent().find('.attr_value').hasClass('colorid')){//颜色图片属性
			$.ajax({
				url:"/?do_action=buynow.goods_detail_pic",
				async:false,
				type:'get',
				data:{"ProId":$("#ProId").val(), "ColorId":$(".colorid").length?$(".colorid").val():$(".attr_value").val()},
				dataType:'html',
				success:function(result){
					if(result){
						$(".detail_pic").html(result);
						goods_pic();
						//small_pic();
					}
				}
			});
		}
	});
	
	//发货地仅有“中国”一个，就自动默认执行
	if($('.attr_show').length){
		var obj=$('#attr_Overseas').parent().find("span").not(".out_stock").eq(0);
		if(!obj.hasClass('selected')){
			obj.click();
		}
	}
	
	//购物车属性以选择按钮显示，默认执行第一个选项
	if(attrSelected && $('.attr_show').length){
		$(".attr_show").each(function(){
			if($(this).find('input:hidden').attr('id')!='attr_Overseas'){
				$(this).find("span").not(".out_stock").eq(0).click();
			}
		});
	}
	
	$('.prod_info_line .cover_bg, .prod_info_line .cover_close').click(function(){
		var winTop=parseInt($('.wrapper').css('top'));
		$('.wrapper').removeAttr('style');
		$(window).scrollTop(winTop*-1);
		$('.prod_info_line').removeClass('show');
		setTimeout(function(){
			$('.prod_info_line').css('display', 'none');
		}, 300);
	});
	
	//加入购物车
	var addcart=true;
	$('#buynow_button').on('click', function(){
		if(addcart){
			var attr_null=0;
			o.goods_form.submit(function(){ return false; });
			o.attr_show.find(".attr_value").each(function(){
				if(!$(this).val()){
					attr_null++;
				}
			});
			if(attr_null){
				if($('.prod_info_line').hasClass('show')){
					$('html').tips_box(lang_obj.cart.plz_sel_para, 'error');
				}else{
					var winTop=parseInt($(window).scrollTop());
					$('.prod_info_line').show().addClass('show');
					$('.wrapper').css({'position':'fixed', 'top':(winTop*-1), 'left':0, 'right':0});
				}
				return;
			}
			addcart=false;
			$.post('/?do_action=buynow.addtocart', o.goods_form.serialize()+'&IsBuyNow=1&back=1', function(data){
				addcart=true;
				if(data.ret==1){
					//BuyNow统计
					analytics_click_statistics(1);//暂时统计为添加购物车事件
					parseInt(ueeshop_config.FbPixelOpen)==1 && $('html').fbq_addtocart(data.msg.item_price);
					$.post('/?do_action=buynow.check_low_consumption&t='+Math.random(), {'CId':data.msg.CId}, function(json){ //最低消费金额判断
						if(json.ret==1){ //符合
							window.top.location.href=data.msg.location;
						}else{ //不符合
							var tips=(lang_obj.cart.consumption).replace('%low_price%', ueeshop_config.currency_symbols+$('html').currencyFormat(json.msg.low_price, ueeshop_config.currency)).replace('%difference%', ueeshop_config.currency_symbols+$('html').currencyFormat(json.msg.difference, ueeshop_config.currency));
							$('html').tips_box(tips, 'error');
						}
					}, 'json');
				}
			}, 'json');
		}
	});
	
	
	$('html').on('click', '#div_mask, .btn_return', function(){ //提示窗关闭事件
		$('#tips_cart').hide();
		global_obj.div_mask(1);
	});
	
	$(document).scroll(function(){
		var $winTop			= $(window).scrollTop(),
			$actionsTop		= o.prod_info_actions.offset().top,
			$actionsHeight	= o.prod_info_actions.outerHeight();
		
		if($winTop>$actionsTop+$actionsHeight && $('#goods_cart_btn:hidden').length){
			$('#goods_cart_btn').slideDown();
		}
		if($winTop<$actionsTop+$actionsHeight && $('#goods_cart_btn:visible').length){
			$('#goods_cart_btn').slideUp();
		}
	});
	//加入购物车 结束
	
	//添加收藏夹
	$(".add_favorite").on('click', function(){
		var ProId=$(this).attr("data");
		$.get('/account/favorite/add'+ProId+'.html', function(data){
			if(data.ret==1 || data.ret==0){
				if(data.ret==1){
					$('html').tips_box(lang_obj.user.favorite_success, 'success');
					if(parseInt(ueeshop_config.FbPixelOpen)==1){
						//When a product is added to a wishlist.
						fbq('track', 'AddToWishlist', {content_ids:'['+data.msg.Num+']', content_name:data.msg.Name, currency:data.msg.Currency, value:'0.00'});
					}
				}else{
					$('html').tips_box(lang_obj.user.favorite_saved, 'success');
				}
			}else{
				window.top.location.href='/account/';
			}
		}, 'json');
	});
	
	//产品详细页折扣倒计时
	$(".discount_count").find(".discount_time").each(function(){
		var time=new Date();
		$(this).genTimer({
			beginTime: ueeshop_config.date,
			targetTime: $(this).attr("endTime"),
			callback: function(e){
				this.html(e)
			}
		});
	});
	$(".prod_info_seckill, .prod_info_tuan").find(".flashsale_time").each(function(){
		var time=new Date();
		$(this).genTimer({
			beginTime: ueeshop_config.date,
			targetTime: $(this).attr("endTime"),
			callback: function(e){
				this.html(e)
			}
		});
	});
	
	//分享
	$('.share_toolbox .share_s_btn').on('click', function(){
		var $obj=$('.share_toolbox');
		if(!$(this).hasClass('share_s_more')){
			$(this).shareThis($(this).attr('data'), $obj.attr('data-title'), $obj.attr('data-url'));
		}
	});
	
	$.ajax({
		url:"/?do_action=buynow.goods_detail_pic",
		async:false,
		type:'get',
		data:{"ProId":$("#ProId").val(), "ColorId":$(".colorid").length?$(".colorid").attr("attr")+''+$(".colorid").val():$(".attr_value").attr("attr")+''+$(".attr_value").val()},
		dataType:'html',
		success:function(result){
			if(result){
				$(".detail_pic").html(result);
			}
		}
	});
	
	//切换图片
	function goods_pic(){
		//切换图片
		var goods_pic	= $('.goods_pic'),
			olist		= $('.goods_pic ul'),
			oitem		= $('li', olist),
			pic			= $('img', oitem),
			oitemLen	= oitem.length,
			boxW		= $(window).width(),
			small_pic	= goods_pic.find('.trigger .item');
		if(boxW>750) boxW=750;
        olist.css({'width':boxW*oitemLen, 'display':'block'});
		oitem.css('width', boxW);
        
		if(oitemLen>1){
			//********** 拨动切换 **************
			var startX		= 0,
				endX		= 0,
				disX		= 0,//偏移量
				basicX		= boxW*0.15,//偏移量小于此值时还原
				i			= 0,//当前索引
				startML		= 0,
				str			= '',
				startPos	= {},
				MovePos		= {},
				isScrolling	= 0;
			
			function move(e){
				var touch=e.originalEvent.touches;
				goods_pic.css('background-image', 'none');
				if(touch.length>1 || e.originalEvent.scale && e.originalEvent.scale!==1) return;
				MovePos={x:touch[0].pageX-startPos.x, y:touch[0].pageY-startPos.y};
				isScrolling=Math.abs(MovePos.x)<Math.abs(MovePos.y)?1:0;
				if(isScrolling==0){ //左右
					e.preventDefault();
					disX=touch[0].pageX-startX;
					startML=-i*boxW;
					this.style.MozTransform=this.style.webkitTransform='translate3d('+(startML+disX)+'px,0,0)';
					this.style.msTransform=this.style.OTransform='translateX('+(startML+disX)+'px)';
				}else{ //上下
					olist.unbind("touchmove", move);
					olist.unbind("touchend", end);
				}
			}
			function end(e){
				var _x=Math.abs(disX);
				if(_x>=basicX){
					if(disX>0){//右移
						i--;
						if(i<0){
							i=0;
						}
					}else{//左移
						i++;
						if(i>=oitemLen){
							i=oitemLen-1;
						}
					}
				}
				small_pic.eq(i).addClass('FontBgColor').removeClass('off').siblings('.item').addClass('off').removeClass('FontBgColor');
				this.style.MozTransitionDuration=this.style.webkitTransitionDuration='0.3s';
				this.style.MozTransform=this.style.webkitTransform='translate3d('+ -(i*boxW) +'px,0,0)';
				this.style.msTransform=this.style.OTransform='translateX('+ -(i*boxW) +'px)';
				var img_h=$(this).find('li:eq('+i+') img').outerHeight();
				// $('.goods_pic, .goods_pic ul').css({'height':img_h});
				startX=disX=_x=0;
			}
			olist.get(0).ontouchstart=function(e){
				startX=e.touches[0].pageX;
				this.style.MozTransitionDuration=this.style.webkitTransitionDuration=0;
				startPos={x:e.touches[0].pageX, y:e.touches[0].pageY, time:+new Date};
				isScrolling=0;
				olist.bind("touchmove", move);
				olist.bind("touchend", end);
			};
			//*********** 拨动切换 结束 ***********
			
			$('.goods_pic .trigger .item').click(function(){
				i=$(this).index();
				$('.goods_pic .trigger .item').removeClass('FontBgColor').addClass('off');
				$(this).addClass('FontBgColor').removeClass('off');
				startML=-i*boxW;
				$('.goods_pic ul')[0].style.MozTransform=$('.goods_pic ul')[0].style.webkitTransform='translate3d('+(startML)+'px,0,0)';
				$('.goods_pic ul')[0].style.msTransform=$('.goods_pic ul')[0].style.OTransform='translateX('+(startML)+'px)';
			});
			
		}
	}
	(function(){
		goods_pic();
		var windowWidth='',
			picHeight='';
		if(window.innerWidth){
			windowWidth=window.innerWidth;
		}else if((document.body) && (document.body.clientWidth)){
			windowWidth=document.body.clientWidth;
		}
		if(windowWidth>414) windowWidth=414;
		$('.goods_pic, .goods_pic ul, .goods_pic ul li').css({'height':windowWidth});
		$('.goods_pic ul li:eq(0) img').load(function(){
			picHeight=$(this).outerHeight();
			picHeight=(picHeight<windowWidth?picHeight:windowWidth);
			$('.goods_pic, .goods_pic ul, .goods_pic ul li').css({'height':picHeight});
		});
		$(window).resize(function(){
			goods_pic();
		});
		$('.video_container').click(function(){
			$('.pro_video').addClass('play');
		});
		$('.pro_video,.pro_video .close').click(function(){
			$('.pro_video').removeClass('play');
		});
	})();
	
	$('.wrapper .detail_desc img').each(function(){
		$(this).removeAttr('width').removeAttr('height').css({'max-width':'', 'width':'auto', 'height':'auto'});
	});
	
	/************************* 批发价拖动 Start *************************/
	if($('.list_wholesale').length){
		var _w=$(window).width();
		function touch_wholesale(){
			var touch0=$('.list_wholesale'),
				list0=touch0.find('.wholesale_list'),
				item0=touch0.find('.item'),
				w0=0;
			item0.each(function(index, element){
				w0+=Math.ceil($(element).outerWidth(true));
			});
			list0.width(w0);
			touch_nav(list0, w0, _w);
		}
		touch_wholesale();
	}
	/************************* 批发价拖动 End *************************/
	
	/************************* 产品弹窗 Start *************************/
	var detail_sale_size=function(){
		var H=$(window).height(),
			o=$('#detail_sale_layer');
		$('#detail_sale_layer').css({'height':H});
		
		o.find('.sale_box').css({'height':(o.height()-o.find('.layer_head').outerHeight(true))});
	}
	$('#detail_sale').on('click', function(){
		var H=$(window).height();
		$('#detail_sale_layer').addClass('show').fadeIn(500).animate('', 500, function(){
			detail_sale_size();
			$(this).css({'position':'absolute'});
			$(document.body).css({'height':H, 'overflow':'hidden'});
			$('#header_fix, .crumb, .detail_pic, .goods_info, .detail_desc, footer, #goods_cart_btn').hide();
			$('#detail_sale_layer').bind('touchmove', function(e){//禁止拖动
				e.preventDefault();
			});
			$('#detail_sale_layer .promotion_body').bind('touchmove', function(e){//允许拖动
				e.stopPropagation();
			});
		});
	});
	$(window).resize(function(){
		detail_sale_size();
	});
	
	$('.prod_layer').on('click', '.layer_back', function(){
		var W=$(window).width(),
			$obj=$('.prod_layer');
		if($obj.hasClass('show')){//已开启
			$obj.removeClass('show').fadeOut(500).css({'position':'fixed', 'height':'100%'});
			$(document.body).css({'height':'auto', 'overflow':'auto'});
			$('#header_fix, .crumb, .detail_pic, .goods_info, .detail_desc, footer, #goods_cart_btn').show();
			$('body,html').scrollTop($('.detail_list').offset().top-200);
		}
	});
	/************************* 产品弹窗 End *************************/
	
	
	/************************* 产品选项卡 Start *************************/
	$('.detail_desc .t').click(function(){
		if($(this).parent().hasClass('detail_close')){
			$(this).parent().removeClass('detail_close').children('.text').show();
		}else{
			$(this).parent().addClass('detail_close').children('.text').hide();
		}
	});
	/************************* 产品选项卡 End *************************/


});