<?php !isset($c) && exit();?>
<?php
$cutArr=str::json_data(db::get_value('config', "GroupId='cart' and Variable='discount'", 'Value'), 'decode');
$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');//模板风格色调的数据
$style_data=str::json_data($StyleData, 'decode');
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="noindex,nofollow" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection" />
<?php
echo ly200::seo_meta();
include("{$c['mobile']['theme_path']}inc/resource.php");
echo ly200::load_static('/static/themes/default/mobile/buynow/js/cart.js');
?>
</head>

<body>
<?php include('inc/header.php');?>
<div class="wrapper">
	<?php
	//BuyNow数据
	if($_GET['Data']){
		$Data=array();
		$data_ary=explode('&', rawurldecode(base64_decode($_GET['Data'])));//Buy Now参数，已通过加密
		foreach($data_ary as $v){
			$arr=explode('=', $v);
			$Data[$arr[0]]=$arr[1];
		}
		$CId=(int)$Data['CId'];
	}
	
	//Checkout数据
	if($_GET['CId']){
		$CId=$_GET['CId'];
	}
	
	$CIdStr=$cCIdStr='';
	if($CId){
		$CIdStr=' and CId in('.str_replace('.', ',', $CId).')';
		$cCIdStr=' and c.CId in('.str_replace('.', ',', $CId).')';
	}
	
	//购物车产品
	$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', "c.{$c['where']['cart']}{$cCIdStr}", "c.*, c.Attr as CartAttr, p.Name{$c['lang']}, p.Prefix, p.Number, p.AttrId, p.Attr, p.IsCombination, p.IsOpenAttrPrice", 'c.CId desc');
	
	if(!$cart_row){
	?>
		<div class="cart_head ui_border_b"><a href="javascript:window.history.back(-1);" class="cart_head_back"><i></i></a><strong><?=$c['lang_pack']['mobile']['step_ary'][0];?></strong></div>
		<div id="cart">
			<div class="nocart"><?=$c['lang_pack']['mobile']['cart_empty'];?></div>
			<?php /*<div class="cart_btn"><a href="/" class="btn_global btn_continue"><?=$c['lang_pack']['mobile']['continue_shop'];?></a></div>*/?>
		</div>
	<?php
	}else{
		//产品总金额
		$total_price=db::get_sum('shopping_cart', $c['where']['cart'].$CIdStr, '(Price+PropertyPrice)*Discount/100*Qty');
		$iconv_total_price=cart::cart_total_price($cCIdStr, 1);
		//产品总重量（自身+包装）
		$total_weight=cart::cart_product_weight($cCIdStr, 2);
		
		//检查产品资料是否完整
		$oversea_id_ary=array();
		$products_attribute_error=0;
		foreach((array)$cart_row as $v){
			!in_array($v['OvId'], $oversea_id_ary) && $oversea_id_ary[]=$v['OvId'];
			if($v['BuyType']!=4 && $v['CartAttr']!='[]' && $v['CartAttr']!='{}' && $v['CartAttr']!='{"Overseas":"Ov:1"}'){
				if(!(int)$v['IsCombination'] && !(int)$v['IsOpenAttrPrice']) continue;//多规格模式和属性价格，都没有开启
				$IsError=0;
				$prod_selected_ary=$ext_ary=array();
				$AttrAry=@str::json_data(str::attr_decode($v['CartAttr']), 'decode');
				$prod_selected_row=db::get_all('products_selected_attribute', "ProId='{$v['ProId']}' and VId>0 and IsUsed=1");
				foreach((array)$prod_selected_row as $v2){
					$prod_selected_ary[]=$v2['VId'];
				}
				if((int)$v['IsCombination'] && db::get_row_count('products_selected_attribute_combination', "ProId='{$v['ProId']}'")){//开启规格组合
					$OvId=1;
					foreach($AttrAry as $k2=>$v2){
						if($k2=='Overseas'){//发货地
							$OvId=str_replace('Ov:', '', $v2);
							(int)$c['config']['global']['Overseas']==0 && $OvId!=1 && $OvId=0;//关闭海外仓功能，发货地不是China，不能购买
						}else{
							!in_array($v2, $prod_selected_ary) && $IsError=1;
							$ext_ary[]=$v2;
						}
					}
					sort($ext_ary); //从小到大排序
					$Combination='|'.implode('|', $ext_ary).'|';
					$row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='{$Combination}' and OvId='{$OvId}'"));
				}else{
					foreach((array)$AttrAry as $k2=>$v2){
						if($k2=='Overseas') continue;
						$row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='|{$v2}|' and OvId=1"));
						$row && $PropertyPrice+=(float)$row['Price'];//固定是加价
					}
					if($v['BuyType']==3 && !$AttrAry) $row=1;//放过组合购买
				}
				if(!$row || $IsError>0) $products_attribute_error=1;//检查此产品是否有选择购物车属性
			}
		}
		
		if((int)$_SESSION['User']['UserId']){ //会员收货地址信息
			//收货地址
			$address_row=str::str_code(db::get_all('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', 'a.'.$c['where']['cart']." and a.IsBillingAddress=0".($_SESSION['Cart']['ShippingAddressAId']?" and a.AId='{$_SESSION['Cart']['ShippingAddressAId']}'":''), 'a.*, c.Country, s.States as StateName', 'a.AccTime desc, a.AId desc'));
		}elseif($_SESSION['Cart']['ShippingAddress']){ //非会员收货地址信息
			$address_ary=$_SESSION['Cart']['ShippingAddress'];
			$country_val=str::str_code(db::get_value('country', "CId='{$address_ary['CId']}'", 'Country'));
			$states_val=str::str_code(db::get_value('country_states', "SId='{$address_ary['SId']}'", 'States'));
			if($country_val || $states_val){
				$address_ary['Country']=$country_val;
				$address_ary['StateName']=$states_val;
			}
			$address_row[0]=$address_ary;
			unset($address_ary);
		}
		//付款方式
		$payment_row=db::get_all('payment', 'Method="CashOnDelivery"', '*', $c['my_order'].'IsOnline desc,PId asc');
		
		$IsInsurance=str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
		
		$total_weight=$total_quantity=0;
		$total_quantity=db::get_sum('shopping_cart', $c['where']['cart'].($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 'Qty');
		
		//会员优惠价 与 全场满减价 比较
		$AfterPrice_0=$AfterPrice_1=0;
		$user_discount=0;
		if((int)$_SESSION['User']['UserId'] && (int)$_SESSION['User']['Level']){
			$user_discount=(float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
			$user_discount=($user_discount>0 && $user_discount<100)?$user_discount:100;
			$AfterPrice_0=$iconv_total_price-($iconv_total_price*($user_discount/100));
		}
		if($cutArr['IsUsed']==1 && $c['time']>=$cutArr['StartTime'] && $c['time']<=$cutArr['EndTime']){
			foreach((array)$cutArr['Data'] as $k=>$v){
				if($total_price<$k) break;
				$AfterPrice_1=($cutArr['Type']==1?cart::iconv_price($v[1], 2, '', 0):($iconv_total_price*(100-$v[0])/100));
			}
		}
		if($AfterPrice_0==$AfterPrice_1){//当会员优惠价和全场满减价一致，默认只保留会员优惠价
			$AfterPrice_1=0;
		}
		
		if((int)$c['config']['global']['Overseas']==0 || count($c['config']['Overseas'])<2 || count($oversea_id_ary)<2){//关闭海外仓功能 或者 仅有一个海外仓选项
		?>
			<style type="text/css">.checkout_shipping .shipping:first-child .title{display:none;}</style>
		<?php 
		}
		$oversea_id_hidden=$oversea_id_ary;
		$NotDefualtOvId=0;
		if(!in_array(1, $oversea_id_ary)){//购物车没有默认海外仓追加隐藏选项
			$oversea_id_ary[]=1;
			$NotDefualtOvId=1;
		}
		sort($oversea_id_ary); //排列正序
		$oversea_count=count($oversea_id_ary);
	?>
		<script type="text/javascript">
		var address_perfect=<?=(!$address_row || ($address_row && (!$address_row[0]['FirstName'] || !$address_row[0]['LastName'] || !$address_row[0]['AddressLine1'] || !$address_row[0]['City'] || !$address_row[0]['CId'] || !$address_row[0]['ZipCode'] || !$address_row[0]['PhoneNumber'])))?1:0;?>;
		var address_perfect_aid=<?=$address_row?(int)$address_row[0]['AId']:0;?>;
		<?php if($a=='buynow'){?>
			if(!$('html').loginOrVisitors()){ //跳转到登录页面
				window.top.location.href='/account/login.html?&jumpUrl='+decodeURIComponent('<?=$_SERVER['REQUEST_URI'];?>');
			}
		<?php }?>
		$(function(){
			cart_obj.cart_checkout();
			<?php if($_SESSION['Cart']['ShippingAddress']){?>
				cart_obj.cart_init.checkout_no_login(<?=str::json_data($_SESSION['Cart']['ShippingAddress']);?>);
			<?php }?>
			<?php
			if($c['NewFunVersion']>=4){//新用户版本 和 Paypal支付
				$IsCreditCard=(int)db::get_value('payment', 'IsUsed=1 and Method="Paypal"', 'IsCreditCard');//是否开启信用卡支付
			?>
				cart_obj.paypal_init(<?=$IsCreditCard;?>);
			<?php }?>
		});
		</script>
		<style type="text/css">
		.checkout_shipping .shipping:hover .icon_shipping_title, .checkout_shipping .current .icon_shipping_title, .checkout_shipping .icon_shipping_title{background-color:<?=$style_data['BuyNowBgColor'];?>;}
		</style>
		<div id="cart">
			<div class="cart_head ui_border_b"><a href="javascript:window.history.back(-1);" class="cart_head_back"><i></i></a><strong><?=$c['lang_pack']['mobile']['step_ary'][0];?></strong></div>
			<div class="cart_checkout">
				<div class="checkout_box checkout_address">
					<div class="box_title"><?=$c['lang_pack']['cart']['shippingInfo'];?></div>
					<div class="box_content">
						<?php /*
						<div class="address_button clearfix"><a href="<?=(int)$_SESSION['User']['UserId']?'/account/address/?Shipping=1':'javascript:;';?>" class="btn_global btn_address_more" id="moreAddress"><?=$c['lang_pack']['cart']['edit'];?> / <?=$c['lang_pack']['more'];?></a></div>
						<div class="address_default item clearfix">
							<?php if($address_row){?>
								<p class="clearfix"><strong><?=$address_row[0]['FirstName'].' '.$address_row[0]['LastName'];?></strong></p>
								<p class="address_line"><?=$address_row[0]['AddressLine1'].' '.($address_row[0]['AddressLine2']?$address_row[0]['AddressLine2'].' ':'');?></p>
								<p><?=$address_row[0]['City'].' '.($address_row[0]['StateName']?$address_row[0]['StateName']:$address_row[0]['State']).' '.$address_row[0]['Country'].' ('.$address_row[0]['ZipCode'].')';?></p>
								<p>+<?=$address_row[0]['CountryCode'].' '.$address_row[0]['PhoneNumber'];?></p>
							<?php }?>
						</div>
						<div id="addressInfo" style="display:none;"></div>
						*/ ?>
						<div id="ShipAddrFrom"><?php include('inc/shippingAddress.php');?></div>
					</div>
				</div>
				<div class="checkout_divide"></div>
				<div class="checkout_box checkout_payment">
					<div class="box_title"><?=$c['lang_pack']['cart']['paymethod'];?></div>
					<div class="box_content">
						<?php
						foreach((array)$payment_row as $v){
							$name=$v['Name'.$c['lang']]?$v['Name'.$c['lang']]:$v['Name_en'];
						?>
							<div class="payment_row clean" min="<?=cart::iconv_price($v['MinPrice'], 2, '', 0);?>" max="<?=cart::iconv_price($v['MaxPrice'], 2, '', 0);?>" pid="<?=$v['PId'];?>" method="<?=$v['Method'];?>">
								<div class="img fl"><img src="<?=$v['LogoPath'];?>" alt="<?=$name;?>" /></div>
								<div class="name fl"><?=$name;?></div>
								<div class="payment_contents fl" fee="<?=$v['AdditionalFee'];?>" affix="<?=cart::iconv_price($v['AffixPrice'], 2, '', 0);?>">
									<?php if($v['Description'.$c['lang']]){?><div class="desc"><?=$v['Description'.$c['lang']]?></div><?php }?>
								</div>
							</div>
						<?php }?>
					</div>
				</div>
				<div class="checkout_divide"></div>
				<?php /*if($c['FunVersion']>=1){?>
					<div class="checkout_box checkout_coupon">
						<div class="box_title"><?=$c['lang_pack']['mobile']['coupon_code'];?></div>
						<div class="box_content">
							<div class="clean code_input">
								<input type="text" name="couponCode" class="box_input fl" placeholder="<?=$c['lang_pack']['mobile']['apply'].' '.$c['lang_pack']['mobile']['coupon_code'];?>"><div class="btn_global btn_submit fl FontBgColor" id="coupon_apply"><?=$c['lang_pack']['submit'];?></div>
							</div>
							<div class="code_valid" id="code_valid" style="display:none;">
								<?=$c['lang_pack']['mobile']['coupon_txt'];?><br />
								<div class="valid_ex"><?=$c['lang_pack']['mobile']['discount_txt'];?> <strong></strong></div>
								<a href="javascript:;" id="removeCoupon"><?=$c['lang_pack']['mobile']['remove'];?></a>
							</div>
						</div>
					</div>
					<div class="checkout_divide"></div>
				<?php }*/?>
				<div class="checkout_box">
					<div class="box_title"><?=$c['lang_pack']['mobile']['summary'];?></div>
					<div class="box_content">
						<div class="cart_item_list">
							<?php
							$ptotal=0;
							$cart_attr=$cart_attr_data=array();
							foreach($cart_row as $v){
								$attr=array();
								$v['Property']!='' && $attr=str::json_data(str::attr_decode($v['Property']), 'decode');
								!$attr && $attr=str::json_data(htmlspecialchars_decode($v['Property']), 'decode');
								$price=$v['Price']+$v['PropertyPrice'];
								$v['Discount']<100 && $price*=$v['Discount']/100;
								$img=ly200::get_size_img($v['PicPath'], '240x240');
								$url=ly200::get_url($v, 'products');
								$total_weight+=($v['Weight']*$v['Qty']);
							?>
							<div class="item clean ui_border_b">
								<div class="img fl"><a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$v['Name'.$c['lang']];?>"></a></div>
								<div class="info">
									<div class="name"><a href="<?=$url;?>"><?=$v['Name'.$c['lang']];?></a></div>
									<?php
									if(count($attr)){
										foreach($attr as $k=>$z){
											if($k=='Overseas' && ((int)$c['config']['global']['Overseas']==0 || $v['OvId']==1)) continue; //发货地是中国，不显示
											echo '<div class="rows">'.($k=='Overseas'?$c['lang_pack']['products']['shipsFrom']:$k).': &nbsp;'.$z.'</div>';
										}
									}
									if((int)$c['config']['global']['Overseas']==1 && $v['OvId']==1){
										echo '<div class="rows">'.$c['lang_pack']['products']['shipsFrom'].': &nbsp;'.$c['config']['Overseas'][$v['OvId']]['Name'.$c['lang']].'</div>';
									}?>
									<?php if($v['Remark']){?><div class="rows"><?=$c['lang_pack']['mobile']['notice'].': &nbsp;'.$v['Remark'];?></div><?php }?>
									<div class="clear"></div>
									<div class="price"><?=cart::iconv_price($price);?><span class="quantity">x<?=$v['Qty'];?></span></div>
								</div>
							</div>
							<?php }?>
						</div>
					</div>
				</div>
				<form id="PlaceOrderFrom" method="post" action="?" amountPrice="<?=$iconv_total_price;?>" userPrice="<?=(($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0>$AfterPrice_1))?$AfterPrice_0:0;?>">
					<div class="checkout_summary">
						<div class="clean"><!-- subtotal -->
							<div class="key"><?=$c['lang_pack']['mobile']['subtotal'].' ('.str_replace('%num%', $total_quantity, $c['lang_pack']['cart'][($total_quantity>1?'itemsCount':'itemCount')]).')';?>:</div>
							<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_subtotal"><?=cart::currency_format($iconv_total_price, 0, $_SESSION['Currency']['Currency']);?></span></div>
						</div>
						<?php if(($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0>$AfterPrice_1)){?>
							<div class="clean" id="memberSavings"><!-- Member Savings -->
								<div class="key">(-) <?=$c['lang_pack']['cart']['user_save'];?></div>
								<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_user"><?=cart::currency_format($AfterPrice_0, 0, $_SESSION['Currency']['Currency']);?></span></div>
							</div>
						<?php }?>
						<div class="clean" style="display:none;" id="couponSavings"><!-- Coupon Savings -->
							<div class="key">(-) <?=$c['lang_pack']['mobile']['coupon_save'];?>:</div>
							<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_coupon">0</span></div>
						</div>
						<?php 
						$cutprice=0;
						if(($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1>$AfterPrice_0)){
						?>
							<div class="clean" id="subtotalDiscount"><!-- subtotal discount -->
								<div class="key">(-) <?=$c['lang_pack']['cart']['save'];?>:</div>
								<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_subtotal_discount"><?=cart::currency_format($AfterPrice_1, 0, $_SESSION['Currency']['Currency']);?></span></div>
							</div>
						<?php }?>
						<div class="clean" style="display:none;" id="serviceCharge"><!-- Service Charge -->
							<div class="key">(+) <?=$c['lang_pack']['cart']['fee'];?>:</div>
							<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_fee">0</span></div>
						</div>
						<div class="clean" id="total"><!-- Total -->
							<div class="key"><?=$c['lang_pack']['mobile']['grand_total'];?>:</div>
							<div class="value"><?=$_SESSION['Currency']['Symbol'];?><span id="ot_total"></span></div>
						</div>
					</div>
					<div class="checkout_button">
						<div class="btn_global btn BuyNowBgColor" id="cart_checkout"><?=$c['lang_pack']['mobile']['place_order'];?></div>
						<?php if($c['NewFunVersion']>=4){//新用户版本 和 Paypal支付?>
							<div id="paypal_button_container"></div>
						<?php }?>
					</div>
					<input type="hidden" name="order_coupon_code" value="<?=$_SESSION['Cart']['Coupon'];?>" cutprice="0" />
					<input type="hidden" name="order_discount_price" value="<?=($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1>$AfterPrice_0)?$AfterPrice_1:0;?>" />
					<input type="hidden" name="order_shipping_address_aid" value="<?=$address_row?$address_row[0]['AId']:-1;?>" />
					<input type="hidden" name="order_shipping_address_cid" value="<?=$address_row?$address_row[0]['CId']:-1;?>" />
					<input type="hidden" name="order_shipping_method_sid" value="[]" />
					<input type="hidden" name="order_shipping_method_type" value="[]" />
					<input type="hidden" name="order_shipping_price" value="[]" />
					<input type="hidden" name="order_shipping_insurance" value="[]" price="[]" />
					<input type="hidden" name="order_shipping_oversea" value="<?=$oversea_id_hidden?implode(',', $oversea_id_hidden):'';?>" />
					<input type="hidden" name="order_payment_method_pid" value="-1" />
					<input type="hidden" name="order_products_attribute_error" value="<?=(int)$products_attribute_error;?>" />
					<input type="hidden" name="shipping_method_where" value="&ProId=<?=$cart_row[0]['ProId'];?>&Qty=<?=$cart_row[0]['Qty'];?>&Type=shipping_cost" attr="<?=str::str_code(str::json_data($Attr));?>" />
					<?php if($CId){?><input type="hidden" name="order_cid" value="<?=$CId;?>" /><?php }?>
				</form> 
			</div>
		</div>
	<?php }?>
</div>
</body>
</html>

