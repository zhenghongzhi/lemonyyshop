<?php !isset($c) && exit();?>
<?php
$cutArr=str::json_data(db::get_value('config', "GroupId='cart' and Variable='discount'", 'Value'), 'decode');
$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');//模板风格色调的数据
$style_data=str::json_data($StyleData, 'decode');
?>
<!DOCTYPE HTML>
<html lang="us">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="noindex,nofollow" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection" />
<?php
echo ly200::seo_meta();
include("{$c['mobile']['theme_path']}inc/resource.php");
echo ly200::load_static('/static/themes/default/mobile/buynow/js/cart.js');
?>
</head>

<body>
<?php include('inc/header.php');?>
<div class="wrapper">
	<div id="cart">
		<?=html::mobile_cart_step(2);?>
		<div class="complete_box">
			<div class="complete_tips"><?=$c['lang_pack']['mobile']['received_txt'];?></div>
		</div>
	</div>
</div>
</body>
</html>