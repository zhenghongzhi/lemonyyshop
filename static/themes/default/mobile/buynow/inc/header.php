<?php !isset($c) && exit();?>
<?php 
echo ly200::set_custom_style();
$MobileHeadData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'MobileHeadData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'MobileHeadData');
$mobile_head_data=str::json_data($MobileHeadData, 'decode');
$FbData=$c['config']['Platform']['Facebook']['SignIn']['Data'];

$storeUrl='';
if($c['mobile']['HomeTpl']=='store'){//店铺模式
	$storeUrl='/store';
}
?>
<style>
	.MobileHeadBgColor{background-color:<?=$mobile_head_data['BgColor'];?>;}
</style>
<script type="text/javascript">
var ueeshop_config={
	"domain":"<?=ly200::get_domain();?>",
	"date":"<?=date('Y/m/d H:i:s', $c['time']);?>",
	"lang":"<?=substr($c['lang'], 1);?>",
	"currency":"<?=$_SESSION['Currency']['Currency'];?>",
	"currency_symbols":"<?=$_SESSION['Currency']['Symbol'];?>",
	"currency_rate":"<?=$_SESSION['Currency']['Rate'];?>",
	"FbAppId":"<?=$FbData?$FbData['appId']:'';?>",
	"FbPixelOpen":"<?=in_array('facebook_pixel', $c['plugins']['Used']) ? 1 : 0 ;?>",
	"UserId":"<?=(int)$_SESSION['User']['UserId'];?>",
	"TouristsShopping":"<?=(int)$c['config']['global']['TouristsShopping'];?>",
	"IsMobile":1
}
</script>
<?php if($c['config']['global']['IsCopy']){?>
	<script type="text/javascript">
		document.oncontextmenu=new Function("event.returnValue=false;");
		document.onselectstart=new Function("event.returnValue=false;");
		document.oncontextmenu=function(e){return false;}
	</script>
	<style>
	html, img{-moz-user-select:none; -webkit-user-select:none;}
	</style>
<?php }?>