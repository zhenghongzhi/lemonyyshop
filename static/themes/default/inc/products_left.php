<?php !isset($c) && exit();?>
<?php
if($cfg_module_ary['IsLeftbar']['Category']) include("{$c['static_path']}inc/products_left/catalog.php");
if(in_array('screening', $c['plugins']['Used']) && !substr_count($_SERVER['REQUEST_URI'], '/search/') && $attr_ary && $cfg_module_ary['Narrow']==1 && $a=='products'){
	include("{$c['static_path']}inc/products_left/narrow_by.php");
}
if($cfg_module_ary['IsLeftbar']['Hot']) include("{$c['static_path']}inc/products_left/what_hot.php");
if($cfg_module_ary['IsLeftbar']['Special']) include("{$c['static_path']}inc/products_left/special_offer.php");
if($cfg_module_ary['IsLeftbar']['Popular']) include("{$c['static_path']}inc/products_left/popular_search.php");
if($cfg_module_ary['IsLeftbar']['Newsletter']) include("{$c['static_path']}inc/products_left/newsletter.php");
if($cfg_module_ary['IsLeftbar']['TheSay']) include("{$c['static_path']}inc/products_left/the_say.php");
if($cfg_module_ary['IsLeftbar']['Recommended']) include("{$c['static_path']}inc/products_left/recommended.php");
if($cfg_module_ary['IsLeftbar']['Seckill']) include("{$c['static_path']}inc/products_left/seckill.php");
?>
<div class="blank20"></div>
<?php
if($cfg_module_ary['IsLeftbar']['Banner']){
	echo ly200::ad(7);
}?>