<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/16
 * Time: 10:48
 */
?>
<?php !isset($c) && exit(); ?>
<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
$d_ary = array('list', 'view', 'cancel', 'contact', 'shipping');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];

//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach ($currency_row as $k => $v) {
    $all_currency_ary[$v['Currency']] = $v;
}
?>
    <script type="text/javascript">$(document).ready(function () {
            user_obj.order_init()
        });</script>
<?php
if ($d == 'list') {
    //查询
    if ($user_row['yy_code']){
        $url = "http://119.23.214.213/yy_app/yyshopapi/orderlist.php?userid=".$user_row['yy_code']."&search=".$_GET['search'];
        $orderlist = ly200::curl($url);
        $orderlist = json_decode($orderlist);
        $list = $orderlist->list;
        $count = $orderlist->count;
    }else{
        $count = 0;
    }
//    print_r($orderlist);die;
    ?>
    <div id="user_heading" class="fl">
        <h2><?= $c['lang_pack']['user']['zhz_pruchaseorders']; ?></h2>
    </div>
    <div class="message_list fr">

    </div>
    <div class="clear"></div>
    <div class="blank20"></div>
    <table class="order_table" style="margin-bottom:5px;">
        <tr>
            <th><?= $c['lang_pack']['mobile']['order_info']; ?></th>
            <th width="16%"><?= $c['lang_pack']['user']['zhz_nubmer']; ?></th>
            <th width="16%" class="order_status">
                <div class="user_action_down">
                    <?= $c['lang_pack']['user']['orderStatus']; ?>

                </div>
            </th>
            <th width="16%"><?= $c['lang_pack']['user']['action']; ?></th>
        </tr>
    </table>
    <?php

    foreach ($list as $item) { ?>
        <table class="order_table">
            <tbody>
            <tr class="list_oid">
                <td colspan="4">
                    <?= date('M.d.Y', $item->addtime); ?> &nbsp;&nbsp; <?= $c['lang_pack']['user']['orderNo']; ?>
                    <a href="/account/pruchaseorder/view<?= $item->id; ?>.html"
                       title="<?= $item->id; ?>"><?= $item->order_id; ?></a>
                </td>
            </tr>
            <tr class="list_opl">
                <td>
                    <div class="list">
                        <div class="">

                                <? if ($item->images){ ?>
                                    <a class="pic" href="http://119.23.214.213/yy_app/<?=$item->images; ?>" target="_blank">
                                        <img src="http://119.23.214.213/yy_app/<?=$item->images; ?>" alt="Compass Tattoo Power">
                                    </a>
                                <? }else{ ?>
                                    <a class="pic">
                                        <img src="/static/themes/default/mobile/images/orderlist.png" alt="Compass Tattoo Power">
                                    </a>
                                <? } ?>
                                <span></span>
                            </a>
                            <div class="desc">
                                <p class="name"><?=$item->son->subject; ?></p>
                                <ul>
                                    <li><?=$item->son->type1; ?></li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </td>
                <td width="16%"
                    class="o_price">x<?=$item->type2; ?></td>
                <td width="16%"
                    class="o_status"><?=$item->status==1?$c['lang_pack']['user']['zhz_orderstauts']['chulizhong']:$c['lang_pack']['user']['zhz_orderstauts']['wancheng']; ?></td>
                <td width="16%" class="options">
                    <a href="/account/pruchaseorder/view<?= $item->id; ?>.html"
                       class="sys_bg_button"><?= $c['lang_pack']['user']['view_more']; ?>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    <?php } ?>
    <?php
} else {
    $OId = $_GET['OId'];
    $url = "http://119.23.214.213/yy_app/yyshopapi/orderview.php?orderid=".$OId;
    $order = ly200::curl($url);
    $order = json_decode($order);
    $orderview = $order->list;

    $prourl = "http://119.23.214.213/yy_app/yyshopapi/orderpro.php?orderid=".$OId;
    $product = ly200::curl($prourl);
    $product1 = json_decode($product);
    $productlist = $product1->list;
//    print_r($orderview);die;
    ?>
    <style>
        .order_base_table{
            width: 46%;
        }
        .grand_total_chang_pay{
            width: 54%;
        }
        .grand_total_chang_pay table tr th{
            width: 30%;
        }
    </style>
    <div class="order_body">
        <div class="order_base">
            <a href="javascript:javascript :history.back(-1);" class="user_back"><?= $c['lang_pack']['user']['order_details']; ?></a>
            <h3 class="title">
                <span class="fr"><?=$orderview->status==1?$c['lang_pack']['user']['zhz_orderstauts']['chulizhong']:$c['lang_pack']['user']['zhz_orderstauts']['wancheng']; ?></span>No.<?=$orderview->order_id; ?>
            </h3>

            <h3 class="title"><?= $c['lang_pack']['mobile']['order_info']; ?></h3>
            <div class="order_base_div">
                <table class="order_base_table" cellpadding="3">
                    <!-- <tr><th>Order Number:</th><td>19122512041992</td></tr> -->
                    <tbody><tr class="tr">
                        <th><?= $c['lang_pack']['user']['orderDate']; ?>:</th>
                        <td width="70%"><?=date('F d, Y', $orderview->addtime); ?></td>
                    </tr>
                    <tr class="tr">
                        <th><?= $c['lang_pack']['user']['zhz_customername']; ?></th>
                        <td><?=$orderview->username; ?></td>
                    </tr>
                    <tr class="tr">
                        <th><?= $c['lang_pack']['user']['zhz_customernumber']; ?></th>
                        <td><?=$orderview->kehu_number; ?></td>
                    </tr>
                    <tr class="tr">
                        <th><?= $c['lang_pack']['user']['zhz_translationname']; ?></th>
                        <td><?= $orderview->nickname; ?></td>
                    </tr>
                    </tbody></table>
                <div class="grand_total grand_total_chang_pay">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <th></th>
                            <td></td>
                        </tr>
                        <tr>
                            <th><?= $c['lang_pack']['user']['zhz_merchant']; ?>
                            </th>
                            <td><?=$orderview->subject; ?></td>
                        </tr>
                        <tr>
                            <th ><?= $c['lang_pack']['user']['zhz_businessaddress']; ?></th>
                            <td>
                                <?=$orderview->address?>
                            </td>
                        </tr>
                        <tr>
                            <th><?= $c['lang_pack']['user']['zhz_totalamount']; ?>
                            </th>
                            <td><?= $orderview->type1; ?></td>
                        </tr>
                        <tr>
                            <th ><?= $c['lang_pack']['user']['zhz_estimateddelivery']; ?>
                            </th>
                            <td><?= $orderview->addtime ? date('F d, Y',  ($orderview->addtime+(25*24*3600))) : 'N/A';?></td>
                        </tr>
                        </tbody>
                        <tfoot>
<!--                        <tr>-->
<!--                            <th width="100%"></th>-->
<!--                            <td>-->
<!--                            </td>-->
<!--                        </tr>-->

<!--                        <tr>-->
<!--                            <th width="100%">-->
<!--                            </th>-->
<!--                            <td></td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr>-->
<!--                            <th width="100%" class="totalprod"></th>-->
<!--                            <td class="totalPrice">-->
<!--                            </td>-->
<!--                        </tr>-->
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="blank20"></div>
        <div class="order_menu order_summary">
            <h3 class="title"><?= $c['lang_pack']['user']['orderSummary']; ?></h3>
            <div class="waybill_products_list">
                <div class="products_list_item">
                    <div class="row_hd fir">
                        <div class="top">
<!--                            <i class="fr">In warehousing/Awaiting Balance Payment</i>Shipment-->
                            <div class="clear"></div>
                        </div>
                        <div class="bot">
                            <span>( <?=$c['lang_pack']['user']['trackNo']; ?>: <a href="/account/logistics/<?=$orderview->id; ?>.html"><?=$orderview->order_id; ?></a> )                                            										</span>
                            <script type="text/javascript">
                                //YQV5.trackSingleF1({
                                //    YQ_ElementId: "//",	//必须，指定悬浮位置的元素ID。
                                //    YQ_Width: 600,	//可选，指定查询结果宽度，最小宽度为600px，默认撑满容器。
                                //    YQ_Height: 400,	//可选，指定查询结果高度，最大高度为800px，默认撑满容器。
                                //    YQ_Lang: "//",	//可选，指定UI语言，默认根据浏览器自动识别。
                                //    YQ_Num: "//"	//必须，指定要查询的单号。
                                //});


                            </script>
<!--                            <span id="1912141046352311_2" class="detail_track">Detail Tracking</span>-->
                            <script type="text/javascript">

                            </script>
                        </div>
                    </div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="row_table">
                        <thead>
                            <tr>
                                <th><?= $c['lang_pack']['user']['zhz_sectionnumber']; ?></th>
                                <th><?= $c['lang_pack']['user']['zhz_productname']; ?></th>
                                <th><?= $c['lang_pack']['user']['zhz_quantity']; ?></th>
                                <th><?= $c['lang_pack']['user']['zhz_unitprice']; ?></th>
                                <th><?= $c['lang_pack']['user']['zhz_amount']; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <? foreach ($productlist as $item) { ?>
                        <tr>
                            <td><?=$item->type1; ?></td>
                            <td><?=$item->subject; ?></td>
                            <td><?=$item->type4; ?></td>
                            <td><?=$item->type5; ?></td>
                            <td><?=$item->type6; ?></td>
                        </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>