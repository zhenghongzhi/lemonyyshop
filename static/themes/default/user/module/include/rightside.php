<?php !isset($c) && exit();?>
<?php
$sign_count=0;
foreach($c['config']['Platform'] as $v){
	$v['SignIn']['IsUsed']==1 && $sign_count+=1;
}
?>
<div class="info fr">
	<div class="box"><a href="/" class="home"><?=$c['lang_pack']['user']['returnHome'];?></a></div>
    <div class="box member">
        <p><?=$c['lang_pack']['user']['already'];?></p>
        <div class="sign_btn"><a href="javascript:;" class="SignInButton signinbtn"><?=$c['lang_pack']['user']['signInNow'];?></a></div>
        <p class="forgot"><a href="/account/forgot.html" class="FontColor"><?=$c['lang_pack']['user']['forgotPWD'];?></a></p>
        <?php
		if($sign_count>0){
			$facebook_data=$c['config']['Platform']['Facebook']['SignIn'];
			if(in_array('facebook_login', $c['plugins']['Used']) && $facebook_data['Data']['appId']){
				echo ly200::load_static('/static/js/oauth/facebook.js');
        ?>
            <fb:login-button scope="public_profile,email" onlogin="checkLoginState();" class="fb-login-button" data-size="medium" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false" data-width="182px"></fb:login-button>
        <?php
			}
			$twitter_data=$c['config']['Platform']['Twitter']['SignIn'];
			if(in_array('twitter_login', $c['plugins']['Used']) && $twitter_data['Data']['CONSUMER_KEY'] && $twitter_data['Data']['CONSUMER_SECRET']){
				echo ly200::load_static('/static/js/oauth/twitter.js');
			?>
				<span class="twitter_button" id="twitter_btn" key="<?=base64_encode(~$twitter_data['Data']['CONSUMER_KEY']);?>" secret="<?=base64_encode(~$twitter_data['Data']['CONSUMER_SECRET']);?>" callback="<?=urlencode(ly200::get_domain().$c['config']['Platform']['Twitter']['ReturnUrl']);?>">
					<span class="icon"></span>
					<span class="text">Log In with Twitter</span>
				</span>
			<?php
			}
			$google_data=$c['config']['Platform']['Google']['SignIn'];
			if(in_array('google_login', $c['plugins']['Used']) && $google_data['Data']['clientid']){
				echo '<meta name="google-signin-scope" content="profile email"><meta name="google-signin-client_id" content="'.$google_data['Data']['clientid'].'"><script src="https://apis.google.com/js/platform.js" async defer></script>';
                echo ly200::load_static('/static/js/oauth/google.js');
			?>
				<div class="google_button">
					<div class="g-signin2" id="google_btn" data-onsuccess="GoogleSignIn" data-theme="dark" data-login-status="">&nbsp;</div>
					<span class="icon"></span>
					<span class="button_text">Log In with Google</span>
				</div>
        <?php
			}
			$paypal_data=$c['config']['Platform']['Paypal']['SignIn'];
			if(in_array('paypal_login', $c['plugins']['Used']) && $paypal_data['Data']['client_id']){
				echo ly200::load_static('/static/js/oauth/paypal/api.js');
				$_domain=!$paypal_data['Data']['domain']?ly200::get_domain():$paypal_data['Data']['domain'];
        ?>
			<div id="paypalLogin" appid="<?=$paypal_data['Data']['client_id'];?>" u="<?=htmlspecialchars_decode(trim($_domain,'/').$c['config']['Platform']['Paypal']['ReturnUrl']);?>" scopes="<?=(int)db::get_row_count('user', 'PaypalId like "%@%"', 'UserId'); ?>"></div>
        <?php
			}
			$vk_data=$c['config']['Platform']['VK']['SignIn'];
			if(in_array('vk_login', $c['plugins']['Used']) && $vk_data['Data']['apiId']){
				echo ly200::load_static('/static/js/oauth/vk.js');
        ?>
            <div id="vk_button" class="vk_button" apiid="<?=$vk_data['Data']['apiId']?>">
                <span class="icon"></span>
                <span class="button_text">Log In with VK</span>
            </div>
        <?php
			}
			/*
			$instagram_data=$c['config']['Platform']['Instagram']['SignIn'];
			if(in_array('instagram_login', $c['plugins']['Used']) && $instagram_data['Data']['client_id'] && $instagram_data['Data']['client_secret']){
				echo ly200::load_static('/static/js/oauth/instagram.js');
        ?>
            <div id="instagram_button" class="instagram_button" client_id="<?=$instagram_data['Data']['client_id']?>" client_secret="<?=$instagram_data['Data']['client_secret']?>" redirect_uri="<?=(rtrim(ly200::get_domain(),'/').'/account/sign-up.html?login_with_instagram=1');?>" login="<?=(int)$_GET['login_with_instagram'] ? 1 : 0; ?>" code="<?=trim($_GET['code']); ?>">
                <span class="icon"></span>
                <span class="button_text">Log In with Instagram</span>
            </div>
        <?php
			}
			*/
		}?>
    </div>
    <?php
	$help_row=str::str_code(db::get_limit('article', 'CateId=99', "AId, Title{$c['lang']}, PageUrl, Url", $c['my_order'].'AId desc', 0, 5));
	if(count($help_row)){
	?>
    <div class="box">
        <h3><?=str::str_code(db::get_value('article_category', 'CateId=99', "Category{$c['lang']}"));?></h3>
        <ul>
			<?php
			foreach((array)$help_row as $v){
			?>
            <li><a href="<?=ly200::get_url($v, 'article');?>" title="<?=$v['Title'.$c['lang']];?>" target="_blank"><?=$v['Title'.$c['lang']];?></a></li>
			<?php }?>
        </ul>
    </div>
	<?php }?>
</div>