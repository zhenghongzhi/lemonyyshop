<?php !isset($c) && exit(); ?>
<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
$d_ary = array('list', 'view', 'contact');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];

//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach ($currency_row as $k => $v) {
    $all_currency_ary[$v['Currency']] = $v;
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        user_obj.rfq_init()
    });
</script>
<?php
if ($d == 'list') {
    //查询
    $status = (int)$_GET['status'];//订单状态（搜索）
    $g_Page = (int)$_GET['page'];
    $g_Page < 1 && $g_Page = 1;
    $row_count = 10;
    $select_rfq_row = str::str_code(db::get_limit_page('purchase_request', "{$c['where']['user_id']}" . ($status ? " and status='$status'" : ''), '*', 'id desc', $g_Page, $row_count));
    $query_string = ly200::query_string('m, a, page');
    ?>
    <style>
        .order_table .list{
            line-height: 80px;
            border-right: none;
        }

        .order_table .list p{
            line-height: 20px;
        }

        #lib_user_main .order_table .list_opl .o_status{
            line-height: 80px;
            color: red;
            border-left: 1px solid #e5e5e5;
            font-size: 16px;
            vertical-align: middle;
        }

        #lib_user_main .order_table .list_opl .list img{
            width: 100%;
            height: 80%;
            object-fit: cover;
            display: block;
        }

        #lib_user_main .order_table .list_opl .options{
            line-height: 80px;
        }

        #lib_user_main .order_table .list_opl .options .fr {
            float: right;
            text-align: center;
            position: relative;
            line-height: 80px;
        }

        #lib_user_main .order_table .list_opl .options .fr span {
            float: right;
            font-size: 16px;
            margin-right: 16px;
        }
        #lib_user_main .order_table .list_opl .options .fr em {
            margin-top: 2.0rem;
            margin-right: 16px;
            border-width: .4rem 0 .4rem .4rem;
            border-color: transparent transparent transparent #999;
            border-style: solid;
            float: right;
            display: block;
            position: relative;
            z-index: 10;
        }

        #lib_user_main .order_table .list_opl .options .fr em > i {
            border-width: .4rem 0 .4rem .4rem;
            border-color: transparent transparent transparent #fff;
            border-style: solid;
            display: block;
            position: absolute;
            top: -.4rem;
            right: .1rem;
            z-index: 11;
        }
    </style>
    <div id="user_heading" class="fl">
        <h2><?= $c['lang_pack']['my_rfq']; ?></h2>
    </div>
    <div class="message_list fr">
        <a href="/account/rfq/?status=1" class="sys_bg_button m0<?= $status == 1 ? ' cur' : ''; ?>">
            <?php
            echo $c['lang_pack']['user']['PurchaseStatusAry'][1];
            if ($num = db::get_row_count('purchase_request', "user_id='{$_SESSION['User']['UserId']}' and status=1")) {
                echo '<span>' . $num . '</span>';
            } ?>
        </a>
        <a href="/account/rfq/?status=2" class="sys_bg_button m0<?= $status == 2 ? ' cur' : ''; ?>">
            <?php
            echo $c['lang_pack']['user']['PurchaseStatusAry'][2];
            if ($num = db::get_row_count('purchase_request', "user_id='{$_SESSION['User']['UserId']}' and status=2")) {
                echo '<span>' . $num . '</span>';
            } ?>
        </a>
        <a href="/account/rfq/?status=3" class="sys_bg_button m0<?= $status == 3 ? ' cur' : ''; ?>">
            <?php
            echo $c['lang_pack']['user']['PurchaseStatusAry'][3];
            if ($num = db::get_row_count('purchase_request', "user_id='{$_SESSION['User']['UserId']}' and status=3")) {
                echo '<span>' . $num . '</span>';
            } ?>
        </a>
        <a href="/account/rfq/?status=4" class="sys_bg_button m0<?= $status == 4 ? ' cur' : ''; ?>">
            <?php
            echo $c['lang_pack']['user']['PurchaseStatusAry'][4];
            if ($num = db::get_row_count('purchase_request', "user_id='{$_SESSION['User']['UserId']}' and status=4")) {
                echo '<span>' . $num . '</span>';
            } ?>
        </a>
    </div>
    <div class="clear"></div>
    <div class="blank20"></div>
    <table class="order_table" style="margin-bottom:5px;">
        <tr>
            <th><?= $c['lang_pack']['user']['rfq_info']; ?></th>
            <th width="20%" class="order_status">
                <div class="user_action_down">
                    <?= $c['lang_pack']['user']['rfq_status']; ?>
                    <i></i>
                    <ul>
                        <?php foreach ($c['lang_pack']['user']['PurchaseStatusAry'] as $k => $v) { ?>
                            <li><a href="/account/rfq/?status=<?= $k; ?>"
                                   status="<?= $k; ?>"<?= $status == $k ? ' class="current"' : ''; ?>><?= $c['lang_pack']['user']['PurchaseStatusAry'][$k]; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </th>
            <th width="20%"><?= $c['lang_pack']['user']['action']; ?></th>
        </tr>
    </table>
    <?php
    foreach ((array)$select_rfq_row[0] as $key => $val) {
        $pr_id = $val['id'];
        $img = str::str_code(db::get_one('purchase_img', "pr_id = $pr_id", 'img_url'));
        ?>
        <table class="order_table">
            <tbody>
            <tr class="list_oid">
                <td colspan="4">
                    <?= date('M.d.Y', $val['created_at']); ?> &nbsp;&nbsp; <?= $c['lang_pack']['user']['rfq_no']; ?>
                    <a href="/account/rfq/view/<?= $val['OId']; ?>.html"
                       title="<?= $val['OId']; ?>"><?= $val['OId']; ?></a>
                </td>
            </tr>
            <tr class="list_opl">
                <td>
                    <?php
                    $hide_num = $hide_item_num = 0;
                    ?>
                    <div class="list">
                        <div>
                            <!-- edit by zhz start-->
                            <a href="<?= $img['img_url'] ?>" target="_blank" class="pic"><img
                                        src="<?= $img['img_url']?$img['img_url']:'/static/themes/default/images/caigou.png' ?>"><span></span></a>
                            <!-- edit by zhz end  -->
                            <div class="desc">
                                <p class="name"><?= $val['product_name']; ?></p>
                                <p class="description"><?= $val['description']; ?></p>
                            </div>

                        </div>
                        <?php
                        $hide_num++;
                        ?>
                </td>
                <td width="20%"
                    class="o_status"><?= $c['lang_pack']['user']['PurchaseStatusAry'][$val['status']]; ?></td>
                <td width="20%" class="options">

                    <div class="fr">
                        <em>

                            <i></i>
                        </em>
                        <span>
                                           <a href="/account/rfq/view/<?= $val['OId']; ?>.html"
                                              class="sys_bg_button"><?= $c['lang_pack']['user']['view_more']; ?></a>
                                        </span>
                    </div>
<!--                    <div class="user_action_down">-->
<!--                        <a href="/account/rfq/view/--><?//= $val['OId']; ?><!--.html"-->
<!--                           class="sys_bg_button">--><?//= $c['lang_pack']['user']['view_more']; ?><!--</a>-->
<!--                        <i></i>-->
<!--                        <ul>-->
<!--                            <li>-->
<!--                                <a href="/account/rfq/view/--><?//= $val['OId']; ?><!--.html"-->
<!--                                   class="sys_bg_button">--><?//= $c['lang_pack']['user']['view_more']; ?><!--</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
                </td>
            </tr>
            </tbody>
            <?php if ($hide_num > 3 || $hide_item_num > 3) { ?>
                <tfoot>
                <tr>
                    <td colspan="4"><a href="javascript:;"
                                       class="see_more sys_bg_button"><?= $c['lang_pack']['more']; ?></a></td>
                </tr>
                </tfoot>
            <?php } ?>
        </table>
    <?php } ?>
    <div id="turn_page"><?= ly200::turn_page_html($select_rfq_row[1], $select_rfq_row[2], $select_rfq_row[3], $query_string, $c['lang_pack']['user']['previous'], $c['lang_pack']['user']['next'], 3, '.html', $html = 1); ?></div>
    <?php
} else {
    $OId = $_GET['OId'];
    $purchase_row = str::str_code(db::get_one('purchase_request', "OId='$OId' and user_id='{$_SESSION['User']['UserId']}'"));
    !$purchase_row && js::location('/account/rfq/view/' . $OId . '.html');

    ?>
    <style>
        .pic_box {
            width: 100px;
            height: 100px;
            font-size: 0;
            vertical-align: middle;
            margin: 5px;
        }

        .pic_box > img {
            max-width: 100px;
            max-height: 100px;
        }
    </style>
    <div class="order_body">
    <div id="user_heading" class="fl">
        <h2><?= $c['lang_pack']['my_rfq']; ?></h2>
    </div>
    <div class="blank20"></div>


    <div class="order_base">
        <div class="order_con">
            <span class="or_name"><?= $c['lang_pack']['user']['rfq_no']; ?></span> :
            <?= $OId ?>
        </div>
        <div class="clear"></div>
        <div class="blank12"></div>
        <div class="order_base_div">
            <table class="order_base_table" cellpadding="3">
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['productName']; ?>:</th>
                    <td width="70%"><?= $purchase_row['product_name'] ? $purchase_row['product_name'] : 'N/A'; ?></td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['quantity']; ?>:</th>
                    <td>
                        <?= $purchase_row['quantity']; ?>
                        <?= $purchase_row['unit']; ?>
                    </td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['description']; ?>:</th>
                    <td width="70%"><?= $purchase_row['description'] ? $purchase_row['description'] : 'N/A'; ?></td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['productURL']; ?>:</th>
                    <td width="70%"><?= $purchase_row['product_url'] ? $purchase_row['product_url'] : 'N/A'; ?></td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['referQuotation']; ?>:</th>
                    <td width="70%"><?= $purchase_row['refer_price'] ? $purchase_row['refer_price'] : 'N/A'; ?></td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['productImage']; ?>:</th>
                    <td width="70%">
                        <div style="white-space: nowrap;">
                            <?php
                            $rfq_img_row = str::str_code(db::get_all('purchase_img', 'pr_id=' . $purchase_row['id'], 'img_url', 'id desc'));
                            if ($rfq_img_row) {
                                foreach ((array)$rfq_img_row as $k => $v) {
                                    ?>
                                    <a href="<?= $v['img_url'] ?>" target="_blank" class="pic_box"><img
                                                src="<?= $v['img_url'] ?>"><span></span></a>
                                    <?php
                                }
                            } else {
                                echo 'N/A';
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <tr class="tr">
                    <th><?= $c['lang_pack']['products']['factoryInformation']; ?>:</th>
                    <td width="70%"><?= $purchase_row['factory_info'] ? $purchase_row['factory_info'] : 'N/A'; ?></td>
                </tr>
            </table>
        </div>
        <div class="blank12"></div>
        <hr>
        <div class="blank12"></div>
        <div class="clear"></div>
        <?php
        $OId = $_GET['OId'];
        $purchase_row = str::str_code(db::get_one('purchase_request', "OId='$OId' and user_id='{$_SESSION['User']['UserId']}'"));
        !$purchase_row && js::location('/account/rfq/contact/' . $OId . '.html');
        $row = str::str_code(db::get_one('request_message', "UserId='{$_SESSION['User']['UserId']}' and Module='purchase' and Subject='$OId'"));
        if ($row) {
            if ($row['IsReply']) db::update('request_message', "MId='{$row['MId']}'", array('IsReply' => 0));
            $reply_row = str::str_code(db::get_all('request_message_reply', "MId='{$row['MId']}'"));
        }
        $back_url = 'javascript:history.back(-1);';
        if ($_SESSION['Ueeshop']['RequestReturnUrl']) {
            $back_url = $_SESSION['Ueeshop']['RequestReturnUrl'];
        }
        unset($_SESSION['Ueeshop']['RequestReturnUrl']);
        ?>

        <div class="item_title" style="margin: 10px;font-size: 18px; font-weight: bold;">
            <span class="or_name"><?= $c['lang_pack']['user']['messageTitle']; ?></span> :
        </div>
        <div class="blank15"></div>

        <div id="lib_user_products" style="border: dimgrey 1px solid;">
            <?php if ($row) { ?>
                <div class="content_box">
                    <?php foreach ((array)$reply_row as $k => $v) { ?>
                        <div class="item <?= $v['UserId'] ? '' : 'mine' ?>">
                            <?php if ($k == count($reply_row) - 1) { ?>
                                <div id="View"></div><?php } ?>
                            <div class="item_date"><?= date('Y-m-d H:i', $v['AccTime']) ?></div>
                            <div class="clear"></div>
                            <div class="item_img"><img
                                        src="/static/themes/default/images/user/<?= $v['UserId'] ? 'icon_faq' : 'icon_reply' ?>.png"/>
                            </div>
                            <div class="item_con">
                                <div class="item_txt"><?= nl2br($v['Content']) ?></div>
                                <?php if ($v['PicPath']) { ?><a class="light_box_pic" target="_blank"
                                                                href="<?= $v['PicPath'] ?>"><img
                                            src="<?= $v['PicPath'] ?>"/></a><?php } ?>
                                <span></span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    <?php } ?>
                    <div id="View"></div>
                </div>
            <?php } ?>
            <div class="blank20"></div>
            <form id="reply_form" class="reply_form user_form" method="post" enctype="multipart/form-data"
                  style="margin: 16px;">
                <div class="reply_tips"><?= $c['lang_pack']['products']['reply'] . ' ' . $c['lang_pack']['user']['content'] ?></div>
                <div class="rows">
                    <label><?= $c['lang_pack']['user']['content']; ?>:</label>
                    <span class="input"><textarea name="Content"
                                                  placeholder="<?= $c['lang_pack']['user']['content']; ?>"
                                                  class="form_text" notnull=""></textarea></span>
                    <div class="clear"></div>
                </div>
                <div class="rows">
                    <label><?= $c['lang_pack']['user']['image']; ?>:</label>
                    <div class="input upload_box">
                        <input class="upload_file" id="upload_file" type="file" name="PicPath" onchange="loadImg(this);"
                               accept="image/gif,image/jpeg,image/png">
                        <div id="pic_show" class="pic_box"></div>
                    </div>
                    <div class="submit">
                        <input type="submit" class="submit_btn" name="submit_button"
                               value="<?= $c['lang_pack']['user']['submit']; ?>"/>
                    </div>
                    <div class="clear"></div>
                </div>
                <input type="hidden" name="Subject" value="<?= $OId ?>"/>
                <input type="hidden" name="MId" value="<?= $row['MId'] ?>"/>
                <input type="hidden" name="UserId" value="<?= $_SESSION['User']['UserId'] ?>"/>
                <input type="hidden" name="OId" value="<?= $OId ?>"/>
                <input type="hidden" name="do_action" value="user.reply_rfq_msg"/>
            </form>
        </div>
    </div>
    <script>
        document.getElementById('View').scrollIntoView();

        function loadImg(obj) {
            //获取文件
            var file = obj.files[0];
            //创建读取文件的对象
            var reader = new FileReader();
            //为文件读取成功设置事件
            reader.onload = function (e) {
                var imgFile = e.target.result;
                $(obj).next('#pic_show').html('<img src="' + imgFile + '"/><span></span>');
            };
            //正式读取文件
            reader.readAsDataURL(file);
        }
    </script>
<?php } ?>