<?php !isset($c) && exit(); ?>
<?php
/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
//货币汇率
$all_currency_ary = array();
$currency_row = db::get_all('currency', '1', 'Currency, Symbol, Rate');
foreach ($currency_row as $k => $v) {
    $all_currency_ary[$v['Currency']] = $v;
}

$ship_row = str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', 'a.' . $c['where']['user'] . " and a.IsBillingAddress=0", 'a.*, c.Country, c.CountryData, s.States as StateName', 'a.AccTime desc, a.AId desc'));
$msg_row = str::str_code(db::get_limit('message', 1, 'MId, Title', 'MId desc', 0, 4));
$review_row = str::str_code(db::get_limit('products_review', $c['where']['user'] . " and ReId=0", '*', 'RId desc', 0, 3));
$newsletter_row = db::get_one('newsletter', "Email='{$user_row['Email']}'");

$country = $ship_row['Country'];
if ($c['lang'] != '_en') {
    $country_data = str::json_data(htmlspecialchars_decode($ship_row['CountryData']), 'decode');
    $country = $country_data[substr($c['lang'], 1)];
}

$level_row = db::get_one('user_level', "IsUsed=1 and LId = '{$user_row['Level']}'");
$next_level_row = db::get_one('user_level', "IsUsed=1 and FullPrice > '{$level_row['FullPrice']}'", '*', 'FullPrice asc');
$next_level_row || $next_level_row = $level_row;
$_UserName = substr($c['lang'], 1) == 'jp' ? $user_row['LastName'] . ' ' . $user_row['FirstName'] : $user_row['FirstName'] . ' ' . $user_row['LastName'];

$vid_data_ary = array();
$attribute_row = str::str_code(db::get_all('products_attribute', '1', "AttrId, Type, Name{$c['lang']}, ParentId, CartAttr, ColorAttr"));
foreach ($attribute_row as $v) {
    $attribute_ary[$v['AttrId']] = array(0 => $v['Type'], 1 => $v["Name{$c['lang']}"]);
}
$value_row = str::str_code(db::get_all('products_attribute_value', '1', '*', $c['my_order'] . 'VId asc')); //属性选项
foreach ($value_row as $v) {
    $vid_data_ary[$v['AttrId']][$v['VId']] = $v["Value{$c['lang']}"];
}

$long = $user_row['Consumption'] / $next_level_row['FullPrice'] * 100;
($user_row['IsLocked'] || $long > 100) && $long = 100;
$Poor = $next_level_row['FullPrice'] - $user_row['Consumption'];
($user_row['IsLocked'] || $Poor < 0) && $Poor = 0;

$yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');
$balance = ly200::getyy_balance($yy_code['yy_code']);
?>


<?php
if ($yy_code['yy_code']) {
    ?>
    <div style="width: 100%; height: auto;">
        <iframe id='iframeBlance'
                src='http://47.106.88.138:8082/#/api/customer/info?customerNumber=<?= $yy_code['yy_code'] ?>'
                width="100%"
                height='1080'
                frameBorder='0'
                scrolling='yes'
                onLoad="resetHeight()"/>
    </div>
    <script type="text/javascript" language="javascript">
        function resetHeight() {
            var ifm = document.getElementById("iframeBlance");
            var subWeb = document.frames ? document.frames["iframeBlance"].document : ifm.contentDocument;
            if (ifm != null && subWeb != null) {
                ifm.height = subWeb.body.scrollHeight;
            }
        }
    </script>
    <?php
} else {
    ?>
    <div class="no_data">
        <div class="content_blank"><?= $c['lang_pack']['mobile']['no_data']; ?></div>
    </div>
    <?php
}
?>
