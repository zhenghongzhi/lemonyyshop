<?php !isset($c) && exit();?>
<?php
$CateId=(int)$_GET['CateId'];

$cate_row=str::str_code(db::get_limit('products_category', 'UId="0,"', '*',  $c['my_order'].'CateId asc', 0, 8));
$proid_row=db::get_all('sales_tuan', "{$c['time']} > StartTime and {$c['time']} < EndTime and BuyerCount<TotalCount", 'ProId');
$proid='0';
foreach((array)$proid_row as $v){
	$proid.=','.$v['ProId'];
}
$cateid_row=db::get_all('products', "ProId in({$proid})", 'CateId');
$cateid='0';
foreach((array)$cateid_row as $v){
	$cateid.=','.$v['CateId'];
}
$cate_topid_row=db::get_all('products_category', "CateId in({$cateid})", 'CateId,UId');
$cate_topid='0';
foreach((array)$cate_topid_row as $v){
	if($v['UId']=='0,'){
		$cate_topid.=','.$v['CateId'];
	}else{
		$uid = @explode(',', $v['UId']);
		$cate_topid.=','.$uid[1];
	}
}
?>
<script type="text/javascript">$(document).ready(function(){tuan_obj.tuan_init()});</script>
<div id="tuan" class="wide">
	<?php 
		$cur_lang=substr($c['lang'], 1);
		$ad=db::get_one('billing', 'Position=10 and Device=0');
		$ad_pic=str::json_data($ad['PicPath'], 'decode');
		$ad_url=$ad['Url'];
		if($ad['IsUsed'] && is_file($c['root_path'].$ad_pic[$cur_lang])){
		?>
		<div class="tuan_ad"><a href="<?=$ad_url?$ad_url:'javascript:;';?>"<?=$ad_url?' target="_blank"':'';?>><img src="<?=$ad_pic[$cur_lang];?>" alt="" /></a></div>
	<?php } ?>
	<div class="tuan_head">
		<div class="title fl"><?=$c['lang_pack']['groupBuy'];?></div>
		<div class="view fr" id="tuan_title">
			<a href="javascript:;" rel="nofollow" data-type="this" class="current"><?=$c['lang_pack']['groupThis'];?></a>
			<a href="javascript:;" rel="nofollow" data-type="previous"><?=$c['lang_pack']['groupPrevious'];?></a>
		</div>
		<div class="clear"></div>
    </div>
	<div class="tuan_menu">
		<div class="category" catalog="0" past="" page="0">
			<a href="javascript:;" data="0" title="<?=$c['lang_pack']['all_category'];?>" class="current"><?=$c['lang_pack']['all'];?></a>
			<?php
			$cate_row=str::str_code(db::get_all('products_category', 'UId="0," and IsSoldOut=0'." and CateId in({$cate_topid})", "CateId, Category{$c['lang']}",  $c['my_order'].'CateId asc'));
			foreach((array)$cate_row as $k=>$v){
			?>
				<a href="javascript:;" data="<?=$v['CateId'];?>" title="<?=$v['Category'.$c['lang']];?>"><?=$v['Category'.$c['lang']];?></a>
			<?php }?>
		</div>
		<div class="tuan_sort">
			<a href="javascript:;" data-sort="1"><?=$c['lang_pack']['price'];?><i class="icon_sort"></i></a>
			<a href="javascript:;" data-sort="2"><?=$c['lang_pack']['customer_review'];?><i class="icon_sort"></i></a>
			<a href="javascript:;" data-sort="3"><?=$c['lang_pack']['most_popular'];?><i class="icon_sort"></i></a>
		</div>
        <div class="clear"></div>
    </div>
	<div id="prolist" class="over" Num="0"></div>
</div>


