<?php !isset($c) && exit();?>
<?php 
$a=trim($_GET['a']);
$d=trim($_GET['d']);

if($a=='gallery' && $d=='gallery_list'){
	//买家秀
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php
	echo ly200::seo_meta($InfoId?$info_row:$info_category_ary[$UId][$CateId], $spare_ary);
	include("{$c['static_path']}/inc/static.php");
	echo ly200::load_static('/static/themes/default/css/plugins.css','/static/themes/default/js/plugins.js');
	?>
	</head>
	<body class="lang<?=$c['lang'];?>">
	<?php include("{$c['theme_path']}/inc/header.php");?>
	<div id="app_<?=$a;?>" class="app">
		<div class="wide">
			<?php 
				if($c['plugin_app']->trigger($a, '__config', $d)=='enable'){//app插件是否存在
					$c['plugin_app']->trigger($a, $d);
				}
			 ?>
		</div>
	</div>
	<?php include("{$c['theme_path']}/inc/footer.php");?>
	</body>
	</html>
<?php
}elseif($m=='user' && $a=='distribution' && $d){
	//分销
	if($c['plugin_app']->trigger('distribution', '__config', "DIST_user_distribution_{$d}")=='enable'){
		$c['plugin_app']->trigger('distribution', "DIST_user_distribution_{$d}");
	}
}?>