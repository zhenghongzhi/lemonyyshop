<?php !isset($c) && exit();?>
<?php

//模块设置
$cfg_module_row=db::get_value('config_module', "Themes='{$c['theme']}'", 'ListData');
$list_data=str::json_data($cfg_module_row, 'decode');
$cfg_module_ary=array();
foreach((array)$list_data as $k=>$v){
	$cfg_module_ary[$k]=$v;
}
substr_count($_SERVER['REQUEST_URI'], '/search/') && $cfg_module_ary['Narrow']=0;

include('static.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="<?=ly200::get_domain();?>/products/"/>
<?=ly200::seo_meta($category_row, $spare_ary);?>
<?php
    include("{$c['static_path']}/inc/static.php");
    echo ly200::load_static("/static/themes/default/css/products/request/mini-upload.css");
    echo ly200::load_static("/static/themes/default/css/products/request/rfq.css");
    echo ly200::load_static("/static/themes/default/css/products/request/animate.css");
    echo ly200::load_static("/static/themes/default/css/products/request/global.css");
    echo ly200::load_static("/static/themes/default/css/products/request/loading.css");
    echo ly200::load_static('/static/js/plugin/lightbox/js/lightbox.min.js','/static/js/plugin/lightbox/css/lightbox.min.css');
    echo ly200::load_static("/static/themes/default/js/products/request/rfq.js");
    echo ly200::load_static("/static/themes/default/js/products/request/lrz.bundle.js");
    echo ly200::load_static("/static/themes/default/js/products/request/loading.js");

?>

<?php
if(in_array('facebook_pixel', $c['plugins']['Used']) && substr_count($_SERVER['REQUEST_URI'], '/search/')){
	//Facebook Pixel
?>
	<!-- Facebook Pixel Code -->
	<script type="text/javascript">
	<!-- When a search is made, such as a product query. -->
	fbq('track', 'Search', {
		search_string: '<?=str::str_code(stripslashes($_GET['Keyword']));?>',//搜索关键词
		content_category: 'Product Search',//分类
		content_ids: ['0'],
		value: '0.00',//数值
		currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
	});
	</script>
	<!-- End Facebook Pixel Code -->
<?php }?>
</head>
<script type="text/javascript">
    $(function (){
        user_obj.user_purchase()
    });
</script>
<body class="lang<?=$c['lang'];?>">
<?php
include("{$c['theme_path']}/inc/header.php");
?>
<div id="main" class="wide">

    <div id="wrap_rfq">

        <form id="rfq_from" class="frm_rfq" method="post" name="frm_rfq"  enctype="multipart/form-data">
            <h2 class="title"><?=$c['lang_pack']['products']['rfqTitle'];?></h2>
            <div class="half_line" ></div>
            <div class="clear"></div>
            <div class="row">
                <label style="font-size: 16px;">
                    <?=$c['lang_pack']['products']['rfqComplete'];?>
                </label>
                <label style="font-size: 12px; margin-top: 10px;">
                    <?=$c['lang_pack']['products']['rfqTips'];?>
                </label>
            </div>

            <div id="error_register_box" class="error_note_box"></div>
            <div class="clear"></div>

            <div class="row">
                <label for="quantity">
                    <?=$c['lang_pack']['products']['quantity'];?>
                    <span class="fc_red">*</span>
                </label>
                <input style="width: 270px;" id="quantity" name="quantity" class="lib_txt fl " type="text"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" notnull/>
                <select style="width: 270px;" class="fl" id="quantityUtil" name="quantityUtil" notnull>
                    <option value="pieces"><?=$c['lang_pack']['products']['pieces'];?></option>
                    <option value="hq_container_40"><?=$c['lang_pack']['products']['hq_container_40'];?></option>
                    <option value="bags"><?=$c['lang_pack']['products']['bags'];?></option>
                    <option value="boxes"><?=$c['lang_pack']['products']['boxes'];?></option>
                    <option value="foot"><?=$c['lang_pack']['products']['foot'];?></option>
                    <option value="liter"><?=$c['lang_pack']['products']['liter'];?></option>
                    <option value="meter"><?=$c['lang_pack']['products']['meter'];?></option>
                    <option value="mille"><?=$c['lang_pack']['products']['mille'];?></option>
                    <option value="pack"><?=$c['lang_pack']['products']['pack'];?></option>
                    <option value="pairs"><?=$c['lang_pack']['products']['pairs'];?></option>
                    <option value="tons"><?=$c['lang_pack']['products']['tons'];?></option>
                    <option value="square_feet"><?=$c['lang_pack']['products']['square_feet'];?></option>
                    <option value="square_inch"><?=$c['lang_pack']['products']['square_inch'];?></option>
                    <option value="square_meters"><?=$c['lang_pack']['products']['square_meters'];?></option>
                    <option value="square_mile"><?=$c['lang_pack']['products']['square_mile'];?></option>
                    <option value="square_yard"><?=$c['lang_pack']['products']['square_yard'];?></option>
                    <option value="unit"><?=$c['lang_pack']['products']['unit'];?></option>
                    <option value="yard"><?=$c['lang_pack']['products']['yard'];?></option>
                </select>

                <div class="error_info fl">
                    <img src="/static/ico/error.png" alt="">
                    <p class="error"></p>
                </div>

            </div>
            <div class="clear"></div>
            <div class="row">
                <label for="productName"><?=$c['lang_pack']['products']['productName'];?><span class="fc_red">*</span></label>
                <input style="width: 540px;" name="productName" id="productName" class="lib_txt lib_input fl" type="text" maxlength="20" notnull/>
                <div class="error_info fl">
                    <img src="/static/ico/error.png" alt="">
                    <p class="error"></p>
                </div>
            </div>
            <div class="clear"></div>

            <div class="row">
                <label for="description"><?=$c['lang_pack']['products']['description'];?><span class="fc_red">*</span></label>
                <textarea style="width: 540px;" name="description" id="description" class="lib_txt lib_input box_textarea fl" rows="3" maxlength="500" notnull></textarea>
                <div class="error_info fl" style="margin-top: 40px;">
                    <img src="/static/ico/error.png" alt="">
                    <p class="error"></p>
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
                <label for="productURL"><?=$c['lang_pack']['products']['productURL'];?></label>
                <input style="width: 540px;" name="productURL" id="productURL" class="lib_txt lib_input" type="text" />
            </div>
            <div class="row">
                <label for="referQuotation"><?=$c['lang_pack']['products']['referQuotation'];?></label>
                <input style="width: 480px;" name="referQuotation" id="referQuotation" class="lib_txt" type="text" size="30" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                <span class="lib_txt" style="font-size: 16px; padding: 9px 8px;"><?=$c['lang_pack']['products']['RMB'];?></span>
            </div>
            <div class="clear"></div>
            <div class="row">
                <label for="productImage"><?=$c['lang_pack']['products']['productImage'];?></label>
                <label><?=$c['lang_pack']['products']['productImageTip'];?></label>
                <div id="productImage">
                    <div class="input upload_box">
                        <input class="upload_file" type="file" name="picPath_0" onchange="loadImgPreVView(this);" accept="image/gif,image/jpeg,image/png">
                        <div id="pic_show" class="pic_box"></div>
                        <div class="hint_text"><?=$c['lang_pack']['upload_attachments']?></div>
                        <div class="btn_delete hidden" onclick="removeBox(this);"></div>
                    </div>


                </div>

            </div>
            <div class="clear"></div>
            <div class="row">
                <label for="factoryInformation"><?=$c['lang_pack']['products']['factoryInformation'];?></label>
                <input style="width: 540px;" name="factoryInformation" id="factoryInformation" class="lib_txt lib_input" type="text" />
            </div>
            <div class="clear"></div>
            <div class="row"><button class="subbtn wrap_rfq form_button_bg btn_submit" type="submit"><?=$c['lang_pack']['products']['submitRFQ'];?></button></div>
            <input type="hidden" id="fileCount" name="fileCount" />
            <input type="hidden" id="source" name="1" />
            <input type="hidden" name="jumpUrl" value="/account/rfq/" />
            <input type="hidden" name="BackUrl" value="<?=$_SERVER['HTTP_REFERER'];?>" />
            <input type="hidden" name="do_action" value="user.purchease_submit" />
        </form>

    </div>

	<div class="blank25"></div>
</div>
<?php include("{$c['theme_path']}/inc/footer.php");?>
<?php
//列表页特效跟随主色调
$cfg_module_style=db::get_value('config_module', 'IsDefault=1', 'StyleData');
$cfg_module_style=str::json_data($cfg_module_style, 'decode');
?>
<style>
/*    add by jay:注释掉prod_list*/
/*.prod_list .prod_box.hover_1 .prod_box_pic{ border-color:*/<?//=$cfg_module_style['FontColor']?>/*;}*/
/*.prod_list .prod_box.hover_1 .prod_box_info{ background:*/<?//=$cfg_module_style['FontColor']?>/*;}*/
/*.prod_list .prod_box .prod_box_view{ background:*/<?//=$cfg_module_style['FontColor']?>/*;}*/
/*.prod_list .prod_box.hover_1 .prod_box_view{ border-top:1px solid #fff;}*/
/*.prod_list .prod_box .prod_box_button{ border-top:1px #fff solid;}*/
/*.prod_list .prod_box .prod_box_button>div{ border-right:1px solid #fff;}*/
</style>

<script type="text/javascript">


</script>
</body>
</html>