<?php !isset($c) && exit();?>
<div class="market_prod_list clearfix">
	<?php
    if(method_exists('themes_set', 'themes_products_view')){
		echo themes_set::themes_products_view($products_list_row[0], 4);
	}else{
        foreach((array)$products_list_row[0] as $k => $v){
            $url=ly200::get_url($v, 'products');
            $img=ly200::get_size_img($v['PicPath_0'], '240x240');
            $name=$v['Name'.$c['lang']];
            $price_ary=cart::range_price_ext($v);
            $price_0=$price_ary[1];
            $is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
            $promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
            if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
            $pro_info=ly200::get_pro_info($v);
            $rating=$pro_info[2];
            $total_rating=$pro_info[3];
        ?>
        <dl class="pro_item<?=$k%4==0?' fir':'';?>">
            <dt>
                <a class="pic_box" href="<?=$url;?>" title="<?=$name;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a>
                <div class="view trans"><a href="javascript:;" class="add_cart" data="<?=$v['ProId']; ?>" title="<?=$name;?>"><?=$c['lang_pack']['products']['quickView']; ?></a></div>
                <?php if($is_promition && $promotion_discount){?>
                    <em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>%<br />OFF</em>
                    <em class="icon_discount_foot DiscountBorderColor"></em>
                <?php }?>
                <em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
            </dt>
            <dd class="desc_box">
                <div class="pro_name"><a class="themes_title" href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
                <div class="themes_price pro_price">
                    <em class="currency_data PriceColor"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data PriceColor" data="<?=$price_ary[0];?>" data-mall-price="<?=$v['Price_0']>0?$v['Price_0']:$v['Price_1'];?>" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span>
                    <?php if($price_ary[2]){?>
                        <del class="themes_subtitle"><em class="currency_data"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data" data="<?=$price_0;?>"><?=cart::iconv_price($price_0, 2);?></span></del>
                    <?php }?>
                </div>
                <?php if($c['config']['products_show']['Config']['review']){?>
                    <div class="themes_subtitle pro_view">
                        <?=($c['config']['products_show']['Config']['favorite']?'<span class="trans favorite add_favorite fr'.(in_array($v['ProId'], $user_favorite_ary)?' is_in':'').'" data="'.$v['ProId'].'">'.$c['lang_pack']['favorite'].'</span>':'');?>
						<?php
						if($total_rating){
							echo '<span class="trans review">'.html::review_star($rating);
							echo '<a class="review_count" href="'.$url.'#review_box">('.$total_rating.')</a></span>';
						}?>
                    </div>
                <?php }?>
            </dd>
        </dl>
    <?php
		}
	}
	?>
</div>
<?php if($products_list_row[3]>1){?>
	<div id="turn_page"><?=ly200::turn_page_html($products_list_row[1], $products_list_row[2], $products_list_row[3], $no_page_url, $c['lang_pack']['previous'], $c['lang_pack']['next']);?></div>
<?php }?>