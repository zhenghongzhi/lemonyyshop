<?php !isset($c) && exit();?>
<div class="sort_by_box">
	<span class="count themes_subtitle"> <?=(int)$products_list_row[1].' '.$c['lang_pack']['cart']['products']; ?></span>
	<span><?=$c['lang_pack']['products']['sort_by']; ?></span>
	<?php 
		$sort_ary=array(
			'1d'	=>	$c['lang_pack']['most_popular'],
			'2d'	=>	$c['lang_pack']['sales'],
			'3d'	=>	$c['lang_pack']['fav'],
			'4d'	=>	$c['lang_pack']['new'],
			'5d'	=>	$c['lang_pack']['price'],
			'5a'	=>	$c['lang_pack']['price'],
		); 
	?>
	<dl class="select_box">
		<dt><?=$sort_ary[$Sort] ? $sort_ary[$Sort] . ($Sort=='5d'?'<i class="sort_icon_arrow_down"></i>':'') . ($Sort=='5a'?'<i class="sort_icon_arrow_up"></i>':'') : $c['lang_pack']['mobile']['default'] ; ?></dt>	
		<dd class="themes_bor">
			<a rel="nofollow"<?=$Sort=='1a'?' class="cur"':'';?> href="<?=$no_sort_url;?>"><em class="sort_icon_popular"></em><?=$c['lang_pack']['mobile']['default'];?></a>
			<a rel="nofollow"<?=$Sort=='1d'?' class="cur"':'';?> href="<?=$no_sort_url.'&Sort=1d';?>"><em class="sort_icon_popular"></em><?=$c['lang_pack']['most_popular'];?></a>
            <?php if (!@in_array('batch_edit', $c['plugins']['Used'])) { ?>
    			<a rel="nofollow"<?=$Sort=='2d'?' class="cur"':'';?> href="<?=$no_sort_url.'&Sort=2d';?>"><em class="sort_icon_sales"></em><?=$c['lang_pack']['sales'];?></a>
    			<a rel="nofollow"<?=$Sort=='3d'?' class="cur"':'';?> href="<?=$no_sort_url.'&Sort=3d';?>"><em class="sort_icon_favorites"></em><?=$c['lang_pack']['fav'];?></a>
            <?php } ?>
			<a rel="nofollow"<?=$Sort=='4d'?' class="cur"':'';?> href="<?=$no_sort_url.'&Sort=4d';?>"><em class="sort_icon_new"></em><?=$c['lang_pack']['new'];?></a>
			<a rel="nofollow"<?=($Sort=='5d' || $Sort=='5a')?' class="cur"':'';?> href="<?=$no_sort_url.'&Sort='.($Sort=='5a'?'5d':'5a');?>"><em class="sort_icon_price"></em><?=$c['lang_pack']['price'];?><i class="<?php if($Sort=='5d') echo 'sort_icon_arrow_down'; elseif($Sort=='5a') echo 'sort_icon_arrow_up'; else echo 'sort_icon_arrow';?>"></i></a>
		</dd>
	</dl>
	<div class="clear"></div>
</div>
<div class="brand_prod_list">
	<?php
    if(method_exists('themes_set', 'themes_products_view')){
		echo themes_set::themes_products_view($products_list_row[0], 4);
	}else{
        foreach((array)$products_list_row[0] as $v){
            $url=ly200::get_url($v, 'products');
            $img=ly200::get_size_img($v['PicPath_0'], '500x500');
            $name=$v['Name'.$c['lang']];
            $price_ary=cart::range_price_ext($v);
            $price_0=$price_ary[1];
            $is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
            $promotion_discount=@round(sprintf('%01.2f', ($price_0-$price_ary[0])/$price_0*100));
            if($v['PromotionType']) $promotion_discount=100-$v['PromotionDiscount'];
            $pro_info=ly200::get_pro_info($v);
            $rating=$pro_info[2];
            $total_rating=$pro_info[3];
        ?>
        <dl class="prod_box pro_item trans">
            <dt>
                <a class="pic_box" href="<?=$url;?>" title="<?=$name;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a>
                <?php if($is_promition && $promotion_discount){?>
                    <em class="icon_discount DiscountBgColor"><b><?=$promotion_discount;?></b>% off</em>
                <?php }?>
                <em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
                <?php if($c['config']['products_show']['Config']['favorite']){?>
                    <span class="favorite trans add_favorite<?=in_array($v['ProId'], $user_favorite_ary)?' is_in':'';?>" data="<?=$v['ProId'];?>"></span>
                <?php }?>
                <div class="view trans"><a href="javascript:;" class="add_cart" data="<?=$v['ProId'];?>" title="<?=$name;?>"><?=$c['lang_pack']['products']['quickView'];?></a></div>
            </dt>
            <dd class="desc_box">
                <div class="pro_name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
                <div class="pro_view">
                    <?=$rating?html::review_star($rating):''; ?>
                </div>
                <div class="pro_price">
                    <em class="currency_data PriceColor"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data PriceColor" data="<?=$price_ary[0];?>" data-mall-price="<?=$v['Price_0']>0?$v['Price_0']:$v['Price_1'];?>" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span>
                    <?php if($price_ary[2]){?><del><em class="currency_data"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data" data="<?=$price_0;?>"><?=cart::iconv_price(($price_0), 2);?></span></del><?php }?>
                </div>
            </dd>
        </dl>
    <?php
		}
	}
	?>    
	<div class="clear"></div>
</div>
<?php if($products_list_row[3]>1){?>
	<div id="turn_page" class="themes_subtitle"><?=ly200::turn_page_html($products_list_row[1], $products_list_row[2], $products_list_row[3], $no_page_url, $c['lang_pack']['previous'], $c['lang_pack']['next']);?></div>
<?php }?>