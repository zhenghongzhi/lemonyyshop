<?php !isset($c) && exit();?>
<?php
$ext_ary = array();
$isHaveAttr = (int)($attr_ary['Cart'] && $products_row['AttrId'] == ($TopCategory_row ? $TopCategory_row['AttrId'] : $category_row['AttrId'])); //是否有规格属性
if ($isHaveAttr || $isHaveOversea) {
    //初始化
	$combinatin_ary = array();
    $all_value_ary = array();
    $attrid = array();
	foreach ($attr_ary['Cart'] as $v) {
        $attrid[] = $v['AttrId'];
    }
	$attrid_list = implode(',', $attrid);
	!$attrid_list && $attrid_list = 0;
	$value_row = str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'] . 'VId asc')); //属性选项
	foreach ($value_row as $v) {
        $all_value_ary[$v['AttrId']][$v['VId']] = $v;
    }
	//属性组合数据
	$combinatin_row = str::str_code(db::get_all('products_selected_attribute_combination', "ProId='{$ProId}'", '*', 'CId asc'));
	foreach ($combinatin_row as $v) {
		$combinatin_ary[$v['Combination']][$v['OvId']] = array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
		$key = str_replace('|', '_', substr($v['Combination'], 1, -1));
		$v['OvId'] < 1 && $v['OvId'] = 1;
		$IsCombination == 1 && $key .= ($key ? '_' : '') . 'Ov:' . $v['OvId'];
		$ext_ary[$key] = array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
	}
?>

	<ul class="widget attributes" default_selected="<?=(int)$c['config']['products_show']['Config']['selected'];?>" data-combination="<?=$IsCombination;?>" data-stock="<?=(int)$products_row['SoldStatus'];?>" data-attr-price="<?=(int)$products_row['IsOpenAttrPrice'];?>">
		<div class="attr_sure"><span class="attr_sure_choice"><?=$c['lang_pack']['products']['attributes_tips'];?></span><span class="attr_sure_close">X</span></div>
		<?php
		$num = 0;
		foreach ((array)$attr_ary['Cart'] as $k => $v) {
			if (!$selected_ary['Id'][$v['AttrId']]) continue; //踢走
			foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
				if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
				$num++;
			}
		}
		$c['config']['products_show']['Config']['attr']=($c['themes_type'] == 2 ? 0 : 1);
		$isMaxAttr = ($num < 30);
		foreach ((array)$attr_ary['Cart'] as $k => $v) {
			if (!$selected_ary['Id'][$v['AttrId']]) continue; //踢走
			$attr_value_count = 0;
			foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
				if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
				$attr_value_count++;
			}
			if (!$attr_value_count) continue;
			$new_value_ary = array();//重新定义，属性选项排序
			foreach ((array)$all_value_ary[$v['AttrId']] as $k2 => $v2) {
				if (!in_array($k2, $selected_ary['Id'][$v['AttrId']])) continue; //踢走
				$MyOrder = (int)$selected_ary['MyOrder'][$k2];
				if ($MyOrder) {
					$new_value_ary[$v['AttrId']][$MyOrder] = array('VId'=>$k2, 'Data'=>$v2);
				} else {
					$new_value_ary[$v['AttrId']][] = array('VId'=>$k2, 'Data'=>$v2);
				}
				ksort($new_value_ary[$v['AttrId']]);
			}
			$IsColor = in_array($v['AttrId'], $color_attr_ary) ? 1 : 0;
			if ($c['config']['products_show']['Config']['attr']) {
				if ($isMaxAttr) {
		?>

					<li class="attr_show<?=$IsColor ? ' attr_show_color' : '';?>" name="<?=$v['Name' . $c['lang']];?>">
						<h5><?=$v['Name' . $c['lang']];?>:</h5>
                        <br>
                        <br>
						<?php
						foreach ((array)$new_value_ary[$v['AttrId']] as $k2 => $v2) {
							if (!in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])) continue; //踢走
							$value = $combinatin_ary["|{$v2['VId']}|"][1];
							$price = (float)$value[0];
							$qty = (int)$value[1];
							$weight = (float)$value[2];
							$sku = $value[3];
							$increase = (int)$value[4];
						?>
							<div value="<?=$v2['VId'];?>" data="<?=htmlspecialchars('{"Price":' . $price . ',"Qty":' . $qty . ',"Weight":' . $weight . ',"SKU":' . $sku . ',"IsIncrease":' . $increase . '}');?>" class="zhzshuxing GoodBorderColor GoodBorderHoverColor<?=((int)$products_row['SoldStatus'] == 0 && $IsCombination && $value && $qty < 1) ? ' out_stock' : '';?><?=$color_picpath_ary[$v2['VId']] ? ' himg' : '';?>" title="<?=($v2['Data']['Value' . $c['lang']]);?>">
								<?php
								if ($color_picpath_ary[$v2['VId']]) {
									echo '<a class="attr_pic"><img src="' . ly200::get_size_img($color_picpath_ary[$v2['VId']], '240x240') . '" alt="' . $v2['Data']['Value' . $c['lang']] . '" /></a>';
								} else {
									echo $v2['Data']['Value' . $c['lang']];
								}?>
                                <input type="hidden" name="zhzid[][<?=$v['AttrId'];?>]" id="zhzattr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="<?=$v2['VId']; ?>" class="attr_value<?=$IsColor ? ' colorid' : '';?>" />

                            </div>
                            <div class="widget prod_info_quantity">
                                <div class="qty_box"><div id="btn_cut">-</div></div>
                                <div class="quantity_box" data="<?=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?>"><input class="qty_num zhzquantity" name="Qty[]" autocomplete="off" type="text" value="0" stock="<?=$Max;?>" /></div>
                                <div class="qty_box"><div id="btn_add">+</div></div>
                                <span class="themes_subtitle prod_info_inventory">(<?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?>)</span>
                                <div class="clear"></div>
                            </div>
                            <br>
						<?php }?>
						<input type="hidden" name="id[<?=$v['AttrId'];?>]" id="attr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="" class="attr_value<?=$IsColor ? ' colorid' : '';?>" />
					</li>
		<?php
				} else {
		?>

                    <li class="attr_show<?=$IsColor ? ' attr_show_color' : '';?>" name="<?=$v['Name' . $c['lang']];?>">
                        <h5><?=$v['Name' . $c['lang']];?>:</h5>
                        <?php
                        foreach ((array)$new_value_ary[$v['AttrId']] as $k2 => $v2) {
                            if (!in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])) continue; //踢走
                            $value = $combinatin_ary["|{$v2['VId']}|"][1];
                            $price = (float)$value[0];
                            $qty = (int)$value[1];
                            $weight = (float)$value[2];
                            $sku = $value[3];
                            $increase = (int)$value[4];
                            ?>
                            <div value="<?=$v2['VId'];?>" data="<?=htmlspecialchars('{"Price":' . $price . ',"Qty":' . $qty . ',"Weight":' . $weight . ',"SKU":' . $sku . ',"IsIncrease":' . $increase . '}');?>" class="zhzshuxing GoodBorderColor GoodBorderHoverColor<?=((int)$products_row['SoldStatus'] == 0 && $IsCombination && $value && $qty < 1) ? ' out_stock' : '';?><?=$color_picpath_ary[$v2['VId']] ? ' himg' : '';?>" title="<?=($v2['Data']['Value' . $c['lang']]);?>">
                                <?php
                                if ($color_picpath_ary[$v2['VId']]) {
                                    echo '<a class="attr_pic"><img src="' . ly200::get_size_img($color_picpath_ary[$v2['VId']], '240x240') . '" alt="' . $v2['Data']['Value' . $c['lang']] . '" /></a>';
                                } else {
                                    echo $v2['Data']['Value' . $c['lang']];
                                }?>
                                <input type="hidden" name="zhzid[][<?=$v['AttrId'];?>]" id="zhzattr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="<?=$v2['VId']; ?>" class="attr_value<?=$IsColor ? ' colorid' : '';?>" />

                            </div>
                            <div class="widget prod_info_quantity">
                                <div class="qty_box"><div id="btn_cut">-</div></div>
                                <div class="quantity_box" data="<?=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?>"><input class="qty_num zhzquantity" name="Qty[]" autocomplete="off" type="text" value="0" stock="<?=$Max;?>" /></div>
                                <div class="qty_box"><div id="btn_add">+</div></div>
                                <span class="themes_subtitle prod_info_inventory">(<?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?>)</span>
                                <div class="clear"></div>
                            </div>
                            <br>
                        <?php }?>
                        <input type="hidden" name="id[<?=$v['AttrId'];?>]" id="attr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>" value="" class="attr_value<?=$IsColor ? ' colorid' : '';?>" />
                    </li>
					<!-- <li class="attr_select" name="<?=$v['Name' . $c['lang']];?>">
						<h5><?=$v['Name' . $c['lang']];?>:</h5>
						<select name="id[<?=$v['AttrId'];?>]" id="attr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>"<?=$IsColor ? ' class="colorid"' : '';?>>
							<option value=""><?=str_replace('%name%', $v['Name' . $c['lang']], $c['lang_pack']['products']['select']);?></option>
							<?php
							foreach ((array)$new_value_ary[$v['AttrId']] as $k2 => $v2) {
								if (!in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])) continue; //踢走
								$value = $combinatin_ary["|{$v2['VId']}|"][1];
								$price = (float)$value[0];
								$qty = (int)$value[1];
								$weight = (float)$value[2];
								$sku = $value[3];
								$increase = (int)$value[4];
							?>
							    <option value="<?=$v2['VId'];?>" data-title="<?=$v2['Data']['Value' . $c['lang']];?>" data="<?=htmlspecialchars('{"Price":' . $price . ',"Qty":' . $qty . ',"Weight":' . $weight . ',"SKU":' . $sku . ',"IsIncrease":' . $increase . '}');?>"<?=((int)$products_row['SoldStatus'] == 0 && $IsCombination && $value && $qty < 1) ? ' class="hide" disabled' : '';?>><?=$v2['Data']['Value' . $c['lang']] . ' ' . ($price > 0 && (($IsCombination == 1 && $increase == 1) || $products_row['IsOpenAttrPrice'] == 1) ? ' (+' . cart::iconv_price($price) . ')' : '');?></option>
							<?php }?>
						</select>
					</li> -->
		<?php
				}
			} else {
	    ?>

				<li class="attr_select themes_bor" name="<?=$v['Name' . $c['lang']];?>">
					<label for="<?=$v['Name' . $c['lang']];?>"><?=$v['Name' . $c['lang']];?>:</label>
					<select name="id[<?=$v['AttrId'];?>]" id="attr_<?=$v['AttrId'];?>" attr="<?=$v['AttrId'];?>"<?=$IsColor ? ' class="colorid"' : '';?>>
						<option value=""><?=str_replace('%name%', $v['Name' . $c['lang']], $c['lang_pack']['products']['select']);?></option>
						<?php
						foreach ((array)$new_value_ary[$v['AttrId']] as $k2 => $v2) {
							if (!in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])) continue; //踢走
							$value = $combinatin_ary["|{$v2['VId']}|"][1];
							$price = (float)$value[0];
							$qty = (int)$value[1];
							$weight = (float)$value[2];
							$sku = $value[3];
							$increase = (int)$value[4];
						?>
						    <option value="<?=$v2['VId'];?>" data-title="<?=$v2['Data']['Value' . $c['lang']];?>" data="<?=htmlspecialchars('{"Price":' . $price . ',"Qty":' . $qty . ',"Weight":' . $weight . ',"SKU":' . $sku . ',"IsIncrease":' . $increase . '}');?>"<?=((int)$products_row['SoldStatus'] == 0 && $IsCombination && $value && $qty < 1) ? ' class="hide" disabled' : '';?>><?=$v2['Data']['Value' . $c['lang']] . ' ' . ($price > 0 && (($IsCombination == 1 && $increase == 1) || $products_row['IsOpenAttrPrice'] == 1) ? ' (+' . cart::iconv_price($price) . ')' : '');?></option>
						<?php }?>
					</select>
				</li>
		<?php
			}
		}?>

		<?php
		if ($isHaveOversea ){
			if ($c['config']['products_show']['Config']['attr']) {
				if ($isMaxAttr) {
		?>
					<li class="attr_show" name="<?=$c['lang_pack']['products']['shipsFrom'];?>" style="display:<?=((int)$c['config']['global']['Overseas']==1 && count($selected_ary['Overseas'])>1)?'block':'none';?>;">
						<h5><?=$c['lang_pack']['products']['shipsFrom'];?>:</h5>
                        <br>
                        <br>
						<?php
						foreach($c['config']['Overseas'] as $k=>$v){
							$Ovid='Ov:'.$v['OvId'];
							if(!$selected_ary['Overseas'] && $v['OvId']>1) continue; //踢走
							if($selected_ary['Overseas'] && !in_array($v['OvId'], $selected_ary['Overseas'])) continue; //踢走
							$value=$combinatin_ary['||'][$v['OvId']];
							$price=(float)$value[0];
							$qty=(int)$value[1];
							$weight=(float)$value[2];
							$sku=$value[3];
							$increase=(int)$value[4];
						?>
							<div value="<?=$Ovid;?>" data="<?=htmlspecialchars('{"Price":'.$price.',"Qty":'.$qty.',"Weight":'.$weight.',"SKU":'.$sku.',"IsIncrease":'.$increase.'}');?>" class="GoodBorderColor GoodBorderHoverColor<?=((int)$products_row['SoldStatus']==0 && $IsCombination && $value && $qty<1)?' out_stock':'';?>" title="<?=($v['Name'.$c['lang']]);?>"><?=$v['Name'.$c['lang']];?></div>
						<?php }?>
						<input type="hidden" name="id[Overseas]" id="attr_Overseas" attr="Overseas" value="" class="attr_value" />
					</li>
		<?php
				}else{
		?>
					<li name="<?=$c['lang_pack']['products']['shipsFrom'];?>" class="attr_select" style="display:<?=((int)$c['config']['global']['Overseas']==1 && count($selected_ary['Overseas'])>1)?'block':'none';?>;">
						<h5><?=$c['lang_pack']['products']['shipsFrom'];?>:</h5>
						<select name="id[Overseas]" id="attr_Overseas" attr="Overseas">
							<option value=""><?=str_replace('%name%', $c['lang_pack']['products']['shipsFrom'], $c['lang_pack']['products']['select']);?></option>
							<?php
							foreach($c['config']['Overseas'] as $k=>$v){
								$Ovid='Ov:'.$v['OvId'];
								if(!$selected_ary['Overseas'] && $v['OvId']>1) continue; //踢走
								if($selected_ary['Overseas'] && !in_array($v['OvId'], $selected_ary['Overseas'])) continue; //踢走
								$value=$combinatin_ary['||'][$v['OvId']];
								$price=(float)$value[0];
								$qty=(int)$value[1];
								$weight=(float)$value[2];
								$sku=$value[3];
								$increase=(int)$value[4];
							?>
							    <option value="<?=$Ovid;?>" data="<?=htmlspecialchars('{"Price":'.$price.',"Qty":'.$qty.',"Weight":'.$weight.',"SKU":'.$sku.',"IsIncrease":'.$increase.'}');?>"<?=((int)$c['config']['products_show']['Config']['stock'] && $IsCombination && $value && $qty<1)?' class="hide" disabled':'';?>><?=$v['Name'.$c['lang']].' '.($price > 0 && (($IsCombination == 1 && $increase == 1) || $products_row['IsOpenAttrPrice'] == 1) ? ' (+' . cart::iconv_price($price) . ')' : '');?></option>
							<?php }?>
						</select>
					</li>
		<?php
				}
			} else {
		?>

				<li name="<?=$c['lang_pack']['products']['shipsFrom'];?>" class="attr_select themes_bor" style="display:<?=((int)$c['config']['global']['Overseas']==1 && count($selected_ary['Overseas'])>1)?'block':'none';?>;">
					<label for="<?=$c['lang_pack']['products']['shipsFrom'];?>"><?=$c['lang_pack']['products']['shipsFrom'];?>:</label>
					<select name="id[Overseas]" id="attr_Overseas" attr="Overseas">
						<option value=""><?=str_replace('%name%', $c['lang_pack']['products']['shipsFrom'], $c['lang_pack']['products']['select']);?></option>
						<?php
						foreach($c['config']['Overseas'] as $k=>$v){
							$Ovid='Ov:'.$v['OvId'];
							if(!$selected_ary['Overseas'] && $v['OvId']>1) continue; //踢走
							if($selected_ary['Overseas'] && !in_array($v['OvId'], $selected_ary['Overseas'])) continue; //踢走
							$value=$combinatin_ary['||'][$v['OvId']];
							$price=(float)$value[0];
							$qty=(int)$value[1];
							$weight=(float)$value[2];
							$sku=$value[3];
							$increase=(int)$value[4];
						?>
						    <option value="<?=$Ovid;?>" data="<?=htmlspecialchars('{"Price":'.$price.',"Qty":'.$qty.',"Weight":'.$weight.',"SKU":'.$sku.',"IsIncrease":'.$increase.'}');?>"<?=((int)$c['config']['products_show']['Config']['stock'] && $IsCombination && $value && $qty<1)?' class="hide" disabled':'';?>><?=$v['Name'.$c['lang']].' '.($price > 0 && (($IsCombination == 1 && $increase == 1) || $products_row['IsOpenAttrPrice'] == 1) ? ' (+' . cart::iconv_price($price) . ')' : '');?></option>
						<?php }?>
					</select>
				</li>
		<?php
			}
		}?>
        <!--add by zhz start  -->
        <?php if (!$combinatin_row){ ?>
            <h5><?=$c['lang_pack']['products']['qty'];?>:</h5>
            <div class="widget prod_info_quantity">
                <div class="qty_box"><div id="btn_cut">-</div></div>
                <div class="quantity_box" data="<?=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?>"><input id="quantity" class="qty_num zhzquantity" name="Qty[]" autocomplete="off" type="text" value="<?=$MOQ;?>" stock="<?=$Max;?>" /></div>
                <div class="qty_box"><div id="btn_add">+</div></div>
                <span class="themes_subtitle prod_info_inventory">(<?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?>)</span>
                <div class="clear"></div>
            </div>
        <?php } ?>
        <!--add by zhz end    -->
	</ul>
<?php }?>