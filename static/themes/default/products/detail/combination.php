<?php !isset($c) && exit();?>
<?php
//产品数据
$proid_ary=array();
foreach((array)$group_promotion_ary as $v){
	foreach($v['data'] as $key=>$val){
		$proid_ary[]=$val['ProId'];
		$pakeageId=@explode('|', trim($val['PackageProId'], '|'));
		$proid_ary=@array_merge($proid_ary, (array)$pakeageId);
	}
}
$proid_list=@implode(',', $proid_ary);
!$proid_list && $proid_list="-1";
//初始化
$c_all_attr_ary=$c_all_value_ary=$c_selected_ary=$vid_data_ary=$combinatin_ary=$ext_ary=$cate_where_ary=$c_color_ary=array();
//产品相关的产品分类数据
$cate_row=db::get_all('products_category', "CateId in(select CateId from products where ProId in($proid_list))", 'UId, AttrId, CateId');
foreach($cate_row as $v){
	if($v['CateId']){
		$cate_where_ary[]="CateId like '%|{$v['CateId']}|%'";
		if($v['UId']!='0,'){
			$c_TopCateId=category::get_top_CateId_by_UId($v['UId']);//寻找顶级分类
			$cate_where_ary[]="CateId like '%|{$c_TopCateId}|%'";
		}
	}
}
$cate_where=implode(' or ', $cate_where_ary);
!$cate_where && $cate_where=1;
//规格属性数据
$cart_attr_row=str::str_code(db::get_all('products_attribute', "CartAttr=1 and ($cate_where)", "AttrId, Name{$c['lang']}", $c['my_order'].'AttrId asc')); //所有购物车属性
$_attribute_value_where='-1';
foreach((array)$cart_attr_row as $v){
	$c_all_attr_ary[$v['AttrId']]=$v;
	$_attribute_value_where.=",{$v['AttrId']}";
}
$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in($_attribute_value_where)", '*', $c['my_order'].'VId asc')); //所有属性选项
foreach($value_row as $v){
	$c_all_value_ary[$v['AttrId']][$v['VId']]=$v;
	$vid_data_ary[$v['VId']]=$v;
}
$selected_row=db::get_all('products_selected_attribute', "IsUsed=1 and ProId in($proid_list) and AttrId in($_attribute_value_where, 0)", 'ProId, AttrId, VId, OvId', 'SeleteId asc');
foreach($selected_row as $v){
	if($v['AttrId']==0 && $v['VId']==0 && $v['OvId']>=0){//记录勾选属性ID 发货地
		$c_selected_ary[$v['ProId']]['Overseas'][]=$v['OvId'];
	}else{
		$c_selected_ary[$v['ProId']]['Id'][$v['AttrId']][]=$v['VId'];
	}
}
//属性组合数据
$combinatin_row=str::str_code(db::get_all('products_selected_attribute_combination', "ProId in($proid_list)", '*', 'CId asc'));
foreach($combinatin_row as $v){
	$combinatin_ary[$v['ProId']][$v['Combination']][$v['OvId']]=array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
	$key=str_replace('|', '_', substr($v['Combination'], 1, -1));
	$key.=($key?'_':'').'Ov:'.$v['OvId'];
	$ext_ary[$v['ProId']][$key]=array($v['Price'], $v['Stock'], $v['Weight'], $v['SKU'], $v['IsIncrease']);
}
//颜色属性
$color_row=str::str_code(db::get_all('products_color', "ProId in($proid_list)", 'ProId, VId, PicPath_0'));
foreach((array)$color_row as $k=>$v){//统计产品颜色图片
	if(!$v['PicPath_0']) continue;
	if(is_file($c['root_path'].$v['PicPath_0'])){
		$c_color_ary[$v['ProId']][$v['VId']]=$v['PicPath_0'];
	}
}
?>
<div class="widget group_promotion">
	<div class="gp_title">
		<ul>
			<?php
			//选项栏
			$i=0;
			foreach((array)$group_promotion_ary as $key=>$val){
				foreach((array)$val['data'] as $k=>$v){
					if(!$v) continue;
					$name=$v['Name'];
					if(!$name) $name=(int)$v['Type']?$c['lang_pack']['products']['sales']:$c['lang_pack']['products']['group'];
			?>
			<li<?=$i?'':' class="current"';?>><span data="<?=(int)$v['Type']?'promotion':'purchase';?>" class="themes_title" data-id="<?=$i;?>"><?=$name;?></span></li>
			<?php
					++$i;
				}
			}?>
		</ul>
	</div>
	<div class="widget gp_list">
		<?php
		$i=0;
		foreach((array)$group_promotion_ary as $key=>$val){
			foreach((array)$val['data'] as $k=>$v){
				if(!$v) continue;
				$not_prod=0;
				$type=(int)$v['Type'];
				$v['ReverseAssociate']==1 && $v['PackageProId']=str_replace("|{$ProId}|", "|{$v['ProId']}|", $v['PackageProId']);
				$pid_ary=@array_filter(@explode('|', $v['PackageProId']));
				$length=count($pid_ary);
				$data_ary=str::json_data(htmlspecialchars_decode($v['Data']), 'decode');
				$master_img=$products_row['PicPath_0'];
                /*
				if(!(int)$v['IsAttr'] && $data_ary[$ProId]){//颜色图片(主产品)
					foreach($data_ary[$ProId] as $v2){
						if($c_color_ary[$ProId][$v2]) $master_img=$c_color_ary[$ProId][$v2];
					}
				}
                */
		?>
				<div class="widget promotion_body<?=(int)$type?' gp_list_promotion':' gp_list_purchase';?><?=$i?' hide':'';?>" data-id="<?=$i;?>">
					<div class="master">
						<s></s>
						<div class="prod_img"><a class="pic_box" href="javascript:;"><img src="<?=ly200::get_size_img($master_img, '240x240');?>" title="<?=$Name;?>" alt="<?=$Name;?>" /><span></span></a></div>
						<div class="prod_name"><a href="javascript:;" class="themes_title" title="<?=$Name;?>"><?=$Name;?></a></div>
						<?php if($type){?>
							<div class="prod_qty"><?=$c['lang_pack']['products']['qty'].': '.$MOQ;?></div>
							<dl class="attribute">
								<?php
								if(!(int)$v['IsAttr'] && $data_ary[$ProId]){
									foreach($data_ary[$ProId] as $k2=>$v2){
										if(!$v2) continue;
										if($k2=='Overseas'){//发货地
											$OvId=str_replace('Ov:', '', $v2);
											if((int)$c['config']['global']['Overseas']==0){//关闭海外仓功能，不显示
												$OvId!=1 && $not_prod+=1;//发货地不是中国，不能购买
												continue;
											}
											echo '<dd>'.$c['lang_pack']['products']['shipsFrom'].': '.$c['config']['Overseas'][$OvId]['Name'.$c['lang']].'</dd>';
										}else{
											echo '<dd>'.$c_all_attr_ary[$vid_data_ary[$v2]['AttrId']]['Name'.$c['lang']].': '.$vid_data_ary[$v2]['Value'.$c['lang']].'</dd>';
										}
									}
									echo '<input type="hidden" class="master_attr_hide" value="'.htmlspecialchars(str::json_data($data_ary[$ProId])).'" />';
								}?>
							</dl>
						<?php
                        }else{
                            $IsSeck = (int)$IsSeckill && (int)$SId ? 1 : 0;
                        ?>
                            <div class="prod_price prod_price_check clearfix">
                                <div class="checked_box">
                                    <input type="checkbox" id="group_<?=$v['PId'];?>_<?=$ProId;?>" oldprice="<?=cart::iconv_price($MOQ*$oldPrice, 2, '', 0);?>" curprice="<?=cart::iconv_price($MOQ*$CurPrice, 2, '', 0);?>" price="<?=cart::iconv_price($CurPrice, 2, '', 0);?>" attrprice="0" moq="<?=$MOQ;?>" proid="<?=$ProId;?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=($is_promotion && !$products_row['PromotionType'])?$products_row['PromotionPrice']:'';?>" discount="<?=($is_promotion && $products_row['PromotionType'])?$products_row['PromotionDiscount']:'';?>" isSeckill="<?=$IsSeck;?>" data-pid="<?=$v['PId'];?>" data-combination="<?=(int)$products_row['IsCombination'];?>" data-isattr="1" data-attrcount="<?=count($ext_ary[$ProId]);?>" data-attr-price="<?=(int)$products_row['IsOpenAttrPrice'];?>" />
                                </div>
                                <span class="currency_data themes_price"><?=$_SESSION['Currency']['Symbol'];?></span>
                                <span class="price_data themes_price" data="<?=$CurPrice;?>"><?=cart::currency_format($CurPrice, 1, $_SESSION['Currency']['Currency']);?></span>
                            </div>
                            <input type="hidden" id="attr_hide_<?=$v['PId'];?>_<?=$ProId;?>" class="attr_hide" value="" />
						<?php }?>
					</div>
					<div class="suits">
						<a href="javascript:;" rel="nofollow" class="prev"></a>
						<a href="javascript:;" rel="nofollow" class="next"></a>
						<div class="suits_box">
							<ul>
								<?php
								foreach((array)$pid_ary as $v2){
									$row=$val['pro'][$v2];
									$url=ly200::get_url($row, 'products');
									$img=ly200::get_size_img($row['PicPath_0'], '240x240');
									$name=$row['Name'.$c['lang']];
									$_moq=$row['MOQ'];
									$_moq<1 && $_moq=1;
									$oldprice=$row['Price_0'];
									$cprice=$price=cart::products_add_to_cart_price($row, $_moq);
									if(($row['SoldStatus']!=1 && ($row['Stock']<$_moq || $row['Stock']<1)) || $row['SoldOut']==1 || ($row['IsSoldOut'] && ($row['SStartTime']>$c['time'] || $c['time']>$row['SEndTime'])) || in_array($row['CateId'], $c['procate_soldout'])){
										$not_prod+=1;
										continue;//总库存为空
									}
									$OvId=1;
									$PropertyPrice=0;
									if(!(int)$v['IsAttr']){//产品关联属性
										if($data_ary[$row['ProId']]){//有属性数据
											$_dara_ary=array();//临时储存
											foreach((array)$data_ary[$row['ProId']] as $k3=>$v3){
												if($k3=='Overseas'){//发货地
													$OvId=str_replace('Ov:', '', $v3);
												}else{
													$_dara_ary[$k3]=$v3;
													if($c_color_ary[$row['ProId']][$v3]) $img=$c_color_ary[$row['ProId']][$v3];
												}
											}
											sort($_dara_ary);
											$attr_name='|'.implode('|', $_dara_ary).'|';
											if($attr_name!='||' && $OvId!=1){ //不是仅有默认发货地
												if($row['SoldStatus']!=1 && (int)$row['IsCombination'] && !$combinatin_ary[$row['ProId']][$attr_name][$OvId][1]){
													$not_prod+=1;
													continue;//属性库存为空
												}
												if((int)$row['IsCombination']){//开启规格组合
													if((int)$combinatin_ary[$row['ProId']][$attr_name][$OvId][4]){//加价
														$PropertyPrice+=$combinatin_ary[$row['ProId']][$attr_name][$OvId][0];
													}else{//单价
														$cprice=$price=$combinatin_ary[$row['ProId']][$attr_name][$OvId][0];
													}
												}else{//关闭规格组合
													foreach($data_ary[$row['ProId']] as $k3=>$v3){
														if($combinatin_ary[$row['ProId']]["|{$v3}|"][$OvId]){
															$PropertyPrice+=$combinatin_ary[$row['ProId']]["|{$v3}|"][$OvId][0];//固定是加价
														}
													}
												}
											}
										}else{//没有属性数据
											if(count($c_selected_ary[$row['ProId']]['Id'])>0){//产品是有关联属性数据，证明属性已经丢失了
												$not_prod+=1;
												continue;//属性库存为空
											}
										}
									}
									//价格计算
									$IsSeck=$is_promotion=0;
									$seck_row=str::str_code(db::get_one('sales_seckill', "ProId='$v2' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
									if($seck_row){//秒杀产品
										$price=$cprice=$seck_row['Price']+$PropertyPrice;
										$IsSeck=1;
									}else{//普通产品
										$price+=$PropertyPrice;
										$cprice+=$PropertyPrice;
										//$is_discount=($row['IsPromotion'] && $row['PromotionType'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
										$is_promotion=((int)$row['IsPromotion'] && $row['StartTime']<$c['time'] && $c['time']<$row['EndTime'])?1:0;
										if($is_promotion && $row['PromotionType']){//促销折扣
											$price=$price*($row['PromotionDiscount']/100);
										}
									}
									$_price=cart::iconv_price($price, 2, '', 0);
									$cprice=cart::iconv_price($cprice, 2, '', 0);
									$oldprice=cart::iconv_price($oldprice, 2, '', 0);
								?>
									<li<?=$k2+1==$length?' class="last"':''?>>
										<div class="prod_img">
											<a class="pic_box" href="<?=$url;?>" target="_blank"><img src="<?=$img;?>" title="<?=$name;?>" alt="<?=$name;?>" /><span></span></a>
											<?php if($v['IsAttr']==0 && $data_ary[$row['ProId']]){?>
												<div class="attributes_show">
													<?php
													foreach($data_ary[$row['ProId']] as $k3=>$v3){
														if(!$v3) continue;
														if($k3=='Overseas'){//发货地
															if((int)$c['config']['global']['Overseas']==0) continue;
															$OvId=str_replace('Ov:', '', $v3);
															echo '<p>'.$c['lang_pack']['products']['shipsFrom'].': '.$c['config']['Overseas'][$OvId]['Name'.$c['lang']].'</p>';
														}else{
															echo '<p>'.$c_all_attr_ary[$vid_data_ary[$v3]['AttrId']]['Name'.$c['lang']].': '.$vid_data_ary[$v3]['Value'.$c['lang']].'</p>';
														}
													}
													echo '<input type="hidden" class="attr_hide" value="'.htmlspecialchars(str::json_data($data_ary[$row['ProId']])).'" />';
													?>
												</div>
											<?php }?>
										</div>
										<div class="prod_name"><a href="<?=$url;?>" class="themes_title" target="_blank" title="<?=$name;?>"><?=$name;?></a></div>
										<?php if(!$type){?>
											<div class="prod_price prod_price_check clearfix">
												<div class="checked_box">
													<input type="checkbox" id="group_<?=$v['PId'];?>_<?=$row['ProId'];?>" oldprice="<?=$_moq*$oldprice;?>" curprice="<?=$_moq*$_price;?>" price="<?=$cprice;?>" attrprice="0" moq="<?=$_moq;?>" proid="<?=$row['ProId'];?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=($is_promotion && !$row['PromotionType'])?$row['PromotionPrice']:'';?>" discount="<?=($is_promotion && $row['PromotionType'])?$row['PromotionDiscount']:'';?>" isSeckill="<?=$IsSeck;?>" data-pid="<?=$v['PId'];?>" data-combination="<?=(int)$row['IsCombination'];?>" data-isattr="<?=$v['IsAttr'];?>" data-attrcount="<?=count($ext_ary[$row['ProId']]);?>" data-attr-price="<?=(int)$row['IsOpenAttrPrice'];?>" />
												</div>
												<span class="currency_data themes_price"><?=$_SESSION['Currency']['Symbol'];?></span>
												<span class="price_data themes_price" data="<?=$price;?>"><?=cart::currency_format($_price, 1, $_SESSION['Currency']['Currency']);?></span>
											</div>
										<?php }?>
										<input type="hidden" id="attr_hide_<?=$v['PId'];?>_<?=$row['ProId'];?>" class="attr_hide" value="" />
									</li>
								<?php }?>
							</ul>
						</div>
						<div class="not_prod_number" value="<?=$not_prod;?>"></div>
					</div>
					<div class="info">
						<div class="prod_name"><?=str_replace('%packageLen%', ($type?$length:'0'), $c['lang_pack']['products']['packageLen']);?></div>
						<strong class="group_curprice clearfix"><em class="currency_data themes_price themes_types"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price themes_price themes_types" data="<?=$type?$v['CurPrice']:$MOQ*$ItemPrice;?>"><?=cart::iconv_price(($type?$v['CurPrice']:$MOQ*$ItemPrice), 2);?></span></strong>
						<div class="group_oldprice clearfix"<?=(int)$c['config']['products_show']['Config']['price']==0?' style="display:none;"':'';?>><del><em class="currency_data"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data" data="<?=$type?$MOQ*$ItemPrice:$MOQ*$oldPrice;?>"><?=cart::iconv_price(($type?$MOQ*$ItemPrice:$MOQ*$oldPrice), 2);?></span></del></div>
						<div class="group_saveprice clearfix"<?=(int)$c['config']['products_show']['Config']['price']==0?' style="display:none;"':'';?>><?=$c['lang_pack']['products']['save'];?> <em class="currency_data"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data" data="<?=$MOQ*($oldPrice-$ItemPrice);?>"><?=cart::iconv_price($MOQ*($oldPrice-$ItemPrice), 2);?></span></div>
						<?php if(!$is_stockout){?><input type="button" value="<?=$c['lang_pack']['products']['buyNow'];?>" class="gp_btn" /><?php }?>
						<input type="hidden" name="PId" value="<?=$v['PId'];?>" />
					</div>
				</div>
		<?php
				++$i;
			}
		}?>
	</div>
</div>
<div id="promotion_attr">
	<div class="box_content">
		<a href="javascript:;" rel="nofollow" class="close"></a>
		<div class="attr_box"></div>
		<div class="btn_box">
			<input type="button" class="submit" value="<?=$c['lang_pack']['cart']['submit'];?>" />
			<input type="button" class="cancel" value="<?=$c['lang_pack']['cart']['cancel'];?>" />
		</div>
	</div>
</div>