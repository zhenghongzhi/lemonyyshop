<?php !isset($c) && exit();?>
<div class="clearfix">
	<div class="detail_box">
		<div class="detail_box_left">
			<div class="pic_size_width"></div>
			<div class="detail_left prod_gallery_x"></div>
			<div class="clear"></div>
			<div class="prod_info_share <?=in_array('pdf', $c['plugins']['Used']) ? '' : 'no_pdf'; ?>">
				<div class="center">
					<?php include("{$c['default_path']}/products/detail/share.php");?>
				</div>
			</div>
			<?php if(in_array('pdf', $c['plugins']['Used'])){?><a class="prod_info_pdf" href="javascript:;"><em class="icon_pdf"></em><?=$c['lang_pack']['products']['pdf'];?></a><?php }?>
			<div class="clear"></div>
        	<div class="favorite_box">
				<a href="javascript:;" class="global_trans favorite_btn add_favorite <?=in_array($ProId, $user_favorite_ary) ? 'is_in' : ''; ?>" data="<?=$ProId;?>"><?=$c['lang_pack']['products']['favorite'];?></a>
        	</div>
			<div class="prod_description_padding"></div>
			<?php include("{$c['default_path']}/products/detail/description_1.php");?>
			<div class="prod_description">
				<ul class="prod_content">
					<li class="themes_bor">
						<div class="themes_title item" data="0">
							<a name="review_box" id="review_box"></a>
							<?=$c['lang_pack']['reviews']; ?> <?=$TotalRating ? '('.$TotalRating.')' : ''; ?>
						</div>
						<div class="desc" data-number="0">
							<div class="review_box">
								<div class="star_num"><?=$Rating; ?></div>
								<div class="re_info">
									<?=html::review_star($Rating); ?>
									<div class="count"><?=(int)$TotalRating; ?> <?=$c['lang_pack']['reviews']; ?></div>
								</div>
								<a href="<?=ly200::get_url($products_row, 'write_review');?>" class="write_review"><?=$c['lang_pack']['products']['writeReview'];?></a>
								<div class="clear"></div>
							</div>
							<?php include("{$c['default_path']}/products/review/review_box.php");?>
						</div>
					</li>
				</ul>
			</div>
			<div class="prod_description_padding"></div>
		</div>
		<div class="detail_right fr">
			<div class="widget prod_info_title"><?=html::website_h1('h1', $Name, 'itemprop="name" class="themes_title"'); ?></div>
			<?php if($products_row['BriefDescription'.$c['lang']]){?>
				<div class="widget prod_info_desc"><?=str::format(htmlspecialchars_decode($products_row['BriefDescription'.$c['lang']]));?></div>
			<?php }?>

			<?php 
			$pro_info=ly200::get_pro_info($products_row);
			if($pro_info[0]){
			?>
				<div class="sold_box">
					<div class="fl"><?=$c['lang_pack']['sold'];?>:</div>
					<div class="fl"><?=$pro_info[0];?></div>
					<div class="clear"></div>
				</div>
			<?php }?>
			<div class="widget prod_info_review">
				<?=html::review_star($Rating, 's_review_star');?>
				<a class="write_review review_count" href="#review_box">(<?=$TotalRating.' '.$c['lang_pack']['user']['reviewCount'];?>)</a>
	            <?php
                if($c['FunVersion'] && in_array('product_inbox', $c['plugins']['Used'])){
					$UserUsed=(int)db::get_value('config', "GroupId='product_inbox' and Variable='UserUsed'", 'Value');
				?>
	            	<div class="prod_info_inquiry fl"><a class="product_inquiry" href="javascript:;" data-user="<?=(int)$_SESSION['User']['UserId']?>" data-proid="<?=$ProId;?>" data-userused="<?=$UserUsed;?>" data-email="<?=$_SESSION['User']['Email'];?>"><?=$c['lang_pack']['products']['haq'];?></a></div>
	            <?php }?>
			</div>
			<div class="widget prod_info_price">
				<div class="widget price_left price_1">
	                <?php $currency_row=db::get_all('currency', "IsUsed='1'");?>
					<div class="current_price">
						<div class="left">
							<dl class="widget prod_info_currency <?=count($currency_row)>1?'prod_info_currency_more':''?>">
								<dt><a href="javascript:;"><?=$_SESSION['Currency']['Currency'];?><div class="arrow"><em></em><i></i></div></a></dt>
								<dd>
									<ul>
										<?php foreach((array)$currency_row as $v){?>
											<li><a href="javascript:;" data="<?=$v['Currency'];?>"><?=$v['Currency'];?></a></li>
										<?php }?>
									</ul>
								</dd>
							</dl>
							<strong id="cur_price" class="themes_price price"><?=$_SESSION['Currency']['Symbol'].cart::iconv_price($CurPrice, 2);?></strong>
						</div>
						<?php if($oldPrice>$CurPrice && $oldPrice>0){?>
							<div class="fl save_box">
								<del class="themes_subtitle"><?=cart::iconv_price($oldPrice);?></del>
								<div><?=$save_discount;?>% OFF</div>
							</div>
						<?php }?>
						<?php
						if((int)$SId && (int)$IsSeckill){
							$time=$sales_row['EndTime']-$c['time'];
							echo ($time<86400*31)?'<div class="discount_count fl"><div class="themes_sales discount_time" endtime="'.date('Y/m/d H:i:s', $sales_row['EndTime']).'"></div></div>':'';
						}elseif($is_promotion){
							$time=$products_row['EndTime']-$c['time'];
							$promotion_discount=sprintf('%01.2f', ($Price_1-$ItemPrice)/$Price_1*100);
							$promotion_discount=($promotion_discount>0 && $promotion_discount<1)?1:@intval($promotion_discount);
							if($products_row['PromotionType']) $promotion_discount=100-$products_row['PromotionDiscount'];
							$month=ceil($time/86400);
							echo ($time<86400*31)?'<div class="discount_count fl"><div class="themes_sales discount_time" endtime="'.date('Y/m/d H:i:s', $products_row['EndTime']).'"></div></div>':'';
						}
						?>
					</div>
				</div>
			</div>
			<form class="prod_info_form" name="prod_info_form" id="goods_form" action="/cart/add.html" method="post" target="_blank">
				<div class="attr_bg">
					<?php include("{$c['default_path']}/products/detail/attribute.php");?>
					<div class="widget prod_info_quantity">
						<div class="themes_bor quan_input">
							<label for="quantity"><?=$c['lang_pack']['products']['qty'];?>:</label>
							<div class="quantity_box" data="<?=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?>"><input id="quantity" class="qty_num" name="Qty" autocomplete="off" type="text" value="<?=$MOQ;?>" stock="<?=$Max;?>" /></div>
						</div>
						<div class="quan_btn">
		                	<div class="qty_box"><div id="btn_add" class="themes_bor">+</div></div>
		                	<div class="qty_box"><div id="btn_cut" class="themes_bor">-</div></div>
						</div>
						<!-- <span><?=$Unit;?></span> -->
						<?php if(!$IsSeckill && $is_wholesale){ ?>
							<div class="prod_info_wholesale_box">
								<a href="javascript:;" class="more_price"><?=$c['lang_pack']['products']['wholesale']; ?></a>
								<div class="widget prod_info_wholesale" data="<?=$products_row['Wholesale'];?>">
									<div class="pw_table clearfix">
										<div class="pw_column" data-num="0">
											<div class="pw_td pw_th"><?=$c['lang_pack']['products']['qty']; ?></div>
											<div class="pw_td hide"></div>
											<div class="pw_td pw_th"><?=$c['lang_pack']['price']; ?> ( <?=$_SESSION['Currency']['Symbol'];?> )</div>
										</div>
										<?php 
										$wholesale_key=array();
										foreach((array)$wholesale_price as $k=>$v){
											$wholesale_key[]=$k;
										}
										$cur=0;
										foreach((array)$wholesale_price as $k=>$v){
											$cur++;
											?>
											<div class="pw_column" data-num="<?=$k;?>">
												<div class="pw_td"><?=count($wholesale_price)==$cur ? $k.'+' : (int)$k.'-'.($wholesale_key[$cur]-1);?></div>
												<div class="pw_td" data-price="<?=$v;?>" data-discount="<?=1-($v/$Price_1);?>"><?=cart::iconv_price($v);?></div>
											</div>
										<?php } ?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<script>
								var wholesale_white=0;
								$('.prod_info_wholesale_box .more_price').hover(function(){
									if(wholesale_white==0){
										$('.prod_info_wholesale').css({'width': ($('.prod_info_wholesale .pw_table').width()+1)+'px','margin-left':-($('.prod_info_wholesale .pw_table').width()+1)/2+'px'});
										wholesale_white=1;
									}
								});
							</script>
						<?php }?>
						<?php 
						$OpenParameter=array();
						if($products_row['OpenParameter']){
							$OpenParameter=explode('|', $products_row['OpenParameter']);
						}
						in_array('Unit', $OpenParameter) || $Unit='';
						?>
						<span class="prod_info_inventory">(<?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?>)</span>
		                <div class="clear"></div>
					</div>
					<?php if(in_array('freight', $c['plugins']['Used'])){?>
						<div class="widget key_info_line">
							<div class="key_info_left"><?=$c['lang_pack']['products']['shippingCost'];?>:</div>
							<div class="key_info_right"> 
								<div class="shipping_cost_detail"> 
									<span class="shipping_cost_price"></span>
									<span class="shipping_cost_to"><?=$c['lang_pack']['products']['to'];?></span>
									<span id="shipping_flag" class="icon_flag"></span>
									<span id="shipping_cost_button" class="shipping_cost_button FontColor"></span>
								</div>
								<div class="shipping_cost_info"><?=$c['lang_pack']['products']['shipEstimated'];?>:<span class="delivery_day"></span></div>
								<div class="shipping_cost_error"><?=$c['lang_pack']['products']['shipError'];?></div>
							</div>
						</div>
					<?php }?>
				</div>
				<div class="widget prod_info_actions">
					<?php
					if($is_stockout){
						echo '<input type="button" value="'.$c['lang_pack']['products']['soldOut'].'" class="themes_btn add_btn soldout" />';
						echo '<input type="button" value="'.$c['lang_pack']['products']['notice'].'" class="themes_btn add_btn arrival" id="arrival_button" />';
					}else{
						echo '<div class="addtocart_box"><input type="submit" value="'.$c['lang_pack']['products']['addToCart'].'" class="themes_btn add_btn addtocart themes_button" id="addtocart_button" /></div>';
						if($is_paypal_checkout){
							echo ly200::load_static('/static/themes/default/css/cart.css', '/static/themes/default/js/cart.js');
							if($c['NewFunVersion']>=4){//新版Paypal checkout
								echo '<div class="box_paypal">';
									echo '<input type="button" value="" class="themes_btn add_btn" id="btn_paypal_replace" />';
									echo '<div id="paypal_button_container"></div>';
								echo '</div>';
							}else{//旧版Paypal checkout
								echo '<input type="button" value="" class="themes_btn add_btn paypal_checkout_button" id="paypal_checkout_button" />';
								echo '<script src="//www.paypalobjects.com/api/checkout.js" async></script>';
							}
						}else{
							echo '<input type="button" value="'.$c['lang_pack']['products']['buyNow'].'" class="themes_btn add_btn buynow BuyNowBgColor" id="buynow_button" />';
						}
					}?>
					<div class="clear"></div>
					<?php
					//平台导流
					if(in_array('platform', $c['plugins']['Used'])){
						$platform=str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
						if(count($platform)) echo '<div class="platform_tit">'.$c['lang_pack']['products']['platform_tips'].':</div>';
						$j=0;
						foreach((array)$platform as $k => $v){
							if(!$v[0]['Url'.$c['lang']]) continue;
							$j++;
							if(count($v)>1){
					?>
								<span class="themes_btn themes_bor add_btn platform_btn <?=$k;?>_btn"><?=$c['lang_pack']['products'][$k];?><em></em>
									<div class="platform_ab">
										<?php foreach((array)$v as $v1){?>
											<a href="<?=$v1['Url'.$c['lang']];?>" target="_blank"><?=$v1['Name'.$c['lang']];?></a>
										<?php }?>
									</div>
								</span>
					<?php
							}else{
								$icon=@explode('_', $k);
								echo '<a href="'.$v[0]['Url'.$c['lang']].'" target="_blank" class="'.($j%2==0?'fr':'fl').' themes_btn themes_bor add_btn plat_btn '.$icon[0].'_btn">'.$c['lang_pack']['products'][$k].'</a>';
							}
						}
					}?>
	                <div class="clear"></div>
				</div>
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" id="ItemPrice" name="ItemPrice" value="<?=$ItemPrice;?>" initial="<?=$ItemPrice;?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=$is_promotion && !$products_row['PromotionType']?$products_row['PromotionPrice']:'';?>" discount="<?=$is_promotion && $products_row['PromotionType']?$products_row['PromotionDiscount']:'';?>" old="<?=$oldPrice;?>" />
				<input type="hidden" name="Attr" id="attr_hide" value="[]" />
				<input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data(str::str_code($ext_ary,'htmlspecialchars_decode')));?>" />
				<input type="hidden" name="products_type" value="<?=((int)$IsSeckill && (int)$SId)?2:0;?>" />
				<input type="hidden" name="SId" value="<?=(int)$SId;?>"<?=((int)$IsSeckill && (int)$SId)?' stock="'.$Max.'"':'';?> />
				<input type="hidden" id="CId" value="<?=(int)$country_default_row['CId'];?>" />
				<input type="hidden" id="CountryName" value="<?=$country_default_row['Country'];?>" />
				<input type="hidden" id="CountryAcronym" value="<?=$country_default_row['Acronym'];?>" />
				<input type="hidden" id="ShippingId" value="0" />
				<input type="hidden" id="attrStock" value="<?=(int)$products_row['SoldStatus'];?>" />
			</form>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<?php if((int)$c['FunVersion'] && $group_promotion_ary) include("{$c['default_path']}/products/detail/combination.php");?>
<div class="clearfix">
	<div id="may_like" class="themes_box sidebar">
		<h2 class="themes_title themes_box_title themes_bor b_title"><?=$c['lang_pack']['products']['mayLike'];?></h2>
		<div class="themes_box_main b_list">
			<?php
			if($TopCateId){
				$UId="0,{$TopCateId},";
				$cateid=$TopCateId;
			}else{
				$UId=category::get_UId_by_CateId($CateId);
				$cateid=$CateId;
			}
			$row=str::str_code(db::get_limit('products', "1 and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$cateid}' or ".category::get_search_where_by_ExtCateId($cateid, 'products_category').')'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 4));
			$len=count($row);
			foreach((array)$row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$price_ary=cart::range_price_ext($v);
			?>
				<dl class="pro_item clearfix<?=$k==0 ?' fir':'';?>">
					<dt><a class="pic_box" href="<?=$url;?>"><img src="<?=ly200::get_size_img($v['PicPath_0'], '500x500');?>" title="<?=$v['Name'.$c['lang']];?>" alt="<?=$v['Name'.$c['lang']];?>" /><span></span></a></dt>
					<dd class="pro_info">
						<div class="pro_name"><a href="<?=$url;?>" class="themes_title" title="<?=$v['Name'.$c['lang']];?>"><?=$v['Name'.$c['lang']];?></a></div>
						<div class="pro_price"><em class="themes_price currency_data"><?=$_SESSION['Currency']['Symbol'];?></em><span class="themes_price price_data" data="<?=$price_ary[0];?>" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></div>
					</dd>
				</dl>
			<?php }?>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="blank12"></div>
<script>
	var pw_btn = $('.pw_table .pw_next'),
		pw_itlen = $('.pw_column_abs .pw_column').length,
		pw_len = 3;
	if($('body').hasClass('w_1200')) pw_len=4;
	if(pw_itlen>pw_len) pw_btn.show();
	pw_btn.click(function(){
		var $this = $(this),
			$it_w = parseFloat($('.pw_column_box .pw_column').width()),
			$left = '-'+$it_w*2+'px';
		if($('body').hasClass('w_1200')) $left = '-'+$it_w+'px';
		if($this.hasClass('pw_prev')){
			$this.removeClass('pw_prev');
			$('.pw_column_abs').animate({left:0});
		}else{
			$this.addClass('pw_prev');
			$('.pw_column_abs').animate({left:$left});
		}
	});
</script>