<?php !isset($c) && exit();?>
<div class="detail_right fr">
	<div class="widget prod_info_title"><?=html::website_h1('h1', $Name, 'itemprop="name" class="themes_title"');?></div>
	<?php 
	$pro_info = ly200::get_pro_info($products_row);
	if ($pro_info[0]) {
	?>
		<div class="sold_box">
			<div class="fl"><?=$c['lang_pack']['sold'];?>:</div>
			<div class="fl"><?=$pro_info[0];?></div>
			<div class="clear"></div>
		</div>
	<?php }?>
	<div class="widget prod_info_review">
		<?php
		if ($c['config']['products_show']['Config']['review']) {
			echo html::review_star($Rating, 1);
            echo '<a class="write_review review_count" href="#review_box">' . $TotalRating.' '.$c['lang_pack']['user']['reviewCount'] . '</a>';
		}?>
	</div>
	<div class="widget prod_info_price">
		<div class="widget price_left price_1">
			<?php $currency_row=db::get_all('currency', "IsUsed='1'");?>
			<div class="current_price">
				<div class="left">
					<dl class="widget prod_info_currency<?=count($currency_row) > 1 ? ' prod_info_currency_more' : '';?>">
						<dt><a href="javascript:;"><?=$_SESSION['Currency']['Currency'];?><div class="arrow"><em></em><i></i></div></a></dt>
						<dd>
							<ul>
								<?php foreach ((array)$currency_row as $v) {?>
									<li><a href="javascript:;" data="<?=$v['Currency'];?>"><?=$v['Currency'];?></a></li>
								<?php }?>
							</ul>
						</dd>
					</dl>
					<strong id="cur_price" class="price"><?=$_SESSION['Currency']['Symbol'] . cart::iconv_price($CurPrice, 2);?></strong>
				</div>
				<?php
                if ($oldPrice > $CurPrice && $oldPrice > 0) {
					echo '<div class="save_box"><del>' . cart::iconv_price($oldPrice) . '</del></div>';
				}
				if ((int)$SId && (int)$IsSeckill) {
					$time = $sales_row['EndTime'] - $c['time'];
					if ($time < 86400 * 31) {
                        echo '<div class="discount_count fl"><div class="discount_time" endtime="' . date('Y/m/d H:i:s', $sales_row['EndTime']) . '"></div></div>';
					}
				} elseif ($is_promotion) {
					$time = $products_row['EndTime'] - $c['time'];
					$promotion_discount = sprintf('%01.2f', ($Price_1 - $ItemPrice) / $Price_1 * 100);
					$promotion_discount = ($promotion_discount > 0 && $promotion_discount < 1) ? 1 : @intval($promotion_discount);
					$products_row['PromotionType'] && $promotion_discount = 100 - $products_row['PromotionDiscount'];
					$month = ceil($time / 86400);
					if ($time < 86400 * 31) {
                        echo '<div class="discount_count fl"><div class="discount_time" endtime="' . date('Y/m/d H:i:s', $products_row['EndTime']) . '"></div></div>';
					}
				}?>
			</div>
		</div>
	</div>
	<form class="prod_info_form" name="prod_info_form" id="goods_form" action="/cart/add.html" method="post" target="_blank">
		<?php include("{$c['default_path']}/products/detail/attribute.php");?>
		<div class="prod_info_detail">
			<?php
            if (!$IsSeckill && $is_wholesale) {
				echo '<div class="widget prod_info_wholesale hide" data="' . $products_row['Wholesale'] . '"></div>';
			}?>
			<div class="widget prod_info_quantity">
				<div class="quan_input">
					<label for="quantity"><?=$c['lang_pack']['products']['qty'];?>:</label>
					<div class="quantity_box" data="<?=htmlspecialchars('{"min":' . $MOQ . ',"max":' . $Max . ',"count":' . $MOQ . '}');?>"><input id="quantity" class="qty_num" name="Qty" autocomplete="off" type="text" value="<?=$MOQ;?>" stock="<?=$Max;?>" /></div>
				</div>
				<div class="quan_btn">
					<div class="qty_box"><div id="btn_add">+</div></div>
					<div class="qty_box"><div id="btn_cut">-</div></div>
				</div>
				<?php 
				$OpenParameter=array();
				if ($products_row['OpenParameter']) {
					$OpenParameter=explode('|', $products_row['OpenParameter']);
				}
				in_array('Unit', $OpenParameter) || $Unit='';
				?>
				<span class="prod_info_inventory">(<?=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?>)</span>
				<div class="clear"></div>
			</div>
		</div>
		<div class="widget prod_info_actions">
			<?php
            if ($is_stockout && $products_row['StockOut']) {
				echo '<input type="button" value="' . $c['lang_pack']['products']['notice'] . '" class="add_btn arrival" id="arrival_button">';
			} elseif ($is_stockout && !$products_row['StockOut']) {
				echo '<input type="button" value="' . $c['lang_pack']['products']['soldOut'] . '" class="add_btn soldout">';
			} else {
				echo '<div class="button_box"><input type="submit" value="' . $c['lang_pack']['products']['addToCart'] . '" class="add_btn addtocart AddtoCartBgColor" id="addtocart_button"></div>';
                if ($is_paypal_checkout) {
                    echo ly200::load_static('/static/themes/default/css/cart.css', '/static/themes/default/js/cart.js');
                    if ($c['NewFunVersion'] >= 4) {
                        //新版Paypal checkout
                        echo '<div class="box_paypal">';
                            echo '<input type="button" value="" class="add_btn" id="btn_paypal_replace" />';
                            echo '<div id="paypal_button_container"></div>';
                        echo '</div>';
                    } else {
                        //旧版Paypal checkout
                        echo '<input type="button" value="" class="add_btn paypal_checkout_button" id="paypal_checkout_button" />';
                        echo '<script src="//www.paypalobjects.com/api/checkout.js" async></script>';
                    }
                } else {
					echo '<div class="button_box"><input type="button" value="' . $c['lang_pack']['products']['buyNow'] . '" class="add_btn buynow BuyNowBgColor" id="buynow_button" /></div>';
				}
			}?>
			<div class="clear"></div>
            <?php
			//平台导流
			$platform = str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
			if (in_array('platform', $c['plugins']['Used']) && $platform) {
			?>
				<div class="clear"></div>
				<div class="platform_box">
					<div class="title"><?=$c['lang_pack']['products']['platform_tips'];?>:</div>
					<?php
                    foreach ((array)$platform as $k => $v) { 
						$icon=@explode('_', $k);
					?>
						<a href="<?=$v[0]['Url' . $c['lang']];?>" rel="nofollow" title="<?=$c['lang_pack']['products'][$k];?>" target="_blank" class="add_btn platform_btn <?=$icon[0];?>_btn"><?=$c['lang_pack']['products'][$k];?></a>
					<?php }?>
				</div>
            <?php }?>
			<div class="clear"></div>
			<a href="<?=ly200::get_url($products_row, 'products');?>" class="view_btn"><?=$c['lang_pack']['products']['more_details'];?></a>
			<div class="clear"></div>
		</div>
		<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
		<input type="hidden" id="ItemPrice" name="ItemPrice" value="<?=$ItemPrice;?>" initial="<?=$ItemPrice;?>" sales="<?=$is_promotion ? 1 : 0;?>" salesPrice="<?=$is_promotion && !$products_row['PromotionType'] ? $products_row['PromotionPrice'] : '';?>"  discount="<?=$is_promotion && $products_row['PromotionType'] ? $products_row['PromotionDiscount'] : '';?>" old="<?=$oldPrice;?>" />
		<input type="hidden" name="Attr" id="attr_hide" value="[]" />
		<input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data($ext_ary));?>" />
		<input type="hidden" name="products_type" value="<?=((int)$IsSeckill && (int)$SId) ? 2 : 0;?>" />
		<input type="hidden" name="SId" value="<?=(int)$SId;?>"<?=((int)$IsSeckill && (int)$SId) ? ' stock="' . $sales_row['RemainderQty'] . '"' : '';?> />
		<input type="hidden" id="CId" value="<?=(int)$country_default_row['CId'];?>" />
		<input type="hidden" id="CountryName" value="<?=$country_default_row['Country'];?>" />
		<input type="hidden" id="CountryAcronym" value="<?=$country_default_row['Acronym'];?>" />
		<input type="hidden" id="ShippingId" value="0" />
		<input type="hidden" id="attrStock" value="<?=(int)$c['config']['products_show']['Config']['stock'];?>" />
	</form>
</div>
