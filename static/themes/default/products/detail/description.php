<?php !isset($c) && exit();?>
<div class="prod_description">
	<ul class="pd_title">
		<li class="current"><span data="0"><?=$c['lang_pack']['products']['description'];?></span></li>
		<?php
			$num=0; 
			foreach((array)$tab_row as $k=>$v){
			$num++;
			?>
			<li class="<?=!$v['TabName'] ? 'hide' : ''; ?>"><span data="<?=$num;?>"><?=$v['TabName'];?></span></li>
		<?php }?>
		<li><a name="review_box" id="review_box"></a><span data="<?=$num+1;?>"><?=$c['lang_pack']['reviews']; ?><?=$TotalRating ? '('.$TotalRating.')' : ''; ?></span></li>
	</ul>
	<div class="clear"></div>
	<div class="pd_content">
		<div class="desc editor_txt" data-number="0">
			<?php
			if($attr_ary['Common']){
				$all_value_ary=$attrid=array();
				foreach($attr_ary['Common'] as $v){ $attrid[]=$v['AttrId']; }
				$attrid_list=implode(',', $attrid);
				!$attrid_list && $attrid_list=0;
				$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
				foreach($value_row as $v){ $all_value_ary[$v['AttrId']][$v['VId']]=$v; }
			?>
				<div class="item_specifics">
					<div class="title"><?=$c['lang_pack']['products']['specifics'];?></div>
					<?php
					$item=0;
					foreach((array)$attr_ary['Common'] as $k=>$v){
						if(!$v || !$v['Name'.$c['lang']] || $products_attr_status_ary[$v['AttrId']]==2 || ($v['Type']==1 && !$selected_ary['Id'][$v['AttrId']]) || ($v['Type']==0 && !$selected_ary['Value'][$v['AttrId']])) continue;
					?>
						<span>
							<strong><?=$v['Name'.$c['lang']];?>:</strong>
							<?php
							if($v['Type']==1 && is_array($all_value_ary[$v['AttrId']])){
								$i=0;
								foreach($all_value_ary[$v['AttrId']] as $k2=>$v2){
									if(in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])){
										echo ($i?', ':'').$v2['Value'.$c['lang']];
										++$i;
									}
								}
							}else echo stripslashes($selected_ary['Value'][$v['AttrId']]);
							?>
						</span>
					<?php
						++$item;
					}?>
				</div>
			<?php }?>
			<?php if(!$item){?>
				<script type="text/javascript">$('.item_specifics').remove();</script>
			<?php }?>
			<?=str::str_code($products_description_row['Description'.$c['lang']], 'htmlspecialchars_decode');?>
		</div>
		<?php 
		$num=0; 
		foreach((array)$tab_row as $k=>$v){
			++$num;
		?>
			<div class="desc editor_txt hide" data-number="<?=$num;?>"><?=str::str_code(stripslashes($v['Tab']), 'htmlspecialchars_decode');?></div>
		<?php }?>
		<div class="desc editor_txt hide" data-number="<?=$num+1;?>">
			<?php include("{$c['default_path']}/products/review/review_box.php");?>
		</div>
	</div>
</div>