<?php !isset($c) && exit();?>
<div class="clearfix">
	<div class="detail_left_box">
		<div class="pic_size_width"></div>
		<div class="fl">
			<div class="detail_left"></div>
            <div class="clear"></div>
			<div class="prod_info_share <?=in_array('pdf', $c['plugins']['Used']) ? '' : 'no_pdf'; ?>">
				<div class="center">
					<?php include("{$c['default_path']}/products/detail/share.php");?>
				</div>
			</div>
			
			<?php if(in_array('pdf', $c['plugins']['Used'])){?><a class="prod_info_pdf" href="javascript:;" data-url="<?=urlencode(ly200::get_domain(0).ly200::get_url($products_row, 'products').'?print_pdf=1');?>"><em class="icon_pdf"></em><?=$c['lang_pack']['products']['pdf'];?></a><?php }?>
			<div class="clear"></div>
		</div>
		<div class="detail_right fr">
			<div class="widget prod_info_title"><?=html::website_h1('h1', $Name, 'itemprop="name" class="themes_title"'); ?></div>
			<?php if($products_row['Number']){ ?>
				<div class="themes_subtitle widget prod_info_number"><?=$products_row['Prefix'].$products_row['Number'];?></div>
			<?php } ?>
			<?php if($products_row['BriefDescription'.$c['lang']]){ ?>
				<div class="themes_subtitle widget prod_info_desc" style="color:red;"><?=str::format(htmlspecialchars_decode($products_row['BriefDescription'.$c['lang']])); ?></div>
			<?php } ?>
			<div class="widget prod_info_review">
				<?php if($TotalRating){?>
				<?=html::review_star($Rating); ?>
				<a class="themes_subtitle review_count" href="#review_box"><?=$TotalRating.' '.$c['lang_pack']['user']['reviewCount'];?></a><?php }?>
	            <?php 
				$pro_info=ly200::get_pro_info($products_row);
				if($pro_info[0]){
				?>
					<span class="sold">
						<?=$c['lang_pack']['sold'];?>:<?=$pro_info[0];?>
					</span>
				<?php }?>
	            <?php
                if(in_array('product_inbox', $c['plugins']['Used'])){
					$UserUsed=(int)db::get_value('config', "GroupId='product_inbox' and Variable='UserUsed'", 'Value');
				?>
	            	<div class="prod_info_inquiry"><a class="product_inquiry" href="javascript:;" data-user="<?=(int)$_SESSION['User']['UserId']?>" data-proid="<?=$ProId;?>" data-userused="<?=$UserUsed;?>" data-email="<?=$_SESSION['User']['Email'];?>"><?=$c['lang_pack']['products']['haq'];?></a></div>
	            <?php }?>
			</div>
			<form class="prod_info_form" name="prod_info_form" id="goods_form" action="/cart/add.html" method="post" target="_blank">
				<div class="bg">
					<div class="rows">
						<h5 class="row_th"><?=$c['lang_pack']['price']; ?>:</h5>
						<div class="widget prod_info_price">
							<div class="widget price_left price_1">
				                <?php $currency_row=db::get_all('currency', "IsUsed='1'");?>
								<div class="current_price">
									<div class="left">
										<dl class="widget prod_info_currency <?=count($currency_row)>1?'prod_info_currency_more':''?>">
											<dt><a href="javascript:;"><?=$_SESSION['Currency']['Currency'];?><div class="arrow"><em></em><i></i></div></a></dt>
											<dd>
												<ul>
													<?php foreach((array)$currency_row as $v){?>
													<li><a href="javascript:;" data="<?=$v['Currency'];?>"><?=$v['Currency'];?></a></li>
													<?php }?>
												</ul>
											</dd>
										</dl>
										<strong id="cur_price" class="price"><?=$_SESSION['Currency']['Symbol'].cart::iconv_price($CurPrice, 2);?></strong>
									</div>
									<?php if($oldPrice>$CurPrice && $oldPrice>0){ ?>
										<del><?=cart::iconv_price($oldPrice);?></del>
									<?php }?>
									<?php
									if((int)$SId && (int)$IsSeckill){
										$time=$sales_row['EndTime']-$c['time'];
									?>
										<?php if($oldPrice>$CurPrice && $oldPrice>0){ ?><div class="discount_price discount_attr fl">(<span><?=$discount;?>% OFF</span>)</div><?php }?>
										<?php if($time<86400*31){?><div class="discount_count fl"><div class="discount_time" endtime="<?=date('Y/m/d H:i:s', $sales_row['EndTime']);?>"></div></div><?php }?>
									<?php
									}elseif($is_promotion){
										$time=$products_row['EndTime']-$c['time'];
										$promotion_discount=sprintf('%01.2f', ($Price_1-$ItemPrice)/$Price_1*100);
										$promotion_discount=($promotion_discount>0 && $promotion_discount<1)?1:@round($promotion_discount);
										if($products_row['PromotionType']) $promotion_discount=100-$products_row['PromotionDiscount'];
										echo '<div class="discount_price discount_attr fl">(<span>'.$promotion_discount.'% OFF</span>)</div>';
										$month=ceil($time/86400);
										// if($month<31) echo '<div class="discount_sales discount_attr fl">(<span>'.str_replace('%time%', $month, $c['lang_pack']['products']['onlyDays']).'</span>)</div>';
										if($time<86400*31){?><div class="discount_count fl"><div class="discount_time" endtime="<?=date('Y/m/d H:i:s', $products_row['EndTime']);?>"></div></div>
									<?php
										}
									}
									?>
									<?php /*if((int)$c['config']['products_show']['Config']['price']){?>
										<div class="clear"></div>
										<div class="save_price"><?=$c['lang_pack']['products']['save'].' <span class="save_p">'.cart::iconv_price($oldPrice-$CurPrice).'</span>';?><span class="save_style">(<?=$save_discount;?>% Off)</span></div>
									<?php }*/ ?>
								</div>
							</div>
						</div>
					</div>
					<?php if(!$IsSeckill && $is_wholesale){?>
						<div class="rows">
							<h5><?=$c['lang_pack']['products']['wholesale']; ?>:</h5>
							<div class="widget prod_info_wholesale" data="<?=$products_row['Wholesale'];?>">
								<div class="pw_table clearfix">
									<?php 
									$wholesale_key=array();
									foreach((array)$wholesale_price as $k=>$v){
										$wholesale_key[]=$k;
									}
									$cur=0;
									foreach((array)$wholesale_price as $k=>$v){
										$cur++;
										?>
										<div class="pw_column" data-num="<?=$k;?>">
											<div class="pw_td bt"><?=count($wholesale_price)==$cur ? $k.'+' : (int)$k.'-'.($wholesale_key[$cur]-1);?></div>
											<div class="pw_td" data-price="<?=$v;?>" data-discount="<?=1-($v/$Price_1);?>"><?=cart::iconv_price($v);?></div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php }?>
					<?php 
						$OpenParameter=array();
						if($products_row['OpenParameter']){
							$OpenParameter=explode('|', $products_row['OpenParameter']);
						}
						in_array('Unit', $OpenParameter) || $Unit='';
						$pSKU=in_array('SKU', $OpenParameter)?$products_row['SKU']:'';
					?>
					<div class="rows prod_info_sku" sku="<?=$pSKU;?>"<?=!$products_row['SKU'] || !in_array('SKU', $OpenParameter)?' style="display:none;"':'';?>>
						<h5 class="row_th">SKU:</h5>
						<div class="widget prod_info_sku"><span><?=$pSKU;?></span></div>
					</div>
                    <?php if($MOQ>1){?>
                        <div class="widget prod_info_moq">
                            <label for="moq"><?=$c['lang_pack']['products']['moq'];?>: <?=$MOQ;?></label>
                        </div>
                    <?php }?>
					<?php 
						$isHaveAttr=(int)($attr_ary['Cart'] && $products_row['AttrId']==($TopCategory_row?$TopCategory_row['AttrId']:$category_row['AttrId'])); //是否有规格属性
						if($isHaveAttr || $isHaveOversea){
						?>
						<div class="rows">
							<?php include("{$c['default_path']}/products/detail/attribute.php");?>
                            <input id="quantity" class="qty_num" name="Qty1" autocomplete="off" type="hidden" value="" />
						</div>

					<?php } ?>

<!--					<div class="rows">-->
<!--						<h5>--><?//=$c['lang_pack']['products']['qty'];?><!--:</h5>-->
<!--						<div class="widget prod_info_quantity">-->
<!--			                <div class="qty_box"><div id="btn_cut">-</div></div>-->
<!--							<div class="quantity_box" data="--><?//=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?><!--"><input id="quantity" class="qty_num" name="Qty" autocomplete="off" type="text" value="--><?//=$MOQ;?><!--" stock="--><?//=$Max;?><!--" /></div>-->
<!--			                <div class="qty_box"><div id="btn_add">+</div></div>-->
<!--							<span class="themes_subtitle prod_info_inventory">(--><?//=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?><!--)</span>-->
<!--			                <div class="clear"></div>-->
<!--						</div>-->
<!--					</div>-->

					<?php if(in_array('freight', $c['plugins']['Used'])){?>
						<div class="rows">
							<h5><?=$c['lang_pack']['products']['shippingCost'];?>:</h5>
							<div class="widget key_info_line">
								<div class="key_info_right"> 
									<div class="shipping_cost_detail">
										<span class="shipping_cost_price"></span>
										<span class="shipping_cost_to"><?=$c['lang_pack']['products']['to'];?></span>
										<span id="shipping_flag" class="icon_flag"></span>
										<span id="shipping_cost_button" class="shipping_cost_button FontColor"></span>
									</div>
									<div class="themes_subtitle shipping_cost_info"><?=$c['lang_pack']['products']['shipEstimated'];?>:<span class="delivery_day"></span></div>
									<div class="shipping_cost_error"><?=$c['lang_pack']['products']['shipError'];?></div>
								</div>
							</div>
						</div>
					<?php }?>
					
				</div>
				<div class="widget prod_info_actions">
					<?php
					if($is_stockout){
						echo '<input type="button" value="'.$c['lang_pack']['products']['soldOut'].'" class="add_btn soldout" />';
						echo '<input type="button" value="'.$c['lang_pack']['products']['notice'].'" class="add_btn arrival" id="arrival_button" />';
					}else{
						echo '<input type="submit" value="'.$c['lang_pack']['products']['addToCart'].'" class="add_btn addtocart themes_button" id="addtocart_button" />';
						if($is_paypal_checkout){
							echo ly200::load_static('/static/themes/default/css/cart.css', '/static/themes/default/js/cart.js');
							if($c['NewFunVersion']>=4){//新版Paypal checkout
								echo '<div class="box_paypal">';
									echo '<input type="button" value="" class="add_btn" id="btn_paypal_replace" />';
									echo '<div id="paypal_button_container"></div>';
								echo '</div>';
							}else{//旧版Paypal checkout
								echo '<input type="button" value="" class="add_btn paypal_checkout_button" id="paypal_checkout_button" />';
								echo '<script src="//www.paypalobjects.com/api/checkout.js" async></script>';
							}
						}else{
							echo '<input type="button" value="'.$c['lang_pack']['products']['buyNow'].'" class="add_btn buynow BuyNowBgColor" id="buynow_button" />';
						}
					}?>
<!--                    <a class="btn_global add_btn" target="_blank"  href="https://api.whatsapp.com/send?phone=8619924351059&amp;text=May I help you?" style="display: inline-block;width: 170px;height:46px;background:  #fcd340; color: #fff;">--><?//=$c['lang_pack']['contactUs']; ?><!--</a>-->
                    <a class="btn_global add_btn" onclick="shareWa()" style="display: inline-block;width: 170px;height:46px;background:  #fcd340; color: #fff;"><?=$c['lang_pack']['contactUs']; ?></a>

                    <div class="clear"></div>
	            	<div class="favorite_box">
						<a href="javascript:;" class="global_trans favorite_btn add_favorite <?=in_array($ProId, $user_favorite_ary) ? 'is_in' : ''; ?>" data="<?=$ProId;?>"><?=$c['lang_pack']['products']['favorite'];?></a>
	            	</div>
					<div class="clear"></div>
					<?php
					//平台导流
					if(in_array('platform', $c['plugins']['Used'])){
						$platform=str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
						if(count($platform)) echo '<div class="platform_tit">'.$c['lang_pack']['products']['platform_tips'].':</div>';
						foreach((array)$platform as $k=>$v){
							if(!$v[0]['Url'.$c['lang']]) continue;
							if(count($v)>1){
					?>
								<span class="add_btn platform_btn <?=$k;?>_btn"><?=$c['lang_pack']['products'][$k];?><em></em>
									<div class="platform_ab">
										<?php foreach((array)$v as $v1){?>
											<a href="<?=$v1['Url'.$c['lang']];?>" target="_blank"><?=$v1['Name'.$c['lang']];?></a>
										<?php }?>
									</div>
								</span>
					<?php
							}else{
								$icon=@explode('_', $k);
								echo '<a href="'.$v[0]['Url'.$c['lang']].'" target="_blank" class="add_btn platform '.$icon[0].'_btn">'.$c['lang_pack']['products'][$k].'</a>';
							}
						}
					}?>
				</div>
				<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
				<input type="hidden" id="ItemPrice" name="ItemPrice" value="<?=$ItemPrice;?>" initial="<?=$ItemPrice;?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=$is_promotion && !$products_row['PromotionType']?$products_row['PromotionPrice']:'';?>" discount="<?=$is_promotion && $products_row['PromotionType']?$products_row['PromotionDiscount']:'';?>" old="<?=$oldPrice;?>" />
				<input type="hidden" name="Attr" id="attr_hide" value="[]" />
				<input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data(str::str_code($ext_ary, 'htmlspecialchars_decode')));?>" />
				<input type="hidden" name="products_type" value="<?=((int)$IsSeckill && (int)$SId)?2:0;?>" />
				<input type="hidden" name="SId" value="<?=(int)$SId;?>"<?=((int)$IsSeckill && (int)$SId)?' stock="'.$Max.'"':'';?> />
				<input type="hidden" id="CId" value="<?=(int)$country_default_row['CId'];?>" />
				<input type="hidden" id="CountryName" value="<?=$country_default_row['Country'];?>" />
				<input type="hidden" id="CountryAcronym" value="<?=$country_default_row['Acronym'];?>" />
				<input type="hidden" id="ShippingId" value="0" />
				<input type="hidden" id="attrStock" value="<?=(int)$products_row['SoldStatus'];?>" />
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script>
    function shareWa(str){
        // content是我们自己定义的一些需要分享的内容;
        var content="";
        content+="<?=$Name; ?>";
        //content+="\n"+"<?//=$BriefDescription; ?>//"+"\n";
        content+="\n"+"http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"+"\n";
        //主要实现是下面这行代码,来完成页面跳转到WhatsApp,并分享内容
        location="https://api.whatsapp.com/send?phone=8614739070501&text="+ encodeURIComponent(content);
    }

</script>
<?php if((int)$c['FunVersion'] && $group_promotion_ary) include("{$c['default_path']}/products/detail/combination.php");?>
<?php include("{$c['default_path']}/products/detail/description.php");?>
<div class="clear"></div>
<div class="clearfix">
	<div id="may_like" class="themes_box">
		<h2 class="b_title themes_box_title"><?=$c['lang_pack']['products']['mayLike'];?></h2>
		<div class="b_list">
			<?php
			if($TopCateId){
				$UId="0,{$TopCateId},";
				$cateid=$TopCateId;
			}else{
				$UId=category::get_UId_by_CateId($CateId);
				$cateid=$CateId;
			}
			$row=str::str_code(db::get_limit('products', "1 and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$cateid}' or ".category::get_search_where_by_ExtCateId($cateid, 'products_category').')'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 4));
			$len=count($row);
			foreach((array)$row as $k=>$v){
				$url=ly200::get_url($v, 'products');
				$price_ary=cart::range_price_ext($v);
			?>
				<dl class="pro_item clearfix<?=$k==0 ?' fir':'';?>">
					<dt><a class="pic_box" href="<?=$url;?>"><img src="<?=ly200::get_size_img($v['PicPath_0'], '500x500');?>" title="<?=$v['Name'.$c['lang']];?>" alt="<?=$v['Name'.$c['lang']];?>" /><span></span></a></dt>
					<dd class="pro_info">
						<div class="pro_name"><a href="<?=$url;?>" title="<?=$v['Name'.$c['lang']];?>"><?=$v['Name'.$c['lang']];?></a></div>
						<div class="pro_price"><em class="currency_data PriceColor"><?=$_SESSION['Currency']['Symbol'];?></em><span class="price_data PriceColor" data="<?=$price_ary[0];?>" keyid="<?=$v['ProId'];?>"><?=cart::iconv_price($price_ary[0], 2);?></span></div>
					</dd>
				</dl>
			<?php }?>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="blank12"></div>


<script>
	var $h5_w=0;
	$('.prod_info_form .rows h5').each(function(){
		var $wid=$(this).outerWidth();
		if($wid>$h5_w) $h5_w=$wid;
	});
	$('.prod_info_form .rows').css('padding-left',$h5_w+'px');
	$('.prod_info_form .rows h5').css('margin-left',-$h5_w+'px');
	// if($('body').hasClass('lang_fa')){
	// 	$('.detail_img_box').css('right':'389px') 
	// }
</script>