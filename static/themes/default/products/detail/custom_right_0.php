<?php !isset($c) && exit();?>
<div class="detail_right fr">
	<div class="widget prod_info_title"><?=html::website_h1('h1', $Name, 'itemprop="name"'); ?></div>
	<?php if($products_row['Number']){ ?>
		<div class="widget prod_info_number"><?=$products_row['Prefix'].$products_row['Number'];?></div>
	<?php } ?>
	<div class="widget prod_info_review">
		<?php
		if($c['config']['products_show']['Config']['review']){
			echo html::review_star($Rating);
		?>
			<a class="write_review review_count" href="#review_box"><?=$TotalRating.' '.$c['lang_pack']['user']['reviewCount'];?></a>
			<?php /*<a class="write_review track" href="<?=ly200::get_url($products_row, 'write_review');?>"><?=$c['lang_pack']['products']['writeReview'];?></a>*/ ?>
		<?php
		}
		/*if($c['FunVersion'] && in_array('product_inbox', $c['plugins']['Used'])){
		?>
			<div class="prod_info_inquiry fl"><a class="product_inquiry" href="javascript:;" data-user="<?=(int)$_SESSION['User']['UserId'];?>" data-proid="<?=$ProId;?>"><?=$c['lang_pack']['products']['haq'];?></a></div>
		<?php }*/ ?>
		<?php 
			$pro_info=ly200::get_pro_info($products_row);
			if($pro_info[0]){
			?>
			<span><?=$c['lang_pack']['sold']; ?>:<?=$pro_info[0]; ?></span>
		<?php } ?>
	</div>
	<form class="prod_info_form" name="prod_info_form" id="goods_form" action="/cart/add.html" method="post" target="_blank">
		<div class="bg">
			<div class="widget prod_info_price">
				<div class="widget price_left price_1">
					<?php $currency_row=db::get_all('currency', "IsUsed='1'");?>
					<div class="current_price">
						<div class="left">
							<dl class="widget prod_info_currency<?=count($currency_row)>1?' prod_info_currency_more':'';?>">
								<dt><a href="javascript:;"><?=$_SESSION['Currency']['Currency'];?><div class="arrow"><em></em><i></i></div></a></dt>
								<dd>
									<ul>
										<?php foreach((array)$currency_row as $v){?>
											<li><a href="javascript:;" data="<?=$v['Currency'];?>"><?=$v['Currency'];?></a></li>
										<?php }?>
									</ul>
								</dd>
							</dl>
							<strong id="cur_price" class="price"><?=$_SESSION['Currency']['Symbol'].cart::iconv_price($CurPrice, 2);?></strong>
						</div>
						<?php if($oldPrice>$CurPrice && $oldPrice>0){ ?>
							<div class="save_box fl">
								<del><?=cart::iconv_price($oldPrice);?></del>
							</div>
						<?php } ?>
						<?php
						if((int)$SId && (int)$IsSeckill){
							$time=$sales_row['EndTime']-$c['time'];
							if($time<86400*31){
						?>
								<div class="discount_count fl"><div class="discount_time" endtime="<?=date('Y/m/d H:i:s', $sales_row['EndTime']);?>"></div></div>
						<?php
							}
						}elseif($is_promotion){
							$time=$products_row['EndTime']-$c['time'];
							$promotion_discount=sprintf('%01.2f', ($Price_1-$ItemPrice)/$Price_1*100);
							$promotion_discount=($promotion_discount>0 && $promotion_discount<1)?1:@intval($promotion_discount);
							if($products_row['PromotionType']) $promotion_discount=100-$products_row['PromotionDiscount'];
							$month=ceil($time/86400);
							if($time<86400*31){
						?>
								<div class="discount_count fl"><div class="discount_time" endtime="<?=date('Y/m/d H:i:s', $products_row['EndTime']);?>"></div></div>
						<?php
							}
						}?>
					</div>
				</div>
			</div>
			<?php if(!$IsSeckill && $is_wholesale){?>
				<div class="rows">
					<h5><?=$c['lang_pack']['products']['wholesale']; ?>:</h5>
					<div class="widget prod_info_wholesale" data="<?=$products_row['Wholesale'];?>">
						<div class="pw_table clearfix">
							<?php 
							$wholesale_key=array();
							foreach((array)$wholesale_price as $k=>$v){
								$wholesale_key[]=$k;
							}
							$cur=0;
							foreach((array)$wholesale_price as $k=>$v){
								$cur++;
								?>
								<div class="pw_column" data-num="<?=$k;?>">
									<div class="pw_td bt"><?=count($wholesale_price)==$cur ? $k.'+' : (int)$k.'-'.($wholesale_key[$cur]-1);?></div>
									<div class="pw_td" data-price="<?=$v;?>" data-discount="<?=1-($v/$Price_1);?>"><?=cart::iconv_price($v);?></div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			<?php }?>
			<?php 
				$OpenParameter=array();
				if($products_row['OpenParameter']){
					$OpenParameter=explode('|', $products_row['OpenParameter']);
				}
				in_array('Unit', $OpenParameter) || $Unit='';
				$pSKU=in_array('SKU', $OpenParameter)?$products_row['SKU']:'';
			?>
			<div class="rows prod_info_sku" sku="<?=$pSKU;?>"<?=!$products_row['SKU'] || !in_array('SKU', $OpenParameter)?' style="display:none;"':'';?>>
				<h5 class="row_th">SKU:</h5>
				<div class="widget prod_info_sku"><span><?=$pSKU;?></span></div>
			</div>
			<div class="rows">
				<?php include("{$c['default_path']}/products/detail/attribute.php");?>
			</div>
			<div class="prod_info_detail">
				<?php /*if($c['config']['products_show']['Config']['wholesale'] && !$IsSeckill && $is_wholesale){?>
					<div class="widget prod_info_wholesale" data="<?=$products_row['Wholesale'];?>">
						<div class="pw_title"><?=$c['lang_pack']['products']['wholesale'];?>:</div>
						<div class="pw_table clearfix">
							<div class="pw_table_box clearfix" data-show="3" data-sort="right">
								<?php foreach((array)$wholesale_price as $k=>$v){?>
									<div class="pw_column" data-num="<?=$k;?>">
										<div class="pw_td"><?=$k;?>+</div>
										<div class="pw_td" data-price="<?=$v;?>" data-discount="<?=sprintf('%01.2f', 1-($v/$Price_1));?>"><?=cart::iconv_price($v);?></div>
									</div>
								<?php }?>
							</div>
						</div>
						<?php if(count($wholesale_price)>3){?><a href="javascirpt:;" class="pw_btn"><i></i><em></em></a><?php }?>
					</div>
				<?php }*/?>
				<?php /*if($c['config']['products_show']['Config']['freight']){?>
					<div class="widget key_info_line">
						<div class="key_info_left"><?=$c['lang_pack']['products']['shippingCost'];?>:</div>
						<div class="key_info_right"> 
							<div class="shipping_cost_detail">
								<span class="shipping_cost_price"></span>
								<span class="shipping_cost_to"><?=$c['lang_pack']['products']['to'];?></span>
								<span id="shipping_flag" class="icon_flag"></span>
								<span id="shipping_cost_button" class="shipping_cost_button FontColor"></span>
							</div>
							<div class="shipping_cost_info"><?=$c['lang_pack']['products']['shipEstimated'];?>:<span class="delivery_day"></span></div>
							<div class="shipping_cost_error"><?=$c['lang_pack']['products']['shipError'];?></div>
						</div>
					</div>
				<?php }*/?>
<!--				<div class="rows">-->
<!--					<h5>--><?//=$c['lang_pack']['products']['qty'];?><!--:</h5>-->
<!--					<div class="widget prod_info_quantity">-->
<!--						<div class="qty_box"><div id="btn_cut">-</div></div>-->
<!--						<div class="quantity_box" data="--><?//=htmlspecialchars('{"min":'.$MOQ.',"max":'.$Max.',"count":'.$MOQ.'}');?><!--"><input id="quantity" class="qty_num" name="Qty" autocomplete="off" type="text" value="--><?//=$MOQ;?><!--" stock="--><?//=$Max;?><!--" /></div>-->
<!--						<div class="qty_box"><div id="btn_add">+</div></div>-->
<!--						<span class="prod_info_inventory">(--><?//=str_replace(array('%num%','%pieces%'), array('<b id="inventory_number">'.$Max.'</b>', $Unit), $c['lang_pack']['products']['available']);?><!--)</span>-->
<!--						<div class="clear"></div>-->
<!--					</div>-->
<!--				</div>-->
			</div>
		</div>
		<div class="widget prod_info_actions">
			<?php if($is_stockout && $products_row['StockOut']){?>
				<input type="button" value="<?=$c['lang_pack']['products']['notice'];?>" class="add_btn arrival" id="arrival_button">
			<?php }elseif($is_stockout && !$products_row['StockOut']){?>
				<input type="button" value="<?=$c['lang_pack']['products']['soldOut'];?>" class="add_btn soldout">
			<?php }else{?>
				<input type="submit" value="<?=$c['lang_pack']['products']['addToCart'];?>" class="add_btn addtocart AddtoCartBgColor" id="addtocart_button">
				<?php
                if($is_paypal_checkout){
                    echo ly200::load_static('/static/themes/default/css/cart.css', '/static/themes/default/js/cart.js');
                    if($c['NewFunVersion']>=4){//新版Paypal checkout
                        echo '<div class="box_paypal">';
                            echo '<input type="button" value="" class="add_btn" id="btn_paypal_replace" />';
                            echo '<div id="paypal_button_container"></div>';
                        echo '</div>';
                    }else{//旧版Paypal checkout
                        echo '<input type="button" value="" class="add_btn paypal_checkout_button" id="paypal_checkout_button" />';
                        echo '<script src="//www.paypalobjects.com/api/checkout.js" async></script>';
                    }
                }else{?>
					<input type="button" value="<?=$c['lang_pack']['products']['buyNow'];?>" class="add_btn buynow BuyNowBgColor" id="buynow_button" />
				<?php }?>
			<?php }?>
            <?php
			//平台导流
			$platform=str::json_data(str::str_code($products_row['Platform'], 'htmlspecialchars_decode'), 'decode');
			if(in_array('platform', $c['plugins']['Used']) && $platform){
			?>
				<div class="clear"></div>
				<div class="platform_box">
					<div class="title"><?=$c['lang_pack']['products']['platform_tips'];?>:</div>
					<?php foreach((array)$platform as $k=>$v){ 
						$icon=@explode('_', $k);
						?>
						<a href="<?=$v[0]['Url'.$c['lang']];?>" rel="nofollow" target="_blank" title="<?=$c['lang_pack']['products'][$k];?>" class="add_btn platform_btn <?=$icon[0];?>_btn"><?=$c['lang_pack']['products'][$k];?></a>
					<?php }?>
				</div>
            <?php }?>
			<div class="clear"></div>
			<?php /*if($c['config']['products_show']['Config']['favorite']){?>
				<a href="javascript:;" class="favorite_btn add_favorite" data="<?=$ProId;?>"><?=$c['lang_pack']['products']['favorite'];?></a>
			<?php }*/ ?>
			<a href="<?=ly200::get_url($products_row, 'products');?>" class="view_btn"><?=$c['lang_pack']['products']['more_details'];?></a>
			<div class="clear"></div>
		</div>
		<input type="hidden" id="ProId" name="ProId" value="<?=$ProId;?>" />
		<input type="hidden" id="ItemPrice" name="ItemPrice" value="<?=$ItemPrice;?>" initial="<?=$ItemPrice;?>" sales="<?=$is_promotion?1:0;?>" salesPrice="<?=$is_promotion && !$products_row['PromotionType']?$products_row['PromotionPrice']:'';?>"  discount="<?=$is_promotion && $products_row['PromotionType']?$products_row['PromotionDiscount']:'';?>" old="<?=$oldPrice;?>" />
		<input type="hidden" name="Attr" id="attr_hide" value="[]" />
		<input type="hidden" id="ext_attr" value="<?=htmlspecialchars(str::json_data($ext_ary));?>" />
		<input type="hidden" name="products_type" value="<?=((int)$IsSeckill && (int)$SId)?2:0;?>" />
		<input type="hidden" name="SId" value="<?=(int)$SId;?>"<?=((int)$IsSeckill && (int)$SId)?' stock="'.$sales_row['RemainderQty'].'"':'';?> />
		<input type="hidden" id="CId" value="<?=(int)$country_default_row['CId'];?>" />
		<input type="hidden" id="CountryName" value="<?=$country_default_row['Country'];?>" />
		<input type="hidden" id="CountryAcronym" value="<?=$country_default_row['Acronym'];?>" />
		<input type="hidden" id="ShippingId" value="0" />
		<input type="hidden" id="attrStock" value="<?=(int)$c['config']['products_show']['Config']['stock'];?>" />
	</form>
	<?php /*
	<div class="prod_info_item">
		<?php
		if($attr_ary['Common']){
			$all_value_ary=$attrid=array();
			foreach($attr_ary['Common'] as $v){ $attrid[]=$v['AttrId']; }
			$attrid_list=implode(',', $attrid);
			$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
			foreach($value_row as $v){ $all_value_ary[$v['AttrId']][$v['VId']]=$v; }
		?>
			<div class="item_specifics">
				<div class="title"><?=$c['lang_pack']['products']['specifics'];?></div>
				<?php
				foreach((array)$attr_ary['Common'] as $k=>$v){
					if(!$v || !$v['Name'.$c['lang']] || ($v['Type']==1 && !$selected_ary['Id'][$v['AttrId']]) || ($v['Type']==0 && !$selected_ary['Value'][$v['AttrId']])) continue;
				?>
					<span>
						<strong><?=$v['Name'.$c['lang']];?>:</strong>
						<?php
						if($v['Type']==1 && is_array($all_value_ary[$v['AttrId']])){
							$i=0;
							foreach($all_value_ary[$v['AttrId']] as $k2=>$v2){
								if(in_array($v2['VId'], $selected_ary['Id'][$v['AttrId']])){
									echo ($i?', ':'').$v2['Value'.$c['lang']];
									++$i;
								}
							}
						}else echo $selected_ary['Value'][$v['AttrId']];
						?>
					</span>
				<?php }?>
			</div>
		<?php }?>
	</div>
	*/?>
</div>
<script>
	var $h5_w=0;
	$('.prod_info_form .rows h5').each(function(){
		var $wid=$(this).outerWidth();
		if($wid>$h5_w) $h5_w=$wid;
	});
	$('.prod_info_form .rows').css('padding-left',$h5_w+'px');
	$('.prod_info_form .rows h5').css('margin-left',-$h5_w+'px');
</script>