<?php !isset($c) && exit();?>
<?php

//模块设置
$cfg_module_row=db::get_value('config_module', "Themes='{$c['theme']}'", 'ListData');
$list_data=str::json_data($cfg_module_row, 'decode');
$cfg_module_ary=array();
foreach((array)$list_data as $k=>$v){
	$cfg_module_ary[$k]=$v;
}
substr_count($_SERVER['REQUEST_URI'], '/search/') && $cfg_module_ary['Narrow']=0;

$no_narrow_url='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, Narrow'));
$no_list_url='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, List'));
$no_sort_url='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, Sort'));
$no_price_url='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, Ext, page, Price'));
$no_page_url=ly200::get_query_string(ly200::query_string('m, a, CateId, page'));

$CateId=(int)$_GET['CateId'];
$Narrow=str::str_code($_GET['Narrow'], 'urlencode');
$PriceRange=str::str_code($_GET['Price']);
$Ext=(int)$_GET['Ext'];
$Sort=($_GET['Sort'] && $c['products_sort'][$_GET['Sort']])?$_GET['Sort']:'1a';

$Column=$c['nav_cfg'][3]['name'.$c['lang']];//Products
$narrow_len=2;//筛选显示行数
$page_count=(int)$cfg_module_ary['OrderNumber'];//显示数量

//产品筛选
$where='1';
if($CateId){
	$UId=category::get_UId_by_CateId($CateId);
	$where.=" and (CateId in(select CateId from products_category where UId like '{$UId}%') or CateId='{$CateId}' or ".category::get_search_where_by_ExtCateId($CateId, 'products_category').')';
	$category_row=db::get_one('products_category', "CateId='$CateId'");
	if(!$category_row){//分类不存在
		@header('HTTP/1.1 404');
		exit;
	}
	
	$Column=$category_row['Category'.$c['lang']];
	$category_description_row=str::str_code(db::get_one('products_category_description', "CateId='$CateId'", "Description{$c['lang']}"));
}
if($Ext){
	$Ext=($Ext<1 || $Ext>4)?1:$Ext;
	$where.=$c['where']['products_ext'][$Ext];
	switch($Ext){
		case 1: $Column=$c['nav_cfg'][8]['name'.$c['lang']]; break;
		case 2: $Column=$c['nav_cfg'][9]['name'.$c['lang']]; break;
		case 3: $Column=$c['nav_cfg'][10]['name'.$c['lang']]; break;
		case 4: $Column=$c['nav_cfg'][11]['name'.$c['lang']]; break;
	}

}
$ScreenType=(int)db::get_value('config', 'GroupId="products_show" and Variable="ScreenType"', 'Value');
$ScreenPosition=(int)db::get_value('config', 'GroupId="products_show" and Variable="ScreenPosition"', 'Value');

$screenAry=where::products($PriceRange, $Narrow);
$where.=$screenAry[0];//条件
$screenAry[1] && $Column=$screenAry[1];//标题
$price_range=$screenAry[2];//价格范围
$Narrow_ary=$screenAry[3];//筛选属性
$OrderBy=$screenAry[4];//条件排序
$Keyword=($_GET['Keyword']?$_GET['Keyword']:$_POST['Keyword']);

$page=(int)$_GET['page'];
$products_list_row=str::str_code(db::get_limit_page('products', $where.$c['where']['products'], '*', $OrderBy.$c['products_sort'][$Sort].$c['my_order'].'ProId desc', $page, $page_count));

//记录搜索痕迹
ly200::search_logs($products_list_row[1]);

(!$page || $page>$products_list_row[3]) && $page=1;
$Column=$screenAry[1]?str_replace(array('%a','%b','%c','%k'), array(($page_count*($page-1)+1), $page_count*($page-1)+count($products_list_row[0]), $products_list_row[1], $screenAry[1]), $c['lang_pack']['item_column']):$Column;

//分类属性
if($category_row['UId']!='0,'){
	$TopCateId=category::get_top_CateId_by_UId($category_row['UId']);
	$SecCateId=category::get_FCateId_by_UId($category_row['UId']);
	$TopCateId && $TopCategory_row=str::str_code(db::get_one('products_category', "CateId='$TopCateId'"));
	$SecCateId && $SecCategory_row=str::str_code(db::get_one('products_category', "CateId='$SecCateId'"));
}
$UId_ary=@explode(',', $category_row['UId']);

//该分类的普通属性
if($CateId>0){
	$attr_ary=$all_value_ary=$vid_name_ary=$attrid=$value_ary=$attr_type_ary=array();
	$whereValue=" and (CateId like '%|{$CateId}|%'";
	if(in_array('screening', $c['plugins']['Used'])){
		$all_attribute_row=db::get_all('products_category', "UId like '{$category_row['UId']}{$CateId}%'", 'CateId');
		foreach($all_attribute_row as $v){
			$whereValue.=" or CateId like '%|{$v['CateId']}|%'";
		}
	}
	$whereValue.=')';
	$where='1'.$whereValue;
	//$where='1'.where::keyword("|{$CateId}|", array('CateId'));
	$where.=where::equal(array('CartAttr'=>'0', 'DescriptionAttr'=>'0', 'ScreenAttr'=>'1'));
	$products_attr=str::str_code(db::get_all('products_attribute', $where, "AttrId, Type, Name{$c['lang']}", $c['my_order'].'AttrId asc'));
	foreach((array)$products_attr as $v){
		$attr_ary[$v['AttrId']]=$v;
		if($v['Type']==1){//选项
			$attrid['Option'][]=$v['AttrId'];
		}else{//文本框
			$attrid['Text'][]=$v['AttrId'];
		}
	}
	if($attrid['Option']){
		//普通属性（选项）
		$attrid_list=@implode(',', $attrid['Option']);
		!$attrid_list && $attrid_list=-1;
		$value_row=str::str_code(db::get_all('products_attribute_value', "AttrId in ($attrid_list)", '*', $c['my_order'].'VId asc')); //属性选项
		foreach($value_row as $v){
			$all_value_ary[$v['AttrId']][$v['VId']]=$v;
			$vid_name_ary[$v['VId']]=$v['Value'.$c['lang']];
			$attr_type_ary[$v['VId']]=$attr_ary[$v['AttrId']]['Type'];
		}
	}
	if($attrid['Text']){
		//普通属性（文本框）
		$attrid_list=@implode(',', $attrid['Text']);
		!$attrid_list && $attrid_list=-1;
		$value_row=str::str_code(db::get_all('products_selected_attribute', "IsUsed=1 and Value{$c['lang']}!='' and AttrId in ($attrid_list)", "SeleteId, AttrId, VId, Value{$c['lang']}", 'SeleteId asc'));//属性选项，SeleteId以最小值为准
		foreach($value_row as $v){
			if(!$value_ary[$v['AttrId']][$v['Value'.$c['lang']]]){//没记录
				$value_ary[$v['AttrId']][$v['Value'.$c['lang']]]=$v['SeleteId'];
				$all_value_ary[$v['AttrId']][$v['SeleteId']]=$v;
				$vid_name_ary['s'.$v['SeleteId']]=$v['Value'.$c['lang']];
			}
		}
	}
	unset($value_ary);
}

//SEO
if(!$category_row){
	$spare_ary=(array)str::str_code(db::get_one('meta', "Type='products'"));
}elseif($category_row['UId']!='0,'){//非大类
	if($category_row['SubCateCount']){
		$subcate_row=db::get_limit('products_category', "UId like '{$category_row['UId']}{$CateId},%'", 'Category'.$c['lang'], $c['my_order'].'CateId asc', 0, 20);
		foreach((array)$subcate_row as $v) $subcateStr.=','.$v['Category'.$c['lang']];
	}
	$spare_ary=array(
		'SeoTitle'		=>	$category_row['Category'.$c['lang']],
		'SeoKeyword'	=>	$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']].$subcateStr,
		'SeoDescription'=>	$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']].$subcateStr
	);
}else{//大类
	$subcateStr='';
	$subcate_row=db::get_limit('products_category', "UId like '0,{$CateId},%'", 'Category'.$c['lang'], $c['my_order'].'CateId asc', 0, 20);
	foreach((array)$subcate_row as $v) $subcateStr.=','.$v['Category'.$c['lang']];
	$spare_ary=array(
		'SeoTitle'		=>	$category_row['Category'.$c['lang']],
		'SeoKeyword'	=>	$category_row['Category'.$c['lang']].$subcateStr,
		'SeoDescription'=>	$category_row['Category'.$c['lang']].$subcateStr
	);
}
if($Ext){
	$ExtTypeAry=array(1	=>'new', 2=>'hot', 3=>'best_deals', 4=>'special_offer');
	$seo_row=db::get_one('meta', "Type='{$ExtTypeAry[$Ext]}'");
	$spare_ary=array(
		'SeoTitle'		=>	$seo_row['SeoTitle'.$c['lang']],
		'SeoKeyword'	=>	$seo_row['SeoKeyword'.$c['lang']],
		'SeoDescription'=>	$seo_row['SeoDescription'.$c['lang']]
	);
}

include('static.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="canonical" href="<?=ly200::get_domain();?>/products/"/>
<?=ly200::seo_meta($category_row, $spare_ary);?>
<?php include("{$c['static_path']}/inc/static.php");?>
<script type="text/javascript">$(function(){products_list_obj.init();});</script>
<?php
if(in_array('facebook_pixel', $c['plugins']['Used']) && substr_count($_SERVER['REQUEST_URI'], '/search/')){
	//Facebook Pixel
?>
	<!-- Facebook Pixel Code -->
	<script type="text/javascript">
	<!-- When a search is made, such as a product query. -->
	fbq('track', 'Search', {
		search_string: '<?=str::str_code(stripslashes($_GET['Keyword']));?>',//搜索关键词
		content_category: 'Product Search',//分类
		content_ids: ['0'],
		value: '0.00',//数值
		currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
	});
	</script>
	<!-- End Facebook Pixel Code -->
<?php }?>
</head>

<body class="lang<?=$c['lang'];?>">
<!--add by jay start-->
<!--edit by zhz -->
<?php
//    if ($products_list_row[1] == 0){
//        header('location:/products/purchase_request.html');
//    }
?>
<!--add by jay end-->
<?php
include("{$c['theme_path']}/inc/header.php");
?>
<div id="main" class="wide">
	<div id="location" class="themes_subtitle"><?=$c['lang_pack']['products']['position'];?>: <a href="/"><?=$c['lang_pack']['products']['home'];?></a><?=$CateId?ly200::get_web_position($category_row, 'products_category', '', ' / '):' / '.$Column;?></div>
	<?php if($cfg_module_ary['IsColumn']){?>
		<div class="pro_left fl">
			<?php include("{$c['static_path']}inc/products_left/products_left.php");?>
		</div>
	<?php }?>
    <!--edit by zhz start -->
    <?php if ($products_list_row[1] != 0){ ?>
        <div class="<?=$cfg_module_ary['IsColumn']?'pro_right fr':'pro_main';?>">
            <?php if($category_description_row['Description'.$c['lang']]){?>
                <div class="category_description">
                    <?=str::str_code($category_description_row['Description'.$c['lang']], 'htmlspecialchars_decode'); ?>
                </div>
            <?php }?>
            <?php
            if(in_array('screening', $c['plugins']['Used']) && $attr_ary && !substr_count($_SERVER['REQUEST_URI'], '/search/') && $ScreenPosition==0){
                if($ScreenType==0){
                    //下拉形式
            ?>
                <div class="new_ns_list clean">
                    <div class="tit"><?=$c['lang_pack']['narrow_by'];?>:</div>
                    <?php
                    $all_count=0;
                    foreach((array)$attr_ary as $v){
                    ?>
                        <dl>
                            <dt><?=$v['Name'.$c['lang']];?><i></i><em></em></dt>
                            <dd>
                                <?php
                                foreach((array)$all_value_ary[$v['AttrId']] as $k2=>$v2){
                                    $v['Type']==0 && $k2='s'.$k2;
                                    $url=ly200::get_narrow_url($no_narrow_url, $Narrow_ary, $k2, $v['Type']);
                                    $num=ly200::get_narrow_pro_count($Narrow_ary, $k2, $CateId, $v['Type']);
                                    if(!$num) continue;
                                    $all_count++;
                                ?>
                                    <a rel="nofollow" href="<?=$url;?>"<?=in_array($k2, $Narrow_ary)?' class="current"':'';?>>
                                        <em class="ns_icon_checkbox"></em>
                                        <span><?=$v2['Value'.$c['lang']];?><i>(<?=$num;?>)</i></span>
                                    </a>
                                <?php }?>
                            </dd>
                        </dl>
                    <?php }?>
                </div>
                <?php if(!$all_count){?><script type="text/javascript">$('.new_ns_list').remove();</script><?php }?>
            <?php
                }else{
                    //按钮形式
            ?>
                <div class="ns_list">
                    <?php
                    $i=0;
                    foreach((array)$attr_ary as $v){
                        $narrow_count=0;
                    ?>
                        <dl class="clearfix">
                            <dt><?=$v['Name'.$c['lang']];?>: </dt>
                            <dd>
                                <?php
                                foreach((array)$all_value_ary[$v['AttrId']] as $k2=>$v2){
                                    $v['Type']==0 && $k2='s'.$k2;
                                    $url=ly200::get_narrow_url($no_narrow_url, $Narrow_ary, $k2, $v['Type']);
                                    $num=ly200::get_narrow_pro_count($Narrow_ary, $k2, $CateId, $v['Type']);
                                    if(!$num) continue;
                                    ++$narrow_count;
                                ?>
                                <a rel="nofollow" href="<?=$url;?>" hidefocus="true"<?=in_array($k2, $Narrow_ary)?' class="current"':'';?>>
                                    <em class="ns_icon_checkbox"></em>
                                    <span><?=$v2['Value'.$c['lang']];?><i>(<?=$num;?>)</i></span>
                                </a>
                                <?php }?>
                            </dd>
                        </dl>
                        <?php if(!$narrow_count){?><script type="text/javascript">$('.ns_list>dl:eq(<?=$i;?>)').addClass('hide');</script><?php }?>
                    <?php
                        ++$i;
                    }?>
                    <div class="themes_bor prop_more">
                        <?php if(count($attr_ary)>$narrow_len){?>
                            <div id="more_prop" class="attr_extra"><b></b><?=$c['lang_pack']['more_options'];?></div>
                            <div id="less_prop" class="attr_extra" style="display:none;"><b class="up"></b><?=$c['lang_pack']['hide'];?></div>
                        <?php }?>
                    </div>
                </div>
                <script type="text/javascript">
                $i=0;
                $('.ns_list>dl').not('.hide').each(function(){
                    if($i<<?=$narrow_len;?>){
                        $(this).attr('overshow', 'false').show();
                    }else{
                        $(this).attr('overshow', 'true');
                    }
                    ++$i;
                });
                if($i><?=$narrow_len;?>){
                    $('#more_prop').css('display', 'inline-block');
                }
                </script>
            <?php
                }
            }?>
            <?php if($Narrow || $price_range){?>
                <p class="new_ns_title">
                    <?php foreach((array)$Narrow_ary as $v){?>
                        <a rel="nofollow" href="<?=ly200::get_narrow_url($no_narrow_url, $Narrow_ary, $v, $attr_type_ary[$v]);?>" class="remove" title="Remove"><span><?=$vid_name_ary[$v];?></span><em></em></a>
                    <?php }?>
                    <?php if($price_range){?>
                        <a rel="nofollow" href="<?=$no_price_url;?>" class="remove" title="Remove"><span><i class="currency_data"></i><?=$price_range[0];?>-<i class="currency_data"></i><?=$price_range[1];?></span><em></em></a>
                    <?php }?>
                    <a rel="nofollow" href="<?='?'.ly200::get_query_string(ly200::query_string('m, a, CateId, page, Price, Narrow'));?>" class="remove_all" title="Remove All"><span><?=$c['lang_pack']['remove_all'];?></span><em></em></a>
                    <div class="clear"></div>
                </p>
            <?php }?>
            <?php
            if($c['themes_type']==1){ //不是新版营销型模板
            ?>
                <div id="prod_sort">
                    <span><?=$c['lang_pack']['products']['sort_by']; ?>:</span>
                    <a<?=$Sort=='1d'?' class="cur"':'';?> rel="nofollow" href="<?=$no_sort_url.'&Sort=1d';?>"><em class="sort_icon_popular"></em><?=$c['lang_pack']['most_popular'];?></a>
                    <?php if (!@in_array('batch_edit', $c['plugins']['Used'])) { ?>
                        <a<?=$Sort=='2d'?' class="cur"':'';?> rel="nofollow" href="<?=$no_sort_url.'&Sort=2d';?>"><em class="sort_icon_sales"></em><?=$c['lang_pack']['sales'];?></a>
                        <a<?=$Sort=='3d'?' class="cur"':'';?> rel="nofollow" href="<?=$no_sort_url.'&Sort=3d';?>"><em class="sort_icon_favorites"></em><?=$c['lang_pack']['fav'];?></a>
                    <?php } ?>
                    <a<?=$Sort=='4d'?' class="cur"':'';?> rel="nofollow" href="<?=$no_sort_url.'&Sort=4d';?>"><em class="sort_icon_new"></em><?=$c['lang_pack']['new'];?></a>
                    <a<?=($Sort=='5d' || $Sort=='5a')?' class="cur"':'';?> rel="nofollow" href="<?=$no_sort_url.'&Sort='.($Sort=='5a'?'5d':'5a');?>"><em class="sort_icon_price"></em><?=$c['lang_pack']['price'];?><i class="<?php if($Sort=='5d') echo 'sort_icon_arrow_down'; elseif($Sort=='5a') echo 'sort_icon_arrow_up'; else echo 'sort_icon_arrow';?>"></i></a>
                    <div class="prod_price fl">
                        <span class="pp_inputbox"><em class="currency_data"></em><input type="text" id="minprice" class="min_box" autocomplete="off" value="<?=$price_range[0];?>"></span>
                        <span class="pp_heng">-</span>
                        <span class="pp_inputbox"><em class="currency_data"></em><input type="text" id="maxprice" class="max_box" autocomplete="off" value="<?=$price_range[1];?>"></span>
                        <input type="button" id="submit_btn" class="pp_btn" value="<?=$c['lang_pack']['zhzgo']; ?>" />
                        <input type="hidden" class="no_price_url" value="<?=$no_price_url;?>" />
                    </div>
                    <div class="clear"></div>
                </div>
            <?php }?>
            <?php echo $products_page_contents;?>
        </div>
    <?php }else{ ?>
        <div class="<?=$cfg_module_ary['IsColumn']?'pro_right fr':'pro_main';?> content_blank" style="background: url(/static/themes/default/mobile/images/bg_no_product.png) no-repeat center 3rem;padding: 10rem 0rem;">
            <br>
            <br>
            <br>
            <?=$c['lang_pack']['mobile']['no_data'];?>
            <br>
            <br>
            <a style="text-decoration: underline;" href="/account/rfp/"><?=$c['lang_pack']['products']['rfqTitle'];?></a>
        </div>
    <?php  }?>
    <!--edit by zhz end -->
	<div class="blank25"></div>
</div>
<?php include("{$c['theme_path']}/inc/footer.php");?>
<?php
//列表页特效跟随主色调
$cfg_module_style=db::get_value('config_module', 'IsDefault=1', 'StyleData');
$cfg_module_style=str::json_data($cfg_module_style, 'decode');
?>
<style>
.prod_list .prod_box.hover_1 .prod_box_pic{ border-color:<?=$cfg_module_style['FontColor']?>;}
.prod_list .prod_box.hover_1 .prod_box_info{ background:<?=$cfg_module_style['FontColor']?>;}
.prod_list .prod_box .prod_box_view{ background:<?=$cfg_module_style['FontColor']?>;}
.prod_list .prod_box.hover_1 .prod_box_view{ border-top:1px solid #fff;}
.prod_list .prod_box .prod_box_button{ border-top:1px #fff solid;}
.prod_list .prod_box .prod_box_button>div{ border-right:1px solid #fff;}
</style>
</body>
</html>