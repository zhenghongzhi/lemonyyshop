<?php !isset($c) && exit();?>
<?php
$IsMarketing=$c['themes_type']==2 ? 1 : 0; //是否为品牌风格
$ProId=(int)$_GET['ProId'];
$products_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
if(!$products_row){
	@header('HTTP/1.1 404');
	exit;
}

//首先更新评论
$count=(int)db::get_row_count('products_review', "ProId='{$ProId}' and ReId=0");
$rating=(float)db::get_sum('products_review', "ProId='{$ProId}' and ReId=0", 'Rating');
db::update('products', "ProId='{$ProId}'", array('Rating'=>($count?($rating/$count):0), 'TotalRating'=>$count));

//模块设置
$cfg_module_row=db::get_value('config_module', "Themes='{$c['theme']}'", 'ListData');
$list_data=str::json_data($cfg_module_row, 'decode');
$cfg_module_ary=array();
foreach((array)$list_data as $k=>$v){
	$cfg_module_ary[$k]=$v;
}

$Name=htmlspecialchars_decode($products_row['Name'.$c['lang']]);
$BriefDescription=htmlspecialchars_decode($products_row['BriefDescription'.$c['lang']]);
$Price_0=(float)$products_row['Price_0'];
$Price_1=(float)$products_row['Price_1'];
$MOQ=(int)$products_row['MOQ'];
$Max=(int)$products_row['Stock']; //最大购买上限
//$products_row['MaxOQ']>0 && $products_row['MaxOQ']<$Max && $Max=$products_row['MaxOQ'];//最大购买量
$CateId=(int)$products_row['CateId'];
$CateId && $category_row=str::str_code(db::get_one('products_category', "CateId='$CateId'"));
$products_description_row=str::str_code(db::get_one('products_description', "ProId='$ProId'"));

//产品分类
if($category_row['UId']!='0,'){
	$TopCateId=category::get_top_CateId_by_UId($category_row['UId']);
	$SecCateId=category::get_FCateId_by_UId($category_row['UId']);
	$TopCategory_row=str::str_code(db::get_one('products_category', "CateId='$TopCateId'"));
}
$UId_ary=@explode(',', $category_row['UId']);

//产品售卖状态
$is_stockout=(($products_row['SoldStatus']!=1 && ($products_row['Stock']<$products_row['MOQ'] || $products_row['Stock']<1)) || ($products_row['SoldOut']==1 && $products_row['IsSoldOut']==0) || ($products_row['IsSoldOut'] && ($products_row['SStartTime']>$c['time'] || $c['time']>$products_row['SEndTime'])));

//产品单位
if($products_row['Unit']){//产品自身设置单位
	$Unit=$products_row['Unit'];
}elseif($c['config']['products_show']['Config']['item'] && $c['config']['products_show']['item']){//产品统一设置单位
	$Unit=$c['config']['products_show']['item'];
}else{
	$Unit=$c['lang_pack']['products']['units'];
}

//产品评论
$pro_info=ly200::get_pro_info($products_row);
$Rating=$pro_info[2];
$TotalRating=$pro_info[3];

//产品属性
$IsCombination=(int)$products_row['IsCombination']; //是否开启规格组合
$attr_ary=$color_attr_ary=$selected_ary=$color_picpath_ary=array();
$isHaveOversea=count($c['config']['Overseas']); //是否开启海外仓
if((int)$c['config']['global']['Overseas']==0){ //关闭海外仓功能
	$isHaveOversea=1;
}
if($CateId || $isHaveOversea){
	$products_attr=str::str_code(db::get_all('products_attribute', "CateId like '%|{$CateId}|%' and DescriptionAttr=0", "AttrId, Name{$c['lang']}, CartAttr, ColorAttr, Type", $c['my_order'].'AttrId asc'));
	foreach((array)$products_attr as $v){
		if($v['CartAttr']){ //规格属性
			$attr_ary['Cart'][$v['AttrId']]=$v;
		}else{ //普通属性
			$attr_ary['Common'][$v['AttrId']]=$v;
		}
		(int)$v['ColorAttr'] && $color_attr_ary[]=$v['AttrId'];
	}
	$products_selected_attr=str::str_code(db::get_all('products_selected_attr', "ProId='{$ProId}'"));
	if($products_selected_attr){
		$products_attr_status_ary=$color_attr_ary=array();
		foreach((array)$products_selected_attr as $v){
			$products_attr_status_ary[$v['AttrId']]=(int)$v['IsUsed'];
			(int)$v['IsColor'] && $color_attr_ary[]=$v['AttrId'];
		}
	}
	$selected_row=str::str_code(db::get_all('products_selected_attribute', "ProId='{$ProId}' and IsUsed=1", '*', 'SeleteId asc'));
	foreach($selected_row as $v){
		$selected_ary['Id'][$v['AttrId']][]=$v['VId']; //记录勾选属性ID
		$selected_ary['MyOrder'][$v['VId']]=$v['MyOrder']; //记录勾选属性排序
		$v['AttrId']>0 && $v['VId']==0 && $v['OvId']<2 && $selected_ary['Value'][$v['AttrId']]=$v['Value'.$c['lang']]; //文本框内容
		$v['AttrId']==0 && $v['VId']==0 && $v['OvId']>0 && $selected_ary['Overseas'][]=$v['OvId']; //记录勾选属性ID 发货地
	}
	$color_row=str::str_code(db::get_all('products_color', "ProId='{$ProId}'", 'VId, PicPath_0'));
	foreach((array)$color_row as $k=>$v){//统计产品颜色图片
		if(!$v['PicPath_0']) continue;
		if(is_file($c['root_path'].$v['PicPath_0'])){
			$color_picpath_ary[$v['VId']]=$v['PicPath_0'];
		}
	}
}

//产品价格和折扣
$CurPrice=$products_row['Price_1'];
$is_wholesale=(in_array('wholesale', $c['plugins']['Used']) && $products_row['Wholesale'] && $products_row['Wholesale']!='[]');
if($is_wholesale){
	$wholesale_price=str::json_data(htmlspecialchars_decode($products_row['Wholesale']), 'decode');
	foreach((array)$wholesale_price as $k=>$v){
		if($MOQ<$k) break;
		$CurPrice=(float)$v;
	}
	$maxPrice=reset($wholesale_price);
	$minPrice=end($wholesale_price);
}
$discount=($Price_1-$CurPrice)/((float)$Price_1?$Price_1:1)*100;

//产品促销
$is_promotion=((int)$products_row['IsPromotion'] && $products_row['StartTime']<$c['time'] && $c['time']<$products_row['EndTime']);
if($is_promotion && !$products_row['PromotionType']){//现金类型
	$CurPrice=$products_row['PromotionPrice'];
}

//秒杀
$SId=(int)$_GET['SId'];
$secWhere="ProId='$ProId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime";
$SId && $secWhere="SId='{$SId}' and ".$secWhere;
$sales_row=str::str_code(db::get_one('sales_seckill', $secWhere));
if($sales_row){
	$is_promotion=0;//秒杀优先于促销
	$IsSeckill=1;
	$CurPrice=$sales_row['Price'];
	$_OldPrice=$Price_0>0?$Price_0:$Price_1;
	$discount=($_OldPrice-$CurPrice)/((float)$_OldPrice?$_OldPrice:1)*100;
	$SId=$sales_row['SId'];
	$SMax=($sales_row['MaxQty'] && $sales_row['RemainderQty'] && $sales_row['RemainderQty']>=$sales_row['MaxQty']?$sales_row['MaxQty']:$sales_row['RemainderQty']); //最大购买上限
	$SMax<=$Max && $Max=$SMax;
}

//最后拍板
if(!$IsSeckill && $is_wholesale){
	$CurPrice>$maxPrice && $maxPrice=$CurPrice;
	$CurPrice<$minPrice && $minPrice=$CurPrice;
}
$discount=sprintf('%01.0f', $discount);
$discount=$discount<1?1:$discount;
$oldPrice=$is_promotion?$Price_1:$Price_0;
($SId && $IsSeckill) && $oldPrice=$Price_0>0?$Price_0:0;
$ItemPrice=$CurPrice;
$CurPrice=($is_promotion && $products_row['PromotionType']?$CurPrice*($products_row['PromotionDiscount']/100):$CurPrice);
$save_discount=@intval(sprintf('%01.2f', ($oldPrice-$CurPrice)/$oldPrice*100));
$save_discount=$save_discount<1?1:$save_discount;
$Max=$Max<1?0:$Max;

//默认国家参数
if($_SESSION['User']['UserId']){
	$country_default_row=str::str_code(db::get_one('orders o left join country c on o.ShippingCId=c.CId', "UserId='{$_SESSION['User']['UserId']}'", 'o.ShippingCId, c.Country, c.Acronym, c.CountryData', 'OrderId desc'));//会员订单信息
	if(!$country_default_row['ShippingCId'] || !$country_default_row['CountryData']){//获取会员地址信息
		$address_where=($_SESSION['Cart']['ShippingAddressAId']?"a.AId={$_SESSION['Cart']['ShippingAddressAId']}":"a.UserId='{$_SESSION['User']['UserId']}' and a.IsBillingAddress=0");//默认？第一个？
		$country_default_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId', $address_where, 'a.CId, c.Country, c.Acronym, c.CountryData', 'a.AccTime desc, a.AId desc'));
	}else{
		$country_default_row['CId']=$country_default_row['ShippingCId'];
	}
}else{//默认国家
	$country_default_row=str::str_code(db::get_one('country', 'IsUsed=1', 'CId, Country, Acronym, CountryData', 'IsDefault desc, Country asc'));
}
if($country_default_row['CountryData']){
	$country_default_data=str::json_data(htmlspecialchars_decode($country_default_row['CountryData']), 'decode');
	$country_default_row['Country']=$country_default_data[substr($c['lang'], 1)];
}

//SEO
$products_seo_row=str::str_code(db::get_one('products_seo', "ProId='$ProId'"));
$spare_ary=array(
	'SeoTitle'		=>	$Name.','.$category_row['Category'.$c['lang']],
	'SeoKeyword'	=>	$Name.','.$category_row['Category'.$c['lang']],
	'SeoDescription'=>	$Name.','.$category_row['Category'.$c['lang']].','.$TopCategory_row['Category'.$c['lang']]
);

//快捷支付
$is_paypal_checkout=(int)db::get_row_count('payment', "Method='Excheckout' and IsUsed=1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
echo ly200::seo_meta($products_seo_row, $spare_ary);
echo cart::products_pins($products_row, $CurPrice);
include("{$c['static_path']}/inc/static.php");
include("{$c['static_path']}/inc/header.php");
echo ly200::load_static('/static/themes/default/css/products/detail/custom.css');

if(in_array('facebook_pixel', $c['plugins']['Used'])){
	//Facebook Pixel
?>
	<!-- Facebook Pixel Code -->
	<script type="text/javascript">
	<!-- When a page viewed such as landing on a product detail page. -->
	fbq('track', 'ViewContent', {
		content_type: 'product',//产品类型为产品
		content_ids: ['<?=$products_row['SKU']?addslashes(htmlspecialchars_decode($products_row['SKU'])):addslashes(htmlspecialchars_decode($products_row['Prefix'])).addslashes(htmlspecialchars_decode($products_row['Number']));?>'],//产品ID
		content_name: '<?=addslashes($Name);?>',//产品名称
		value: <?=cart::iconv_price($CurPrice, 2, '', 0);?>,//产品价格
		currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
	});
	
	<!-- When some adds a product to a shopping cart. -->
	$.fn.fbq_addtocart=function(val){
		fbq('track', 'AddToCart', {
			content_type: 'product',//产品类型为产品
			content_ids: ['<?=$products_row['SKU']?addslashes(htmlspecialchars_decode($products_row['SKU'])):addslashes(htmlspecialchars_decode($products_row['Prefix'])).addslashes(htmlspecialchars_decode($products_row['Number']));?>'],//产品ID
			content_name: '<?=addslashes($Name);?>',//产品名称
			value: val,//数值
			currency: '<?=$_SESSION['Currency']['Currency'];?>'//货币类型
		});
	}
	</script>
	<!-- End Facebook Pixel Code -->
<?php }?>
<script type="text/javascript">
$(document).ready(function(){
	<?php
	if($c['NewFunVersion']>=4 && $is_paypal_checkout){//新版Paypal checkout
		echo 'cart_obj.paypal_checkout_init();';
	}?>
});
</script>
</head>

<body class="lang<?=$c['lang'];?>">
<div id="shopbox_outer" class="clearfix <?=$IsMarketing?'market_shopbox':'brand_shopbox';?>">
	<div class="detail_left"></div>
	<?php
		include("detail/custom_right_{$IsMarketing}.php");
	?>
</div>
<div class="blank12"></div>
<?=ly200::load_static("/static/js/plugin/products/detail/module.js");?>
<div class="hide">
<?=ly200::out_put_third_code();?>
</div>
</body>
</html>