<?php !isset($c) && exit();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
echo ly200::seo_meta($InfoId?$info_row:$info_category_ary[$UId][$CateId], $spare_ary);
include("{$c['static_path']}/inc/static.php");
?>
</head>

<body class="lang<?=$c['lang'];?> w_1200">
<?php
	$allcate_row=str::str_code(db::get_all('products_category', ' IsSoldOut=0', "CateId,UId,Category{$c['lang']},SubCateCount",  $c['my_order'].'CateId asc'));
	$allcate_ary=array();
	foreach((array)$allcate_row as $k=>$v){
		$allcate_ary[$v['UId']][]=$v;
	}
?>
<div id="nav">
	<div class="nav_box">
	    <ul class="nav_item">
	        <?php
	        $nav_row=db::get_value('config', "GroupId='themes' and Variable='NavData'", 'Value');
	        $nav_data=str::json_data($nav_row, 'decode');
	        //导航图片
	        foreach((array)$nav_data as $k=>$v){
	            $nav=ly200::nav_style($v, 1);
	            if(!$nav['Name']) continue;
	        ?>
	        <li>
	            <a href="javascript:;"<?=$nav['Target'];?>><?=$nav['Name'];?></a>
	            <?php
				if($nav['Select'] && count($allcate_ary[$nav['UId']])){
				    $nav_img_count = 0;
				    $img_lang = substr($c['lang'], 1);
				    for($i=0;$i<4;$i++){
				        if(!@is_file($c['root_path'].$v['PicPath_'.$i][$img_lang])) continue;
				        $nav_img_count++;
				    }
				    ?>
				    <div class="nav_sec nav_sec_<?=$nav['Name']; ?>">
				        <div class="top"></div>
				        <div class="nav_sec_box">
				            <div class="wide">
				                <div class="nav_list <?=$nav_img_count<2 ? 'big' : ''; ?> <?=$nav_img_count==0 ? 'bigger' : ''; ?>">
				                    <?php
				                    $sec_allcate_ary = array();
				                    foreach ($allcate_ary[$nav['UId']] as $key => $value) {
										for($i=0;$i<6;$i++){
											if($key%6==$i) $sec_allcate_ary[$i][] = $value; 
										}
									}
				                    foreach((array)$sec_allcate_ary as $key=>$value){
				                        ?>
				                        <div class="nav_sec_list">
				                        	<?php foreach((array)$value as $k1=>$v1){
				                        		$n=$v1['Category'.$c['lang']];
				                        		?>
						                        <div class="nav_sec_list">
						                            <a class="nav_sec_a" href="<?=ly200::get_url($v1, 'products_category');?>" title="<?=$n;?>"><?=$n;?></a>
						                            <?php if(count($allcate_ary[$nav['UId'].$v1['CateId'].','])){?>
						                            <div class="nav_thd">
						                                <?php
						                                foreach((array)$allcate_ary[$nav['UId'].$v1['CateId'].','] as $k2=>$v2){
						                                    $nn=$v2['Category'.$c['lang']];
						                                    if($k2<8){
						                                    ?>
						                                    <div class="nav_thd_list"><a class="FontHoverColor" href="<?=ly200::get_url($v2, 'products_category');?>" title="<?=$nn;?>"><?=$nn;?></a></div>
						                                    <?php }else{ ?>
						                                        <a href="<?=ly200::get_url($v1, 'products_category');?>"  title="<?=$n;?>" class="more"><?=$c['lang_pack']['more'].' >'; ?></a>
						                                    <?php } ?>
						                                <?php }?>
						                            </div>
						                            <?php }?>
						                        </div>
					                        <?php /*if(($nav_img_count==0 && $k1%6==5) || ($nav_img_count==1 && $k1%5==4) || ($nav_img_count>1 && $k1%4==3)){ ?><div class="clear"></div><?php }*/ ?>
					                        <?php } ?>
				                        </div>
				                    <?php }?>
				                    <div class="clear"></div>
				                </div>
				                <div class="nav_img <?=$nav_img_count==1 ? 'small' : ''; ?> <?=$nav_img_count==0 ? 'smaller' : ''; ?>">
				                    <?php 
				                        for($i=0;$i<4;$i++){
				                            if(!@is_file($c['root_path'].$v['PicPath_'.$i][$img_lang])) continue;
				                        ?>
				                        <div class="imgl">
				                            <?php
				                                $img_str='';
				                                $v['Url_'.$i][$img_lang] && $img_str.="<a href='{$v['Url_'.$i][$img_lang]}' title='{$v['ImgName_'.$i][$img_lang]}{$v['Brief_'.$i][$img_lang]}'>";
				                                $img_str.="<img src='{$v['PicPath_'.$i][$img_lang]}' alt='{$v['ImgName_'.$i][$img_lang]}' />";
				                                $v['Url_'.$i][$img_lang] && $img_str.="</a>";
				                                echo $img_str;
				                            ?>
				                        </div>
				                    <?php } ?>
				                    <div class="clear"></div>
				                </div>
				                <div class="clear"></div>
				            </div>
				        </div>
				    </div>
				<?php }?>
	        </li>
	        <?php }?>
	    </ul>
	    <div class="clear"></div>
	</div>
</div>
<style>
	#nav .nav_sec{top: 0;}
	#nav .nav_sec.nav_sec_Products{display: block;}
</style>
</body>
</html>