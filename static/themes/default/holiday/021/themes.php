<?php !isset($c) && exit();?>
<?php
$where='0';
$proid_obj[0] && $where.=','.implode(',', $proid_obj[0]);
$pro_ary=array();
$pro_row=str::str_code(db::get_all('products', "ProId in($where)", '*', 'ProId desc'));
foreach((array)$pro_row as $v) $pro_ary[$v['ProId']]=$v;
ob_start();
?>
</style>
<a href="/" class="go_home"></a>
<div id="bodyer" class="full">
	<div class="template_box t1 box box0">
		<a href="<?=$theme_ary[0]['Url'] ? $theme_ary[0]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[0]['Title']; ?>">
			<img src="<?=$theme_ary[0]['PicPath']; ?>" alt="<?=$theme_ary[0]['Title']; ?>">
		</a>
	</div>
	<div class="template_list box box1">
		<div class="mid">
			<div class="template_box title">
				<img src="<?=$theme_ary[1]['PicPath']; ?>" alt="<?=$theme_ary[1]['Title']; ?>">
			</div>
			<div class="template_box t3">
				<a href="<?=$theme_ary[2]['Url'] ? $theme_ary[2]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[2]['Title']; ?>">
					<img src="<?=$theme_ary[2]['PicPath']; ?>" alt="<?=$theme_ary[2]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t3">
				<a href="<?=$theme_ary[3]['Url'] ? $theme_ary[3]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[3]['Title']; ?>">
					<img src="<?=$theme_ary[3]['PicPath']; ?>" alt="<?=$theme_ary[3]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t3">
				<a href="<?=$theme_ary[4]['Url'] ? $theme_ary[4]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[4]['Title']; ?>">
					<img src="<?=$theme_ary[4]['PicPath']; ?>" alt="<?=$theme_ary[4]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t4">
				<a href="<?=$theme_ary[5]['Url'] ? $theme_ary[5]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[5]['Title']; ?>">
					<img src="<?=$theme_ary[5]['PicPath']; ?>" alt="<?=$theme_ary[5]['Title']; ?>">
				</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="template_list box box2">
		<div class="mid">
			<div class="template_box title">
				<a href="<?=$theme_ary[6]['Url'] ? $theme_ary[6]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[6]['Title']; ?>">
					<img src="<?=$theme_ary[6]['PicPath']; ?>" alt="<?=$theme_ary[6]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t6">
				<a href="<?=$theme_ary[7]['Url'] ? $theme_ary[7]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[7]['Title']; ?>">
					<img src="<?=$theme_ary[7]['PicPath']; ?>" alt="<?=$theme_ary[7]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t6">
				<a href="<?=$theme_ary[8]['Url'] ? $theme_ary[8]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[8]['Title']; ?>">
					<img src="<?=$theme_ary[8]['PicPath']; ?>" alt="<?=$theme_ary[8]['Title']; ?>">
				</a>
			</div>
			<div class="clear"></div>
			<div class="template_box t6">
				<a href="<?=$theme_ary[9]['Url'] ? $theme_ary[9]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[9]['Title']; ?>">
					<img src="<?=$theme_ary[9]['PicPath']; ?>" alt="<?=$theme_ary[9]['Title']; ?>">
				</a>
			</div>
			<div class="template_box t6">
				<a href="<?=$theme_ary[10]['Url'] ? $theme_ary[10]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[10]['Title']; ?>">
					<img src="<?=$theme_ary[10]['PicPath']; ?>" alt="<?=$theme_ary[10]['Title']; ?>">
				</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="template_list box box3">
		<div class="mid">
			<div class="template_box title">
				<a href="<?=$theme_ary[11]['Url'] ? $theme_ary[11]['Url'] : 'javascript:;'; ?>" title="<?=$theme_ary[11]['Title']; ?>">
						<img src="<?=$theme_ary[11]['PicPath']; ?>" alt="<?=$theme_ary[11]['Title']; ?>">
					</a>
			</div>
			<div class="template_box t8">
				<?php
				$no_where='0';
				foreach((array)$proid_obj[0] as $k=>$v){
					$row=$pro_ary[$v];
					$url=ly200::get_url($row, 'products');
					$img=ly200::get_size_img($row['PicPath_0'], '500x500');
					$name=$row['Name'.$c['lang']];
					$price_ary=cart::range_price_ext($row);
				?>
				<dl class="pro_box">
					<dd class="img"><a class="pic_box" href="<?=$url;?>" target="_blank" title="<?=$name;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a></dd>
					<dd class="view">
						<a href="<?=$url;?>" target="_blank" class="name" title="<?=$name;?>"><?=$name;?></a>
						<div class="price cur_price"><b><em class="currency_data"></em><span class="price_data" data="<?=$price_ary[0];?>"></span></b></div>
						<a class="addtocart" href="<?=$url;?>" target="_blank" title="<?=$name; ?>"><img src="/static/themes/default/holiday/021/images/icon_addtocart.png" alt=""></a>
					</dd>
				</dl>
				<?php }?>
				<div class="clear"></div>
			</div>
		</div>
	</div>

</div>
<?=ly200::load_static("/static/themes/default/holiday/{$theme}/js/template.js");?>
<?php
$theme_content=ob_get_contents();
ob_end_clean();
?>