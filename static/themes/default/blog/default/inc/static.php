<?php !isset($c) && exit();?>
<?php
echo ly200::load_static(
	'/static/css/global.css',
	'/static/themes/default/css/global.css',
	'/static/js/jquery-1.7.2.min.js',
	'/static/js/lang/'.substr($c['lang'], 1).'.js',
	'/static/js/global.js',
	'/static/themes/default/blog/default/css/style.css',
	'/static/themes/default/blog/default/js/blog.js'
);
if($c['FunVersion']>=10){
	echo ly200::load_static('/static/themes/default/css/cdx_global.css');
}
?>
<script type="text/javascript">
	<?php
	$FbData=$c['config']['Platform']['Facebook']['SignIn']['Data'];
	$payment_row=db::get_one('payment', "PId=2 and IsUsed=1", 'IsOnline, Method, Attribute'); //Paypal快捷支付
	$payment_row && $account=str::json_data($payment_row['Attribute'], 'decode');
	?>
	var ueeshop_config={
		"domain":"<?=ly200::get_domain();?>",
		"date":"<?=date('Y/m/d H:i:s', $c['time']);?>",
		"lang":"<?=substr($c['lang'], 1);?>",
		"currency":"<?=$_SESSION['Currency']['Currency'];?>",
		"currency_symbols":"<?=$_SESSION['Currency']['Symbol'];?>",
		"currency_rate":"<?=$_SESSION['Currency']['Rate'];?>",
		"FbAppId":"<?=$FbData?$FbData['appId']:'';?>",
		"FbPixelOpen":"<?=(int)$c['config']['Platform']['Facebook']['Pixel']['IsUsed'];?>",
		"UserId":"<?=(int)$_SESSION['User']['UserId'];?>",
		"TouristsShopping":"<?=(int)$c['config']['global']['TouristsShopping'];?>",
		"PaypalExcheckout":"<?=$acount['Username'];?>",
		"IsMobile":<?=ly200::is_mobile_client(); ?>
	}
</script>
