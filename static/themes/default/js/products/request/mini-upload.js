﻿/*author 小爵*/
/*wechat jimsfriend*/

; (function ($, window, document, undefined)  {
           
        var Upload = function (el, setting) {
                var _default = {
                    data: [],
                    maxcount: 5,    //5 pic
                    maxsize: 5,   //default 5M
                    allow: ".jpg|.jpef|.png|.image|.gif|.bmp",
                    extra: null
                };
                this._options = $.extend(_default, setting);

                this._container = null;
                this._inputfile = null;
                this._ul = null;
                this._lastli = null;
                this._currentli = null;
                this._file = null;
                this._self = null;
                this._el = null;
          
                this.getValue = function () {

                   
                    var r = [];
                    var arr = this._ul.find(".mini-upload-img");
                    var c = arr.each(function (a) {

                        r.push($(arr[a]).attr("src"));
                    });
                    return r;

                };

                this.getCurrenCount = function () {


                    return $(this._ul).find(".mini-upload-img").not(".mini-upload-last").length;
                };
;
                this.setValue = function (arr) {
                   
                    if (arr == null || arr == undefined) {

                        console.log("参数不可为空");
                        return;
                    }

                   
                    var that = this;
                    that._ul.empty(); 
                    console.log(arr);
                    that._lastli = $('<li class="mini-upload-li mini-upload-li-last"><p class="mini-upload-last-btn">点击添加</p></li>').bind("click", function () {
                        that._currentli = that._lastli;
                        that._inputfile.click()

                    })


                    console.log(arr);
                    that._lastli.appendTo(that._ul);
                   
                    for (var i = 0; i < arr.length; i++) {
                        
                        that.insertli(arr[i]);
                        
                    }
                  

                    var clear = $('<li class="mini-upload-li-clear"></li>');

                    clear.appendTo(that._ul);
                    that._ul.appendTo(that._container);

                    that._container.appendTo(that._self);
                    return that;

                };;
                this.init = function () {


                    var that = this;
                    
                    if ($(el).length>0) {
                        that._self = $(el)
                    }
                    else {

                        console.log("容器元素不可为空");
                        return;
                    }


                    that._container = $('<div class="mini-upload-container"> </div>');

                    that._inputfile = $('<input type="file" style="display:none" /> ');

                    that._inputfile.appendTo(that._container);

                    that._ul = $('<ul class="mini-upload-ul"></ul>');
                    that.setValue(that._options.data)
                    that._inputfile.bind("change", function () {
                        var f = $(this).get(0).files[0];
                        that._file = f;
                        if (that._file != null && that._file != undefined) {

                            if (!that.validate()) {

                                return;
                            }

                            var formdata = new FormData();

                            formdata.append("file", f)

                            if (that._options.extra != null) {
                                for (var i in that._options.extra) {
                                    formdata.append(i, that._options.extra[i])

                                }
                            }
                            $.ajax({
                                url: that._options.url,
                                data: formdata,
                                type: 'post',
                                dataType: "json",
                                cache: false,
                                processData: false,
                                contentType: false,
                                success: function (r) {

                                   that.callback(r);
                                }


                            })
                        }

                    });


                };
                
            this.insertli = function (src) {
                console.log("insertli")
                    var that = this;

                    var del = $('<a href="javascript:void(0)" class="mini-upload-del">删除</a>').bind("click", function () {


                        $(this).parent().remove();

                        if (that.getCurrenCount() < that._options.maxcount) {

                            that._lastli.css("display", "block")
                        }
                    });
                    var img = $('<img class="mini-upload-img" src="' + src + '" />');
                    var li = $('<li class="mini-upload-li"></li>').append(del).append(img).bind("click", function () {

                        that._currentli = li;
                        that._inputfile.click()

                    });
                 
                    li.insertBefore(that._lastli);
                    return that;
                };;
                this.validate = function () {

                
                    if (this._file == null || this._file  == undefined) {

                        return false;
                    }

                    if (this._file.size > this._options.maxsize * 1024 * 1024) {
                        alert("文件不能超过" + this._options.maxsize + "M");
                        return false;
                    }
                    var fix = this._file.name.substring(this._file.name.lastIndexOf('.')).toLowerCase();

                    if (this._options.allow.split("|").indexOf(fix) < 0) {
                        alert("文件只允许" + this._options.allow);
                        return false;
                    }

                    return true;

                };
                this.callback = function (response) {
                    var that = this;
                    var d = response;

                    if (d.code == 0) {

                        that._inputfile.val("");
                        if (that._currentli.hasClass("mini-upload-li-last")) {

                             that.insertli(d.data);

                          

                            if (that.getCurrenCount() >= that._options.maxcount) {

                                that._lastli.css("display", "none")
                            }


                        }

                        else {
                            that._currentli.find("img").attr("src", d.data);

                        }
                    }

                    else {

                        alert("上传失败");
                    }
                };
                this.init();

            };
    window.Upload = Upload;
})(jQuery, window, document, undefined) 



 