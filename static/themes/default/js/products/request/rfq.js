function loadImgPreVView(obj){
    var file	= obj.files[0],		//获取文件
        reader	= new FileReader(),	//创建读取文件的对象
        imgFile = '';
    $obj = $(obj);
    $box = $(obj.parentElement);
    $picbox = $obj.next();
    $rootWrap = $('#productImage');
    var count = $rootWrap.find('.upload_box').length;

    var divHtml = '<div class="input upload_box">\n' +
        '                        <input class="upload_file" type="file" onchange="loadImgPreVView(this);" accept="image/gif,image/jpeg,image/png">\n' +
        '                        <div id="pic_show" class="pic_box">\n' +
        '                        </div>\n' +
        '<div class="hint_text">'+lang_obj.global.upload_attachments+'</div>'+
        '                        <div class="btn_delete hidden" onclick="removeBox(this);"></div>\n' +
        '                    </div>';

    if(file){
        lrz(file, {
            width: 512
        }).then(function (rst) {
            $picbox.append('<img src="'+rst.base64+'" alt="" /><span></span>');
            $obj.attr('data', rst.base64);
        });
        // //已选择
        // reader.onload=function(e){ //为文件读取成功设置事件
        //     imgFile=e.target.result;
        //     $picbox.append('<img src="'+imgFile+'" alt="" /><span></span>');
        // };
        // reader.readAsDataURL(file); //正式读取文件

        //禁止再次选择
        $obj.attr('onclick', 'return false;');
        $picbox.next().addClass('hidden');
        $($picbox.next()).next().removeClass('hidden');

        //判断是否继续上传
        if (count < 10){
            if (count === 5) {
                $rootWrap.append('<div class="clear"></div>');
            }
            //添加一个容器
            $rootWrap.append(divHtml);
        }else{
            alert("最多上传10个图片");
        }

    }else{
        if (count === 1){
            //取消选择
            $picbox.append('<img src="" alt="" /><span></span>');
        }

    }

    resetInputFileName();
}

function removeBox(obj){
    $rootWrap = $('#productImage');
    var count = $rootWrap.find('.upload_box').length;
    $obj = $(obj);
    $box = $(obj.parentElement);

    var divHtml = '<div class="input upload_box">\n' +
        '                        <input class="upload_file" type="file" onchange="loadImgPreVView(this);" accept="image/gif,image/jpeg,image/png">\n' +
        '                        <div id="pic_show" class="pic_box">\n' +
        '                        </div>\n' +
        '<div class="hint_text">'+lang_obj.global.upload_attachments+'</div>'+
        '                        <div class="btn_delete hidden" onclick="removeBox(this);"></div>\n' +
        '                    </div>';

    if (count > 1){
        $box.remove();
        //如果删完以后就剩一个了，隐藏删除按钮，恢复input点击事件
        count = $rootWrap.find('.upload_box').length;
        var del_hidden_num = $rootWrap.find('.hidden').length;
        var del_count = $rootWrap.find('.btn_delete').length;
        if (count === 1){
            $box = $($rootWrap.find('.upload_box')[0]);
            //隐藏删除按钮
            $($box.find('.btn_delete')[0]).addClass('hidden');
            $($box.find('.hint_text')[0]).removeClass('hidden');
            //恢复点击事件
            $($box.find('.upload_file')[0]).removeAttr('disabled')
        }else if (count === 9 && del_hidden_num === 0){
            $rootWrap.append(divHtml);
        }
        //重新排版
        if (count > 4){
            $rootWrap.find('.clear').remove();
            $rootWrap.find('.upload_box').eq(4).after('<div class="clear"></div>');
        }
    }else{
        $($box.find('.upload_file')[0]).val('');
        $($box.find('.pic_box')[0]).append('<img src="" alt="" /><span></span>');
    }



    resetInputFileName();
}

function resetInputFileName() {
    $rootWrap = $('#productImage');
    var count = $rootWrap.find('.upload_box').length;
    var fileCount = 0;

    for (let i = 0; i < count; i++) {
        $itemInput = $rootWrap.find('.upload_file').eq(i);
        if ($itemInput.attr('onclick') != null){
            fileCount++;
            $itemInput.attr('name','picPath_'+i);
        }
    }

    $('#fileCount').val(fileCount);

}