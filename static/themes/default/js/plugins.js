/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var plugins_obj={
	/******************* 买家秀 Start *******************/
	gallery_init:function(){
		$('#app_gallery').on('click','.gly_list .img',function(){
			var	$gallery_img = $(this).attr('data-gallery_img'),
				$pid = $(this).attr('data-pid'),
				$img = $(this).attr('data-img'),
				$url = $(this).attr('data-url'),
				$name = $(this).attr('data-name'),
				$has_pro = '';
			if($pid>0) $has_pro = 'has_pro';
			var pay_html='<div id="alert_choose" class="alert_choose alert_choose_gallery '+$has_pro+'">';
				pay_html+='<div class="box_bg"></div><a class="noCtrTrack" id="choose_close">×</a>';
				pay_html+='<div class="choose_content">';
					pay_html+='<div class="gallery_img pic_box"><img src="'+$gallery_img+'" alt="" /><span></span></div>';
					if($pid>0){	
						pay_html+='<div class="product">';
							pay_html+='<div class="name">'+$name+'</div>';
							pay_html+='<div class="img pic_box"><img src="'+$img+'" alt="'+$name+'" /><span></span></div>';
							pay_html+='<a href="'+$url+'" title="'+$name+'" class="view BuyNowBgColor">'+lang_obj.global.view_it+'</a>';
						pay_html+='</div>';
					}
					pay_html+='<div class="clear"></div>';
				pay_html+='</div>';
			pay_html+='</div>';
			
			$('#alert_choose').length && $('#alert_choose').remove();
			$('body').prepend(pay_html);
			$('#alert_choose').css({left:$(window).width()/2-$('#alert_choose').width()/2,top:'20%'});
			global_obj.div_mask();
			$('body').on('click', '#choose_close, #div_mask', function(){
				if($('#alert_choose').length){
					$('#alert_choose').remove();
					global_obj.div_mask(1);
				}
			});
		});
	},

	gallery_list_init:function(){
		BoardLayout.setup();
		
		$(window).resize(function(){
			BoardLayout.columnWidthInner=$('.gly_list .item').width();
			BoardLayout.allPins();
			//BoardLayout.newPins();
		});
		$(window).resize();
	},
	/******************* 买家秀 End *******************/
	
	/******************* 分销 Start *******************/
	distribution_init:function(){
		$('#lib_user_distribution .menu_list .item.link').click(function(){
			window.top.location.href=$(this).attr('data-link');
		});
		
		$('.btn_copy').click(function(){ //复制地址
			$('.share_input')[0].select(); //选择对象
			document.execCommand("Copy"); //执行浏览器复制命令
			alert(lang_obj.global.copy_complete);
		});
		
		$('.share_third>a').click(function(){ //分享地址
			$(this).shareThis($(this).attr('data'), $('.share_third').attr('data-title'), $('.share_third').attr('data-url'));
		});
		
		var $frm_balance=$('.balance_form');
		if($frm_balance.length){
			$frm_balance.submit(function(){ return false; });
			$frm_balance.on('click', 'button:submit', function(){ //会员登录
				if(global_obj.check_form($frm_balance.find('*[notnull]'))){return false;};
				$.post('/', $frm_balance.serialize(), function(data){
					if(data.ret==1){
						window.top.location.href='/account/p-distribution/record/';
					}else{
						global_obj.new_win_alert(data.msg);
					}
				}, 'json');
				return false;
			});
		}
	}
	/******************* 分销 End *******************/
};



(function(a){
	a.pageless = function(c) {
		a.isFunction(c) ? c.call() : a.pageless.init(c)
	};
	a.pageless.settings = {
		currentPage: 1,
		pagination: ".pagination",
		url: location.href,
		params: {},
		distance: 100,
		loaderImage: "",
		marker: null,
		scrape: function(c) {
			return c
		}
	};
	a.pageless.loaderHtml = function() {
		return a.pageless.settings.loaderHtml || '<div id="pageless_loader" style="display:none; text-align:center; width:100%;"></div>'
	};
	a.pageless.init = function(c) {
		if (!a.pageless.settings.inited) {
			a.pageless.settings.inited = true;
			c && a.extend(a.pageless.settings, c);
			a.pageless.settings.pagination && a(a.pageless.settings.pagination).remove();
			a.pageless.startListener()
		}
	};
	a.pageless.isLoading = false;
	a.fn.pageless = function(c) {
		a.pageless.init(c);
		a.pageless.el = a(this);
		if (c.loader && a(this).find(c.loader).length) a.pageless.loader = a(this).find(c.loader);
		else {
			a.pageless.loader = a(a.pageless.loaderHtml());
			a(this).append(a.pageless.loader);
			c.loaderHtml || a("#pageless_loader .msg").html(c.loaderMsg)
		}
	};
	a.pageless.loading = function(c) {
		if (c === true) {
			a.pageless.isLoading = true;
			a.pageless.loader && a.pageless.loader.fadeIn("normal")
		} else {
			a.pageless.isLoading = false;
			a.pageless.loader && a.pageless.loader.fadeOut("normal")
		}
	};
	a.pageless.stopListener = function() {
		a(window).unbind(".pageless");
		a("#" + a.pageless.settings.loader).hide()
	};
	a.pageless.startListener = function() {
		a(window).bind("scroll.pageless", a.pageless.scroll);
		a("#" + a.pageless.settings.loader).show()
	};
	a.pageless.scroll = function() {
		if (a.pageless.settings.totalPages <= a.pageless.settings.currentPage) {
			a.pageless.stopListener();
			a.pageless.settings.afterStopListener && a.pageless.settings.afterStopListener.call()
		} else {
			var c = a(document).height() - a(window).scrollTop() - a(window).height();
			if (!a.pageless.isLoading && c < a.pageless.settings.distance) {
				a.pageless.loading(true);
				a.pageless.settings.currentPage++;
				a.extend(a.pageless.settings.params, {
					page: a.pageless.settings.currentPage
				});
				a.pageless.settings.marker && a.extend(a.pageless.settings.params, {
					marker: a.pageless.settings.marker
				});
				c = a.pageless.settings.url;
				c = c.split("#")[0];
				var c_ext = '';
				if(!$('body').hasClass('w_1200')){
					c_ext = '&width=small';
				}
				a.ajax({
					url: c+c_ext,
					type: "GET",
					dataType: "html",
					data: a.pageless.settings.params,
					success: function(d) {
						d = a.pageless.settings.scrape(d);
						d = '<div class="BoardLayout" style="visibility:visible;">'+$(d).find('#app_gallery_content').html()+'</div>';
						a.pageless.loader ? a.pageless.loader.before(d) : a.pageless.el.append(d);
						a.pageless.loading(false);
						a.pageless.settings.complete && a.pageless.settings.complete.call();
					}
				})
			}
		}
	}
})(jQuery);

var BoardLayout=function() {
	return {
		setup: function(a) {
			if (!this.setupComplete) {
				$(document).ready(function() {
					BoardLayout.allPins();					
				});
				this.center = !!a;
				this.setupComplete = true
			}
		},
		pinsContainer: ".BoardLayout",
		pinArray: [],
		orderedPins: [],
		mappedPins: {},
		columnCount: 5,
		columns: 0,
		columnWidthInner: $('.gly_list .item').outerWidth(),
		columnMargin: 12,
		columnPadding: 0,
		columnContainerWidth: 0,
		allPins: function() {			
			var a = $(this.pinsContainer + " .pin"),
				c = $('#gly_list').width();//document.documentElement.clientWidth;
			
			this.columnWidthOuter = this.columnWidthInner + this.columnMargin + this.columnPadding;
			this.columns = Math.max(this.columnCount, parseInt(c / this.columnWidthOuter));
			if (a.length < this.columns) this.columns = Math.max(this.columnCount, a.length);
			c = this.columnWidthOuter * this.columns - this.columnMargin;
			var d = document.getElementById("gly_list");
			if (d) {
				d.style.width = c + "px";
			}
			$(".LiquidContainer").css("width", c + "px");
			for (c = 0; c < this.columns; c++) this.pinArray[c] = 0;
			document.getElementById("SortableButtons") ? this.showPins() : this.flowPins(a, true);
			if ($("#app_gallery_content .pin").length === 0 && window.location.pathname === "/") {
				$("#app_gallery_content").addClass("empty");
				setTimeout(function() {
					window.location.reload()
				},
				5E3)
			}
		},
		newPins: function(){
			this.flowPins($(this.pinsContainer + ":last .pin"));
		},
		flowPins: function(a, c){			
			if(c){
				this.mappedPins={};
				this.orderedPins=[];
			}
			if(this.pinArray.length > this.columns) this.pinArray = this.pinArray.slice(0, this.columns);
			for(i=0; i<a.length; i++){
				c = a[i];
				var d=$(c).attr("data-id");
				if(d && this.mappedPins[d]){
					$(c).remove();
				}else{
					var e=jQuery.inArray(Math.min.apply(Math, this.pinArray), this.pinArray),
						f=this.pinArray[e];
					
					c.style.top=f+"px";
					c.style.left=e*this.columnWidthOuter+"px";
					this.pinArray[e]=f+c.offsetHeight+this.columnMargin;
					this.mappedPins[d]=this.orderedPins.length;
					this.orderedPins.push(d);
				}
			}
			var columnContainer = document.getElementById("app_gallery_content");
			if(columnContainer) columnContainer.style.height = Math.max.apply(Math, this.pinArray) + "px";
			this.showPins();
		},
		showPins: function() {
			$.browser.msie && parseInt($.browser.version);
			var a = $(this.pinsContainer);
			setTimeout(function() {
				a.css({
					visibility: "visible"
				});
				$('.gly_list .item .img').attr('style','').removeAttr('style');
				$(window).resize();
			},
			100)
		}
	}
}();
