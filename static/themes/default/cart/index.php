<?php !isset($c) && exit();?>
<?php
development::load_file('static/themes/default/cart/development/index.php', 1);

$login_module_ary=array('checkout', 'buynow', 'ajax_get_coupon_info', 'remove_coupon', 'placeorder', 'complete', 'payment', 'success');	//需要登录的模块列表
$un_login_module_ary=array('list', 'additem', 'add_success', 'modify', 'remove', 'set_no_login_address', 'get_default_country', 'get_shipping_methods', 'get_excheckout_country', 'excheckout', 'offline_payment');	//不需要登录的模块列表

if((int)$_SESSION['User']['UserId']){	//已登录
	$module_ary=array_merge($un_login_module_ary, $login_module_ary);
}else{	//未登录
	if($c['orders']['mode']==0){	//必须登录方可下订单
		@in_array($a, $login_module_ary) && js::location("/account/login.html?jump_url=".urlencode($_SERVER['REQUEST_URI']));	//访问需要登录的模块但用户并未登录  .'?'.ly200::query_string()
		$module_ary=$un_login_module_ary;
	}else{	//未登录也可以下订单
		$module_ary=@array_merge($un_login_module_ary, $login_module_ary);
	}
}
($a=='' || !in_array($a, $module_ary)) && $a=$module_ary[0];

//快捷支付模板
if($a=='excheckout'){
	$d_ary=array('SetExpressCheckout', 'ReviewOrder', 'APIError', 'checkout', 'DoExpressCheckoutPayment', 'GetExpressCheckoutDetails', 'cancel');
	$d=$_GET['d']?$_GET['d']:$_POST['d'];
	!in_array($d, $d_ary) && $d=$d_ary[0];
	
	include("{$c['root_path']}static/themes/default/gateway/paypal_excheckout/{$d}.php");
	exit();
}else{
	if($a=='list' || $a=='checkout' || $a=='buynow'){//自动更新产品信息
		cart::open_update_cart();
	}
	
	$cutArr=str::json_data(db::get_value('config', 'GroupId="cart" and Variable="discount"', 'Value'), 'decode');//全场满减的数据
	$StyleData=(int)db::get_row_count('config_module', 'IsDefault=1')?db::get_value('config_module', 'IsDefault=1', 'StyleData'):db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData');//模板风格色调的数据
	$style_data=str::json_data($StyleData, 'decode');
	
	$file_name=$a;
	//add by jay start
    //BuyNow页面归纳到Checkout页面
	$a=='buynow' && $file_name='checkout';
	//add by jay end
	ob_start();
	include("module/{$file_name}.php");
	$cart_page_contents=ob_get_contents();
	ob_end_clean();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex,nofollow" />
<?php
echo ly200::seo_meta();
include("{$c['static_path']}/inc/static.php");
echo ly200::load_static('/static/themes/default/css/cart.css', "/static/themes/{$c['theme']}/css/cart.css", '/static/themes/default/js/cart.js', '/static/js/plugin/tool_tips/tool_tips_web.js');

$OId = trim($_GET['OId']);
$order_row = db::get_one('orders', "OId='$OId'");

//第三方统计
$facebook_pixel = in_array('facebook_pixel', $c['plugins']['Used']) ? 1 : 0;
$google_pixel = in_array('google_pixel', $c['plugins']['Used']) ? 1 : 0;
if ($facebook_pixel || $google_pixel) {
	if ($a == 'payment' || $a == 'complete') {
		if ($facebook_pixel && (int)$order_row['CutPay'] == 0) {
            //Facebook Pixel
            //When a person enters the checkout flow prior to completing the checkout flow.
            $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            fbq('track', 'InitiateCheckout', {
                content_type: 'product',
                content_ids: ['0'],
                value: '<?=$total_price;?>',
                currency: '<?=$order_row['Currency'];?>'
            });
            </script>
            <!-- End Facebook Pixel Code -->
<?php
		}
	}
    if ($a == 'checkout' || $a == 'buynow') {
		if ($facebook_pixel) {
		    //When a person enters the checkout flow prior to completing the checkout flow.
		    $iconv_total_price = cart::cart_total_price(($_GET['CId']?' and CId in('.str_replace('.', ',', $_GET['CId']).')':''), 1);
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            $.fn.fbq_checkout=function(){
                fbq('track', 'InitiateCheckout', {
                    content_ids: ['0'],
                    value:'<?=$iconv_total_price;?>',
                    currency:'<?=$_SESSION['Currency']['Currency'];?>'
                });
            }
            </script>
            <!-- End Facebook Pixel Code -->
<?php
        }
	} elseif ($a == 'complete') {
		if ($facebook_pixel) {
            //Facebook Pixel
		    //When payment information is added in the checkout flow.
            $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
            $Number_ary = array();
            $order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'p.Prefix,p.Number', 'o.LId asc');
            foreach ($order_list_row as $k => $v) {
                $v['Number'] != '' && $Number_ary[$k] = $v['Prefix'] . $v['Number'];
            }
?>
            <!-- Facebook Pixel Code -->
            <script type="text/javascript">
            fbq('track', 'AddPaymentInfo', {
                content_name: 'Shipping Cart',//关键词
                content_type: 'product',//产品类型为产品
                content_ids: ['<?=@implode("','", $Number_ary);?>'],//产品ID
                value:'<?=$total_price;?>',
                currency:'<?=$order_row['Currency'];?>'
            });
            </script>
            <!-- End Facebook Pixel Code -->
<?php
		}
	} elseif ($a == 'success') {
		//When a purchase is made or checkout flow is completed.
		if ((int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
            //仅有付款成功才执行
			$total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
			$Number_ary = array();
			$order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'p.Prefix,p.Number,o.*', 'o.LId asc');
			foreach ($order_list_row as $k => $v) {
				$v['Number'] != '' && $Number_ary[$k] = addslashes(htmlspecialchars_decode($v['Prefix'] . $v['Number']));
			}
			if ($facebook_pixel) {
?>
                <!-- Facebook Pixel Code -->
                <script type="text/javascript">
                fbq('track', 'Purchase', {
                    content_type: 'product',//产品类型为产品
                    content_ids: ['<?=@implode("','", $Number_ary);?>'],//产品ID
                    value: <?=$total_price;?>,//订单总金额
                    currency: '<?=$order_row['Currency'];?>'//货币类型
                });
                </script>
                <!-- End Facebook Pixel Code -->
<?php
			}
			if ($google_pixel) {
?>
                <script type="text/javascript">
                gtag('event', 'purchase', {
                    "transaction_id": "<?=$order_row['OId'];?>",
                    "affiliation": "<?=addslashes($c['config']['global']['SiteName']);?>",
                    "value": <?=$total_price;?>,
                    "currency": "<?=$order_row['Currency'];?>",
                    "tax": 0,
                    "shipping": <?=cart::iconv_price($order_row['ShippingPrice'], 2, $_SESSION['Currency']['Currency'], 0);?>,
                    "items": [
                        <?php
                        foreach ((array)$order_list_row as $k => $v) {
                            echo $k?',':'';
                        ?>
                            {
                                "id": "<?=$Number_ary[$k];?>",
                                "name": "<?=addslashes($v['Name']);?>",
                                "list_position": <?=$k+1;?>,
                                "quantity": <?=$v['Qty'];?>,
                                "price": '<?=cart::iconv_price($v['Price']+$v['PropertyPrice'], 2, $_SESSION['Currency']['Currency'], 0);?>'
                            }
                        <?php }?>
                    ]
                });
                </script>
<?php
			}
		}
	}
}

//统计结尾
if (($a == 'payment' || $a == 'complete') && (int)$order_row['CutPay'] == 0) {
    db::update('orders', "OId='{$order_row['OId']}'", array('CutPay'=>1));
}
if ($a == 'success' && (int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
    db::update('orders', "OId='{$order_row['OId']}'", array('CutSuccess'=>1));
}
?>
</head>

<body class="lang<?=$c['lang'];?>">
<?php
if ($a == 'checkout' || $a == 'buynow' || ($a=='complete' && $order_row['OrderStatus']==1 && $order_row['PId']==2)) {
    //独立页面
	include("{$c['static_path']}inc/header.php"); //加载公共头部文件
	echo '<div id="cart_checkout_container">' . $cart_page_contents . '</div>';
	echo ly200::out_put_third_code();
} elseif ($a == 'payment') {
    //付款页面 (不包含头部和底部)
	echo $cart_page_contents;
} else {
    //公共页面
	include("{$c['theme_path']}/inc/header.php");
	echo '<div id="cart_container">' . $cart_page_contents . '</div>';
	include("{$c['theme_path']}/inc/footer.php");
}

//Analytics统计
if ($a == 'checkout' || $a == 'buynow') {
    //Checkout页面
    $analytics = 4;
} elseif (($a == 'payment' || $a == 'complete') && (int)$order_row['CutPay'] == 0) {
    //Place an Order 生成订单
    $analytics = 5;
} elseif ($a == 'success' && (int)$order_row['OrderStatus'] == 4 && (int)$order_row['CutSuccess'] == 0) {
    //付款成功页面
    $analytics = 6;
}
if ($analytics > 0) {
    echo '<script type="text/javascript">$(function(){ if($.isFunction(analytics_click_statistics)){ analytics_click_statistics('.$analytics.') } });</script>';
}
?>
</body>
</html>