<?php !isset($c) && exit(); ?>
<?php
$OId = trim($_GET['OId']);
!$OId && js::location('/cart/');

//add by jay start
$d_ary = array('view', 'balance');
$d = $_GET['d'];
!in_array($d, $d_ary) && $d = $d_ary[0];
//add by jay end

$order_row = db::get_one('orders', "OId='$OId'");
if (!$_GET['utm_nooverride']) {//除了刚下单，都要例行检查
    //订单不存在
    (!$order_row) && js::location('/');
    //订单无产品
    if (!(int)db::get_row_count('orders_products_list', "OrderId='{$order_row['OrderId']}'")) {
        db::delete('orders', "OrderId='{$order_row['OrderId']}'");
        js::location('/');
    }
    //会员订单，未登录不允许查看
    ((int)$order_row['UserId'] && !(int)$_SESSION['User']['UserId']) && js::location('/account/login.html?JumpUrl=' . urlencode("/cart/complete/{$OId}.html"));
    //当前会员非订单会员
    ((int)$order_row['UserId'] && (int)$order_row['UserId'] != (int)$_SESSION['User']['UserId']) && js::location('/account/');
    //会员订单发货后状态在会员中心查询
    ((int)$_SESSION['User']['UserId'] && (int)$order_row['OrderStatus'] > 4) && js::location("/account/orders/view{$OId}.html");
    //订单总金额低于等于0
    $total_price = sprintf('%01.2f', orders::orders_price($order_row, 1));
    if ($total_price <= 0 && (int)$order_row['OrderStatus'] < 4) {
        if ((int)$order_row['OrderStatus'] == 1) {//更改为“待确认”状态
            $UserName = (int)$_SESSION['User']['UserId'] ? $_SESSION['User']['FirstName'] . ' ' . $_SESSION['User']['LastName'] : 'Tourist';
            $Log = 'Update order status from ' . $c['orders']['status'][1] . ' to ' . $c['orders']['status'][2];
            db::update('orders', "OId='$OId'", array('OrderStatus' => 2));
            orders::orders_log($order_row['UserId'], $UserName, $order_row['OrderId'], 2, $Log);
        }
        js::location("/cart/success/{$OId}.html");
    }

    $payment_row = db::get_one('payment', "PId='{$order_row['PId']}' and IsUsed=1");
    !$payment_row && js::location("/cart/success/{$OId}.html");//支付方式已关闭

    if ($payment_row['IsOnline'] == 1 && (int)$order_row['OrderStatus'] < 4) {//支付方式为在线付款，并且状态为未付款
        echo '<div id="payment_loading" style="width:150px; height:32px; margin:30% auto 0; padding-top:40px; text-align:center; font-size:24px; color:#333; background:url(/static/themes/default/images/global/loading.gif) no-repeat center top;">Loading</div>';
        if ($order_row['PId'] == 1 && $c['NewFunVersion'] >= 4) {
            //新版Paypal支付
            js::location("/cart/success/{$OId}.html");
            exit;
        }
        include('payment.php');
        exit();
    }
} else {
    $payment_row = db::get_one('payment', "PId='{$order_row['PId']}' and IsUsed=1");
    !$payment_row && js::location("/cart/success/{$OId}.html");//支付方式已关闭
}
$payOffline = ($payment_row['IsOnline'] != 1 && $order_row['OrderStatus'] != 1 && $order_row['OrderStatus'] != 3) ? 0 : 1;
?>
<script type="text/javascript">$(document).ready(function () {
        cart_obj.complete_init()
    });</script>

<style>
    .btn_view_order {
        width: 440px;
        margin-left: 50px;
        background-color: #666666;
        text-align: center;
    }
</style>
<div id="lib_cart" class="wide complete_container">

    <?php
    if ($d == 'view') {
        ?>

        <?php
        /************************************************** Paypal Checkout Start **************************************************/
    if ($order_row['OrderStatus'] == 1 && $order_row['PId'] == 2) {
        //相关费用
        $Symbol = db::get_value('currency', "Currency='{$order_row['Currency']}'", 'Symbol'); //货币符号
        $ProductPrice = orders::orders_product_price($order_row, 1); //产品总价
        $UserDiscount = ($order_row['UserDiscount'] > 0 && $order_row['UserDiscount'] < 100) ? $order_row['UserDiscount'] : 100;
        $_price = $ProductPrice;
        $_price -= $ProductPrice * (1 - (100 - $order_row['Discount']) / 100); //折扣
        $_price -= $ProductPrice * (1 - $UserDiscount / 100); //会员折扣
        $_price -= $ProductPrice * $order_row['CouponDiscount']; //优惠券折扣
        $DiscountPrice = $_price - $ProductPrice;
        $SaleDiscountPrice = cart::iconv_price($order_row['DiscountPrice'], 2, $order_row['Currency'], 0);
        //快递选择
        $IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
        $shipping_mothed_ary = orders::orders_confirm_shipping_method($order_row);
        (!$shipping_mothed_ary['info'][1]) && $shipping_mothed_ary['info'][1] = array(); //购物车没有默认海外仓追加隐藏选项
        $oversea_count = count($shipping_mothed_ary['info']);
        if ((int)$c['config']['global']['Overseas'] == 0 || count($c['config']['Overseas']) < 2 || $oversea_count < 2) {
            //关闭海外仓功能 或者 仅有一个海外仓选项
            echo '<style type="text/css">.information_shipping .shipping:first-child .title{display:none;}</style>';
        }
        ?>
        <script type="text/javascript">
            var address_perfect = 0,
                address_perfect_aid = 0;
            $(document).ready(function () {
                cart_obj.checkout_init();
                $('.information_shipping .shipping').each(function () {
                    $(this).find('.shipping_method_list li:eq(0)').click(); //默认点击第一个选项
                });
                $('.payment_list .payment_row').eq(0).click(); //默认点击第一个选项
            });
        </script>
        <div id="lib_cart" class="success_container checkout_container clean">
            <?php include('include/header.php'); ?>
            <div class="order_info">
                <div class="checkout_error_tips hide" data-country=""
                     data-email="<?= $c['config']['global']['AdminEmail']; ?>"></div>
                <div class="information_box information_address">
                    <div class="box_title"><?= $c['lang_pack']['cart']['address']; ?></div>
                    <div class="box_content">
                        <div class="address_default item clearfix"></div>
                        <div class="address_list clearfix">
                            <div class="item odd">
                                <input type="radio" name="shipping_address_id" id="address_99" value="99"
                                       data-cid="<?= $order_row['ShippingCId']; ?>"
                                       data-country-name="<?= $order_row['ShippingCountry']; ?>"/>
                                <p class="clearfix">
                                    <strong><?= $order_row['ShippingFirstName'] . ' ' . $order_row['ShippingLastName']; ?></strong>
                                </p>
                                <p class="address_line"><?= $order_row['ShippingAddressLine1'] . ' ' . ($order_row['ShippingAddressLine2'] ? $order_row['ShippingAddressLine2'] . ' ' : ''); ?></p>
                                <p><?= $order_row['ShippingCity'] . ' ' . ($order_row['ShippingStateName'] ? $order_row['ShippingStateName'] : $order_row['ShippingState']) . ' ' . $order_row['ShippingCountry'] . ' (' . $order_row['ShippingZipCode'] . ')'; ?></p>
                                <?= $order_row['ShippingPhoneNumber'] ? '<p>' . $order_row['ShippingCountryCode'] . ' ' . $order_row['ShippingPhoneNumber'] . '</p>' : ''; ?>
                            </div>
                        </div>
                        <div id="addressInfo" style="display:none;"></div>
                    </div>
                </div>
                <?php if ($order_row['ShippingPhoneNumber'] == '') { ?>
                    <div class="information_box information_phone">
                        <div class="box_title"><?= $c['lang_pack']['cart']['shipPhone']; ?></div>
                        <div class="box_content">
                            <div class="phone_input clearfix"><input type="text" name="phoneCode" class="box_input"
                                                                     placeholder="<?= $c['lang_pack']['cart']['phoneTips']; ?>"
                                                                     autocomplete="off" notnull/></div>
                            <p class="phone_error"></p>
                        </div>
                    </div>
                <?php } ?>
                <div class="information_box information_shipping">
                    <div class="box_title<?= $oversea_count == 1 ? ' no_border' : ''; ?>"><?= $c['lang_pack']['cart']['shipmethod']; ?></div>
                    <div class="box_content">
                        <?php foreach ((array)$shipping_mothed_ary['info'] as $OvId => $v) { ?>
                            <div class="shipping<?= ($NotDefualtOvId && (int)$OvId == 1) ? ' hide' : ''; ?>"
                                 data-id="<?= $OvId; ?>">
                                <div class="title">
                                    <strong><?= $c['lang_pack']['products']['shipsFrom'] . ' ' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']]; ?></strong>
                                    <div class="shipping_info"><span class="name"></span><span class="price"></span>
                                    </div>
                                    <i class="icon_shipping_title"></i>
                                </div>
                                <div class="list" style="display:block;">
                                    <ul class="shipping_method_list clearfix"></ul>
                                    <?php if ($IsInsurance) { ?>
                                        <div class="insurance">
                                            <span class="name"><?= $c['lang_pack']['cart']['insurance']; ?>:</span>
                                            <input type="checkbox" name="_shipping_method_insurance"
                                                   class="shipping_insurance" value="1" checked/>
                                            <label for="shipping_insurance"><?= $c['lang_pack']['cart']['add_insur']; ?></label>
                                            <a href="javascript:;" class="delivery_ins"
                                               content="<?= $c['lang_pack']['cart']['tips_insur']; ?>"><?= $c['lang_pack']['cart']['why_insur']; ?></a>
                                            <span class="price"><?= $_SESSION['Currency']['Symbol']; ?><em></em></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="tips"><?= $c['lang_pack']['cart']['arrive']; ?></div>
                        <div class="editor_txt"><?= str::str_code($c['config']['global']['ArrivalInfo']['ArrivalInfo' . $c['lang']], 'htmlspecialchars_decode'); ?></div>
                    </div>
                </div>
                <div class="information_box information_payment hide">
                    <div class="box_title"><?= $c['lang_pack']['cart']['paymethod']; ?></div>
                    <div class="box_content">
                        <?php
                        $payment_row = db::get_one('payment', 'IsUsed=1 and PId=2');
                        ?>
                        <div class="payment_list clearfix">
                            <div class="payment_row" value="<?= $payment_row['PId']; ?>"
                                 min="<?= cart::iconv_price($payment_row['MinPrice'], 2, '', 0); ?>"
                                 max="<?= cart::iconv_price($payment_row['MaxPrice'], 2, '', 0); ?>"
                                 method="<?= $payment_row['Method']; ?>">
                                <div class="check">&nbsp;<input name="PId" type="radio"/></div>
                                <div class="img"><img src="<?= $payment_row['LogoPath']; ?>"
                                                      alt="<?= $payment_row['Name' . $c['lang']]; ?>"><span></span>
                                </div>
                                <em class="icon_dot"></em>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="payment_contents clearfix" style="display:none;">
                            <div class="payment_note" data-id="<?= $payment_row['PId']; ?>"
                                 data-fee="<?= $payment_row['AdditionalFee']; ?>"
                                 data-affix="<?= cart::iconv_price($payment_row['AffixPrice'], 2, '', 0); ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="information_box information_products">
                    <div class="box_title"><?= $c['lang_pack']['cart']['products']; ?></div>
                    <div class="box_content information_product cartFrom">
                        <table class="itemFrom">
                            <thead>
                            <tr>
                                <th class="item_product item_header"><?= $c['lang_pack']['cart']['item']; ?></th>
                                <th class="item_price item_header"><?= $c['lang_pack']['cart']['price']; ?></th>
                                <th class="item_quantity item_header"><?= $c['lang_pack']['cart']['qty']; ?></th>
                                <th class="item_operate item_header"><?= $c['lang_pack']['cart']['amount']; ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_price = $s_total_price = $quantity = $OvId = 0;
                            $cart_attr = $cart_attr_data = $cart_attr_value = array();
                            $order_list_row = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, o.SKU as OrderSKU, p.Prefix, p.Number, p.SKU, p.PicPath_0, p.Business, p.CateId, p.IsFreeShipping, p.TId', 'o.LId asc');
                            foreach ((array)$order_list_row as $v) {
                                if ($v['BuyType'] == 4) {
                                    //组合促销
                                    $package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
                                    if (!$package_row) continue;
                                    $attr = array();
                                    $v['Property'] != '' && $attr = str::json_data($v['Property'], 'decode');
                                    $products_row = str::str_code(db::get_all('products', "SoldOut=0 and ProId='{$package_row['ProId']}'"));
                                    $pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
                                    $pro_where == '' && $pro_where = 0;
                                    $products_row = array_merge($products_row, str::str_code(db::get_all('products', "SoldOut=0 and ProId in($pro_where)")));
                                    $data_ary = str::json_data($package_row['Data'], 'decode');
                                } else {
                                    //普通产品
                                    $attr = array();
                                    $v['Property'] != '' && $attr = str::json_data($v['Property'], 'decode');
                                    $attr_len = count($attr);
                                    $oversea_len = db::get_row_count('products_selected_attribute', "ProId='{$v['ProId']}' and AttrId=0 and VId=0 and OvId>1 and IsUsed=1");
                                    (int)$c['config']['global']['Overseas'] == 0 && $attr_len == 1 && $attr['Overseas'] && $attr_len -= 1;
                                    $img = ly200::get_size_img($v['PicPath'], '240x240');
                                    $url = ly200::get_url($v, 'products');
                                }
                                $price = $v['Price'] + $v['PropertyPrice'];
                                $v['Discount'] < 100 && $price *= $v['Discount'] / 100;
                                $s_total_price += $price * $v['Qty'];
                                $total_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
                                $quantity += $v['Qty'];
                                $OvId = $v['OvId'];
                                ?>
                                <tr>
                                    <td class="prod_info_detail">
                                        <?php
                                        if ($v['BuyType'] == 4) {
                                            //组合促销
                                            echo '<strong>[ ' . $c['lang_pack']['cart']['package'] . ' ] ' . $package_row['Name'] . '</strong>';
                                            foreach ((array)$products_row as $k2 => $v2) {
                                                $img = ly200::get_size_img($v2['PicPath_0'], '240x240');
                                                $url = ly200::get_url($v2, 'products');
                                                ?>
                                                <dl class="clearfix pro_list<?= $k2 ? '' : ' first'; ?>">
                                                    <dt class="prod_pic"><a href="<?= $url; ?>"
                                                                            title="<?= $v2['Name' . $c['lang']]; ?>"
                                                                            class="pic_box"><img src="<?= $img; ?>"
                                                                                                 alt="<?= $v2['Name' . $c['lang']]; ?>"/><span></span></a>
                                                    </dt>
                                                    <dd class="prod_info">
                                                        <div class="invalid FontBgColor"><?= $c['lang_pack']['cart']['invalid']; ?></div>
                                                        <h4 class="prod_name"><?= $v2['Name' . $c['lang']]; ?></h4>
                                                        <?php
                                                        if ($k2 == 0) { //主产品
                                                            echo '<div>';
                                                            foreach ((array)$attr as $k => $z) {
                                                                if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                                echo '<p class="attr_' . $k . '">' . ($k == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $k) . ': ' . $z . '</p>';
                                                            }
                                                            if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                                echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                                            }
                                                            echo '</div>';
                                                        } elseif ($data_ary[$v2['ProId']]) { //捆绑产品
                                                            echo '<div>';
                                                            $OvId = 1;
                                                            foreach ((array)$data_ary[$v2['ProId']] as $k3 => $v3) {
                                                                if ($k3 == 'Overseas') { //发货地
                                                                    $OvId = str_replace('Ov:', '', $v3);
                                                                    if ((int)$c['config']['global']['Overseas'] == 0 || $OvId == 1) continue; //发货地是中国，不显示
                                                                    echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</p>';
                                                                } else {
                                                                    echo '<p class="attr_' . $k3 . '">' . $attribute_ary[$k3][1] . ': ' . $vid_data_ary[$k3][$v3] . '</p>';
                                                                }
                                                            }
                                                            if ((int)$c['config']['global']['Overseas'] == 1 && $OvId == 1) {
                                                                echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</p>';
                                                            }
                                                            echo '</div>';
                                                        } ?>
                                                    </dd>
                                                </dl>
                                                <?php
                                            }
                                        } else {
                                            //普通产品
                                            ?>
                                            <dl>
                                                <dt class="prod_pic"><a href="<?= $url; ?>"
                                                                        title="<?= $v['Name' . $c['lang']]; ?>"
                                                                        class="pic_box"><img src="<?= $img; ?>"
                                                                                             alt="<?= $v['Name' . $c['lang']]; ?>"/><span></span></a>
                                                </dt>
                                                <dd class="prod_info">
                                                    <div class="invalid FontBgColor"><?= $c['lang_pack']['cart']['invalid']; ?></div>
                                                    <h4 class="prod_name"><?= $v['Name']; ?></h4>
                                                    <p class="prod_number"><?= $v['Prefix'] . $v['Number']; ?></p>
                                                    <?php
                                                    if ($attr_len) {
                                                        echo '<div>';
                                                        foreach ((array)$attr as $k => $z) {
                                                            if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                            echo '<p class="attr_' . $k . '">' . ($k == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $k) . ': ' . $z . '</p>';
                                                        }
                                                        if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                            echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                                        }
                                                        echo '</div>';
                                                    } elseif ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                        echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                                    } ?>
                                                </dd>
                                            </dl>
                                        <?php } ?>
                                    </td>
                                    <td class="prod_price"><p price="<?= $v['Price']; ?>"
                                                              discount="<?= $v['Discount']; ?>"><?= cart::iconv_price($price); ?></p>
                                    </td>
                                    <td class="prod_quantity" start="<?= $v['StartFrom']; ?>"><p><?= $v['Qty']; ?></p>
                                    </td>
                                    <td class="prod_operate">
                                        <p price="<?= cart::iconv_price($price, 2, '', 0) * $v['Qty']; ?>"><?= cart::iconv_price(0, 1) . cart::currency_format(cart::iconv_price($price, 2, '', 0) * $v['Qty'], 0, $order_row['Currency']); ?></p>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="order_summary clearfix">
                        <?php if ($c['FunVersion']) { ?>
                            <div class="coupon_box fl">
                                <div class="code_input clearfix">
                                    <input type="text" name="couponCode" class="box_input"
                                           placeholder="<?= $c['lang_pack']['mobile']['apply'] . ' ' . $c['lang_pack']['mobile']['coupon_code']; ?>"
                                           autocomplete="off"/>
                                    <div class="btn_coupon_submit btn_global sys_shadow_button FontBgColor"
                                         id="coupon_apply"><?= $c['lang_pack']['submit']; ?></div>
                                </div>
                                <p class="code_error"><?= $c['lang_pack']['cart']['coupon_error']; ?></p>
                                <div class="code_valid clearfix" id="code_valid">
                                    <div class="code_valid_key"></div>
                                    <div class="code_valid_content"><?= $c['lang_pack']['mobile']['code']; ?>:
                                        <strong></strong></div>
                                    <a href="javascript:;" class="btn_coupon_remove sys_shadow_button"
                                       id="removeCoupon">X</a>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="amount_box fr">
                            <div class="rows clearfix">
                                <div class="name"><?= $c['lang_pack']['cart']['subtotal']; ?>:</div>
                                <div class="value"><em><?= $Symbol; ?></em><span
                                            id="ot_subtotal"><?= cart::currency_format($ProductPrice, 0, $order_row['Currency']); ?></span>
                                </div>
                            </div>
                            <?php
                            $UserPrice = 0;
                            if ($order_row['UserDiscount'] > 0) { //会员折扣
                                $UserPrice = $ProductPrice - $ProductPrice * ($order_row['UserDiscount'] / 100);
                                ?>
                                <div class="rows clearfix" id="MemberCharge">
                                    <div class="name"><?= $c['lang_pack']['cart']['user_save']; ?>:</div>
                                    <div class="value"><em>- <?= $Symbol; ?></em><span
                                                id="ot_user"><?= cart::currency_format($UserPrice, 0, $order_row['Currency']); ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="rows clearfix" id="ShippingCharge">
                                <div class="name"><?= $c['lang_pack']['cart']['shipcharge']; ?>:</div>
                                <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                            id="ot_shipping"></span></div>
                            </div>
                            <div class="rows clearfix" id="ShippingInsuranceCombine">
                                <div class="name"><?= $c['lang_pack']['cart']['ship_insur']; ?>:</div>
                                <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                            id="ot_combine_shippnig_insurance"></span></div>
                            </div>
                            <div class="rows clearfix" id="CouponCharge">
                                <div class="name"><?= $c['lang_pack']['cart']['code_save']; ?>:</div>
                                <div class="value"><em>- <?= $_SESSION['Currency']['Symbol']; ?></em><span
                                            id="ot_coupon"></span></div>
                            </div>
                            <?php
                            if ($order_row['Discount'] > 0) { //优惠折扣 折扣形式
                                $DiscountPrice = $ProductPrice - $ProductPrice * ((100 - $order_row['Discount']) / 100);
                                ?>
                                <div class="rows clearfix" id="DiscountCharge">
                                    <div class="name"><?= $c['lang_pack']['cart']['save']; ?>:</div>
                                    <div class="value"><em>- <?= $Symbol; ?></em><span
                                                id="ot_subtotal_discount"><?= cart::currency_format($DiscountPrice, 0, $order_row['Currency']); ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            if ($order_row['DiscountPrice'] > 0) { //优惠折扣 金额形式
                                $DiscountPrice = $order_row['DiscountPrice'];
                                ?>
                                <div class="rows clearfix" id="DiscountCharge">
                                    <div class="name"><?= $c['lang_pack']['cart']['save']; ?>:</div>
                                    <div class="value"><em>- <?= $Symbol; ?></em><span
                                                id="ot_subtotal_discount"><?= cart::currency_format($order_row['DiscountPrice'], 0, $order_row['Currency']); ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="rows clearfix" id="ServiceCharge" style="display:block;">
                                <div class="name"><?= $c['lang_pack']['cart']['fee']; ?>:</div>
                                <div class="value"><em><?= $Symbol; ?></em><span id="ot_fee"></span></div>
                            </div>
                            <div class="rows clearfix" id="TotalCharge">
                                <div class="name"><?= $c['lang_pack']['cart']['grand']; ?>:</div>
                                <div class="value"><em><?= $Symbol; ?></em><span id="ot_total"></span></div>
                            </div>
                            <form id="PlaceOrderFrom" method="post" action="/"
                                  amountPrice="<?= $ProductPrice; ?>"<?= $UserPrice > 0 ? ' userPrice="' . $UserPrice . '" userRatio="' . $order_row['UserDiscount'] . '"' : ' userPrice="0" userRatio="100"'; ?>>
                                <input type="button" value="<?= $c['lang_pack']['mobile']['confirm']; ?>"
                                       class="btn_place_order btn_global sys_shadow_button" id="excheckoutFormSubmit"/>
                                <?php if ($c['NewFunVersion'] >= 4) { //新版Paypal支付?>
                                    <div id="paypal_button_container"></div>
                                <?php } ?>
                                <input type="button" value="<?= $c['lang_pack']['cart']['cancel']; ?>"
                                       class="btn_cancel btn_global sys_shadow_button"/>
                                <input type="hidden" name="OId" value="<?= $OId; ?>"/>
                                <input type="hidden" name="ProductPrice"
                                       value="<?= sprintf('%01.2f', $ProductPrice); ?>"/>
                                <input type="hidden" name="DiscountPrice"
                                       value="<?= sprintf('%01.2f', $DiscountPrice); ?>"/>
                                <input type="hidden" name="order_coupon_code"
                                       value="<?= $_SESSION['Cart']['Coupon']; ?>" cutprice="0"/>
                                <input type="hidden" name="order_discount_price"
                                       value="<?= sprintf('%01.2f', $DiscountPrice); ?>"/>
                                <input type="hidden" name="order_shipping_address_aid" value="-1"/>
                                <input type="hidden" name="order_shipping_states_sid"
                                       value="<?= (int)$order_row['ShippingSId']; ?>"/>
                                <input type="hidden" name="order_shipping_address_cid" value="-1"/>
                                <input type="hidden" name="order_shipping_method_sid" value="[]"/>
                                <input type="hidden" name="order_shipping_method_type" value="[]"/>
                                <input type="hidden" name="order_shipping_price" value="[]"/>
                                <input type="hidden" name="order_shipping_insurance" value="[]" price="[]"/>
                                <input type="hidden" name="order_shipping_oversea"
                                       value="<?= $oversea_id_hidden ? implode(',', $oversea_id_hidden) : ''; ?>"/>
                            </form>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    <?php
    /************************************************** “确认订单”页面 End **************************************************/
    } else {
    ?>
        <div class="position"><strong><?= $c['lang_pack']['cart']['position']; ?>: </strong> <a
                    href="/"><?= $c['lang_pack']['cart']['home']; ?></a> &gt; <a
                    href="/cart/"><?= $c['lang_pack']['cart']['cart']; ?></a> &gt;
            <strong><?= ((int)$order_row['OrderStatus'] >= 4 && (int)$order_row['OrderStatus'] < 7) ? $c['lang_pack']['cart']['complete'] : $c['lang_pack']['cart']['payment']; ?></strong>
        </div>
        <div class="blank12"></div>
        <div class="complete">
            <div class="tips fl">
                <h3><?= $c['lang_pack']['cart']['thanks']; ?></h3>
                <div class="payment_info"><?= $payment_row['Description' . $c['lang']]; ?></div>
                <?php
                if ((int)$payOffline && $payment_row['PId'] != 9) {    //线下支付，并且非货到付款
                    $currency_row = db::get_all('currency', 'IsUsed="1"');
                    ?>
                    <div class="payment_form" style="display: none">
                        <form method="post" id="PaymentForm">
                            <?php
                            if ($payment_row['Method'] == 'WesternUnion') {
                                //西联
                                ?>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['firstname']; ?></span>
                                                <input type="text" class="input_box_txt" name="FirstName"
                                                       placeholder="<?= $c['lang_pack']['cart']['firstname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['lastname']; ?></span>
                                                <input type="text" class="input_box_txt" name="LastName"
                                                       placeholder="<?= $c['lang_pack']['cart']['lastname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Currency" notnull="">
                                                    <?php foreach ((array)$currency_row as $v) { ?>
                                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['sentmoney']; ?></span>
                                                <input type="text" class="input_box_txt" name="SentMoney"
                                                       placeholder="<?= $c['lang_pack']['cart']['sentmoney']; ?>"
                                                       maxlength="8" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['mtcn']; ?>
                                                    (<?= $c['lang_pack']['mobile']['10_digits'] ?>)</span>
                                                <input type="text" class="input_box_txt" name="MTCNNumber"
                                                       placeholder="<?= $c['lang_pack']['cart']['mtcn']; ?> (<?= $c['lang_pack']['mobile']['10_digits'] ?>)"
                                                       maxlength="10" format="Length|10" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Country"
                                                        placeholder="<?= $c['lang_pack']['cart']['choosecty']; ?>"
                                                        notnull="">
                                                    <option value=""><?= $c['lang_pack']['cart']['selectcty']; ?></option>
                                                    <?php
                                                    $country_row = str::str_code(db::get_all('country', 'IsUsed=1', '*', 'IsHot desc, Country asc'));
                                                    foreach ((array)$country_row as $v) {
                                                        $Country = $v['Country'];
                                                        if ($c['lang'] != '_en') {
                                                            $country_data = str::json_data(htmlspecialchars_decode($v['CountryData']), 'decode');
                                                            $Country = $country_data[substr($c['lang'], 1)];
                                                        }
                                                        ?>
                                                        <option value="<?= $v['CId']; ?>"><?= $Country; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['contents']; ?></span>
                                            <textarea name="Contents" class="input_box_txt input_box_textarea"
                                                      placeholder="<?= $c['lang_pack']['cart']['contents']; ?>"></textarea>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="button">
                                    <a href="javascript:;" class="btn_cancel btn_global sys_shadow_button"
                                       id="Cancel"><?= $c['lang_pack']['cart']['cancel']; ?></a>
                                    <button type="submit" class="btn_save btn_global sys_shadow_button"
                                            id="paySubmit"><?= $c['lang_pack']['cart']['submit']; ?></button>
                                </div>
                                <?php
                            } elseif ($payment_row['Method'] == 'MoneyGram' || $payment_row['Method'] == 'TT' || $payment_row['Method'] == 'BankTransfer') {
                                //其他银行转账
                                ?>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['firstname']; ?></span>
                                                <input type="text" class="input_box_txt" name="FirstName"
                                                       placeholder="<?= $c['lang_pack']['cart']['firstname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['surname']; ?></span>
                                                <input type="text" class="input_box_txt" name="LastName"
                                                       placeholder="<?= $c['lang_pack']['cart']['surname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Currency" notnull="">
                                                    <?php foreach ((array)$currency_row as $v) { ?>
                                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['sentmoney']; ?></span>
                                                <input type="text" class="input_box_txt" name="SentMoney"
                                                       placeholder="<?= $c['lang_pack']['cart']['sentmoney']; ?>"
                                                       maxlength="8" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['bankSlipCode']; ?></span>
                                            <input type="text" class="input_box_txt" name="MTCNNumber"
                                                   placeholder="<?= $c['lang_pack']['cart']['bankSlipCode']; ?>"
                                                   maxlength="50" notnull/>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['contents']; ?></span>
                                            <textarea name="Contents" class="input_box_txt input_box_textarea"
                                                      placeholder="<?= $c['lang_pack']['cart']['contents']; ?>"></textarea>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="button">
                                    <a href="javascript:;" class="btn_cancel btn_global sys_shadow_button"
                                       id="Cancel"><?= $c['lang_pack']['cart']['cancel']; ?></a>
                                    <button type="submit" class="btn_save btn_global sys_shadow_button"
                                            id="paySubmit"><?= $c['lang_pack']['cart']['submit']; ?></button>
                                </div>
                            <?php } ?>
                            <input type="hidden" name="PaymentMethod" value="<?= $payment_row['Method']; ?>"/>
                            <input type="hidden" name="OId" value="<?= $order_row['OId']; ?>"/>
                        </form>
                    </div>
                <?php } ?>
            </div>
            <div class="fl">

                <div class="orders_info">
                    <h3><?= $c['lang_pack']['cart']['summary']; ?></h3>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['orderNo']; ?></label>
                        <span class="red"><?= $OId; ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['totalamount']; ?>:</label>
                        <span class="red"><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price(orders::orders_price($order_row, 1, 1), 2); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['totalqty']; ?>:</label>
                        <span><?= (int)db::get_sum('orders_products_list', "OrderId='{$order_row['OrderId']}'", 'Qty'); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['deposit']; ?>:</label>
                        <span><?= $_SESSION['Currency']['Symbol'] . cart::iconv_price($order_row['DepositPrice'], 2); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['status']; ?>:</label>
                        <span><?= $c['lang_pack']['user']['OrderStatusAry'][$order_row['OrderStatus']]; ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['paymethod']; ?>:</label>
                        <span><?= $order_row['PaymentMethod']; ?></span>
                        <div class="clear"></div>
                    </div>
                </div>
                <!--                    add by jay start -->

                <a href="/account/orders/"
                   class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>

                <!--                    add by jay end -->
            </div>
            <div class="blank12"></div>

            <div class="product_list"></div>
        </div>
        <div class="blank12"></div>
    <?php } ?>

    <?php
    } elseif ($d == 'balance'){
    $data = array(
        'OrderStatus' => 5
    );
    db::update('orders', "OId='$OId'", $data);
    $order_row = db::get_one('orders', "OId='$OId'");
    ?>

        <div class="position"><strong><?= $c['lang_pack']['cart']['position']; ?>: </strong> <a
                    href="/"><?= $c['lang_pack']['cart']['home']; ?></a> &gt; <a
                    href="/cart/"><?= $c['lang_pack']['cart']['cart']; ?></a> &gt;
            <strong><?= ((int)$order_row['OrderStatus'] >= 4 && (int)$order_row['OrderStatus'] < 7) ? $c['lang_pack']['cart']['complete'] : $c['lang_pack']['cart']['payment']; ?></strong>
        </div>
        <div class="blank12"></div>
        <div class="complete">
            <div class="tips fl">
                <h3><?= $c['lang_pack']['cart']['thanks']; ?></h3>
                <div class="payment_info"><?= $payment_row['Description' . $c['lang']]; ?></div>
                <?php
                if ((int)$payOffline && $payment_row['PId'] != 9) {    //线下支付，并且非货到付款
                    $currency_row = db::get_all('currency', 'IsUsed="1"');
                    ?>
                    <div class="payment_form" style="display: none">
                        <form method="post" id="PaymentForm">
                            <?php
                            if ($payment_row['Method'] == 'WesternUnion') {
                                //西联
                                ?>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['firstname']; ?></span>
                                                <input type="text" class="input_box_txt" name="FirstName"
                                                       placeholder="<?= $c['lang_pack']['cart']['firstname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['lastname']; ?></span>
                                                <input type="text" class="input_box_txt" name="LastName"
                                                       placeholder="<?= $c['lang_pack']['cart']['lastname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Currency" notnull="">
                                                    <?php foreach ((array)$currency_row as $v) { ?>
                                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['sentmoney']; ?></span>
                                                <input type="text" class="input_box_txt" name="SentMoney"
                                                       placeholder="<?= $c['lang_pack']['cart']['sentmoney']; ?>"
                                                       maxlength="8" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['mtcn']; ?>
                                                    (<?= $c['lang_pack']['mobile']['10_digits'] ?>)</span>
                                                <input type="text" class="input_box_txt" name="MTCNNumber"
                                                       placeholder="<?= $c['lang_pack']['cart']['mtcn']; ?> (<?= $c['lang_pack']['mobile']['10_digits'] ?>)"
                                                       maxlength="10" format="Length|10" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Country"
                                                        placeholder="<?= $c['lang_pack']['cart']['choosecty']; ?>"
                                                        notnull="">
                                                    <option value=""><?= $c['lang_pack']['cart']['selectcty']; ?></option>
                                                    <?php
                                                    $country_row = str::str_code(db::get_all('country', 'IsUsed=1', '*', 'IsHot desc, Country asc'));
                                                    foreach ((array)$country_row as $v) {
                                                        $Country = $v['Country'];
                                                        if ($c['lang'] != '_en') {
                                                            $country_data = str::json_data(htmlspecialchars_decode($v['CountryData']), 'decode');
                                                            $Country = $country_data[substr($c['lang'], 1)];
                                                        }
                                                        ?>
                                                        <option value="<?= $v['CId']; ?>"><?= $Country; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['contents']; ?></span>
                                            <textarea name="Contents" class="input_box_txt input_box_textarea"
                                                      placeholder="<?= $c['lang_pack']['cart']['contents']; ?>"></textarea>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="button">
                                    <a href="javascript:;" class="btn_cancel btn_global sys_shadow_button"
                                       id="Cancel"><?= $c['lang_pack']['cart']['cancel']; ?></a>
                                    <button type="submit" class="btn_save btn_global sys_shadow_button"
                                            id="paySubmit"><?= $c['lang_pack']['cart']['submit']; ?></button>
                                </div>
                                <?php
                            } elseif ($payment_row['Method'] == 'MoneyGram' || $payment_row['Method'] == 'TT' || $payment_row['Method'] == 'BankTransfer') {
                                //其他银行转账
                                ?>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['firstname']; ?></span>
                                                <input type="text" class="input_box_txt" name="FirstName"
                                                       placeholder="<?= $c['lang_pack']['cart']['firstname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['surname']; ?></span>
                                                <input type="text" class="input_box_txt" name="LastName"
                                                       placeholder="<?= $c['lang_pack']['cart']['surname']; ?>"
                                                       notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="form_box clean">
                                        <div class="box">
                                            <div class="box_select">
                                                <select name="Currency" notnull="">
                                                    <?php foreach ((array)$currency_row as $v) { ?>
                                                        <option value="<?= $v['Currency']; ?>" <?= $_SESSION['Currency']['Currency'] == $v['Currency'] ? 'selected' : ''; ?>><?= $v['Currency']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <p class="error"></p>
                                        </div>
                                        <div class="box">
                                            <label class="input_box">
                                                <span class="input_box_label"><?= $c['lang_pack']['cart']['sentmoney']; ?></span>
                                                <input type="text" class="input_box_txt" name="SentMoney"
                                                       placeholder="<?= $c['lang_pack']['cart']['sentmoney']; ?>"
                                                       maxlength="8" notnull/>
                                            </label>
                                            <p class="error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['bankSlipCode']; ?></span>
                                            <input type="text" class="input_box_txt" name="MTCNNumber"
                                                   placeholder="<?= $c['lang_pack']['cart']['bankSlipCode']; ?>"
                                                   maxlength="50" notnull/>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="rows">
                                    <div class="input clean">
                                        <label class="input_box">
                                            <span class="input_box_label"><?= $c['lang_pack']['cart']['contents']; ?></span>
                                            <textarea name="Contents" class="input_box_txt input_box_textarea"
                                                      placeholder="<?= $c['lang_pack']['cart']['contents']; ?>"></textarea>
                                        </label>
                                        <p class="error"></p>
                                    </div>
                                </div>
                                <div class="button">
                                    <a href="javascript:;" class="btn_cancel btn_global sys_shadow_button"
                                       id="Cancel"><?= $c['lang_pack']['cart']['cancel']; ?></a>
                                    <button type="submit" class="btn_save btn_global sys_shadow_button"
                                            id="paySubmit"><?= $c['lang_pack']['cart']['submit']; ?></button>
                                </div>
                            <?php } ?>
                            <input type="hidden" name="PaymentMethod" value="<?= $payment_row['Method']; ?>"/>
                            <input type="hidden" name="OId" value="<?= $order_row['OId']; ?>"/>
                        </form>
                    </div>
                <?php } ?>
            </div>
            <div class="fl">
                <div class="orders_info ">
                    <h3><?= $c['lang_pack']['cart']['summary']; ?></h3>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['orderNo']; ?></label>
                        <span class="red"><?= $OId; ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['totalamount']; ?>:</label>
                        <span class="red"><?= $_SESSION['Currency']['Symbol'] . ' ' . cart::iconv_price(orders::orders_price($order_row, 1, 1), 2); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['totalqty']; ?>:</label>
                        <span><?= (int)db::get_sum('orders_products_list', "OrderId='{$order_row['OrderId']}'", 'Qty'); ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['deposit']; ?>:</label>
                        <span>
                        <?= $_SESSION['Currency']['Symbol'] . cart::iconv_price($order_row['DepositPrice'], 2); ?>
                        <?php
                        if ($order_row['OrderStatus'] > 1) {
                            echo '(' . $c['lang_pack']['user']['paid'] . ')';
                        }
                        ?>

                    </span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['user']['_balance']; ?>:</label>
                        <span>
                        <?php
                        $total_price = cart::iconv_price(orders::orders_price($order_row, 1, 1), 2);
                        echo $_SESSION['Currency']['Symbol'] . cart::iconv_price(cart::getBalance($total_price, $order_row['DepositPrice']), 2);
                        if ($order_row['OrderStatus'] > 4) {
                            echo '(' . $c['lang_pack']['user']['paid'] . ')';
                        }
                        ?>
                    </span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['status']; ?>:</label>
                        <span><?= $c['lang_pack']['user']['OrderStatusAry'][$order_row['OrderStatus']]; ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="rows">
                        <label><?= $c['lang_pack']['cart']['paymethod']; ?>:</label>
                        <span><?= $order_row['PaymentMethod']; ?></span>
                        <div class="clear"></div>
                    </div>
                </div>

                <!--                    add by jay start -->
                <a href="/account/orders/"
                   class="btn_global btn_view_order"><?= $c['lang_pack']['mobile']['view_order']; ?></a>
                <!--                    add by jay end -->
            </div>
            <div class="blank12"></div>

            <div class="product_list"></div>
        </div>
        <div class="blank12"></div>

        <?php
    }
    ?>
</div>
