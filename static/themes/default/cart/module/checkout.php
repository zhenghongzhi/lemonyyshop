<?php !isset($c) && exit(); ?>
<?php

$error_ary = array();
$yy_code = db::get_one('user', "UserId='{$_SESSION['User']['UserId']}'", 'yy_code');
if($yy_code['yy_code']){

    $balance = ly200::getyy_balance($yy_code['yy_code']);
}

//BuyNow数据
if ($_GET['Data']) {
    $Data = array();
    $data_ary = explode('&', rawurldecode(base64_decode($_GET['Data'])));//Buy Now参数，已通过加密
    foreach ($data_ary as $v) {
        $arr = explode('=', $v);
        $Data[$arr[0]] = $arr[1];
    }
    $CId = (int)$Data['CId'];
}

//Checkout数据
if ($_GET['CId']) {
    $CId = $_GET['CId'];
}

$CIdStr = $cCIdStr = '';
if ($CId) {
    $CIdStr = ' and CId in(' . str_replace('.', ',', $CId) . ')';
    $cCIdStr = ' and c.CId in(' . str_replace('.', ',', $CId) . ')';
}

//购物车产品
$cart_row = db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', "c.{$c['where']['cart']}{$cCIdStr}", "c.*, c.Attr as CartAttr, p.Name{$c['lang']}, p.Prefix, p.Number, p.AttrId, p.Attr, p.IsCombination, p.IsOpenAttrPrice, p.CateId, p.price_rate", 'c.CId desc');
!count($cart_row) && js::location("/cart/");
//产品总金额
$total_price = db::get_sum('shopping_cart', $c['where']['cart'] . $CIdStr, '(Price+PropertyPrice)*Discount/100*Qty');
$iconv_total_price = cart::cart_total_price($cCIdStr, 1);
//产品总重量（自身+包装）
$total_weight = cart::cart_product_weight($cCIdStr, 2);
//检查产品资料是否完整
$oversea_id_ary = $CateId_ary = array();
foreach ((array)$cart_row as $v) {
    !in_array($v['OvId'], $oversea_id_ary) && $oversea_id_ary[] = $v['OvId'];
    $CateId_ary[$v['CateId']] = "CateId like '%|{$v['CateId']}|%'";//记录产品分类
    if ($v['BuyType'] != 4 && $v['CartAttr'] != '[]' && $v['CartAttr'] != '{}' && $v['CartAttr'] != '{"Overseas":"Ov:1"}') {
        if (!(int)$v['IsCombination'] && !(int)$v['IsOpenAttrPrice']) continue;//多规格模式和属性价格，都没有开启
        $IsError = 0;
        $prod_selected_ary = $ext_ary = array();
        $AttrAry = @str::json_data(str::attr_decode($v['CartAttr']), 'decode');
        $prod_selected_row = db::get_all('products_selected_attribute', "ProId='{$v['ProId']}' and VId>0 and IsUsed=1");
        foreach ((array)$prod_selected_row as $v2) {
            $prod_selected_ary[] = $v2['VId'];
        }
        if ((int)$v['IsCombination'] && db::get_row_count('products_selected_attribute_combination', "ProId='{$v['ProId']}'")) {//开启规格组合
            $OvId = 1;
            foreach ($AttrAry as $k2 => $v2) {
                if ($k2 == 'Overseas') {//发货地
                    $OvId = str_replace('Ov:', '', $v2);
                    (int)$c['config']['global']['Overseas'] == 0 && $OvId != 1 && $OvId = 0;//关闭海外仓功能，发货地不是China，不能购买
                } else {
                    !in_array($v2, $prod_selected_ary) && $IsError = 1;
                    $ext_ary[] = $v2;
                }
            }
            sort($ext_ary); //从小到大排序
            $Combination = '|' . implode('|', $ext_ary) . '|';
            $row = str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='{$Combination}' and OvId='{$OvId}'"));
        } else {
            foreach ((array)$AttrAry as $k2 => $v2) {
                if ($k2 == 'Overseas') continue;
                $row = str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$v['ProId']}' and Combination='|{$v2}|' and OvId=1"));
                $row && $PropertyPrice += (float)$row['Price'];//固定是加价
            }
            if ($v['BuyType'] == 3 && !$AttrAry) $row = 1;//放过组合购买
        }
        if (!$row || $IsError > 0) $error_ary["{$v['ProId']}_{$v['CId']}"] = 1;//检查此产品是否有选择购物车属性
    }
}

//收货地址
if ((int)$_SESSION['User']['UserId']) {
    //会员收货地址信息
    $address_row = str::str_code(db::get_all('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', 'a.' . $c['where']['cart'] . " and a.IsBillingAddress=0", 'a.*, c.Country, s.States as StateName', 'a.AccTime desc, a.AId desc'));
    if ((int)$_SESSION['Cart']['ShippingAddressAId']) {
        //有默认收货地址参数
        $i = 0;
        foreach ((array)$address_row as $v) {
            if ($v['AId'] == $_SESSION['Cart']['ShippingAddressAId']) {
                $address_row[0] = $v;
            } else {
                $address_row[++$i] = $v;
            }
        }
    }
} elseif ($_SESSION['Cart']['ShippingAddress']) {
    //非会员收货地址信息
    $address_ary = $_SESSION['Cart']['ShippingAddress'];
    $country_val = str::str_code(db::get_value('country', "CId='{$address_ary['CId']}'", 'Country'));
    $states_val = str::str_code(db::get_value('country_states', "SId='{$address_ary['SId']}'", 'States'));
    if ($country_val || $states_val) {
        $address_ary['Country'] = $country_val;
        $address_ary['StateName'] = $states_val;
    }
    $address_row[0] = $address_ary;
    unset($address_ary);
}
$IsStates = (int)db::get_row_count('country_states', "CId='{$address_row[0]['CId']}'", 'SId');
//付款方式
$payment_row = db::get_all('payment', "IsUsed=1 and PId!=2", '*', $c['my_order'] . 'IsOnline desc,PId asc');

$IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));

// 检测优惠
$DiscountData = cart::check_list_discounts($total_price, $iconv_total_price);

//会员优惠价 与 全场满减价 比较
$AfterPrice_0 = $AfterPrice_1 = 0;
$user_discount = 100;
if ((int)$_SESSION['User']['UserId'] && (int)$_SESSION['User']['Level']) {//会员优惠
    $user_discount = (float)db::get_value('user_level', "LId='{$_SESSION['User']['Level']}' and IsUsed=1", 'Discount');
    $user_discount = ($user_discount > 0 && $user_discount < 100) ? $user_discount : 100;
    $AfterPrice_0 = $iconv_total_price - ($iconv_total_price * ($user_discount / 100));
}
if ($cutArr['IsUsed'] == 1 && $c['time'] >= $cutArr['StartTime'] && $c['time'] <= $cutArr['EndTime']) {//全场满减
    foreach ((array)$cutArr['Data'] as $k => $v) {
        if ($total_price < $k) break;
        $AfterPrice_1 = ($cutArr['Type'] == 1 ? cart::iconv_price($v[1], 2, '', 0) : ($iconv_total_price * (100 - $v[0]) / 100));
    }
}
if ($AfterPrice_0 == $AfterPrice_1) {//当会员优惠价和全场满减价一致，默认只保留会员优惠价
    $AfterPrice_1 = 0;
}

//所有产品属性
$attribute_cart_ary = $vid_data_ary = array();
$CateIdStr = implode(' or ', $CateId_ary);
$attribute_row = str::str_code(db::get_all('products_attribute', '1' . ($CateIdStr ? " and $CateIdStr" : ''), "AttrId, Type, Name{$c['lang']}, CartAttr, ColorAttr"));
foreach ($attribute_row as $v) {
    $attribute_ary[$v['AttrId']] = array(0 => $v['Type'], 1 => $v["Name{$c['lang']}"]);
}
$AttrIdStr = implode(',', array_keys($attribute_ary));
$value_row = str::str_code(db::get_all('products_attribute_value', '1' . ($AttrIdStr ? " and AttrId in($AttrIdStr)" : ''), '*', $c['my_order'] . 'VId asc')); //属性选项
foreach ($value_row as $v) {
    $vid_data_ary[$v['AttrId']][$v['VId']] = $v["Value{$c['lang']}"];
}

if ((int)$c['config']['global']['Overseas'] == 0 || count($c['config']['Overseas']) < 2 || count($oversea_id_ary) < 2) {//关闭海外仓功能 或者 仅有一个海外仓选项
    ?>
    <style type="text/css">.information_shipping .shipping:first-child .title {
            display: none;
        }</style>
    <?php
}
$oversea_id_hidden = $oversea_id_ary;
$NotDefualtOvId = 0;
if (!in_array(1, $oversea_id_ary)) {//购物车没有默认海外仓追加隐藏选项
    $oversea_id_ary[] = 1;
    $NotDefualtOvId = 1;
}
sort($oversea_id_ary); //排列正序
$oversea_count = count($oversea_id_ary);

$o_id_ary = array();
//$_get_only=array();

//获取运输方式
    foreach ((array)$cart_row as $k=>$v) {
//         if (isset($o_id_ary[$v['ProId']]))
//         {
//             continue;
//         }
$ship_arr=array();
        $o_ary = get_shipping_methods_by_proId($address_row[0]["CId"], $address_row[0]["AId"], $v['ProId'],$v['Qty']);

//     file_put_contents("t1.txt", "o_ary=>" . var_export($o_ary, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        $p_s_all = db::get_all("product_shipping", "products_id='" . $v["ProId"] . "'");
//     file_put_contents("t1.txt", "p_s_all=>" . var_export($p_s_all, true) . PHP_EOL . PHP_EOL, FILE_APPEND);

        foreach ($o_ary[1] as $oir) {
            foreach ($p_s_all as $ps_item) {
                if ($oir["SId"] == $ps_item["shipping_id"]) {
                    $ship_arr[]= $oir;
                    break;
                }
            }
        }
        $o_id_ary[$v['ProId']][$k]=$ship_arr;
    }

//     var_dump(json_encode($o_id_ary));


// file_put_contents("t1.txt","p_s_all".var_export($p_s_all,true),FILE_APPEND);
// get_shipping_methods_by_proId($address_row[0]["CId"],$address_row[0]["AId"],$proid);

    function get_shipping_methods_by_proId($CId, $AId, $ProId,$Qty,$Attr="")
{
    $_POST = array('CId' => $CId,
        'AId' => $AId,
        'ProId' => $ProId,
        'Qty'=>$Qty,
        'Attr'=>$Attr
    );
    $info = get_shipping_methods();

//     file_put_contents("t1.txt", var_export($info, true), FILE_APPEND);
    return $info;

}

function get_shipping_methods()
{    //get shipping methods
    global $c;
    @extract($_POST, EXTR_PREFIX_ALL, 'p');
//     file_put_contents("t.txt", var_export($_POST, true), FILE_APPEND);
    $p_Type="shipping_cost";
    $p_CId = (int)$p_CId;
    $p_AId = (int)$p_AId;
    $p_ProId = (int)$p_ProId;
    $p_Qty = (int)$p_Qty;
    $info = array();
    $IsFreeShipping = $ProductPrice = 0;
    $shipping_template = @in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0;
    $provincial_freight = @in_array('provincial_freight', (array)$c['plugins']['Used']) ? 1 : 0; //开启省份运费
    $pro_info_ary = array();

    if ($p_Type == 'order') {
        $order_row = db::get_one('orders', "OId='{$p_OId}'");
        $sProdInfo = db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, p.IsFreeShipping, p.TId');
        foreach ($sProdInfo as $v) {
            $price = ($v['Price'] + $v['PropertyPrice']) * $v['Qty'] * ($v['Discount'] < 100 ? $v['Discount'] / 100 : 1);
            $ProductPrice += $price;
            if ($shipping_template) {
                if (!$pro_info_ary[$v['CId']]) {
                    $pro_info_ary[$v['CId']] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0, 'OvId' => $v['OvId']);
                }
                $pro_info_ary[$v['CId']]['tWeight'] = ($v['Weight'] * $v['Qty']);
                $pro_info_ary[$v['CId']]['tVolume'] = ($v['Volume'] * $v['Qty']);
                $pro_info_ary[$v['CId']]['tQty'] = $v['Qty'];
                $pro_info_ary[$v['CId']]['Price'] = $price;
                $pro_info_ary[$v['CId']]['TId'] = $v['TId'];
                if ((int)$v['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$v['CId']]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$v['CId']]['Weight'] = ($v['Weight'] * $v['Qty']);
                    $pro_info_ary[$v['CId']]['Volume'] = ($v['Volume'] * $v['Qty']);
                    $pro_info_ary[$v['CId']]['Qty'] = $v['Qty'];
                }
            } else {
                if (!$pro_info_ary[$v['OvId']]) {
                    $pro_info_ary[$v['OvId']] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0);
                }
                $pro_info_ary[$v['OvId']]['tWeight'] += ($v['Weight'] * $v['Qty']);
                $pro_info_ary[$v['OvId']]['tVolume'] += ($v['Volume'] * $v['Qty']);
                $pro_info_ary[$v['OvId']]['tQty'] += $v['Qty'];
                $pro_info_ary[$v['OvId']]['Price'] += $price;
                if ((int)$v['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$v['OvId']]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$v['OvId']]['Weight'] += ($v['Weight'] * $v['Qty']);
                    $pro_info_ary[$v['OvId']]['Volume'] += ($v['Volume'] * $v['Qty']);
                    $pro_info_ary[$v['OvId']]['Qty'] += $v['Qty'];
                }
            }
        }
    } elseif ($p_Type == 'shipping_cost') {
        if ($p_Attr) {//产品属性
            $Attr = str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
            ksort($Attr);
        }
        $proInfo = db::get_one('products', "ProId='$p_ProId'");
        $Weight = $proInfo['Weight'];//产品默认重量
        $volume_ary = $proInfo['Cubage'] ? @explode(',', $proInfo['Cubage']) : array(0, 0, 0);
        $Volume = $volume_ary[0] * $volume_ary[1] * $volume_ary[2];
        $Volume = sprintf('%.f', $Volume);//防止数额太大，程序自动转成科学计数法
        if ((int)$proInfo['IsVolumeWeight']) {//体积重
            $VolumeWeight = ($Volume * 1000000) / 5000;//先把立方米转成立方厘米，再除以5000
            $VolumeWeight > $Weight && $Weight = $VolumeWeight;
        }
        $StartFrom = (int)$proInfo['MOQ'] > 0 ? (int)$proInfo['MOQ'] : 1;    //起订量
        $p_Qty < $StartFrom && $p_Qty = $StartFrom;    //小于起订量
        $CurPrice = cart::products_add_to_cart_price($proInfo, $p_Qty);
        $PropertyPrice = 0;
        //产品属性
        $AttrData = cart::get_product_attribute(array(
            'Type' => 0,//不用获取产品属性名称
            'ProId' => $p_ProId,
            'Price' => $CurPrice,
            'Attr' => $Attr,
            'IsCombination' => $proInfo['IsCombination'],
            'IsAttrPrice' => $proInfo['IsOpenAttrPrice'],
            'Weight' => $Weight
        ));
        $CurPrice = $AttrData['Price'];        //产品单价
        $PropertyPrice = $AttrData['PropertyPrice'];//产品属性价格
        $combinatin_ary = $AttrData['Combinatin'];    //产品属性的数据
        $OvId = $AttrData['OvId'];        //发货地ID
        $Weight = $AttrData['Weight'];        //产品重量
        $Attr = $AttrData['Attr'];        //产品属性数据
        //秒杀产品
        if ($p_proType == 2) {
            $seckill_row = str::str_code(db::get_one('sales_seckill', "SId='$p_SId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
            if ($seckill_row) {
                $CurPrice = $seckill_row['Price'];
            }
        }
        //产品数据
        if ($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime'] < $c['time'] && $c['time'] < $proInfo['EndTime']) {
            $CurPrice = $CurPrice * ($proInfo['PromotionDiscount'] / 100);
        }
        $total_price = ($CurPrice + $PropertyPrice) * $p_Qty;
        $IsFreeShipping = (int)$proInfo['IsFreeShipping'];
        $total_volume = $Volume * $p_Qty;
        $total_weight = $Weight * $p_Qty;
        $s_total_weight = $total_weight;
        $s_total_volume = $total_volume;
        //产品的海外仓数据
        $pro_info_ary[$OvId]['Weight'] = $pro_info_ary[$OvId]['tWeight'] = $total_weight;
        $pro_info_ary[$OvId]['Volume'] = $pro_info_ary[$OvId]['tVolume'] = $total_volume;
        $pro_info_ary[$OvId]['Price'] = $total_price;
        $pro_info_ary[$OvId]['Qty'] = $p_Qty;
        $pro_info_ary[$OvId]['TId'] = (int)$proInfo['TId'];
        $ProductPrice += $total_price;
        if ($IsFreeShipping == 1) {
            $pro_info_ary[$OvId]['Weight'] = $pro_info_ary[$OvId]['Volume'] = 0;
        }
    } else {
        $CIdStr = '';
        if ($p_order_cid) {
            $in_where = str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
            $CIdStr = " and CId in({$in_where})";
        }
//         file_put_contents("t.txt", "p_order_cid" . var_export($p_order_cid, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        $cartInfo = db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.' . $c['where']['cart'] . $CIdStr, 'p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr', 's.CId desc');
        $IsFreeShipping = 0;
        $total_weight = $total_volume = 0;
//         file_put_contents("t.txt", "cartInfo" . var_export($cartInfo, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
        foreach ($cartInfo as $val) {
            $CartId = (int)$val['CId'];
            $OvId = 1;
            $CartAttr = str::json_data(stripslashes($val['CartAttr']), 'decode');
            if (count($CartAttr)) { //include attribute values
                foreach ((array)$CartAttr as $k => $v) {
                    if ($k == 'Overseas') { //发货地
                        $OvId = str_replace('Ov:', '', $v);
                        !$OvId && $OvId = 1;//丢失发货地，自动默认China
                    }
                }
            }
            $price = ($val['Price'] + $val['PropertyPrice']) * $val['Qty'] * ($val['Discount'] < 100 ? $val['Discount'] / 100 : 1);
            if ($shipping_template) {
                if (!$pro_info_ary[$CartId]) {
                    $pro_info_ary[$CartId] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0, 'OvId' => $OvId);
                }
                $pro_info_ary[$CartId]['tWeight'] = ($val['CartWeight'] * $val['Qty']);
                $pro_info_ary[$CartId]['tVolume'] = ($val['Volume'] * $val['Qty']);
                $pro_info_ary[$CartId]['tQty'] = $val['Qty'];
                $pro_info_ary[$CartId]['Price'] = $price;
                $pro_info_ary[$CartId]['TId'] = (int)$val['TId'];
                if ((int)$val['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$CartId]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$CartId]['Weight'] = ($val['CartWeight'] * $val['Qty']);
                    $pro_info_ary[$CartId]['Volume'] = ($val['Volume'] * $val['Qty']);
                    $pro_info_ary[$CartId]['Qty'] = $val['Qty'];
                }
            } else {
                if (!$pro_info_ary[$OvId]) {
                    $pro_info_ary[$OvId] = array('Weight' => 0, 'Volume' => 0, 'tWeight' => 0, 'tVolume' => 0, 'Price' => 0, 'IsFreeShipping' => 0);
                }
                $pro_info_ary[$OvId]['tWeight'] += ($val['CartWeight'] * $val['Qty']);
                $pro_info_ary[$OvId]['tVolume'] += ($val['Volume'] * $val['Qty']);
                $pro_info_ary[$OvId]['tQty'] += $val['Qty'];
                $pro_info_ary[$OvId]['Price'] += $price;
                if ((int)$val['IsFreeShipping'] == 1) {//免运费
                    $pro_info_ary[$OvId]['IsFreeShipping'] = 1; //其中有免运费
                } else {
                    $pro_info_ary[$OvId]['Weight'] += ($val['CartWeight'] * $val['Qty']);
                    $pro_info_ary[$OvId]['Volume'] += ($val['Volume'] * $val['Qty']);
                    $pro_info_ary[$OvId]['Qty'] += $val['Qty'];
                }
            }
            $total_weight += ($val['CartWeight'] * $val['Qty']);
            $total_volume += ($val['Volume'] * $val['Qty']);
            $ProductPrice += $price;
        }
        /*
         //产品包装重量  //没有多少客户用 4.0先注释
         $cartProAry=cart::cart_product_weight($CIdStr, 1);
         foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
         foreach((array)$v as $k2=>$v2){//$k2是ProId
         $pro_info_ary[$k]['tWeight']+=$v2;
         }
         }
         foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
         foreach((array)$v as $k2=>$v2){//$k2是ProId
         $pro_info_ary[$k]['Weight']+=$v2;
         }
         }*/
        $total_weight = $pro_info_ary[$OvId]['Weight'];
        $total_volume = $pro_info_ary[$OvId]['tVolume'];
    }
//     file_put_contents("t.txt", 'pro_info_ary' . var_export($pro_info_ary, true), FILE_APPEND);
    ksort($pro_info_ary); //排列正序
    $shipping_cfg = db::get_one('shipping_config', 'Id="1"');
    $weight = @ceil($total_weight);

    if ($_SESSION['User']['UserId']) {
        if ($p_AId) {
            $address_aid = $p_AId;
        } else {
            $address_aid = (int)$_SESSION['Cart']['ShippingAddressAId'];
        }
        if ($address_aid > 0) $StatesSId = (int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
    } else {
        $StatesSId = (int)$_SESSION['Cart']['ShippingAddress']['Province'];
    }
    $StatesSId = $p_StatesSId ? (int)$p_StatesSId : $StatesSId;
    $provincial_freight && $p_CId == 226 && $StatesSId && $area_where = " and states like '%|{$StatesSId}|%'";
    $IsInsurance = str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
    $row = db::get_all('shipping_area a left join shipping s on a.SId=s.SId', "a.AId in(select AId from shipping_country where CId='{$p_CId}' {$area_where})", 's.Express, s.Logo, s.IsWeightArea, s.WeightArea, s.ExtWeightArea, s.VolumeArea, s.IsUsed, s.IsAPI, s.FirstWeight, s.ExtWeight, s.StartWeight, s.MinWeight, s.MaxWeight, s.MinVolume, s.MaxVolume, s.FirstMinQty, s.FirstMaxQty, s.ExtQty, s.WeightType, s.TId, a.*', 'if(s.MyOrder>0, s.MyOrder, 100000) asc, a.SId asc, a.AId asc');
    //添加获取清关费费率 所有id
    $qg_rows=array();
    foreach ((array)$row as $value) {
        $r=array();
        $r["SId"]=$value["SId"];
        $r["AffixPrice_rate"]=$value["AffixPrice_rate"]?$value["AffixPrice_rate"]:0;
        $qg_rows[]=$r;
    }
    //-------------------------------------
//     file_put_contents("t4.txt","row_qg_1=>".var_export($qg_rows,true).PHP_EOL.PHP_EOL,FILE_APPEND);
//     file_put_contents("t4.txt","row=>".var_export($row,true).PHP_EOL.PHP_EOL,FILE_APPEND);
    $shipping_row = db::get_all('shipping', '1', 'SId, TId');
    $row_ary = $shipping_tid_ary = array();
    foreach ((array)$row as $v) {
        !$row_ary[$v['SId']] && $row_ary[$v['SId']] = array('info' => $v, 'overseas' => array());
        $row_ary[$v['SId']]['overseas'][$v['OvId']] = $v;
    }
    $shipping_template && $DefaultTId = db::get_value('shipping_template', 'IsDefault=1', 'TId');
    foreach ((array)$shipping_row as $v) {
        if ($shipping_template) $shipping_tid_ary[$v['TId']][] = $v['SId'];
    }
//     file_put_contents("t4.txt","row1=>".var_export($row,true).PHP_EOL.PHP_EOL,FILE_APPEND);
    unset($row);
//     file_put_contents("t4.txt","row2=>".var_export($row,true).PHP_EOL.PHP_EOL,FILE_APPEND);

    foreach ((array)$row_ary as $key => $val) {
        $row = $val['info'];
        $isOvId = 0;
        foreach ((array)$pro_info_ary as $k => $v) {
            if ($shipping_template && $p_Type != 'shipping_cost') {
                $val['overseas'][$v['OvId']] && $isOvId += 1;
            } else {
                $val['overseas'][$k] && $isOvId += 1;
            }
        }//循环产品数据

        if ($isOvId == 0 && $pro_info_ary['1']) {
            $info[1][] = array('SId' => '', 'Name' => '', 'Brief' => '', 'IsAPI' => '', 'type' => '', 'ShippingPrice' => '-1');
            continue;
        }
        /************************************** 新增折后之后计算运费 开始**********************************/
        $_total_price = orders::orders_discount_total_price($ProductPrice, $_SESSION['Cart']['Coupon'], $_SESSION['User']['UserId'], $p_order_cid, $p_ProId);
        $pro_info_ary = orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
        /************************************** 新增折后之后计算运费 结束**********************************/
        //循环产品数据 Start
        foreach ((array)$pro_info_ary as $k => $v) {
            $overseas = $val['overseas'][$k];
            $shipping_template && $p_Type != 'shipping_cost' && $overseas = $val['overseas'][$v['OvId']];
            if ($shipping_template && (!in_array($key, (array)$shipping_tid_ary[$v['TId']]) || ($DefaultTId && !$shipping_tid_ary[$v['TId']] && !in_array($key, (array)$shipping_tid_ary[$DefaultTId])))) continue;
            $open = 0;//默认不通过
            if (in_array($row['IsWeightArea'], array(0, 1, 2)) && ((float)$row['MaxWeight'] ? ($v['tWeight'] >= $row['MinWeight'] && $v['tWeight'] <= $row['MaxWeight']) : ($v['tWeight'] >= $row['MinWeight']))) {//重量限制
                $open = 1;
            } elseif ($row['IsWeightArea'] == 4 && ($v['tWeight'] >= $row['MinWeight'] || $v['tVolume'] >= $row['MinVolume'])) {//重量限制+体积限制
                $open = 1;
            } elseif ($row['IsWeightArea'] == 3) {//按数量计算，直接不限制
                $open = 1;
            }
            if ($overseas && (int)$row['IsUsed'] == 1 && $open == 1) {
                $sv = array(
                    'SId' => $row['SId'],
                    'Name' => $overseas['Express'],
                    'Logo' => ($overseas['Logo'] && is_file($c['root_path'] . $overseas['Logo'])) ? $overseas['Logo'] : '',
                    'Brief' => $overseas['Brief'],
                    'IsAPI' => $overseas['IsAPI'],
                    'type' => '',
                    'weight' => $v['Weight']
                );
                if ($IsFreeShipping || ($v['IsFreeShipping'] == 1 && $v['Weight'] == 0) || ((int)$c['config']['products_show']['Config']['freeshipping'] && $v['Weight'] == 0) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingPrice'] > 0 && $v['Price'] >= $overseas['FreeShippingPrice']) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingWeight'] > 0 && $v['Weight'] < $overseas['FreeShippingWeight']) || ($overseas['IsFreeShipping'] == 1 && $overseas['FreeShippingPrice'] == 0 && $overseas['FreeShippingWeight'] == 0)) {
                    $shipping_price = 0;
                } else {
                    $shipping_price = 0;
                    if ($overseas['IsWeightArea'] == 1 || ($overseas['IsWeightArea'] == 2 && $v['Weight'] >= $overseas['StartWeight'])) {
                        //重量区间 重量混合
                        $WeightArea = str::json_data($overseas['WeightArea'], 'decode');
                        $WeightAreaPrice = str::json_data($overseas['WeightAreaPrice'], 'decode');
                        $areaCount = count($WeightArea) - 1;
                        foreach ((array)$WeightArea as $k2 => $v2) {
                            if ($k2 <= $areaCount && (($WeightArea[$k2 + 1] && $v['Weight'] < $WeightArea[$k2 + 1]) || (!$WeightArea[$k2 + 1] && $v['Weight'] >= $v2))) {
                                if ($overseas['WeightType'] == 1) {//按每KG计算
                                    $shipping_price = $WeightAreaPrice[$k2] * $v['Weight'];
                                } else {//按整价计算
                                    $shipping_price = $WeightAreaPrice[$k2];
                                }
                                break;
                            }
                        }
                        //$v['Weight']>$WeightArea[$areaCount] && $shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
                    } elseif ($overseas['IsWeightArea'] == 3) {
                        //按数量
                        $shipping_price = $overseas['FirstQtyPrice'];//先收取首重费用
                        $ExtQtyValue = $v['Qty'] > $overseas['FirstMaxQty'] ? $v['Qty'] - $overseas['FirstMaxQty'] : 0;//超出的数量
                        if ($ExtQtyValue) {//续重
                            $shipping_price += (float)(@ceil($ExtQtyValue / $overseas['ExtQty']) * $overseas['ExtQtyPrice']);
                        }
                    } elseif ($overseas['IsWeightArea'] == 4) {
                        //重量体积混合计算
                        $weight_shipping_price = $volume_shipping_price = 0;
                        if ($v['Weight'] >= $overseas['MinWeight']) {//重量
                            $WeightArea = str::json_data($overseas['WeightArea'], 'decode');
                            $WeightAreaPrice = str::json_data($overseas['WeightAreaPrice'], 'decode');
                            $areaCount = count($WeightArea) - 1;
                            foreach ((array)$WeightArea as $k2 => $v2) {
                                if ($k2 <= $areaCount && (($WeightArea[$k2 + 1] && $v['Weight'] < $WeightArea[$k2 + 1]) || (!$WeightArea[$k2 + 1] && $v['Weight'] >= $v2))) {
                                    if ($overseas['WeightType'] == 1) {//按每KG计算
                                        $weight_shipping_price = $WeightAreaPrice[$k2] * $v['Weight'];
                                    } else {//按整价计算
                                        $weight_shipping_price = $WeightAreaPrice[$k2];
                                    }
                                    break;
                                }
                            }
                            $overseas['WeightType'] == 1 && $v['Weight'] > $WeightArea[$areaCount] && $weight_shipping_price = $WeightAreaPrice[$areaCount] * $v['Weight'];
                        }
                        if ($v['Volume'] >= $overseas['MinVolume']) {//体积
                            $VolumeArea = str::json_data($overseas['VolumeArea'], 'decode');
                            $VolumeAreaPrice = str::json_data($overseas['VolumeAreaPrice'], 'decode');
                            $areaCount = count($VolumeArea) - 1;
                            foreach ((array)$VolumeArea as $k2 => $v2) {
                                if ($k2 <= $areaCount && (($VolumeArea[$k2 + 1] && $v['Volume'] < $VolumeArea[$k2 + 1]) || (!$VolumeArea[$k2 + 1] && $v['Volume'] >= $v2))) {
                                    $volume_shipping_price = $VolumeAreaPrice[$k2] * $v['Volume'];
                                    break;
                                }
                            }
                            $v['Volume'] > $VolumeArea[$areaCount] && $volume_shipping_price = $VolumeAreaPrice[$areaCount] * $v['Volume'];
                        }
                        $shipping_price = max($weight_shipping_price, $volume_shipping_price);
                    } else {
                        //首重续重
                        $ExtWeightArea = str::json_data($overseas['ExtWeightArea'], 'decode');
                        $ExtWeightAreaPrice = str::json_data($overseas['ExtWeightAreaPrice'], 'decode');
                        $areaCount = count($ExtWeightArea) - 1;
                        $ExtWeightValue = $v['Weight'] > $overseas['FirstWeight'] ? $v['Weight'] - $overseas['FirstWeight'] : 0;//超出的重量
                        if ($areaCount > 0) {
                            $shipping_price = $overseas['FirstPrice'];//先收取首重费用
                            foreach ((array)$ExtWeightArea as $k2 => $v2) {
                                if ($v['Weight'] > $v2 && $ExtWeightArea[$k2 + 1]) {
                                    $ext = $v['Weight'] > $ExtWeightArea[$k2 + 1] ? ($ExtWeightArea[$k2 + 1] - $v2) : ($v['Weight'] - $v2);
                                    $shipping_price += (float)(@ceil(sprintf('%01.4f', $ext / $overseas['ExtWeight'])) * $ExtWeightAreaPrice[$k2]);
                                } elseif ($v['Weight'] > $v2 && !$ExtWeightArea[$k2 + 1]) {//达到以上费用
                                    $ext = $v['Weight'] - $v2;
                                    $shipping_price += (float)(@ceil(sprintf('%01.4f', $ext / $overseas['ExtWeight'])) * $ExtWeightAreaPrice[$k2]);
                                }
                            }
                        } else {
                            $ExtWeightCount = sprintf('%01.4f', $ExtWeightValue / $overseas['ExtWeight']);
                            $ExtWeightCountdecimal = floor($ExtWeightCount);
                            if (strstr($ExtWeightCount, '.') != false && ($ExtWeightCount - $ExtWeightCountdecimal) > 0) {
                                $ExtWeightCount = @ceil($ExtWeightCount);
                            }
                            $shipping_price = (float)($ExtWeightCount * $ExtWeightAreaPrice[0] + $overseas['FirstPrice']);
                        }
                    }
                    if ($overseas['AffixPrice']) {//附加费用
                        $shipping_price += $overseas['AffixPrice'];
                    }
                }
                $sv['ShippingPrice'] = cart::iconv_price($shipping_price, 2, '', 0);
                $sv['InsurancePrice'] = cart::get_insurance_price_by_price($v['Price']);
                $info[$k][] = $sv;
            }
        }
        //循环产品数据 End
    }
//     file_put_contents("t4.txt","row3=>".var_export($row,true).PHP_EOL.PHP_EOL,FILE_APPEND);
//     file_put_contents("t4.txt","row3_info=>".var_export($info,true).PHP_EOL.PHP_EOL,FILE_APPEND);

    if ($info) {
        $info_ary = array();
        foreach ((array)$info as $k => $v) {
            $sort_ary = $price_ary = array();
            foreach ((array)$v as $k2 => $v2) { //
                $price_ary[$v2['ShippingPrice']][] = $k2;
            }
            ksort($price_ary);
            foreach ((array)$price_ary as $k2 => $v2) {
                foreach ((array)$v2 as $k3 => $v3) {
                    $info_ary[$k][] = $info[$k][$v3];
                }
            }
        }
        //添加清关费到返回值
        foreach ((array)$info_ary as $k=>&$v) {
            foreach ((array)$v as $k2=>&$v2) { //
                foreach ($qg_rows as $q_k=>$q_v)
                {
                    if ($v2["SId"]==$q_v['SId'])
                    {
                        $info_ary[$k][$k2]["AffixPrice_rate"]=$q_v["AffixPrice_rate"];
                        break;
                    }
                }
            }
        }
        //-------------
        return $info_ary;
        ly200::e_json(array('Symbol' => cart::iconv_price(0, 1), 'info' => $info_ary, 'IsInsurance' => $IsInsurance, 'total_weight' => $total_weight, 'shipping_template' => (int)$shipping_template), 1);
    } else {
        return false;
    }
}

?>
<script type="text/javascript">
    var address_perfect =<?=(!$address_row || ($address_row && (!$address_row[0]['FirstName'] || !$address_row[0]['LastName'] || !$address_row[0]['AddressLine1'] || !$address_row[0]['City'] || !$address_row[0]['CId'] || ($IsStates > 0 && !$address_row[0]['SId']) || !$address_row[0]['ZipCode'] || !$address_row[0]['PhoneNumber']))) ? 1 : 0;?>;
    var address_perfect_aid =<?=$address_row ? (int)$address_row[0]['AId'] : 0;?>;
    $(document).ready(function () {
        <?=$a == 'buynow' ? 'cart_obj.list_init();' : '';?>
        cart_obj.checkout_init();
        user_obj.sign_in_init();
        <?php
        if ($_SESSION['Cart']['ShippingAddress']) {
            echo 'cart_obj.cart_init.checkout_no_login(' . str::json_data($_SESSION['Cart']['ShippingAddress']) . ');';
        }
        if ($c['NewFunVersion'] >= 4) {//新用户版本 和 Paypal支付
            echo 'cart_obj.paypal_init();';
        }?>
    });
    $('html').loginOrVisitors('<?=$_SERVER['REQUEST_URI'];?>', 0, function () {
        ueeshop_config['_login'] = 1;
        return false;
    });
</script>
<style type="text/css">
    .information_shipping .shipping:hover .icon_shipping_title, .information_shipping .current .icon_shipping_title, .information_payment .icon_shipping_title {
        background-color: <?=$style_data['BuyNowBgColor'];?>;
    }

    .information_shipping .list li {
        width: 399px;
    }

    .information_shipping .list {
        display:;
    }

    #orderFormSubmit {
        width: 90%;
        height: 50px;
        line-height: 20px;
        padding-top: 6px;
        text-align: center;
    }
</style>
<div id="lib_cart" class="checkout_container<?= $a == 'buynow' ? ' buynow_content' : ''; ?>">

    <?php include('include/header.php'); ?>
    <div class="checkout_content">
        <div class="checkout_error_tips hide" data-country=""
             data-email="<?= $c['config']['global']['AdminEmail']; ?>"></div>
        <?php if ((int)$_SESSION['User']['UserId'] == 0) { ?>
            <div class="information_box information_customer">
                <div class="box_title"><?= $c['lang_pack']['cart']['customerInfo']; ?></div>
                <div class="box_content">
                    <label class="input_box">
                        <span class="input_box_label"><?= $c['lang_pack']['mobile']['enter_email']; ?></span>
                        <input type="text" class="input_box_txt elmbBlur" name="Email"
                               placeholder="<?= $c['lang_pack']['mobile']['enter_email']; ?>" maxlength="200"/>
                    </label>
                    <p class="error"></p>
                    <div class="information_login"><?= $c['lang_pack']['cart']['already']; ?> <a href="javascript:;"
                                                                                                 class="SignInButton btn_signin"><?= $c['lang_pack']['cart']['login']; ?></a>
                    </div>
                    <input type="hidden" name="jumpUrl" value="<?= $_SERVER['REQUEST_URI']; ?>"/>
                </div>
            </div>
        <?php } ?>
        <div class="information_box information_address">
            <div class="box_title"><?= $c['lang_pack']['cart']['address']; ?></div>
            <div class="box_content">
                <?php if ((int)$_SESSION['User']['UserId']) { ?>
                    <div class="address_button clearfix">
                        <a href="javascript:;" class="btn_address_add"
                           id="addAddress"><?= $c['lang_pack']['cart']['add']; ?></a><i>|</i><a href="javascript:;"
                                                                                                class="btn_address_more"
                                                                                                id="moreAddress"><?= $c['lang_pack']['cart']['moreAddress']; ?></a>
                    </div>
                <?php } ?>
                <div class="address_default item clearfix"></div>
                <div class="address_list clearfix">
                    <?php
                    if (count($address_row) > 0) {
                        foreach ((array)$address_row as $k => $v) {
                            ?>
                            <div class="item<?= $k % 2 == 0 ? ' odd' : ''; ?>">
                                <input type="radio" name="shipping_address_id" id="address_<?= $v['AId']; ?>"
                                       value="<?= $v['AId']; ?>" data-cid="<?= $v['CId']; ?>"
                                       data-country-name="<?= $v['Country']; ?>"/>
                                <p class="clearfix"><strong><?= $v['FirstName'] . ' ' . $v['LastName']; ?></strong><a
                                            href="javascript:;"
                                            class="edit_address_info"><?= $c['lang_pack']['cart']['edit']; ?></a></p>
                                <p class="address_line"><?= $v['AddressLine1'] . ' ' . ($v['AddressLine2'] ? $v['AddressLine2'] . ' ' : ''); ?></p>
                                <p><?= $v['City'] . ' ' . ($v['StateName'] ? $v['StateName'] : $v['State']) . ' ' . $v['Country'] . ' (' . $v['ZipCode'] . ')'; ?></p>
                                <p>+<?= $v['CountryCode'] . ' ' . $v['PhoneNumber']; ?></p>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div id="addressInfo" style="display:none;"></div>
                <div id="ShipAddrFrom" style="display:none;"><?php include('include/shippingAddress.php'); ?></div>
            </div>
        </div>
        <div class="information_box information_shipping" style="display: none;">
            <div class="box_title<?= $oversea_count == 1 ? ' no_border' : ''; ?>"><?= $c['lang_pack']['cart']['shipmethod']; ?></div>
            <div class="box_content">
                <?php foreach ($oversea_id_ary as $k => $v) { ?>
                    <div class="shipping<?= ($NotDefualtOvId && (int)$v == 1) ? ' hide' : ''; ?>" data-id="<?= $v; ?>">
                        <div class="title">
                            <strong><?= $c['lang_pack']['products']['shipsFrom'] . ' ' . $c['config']['Overseas'][$v]['Name' . $c['lang']]; ?></strong>
                            <div class="shipping_info"><span class="name"></span><span class="price"></span></div>
                            <i class="icon_shipping_title"></i>
                        </div>
                        <div class="list">
                            <ul class="shipping_method_list clearfix"></ul>
                            <?php if ($IsInsurance) { ?>
                                <div class="insurance">
                                    <span class="name"><?= $c['lang_pack']['cart']['insurance']; ?>:</span>
                                    <input type="checkbox" name="_shipping_method_insurance" class="shipping_insurance"
                                           value="1" checked/>
                                    <label for="shipping_insurance"><?= $c['lang_pack']['cart']['add_insur']; ?></label>
                                    <a href="javascript:;" class="delivery_ins"
                                       content="<?= $c['lang_pack']['cart']['tips_insur'] ?>"><?= $c['lang_pack']['cart']['why_insur']; ?></a>
                                    <span class="price"><?= $_SESSION['Currency']['Symbol']; ?><em></em></span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="tips"><?= $c['lang_pack']['cart']['arrive']; ?></div>
                <div class="editor_txt"><?= str::str_code($c['config']['global']['ArrivalInfo']['ArrivalInfo' . $c['lang']], 'htmlspecialchars_decode'); ?></div>
            </div>
        </div>
        <div class="information_box information_payment">
            <div class="box_title"><?= $c['lang_pack']['cart']['paymethod']; ?></div>
            <div class="box_content">
                <?php
                $payment_count = count($payment_row);
                $pages = ceil($payment_count / 7);
                for ($i = 0; $i < $pages; ++$i) {
                    ?>
                    <div class="payment_list clearfix" style="display:<?= $i == 0 ? 'block' : 'none'; ?>;">
                        <?php
                        for ($j = $i * 7; $j < ($i + 1) * 7; ++$j) {
                            if ($j >= $payment_count) break;
                            ?>
                            <div class="payment_row" value="<?= $payment_row[$j]['PId']; ?>"
                                 min="<?= cart::iconv_price($payment_row[$j]['MinPrice'], 2, '', 0); ?>"
                                 max="<?= cart::iconv_price($payment_row[$j]['MaxPrice'], 2, '', 0); ?>"
                                 method="<?= $payment_row[$j]['Method']; ?>">
                                <div class="check">&nbsp;<input name="PId" type="radio"/></div>
                                <div class="img"><img src="<?= $payment_row[$j]['LogoPath']; ?>"
                                                      alt="<?= $payment_row[$j]['Name' . $c['lang']]; ?>"><span></span>
                                </div>
                                <em class="icon_dot"></em>
                                <div class="clear"></div>
                            </div>
                        <?php } ?>
                        <?php if ($i == 0 && $payment_count > 7) { ?><i class="icon_shipping_title"></i><?php } ?>
                    </div>
                    <div class="payment_contents clearfix" style="display:<?= $i == 0 ? 'block' : 'none'; ?>;">
                        <?php
                        for ($j = $i * 7; $j < ($i + 1) * 7; ++$j) {
                            if ($j >= $payment_count) break;
                            ?>
                            <div class="payment_note" data-id="<?= $payment_row[$j]['PId']; ?>"
                                 data-fee="<?= $payment_row[$j]['AdditionalFee']; ?>"
                                 data-affix="<?= cart::iconv_price($payment_row[$j]['AffixPrice'], 2, '', 0); ?>">
                                <div class="name"><?= $payment_row[$j]['Name' . $c['lang']]; ?></div>
                                <?php if ($payment_row[$j]['Description' . $c['lang']]) { ?>
                                    <div class="ext_txt editor_txt"><?= $payment_row[$j]['Description' . $c['lang']] ?></div><?php } ?>
                                <?php if($payment_row[$j]['PId'] == 11){ ?>
                                    <br>
                                    <!--<span>--><?//=$c['lang_pack']['user']['balance']; ?><!----><?//=$balance; ?><!--</span>-->
                                    <span style="font-weight: bold;"><?=$c['lang_pack']['user']['yourbalance']; ?>&nbsp;&nbsp;<?=$balance; ?></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="information_box information_products">
            <div class="box_title"><?= $c['lang_pack']['cart']['products']; ?></div>
            <div class="box_content information_product cartFrom">
                <table class="itemFrom" id="tb_prod">
                    <thead>
                    <tr>
                        <th class="item_select item_header hide">
                            <em class="btn_checkbox FontBgColor<?= !count($error_ary) ? ' current' : ''; ?>"></em>
                            <input type="checkbox" name="select_all" value=""
                                   class="va_m"<?= !count($error_ary) ? ' checked' : ''; ?> />
                        </th>
                        <th class="item_product item_header"><?= $c['lang_pack']['cart']['item']; ?></th>
                        <th class="item_price item_header"><?= $c['lang_pack']['cart']['price']; ?></th>
                        <th class="item_quantity item_header"><?= $c['lang_pack']['cart']['qty']; ?></th>
                        <th class="item_operate item_header"><?= $c['lang_pack']['cart']['amount']; ?></th>
                        <?php if (@in_array('shipping_template', (array)$c['plugins']['Used'])) { ?>
                            <th class="item_shipping item_header"><?= $c['lang_pack']['cart']['shipmethod']; ?></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total_price = $s_total_price = $quantity = 0;
                    $deposit_ot_total_price = 0;
                    $cart_attr = $cart_attr_data = $cart_attr_value = array();
                    foreach ((array)$cart_row as $cr_key => $v) {
                        $is_error = $error_ary["{$v['ProId']}_{$v['CId']}"] != '' ? 1 : 0;
                        if ($is_error == 1) continue;
                        if ($v['BuyType'] == 4) {
                            //组合促销
                            $package_row = str::str_code(db::get_one('sales_package', "PId='{$v['KeyId']}'"));
                            !$package_row && $is_error = 1;
                            // $attr=array();
                            // $v['Property']!='' && $attr=str::json_data($v['Property'], 'decode');
                            $v['Property'] != '' && $attr = ly200::get_order_property($v['Property']);
                            $products_row = str::str_code(db::get_all('products', "ProId='{$package_row['ProId']}'"));
                            $pro_where = str_replace('|', ',', substr($package_row['PackageProId'], 1, -1));
                            $pro_where == '' && $pro_where = 0;
                            $products_row = array_merge($products_row, str::str_code(db::get_all('products', "SoldOut!=1 and ProId in($pro_where)")));
                            $data_ary = str::json_data($package_row['Data'], 'decode');
                        } else {
                            //普通产品
                            // $attr=array();
                            // $v['Property']!='' && $attr=str::json_data($v['Property'], 'decode');
                            $v['Property'] != '' && $attr = ly200::get_order_property($v['Property']);
                            $attr_len = count($attr);
                            $oversea_len = db::get_row_count('products_selected_attribute', "ProId='{$v['ProId']}' and AttrId=0 and VId=0 and OvId>1 and IsUsed=1");
                            (int)$c['config']['global']['Overseas'] == 0 && $attr_len == 1 && $attr['Overseas'] && $attr_len -= 1;
                            $img = ly200::get_size_img($v['PicPath'], '240x240');
                            if ($v['BuyType'] == 1) {//团购产品
                                $v['TId'] = $v['KeyId'];
                                $url = ly200::get_url($v, 'tuan');
                            } else {
                                $url = ly200::get_url($v, 'products');
                            }
                        }
                        $price = $v['Price'] + $v['PropertyPrice'];
                        $v['Discount'] < 100 && $price *= $v['Discount'] / 100;
                        $s_total_price += $price * $v['Qty'];
                        $total_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'];
                        $deposit_ot_total_price += cart::iconv_price($price, 2, '', 0) * $v['Qty'] * $v["price_rate"];
                        $quantity += $v['Qty'];
                        ?>
                        <tr ovid="<?= $v['OvId']; ?>"
                            cid="<?= $v['CId']; ?>"<?= $is_error == 1 ? ' class="error"' : ''; ?>>
                            <td class="prod_select hide">
                                <em class="btn_checkbox FontBgColor current"></em>
                                <input type="checkbox" name="select" value="<?= $v['CId']; ?>"
                                       class="va_m<?= $is_error == 1 ? ' null' : ''; ?>"<?= $is_error == 1 ? ' disabled' : ''; ?>
                                       checked/>
                            </td>
                            <td class="prod_info_detail">
                                <?php
                                if ($v['BuyType'] == 4) {
                                    //组合促销
                                    echo '<strong>[ ' . $c['lang_pack']['cart']['package'] . ' ] ' . $package_row['Name'] . '</strong>';
                                    foreach ((array)$products_row as $k2 => $v2) {
                                        $img = ly200::get_size_img($v2['PicPath_0'], '240x240');
                                        $url = ly200::get_url($v2, 'products');
                                        ?>
                                        <dl class="clearfix pro_list<?= $k2 ? '' : ' first'; ?>">
                                            <dt class="prod_pic"><a href="<?= $url; ?>"
                                                                    title="<?= $v2['Name' . $c['lang']]; ?>"
                                                                    class="pic_box"><img src="<?= $img; ?>"
                                                                                         alt="<?= $v2['Name' . $c['lang']]; ?>"/><span></span></a>
                                            </dt>
                                            <dd class="prod_info">
                                                <div class="invalid FontBgColor"><?= $c['lang_pack']['cart']['invalid']; ?></div>
                                                <h4 class="prod_name"><a
                                                            href="<?= $url; ?>"><?= $v2['Name' . $c['lang']]; ?></a>
                                                </h4>
                                                <?php
                                                if ($k2 == 0) { //主产品
                                                    echo '<div>';
                                                    foreach ((array)$attr as $k => $z) {
                                                        if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                        echo '<p class="attr_' . $k . '">' . ($k == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $k) . ': ' . $z . '</p>';
                                                    }
                                                    if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                        echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                                    }
                                                    echo '</div>';
                                                } elseif ($data_ary[$v2['ProId']]) { //捆绑产品
                                                    echo '<div>';
                                                    $OvId = 1;
                                                    foreach ((array)$data_ary[$v2['ProId']] as $k3 => $v3) {
                                                        if ($k3 == 'Overseas') { //发货地
                                                            $OvId = str_replace('Ov:', '', $v3);
                                                            if ((int)$c['config']['global']['Overseas'] == 0 || $OvId == 1) continue; //发货地是中国，不显示
                                                            echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</p>';
                                                        } else {
                                                            echo '<p class="attr_' . $k3 . '">' . $attribute_ary[$k3][1] . ': ' . $vid_data_ary[$k3][$v3] . '</p>';
                                                        }
                                                    }
                                                    if ((int)$c['config']['global']['Overseas'] == 1 && $OvId == 1) {
                                                        echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$OvId]['Name' . $c['lang']] . '</p>';
                                                    }
                                                    echo '</div>';
                                                } ?>
                                            </dd>
                                            <?= $k2 ? '<dd class="prod_dot"></dd>' : ''; ?>
                                        </dl>
                                        <?php
                                    }
                                } else {
                                    //普通产品
                                    ?>
                                    <dl>
                                        <dt class="prod_pic"><a href="<?= $url; ?>"
                                                                title="<?= $v['Name' . $c['lang']]; ?>" class="pic_box"><img
                                                        src="<?= $img; ?>"
                                                        alt="<?= $v['Name' . $c['lang']]; ?>"/><span></span></a></dt>
                                        <dd class="prod_info">
                                            <div class="invalid FontBgColor"><?= $c['lang_pack']['cart']['invalid']; ?></div>
                                            <h4 class="prod_name"><a
                                                        href="<?= $url; ?>"><?= $v['Name' . $c['lang']]; ?></a></h4>
                                            <p class="prod_number"><?= $v['Prefix'] . $v['Number']; ?></p>
                                            <?php
                                            if ($attr_len) {
                                                echo '<div>';
                                                foreach ((array)$attr as $k => $z) {
                                                    if ($k == 'Overseas' && ((int)$c['config']['global']['Overseas'] == 0 || $v['OvId'] == 1)) continue; //发货地是中国，不显示
                                                    echo '<p class="attr_' . $k . '">' . ($k == 'Overseas' ? $c['lang_pack']['products']['shipsFrom'] : $k) . ': ' . $z . '</p>';
                                                }
                                                if ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                    echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                                }
                                                echo '</div>';
                                            } elseif ((int)$c['config']['global']['Overseas'] == 1 && $v['OvId'] == 1) {
                                                echo '<p class="attr_Overseas">' . $c['lang_pack']['products']['shipsFrom'] . ': ' . $c['config']['Overseas'][$v['OvId']]['Name' . $c['lang']] . '</p>';
                                            } ?>
                                            <p class="remark"><input type="text" name="Remark[]" value=""
                                                                     maxlength="200"
                                                                     placeholder="<?= $c['lang_pack']['cart']['remark']; ?>"
                                                                     cid="<?= $v['CId']; ?>"
                                                                     proid="<?= $v['ProId']; ?>"/></p>
                                        </dd>
                                    </dl>
                                <?php } ?>
                            </td>
                            <td class="prod_price"><p price="<?= $v['Price']; ?>"
                                                      discount="<?= $v['Discount']; ?>"><?= cart::iconv_price($price); ?></p>
                            </td>
                            <td class="prod_quantity" start="<?= $v['StartFrom']; ?>">
                                <?php if ($a == 'buynow') {//BuyNow页面?>
                                    <?php if ($v['BuyType'] == 4) { ?>
                                        <div class="quantity_box clearfix">
                                            <div class="cut">-</div>
                                            <div class="qty"><input type="text" name="Qty[]" value="<?= $v['Qty']; ?>"
                                                                    maxlength="6" disabled/></div>
                                            <div class="add">+</div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="quantity_box clearfix">
                                            <div class="cut">-</div>
                                            <div class="qty"><input type="text" name="Qty[]" value="<?= $v['Qty']; ?>"
                                                                    maxlength="6"<?= $is_error == 1 ? ' disabled' : ''; ?> />
                                            </div>
                                            <div class="add">+</div>
                                        </div>
                                    <?php } ?>
                                    <input type="hidden" name="S_Qty[]" value="<?= $v['Qty']; ?>"/>
                                    <input type="hidden" name="CId[]" value="<?= $v['CId']; ?>"/>
                                    <input type="hidden" name="ProId[]" value="<?= $v['ProId']; ?>"/>
                                <?php } else {//Checkout页面?>
                                    <p><?= $v['Qty']; ?></p>
                                <?php } ?>
                            </td>
                            <td class="prod_operate">
                                <p price="<?= cart::iconv_price($price, 2, '', 0) * $v['Qty']; ?>"><?= cart::iconv_price(0, 1) . cart::currency_format(cart::iconv_price($price, 2, '', 0) * $v['Qty'], 0, $_SESSION['Currency']['Currency']); ?></p>
                            </td>
                            <?php if (@in_array('shipping_template', (array)$c['plugins']['Used'])) { ?>
                                <td class="prod_shipping"></td>
                            <?php } ?>
                        </tr>
                        <?php /*=============绑定运输方式 供用户选择 开始*/
                        ?>
                        <tr>
                            <td colspan="4">
                                <div class="information_box information_shipping">

                                    <div class="box_title no_border"><?= $c['lang_pack']['cart']['shipmethod']; ?></div>
                                    <div class="box_content">
                                        <div class="shipping current" data-id="<?= $v; ?>">
                                            <div class="list" style="display: flex;">
                                                <ul class="shipping_list_moth_php">
<!--                                                <ul class="shipping_method_list">-->
                                                    <?php
                                                    $i = 0;
                                                    $Shipping_Charges = 0;
//                                                    file_put_contents("t1.txt", "o_id_ary=>" . var_export($o_id_ary, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
                                                    foreach ($o_id_ary as $ke => $s_valu) {
                                                        if ($ke != $v['ProId']) {
                                                            continue;
                                                        }
                                                        $real_ship_arr=array();
                                                        foreach ($s_valu as $kk=>$vv)
                                                        {
                                                            if ($cr_key==$kk)
                                                            {
                                                                $real_ship_arr=$vv;
                                                            }
                                                        }
                                                        foreach ($real_ship_arr as $s_k => $val) {
//                                                             foreach ($s_val as $kk=>$val){
//                                                                 if ($cr_key!=$kk){
//                                                                     continue;
//                                                                 }
                                                            $i++;
                                                            if ($i % 2 == 1) {
                                                                ?>
                                                                <li name="<?= $val["Name"] ?>"  class="odd" title="<?= $val["Name"] ?>">
                                                                <?php
                                                            } else {

                                                                ?>
                                                                <li name="<?= $val["Name"] ?>"  title="<?= $val["Name"] ?>">
                                                                <?php
                                                            }
                                                            ?>
                                                            <span class="name">
							<input type="radio" name="_shipping_method<?= $cr_key ?>" value="<?= $val["SId"] ?>"
                                   price="<?= $val["ShippingPrice"] ?>" insurance="0" shippingtype="" cid="223"
                                   data-proid="<?= $v['ProId'] ?>" data-price="<?= $val["ShippingPrice"] ?>"
                                   data-sid="<?= $val["SId"] ?>" data-affixprice-rate="<?=$val["AffixPrice_rate"] ?>"
                                   s-total-price="<?=$s_total_price?>"
                            >
							<img src="<?= $val["Logo"] ?>" alt="DHL">
							<label><?= $val["Name"] ?></label>
							<span class="price"><?= cart::iconv_price($val["ShippingPrice"]); ?></span>
<!--							<span class="affix_price">--><?//= cart::iconv_price($val["AffixPrice_rate"]); ?><!--</span>-->
							</span><span class="brief" title=""></span>
                                                            <div class="clear"></div>
                                                            </li>


                                                            <!-- 							<li name="LEMON  FREIGHT(DUBAI)" title="Lemon  Freight(Dubai)"> -->
                                                            <!-- 							<span class="name"> -->
                                                            <!-- 							<input type="radio" name="_shipping_method[1]" value="26" price="38" insurance="0" shippingtype="" cid="223"> -->
                                                            <!-- 							<img src="/u_file/1909/photo/d3a8ff87ec.png" alt="Lemon  Freight(Dubai)"> -->
                                                            <!-- 							<label>Lemon  Freight(Dubai)</label> -->
                                                            <!-- 							<span class="price">￥38.00</span> -->
                                                            <!-- 							</span><span class="brief" title=""></span> -->
                                                            <!-- 							<div class="clear"> -->
                                                            <!-- 							</div></li> -->
                                                        <?php }
                                                    } ?>
                                                </ul>
                                                <?php if ($IsInsurance) { ?>
                                                    <div class="insurance">
                                                        <span class="name"><?= $c['lang_pack']['cart']['insurance']; ?>
                                                            :</span>
                                                        <input type="checkbox" name="_shipping_method_insurance"
                                                               class="shipping_insurance" value="1" checked/>
                                                        <label for="shipping_insurance"><?= $c['lang_pack']['cart']['add_insur']; ?></label>
                                                        <a href="javascript:;" class="delivery_ins"
                                                           content="<?= $c['lang_pack']['cart']['tips_insur'] ?>"><?= $c['lang_pack']['cart']['why_insur']; ?></a>
                                                        <span class="price"><?= $_SESSION['Currency']['Symbol']; ?>
                                                            <em></em></span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>


                                        <div class="tips"><?= $c['lang_pack']['cart']['arrive']; ?></div>
                                        <div class="editor_txt"><?= str::str_code($c['config']['global']['ArrivalInfo']['ArrivalInfo' . $c['lang']], 'htmlspecialchars_decode'); ?></div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        <?php /*=============绑定运输方式 供用户选择 结束*/
                        ?>

                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="order_summary clearfix">
                <?php if ($c['FunVersion']) { ?>
                    <div class="coupon_box fl">
                        <div class="code_input clearfix">
                            <input type="text" name="couponCode" class="box_input"
                                   placeholder="<?= $c['lang_pack']['mobile']['apply'] . ' ' . $c['lang_pack']['mobile']['coupon_code']; ?>"
                                   autocomplete="off"/>
                            <div class="btn_coupon_submit btn_global sys_shadow_button FontBgColor"
                                 id="coupon_apply"><?= $c['lang_pack']['submit']; ?></div>
                        </div>
                        <p class="code_error"><?= $c['lang_pack']['cart']['coupon_error']; ?></p>
                        <div class="code_valid clearfix" id="code_valid">
                            <div class="code_valid_key"></div>
                            <div class="code_valid_content"><?= $c['lang_pack']['mobile']['code']; ?>: <strong></strong>
                            </div>
                            <a href="javascript:;" class="btn_coupon_remove sys_shadow_button" id="removeCoupon">X</a>
                        </div>
                    </div>
                <?php } ?>
                <div class="amount_box fr">
                    <?php if ($c['config']['global']['CartWeight']) { ?>
                        <div class="rows clearfix">
                            <div class="name"><?= $c['lang_pack']['cart']['weight']; ?> (KG):</div>
                            <div class="value" id="ot_weight"><?= sprintf('%01.3f', $total_weight); ?></div>
                        </div>
                    <?php } ?>
                    <div class="rows clearfix">
                        <div class="name"><?= $c['lang_pack']['cart']['subtotal']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="ot_subtotal"><?= cart::currency_format($iconv_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                        </div>
                    </div>
                    <?php if (($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0 > $AfterPrice_1)) { ?>
                        <div class="rows clearfix" id="MemberCharge">
                            <div class="name"><?= $c['lang_pack']['cart']['user_save']; ?>:</div>
                            <div class="value"><em>- <?= $_SESSION['Currency']['Symbol']; ?></em><span
                                        id="ot_user"><?= cart::currency_format($AfterPrice_0, 0, $_SESSION['Currency']['Currency']); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="rows clearfix" id="ShippingCharge">
                        <div class="name"><?= $c['lang_pack']['cart']['shipcharge']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="ot_shipping"></span></div>
                    </div>
                    <div class="rows clearfix" id="ShippingInsuranceCombine">
                        <div class="name"><?= $c['lang_pack']['cart']['ship_insur']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="ot_combine_shippnig_insurance"></span></div>
                    </div>
                    <div class="rows clearfix" id="CouponCharge">
                        <div class="name"><?= $c['lang_pack']['cart']['code_save']; ?>:</div>
                        <div class="value"><em>- <?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="ot_coupon"></span></div>
                    </div>
                    <div class="rows clearfix"
                         id="DiscountCharge"<?= (($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1 > $AfterPrice_0)) ? '' : ' style="display:none;"'; ?>>
                        <div class="name"><?= $c['lang_pack']['cart']['save']; ?>:</div>
                        <div class="value"><em>- <?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="ot_subtotal_discount"><?= cart::currency_format($AfterPrice_1, 0, $_SESSION['Currency']['Currency']); ?></span>
                        </div>
                    </div>
                    <div class="rows clearfix" id="ServiceCharge">
                        <div class="name"><?= $c['lang_pack']['cart']['fee']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span id="ot_fee" data-fee=""
                                                                                                 data-affix=""></span>
                        </div>
                    </div>
                    <div class="rows clearfix" id="TotalCharge">
                        <div class="name"><?= $c['lang_pack']['cart']['grand']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span id="ot_total"></span>
                        </div>
                    </div>
                    <div class="rows clearfix" id="deposit_total">
                        <div class="name"><?= $c['lang_pack']['cart']['deposit']; ?>:</div>
                        <div class="value"><em><?= $_SESSION['Currency']['Symbol']; ?></em><span
                                    id="deposit_ot"><?= cart::currency_format($deposit_ot_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                        </div>
                    </div>
                    <input id="_ot_total_" type="hidden" name="_ot_total_" value="<?= $s_total_price ?>"/>
                    <form id="PlaceOrderFrom" method="post" action="/cart/"
                          amountPrice="<?= $iconv_total_price; ?>"<?= (($AfterPrice_0 && !$AfterPrice_1) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_0 > $AfterPrice_1)) ? ' userPrice="' . $AfterPrice_0 . '" userRatio="' . $user_discount . '"' : ' userPrice="0" userRatio="100"'; ?>>
                        <div id="orderFormSubmit" class="btn_place_order btn_global sys_shadow_button btn_disabled">
                            <span>
                                <?= $c['lang_pack']['mobile']['place_order']; ?>
                                
                            </span>
                            <br>
                            <span>
                                <?= $c['lang_pack']['cart']['deposit']; ?>
                                :<em><?= $_SESSION['Currency']['Symbol']; ?></em>
                                <span id="deposit_ot"><?= cart::currency_format($deposit_ot_total_price, 0, $_SESSION['Currency']['Currency']); ?></span>
                            </span>
                        </div>
                        <?php if ($c['NewFunVersion'] >= 4) {//新用户版本 和 Paypal支付?>
                            <div id="paypal_button_container"></div>
                        <?php } ?>
                        <input type="hidden" name="order_coupon_code" value="<?= $_SESSION['Cart']['Coupon']; ?>"
                               cutprice="0"/>
                        <input type="hidden" name="order_discount_price"
                               value="<?= ($AfterPrice_1 && !$AfterPrice_0) || ($AfterPrice_0 && $AfterPrice_1 && $AfterPrice_1 > $AfterPrice_0) ? $AfterPrice_1 : 0; ?>"/>
                        <input type="hidden" name="order_shipping_address_aid" value="-1"/>
                        <input type="hidden" name="order_shipping_address_cid" value="-1"/>
                        <input type="hidden" name="order_shipping_method_sid" value="[]"/>
                        <input type="hidden" name="order_shipping_method_type" value="[]"/>
                        <input type="hidden" name="order_shipping_price" value="[]"/>
                        <input type="hidden" name="order_shipping_insurance" value="[]" price="[]"/>
                        <input type="hidden" name="order_shipping_oversea"
                               value="<?= $oversea_id_hidden ? implode(',', $oversea_id_hidden) : ''; ?>"/>
                        <input type="hidden" name="order_payment_method_pid" value="-1"/>
                        <input type="hidden" name="order_shipping_info" value=""/>
                        <input type="hidden" name="order_affix_price" value=""/>
                        <input type="hidden" name="order_deposit_price" value="<?= $deposit_ot_total_price ?>"/>
                        <?php if ($CId) { ?><input type="hidden" name="order_cid" value="<?= $CId; ?>" /><?php } ?>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div id="payment_ready">
    <div class="load">
        <div class="load_payment">
            <div class="load_image"></div>
            <div class="load_loader"></div>
        </div>
    </div>
    <div class="info">
        <p><?= $c['lang_pack']['cart']['payment_tip_0']; ?></p>
        <p><?= $c['lang_pack']['cart']['payment_tip_1']; ?></p>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#ot_shipping').val(0);
        $('#ot_combine_shippnig_insurance').val(0);
    });
</script>