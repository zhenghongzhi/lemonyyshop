<?php !isset($c) && exit();?>
<?php
$proid_row=db::get_all('sales_seckill', "{$c['time']} > StartTime and {$c['time']} < EndTime and RemainderQty>0", 'ProId');
$proid='0';
foreach((array)$proid_row as $v){
	$proid.=','.$v['ProId'];
}
$cateid_row=db::get_all('products', "ProId in({$proid})", 'CateId');
$cateid='0';
foreach((array)$cateid_row as $v){
	$cateid.=','.$v['CateId'];
}
$cate_topid_row=db::get_all('products_category', "CateId in({$cateid})", 'CateId,UId');
$cate_topid='0';
foreach((array)$cate_topid_row as $v){
	if($v['UId']=='0,'){
		$cate_topid.=','.$v['CateId'];
	}else{
		$uid = @explode(',', $v['UId']);
		$cate_topid.=','.$uid[1];
	}
}
?>
<script type="text/javascript">
$(document).ready(function(){seckill_obj.seckill_init()});
var seckill_timer=new Array();
</script>
<div id="seckill" class="wide">
	
	<?php 
		$cur_lang=substr($c['lang'], 1);
		$ad=db::get_one('billing', 'Position=11 and Device=0');
		$ad_pic=str::json_data($ad['PicPath'], 'decode');
		$ad_url=$ad['Url'];
		if($ad['IsUsed'] && is_file($c['root_path'].$ad_pic[$cur_lang])){
		?>
		<div class="seck_ad"><a href="<?=$ad_url?$ad_url:'javascript:;';?>"<?=$ad_url?' target="_blank"':'';?>><img src="<?=$ad_pic[$cur_lang];?>" alt="" /></a></div>
	<?php } ?>
	<div class="seck_head">
		<div class="title fl"><?=$c['lang_pack']['flashSale'];?></div>
		<div class="view fr" id="seck_title">
			<a href="javascript:;" rel="nofollow" data-type="dealing" class="current"><?=$c['lang_pack']['dailyDeals'];?></a>
			<a href="javascript:;" rel="nofollow" data-type="upcoming"><?=$c['lang_pack']['upcomingDeals'];?></a>
			<a href="javascript:;" rel="nofollow" data-type="past"><?=$c['lang_pack']['pastDeals'];?></a>
		</div>
		<div class="clear"></div>
    </div>
	<div class="seck_menu">
		<div class="category" catalog="0" past="" page="0">
			<a href="javascript:;" data="0" title="<?=$c['lang_pack']['all_category'];?>" class="current"><?=$c['lang_pack']['all'];?></a>
			<?php
			$cate_row=str::str_code(db::get_all('products_category', 'UId="0," and IsSoldOut=0'." and CateId in({$cate_topid})", "CateId, Category{$c['lang']}",  $c['my_order'].'CateId asc'));
			foreach((array)$cate_row as $k=>$v){
			?>
				<a href="javascript:;" data="<?=$v['CateId'];?>" title="<?=$v['Category'.$c['lang']];?>"><?=$v['Category'.$c['lang']];?></a>
			<?php }?>
		</div>
		<div class="seck_sort">
			<a href="javascript:;" data-sort="1"><?=$c['lang_pack']['price'];?><i class="icon_sort"></i></a>
			<a href="javascript:;" data-sort="2"><?=$c['lang_pack']['customer_review'];?><i class="icon_sort"></i></a>
			<a href="javascript:;" data-sort="3"><?=$c['lang_pack']['most_popular'];?><i class="icon_sort"></i></a>
		</div>
        <div class="clear"></div>
    </div>
	<div id="prolist" class="over" Num="0"></div>
</div>



