<?php !isset($c) && exit();?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?=substr($c['lang'], 1);?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?=ly200::seo_meta();?>
<?php include("{$c['static_path']}/inc/static.php");?>
<?=ly200::load_static("/static/themes/{$c['theme']}/css/index.css");?>
</head>

<body class="lang<?=$c['lang'];?>">
<?php 
include("{$c['theme_path']}/inc/header.php");
if(!file::check_cache('index.html')){
	ob_start();
?>
<div class="wide">
	<div class="brisze">
		<div class="tit"><?=$c['lang_pack']['bestSeller']; ?></div>
			<div class="box_rlt">
				<div class="box_abs">
					<?php 
					$products_list_row=str::str_code(db::get_limit('products', 'IsBestDeals=1'.$c['where']['products'], '*', $c['my_order'].'ProId desc', 0, 8));
					foreach((array)$products_list_row as $k=>$v){
						$is_promition=($v['IsPromotion'] && $v['StartTime']<$c['time'] && $c['time']<$v['EndTime'])?1:0;
						$url=ly200::get_url($v, 'products');
						$img=ly200::get_size_img($v['PicPath_0'], '500x500');
						$name=$v['Name'.$c['lang']];
						$price_ary=cart::range_price_ext($v);
						$price_0=$price_ary[1];
					?>
						<div class="item">
							<div class="img trans_img">
								<a href="<?=$url; ?>" title="<?=$name; ?>"><img src="<?=$img; ?>" alt="<?=$name; ?>" /></a>
							</div>
							<p>
								<a href="<?=$url; ?>" title="<?=$name; ?>"><?=$name; ?></a>
							</p>
							<div class="price prod_price PriceColor">
								<b><em class="currency_data"></em><span class="price_data" data="<?=$price_ary[0];?>" keyid="<?=$v['ProId'];?>"></span> </b>
								<?php if($price_ary[2]){?>
									<del><em class="currency_data"></em><span class="price_data" data="<?=$price_0;?>"></span></del>
								<?php }?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		<span class="hd"></span>
		<a href="javascript:void(0)" class="btn left"></a>
		<a href="javascript:void(0)" class="btn right"></a>
	</div>
	<div id="banner">
		<?=ly200::visual_ad(); ?>
	</div>
</div>
<?php 
$seckill_row = str::str_code(db::get_limit('sales_seckill s left join products p on s.ProId=p.ProId', "s.StartTime<={$c['time']} and s.EndTime>{$c['time']}".' and s.RemainderQty>0', "s.*, p.Name{$c['lang']}, p.PicPath_0, p.Price_0, p.Price_1, p.TotalRating, p.IsHot, p.Unit", 'if(s.MyOrder>0, if(s.MyOrder=999, 1000001, s.MyOrder), 1000000) asc, s.SId desc', 0, 4));
if($seckill_row){
?>
<div class="wide flash" plugins="index-0" effect="0-1">
	<div class="tit" plugins_pos="0" plugins_mod="Title"><?=$c['web_pack']['index'][0][0]['Title']; ?></div>
	<div class="lad">
		<div plugins_pos="1">
			<?=ly200::visual_ad($c['web_pack']['index'][0][1]);?>
		</div>
		<div class="Countdown">
			<div class="discount_time" endTime="<?=date('Y/m/d H:i:s',db::get_value('sales_seckill_period', "{$c['time']} > StartTime and {$c['time']} < EndTime", 'EndTime', 'EndTime asc')); ?>"></div>
		</div>
	</div>
	<?php 
		if($seckill_row){
		?>
		<div class="products_box">
			<?php 
			foreach((array)$seckill_row as $k=>$v){
				$url=ly200::get_url($v, 'seckill');
				$img=ly200::get_size_img($v['PicPath_0'], '240x240');
				$price_ary=cart::range_price_ext($v);
				$price_0 = $price_ary[1];
				$discount=sprintf('%01.0f', ($price_0-$v['Price'])/$price_0*100);
				$name = $v['Name'.$c['lang']];
				$discount=$discount<1?1:$discount;
				$progress=ceil((1-$v['RemainderQty']/$v['Qty'])*100);
				if($v['Unit']){//产品自身设置单位
					$Unit=$v['Unit'];
				}elseif($c['config']['products_show']['Config']['item'] && $c['config']['products_show']['item']){//产品统一设置单位
					$Unit=$c['config']['products_show']['item'];
				}else{
					$Unit=$c['lang_pack']['piece'];
				}
			?>
				<div class="prod_box trans fl <?=$k==3 ? 'last' : ''; ?>">
					<div class="pic_box trans_img"><a href="<?=$url; ?>"><img src="<?=$img; ?>" alt="<?=$name; ?>" /><span></span></a></div>
					<div class="pro_name"><a href="<?=$url; ?>" title="<?=$name; ?>"><?=$name; ?></a></div>
					<div class="pro_mp"><span><?=$c['lang_pack']['only'];?> <?=100-$progress;?>%</span><div class="color" style="width: <?=$progress;?>%"></div></div>
					<div class="prod_price PriceColor">
						<span><?=cart::iconv_price($v['Price']);?></span> 
						<?php if($price_ary[2]){?>
							<del><?=cart::iconv_price($price_0);?></del>
							<em class="icon_discount"><b><?=$discount;?></b><b>% OFF</b></em>
						<?php }?>
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<div class="clear"></div>
</div>
<?php } ?>
<div class="lanad wide" plugins="index-1" effect="0-1">
	<div class="trans_img" plugins_pos="0">
		<?=ly200::visual_ad($c['web_pack']['index'][1][0]);?>
	</div>
</div>
<?php 
$index_category_row=db::get_limit('products_category','IsIndex=1 and Dept=1',"CateId,PicPath,Category{$c['lang']}",$c['my_order'].'CateId asc',0,4);
?>
<div class="wide">
	<?php
	foreach((array)$index_category_row as $k => $v){ 
		$k_2=$k+2;
		if(!db::get_row_count('products',(category::get_search_where_by_CateId($v['CateId'], 'products_category').' or '.category::get_search_where_by_ExtCateId($v['CateId'], 'products_category')).$c['where']['products'],'ProId')) continue;
		?>
		<div class="floor floor<?=$k+1;?>" plugins="index-<?=$k_2; ?>" effect="2-1">
			<div class="style<?=$c['web_pack']['index'][$k_2]['Layout'];?>">
				<div class="flad" plugins_pos="0">
					<div class="tit"><?=$v['Category'.$c['lang']]; ?></div>
					<div class="img trans_img">
						<?=ly200::visual_ad($c['web_pack']['index'][$k_2][0]);?>
					</div>
				</div>
				<div class="frb">
					<div class="cate">
						<?php 
						$i=0;
						$sec_pro_row = array();
						foreach((array)$allcate_ary["0,{$v['CateId']},"] as $k1 => $v1){
							$pro_row = db::get_limit('products',(category::get_search_where_by_CateId($v1['CateId'], 'products_category').' or '.category::get_search_where_by_ExtCateId($v1['CateId'], 'products_category')).$c['where']['products'],'*',$c['my_order'].'ProId desc',0,16);
							if($i>3 || !$pro_row) continue;
							$sec_pro_row[] = $pro_row;
							?>
							<a href="javascript:;" rel="nofollow" title="<?=$v1['Category'.$c['lang']]; ?>" class="<?=$i==0 ? 'on' : ''; ?>"><?=$v1['Category'.$c['lang']]; ?></a>
						<?php $i++; } ?>
					</div>
					<?php
					$products_list_row = array();
//                    echo (category::get_search_where_by_CateId($v['CateId'], 'products_category') . ' or ' . category::get_search_where_by_ExtCateId($v['CateId'], 'products_category')) . $c['where']['products'];
					$sec_pro_row || $sec_pro_row[] = db::get_limit('products', (category::get_search_where_by_CateId($v['CateId'], 'products_category') . ' or ' . category::get_search_where_by_ExtCateId($v['CateId'], 'products_category')) . $c['where']['products'], '*', $c['my_order'] . 'ProId desc', 0, 16);
					foreach ((array)$sec_pro_row as $k1 => $v1) {
						$array = array();
						foreach ((array)$v1 as $k2 => $v2) {
							if ($k2 % 2 == 0)
							$array[] = array_slice($v1,$k2,2);
						}
						$products_list_row[] = $array;
					}
					foreach ((array)$products_list_row as $k1 => $v1) {
					?>
					<div class="frpb frpb_<?=$k;?>_<?=$k1;?>"<?=$k1 == 0 ? ' style="display:block;"' : '';?>>
						<a href="javascript:;" rel="nofollow" class="left"></a>
						<a href="javascript:;" rel="nofollow" class="right"></a>
						<div class="box_rlt">
							<div class="box_abs">
								<?php
								foreach ((array)$v1 as $k2 => $v2) {
								?>
									<div class="box_item clean">
										<?php
										foreach ((array)$v2 as $k3 => $v3){
											$is_promition = ($v3['IsPromotion'] && $v3['StartTime']<$c['time'] && $c['time']<$v3['EndTime'])?1:0;
											$url = ly200::get_url($v3, 'products');
											$img = ly200::get_size_img($v3['PicPath_0'], '500x500');
											$name = $v3['Name'.$c['lang']];
											$price_ary = cart::range_price_ext($v3);
											$price_0 = $price_ary[1];
										?>
											<div class="prod_box trans fl<?=($k3==1 ? ' bb' : '').($k3==2 ? ' br' : '');?>">
												<div class="pic_box trans_img"><a href="<?=$url;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /><span></span></a></div>
												<div class="pro_name"><a href="<?=$url;?>" title="<?=$name;?>"><?=$name;?></a></div>
												<div class="clean">
													<div class="prod_price">
														<div class="i_prod2_price1">
															<em class="currency_data PriceColor"></em><span class="price_data PriceColor" data="<?=$price_ary[0];?>" keyid="<?=$v3['ProId'];?>"></span>
														</div>
														<?php
                                                        if ($price_ary[2]) {
															echo '<del class="i_prod2_price0"><em class="currency_data"></em><span class="price_data" data="'.$price_0.'"></span></del>';
														}
                                                        if ($is_promition) {
                                                            echo '<em class="icon_discount DiscountBgColor"><b>'.@round(sprintf('%01.2f', ($price_0 - $price_ary[0]) / $price_0 * 100)).'</b><b>% OFF</b></em>';
                                                        }?>
														<em class="icon_seckill DiscountBgColor"><?=$c['lang_pack']['products']['sale'];?></em>
													</div>
													<div class="add_to_cart trans"><a href="<?=$url;?>"><?=$c['lang_pack']['addToCart'];?></a></div>
													<a href="javascript:;" class="favorite trans add_favorite" data="<?=$v3['ProId'] ?>"></a>
												</div>
												<?php if ($v3['IsNew']) {?>
                                                    <em class="icon_new"><?=$c['lang_pack']['new'];?></em>
                                                <?php }?>
											</div>
										<?php }?>
									</div>
								<?php }?>
							</div>
						</div>
					</div>
					<?php }?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<?php
        foreach((array)$products_list_row as $k1 => $v1){
            $val=$k.'_'.$k1;
        ?>
			<script>Carousel('.frpb_<?=$val;?> .box_abs','.frpb_<?=$val;?> .box_abs .box_item','.frpb_<?=$val;?> .left','.frpb_<?=$val;?> .right',1);</script>
		<?php }?>
	<?php }?>
</div>
<?php 
if($c['plugin_app']->trigger('blog', '__config', 'get_blog')=='enable'){//博客
	$c['plugin_app']->trigger('blog', 'get_blog');
}
if($c['plugin_app']->trigger('gallery', '__config', 'gallery_index')=='enable'){//app插件是否存在
    $c['plugin_app']->trigger('gallery', 'gallery_index','');
} 
?>
<div class="newsletter">
	<div class="wide">
		<div class="newto">
			<p><?=$c['lang_pack']['newsletter_title']; ?></p>
			<span><?=$c['lang_pack']['newsletter_notes']; ?></span>
		</div>
		<div class="form">
			<form id="newsletter_form">
				<div class="input">
					<input type="text" class="text" name="Email" value="" notnull="" placeholder="<?=$c['lang_pack']['user']['enterEmail'].'...'; ?>" format="Email">
					<input type="submit" value="<?=$c['lang_pack']['newsletter_btn']; ?>">
				</div>
			</form>
		</div>
	</div>
</div>
<?php 
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'index.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'index.html');

include("{$c['theme_path']}inc/footer.php");
?>
</body>
</html>