<?php !isset($c) && exit();?>
<?php
class themes_set{
	public static function config_edit_init(){
		global $c;
		$ary=array(
			'Effects'		=>	0, //特效方式
			'HeaderContent'	=>	0, //头部内容
			'IndexContent'	=>	1, //首页内容
			'TopMenu'		=>	0, //顶部栏目
			'ContactMenu'	=>	0, //联系我们
			'ShareMenu'		=>	1, //分享栏目
			'IndexTitle'	=>	0, //首页标题
		);
		return $ary;
	}
	
	public static function themes_products_list_reset(){
		return '{"IsColumn":"1","Narrow":"1","IsLeftbar":{"Category":"1","Hot":"1","Special":"1","Banner":"1"},"Order":"row_4","OrderNumber":"20","Effects":"4"}';
	}
	
	public static function themes_style_reset(){
		return '{"FontColor":"#5BAA60","NavBgColor":"#277F09","NavHoverBgColor":"#277F09","NavBorderColor1":"#1E6109","NavBorderColor2":"#4D9632","CategoryBgColor":"#194C08","PriceColor":"#44A708","AddtoCartBgColor":"#9ABE14","BuyNowBgColor":"#F28810","ReviewBgColor":"#F28810","DiscountBgColor":"#FE8A27","ProListBgColor":"#277F09","ProListHoverBgColor":"#4D9632","GoodBorderColor":"#277F09","GoodBorderHoverColor":"#4D9632"}';
	}
	
	public static function themes_banner_init(){
		$ary=array(
			'Type'	=>	1,
		);
		return $ary;
	}
}
?>