<?php !isset($c) && exit();?>
<?php
if(!file::check_cache('footer.html')){
	ob_start();
?>
<div id="footer_outer">
	<div id="service" class="wide clearfix">
		<?php
		$article_category_row=str::str_code(db::get_limit('article_category', 'UId="0," and CateId not in(1,99)', "CateId, Category{$c['lang']}", $c['my_order'].'CateId asc', 0, 4));
		foreach((array)$article_category_row as $k=>$v){
		?>
		<dl class="fore_<?=$k;?> fl">
			<dt><?=$v['Category'.$c['lang']];?></dt>
			<dd>
				<?php
				$article_row=str::str_code(db::get_limit('article', "CateId='{$v['CateId']}'", "AId, Title{$c['lang']}, PageUrl, Url", $c['my_order'].'AId desc', 0, 5));
				foreach((array)$article_row as $v2){
					echo html::article_a($v2);
				}?>
			</dd>
		</dl>
		<?php }?>
		<?php 
            $share_count = 0;
            foreach($c['follow'] as $v){
                if(!$c['config']['global']['ShareMenu'][$v]) continue;
                $share_count++;
            }
            if($share_count){
            ?>
			<dl>
				<dt><?=$c['lang_pack']['followUs'];?></dt>
				<dd>
	                <?php 
	                    $icon_follow_type=0; 
	                    include("{$c['static_path']}inc/follow_us.php");
	                ?>
				</dd>
			</dl>
		<?php } ?>
		<span class="br"></span>
	</div>
	<div id="footer" class="wide clearfix">
		<?php if(db::get_row_count('partners', "IsUsed=1")){ ?>
			<div class="foot_pay">
				<?=ly200::partners();?>
			</div>
		<?php } ?>
		<div class="foot_copy copyright"><?=$c['config']['global']['CopyRight']['CopyRight'.$c['lang']];?><?=$c['powered_by']?' &nbsp;&nbsp;&nbsp;&nbsp; '.$c['powered_by']:'';?></div>
	</div>
</div>
<?php
	include("{$c['static_path']}/inc/chat.php");
	echo ly200::out_put_third_code();
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'footer.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'footer.html');
?>
