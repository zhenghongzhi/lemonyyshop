/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

$(document).ready(function(){
	/****** 导航分类下拉 start ******/
	if($('#nav').attr('page')!='index'){
		$('#nav').delegate('.nav_menu', 'mouseover', function(){$(this).find('.nav_categories').stop(true, true).fadeIn(400);});
		$('#nav').delegate('.nav_menu', 'mouseleave', function(){$(this).find('.nav_categories').stop(true, true).fadeOut(400);});
	}else{
		$('#nav_outer .nav_categories').show();
		var obj=$('#nav .nav_categories>ul>li');
		var index_category=function(obj){
			if($('body').hasClass('w_1200')){
				obj.css('display', 'block');
			}else{
				obj.eq(8).css('display', 'none');
				obj.eq(9).css('display', 'none');
			}
		}
		
		jQuery(window).resize(function(){index_category(obj)});
		index_category(obj)
	}
	$('#nav').delegate('.nav_categories>ul>li', 'mouseover', function(){
		$(this).find('h2>a').addClass('FontColor').next('em').addClass('NavArrowHoverColor');
		var json=$.evalJSON($(this).attr('data'));
		if(json.length){
			var index=$(this).addClass('hover').index();
			if(!$(this).find('.nav_subcate').length){
				var html='<div class="nav_subcate">';
				for(i=0; i<json.length; i++){
					html=html+'<dl'+(i>=3?' class="tline"':'')+'><dt><a href="'+json[i].url+'" title="'+json[i].text+'">'+json[i].text+'</a></dt>';
					if(json[i].children){
						var jsonchild=json[i].children;
						html=html+'<dd>';
						for(j=0; j<jsonchild.length; j++){
							html=html+'<a href="'+jsonchild[j].url+'" title="'+jsonchild[j].text+'">'+jsonchild[j].text+'</a>';
						}
						html=html+'</dd>';
					}
					html=html+'</dl>';
					if((i+1)%3==0){html=html+'<div class="blank12"></div>';}
				}
				html=html+"</div>";
				$(this).append(html);
			}
			if(index<=11){
				$(this).find('.nav_subcate').css('top',(-index*40-8)+'px');
			}else{
				$(this).find('.nav_subcate').css('bottom',-40+'px');
			}
		}
		$(this).find('em').css('border-color', 'transparent transparent transparent '+$('.CategoryBgColor').css('background-color'));
	});
	$('#nav').delegate('.nav_categories>ul>li', 'mouseleave', function(){$(this).removeClass('hover').find('h2>a').removeClass('FontColor').next('em').css('border-color', 'transparent transparent transparent #ccc').parent().parent().find('.nav_subcate').remove();});
	$('#nav .nav_item li').addClass('NavBorderColor2').children('a').addClass('NavBorderColor1 NavHoverBgColor');
	/****** 导航分类下拉 end ******/
	
	/****** 导航显示 Start ******/
	function navShow(){
		var $obj=$('.nav_item'),
			navItemWidth=0,
			navWidth=$obj.width();//IE下要多出一像素
		//var navWidth=$('body').hasClass('w_1200')?964:744;
		
		$obj.css('overflow', 'visible').find('li').each(function(){
			navItemWidth+=$(this)[0].getBoundingClientRect().width;
			if(navItemWidth>navWidth){
				$(this).hide();
			}else{
				$(this).show();
			}
		});
	}
	navShow();
	$(window).resize(function(){ navShow(); });
	/****** 导航显示 End ******/
	$('.frb .cate a').click(function(){
		$(this).parent().find('a').removeClass('on');
		$(this).addClass('on');
		$(this).parents('.frb').find('.frpb').hide().eq($(this).index()).show();
	});
	$('.brisze .hd').text(1+'  /  '+$('..brisze .box_abs .item').length);
	Carousel('.brisze .box_abs','.brisze .box_abs .item','.brisze .left','.brisze .right',1,function(a,b){
		$('.brisze .hd').text(a+'  /  '+b);
	});
});
function Carousel(box,item,prev,next,length,callback){
	var box = $(box),
		item = $(item),
		item_width = item.outerWidth(true),
        old_par_len = par_len = item.length,
        boxWidth = par_len*item_width,
        item_index = 1;
    box.width(boxWidth);
    $(window).resize(function(){ //响应式处理
    	var offset = parseInt(box.css('left')),
    		index = parseInt(offset/item_width),
			item_width = item.outerWidth(true),
			par_len = item.length,
			boxWidth = par_len*item_width;
    	box.css('left',index*item_width+'px');
    	box.width(boxWidth);
    	if(par_len>length){
    		box.width(boxWidth*2);
    	}
    });
    if(par_len>length){
        box.append(item.clone());
        box.width(boxWidth*2);
        $(prev).click(function(){
            if(box.is(':animated')){return false;}
            var offset = parseInt(box.css('left'));
            if(offset>=0){
                box.css('left','-'+boxWidth+'px');
            }
            box.animate({'left':'+='+item_width+'px'});
            item_index = (item_index-1 <= 0) ? old_par_len : item_index-1; 
            if(callback) callback(item_index,old_par_len);
        });
        $(next).click(function(){
            if(box.is(':animated')){return false;}
            var offset = parseInt(box.css('left'));
            if(offset<=-boxWidth){
                box.css('left','0px');
            }
            box.animate({'left':'-='+item_width+'px'});
            item_index = (item_index+1 > old_par_len) ? 1 : item_index+1;
            if(callback) callback(item_index,old_par_len);
        });
    }
}

$(function(){    
    //折扣倒计时
    $(".Countdown").find(".discount_time").each(function(){
        var time=new Date();
        $(this).genTimer({
            beginTime: ueeshop_config.date,
            targetTime: $(this).attr("endTime"),
            day_label:"DAY",
            days_label:'DAYS',
            unitWord:{hours:"</span><span class='str'>:</span><span class='day_seconds'>", minutes:"</span><span class='str'>:</span><span class='day_seconds'>", seconds:"</span>"},
            callback: function(e){
                this.html(e)
            }
        });
    });
});
