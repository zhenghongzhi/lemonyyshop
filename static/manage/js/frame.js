/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var frame_obj={
	page_init:function(){
		//页面加载
		var resize=function(){
			$(window).width()>=1280?$('body').addClass('w_1200'):$('body').removeClass('w_1200');
			$(window).width()>=1400?$('body').addClass('w_1400'):$('body').removeClass('w_1400');
			var $mainWidth=($(window).width()>=980?$(window).width():980),
				$mainHeight=$(window).height()-$('#header').outerHeight(true);
			$('#main').css({'width':$mainWidth, 'height':$mainHeight});
			if(ueeshop_config.FunVersion>=10){
				$('#main .oth_version_menu').width($('#main .menu_ico').width()+$('#main .menu_list').width()+$('#main .menu_button').width());
				$('#main .oth_version_menu .menu_list').height($mainHeight);
				$('#main .oth_version_menu .menu_list').jScrollPane();
				//$('#main .free_version_menu .new_menu_list').height($mainHeight);
			}else{
				$('#main .menu').width($('#main .menu_ico').width()+$('#main .menu_list').width()+$('#main .menu_button').width());
				$('#main .menu_list').height($mainHeight);
				$('#main .menu_list').jScrollPane();
			}
			$('#main .menu_button i').css('marginTop', $('#main .menu_button a').height()/2-6);
			$('#main .righter').width($mainWidth-$('#main .menu').width());
			$('#main .r_con_wrap').css({height:$mainHeight-$('.fixed_btn_submit').outerHeight(true)});
			$('#header').width($mainWidth);
			if($('#edit_form').length && $('#edit_form input.btn_submit').length){ //提交按钮固定于底部
				$('#edit_form input.btn_submit').parents('.rows').addClass('fixed_btn_submit');
				$('#main .righter').append($('.fixed_btn_submit'));
				$('.fixed_btn_submit').css({width:$mainWidth-$('#main .menu').width(), left:$('#main .menu').width()});
				$('#main .r_con_wrap').css({height:$mainHeight-$('.fixed_btn_submit').outerHeight(true)-5});
			}
			if($('.fixed_btn_submit').length){
				$('.fixed_btn_submit').css({width:$mainWidth-$('#main .menu').width(), left:$('#main .menu').width()});
				$('#main .r_con_wrap').css({height:$mainHeight-$('.fixed_btn_submit').outerHeight(true)-5});
			}
			if($('#div_mask').length) $('#div_mask').css({height:$(document).height()}); //刷新遮罩层高度
			if($('#main .righter .fixed_loading').length) $('#main .righter .fixed_loading').css({width:$('#main .righter').width(), height:$('#main .righter').height()});
			//表格列表没有任何数据
			if($('.bg_no_table_data').length && !$('.bg_no_table_data').hasClass('bg_no_table_fixed')){
				var $insideTable=($('.inside_table').length?40:0), //40:内间距
					$content=$('.bg_no_table_data .content').outerHeight(true),
					$H=$('.r_con_wrap').outerHeight()-$('.inside_container').outerHeight(true)-$insideTable-$('.list_menu').outerHeight(),
					$half=($H-$content)/2;
				$('.bg_no_table_data').css('height', $H);
				$('.bg_no_table_data .content').css('top', ($half<0?0:$half));
			}
		}
		resize();
		$(window).resize(function(){resize();});
		
		//加载效果
		$(window).ready(function(){
			$('#main .fixed_loading').fadeOut(500);
			// 产品列表,加载完成之后懒加载图片
			$(window).ready(function(){
				if($('.loading_img').length>0){
					$('.loading_img').each(function(index,elem){
						$(elem).attr('src', $(elem).attr('data-src'));
					});
				}
			});
		});

		$('#header .menu .menu_home a').click(function(){
			$.post('./', 'do_action=action.file_clear_cache', function(data){
			}, 'json');
		});
		
		//头部管理员下拉 or 消息提示下拉
		frame_obj.message_orders_init();
		
		if(ueeshop_config.FunVersion>=10){
			//权限
			function prevent_limit_fun(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				$(".popup_limit_tips,.limit_mask").addClass('visible');
			}
			$(".cdx_limit").off().click(function(e){prevent_limit_fun(e);});
			$(".cdx_limit").find("*").each(function(){
				$(this).off().click(function(e){
					prevent_limit_fun(e);
				});
			});
	
			$(".popup_limit_tips .go_back,.limit_mask").click(function(e){
				$(".popup_limit_tips,.limit_mask").removeClass('visible');
			});
			
			//升级版下拉
			$("#header .menu > li.menu_upgrade .ug_drop .top_btn, #header .menu .menu_upgrade a").click(function(){
				console.log(1);
				$(".contact_qrcode,.qr_black_mask").addClass('visible');
			});
	
			$(".qr_black_mask").click(function(){
				$(".contact_qrcode,.qr_black_mask").removeClass('visible');
			});
			
			//微信客服下拉
			$('#header .menu_qrcode').hover(function(){
				$(this).find('.qrcode_box').show().stop(true).animate({'top':50, 'opacity':1}, 250);
			}, function(){
				$(this).find('.qrcode_box').stop(true).animate({'top':40, 'opacity':0}, 100, function(){ $(this).hide(); });
			});
	
			// 免费版 contentWidth
			$('.free_version_menu .new_menu_list .menu_item .item_name').on('click', function(){
				$('.free_version_menu .new_menu_list .menu_item').removeClass('open');
				$(this).parent().toggleClass('open');
			});
	
			$('#main .oth_version_menu').on('mouseenter', '.menu_ico .menu_item', function(){
				var $Index=$(this).index();
				if($(this).find('.icon_menu').hasClass('current')){ //当前管理栏目不用触发，如果有显示就隐藏起来
					$('#main .oth_version_menu .menu_ico .icon_menu').removeClass('hover');
					$('#main .oth_version_menu .menu_ico_name').removeClass('show');
					$('#main .oth_version_menu .menu_ico_name .menu_item').removeClass('current');
					$('#main .oth_version_menu .menu_list .jspPane').css('left', 0);
					return false;
				}
				if(!$('#main .oth_version_menu .menu_ico_name').hasClass('show')){
					$('#main .oth_version_menu .menu_ico_name').addClass('show');
					$('#main .oth_version_menu .menu_list .jspPane').css('left', 60);
				}
				$(this).find('.icon_menu').addClass('hover');
				$(this).siblings().find('.icon_menu').removeClass('hover');
				$('#main .oth_version_menu .menu_ico_name .menu_item').eq($Index).addClass('current').siblings().removeClass('current');
			}).on('mouseenter', '.menu_ico_name .menu_item', function(){
				var $Index=$(this).index();
				$(this).addClass('current').siblings().removeClass('current');
				$('#main .oth_version_menu .menu_ico .menu_item').eq($Index).find('.icon_menu').addClass('hover');
				$('#main .oth_version_menu .menu_ico .menu_item').eq($Index).siblings().find('.icon_menu').removeClass('hover');
			}).on('mouseleave', '.menu_ico_name', function(){
				$('#main .oth_version_menu .menu_ico .icon_menu').removeClass('hover');
				$('#main .oth_version_menu .menu_ico_name').removeClass('show');
				$('#main .oth_version_menu .menu_ico_name .menu_item').removeClass('current');
				$('#main .oth_version_menu .menu_list .jspPane').css('left', 0);
			});
			//左侧子栏目
			$('#main .oth_version_menu dt').on('click', function(){
				var $this=$(this);
				$this.addClass('cur').siblings().removeClass('cur');
				if($this.next('dd').length){
					if($this.next('dd').is(':hidden')){
						$('#main .oth_version_menu dt div').html('-');
						$this.next().filter('dd').slideDown(function(){
							$this.addClass('cur');
							$('#main .menu_list').jScrollPane();
						});
					}else{
						$this.children('div').html('+');
						$this.next().filter('dd').slideUp(function(){
							$this.removeClass('cur');
							$('#main .menu_list').jScrollPane();
						});
					}
				}
			});
		}else{
			//左侧子栏目
			$('#main .menu dt').on('click', function(){
				var $this=$(this);
				$this.addClass('cur').siblings().removeClass('cur');
				if($this.next('dd').length){
					if($this.next('dd').is(':hidden')){
						$('#main .menu dt div').html('-');
						$this.next().filter('dd').slideDown(function(){
							$this.addClass('cur');
							$('#main .menu_list').jScrollPane();
						});
					}else{
						$this.children('div').html('+');
						$this.next().filter('dd').slideUp(function(){
							$this.removeClass('cur');
							$('#main .menu_list').jScrollPane();
						});
					}
				}
			});
		}
				
		//左侧子栏目显藏按钮
		$('#main .menu_button a').on('click', function(){
			$(this).blur();
			if($('#main .menu_list').attr('status')=='off'){
				$(this).children('i').removeClass('show');
				$('#main .menu_list').attr('status', 'on').stop(true, false).animate({'width':130}, 200, function(){
					$('#main .menu_list').jScrollPane();
				});
				$('#main .menu').width($('#main .menu_ico').width()+$('#main .menu_button').width()+130);
				$('#main .righter').width($('#main').width()-$('#main .menu').width());
				$('#main .r_con_wrap').css({width:$(window).width()-$('#main .menu').width()});
				$('#edit_form .fixed_btn_submit').css({width:$('#main').width()-$('#main .menu').width(), left:$('#main .menu').width()});
			}else{
				$(this).children('i').addClass('show');
				$('#main .menu_list').attr('status', 'off').stop(true, false).animate({'width':0}, 200, function(){
					$('#main .menu').width($('#main .menu_ico').width()+$('#main .menu_button').width());
					$('#main .righter').width($('#main').width()-$('#main .menu').width());
					$('#main .r_con_wrap').css({width:$(window).width()-$('#main .menu').width()});
					$('#edit_form .fixed_btn_submit').css({width:$('#main').width()-$('#main .menu').width()-20, left:$('#main .menu').width()});
				});
			}
		});
		
		//弹出提示
		$('.tool_tips_ico').each(function(){
			if($(this).attr('content')==''){
				$(this).hide();
				return;
			}else{
				$(this).html(' ');
				$('#main .r_con_wrap').tool_tips($(this), {position:'horizontal', html:$(this).attr('content'), width:260});
			}
		});
		/*
		$('*[placeholder]').each(function(){
			if(!$(this).val()){
				$(this).val($(this).attr('placeholder')).css('color', '#bbb');
			}
		}).focus(function(){
			if($(this).val()==$(this).attr('placeholder')){
            	$(this).val('').css('color', '#333');
			}
		}).blur(function(){
			if(!$(this).val()){
				$(this).val($(this).attr('placeholder')).css('color', '#bbb');
			}
		});
		*/
		$('.tip_ico').hover(function(){
			$(this).append('<span class="tip_ico_txt'+($(this).hasClass('tip_min_ico')?' tip_min_ico_txt':'')+' fadeInUp animate">'+$(this).attr('label')+'<em></em></span>');
		}, function(){
			$(this).removeAttr('style').children('span').remove();
		});
		$('.tip_ico_down').hover(function(){
			$(this).append('<span class="tip_ico_txt_down'+($(this).hasClass('tip_min_ico')?' tip_min_ico_txt_down':'')+' fadeInDown animate">'+$(this).attr('label')+'<em></em></span>');
		}, function(){
			$(this).removeAttr('style').children('span').remove();
		});
		
		//手机版预览效果
		$('#main .menu .ico_mpreview').on('click', function(){
			if(!$("#mpreview_box").length){
				var html='',
					src=$(this).children('a').attr('src');
				global_obj.div_mask();
				html+='<div id="mpreview_box">';
					html+='<div class="mpreview_main">';
						html+='<a class="mpreview_close" href="javascript:;"></a>';
						html+='<iframe src="'+src+'" frameborder="0" name="mpreview_iframe" id="mpreview_iframe" scrolling="yes"></iframe>';
					html+='</div>';
				html+='</div>';
				$('body').prepend(html);
				$('#mpreview_box').fadeIn().css({left:$(window).width()/2-186});
				
				$("#mpreview_box .mpreview_close").on("click", function(){
					$("#mpreview_box").fadeOut().remove();
					global_obj.div_mask(1);
				});
			}
		});
		
		//选项卡效果
		$('body').on('click', '.tab_box .tab_box_btn', function(){
			var $this	= $(this),
				$lang	= $this.attr('data-lang'),
				//$obj	= $this.parents('.rows');
				$obj	= $this.parents('form');
			$obj.find('.tab_box_btn').show();
			//$this.hide();
			// $obj.find('.tab_box_btn[data-lang='+$lang+']').hide();
			$obj.find('dt span').text($this.find('span').text());
			$obj.find('.tab_txt').hide();
			$obj.find('.tab_txt_'+$lang).show();
		});
		
		//勾选按钮
		$('.btn_checkbox, .btn_choice').on('click', function(){
			var $this=$(this),
				$obj=$(this).find('input');
			if($this.hasClass('disabled')) return false; //禁止调用
			if($obj.is(':checked')){ //已勾选
				$obj.removeAttr('checked');
				$this.removeClass('current');
			}else{ //未勾选
				$obj.attr('checked', true);
				$this.addClass('current');
			}
		});
		
		//属性勾选按钮
		$('.btn_attr_choice').on('click', function(){
			var $this=$(this),
				$obj=$(this).find('input');
			if($this.hasClass('disabled')) return false; //禁止调用
			if($obj.is(':checked')){ //已勾选
				$obj.removeAttr('checked');
				$this.removeClass('current');
			}else{ //未勾选
				$obj.attr('checked', true);
				$this.addClass('current');
			}
		});
		
		$('body').on('click', '.input_radio_box', function(){
			//单选按钮
			var name=$(this).find('input').attr('name');
			$('input[name='+name+']').removeAttr('checked').parent().parent().removeClass('checked');
			$(this).addClass('checked').find('input').attr('checked', true);
		}).on('click', '.input_checkbox_box', function(e){
			//多选按钮
			var $obj=$(this);
			if($obj.hasClass('checked')){
				$obj.find('input').removeAttr('checked');
				$obj.removeClass('checked');
			}else{
				$obj.find('input').attr('checked', true);
				$obj.addClass('checked');
			}
		});
		
		//表头效果
		if($('.list_menu').length){
			$('.r_con_wrap').scroll(function(){
				frame_obj.fixed_list_menu();
			});
			$(document).ready(function(){
				frame_obj.fixed_list_menu();
			});
		}
		
		//搜索框
		if($('.list_menu .search_form .ext>div').size()){
			$('.list_menu .search_form .more').click(function(){
				if($('.list_menu .search_form .ext').is(':hidden')){
					$('.list_menu .search_form .ext').show();
					$('.list_menu .search_form form').css('border-radius', '5px 5px 0 0');
					$('.list_menu .search_form .more').addClass('more_up');
				}else{
					$('.list_menu .search_form .ext').hide();
					$('.list_menu .search_form form').css('border-radius', '5px');
					$('.list_menu .search_form .more').removeClass('more_up');
				}
			});
		}else{
			$('.list_menu .search_form .more').remove();
			$('.list_menu .search_form .form_input').addClass('long_form_input');
		}
		
		//表格更多下拉效果
		$('.inside_table .more').parent().hover(function(){
			$(this).find('.more_menu').show().stop(true).animate({'top':31, 'opacity':1}, 250);
		}, function(){
			$(this).find('.more_menu').show().stop(true).animate({'top':21, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
		$('.r_con_table .operation dl').hover(function(){
			$(this).find('dd').show().stop(true).animate({'top':20, 'opacity':1}, 250);
		}, function(){
			$(this).find('dd').show().stop(true).animate({'top':10, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
		
		//自定义下拉
		$('body').on('click', '.down_select_box dt', function(){
			$(this).addClass('box_drop_focus');
			$(this).next('dd').toggle();
			return false;
		}).on('click', '.down_select_box .select', function(){
			var value=$(this).attr('value');
			$(this).parents('dd').hide();
			if($(this).parents('.down_select_box').hasClass('edit_box')){
				$(this).parents('.down_select_box').find('dt input[type=text]').val($(this).text()).parent().find('input[type=hidden]').val(value);
			}else{
				$(this).parents('.down_select_box').find('dt>div>span').text($(this).text()).parent().find('input[type=hidden]').val(value);
			}
			return false;
		}).on('click', '.down_select_box .select i', function(){
			$(this).parent().remove();
			return false;
		}).on('click', '.down_select_box', function(){
			return false;
		});
		$(document).click(function(){
			$('.down_select_box dt').removeClass('box_drop_focus');
			$('.down_select_box dd').hide();
		});
		$('.down_select_box .select[selected]').each(function(){
			$(this).click();
		});

		//图片默认设置
		frame_obj.upload_img_init();
		frame_obj.upload_pro_img_init(1);
		
		//多功能选项框
		frame_obj.box_option_list();
		frame_obj.box_option_button_choice();
		
		//下拉文本两用
		frame_obj.box_drop_double();

		frame_obj.rows_input();
		
		//登录
		frame_obj.expire_login();
		
		//临时域名提示
		var domain=window.location.host;  //会用代理也会拿到当前地址栏域名
		if(domain.indexOf('myueeshop.com')!=-1 && !global_obj.getCookie('ueeshop_tmp_domain')){
			$.post('./', 'do_action=action.ueeshop_tmp_domain', function(data){
				if(data.ret==1){
					$('#div_mask, .win_alert').remove();//优先清空多余的弹出框
					global_obj.div_mask();
					var html='<div class="win_alert">';
						html+='<div class="win_close"><button class="close">x</button></div>';
						html+='<div class="win_tips">'+domain+data.msg+'</div>';
						html+='<div class="win_btns">';
							html+='<button class="btn btn_sure btn_once_sure">'+lang_obj.global.confirm+'</button>';
						html+='</div>';
					html+='</div>';
					$('body').prepend(html);
					$('.win_alert').css('width', '600px');
					$('.win_alert .win_tips').css('padding', '100px 30px 50px');
					$('.win_alert').css({left:$(window).width()/2-$('.win_alert').outerWidth(true)/2,top:'30%'});
					$('.win_alert').delegate('.close, .btn_sure', 'click', function(){
						$('.win_alert').remove();
						global_obj.div_mask(1);
					});
				}
			}, 'json');
		}


		//定时删除订单和产品
		if($('input:hidden[name=clear_products_recycle]').length){
			$.post('?', 'do_action=account.products_recycle_del', function(data){}, 'json');
		}
		if($('input:hidden[name=clear_orders_recycle]').length){
			$.post('?', 'do_action=account.orders_recycle_del', function(data){}, 'json');
		}

		//统计页面停留时间
		if($('input:hidden[name=StatisticalID]').length){
			var $id=$('input:hidden[name=StatisticalID]').val(),
				$url=location.href;
				var $setInterval = setInterval(function(){
					$.post('?', {'do_action':'action.manage_page_statistics', 'StatisticalID':$id, 'Url':$url}, function(data){
						if( data.ret==0 ) {
							clearInterval($setInterval);
						}
					}, 'json');
				}, 5000);
		}
		
	},
	
	translation_init:function(){ //翻译器
		var translation_get_chars=function(){
			$.get('./?do_action=action.translation_get_chars', function(data){
				data.ret==1 && $('#translation .t span').show().html('('+data.msg+')');
			}, 'json');
		}
		$('.btn_translation').click(function(e) {
			if(typeof(CKEDITOR)=='object'){
				for(var i in CKEDITOR.instances) CKEDITOR.instances[i].updateElement(); //更新编辑器内容
			}
			var o=$('#translation'), language_html='';
			frame_obj.pop_form(o, 0, 1);
			o.find('.r_con_table').css({margin:0, border:'none'});
			o.find('.t span').hide();
			o.find('.btn_submit').show();
			o.find('.btn_cancel').val(lang_obj.global.close).removeAttr('style');
			$('.tab_box_row .drop_down:first a').each(function(index, element){
				if(index==0){return;}
				language_html+='<td nowrap="nowrap"><span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="" value="'+$(this).attr('data-lang')+'"></span></span></td>';
			});
			o.find('.r_con_table tbody').html('');
			$('.rows.translation:visible').each(function(index, element){
				if($(this).hasClass('description_box')){ //产品编辑 详细介绍
					var title=$(this).find('.head_hide li:first>span').text();
				}else{ //其他
					var title=$(this).find('label').contents().filter(function(index, content){
						return content.nodeType===3;
					}).text();
				}
				var text=$(this).find('.tab_txt:first, .lang_txt:first').find('input, textarea').val();
				o.find('.r_con_table tbody').append('<tr><td>'+title+' <font class="fc_0">('+text.length+lang_obj.manage.translation.char+')</font></td>'+language_html+'</tr>');
				$(this).attr('translation', 1);
				if(text==''){
					o.find('.r_con_table tbody tr:last .input_checkbox_box').addClass('disabled no_checked');
					$(this).attr('translation', 0);	//不能进行翻译
				}else{
					$(this).find('.tab_txt, .lang_txt').not(':first').each(function(){
						var translation_over=$(this).attr('translation_over');
						if(translation_over!=1){
							var input=o.find('.r_con_table tbody tr:last .input_checkbox_box input[value='+$(this).attr('lang')+']');
							input.attr('checked', true).parents('.input_checkbox_box').addClass('checked');
						}
					});
				}
			});
			translation_get_chars();
		});
		$('#translation form').submit(function(){return false;});
		$('#translation .btn_submit').click(function(e){
			if($('.rows.translation[translation=1]').size()==0 || $('#translation .r_con_table tbody tr input:checked').size()==0){return;}	//没有内容可进行翻译
			var query_string='';
			$('.rows.translation:visible').each(function(index, element){
				var input_obj=$('#translation .r_con_table tbody tr:eq('+index+') input:checked');
				if($(this).attr('translation')!=1 || input_obj.size()==0){return;}
				var language='';
				input_obj.each(function(index, element){
					language+=$(this).val()+',';
				});
				var text=$(this).find('.tab_txt:first, .lang_txt:first').find('input, textarea').val();
				query_string+='&text['+index+']='+encodeURIComponent(text)+'&language['+index+']='+language;
			});
			$(this).attr('disabled', true);
			global_obj.win_alert_auto_close(lang_obj.manage.translation.in_translation, 'loading', -1, 0, 0);
			$.post('./', 'do_action=action.translation&'+query_string, function(data){
				if(data.ret==1){
					$.each(data.msg, function(index, value){
						$.each(value['msg'], function(i, n){
							var o=$('.rows.translation:visible').eq(i).find('.input .tab_txt[lang='+index+'], .input .lang_txt[lang='+index+']').attr('translation_over', data.msg[index]['ret']).find('input, textarea');
							o.val(n);
							$('#cke_'+o.attr('id')).size() && CKEDITOR.instances[o.attr('id')].setData(n);//更新编辑器内容
						});
					});
					var completed=1;
					$('#translation .r_con_table tbody input:checked').each(function(index, element){
						var o=$(this).parents('td');
						if(data.msg[$(this).val()]['ret']==1){
							o.html(lang_obj.manage.translation.translation_success);
						}else{
							completed=0;
							o.html(o.find('.input_checkbox_box').prop('outerHTML')+lang_obj.manage.translation.translation_fail);
						}
					});
					if(completed){
						$('#translation .btn_submit').hide();
						$('#translation .btn_cancel').val(lang_obj.manage.translation.translation_success).css({
							backgroundColor:'#ff6600',
							color:'#fff',
							border:'1px solid #ff6600'
						});
					}
					translation_get_chars();
					global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
				}
				$('#translation .btn_submit').attr('disabled', false);
			}, 'json');
		});
	},
	
	rows_input:function(){ //计算多语言表单长度
		$('.global_form .rows .input:visible').has('.lang_input').each(function(){
			var o=$(this);
			var input_width=Math.max(o.find('input').outerWidth(), o.find('textarea').outerWidth());
			var title_width=last_width=0;
			o.find('.lang_input b[class!=last]').each(function(){
				var w=$(this).outerWidth();
				w>title_width && (title_width=w);
			});
			title_width+=1;
			o.find('.lang_input b.last').each(function(){
				var w=$(this).outerWidth();
				w>last_width && (last_width=w);
			});
			last_width+=1;
			o.find('.lang_input').width(input_width+title_width+last_width);
			o.find('.lang_input b[class!=last]').width(title_width-(title_width?19:0));
			o.find('.lang_input b.last').width(last_width-(last_width?19:0));
			o.find('.lang_input input, .lang_input textarea').width(o.find('.lang_input').outerWidth()-o.find('.lang_input b[class!=last]').outerWidth()-o.find('.lang_input b.last').outerWidth()-24);
		});
	},
	
	//订单消息
	message_orders_init:function(){
		//后台账号信息下拉
		$('#header .user_info').hover(function(){
			$(this).find('dd').show().stop(true).animate({'top':40, 'opacity':1}, 250);
		}, function(){
			$(this).find('dd').stop(true).animate({'top':30, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
		
		//消息下拉
		$('#header .menu_down').hover(function(){
			$(this).find('.message_info').show().stop(true).animate({'top':40, 'opacity':1}, 250);
		}, function(){
			$(this).find('.message_info').stop(true).animate({'top':30, 'opacity':0}, 100, function(){ $(this).hide(); });
		});
		
		//订单消息
		frame_obj.pop_up($('#header .btn_orders_message'), '.pop_orders_message_list', 1, function(){
			var $Obj=$('.pop_orders_message_list');
			$Obj.find('.message_list').css('height', $Obj.outerHeight()-$Obj.find('.message_title').outerHeight());
			frame_obj.pop_up($Obj.find('.message_list li'), '.pop_orders_message', 2, function($This){
				var $Obj=$('.pop_orders_message'),
					$MId=$This.attr('data-id');
				$Obj.find('.message_dialogue_list').html(''); //清空对话栏
				$Obj.find('.message_dialogue').css('height', $Obj.outerHeight()-$Obj.find('.message_title').outerHeight()-$Obj.find('.message_bottom').outerHeight()-10);
				$.post('?', {'do_action':'action.message_orders_view', 'MId':$MId}, function(data){
					if(data.ret==1){
						var $Html='';
						$Obj.find('.message_title .title strong').html('No#'+data.msg.OId);
						$Obj.find('.message_title .email').html(data.msg.Email);
						for(k in data.msg.Reply){
							if(data.msg.Reply[k].UserId==0){ //回答
								$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
								$Html+=		'<div class="time">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}else{ //追问
								$Html+=	'<div class="dialogue_box dialogue_box_left clean">';
								$Html+=		'<div class="time">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}
						}
						$Obj.find('.message_dialogue_list').append($Html);
						$Obj.find('.message_dialogue').animate({scrollTop:$Obj.find('.message_dialogue_list .dialogue_box:last').offset().top}, 10); //自动滚动到最后一个消息
						$Obj.find('input[name=MId]').val($MId);
					}
				}, 'json');
			});
		});
		
		//订单消息回复图片上传
		frame_obj.mouse_click($('#MsgPicDetail .upload_btn, #MsgPicDetail .pic_btn .edit'), 'img', function($this){
			frame_obj.photo_choice_init('MsgPicDetail', '', 1);
		});
        
        //直接回车发送消息
        $('#message_orders_form textarea[name=Message]').on('keyup', function(e){
			var $Key=window.event?e.keyCode:e.which;
			if($Key==13){ //回车键
				$('#message_orders_form .btn_submit').click();
			}
            return false;
        });
		
		//订单消息提交回复
		frame_obj.submit_form_init($('#message_orders_form'), '', '', 0, function(data){
			if(data.ret==1){
				var $Obj=$('.pop_orders_message'),
					$Form=$('#message_orders_form'),
					$Pic=$('#MsgPicDetail .img');
					$Html='';
				$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
				$Html+=		'<div class="time">'+data.msg.Time+'</div>';
				$Html+=		'<div class="message">'+data.msg.Content+'</div>';
				if(data.msg.PicPath){
				$Html+=		'<div class="picture"><a href="'+data.msg.PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.PicPath+'" /><span></span></a></div>';
				}
				$Html+=	'</div>';
				$Html+=	'<div class="clear"></div>';
				$Obj.find('.message_dialogue_list').append($Html);
				$Obj.find('.message_dialogue').animate({scrollTop:$('.pop_orders_message .message_dialogue_list').outerHeight()}, 500); //自动滚动到最后一个消息
				$Form.find('textarea[name=Message]').val('');
				$Pic.removeClass('isfile').removeClass('show_btn');
				$Pic.find('.preview_pic .upload_btn').show();
				$Pic.find('.preview_pic a').remove();
				$Pic.find('.preview_pic input:hidden').val('').attr('save', 0);
				global_obj.win_alert_auto_close(lang_obj.manage.email.send_success, '', 1000, '8%');
			}
			return false;
		});
	},
	
	//客服专员和备份时间
	windows_init:function(){
		var $user_service_status=0;
		$("#header").on("mouseover", ".user_service", function(){
			// if($user_service_status<2){
			// 	$.post('?', 'do_action=account.ueeshop_web_get_service_data', function(data){
			// 		if(data.ret==1){
			// 			$('.menu_agent .message_info').show();
			// 			if(data.msg.trial==1){
			// 				$user_service_status++;
			// 				var html='';
			// 				html+='<li>';
			// 					html+='<div class="tit">'+lang_obj.manage.frame.support[0]+'</div>';								
			// 					html+='<div class="desc">'+data.msg.service.QQ+'</div>';
			// 				html+='</li>';
			// 				html+='<li>';
			// 					html+='<div class="tit">'+lang_obj.manage.frame.support[2]+'</div>';								
			// 					html+='<div class="desc">'+data.msg.service.Telephone+'</div>';
			// 				html+='</li>';
			// 				html+='<li>';
			// 					html+='<div class="tit">'+lang_obj.manage.frame.support[3]+'</div>';								
			// 					html+='<div class="desc">'+data.msg.service.Wechat+'</div>';
			// 				html+='</li>';
			// 				html+='<li>';
			// 					html+='<div class="tit">'+lang_obj.manage.frame.support[4]+'</div>';								
			// 					html+='<div class="desc">'+data.msg.service.Complain+'</div>';
			// 				html+='</li>';
			// 				html+='<li>';
			// 					html+='<div class="tit">'+lang_obj.manage.frame.support[5]+'</div>';								
			// 					html+='<div class="desc">'+data.msg.expired+'</div>';
			// 				html+='</li>';
			// 				$('.menu_agent .msg_box ul').html(html);
			// 				$('.menu_agent .msg_box .pic img').attr('src','');
			// 				$('.menu_agent .msg_box .name').html(data.msg.service.Contacts);
			// 				$('.menu_agent .msg_box .email').html(data.msg.service.Email);
			// 				$('.menu_agent .msg_box .work_time').html(data.msg.service.WorkTime);
			// 			}else{
			// 				$('.menu_agent .message_info').hide();
			// 			}
			// 		}else{
			// 			$('.menu_agent .message_info').hide();
			// 		}
			// 	},'json');
			// }
		}).on("click", ".user_backup", function(){
			if(!$("#user_backup_box").length){
				$.post('?', 'do_action=account.ueeshop_web_get_service_data', function(data){
					if(data.ret==1){
						if(data.msg.trial==1){
							var html='';
							global_obj.div_mask();
							html+='<div id="user_backup_box">';
								html+='<div class="chat_title"><h3>'+lang_obj.manage.frame.data_backup+'</h3><button class="close">X</button></div>';
								html+='<div class="chat_container service_backup clean">';
									html+='<dl class="backup_box fl">';
										html+='<dt>'+lang_obj.manage.frame.backup[0]+'</dt>';
										html+='<dd>';
											html+='<div class="row">';
												html+='<div class="item">'+lang_obj.manage.frame.time+'</div>';
												html+='<div class="item">'+lang_obj.manage.frame.status+'</div>';
											html+='</div>';
											//for($i=0; $i<7; ++$i){
												html+='<div class="row">';
													html+='<div class="item">'+data.msg.backup+'</div>';
													html+='<div class="item"></div>';
												html+='</div>';
											//}
										html+='</dd>';
									html+='</dl>';
									html+='<dl class="backup_box fl">';
										html+='<dt>'+lang_obj.manage.frame.backup[1]+'</dt>';
										html+='<dd>';
											html+='<div class="row">';
												html+='<div class="item">'+lang_obj.manage.frame.time+'</div>';
												html+='<div class="item">'+lang_obj.manage.frame.status+'</div>';
											html+='</div>';
											//for($i=0; $i<7; ++$i){
												html+='<div class="row">';
													html+='<div class="item">'+data.msg.backup+'</div>';
													html+='<div class="item"></div>';
												html+='</div>';
											//}
										html+='</dd>';
									html+='</dl>';
								html+='</div>';
							html+='</div>';
							$('body').prepend(html);
							$('#user_backup_box').fadeIn().css({left:$(window).width()/2-332});
							
							$("#div_mask, #user_backup_box .close").on("click", function(){
								$("#user_backup_box").fadeOut().remove();
								global_obj.div_mask(1);
							});
						}
					}
				},'json');
			}
		}).on('click', '.menu_agent .agent_menu>a', function(){
			var _ind=$(this).index();
			$('.menu_agent .agent_menu>a').removeClass('cur').eq(_ind).addClass('cur');
			$('#header .agent_box .agent_list').hide().eq(_ind).show();
		}).on('click', '.menu_agent .iknow', function(){
			$('.menu_agent').fadeOut('1000');
			$.post('?do_action=account.update_version_status');
		});

		$(window).on('resize', function(){
			if($("#user_service_box:visible").length){
				$('#user_service_box').css({left:$(window).width()/2-332});
			}
			if($("#user_backup_box:visible").length){
				$("#user_backup_box").css({left:$(window).width()/2-332});
			}
		});
	},
	
	category_wrap_page_init:function(){
		var resize=function(){
			$('#photo .wrap_content').css({'overflow':'auto', 'height':($(window).height()-$('.list_menu').outerHeight(true)-40)});
		};
		resize();
		$(window).resize(function(){resize();});
	},
	
	submit_form_init:function(o, jump, fun, debug, callback, is_pop){
		if(!o.length) return false;
		frame_obj.check_amount(o); //检查价格
		o.find('input[rel=digital]').keydown(function(e){
			var value=$(this).val(),
				key=window.event?e.keyCode:e.which;
			if((key>95 && key<106) || (key>47 && key<60)){
				value==0 && $(this).val('');
			}else if(key!=8){
				if(window.event){//IE
					e.returnValue=false;
				}else{//Firefox
					e.preventDefault();
				}
				return false;
			}
		});
		if(o.find('.btn_submit').length){ //表单自身是否包含了提交按钮
			var obj=o.find('.btn_submit');
		}else{ //提交按钮在定位栏显示
			var obj=$('.fixed_btn_submit .btn_submit');
		}
		o.submit(function(){return false;});
		obj.click(function(){
			if($.isFunction(fun) && fun()===false){ return false; };
			if(global_obj.check_form(o.find('*[notnull]'), o.find('*[format]'), 1)){ return false; };
			if(typeof(CKEDITOR)=='object'){
				for(var i in CKEDITOR.instances) CKEDITOR.instances[i].updateElement();//更新编辑器内容
			}
			$(this).attr('disabled', true);
			console.log(callback);
            $.post('?', o.serialize(), function(data){
				if($.isFunction(callback)){
					callback(data);
					obj.attr('disabled', false);
				}else{
					if(debug){
						obj.attr('disabled', false);
						alert(unescape(data.replace(/\\/g, '%')));
					}else if(data.ret==1){
						if(data.msg.jump){
							window.location=data.msg.jump;
						}else if(data.msg){
							obj.attr('disabled', false);
							//global_obj.win_alert(data.msg, '', 'alert', is_pop);
							global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
						}else if(jump){
							window.location=jump;
						}else{
							window.location.reload();
						}
					}else{
						obj.attr('disabled', false);
						//global_obj.win_alert(data.msg, '', 'alert', is_pop);
						global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
					}
				}
			}, debug?'text':'json');
		});
	},
	
	switchery_checkbox:function(confirmBind, cancelBind, obj){
		var $obj = $('.switchery');
		if (obj) {
			$obj = $(obj);
		}
		$obj.on('click', function(){
			if($(this).hasClass('checked')){
				$(this).removeClass('checked').find('input').attr('checked', false);
				cancelBind && cancelBind($(this));
			}else{
				$(this).addClass('checked').find('input').attr('checked', true);
				confirmBind && confirmBind($(this));
			}
		});
	},

	config_switchery:function(obj, do_action, param_0, param_1, callback){
		obj.click(function(){
			var $this=$(this),
				$data_value=$this.attr(param_0),
				$used,
				$data=new Object;
				if($this.hasClass('checked')){
					$used=0;
					$this.removeClass('checked');
				}else{
					$used=1;
					$this.addClass('checked');
				}
				$data[param_1]=$data_value;
				$data['IsUsed']=$used;
				$.post('?do_action='+do_action, $data, function(data){
					if(!data.ret){
						global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
						if($used){
							$this.removeClass('checked');
						}else{
							$this.addClass('checked');
						}
					}else if(data.ret==1){
						global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
					}
					if($.isFunction(callback)){
						callback(data);
						return false;
					}
				}, 'json');
		});
	},
	
	select_all:function(checkbox_select_btn, checkbox_list, process_obj, callback){
		var process=function(){
			if(checkbox_list.parent('.current').length>0){
				process_obj.css('display', 'block');
			}else{
				process_obj.css('display', 'none');
			}
		}
		checkbox_select_btn.parent().on('click', function(){ //全选
			var $isChecked=checkbox_select_btn.is(':checked')?1:0;
			checkbox_list.each(function(){
				if($isChecked==1){ //勾选
					$(this).attr('checked', true);
					$(this).parent().addClass('current');
				}else{ //取消
					$(this).removeAttr('checked');
					$(this).parent().removeClass('current');
				}
            });
			if($.isFunction(callback)){
				callback(checkbox_select_btn, checkbox_list);
			}else{
				// process();
			}
		});
		checkbox_list.parent().on('click', function(){ //部分勾选
			if(checkbox_list.parent('.current').length==checkbox_list.parent().length){
				checkbox_select_btn.attr('checked', true);
				checkbox_select_btn.parent().addClass('current');
			}else{
				checkbox_select_btn.removeAttr('checked');
				checkbox_select_btn.parent().removeClass('current');
			};
			if($.isFunction(callback)){
				callback(checkbox_select_btn, checkbox_list);
			}else{
				// process();
			}
		});	
	},
	
	del_bat:function(btn_del_bat, checkbox_list, do_action, callback, alert_txt, confirm_txt){ 
		btn_del_bat.on('click', function(){
			var id_list='';
			checkbox_list.each(function(index, element) {
				id_list+=$(element).get(0).checked?$(element).val()+'-':'';
            });
			if(id_list){
				id_list=id_list.substring(0,id_list.length-1);
				if($.isFunction(callback)){
					callback(id_list);
				}else{
					global_obj.win_alert(confirm_txt?confirm_txt:lang_obj.global.del_confirm, function(){
						$.get('?', {do_action:do_action, id:id_list}, function(data){
							if(data.ret==1){
								window.location.reload();
							}
						}, 'json');
					}, 'confirm');
				}
			}else{
				global_obj.win_alert(alert_txt?alert_txt:lang_obj.global.del_dat_select);
			}
		});
	},
	
	del_init:function(o){
		o.find('.del').click(function(){
			var o=$(this);
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.get(o.attr('href'), function(data){
					if(data.ret==1){
						window.location.reload();
					}else{
						global_obj.win_alert(data.msg);
					}
				}, 'json');
			}, 'confirm');
			return false;
		});
	},
	
	dragsort:function(obj, do_action, dragSelector, dragSelectorExclude, placeHolderTemplate, itemSelector){
		typeof(dragSelector)=='undefined' && (dragSelector='tr');
		typeof(dragSelectorExclude)=='undefined' && (dragSelectorExclude='a, td[data!=move_myorder]');
		typeof(placeHolderTemplate)=='undefined' && (placeHolderTemplate='<tr class="placeHolder"></tr>');
		typeof(itemSelector)=='undefined' && (itemSelector='');
		obj.dragsort({
			dragSelector:dragSelector,
			dragSelectorExclude:dragSelectorExclude,
			placeHolderTemplate:placeHolderTemplate,
			itemSelector:itemSelector,
			scrollSpeed:5,
			dragEnd:function(){
				if(do_action){
					var target=(itemSelector?itemSelector:dragSelector);
					var data=obj.find(target).map(function(){
						return $(this).attr('data-id');
					}).get();
					$.get('?', {do_action:do_action, sort_order:data.join(',')});
				}
			}
		});
	},
	
	fixed_list_menu:function(){
		var $ScrollTop=$('.r_con_wrap').scrollTop(),
			$Obj=$('.list_menu'),
			$SideTop=$Obj.offset().top,
			$SideHeight=$Obj.outerHeight(),
			$BoxWidth=$Obj.width(),
			$BoxHeight=$Obj.outerHeight(true),
			$BoxTop=$('#header').outerHeight(true),
			$BoxLeft=$Obj.offset().left,//$('#main>.menu').outerWidth(),
			$HeaderHeight=$('#header').outerHeight(true);
			if($Obj.hasClass('no_fixed')) return false;
		if($ScrollTop>($SideTop+$BoxHeight)){
			if(!$('.list_menu_support_div').length) $Obj.after('<div class="list_menu_support_div"></div>');
			$('.list_menu_support_div').height($BoxHeight);
			$Obj.css({'width':$BoxWidth, 'position':'fixed', 'top':$BoxTop, 'left':$BoxLeft});
			$Obj.addClass('fixed');
			$('.inside_table').css({'padding-top':$SideHeight});
		}else{
			$('.list_menu_support_div').remove();
			$Obj.removeAttr('style');
			$Obj.removeClass('fixed');
			$('.inside_table').removeAttr('style');
		}
	},
	
	fixed_right_div_mask:function(remove){
		if(remove==1){
			$('#fixed_right_div_mask').remove();
		}else{
			if(!$('#fixed_right_div_mask').size()){
				$('body').prepend('<div id="fixed_right_div_mask"></div>');
				$('#fixed_right_div_mask').show();
			}
		}
	},
	
	fixed_right:function(click_obj, target_class){
		var fun=(typeof(arguments[2])=='undefined')?'':arguments[2];
		click_obj.off().on('click', function(){
			$('#fixed_right').addClass('show').find(target_class).show().siblings().hide();
			frame_obj.fixed_right_div_mask();
			if($.isFunction(fun)){ fun($(this)); }
		});
		$('#fixed_right').off().on('click', '.close, .btn_cancel', function(){
			$('#fixed_right').removeClass('show').find('.global_container').hide();
			frame_obj.fixed_right_div_mask(1);
		});
		$('body').on('click', '#fixed_right_div_mask', function(){
			$('#fixed_right').removeClass('show').find('.global_container').hide();
			frame_obj.fixed_right_div_mask(1);
		});
	},
	
	pop_up:function(click_obj, target_class){
		var number=(typeof(arguments[2])=='undefined')?1:arguments[2],
			fun=(typeof(arguments[3])=='undefined')?'':arguments[3];
		click_obj.off().on('click', function(){
			$(target_class).addClass('show');
			number==1 && frame_obj.fixed_right_div_mask();
			if($.isFunction(fun)){ fun($(this)); }
		});
		if(number==1){ //第一层
			$(target_class).off().on('click', '.btn_close, .btn_cancel', function(){
				$(target_class).removeClass('show');
				frame_obj.fixed_right_div_mask(1);
			});
		}else{
			$(target_class).off().on('click', '.btn_close, .btn_cancel', function(){
				$(target_class).removeClass('show');
			});
		}
		$('body').on('click', '#fixed_right_div_mask', function(){
			$(target_class).removeClass('show');
			frame_obj.fixed_right_div_mask(1);
		});
	},
	
	pop_form:function(obj, remove, no_mask){ //弹出编辑框
		if(remove==1){
			obj.slideUp(250, function(){
				no_mask==1 && global_obj.div_mask(1);
			});
		}else{
			global_obj.div_mask();
			if(obj.hasClass('photo_choice')){ //图片银行
				obj.css({'top':0, 'opacity':0}).show().animate({'top':30, 'opacity':1}, 250);
			}else if(obj.hasClass('gallery_pop')){  //图库
				obj.css({'top':0, 'opacity':0}).show().animate({'top':50, 'opacity':1}, 250);
			}else{
				obj.css({'top':110, 'opacity':0}).show().animate({'top':150, 'opacity':1}, 250);
			}
			if($(document).height()<=680){
				obj.css('top', 70);
				obj.find('.r_con_form').css({'max-height':350});
			}
			obj.find('.t h2').add(obj.find('.btn_cancel')).click(function(){
				if(obj.hasClass('photo_choice')){ //图片银行
					obj.slideUp(250, function(){
						no_mask==1 && global_obj.div_mask(1);
					});
				}else{
					obj.fadeOut(250, function(){
						no_mask==1 && global_obj.div_mask(1);
					});
				}
			});
		}
	},
	
	pop_iframe:function(url, title, no_mask, callback){ //弹出框架显示框
		var o, $html='';
		$html+='<div class="pop_form pop_iframe">';
			$html+='<form id="pop_iframe_form" class="w_1000">';
				$html+='<div class="t"><h1>'+title+'</h1><h2>×</h2></div>';
				$html+='<div class="r_con_form"><iframe src="" frameborder="0"></iframe></div>';
			$html+='</form>';
		$html+='</div>';
		$('.r_con_wrap').append($html);
		o=$('.pop_iframe');
		frame_obj.pop_form(o, 0, no_mask, callback);
		setTimeout(function(){
			o.find('iframe').attr('src', url+'&iframe=1&r='+Math.random()).load(function(){ //效果执行完，才加载内容显示
				iframe_resize();
			});
		}, 200);
		
		var resize=function(){
			var $h=$(window).height()-o.find('form>.t').outerHeight()-70;
			//o.css('top', 30).find('.r_con_form').css({'height':$h, 'max-height':$h});
			o.find('.r_con_form').css({'height':$h, 'max-height':$h});
		}, iframe_resize=function(){
			var $h=$(window).height()-o.find('form>.t').outerHeight()-70,
				$iframe=o.find('iframe').contents();
				$iframe.find('.r_con_wrap').css({'overflow':'auto', 'height':($h-10)});
				$iframe.find('.menu_list').height($iframe.find('.r_con_wrap').height()-$iframe.find('.nav_list').outerHeight(true)-10).jScrollPane();
				$iframe.find('.shipping_area_edit').height($iframe.find('.r_con_wrap').height()-$iframe.find('.nav_list').outerHeight(true)-8);
		}
		resize();
		$(window).resize(function(){
			resize();
			if(o.find('.r_con_form').outerHeight()>300){ //已经固定min-height为300px
				iframe_resize();
			}
		});
		
		o.find('.t h2').add(o.find('.btn_cancel')).click(function(){ //取消
			frame_obj.pop_form(o, 1, no_mask, callback);
			o.remove();
		});
	},
	
	upload_img_detail:function(img){
		if(!img){return;}
		var del=(typeof(arguments[1])=='undefined')?'':arguments[1];
		return '<a href="javascript:;"><img src="'+img+'"><em></em></a>';
	},
	
	pop_contents_close_init:function(o, remove, no_mask, callback){
		if(callback) eval(callback); //执行回调函数
		/*
		if(no_mask==1){ //关闭遮罩层
			global_obj.div_mask(1);
		}
		*/
		frame_obj.pop_form(o, 1, no_mask);
		remove==1?o.remove():o.hide();
	},
	
	photo_choice_iframe_init:function(url, id, no_mask, callback){
		var $Obj, $html='';
		$html+='<div class="pop_form photo_choice">';
			$html+='<form id="photo_choice_edit_form">';
				$html+='<div class="t"><h1>'+lang_obj.manage.photo.picture_upload+'</h1><h2>×</h2></div>';
				$html+='<div class="r_con_form"><iframe src="" frameborder="0"></iframe></div>';
				$html+='<div class="button"><input type="submit" class="btn_global btn_submit" id="button_add" name="submit_button" value="'+lang_obj.global.submit+'" /><input type="button" class="btn_global btn_cancel" value="'+lang_obj.global.cancel+'" /></div>';
			$html+='</form>';
		$html+='</div>';
		$('#righter').append($html);
		$Obj=$('.photo_choice');
		frame_obj.pop_form($Obj, 0, no_mask, callback);
		setTimeout(function(){
			$Obj.find('iframe').attr('src', url+'&iframe=1&r='+Math.random()); //效果执行完，才加载内容显示
		}, 200);
		
		var resize=function(){
			var $h=$(window).height()-$Obj.find('form>.t').outerHeight()-$Obj.find('form>.button').outerHeight()-70;
			$Obj.find('.r_con_form').css({'height':$h, 'max-height':$h});
		}
		resize();
		$(window).resize(function(){resize();});

		$Obj.find('#button_add').click(function(){ //提交
			var obj=$Obj.find('iframe').contents(),
				id=obj.find('input[name=id]').val(),//显示元素的ID
				type=obj.find('input[name=type]').val(),//类型
				maxpic=obj.find('input[name=maxpic]').val();//最大允许图片数
			frame_obj.photo_choice_return(id, type, maxpic);
			return false;
		});
		$Obj.find('.t h2').add($Obj.find('.btn_cancel')).click(function(){ //取消
			frame_obj.pop_contents_close_init($Obj, 1, no_mask, callback);
		});
	},
	
	/*
	 *	@param:	id			保存图片隐藏域(单图：id值；多图：name值)
	 *	@param:	type		类型，例如：editor即添加到编辑器
	 *	@param:	maxpic		最大允许图片数
	 *	@param:	del_url		删除图片地址('./?'+del_url+'&...')
	 *	@param:	no_mask		执行完过后，是否需要关闭遮罩层
	 *	@param:	callback	回调函数
	 */
	photo_choice_init:function(id, type, maxpic, del_url, no_mask, callback){
		var del_url=(typeof(arguments[3])=='undefined')?'':arguments[3],
			no_mask=(typeof(arguments[4])=='undefined')?1:arguments[4],	//默认关闭遮罩层
			callback=(typeof(arguments[5])=='undefined')?'':arguments[5],
			html='';
		maxpic==null && (maxpic=-1);
		type=='editor' && (maxpic=9999); //编辑器上传没上限
		if(ueeshop_config.u_file_size>1024 && ueeshop_config.FunVersion==10){
			window.parent.global_obj.new_win_alert(lang_obj.manage.global.u_file_size_2big);
		}else{
			frame_obj.photo_choice_iframe_init('./?m=set&a=photo&d=choice&id='+id+'&type='+type+'&maxpic='+maxpic, 'photo_choice', no_mask, callback);
			html='<input type="hidden" class="del_url" value="'+del_url+'" /><input type="hidden" class="callback" value="'+callback+'" /><input type="hidden" class="no_mask" value="'+no_mask+'" />';
			$('.photo_choice').append(html);
		}
	},
	
	photo_choice_return:function(id, type){
		var maxpic=(typeof(arguments[2])=='undefined')?'':arguments[2],		//最大允许图片数
			num=(typeof(arguments[3])=='undefined')?'':arguments[3],		//类型(本地上传/图片银行)
			imgpath=(typeof(arguments[4])=='undefined')?'':arguments[4],	//已上传图片地址
			surplus=(typeof(arguments[5])=='undefined')?0:arguments[5],		//剩余图片数
			number=(typeof(arguments[6])=='undefined')?1:arguments[6],		//当前图片序号
			img_name=(typeof(arguments[7])=='undefined')?1:arguments[7],	//当前图片名称
			del_url=parent.$('.photo_choice .del_url').val(),
			callback=parent.$('.photo_choice .callback').val(),
			no_mask=parent.$('.photo_choice .no_mask').val();
		id=id.replace(';', '=');
		if(type=='editor' || type=='isspecial'){ // isspecial 需要特殊处理 :
			id=id.replace(':', '\:');
		}else{
			id=id.replace(':', '\\:');
		}
		var error_img = $("input[name='file2BigName_hidden_text']").val();
		var big_pic_size;
		if(!surplus){
			big_pic_size = $('.fileupload-buttonbar .classIt').length;
		}
		if(num){
			function isHasImg(pathImg){
				if(pathImg.length==0){
					return false;
				}
				var isExist=true;
				$.ajax(pathImg, {
					type: 'get',
					async:false,//取消ajax的异步实现
					timeout: 1000,
					success: function() {
					},
					error: function() {
						isExist = false;  
					}
				});
				return isExist;
			}
			/* 本地上传 */
			if(isHasImg(imgpath) || (type=='editor' && big_pic_size)){//防止代码自动执行
				if(type!='editor' && parent.$('#'+id+' div').size()>=parseInt(maxpic)){
					global_obj.win_alert(lang_obj.manage.account.picture_tips.replace('xxx', maxpic), function(){
						parent.frame_obj.pop_contents_close_init(parent.$('.photo_choice'), 1, no_mask);
					}, '', no_mask);
					return;
				}
				if(type=='editor'){//编辑框
					$('.fileupload-buttonbar').find('[name=Name\\[\\]][value="'+img_name+'"]').attr('picpath',imgpath);
					if(!surplus){
						$('.fileupload-buttonbar').find('[name=Name\\[\\]]').each(function(){
							var imgpath_list = $(this).attr('picpath');
							var obj=parent.CKEDITOR.instances[id].insertHtml('<img src="'+imgpath_list+'" />');//向编辑器增加内容
						});
					}
				}else{
					var obj=parent.$('#'+id);
					if(number==1){//优先上传第一张图片
						obj.find('.pic_btn .zoom').attr('href',imgpath);
						obj.find('.preview_pic a').remove();
						obj.find('.preview_pic').append(frame_obj.upload_img_detail(imgpath)).children('.upload_btn').hide().parent().parent().addClass('isfile').removeClass('show_btn');
						obj.find('.preview_pic').children('input[type=hidden]').val(imgpath).attr('save', 1).trigger('change');
					}else{
						callback && eval(callback);
						var oth_obj='', i;
						for(i=0; i<maxpic; ++i){
							oth_obj=obj.parents('.multi_img').find('.img[num='+i+']');
							if(imgpath && oth_obj.find('input[type=hidden]').attr('save')==0){
								oth_obj.find('.pic_btn .zoom').attr('href',imgpath);
								oth_obj.find('.preview_pic').append(frame_obj.upload_img_detail(imgpath)).children('.upload_btn').hide().parent().parent().addClass('isfile').removeClass('show_btn');
								oth_obj.find('.preview_pic').children('input[type=hidden]').val(imgpath).attr('save', 1);
								break;
							}
						}
					}
				}
				callback && eval(callback);
				//if(!surplus) parent.frame_obj.pop_contents_close_init(parent.$('.photo_choice'), 1, no_mask);//最后一张，自动关闭
				imgpath && parent.global_obj.win_alert_auto_close(lang_obj.manage.photo.upload_success, '', 1000, '8%');
			}
			if(!surplus){
				$(".photo_multi_img .classIt").remove();
				$("input[name='file2BigName_hidden_text']").val(" ");
				error_img && window.parent.global_obj.win_alert(lang_obj.manage.global.photo_gt_2m+error_img,'',"alert");
				parent.frame_obj.pop_contents_close_init(parent.$('.photo_choice'), 1, no_mask);//最后一张，自动关闭
			}	
		}else{
			/* 从图片银行复制 */
			//$.post('./', $('#photo_list_form').serialize()+'&type='+type, function(data){
			$.post('./', $('.photo_choice').find('iframe').contents().find('#photo_list_form').serialize()+'&type='+type, function(data){
				$('#button_add').attr('disabled', 'disabled');
				if(data.ret!=1){
					//parent.global_obj.win_alert(data.msg);
					global_obj.win_alert(data.msg, function(){
						frame_obj.pop_contents_close_init($('.photo_choice'), 1, no_mask);
					}, '', callback);
					callback && eval(callback);
					return false;
				}else{
					if(data.type=='editor'){
						/* 编辑框 */
						var html='';
						var obj = parent.CKEDITOR.instances[id];
						for (var i in data.Pic){
							html += '<img src="'+data.Pic[i]+'" />';
						}
						obj.insertHtml(html);//向编辑器增加内容
					}else{
						var html='';
						var obj=parent.$('#'+id);
						//优先上传第一张图片
						obj.find('.pic_btn .zoom').attr('href',data.Pic[0]);
						obj.find('.preview_pic a').remove();
						obj.find('.preview_pic').append(frame_obj.upload_img_detail(data.Pic[0])).children('.upload_btn').hide().parent().parent().addClass('isfile').removeClass('show_btn');
						obj.find('.preview_pic').children('input[type=hidden]').val(data.Pic[0]).attr('save', 1).trigger('change');

						if(data.Pic.length>1){//选择多张执行下面代码
							var oth_obj='', i, j=1;
							for(i=0; i<maxpic; ++i){
								oth_obj=obj.parents('.multi_img').find('.img[num='+i+']');
								if(data.Pic[j] && oth_obj.find('input[type=hidden]').attr('save')==0){
									oth_obj.find('.pic_btn .zoom').attr('href',data.Pic[j]);
									oth_obj.find('.preview_pic').append(frame_obj.upload_img_detail(data.Pic[j])).children('.upload_btn').hide().parent().parent().addClass('isfile').removeClass('show_btn');
									oth_obj.find('.preview_pic').children('input[type=hidden]').val(data.Pic[j]).attr('save', 1);
									j++;
								}
							}
						}
						callback && eval(callback);
					}
				}
				frame_obj.pop_contents_close_init($('.photo_choice'), 1, no_mask);
			}, 'json');
			parent.global_obj.win_alert_auto_close(lang_obj.manage.photo.upload_success, '', 1000, '8%');
		}
		
	},
	
	ajax_photo_del:function(obj, id, Url){
		if(Url==''){return;}
		var $this=$(this);
		global_obj.win_alert(lang_obj.global.del_confirm, function(){
			$.ajax({
				url:'./?'+Url+'&Path='+$(obj).prev().attr('href')+'&Index='+$(obj).parent().index(),
				success:function(data){
					json=eval('('+data+')');
					$('#'+id+' div:eq('+json.msg[0]+')').remove();
				}
			});
		}, 'confirm');
		return false;
	},
	
	file_upload:function(file_input_obj, filepath_input_obj, img_detail_obj, size){
		var size=(typeof(arguments[3])=='undefined')?'':arguments[3];
		var multi=(typeof(arguments[4])=='undefined')?false:arguments[4];
		var queueSizeLimit=(typeof(arguments[5])=='undefined')?5:arguments[5];
		var callback=arguments[6];
		var fileExt=(typeof(arguments[7])=='undefined' || arguments[7]=='')?'*.jpg;*.png;*.gif;*.jpeg;*.ico;*.jp2':arguments[7];//;*.bmp
		var do_action=(typeof(arguments[8])=='undefined')?'action.file_upload':arguments[8];
		file_input_obj.omFileUpload({
			action:'./?session_id='+session_id,
			actionData:{
				do_action:do_action,
				size:size
			},
			fileExt:fileExt,
			fileDesc:'Files',
			autoUpload:true,
			multi:multi,
			queueSizeLimit:queueSizeLimit,
			swf:'/inc/file/fileupload.swf?r='+Math.random(),
			method:'post',
			buttonText:lang_obj.manage.frame.file_upload,
			onComplete:function(ID, fileObj, response, data, event){
				var jsonData=eval('('+response+')');
				if(jsonData.status==1){
					if($.isFunction(callback)){
						callback(jsonData.filepath, data.fileCount, jsonData.name);
					}else{
						filepath_input_obj.val(jsonData.filepath);
						img_detail_obj.html(frame_obj.upload_img_detail(jsonData.filepath));
					}
				}
			}
		});
	},
	
	/******************************旧版数据统计(start)******************************/
	chart:function(){
		$('.chart').height(frame_obj.chart_par.height).highcharts({
            chart:{
				type:frame_obj.chart_par.themes,
				backgroundColor:frame_obj.chart_par.bg
            },
            title:{text:''},
			tooltip: {
				shared: true,
				valueSuffix: frame_obj.chart_par.valueSuffix                   
			},
			xAxis:{categories:chart_data.date},
			yAxis:[{
				title:{text:''},
				min:0
			}],
			legend:frame_obj.chart_par.legend,
			plotOptions:{
				line:{
					dataLabels:{enabled:true},
					enableMouseTracking:false
				},
				bar:{
					dataLabels:{enabled:true}
				}
			},
			series:chart_data.count,
			exporting:{enabled:false}
        });
	},
	
	chart_pie:function(){
		$('.chart').height(500).highcharts({
			title:{text:''},
            credits:{enabled:false},
			tooltip:{
				pointFormat:'{series.name}: <b>{point.percentage:.2f}%</b>'
			},
			plotOptions:{
				pie:{
					allowPointSelect:true,
					cursor:'pointer',
					dataLabels:{
						enabled:true,
						color:'#000000',
						connectorColor:'#000000',
						format:'<b>{point.name}</b>: {point.percentage:.2f} %'
					}
				}
			},
			series:[{
				type:'pie',
				name:lang_obj.manage.account.percentage,
				data:user_area_data
			}]
		});
	},
	
	chart_par:{themes:'column',height:500,bg:'',legend:{},valueSuffix:''},
	/******************************旧版数据统计(end)******************************/
	
	prompt_steps:function(){
		if(!$('#prompt_steps_tips').length){
			var PromptContent='';
			PromptContent+='<div id="prompt_steps_bg"></div>';
			PromptContent+='<div id="prompt_steps_tips" status="0"><a href="javascript:;"><img src="/static/manage/images/prompt/prompt_steps_begin.png" /></a></div>';
			PromptContent+='<div id="prompt_steps_light"></div>';
			PromptContent+='<div id="prompt_steps_ico"><span>设置</span></div>';
			$('body').prepend(PromptContent);
			$('#prompt_steps_bg').css({'width':'100%', 'height':$(document).height(), 'overflow':'hidden', 'position':'fixed', 'top':0, 'left':0, 'background':'url(/static/manage/images/prompt/prompt_steps_bg.png) repeat', 'z-index':10000});
			$('#prompt_steps_ico').css({'position':'absolute', 'width':70, 'height':70, 'background-image':'url(/static/manage/images/frame/m-ico.png)', 'background-repeat':'no-repeat', 'background-color':'#53a18e', 'text-align':'center', 'display':'none', 'z-index':10001}).find('span').css({'padding-top':44, 'color':'#fff', 'display':'block'});
			$('#prompt_steps_light').css({'position':'absolute', 'width':70, 'height':70, 'background':'url(/static/manage/images/prompt/prompt_steps_light.png) repeat', 'display':'none', 'z-index':10002});
		}
		var $PromptTips=$('#prompt_steps_tips'),
			$PromptStatus=parseInt($PromptTips.attr('status')),
			$PromptIco=$('#prompt_steps_ico'),
			$PromptLight=$('#prompt_steps_light'),
			$PromptClick=$PromptTips.find('a');
		if($PromptStatus==0){
			$PromptTips.css({'position':'absolute', 'top':74, 'background':'url(/static/manage/images/prompt/prompt_steps_0.png) no-repeat', 'width':619, 'height':107, 'z-index':10003});
			$PromptTips.css({'left':$(window).width()/2-309.5});
			$PromptClick.css({'marginTop':75, 'marginRight':10, 'float':'right'});
		}else if($PromptStatus==1){
			$PromptClick.find('img').attr('src', '/static/manage/images/prompt/prompt_steps_know.png');
			$PromptTips.css({'left':61, 'top':90, 'background':'url(/static/manage/images/prompt/prompt_steps_1.png) no-repeat', 'width':383, 'height':249});
			$PromptIco.css({'display':'block', 'left':0, 'top':53});
			$PromptLight.css({'display':'block', 'left':0, 'top':53});
			$PromptClick.css({'marginTop':180, 'marginRight':33});
		}else if($PromptStatus==2){
			$PromptTips.css({'left':58, 'top':230, 'background':'url(/static/manage/images/prompt/prompt_steps_2.png) no-repeat', 'width':336, 'height':185});
			$PromptIco.css({'top':197, 'background-position':'-140px -140px'}).find('span').text('产品');
			$PromptLight.css({'top':197});
			$PromptClick.css({'marginTop':137, 'marginRight':20});
		}else if($PromptStatus==3){
			$PromptTips.css({'left':62, 'top':158, 'background':'url(/static/manage/images/prompt/prompt_steps_3.png) no-repeat', 'width':337, 'height':158});
			$PromptIco.css({'top':125, 'background-position':'-70px -70px'}).find('span').text('内容');
			$PromptLight.css({'top':125});
			$PromptClick.css({'marginTop':104, 'marginRight':33});
		}else if($PromptStatus==4){
			$('#header ul li.ico-0').removeClass('current');
			$PromptLight.css({'background':'url(/static/manage/images/prompt/prompt_steps_light_to.png) repeat'});
			$PromptTips.css({'left':'auto', 'right':103, 'top':43, 'background':'url(/static/manage/images/prompt/prompt_steps_4.png) no-repeat', 'width':313, 'height':172});
			$PromptIco.css({'left':'auto', 'right':384, 'top':0, 'background-image':'url(/static/manage/images/frame/h-ico.png)', 'background-position':'0 -51px', 'background-color':'#e7e7e7', 'width':64, 'height':51}).find('span').text(' ');
			$PromptLight.css({'left':'auto', 'right':384, 'top':0, 'width':64, 'height':51});
			$PromptClick.css({'marginTop':111, 'marginRight':38});
		}else if($PromptStatus==5){
			$('#header ul li.ico-0').addClass('current');
			$PromptTips.css({'left':'auto', 'right':31, 'top':43, 'background':'url(/static/manage/images/prompt/prompt_steps_5.png) no-repeat', 'width':349, 'height':94});
			$PromptIco.css({'left':'auto', 'right':127, 'background-position':'0 -357px'});
			$PromptLight.css({'left':'auto', 'right':127});
			$PromptClick.css({'marginTop':49, 'marginRight':38});
		}else{
			$('#prompt_steps_bg, #prompt_steps_tips, #prompt_steps_ico, #prompt_steps_light').remove();
		}
		$PromptClick.on('click', function(){
			$PromptTips.attr('status', $PromptStatus+1);
			frame_obj.prompt_steps();
		});
	},
	
	mouse_click:function(o, name, fun){ //兼容个别浏览器，点击事件与拖动效果相互冲突
		var $obj={};
		$obj[name+'_hasMove'] = false;
		$obj[name+'_obj'] = {};
		$obj[name+'_end'] = 1;
		o.on('mousedown', function(e){ //按下鼠标键事件
			$obj[name+'_obj'].x = e.pageX;
			$obj[name+'_obj'].y = e.pageY;
			$obj[name+'_end'] = 0;
			$obj[name+'_hasMove'] = false;
		}).on('mousemove', function(e){ //移动鼠标键事件
			return false; //临时
			if($obj[name+'_end'] == 1) return false;
			if(e.pageX === $obj[name+'_obj'].x && e.pageY === $obj[name+'_obj'].y) {
				$obj[name+'_hasMove'] = false;
			}else{
				$obj[name+'_hasMove'] = true;
			}
		}).on('mouseup', function(e){ //松开鼠标键事件
			$obj[name+'_obj'].x = e.pageX;
			$obj[name+'_obj'].y = e.pageY;
			$obj[name+'_end'] = 1;
			if(!$obj[name+'_hasMove']){ //没有移动过当前元素
				$.isFunction(fun) && fun($(this));
			}
			$obj[name+'_hasMove'] = false;
		});
	},

	upload_img_init:function(){
		//图片上传默认设置
		$('.multi_img .preview_pic input:hidden').each(function(){
			if($(this).attr('save')==1){
				$(this).parent().append(frame_obj.upload_img_detail($(this).val())).children('.upload_btn').hide();
			}
		});
		frame_obj.mouse_click($('.multi_img .pic_btn .del'), 'Del', function($this){ //产品颜色图点击事件
			var $obj=$this.parents('.img');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$obj.removeClass('isfile').removeClass('show_btn').parent().append($obj);
				$obj.find('.pic_btn .zoom').attr('href','javascript:;');
				$obj.find('.preview_pic .upload_btn').show();
				$obj.find('.preview_pic a').remove();
				$obj.find('.preview_pic input:hidden').val('').attr('save', 0).trigger('change');
			}, 'confirm');
		});
	},
	
	//产品图片上传默认设置
	upload_pro_img_init:function(type){
		var obj=(typeof(arguments[1])=='undefined')?'':arguments[1],
			isParent=(typeof(arguments[2])=='undefined')?0:parseInt(arguments[2]),
			o=new Object;
		if(obj){
            obj=obj.replace('ADD:', 'ADD\\:');
			if(isParent==1){ o=parent.$(obj).find('.img'); }
			else{ o=$(obj).find('.img'); }
		}else{
			if(isParent==1){ o=parent.$('.pro_multi_img .img'); }
			else o=$('.pro_multi_img .img');
		}
		if(type==1){ //从左到右
			o.each(function(){
				if(!$(this).hasClass('isfile')){
					$(this).addClass('show_btn');
					return false;
				}
			});
		}else{ //从右到左
			o.each(function(){
				if($(this).hasClass('isfile')){
					$(this).prev().addClass('show_btn'); //上一个显示
					return false;
				}
				if($(this).index()+1==$(this).parent().find('.img').size()){ //最后一个，还没有关联图片可以显示
					$(this).addClass('show_btn');
				}
			});
		}		
	},
	
	//多功能选项框
	box_option_list:function(){
		//全选按钮
		$('.box_option_list .select_all').off().on('click', function(){
			var $Obj=$(this).parents('.box_option_list'),
				$DataType=$Obj.find('.option_button_menu a.current').attr('data-type');
			$Obj.find('.option_not_yet .btn_attr_choice[data-type='+$DataType+']').click();
			return false;
		});
		
		//类型选项卡
		$('.box_option_list .option_button_menu>a').off().on('click', function(){
			var $This=$(this),
				$DateType=$This.attr('data-type');
			$This.addClass('current').siblings().removeClass('current');
			$This.parents('.box_option_list').find('.select_list[data-type='+$DateType+']').show().siblings().hide();
			return false;
		});
		$('.box_option_list').each(function(){
			$(this).find('.option_button_menu>a:eq(0)').click(); //默认点击第一个
		});
		
		//触发属性选项文本框
		$('.option_selected').off().on('click', function(){
			var $Obj=$(this).find('.box_input'),
				$Box=$Obj.parents('.box_option_list'),
				$DataType=$Box.find('.option_button_menu>a.current').attr('data-type'),
				$ItemLast=$(this).find('.btn_attr_choice:last'),
				$ItemLastLeft=($ItemLast.size()?$ItemLast.offset().left:0),
				$BoxLeft=0, $BoxWidth=0;
			$(this).addClass('option_focus');
			$(this).nextAll('.option_not_yet').show();
			$(this).nextAll('.option_button').show();
			$Obj.show().focus();
			$BoxLeft=($ItemLastLeft?($ItemLastLeft+$ItemLast.outerWidth(true))-$(this).offset().left:0);
			$BoxWidth=($(this).outerWidth()-$BoxLeft-31);
			if($BoxWidth<20){ //小于20，自动换行
				$Obj.css({'width':$(this).outerWidth()-41});
			}else{
				$Obj.css({'width':$BoxWidth, 'position':'absolute', 'bottom':5, 'left':$BoxLeft});
			}
			$Obj.off().on('blur', function(){ //文本框失去焦点
				$Obj.val('').hide().removeAttr('style');
			}).on('keyup', function(e){ //属性选项添加
				var value=$.trim($(this).val()),
					key=window.event?e.keyCode:e.which,
					html;
				if(key==13){ //回车键
					if(value){
						var MaxNum=0, vid='ADD:', Stop=0;
						$Obj.val('').hide().removeAttr('style');
						//检查内容有没有重复一致的选项存在
						$Box.find('.option_selected input[name='+$DataType+'Name\\[\\]]').each(function(){ //已选中区域
							if($(this).val()==value){
								Stop=1;
								return false; //终止继续执行
							}
						});
						if(Stop==0){ //还没有被终止
							$Box.find('.option_not_yet input[name='+$DataType+'Name\\[\\]]').each(function(){ //未选中区域
								if($(this).val()==value){
									$(this).parent().click();
									Stop=1;
									return false; //终止继续执行
								}
							});
						}
						if(Stop==1){ return false; } //终止继续执行
						MaxNum=parseInt($Box.find('.option_max_number').val()); //获取新标签ID的最大值
						MaxNum+=1;
						vid+=MaxNum;
						$Obj.prev('.select_list').append(frame_obj.box_option_button(value, vid, $DataType, 1, 0));
						$Box.find('.option_max_number').val(MaxNum);
						frame_obj.box_option_button_choice();
						if($Box.attr('data-choice-type')=='radio'){ //属于单选状态，已选中区域只能呆一个选中选项
							$Box.find('.option_selected .btn_attr_choice[data-type='+$DataType+'] .option_current[value="'+vid+'"]').parent().siblings().find('i').click();
						}
						if($Box.find('.option_selected .btn_attr_choice').size()<1){ //没有选项，显示placeholder
							$Box.find('.placeholder').removeClass('hide');
						}else{
							$Box.find('.placeholder').addClass('hide');
						}
					}
					if(window.event){//IE
						e.returnValue=false;
					}else{//Firefox
						e.preventDefault();
					}
					return false;
				}else{ //其他按键
					if(value.length>2){ //两个字母以上才能进行搜索功能
						$.post('?', {"do_action":"action.check_option", 'Keyword':value, 'Type':$DataType}, function(data){
							if(data.ret==1){
								html='';
								for(k in data.msg.Fixed){
									html+=frame_obj.box_option_button(data.msg.Fixed[k], k, $DataType, 0, 1);
								}
								for(k in data.msg.Option){
									html+=frame_obj.box_option_button(data.msg.Option[k], k, $DataType, 0, 0);
								}
								$Box.find('.option_not_yet .select_list[data-type=search]').html(html);
								$Box.find('.option_not_yet .select_list[data-type=search]').show().siblings().hide();
								frame_obj.box_option_button_choice();
							}
						}, 'json');
					}else{
						$Box.find('.option_not_yet .select_list[data-type=search]').html('');
						$Box.find('.option_not_yet .select_list[data-type='+$DataType+']').show().siblings().hide();
					}
				}
			});
		});
		
		$(document).click(function(e){ //属性框去掉着色
			if($(e.target).parents('.box_option_list').length==0){
				$('.option_selected').removeClass('option_focus');
				$('.option_selected').nextAll('.option_not_yet').hide();
				$('.option_selected').nextAll('.option_button').hide();
			}
		});
	},
	
	//多功能选项按钮
	box_option_button:function(name, value, type, isCurrent, isFixed, $icon_txt){
		var html='';
		html+='<span class="btn_attr_choice'+(isCurrent?' current':'')+'" data-type="'+type+'"'+(isFixed?' data-fixed="1"':'')+'>';
		html+=	($icon_txt?$icon_txt:'');
		html+=	'<b>'+name+'</b>';
		html+=	'<input type="checkbox" name="'+type+'Current[]" value="'+value+'" class="option_current"'+(isCurrent?' checked':'')+' />';
		html+=	'<input type="hidden" name="'+type+'Option[]" value="'+value+'" />';
		html+=	'<input type="hidden" name="'+type+'Name[]" value="'+name+'" />';
		html+=	'<i></i>';
		html+='</span>';
		return html;
	},
	
	//多功能选项按钮事件
	box_option_button_choice:function(){
		//选项整体按钮
		$('.box_option_list .btn_attr_choice').off().on('click', function(){
			var $This=$(this),
				$DataType=$This.attr('data-type'),
				$Obj=$This.find('input.option_current'),
				$Parent=$This.parent().parent()
				$Target=$Parent.prev().find('.select_list'),
				$Box=$Obj.parents('.box_option_list'),
				$IsSave=0;
			if($Parent.hasClass('option_not_yet')){ //未选择 -> 已选中
				$Target.find('.btn_attr_choice[data-type='+$DataType+'] .option_current').each(function(){ //已选中区域里面，相同类型的选项
					if($(this).val()==$Obj.val()){ //有相同的数值
						$IsSave=1;
						return false; //终止继续执行
					}
				});
				if($IsSave==1){ //已经有相同的选项，立马删掉
					$This.remove();
				}else{ //改成选中状态
					$This.appendTo($Target);
					$This.addClass('current');
					$This.find('i').show();
					$Obj.attr('checked', true);
					if($Parent.find('.btn_attr_choice').size()<1){
						$Parent.hide();
					}
					if($This.attr('data-fixed')==1){ //固有标签
						$This.find('i').show();
					}
					if($Box.attr('data-choice-type')=='radio'){ //属于单选状态，已选中区域只能呆一个选中选项
						$This.siblings().find('i').click();
					}
				}
				$Box.find('.placeholder').addClass('hide');
			}
			return false;
		});
		
		//选项删除按钮
		$('.box_option_list .btn_attr_choice>i').off().on('click', function(){
			var $This=$(this).parent(),
				$Obj=$This.find('input.option_current'),
				$Parent=$This.parent().parent(),
				$DataType=$This.attr('data-type'),
				$Target='',
				$IsSave=0;
			if($Parent.hasClass('option_selected')){ //已选中 -> 未选择
				$Target=$Parent.next().find('.select_list[data-type='+$DataType+']');
				$Target.find('.btn_attr_choice .option_current').each(function(){ //未选中区域里面，相同类型的选项
					if($(this).val()==$Obj.val()){ //有相同的数值
						$IsSave=1;
						return false; //终止继续执行
					}
				});
				$Parent.find('.box_input').val('').hide().removeAttr('style');
				if($IsSave==1){ //已经有相同的选项，立马删掉
					$This.remove();
				}else{ //改成未选中状态
					$This.appendTo($Target);
					$This.removeClass('current');
					$Obj.removeAttr('checked');
					$Parent.next().show();
					if($This.attr('data-fixed')==1){ //固有标签
						$(this).hide();
					}
				}
				if($Parent.find('.btn_attr_choice').size()<1){ //没有选项，显示placeholder
					$Parent.find('.placeholder').removeClass('hide');
				}
			}else{ //未选择 -> 删除
				$This.remove();
				if($Parent.find('.btn_attr_choice').size()<1){
					$Parent.hide();
				}
			}
			return false;
		});
	},
	
	box_drop_double:function(){
		$('body').on('click', '.box_drop_double dt', function(){
			var $This=$(this),
				$Height=$This.outerHeight(true),
				$Obj=$This.parents('.box_drop_double');
			//隐藏其他相同的下拉效果
			$('.box_drop_double dt').removeClass('box_drop_focus');
			$('.box_drop_double dd').hide();
			//着色
			$This.addClass('box_drop_focus');
			//展开下拉
			$This.next('dd').toggle().css('top', $Height);
			//显示默认内容
			var $IsDefaultList=true;
			if($Obj.find('.box_input').length){
				var $Value=$.trim($Obj.find('.box_input').val());
				if($Value.length>0){
					$IsDefaultList=false;
				}
			}
			if($IsDefaultList){
				frame_obj.box_drop_double_default($Obj);
			}
			$Obj.find('.btn_back').hide();
			//显示文本框（多选）
			if($Obj.attr('data-checkbox')==1){
				var $Input=$Obj.find('.check_input');
				$Input.show().focus();
				$Input.css({'width':($This.outerWidth()-($Input.offset().left-$This.offset().left))-31});
			}
			return false;
		}).on('click', '.box_drop_double .children', function(e){ //下一级选项
			var $This=$(this),
				$CBox=$This.find('.input_checkbox_box');
			if($(e.target).hasClass('input_checkbox_box') || $(e.target).hasClass('input_checkbox')){ //其实是在点击多选按钮
				return false;
			}
			var $Name=$This.attr('data-name'),
				$Value=$This.attr('data-value'),
				$Type=$This.attr('data-type'),
				$Table=$This.attr('data-table'),
				$Top=$This.attr('data-top'),
				$All=$This.attr('data-all'),
				$CheckAll=$This.attr('data-check-all'),
				$Obj=$This.parents('.box_drop_double'),
				$Checkbox=$Obj.attr('data-checkbox');
			frame_obj.box_drop_double_ajax($Obj, {'Value':$Value, 'Type':$Type, 'Table':$Table, 'Top':$Top, 'All':$All, 'CheckAll':$CheckAll, 'Checkbox':$Checkbox});
			$Obj.find('.btn_load_more').attr({'data-value':$Value, 'data-type':$Type, 'data-table':$Table, 'data-top':$Top, 'data-all':$All, 'data-check-all':$CheckAll, 'data-start':1});//加载更多按钮
			return false;
		}).on('click', '.box_drop_double .item', function(){ //多选勾选按钮
			var $Item=$(this),
				$Name=$Item.attr('data-name'),
				$Value=$Item.attr('data-value'),
				$Type=$Item.attr('data-type'),
				$Table=$Item.attr('data-table'),
				$Top=$Item.attr('data-top'),
				$Obj=$Item.parents('.box_drop_double'),
				$Checkbox=$Obj.attr('data-checkbox');
			if($Checkbox){ //多选
				var $List=$Obj.find('.box_checkbox_list'),
					$Input=$List.find('.current[data-type='+$Type+'] .option_current[value="'+$Value+'"]'),
					$Check=$Obj.find('.check_input'),
					$Icon='';
				if($Item.hasClass('btn_check_all')){
					//全选事件
					if($Item.find('.input_checkbox_box').hasClass('checked')){ //勾选 生成按钮
						$Obj.find('.item:gt(0) .input_checkbox_box').not('.checked').click();
					}else{ //取消勾选
						$Obj.find('.item:gt(0) .input_checkbox_box.checked').click();
					}
				}else{
					//勾选事件
					if($Item.find('.input_checkbox_box').hasClass('checked')){ //勾选 生成按钮
						if($Value==-1){ //“所有”选项
							$Item.siblings().find('.input_checkbox_box').removeClass('checked').find(':checkbox').removeAttr('checked'); //取消其他选项的勾选
							$List.find('.current[data-type='+$Type+']').remove();
							$List.find('.select_list').append(frame_obj.box_option_button($Name, $Value, $Type, 1, 0));
							$Check.css({'width':20});
							$Check.css({'width':($List.outerWidth()-($Check.offset().left-$List.offset().left))-31});
						}else{
							if($Item.siblings('.item[data-value="-1"]').length){ //取消全选的勾选
								$Item.siblings('.item[data-value="-1"]').find('.input_checkbox_box').removeClass('checked').find(':checkbox').removeAttr('checked');
							}
							$List.find('.current[data-type='+$Type+'] .option_current[value="-1"]').parent().remove();
							if(!$Input.length){
								if($Item.find('.icon_products').length){ //带有Icon
									$Icon=$Item.find('.icon_products img').attr('src');
									$Icon='<em class="icon icon_products pic_box"><img src="'+$Icon+'" /><span></span></em>';
								}
								$List.find('.select_list').append(frame_obj.box_option_button($Name, $Value, $Type, 1, 0, $Icon));
							}
							$Check.css({'width':20});
							$Check.css({'width':($List.outerWidth()-($Check.offset().left-$List.offset().left))-31});
						}
					}else{ //取消勾选 删除按钮
						$Input.parent().remove();
					}
					if($Obj.find('.btn_check_all').length){ //有全选按钮
						if($Obj.find('.item:gt(0) .input_checkbox_box.checked').length==$Obj.find('.item:gt(0) .input_checkbox_box').length){
							$Obj.find('.btn_check_all .input_checkbox_box').addClass('checked').find(':checkbox').attr('checked');
						}else{
							$Obj.find('.btn_check_all .input_checkbox_box').removeClass('checked').find(':checkbox').removeAttr('checked');
						}
					}
				}
			}else{ //单选
				if($Obj.hasClass('edit_box')){
					if($Type=='add') $Name=$Value; //添加
					$Obj.find('dt input[type=text]').val($Name).parent().find('.hidden_value').val($Value).next().val($Type);
				}else{
					$Obj.find('dt>div>span').text($Name).parent().find('.hidden_value').val($Value).next().val($Type);
				}
				if(!$Item.hasClass('children')){ //没有下一级选项，最终确定
					$Item.parents('dd').hide();
				}
			}
			return false;
		}).on('click', '.box_drop_double .box_checkbox_list i', function(e){
			$(this).parent().remove();
			if(window.event){//IE
				e.returnValue=false;
			}else{//Firefox
				e.preventDefault();
			}
			return false;
		}).on('click', '.box_drop_double', function(){
			return false;
		}).on('click', '.btn_back', function(){ //返回
			var $This=$(this),
				$Value=$This.attr('data-value'),
				$Type=$This.attr('data-type'),
				$Table=$This.attr('data-table'),
				$Top=$This.attr('data-top'),
				$All=$This.attr('data-all'),
				$CheckAll=$This.attr('data-check-all'),
				$Obj=$This.parents('.box_drop_double'),
				$Checkbox=$Obj.attr('data-checkbox');
			frame_obj.box_drop_double_ajax($Obj, {'Value':$Value, 'Type':$Type, 'Table':$Table, 'Top':$Top, 'All':$All, 'CheckAll':$CheckAll, 'Checkbox':$Checkbox});
			return false;
		}).on('click', '.btn_load_more', function(){ //加载更多
			var $This=$(this),
				$Value=$This.attr('data-value'),
				$Type=$This.attr('data-type'),
				$Table=$This.attr('data-table'),
				$Top=$This.attr('data-top'),
				$All=$This.attr('data-all'),
				$CheckAll=$This.attr('data-check-all'),
				$Start=$This.attr('data-start'),
				$Obj=$This.parents('.box_drop_double'),
				$Checkbox=$Obj.attr('data-checkbox');
			frame_obj.box_drop_double_ajax($Obj, {'Value':$Value, 'Type':$Type, 'Table':$Table, 'Top':$Top, 'All':$All, 'CheckAll':$CheckAll, 'Checkbox':$Checkbox, 'Start':$Start});
			$Obj.find('.btn_load_more').attr({'data-value':$Value, 'data-type':$Type, 'data-table':$Table, 'data-top':$Top, 'data-all':$All, 'data-check-all':$CheckAll, 'data-start':parseInt($Start)+1});//加载更多按钮
			return false;
		}).on('keyup', '.box_drop_double dt .box_input', function(e){ //搜索功能
			var $Value=$.trim($(this).val()),
				$Key=window.event?e.keyCode:e.which,
				$Obj=$(this).parents('.box_drop_double'),
				$Checkbox=$Obj.attr('data-checkbox');
			if($Key==13){ //回车键
				//忽略...
			}else{ //其他按键
				if($Value.length>2){ //两个字母以上才能进行搜索功能
					$Data=$Obj.find('.drop_list').attr('data');
					$.post('?', {"do_action":"action.check_drop_double", 'Keyword':$Value, 'Data':$Data, 'Checkbox':$Checkbox}, function(result){
						if(result.ret==1){
							$Obj.find('.btn_back, .btn_load_more').hide();
							$Obj.find('.drop_skin').hide();
							$Obj.find('.drop_list').html(result.msg.Html);
						}
					}, 'json');
				}else{ //否则自动还原默认
					frame_obj.box_drop_double_default($Obj);
					$Obj.find('.btn_back, .btn_load_more').hide();
				}
			}
			if(window.event){//IE
				e.returnValue=false;
			}else{//Firefox
				e.preventDefault();
			}
			return false;
		});
		$(document).click(function(){
			$('.box_drop_double dt').removeClass('box_drop_focus');
			$('.box_drop_double dd').hide();
		});
		$('.box_drop_double .select[selected]').each(function(){
			$(this).click();
		});
		
		$('.box_drop_double').each(function(){
			frame_obj.box_drop_double_default($(this));
		});
	},
	
	box_drop_double_default:function(obj){
		var $Data=obj.find('.drop_list').attr('data'),
			$Checkbox=obj.attr('data-checkbox'),
			$Html='';
		obj.find('.drop_skin').show();
		obj.find('.drop_list').html(' ');
		setTimeout(function(){
			$Data=$.evalJSON($Data);
			for(k in $Data){
				$Html+='<div class="item'+($Data[k]['Table']?' children':'')+'" data-name="'+$Data[k]['Name']+'" data-value="'+k+'" data-type="'+$Data[k]['Type']+'" data-table="'+$Data[k]['Table']+'" data-top="0" data-all="'+($Data[k]['All']==0?0:1)+'" data-check-all="'+($Data[k]['CheckAll']==1?1:0)+'">';
				if($Checkbox==1 && !$Data[k]['Table']){ //多选，根目录除外
					$Html+='<span class="input_checkbox_box"><span class="input_checkbox"><input type="checkbox" name="_DoubleOption[]" value="'+k+'" /></span>'+($Data[k]['Icon']?$Data[k]['Icon']:'')+$Data[k]['Name']+'</span>';
				}else{ //单选
					$Html+='<span>'+($Data[k]['Icon']?$Data[k]['Icon']:'')+$Data[k]['Name']+'</span>';
				}
				$Html+=($Data[k]['Table']?'<em></em>':'');
				$Html+='</div>';
			}
			obj.find('.drop_skin').hide();
			obj.find('.drop_list').html($Html);
		}, 500);
	},
	
	box_drop_double_ajax:function(obj, data, callback){
		obj.find('.drop_skin').show();
		//obj.find('.drop_list').html(' ');
		setTimeout(function(){
			$.post('./?do_action=action.box_drop_double', data, function(result){
				if(result.ret==1){
					if(data.Top==1){
						frame_obj.box_drop_double_default(obj);
						obj.find('.btn_back, .btn_load_more').hide();
					}else{
						obj.find('.btn_back').show().attr({'data-value':result.msg.Back.Value, 'data-type':result.msg.Back.Type, 'data-table':result.msg.Back.Table, 'data-top':result.msg.Back.Top});
						obj.find('.drop_skin').hide();
						if(result.msg.Start==0){//第一页
							obj.find('.drop_list').html(result.msg.Html);
						}else{//翻页
							obj.find('.drop_list').append(result.msg.Html);
						}
						if(result.msg.Count==20){
							obj.find('.btn_load_more').show();
						}else{
							obj.find('.btn_load_more').html(lang_obj.manage.global.no_more).animate('', 2000, function(){
								$(this).fadeOut(2000, function(){
									$(this).html(lang_obj.manage.global.load_more);
								})
							})
						}
					}
				}
			}, 'json');
		}, 500);
	},

	Waterfall:function(container, colWidth, colCount, cls){
		function Waterfall(param){
		    this.id = typeof param.container == 'string' ? document.getElementById(param.container) : param.container;
		    this.colWidth = param.colWidth;
		    this.colCount = param.colCount || 4;
		    this.cls = param.cls && param.cls != '' ? param.cls : 'wf-cld';
		    this.init();
		}
		//瀑布流
		Waterfall.prototype = {
		    getByClass:function(cls,p){
		        var arr = [],reg = new RegExp("(^|\\s+)" + cls + "(\\s+|$)","g");
		        var nodes = p.getElementsByTagName("*"),len = nodes.length;
		        for(var i = 0; i < len; i++){
		            if(reg.test(nodes[i].className)){
		                arr.push(nodes[i]);//
		                reg.lastIndex = 0;
		            }
		        }
		        return arr;
		    },
		    maxArr:function(arr){
		        var len = arr.length,temp = arr[0];
		        for(var ii= 1; ii < len; ii++){
		            if(temp < arr[ii]){
		                temp = arr[ii];
		            }
		        }
		        return temp;
		    },
		    getMar:function(node){
		        var dis = 0;
		        if(node.currentStyle){
		            dis = parseInt(node.currentStyle.marginBottom);
		        }else if(document.defaultView){
		            dis = parseInt(document.defaultView.getComputedStyle(node,null).marginBottom);
		        }
		        return dis;
		    },
		    getMinCol:function(arr){
		        var ca = arr,cl = ca.length,temp = ca[0],minc = 0;
		        for(var ci = 0; ci < cl; ci++){
		            if(temp > ca[ci]){
		                temp = ca[ci];
		                minc = ci;
		            }
		        }
		        return minc;
		    },
		    init:function(){
		        var _this = this;
		        var col = [],//列高
		            iArr = [];//索引
		        var nodes = _this.getByClass(_this.cls,_this.id),len = nodes.length;
		        for(var i = 0; i < _this.colCount; i++){
		            col[i] = 0;
		        }
		        for(var i = 0; i < len; i++){
		            nodes[i].h = nodes[i].offsetHeight + _this.getMar(nodes[i]);
		            iArr[i] = i;
		        }
		         
		        for(var i = 0; i < len; i++){
		            var ming = _this.getMinCol(col);
		            nodes[i].style.left = ming * _this.colWidth + "px";
		            nodes[i].style.top = col[ming] + "px";
		            col[ming] += nodes[i].h;
		        }
		         
		        _this.id.style.height = _this.maxArr(col) + "px";
		    }
		};

		new Waterfall({
		    "container": container, //id
		    "colWidth": colWidth,
		    "colCount": colCount,//parseInt($('#themes_box').width()/220)
		    "cls": cls //class
		});
			
	},
	
	highcharts_init:{
		colors:['#0cb083', '#4289ff', '#90ed7d', '#f7a35c', '#91e8e1',  '#f15c80', '#e4d354', '#8d4653'],
		cdx_colors:['#4a8bf1', '#4289ff', '#90ed7d', '#f7a35c', '#91e8e1',  '#f15c80', '#e4d354', '#8d4653'],
		
		//曲线面积图
		areaspline:function(ObjName, data){
			var chart=Highcharts.chart(ObjName, $.extend(true, {
					chart:	{type:'areaspline'},
					colors:	ueeshop_config.FunVersion>=10?frame_obj.highcharts_init.cdx_colors:frame_obj.highcharts_init.colors,
					credits:{enabled:false},
					title:	{text:''},
					exporting:{enabled:true},
					legend:	{enabled:false},
					yAxis:	{title:{text:''}},
					plotOptions: {areaspline:{fillOpacity:0.2}}
				}, data)
			);
		},
		
		//基础柱形图
		column_basic:function(ObjName, data){
			var chart=Highcharts.chart(ObjName, $.extend(true, {
					chart:	{type:'column'},
					colors:	ueeshop_config.FunVersion>=10?frame_obj.highcharts_init.cdx_colors:frame_obj.highcharts_init.colors,
					credits:{enabled:false},
					title:	{text:''},
					legend:	{enabled:false},
					yAxis:	{title:{text:''}},
					plotOptions: {column:{borderWidth:0}}
				}, data)
			);
		},
		
		//包含图例的饼图
		pie_legend:function(ObjName, data){
			var chart=Highcharts.chart(ObjName, $.extend(true, {
					chart:	{type:'pie'},
					colors:	ueeshop_config.FunVersion>=10?frame_obj.highcharts_init.cdx_colors:frame_obj.highcharts_init.colors,
					credits:{enabled:false},
					title:	{text:''},
					plotOptions: {
							pie:{allowPointSelect:true, cursor:'pointer', dataLabels:{enabled:false}, showInLegend:true}
					}
				}, data)
			);
		},
		
		//双坐标的折线图、柱状图混合图
		combo_dual_axes:function(ObjName, data){
			var chart = Highcharts.chart(ObjName, $.extend(true, {
					chart:	{zoomType: 'xy'},
					credits:{enabled:false},
					colors:	ueeshop_config.FunVersion>=10?frame_obj.highcharts_init.cdx_colors:frame_obj.highcharts_init.colors,
					tooltip:{valuePrefix:''}
				}, data)
			);
		},
		
		//标题居中的环形图
		pie_donut_center_title:function(ObjName, data){
			var chart = Highcharts.chart(ObjName, $.extend(true, {
				chart:	{spacing:[40, 0 , 40, 0]},
				title: 	{floating:true, text:'圆心显示的标题'},
				credits:{enabled:false},
				legend:	{itemMarginTop:15},
				tooltip:{pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
				plotOptions:{
					pie:{
						allowPointSelect: true,
						showInLegend: true,
						cursor: 'pointer',
						dataLabels: {enabled: false},
						point:{
							events:{
								mouseOver: function(e){ //鼠标滑过时动态更新标题
									chart.setTitle({
										text: e.target.name+'\t'+e.target.y+' %'
									});
								}
							}
						},
					}
				}
			}, data),
			function(c){ //图表初始化完毕后的会掉函数
				//环形图圆心
				var centerY = c.series[0].center[1],
					titleHeight = parseInt(c.title.styles.fontSize);
				//动态设置标题位置
				c.setTitle({
					y:centerY + titleHeight/2
				});
			});
		}
	},
	
	//后台SESSION过期重新登录
	expire_login:function(){
		if(!ueeshop_config.UserName) return false;
		$.post('?','do_action=account.set_login_status');
		setTimeout(function(){
			setInterval(function(){
				var session_time=global_obj.getCookie('session_time');
				if(!session_time && !$('#expire_login_form').size()){
					$.post('?','do_action=account.check_login_status',function(data){
						if(data.ret==1){
							global_obj.div_mask();
							$('body').prepend(data.msg);
							$('#expire_login_form input[name=UserName]').val(ueeshop_config.UserName);
							frame_obj.submit_form_init($('#expire_login_form'), '', '', 0, function(data){
								if(data.ret==1){
									global_obj.div_mask(1);
									$('#expire_login_form').remove();
								}else{
									$('#expire_login_form .tips').html(data.msg);
								}
							});
						}
					},'json');
				}
			},1000);
		}, 7200000);	//SESSION过期时间7200000
	},
	
	//进度条(批量处理)
	box_progress:function($callBack){
		$('#btn_progress_keep').click(function(){
			var $Data=new Object;
			$('.box_progress input[type=hidden]').each(function(){
				$Data[$(this).attr('name')]=$(this).val();
			});
			frame_obj.box_progress_ajax($Data, $callBack);
		});
	},
	
	box_progress_ajax:function(data, callBack){
		$.ajax({
			url:(data.url?data.url:'./'),
			type:'post',
			data:data,
			dataType:'json',
			success:function(result){
				if(result){
					if(result.ret==3){ //进度完成
						$('.box_progress .progress .num').css('width', '100%').find('span').text('100%');
						$('.box_progress .status').text(lang_obj.manage.global.update_status[1]);
						$('.box_progress .tips').hide();
					}else if(result.ret==2){ //下一页
						$('.box_progress input[name=Start]').val(parseInt(result.msg.Start));
						if(parseInt(result.msg.Percent)>10){
							$('.box_progress .progress .num').css('width', parseInt(result.msg.Percent)+'%').find('span').text(parseInt(result.msg.Percent)+'%');
						}
					}else if(result.ret==1){ //反复请求当前的进度状态
						if(parseInt(result.msg.Percent)>10){
							$('.box_progress .progress .num').css('width', parseInt(result.msg.Percent)+'%').find('span').text(parseInt(result.msg.Percent)+'%');
						}
						setTimeout(function(){frame_obj.box_progress_ajax(data, callBack)}, 1000);
					}else{ //进度中断
						$('.box_progress .status').text(lang_obj.manage.global.update_status[2]);
					}
				}else{ //进度异常
					$('.box_progress .status').text(lang_obj.manage.global.update_status[2]);
				}
				$.isFunction(callBack) && callBack(result); //回调函数
			},
			complete:function(XMLHttpRequest, status){
				if(status=='timeout'){ //超时
					frame_obj.box_progress_ajax(data, callBack);
				}
			}
		});
	},

	check_amount:function(obj){ //只能填入数字
		obj.find('input[rel=amount]').off().keydown(function(e){
			var value	= $(this).val(),
				key		= (window.event?e.keyCode:e.which),
				ctrl	= (e.ctrlKey || e.metaKey),
				ext   	= 0;
				if(((key==109  && value.indexOf('-')<0) || (key==189  && value.indexOf('-')<0)) && $(this).attr('data-key')=='-') ext=1;
			if((key>95 && key<106) || (key>47 && key<60) || (key==110 && value.indexOf('.')<0) || (key==190 && value.indexOf('.')<0) || ext){ //[0~9][.]
			}else if((ctrl && key==67) || (ctrl && key==86) || (ctrl && key==65)){ //Ctrl+C Ctrl+V Ctrl+A
			}else if(key!=8){ //删除键
				if(window.event){//IE
					e.returnValue=false;
				}else{//Firefox
					e.preventDefault();
				}
				return false;
			}
		});
	}
}