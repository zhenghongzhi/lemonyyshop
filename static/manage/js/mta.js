/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
var mta_obj={
	visits_init:function(){
		mta_obj.visits_resize_init();
		$(window).resize(function(){
			mta_obj.visits_resize_init();
		});
		mta_obj.tab_switching();
		//时间段相关数据
		mta_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=mta.api_get_data&Action=ueeshop_analytics_get_visits_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare,
				Data=new Object;
			$.post('./', par, function(data){
				//数据整合
				$('.box_data_list .pv').html(data.msg.total.pv);
				$('.box_data_list .average_pv').html(data.msg.total.average_pv);
				$('.box_data_list .uv').html(data.msg.total.uv);
				$('.box_data_list .session').html(data.msg.total.session);
				var html='';
				$.each(data.msg.detail, function(key, value){
					html+='<tr>\
						<td nowrap="nowrap" valign="top">'+value['title']+'</td>\
						<td nowrap="nowrap">'+value['pv']+'<div class="compare compare_pv">0</div></td>\
						<td nowrap="nowrap">'+value['average_pv']+'<div class="compare compare_average_pv">0</div></td>\
						<td nowrap="nowrap">'+value['session']+'<div class="compare compare_session">0</div></td>\
						<td nowrap="nowrap">'+value['uv']+'<div class="compare compare_uv">0</div></td>\
					</tr>';
				});
				$('#visits_detail .data_table tbody').html(html);
				
				//概况
				Data=new Object;
				Data['xAxis']={'categories': data.msg.line_charts.xAxis.categories};
				Data['series']=new Array;
				for(k in data.msg.line_charts.series){
					Data['series'][k]={'name':'Pv', 'data':data.msg.line_charts.series[k].data};
				}
				frame_obj.highcharts_init.areaspline('line_charts', Data);
				
				//设备数据整合
				var browser_total=new Object,
					browser_percent=new Object,
					total=0;
				for(k in data.msg.browser_all_charts){
					browser_total[k]=0;
					for(k2 in data.msg.browser_all_charts[k]){
						browser_total[k]+=data.msg.browser_all_charts[k][k2];
					}
				}
				total=browser_total[0]+browser_total[1];
				browser_percent[0]=(browser_total[0]/total*100).toFixed(2);
				browser_percent[1]=(browser_total[1]/total*100).toFixed(2);
				browser_percent=Array({'name':lang_obj.global.source.pc, 'y':parseFloat(browser_percent[0])}, {'name':lang_obj.global.source.mobile, 'y':parseFloat(browser_percent[1])});
				
				//设备占比
				Data=new Object;
				Data['series']=new Array;
				Data['series'][0]={'name':lang_obj.manage.mta.equipment_ratio, 'data':browser_percent};
				frame_obj.highcharts_init.pie_legend('browser_charts', Data);
				
				if(data.msg.browser_all_charts.length>0){
					var count=0, i=0, j=0, countData=new Array;
					//PC端浏览器
					html='';
					if(data.msg.browser_all_charts[0]){
						$.each(data.msg.browser_all_charts[0], function(key, value){
							countData[i]={'title':key, 'value':(value/browser_total[0]*100).toFixed(2)};
							++i;
						});
						countData=countData.sort(function(a, b){
							return b.value - a.value;
						});
						$.each(countData, function(key, value){
							html+='<div class="browser_data">\
								<div class="browser_left">'+value.title+'</div>\
								<div class="browser_right"><div class="process" style="width:'+value.value+'%;"></div></div>\
								<div class="browser_percent">'+value.value+'%</div>\
							</div>';
						});
					}
					$('.box_browser_pc').html(html);
					
					//移动端浏览器
					html='';
					countData=new Array;
					if(data.msg.browser_all_charts[1]){
						$.each(data.msg.browser_all_charts[1], function(key, value){
							countData[j]={'title':key, 'value':(value/browser_total[1]*100).toFixed(2)};
							++j;
						});
						countData=countData.sort(function(a, b){
							return b.value - a.value;
						});
						$.each(countData, function(key, value){
							html+='<div class="browser_data">\
								<div class="browser_left">'+value.title+'</div>\
								<div class="browser_right"><div class="process" style="width:'+value.value+'%;"></div></div>\
								<div class="browser_percent">'+value.value+'%</div>\
							</div>';
						});
					}
					$('.box_browser_mobile').html(html);
				}
				
				//对比选项
				if(Time==-89){
					$('.compare').show();
					$('.box_data_list .compare_pv').html(data.msg.compare.total.pv);
					$('.box_data_list .compare_pv_img').removeClass('down up').addClass(data.msg.total.pv>=data.msg.compare.total.pv?'up':'down');
					$('.box_data_list .compare_average_pv').html(data.msg.compare.total.average_pv);
					$('.box_data_list .compare_average_pv_img').removeClass('down up').addClass(data.msg.total.average_pv>=data.msg.compare.total.average_pv?'up':'down');
					$('.box_data_list .compare_uv').html(data.msg.compare.total.uv);
					$('.box_data_list .compare_uv_img').removeClass('down up').addClass(data.msg.total.uv>=data.msg.compare.total.uv?'up':'down');
					$('.box_data_list .compare_session').html(data.msg.compare.total.session);
					$('.box_data_list .compare_session_img').removeClass('down up').addClass(data.msg.total.ip>=data.msg.compare.total.session?'up':'down');
					$('#visits_detail .data_table tbody tr').each(function(index){
						var d=data.msg.compare.detail[index];
						$(this).find('.compare_pv').html(d.pv);
						$(this).find('.compare_average_pv').html(d.average_pv);
						$(this).find('.compare_uv').html(d.uv);
						$(this).find('.compare_session').html(d.session);
					});
				}else{
					$('.compare').hide();
				}
			}, 'json');
		});
		
		//平台相关数据
		mta_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var lang=$('body').hasClass('en')?'en':'zh-cn',
				Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=mta.api_get_data&Action=ueeshop_analytics_get_visits_referrer_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare+'&lang='+lang,
				html='';
			$.post('./', par, function(data){
				//总览
				html='';
				$.each(data.msg.total, function(key, value){
					html+='<tr data-key="'+key+'">\
						<td nowrap="nowrap" valign="top"><a href="javascript:;" class="'+key+'">'+value['title']+'</a></td>\
						<td nowrap="nowrap">'+value['pv']+'<div class="compare compare_pv">0</div></td>\
						<td nowrap="nowrap">'+value['average_pv']+'<div class="compare compare_average_pv">0</div></td>\
						<td nowrap="nowrap">'+value['ip']+'<div class="compare compare_ip">0</div></td>\
						<td nowrap="nowrap">'+value['uv']+'<div class="compare compare_uv">0</div></td>\
					</tr>';
				});
				$('#visits_referrer_detail .data_table tbody').html(html);
				//来源网址
				var $urlObj=new Object, $urlData=new Array, $urlCount=0, i=0;
				html='';
				$.each(data.msg.detail.share_platform, function(key, value){
					$urlObj[value['title']]=0;
					$urlObj[value['title']]+=value['uv'];
				});
				$.each(data.msg.detail.search_engine, function(key, value){
					!$urlObj[value['title']] && ($urlObj[value['title']]=0);
					$urlObj[value['title']]+=value['uv'];
				});
				$.each(data.msg.detail.other, function(key, value){
					!$urlObj[value['title']] && ($urlObj[value['title']]=0);
					$urlObj[value['title']]+=value['uv'];
				});
				i=0;
				$.each($urlObj, function(key, value){
					$urlData[i]={'title':key, 'value':value};
					++i;
				});
				$urlData=$urlData.sort(function(a, b){
					return b.value - a.value;
				});
				$.each($urlData, function(key, value){
					if(key>9) return false;
					html+='<tr>\
						<td valign="top"><div class="title">'+value.title+'</div></td>\
						<td nowrap="nowrap"><div class="url_uv color_000">0%</div></td>\
						<td nowrap="nowrap"><div class="color_aaa">'+value.value+'</div></td>\
					</tr>';
					$urlCount+=value.value;
					++i;
				});
				$('.box_url_info tbody').html(html);
				if(html){
					$('.box_url_info .no_data').hide();
				}else{
					$('.box_url_info .no_data').show();
				}
				$.each($urlData, function(key, value){
					if(key>8) return false;
					$('.box_url_info tbody tr:eq('+key+') .url_uv').html((parseFloat(value.value)/$urlCount*100).toFixed(2)+'%');
				});
				
				if(Time==-89){
					$('.compare').show();
					//总览
					$('#visits_referrer_detail .data_table tbody tr').each(function(index){
						var key=$(this).attr('data-key');
						var d=data.msg.compare.total[key];
						$(this).find('.compare_pv').html(d.pv);
						$(this).find('.compare_average_pv').html(d.average_pv);
						$(this).find('.compare_uv').html(d.uv);
						$(this).find('.compare_ip').html(d.ip);
					});
					//搜索引擎
					$('#visits_referrer_detail .search_engine tbody tr').each(function(index){
						var d=data.msg.compare.detail.search_engine[index];
						$(this).find('.compare_pv').html(d.pv);
						$(this).find('.compare_average_pv').html(d.average_pv);
						$(this).find('.compare_uv').html(d.uv);
						$(this).find('.compare_ip').html(d.ip);
					});
					//分享平台
					$('#visits_referrer_detail .share_platform tbody tr').each(function(index){
						var d=data.msg.compare.detail.share_platform[index];
						$(this).find('.compare_pv').html(d.pv);
						$(this).find('.compare_average_pv').html(d.average_pv);
						$(this).find('.compare_uv').html(d.uv);
						$(this).find('.compare_ip').html(d.ip);
					});
					//其他
					$('#visits_referrer_detail .other tbody tr').each(function(index){
						var d=data.msg.compare.detail.other[index];
						$(this).find('.compare_pv').html(d.pv);
						$(this).find('.compare_average_pv').html(d.average_pv);
						$(this).find('.compare_uv').html(d.uv);
						$(this).find('.compare_ip').html(d.ip);
					});
				}else{
					$('.compare').hide();
				}
			}, 'json');
		});
		
		//国家相关数据
		mta_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=mta.api_get_data&Action=ueeshop_analytics_get_visits_country_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare,
				Data=new Object;
			$.post('./', par, function(data){
				//国家分布
				Data=new Object;
				Data['xAxis']={'categories': data.msg.line_charts.xAxis.categories};
				Data['series']=new Array;
				for(k in data.msg.line_charts.series){
					Data['series'][k]={'name':'PV', 'data':data.msg.line_charts.series[k].data, 'pointWidth':40};
				}
				frame_obj.highcharts_init.column_basic('country_charts', Data);
			}, 'json');
		});
	},
	
	visits_resize_init:function(){
		var $browser=$('.box_browser')
			$browserWidth=$browser.width(),
			$halfWidth=(($browserWidth-$browser.find('.item:eq(0)').outerWidth())/2);
		$browser.find('.item:gt(0)').css('width', $halfWidth-61);
	},
	
	visits_referrer_init:function(){
		$('#mta_detail_data .back').width($('#mta .nav').width());
		$('#mta_total_data .data_table tbody').delegate('tr td a', 'click', function(){
			var class_name=$(this).attr('class');
			if(class_name!='enter_directly'){
				$('#mta_detail_data .detail table').hide();
				$('#mta_detail_data .detail table.'+class_name).show();
				$('#mta_total_data').slideUp(500);
				$('#mta_detail_data').slideDown(500);
			}
		});
		$('#mta_detail_data .back a').click(function(){
			$('#mta_total_data').slideDown(100);
			$('#mta_detail_data').slideUp(100);
			$('#mta_detail_data .detail table').hide(800);
		});
	},
	
	visits_conversion_init:function(){
		mta_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=mta.get_visits_conversion_data&Action=ueeshop_analytics_new_get_visits_conversion_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare;
			$.post('./', par, function(data){
				if(data.ret==1){
                    //国家分布
                    /*
                    Data=new Object;
                    Data['xAxis']={'categories': data.msg.conversion_charts.xAxis.categories};
                    Data['tooltip']={'valueSuffix': '%'};
                    Data['series']=new Array;
                    for(k in data.msg.conversion_charts.series){
                        Data['series'][k]={'name':lang_obj.manage.mta.total_ratio, 'data':data.msg.conversion_charts.series[k].data, 'maxPointWidth':50};
                    }
                    frame_obj.highcharts_init.column_basic('conversion_charts', Data);
                    */
                    Data=new Object;
                    Data['xAxis']={'categories': data.msg.conversion_charts.xAxis.categories};
                    Data['tooltip']={'valueSuffix': '%'};
                    Data['series']=new Array;
                    for(k in data.msg.conversion_charts.series){
                        Data['series'][k]={'name':lang_obj.manage.mta.total_ratio, 'data':data.msg.conversion_charts.series[k].data};
                    }
                    frame_obj.highcharts_init.areaspline('conversion_charts', Data);
                    
                    /*全部*/
                    $('.ratio').text(data.msg.ratio.complete_session_ratio);
					$('.ratio_uv').text(data.msg.ratio.session);
					$('.ratio_addtocart').text(data.msg.ratio.addtocart_session);
					$('.ratio_operate_addtocart').text(data.msg.ratio.addtocart_session_ratio);
					$('.ratio_placeorder').text(data.msg.ratio.placeorder_session);
					$('.ratio_operate_placeorder').text(data.msg.ratio.placeorder_session_ratio);
					$('.ratio_complete').text(data.msg.ratio.complete_session);
					$('.ratio_operate_complete').text(data.msg.ratio.complete_session_ratio);
                    $('.ratio_operate_total').text(data.msg.ratio.total_session_ratio);
					/*直接输入*/
					$('.enter_directly').text(data.msg.enter_directly.complete_session_ratio);
					$('.enter_directly_uv').text(data.msg.enter_directly.session);
					$('.enter_directly_addtocart').text(data.msg.enter_directly.addtocart_session);
					$('.enter_directly_operate_addtocart').text(data.msg.enter_directly.addtocart_session_ratio);
					$('.enter_directly_placeorder').text(data.msg.enter_directly.placeorder_session);
					$('.enter_directly_operate_placeorder').text(data.msg.enter_directly.placeorder_session_ratio);
					$('.enter_directly_complete').text(data.msg.enter_directly.complete_session);
					$('.enter_directly_operate_complete').text(data.msg.enter_directly.complete_session_ratio);
                    $('.enter_directly_operate_total').text(data.msg.enter_directly.total_session_ratio);
					/*分享平台*/
					$('.share_platform').text(data.msg.share_platform.complete_session_ratio);
					$('.share_platform_uv').text(data.msg.share_platform.session);
					$('.share_platform_addtocart').text(data.msg.share_platform.addtocart_session);
					$('.share_platform_operate_addtocart').text(data.msg.share_platform.addtocart_session_ratio);
					$('.share_platform_placeorder').text(data.msg.share_platform.placeorder_session);
					$('.share_platform_operate_placeorder').text(data.msg.share_platform.placeorder_session_ratio);
					$('.share_platform_complete').text(data.msg.share_platform.complete_session);
					$('.share_platform_operate_complete').text(data.msg.share_platform.complete_session_ratio);
                    $('.share_platform_operate_total').text(data.msg.share_platform.total_session_ratio);
					/*搜索引擎*/
					$('.search_engine').text(data.msg.search_engine.complete_session_ratio);
					$('.search_engine_uv').text(data.msg.search_engine.session);
					$('.search_engine_addtocart').text(data.msg.search_engine.addtocart_session);
					$('.search_engine_operate_addtocart').text(data.msg.search_engine.addtocart_session_ratio);
					$('.search_engine_placeorder').text(data.msg.search_engine.placeorder_session);
					$('.search_engine_operate_placeorder').text(data.msg.search_engine.placeorder_session_ratio);
					$('.search_engine_complete').text(data.msg.search_engine.complete_session);
					$('.search_engine_operate_complete').text(data.msg.search_engine.complete_session_ratio);
                    $('.search_engine_operate_total').text(data.msg.search_engine.total_session_ratio);
					/*其他*/
					$('.other').text(data.msg.other.complete_session_ratio);
					$('.other_uv').text(data.msg.other.session);
					$('.other_addtocart').text(data.msg.other.addtocart_session);
					$('.other_operate_addtocart').text(data.msg.other.addtocart_ratio_session);
					$('.other_placeorder').text(data.msg.other.placeorder_session);
					$('.other_operate_placeorder').text(data.msg.other.placeorder_session_ratio);
					$('.other_complete').text(data.msg.other.complete_session);
					$('.other_operate_complete').text(data.msg.other.complete_session_ratio);
                    $('.other_operate_total').text(data.msg.other.total_session_ratio);
					/*概况*/
					$('.charts_enter .rect').css('height', '100%');
					$('.charts_addtocart .rect').css('height', data.msg.ratio.addtocart_ratio+'%');
					$('.charts_placeorder .rect').css('height', data.msg.ratio.placeorder_ratio+'%');
					$('.charts_complete .rect').css('height', data.msg.ratio.complete_ratio+'%');
					
					//对比选项
					if(Time==-89){
						var compare=data.msg.compare;
						$('.compare').show();
						/*转化率*/
						$('.compare_ratio').text(compare.ratio.complete_ratio);
						$('.compare_ratio_uv').text(compare.ratio.uv);
						$('.compare_ratio_addtocart').text(compare.ratio.addtocart);
						$('.compare_ratio_operate_addtocart').text(compare.ratio.addtocart_ratio);
						$('.compare_ratio_placeorder').text(compare.ratio.placeorder);
						$('.compare_ratio_operate_placeorder').text(compare.ratio.placeorder_ratio);
						$('.compare_ratio_complete').text(compare.ratio.complete);
						$('.compare_ratio_operate_complete').text(compare.ratio.complete_ratio);
						/*直接输入*/
						$('.compare_enter_directly').text(compare.enter_directly.complete_ratio);
						$('.compare_enter_directly_uv').text(compare.enter_directly.uv);
						$('.compare_enter_directly_addtocart').text(compare.enter_directly.addtocart);
						$('.compare_enter_directly_operate_addtocart').text(compare.enter_directly.addtocart_ratio);
						$('.compare_enter_directly_placeorder').text(compare.enter_directly.placeorder);
						$('.compare_enter_directly_operate_placeorder').text(compare.enter_directly.placeorder_ratio);
						$('.compare_enter_directly_complete').text(compare.enter_directly.complete);
						$('.compare_enter_directly_operate_complete').text(compare.enter_directly.complete_ratio);
						/*分享平台*/
						$('.compare_share_platform').text(compare.share_platform.complete_ratio);
						$('.compare_share_platform_uv').text(compare.share_platform.uv);
						$('.compare_share_platform_addtocart').text(compare.share_platform.addtocart);
						$('.compare_share_platform_operate_addtocart').text(compare.share_platform.addtocart_ratio);
						$('.compare_share_platform_placeorder').text(compare.share_platform.placeorder);
						$('.compare_share_platform_operate_placeorder').text(compare.share_platform.placeorder_ratio);
						$('.compare_share_platform_complete').text(compare.share_platform.complete);
						$('.compare_share_platform_operate_complete').text(compare.share_platform.complete_ratio);
						/*搜索引擎*/
						$('.compare_search_engine').text(compare.search_engine.complete_ratio);
						$('.compare_search_engine_uv').text(compare.search_engine.uv);
						$('.compare_search_engine_addtocart').text(compare.search_engine.addtocart);
						$('.compare_search_engine_operate_addtocart').text(compare.search_engine.addtocart_ratio);
						$('.compare_search_engine_placeorder').text(compare.search_engine.placeorder);
						$('.compare_search_engine_operate_placeorder').text(compare.search_engine.placeorder_ratio);
						$('.compare_search_engine_complete').text(compare.search_engine.complete);
						$('.compare_search_engine_operate_complete').text(compare.search_engine.complete_ratio);
						/*其他*/
						$('.compare_other').text(compare.other.complete_ratio);
						$('.compare_other_uv').text(compare.other.uv);
						$('.compare_other_addtocart').text(compare.other.addtocart);
						$('.compare_other_operate_addtocart').text(compare.other.addtocart_ratio);
						$('.compare_other_placeorder').text(compare.other.placeorder);
						$('.compare_other_operate_placeorder').text(compare.other.placeorder_ratio);
						$('.compare_other_complete').text(compare.other.complete);
						$('.compare_other_operate_complete').text(compare.other.complete_ratio);
						/*概况*/
						var count=0;
						$('.charts_enter .rect_compare').css('height', '100%');
						if(data.msg.ratio.uv>compare.ratio.uv){
							count=(compare.ratio.uv/data.msg.ratio.uv*100).toFixed(2);
							$('.charts_enter .rect_compare').css('height', count+'%');
						}else{
							count=(data.msg.ratio.uv/compare.ratio.uv*100).toFixed(2);
							$('.charts_enter .rect_compare').css('height', count+'%');
						}
						$('.charts_addtocart .rect_compare').css('height', compare.ratio.addtocart_ratio+'%');
						$('.charts_placeorder .rect_compare').css('height', compare.ratio.placeorder_ratio+'%');
						$('.charts_complete .rect_compare').css('height', compare.ratio.complete_ratio+'%');
					}else{
						$('.compare').hide();
					}
				}
			}, 'json');
		});
	},
	
	orders_init:function(){
		mta_obj.orders_resize_init();
		$(window).resize(function(){
			mta_obj.orders_resize_init();
		});
		mta_obj.tab_switching();
		mta_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var lang=$('body').hasClass('en')?'en':'zh-cn',
				Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=mta.get_orders_data&Action=ueeshop_analytics_get_order_visits_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare+'&lang='+lang;
			$.post('./', par, function(data){
				//数据整合
				var obj=$('#mta');
				var total=data.msg.total;
				obj.find('.order_price').html(total.total_price);
				obj.find('.order_count').html(total.order_count);
				obj.find('.order_unit_price').html(total.order_unit_price);
				obj.find('.customer_price').html(total.order_customer_price);
				obj.find('.payment_rate').html(total.payment_rate);
				obj.find('.order_customer').html(total.order_customer_count);
				obj.find('.ratio').html(total.ratio);
				obj.find('.visit_customer').html(total.visit_customer);
				obj.find('.discount_price').html(total.discount_price);
				obj.find('.use_times').html(total.coupon_count);
				obj.find('.coupon_rate').html(total.coupon_rate);
				
				//概况
				var prefix=lang_obj.manage.mta.order_amount; //订单金额(默认显示)
				if(data.msg.orders_charts){
					Data=new Object;
					Data['xAxis']={'categories': data.msg.orders_charts.xAxis.categories};
					Data['tooltip']={'valuePrefix': ueeshop_config.currSymbol};
					Data['series']=new Array;
					prefix=data.msg.orders_charts.tooltip.valuePrefix;
					for(k in data.msg.orders_charts.series){
						Data['series'][k]={'name':data.msg.orders_charts.tooltip.valuePrefix, 'data':data.msg.orders_charts.series[k].data};
					}
					frame_obj.highcharts_init.areaspline('line_charts', Data);
				}
				
				//国家分布
				var Data=new Object, country_categories=new Array, country_price=new Object, i=0;
				country_price[0]=new Array;
				country_price[1]=new Array;
				$.each(data.msg.detail.country, function(key, value){
					country_categories[i]=value.title;
					country_price[0][i]=parseFloat(value.price);
					++i;
				});
				i=0;
				if(data.msg.compare){ //对比选项
					$.each(data.msg.compare.detail.country, function(key, value){
						country_price[1][i]=parseFloat(value.price);
						++i;
					});
				}
				Data['xAxis']={'categories': country_categories};
				Data['tooltip']={'valuePrefix': ueeshop_config.currSymbol};
				Data['series']=new Array;
				for(k in country_price){
					Data['series'][k]={'name':prefix, 'data':country_price[k], 'pointWidth':40};
				}
				frame_obj.highcharts_init.column_basic('country_charts', Data);
				
				/*支付*/
				var html='';
				if(data.msg.detail.payment){
					$.each(data.msg.detail.payment, function(key, value){
						html+='<tr data-key="'+key+'">\
							<td nowrap="nowrap" valign="top">'+value['title']+'</td>\
							<td nowrap="nowrap">'+value['rate']+'%<div class="compare compare_peymnet_rate">0</div></td>\
							<td nowrap="nowrap">'+value['price']+'<div class="compare compare_peymnet_price">0</div></td>\
							<td nowrap="nowrap">'+value['count']+'<div class="compare compare_peymnet_count">0</div></td>\
							<td nowrap="nowrap">'+value['pay_rate']+'%<div class="compare compare_peymnet_pay_rate">0</div></td>\
						</tr>';
					});
				}
				$('.box_payment .data_table tbody').html(html);
				
				/*优惠券*/
				var html='';
				if(data.msg.detail.coupon){
					$.each(data.msg.detail.coupon, function(key, value){
						html+='<tr data-key="'+key+'">\
							<td nowrap="nowrap" valign="top">'+value['code']+'</td>\
							<td nowrap="nowrap">'+value['count']+'</td>\
							<td nowrap="nowrap">'+value['rate']+'%</td>\
						</tr>';
					});
				}
				$('.box_coupon .data_table tbody').html(html);
				
				//设备占比
				frame_obj.highcharts_init.pie_legend('browser_charts', data.msg.browser_charts);
				
				//PC端浏览器
				html='';
				$.each(data.msg.coupon.pc, function(key, value){
					html+='<div class="browser_data">\
						<div class="browser_left color_555">'+value['title']+'</div>\
						<div class="browser_right">'+value['count']+'</div>\
					</div>';
				});
				$('.box_browser_pc').html(html);
				
				//移动端浏览器
				html='';
				$.each(data.msg.coupon.mobile, function(key, value){
					html+='<div class="browser_data">\
						<div class="browser_left color_555">'+value['title']+'</div>\
						<div class="browser_right">'+value['count']+'</div>\
					</div>';
				});
				$('.box_browser_mobile').html(html);
				
				//对比选项
				if(Time==-89){
					$('.compare').show();
					var compareTotal=data.msg.compare.total;
					obj.find('.compare_order_price').html(compareTotal.total_price);
					obj.find('.compare_order_count').html(compareTotal.order_count);
					obj.find('.compare_order_unit_price').html(compareTotal.order_unit_price);
					obj.find('.compare_customer_price').html(compareTotal.order_customer_price);
					obj.find('.compare_payment_rate').html(compareTotal.payment_rate);
					obj.find('.compare_order_customer').html(compareTotal.order_customer_count);
					obj.find('.compare_ratio').html(compareTotal.ratio);
					obj.find('.compare_visit_customer').html(compareTotal.visit_customer);
					obj.find('.compare_discount_price').html(compareTotal.discount_price);
					obj.find('.compare_use_times').html(compareTotal.coupon_count);
					obj.find('.compare_coupon_rate').html(compareTotal.coupon_rate);
					
					/*支付*/
					$('.box_payment .data_table tbody tr').each(function(index){
						var key=$(this).attr('data-key');
						var d=data.msg.compare.detail.payment[key];
						if(d){
							$(this).find('.compare_peymnet_rate').html(d.rate);
							$(this).find('.compare_peymnet_price').html(d.price);
							$(this).find('.compare_peymnet_count').html(d.count);
							$(this).find('.compare_peymnet_pay_rate').html(d.pay_rate);
						}
					});

				}else{
					$('.compare').hide();
				}
			}, 'json');
		});
	},
	
	orders_resize_init:function(){
		var $browser=$('.box_orders_browser')
			$browserWidth=$browser.width(),
			$halfWidth=(($browserWidth-$browser.find('.item:eq(0)').outerWidth())/2);
			$W=($browserWidth/10);
		$browser.find('.item:eq(0)').css('width', $W*3);
		$browser.find('.item:eq(1)').css('width', $W*3-31);
		$browser.find('.item:eq(2)').css('width', $W*3-31);
		
		//$browser.find('.item:gt(0)').css('width', $halfWidth-61);
	},
	
	orders_repurchase_init:function(){
		mta_obj.nav_condition(function(_, _, terminal, _, _, mta_cycle){
			var par='do_action=mta.get_orders_repurchase_data&Cycle='+mta_cycle+'&Terminal='+terminal;
			$.post('./', par, function(data){
				frame_obj.highcharts_init.combo_dual_axes('line_charts', data.msg.repurchase_charts);
			}, 'json');
		});
	},
	
	products_sales_init:function(){
		mta_obj.nav_condition(function(time_s, _, terminal){
			var page=parseInt($('#sales_list').attr('data-page'));
			if(!page) page=1;
			var par='do_action=mta.get_products_sales_data&TimeS='+time_s+'&Terminal='+terminal+'&page='+page;
			$.post('./', par, function(data){
				if(data.ret==1 && data.msg.length){
					var strData='';
					$.each(data.msg, function(key, value){
						strData+='<div class="list clean global_container" data-id="'+value['ProId']+'">';
							//产品信息
							strData+='<div class="products">';
								strData+='<div class="img pic_box"><img src="'+value['PicPath']+'" /><span></span></div>';
								strData+='<div class="info">';
									strData+='<p class="name"><a href="'+value['Url']+'" target="_blank">'+value['Name']+'</a></p>';
									strData+='<p class="number">#'+value['Number']+'</p>';
								strData+='</div>';
							strData+='</div>';
							//销量总计
							strData+='<div class="total_sales">';
								strData+='<p>'+value['BuyCount']+'</p>';
								strData+='<p class="number">'+lang_obj.manage.mta.total_sales+'</p>';
							strData+='</div>';
							//国家
							strData+='<div class="country">';
								strData+='<p><strong>'+value['CountryBuyCount']+'</strong><span>'+value['CountryBuyRate']+'</span></p>';
								strData+='<p>'+value['Country']+'</p>';
								strData+='<dl class="country_more"><dt><a href="javascript:;">'+lang_obj.manage.global.more+'<i></i></a></dt><dd class="drop_down country_list"></dd></dl>';
							strData+='</div>';
							//同时购买
							strData+='<div class="related">';
								if(value['IsRelated']==1){
									strData+='<dl class="related_more"><dt><a href="javascript:;">'+lang_obj.manage.mta.also_buy+'<i></i></a></dt><dd class="drop_down related_list"></dd></dl>';
								}
							strData+='</div>';
						strData+='</div>';
					});
					$('#sales_list').show().html(strData);
				}else{
					$('#sales_list').hide();
					$('.bg_no_table_data').show();
				}
			}, 'json');
		}, true);
		
		$('#sales_list').delegate('.country_more, .related_more', 'mouseenter', function(){
			var $This		= $(this),
				$List		= $This.find('.drop_down'),
				$Obj		= $This.parent().parent(),
				$ID			= $Obj.attr('data-id'),
				$TimeS		= $('.mta_menu .box_time a.current').attr('data-time'),
				$Terminal	= $('.mta_menu .box_terminal a.current').attr('data-value'),
				$Par		= 'do_action=mta.get_products_sales_related_data&ProId='+$ID+'&TimeS='+$TimeS+'&Terminal='+$Terminal,
				$Html		= '';
			$This.find('dd').show().stop(true).animate({'top':30, 'opacity':1}, 250);
			if(!$Obj.hasClass('data_loaded')){
				//没有内容，需要加载一次
				$.post('./', $Par, function(data){
					if(data.ret==1){
						//国家
						$Html+='<ul class="related_box">';
						$.each(data.msg.country, function(key, value){
							$Html+='<li>';
								$Html+='<div class="name">'+value['Country']+'</div>';
								$Html+='<div class="count">'+value['CountryBuyCount']+'</div>';
								$Html+='<div class="rate">'+value['CountryBuyRate']+'</div>';
							$Html+='</li>';
						});
						$Html+='</ul>';
						$Obj.find('.country_list').html($Html);
						//同时购买
						$Html='';
						if(data.msg.related){
							$Html+='<ul class="related_box">';
							$.each(data.msg.related, function(key, value){
								$Html+='<li>';
									$Html+='<div class="img pic_box"><img src="'+value['PicPath']+'" /><span></span></div>';
									$Html+='<div class="pro_info">';
										$Html+='<p><a href="'+value['Url']+'" target="_blank">'+value['Name']+'</a></p>';
										$Html+='<p>#'+value['Number']+'</p>';
									$Html+='</div>';
									$Html+='<div class="count">'+value['BuyCount']+'</div>';
								$Html+='</li>';
							});
							$Html+='</ul>';							
						}
						$Obj.find('.related_list').html($Html);
						
						$Obj.addClass('data_loaded');
					}
				},'json');
			}
		}).delegate('.country_more, .related_more', 'mouseleave', function(){
			$(this).find('dd').show().stop(true).animate({'top':20, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
	},
	
	user_init:function(){
		var trend=true;
		mta_obj.tab_switching();
		mta_obj.nav_condition(function(){
			var par='do_action=mta.get_user_data';
			if(trend) par=par+'&trend=1';
			$.post('./', par, function(data){
				$('.data_list .new_member').html(data.msg.total.new);
				$('.data_list .active_member').html(data.msg.total.active);
				$('.data_list .core_member').html(data.msg.total.core);
				$('.data_list .total_member').html(data.msg.total.total);

				if(trend){
					trend=false;
					//会员趋势图
					frame_obj.highcharts_init.combo_dual_axes('trend_charts', data.msg.trend_charts);
					//国家分布图
					frame_obj.highcharts_init.combo_dual_axes('country_charts', data.msg.country_charts);
					/*********************************** 会员性别、等级、年龄、快捷登录 Start ***********************************/
					var html='', i=0, rate_w=0.9,
						colors={'level':'#4ae96b', 'gender':'#38d5e4', 'age':'#52e1b3', 'login':'#618bf7'};
					if($(window).width()<1200){
						rate_w=(($(window).width()/1200).toFixed(2))-0.2;
					}
					
					$.each(data.msg.user_detail, function(key, value){
						$('#'+key+' .rate').radialIndicator({radius:rate_w*100, barColor:colors[key], barWidth:5, percentage:true, fontSize:'28', fontWeight:'normal', fontColor:colors[key]});
						html='';
						i=0;
						$.each(value, function(k2, v2){
							if(i==0){
								$('#'+key+' .rate').data('radialIndicator').animate(v2);
								$('#'+key+' .rate').prepend('<div style="top:'+125*rate_w+'px">'+k2+'</div>');
							}else{
								html+='<div class="list_item clean">\
									<div class="list_item_lbar">'+k2+'</div>\
									<div class="list_item_rbar">\
											<div class="value">'+v2+'%</div>\
											<div class="process"><div style="width:'+v2+'%;" class="color_38d5e4"></div></div>\
									</div>\
								</div>';
							}
							i++;
						});
						$('#'+key+' .data_list').html(html);
					});
					/*********************************** 会员性别、等级、年龄、快捷登录 End ***********************************/
				}
			}, 'json');
		});
	},
	
	nav_condition:function(callback, not_limit){
		if(not_limit){//不限制日期选择
			$('.pop_compared').find('input[name=TimeS], input[name=TimeE]').daterangepicker({
				timePicker: false,
				format: 'YYYY-MM-DD'
			});
		}else{//限制日期选择
			var mydate = new Date();
			var y = mydate.getFullYear();
			var m = mydate.getMonth()+1;
			var d = mydate.getDate();
			var maxD=y+'-'+m+'-'+d;
			var limitM=6;//限制只可以选择最近6个月
			if(m-limitM<1){
				y=y-1;
				m=m+12-limitM;
			}else{
				m=m-limitM;
			}
			var minD=y+'-'+m+'-'+d;
			
			$('.pop_compared').find('input[name=TimeS], input[name=TimeE]').daterangepicker({
				minDate: minD,
				maxDate: maxD,
				timePicker: false,
				format: 'YYYY-MM-DD'
			});
		}
		
		//自定义日期选择
		if($('.pop_compared .custon_time').size()){
			var mydate = new Date();
			var y = mydate.getFullYear();
			var m = mydate.getMonth()+1;
			var d = mydate.getDate();
			var maxD=y+'-'+m+'-'+d;
			var limitM=1;//限制只可以选择最近6个月
			if(m-limitM<1){
				y=y-1;
				m=m+12-limitM;
			}else{
				m=m-limitM;
			}
			var minD=y+'-'+m+'-'+d;
			
			$('.pop_compared').find('.custon_time').daterangepicker({
				minDate: minD,
				maxDate: maxD,
				timePicker: false,
				format: 'YYYY-MM-DD'
			});
		}
		
		$('.mta_menu .item').click(function(){
			var $This=$(this),
				$Menu=$This.parents('.box_drop_down_menu');
			if($Menu.hasClass('box_terminal')){ //设备切换
				$Menu.find('.more>i').attr('class', $This.find('em').attr('class'));
			}else{ //其他
				$Menu.find('.more>span').html($This.html());
			}
			$This.addClass('current').siblings().removeClass('current');
			$Menu.find('.more_menu').removeAttr('style');
			if($Menu.hasClass('box_time')){ //时间切换
				if($This.attr('data-value')==-89){ //对比时间
					$('.mta_menu .legend').show();
				}else{
					$('.mta_menu .legend').hide();
				}
			}
			if($This.attr('data-value')==-99 || ($Menu.hasClass('box_time') && $This.attr('data-value')==-89)){ //自定义时间 Or (时间切换 对比时间)
				var $Obj=$('.pop_compared');
				frame_obj.pop_form($Obj, 0, 1);
				if($This.attr('data-value')==-99){ //自定义时间
					$('#compared_form .h1_compared').hide();
					$('#compared_form .h1_custom').show();
					if($('#compared_form .unit_input:eq(1)').size()){ //产品销量排行除外
						$('#compared_form .unit_input:lt(2)').hide();
						$('#compared_form .custon_time').parent().show();
					}
				}else{ //对比时间
					$('#compared_form .h1_compared').show();
					$('#compared_form .h1_custom').hide();
					$('#compared_form .unit_input:lt(2)').show();
					$('#compared_form .custon_time').parent().hide();
				}
			}else{
				mta_obj.nav_condition_callback(callback);
			}
		});
		
		$('#compared_form .btn_submit, #compared_form .btn_cancel').click(function(){
			var $Form=$('#compared_form'),
				$TimeS=$Form.find('input[name=TimeS]').val(),
				$TimeE=$Form.find('input[name=TimeE]').val();
			$('.mta_menu input[name=TimeS]').val($TimeS);
			$('.mta_menu input[name=TimeE]').val($TimeE);
			$('.mta_menu .legend .time_1').html($TimeS);
			$('.mta_menu .legend .time_2').html($TimeE);
			$('.mta_menu .box_time a[data-value=-99]').attr('data-time', $('.pop_compared .input_time').val());
			$('.pop_compared .t h2').click(); //关闭弹窗
			mta_obj.nav_condition_callback(callback);
			return false;
		});
		
		global_obj.win_alert_auto_close(lang_obj.global.loading, 'loading', -1);
		mta_obj.nav_condition_callback(callback);
		global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
	},
	
	nav_condition_callback:function(callback){
		var compare		= 1,
			time_s		= $('.mta_menu input[name=TimeS]').val(),
			time_e		= $('.mta_menu input[name=TimeE]').val(),
			time		= $('.mta_menu .box_time a.current').attr('data-value'),
			terminal	= $('.mta_menu .box_terminal a.current').attr('data-value'),
			mta_method	= $('.mta_menu .mta_method a.cur').attr('rel'),
			mta_cycle	= $('.mta_menu .box_cycle a.current').attr('data-value');
		if(time!=-89){ //不是对比选项
			time_s=time;
			time_e='';
			compare=0;
		}
		if(time==-99){ //自定义选项
			if($('.pop_compared .custon_time').length){
				time_s=$('.pop_compared .custon_time').val();
			}else{
				time_s=$('.pop_compared .input_time').val();
			}
			time_e='';
			compare=0;
		}
		callback(time_s, time_e, terminal, compare, mta_method, mta_cycle);
	},

	tab_switching:function(){
		$('.box_menu a').click(function(){
			var $index=$(this).index();
			$('.box_menu a').removeClass('cur').eq($index).addClass('cur');
			$('.box_list .box_item').hide().eq($index).show();
			$(window).resize();
		});
	},
}