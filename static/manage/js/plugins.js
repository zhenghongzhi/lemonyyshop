/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var plugins_obj={	
	plugins_init:function(){
		$('#form_app_search .search_btn').click(function(){
			var $Form=$('#form_app_search'),
				$Keyword=$.trim($Form.find('.form_input').val()).toLowerCase();
			if($Keyword){ //带有关键词
				$('.app_container .app_item').hide();
				$('.app_container .app_item[data-name*="'+$Keyword+'"]').show();
			}else{ //显示所有
				$('.app_container .app_item').show();
			}
			return false;
		});
		
		$('.pop_app_menu .app_category_list a').click(function(){
			var $Keyword=$.trim($(this).attr('data-value'));
			if($Keyword){ //带有关键词
				$('.app_container .app_item').hide();
				$('.app_container .app_item[data-name*="'+$Keyword.toLowerCase()+'"]').show();
				$('#form_app_search .form_input').val($Keyword);
			}
			$('.pop_app_menu .t h2').click(); //关闭弹窗
			return false;
		});
		
		$('.btn_app_menu').click(function(){
			var $Obj=$('.pop_app_menu');
			frame_obj.pop_form($Obj, 0, 1);
			frame_obj.Waterfall('app_category_list_box', '183', 4, 'item'); // 瀑布流
		});

		$('.app_header .category_menu a').click(function(){
			var $index=$(this).index();
			$('.app_header .category_menu a').removeClass('current').eq($index).addClass('current');
			$('.app_main .category_box').hide().eq($index).show();
		});
		
		$('.btn_install').click(function(){
			var $Obj=$('.pop_app_install'),
				$ClassName=$(this).parents('.app_item').attr('data-type');
				$Name=$('.app_item_'+$ClassName+' .box_info strong').text();
				$Tips=$(this).parents('.app_item').attr('data-tips');
			frame_obj.pop_form($Obj, 0, 1);
			$Obj.find('.install_bodyer .title>strong').text($Name+(ueeshop_config.manage_language=='zh-cn'?'应用':''));
			$Obj.find('.install_bodyer .infomation>span').text($Name);
			if($Tips) $Obj.find('.install_bodyer .infomation').text($Tips);
			$Obj.find('input[name=ClassName]').val($ClassName);
		});
		
		frame_obj.submit_form_init($('#app_install_form'), '', '', 0, function(data){
			var $Form=$('#app_install_form');
			if(data.ret==1){
				var $ClassName=$Form.find('input[name=ClassName]').val();
				$('.app_item_'+$ClassName).find('.installed').show().next().hide();
				$('.app_item_'+$ClassName).find('.btn_install').hide();
				$('.pop_app_install').fadeOut(250); //关闭弹窗，无须让遮罩层消失
				plugins_obj.plugins_app_account_init($ClassName, 'app');
			}else{
				global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
				$Form.find('.t h2').click(); //关闭弹窗
			}
			$Form.find('.btn_submit').removeAttr('disabled');
			return false;
		});
	},
	
	plugins_my_app_init:function(){
		$('#app .inside_menu li').click(function(){
			var _this=$(this).index();
			$('#app .inside_menu li a').removeClass('current');
			$(this).find('a').addClass('current');
			$('.app_container .app_category_list').hide().eq(_this).show();
		});

		$('.btn_oauth').click(function(){
			var $ClassName=$(this).parent().parent().attr('data-type');
			plugins_obj.plugins_app_account_init($ClassName, 'my_app');
		});
		
		$('.btn_switch').click(function(){
			var $ClassName=$(this).parent().parent().attr('data-type');
			plugins_obj.plugins_app_switch_init($ClassName, 'my_app');
		});
		
		//评论库导入
		$('.btn_review').click(function(){
			var $Obj=$('.app_category_list li[data-type="review"]'),
				$Page=$(this).attr('data-page');
			$Obj.find('.info>p').hide();
			$Obj.find('.import_progress').show();
			$Obj.find('.btn_review').hide();
			$.post('?', {'do_action':'plugins.review', 'Page':$Page}, function(data){
				if(data.ret==2){
					$Obj.find('.import_progress .progress').css('width', data.msg.percent+'%');
					$Obj.find('.import_progress .progress_percent>span').html(data.msg.percent);
					$Obj.find('.btn_review').attr('data-page', data.msg.page).click();
				}else if(data.ret==1){
					$Obj.find('.import_progress .progress').css('width', data.msg.percent+'%');
					$Obj.find('.import_progress .progress_percent>span').html(data.msg.percent);
					global_obj.win_alert_auto_close(data.msg.tips, '', 1000, '8%');
					setTimeout(function(){ //5秒钟后，还原默认状态
						$Obj.find('.info>p').show();
						$Obj.find('.import_progress').hide();
						$Obj.find('.import_progress .progress').removeAttr('style');
						$Obj.find('.import_progress .progress_percent>span').html(0);
						$Obj.find('.btn_review').show().attr('data-page', 1);
					}, 3000);
				}else{
					global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
				}
			}, 'json');
			return false;
		});
		
		frame_obj.switchery_checkbox(function(obj){ //开启
			/*var $ClassName=obj.parent().parent().parent().attr('data-type');
			$.post('?', {'do_action':'plugins.app_used', 'ClassName':$ClassName, 'IsUsed':1}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');*/
		}, function(obj){ //关闭
			var $ClassName=obj.parent().parent().parent().attr('data-type');
			$.post('?', {'do_action':'plugins.app_used', 'ClassName':$ClassName, 'IsUsed':0}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
					obj.hide();
					obj.parent().parent().parent().animate({'opacity':0});
					setTimeout(function(){
						obj.parent().parent().parent().hide();
						$('.app_category_list li[data-type="'+$ClassName+'"]').hide();
					}, 400);
				}
			}, 'json');
		});
	},
	
	plugins_app_account_init:function(ClassName, Position){
		var $Obj=$('.pop_app_oauth');
		$Obj.find('.success_bodyer').addClass('loading');
		$Obj.find('.infomation .data_list, .infomation .content').html('');
		$Obj.find('.picture, .infomation .button input, .infomation a').hide();
		frame_obj.pop_form($Obj, 0, 1);
		$.post('?', {'do_action':'plugins.app_account_info', 'ClassName':ClassName}, function(data){
			if(data.ret==1){	
				var $Html	= '',
					$InChina= parseInt($('#app').attr('data-in-china'));
				for(k in data.msg.Data){
					for(m in data.msg.Data[k]){
						$Html+=	'<div class="rows clean">';
						$Html+=		'<label>'+(data.msg.Label[m]?data.msg.Label[m]:m)+'</label>';
						$Html+=		'<div class="input">';
						$Html+=			'<input type="text" class="box_input" name="Value['+k+'][]" value="'+data.msg.Data[k][m]+'" notnull /><input type="hidden" name="Name['+k+'][]" value="'+m+'">';
						$Html+=		'</div>';
						$Html+=	'</div>';
					}
				}
				if(ClassName=='facebook_store'){ //Facebook专页
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.facebook_store.page+'</label>';
					$Html+=		'<div class="input">'+data.msg.Url+'</div>';
					$Html+=		'<input type="hidden" value="https://www.facebook.com/dialog/pagetab?app_id=APPID&next='+data.msg.Url+'" id="facebook_store_authorized" />';
					$Html+=	'</div>';
				}
				if(ClassName=='facebook_ads_extension'){ //Facebook官方商务插件
					var $IsSetting=(data.msg.FbData && data.msg.FbData.setting_id)?1:0;
					$Html+=	'<div id="facebook_flow">';
					$Html+=	'	<div class="facebook_flow_container">';
					$Html+=	'		<div class="content"></div>';
					$Html+=	'		<div class="store_id'+($IsSetting?'':' hide')+'">'+lang_obj.manage.app.facebook_ads_extension.store_id+': <span id="facebook_store_id">'+($IsSetting?data.msg.FbData.page_id:'')+'</span></div>';
					$Html+=	'		<div class="button">';
					if($InChina==1){ //typeof(plugins_ads)!=='undefined'
						$Html+=	'		<div>'+lang_obj.manage.app.facebook_ads_extension.tips+'</div>';
					}
                    //$Html+=	'			<fb:login-button scope="public_profile,email,manage_pages,ads_management,ads_read" onlogin="plugins_ads.checkLoginState();" class="fb-login-button fb-bg-login-button" data-size="large" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false" data-width="184px" data-height="50px"></fb:login-button>';
                    $Html+=	'			<iframe id="facebook_login_box" src="https://sync.ly200.com/plugin/facebook/login.php?domain='+data.msg['domain']+'" style="width:240px; height:40px;"></iframe>';
					$Html+=	'			<button id="button_settings" style="display:none;">'+($IsSetting?lang_obj.manage.app.facebook_ads_extension.manage_settings:lang_obj.manage.app.facebook_ads_extension.get_started)+'</button>';
					//$Html+=	'		    <button id="button_ad" style="display:none;"'+($IsSetting?'':' class="hide"')+'>'+lang_obj.manage.app.facebook_ads_extension.create_ad+'</button>';
					$Html+=	'		</div>';
					$Html+=	'	</div>';
					$Html+=	'</div>';
				}	
				if(ClassName=='massive_email' && data.msg.Email && data.msg.Key){
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.massive_email.authorized_email+'</label>';
					$Html+=		'<div class="input">'+data.msg.Email+'</div>';
					$Html+=	'</div>';
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.massive_email.authorized_key+'</label>';
					$Html+=		'<div class="input">'+data.msg.Key+'</div>';
					$Html+=	'</div>';
				}
				if(ClassName=='dhl_account_info'){
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.dhl_account_info.client_id+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="text" class="box_input" name="ClientId" value="'+data.msg.ClientId+'" notnull />';
					$Html+=		'</div>';
					$Html+=	'</div>';
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.dhl_account_info.password+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="password" class="box_input" name="Password" value="'+data.msg.Password+'" notnull />';
					$Html+=		'</div>';
					$Html+=	'</div>';
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.dhl_account_info.pickupAccountId+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="text" class="box_input" name="pickupAccountId" value="'+data.msg.pickupAccountId+'" notnull maxlength="10" />';
					$Html+=		'</div>';
					$Html+=	'</div>';
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.dhl_account_info.soldToAccountId+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="text" class="box_input" name="soldToAccountId" value="'+data.msg.soldToAccountId+'" notnull maxlength="10" />';
					$Html+=		'</div>';
					$Html+=	'</div>';
				}
				if(ClassName=='asiafly'){
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.asiafly.merchantCode+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="text" class="box_input" name="merchantCode" value="'+data.msg.merchantCode+'" notnull />';
					$Html+=		'</div>';
					$Html+=	'</div>';
					$Html+=	'<div class="rows clean">';
					$Html+=		'<label>'+lang_obj.manage.app.asiafly.privateKey+'</label>';
					$Html+=		'<div class="input">';
					$Html+=			'<input type="text" class="box_input" name="privateKey" value="'+data.msg.privateKey+'" notnull />';
					$Html+=		'</div>';
					$Html+=	'</div>';
				}
				$Obj.find('.success_bodyer').removeClass('loading');
				$Obj.find('.infomation .data_list').html($Html);
				Position=='my_app' && $Obj.find('.t span').text(data.msg.Title);
				$Obj.find('.infomation .content').html(data.msg.Tips).show();
				if(ueeshop_config.FunVersion>=10){
					$Obj.find('.picture>img').attr('src', '/static/manage/images/plugins/cdx/pic_success_'+ClassName+'.png');
				}else{
					$Obj.find('.picture>img').attr('src', '/static/manage/images/plugins/pic_success_'+ClassName+'.png');
				}
				$Obj.find('.infomation').show();
				$Obj.find('.picture').show();
				$Obj.find('.form_data').hide();
				$IsShowDataList=0;
				if(ClassName=='batch_edit'){ //批量修改
					$Obj.find('.infomation').hide();
					$Obj.find('.picture').hide();
					if(!$Obj.find('.success_bodyer .form_data').length) $Obj.find('.success_bodyer').append('<div class="form_data"></div>');
					$Obj.find('.form_data').show();
					$Obj.find('.form_data').html(data.msg.Content);
				}else if(ClassName=='facebook_store'){ //Facebook专页
					$IsShowDataList=1;
					$Obj.find('.button .btn_authorized').show().siblings().hide();
				}else if(ClassName=='facebook_ads_extension'){ //Facebook官方商务插件
					$IsShowDataList=1;
					$Obj.find('.infomation>.content').hide();
					$Obj.find('.button>input, .button>a').hide();
					$Obj.find('.picture').hide();
				}else if(ClassName=='paypal_marketing_solution'){ //Paypal营销解决方案
					$Obj.find('.infomation .data_list, .infomation>.content').hide();
					$Obj.find('.button>input, .button>a').hide();
					$Obj.find('.picture').hide();
				}else if(ClassName=='massive_email'){ //邮件群发
					$IsShowDataList=1;
					$Obj.find('.button .btn_authorized').show().siblings().hide();
				}else if(ClassName=='dhl_account_info'){ //DHL账号信息
					$IsShowDataList=1;
					$Obj.find('.button .btn_save').show().siblings('.btn_know').hide();
				}else if(ClassName=='asiafly'){ //DHL账号信息
					$IsShowDataList=1;
					$Obj.find('.button .btn_save').show().siblings('.btn_know').hide();
				}else if(data.msg.Data){ //表单填写
					$IsShowDataList=1;
					$Obj.find('.button .btn_save').show().siblings().hide();
				}else{
					$Obj.find('.infomation .data_list').hide();
					$Obj.find('.button .btn_know').show().siblings().hide();
				}				
				if(data.msg.ReturnUrl){
					$Obj.find('.button .btn_enter').attr('data-url', data.msg.ReturnUrl).show().siblings('.btn_know').hide();
				}
				if($IsShowDataList==1){
					$Obj.find('.infomation .data_list').show();
				}
				$Obj.find('input[name=ClassName]').val(ClassName);
				if(ClassName=='facebook_store'){
					//Facebook专页
					plugins_obj.plugins_app_facebook_store_init();
					$('.pop_app_oauth').off().on('keyup', '.data_list .rows:eq(0) .box_input', function(e){
						var $Value	= $.trim($(this).val()),
							$Key	= window.event?e.keyCode:e.which;
						if($Key==13){ //回车键
							return false;
						}else{ //其他按键
							plugins_obj.plugins_app_facebook_store_init();
						}
					}).on('click', '.btn_authorized', function(){
						window.open($(this).attr('data-url'), '', 'width=1000,height=500');
					});
				}else if(ClassName=='massive_email'){
					//邮件群发
					var Email=$('#app_account_info_form .data_list').find('Value\\[Email\\]\\[\\]').val();
					$.post('?', {'do_action':'plugins.massive_email_url'}, function(data){
						$('.pop_app_oauth').find('.btn_authorized').attr('data-url', data.msg.url);
					}, 'json');
					$('#app_account_info_form').off().on('click', '.btn_authorized', function(){
						window.open($(this).attr('data-url'), '', 'width=1000,height=500');
					});
				}else if(ClassName=='dhl_account_info'){
					// DHL账号信息
					$('.pop_app_oauth').find('.btn_set').attr('data-url', './?m=plugins&a=dhl_account_info');
					$('#app_account_info_form').off().on('click', '.btn_set', function(){
						window.location.href=$(this).attr('data-url');
					});
				}
				if(ClassName=='facebook_ads_extension'){
					//Facebook官方商务插件
                    //插件数据
					window.facebookAdsToolboxConfig=data.msg.Config;
					window.facebookAdsConfig=new Object;
					window.facebookMessenger=new Object;
					$('.pop_app_oauth').off().on('click', '#button_settings', function(){
						plugins_ads.launchDiaWizard();
					}).on('click', '#button_ad', function(){
						plugins_ads.launchAutomatedAds();
					});
                    plugins_ads.getTokenInfo();
				}
				if(ClassName=='paypal_marketing_solution'){
					//Paypal营销解决方案
                    $('#btn_paypal_marketing').remove();
					$.getScript('//www.paypalobjects.com/muse/partners/muse-button-bundle.js', function(){
						$('.pop_app_oauth .success_bodyer .button').append('<button id="btn_paypal_marketing"></button>');
						var $Data={
							onContainerCreate: function(ContainerId){
								$.post('?', {'do_action':'plugins.app_paypal_marketing_solution_edit', 'ContainerId':ContainerId}, function(result){
									if(result.ret==1){
										global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
										$Form.find('.t h2').click(); //关闭弹窗
									}
								}, 'json');
							},
							styles:{
								button: {
									width:'auto', height:'32px', background:'#0cb083', borderRadius:'4px', padding:'0 30px', color:'#fff', ':hover':{background:'#0cb083'}
								}
							},
							// env: 'sandbox',
                            url: ueeshop_config.domain, //Merchant’s website URL
                            parnter_name: 'ueeshop', //Your partner name
                            bn_code: 'ueeshop_Cart', //Partner code provided by PayPal partner sales rep
						}
						if(data.msg.ContainerId){
							$Data.cid=data.msg.ContainerId;
						}
						MUSEButton('btn_paypal_marketing', $Data);
					});
				}
				if(data.msg.Data && data.msg.Data.SignIn || ClassName=='massive_email'){
					//群发邮件
					var oauth_item	= ClassName.replace('_login',''),
						help_url	= 'http://www.ueeshop.com/u_file/other/4.0/'+oauth_item+'.pdf';
					$Obj.find('.button .help').attr('href',help_url).show();
				}
				if(ueeshop_config.FunVersion>=10){
					var help_url_ext;
					if(ClassName=='google_login'){
						help_url_ext='http://help.shop.shopcto.com/i-493.html?IsShop=1';
					}else if(ClassName=='paypal_login'){
						help_url_ext='http://help.shop.shopcto.com/i-472.html?IsShop=1';
					}else if(ClassName=='twitter_login'){
						help_url_ext='http://help.shop.shopcto.com/i-442.html?IsShop=1';
					}else if(ClassName=='facebook_login'){
						help_url_ext='http://help.shop.shopcto.com/i-428.html?IsShop=1';
					}else if(ClassName=='facebook_messenger'){
						help_url_ext='http://help.shop.shopcto.com/i-477.html?IsShop=1';
					}else if(ClassName=='googlefeed'){
						help_url_ext='http://help.shop.shopcto.com/i-375.html?IsShop=1';
					}else if(ClassName=='shopify'){
						help_url_ext='http://help.shop.shopcto.com/i-482.html?IsShop=1';
					}else if(ClassName=='wish'){
						help_url_ext='http://help.shop.shopcto.com/i-478.html?IsShop=1';
					}else if(ClassName=='amazon'){
						help_url_ext='http://help.shop.shopcto.com/i-457.html?IsShop=1';
					}else if(ClassName=='google_pixel'){
						help_url_ext='http://help.shop.shopcto.com/i-483.html?IsShop=1';
					}else if(ClassName=='facebook_store'){
						help_url_ext='http://help.shop.shopcto.com/i-372.html';
					}else if(ClassName=='vk_login'){
						help_url_ext='http://help.shop.shopcto.com/i-343.html';
					}else if(ClassName=='facebook_pixel'){
						help_url_ext='http://help.shop.shopcto.com/i-327.html';
					}
					if(help_url_ext) $Obj.find('.button .help').attr('href',help_url_ext).show();
				}
			}
		}, 'json');
		
		//保存
		$('#app_account_info_form').submit(function(){ return false; });
		$('body, html').on('click', '.pop_app_oauth .btn_save, .pop_app_oauth .btn_authorized', function(){
			var $Form=$('#app_account_info_form'),
				$Stop=0;
			$Form.find('*[notnull]').each(function(){
				if($.trim($(this).val())==''){
					$(this).css('border-color', 'red');
					$Stop=1;
				}else{
					$(this).removeAttr('style');
				}
			});
			if($Stop>0){ return false; }
			$.post('?', $Form.serialize(), function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
					$Form.find('.t h2').click(); //关闭弹窗
				}
			}, 'json');
			return false;
		});
		
		//知道了
		$('body, html').on('click', '.pop_app_oauth .btn_know', function(){
			var $Form=$('#app_account_info_form');
			$Form.find('.t h2').click(); //关闭弹窗
			return false;
		});

		//进入
		$('body, html').on('click', '.pop_app_oauth .btn_enter', function(){
			location.href=$(this).attr('data-url');
			return false;
		});
	},
	
	//Facebook专页授权地址的生成
	plugins_app_facebook_store_init:function(){
		var $Url=$('#facebook_store_authorized').val(),
			$AppId=$.trim($('.pop_app_oauth .data_list .rows:eq(0) .box_input').val());
		if($AppId){
			$('.pop_app_oauth .btn_authorized').attr('data-url', $Url.replace('APPID', $AppId));
		}else{
			$('.pop_app_oauth .btn_authorized').attr('data-url', '');
		}
	},
	
	plugins_app_switch_init:function(ClassName, Position){
		var $Obj=$('.pop_app_switch');
		frame_obj.pop_form($Obj, 0, 1);
		$.post('?', {'do_action':'plugins.app_switch', 'ClassName':ClassName}, function(data){
			if(data.ret==1){
				Position=='my_app' && $Obj.find('.t span').text(data.msg.Title);
				$Obj.find('.infomation').html(data.msg.Content);
				$Obj.find('input[name=ClassName]').val(ClassName);
				if(ClassName=='batch_edit'){
					$Obj.find('.button .btn_save').show();
				}else{
					$Obj.find('.button .btn_save').hide();
				}
				$('.infomation .tool_tips_ico').each(function(){
					if($(this).attr('content')==''){
						$(this).hide();
						return;
					}else{
						$(this).html('&nbsp;');
						$('#main .r_con_wrap').tool_tips($(this), {position:'horizontal', html:$(this).attr('content'), width:260});
					}
				});
			}
		}, 'json');
		
		$('#main').off('click', '.pop_app_switch .switchery').on('click', '.pop_app_switch .switchery', function(){
			var $This=$(this),
				$Form=$('#app_switch_form'),
				$IsChecked=$This.hasClass('checked')?1:0;
			$.post('?', {'do_action':'plugins.app_switch_change', 'Type':$This.find('input:checkbox').attr('name'), 'Status':$IsChecked}, function(data){
				if(data.ret==1){
					if($IsChecked==1){ //关闭
						$This.removeClass('checked').find('input').attr('checked', false);
						if($This.find('input[name=IsCloseWeb]').length){ //关闭网站
							$('.close_web_box').hide();
						}
						if($This.find('input[name=NumberSort]').length){ //编号自动排序
							$('.number_box').hide();
						}
					}else{ //开启
						$This.addClass('checked').find('input').attr('checked', true);
						if($This.find('input[name=IsCloseWeb]').length){ //关闭网站
							$('.close_web_box').show();
						}
						if($This.find('input[name=NumberSort]').length){ //编号自动排序
							$('.number_box').css('display', 'inline-block');
						}
					}
				}else{
					global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
				}
			}, 'json');
			return false;
		});
		
		$('#main').on('click', '.pop_app_switch .input_radio_box', function(){
			var $This=$(this);
			$Form=$('#app_switch_form'),
			$IsChecked=$This.find('input:radio').val();
			$.post('?', {'do_action':'plugins.app_switch_change', 'Type':$This.find('input:radio').attr('name'), 'Status':$IsChecked}, function(data){
				if(data.ret==1){
					var name=$This.find('input').attr('name');
					$('input[name='+name+']').removeAttr('checked').parent().parent().removeClass('checked');
					$This.addClass('checked').find('input').attr('checked', true);
				}else{
					global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
				}
			}, 'json');
			return false;
		});
		
		$('#app_switch_form').submit(function(){ return false });
		$('#main').off('keyup', '.pop_app_switch .number_box .box_input').on('keyup', '.pop_app_switch .number_box .box_input', function(e){
			var $Form=$('#app_switch_form'),
				value=$.trim($(this).val()),
				key=window.event?e.keyCode:e.which,
				html;
			if(key==13){ //回车键
				$.post('?', $Form.serialize(), function(data){
					if(data.ret==1){
						global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
						$Form.find('.t h2').click(); //关闭弹窗
					}
				}, 'json');
			}
			return false;
		});
		
		//保存
		$('#main').off('click', '.pop_app_switch .btn_save').on('click', '.pop_app_switch .btn_save', function(){
			var $Form=$('#app_switch_form');
			if(typeof(CKEDITOR)=='object'){
				for(var i in CKEDITOR.instances) CKEDITOR.instances[i].updateElement();//更新编辑器内容
			}
			$.post('?', $Form.serialize(), function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
					$Form.find('.t h2').click(); //关闭弹窗
				}
			}, 'json');
			return false;
		});
	},
	
	//买家秀start
	gallery_init:function(){
		frame_obj.del_init($('#gallery .r_con_table'));
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('#gallery .r_con_table input[name=select]'), '', function(id_list){
			var $this=$(this);
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.get('./?do_action=plugins.gallery_del_bat&group_pid='+id_list, function(data){
					if(data.ret==1){
						window.location=window.location;
					}
				}, 'json');
			}, 'confirm');
			return false;
		});
	},
	
	gallery_edit_init:function(){
		/* 友情连接图片上传 */
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', 'info', 1);
		});

		$('.upload_pic .del').on('click', function(){
			var $this=$(this);
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.ajax({
					url:'./?do_action=action.file_del&PicPath='+$this.prev().attr('href'),
					success:function(){
					}
				});
			}, 'confirm');
			return false;
		});
		frame_obj.submit_form_init($('#gallery_edit_form'),$('#gallery_edit_form').attr('data-url'));
	},
	//买家秀end
	
	//分销start
	distribution_init:function(){
		$('input[name=WithdrawTime]').daterangepicker({
			showDropdowns:true,
			timePicker:false,
			format:'YYYY-MM-DD'
		});
		
		frame_obj.submit_form_init($('#dis_edit_form'), $('#dis_edit_form').attr('data-url'));

	},
	
	distribution_orders_init:function(){
		plugins_obj.distribution_orders_resize_init();
		$(window).resize(function(){
			plugins_obj.distribution_orders_resize_init();
		});
		
		plugins_obj.nav_condition(function(time_s, time_e, terminal, compare){
			var lang=$('body').hasClass('en')?'en':'zh-cn',
				Time=$('.mta_menu .box_time a.current').attr('data-value'),
				par='do_action=plugins.get_orders_data&Action=ueeshop_analytics_get_order_visits_data&TimeS='+time_s+'&TimeE='+time_e+'&Terminal='+terminal+'&Compare='+compare+'&lang='+lang;
			$.post('./', par, function(data){
				//数据整合
				var obj=$('#mta');
				var total=data.msg.total;
				obj.find('.distor_order_price').html(total.distor_order_price);
				obj.find('.distor_order_count').html(total.distor_order_count);
				obj.find('.order_price').html(total.total_price);
				obj.find('.order_count').html(total.order_count);
				obj.find('.distributor').html(total.distributor);
				obj.find('.commission').html(total.commission);
				
				//概况
				var prefix=lang_obj.manage.mta.order_amount; //订单金额(默认显示)
				if(data.msg.orders_charts){
					Data=new Object;
					Data['xAxis']={'categories': data.msg.orders_charts.xAxis.categories};
					Data['tooltip']={'valuePrefix': ueeshop_config.currSymbol};
					Data['series']=new Array;
					prefix=data.msg.orders_charts.tooltip.valuePrefix;
					for(k in data.msg.orders_charts.series){
						Data['series'][k]={'name':data.msg.orders_charts.tooltip.valuePrefix, 'data':data.msg.orders_charts.series[k].data};
					}
					frame_obj.highcharts_init.areaspline('line_charts', Data);
				}
				
				//分销率
				frame_obj.highcharts_init.combo_dual_axes('count_charts', data.msg.count_charts);
				
				//对比选项
				if(Time==-99){
					$('.compare').show();
					var compareTotal=data.msg.compare.total;
					obj.find('.compare_distor_order_price').html(compareTotal.distor_order_price);
					obj.find('.compare_distor_order_count').html(compareTotal.distor_order_count);
					obj.find('.compare_order_price').html(compareTotal.total_price);
					obj.find('.compare_order_count').html(compareTotal.order_count);
					obj.find('.compare_distributor').html(compareTotal.distributor);
					obj.find('.compare_commission').html(compareTotal.commission);
				}else{
					$('.compare').hide();
				}
			}, 'json');
		});
	},
	
	distribution_orders_resize_init:function(){
		var $browser=$('.box_orders_browser')
			$browserWidth=$browser.width(),
			$halfWidth=(($browserWidth-$browser.find('.item:eq(0)').outerWidth())/2);
			$W=($browserWidth/10);
		$browser.find('.item:eq(0)').css('width', $W*3);
		$browser.find('.item:eq(1)').css('width', $W*3-31);
		$browser.find('.item:eq(2)').css('width', $W*3-31);
		
		//$browser.find('.item:gt(0)').css('width', $halfWidth-61);
	},
	
	nav_condition:function(callback, not_limit){
		if(not_limit){//不限制日期选择
			$('.pop_compared').find('input[name=TimeS], input[name=TimeE]').daterangepicker({
				timePicker: false,
				format: 'YYYY-MM-DD'
			});
		}else{//限制日期选择
			var mydate = new Date();
			var y = mydate.getFullYear();
			var m = mydate.getMonth()+1;
			var d = mydate.getDate();
			var maxD=y+'-'+m+'-'+d;
			var limitM=6;//限制只可以选择最近6个月
			if(m-limitM<1){
				y=y-1;
				m=m+12-limitM;
			}else{
				m=m-limitM;
			}
			var minD=y+'-'+m+'-'+d;
			
			$('.pop_compared').find('input[name=TimeS], input[name=TimeE]').daterangepicker({
				minDate: minD,
				maxDate: maxD,
				timePicker: false,
				format: 'YYYY-MM-DD'
			});
		}
		
		$('.mta_menu .item').click(function(){
			var $This=$(this),
				$Menu=$This.parents('.box_drop_down_menu');
			if($Menu.hasClass('box_terminal')){ //设备切换
				$Menu.find('.more>i').attr('class', $This.find('em').attr('class'));
			}else{ //其他
				$Menu.find('.more>span').html($This.html());
			}
			$This.addClass('current').siblings().removeClass('current');
			$Menu.find('.more_menu').removeAttr('style');
			if($Menu.hasClass('box_time')){ //时间切换
				if($This.attr('data-value')==-99){ //对比时间
					$('.mta_menu .legend').show();
				}else{
					$('.mta_menu .legend').hide();
				}
			}
			if($Menu.hasClass('box_time') && $This.attr('data-value')==-99){ //时间切换 对比时间
				var $Obj=$('.pop_compared');
				frame_obj.pop_form($Obj, 0, 1);
			}else{
				plugins_obj.nav_condition_callback(callback);
			}
		});
		
		$('#compared_form .btn_submit, #compared_form .btn_cancel').click(function(){
			var $Form=$('#compared_form'),
				$TimeS=$Form.find('input[name=TimeS]').val(),
				$TimeE=$Form.find('input[name=TimeE]').val();
			$('.mta_menu input[name=TimeS]').val($TimeS);
			$('.mta_menu input[name=TimeE]').val($TimeE);
			$('.mta_menu .legend .time_1').html($TimeS);
			$('.mta_menu .legend .time_2').html($TimeE);
			$('.pop_compared .t h2').click(); //关闭弹窗
			plugins_obj.nav_condition_callback(callback);
			return false;
		});
		
		global_obj.win_alert_auto_close(lang_obj.global.loading, 'loading', -1);
		plugins_obj.nav_condition_callback(callback);
		global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
	},
	
	nav_condition_callback:function(callback){
		var compare		= 1,
			time_s		= $('.mta_menu input[name=TimeS]').val(),
			time_e		= $('.mta_menu input[name=TimeE]').val(),
			time		= $('.mta_menu .box_time a.current').attr('data-value'),
			terminal	= $('.mta_menu .box_terminal a.current').attr('data-value'),
			mta_method	= $('.mta_menu .mta_method a.cur').attr('rel'),
			mta_cycle	= $('.mta_menu .box_cycle a.current').attr('data-value');
		if(time>-99){ //不是对比选项
			time_s=time;
			time_e='';
			compare=0;
		}
		callback(time_s, time_e, terminal, compare, mta_method, mta_cycle);
	},
	
	//Facebook专页店铺start
	facebook_store_init:function(){
		$('.abs_item').click(function(){
			var $this=$(this);
			$('.abs_item').removeClass('cur');
			$this.addClass('cur');
			plugins_obj.load_edit_form('.index_set_exit', './?m=operation&a=facebook_store&d=index_set', 'get', '&WId='+$this.attr('data-wid'), function(){
				if($this.attr('data-type')=='Products'){
					product_init();
				}else{
					web_init();
				}
				frame_obj.upload_img_init();
				frame_obj.submit_form_init($('#index_set_edit_form'), $('input[name=return_url]').val()+'&WId='+$('.abs_item.cur').attr('data-wid'));
			});
		});
		if($('.abs_item.cur').length){
			$('.abs_item.cur').click();
		}else{
			$('.abs_item').eq(0).click();
		}
		function web_init(){
			frame_obj.mouse_click($('#index_set .multi_img .upload_btn, #index_set .multi_img .pic_btn .edit'), 'img', function($this){ //点击上传图片
				var $id=$this.parents('.multi_img').attr('id');
				frame_obj.photo_choice_init($id, '', 1);
			});
		}
		function product_init(){
			frame_obj.fixed_right($('#facebook_store .set_add'), '.fixed_add_products');
			//frame_obj.submit_form_init($('#edit_form'), $('#edit_form').attr('data-return'));
			$('.add_products_list').on('click', '.add_products_item', function(){
				var $obj=$(this),
					$proid=$obj.attr('data-proid');
				if($obj.hasClass('cur')){
					$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
					$obj.removeClass('cur').append($('.set_products_list').find('.set_products_item[data-proid='+$proid+']'));
				}else{
					if(parseInt($('.set_products_list').attr('max'))<=$('.set_products_list .set_products_item').length){ //节日模板限制个数
						global_obj.win_alert(lang_obj.manage.products.beyondNumber+$('.set_products_list').attr('max'));
						return false;
					}
					if($('#proid_hide').val()){
						$('#proid_hide')[0].value+=$proid+'|';
					}else{
						$('#proid_hide').val('|'+$proid+'|');
					}
					$obj.addClass('cur');
					$('.set_products_list').append($obj.find('.set_products_item'));
				}
			});
			$('#facebook_store').on('click', '.set_products_item .del', function(){
				var $obj=$(this),
					$proid=$obj.parents('.set_products_item').attr('data-proid');
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					if($('.add_products_list').find('.add_products_item[data-proid='+$proid+']').length){
						$('.add_products_list').find('.add_products_item[data-proid='+$proid+']').removeClass('cur').append($obj.parents('.set_products_item'));
					}else{
						$obj.parents('.set_products_item').remove();
					}
					$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
				},'confirm');
			}).on('click', '.set_products_list .main', function(){
				var $obj=$(this),
					$proid=$obj.parents('.set_products_item').attr('data-proid'),
					$old_proid=parseInt($('#proid_hide').val());
				if($old_proid){
					$('#proid_hide')[0].value+=$old_proid+'|';
					$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
	
				}else{
					$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
				}
				$('.set_products_list').find('.main').removeClass('p_main');
				$obj.addClass('p_main')
				$('#proid_hide').val($proid);
			}).on('mouseover', '.attr_list .more', function(){
				$(this).hide().parent().addClass('cur');
			}).on('mouseleave', '.set_products_item', function(){
				$(this).find('.attr_abs').removeClass('cur').find('.more').show();
			});
			//右侧产品滚动加载
			var is_animate=0;
			$('#fixed_right').scroll(function(){
				var viewHeight=$('#fixed_right').height(), //可见高度  
					contentHeight=$("#fixed_right").get(0).scrollHeight, //内容高度  
					scrollHeight=$('#fixed_right').scrollTop(), //滚动高度 
					page=parseInt($('.add_products_list').attr('data-page')),
					total_pages=parseInt($('.add_products_list').attr('data-total-pages'));
				if((contentHeight-viewHeight<=scrollHeight) && is_animate==0 && page<=total_pages){ 
					is_animate=1;
					plugins_obj.load_edit_form('.add_products_list', './?'+$('.search_form form').serialize()+'&remove_pid='+$('#proid_hide').val(), 'get', '&page='+(page+1), function(){
						$('.add_products_list').attr('data-page', (page+1));
						is_animate=0;
					}, 'append');
				}  
				if(page>=total_pages && is_animate==0){
					global_obj.win_alert_auto_close(lang_obj.manage.products.lastpage, 'await', 2000, '8%', 0);
					is_animate=1;
				}
			});
			$('.search_form form').submit(function(){
				plugins_obj.load_edit_form('.add_products_list', './?'+$(this).serialize()+'&remove_pid='+$('#proid_hide').val(), 'get', '', function(data){
					is_animate=0;
					$('.add_products_list').attr('data-total-pages', $(data).find('.add_products_list').attr('data-total-pages')).attr('data-page', 1);
				});
				return false;
			});
			frame_obj.dragsort($('#set_products_list'), '', 'div', 'a, input', '<div class="set_products_item placeHolder"></div>', 'div.set_products_item'); //产品拖动
			
			frame_obj.submit_form_init($('#edit_form'), '', '', '', function(data){
				if(data.ret==1){
					window.top.location.reload();
				}else{
					global_obj.win_alert(data.msg);
				}
			});
		}
		/*
		//图片上传
		frame_obj.dragsort($('.ad_drag'), '', '.adpic_row', '.ad_view, .l_img', '<li class="adpic_row placeHolder"></li>');
		frame_obj.mouse_click($('.multi_img .upload_btn, .pic_btn .edit'), 'ad', function($this){ //产品颜色图点击事件
			var $id=$this.attr('id'),
				$lang=$this.parents('.img').attr('data-lang'),
				$num=$this.parents('.img').attr('num'),
				piccount=parseInt($this.attr('data-count'));
			frame_obj.photo_choice_init('PicDetail_'+$lang+' .img[num;'+$num+']', 'ad', 5, 'do_action=products.products_img_del&Model=products');
		});
		
		frame_obj.fixed_right($('#facebook_store .set_add'), '.fixed_add_products');
		frame_obj.submit_form_init($('#edit_form'), $('#edit_form').attr('data-return'));
		$('.add_products_list').on('click', '.add_products_item', function(){
			var $obj=$(this),
				$proid=$obj.attr('data-proid');
			if($obj.hasClass('cur')){
				$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
				$obj.removeClass('cur').append($('.set_products_list').find('.set_products_item[data-proid='+$proid+']'));
			}else{
				if(parseInt($('.set_products_list').attr('max'))<=$('.set_products_list .set_products_item').length){ //节日模板限制个数
					global_obj.win_alert(lang_obj.manage.products.beyondNumber+$('.set_products_list').attr('max'));
					return false;
				}
				if($('#proid_hide').val()){
					$('#proid_hide')[0].value+=$proid+'|';
				}else{
					$('#proid_hide').val('|'+$proid+'|');
				}
				$obj.addClass('cur');
				$('.set_products_list').append($obj.find('.set_products_item'));
			}
		});
		$('#facebook_store').on('click', '.set_products_item .del', function(){
			var $obj=$(this),
				$proid=$obj.parents('.set_products_item').attr('data-proid');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				if($('.add_products_list').find('.add_products_item[data-proid='+$proid+']').length){
					$('.add_products_list').find('.add_products_item[data-proid='+$proid+']').removeClass('cur').append($obj.parents('.set_products_item'));
				}else{
					$obj.parents('.set_products_item').remove();
				}
				$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
			},'confirm');
		}).on('click', '.set_products_list .main', function(){
			var $obj=$(this),
				$proid=$obj.parents('.set_products_item').attr('data-proid'),
				$old_proid=parseInt($('#proid_hide').val());
			if($old_proid){
				$('#proid_hide')[0].value+=$old_proid+'|';
				$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));

			}else{
				$('#proid_hide').val($('#proid_hide').val().replace($proid+'|', ''));
			}
			$('.set_products_list').find('.main').removeClass('p_main');
			$obj.addClass('p_main')
			$('#proid_hide').val($proid);
		}).on('mouseover', '.attr_list .more', function(){
			$(this).hide().parent().addClass('cur');
		}).on('mouseleave', '.set_products_item', function(){
			$(this).find('.attr_abs').removeClass('cur').find('.more').show();
		});
		//右侧产品滚动加载
		var is_animate=0;
		$('#fixed_right').scroll(function(){
			var viewHeight=$('#fixed_right').height(), //可见高度  
				contentHeight=$("#fixed_right").get(0).scrollHeight, //内容高度  
				scrollHeight=$('#fixed_right').scrollTop(), //滚动高度 
				page=parseInt($('.add_products_list').attr('data-page')),
				total_pages=parseInt($('.add_products_list').attr('data-total-pages'));
			if((contentHeight-viewHeight<=scrollHeight) && is_animate==0 && page<=total_pages){ 
				is_animate=1;
				plugins_obj.load_edit_form('.add_products_list', './?'+$('.search_form form').serialize()+'&remove_pid='+$('#proid_hide').val(), 'get', '&page='+(page+1), function(){
					$('.add_products_list').attr('data-page', (page+1));
					is_animate=0;
				}, 'append');
			}  
			if(page>=total_pages && is_animate==0){
				global_obj.win_alert_auto_close(lang_obj.manage.products.lastpage, 'await', 2000, '8%', 0);
				is_animate=1;
			}
		});
		$('.search_form form').submit(function(){
			plugins_obj.load_edit_form('.add_products_list', './?'+$(this).serialize()+'&remove_pid='+$('#proid_hide').val(), 'get', '', function(data){
				is_animate=0;
				$('.add_products_list').attr('data-total-pages', $(data).find('.add_products_list').attr('data-total-pages')).attr('data-page', 1);
			});
			return false;
		});
		frame_obj.dragsort($('#set_products_list'), '', 'div', 'a, input', '<div class="set_products_item placeHolder"></div>', 'div.set_products_item'); //产品拖动
		
		frame_obj.submit_form_init($('#edit_form'), '', '', '', function(data){
			if(data.ret==1){
				window.top.location.reload();
			}else{
				global_obj.win_alert(data.msg);
			}
		});
		*/
	},
	
	load_edit_form: function(target_obj, url, type, value, callback, fuc){
		$.ajax({
			type:type,
			url:url+value,
			success:function(data){
				if(fuc=='append'){
					$(target_obj).append($(data).find(target_obj).html());
				}else{
					$(target_obj).html($(data).find(target_obj).html());
				}
				callback && callback(data);
			}
		});
	},
	//Facebook专页店铺end
	
	//Google Feed start
	googlefeed_edit:function(){
		frame_obj.submit_form_init($('#googlefeed_edit_form'), './?m=operation&a=googlefeed');
	},
	googlefeed_update:function(){
		frame_obj.submit_form_init($('#googlefeed_update_form'), '', '', '', function(data){
			$('#updating').append('<li>'+data.msg+'</li>');
			if(data.ret==-1){
				$('#googlefeed_update_form .btn_submit').click();
			}else{
				global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
			}
		});
	},
	//Google Feed end
	
	//交换链接 start
	swap_chain_init:function(){
		frame_obj.fixed_right($('#swap_chain .add'), '.swap_chain_add');
		frame_obj.submit_form_init($('#swap_chain_form'));
		frame_obj.del_init($('#swap_chain .r_con_table'));
		$('#swap_chain .refresh').click(function(){
			$.get('?',{'do_action':'plugins.swap_chain_refresh', 'UrlsId':$(this).data('urlsid'), 'AllocationId':$(this).data('allocationid')},function(data){
				if(data.ret==1){
					window.location.reload();
				}else{
					global_obj.win_alert(data.msg);
				}
			},'json');
		});
	},
	//交换链接 end
	
	//google验证文件 start
	google_verification_init:function(){
		frame_obj.fixed_right($('#google_verification .add'), '.google_verification_edit',function(){
			$('#google_verification_form .tips').text('');
		});
		frame_obj.submit_form_init($('#google_verification_form'));
		frame_obj.del_init($('#google_verification .item'));

		$('#google_verification_form').fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			acceptFileTypes: /^text\/html$/i,
			callback: function(filepath, count, name){
				$('#FilePath').val(filepath);
				$('#google_verification_form .tips').text(name+'.html');
			}
		});
		$('#google_verification_form').fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
	},
	//google验证文件 end
	
	//Paypal纠纷 start
    paypal_dispute_init:function(){
        //默认加载
        var Start = 1;
        plugins_obj.paypal_dispute_check(Start);
    },
    
    paypal_dispute_check:function(Start){
        if (Start == 1) {
            $('#dispute_check_info').html('<i></i><span>'+lang_obj.manage.app.paypal_dispute.loading+'</span>');
            $('#dispute_check_info').slideDown();
        }
        $.post('./?do_action=plugins.dispute_check_list', {'Start':Start}, function(result){
            if (result.ret == 1) {
                if (result.msg <= 50) {
                    plugins_obj.paypal_dispute_check(result.msg);
                    return false;
                }
            }
            $('#dispute_check_info').html(lang_obj.manage.app.paypal_dispute.updated);
            setTimeout(function(){ 
                $('#dispute_check_info').slideUp();
            }, 3000);
            return false;
        }, 'json');
    },
    
	paypal_dispute_view:function(){
		//进入仲裁阶段
		frame_obj.fixed_right($('.dispute_info .btn_claim'), '.fixed_claim');
		frame_obj.submit_form_init($('#claim_form'), '', '', '', function(result){
			if(result.ret==1){
				global_obj.win_alert_auto_close(result.msg, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
		});
		
		//提供证据
		frame_obj.fixed_right($('.dispute_info .btn_evidence'), '.fixed_evidence');
		frame_obj.submit_form_init($('#provide_evidence_form'), '', '', '', function(result){
			if(result.ret==1){
				global_obj.win_alert_auto_close(result.msg, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
		});
		
		//退款
		frame_obj.fixed_right($('.dispute_info .btn_refund'), '.fixed_refund');
		frame_obj.submit_form_init($('#refund_form'), '', '', '', function(result){
			if(result.ret==1){
				global_obj.win_alert_auto_close(result.msg, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
		});
		
		//发送消息给顾客
		frame_obj.submit_form_init($('#dispute_message_form'), '', '', '', function(result){
			if(result.ret==1){
				 //返回信息
				global_obj.win_alert_auto_close(result.msg.tips, '', 1000, '8%');
				$('#dispute_message_form .form_control').text('');
                //即时显示
                var $Html='';
                $Html+= '<div class="row clean">';
                $Html+=     '<div class="col_box col_left"><div class="avatar isMe">'+lang_obj.manage.global.you+'</div></div>';
                $Html+=     '<div class="col_box col_right">';
                $Html+=         '<div class="message_bubble">';
                $Html+=             '<p class="message_body">'+result.msg.content+'</p>';
                $Html+=             '<p class="message_footer"><span class="timestamp">'+result.msg.time+'</span></p>';
                $Html+=         '</div>';
                $Html+=     '</div>';
                $Html+= '</div>';
                $('#messages .message_list').prepend($Html);
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
		});
		
		var $Form=$('#provide_evidence_form');
		$Form.fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			acceptFileTypes: /^((image\/(gif|jpe?g|png))|(application\/pdf))$/i, //JPG, GIF, PNG, and PDF
			callback: function(filepath, count){
				$('#file_path').val(filepath);
			}
		});
		$Form.fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
	},
	//Paypal纠纷 end
	
	// DHL在线开户 start
	dhl_account_open_init:function(){
		/*************************** 全局 start ****************************/
		$('.cancel_register').click(function(){
			global_obj.win_alert(lang_obj.global.can_reg_confirm, function(){
				window.location.href = $('.cancel_register').attr('data-url');
			}, 'confirm');
		});

		$('.fixed_btn_submit').find('.btn_save').click(function(){
			if(global_obj.check_form('', $('#edit_form').find('*[format]'), 1)){ return false; };
			$(this).attr('disabled', true);
			$.post('?', $('#edit_form').serialize() + '&btn_save=1', function(data){
				if(data.ret==1){
					window.location.reload();
				}
			}, 'json');
		});

		if($('#upload_edit_form').length && $('#upload_edit_form input.btn_submit').length){ //提交按钮固定于底部
			var $mainWidth=($(window).width()>=980?$(window).width():980),
				$mainHeight=$(window).height()-$('#header').outerHeight(true);
			$('#upload_edit_form input.btn_submit').parents('.rows').addClass('fixed_btn_submit');
			$('#main .righter').append($('.fixed_btn_submit'));
			$('.fixed_btn_submit').css({width:$mainWidth-$('#main .menu').width(), left:$('#main .menu').width()});
			$('#main .r_con_wrap').css({height:$mainHeight-$('.fixed_btn_submit').outerHeight(true)-5});
		}
		/*************************** 全局 end ****************************/

		/*************************** 同注册地址 start ****************************/
		$('#edit_form .rows_title .input_checkbox_box').click(function(){
			var Step = $('#edit_form').find('input[name=Step]').val();
			if(!$(this).hasClass('checked')){
				$.post('./?do_action=plugins.dhl_same_address', {},function(data){
					var CompanyName = data.msg.CompanyName,
						CompanyAddress = data.msg.CompanyAddress,
						City = data.msg.City,
						Telephone = data.msg.Telephone,
						FaxNo = data.msg.FaxNo,
						ContactName = data.msg.ContactName,
						ContactEmail = data.msg.ContactEmail,
						ContactMobile = data.msg.ContactMobile;
					if(Step == 'Billing'){
						$('#edit_form').find('input[name=BillCompanyName]').val(CompanyName);
						$('#edit_form').find('input[name=BillCompanyAddress]').val(CompanyAddress);
						$('#edit_form').find('input[name=BillZipCode]').val(City);
						$('#edit_form').find('input[name=BillTelephone]').val(Telephone);
						$('#edit_form').find('input[name=BillFaxNo]').val(FaxNo);
						$('#edit_form').find('input[name=BillAuthorizedName]').val(ContactName);
						$('#edit_form').find('input[name=BillContactName]').val(ContactName);
						$('#edit_form').find('input[name=BillContactEmail]').val(ContactEmail);
						$('#edit_form').find('input[name=BillContactMobile]').val(ContactMobile);
					}else if(Step == 'Pickup'){
						$('#edit_form').find('input[name=PickupAddress]').val(CompanyAddress);
						$('#edit_form').find('input[name=PickupZipCode]').val(City);
						$('#edit_form').find('input[name=PickupTelephone]').val(Telephone);
						$('#edit_form').find('input[name=PickupFaxNo]').val(FaxNo);
						$('#edit_form').find('input[name=PickupAuthorizedName]').val(ContactName);
						$('#edit_form').find('input[name=PickupContactName]').val(ContactName);
						$('#edit_form').find('input[name=PickupContactEmail]').val(ContactEmail);
						$('#edit_form').find('input[name=PickupContactMobile]').val(ContactMobile);
					}
				}, 'json');
			}else{
				if(Step == 'Billing'){
					$('#edit_form').find('input[name=BillCompanyName]').val('');
					$('#edit_form').find('input[name=BillCompanyAddress]').val('');
					$('#edit_form').find('input[name=BillZipCode]').val('');
					$('#edit_form').find('input[name=BillTelephone]').val('');
					$('#edit_form').find('input[name=BillFaxNo]').val('');
					$('#edit_form').find('input[name=BillAuthorizedName]').val('');
					$('#edit_form').find('input[name=BillContactName]').val('');
					$('#edit_form').find('input[name=BillContactEmail]').val('');
					$('#edit_form').find('input[name=BillContactMobile]').val('');
				} else if(Step == 'Pickup'){
					$('#edit_form').find('input[name=PickupAddress]').val('');
					$('#edit_form').find('input[name=PickupZipCode]').val('');
					$('#edit_form').find('input[name=PickupTelephone]').val('');
					$('#edit_form').find('input[name=PickupFaxNo]').val('');
					$('#edit_form').find('input[name=PickupAuthorizedName]').val('');
					$('#edit_form').find('input[name=PickupContactName]').val('');
					$('#edit_form').find('input[name=PickupContactEmail]').val('');
					$('#edit_form').find('input[name=PickupContactMobile]').val('');
				}
			}
		});
		/*************************** 同注册地址 end ****************************/
		/*************************** 签名盖章 start ****************************/
		$('#upload_edit_form').find('.input_radio_box').click(function(){
			var HasCharged = parseInt($(this).find('input[name=HasCharged]').val());
			if(HasCharged){
				// 选择有带电货品，显示DG Indemnity Letter.pdf
				$('#upload_edit_form').find('.dg_link').removeClass('hide');
				$('#upload_edit_form').find('.dg_filepath_box').removeClass('hide');
			}else{
				// 选择无带电货品，隐藏DG Indemnity Letter.pdf
				$('#upload_edit_form').find('.dg_link').addClass('hide');
				$('#upload_edit_form').find('.dg_filepath_box').addClass('hide').find('input').val('');
			}
		});
		/*************************** 签名盖章 end ****************************/

		/*************************** 增值税类型 start ****************************/
		/**
		 * 根据增值税类型，显示隐藏对应的内容
		 * 值为0：如果您属于“增值税一般纳税人”，请务必完整填列下表全部信息，并提供贵公司一般纳税人认定通知书复印件。
		 * 值为1：如果您属于“非增值税一般纳税人”，请填列下表带*项信息。
		 * 值0：显示，值1：隐藏  vat_company_row
		 */
		$('#edit_form .taxpayer_type .input_radio_box').click(function(){
			var val = $(this).find('input[name=TaxpayerType]').val();
			if(val==0){
				$('#edit_form .vat_company_row').slideDown().find('input').attr('notnull', '');
				$('#edit_form .vat_invoice_type .only_taxpayer_type').parents('.input_radio_box').show();
			}else{
				$('#edit_form .vat_company_row').slideUp().find('input').removeAttr('notnull');
				$('#edit_form .vat_invoice_type .only_taxpayer_type').parents('.input_radio_box').hide();
			}
		});
		$('#edit_form .taxpayer_type .input_radio_box').each(function(index, elem){
			if($(elem).hasClass('checked')){
				var val = $(elem).find('input[name=TaxpayerType]').val();
				if(val==0){
					$('#edit_form .vat_company_row').slideDown().find('input').attr('notnull', '');
					$('#edit_form .vat_invoice_type .only_taxpayer_type').parents('.input_radio_box').show();
				}else{
					$('#edit_form .vat_company_row').slideUp().find('input').removeAttr('notnull');
					$('#edit_form .vat_invoice_type .only_taxpayer_type').parents('.input_radio_box').hide();
				}
			}
		});

		/*************************** 增值税类型 end ****************************/
		/*************************** 委托付款 start ****************************/
		/**
		 * 根据是否委托第三方显示隐藏对应内容
		 */
		$('#edit_form .is_payment_auth .input_radio_box').click(function(){
			var val = $(this).find('input[name=IsPaymentAuth]').val();
			if(val==0){
				$('#edit_form .payment_row').slideDown().find('input').attr('notnull', '');
			}else{
				$('#edit_form .payment_row').slideUp().find('input').removeAttr('notnull');
			}
		});
		$('#edit_form .is_payment_auth .input_radio_box').each(function(index, elem){
			if($(elem).hasClass('checked')){
				var val = $(this).find('input[name=IsPaymentAuth]').val();
				if(val==0){
					$('#edit_form .payment_row').slideDown().find('input').attr('notnull', '');
				}else{
					$('#edit_form .payment_row').slideUp().find('input').removeAttr('notnull');
				}
			}
		});
		/*************************** 委托付款 end ****************************/
		/*************************** 上传文件 start ****************************/
		var $Form=$('#upload_edit_form');
		$Form.fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			// acceptFileTypes: /^application\/(pdf|rar|zip|gzip|apk|ipa|doc|docx|x-zip-compressed|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)$/i, //csv xlsx xls
			acceptFileTypes: /^application\/(pdf)$/i, //csv xlsx xls
			callback: function(filepath, count, name){
				if($('input[name=KCFilePath]').length > 0 && $('input[name=KCFilePath]').val() == ''){
					$('input[name=KCFilePath]').val(filepath);
					$('input[name=KCFileName]').val(name);
					return false;
				}else if(!$('.dg_filepath_box').hasClass('hide') && $('input[name=DGFilePath]').val() == ''){
					$('input[name=DGFilePath]').val(filepath);
					$('input[name=DGFileName]').val(name);
					return false;
				}else{
					global_obj.win_alert(lang_obj.manage.app.dhl_account_open.filename_error);
				}
			}
		});
		$Form.fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
		frame_obj.submit_form_init($('#upload_edit_form'), '/manage/?m=plugins&a=dhl_account_open&d=step_4');
		/*************************** 上传文件 end ****************************/
		/*************************** 上传资料 start ****************************/
		$('#edit_form .picpath').each(function(index, elem){
			frame_obj.mouse_click($('#PicDetail_' + index + ' .upload_btn, #PicDetail_' + index + ' .pic_btn .edit'), 'pro', function($this){ //产品主图点击事件
				frame_obj.photo_choice_init('PicDetail_' + index, 'file_upload', 1);
			});
			if($('#PicDetail_' + index + ' input[name=PicPath_' + index + ']').length){
				var $obj=$('#PicDetail_' + index + ' input[name=PicPath_' + index + ']');
				if($obj.attr('save')==1){
					$obj.parent().append(frame_obj.upload_img_detail($obj.val())).children('.upload_btn').hide();
					$obj.parent().next().find('.del, .zoom').attr('href', $obj.val());
				}
			}
			$('#PicDetail_' + index + ' .pic_btn .del').on('click', function(){
				var $this=$(this);
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					$.ajax({
						url:'./?do_action=action.file_del&PicPath='+$this.attr('href'),
						success:function(){
							$('#PicDetail_' + index + ' .preview_pic').children('a').remove();
							$('#PicDetail_' + index + ' .preview_pic').children('.upload_btn').show();
							$('#PicDetail_' + index + ' .pic_btn .del, #PicDetail_' + index + ' .pic_btn .zoom').attr('href', '');
							$('#PicDetail_' + index + ' input[name=PicPath_' + index + ']').val('');
						}
					});
				}, 'confirm');
				return false;
			});
		});
		/*************************** 上传资料 end ****************************/

		frame_obj.submit_form_init($('#edit_form'), $('#edit_form').attr('data-url'), function(){
			var Step = $('#edit_form').find('input[name=Step]').val();
			if(Step == 'DeliveryServices'){
				global_obj.win_alert_auto_close(lang_obj.global.data_posting, 'loading', -1);
			}
		});

		/*************************** 上传水单截图 start ****************************/
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'pro', function($this){
			frame_obj.photo_choice_init('PicDetail', 'file_upload', 1);
		});
		if($('#PicDetail input[name=PicPath]').length){
			var $obj=$('#PicDetail input[name=PicPath]');
			if($obj.attr('save')==1){
				$obj.parent().append(frame_obj.upload_img_detail($obj.val())).children('.upload_btn').hide();
				$obj.parent().next().find('.del, .zoom').attr('href', $obj.val());
			}
		}
		$('#PicDetail .pic_btn .del').on('click', function(){
			var $this=$(this);
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.ajax({
					url:'./?do_action=action.file_del&PicPath='+$this.attr('href'),
					success:function(){
						$('#PicDetail .preview_pic').children('a').remove();
						$('#PicDetail .preview_pic').children('.upload_btn').show();
						$('#PicDetail .pic_btn .del, #PicDetail .pic_btn .zoom').attr('href', '');
						$('#PicDetail input[name=PicPath]').val('');
					}
				});
			}, 'confirm');
			return false;
		});
		/*************************** 上传水单截图 end ****************************/

	},
	// DHL在线开户 end

	// 自定义 start
	review_init:function(){
		frame_obj.del_init($('#custom_comments .r_con_table, .review_box')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'plugins.review_del_bat'); //批量删除


		/*批量修改产品销量和收藏数 start*/
		frame_obj.select_all($('#custom_comments .r_con_table input[name=select_all]'), $('#custom_comments .r_con_table input[name=select\\[\\]]'), $('.list_menu_button .save'));
		$('#custom_comments .r_con_table tbody .btn_checkbox').click(function(){
			var $this=$(this);
			if(!$this.find('input:checked').length){
				$this.parent().parent().find('input').removeClass('cur').attr('disabled', 'disabled');
			}else{
				$this.parent().parent().find('input').addClass('cur').removeAttr('disabled');
			}
		});
		$('#custom_comments .r_con_table thead .btn_checkbox').click(function(){
			var $this=$(this);
			if(!$this.find('input:checked').length){
				$this.parents('.r_con_table').find('input').removeClass('cur').attr('disabled', 'disabled');
			}else{
				$this.parents('.r_con_table').find('input').addClass('cur').removeAttr('disabled');
			}
		});
		$('#custom_comments .bat_close').click(function(){
			if($('input[name=select\\[\\]]:checked').length){
				$(this).hide().parent().parent().find('.save').css('display', 'block');
			}else{
				global_obj.win_alert(lang_obj.global.dat_select, '', 'confirm');
			}
		});

		$('#custom_comments .save').click(function(){
			if($('input[name=select\\[\\]]:checked').length){
				$('#list_bat_edit .btn_submit').click();
			}else{
				global_obj.win_alert(lang_obj.global.dat_select, '', 'confirm');
			}
		});

		frame_obj.submit_form_init($('#list_bat_edit'), location.href);
		/*批量修改产品销量和收藏数 end*/

		$('.review_box .switchery').click(function(){
			var $this=$(this),
				$rid=$this.attr('data-id'),
				$review=0,
				$audit;
			if($this.hasClass('checked')){
				$audit=0;
				$this.removeClass('checked');
			}else{
				$audit=1;
				$this.addClass('checked');
			}
			if($this.hasClass('review')) $review=1;
			$.post('?do_action=plugins.review_reply_audit', {'RId':$rid,'Audit':$audit,'Review':$review},function(){}, 'json');
		});

		$('.review_edit_box input[name=AccTime]').daterangepicker({singleDatePicker:true});
		frame_obj.fixed_right($('#custom_comments .review_add_btn'), '.review_edit_box', function($this){
			$('.review_edit_box input[name=ProId]').val($this.data('proid'));
		});
		frame_obj.submit_form_init($('#review_reply_form'), location.href);
		frame_obj.submit_form_init($('#review_edit_form'), location.href);

		frame_obj.switchery_checkbox(function(obj){ //开启
			$('.virtual_box').show();
		}, function(obj){ //关闭
			$('.virtual_box').hide();
		}, '#products_development_form .switchery');


		frame_obj.fixed_right($('#custom_comments .development_btn'), '.products_development', function($this){
			$('#fixed_right').addClass('loading');
			$('#products_development_form').hide();
			$('.products_development input[name=ProId]').val($this.data('proid'));
			$.post('?do_action=plugins.products_development', {'select_data':'1', 'ProId':$this.data('proid')},function(data){
				$('#fixed_right').removeClass('loading');
				$('#products_development_form').show();
				if (data.msg.IsVirtual == 1) {
					$('#products_development_form .switchery').addClass('checked').find('input').attr('checked', true);
					$('.virtual_box').show();
				}else{
					$('#products_development_form .switchery').removeClass('checked').find('input').attr('checked', false);
					$('.virtual_box').hide();
				}
				$('input[name=Sales]').val(data.msg.Sales);
				$('input[name=FavoriteCount]').val(data.msg.FavoriteCount);
				$('input[name=DefaultReviewTotalRating]').val(data.msg.DefaultReviewTotalRating);
				$('input[name=DefaultReviewRating]').val(parseInt(data.msg.DefaultReviewRating));
				$('.products_development .review_star span').attr('class', 'star_0');
				$('.products_development .review_star span:lt('+parseInt(data.msg.DefaultReviewRating)+')').attr('class', 'star_1');
			}, 'json');
		});
		frame_obj.submit_form_init($('#products_development_form'), location.href);

		//star
		$('.review_star span').hover(function(){
			var ind=parseInt($(this).index());			
			$(this).parent().find('span:gt('+ind+')').removeClass('star_1').addClass('star_0');
			$(this).parent().find('span:lt('+(ind+1)+')').removeClass('star_0').addClass('star_1');
		},function(){
			var star=parseInt($(this).parents('form').find('input[name=Rating]').val());
			if($(this).parents('form').find('input[name=DefaultReviewRating]').length){
				star=parseInt($(this).parents('form').find('input[name=DefaultReviewRating]').val());
			}
			if(isNaN(star)) star=0;
			$(this).parent().find('span').removeClass('star_1').addClass('star_0');
			$(this).parent().find('span:lt('+star+')').removeClass('star_0').addClass('star_1');
		}).click(function(){
			var ind=parseInt($(this).index());			
			$(this).parents('form').find('input[name=Rating], input[name=DefaultReviewRating]').val((ind+1));
		});
	},

	review_import_init:function(){
		var $Form=$('#review_import_form');
		$Form.fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
			callback: function(filepath, count){
				$('#excel_path').val(filepath);
				$('#file_path').text('文件已上传');
			}
		});
		$Form.fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
		frame_obj.submit_form_init($Form, '', function(){
			$('.explode_box').hide();
			$('#progress_container').show();
			return true;
		}, '', function(data){
			if(data.ret==2){
				//继续产品资料上传
				var $Html='', $Number=0;
				if($('#progress_container tbody tr').size()){
					$Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
				}
				if(data.msg[1]){
					for(k in data.msg[1]){
						$Html+='<tr class="'+(data.msg[1][k].Status==0?'error':'success')+'">\
									<td nowrap="nowrap">'+($Number+parseInt(k)+1)+'</td>\
									<td nowrap="nowrap"><div class="name" title="'+data.msg[1][k].Number+'">'+data.msg[1][k].Number+'</div></td>\
									<td nowrap="nowrap">'+data.msg[1][k].Content+'</td>\
									<td nowrap="nowrap" data-type="message">'+data.msg[1][k].Tips+'</td>\
								</tr>';
					}
					$('#progress_container tbody').append($Html);
					$Form.find('input[name=Number]').val(data.msg[0]);
					$Form.find('.btn_submit').removeAttr('disabled').click();
				}
			}else if(data.ret==1){
				$('#progress_loading').addClass('completed').html(data.msg).append('<a href="'+location.href+'" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
				$('.explode_box').show();
				$('#progress_container').hide();
			}
		});
	},
	// 自定义 end
	
}