/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
var view_obj={
	visual_init:function(){
		var loading='<div class="fixed_loading"><div class="load"><div><div class="load_image">'+(ueeshop_config.FunVersion>=10?'创店':'UEESHOP')+'</div><div class="load_loader"></div></div></div></div>';
		var lang=$('#form_visual .tab_box_btn:first').data('lang');  //语言
		var data_init=function(loc, iframe){  //初始化函数
			var iframe=iframe?iframe:($('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents());
			var item_box=$('#form_visual .loc_box[plugins='+loc+'] .tab_txt_'+lang+' .item_box');
			item_box.each(function(pos){
				if($(this).hasClass('item_box_banner')){  //Banner模块
					if(!$(this).hasClass('item_box_banner_current')) return;  //踢掉不是当前编辑中的板块
					//图片
					var obj=iframe.find('#banner_edit');
					var obj_width=obj.width();
					var banner_obj=iframe.find('#banner_edit .banner_list:eq('+pos+')');
					var url=$(this).find('.slide_img .preview_pic input[type=hidden]').val();
					var rate=obj.width()/obj.data('width');
					var banner_style={};
					if(obj.data('type')!=2){
						banner_style['left']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][Banner][left]']").val();  //同步定位左
						banner_style['top']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][Banner][top]']").val();  //同步定位上
					}
					var image=new Image();  //同步图片
					image.src=url;
					image.onload=function(){
						if(obj.data('type')==2){
							obj.find('.banner_edit_mask_position').css({'width':obj.data('width'), 'height':image.height, 'left':'50%', 'margin-left':-obj.data('width')/2});
						}else if(obj.data('type')==1){
							obj.find('.banner_edit_mask_position').css({'width':image.width>obj_width?obj_width:image.width*rate, 'height':image.width>obj_width?obj_width*image.height/image.width:image.height*rate});
						}else{
							height=parseInt(obj_width*image.height/image.width);
							if(image.width<obj.width()) height=image.height;  //图片小于容器宽度
							height=height+1;  //兼容像素小数点导致左右有白边
							obj.height(height);
							obj.parents('.plugins_edit_contents').parent().parent().height(height);
							obj.parents('.plugins_edit_contents').height(height);
							obj.find('.banner_edit_mask_position').css({width:obj_width, height:height});
						}
					}
					obj.find('.banner_edit_mask_position').css(banner_style);
					obj.find('.banner_edit_mask_position img').attr('src', url);
					if(url){  //有图片
						obj.find('#banner_edit_mask .banner_edit_mask_noimg').hide();
					}else{  //没图片
						obj.find('#banner_edit_mask .banner_edit_mask_noimg').show();
					}
					//标题
					var title_style={};
					title_style['font-family']=$(this).find('.slide_title .plugins_select').val();  //同步字体
					title_style['color']=$(this).find('.slide_title .font_style .color_style').val();  //同步颜色
					title_style['font-size']=$(this).find('.slide_title .font_size input').val();  //同步字号
					title_style['left']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][Title][left]']").val();  //同步定位左
					title_style['top']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][Title][top]']").val();  //同步定位上
					$(this).find('.slide_title .font_style i').each(function(){
						if($(this).hasClass('underline')){    //下划线
							var style='text-decoration';
						}else if($(this).hasClass('italic')){    //斜体
							var style='font-style';
						}else{    //加粗
							var style='font-weight';
						}
						title_style[style]=$(this).children('input').val();  //同步字体样式
					});
					//副标题
					var subtitle_style={};
					subtitle_style['font-family']=$(this).find('.slide_subtitle .plugins_select').val();  //同步字体
					subtitle_style['color']=$(this).find('.slide_subtitle .font_style .color_style').val();  //同步颜色
					subtitle_style['font-size']=$(this).find('.slide_subtitle .font_size input').val();  //同步字号
					subtitle_style['left']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][SubTitle][left]']").val();  //同步定位左
					subtitle_style['top']=$(this).find("input[name='banner[0]["+lang+"]["+pos+"][SubTitle][top]']").val();  //同步定位上
					$(this).find('.slide_subtitle .font_style i').each(function(){
						if($(this).hasClass('underline')){    //下划线
							var style='text-decoration';
						}else if($(this).hasClass('italic')){    //斜体
							var style='font-style';
						}else{    //加粗
							var style='font-weight';
						}
						subtitle_style[style]=$(this).children('input').val();  //同步字体样式
					});
					if(rate!=1 && obj.data('type')!=2){  //配置比例
						title_style['font-size']=parseInt(title_style['font-size'])*rate+'px';
						subtitle_style['font-size']=parseInt(subtitle_style['font-size'])*rate+'px';
					}
					//配置赋值
					obj.find('.banner_edit_mask_title').html($(this).find('.slide_title .plugins_textarea').val());  //同步内容
					obj.find('.banner_edit_mask_title').css(title_style);
					obj.find('.banner_edit_mask_brief').html($(this).find('.slide_subtitle .plugins_textarea').val());  //同步内容
					obj.find('.banner_edit_mask_brief').css(subtitle_style);
				}else if(loc.split('-')[0]=='mbanner'){  //手机版Banner模块
					var obj=iframe.find('#mbanner_edit_mask');
					var url=$(this).find('.preview_pic input[type=hidden]').val();
					if(!$(this).parent().find('.item_box.current').size()){  //初始化每张图
						obj.height(100).find('.mbanner_edit_list').eq(pos).height(100).children().remove();
						obj.parents('.plugins_edit_contents').parent().height(obj.height());
						obj.parents('.plugins_edit_contents').height(obj.height());
						var image=new Image();  //同步图片
						image.src=url;
						image.onload=function(){
							var box_height=obj.width()*image.height/image.width;
							obj.find('.mbanner_edit_list').eq(pos).height(box_height).html('<img src="'+url+'" />');
							if(!pos){
								obj.height(box_height);	
								obj.parents('.plugins_edit_contents').parent().height(box_height);
								obj.parents('.plugins_edit_contents').height(box_height);
							}
						}
					}else{  //初始化当前
						if(!$(this).hasClass('item_box_mbanner_current')) return;
						obj.find('.mbanner_edit_list').eq(pos).fadeIn().siblings().fadeOut();
						if(url){ //有图片
							var image=new Image();  //同步图片
							image.src=url;
							image.onload=function(){
								var box_height=obj.width()*image.height/image.width
								obj.find('.mbanner_edit_list').eq(pos).height(box_height).html('<img src="'+url+'" />');
								obj.height(box_height);	
								obj.parents('.plugins_edit_contents').parent().height(box_height);
								obj.parents('.plugins_edit_contents').height(box_height);
							}
						}else{
							obj.height(100).find('.mbanner_edit_list').eq(pos).height(100).children().remove();
							obj.parents('.plugins_edit_contents').parent().height(obj.height());
							obj.parents('.plugins_edit_contents').height(obj.height());
						}
					}
				}else{  //普通模块
					$(this).find('*[class*=plugins_mod_]').each(function(){
						var mod=$(this).attr('class').split('_')[2];
						var obj=iframe.find('*[plugins='+loc+']');
						var layout=obj.attr('effect').split('-')[0];
						obj=obj.attr('plugins_pos')?obj:(obj.find('*[plugins_pos='+pos+']').size()?obj.find('*[plugins_pos='+pos+']'):0);
						if(!obj) return;  //找不到元素不处理
						obj=obj.attr('plugins_mod')==mod?obj:(obj.find('*[plugins_mod='+mod+']').size()?obj.find('*[plugins_mod='+mod+']'):0);
						if(!obj) return;  //找不到元素不处理
						var value,ext,PicSize;
						if(obj.children('.plugins_pos').size()){  //色块元素和修改元素在同一层
							ext=obj.children('.plugins_pos').clone();
						}
						if(mod=='Title' || mod=='SubTitle'){  //标题,副标题
							value=$(this).find('input').val();
							if(typeof(obj.attr('placeholder'))!='undefined'){  //搜索提示语
								obj.attr('placeholder', value);
							}else{
								obj.text(value).append(ext);
							}
						}else if(mod=='Content'){  //内容
							value=$(this).find('textarea').val();
							obj.text(value).append(ext);
						}else if(mod=='Link'){  //链接
							obj.attr('href', 'javascript:;');  //链接全部设置不能跳转,防止跳走
						}else if(mod=='Pic'){  //图片
							value=$(this).find('.preview_pic input[type=hidden]').val();
							//if(value && obj.attr('src')==value) return;  //没有改变图片跳过
							if(obj[0].tagName=='IMG'){  //前台是图片
								if(obj.attr('src')!=value) obj.attr('src', value);
								var PicSize=$(this).find('.pic_size input[type=hidden]').val().split('*');
								var background='none';
								var image_width=PicSize[0];
								var image_height=box_height=PicSize[1]==0?100:PicSize[1];  //没有高度默认一个高度
								if(layout>0){  //图片是有布局切换的,先清空父元素style再赋值
									obj.parent().removeAttr('style');
								}
								if(typeof(obj.parent().attr("href"))!='undefined' && obj.parent().css('display')!='inline-block') obj.parent().css('display', 'block');  //父元素是a元素而且没有设置block,block了它
								var box_width=obj.parent().width();
								if(image_width>box_width && PicSize[1]!=0){  //图片建议尺寸大于容器,等比例缩小
									box_height=box_width*image_height/image_width;
								}
								if(value){  //有图片
									var image=new Image();
									image.src=value;
									image.onload=function(){
										box_height=PicSize[1]==0?100:PicSize[1];  //没有高度默认一个高度
										if(PicSize[1]==0){  //高度自适应
											box_height=image.height;
											if(image.width>box_width){  //图片尺寸大于容器,等比例缩小
												box_height=box_width*image.height/image.width;
											}
											obj.parent().css({'width':box_width, 'height':box_height});
											//obj.parents('.plugins_edit_contents').parent().height(box_height);
											//obj.parents('.plugins_edit_contents').height(box_height);
											obj.parents('.plugins_edit_contents').height('auto');
											obj.parents('.plugins_edit_contents').parent().height(obj.parents('.plugins_edit_contents').height());
										}
										obj.show();
									}
								}else{  //没图片
									obj.hide();
									if(ueeshop_config.FunVersion>=10){
										background='url(/static/manage/images/view/cdx/icon_noimg.png)';
									}else{
										background='url(/static/manage/images/view/icon_noimg.png)';
									}
								}
								obj.parent().css({'width':box_width, 'height':box_height, 'background-image':background});
								if(obj.parents('.plugins_edit_contents').height()<box_height || PicSize[1]==0){  //容器不够图片高,或高度自适应的,适应会图片
									//obj.parents('.plugins_edit_contents').parent().height(box_height);
									//obj.parents('.plugins_edit_contents').height(box_height);
									obj.parents('.plugins_edit_contents').height('auto');
									obj.parents('.plugins_edit_contents').parent().height(obj.parents('.plugins_edit_contents').height());
								}
							}else{  //前台是图片是背景
								if(ueeshop_config.FunVersion>=10){
									obj.css('background-image', 'url("'+(value?value:'/static/manage/images/view/cdx/icon_noimg.png')+'")');
								}else{
									obj.css('background-image', 'url("'+(value?value:'/static/manage/images/view/icon_noimg.png')+'")');
								}
							}
						}
					});
				}
			});
		}
		var iframe_init=function(iframe){  //加载模板完成初始化函数
			iframe.find('*[plugins]').each(function(index){
				var plugins_name=$(this).attr('plugins').split('-')[0];
				if($(this)[0].tagName=='BODY'){  //插件在body
					$(this).addClass('plugins_edit_contents');
					$(this).css('position', 'static');
				}else{  //正常
					$(this).css({'width':$(this).width(), 'height':$(this).height(), 'position':'relative'});
					$(this).wrapInner('<div class="plugins_edit_contents"></div>');
					$(this).find('.plugins_edit_contents').css({'width':$(this).width(), 'height':$(this).height(), 'padding':$(this).css('padding'), 'overflow':'hidden'});  //padding需要子元素撑开
					var plugins_pos_obj=$(this).attr('plugins_pos')?$(this).find('.plugins_edit_contents'):$(this).find('*[plugins_pos]'); //色块元素在插件板块上?
					plugins_pos_obj.append('<div class="plugins_pos"></div>');
					$(this).find('*[plugins_pos]').each(function(){ //定位色块元素
						if($(this).css('position')!='absolute') $(this).css('position', 'relative');
					});
					if(plugins_name=='banner'){  //Banner图模块
						//初始化banner
						var html='<div id="banner_edit_mask">';
								html+='<div class="banner_edit_mask_list">';
									html+='<div class="banner_edit_mask_position">';
										html+='<img src="" />';
										html+='<div class="banner_edit_mask_title"></div>';
										html+='<div class="banner_edit_mask_brief"></div>';
									html+='</div>';
								html+='</div>';
								html+='<div class="banner_edit_mask_noimg">';
									html+='<div class="banner_edit_mask_noimgtxt">'+lang_obj.manage.photo.no_picture+'</div>';
								html+='</div>';
							html+='</div>';
						iframe.find('#banner_edit').append(html);
						/*第一次进入后台有引导提示*/
						if($('input:hidden[name=first_enter_manage]').length){
							if($('input:hidden[name=first_change_themes]').length){
								$('.switch_style_btn').click(); //第一次默认打开切换风格
								$.get('?do_action=account.clear_first_enter_manage_session');
							}
							var html='<div id="global_guide_box"><span>'+lang_obj.manage.view.guide+'</span><em></em></div>';
							iframe.find('#banner_edit').parents('.plugins_edit_contents').parent().append(html);						
							iframe.find('#global_guide_box>em').hover(function(){							
								$(this).parent().fadeOut(1000, function(){
									$.get('?do_action=account.clear_first_enter_manage_session&type=guide'); //删除第一次进入后台的session
									iframe.find('#global_guide_box').remove();
								});
							});
						}

						document.getElementById('plugins_iframe_themes').contentWindow.document.getElementById('banner_edit_mask').onselectstart=function(){return false;};  //清除Banner图内可以选中状态
						iframe.find('#banner_edit img').on('mousedown',function(e){e.preventDefault()});  //清除Banner图可拖动
						if($(this).find('#banner_edit').data('type')==1) $(this).find('#banner_edit #banner_edit_mask .banner_edit_mask_position').addClass('move');  //添加拖动效果
						var move_obj=$(this).find('#banner_edit #banner_edit_mask');
						move_obj.find('img').on('mousedown', function(e){e.preventDefault()});  //禁止图片拖动
						$(this).on('mousedown', '.current_edit .banner_edit_mask_position, .current_edit .banner_edit_mask_title, .current_edit .banner_edit_mask_brief', function(ev){	//拖动
							ev.stopPropagation();
							var obj=$(this);
							var module='Banner';
							move_obj.find('.editing').removeClass('editing');
							if(obj.hasClass('banner_edit_mask_title')){
								module='Title';
								//着色右侧工具栏
								$('#plugins_visual .tool_bar .item_box_banner_current  .slide_title .title').trigger('click');
								move_obj.find('.banner_edit_mask_title').addClass('editing');
							}else if(obj.hasClass('banner_edit_mask_brief')){
								module='SubTitle';
								//着色右侧工具栏
								$('#plugins_visual .tool_bar .item_box_banner_current  .slide_subtitle .title').trigger('click');
								move_obj.find('.banner_edit_mask_brief').addClass('editing');
							}else{
								//着色右侧工具栏
								$('#plugins_visual .tool_bar .item_box_banner_current  .slide_img .title').trigger('click');
							}
							if(obj.hasClass('banner_edit_mask_position') && !obj.hasClass('move')) return;  //不是可拖动,踢走
							var pos_obj=$("#form_visual .loc_box[plugins=banner-0] .tab_txt_"+lang+" .item_box.item_box_banner_current");
							var pX = $(this).offset().left;  //当前元素X坐标
							var pY = $(this).offset().top;  //当前Y坐标
							var pW = $(this).width();  //当前元素宽度
							var pH = $(this).height();  //当前元素高度
							var ppX = $(this).parent().offset().left;  //父元素X坐标
							var ppY = $(this).parent().offset().top;  //父元素Y坐标
							var ppW = $(this).parent().width();  //父元素宽度
							var ppH = $(this).parent().height();  //父元素高度
							iframe.find('html').on('mousemove', function(e){
								var move_left = pX - ppX + (e.clientX - ev.clientX);  //当前元素与父元素X偏移值
								var move_top = pY - ppY + (e.clientY - ev.clientY);  //当前元素与父元素Y偏移值
								//边界值判断
								if(obj.hasClass('banner_edit_mask_position') && pH>ppH){  //Banner图片高度大于容器
									if(move_left<0) move_left=0;
									if(move_top>0) move_top=0;
									if(move_left>ppW-pW) move_left=ppW-pW;
									if(move_top<ppH-pH)move_top=ppH-pH;
								}else{  //其他普通模式
									if(move_left<0) move_left=0;
									if(move_top<0) move_top=0;
									if(move_left>ppW-pW) move_left=ppW-pW;
									if(move_top>ppH-pH) move_top=ppH-pH;
								}
								var left = (move_left/ppW*100).toFixed(2)+'%';
								var top = (move_top/ppH*100).toFixed(2)+'%';

								pos_obj.find("input[name='banner[0]["+lang+"]["+pos_obj.index()+"]["+module+"][left]']").val(left);
								pos_obj.find("input[name='banner[0]["+lang+"]["+pos_obj.index()+"]["+module+"][top]']").val(top);
								data_init('banner-0', iframe);
							});
							iframe.find('html').on('mouseup', function(){
								$(this).off('mousemove');
								$(this).off('mouseup');
							});
						});
					}else if(plugins_name=='mbanner'){  //手机版Banner图模块
						//初始化Banner
						var html='<div id="mbanner_edit_mask">';
								for(var i=0;i<5;i++){
									html+='<div class="mbanner_edit_list"></div>';
								}
							html+='</div>';
						$(this).children('.plugins_edit_contents').children().remove();  //清空内容
						$(this).children('.plugins_edit_contents').html(html);
					}
					data_init($(this).attr('plugins'), iframe);
				}
				var effect=$(this).attr('effect').split('-');  //功能区域
				var module='<div class="plugin_edit_box">';
					if(plugins_name=='banner') module+='<div class="plugins_eidt_item pluginseffect">'+lang_obj.manage.view.effect+'</div>';
					if(effect[0]>0) module+='<div class="plugins_eidt_item pluginslayout">'+lang_obj.manage.view.layout+'</div>';
					if(effect[1]==1) module+='<div class="plugins_eidt_item pluginseditor">'+lang_obj.global.edit+'</div>';
				module+='</div>';
				$(this).append(module);
				var plugins_item=$(this).find('.plugin_edit_box');
				if($(this)[0].tagName=='BODY'){  //插件在body
					$(this).children('.plugin_edit_box').addClass('current');
				}else{
					if(($(this).width()<plugins_item.width()+20 || $(this).height()<plugins_item.height()+20)){  //宽度不够或者高度不够,按钮以其他定位显示,插件在body不处理
						plugins_item.css({'top':$(this).outerHeight()+2, 'right':'-2px', 'bottom':'auto', 'left':'auto'});
					}else if(plugins_name=='banner'){
						plugins_item.css({'top':'50%', 'left':'50%', 'margin-left':'-94px', 'margin-top':'-20px'});  //Banner按钮在中部显示
					}
				}
			});
			iframe.find('html').css('overflow-x', 'hidden');  //隐藏左右滚动条
			if(ueeshop_config.FunVersion>=10){
				iframe.find('a').on('click', function(){
					global_obj.win_alert(lang_obj.manage.view.no_edit_tips);	
				});
			}
			iframe.find('html').on('click', function(event){  //右侧工具栏隐藏
				if(!$(event.target)[0].offsetParent || $(event.target)[0].offsetParent.className.indexOf('plugin_edit_box')==-1 && $(event.target)[0].offsetParent.className.indexOf('banner_edit_mask_position')==-1){  //点击页面任何位置收起来，不包括编辑按钮和banner图里面的元素
					$('#plugins_visual .tool_bar .close').trigger('click');
				}
			});
			iframe.find('*[plugins]').on('mouseenter', function(){  //按钮显示
				if($(this).hasClass('plugins_edit_contents'))  return;  //插件在body不处理
				if($(this).find('.current_edit').size()) return;  //当前正在编辑中
				$(this).find('.plugins_edit_contents, .plugin_edit_box').addClass('current');
			});
			iframe.find('*[plugins]').on('mouseleave', function(){  //按钮隐藏
				if($(this).hasClass('plugins_edit_contents'))  return;  //插件在body不处理
				if($(this).find('.plugins_edit_contents').hasClass('current_edit')) return;  //设置当前编辑不消失
				$(this).find('.plugins_edit_contents, .plugin_edit_box').removeClass('current');
			});
			iframe.find('.plugin_edit_box .pluginseditor').on('click', function(){  //编辑按钮操作
				plugins_button_click($(this) , 'pluginseditor');  //按钮点击函数
				var loc=$(this).parent().parent().attr('plugins');
				$('#form_visual .loc_box[plugins='+loc+']').show().siblings().hide();
				$('#plugins_visual .tool_bar .set_item').jScrollPane({'scrollbackgroundcolor':'rgba(0,0,0,0.1)','bottombackgroundcolor':'none','scrolltop':true});
				$('#plugins_visual .tool_bar').removeClass('current_banner current');
				if(loc.split('-')[0]=='banner' || loc.split('-')[0]=='mbanner'){  //Banner图类型
					$('#plugins_visual .tool_bar').addClass('current_banner');
					set_nav_bar(1);
					if(!$('#plugins_visual .tool_bar .nav_bar .item.pic:first').hasClass('current')){  //非第一个选中情况下,默认第一个点击
						$('#plugins_visual .tool_bar .nav_bar .item.pic:first').trigger('click');
					}
				}else{
					$('#plugins_visual .tool_bar').addClass('current');
				}
			});
			iframe.find('.plugin_edit_box .pluginseffect').on('click', function(){  //效果按钮操作
				plugins_button_click($(this) , 'pluginseffect');  //按钮点击函数
				var loc=$(this).parent().parent().attr('plugins');
				var slidetype=$("#form_visual input[name='Layout["+loc+"]']").val();
				slidetype = slidetype < 2 ? ++slidetype : 0;
				if(slidetype==2){
					iframe.find('#banner_edit .banner_edit_mask_list').animate({'left':'-100%'}, function(){$(this).css('left', '100%').animate({'left':0});});
				}else if(slidetype==1){
					iframe.find('#banner_edit .banner_edit_mask_list').animate({'top':'-100%'}, function(){$(this).css('top', '100%').animate({'top':0});});
				}else{
					iframe.find('#banner_edit .banner_edit_mask_list').fadeOut(function(){$(this).fadeIn();});
				}
				$("#form_visual input[name='Layout["+loc+"]']").val(slidetype);
			});
			iframe.find('.plugin_edit_box .pluginslayout').on('click', function(){  //布局按钮操作
				plugins_button_click($(this) , 'pluginslayout');  //按钮点击函数
				var loc_obj=$(this).parent().parent();
				var loc=loc_obj.attr('plugins');
				var style_obj=$(this).parent().parent().find('*[class*=style]');  //查找可切换布局元素
				var style_count=$(this).parent().parent().attr('effect').split('-')[0];  //可切换布局的数量
				var style_current_class=style_obj[0].className.substr(-1);  //当前选择的布局
				style_current_class = style_current_class < style_count-1 ? ++style_current_class : 0;
				style_obj.removeClass().addClass('style'+style_current_class);
				$("#form_visual input[name='Layout["+loc+"]']").val(style_current_class);
				style_obj.css({'position':'relative', 'overflow':'hidden'});
				style_obj.append('<div class="plugins_layout_mask"></div>');
				var absolute_obj=$(this).parent().parent().find('*[style*=left]');  //查找滚动子元素
				if(absolute_obj.size()) absolute_obj.css('left', 0);  //滚动元素已经滚动过,重置为0
				data_init(loc);
				loc_obj.find('.plugins_edit_contents').height('auto');  //先清除高度,布局高度变化后再设置高度
				var height=loc_obj.find('.plugins_edit_contents').height();
				loc_obj.children('.plugins_edit_contents').height(height);
				loc_obj.height(height);
				style_obj.children('.plugins_layout_mask').fadeOut('slow', function(){$(this).remove();});
			});
		}
		var set_nav_bar=function(type){  //设置右侧工具栏导航  type:0添加 1普通
			var nav=$('#plugins_visual .tool_bar .nav_bar');
			if(nav.find('.pic').size()==1){  //初始化导航
				var html=nav.find('.pic').prop('outerHTML');
				var count=$('#form_visual .tab_txt_'+lang+' .item_box_banner').size()-1;
				for(var i=0;i<count;i++){
					nav.prepend(html);
				}
				nav.find('.pic').each(function(index){
					$(this).find('i').text(index+1);
				});
			}
			if(type==1){
				nav.find('.pic').show();
				$('#plugins_visual .set_item .loc_box[style*=block] .tab_txt[style*=block] .item_box').each(function(){
					if(!$(this).find('.slide_img .preview_pic input[type=hidden]').val() && $(this).index()){
						nav.find('.pic').eq($(this).index()).hide();
					}
				});
				//if(!nav.find('.pic.current').size()) nav.find('.pic:first').click();  //点击影响手机版编辑按钮,会出现闪现
				if(!nav.find('.pic.current').size()) nav.find('.pic:first').addClass('current');
			}
			if(nav.find('.pic:hidden').size()){
				nav.find('.add').show();
			}else{
				nav.find('.add').hide();
			}
		}
		var plugins_button_click=function(obj, type){  //按钮点击函数    type:pluginseditor  pluginslayout  pluginseffect
			//先收回工具栏
			//$('#plugins_visual .tool_bar .close').trigger('click');
			//设置当前编辑模式
			obj.parent().parent().find('.plugins_edit_contents').addClass('current_edit');
			//取消页面其他编辑模式
			obj.parents('body').find('.current_edit').each(function(){
				if(obj.parent().parent().attr('plugins')==$(this).parent().attr('plugins')) return;  //跳过当前自己	
				$(this).removeClass('current_edit current').parent().find('.plugin_edit_box').removeClass('current');
			});
			//编辑按钮,隐藏按钮卡
			if(type=='pluginseditor') obj.parent().removeClass('current');
			//banner效果页面
			if(type=='pluginseffect'){
				obj.parent().parent().find('.plugins_edit_contents').removeClass('current_edit');
			}
			$.get('?do_action=view.plugins_visual_statistics'); //统计可视化的编辑次数
		}
		/*
		$('#plugins_visual .top_bar .go_client a').on('click',function(){    //切换多端
			var client=$(this).data('client');
			$(this).addClass('cur').siblings().removeClass('cur');
			$('#plugins_visual .main_bar iframe[data-client='+client+']').css({'visibility':'visible', 'opacity':'1'}).siblings().css({'visibility':'hidden', 'opacity':'0'});
		});
		*/
		$('#plugins_visual .top_bar .go_save').on('click',function(){    //编辑风格保存
			$('#plugins_visual').append(loading);
			$.post('./', $('#form_visual').serialize(), function(data){
				/*if(data.ret==1) setTimeout(function(){$('#plugins_visual .fixed_loading').fadeOut(function(){$(this).remove();});},500);*/
				if(data.ret==1) window.location.reload();
			}, 'json');
		});
		$('#plugins_visual .top_bar .go_preview').on('mouseleave', function(){  //预览按钮触发二维码消失
			$(this).find('.qrcode').remove();
		});
		$('#plugins_visual .top_bar .go_preview').on('click',function(){    //预览按钮
			//判断多端
			var client=$('#plugins_visual .top_bar .go_client a.cur').data('client');
			//提交保存
			$.post('./', $('#form_visual').serialize(), function(data){
				if(data.ret==1){
					if(client=='website'){  //PC端
						$('#header .menu .menu_home a')[0].click();
					}else{  //移动端
						$('#plugins_visual .top_bar .go_preview').append('<div class="qrcode"><img src="'+data.msg+'" /></div>');
					}
				}
			}, 'json');
		});
		//加载模板地址(不能直接在地址写,因为有些不兼容-子若写之)
		$('#plugins_iframe_themes').attr('src', '/?client=website');
		$('#plugins_iframe_mobile').attr('src', '/?client=mobile');
		//加载完成500ms后才执行加载程序,防止读取的是小屏的样式
		$('#plugins_iframe_mobile').load(function(){  //手机版前台模板加载完成
			if($('#plugins_visual .main_bar').height()>$('#plugins_iframe_mobile').height()) $('#plugins_iframe_mobile').css({'top':'50%', 'margin-top':'-368px'});
			$('#plugins_iframe_mobile').contents().find('.wrapper').css({'padding':'2px', 'box-sizing':'border-box'});
			setTimeout(function(){iframe_init($('#plugins_iframe_mobile').contents());}, 200);
		});
		$('#plugins_iframe_themes').load(function(){    //PC前台模板加载完成
			setTimeout(function(){iframe_init($('#plugins_iframe_themes').contents());}, 200);
		});
		/*
		$('#form_visual .loc_box[plugins=banner-0] .item_box_banner .multi_img input[type=hidden]').each(function(){  //初始化Banner默认图片
			if($(this).val() && $(this).attr('save')==0) $(this).parent().append(frame_obj.upload_img_detail($(this).val())).children('.upload_btn').hide().parent().parent().addClass('isfile');
		});
		*/
		frame_obj.mouse_click($('#plugins_visual .tool_bar .set_item').find('.multi_img .upload_btn, .pic_btn .edit'), 'visual', function($this){	//图片上传
			var id=$this.parents('.image').data('id');
			frame_obj.photo_choice_init('PicDetail_'+id, 'visual', 1, '', 1, '');
		});
		$('#form_visual .multi_img input[type=hidden]').on('change', function(){  //图片改变
			var pos_obj=$(this).parents('.item_box_banner');
			if(pos_obj.size()){  //Banner图上传图片,初始化定位
				pos_obj.find("input[name='banner[0]["+lang+"]["+pos_obj.index()+"][Banner][left]']").val('0%');
				pos_obj.find("input[name='banner[0]["+lang+"]["+pos_obj.index()+"][Banner][top]']").val('0%');
			}
			data_init($(this).parents('.loc_box').attr('plugins'));
		});
		$('#plugins_visual .tool_bar .nav_bar').on('click', '.item.pic' ,function(){    //右侧工具栏选项卡
			$(this).addClass('current').siblings().removeClass('current');
			if($('#plugins_visual .loc_box[style*=block]').attr('plugins')=='banner-0'){  //Banner广告图
				$(this).parent().next('.set_bar').find('.set_item .loc_box .item_box_banner_current').removeClass('item_box_banner_current').parents('.tab_txt').find('.item_box:eq('+$(this).index()+')').addClass('item_box_banner_current');
				data_init('banner-0');
			}else{ //手机版Banner广告图
				$(this).parent().next('.set_bar').find('.set_item .loc_box .item_box_mbanner_current').removeClass('item_box_mbanner_current').parents('.tab_txt').find('.item_box:eq('+$(this).index()+')').addClass('item_box_mbanner_current');
				data_init('mbanner-0');
			}
		});
		$('#plugins_visual .tool_bar .nav_bar .add').on('click', function(){  //右侧工具栏添加图片按钮
			$('#plugins_visual .tool_bar .nav_bar .pic:hidden:first').show();
			set_nav_bar();
		});
		$('#plugins_visual .tool_bar .slide_contents .title').on('click',function(){    //右侧工具栏子栏目滚动卡
			if($(this).parent().hasClass('current')) return;
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			if($(this).parents('.loc_box').find('.item_box_banner').size()){  //Banner图
				$(this).parents('.loc_box').find('.slide_contents').removeClass('current');
				$(this).parents('.loc_box').find('.item_box_banner').find('.slide_contents:eq('+$(this).parent().index()+')').addClass('current');
				$(this).parents('.loc_box').find('.slide_contents .box').slideUp('fast', function(){$(this).hide();});
				$(this).parents('.loc_box').find('.item_box_banner').find('.slide_contents:eq('+$(this).parent().index()+')').find('.box').slideDown('fast');
				iframe.find('#banner_edit_mask .editing').removeClass('editing');
				if($(this).parent().hasClass('slide_title')){
					iframe.find('#banner_edit_mask .banner_edit_mask_title').addClass('editing');
				}else if($(this).parent().hasClass('slide_subtitle')){
					iframe.find('#banner_edit_mask .banner_edit_mask_brief').addClass('editing');
				}
			}else{ //手机版Banner图
				$(this).parents('.loc_box').find('.slide_contents').removeClass('current')
				$(this).parents('.loc_box').find('.item_box_mbanner').find('.slide_contents:eq('+$(this).parent().index()+')').addClass('current');
				$(this).parents('.loc_box').find('.slide_contents .box').slideUp('fast', function(){$(this).hide();});
				$(this).parents('.loc_box').find('.item_box_mbanner').find('.slide_contents:eq('+$(this).parent().index()+')').find('.box').slideDown('fast');
			}
		});
		$('body').on('click', '.tab_box .tab_box_btn', function(){  //切换语言
			lang=$(this).data('lang');
			set_nav_bar();
			$('#form_visual .loc_box').each(function(){
				var loc=$(this).attr('plugins');
				var iframe=loc.substr(0,1)=='m'?$('#plugins_iframe_mobile').contents():$('#plugins_iframe_themes').contents();  //m开头为手机版插件
				data_init($(this).attr('plugins'), iframe);
			});
		});
		$('#plugins_visual .tool_bar .set_item .item_box').on('mouseenter', function(){  //选中编辑区域
			$(this).addClass('current').siblings().removeClass('current');
			var loc=$(this).parents('.loc_box').attr('plugins');
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			var obj=iframe.find('*[plugins='+loc+']');
			if(obj[0].tagName=='BODY') return;  //插件在body
			if($(this).siblings().size()){  //多个
				obj.find('.plugins_pos').hide();
				obj.find('*[plugins_pos='+$(this).index()+'] .plugins_pos').show();
			}else{
				obj.find('.plugins_pos').show();
			}
		});
		$('#plugins_visual .tool_bar').on('mouseleave', function(){  //清除选中编辑区域
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			iframe.find('.plugins_pos').hide();
		});
		$('#plugins_visual .tool_bar .set_item .item_box .plugins_mod_Title input, #plugins_visual .tool_bar .set_item .item_box .plugins_mod_SubTitle input, #plugins_visual .tool_bar .set_item .item_box .plugins_mod_Content textarea, #plugins_visual .tool_bar .set_item .item_box.item_box_banner textarea').on('keyup', function(){  //输入内容
				data_init($(this).parents('.loc_box').attr('plugins'));
		});
		$('#plugins_visual .tool_bar .set_item .item_box_banner_current .slide_contents .box').on('mouseenter', function(){
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			if($(this).parent().hasClass('slide_title')){
				iframe.find('#banner_edit_mask .banner_edit_mask_title').css('background', 'rgba(0,0,0,0.3)');
			}else if($(this).parent().hasClass('slide_subtitle')){
				iframe.find('#banner_edit_mask .banner_edit_mask_brief').css('background', 'rgba(0,0,0,0.3)');
			}	
		});
		$('#plugins_visual .tool_bar .set_item .item_box_banner_current .slide_contents .box').on('mouseleave', function(){
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			iframe.find('#banner_edit_mask .banner_edit_mask_title').css('background', 'none');
			iframe.find('#banner_edit_mask .banner_edit_mask_brief').css('background', 'none');
		});
		$('#plugins_visual .tool_bar .set_item .item_box.item_box_banner select').on('change', function(){  //选择字体
			data_init($(this).parents('.loc_box').attr('plugins'));
		});
		$('#plugins_visual .tool_bar .set_item .font_style i').on('click',function(){  //Banner字体样式
			$(this).toggleClass('current');
			if($(this).hasClass('underline')){    //下划线
				var value=$(this).hasClass('current')?'underline':'none';
			}else if($(this).hasClass('italic')){    //斜体
				var value=$(this).hasClass('current')?'italic':'normal';
			}else{    //加粗
				var value=$(this).hasClass('current')?'bold':'normal';
			}
			$(this).find('input').val(value);
			data_init($(this).parents('.loc_box').attr('plugins'));
		});
		$('#plugins_visual .tool_bar .set_item .font_size .progress').on('mousedown',function(e){    //选择字号进度条
			var set_width=function(value, obj){
				if(value<0){
					value=0;
				}else if(value>_this.width()){
					value=_this.width();
				}
				//进度条对字号比例   176(长度)/120(最大字号)-16(最低字号)
				var font_size=parseInt(value/1.69+16);
				_this.find('.bar').width(value);
				_this.next('.text').text(font_size);
				_this.nextAll('input').val(font_size+'px');
				data_init(_this.parents('.loc_box').attr('plugins'));
			}
			var _this=$(this);
			var left=$(this).offset().left;
			set_width(e.clientX-left, _this);
			$(document).off().on('mousemove',function(ev){set_width(ev.clientX-left, _this);});
			$(document).on('mouseup',function(){
				$(this).off('mousemove');
				$(this).off('mouseup');
			});
		});
		$('#plugins_visual .tool_bar .set_item .font_style .color_style').colpick({    //选择颜色
			layout:'hex',
			submit:1,
			onChange:function(hsb,hex,rgb,el,bySetColor){
				$(el).css('background', '#'+hex).val('#'+hex);
				data_init($(el).parents('.loc_box').attr('plugins'));
			},
			onSubmit:function(hsb,hex,rgb,el){
				$(el).css('background', '#'+hex).val('#'+hex);
				data_init($(el).parents('.loc_box').attr('plugins'));
				$(el).colpickHide();
			}
		});
		$('#plugins_visual .tool_bar .close').on('click', function(){  //右侧工具栏隐藏
			//解锁编辑模式
			var iframe=$('#plugins_iframe_themes').css('visibility')=='visible'?$('#plugins_iframe_themes').contents():$('#plugins_iframe_mobile').contents();
			iframe.find('.current_edit').removeClass('current_edit current');
			//隐藏工具栏
			$(this).parent().removeClass('current_banner current');	
		});
		/*图库 Start*/
		$('#plugins_visual .gallery').on('click', function(){
			frame_obj.pop_form($('#gallery_pop'), 0, 1);
			$('#gallery_pop input[name=plugins]').val($(this).parents('.loc_box').attr('plugins'));
		});
		$('#gallery_pop .item').on('click', function(){
			$(this).addClass('current').siblings().removeClass('current');
		});
		$('#gallery_pop .btn_submit').on('click', function(){
			if(!$('#gallery_pop .item.current').size()){
				global_obj.win_alert(lang_obj.global.selected+lang_obj.global.picture, '' , 1, 'no_mask');
				return false;
			}
			var loc=$('#gallery_pop input[name=plugins]').val();
			var url=$('#gallery_pop .item.current img').attr('src');
			$("#form_visual .loc_box[plugins="+loc+"] .tab_txt_"+lang+" .item_box.item_box_banner_current .multi_img input[type=hidden]").val(url);
			data_init(loc);
			frame_obj.pop_form($('#gallery_pop'), 1, 1);
		});
		/*图库 End*/
	},

	switch_style_init:function(){
		$('.switch_style_btn').on('click', function(){  //切换风格触发按钮
			$('#template_list').removeClass('visual_edit').fadeIn();
			$('#template_list .terminal_list').show();
			if($(this).hasClass('go_style')){
				var $terminal = $('#plugins_visual .terminal_list a.cur').index();
				$('#template_list').addClass('visual_edit');
				$('#template_list .terminal_list a').eq($terminal).click();
				$('#template_list .terminal_list').hide();
			}
			$('#template_list .tem_box:visible').find('.rows').eq(0).find('img').each(function(){
                $(this).attr('src', $(this).attr('_src'));
            });
		});
		$('#template_list .close').on('click',function(){    //切换风格关闭按钮
			$('#template_list').fadeOut();
		});
		/*****风格选择开始*****/
        $('.tem_box .list .edit_btn').click(function(){
            var $this=$(this);
			var href=window.location.href;
            global_obj.win_alert(lang_obj.manage.module.sure_module, function(){
                if($this.attr('data-mobile')){
                    $action="do_action=view.switch_mobile_template&tpl="+$this.attr('data-themes');
					$type='mobile';
                }else{
                    $action='do_action=view.switch_template&themes='+$this.attr('data-themes');
					$type='website';
                }
                $.get('?', $action, function(data){
                    if(data.ret!=1){
                        global_obj.win_alert(data.msg, function(){
                            window.location.reload();
                        }, 'confirm');
                    }else{
						if(href.indexOf('d=edit')!=-1){ //编辑页
							window.location.href='./?m=view&a=visual&d=edit&type='+$type;
						}else{
							window.location.reload();
						}
                    }
                }, 'json');
            }, 'confirm');
        });
        /*下拉加载开始*/
        var scrollFunc = function(e){
            e = e || window.event;
            var $status=0,
                $Box=$('#template_list'),
                $Top=$Box.scrollTop(),
                $Height=$Box.height(),
                $scrollHeight=$Box[0].scrollHeight;
            if(e.wheelDelta && e.wheelDelta<0){ //ie 谷歌 滑轮向下滚动时
                $status=1;
            }else if(e.detail && e.detail<0){ //firefox 滑轮向下滚动时
                $status=1;
            }
            if($status && ($Top+$Height+300>=$scrollHeight)){ //快下拉到底部的时候提前加载
                var $Show=$Box.find('.tem_box:visible .rows:hidden');
                if($Show.length){
                	$Box.find('.tem_box:visible .loading').addClass('cur').find('span').text(lang_obj.manage.view.loading);
                	$Show.eq(0).fadeIn();
                	if($Box.find('.tem_box:visible .rows:hidden').length){
                		$text=lang_obj.manage.view.down_load;
                	}else{
                		$text=lang_obj.manage.view.no_data;
                	}
                	$Box.find('.tem_box:visible .loading').removeClass('cur').find('span').text($text);
                    $Show.eq(0).find('img').each(function(){
                        $(this).attr('src', $(this).attr('_src'));
                    });
                }
            }
                          
        }
        //给页面绑定滑轮滚动事件
        if(document.addEventListener){ //firefox
            document.addEventListener('DOMMouseScroll', scrollFunc, false);
        }
        //滚动滑轮触发scrollFunc方法
        document.onmousewheel = scrollFunc; //ie 谷歌

        $('#template_list').scroll(function(){ //记录滚动条位置
            var $Box=$('#template_list'),
                $Top=$Box.scrollTop();
                if($Top){
                	$Box.addClass('small_head');
                }else{
                	$Box.removeClass('small_head');
                }
            $Box.find('.tem_box:visible input:hidden').val($Top);  
        });
        /*下拉加载结束*/

        $('#template_list .terminal_list a').click(function(){
            var $index=$(this).index();
            $('#template_list .terminal_list a').removeClass('cur').eq($index).addClass('cur');
            if($index){
            	$('#template_list .tem_box').hide();
            	$('#template_list .tem_box.mobile').show();
            	$('#template_list .sub_title').show();
            	$('#template_list .category').hide();
            	$('#template_list').scrollTop($('#template_list .tem_box.mobile').find('input:hidden').val()); //回到对应滚动的位置
            }else{
            	$('#template_list .tem_box').hide();
            	$('#template_list .tem_box').eq($('#template_list .category .cur').index()).show();
            	$('#template_list .sub_title').hide();
            	$('#template_list .category').show();
            	$('#template_list').scrollTop($('#template_list .tem_box').eq($('#template_list .category .cur').index()).find('input:hidden').val()); //回到对应滚动的位置
            }
            $('#template_list .tem_box:visible').find('.rows').eq(0).find('img').each(function(){
                $(this).attr('src', $(this).attr('_src'));
            });
        });

        /*pc版分类选项卡*/
        $('#template_list .category a').click(function(){
        	var $index=$(this).index();
        	$('#template_list .category a').removeClass('cur').eq($index).addClass('cur');
        	$('#template_list .tem_box').hide().eq($index).show();
        	$('#template_list').scrollTop($('#template_list .tem_box').eq($index).find('input:hidden').val()); //回到对应滚动的位置
        	$('#template_list .tem_box:visible').find('.rows').eq(0).find('img').each(function(){
                $(this).attr('src', $(this).attr('_src'));
            });
        });

        $('.tem_box .list .img').hover(function(){ //图片经过
            if($(this).height()<$(this).find('img').height()){
                var $speed=100;
                if($(this).find('img').height()>649){
                    $speed=200;
                }
                if($(this).find('img').height()>1298){
                    $speed=300;
                }
                if($(this).find('img').height()>1947){
                    $speed=500;
                }
                var $time=($(this).find('img').height()-$(this).height())/$speed;
                $(this).find('img').stop().animate({'margin-top':$(this).height()-$(this).find('img').height()},$time*1000);
            }
        },function(){
            $(this).find('img').stop().animate({'margin-top':0},'fast');
        });
		
		//一键下载所有模板
		var download_func=function(){
			$.post('./', 'do_action=view.download_all_themes', function(data){
				global_obj.win_alert_auto_close(data.msg, 'loading', -1);
				if(data.ret==1){
					setTimeout(function(){$('.win_alert_auto_close').remove();}, 2000);
				}else if(data.ret==2){
					download_func();
				}
			}, 'json');
		}
		$('.download_all_themes').on('click', function(){
			global_obj.div_mask();
			global_obj.win_alert_auto_close('正在下载模板...', 'loading', -1);
			download_func();
		});
        /*****风格选择结束*****/
	},

	nav_global:{
		type:'',
		init:function(){
			frame_obj.del_init($('#nav .config_table')); //删除事件
			$('#nav .config_table_body').dragsort({ //元素拖动
				dragSelector:'div',
				dragSelectorExclude:'tbody',
				placeHolderTemplate:'<div class="table_item placeHolder"></div>',
				scrollSpeed:5,
				dragEnd:function(){
					var data=$(this).parent().children('.table_item').map(function(){
						return $(this).attr('data-id');
					}).get();
					$.get('?', {do_action:'view.nav_order', sort_order:data.join('|'), Type:view_obj.nav_global.type}, function(){
						var num=0;
						$('#nav .config_table_body .table_item').each(function(){
							$(this).attr('data-id', num);
							num++;
						});
					});
				}
			});
			
			$('body, html').on('click', '.box_drop_double .children', function(){
				//下一级选项
				var $Item=$(this),
					$Name=$Item.attr('data-name'),
					$Value=$Item.attr('data-value'),
					$Type=$Item.attr('data-type'),
					$Table=$Item.attr('data-table'),
					$Top=$Item.attr('data-top'),
					$Obj=$('.column_rows .box_drop_double');
				$Obj.find('dt input[type=text]').val($Name).parent().find('.hidden_value').val($Value).next().val($Type);
				//$Item.parents('dd').hide();
			}).on('click', '.box_drop_double .item', function(){
				//选择下拉选项
				var $This=$(this),
					$Type=$This.attr('data-type');
				$('.url_rows, .pic_rows').hide();
				$('.column_rows .tab_box').hide();
				$('.column_rows .tab_txt:hidden').each(function(){
					$(this).find('.box_input').removeAttr('notnull');
				});
				if($Type=='add'){//自定义
					$('.url_rows').show();
					$('.column_rows .tab_box').show();
					$('.column_rows .tab_txt:hidden').each(function(){
						$(this).find('.box_input').attr('notnull', 'notnull');
					});
				}else if($Type=='products' || $Type=='category'){//产品
					if($('.pic_rows').length) $('.pic_rows').show();
				}
			}).on('keyup', '.box_drop_double dt .box_input', function(e){
				//输入触发
				var $Value=$.trim($(this).val()),
					$Key=window.event?e.keyCode:e.which,
					$Obj=$(this).parents('.box_drop_double');
				if($Key!=13){ //除了回车键
					if($Value.length>2){ //输入内容，自动默认为“新添加”
						$Obj.find('.hidden_value').val($Value);
						$Obj.find('.hidden_type').val('add');
						$('.url_rows').show();
						$('.column_rows .tab_box').show();
						$('.column_rows .tab_txt:hidden').each(function(){
							$(this).find('.box_input').attr('notnull', 'notnull');
						});
					}
				}
			});
		}
	},

	nav_init:function(){ //头部导航
		view_obj.nav_global.type='nav';
		view_obj.nav_global.init();
		frame_obj.mouse_click($('.multi_img .upload_btn, .pic_btn .edit'), 'ad', function($this){ //产品颜色图点击事件
			var $id=$this.parents('.multi_img').attr('id'),
				$num=$this.parents('.img').attr('num');
			frame_obj.photo_choice_init($id+' .img[num;'+$num+']', 'ad', 5, 'do_action=products.products_img_del&Model=products');
		});
		//图片上传 开始
		$('.ad_drag').dragsort({
			dragSelector:'.adpic_row',
			dragSelectorExclude:'.ad_info, .upload_file_multi',
			placeHolderTemplate:'<li class="adpic_row placeHolder"></li>',
			scrollSpeed:5
		});
		frame_obj.submit_form_init($('#nav_edit_form'), './?m=view&a=nav');
	},
	
	footer_nav_init:function(){  //底部导航
		view_obj.nav_global.type='foot_nav';
		view_obj.nav_global.init();
		frame_obj.submit_form_init($('#nav_edit_form'), './?m=view&a=footer_nav');
	},
}