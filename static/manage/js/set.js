/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var set_obj={
	config_edit_init:function(){
		var html='', part=0, num=1,
			$is_appkey=1;
		$('#config').find('.rows_hd_part:visible').each(function(){
			if(num<10) num='0'+num;
			$(this).before('<a id="part_'+part+'"></a>');
			html+='<li><a href="#part_'+part+'"'+(part==0 ? 'class="current"' : '')+'>'+$(this).find('span').html()+'</a></li>';
			part++;
			num++;
		});
		if(html){
			$('.center_container').append('<ul class="edit_form_part">'+html+'</ul>');
			$('.edit_form_part a').click(function(){
				$('.edit_form_part a').removeClass('current');
				$(this).addClass('current');
			    $('.r_con_wrap').animate({
			      scrollTop: $($(this).attr("href"))[0].offsetTop-20 + "px"
			    }, 200);
			    return false;
			});
			$(window).resize(function(){
				var left = $('.center_container').offset().left+$('.center_container').width();
				$('.edit_form_part').css('left',left+'px');
			}).resize();
		}

		$('#config').scroll(function(){ //记录滚动条位置
			$('input[name=scrolltop]').val($('#config').scrollTop());
		});
		$('#config #edit_form a.set_edit,#config #edit_form a.edit').click(function(){ //记录滚动条位置
			$.post('?do_action=set.keep_config_scrolltop', {'scrolltop':$('input[name=scrolltop]').val()});
		});
		//勾选按钮(非会员购买)
		frame_obj.config_switchery($('.tourists_checkbox .switchery'), 'set.config_tourists_used_edit', '', '');
		//勾选按钮(产品评论)
		frame_obj.config_switchery($('.review_checkbox .switchery'), 'set.config_review_used_edit', 'data-type', 'type');
		//勾选按钮(邮件设置)
		frame_obj.config_switchery($('.used_checkbox .switchery'), 'set.config_email_used_edit', 'data-type', 'template');
		//勾选按钮(邮箱验证)
		frame_obj.config_switchery($('.verification_checkbox .switchery'), 'set.config_switchery', 'data-config', 'config');
		//水印开关
		frame_obj.config_switchery($('.water_checkbox .switchery'), 'set.config_switchery', 'data-config', 'config', function(data){
			if($('.water_checkbox .switchery').hasClass('checked')){
				$('.water_box').show();
			}else{
				$('.water_box').hide();
			}
		});
		//重置秘钥
		$('.btn_reset_api').click(function(){
			$.post('?', {'do_action':'set.set_open_api'}, function(data){
				$('.api_appkey').text(data.msg.appkey);
				$is_appkey==1 && global_obj.win_alert_auto_close(data.msg.tips, '', 1000, '8%');
				$is_appkey=1;
			}, 'json');
		});
		if($.trim($('.api_appkey').text())==''){//秘钥为空
			$is_appkey=0;
			$('.btn_reset_api').click();
		}
	},
	config_function:{
		switchery:function(obj, do_action, param_0, param_1, callback){
			obj.click(function(){
				var $this=$(this),
					$data_value=$this.attr(param_0),
					$used,
					$data=new Object;
					if($this.hasClass('checked')){
						$used=0;
						$this.removeClass('checked');
					}else{
						$used=1;
						$this.addClass('checked');
					}
					$data[param_1]=$data_value;
					$data['IsUsed']=$used;
					$.post('?do_action='+do_action, $data, function(data){
						if(!data.ret){
							global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
							if($used){
								$this.removeClass('checked');
							}else{
								$this.addClass('checked');
							}
						}else if(data.ret==1){
							global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
						}
						if($.isFunction(callback)){
							callback(data);
							return false;
						}
					}, 'json');
			});
		}
	},
	/******************* 基础设置 *******************/
	config_basis_edit:function(){
		/* LOGO图片上传 */
		frame_obj.mouse_click($('#LogoDetail .upload_btn, #LogoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('LogoDetail', '', 1);
		});

		/* ICO图片上传 */
		frame_obj.mouse_click($('#IcoDetail .upload_btn, #IcoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('IcoDetail', '', 1);
		});
		frame_obj.submit_form_init($('#basis_edit_form'), './?m=set&a=config&d=basis');
	},
	/******************* 购物设置 *******************/
	config_shopping_edit:function(){
		frame_obj.switchery_checkbox(function(obj){
			if(obj.find('input[name=LowConsumption]').length){
				$('.low_consumption').show();
			}
		},function(obj){
			if(obj.find('input[name=LowConsumption]').length){
				$('.low_consumption').hide();
			}
		});
		frame_obj.submit_form_init($('#shopping_edit_form'), './?m=set&a=config&d=shopping_set');
	},

	/******************* 语言设置 *******************/
	config_language_edit:function(){
		frame_obj.config_switchery($('.language_switchery'), 'set.language_used', 'data-lang', 'Language');//开启语言版
		frame_obj.config_switchery($('.manage_language_switchery'), 'set.manage_language_used', 'data-lang', 'Language', function(){
			$.post('?do_action=set.keep_config_scrolltop', {'scrolltop':$('input[name=scrolltop]').val()});//记录滚动条位置
			window.location.reload();
		});//开启后台其它语言版
		frame_obj.config_switchery($('.config_switchery'), 'set.config_switchery', 'data-config', 'config');//网站基本设置开关

		frame_obj.mouse_click($('#FlagDetail .upload_btn, #FlagDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('FlagDetail', '', 1);
		});
		frame_obj.submit_form_init($('#language_edit_form'), './?m=set&a=config');
	},

	/******************* 会员设置 *******************/
	config_user_edit:function(){
		frame_obj.del_init($('.config_table, .r_con_table'));
		//会员等级
		frame_obj.config_switchery($('.user_level_used .switchery'), 'set.user_level_used', 'data-id', 'LId');//会员等级开关
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		frame_obj.submit_form_init($('#user_level_form'), './?m=set&a=config&d=user_level');

		//会员参数
		//frame_obj.config_switchery($('.switchery.UserVerification'), 'set.config_switchery', 'data-config', 'config');//邮箱验证
		$('#reg_set .switchery[field]').click(function(){//注册参数
			var o=$(this);
			if(o.attr('field').indexOf('NotNull')!=-1){
				var notnull_obj=o.attr('field').replace('NotNull', '');
				if($('#reg_set .switchery[field='+notnull_obj+']').attr('status')==0){return false;}
			}
			$.get('?', 'do_action=set.user_reg_set&field='+o.attr('field')+'&status='+o.attr('status'), function(data){
				if(data.ret==1){
					var notnull_obj=$('#reg_set .switchery[field='+o.attr('field')+'NotNull]');
					if(o.attr('status')==0){
						o.attr('status', 1).addClass('checked');
						if(notnull_obj.size()){
							notnull_obj.removeClass('no_drop');
						}
					}else{
						o.attr('status', 0).removeClass('checked');
						if(notnull_obj.size()){
							notnull_obj.attr('status', 0).addClass('no_drop').removeClass('checked');
						}
					}
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}else{
					global_obj.win_alert_auto_close(lang_obj.global.set_error, '', 1000, '8%');
				}
			}, 'json');
		});

		frame_obj.fixed_right($('#reg_set .edit, #reg_set .add'),'.reg_set_edit');
		$('#reg_set .edit, #reg_set .add').click(function(){
			set_obj.load_edit_form('.reg_set_edit',$(this).attr('data-url'),'get','',function(){
				$('#type_select').change(function(){
					$(this).val()==1?$('.row_option').show():$('.row_option').hide();
				});
				frame_obj.submit_form_init($('#reg_set_form'), './?m=set&a=config&d=user_reg_set&p=list');
			});
		});
	},
	/******************* 水印设置 *******************/
	config_watermark_edit:function(){
		/* 水印图片上传 */
		frame_obj.mouse_click($('#WatermarkDetail .upload_btn, #WatermarkDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('WatermarkDetail', '', 1);
		});
		/* 水印位置选择 */
		$('.watermark_position').click(function(){
			var $num=$(this).attr('data-position');
			$('.watermark_position').removeClass('cur').eq($(this).index()).addClass('cur');
			$('input[name=WaterPosition]').val($num);
		});
		$('#preview_pic').css('opacity',$('input[name=Alpha]').val()*0.01);
		$('#slider').slider({
			value:$('input[name=Alpha]').val(),
			change:function(){
				var val=$(this).slider('value');
				$('#slider_value').html(val+'%');
				$('#preview_pic').css('opacity',val*0.01);
				$('input[name=Alpha]').val(val);
			}
		});
		if($('input[name=Start]').length){
			frame_obj.box_progress(function(data){
				if(data){
					if(data.ret==3){ //进度完成
						$('.input_button .btn_cancel').show();
					}else if(data.ret==2){ //下一页
						$('.box_progress input[name=IsColor]').val(parseInt(data.msg.IsColor));
						$('#btn_progress_keep').click();
					}
				}else{
					$('.input_button .btn_cancel, .input_button .btn_proceed').show();
				}
			});
			$('#btn_progress_keep').click();
		}else{
			frame_obj.submit_form_init($('#watermark_edit_form'), './?m=set&a=config&d=watermark_set&update=1');
		}
	},
	/******************* 分享栏目 *******************/
	config_share_edit:function(){
		$('.btn_add_share').on('click', function(){
			var allcount=parseInt($('.share_tpl').attr('data-count')),
				length=parseInt($('.share_box .share_item').length);
			if(length<allcount){
				$('.share_box').append($('.share_tpl').html());
			}
			is_add();
		});
		$('#share_edit_form').on('change', 'select[name=tax_code_type]', function(){
			var $value=$(this).val();
				$old_value=$(this).parents('.share_item').find('.share_del').attr('data-share');
				$('select[name=tax_code_type]>option[value='+$old_value+']').removeClass('hide').removeAttr('disabled');
			if($value==0){
				$(this).parents('.share_item').find('input').attr('name', '');
				$(this).parents('.share_item').find('.share_del').attr('data-share', '');
			}else{
				$(this).parents('.share_item').find('input').attr('name','Share'+$value);
				$(this).parents('.share_item').find('.share_del').attr('data-share',$value);
				$('select[name=tax_code_type]>option[value='+$value+']').addClass('hide').attr('disabled', 'disabled');
				$(this).find('option[value='+$value+']').removeClass('hide').removeAttr('disabled');
			}
		}).on('click', '.share_del', function(){
			var $this=$(this),
				$value=$this.attr('data-share');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$this.parents('.share_item').remove();
				$('select[name=tax_code_type]>option[value='+$value+']').removeClass('hide').removeAttr('disabled');
				is_add();
			}, 'confirm');
		});
		function is_add(){
			var allcount=parseInt($('.share_tpl').attr('data-count')),
				length=parseInt($('.share_box .share_item').length);
			if(length>=allcount){
				$('.btn_add_share').hide();
			}else{
				$('.btn_add_share').show();
			}
		}
		is_add();
		frame_obj.submit_form_init($('#share_edit_form'), './?m=set&a=config&d=share');
	},
	/******************* 绑定域名 *******************/
	domain_binding_init:function(){
		$('.del_domain_binding').click(function(){
			var $this=$(this),
			$domain=$this.attr('data-domain');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.post('?', {'do_action':'set.domain_binding_edit', 'DomainBinding':$domain}, function(data){
					if(data.ret==1){
						location.href=location.href;
					}else{
						global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
					}
				}, 'json');
			}, 'confirm');
		});
		if($('input[name=domain_binding_verification]').length){
			var $domain=$('input[name=domain_binding_verification]').val();
			setTimeout(function(){
				$.post('?', {'do_action':'set.domain_binding_verification', 'DomainBinding':$domain}, function(data){
					$('label.title').text(data.msg);
					if(data.ret!=1){
						$('.input.tips').fadeIn();
					}
				}, 'json');
			}, 1000);
		}

   		$('.domain_binding_verification_btn').click(function(){
			$('.verification_table tbody tr[data-status=0]').each(function(){
				var $this=$(this),
					$dm=$this.attr('data-domain'),
					$type=$this.attr('data-type');
				$.post('?', {'do_action':'set.domain_binding_verification', 'DomainBinding':$dm, 'Type':$type}, function(data){
					$this.find('.status').text(data.msg);
				}, 'json');
			});
			$(this).fadeOut();
		});

		frame_obj.submit_form_init($('#domain_binding_edit_form'), './?m=set&a=domain_binding', '', '', function(data){
			global_obj.win_alert_auto_close(data.msg[0], '', 1000, '8%');
			setTimeout(function(){
				if(data.ret==1){
					location.href='./?m=set&a=domain_binding&d=edit&status=2&domain='+data.msg[1];
				}else{
					location.href='./?m=set&a=domain_binding';
				}
			}, 1000);
		});
	},
	/******************* 绑定域名 *******************/
	/******************* SEO设置 *******************/
	config_seo_edit:function(){
		// frame_obj.submit_form_init($('#seo_edit_form'), './?m=set&a=config&d=seo');
	},
	/******************* 网站地图设置 *******************/
	config_sitemap_edit:function(){
		//多语言事件
		
		$(window).resize(function(){
			$('.seo_box .lang_txt .unit_input').each(function(){
				var $this=$(this),
					$width=$(this).parent().width(),
					$b_w=$this.find('b').outerWidth(true);
				$this.find('textarea').css('width',$width-$b_w-23+'px');
				$this.find('input').css('width',$width-$b_w-3+'px');
			});
		}).resize();

		$('#set_edit_form').on('focus', '.multi_lang .box_input, .multi_lang .box_textarea', function(){
			$('.multi_lang').each(function(){
				$(this).find('.lang_txt').not('[data-default="1"]').hide();
			});
			$(this).parents('.multi_lang').find('.lang_txt').show();
			if($(this).hasClass('box_textarea')||$(this).hasClass('box_input')){
				$(window).resize();
			}
		});
		$('html').click(function(e){
			if(!$(e.target).parents('.lang_txt').length){
				$('.multi_lang').each(function(){
					$(this).find('.lang_txt').not('[data-default="1"]').hide();
				});
			}
		});


		frame_obj.submit_form_init($('#set_edit_form'), './?m=set&a=seo');  


		function html_tab_button($class, $lang){
			var $language='';
			$default=ueeshop_config.lang;
			$lang && ($default=$lang);
			$html='';
			$html+='<dl class="tab_box_row'+(ueeshop_config.language.length==1?' hide':'')+($class?' '+$class:'')+'">';
				$html+='<dt><span>'+lang_obj.language[$default]+'</span><i></i></dt>';
				$html+='<dd class="drop_down">';
				for(i in ueeshop_config.language){
					$language=ueeshop_config.language[i];
					$html+='<a class="tab_box_btn item"'+($default==$language?' style="display:none;"':'')+' data-lang="'+$language+'"><span>'+lang_obj.language[$language]+'</span><i></i></a>';
				}
				$html+='</dd>';
				$html+='<dd class="tips">'+lang_obj.manage.error.supplement_lang+'</dd>';
			$html+='</dl>';
			return $html;
		}

		frame_obj.fixed_right($('#edit_keyword'), '.fixed_edit_keyword', function($this){
			//修改 关键词
			var $Form=$('#edit_keyword_form'),
				$List=$('.seo_box .keys_row .option_selected .btn_attr_choice'),
				$Html='', $i=0, $ID='',
				$Type=$('input[name=Type]').val();
			$Form.find('.edit_keyword_list').html(''); //清空内容
			
			$Html+=	'<div class="rows">';
			$Html+=		'<label>'+lang_obj.manage.global.name+'<div class="tab_box">'+html_tab_button()+'</div></label>';
			$Html+=		'<div class="input">';
			for(k in ueeshop_config.language){
				$i=0;
				$Lang=ueeshop_config.language[k];
				$Html+=		'<div class="tab_txt tab_txt_'+$Lang+'" lang="'+$Lang+'"'+(ueeshop_config.lang==$Lang?'style="display:block;"':'')+'>';
				$List.each(function(){
					$Html+=		'<div class="item clean" data-id="'+$i+'">';
					$Html+=			'<input type="text" class="box_input fl" name="Name['+$i+']['+$Lang+']" value="" size="21" maxlength="255" autocomplete="off" notnull />';
					$Html+=			'<a href="javascript:;" class="attr_delete fl icon_delete_1"><i></i></a>';
					$Html+=		'</div>';
					++$i;
				});
				$Html+=		'</div>';
			}
			$Html+=		'</div>';
			$Html+=	'</div>';
			$Form.find('.edit_keyword_list').html($Html);
			if($Html){
				$Form.find('.edit_keyword_list, .box_button').show();
				$Form.find('.bg_no_table_data').hide();
			}else{
				$Form.find('.edit_keyword_list, .box_button').hide();
				$Form.find('.bg_no_table_data').show();
			}
			$.post('?', {'do_action':'set.seo_keyword_select', 'Type':$Type}, function(data){
				if(data.ret==1){
					for(lang in data.msg){ //属性名称
						for(k in data.msg[lang]){
							$Obj=$Form.find('.edit_keyword_list .tab_txt_'+lang+' .item:eq('+k+')');
							$Obj.find('.box_input').val(data.msg[lang][k]);
						}
					}
					//事件
					$Form.on('click', '.attr_delete', function(){
						$Form.find('.edit_keyword_list .item[data-id="'+$(this).parent().attr('data-id')+'"]').remove();
					});
				}
			}, 'json');
		});
	
		//修改关键词
		frame_obj.submit_form_init($('#edit_keyword_form'), '', function(){
			var $name='';
			$('#edit_keyword_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var $Obj=$('.keys_row'),
					$index=0;
				$Obj.find('.option_selected .btn_attr_choice').each(function(){
					$index=$(this).index();
					$(this).find('input').val(result.msg[ueeshop_config.lang][$index]);
					$(this).find('b').html(result.msg[ueeshop_config.lang][$index]);
				});
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});      

		if($('#btn_progress_keep').length){
			frame_obj.box_progress(function(data){
				if(data){
					if(data.ret==3){ //进度完成
						$('.input_button .btn_cancel').show();
					}else if(data.ret==2){ //下一页
						$('.box_progress input[name=WebSiteNum]').val(parseInt(data.msg.WebSiteNum));
						setTimeout(function(){
							$('#btn_progress_keep').click();
						}, 300);					
					}
				}else{
					$('.input_button .btn_cancel, .input_button .btn_proceed').show();
				}
			});
			$('#btn_progress_keep').click();
		}
	},
	/******************* 网站信息设置 *******************/
	config_company_edit:function(){
		frame_obj.mouse_click($('#LogoDetail .upload_btn, #LogoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('LogoDetail', '', 1);
		});
		/* 表单提交 */
		frame_obj.submit_form_init($('#company_edit_form'), './?m=set&a=config&d=company');
	},
	/******************* 邮箱设置 *******************/
	config_email_edit:function(){
		frame_obj.submit_form_init($('#email_config_form'), './?m=set&a=config&d=email&ver=1');
		if($('#smtp_ver_form').length){
			$.get('?', 'do_action=set.config_email_verification', function(data){
				if(data.ret==1){
					$('#smtp_ver_form .tips').fadeOut();
				}else{
					$('#smtp_ver_form .tips').text(data.msg[1]);
				}
				$('#smtp_ver_form .status').text(data.msg[0]);
				$('#smtp_ver_form .loading').fadeOut();
				$('#smtp_ver_form .btn_global').fadeIn();
			}, 'json');
		}
	},
	/******************* 邮件设置 *******************/
	config_email_templete_edit:function(){
		frame_obj.submit_form_init($('#email_edit_form'), './?m=set&a=config');
	},
	
	/******************* 汇率设置 *******************/
	exchange_init:function(){
		frame_obj.del_init($('#exchange .config_table'));
		$('.config_table').on('click', '.used_checkbox .switchery', function(){	//启用
			var $this=$(this),
				$cid=$this.attr('data-cid');
			if(!$this.hasClass('no_drop')){
				if(!$this.hasClass('checked')){
					$this.addClass('checked');
				}else{
					$this.removeClass('checked');
				}
				$.get('?', 'do_action=set.exchange_switch&Type=0&CId='+$cid, function(data){
					if(data.ret!=1){
						global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
					}
				}, 'json');
			}
		}).on('click', '.default_checkbox .switchery', function(){	//默认
			var $this=$(this),
				$tr=$this.parents('tr');
			if(!$this.hasClass('no_drop') && !$this.hasClass('checked')){
				$this.addClass('checked no_drop');
				$tr.find('.used_checkbox .switchery').addClass('checked no_drop');
				$tr.parents('tr').siblings().find('.used_checkbox .switchery').removeClass('no_drop');
				$tr.siblings().find('.default_checkbox .switchery').removeClass('checked no_drop');
				$.get('?', 'do_action=set.exchange_switch&Type=1&CId='+$tr.attr('cid'), function(data){
					if(data.ret==1){
						window.location.href='./?m=set&a=exchange';
					}else{
						global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
					}
				}, 'json');
			}
		}).on('click', '.manage_default_checkbox .switchery', function(){	//后台默认
			var $this=$(this),
				$tr=$this.parents('tr');
			if(!$this.hasClass('no_drop') && !$this.hasClass('checked')){
				$this.addClass('checked no_drop');
				$tr.find('.used_checkbox .switchery').addClass('checked no_drop');
				$tr.parents('tr').siblings().find('.used_checkbox .switchery').removeClass('no_drop');
				$tr.siblings().find('.manage_default_checkbox .switchery').removeClass('checked no_drop');
				$.get('?', 'do_action=set.exchange_switch&Type=2&CId='+$tr.attr('cid'), function(data){
					if(data.ret==1){
						window.location.href='./?m=set&a=exchange';
					}else{
						global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
					}
				}, 'json');
			}
		});
		/* 启用按钮 */
		$('#edit_form').on('click', '.switchery', function(){	//后台默认
			if($(this).hasClass('checked')){
				$(this).removeClass('checked').find('input').attr('checked', false);
				if($(this).find('input[name=IsUsed]').length){
					$("#default").css("display", "none");
				}
			}else{
				$(this).addClass('checked').find('input').attr('checked', true);
				if($(this).find('input[name=IsUsed]').length){
					$("#default").css("display", "block");
				}
			}
		});
		/* 国旗上传 */
		frame_obj.mouse_click($('#FlagDetail .upload_btn, #FlagDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('FlagDetail', '', 1);
		});
		/* 表单提交 */
		frame_obj.submit_form_init($('#exchange_edit_form'), './?m=set&a=exchange');
	},
    /******************* 伊朗汇率设置 *******************/
    newexchange_init:function(){
        frame_obj.del_init($('#newexchange .config_table'));
        $('.config_table').on('click', '.used_checkbox .switchery', function(){	//启用
            var $this=$(this),
                $cid=$this.attr('data-cid');
            if(!$this.hasClass('no_drop')){
                if(!$this.hasClass('checked')){
                    $this.addClass('checked');
                }else{
                    $this.removeClass('checked');
                }
                $.get('?', 'do_action=set.newexchange_switch&Type=0&CId='+$cid, function(data){
                    if(data.ret!=1){
                        global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
                    }
                }, 'json');
            }
        }).on('click', '.default_checkbox .switchery', function(){	//默认
            var $this=$(this),
                $tr=$this.parents('tr');
            if(!$this.hasClass('no_drop') && !$this.hasClass('checked')){
                $this.addClass('checked no_drop');
                $tr.find('.used_checkbox .switchery').addClass('checked no_drop');
                $tr.parents('tr').siblings().find('.used_checkbox .switchery').removeClass('no_drop');
                $tr.siblings().find('.default_checkbox .switchery').removeClass('checked no_drop');
                $.get('?', 'do_action=set.newexchange_switch&Type=1&CId='+$tr.attr('cid'), function(data){
                    if(data.ret==1){
                        window.location.href='./?m=set&a=newexchange';
                    }else{
                        global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
                    }
                }, 'json');
            }
        }).on('click', '.manage_default_checkbox .switchery', function(){	//后台默认
            var $this=$(this),
                $tr=$this.parents('tr');
            if(!$this.hasClass('no_drop') && !$this.hasClass('checked')){
                $this.addClass('checked no_drop');
                $tr.find('.used_checkbox .switchery').addClass('checked no_drop');
                $tr.parents('tr').siblings().find('.used_checkbox .switchery').removeClass('no_drop');
                $tr.siblings().find('.manage_default_checkbox .switchery').removeClass('checked no_drop');
                $.get('?', 'do_action=set.exchange_switch&Type=2&CId='+$tr.attr('cid'), function(data){
                    if(data.ret==1){
                        window.location.href='./?m=set&a=exchange';
                    }else{
                        global_obj.win_alert(lang_obj.global.set_error, '', '', 1);
                    }
                }, 'json');
            }
        });
        /* 启用按钮 */
        $('#edit_form').on('click', '.switchery', function(){	//后台默认
            if($(this).hasClass('checked')){
                $(this).removeClass('checked').find('input').attr('checked', false);
                if($(this).find('input[name=IsUsed]').length){
                    $("#default").css("display", "none");
                }
            }else{
                $(this).addClass('checked').find('input').attr('checked', true);
                if($(this).find('input[name=IsUsed]').length){
                    $("#default").css("display", "block");
                }
            }
        });

        /* 表单提交 */
        frame_obj.submit_form_init($('#newexchange_edit_form'), './?m=set&a=newexchange');
    },
	/******************* 支付方式 *******************/
	payment_edit_init:function(){
		frame_obj.del_init($('.config_table'));
		if($('#payment .config_table_body').length){
			frame_obj.dragsort($('#payment .config_table_body'), 'set.payment_order', 'div', 'tbody', '<div class="table_item placeHolder"></div>'); //元素拖动
		}
		$('.payment_used .switchery').click(function(){
			var $pid=$(this).attr('data-pid'),
				$used;
			if($(this).hasClass('checked')){
				$used=0;
			}else{
				$used=1;
			}
			$.post('?do_action=set.payment_used',{'PId':$pid,'IsUsed':$used},function(){},'json');
		});
		frame_obj.switchery_checkbox();
		/* 支付接口图片上传 */
		frame_obj.mouse_click($('#LogoDetail .upload_btn, #LogoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('LogoDetail', '', 1);
		});
		frame_obj.submit_form_init($('#payment_edit_form'), './?m=set&a=payment');
	},
	/******************* 国家管理 start *******************/
	country_init:function(){

		var html='', part=0, num=1;
		$('#country').find('.rows_hd_part:visible').each(function(){
			if(num<10) num='0'+num;
			$(this).before('<a id="part_'+part+'"></a>');
			html+='<li><a href="#part_'+part+'"'+(part==0 ? 'class="current"' : '')+'>'+$(this).find('span').html()+'</a></li>';
			part++;
			num++;
		});
		if(html){
			$('.center_container_1000').append('<ul class="edit_form_part">'+html+'</ul>');
			$('.edit_form_part a').click(function(){
				$('.edit_form_part a').removeClass('current');
				$(this).addClass('current');
			    $('.r_con_wrap').animate({
			      scrollTop: $($(this).attr("href")).parents('table')[0].offsetTop+40 + "px"
			    }, 300);
			    return false;
			});
			$(window).resize(function(){
				var left = $('.center_container_1000').offset().left+$('.center_container_1000').width();
				$('.edit_form_part').css('left',left+'px');
			}).resize();
		}
		
		$('.r_con_table tbody td.default').css({'color':'#F00', 'font-weight':'bold', 'font-size':'14px'});
		$('.r_con_table').on('click', '.used_checkbox .switchery', function(){	//启用
			var $this=$(this),
				$tr=$this.parents('tr'),
				check=0;
			if(!$this.hasClass('no_drop')){
				if(!$this.hasClass('checked')){
					$this.addClass('checked');
					check=1;
				}else{
					$this.removeClass('checked');
					$tr.find('.hot_checkbox .switchery').removeClass('checked');
				}
				$.get('?', 'do_action=set.country_switch&Type=0&CId='+$tr.attr('cid')+'&Check='+check, function(data){
					if(data.ret!=1){
						global_obj.win_alert(lang_obj.global.set_error);
					}
				}, 'json');
			}
		}).on('click', '.hot_checkbox .switchery', function(){	//热门国家
			var $this=$(this),
				$tr=$this.parents('tr'),
				check=0;
			if(!$this.hasClass('no_drop')){
				if(!$this.hasClass('checked')){
					$this.addClass('checked');
					$tr.find('.used_checkbox .switchery').addClass('checked');
					check=1;
				}else{
					$this.removeClass('checked');
				}
				$.get('?', 'do_action=set.country_switch&Type=1&CId='+$tr.attr('cid')+'&Check='+check, function(data){
					if(data.ret!=1){
						global_obj.win_alert(lang_obj.global.set_error);
					}
				}, 'json');
			}
		});
		frame_obj.del_init($('#country .r_con_table'));
	},
	
	country_edit_init:function(){
		frame_obj.switchery_checkbox(function(obj){
			var $this=obj.parent();
			if($this.find('input[name=IsUsed]').length){
				$('input[name=IsHot], input[name=HasState]').parents('.rows').css('display', 'block');
			}else if($this.find('input[name=IsHot]').length){
				$('input[name=IsDefault]').parents('.rows').css('display', 'block');
			}
		}, function(obj){
			var $this=obj.parent();
			if($this.find('input[name=IsUsed]').length){
				$('input[name=IsHot], input[name=IsDefault], input[name=HasState]').removeAttr('checked').parents('.rows').css('display', 'none');
			}else if($this.find('input[name=IsHot]').length){
				$('input[name=IsDefault]').removeAttr('checked').parents('.rows').css('display', 'none');
			}
		});
		/* 国旗上传 */
		frame_obj.mouse_click($('#FlagDetail .upload_btn, #FlagDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('FlagDetail', '', 1);
		});
		/* 表单提交 */
		frame_obj.submit_form_init($('#country_edit_form'), './?m=set&a=country');
	},
	
	country_states_init:function(){
		var CId=$('.country_states').attr('CId');
		frame_obj.dragsort($('.country_states .country_menu_list dl'), 'set.country_states_order', 'dt i', 'a', '<dt class="placeHolder"></dt>', 'dt');
		frame_obj.fixed_right($('.country_states .edit, .country_states .add'), '.country_states_edit', function($this){
			set_obj.load_edit_form('.country_states_edit', $this.attr('data-url'), 'GET', '', function(){
				frame_obj.submit_form_init($('#country_states_edit_form'), '', '', '', function(){
					window.location.reload();
				});
			});
		});
		frame_obj.del_init($('#country .country_states'));
	},
	/******************* 国家管理 end *******************/

	/******************* 风格公共部分 *******************/
	themes_init:function(){
		//下拉效果
		$('.inside_container .more').parent().hover(function(){
			$(this).find('.more_menu').show().stop(true).animate({'top':31, 'opacity':1}, 250);
		}, function(){
			$(this).find('.more_menu').show().stop(true).animate({'top':21, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
	},

	/******************* 首页设置 *******************/
	themes_index_set_init:function(){
		$('.abs_item').click(function(){
			var $this=$(this);
			$('.abs_item').removeClass('cur');
			$this.addClass('cur');
			set_obj.load_edit_form('.index_set_exit','./?m=set&a=themes&d=index_set','get','&WId='+$this.attr('data-wid'),function(){
				if($this.attr('data-type')=='Banner'){
					ad_init();
				}else{
					web_init();
				}
				frame_obj.upload_img_init();
				frame_obj.submit_form_init($('#index_set_edit_form'),$('input[name=return_url]').val()+'&WId='+$('.abs_item.cur').attr('data-wid'));
			});
		});
		if($('.abs_item.cur').length){
			$('.abs_item.cur').click();
		}else{
			$('.abs_item').eq(0).click();
		}
		function web_init(){
			frame_obj.mouse_click($('#index_set .multi_img .upload_btn, #index_set .multi_img .pic_btn .edit'), 'img', function($this){ //点击上传图片
				var $id=$this.parents('.multi_img').attr('id');
				frame_obj.photo_choice_init($id, '', 1);
			});
		}
		function ad_init(){
			$('.show_type .ty_list').click(function(){
				$('.show_type .ty_list').removeClass('cur').find('input').removeAttr('checked');
				$(this).addClass('cur').find('input').attr('checked','checked');
			});
			frame_obj.dragsort($('.ad_drag'), '', '.adpic_row', '.ad_view, .l_img', '<li class="adpic_row placeHolder"></li>');
			frame_obj.mouse_click($('#index_set .multi_img .upload_btn, #index_set .multi_img .pic_btn .edit'), 'ad', function($this){ //产品颜色图点击事件
				var $id=$this.attr('id'),
					$lang=$this.parents('.img').attr('data-lang'),
					$num=$this.parents('.img').attr('num');
				frame_obj.photo_choice_init('PicDetail_'+$lang+' .img[num;'+$num+']', 'ad', 5, 'do_action=products.products_img_del&Model=products');
			});
		}
	},

	/******************* 风格管理 *******************/
	themes_themes_edit_init:function(){
		$('.themes_current .use').click(function(){
			var $this=$(this);
			global_obj.win_alert(lang_obj.manage.module.sure_module, function(){
				if($this.hasClass('IsMobile')){
					$action="do_action=set.themes_mobile_themes_edit&tpl="+$this.attr('data-themes');
				}else{
					$action='do_action=set.themes_themes_edit&themes='+$this.attr('data-themes');
				}
				$.get('?', $action, function(data){
					if(data.ret!=1){
						global_obj.win_alert(data.msg, function(){
							window.location.reload();
						}, 'confirm');
					}else{
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
		});
		$('#themes_themes .item').click(function(){
			var $themes=$(this).attr('data-themes'),
				$url=$(this).attr('data-url'),
				$img=$(this).attr('data-img'),
				$name=$(this).attr('data-name');
			$('#themes_themes .item').removeClass('current');
			$(this).addClass('current');
			$('.themes_current .themes').text($name);
			$('.themes_current .use').attr('data-themes',$themes);
			$('.themes_current .view').attr('href',$url);
			$('.themes_current .themes_img img').attr('src',$img);
		});
		$('#themes_themes .item.current').click();
		$('.themes_themes').height($(window).height()-$('.themes_themes').offset().top-25);
		$('.themes_current .themes_img').height($(window).height()-$('.themes_img').offset().top-25);
		$('.themes_current .themes_img').hover(
			function(){
				if($(this).height()<$(this).find('img').height()){
					var $speed=100;
					if($(this).find('img').height()>1000){
						$speed=200;
					}
					if($(this).find('img').height()>2000){
						$speed=300;
					}
					if($(this).find('img').height()>3000){
						$speed=500;
					}
					var $time=($(this).find('img').height()-$(this).height())/$speed;
					$(this).find('img').stop().animate({'margin-top':$(this).height()-$(this).find('img').height()},$time*1000);
				}
			},
			function(){
				$(this).find('img').stop().animate({'margin-top':0},'fast');	
			}
		);
		window.onload = function(){
			$(window).resize(function(){
				frame_obj.Waterfall('themes_box', '157', 3, 'item'); // 瀑布流
			}).resize();
		}
	},
	
	themes_products_list_edit_init:function(){
		frame_obj.switchery_checkbox(function(obj){
			if(obj.find('input[name=IsColumn]').length){
				obj.parents('.rows').next().next().slideDown();
			}
		}, function(obj){
			if(obj.find('input[name=IsColumn]').length){
				obj.parents('.rows').next().next().slideUp();
			}
		});
		$('#edit_form').delegate('input[name=reset]', 'click', function(){
			global_obj.win_alert(lang_obj.global.reset_confirm, function(){
				$.get('?', "do_action=set.themes_products_list_reset", function(data){
					if(data.ret!=1){
						global_obj.win_alert(data.msg);
					}else{
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
		});
		$('#edit_form .choice_btn').on('click', function(){
			$(this).addClass('current').siblings().removeClass('current');
			$(this).children('input').attr('checked', true);
		});
		$('#edit_form .order_list select').on('change', function(){
			var $number=$(this).children('option:selected').attr('number'),
				$select=$(this).parents('.rows').next().find('span.input');
			if($number){
				var ary=$number.split(',');
				var $html='<select name="OrderNumber">';
				for(var i=0; i<ary.length; ++i){
					$html+='<option'+($select.attr('number')==ary[i]?' selected':'')+'>'+ary[i]+'</option>';
				}
				$html+='</select>';
			}
			$select.html($html);
		});
		$('#edit_form .order_list select').change();
		$('#edit_form .choice_btn.current').click();
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=themes&d=products_list');
	},
	
	themes_products_detail_edit_init:function(){
		$('#edit_form .item').hover(function(){
			$(this).children('.info').stop(true, true).slideDown(500);
		},function(){
			$(this).children('.info').stop(true, true).slideUp(500);
		}).children('.img').click(function(){
			if(!$(this).parent().hasClass('current')){
				var $this=$(this);
				global_obj.win_alert(lang_obj.manage.module.sure_module, function(){
					$this.parent().addClass('current').siblings().removeClass('current');
					$.post('?', "do_action=set.themes_products_detail_themes_edit&Key="+$this.parent().attr('detail-id'), function(data){
						if(data.ret!=1){
							global_obj.win_alert(data.msg);
						}else{
							window.location.reload();
						}
					}, 'json');
				}, 'confirm');
				return false;
			}
		});
		
		$('.tab_box .tab_txt').on('click', '.add', function(){ //添加帮助选项
			var $box=$(this).parent('.help_item'),
				$num=$box.index(),
				$obj=$(this).parents('.tab_txt');
			if($obj.find('.help_item:eq(0) .not_input').is(':hidden')){
				$('.tab_box .tab_txt').each(function(){
					$(this).find('.help_item:eq(0) .not_input, .help_item:eq(0) .switchery, .help_item:eq(0) b, .help_item:eq(0) .del').show();
					$(this).find('.help_item:eq(0) .not_input input').val('');
					$(this).find('.help_item:eq(0) input').attr('disabled', false);
				});
			}else{
				$('.tab_box .tab_txt').each(function(){
					$(this).find('.help_item:eq('+$num+')').after($(this).find('.help_item:eq('+$num+')').prop('outerHTML'));
					$(this).find('.help_item:eq('+($num+1)+')').siblings().find('.add').hide();
					$(this).find('.help_item:eq('+($num+1)+') .not_input input').val('');
				});
			}
		}).on('click', '.del', function(){ //删除帮助选项
			var $box=$(this).parent('.help_item'),
				$num=$box.index(),
				$obj=$(this).parents('.tab_txt');
			if($obj.find('.help_item').size()==1){
				$('.tab_box .tab_txt').each(function(){
					$(this).find('.help_item:eq(0) .not_input, .help_item:eq(0) .switchery, .help_item:eq(0) b, .help_item:eq(0) .del').hide();
					$(this).find('.help_item:eq(0) input').attr('disabled', true);
				});
			}else{
				$('.tab_box .tab_txt').each(function(){
					$(this).find('.help_item:eq('+$num+')').remove();
					$(this).find('.help_item:last .add').show();
					$(this).find('.help_item:last').siblings().find('.add').hide();
				});
			}
		}).on('change', 'input.input_url', function(){
			var $num=$(this).parents('.help_item').index();
			$(this).parents('.tab_txt').siblings().find('.help_item:eq('+$num+') input[name=Url\\[\\]]').val($(this).val());
		}).on('click', '.switchery', function(){
			var $num=$(this).parents('.help_item').index();
			if($(this).hasClass('checked')){
				$(this).removeClass('checked').find('input').attr('checked', false);
				$(this).parents('.tab_txt').siblings().find('.help_item:eq('+$num+') .switchery input').attr('checked', false).parent().removeClass('checked');
			}else{
				$(this).addClass('checked').find('input').attr('checked', true);
				$(this).parents('.tab_txt').siblings().find('.help_item:eq('+$num+') .switchery input').attr('checked', true).parent().addClass('checked');
			}
		});
		
		frame_obj.dragsort($('#share_list'), '', 'div', '', '<div class="share_btn fl placeHolder"></div>'); //分享图标排序拖动
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=themes&d=products_detail');
	},
	
	themes_nav_global:{
		type:'',
		init:function(){
			frame_obj.del_init($('#themes .config_table')); //删除事件
			$('#themes .config_table_body').dragsort({ //元素拖动
				dragSelector:'div',
				dragSelectorExclude:'tbody',
				placeHolderTemplate:'<div class="table_item placeHolder"></div>',
				scrollSpeed:5,
				dragEnd:function(){
					var data=$(this).parent().children('.table_item').map(function(){
						return $(this).attr('data-id');
					}).get();
					$.get('?', {do_action:'set.themes_nav_order', sort_order:data.join('|'), Type:set_obj.themes_nav_global.type}, function(){
						var num=0;
						$('#themes .config_table_body .table_item').each(function(){
							$(this).attr('data-id', num);
							num++;
						});
					});
				}
			});
			
			$('body, html').on('click', '.box_drop_double .children', function(){
				//下一级选项
				var $Item=$(this),
					$Name=$Item.attr('data-name'),
					$Value=$Item.attr('data-value'),
					$Type=$Item.attr('data-type'),
					$Table=$Item.attr('data-table'),
					$Top=$Item.attr('data-top'),
					$Obj=$('.column_rows .box_drop_double');
				$Obj.find('dt input[type=text]').val($Name).parent().find('.hidden_value').val($Value).next().val($Type);
				$Item.parents('dd').hide();
			}).on('click', '.box_drop_double .item', function(){
				//选择下拉选项
				var $This=$(this),
					$Type=$This.attr('data-type');
				$('.url_rows, .pic_rows').hide();
				$('.column_rows .tab_box').hide();
				$('.column_rows .tab_txt:hidden').each(function(){
					$(this).find('.box_input').removeAttr('notnull');
				});
				if($Type=='add'){//自定义
					$('.url_rows').show();
					$('.column_rows .tab_box').show();
					$('.column_rows .tab_txt:hidden').each(function(){
						$(this).find('.box_input').attr('notnull', 'notnull');
					});
				}else if($Type=='products' || $Type=='category'){//产品
					if($('.pic_rows').length) $('.pic_rows').show();
				}
			}).on('keyup', '.box_drop_double dt .box_input', function(e){
				//输入触发
				var $Value=$.trim($(this).val()),
					$Key=window.event?e.keyCode:e.which,
					$Obj=$(this).parents('.box_drop_double');
				if($Key!=13){ //除了回车键
					if($Value.length>2){ //输入内容，自动默认为“新添加”
						$Obj.find('.hidden_value').val($Value);
						$Obj.find('.hidden_type').val('add');
						$('.url_rows').show();
						$('.column_rows .tab_box').show();
						$('.column_rows .tab_txt:hidden').each(function(){
							$(this).find('.box_input').attr('notnull', 'notnull');
						});
					}
				}
			});
		}
	},
	
	themes_nav_init:function(){
		set_obj.themes_nav_global.type='nav';
		set_obj.themes_nav_global.init();
		frame_obj.mouse_click($('.multi_img .upload_btn, .pic_btn .edit'), 'ad', function($this){ //产品颜色图点击事件
			var $id=$this.parents('.multi_img').attr('id'),
				$num=$this.parents('.img').attr('num');
			frame_obj.photo_choice_init($id+' .img[num;'+$num+']', 'ad', 5, 'do_action=products.products_img_del&Model=products');
		});
		//图片上传 开始
		$('.ad_drag').dragsort({
			dragSelector:'.adpic_row',
			dragSelectorExclude:'.ad_info, .upload_file_multi',
			placeHolderTemplate:'<li class="adpic_row placeHolder"></li>',
			scrollSpeed:5
		});
		frame_obj.submit_form_init($('#nav_edit_form'), './?m=set&a=themes&d=nav');
	},
	
	themes_footer_nav_init:function(){
		set_obj.themes_nav_global.type='foot_nav';
		set_obj.themes_nav_global.init();
		frame_obj.submit_form_init($('#nav_edit_form'), './?m=set&a=themes&d=footer_nav');
	},
	
	themes_style_edit_init:function(){
		var obj=$('#style_edit_form');
		obj.delegate('input[name=reset]', 'click', function(){
			global_obj.win_alert(lang_obj.global.reset_confirm, function(){
				$.get('?', "do_action=set.themes_style_reset", function(data){
					if(data.ret!=1){
						global_obj.win_alert(data.msg);
					}else{
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
		});
		$('input[name=NavBgColor]').length && obj.delegate('input[name=NavBgColor]', 'change', function(){$('.NavBgColor').css('background-color', '#'+$(this).val());});
		$('input[name=NavHoverBgColor]').length && $('.NavHoverBgColor').mouseover(function(){$(this).css('background', '#'+$('input[name=NavHoverBgColor]').val());}).mouseleave(function(){$(this).css('background', 'none');});
		$('input[name=NavBorderColor1]').length && obj.delegate('input[name=NavBorderColor1]', 'change', function(){$('.NavBorderColor1').css('border-color', '#'+$(this).val());});
		$('input[name=NavBorderColor2]').length && obj.delegate('input[name=NavBorderColor2]', 'change', function(){$('.NavBorderColor2').css('border-color', '#'+$(this).val());});
		$('input[name=CategoryBgColor]').length && obj.delegate('input[name=CategoryBgColor]', 'change', function(){$('.CategoryBgColor').css('background-color', '#'+$(this).val());});
		frame_obj.submit_form_init($('#style_edit_form'), './?m=set&a=themes&d=style');
	},

	themes_advanced_edit_init:function(){
		frame_obj.submit_form_init($('#adv_edit_form'), './?m=set&a=themes&d=advanced');
	},
	
	shipping_global:{
		init:function(){ //地区设置
			$('.shipping_area_edit').off().on('click','.country_area_title .down',function(){
				if($(this).hasClass('cur')){
					$(this).removeClass('cur').parent().next('.country_area').hide();
				}else{
					$(this).addClass('cur').parent().next('.country_area').show();
				}
			}).on('click','.continent_area .continent .down',function(){
				if($(this).hasClass('cur')){
					$(this).removeClass('cur');
				}else{
					$(this).addClass('cur');
				}
				$(this).parent().next('.country_item').toggle();
			}).on('click', '.continent .input_checkbox_box', function(){
				var $obj=$(this);
				if($obj.hasClass('checked')){
					$obj.parent().next('.country_item').find('.input_checkbox_box').removeClass('checked');
					$obj.parent().next('.country_item').find('.input_checkbox input').removeAttr('checked');
				}else{
					$obj.parent().next('.country_item').find('.input_checkbox_box').addClass('checked');
					$obj.parent().next('.country_item').find('.input_checkbox input').attr('checked','checked');
				}
			});
		},
		weight:function(){
			var WeightBetween=$('#WeightBetween'),
				VolumeBetween=$('#VolumeBetween'),
				WeightArea=$('#WeightArea'), //重量区间的div
				ExtWeightArea=$('#ExtWeightArea'), //续重区间的div
				Weightrow=$('#Weightrow'), //重量区间，只包含输入框的div
				ExtWeightrow=$('#ExtWeightrow'), //只包含输入框的div
				ExtWeight=$('#ExtWeight'), //首重续重div
				Quantity=$('#Quantity'), //按数量div
				VolumeArea=$('#VolumeArea'), //体积区间的div
				Volumerow=$('#Volumerow'), //体积区间，只包含输入框的div
				FirstWeight=$('#FirstWeight'),
				StartWeight_span=$('#StartWeight_span'),
				StartWeight=$('#StartWeight_span input'), //混合计算，区间开始计算的重量
				MinWeight=$('#MinWeight'); //最小重量限制
				MaxWeight=$('#MaxWeight'); //最大重量限制
				MinVolume=$('#MinVolume'); //最小体积限制
				MaxVolume=$('#MaxVolume'); //最大体积限制
				fixed_weight=$('input[name="WeightArea[]"]:first', WeightArea), //重量区间第一个输入框，固定不能修改
				fixed_Extweight=$('input[name="ExtWeightArea[]"]:first', ExtWeightArea); //重量区间第一个输入框，固定不能修改
				fixed_volume=$('input[name="VolumeArea[]"]:first', VolumeArea), //体积区间第一个输入框，固定不能修改
			$('.weightarea_box .item').click(function(){
				$('.weightarea_box .item').removeClass('cur').find('input[name=IsWeightArea]').removeAttr('checked');
				$(this).addClass('cur').find('input[name=IsWeightArea]').attr('checked','checked');
				$('input[name=IsWeightArea]:checked').change();
			});
			$('input[name="IsWeightArea"]').change(function(){
				var val=parseInt($(this).val());
				WeightBetween.hide();
				VolumeBetween.hide();
				StartWeight_span.hide();
				fixed_weight.val(0); //只按照区间计费就必须从0kg开始
				WeightArea.hide();
				ExtWeight.hide();
				ExtWeightArea.hide();
				Quantity.hide();
				VolumeArea.hide();
				MaxWeight.attr({'disabled':false, 'readonly':false});
				MaxVolume.attr({'disabled':false, 'readonly':false});
				if(val!=3){
					WeightBetween.show();
					WeightBetween.find('.box_max').show();
					WeightBetween.find('.box_unlimited').hide();
				}
				switch(val){
					case 1: //区间
						WeightArea.show();
						break;
					case 2: //重量混合计算，从输入的值开始
						StartWeight_span.show();
						fixed_weight.val(StartWeight.val());
						WeightArea.show();
						ExtWeight.show();
						ExtWeightArea.show();
						break;
					case 3: //按数量
						Quantity.show();
						break;
					case 4: //重量体积混合
						WeightBetween.find('.box_max').hide();
						WeightBetween.find('.box_unlimited').show();
						VolumeBetween.show();
						fixed_weight.val(MinWeight.val());
						WeightArea.show();
						VolumeArea.show();
						MaxWeight.attr({'disabled':true, 'readonly':true});
						MaxVolume.attr({'disabled':true, 'readonly':true});
						break;
					default: //首重
						ExtWeight.show();
						ExtWeightArea.show();
						break;
				}
			});
			$('input[name="IsWeightArea"]:checked').change(); //勾选点击
			StartWeight.focus(function(){
				StartWeight.select();
			});
			StartWeight.keyup(function(){
				fixed_weight.val($(this).val());
			});
			//重量区间
			$('#addWeight').click(function(){//新增重量区间输入节点
				var Weightunit=$(this).attr('data-unit');
				Weightrow.append('<div class="row"><span class="unit_input"><input type="text" name="WeightArea[]" value="" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">'+Weightunit+'</b></span><a class="d_del icon_delete_1" href="javascript:;"><i></i></a></div>');
			});
			WeightArea.on('click', '.row a', function(){//删除重量区间输入节点
				$(this).parent().remove();
			});
			//体积区间
			$('#addVolume').click(function(){//新增重量区间输入节点
				var Volumeunit=$(this).attr('data-unit');
				Volumerow.append('<div class="row"><span class="unit_input"><input type="text" name="VolumeArea[]" value="" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">'+Volumeunit+'</b></span><a class="d_del icon_delete_1" href="javascript:;"><i></i></a></div>');
			});
			VolumeArea.on('click', '.row a', function(){//删除重量区间输入节点
				$(this).parent().remove();
			});
			
			FirstWeight.focus(function(){
				FirstWeight.select();
			});
			FirstWeight.keyup(function(){
				fixed_Extweight.val(($(this).val()?parseFloat($(this).val()):0)+.001);
			});
			$('#addExtWeight').click(function(){//新增续重区间输入节点
				var Weightunit=$(this).attr('data-unit');
				ExtWeightrow.append('<div class="row"><span class="unit_input"><input type="text" name="ExtWeightArea[]" value="" class="box_input" size="6" maxlength="10" rel="amount"><b class="last">'+Weightunit+'</b></span><a class="d_del icon_delete_1" href="javascript:;"><i></i></a></div>');
			});
			ExtWeightArea.on('click', '.row a', function(){//删除续重区间输入节点
				$(this).parent().remove();
			});
			
			MinWeight.focus(function(){
				MinWeight.select();
			});
			MinWeight.keyup(function(){
				fixed_weight.val(($(this).val()?parseFloat($(this).val()):0));
			});
			
			MinVolume.focus(function(){
				MinVolume.select();
			});
			MinVolume.keyup(function(){
				fixed_volume.val(($(this).val()?parseFloat($(this).val()):0));
			});
		},
		volume:function(){
			var WeightArea=$('#VolumeArea');//重量区间的div
			var Weightrow=$('#Volumerow');//只包含输入框的div
			$('#addVolume').click(function(){//新增区间输入节点
				var Weightunit=$(this).attr('data-unit');
				Weightrow.append('<div class="row"><span class="unit_input"><input type="text" name="VolumeArea[]" value=" " class="box_input" size="6" maxlength="5" rel="amount"><b class="last">'+Weightunit+'</b></span><a class="d_del icon_delete_1" href="javascript:;"><i></i></a></div>');
			});
			WeightArea.on('click', '.row a', function(){//删除区间输入节点
				$(this).parent().remove();
			});
		},
		country:function(){
			var left_country=$('#left_country');//左边未选择的国家
			var right_country=$('#right_country');//右边已选择的国家
			var country_anti=function($this, obj){
				var ischeck=0,
					continent=0,
					$input=$this.parent().find('.input_anti');
				if($(obj).parent().find('.continent_list').length){
					continent=$(obj).parent().find('.continent_list a.current').attr('continent');
				}
				$(obj).find('.item'+(continent?'[continent='+continent+']':'')+' .select_cid').each(function(index, element){
					if($(element).attr('checked')){
						$(element).attr('checked', false);
					}else{
						$(element).attr('checked', true);
						ischeck=1;
					}
				});
				if(ischeck==1){ //勾选
					$input.attr('checked', true);
				}else{ //取消
					$input.attr('checked', false);
				}
			};
			$('.left_anti').click(function(e){
				country_anti($(this), left_country);
			});
			$('.right_anti').click(function(e){
				country_anti($(this), right_country);
			});
			$('.country_box .item .select_cid').click(function(e){
				e.stopPropagation();
			});
			$('.country_box .item').click(function(e){
				if($('.select_cid', this).attr('checked')){
					$('.select_cid', this).attr('checked', false);
				}else{
					$('.select_cid', this).attr('checked', true);
				}
			});
			$('.btn_add').click(function(){//批量添加按钮
				$('.item .select_cid', left_country).each(function(index, element){
					if($(element).attr('checked')){
						$(element).attr('checked', false);
						right_country.children('.initial_'+$(element).attr('initial')).after($(element).parent('.item'));
					}
				});
				set_obj.shipping_global.country_size();
				$('#edit_form input:submit').click(); //直接提交
			});
			$('.btn_cut').click(function(){//批量删除按钮
				$('.item .select_cid', right_country).each(function(index, element){
					if($(element).attr('checked')){
						$(element).attr('checked', false);
						left_country.children('.initial_'+$(element).attr('initial')).after($(element).parent('.item'));
					}
				});
				set_obj.shipping_global.country_size();
				$('#edit_form input:submit').click(); //直接提交
			});
			var itemH=$('.country_box .item').eq(0).outerHeight(true);//一个item的高度
			$('.initial_left_list a').click(function(){ //点击字母定位事件 左侧
				var val=$(this).attr('initial');
				var obj=$('.initial_'+val, left_country);
				if(obj.length){
					var sTop=obj.prevAll('.item').length*itemH;
					left_country.scrollTop(sTop);
				}
			});
			$('.initial_right_list a').click(function(){ //点击字母定位事件 右侧
				var val=$(this).attr('initial');
				var obj=$('.initial_'+val, right_country);
				if(obj.length){
					var sTop=obj.prevAll('.item').length*itemH;
					right_country.scrollTop(sTop);
				}
			});
			$('.continent_list a').click(function(){ //点击洲筛选国家事件 左侧
				var val=$(this).attr('continent');
				var obj=$('.item[continent='+val+']', left_country);
				if($(this).hasClass('current')){ //取消
					$(this).removeClass('current');
					$('.item', left_country).show();
				}else{ //勾选
					$(this).addClass('current').siblings().removeClass('current');
					obj.show();
					$('.item[continent!='+val+']', left_country).hide().find(':checkbox').attr('checked', false);
				}
			});
			set_obj.shipping_global.country_size();
			set_obj.shipping_global.country_box_size();
			$(window).resize(function(){
				set_obj.shipping_global.country_box_size();
			});
		},
		country_size:function(){
			$('#left_country, #right_country').children('.initial').each(function(){ //首字母js
				if($(this).next().hasClass('item')){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
			$('.initial_left_list a').each(function(){ //定位js
				var val=$(this).attr('initial');
				var obj=$('.initial_'+val+':visible', left_country);
				if(!obj.length){
					$(this).hide();
				}else{
					$(this).show();
				}
			});
			$('.initial_right_list a').each(function(){ //定位js
				var val=$(this).attr('initial');
				var obj=$('.initial_'+val+':visible', right_country);
				if(!obj.length){
					$(this).hide();
				}else{
					$(this).show();
				}
			});
		},
		country_box_size:function(){
			var $menu_h=$('.menu_list').outerHeight();
			$('.country_list td:eq(0), .country_list td:eq(2)').each(function(){
				$(this).find('.country_box').css('height', ($menu_h-$('.shipping_area_title').outerHeight(true)-$(this).find('.country_title').outerHeight()-$(this).find('.continent_list').outerHeight()-$(this).find('.initial_list').outerHeight()-$(this).find('.btn_anti').outerHeight()-2));
			});
		}
	},
	
	shipping_set_edit_init:function(){
		frame_obj.switchery_checkbox();
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=shipping&d=set');
	},
	
	shipping_express_init:function(){
		frame_obj.del_init($('#shipping .r_con_table'));
		// frame_obj.dragsort($('#shipping .config_table_body'), 'set.shipping_express_order', 'div', 'tbody', '<div class="table_item placeHolder"></div>'); //元素拖动
		set_obj.shipping_global.init();
		$('.express_used .switchery').click(function(){
			var $sid=$(this).attr('data-id'),
				$used;
			if($(this).hasClass('checked')){
				$(this).removeClass('checked');
				$used=0;
			}else{
				$(this).addClass('checked');
				$used=1;
			}
			$.post('?do_action=set.shipping_express_used', {'SId':$sid,'IsUsed':$used}, function(){}, 'json');
		});
		frame_obj.fixed_right($('.big_title .insurance_btn'), '.insurance_edit', function($this){
			set_obj.load_edit_form('.insurance_edit', $this.attr('data-url'), 'GET', '', function(){
				set_obj.shipping_insurance_edit_init();
			});
		});
		//管理运费模板
		frame_obj.fixed_right($('#shipping .template_edit_btn'), '.shipping_template_edit');
		$('#shipping_template_edit_form #add_template').click(function(){
			var $Form=$('#shipping_template_edit_form'),
				$Max=parseInt($Form.find('#template_max_number').val()),
				$Html='', $ID='',
				$ID='ADD:'+$Max;
				
			$Html='';
			$Html+=	'<div class="item clean" data-id="'+$ID+'">';
			$Html+=	'<span class="fl input_radio_box">';
				$Html+=	'<span class="input_radio">';
					$Html+=	'<input type="radio" name="IsDefault" value="'+$ID+'" >';
				$Html+=	'</span>';
				$Html+=	lang_obj.manage.global.default;
			$Html+=	'</span>';
			$Html+=		'<input type="text" class="box_input fl" name="Name['+$ID+']" value="" size="30" maxlength="255" autocomplete="off" notnull />';
			$Html+=		'<div class="product_count fl"><span>0</span>个产品</div>';
			$Html+=		'<a href="javascript:;" class="template_delete fl icon_delete_1"><i></i></a>';
			$Html+=		'<input type="hidden" name="IsDelete['+$ID+']" value="0" />';
			$Html+=	'</div>';
			$Form.find('.edit_template_list .input').append($Html);
			$Max++;
			$Form.find('#template_max_number').val($Max);
		});
		$('#shipping_template_edit_form').on('click', '.template_delete', function(){
			$('#shipping_template_edit_form .edit_template_list .item[data-id="'+$(this).parent().attr('data-id')+'"]').hide().find('input[type=hidden]').val(1);
		});
		frame_obj.submit_form_init($('#shipping_template_edit_form'), '', function(){});
	},
	
	shipping_express_edit_init:function(){
		frame_obj.switchery_checkbox();
		//API接口
		$('.api_box .choice_btn').click(function(){
			var $this=$(this),
				$Attr=new Object,
				$Html='';
			$(this).siblings().removeClass('checked').find('input').removeAttr('checked');
			if($this.find('input').val()==0){ //不使用
				$('#method_shipping_box').show();
				$('#method_api_box').hide().find('.input').html('');
			}else{ //使用
				$('#method_shipping_box').hide();
				$('#method_api_box').show();
				$Attr=$.evalJSON($this.attr('data-attribute'));
				typeof $Attr!=='object' && ($Attr=$.evalJSON($Attr)); //返回是字符串，就再转换一次
				for(k in $Attr){ //数据的输出
					$Html+='<span class="unit_input lang_input"><b>'+k+'<div class="arrow"><em></em><i></i></div></b><input type="text" name="Value[]" value="'+global_obj.htmlspecialchars_decode($Attr[k])+'" class="box_input input_name" size="70" maxlength="100" /><input type="hidden" name="Name[]" value="'+k+'" /></span>';
				}
				if($this.attr('data-name')=='DHL'){ //DHL的单项说明
					$Html+='<div class="blank5"></div><p>'+lang_obj.manage.set.dhl_info+'</p>';
				}
				if($this.attr('data-name')=='4PX'){ //目前仅有4PX
					$Html+='<div class="blank5"></div><a href="/plugins/api/'+$this.attr('data-api-name')+'/note/'+$this.attr('data-name')+'.xls" class="download" target="_blank">'+lang_obj.manage.shipping.documentation+'</a>';
				}
				$('#method_api_box').find('.input').html($Html);
				frame_obj.rows_input();
			}
		});
		if($('.api_box input[name=IsAPI]:checked').val()>0){ //如果有勾选，默认点击触发事件
			$('.api_box input[name=IsAPI]:checked').parent().click();
		}

		/* 快递LOGO上传 */
		frame_obj.mouse_click($('#LogoDetail .upload_btn, #LogoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('LogoDetail', 'shipping', 1);
		});
		set_obj.shipping_global.weight();
		/* 表单提交 */
		frame_obj.submit_form_init($('#shipping_express_edit_form'), './?m=set&a=shipping&d=express');
	},
	
	shipping_express_area_init:function(){
		var obj={
			init: function(){
				//海外仓选项切换
				$('#shipping .multi_tab_row>a.btn_multi_tab').off().on('click', function(){
					var $this=$(this);
					$('#shipping .multi_tab_row>a.btn_multi_tab').removeClass('cur');
					$(this).addClass('cur');
					obj.load_edit_form('.shipping_area .shipping_area_list', $this.attr('data-url'), 'GET', '', function(){
						obj.edit();
						$('.shipping_area_edit').html('');
						$('.unit_box').removeClass('show');
						$('#shipping .multi_tab_row>a[data-id='+$this.attr('data-id')+']').addClass('current').siblings().removeClass('current');
					});
				});
				
				//管理海外仓
				frame_obj.fixed_right($('#shipping .multi_tab_row>a.add'), '.shipping_overseas_edit');
				$('#shipping_overseas_edit_form #add_overseas').click(function(){
					var $Form=$('#shipping_overseas_edit_form'),
						$Max=parseInt($Form.find('#overseas_max_number').val()),
						$Html='', $Lang='', $ID='', i=0;
					$Form.find('.edit_overseas_list').find('.tab_txt').each(function(){
						$Lang=$(this).attr('lang');
						$ID='ADD:'+$Max;
						$Html='';
						$Html+=	'<div class="item clean" data-id="'+$ID+'">';
						$Html+=		'<input type="text" class="box_input fl" name="Name['+$ID+']['+$Lang+']" value="" size="30" maxlength="255" autocomplete="off" notnull />';
						$Html+=		'<div class="product_count fl"><span>0</span>个产品</div>';
						$Html+=		'<a href="javascript:;" class="overseas_delete fl icon_delete_1"><i></i></a>';
						$Html+=		(i==0?'<input type="hidden" name="IsDelete['+$ID+']" value="0" />':'');
						$Html+=	'</div>';
						$(this).append($Html);
						++i;
					});
					$Max++;
					$Form.find('#overseas_max_number').val($Max);
				});
				$('#shipping_overseas_edit_form').on('click', '.overseas_delete', function(){
					$('#shipping_overseas_edit_form .edit_overseas_list .item[data-id="'+$(this).parent().attr('data-id')+'"]').hide().find('input[type=hidden]').val(1);
				});
				frame_obj.submit_form_init($('#shipping_overseas_edit_form'), '', function(){
					/*var $langObj=new Object,
						flag=false;
					$('#shipping_overseas_edit_form *[notnull]').each(function(){
						var $lang=$(this).parents('.tab_txt').attr('lang'); //后台语言版
						if(!$langObj[$lang]) $langObj[$lang]=0;
						if(!$(this).find('input').length && $.trim($(this).val())==''){
							if($lang){ //后台语言版
								$langObj[$lang]=1;
							}
							$(this).css('border', '1px solid red').addClass('null');
							flag==false && ($(this).focus());
							flag=true;					
						}else{
							$(this).removeAttr('style').removeClass('null');
						}
					});
					for(k in $langObj){
						if($langObj[k]==1){
							$('#shipping_overseas_edit_form .tab_box_btn[data-lang='+k+']').addClass('empty');
						}else{
							$('#shipping_overseas_edit_form .tab_box_btn[data-lang='+k+']').removeClass('empty');
						}
					}
					//后台语言版
					$('.rows').find('.tab_box_row .tips').hide();
					$('.rows').find('.tab_box_row .tab_box_btn').each(function(){
						if($(this).hasClass('empty')){
							$(this).parents('.tab_box_row').find('.tips').show();
						}
					});
					if(flag){return flag;};*/
				});
			},
			edit: function(){
				frame_obj.del_init($('#shipping .shipping_area')); //删除事件
				//快递区域编辑
				frame_obj.fixed_right($('.shipping_area_list .edit, .shipping_area_list .set_add'), '.shipping_area_edit_box', function($this){
					obj.load_edit_form('.shipping_area_edit', $this.attr('data-url'), 'GET', '', function(){
						frame_obj.rows_input();
						if($this.hasClass('set')){ //地区编辑
							set_obj.shipping_global.country();
							frame_obj.submit_form_init($('#edit_form'), '', '', '', function(){
								$('#edit_form input:submit').attr('disabled', false);
							});
						}else{
							frame_obj.switchery_checkbox(function(obj){
								if(obj.find('input[name=IsFreeShipping]').length){
									obj.parent().next('.unit_input').show();
								}
							}, function(obj){
								if(obj.find('input[name=IsFreeShipping]').length){
									obj.parent().next('.unit_input').hide();
								}
							});
							frame_obj.submit_form_init($('#shipping_area_edit_form'), '', '', '', function(data){
								global_obj.win_alert_auto_close(data.msg.tips, '', 1000, '8%');
								$('#shipping .box_multi_tab a[data-id='+data.msg.OvId+']').click();
								$('#fixed_right .close').click();
							});
							$('.shipping_area_edit .country_item').each(function(){
								if(!$(this).find('.input_checkbox_box input').length) $(this).parent().hide();
							});
							$('.shipping_area_edit .country_item .input_checkbox_box').each(function(){
								if(!$(this).hasClass('checked') && !$(this).hasClass('disabled')){
									$(this).parent().prev('.continent').find('.input_checkbox_box').removeClass('checked').find('input').removeAttr('checked');
								}
							});

							$('.box_select_down').on('mouseleave', function(){
								$list=$(this).find('.list');
								$(this).find('.head').removeClass('selected');
								$list.hide();
							}).on('mouseenter', function(){
								$list=$(this).find('.list');
								$(this).find('.head').addClass('selected');
								$list.show();
							}).on('click', '.list>li', function(){
								$this=$(this);
								$input=$this.parents('.unit_input');
								$list=$input.find('.list');
								$title=$this.text();
								
								if($this.index()==1){ //KG
									$input.find('input[name=FreeShippingPrice]').addClass('hide').attr('disabled', true);
									$input.find('input[name=FreeShippingWeight]').removeClass('hide').attr('disabled', false);
								}else{ //$
									$input.find('input[name=FreeShippingPrice]').removeClass('hide').attr('disabled', false);
									$input.find('input[name=FreeShippingWeight]').addClass('hide').attr('disabled', true);
								}
								$input.find('.head>span').text($title);
								$list.hide();
							});
							
							//分区设置选项切换
							$('#shipping .box_setting .btn_multi_tab').off().on('click', function(){
								var $This=$(this),
									$Type=$This.attr('data-type');
								$This.addClass('current').siblings().removeClass('current');
								$('#shipping .box_setting .box_setting_item[data-type="'+$Type+'"]').show().siblings('.box_setting_item').hide();
							});
						}
					});
				});
			},
			load_edit_form: function(target_obj, url, type, value, callback){
				$.ajax({
					type:type,
					url:url+value,
					success:function(data){
						if(target_obj=='.shipping_area_list'){ //左侧栏目，保留滚动条效果
							//$(target_obj).find('.jspPane').html($(data).find(target_obj).html());
							$(target_obj).html($(data).find(target_obj).html());
							$(target_obj).jScrollPane();
						}else{
							$(target_obj).html($(data).find(target_obj).html());
							jQuery.getScript('/static/js/plugin/tool_tips/tool_tips_shipping.js').done(function(){
								$('.tool_tips_ico').each(function(){ //弹出提示
									$(this).html('&nbsp;');
									$('#shipping').tool_tips($(this), {position:'horizontal', html:$(this).attr('content'), width:260});
								});
							});
						}
						callback && callback(data);
					}
				});
			}
		}
		obj.init();
		obj.edit();
		$('#shipping .multi_tab_row>a:eq(0)').click();
	},
	
	shipping_air_edit_init:function(){
		set_obj.shipping_global.weight();
		set_obj.shipping_global.volume();
		set_obj.shipping_global.init();
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=shipping&d=air');
	},
	
	shipping_air_area_init:function(){
		frame_obj.del_init($('#shipping .shipping_area'));
		var o=$('.shipping_area');
		var load_edit_form=function(target_obj, url, type, value, callback){
			$.ajax({
				type:type,
				url:url+value,
				success:function(data){
					$(target_obj).html($(data).find(target_obj).html());
					callback && callback(data);
				}
			});
		};
		$('.add, .edit, .set').click(function(){
			var $this=$(this);
			load_edit_form('.shipping_area_edit', $(this).attr('data-url'), 'GET', '', function(){
				frame_obj.rows_input();
				if($this.hasClass('set')){ //地区编辑
					set_obj.shipping_global.country();
					frame_obj.submit_form_init($('#edit_form'), '', '', '', function(){
						$('#edit_form input:submit').attr('disabled', false);
					});
				}else{
					frame_obj.switchery_checkbox();
					frame_obj.submit_form_init($('#shipping_area_edit_form'), '', '', '', function(){
						window.location.reload();
						//$('.shipping_area_edit').html('');
					});
				}
			});
		});
	},
	
	shipping_ocean_edit_init:function(){
		set_obj.shipping_global.weight();
		set_obj.shipping_global.volume();
		set_obj.shipping_global.init();
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=shipping&d=ocean');
	},
	
	shipping_ocean_area_init:function(){
		frame_obj.del_init($('#shipping .shipping_area'));
		var o=$('.shipping_area');
		var load_edit_form=function(target_obj, url, type, value, callback){
			$.ajax({
				type:type,
				url:url+value,
				success:function(data){
					$(target_obj).html($(data).find(target_obj).html());
					callback && callback(data);
				}
			});
		};
		$('.add, .edit, .set').click(function(){
			var $this=$(this);
			load_edit_form('.shipping_area_edit', $(this).attr('data-url'), 'GET', '', function(){
				frame_obj.rows_input();
				if($this.hasClass('set')){ //地区编辑
					set_obj.shipping_global.country();
					frame_obj.submit_form_init($('#edit_form'), '', '', '', function(){
						$('#edit_form input:submit').attr('disabled', false);
					});
				}else{
					frame_obj.switchery_checkbox();
					frame_obj.submit_form_init($('#shipping_area_edit_form'), '', '', '', function(){
						window.location.reload();
						//$('.shipping_area_edit').html('');
					});
				}
			});
		});
	},
	
	shipping_insurance_edit_init:function(){
		var InsArea=$('#InsArea');
		var currency=InsArea.attr('currency');
		var tips=InsArea.attr('tips');
		$('#addArea').click(function(){	//增加节点
			InsArea.append('<tr><td nowrap="nowrap"><span class="unit_input" parent_null><b>'+currency+'<div class="arrow"><em></em><i></i></div></b><input type="text" name="ProPrice[]" value="" class="box_input" size="3" maxlength="5" rel="amount" notnull parent_null="1"><b class="last">'+tips+'</b></span></td><td nowrap="nowrap"><span class="unit_input" parent_null><b>'+currency+'<div class="arrow"><em></em><i></i></div></b><input type="text" name="AreaPrice[]" value="" class="box_input" size="3" maxlength="5" rel="amount" notnull parent_null="1"></span><a href="javascript:;" class="icon_delete_1"></a></td></tr>');
        });
		InsArea.on('click', 'tr a', function(){	//删除区间输入节点
            if(InsArea.find('tr').size()>2){
				$(this).parent().parent().remove();
			}
        });
		frame_obj.submit_form_init($('#insurance_edit_form'), '', '', '', function(){
			$('#fixed_right .close').click();
		});
	},
	
	shipping_overseas_init:function(){
		$('#shipping .r_con_table .del').off().on('click', function(){ //海外仓 删除按钮
			var o=$(this);
			global_obj.win_alert(lang_obj.manage.shipping.overseas_del_confirm, function(){
				$.get(o.attr('href'), function(data){
					if(data.ret==1){
						window.location.reload();
					}else{
						global_obj.win_alert(data.msg);
					}
				}, 'json');
			}, 'confirm');
			return false;
		});
		/* 编辑弹出框 */
		$('.r_nav .ico .add, .r_con_table .edit').on('click', function(){
			var $id=parseInt($(this).attr('data-id')),
				$obj=$('.box_overseas_edit');
			frame_obj.pop_form($obj);
			frame_obj.rows_input();
			if($id>=0){ //编辑
				var $data=$.evalJSON($(this).parents('tr').attr('data'));
				$obj.find('.rows:eq(0) input').each(function(){ //名称
					$(this).val($data[$(this).attr('Name')]?global_obj.htmlspecialchars_decode($data[$(this).attr('Name')]):'');
				});
				$('#edit_form input[name=OvId]').val($id); //id
				$('.box_overseas_edit .t>h1>span').text(lang_obj.global.edit); //编辑框标题
			}else{ //添加
				$obj.find('.rows:eq(0) input').each(function(){ //名称
					$(this).val('');
				});
				$('#edit_form input[name=OvId]').val(0); //id
				$('.box_overseas_edit .t>h1>span').text(lang_obj.global.add); //编辑框标题
			}
			return false;
		});
		/* 表单提交 */
		frame_obj.submit_form_init($('#edit_form'), './?m=set&a=shipping&d=overseas');
	},
	
	photo_global:{
		list:function(){//图片银行列表
			$('body, html').on('click', '.bat_open', function(){//全选
				if($('#photo_list_form input:checkbox').not(':checked').length<301){
					$('#photo_list_form input:checkbox').not(':checked').each(function(){
						// $(this).parent('.img').click();
						$(this).attr('checked','checked').parent().parent().addClass('cur');
					});
					$(this).hide();
					$('.un_bat_open').css('display','block');
					$('.list_menu .move,.list_menu .del').css('display','block')
				}else{
					global_obj.win_alert(lang_obj.manage.photo.too_many, 'confirm');
				}
			}).on('click', '.un_bat_open', function(){//全选
				if($('#photo_list_form input:checkbox:checked').length<301){
					$('#photo_list_form input:checkbox:checked').each(function(){
						// $(this).parent('.img').click();
						$(this).removeAttr('checked').parent().parent().removeClass('cur');
					});
					$(this).hide();
					$('.bat_open').css('display','block');
					$('.list_menu .move').css('display','none');
				}else{
					global_obj.win_alert(lang_obj.manage.photo.too_many, 'confirm');
				}
			}).on('click', '.add', function(){//添加
				return false;
			}).on('click', '.del', function(){//删除
				var $this=$(this);
				if(!$('#photo_list_form input:checkbox:checked').length){
					global_obj.win_alert(lang_obj.global.del_dat_select, '', 'confirm');
					return false;
				}else{
					global_obj.win_alert(lang_obj.global.del_confirm, function(){
						$.post('?', $('#photo_list_form').serialize(), function (data){
							if(data.ret!=1){
								global_obj.win_alert(data.msg);
							}else{
								//window.location='./?m=set&a=photo&CateId='+$('#photo_list_form input[name=CateId]').val()+'&IsSystem='+$('#photo_list_form input[name=IsSystem]').val()+'&page='+$('#photo_list_form input[name=Page]').val();
								window.location='./?m=set&a=photo&CateMenu='+$('.search_form select[name=CateMenu] option:selected').val()+'&Keyword='+$('.search_form input[name=Keyword]').val()+'&page='+$('#photo_list_form input[name=Page]').val();
							}
						}, 'json');
					}, 'confirm');
					return false;
				}
			}).on('click', '.clears', function(e){//清空临时文件夹
				var $this=$(this);
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					$.post('?', {do_action:'set.photo_clear_folder'}, function (data){
						if(data.ret==1){
							global_obj.win_alert(lang_obj.manage.photo.empty_temp);
						}
					}, 'json');
				}, 'confirm');
				return false;
			}).on('click', '.photo_list .item .img', function(){//勾选图片框
				var item_parent=$(this).parent('.item'),
					$sort=$('#photo input[name=sort]').val(),
					$val=$(this).find('input').val();
				if(item_parent.hasClass('cur')){
					item_parent.removeClass('cur');
					$(this).find('input').attr('checked', false);
					if($sort && global_obj.in_array($val, $sort.split('|'))){
						$('#photo input[name=sort]').val($sort.replace('|'+$val+'|', '|'));
					}
				}else{
					if($('.photo_list.auto_load_photo').length && $('input[name=type]').val()=='products'){
						var box_id=$('input[name=id]').val().replace(';', '='),
							img_len=parseInt(parent.$('#'+box_id).parents('.multi_img').find('.img.isfile').length),
							maxpic=parseInt($('input[name=maxpic]').val()),
							cur_len=parseInt($('.photo_list_box').find('.item.cur').length);
							if(maxpic==img_len) maxpic++;
							if(maxpic-(img_len+cur_len)<=0){
								global_obj.win_alert_auto_close(lang_obj.manage.account.picture_tips.replace('xxx', parseInt($('input[name=maxpic]').val())));
								return false;
							}
					}
					item_parent.addClass('cur');
					$(this).find('input').attr('checked', true);
					if($sort && !global_obj.in_array($val, $sort.split('|'))){
						$('#photo input[name=sort]').val($sort+$val+'|');
					}
				}
				if($('#photo_list_form input:checkbox:checked').length){
					$('.list_menu .move,.list_menu .del').css('display','block');
				}else{
					$('.list_menu .move').hide();
				}
				return false;
			}).on('click', '.photo_list .item .zoom', function(e){
				e.stopPropagation();
			}).on('click', '.refresh', function(){ //单个移动（已移除）
				frame_obj.pop_iframe_page_init('./?m=set&a=photo&d=move&PId='+$(this).prev().val(), 'user_group');
			}).on('click', '.move', function(){ //批量移动
				var $obj=$('.box_move_edit'),
					$html='';
				$obj.find('input[name=PId], input[name=PId\\[\\]]').remove();
				$('.PIds:checked').each(function(){
					$html+='<input type="hidden" name="PId[]" value="'+$(this).val()+'" />';
				});
				$obj.find('.rows').after($html);
				return false;
			})/*.on('click', '#button_add', function(){
				var save=$('input[name=save]').val(),//保存图片隐藏域ID
					id=$('input[name=id]').val(),//显示元素的ID
					type=$('input[name=type]').val(),//类型
					maxpic=$('input[name=maxpic]').val();//最大允许图片数
				frame_obj.photo_choice_return(id, type, save, maxpic);
			}).on('click', 'input.btn_cancel', function(){
				var not_div_mask=0,
					callback=parent.$('input:hidden.callback').val();
				callback=='not_div_mask=1;' && eval(callback);
				parent.frame_obj.pop_contents_close_init(parent.$('#photo_choice'), 1, not_div_mask);
			})*/;

			frame_obj.fixed_right($('.list_menu .add'), '.box_photo_edit'); //添加
			frame_obj.fixed_right($('.list_menu .move'), '.box_move_edit'); //移动
			var is_animate=0;
			$('.auto_load_photo').scroll(function(){
				var $obj=$('.auto_load_photo'),
					viewHeight=$obj.outerHeight(true), //可见高度  
					contentHeight=$obj.get(0).scrollHeight, //内容高度  
					scrollHeight=$obj.scrollTop(), //滚动高度 
					page=parseInt($obj.attr('data-page')),
					total_pages=parseInt($obj.attr('data-total-pages'));
				if((contentHeight-viewHeight<=scrollHeight) && is_animate==0 && page<=total_pages){ 
					is_animate=1;
					set_obj.load_edit_form('.photo_list_box', $('.photo_list_box').attr('data-page-url'), 'get', '&page='+(page+1), function(){
						$obj.attr('data-page', (page+1));
						is_animate=0;
					}, 'append');
				}  
				if(page>=total_pages && is_animate==0){
					is_animate=1;
				}
				if(page>=total_pages && is_animate==1 && contentHeight-viewHeight==scrollHeight){
					global_obj.win_alert_auto_close(lang_obj.manage.sales.lastpage, 'await', 2000, '8%', 0);
				}
			});
			
	
			//提交
			frame_obj.submit_form_init($('#box_photo_edit'), './?m=set&a=photo');
			frame_obj.submit_form_init($('#move_edit_form'), './?m=set&a=photo');
		}
	},
	
	photo_choice_init:function(){
		frame_obj.category_wrap_page_init();
		var id			= $('input[name=id]').val(),//显示元素的ID
			type		= $('input[name=type]').val(),//类型
			maxpic		= $('input[name=maxpic]').val(),//最大允许图片数
			number		= 0,//执行次数
			fileHiddenObj= $("input[name='file2BigName_hidden_text']"),
			fileName	= "</br>";
			
		$('form[name=upload_form]').fileupload({
			url: '/manage/?do_action=action.file_upload&size='+type,
			acceptFileTypes: /^image\/(gif|jpe?g|png|x-icon|jp2)$/i,
			disableImageResize:false,
			//maxNumberOfFiles: 1,
			imageMaxWidth: 2000,
			imageMaxHeight: 99999,
			imageForceResize:false,
			//maxFileSize: 2000000,
			messages: {
				maxFileSize: lang_obj.manage.photo.size_limit,
			},
			callback: function(imgpath, surplus, name, error){ //上传后的文件/0/原文件名
				function isHasImg(pathImg){
					if(pathImg.length==0){
						return false;
					}
					var isExist=true;
					$.ajax(pathImg, {
						type: 'get',
						async:false,//取消ajax的异步实现
						timeout: 1000,
						success: function() {
						},
						error: function() {
							isExist = false;  
						}
					});
					return isExist;
				}
				if(error){
					fileName+=name+" </br>";
					fileHiddenObj.val(fileName);
					fileHiddenObj.trigger('change');
					frame_obj.photo_choice_return(id, type, maxpic, 1, imgpath, surplus, ++number, name);
				}else if(!isHasImg(imgpath)){
				    var time=setInterval(function(){ //递归检查图片是否已经上传完成
						if(isHasImg(imgpath)){
							frame_obj.photo_choice_return(id, type, maxpic, 1, imgpath, surplus, ++number, name);
							clearInterval(time);
						}
			        },500);
				}else{
					frame_obj.photo_choice_return(id, type, maxpic, 1, imgpath, surplus, ++number, name);
				}
			}
		});
		$('form[name=upload_form]').fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
		set_obj.photo_global.list();
	},
	
	photo_init:function(){
		frame_obj.del_init($('#photo .category'));
		set_obj.photo_global.list();
	},
	
	photo_category_init:function(){
		frame_obj.del_init($('#photo .r_con_table')); //删除事件
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'set.photo_category_del_bat'); //批量删除
		/* 批量排序 */
		$('#photo .r_con_table .myorder_select').on('dblclick', function(){
			var $obj=$(this),
				$number=$obj.attr('data-num'),
				$CateId=$obj.parents('tr').find('td:eq(0)>input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#myorder_select_hide').html(),
				$val;
			$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
			$number && $obj.find('select').val($number).focus();
			$obj.find('select').on('blur', function(){
				$val=$(this).val();
				if($val!=$number){
					$.post('?', 'do_action=set.photo_category_edit_myorder&Id='+$CateId+'&Number='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.html(data.msg);
							$obj.attr('data-num', $val);
						}
					}, 'json');
				}else{
					$obj.html($obj.find('span').html());
				}
			});
		});
		frame_obj.dragsort($('#photo .r_con_table tbody'), 'set.photo_category_edit_myorder', 'tr', 'a, td[data!=move_myorder], dl', '<tr class="placeHolder"></tr>'); //元素拖动
		frame_obj.dragsort($('#photo .attr_list'), 'set.photo_category_order', 'dl', 'a, dd[class!=attr_ico]', '<dl class="attr_box placeHolder"></dl>'); //元素拖动
		$('#photo .attr_box').hover(function(){
			$(this).children('.attr_menu').stop(true, true).animate({'right':0}, 200);
		}, function(){
			$(this).children('.attr_menu').stop(true, true).animate({'right':-51}, 200);
		});
		/* 编辑弹出框 */
		frame_obj.fixed_right($('.list_menu .add, .attr_add .add, .r_con_table .edit'),'.box_photo_category_edit');//添加
		$('.list_menu .add, .attr_add .add, .r_con_table .edit').on('click', function(){
			var $id=parseInt($(this).attr('data-id')),
				$obj=$('.box_photo_category_edit');
			if($id){ //编辑
				var $data='';
				if($(this).parent().hasClass('attr_menu')){
					$data=$.evalJSON($(this).parents('dl.attr_box').attr('data'));
				}else{
					$data=$.evalJSON($(this).parents('tr').attr('data'));
				}
				$('#box_photo_category_edit input[name=Category]').val($data['Category']); //分类名称
				$.post('?', {do_action:'set.photo_category_select', 'ParentId':0, 'CateId':$id}, function(data){
					$('#box_photo_category_edit .rows:eq(1) .input').html(data); //分类所属
					$('#box_photo_category_edit input[name=UnderTheCateId]').parents('.down_select_box').find('.select[value='+$data['TopCateId']+']').click();
				});
				$('#box_photo_category_edit input[name=CateId]').val($id); //id
				$('.box_photo_category_edit .top_title>span').text(lang_obj.global.edit); //编辑框标题
			}else{ //添加
				var $ParentId=0;
				if($(this).parent().hasClass('attr_add')){
					$ParentId=$(this).parents('tr').attr('data-id');
				}
				$('#box_photo_category_edit input[name=Category]').val(''); //分类名称
				$.post('?', {do_action:'set.photo_category_select', 'ParentId':$ParentId, 'CateId':0}, function(data){
					$('#box_photo_category_edit .rows:eq(1) .input').html(data); //分类所属
					$('#box_photo_category_edit select[name=UnderTheCateId]').val($ParentId);
					$('#box_photo_category_edit input[name=UnderTheCateId]').parents('.down_select_box').find('.select[value='+$ParentId+']').click();
				});
				$('#box_photo_category_edit input[name=CateId]').val(0); //id
				$('.box_photo_category_edit .top_title>span').text(lang_obj.global.add); //编辑框标题
			}
			return false;
		});
		//提交
		frame_obj.submit_form_init($('#box_photo_category_edit'), './?m=set&a=photo&d=category');
	},
	
	photo_upload_init:function(){
		/*
		var callback=function(imgpath, surplus, name){
			if($('#PicDetail .pic').size()>=20){
				global_obj.win_alert(lang_obj.manage.account.picture_tips.replace('xxx', 20));
				return;
			}
			$('#PicDetail').append('<div class="pic"><div>'+frame_obj.upload_img_detail(imgpath)+'<span>'+lang_obj.global.del+'</span><input type="hidden" name="PicPath[]" value="'+imgpath+'" /></div><input type="text" maxlength="30" class="box_input" value="'+name+'" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" notnull></div>');
			$('#PicDetail div span').off('click').on('click', function(){
				var $this=$(this);
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					$.ajax({
						url:'./?m=set&a=photo&do_action=set.photo_upload_del&Path='+$this.prev().attr('href')+'&Index='+$this.parent().index(),
						success:function(data){
							json=eval('('+data+')');
							$('#PicDetail div:eq('+json.msg[0]+')').remove();
						}
					});
				}, 'confirm');
				return false;
			});
		};
		frame_obj.file_upload($('#PicUpload'), '', '', '', true, 20, callback, '', 'set.photo_file_upload');
		*/
		$('form[name=upload_form]').fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=photo',
			acceptFileTypes: /^image\/(gif|jpe?g|png|x-icon|jp2)$/i,
			disableImageResize:false,
			imageMaxWidth: 2000,
			imageMaxHeight: 99999,
			imageForceResize:false,
			maxFileSize: 2000000,
			maxNumberOfFiles: 20,
			messages: {
				maxFileSize: lang_obj.manage.photo.size_limit,
				maxNumberOfFiles: lang_obj.manage.account.picture_tips.replace('xxx', 20),
			},
			callback: function(imgpath, surplus, name){
				if($('#PicDetail .pic').size()>=20){
					global_obj.win_alert(lang_obj.manage.account.picture_tips.replace('xxx', 20));
					return;
				}
				$('#PicDetail').append('<div class="item">'+frame_obj.upload_img_detail(imgpath)+'<span>'+lang_obj.global.del+'</span><input type="hidden" name="PicPath[]" value="'+imgpath+'" /><input type="text" maxlength="30" class="box_input" value="'+name+'" name="Name[]" placeholder="'+lang_obj.global.picture_name+'" notnull></div>');
				$('#PicDetail div span').off('click').on('click', function(){
					var $this=$(this);
					global_obj.win_alert(lang_obj.global.del_confirm, function(){
						$.ajax({
							url:'./?m=set&a=photo&do_action=set.photo_upload_del&Path='+$this.prev().children('img').attr('src')+'&Index='+$this.parent().parent().index(),
							success:function(data){
								$this.parents('.item').remove();
								// json=eval('('+data+')');
								// $('#PicDetail .pic:eq('+json.msg[0]+')').remove();
							}
						});
					}, 'confirm', 1);
					if($('#box_photo_edit').length) global_obj.div_mask(1);
					return false;
				});
			}
		});
		$('form[name=upload_form]').fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
	},
	
	/**************************************************平台授权(start)**************************************************/
	authorization_init:function(){
		//添加授权弹出框
		var box_authorization_add = $('.box_authorization_add');
		$('.r_nav').on('click', '.add', function(){//添加
			frame_obj.pop_form(box_authorization_add);
			$('.r_con_form input', box_authorization_add).val('').removeAttr('style');
			$('.r_con_form select', box_authorization_add).removeAttr('style').find('option').removeAttr('selected').first().attr('selected', 'selected');
			$('.box_authorization_add input[name=do_action]').val('set.authorization_add');
		});

		//删除授权
		$('#authorization').on('click', '.del', function (){
			if(!confirm(lang_obj.global.del_confirm)){return false;}
			
			var $this=$(this),
				$AId=$this.parent().parent().attr('aid');
			
			$.post('./', 'do_action=set.authorization_del&AId='+$AId, function(data){
				if(data.ret==1){
					$this.parent().parent().remove();
					return false;
				}
			},'json');
		});
	},
	
	open_init:function(){//开放接口设置
		function set_open_api(){
			$.post('./', 'do_action=set.set_open_api', function(data){
				if(data.ret==1){
					if(data.msg.jump==1){
						window.top.location=window.top.location.href;
					}else{
						$('#authorization .open_api .appkey').text(data.msg.appkey);
					}
				}else{
					global_obj.win_alert('操作失败！');
				}
			},'json');
		}
		
		$('#authorization .open_api').delegate('.enable', 'click', function(){
			if($(this).hasClass('add')){
				set_open_api();
			}else{
				if(confirm(lang_obj.manage.set.open_api_refresh)) set_open_api();
			}
		});
		
		$('#authorization .open_api').delegate('.del', 'click', function(){
			if(!confirm(lang_obj.global.del_confirm)){return false;}
			
			$.post('./', 'do_action=set.del_open_api', function(data){
				if(data.ret==1){
					window.top.location=window.top.location.href;
				}
			},'json');
		});
	},
	
	aliexpress_init:function(){//速卖通授权部分
		//添加授权提交
		$('.box_authorization_add form').submit(function(){
			var $this=$(this),
				$Name=$this.find('input[name=Name]').val();
			if(global_obj.check_form($this.find('*[notnull]'), $this.find('*[format]'), 1)){return false;};
			
			var wi = window.open('about:blank', '_blank');
			$.post('./', 'do_action=set.authhz_url&Name='+$Name+'&d=aliexpress', function(data){
				if(data.ret==1){
					wi.location.href=data.msg.url;
					return false;
				}
			},'json');
			return false;
		});
		
		//店铺重新授权
		$('#authorization').on('click', '.refresh', function (){
			var $account=$(this).parent().parent().attr('account');
			
			var wi = window.open('about:blank', '_blank');
			$.post('./', 'do_action=set.authhz_url&Account='+$account+'&d=aliexpress', function(data){
				if(data.ret==1){
					wi.location.href=data.msg.url;
					return false;
				}
			},'json');
			return false;
		});
		
		//编辑店铺名称
		var box_authorization_edit = $('.box_authorization_edit');
		$('#authorization').on('click', '.mod', function (){//修改
			var AId=$(this).parent().parent().attr('aid'),
				Name=$(this).parent().siblings('td.name').text();
				
			frame_obj.pop_form(box_authorization_edit);
			$('input[type=submit]', box_authorization_edit).attr('disabled', false);
			$('input[name=Name]', box_authorization_edit).val(Name);
			$('input[name=AId]', box_authorization_edit).val(AId);
			$('input[name=d]', box_authorization_edit).val('aliexpress');
			$('input[name=do_action]', box_authorization_edit).val('set.authorization_edit');
		});
		$('.box_authorization_edit form').submit(function(){
			var $this=$(this);
			var $Name=$this.find('input[name=Name]').val(),
				$AId=$this.find('input[name=AId]').val();
			
			if(global_obj.check_form($this.find('*[notnull]'), $this.find('*[format]'), 1)){return false;};
	
			$.post('./', 'do_action=set.authorization_edit&Name='+$Name+'&AId='+$AId+'&d=aliexpress', function(data){
				if(data.ret==1){
					var obj=$('#authorization').find('tr[aid='+data.msg.AId+']');
					obj.find('td.name').text(data.msg.Name);
					
					frame_obj.pop_form($('.box_authorization_edit'), 1);
				}else{
					global_obj.win_alert(data.msg);
				}
			},'json');
			
			return false;
		});
	},
	
	amazon_init:function(){//亚马逊授权
		$('.pop_form form select[name=MarkectPlace]').change(function(){
			if($(this).val())
				$(this).siblings('a').attr({'href':$(this).find('option:selected').attr('data-url'),'target':'_blank'});
			else
				$(this).siblings('a').attr('href','javascript:;').removeAttr('target');
		});
		$('.box_authorization_add form').submit(function(){
			if(global_obj.check_form($(this).find('*[notnull]'), $(this).find('*[format]'), 1)){return false;};
			
			$(this).find('input:submit').attr('disabled', 'disabled');
			$.post('./', $(this).serialize(), function(data){
				$('.box_authorization_add form input:submit').removeAttr('disabled');
				if(data.ret==1){
					window.location.href=window.location.href;
				}else{
					alert(data.msg);
				}
			},'json');
			return false;
		});
		
		//店铺重新授权
		$('#authorization').on('click', '.refresh', function (){
			var $account=$(this).parent().parent().attr('account');
			
			$.post('./', 'do_action=set.authhz_url&Account='+$account+'&d=amazon', function(data){
				if(data.ret==1){
					window.location.href=window.location.href;
				}
			},'json');
			return false;
		});
		
		
		//编辑店铺名称
		var box_authorization_edit = $('.box_authorization_edit');
		$('#authorization').on('click', '.mod,.refresh', function (){//修改
			var AId=$(this).parent().parent().attr('aid'),
				Name=$(this).parent().siblings('td.name').text(),
				account=jQuery.parseJSON(global_obj.htmlspecialchars_decode($(this).parent().parent().attr('account')));
				
			frame_obj.pop_form(box_authorization_edit);
			$('input[type=submit]', box_authorization_edit).attr('disabled', false);
			$('input[name=Name]', box_authorization_edit).val(Name);
			$('select[name=MarkectPlace]', box_authorization_edit).find('option[value='+account.MarkectPlace+']').attr('selected', 'selected');
			$('input[name=MerchantId]', box_authorization_edit).val(account.MerchantId).addClass('bg_gray');
			$('input[name=AWSAccessKeyId]', box_authorization_edit).val(account.AWSAccessKeyId).addClass('bg_gray');
			$('input[name=SecretKey]', box_authorization_edit).val(account.SecretKey).addClass('bg_gray');
			$('input[name=AId]', box_authorization_edit).val(AId);
			$('input[name=d]', box_authorization_edit).val('amazon');
			$('input[name=do_action]', box_authorization_edit).val('set.authorization_edit');
			
			var method='';
			if($(this).hasClass('refresh')) method='refresh';
			$('input[name=method]', box_authorization_edit).val(method);
			
			var url=$('select[name=MarkectPlace]', box_authorization_edit).find('option[value='+account.MarkectPlace+']').attr('data-url');
			$('a.amazon_url', box_authorization_edit).attr({'href':url, 'target':'_blank'});
		});
		$('.box_authorization_edit form').submit(function(){
			var $this=$(this),
				$Name=$this.find('input[name=Name]').val(),
				$MarkectPlace=$this.find('select[name=MarkectPlace]').val(),
				$AId=$this.find('input[name=AId]').val();
			
			if(global_obj.check_form($this.find('*[notnull]'), $this.find('*[format]'), 1)){return false;};
	
			$.post('./', $(this).serialize(), function(data){
				if(data.ret==1){
					var obj=$('#authorization').find('tr[aid='+data.msg.AId+']');
					obj.find('td.name').text(data.msg.Name);
					data.msg.token && obj.attr('account', data.msg.token);
					
					frame_obj.pop_form($('.box_authorization_edit'), 1);
				}else{
					global_obj.win_alert(data.msg, '', '', 1);
				}
			},'json');
			
			return false;
		});
	},
	/**************************************************平台授权(end)**************************************************/
	/**************************************************第三方代码(start)**************************************************/
	seo_third_init:function(){
		frame_obj.del_init($('#third .config_table')); //删除事件
		$('.used_checkbox .switchery').click(function(){
			var $this=$(this),
				$tid=$this.attr('data-tid'),
				$used;
			if($this.hasClass('checked')){
				$used=0;
				$this.removeClass('checked');
			}else{
				$used=1;
				$this.addClass('checked');
			}
			$.post('?do_action=set.seo_third_used',{'TId':$tid,'IsUsed':$used},function(data){},'json');
		});
	},
	
	seo_third_edit_init:function(){
		frame_obj.submit_form_init($('#third_edit_form'), './?m=set&a=third');
		//meta标签和body标签
		$('input[name=IsMeta], input[name=IsBody]').parent().parent('.input_checkbox_box').on('click', function(){
			var _this=$(this);
			var name=_this.find('input').attr('name');
			setTimeout(function(){  //延迟执行，防止与开关按钮冲突
				if(name=='IsMeta' && _this.hasClass('checked') && $('input[name=IsBody]').parent().parent('.input_checkbox_box').hasClass('checked')){
					$('input[name=IsBody]').parent().parent('.input_checkbox_box').trigger('click');
				}else if(name=='IsBody' && _this.hasClass('checked') && $('input[name=IsMeta]').parent().parent('.input_checkbox_box').hasClass('checked')){
					$('input[name=IsMeta]').parent().parent('.input_checkbox_box').trigger('click');
				}
			}, 50);
		});
	},
	/**************************************************第三方代码(end)**************************************************/
	load_edit_form: function(target_obj, url, type, value, callback, fuc){
		$.ajax({
			type:type,
			url:url+value,
			success:function(data){
				if(fuc=='append'){
					$(target_obj).append($(data).find(target_obj).html());
				}else{
					$(target_obj).html($(data).find(target_obj).html());
				}
				callback && callback(data);
			}
		});
	},
	
	manage_init:function(){
		frame_obj.del_init($('#manage .r_con_table'));
		
		frame_obj.switchery_checkbox(function(obj){
			var $u=obj.find('input').attr('data-id');
			$.post('?', {'do_action':'set.manage_locked', 'u':$u, 'Locked':0}, function(data){
				if(data.ret==1){
					obj.find('input').attr('data-id', data.msg);
					obj.parent().siblings('.operation').find('>a').attr('href', './?m=set&a=manage&d=edit&u='+data.msg);
					global_obj.win_alert_auto_close(lang_obj.manage.global.can_log, '', 1000, '8%');
				}
			}, 'json');
		}, function(obj){
			var $u=obj.find('input').attr('data-id');
			$.post('?', {'do_action':'set.manage_locked', 'u':$u, 'Locked':1}, function(data){
				if(data.ret==1){
					obj.find('input').attr('data-id', data.msg);
					obj.parent().siblings('.operation').find('>a').attr('href', './?m=set&a=manage&d=edit&u='+data.msg);
					global_obj.win_alert_auto_close(lang_obj.manage.global.cant_log, '', 1000, '8%');
				}
			}, 'json');
		});
		
		//权限事件
		frame_obj.fixed_right($('.btn_permit'), '.fixed_permit');
		$('#permit_form').submit(function(){ return false; });
		$('#permit_form .btn_submit').click(function(){
			$('#permit_form .btn_cancel').click();
		});
		
		//权限勾选
		$('#PermitBox :checkbox').click(function(){
			var $obj,
				$this=$(this),
				$myId=$this.attr('id'),//自己的位置
				$checked=$this.attr('checked'),//勾选状态
				$position='';//当前位置
			if($checked=='checked'){
				if($myId=='selected_all'){
					$("#PermitBox :checkbox:visible").each(function(){
						this.checked=true;
					});
				}else{
					$position=$myId.split('_');
					if($position.length>2){
						set_obj.permit_checked($position);
					}
					if($this.parent()[0].tagName=='DIV'){//当前为一级
						$this.parent().find(':checkbox').attr('checked', true);
					}else if($this.parent()[0].tagName=='DT'){//当前为二级
						$this.parent().siblings('dd').find(':checkbox').attr('checked', true);
					}else if($this.parent()[0].tagName=='DD'){//当前为三级
						$this.next().next().find(':checkbox').attr('checked', true);
					}
					if($("#PermitBox .module :checkbox:visible:checked").size()==$("#PermitBox .module :checkbox:visible").size()){
						$('#selected_all').attr('checked', true);
					}else{
						$('#selected_all').removeAttr('checked');
					};
				}
			}else{
				if($myId=='selected_all'){
					$("#PermitBox :checkbox").each(function(){
						this.checked=false;
					});
				}else{
					if($this.parent()[0].tagName=='DIV'){//当前为一级
						$this.parent().find(':checkbox').attr('checked', false);
					}else if($this.parent()[0].tagName=='DT'){//当前为二级
						$this.parent().siblings('dd').find(':checkbox').attr('checked', false);
					}else if($this.parent()[0].tagName=='DD'){//当前为三级
						$this.next().next().find(':checkbox').attr('checked', false);
					}
					if($("#PermitBox .module :checkbox:visible:checked").size()==$("#PermitBox .module :checkbox:visible").size()){
						$('#selected_all').attr('checked', true);
					}else{
						$('#selected_all').removeAttr('checked');
					};
				}
			}
		});
		
		//选择用户组
		$('#manage_edit_form').delegate('select[name=GroupId]', 'change', function(){
			$('#PermitBox').css('display', $(this).val()==1?'none':'block');
			$('#manage_edit_form .btn_permit').css('display', $(this).val()==1?'none':'inline-block');
			if($(this).val()==3){
				$('#PermitBox .module').each(function(){
					if($(this).children('input').attr('id')!='Permit_orders' && $(this).children('input').attr('id')!='Permit_user'){
						$(this).hide().find(':checkbox').attr('checked', false);
					}else{
						$(this).show();
					}
				});
			}else{
				$('#PermitBox .module').show();
			}
		});
		
		//数据提交
		$('#manage_edit_form').submit(function(){ return false; });
		$('#manage_edit_form .btn_submit').click(function(){
			var $Form=$('#manage_edit_form'),
				$Stop=0;
			$Form.find('*[notnull]').each(function(){
				if($.trim($(this).val())==''){
					$(this).css('border-color', 'red');
					$Stop+=1;
				}
			});
			if($Stop>0){
				return false;
			}
			$.post('?', $Form.serialize()+'&'+$('#permit_form').serialize(), function(data){
				if(data.ret==1){
					window.location='./?m=set&a=manage';
				}else{
					global_obj.win_alert_auto_close(data.msg, 'fail', 1000, '8%');
				}
			}, 'json');
		});
	},
	
	permit_checked:function(obj){
		obj.pop();//去掉最后一个元素
		var sObj=obj.join("_");
		$('#'+sObj).attr('checked', true);
		if(obj.length>2){
			set_obj.permit_checked(obj);
		}
	}
}