/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var sales_obj={
	//--------- 公共 start ----------//
	sales_global:{
		del_action:'',
		order_action:'',
		init:function(){
			frame_obj.del_init($('#sales .r_con_table')); //删除提示
			frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button').find('.del, .edit')); //批量操作
			frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), sales_obj.sales_global.del_action); //批量删除
			$('#sales .r_con_table .myorder_select').on('dblclick', function(){
				var $obj=$(this),
					$number=$obj.attr('data-num'),
					$Id=$obj.parents('tr').find('td:eq(0) input').val(),
					$mHtml=$obj.html(),
					$sHtml=$('#myorder_select_hide').html(),
					$val;
				$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
				$number && $obj.find('select').val($number).focus();
				$obj.find('select').on('blur', function(){
					$val=$(this).val();
					if($val!=$number){
						$.post('?', 'do_action='+sales_obj.sales_global.order_action+'&Id='+$Id+'&Number='+$(this).val(), function(data){
							if(data.ret==1){
								$obj.html(data.msg);
								$obj.attr('data-num', $val);
							}
						}, 'json');
					}else{
						$obj.html($obj.find('span').html());
					}
				});
			});
		}
	},
	
	package_main_id:$('#proid_hide').val()?$('#proid_hide').val():0, //用于保存主产品区域的产品id
	package_assist_id:$('#packageproid_hide').val()?$('#packageproid_hide').val().split('|'):'', //用于保存捆绑产品区域的产品id
	package_edit_init:function(){
		/*********************************************** 新版效果 ***********************************************/
			var is_main=parseInt($('#is_main').val());//判断是否为组合产品系列
			frame_obj.fixed_right($('.date_rows .set_add'),'.fixed_add_seles');
			frame_obj.submit_form_init($('#edit_form'), $('#edit_form').attr('data-return'));
			$('.add_sales_list').on('click','.add_sales_item',function(){
				var $obj=$(this),
					$proid=$obj.attr('data-proid');
				if($obj.hasClass('cur')){
					if(is_main && $('#proid_hide').val()==$proid){
						$('#proid_hide').val('');
						$('.set_sales_list').find('.set_sales_item[data-proid='+$proid+']').find('.main').removeClass('p_main');
					} 
					$('#packageproid_hide').val($('#packageproid_hide').val().replace($proid+'|', ''));
					$obj.removeClass('cur').append($('.set_sales_list').find('.set_sales_item[data-proid='+$proid+']'));
				}else{
					if(parseInt($('.set_sales_list').attr('max')) <= $('.set_sales_list .set_sales_item').length){ //节日模板限制个数
						global_obj.win_alert(lang_obj.manage.sales.beyondNumber+$('.set_sales_list').attr('max'));
						return false;
					}
					if(is_main && !$('.set_sales_list .main.p_main').length){
						$obj.find('.set_sales_item .main').addClass('p_main');
						$('#proid_hide').val($proid);
					}else{
						if($('#packageproid_hide').val()){
							$('#packageproid_hide')[0].value+=$proid+'|';
						}else{
							$('#packageproid_hide').val('|'+$proid+'|');
						}
					}
					$obj.addClass('cur');
					$('.set_sales_list').append($obj.find('.set_sales_item'));
					if($('#set_sales_list').length){
						frame_obj.Waterfall('set_sales_list', $('.set_sales_item').width()+8, parseInt($('#set_sales_list').width()/($('.set_sales_item').width()+8)), 'set_sales_item'); //瀑布流
					}
				}
				$(window).resize();
			});
			$('#sales').on('click', '.set_sales_item .del', function(){
				var $obj=$(this),
					$proid=$obj.parents('.set_sales_item').attr('data-proid');
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					if(is_main && $('#proid_hide').val()==$proid){
						$('#proid_hide').val('');
						$obj.parents('.set_sales_item').find('.main').removeClass('p_main');
					} 
					if($('.add_sales_list').find('.add_sales_item[data-proid='+$proid+']').length){
						$('.add_sales_list').find('.add_sales_item[data-proid='+$proid+']').removeClass('cur').append($obj.parents('.set_sales_item'));
					}else{
						$obj.parents('.set_sales_item').remove();
					}
					$('#packageproid_hide').val($('#packageproid_hide').val().replace($proid+'|', ''));
					$(window).resize();
				},'confirm');
			}).on('click','.set_sales_list .main',function(){
				var $obj=$(this),
					$proid=$obj.parents('.set_sales_item').attr('data-proid'),
					$old_proid=parseInt($('#proid_hide').val());
					if($old_proid){
						$('#packageproid_hide')[0].value+=$old_proid+'|';
						$('#packageproid_hide').val($('#packageproid_hide').val().replace($proid+'|', ''));

					}else{
						$('#packageproid_hide').val($('#packageproid_hide').val().replace($proid+'|', ''));
					}
					$('.set_sales_list').find('.main').removeClass('p_main');
					$obj.addClass('p_main')
					$('#proid_hide').val($proid);
			}).on('mouseover','.attr_list .more',function(){
				$(this).hide().parent().addClass('cur');
			}).on('mouseleave ','.set_sales_item',function(){
				$(this).find('.attr_abs').removeClass('cur').find('.more').show();
			});
			/*右侧产品滚动加载*/
			var is_animate=0;
			$('#fixed_right').scroll(function(){
				var viewHeight=$('#fixed_right').height(),//可见高度  
					contentHeight=$("#fixed_right").get(0).scrollHeight,//内容高度  
					scrollHeight=$('#fixed_right').scrollTop(),//滚动高度 
					page=parseInt($('.add_sales_list').attr('data-page')),
					total_pages=parseInt($('.add_sales_list').attr('data-total-pages'));
				if((contentHeight - viewHeight <= scrollHeight)&&is_animate==0&&page<=total_pages){ 
					is_animate=1;
					sales_obj.load_edit_form('.add_sales_list','./?'+$('.search_form form').serialize()+'&remove_pid='+$('#packageproid_hide').val(),'get','&page='+(page+1),function(){
						$('.add_sales_list').attr('data-page',(page+1));
						is_animate=0;
					},'append');
				}  
				if(page>=total_pages&&is_animate==0){
					global_obj.win_alert_auto_close(lang_obj.manage.sales.lastpage, 'await', 2000, '8%', 0);
					is_animate=1;
				}
			});
			$('#fixed_right .search_form form').submit(function(){
				sales_obj.load_edit_form('.add_sales_list','./?'+$(this).serialize()+'&remove_pid='+$('#packageproid_hide').val()+(is_main && $('#proid_hide').val() ? $('#proid_hide').val()+'|' : ''),'get','',function(data){
					is_animate=0;
					$('.add_sales_list').attr('data-total-pages',$(data).find('.add_sales_list').attr('data-total-pages')).attr('data-page',1);
				});
				return false;
			});
			if($('#set_sales_list').length){
				window.onload = function(){
					$(window).resize(function(){
						frame_obj.Waterfall('set_sales_list', $('.set_sales_item').width()+8, parseInt($('#set_sales_list').width()/($('.set_sales_item').width()+8)), 'set_sales_item');// 瀑布流
					}).resize();
				}
			}
		/*********************************************** 新版效果 ***********************************************/
		sales_obj.package_resize();	//设置背景高度
		$(window).resize(sales_obj.package_resize);//设置背景高度
		var type_id=parseInt($('#type_hide').val());//判断(组合购买/组合促销)或者(秒杀/团购/节日模板)
		if((!is_main && type_id==3)||!is_main && !type_id || !is_main && type_id==1) $('.start_time').daterangepicker({showDropdowns:true});
		
		$('.product_item select').on('change', function(){
			$(this).children('option[value='+$(this).val()+']').attr('selected', true).siblings().attr('selected', false);
		});
		
		frame_obj.switchery_checkbox();
	},
	package_resize:function(){
		var h=$('#main').height()-$('.r_nav').outerHeight()-60,oh;
		$('#main .lefter, #main .list_box_righter').height(h);
		oh = $('#main .lefter .p_title').outerHeight(true)*$('#main .lefter .p_title').length+$('#main .lefter .rows').outerHeight(true)*$('#main .lefter .rows').length;
		if(parseInt($('#is_main').val())){//判断是否为组合产品系列
			$('#main .lefter .p_related_frame').height(h-oh);
		}else{
			$('#main .lefter .p_related_frame').height(h-oh);
		}
		$('#main .list_box_righter .product_frame').height(h-$('#main .list_box_righter .p_title').outerHeight(true));
	},
	
	//--------- 公共 end ----------//
	
	//--------- 节日模板 start ----------//
	holiday_list_init:function(){
		$('#holiday .list_bd .item').hover(function(){
			$(this).children('.info').stop(true, true).slideDown(500);
		},function(){
			$(this).children('.info').stop(true, true).slideUp(500);
		}).click(function(){
			if(!$(this).hasClass('current')){
				var $this=$(this);
				global_obj.win_alert(lang_obj.manage.module.sure_module, function(){
					$this.addClass('current').siblings().removeClass('current');
					$.post('?', "do_action=sales.holiday_select&HId="+$this.attr('hid'), function(data){
						if(data.ret!=1){
							global_obj.win_alert(data.msg);
						}else{
							window.location.reload();
						}
					}, 'json');
				}, 'confirm');
				return false;
			}
		}).find('.info').click(function(e){
			e.stopPropagation();
		});
		
	},
	
	holiday_frame_init:function(){
		$('#holiday .m_righter').width($('.r_con_wrap').width()-$('#holiday .m_lefter').width()-100);
	},
	
	holiday_edit_init:function(){
		//加载上传按钮

		frame_obj.mouse_click($('#PicDetail .upload_btn, .upload_pic .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});

		//加载版面内容
		for(i=0; i<web_template_data.length; i++){
			var obj=$("#web_template div").filter('[rel=edit-'+web_template_data[i]['Postion']+']');
			obj.attr('no', i);
			if(web_template_data[i]['NeedLink']==1){
				obj.find('.text').html('<a href="">'+(web_template_data[i]['ContentsType']==1?web_template_data[i]['Title'][0]:web_template_data[i]['Title'])+'</a>');
			}else{
				obj.find('.text').html((web_template_data[i]['ContentsType']==1?web_template_data[i]['Title'][0]:web_template_data[i]['Title']));
			}
			obj.find('.img').html('<img src="'+(web_template_data[i]['ContentsType']==1?web_template_data[i]['PicPath'][0]:web_template_data[i]['PicPath'])+'" />');
		}
		$('.template_box').append('<div class="mod">&nbsp;</div>');	//追加编辑按钮
		$('.template_box').hover(function(){$(this).find('.mod').show();}, function(){$(this).find('.mod').hide();});
		
		//切换编辑内容
		$('#web_template .mod').click(function(){
			var parent=$(this).parent();
			var no=parent.attr('no');
			
			$('.current_box').remove();
			parent.append("<div class='current_box'></div>");
			$('.current_box').css({'height':parent.height()-10, 'width':parent.width()-10})
			
			$("#set_banner, #set_config, #set_config>.rows").hide();
			$("#holiday_form input").removeAttr('notnull');
			
			if(web_template_data[no]['ContentsType']==1){
				$("#set_banner").show();
				var len=parseInt(web_template_data[no]['ListNum']);
				var dataTitle=web_template_data[no]['Title'];
				var dataImgPath=web_template_data[no]['PicPath'];
				var dataUrl=web_template_data[no]['Url'];
				for(var i=0; i<len; i++){
					if(web_template_data[no]['IsTitle']==1) $('#holiday_form div[value=title_list]').eq(i).show();
					if(web_template_data[no]['IsLink']==1) $('#holiday_form div[value=url_list]').eq(i).show();
					if(i>0 && !$('#set_banner>.rows').eq(i).length){
						$('#set_banner>.rows').eq(0).clone().appendTo('#set_banner');
						$('#set_banner>.rows').eq(i).find('.up_input').find('*[name!=PicUpload]').remove();
						$('#set_banner label').eq(i).text(lang_obj.global.picture+(i+1)+':');
						$('#set_banner>.rows').eq(i).find('.multi_img .img').attr('num', i);
					}
					$('#set_banner .img_tips_number').eq(i).text(web_template_data[no]['Size'][i]);
					$('#holiday_form input[name=TitleList\\[\\]]').eq(i).val(dataTitle[i]); //标题
					$('#holiday_form input[name=UrlList\\[\\]]').eq(i).val(dataUrl[i]); //链接
					$('#holiday_form input[name=ImgPath\\[\\]]').eq(i).val(dataImgPath[i]?dataImgPath[i]:'');
					$("#holiday_form .preview_pic:eq("+i+")>a").remove();
					if(dataImgPath[i]){ 
						$('#holiday_form input[name=ImgPath\\[\\]]').eq(i).parents('.img').addClass('isfile');
						$('#holiday_form input[name=ImgPath\\[\\]]').eq(i).parents('.img').find('.zoom').attr('href', dataImgPath[i]);
						$("#holiday_form .preview_pic").eq(i).append(frame_obj.upload_img_detail(dataImgPath[i])).children('.upload_btn').hide();
					}else{
						$('#holiday_form input[name=ImgPath\\[\\]]').eq(i).parents('.img').removeClass('isfile');
						$('#holiday_form input[name=ImgPath\\[\\]]').eq(i).parents('.img').find('.zoom').attr('href', 'javascript:;');
						$("#holiday_form .preview_pic:eq("+i+") .upload_btn").show();
					}
					$('#holiday_form input[name=ImgPathHide\\[\\]]').eq(i).val(dataImgPath[i]?dataImgPath[i]:'');
					//$("#holiday_form .pic_detail").eq(i).html(dataImgPath[i]?frame_obj.upload_img_detail(dataImgPath[i]):'');
				}
				
				frame_obj.mouse_click($('#holiday_form .upload_btn, .upload_picture .pic_btn .edit'), 'img', function($this){ //点击上传图片
					var num=$this.parents('.img').attr('num');
					frame_obj.photo_choice_init('holiday_form .multi_img:eq('+num+')', 'isspecial', 1);
				});

				$('#type_hide').val(1);
				if($('#set_banner>.rows').size()>len) $('#set_banner>.rows:gt('+(len-1)+')').remove();//删除多余
			}else{
				$('#set_config').show();
				if(web_template_data[no]['IsTitle']==1) $('#holiday_form div[value=title]').show().find('input[name=Title]').val(web_template_data[no]['Title']); //标题
				if(web_template_data[no]['IsImg']==1){//图片
					$('#holiday_form div[value=images]').show();
					$('#holiday_form input[name=PicPath]').attr('notnull', '');
				}
				if(web_template_data[no]['IsLink']==1) $('#holiday_form div[value=url]').show().find('input[name=Url]').val(web_template_data[no]['Url']).attr('notnull', ''); //链接
				if(web_template_data[no]['IsPro']==1) $('#holiday_form div[value=products]').show();//列表
				
				$('#holiday_form input').filter('#products_btn').attr({'num':web_template_data[no]['ProList'],'max':web_template_data[no]['ListNum']})
				.end().filter('[name=PicPath]').val(web_template_data[no]['PicPath'])
				.end().filter('[name=Title]').focus();
				
				$('#set_config .img_tips_number').text(web_template_data[no]['Size']);
				if(web_template_data[no]['PicPath']){
					$('#PicDetail .img').addClass('isfile');
					$('#PicDetail').find('.zoom').attr('href', web_template_data[no]['PicPath']);
				}else{
					$('#PicDetail .img').removeClass('isfile');
					$('#PicDetail').find('.zoom').attr('href', 'javascript:;');
				}
				$('#PicDetail .preview_pic>a').remove();
				$('#PicDetail .preview_pic').append(frame_obj.upload_img_detail($('#holiday_form input[name=PicPath]').val())).children('.upload_btn').hide();
				$('#type_hide').val(0);
			}
			frame_obj.upload_img_init();
			$('#number_hide').val(no);
		});
		
		//加载默认内容
		$('#web_template .mod').eq(0).click();
		
		$('#products_btn').click(function(){
			window.location='./?m=sales&a=holiday&d=products&theme='+$('#theme_hide').val()+'&num='+$(this).attr('num')+'&max='+$(this).attr('max');
		});
		
		frame_obj.submit_form_init($('#holiday_form'), '', '', '', function(data){
			if(data.ret!=1){
				global_obj.win_alert(data.msg);
			}else{
				var lang_str = '',
					web_lang = $('input[name=HideLang]').val();
				if(web_lang) lang_str = '&lang='+web_lang;
				window.location='./?m=sales&a=holiday&d=edit&theme='+data.msg[0]+lang_str;
			}
		});

		frame_obj.submit_form_init($('#holiday_set_form'), '', '', '', function(data){
			if(data.ret!=1){
				global_obj.win_alert(data.msg);
			}else{
				window.location='./?m=sales&a=holiday&d=edit&theme='+data.msg[0];
			}
		});
		
		$('.lang_box .tab_box_btn').click(function(){
			$('#holiday_form input.btn_submit').click();
			$('input[name=HideLang]').val($(this).attr('data-lang'));
		});
	},
	//--------- 节日模板 end ----------//
	
	//--------- 产品促销 start ----------//
	sales_list_init:function(){
		sales_obj.sales_global.del_action='sales.sales_del_bat';
		sales_obj.sales_global.order_action='sales.sales_edit_myorder';
		sales_obj.sales_global.init();
		sales_obj.batch_edit('./?m=sales&a=sales');
		frame_obj.fixed_right($('.operation .edit'), '.sales_edit_form', function($This){
			var $url=$This.attr('data-url');
			sales_obj.load_edit_form('.sales_edit_form',$url,'get','',function(){
				sales_obj.sales_edit_init();
			});
		});
	},
	
	sales_edit_init:function(){
		$('#sales_edit_form input[name=PromotionTime]').daterangepicker({showDropdowns:true});
		frame_obj.submit_form_init($('#sales_edit_form'), '',function(){
			$('#fixed_right .click').click();
		});
		$('#sales').on('click','.box_select_down li',function(){			
			var $obj=$(this),
				$type=$obj.find('input').val();
			$obj.parent().find('input').removeAttr('checked');
			$obj.find('input').attr('checked','checked');
			$obj.parents('.content').find('[data-promotiontype]').hide();
			$obj.parents('.content').find('input[data-promotiontype]').removeAttr('notnull');
			$obj.parents('.content').find('[data-promotiontype='+$type+']').show();
			$obj.parents('.content').find('input[data-promotiontype='+$type+']').attr('notnull','notnull');
		});
	},
	//--------- 产品促销 end ----------//
	
	//--------- 限时秒杀 start ----------//
	seckill_list_init:function(){
		sales_obj.sales_global.del_action='sales.seckill_del_bat';
		sales_obj.sales_global.order_action='sales.seckill_edit_myorder';
		sales_obj.sales_global.init();
		sales_obj.batch_edit('./?m=sales&a=seckill');
		$('.more_menu a.del').click(function(){
			var $pid=$(this).attr('data-id');
			global_obj.win_alert(lang_obj.manage.sales.del_seckill, function(){
				$.post('./?do_action=sales.del_seckill_period', {PId:$pid}, function(){
					location.href='./?m=sales&a=seckill';
				});
			}, 'confirm');
			return false;
		});
		frame_obj.fixed_right($('.operation .edit'), '.seckill_edit_form', function($This){
			var $url=$This.attr('data-url');
			sales_obj.load_edit_form('.seckill_edit_form', $url, 'get', '', function(){
				sales_obj.seckill_edit_init();
			});
		});
	},
	
	seckill_edit_init:function(){
		$('#seckill_edit_form input[name=PromotionTime]').daterangepicker({showDropdowns:true});
		frame_obj.submit_form_init($('#seckill_edit_form'), '',function(){
			$('#fixed_right .click').click();
		});
		
	},
	//--------- 限时秒杀 end ----------//
	
	//--------- 团购购买 start ----------//
	tuan_list_init:function(){
		sales_obj.sales_global.del_action='sales.tuan_del_bat';
		sales_obj.sales_global.order_action='sales.tuan_edit_myorder';
		sales_obj.sales_global.init();
		sales_obj.batch_edit('./?m=sales&a=tuan');
		frame_obj.fixed_right($('.operation .edit'), '.tuan_edit_form', function($This){
			var $url=$This.attr('data-url');
			sales_obj.load_edit_form('.tuan_edit_form', $url, 'get', '', function(){
				sales_obj.tuan_edit_init();
			});
		});
	},
	
	tuan_edit_init:function(){
		$('#tuan_edit_form input[name=PromotionTime]').daterangepicker({showDropdowns:true});
		frame_obj.submit_form_init($('#tuan_edit_form'), '',function(){
			$('#fixed_right .click').click();
		});
	},
	//--------- 团购购买 end ----------//
	
	//--------- 产品组合购买 start ----------//
	package_list_init:function(){
		sales_obj.sales_global.del_action='sales.package_del_bat';
		sales_obj.sales_global.init();
		$('.input_checkbox_box').click(function(){
			if($(this).find('input[name=IsAttr]').length){
				if($('.input_checkbox_box input[name=IsAttr]').is(':checked')){
					$('.set_sales_list').find('.attr_list ').show().find('select').attr('notnull','notnull');
				}else{
					$('.set_sales_list').find('.attr_list ').hide().find('select').removeAttr('notnull');
				}
				$(window).resize();
			}
		});
		function checked_nonull(){ //开启前台勾选产品属性的时候隐藏属性选择
			if($('.input_checkbox_box input[name=IsAttr]').is(':checked')){
				$('.set_sales_list').find('.attr_list ').hide().find('select').removeAttr('notnull');
			}else{
				$('.set_sales_list').find('.attr_list ').show().find('select').attr('notnull','notnull');
			}
			$(window).resize();
		}
		checked_nonull();
		$('.add_sales_list').on('click','.add_sales_item',function(){
			checked_nonull();
		});
	},
	//--------- 产品组合购买 end ----------//
	
	//--------- 产品促销购买 start --------//
	promotion_list_init:function(){
		sales_obj.sales_global.del_action='sales.promotion_del_bat';
		sales_obj.sales_global.init();
	},
	
	//--------- 产品促销购买 end ----------//
	
	//--------- 优惠券部分 start ----------//
	coupon_list_init:function(){
		frame_obj.del_init($('#coupon .r_con_table'));
		var arr=[
			{"name":lang_obj.global.n_y[0],"val":0},
			{"name":lang_obj.global.n_y[1],"val":1}
		];
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'),$('input[name=select]'), 'sales.coupon_del_dat');
	},
	coupon_edit_init:function(){
		$('input[name=DeadLine]').daterangepicker({showDropdowns:true});//时间插件
		$('#coupon_form .ty_list').click(function(){
			$(this).parent().find('.ty_list').removeClass('checked').find('input').removeAttr('checked');
			$(this).addClass('checked').find('input').attr('checked','checked');
			if($(this).find('input[name="CouponType"]').length){ //更改优惠券的模式 0.折扣 1.现金
				var value = $(this).find('input[name="CouponType"]').val();
				if(value=='0'){
					$('#coupon_form .discount').show();
					$('#coupon_form .money').hide();
				}else{
					$('#coupon_form .discount').hide();
					$('#coupon_form .money').show();
				}
			}else if($(this).find('input[name="CouponWay"]').length){ //会员领取的时候不需要设置限制条件和生成数量
				var value = $(this).find('input[name="CouponWay"]').val();
				if(value>0){
					$('.rows.couponway').hide();	
				}else{
					$('.rows.couponway').show();
				}
			}
		});
		
		var $BackUrl=($('#back_action').length?$('#back_action').val():'./?m=sales&a=coupon');
		frame_obj.submit_form_init($('#coupon_form'), $BackUrl);
		
		var tools_global={
			init: function(){
				$('.range_tools .current_list em').off().on('click', function(){ //删除选项
					$(this).parent().remove();
					tools_global.cur($(this).attr('data-type'));
					tools_global.save($(this).attr('data-type'));
				});
			},
			menu: function($obj, $tools, $type, $data){
				//返回按钮
				$obj.find('.btn_back').attr({'data-type':$data['data-type'], 'data-id':$data['data-id'], 'data-back-pro':$data['data-back-pro']});
				//点击选项
				$obj.find('.range_search_list .item').off().on('click', function(){
					//if($(this).attr('data-individual')==1) return false; //子类目，不触发
					if($(this).attr('data-individual')==1){
						$(this).find('.children').click();
						return false;
					}
					var s_type=$(this).attr('data-type'),
						s_id=$(this).attr('data-id');
					if($(this).hasClass('current')){ //取消
						$(this).removeClass('current');
						if($tools.find('.current_list[data-type='+s_type+'] .item[data-id='+s_id+']').size()){
							$tools.find('.current_list[data-type='+s_type+'] .item[data-id='+s_id+'] em').click();
						}
					}else{ //勾选
						$(this).addClass('current');
						if($tools.find('.current_list[data-type='+s_type+'] .item[data-id='+s_id+']').size()<1){
							var html='<div class="item" data-id="'+s_id+'"><i class="icon icon_'+s_type+'"></i><span>'+$(this).find('span').text()+'</span><em data-type="'+s_type+'">X</em></div>';
							if(s_id=='-1' || (s_id>0 && $tools.find('.current_list[data-type='+s_type+'] .item[data-id=-1]').length)){
								$tools.find('.current_list[data-type='+s_type+']').html(html);
							}else{
								$tools.find('.current_list[data-type='+s_type+']').append(html);
							}
							tools_global.init();
							tools_global.cur(s_type);
							tools_global.save(s_type);
						}
					}
					return false;
				});
				//产品分类 下一级选项
				$obj.find('.range_search_list .children').off().on('click', function(){
					if($(this).parent().attr('data-individual')==1) return false; //子类目，不触发
					tools_global.load_post($obj, {'Type':$type, 'CateId':$(this).parent().attr('data-id')}, function(data){
						tools_global.menu($obj, $tools, $type, data.msg.Back);
					});
					return false;
				});
				//子目录
				$obj.find('.range_search_list .item[data-individual=1] .children').off().on('click', function(){
					var s_type=$(this).parent().attr('data-type');
					tools_global.load_post($obj, {'Type':s_type, 'Search':'', 'IsCate':(s_type=='products_category'?1:0), 'IsUser':(s_type=='user'?1:0)}, function(data){
						tools_global.menu($obj, $tools, s_type, data.msg.Back);
						if(s_type=='user' || s_type=='level'){
							var s_obj=$('input[name=SearchUser]');
						}else{
							var s_obj=$('input[name=SearchApply]');
						}
						s_obj.attr('data-type', s_type).parent().show().prev('.btn_back').show();
					});
					return false;
				});
				//返回
				$obj.find('.btn_back').off().on('click', function(){
					var s_type=$(this).attr('data-type');
					var is_back_pro=$(this).attr('data-back-pro');
					if(is_back_pro==1){
						var json={'Type':s_type, 'CateId':$(this).attr('data-id')}
					}else{
						var json={'Type':s_type, 'Search':'', 'IsLevel':1}
					}
					tools_global.load_post($obj, json, function(data){
						tools_global.menu($obj, $tools, s_type, data.msg.Back);
						if(s_type=='user' || s_type=='level'){
							var s_obj=$('input[name=SearchUser]');
						}else{
							var s_obj=$('input[name=SearchApply]');
						}
						s_obj.attr('data-type', s_type).parent().hide().prev('.btn_back').hide();
					});
					return false;
				});
			},
			load_post: function(obj, data, callback){ //数据传递
				obj.find('.range_search_skin').show();
				obj.find('.range_search_list').html(' ');
				setTimeout(function(){
					$.post('./?do_action=sales.coupon_range_tools', data, function(result){
						if(result.ret==1){
							obj.find('.range_search_skin').hide();
							obj.find('.range_search_list').html(result.msg.Html);
							callback(result);
						}
					}, 'json');
				}, 500);
			},
			cur: function(type){ //勾选显示
				var obj='.range_user', val='', i=0;
				if(type=='products_category' || type=='products' || type=='tags'){
					obj='.range_apply';
				}
				$(obj+' .current_list').each(function(){
					if($(this).find('.item').size()>0){
						val+=(i?', ':'')+lang_obj.manage.sales.coupon[$(this).attr('data-type')];
						++i;
					}
				});
				$(obj+' .range_search>input').val(val);
			},
			save: function(type){ //数据保存记录
				var val='|', obj='', name='';
				if(type=='products_category'){
					obj='.range_apply';
					name='CateId';
				}else if(type=='products'){
					obj='.range_apply';
					name='ProId';
				}else if(type=='tags'){
					obj='.range_apply';
					name='TagId';
				}else if(type=='level'){
					obj='.range_user';
					name='LevelId';
				}else{
					obj='.range_user';
					name='UserId';
				}
				$(obj+' .current_list[data-type='+type+'] .item').each(function(){
					val+=$(this).attr('data-id')+'|';
				});
				$('input[name='+name+']').val(val);
			}
		}
		//触发下拉框焦点
		$('.range_tools .range_search>input').focus(function(){
			var $tools=$(this).parents('.range_tools'),
				$obj=$tools.find('.range_search_menu'),
				$type=$obj.find('.box_input').attr('data-type'),
				$data={'data-type':$obj.find('.btn_back').attr('data-type'), 'data-id':$obj.find('.btn_back').attr('data-id'), 'data-back-pro':$obj.find('.btn_back').attr('data-back-pro')};
			tools_global.menu($obj, $tools, $type, $data);
			if($('.range_search_fixed').is(':visible')){ //如果有多余的下拉框展开，默认全都收起来
				$('.range_search_fixed').hide();
			}
			$(this).next().show();
		});
		//搜索文本框
		$('.range_tools .unit_input>input').keydown(function(e){
			var key=window.event?e.keyCode:e.which;
			if(key==13){ //回车按键
				$(this).next('.last').click();
				return false;
			}
		});
		//搜索按钮
		$('.range_tools .unit_input .last').click(function(){
			var $tools		= $(this).parents('.range_tools'),
				$obj		= $(this).parents('.range_search_menu'),
				$search		= $obj.find('.box_input').val(),
				$type		= $obj.find('.box_input').attr('data-type');
			tools_global.load_post($obj, {'Search':$search, 'Type':$type}, function(data){
				tools_global.menu($obj, $tools, $type, data.msg.Back);
			});
		});
		//取消下拉框焦点
		$(document)[0].addEventListener('click', function(e){
			var $obj=$(e.target);
			if(!$obj.parents('.range_search').length && $('.range_search_fixed').is(':visible')){
				$('.range_search_fixed').hide();
			}
		}, false);
		tools_global.init();
		$('.range_user .unit_input .last').click().parent().hide().prev('.btn_back').hide(); //默认点击一次会员搜索，并隐藏整个搜索框和返回按钮
		$('.range_apply .unit_input .last').click().parent().hide().prev('.btn_back').hide(); //默认点击一次产品搜索，并隐藏整个搜索框和返回按钮
	},
	
	coupon_explode_init:function(){
		$('input[name=DeadLine]').daterangepicker({showDropdowns:true});//时间插件
		$('#explode_edit_form .coupon_type').click(function(){
			var CouponType = $(this).find('input[name="CouponType"]').val();
			if(CouponType=='1'){
				$('#explode_edit_form .discount').removeClass('none');
				$('#explode_edit_form .money').addClass('none');
			}else if(CouponType=='2'){
				$('#explode_edit_form .discount').addClass('none');
				$('#explode_edit_form .money').removeClass('none');
			}else{
				$('#explode_edit_form .discount, #explode_edit_form .money').addClass('none');
			}
		});
		frame_obj.submit_form_init($('#explode_edit_form'), '', '', '', function(data){
			if(data.ret==2){
				$('#explode_progress').append(data.msg[1]);
				$('#explode_edit_form input[name=Number]').val(data.msg[0]);
				$('#explode_edit_form .btn_submit').click();
			}else if(data.ret==1){
				$('#explode_edit_form input[name=Number]').val(0);
				window.location='./?do_action=sales.coupon_explode_down&Status=ok';
			}else{
				$('#explode_edit_form input[name=Number]').val(0);
				global_obj.win_alert_auto_close(data.msg, 'await', 1000, '8%');
				$('#explode_edit_form .btn_submit').removeAttr('disabled');
			}
		});
	},
	//--------- 优惠券部分 end ----------//

	//--------- 全场满减 end ----------//
	discount_init:function(){
		frame_obj.switchery_checkbox();
		$('input[name=DeadLine]').daterangepicker({showDropdowns:true});//时间插件
		$('#discount_form .discount_type .ty_list').click(function(){	//更改优惠券的模式 0.折扣 1.现金
			var value = $(this).find('input[name=Type]').val();
			$('#discount_form .discount_type .ty_list').removeClass('checked').find('input[name=Type]').removeAttr('checked');
			$(this).addClass('checked').find('input[name=Type]').attr('checked','checked');
			if(value=='0'){
				$('#discount_list .discount').show();
				$('#discount_list .money').hide();
			}else{
				$('#discount_list .discount').hide();
				$('#discount_list .money').show();
			}
		});
		$('#add_discount').click(function(){//添加条件
			var $count=$('#discount_list').find('tr').size(),
				Type=parseInt($('#discount_form input[name="Type"]:checked').val()),
				newrow=document.getElementById('discount_list').insertRow(-1),
				$Html='';
			newcell=newrow.insertCell(-1);
			$Html+=	'<span class="d_list">';
			$Html+=		($count==0?'<span class="d_tit">'+lang_obj.manage.sales.condition+'</span>':'<span class="d_empty"></span>');
			$Html+=		'<span class="unit_input"><b>'+ueeshop_config.currSymbol+'</b><input name="UseCondition[]" value="0" type="text" class="box_input" maxlength="10" size="6"></span>';
			$Html+=	'</span>';
			$Html+=	'<span class="d_list discount'+(Type==0?'':' none')+'">';
			$Html+=		($count==0?'<span class="d_tit">'+lang_obj.manage.sales.discount+'</span>':'<span class="d_empty"></span>');
			$Html+=		'<span class="unit_input"><input name="Discount[]" value="100" type="text" class="box_input" maxlength="3" size="5"><b class="last">%</b></span>';
			$Html+=	'</span>';
			$Html+=	'<span class="d_list money'+(Type==1?'':' none')+'">';
			$Html+=		($count==0?'<span class="d_tit">'+lang_obj.manage.sales.money+'</span>':'<span class="d_empty"></span>');
			$Html+=		'<span class="unit_input"><b>'+ueeshop_config.currSymbol+'</b><input name="Money[]" value="0" type="text" class="box_input" maxlength="5" size="5"></span>';
			$Html+=	'</span>';
			$Html+=	'&nbsp;<a class="d_del fl icon_delete_1'+($count>0?' d_del_empty':'')+'" href="javascript:;" onclick="document.getElementById(\'discount_list\').deleteRow(this.parentNode.parentNode.rowIndex);"><i></i></a>';
			$Html+=	'<div class="clear"></div>';									
			newcell.innerHTML=$Html;
		});
		$('#discount_list .d_del').on('click', function(){
			$(this).parents('tr').remove();
		});
		frame_obj.submit_form_init($('#discount_form'), './?m=sales&a=discount');
	},
	//--------- 全场满减 end ----------//
	batch_edit:function(url){
		$('.list_menu .bat_close').on('click', function(){
			var id_list='';
			$('input[name=select]').each(function(index, element) {
				id_list+=$(element).get(0).checked?$(element).val()+'-':'';
            });
			if(id_list){
				id_list=id_list.substring(0,id_list.length-1);
				window.location=url+'&d=batch_edit&id_list='+id_list;
			}else{
				global_obj.win_alert(lang_obj.global.dat_select);
			}
		});
	},
	load_edit_form: function(target_obj, url, type, value, callback, fuc){
		$.ajax({
			type:type,
			url:url+value,
			success:function(data){
				if(fuc=='append'){
					$(target_obj).append($(data).find(target_obj).html());
				}else{
					$(target_obj).html($(data).find(target_obj).html());
				}
				callback && callback(data);
			}
		});
	}
}