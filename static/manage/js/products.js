/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var products_obj={
	//************************************************* 数据储存 Start *************************************************
	data_init:{
		attr_data:{}, //所有属性选项的数据
		ext_attr_data:{}, //关联属性的数据
		fixed_tags_data:new Array('IsIndex', 'IsNew', 'IsHot', 'IsBestDeals')
	},
	//************************************************* 数据储存 End *************************************************
	
	//************************************************* 函数事件 Start *************************************************
	function_init:{
		//语言选择
		html_tab_button:function($class, $lang){
			var $language='';
			$default=ueeshop_config.lang;
			$lang && ($default=$lang);
			$html='';
			$html+='<dl class="tab_box_row'+(ueeshop_config.language.length==1?' hide':'')+($class?' '+$class:'')+'">';
				$html+='<dt><span>'+lang_obj.language[$default]+'</span><i></i></dt>';
				$html+='<dd class="drop_down">';
				for(i in ueeshop_config.language){
					$language=ueeshop_config.language[i];
					//$html+='<a class="tab_box_btn item"'+($default==$language?' style="display:none;"':'')+' data-lang="'+$language+'"><span>'+lang_obj.language[$language]+'</span><i></i></a>';
					$html+='<a class="tab_box_btn item"'+($default==$language?' style="display:none;"':'')+' data-lang="'+$language+'"><span>'+lang_obj.language[$language]+'</span><i></i></a>';
				}
				$html+='</dd>';
				$html+='<dd class="tips">'+lang_obj.manage.error.supplement_lang+'</dd>';
			$html+='</dl>';
			return $html;
		},
		
		//文本框
		attr_form_edit:function(Data, Type, Name, Size, Max){
			var $Html='';
			for(i in ueeshop_config.language){
				$lang=ueeshop_config.language[i];
				if(Name.indexOf('Tab')!='-1'){ //判断是不是选项卡选项
					$field_name=Name+'_'+$lang;
				}else{
					$field_name=Name+'['+$lang+']';
				}
				$value=(Data[$lang]?Data[$lang]:'');
				$Html+='<div class="tab_txt tab_txt_'+$lang+'" lang="'+$lang+'"'+(ueeshop_config.lang==$lang?' style="display:block;"':'')+'>';
				if(Type=='text'){
					$Html+=	'<input type="text" name="'+$field_name+'" value="'+$value+'" class="box_input check_attribute_name" size="'+Size+'" maxlength="'+Max+'" />';
				}else if(Type=='textarea'){
					$Html+=	'<textarea name="'+$field_name+'" class="box_textarea">'+$value+'</textarea>';
				}else if(Type=='editor'){
					$Html+=	'<textarea id="'+$field_name+'" name="'+$field_name+'">'+$value+'</textarea>';
					$Html+=	'<script type="text/javascript">';
					$Html+=	'CKEDITOR.replace("'+$field_name+'", {"language":"'+ueeshop_config.manage_language+'"});';
					$Html+=	'</script>';
				}
				$Html+='</div>';
			}
			return $Html;
		},
		
		//文本框
		attr_unit_form_edit:function(Data, Type, Name, Size, Max){
			var $Html='',
				$LangCount=0,
				$langAry=[ueeshop_config.lang]; //默认语言排第一个
			for(i in ueeshop_config.language){
				$LangCount++;
				if(ueeshop_config.lang!=ueeshop_config.language[i]){ //非默认语言靠后
					$langAry.push(ueeshop_config.language[i]);
				}
			}
			for(i in $langAry){
				$lang=$langAry[i];
				if(Name.indexOf('Tab')!='-1'){ //判断是不是选项卡选项
					$field_name=Name+'_'+$lang;
				}else{
					$field_name=Name+'['+$lang+']';
				}
				$value=(Data[$lang]?Data[$lang]:'');
				$Html+='<div class="lang_txt lang_txt_'+$lang+'" lang="'+$lang+'"'+(ueeshop_config.lang==$lang?' style="display:block;"':'')+(ueeshop_config.lang==$lang?' data-default="1"':'')+'>';
				if(Type=='text'){
					$Html+='<span class="unit_input">'+($LangCount>1 ? '<b>'+lang_obj.language[$lang]+'</b>' : '')+'<input type="text" class="box_input" name="'+$field_name+'" value="'+$value+'" size="'+Size+'" maxlength="'+Max+'" /></span>';
				}/*else if(Type=='textarea'){
					$Html+=	'<textarea name="'+$field_name+'" class="box_textarea">'+$value+'</textarea>';
				}else if(Type=='editor'){
					$Html+=	'<textarea id="'+$field_name+'" name="'+$field_name+'">'+$value+'</textarea>';
					$Html+=	'<script type="text/javascript">';
					$Html+=	'CKEDITOR.replace("'+$field_name+'", {"language":"'+ueeshop_config.manage_language+'"});';
					$Html+=	'</script>';
				}*/
				$Html+='</div>';
			}
			return $Html;
		},
		
		//产品属性选项按钮
		attr_option_button:function(name, value, attrid, isCurrent){
			var isOverseas=(typeof(arguments[4])=='undefined' || arguments[4]==0)?0:1, //是否为海外仓选项
				html='';
			html+='<span class="btn_attr_choice'+(isCurrent?' current':'')+'">';
			html+=	'<b>'+name+'</b>';
			html+=	'<input type="checkbox" name="AttrCurrent[]" value="'+value+'" class="attr_current"'+(isCurrent?' checked':'')+' />';
			html+=	'<input type="hidden" name="AttrOption[]" value="'+value+'" />';
			html+=	'<input type="hidden" name="AttrParent[]" value="'+attrid+'" />';
			html+=	'<input type="hidden" name="AttrName[]" value="'+(name ? global_obj.htmlspecialchars(name) : name)+'" />';
			html+=	'<i'+(isOverseas==1?' class="hide"':'')+'></i>';
			html+='</span>';
			return html;
		},
		
		//标签选项按钮
		tags_option_button:function(name, value, isCurrent){
			var html='';
			html+='<span class="btn_attr_choice'+(isCurrent?' current':'')+'">';
			html+=	'<b>'+name+'</b>';
			html+=	'<input type="checkbox" name="TagsCurrent[]" value="'+value+'" class="attr_current"'+(isCurrent?' checked':'')+' />';
			html+=	'<input type="hidden" name="TagsOption[]" value="'+value+'" />';
			html+=	'<input type="hidden" name="TagsName[]" value="'+name+'" />';
			html+=	'<i></i>';
			html+='</span>';
			return html;
		},
		
		//产品属性选项
		attr_load:function(){
			products_obj.function_init.attr_input();
			
			//属性选项整体按钮
			$('.box_attr_list .btn_attr_choice').off().on('click', function(){
				var $This=$(this),
					$Obj=$This.find('input.attr_current'),
					$Parent=$This.parent().parent(),
					$Box=$This.parents('.box_attr'),
					$LBox=$This.parents('.attr_not_yet'),
					$IsCart=$Box.find('.attr_cart').val();
				if($Parent.hasClass('attr_not_yet')){ //未选择 -> 已选中
					$This.appendTo($Parent.prev().find('.select_list'));
					$This.addClass('current');
					$This.find('i').show();
					$Obj.attr('checked', true);
					$Box.find('.placeholder').addClass('hide');
					if($Parent.find('.btn_attr_choice').size()<1){
						$Parent.hide();
					}
					if(global_obj.in_array($Obj.val(), products_obj.data_init.fixed_tags_data)){ //固有标签
						$This.find('i').show();
					}
				}
				if($IsCart==1 && $LBox.length){ //确保在属性选项列表里面才触发
					products_obj.function_init.attr_price_show();
				}
				products_obj.function_init.attr_dragsort();//属性选项拖动
				return false;
			});
			
			//属性选项删除按钮
			$('.box_attr_list .btn_attr_choice>i').off().on('click', function(){
				return false;
			});
			frame_obj.mouse_click($('.box_attr_list .btn_attr_choice>i'), 'attr', function($this){
				var $This=$this.parent(),
					$Obj=$This.find('input.attr_current'),
					$Parent=$This.parent().parent(),
					$Box=$This.parents('.box_attr'),
					$IsCart=$Box.find('.attr_cart').val();
				if($Parent.hasClass('attr_selected')){ //已选中 -> 未选择
					$This.removeClass('current');
					$This.appendTo($Parent.next().find('.select_list'));
					$Obj.removeAttr('checked');
					$Parent.next().show();
					$Parent.find('.box_input').val('').hide().removeAttr('style');
					if($Box.attr('data-id')=='Overseas'){ //海外仓
						$this.hide();
					}
					if(global_obj.in_array($Obj.val(), products_obj.data_init.fixed_tags_data)){ //固有标签
						$this.hide();
					}
					if($Parent.find('.btn_attr_choice').size()<1){ //没有选项，显示placeholder
						$Parent.find('.placeholder').removeClass('hide');
					}
				}else{ //未选择 -> 删除
					$This.remove();
					if($Parent.find('.btn_attr_choice').size()<1){
						$Parent.hide();
					}
				}
				if($IsCart==1 && $Box.length){ //确保在属性选项列表里面才触发
					products_obj.function_init.attr_price_show();
				}
				return false;
			});
			
			//属性选项全选按钮
			$('.box_attr_list .select_all').off().on('click', function(){
				$(this).parent().find('.btn_attr_choice').click();
				return false;
			});
			
			//属性删除按钮
			$('.box_general_attribute, .box_cart_attribute').off().on('click', '.attr_delete', function(){
				//$(this).parent().remove();
				$(this).parents('.box_attr, .box_text').remove();
				products_obj.function_init.attr_price_show();
				return false;
			});
			
			//属性选项拖动
			products_obj.function_init.attr_dragsort();
			
			//修改属性选项
			frame_obj.fixed_right($('.btn_option_edit'), '.fixed_edit_attribute_option_edit', function(obj){
				var $Form=$('#edit_attribute_option_edit_form'),
					$Obj=obj.parents('.box_attr'),
					$Data=new Object, $AttrId, $VId, $Name, $Html='', $i=0;
				$AttrId=$Obj.attr('data-id');
				$Form.find('.edit_attr_list').html('');
				$Obj.find('.attr_selected .btn_attr_choice').each(function(){
					$VId=$(this).find('input[name=AttrOption\\[\\]]').val();
					$Name=$(this).find('input[name=AttrName\\[\\]]').val();
					$Data[$VId]=$Name;
					++$i;
				});
				$Obj.find('.attr_not_yet .btn_attr_choice').each(function(){
					$VId=$(this).find('input[name=AttrOption\\[\\]]').val();
					$Name=$(this).find('input[name=AttrName\\[\\]]').val();
					$Data[$VId]=$Name;
					++$i;
				});
				if($i>0){
					$Html+=	'<div class="rows">';
					$Html+=		'<label>'+lang_obj.global.option+'<div class="tab_box">'+products_obj.function_init.html_tab_button()+'</div></label>';
					$Html+=		'<div class="input">';
					for(k in ueeshop_config.language){
						$Lang=ueeshop_config.language[k];
						$Html+=		'<div class="tab_txt tab_txt_'+$Lang+'" lang="'+$Lang+'"'+(ueeshop_config.lang==$Lang?'style="display:block;"':'')+'>';
						for(j in $Data){
							$Name=products_obj.data_init.attr_data[j].Name[$Lang];
							!$Name && ($Name='');
							$Html+=		'<div class="item clean" data-id="'+j+'">';
							$Html+=			'<input type="text" class="box_input fl" name="Name['+j+']['+$Lang+']" value="'+$Name+'" size="30" maxlength="255" autocomplete="off" notnull />';
							$Html+=			'<div class="default_name fl" title="'+$Data[j]+'">'+$Data[j]+'</div>';
							$Html+=		'</div>';
						};
						$Html+=		'</div>';
					}
					$Html+=		'</div>';
					$Html+=	'</div>';
					$Form.find('.edit_attr_list').html($Html);
					$Form.find('.edit_tips, .edit_attr_list, .box_button').show();
					$Form.find('.bg_no_table_data').hide();
				}else{
					$Form.find('.edit_tips, .edit_attr_list, .box_button').hide();
					$Form.find('.bg_no_table_data').show();
				}
				$Form.find('input[name=AttrId]').val($AttrId);
				$Form.prev('.top_title').find('strong').html(lang_obj.manage.products.modification_title.replace('%title%', $Obj.find('label>strong').text())); //标题
			});
			
			//关联图片
			frame_obj.fixed_right($('.btn_associate_pic'), '.fixed_associate_pic', function(obj){
				var $Form=$('#edit_picture_form'),
					$ProId=$Form.find('input[name=ProId]').val(),
					$CateId=$('select[name=CateId]').val(),
					$AttrId=obj.parents('.box_attr').attr('data-id'),
					$AttrTitle=obj.parents('.box_attr').find('.check_attribute_name').val(),
					$VId=0, $VIdStr='0', $Html='';
				$Form.find('input[name=CateId]').val($CateId);
				$Form.find('input[name=AttrId]').val($AttrId);
				$Form.find('input[name=AttrTitle]').val($AttrTitle);
				$Form.find('.edit_picture_list').html('');
				obj.parents('.box_attr').find('.attr_selected .btn_attr_choice').each(function(){
					$VId=$(this).find('input[name=AttrOption\\[\\]]').val();
					$VIdStr+=','+$VId;
					$Html+=	'<div class="item clean" data-id="'+$VId+'">';
					$Html+=		'<div class="title color_000">'+$(this).find('b').text()+'</div>';
					$Html+=		'<div class="multi_img upload_file_multi pro_multi_img" id="ColorDetail_'+$VId+'"></div>';
					$Html+=		'<input type="hidden" name="ColorName['+$VId+']" value="'+$(this).find('b').text()+'" />';
					$Html+=	'</div>';
				});
				$Form.find('.edit_picture_list').html($Html);
				$Form.prev('.top_title').find('strong').html(lang_obj.manage.products.pictures_title.replace('%title%', obj.parents('.box_attr').find('label>strong').text())); //标题
				if($Html){
					products_obj.function_init.associate_pic_load($Form, $ProId, $CateId, $AttrId, $VIdStr);
					$Form.find('.edit_tips, .switchery_pictures, .edit_picture_list, .box_button').show();
					$Form.find('.bg_no_table_data').hide();
				}else{
					$Form.find('.edit_tips, .switchery_pictures, .edit_picture_list, .box_button').hide();
					$Form.find('.bg_no_table_data').show();
				}
			});
			
			//详细介绍和描述属性的切换
			$('.description_box .head ul span').off().on('click', function(){
				var $Obj=$(this).parent(),
					$Index=$Obj.index(),
                    $DataId=$Obj.attr('data-id');
				$Obj.addClass('current').siblings().removeClass('current');
                if($DataId){
                    $('.description_box .input>ul>li[data-id="'+$DataId+'"]').show().siblings().hide();
                }else{
                    $('.description_box .input>ul>li').eq($Index).show().siblings().hide();
                }
			});
			
			//删除描述属性内容
			$('.description_box').off().on('click', '.attr_delete', function(){
				var $Obj=$(this).parent(),
					$ID=$Obj.attr('data-id');
				if($Obj.hasClass('current')){ //当前选项是否为“选中”状态
					$('.description_box .head li:eq(0)>span').click(); //默认勾选回“详细介绍”
				}
				$(this).parent().remove();
				$('.description_box .input>ul>li[data-id="'+$ID+'"]').remove();
				products_obj.function_init.tab_load();
			});
		},
		
		//产品属性选项拖动事件
		attr_dragsort:function(){
			$('.box_general_attribute .attr_selected .select_list').dragsort('destroy'); //先清除所有相关的拖动事件
			$('.box_general_attribute .box_attr').each(function(){
				frame_obj.dragsort($(this).find('.attr_selected .select_list'), '', 'span', '', '<span class="btn_attr_choice placeHolder"></span>');
			});
			$('.box_cart_attribute .attr_selected .select_list').dragsort('destroy'); //先清除所有相关的拖动事件
			$('.box_cart_attribute .box_attr').each(function(){
				frame_obj.dragsort($(this).find('.attr_selected .select_list'), '', 'span', '', '<span class="btn_attr_choice placeHolder"></span>');
			});
		},
		
		//产品描述属性事件
		tab_load:function(){
			var $Target=$('.description_box .head_hide>ul'),
				$ObjWidth=$('.description_box .head_hide').outerWidth(),
				$Width=0, $W, $R;
			$Target.css('width', 'auto');
			$('.description_box .head li').each(function(){
				$W=parseFloat($(this)[0].getBoundingClientRect().width.toFixed(3)); //精准到小数点后三位，以防偏差
				$R=parseInt($(this).css('margin-right'));
				$Width+=$W+$R;
			});
			$Target.css('width', $Width+1); //加1，以防偏差1像素
			if($Width>$ObjWidth){
				$('.description_box .head_btn').css('display', 'inline-block');
			}else{
				$('.description_box .head_btn').css('display', 'none');
			}
		},
		
		//关联图片事件
		associate_pic:function(){
			//初始化
			var $Form=$('#edit_picture_form');
			
			//循环显示
			$Form.find('.edit_picture_list .item').each(function(){
				var $This=$(this);
				frame_obj.upload_pro_img_init(1, '#'+$This.find('.multi_img').attr('id').replace(':', '\\:'));
				$This.find('.preview_pic input:hidden').each(function(){
					if($(this).attr('save')==1){
						$(this).parent().append(frame_obj.upload_img_detail($(this).attr('data-value'))).children('.upload_btn').hide();
					}
				});
			});
			
			//关联图片上传
			frame_obj.mouse_click($Form.find('.multi_img .upload_btn, .pic_btn .edit'), 'pro', function($this){ //产品主图点击事件
				var $VId=$this.parents('.item').attr('data-id'),
					$Num=$this.parents('.img').attr('num'),
					$IsColor=$('#edit_picture_form input[name=IsColor]').is(':checked');
				frame_obj.photo_choice_init('ColorDetail_'+$VId+' .img[num;'+$Num+']', 'products', ($IsColor?ueeshop_config.prodImageCount:1), 'do_action=products.products_img_del&Model=products', 1, "frame_obj.upload_pro_img_init(1, '#ColorDetail_"+$VId+"', 1);");
			});
			
			//拖动关联图片
			$Form.find('.multi_img').dragsort('destroy');
			frame_obj.dragsort($Form.find('.multi_img'), '', 'dl', 'i', '<dl class="img placeHolder"></dl>');
			
			//删除关联图片
			frame_obj.mouse_click($Form.find('.pic_btn .del'), 'proDel', function($this){
				var $obj=$this.parents('.img'),
					$num=parseInt($obj.attr('num')),
					$vid=$this.parents('.item').attr('data-id'),
					$path=$obj.find('input[type=hidden]').val();
				global_obj.win_alert(lang_obj.global.del_confirm, function(){
					$.ajax({
						url:'./?do_action=products.products_img_del&Model=products&Path='+$path+'&Index='+$num+'&ProId='+$('#ProId').val(),
						success:function(data){
							$obj.removeClass('isfile').parent().find('.img').removeClass('show_btn');
							$obj.parent().prepend($obj);
							$obj.find('.preview_pic .upload_btn').show();
							$obj.find('.preview_pic a').remove();
							$obj.find('.preview_pic input[type=hidden]').val('').attr('save', 0);
							frame_obj.upload_pro_img_init(1, '#ColorDetail_'+$vid);
							var $i=9;
							$obj.parent().find('.img').each(function(){
								$(this).attr('num', $i);
								--$i;
							});
						}
					});
				}, 'confirm');
			});
		},
		
		//关联图片加载事件
		associate_pic_load:function($Form, $ProId, $CateId, $AttrId, $VIdStr){
			var IsColor=(typeof(arguments[5])=='undefined')?2:arguments[5];
			$.post('?', {'do_action':'products.products_get_color', 'ProId':$ProId, 'CateId':$CateId, 'AttrId':$AttrId, 'VId':$VIdStr}, function(data){
				if(data.ret==1){
					var j=0, $MaxPic=parseInt(ueeshop_config.prodImageCount);
					IsColor==2 && (IsColor=data.msg.IsColor);
					for(k in data.msg.Img){
						$Html='';
						if(k==0) continue;
						$Form.find('.edit_picture_list .item[data-id='+k.replace(':', '\\:')+'] .multi_img').html('');
						for(j=0; j<$MaxPic; ++j){
							$Html+=	'<dl class="img'+(data.msg.Img[k][j]['isfile']?' isfile':'')+'" num="'+j+'"'+(IsColor==0 && j>0?' style="display:none;"':'')+'>';
							$Html+=		'<dt class="upload_box preview_pic">';
							$Html+=			'<input type="button" id="ColorUpload_'+k+'_'+j+'" class="btn_ok upload_btn" name="submit_button" value="" tips="" />';
							$Html+=			'<input type="hidden" name="ColorPath['+k+'][]" value="'+data.msg.Img[k][j]['big']+'" data-value="'+data.msg.Img[k][j]['small']+'" save="'+(data.msg.Img[k][j]['isfile']?1:0)+'" />';
							$Html+=		'</dt>';
							$Html+=		'<dd class="pic_btn">';
							$Html+=			'<a href="javascript:;" class="edit"><i class="icon_edit_white"></i></a>';
							$Html+=			'<a href="'+data.msg.Img[k][j]['small']+'" class="zoom" target="_blank"><i class="icon_search_white"></i></a>';
							$Html+=			'<a href="javascript:;" class="del" rel="del"><i class="icon_del_white"></i></a>';
							$Html+=		'</dd>';
							$Html+=	'</dl>';
						}
						$Form.find('.edit_picture_list .item[data-id='+k.replace(':', '\\:')+'] .multi_img').append($Html);
					}
					if(IsColor==1){
						$Form.find('.switchery').addClass('checked').find('input').attr('checked', 'checked');
					}else{
						$Form.find('.switchery').removeClass('checked').find('input').removeAttr('checked');
					}
					products_obj.function_init.associate_pic();
				}
			}, 'json');
		},
		
		//关联图片保存事件
		associate_pic_ajax:function($Data, $AttrId, $IsColor, $Num){
			var $ProId=$('#edit_picture_form input[name=ProId]').val(),
				$AddProId=$('#edit_picture_form input[name=AddProId]').val();
			if($Data){
				$.post('?', {'do_action':'products.products_associate_pic_ajax', 'ProId':$ProId, 'Data':$Data, 'AttrId':$AttrId, 'IsColor':$IsColor, 'AddProId':$AddProId, 'Num':$Num}, function(result){
					if(result.ret>0){
						$('#edit_picture_form .edit_picture_list .item[data-id='+result.msg.VId+'] .img>.loading').addClass('success').removeClass('loading'); //去掉加载icon
						if(result.ret==1){ //继续
							setTimeout(function(){ products_obj.function_init.associate_pic_ajax(result.msg.Data, result.msg.AttrId, result.msg.IsColor, ++$Num) }, 2000);
						}else if(result.ret==2){ //完成
							global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
							setTimeout(function(){ $('#fixed_right .btn_cancel').click(); }, 1000);
						}
					}
				}, 'json');
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
				$('#fixed_right .btn_cancel').click();
			}
		},
		
		//产品属性选项添加事件
		attr_input:function(){
			//触发属性选项添加文本框
			$('.attr_selected').off().on('click', function(){
				var $Obj=$(this).find('.box_input'),
					$NotYet=$(this).next('.attr_not_yet'),
					$ItemLast=$(this).find('.btn_attr_choice:last'),
					$ItemLastLeft=($ItemLast.size()?$ItemLast.offset().left:0),
					$BoxLeft=0, $BoxWidth=0;
				$(this).addClass('selected_focus');
				if($NotYet.find('.btn_attr_choice').size()){
					$NotYet.slideDown();
				}
				$Obj.show().focus();
				$BoxLeft=($ItemLastLeft?($ItemLastLeft+$ItemLast.outerWidth(true))-$(this).offset().left:0);
				$BoxWidth=($(this).outerWidth()-$BoxLeft-31);
				if($BoxWidth<20){ //小于20，自动换行
					$Obj.css({'width':$(this).outerWidth()-41});
				}else{
					$Obj.css({'width':$BoxWidth, 'position':'absolute', 'bottom':5, 'left':$BoxLeft});
				}
				$Obj.off().on('blur', function(){ //文本框失去焦点
					$Obj.val('').hide().removeAttr('style');
				}).on('keyup', function(e){ //属性选项添加
					var value=$.trim($(this).val()),
						key=window.event?e.keyCode:e.which,
						html;
					if(key==13){ //回车键
						if(value){
							var obj=$(this).parents('.box_attr'),
								key=obj.attr('data-id'),
								name=obj.find('.rows:eq(0) .tab_txt:eq(0) input').val(), //属性名称
								MaxNum=0, vid='ADD:', Stop=0;
							$Obj.val('').hide().removeAttr('style');
							//检查内容有没有重复一致的选项存在
							$(this).parents('.attr_selected').find('input[name=AttrName\\[\\]]').each(function(){ //当前一行
								if($(this).val()==value){ Stop=1; return false; }
							});
							if(Stop==0){ //还没有被终止
								$(this).parents('.attr_selected').next().find('input[name=AttrName\\[\\]]').each(function(){ //下一行
									if($(this).val()==value){
										$(this).parent().click();
										Stop=1;
									}
								});
							}
							if(Stop==1) return false; //终止继续执行
							if(obj.length){ //确保在属性选项列表里面才触发
								MaxNum=parseInt($('#option_max_number').val()); //获取新属性ID的最大值
								MaxNum+=1;
								vid+=MaxNum;
								$Obj.prev('.select_list').append(products_obj.function_init.attr_option_button(value, vid, key, 1));
								$('#option_max_number').val(MaxNum);
								products_obj.data_init.attr_data[vid]=new Object;
								products_obj.data_init.attr_data[vid]['Info']=new Object;
								products_obj.data_init.attr_data[vid].Info.Column=name;
								products_obj.data_init.attr_data[vid].Info.VId=vid;
								products_obj.data_init.attr_data[vid].Info.Name=value;
								products_obj.data_init.attr_data[vid].Info.Cart=1;
								products_obj.data_init.attr_data[vid].Info.Color=0;
								products_obj.data_init.attr_data[vid].Info.Desc=0;
								products_obj.data_init.attr_data[vid]['Data']='';
								products_obj.data_init.attr_data[vid]['Name']=new Object;
								products_obj.data_init.attr_data[vid]['Name'][ueeshop_config.lang]=value;
								products_obj.function_init.attr_load();
								products_obj.function_init.attr_price_show();
							}else{ //其他
								MaxNum=parseInt($('#tags_max_number').val()); //获取新标签ID的最大值
								MaxNum+=1;
								vid+=MaxNum;
								$Obj.prev('.select_list').append(products_obj.function_init.tags_option_button(value, vid, 1));
								$('#tags_max_number').val(MaxNum);
							}
							$Obj.next('.placeholder').addClass('hide');
							$Obj.click();
						}
						if(window.event){//IE
							e.returnValue=false;
						}else{//Firefox
							e.preventDefault();
						}
						return false;
					}
				});
			});
			
			//【添加属性】 检查已有的属性
			$('.box_cart_attribute .check_attribute_name, #check_general_attribute_name .box_input').off().on('keydown', function(e){
				var key=window.event?e.keyCode:e.which;
				if(key==13){ //禁止使用回车键
					if(window.event){//IE
						e.returnValue=false;
					}else{//Firefox
						e.preventDefault();
					}
					return false;
				}
			}).on('keyup', function(){
				var $This=$(this),
					$Value=$.trim($This.val()),
					$CateId=$('select[name=CateId]').val(),
					$Form=$This.parents('.global_form'),
					$Name=$This.parents('.rows'),
					$List=$This.parents('.rows').next(),
					$Lang=$This.parent().attr('lang'),
					$ID=$This.parent().parent().attr('id'),
					$Html='', $IsChecked=0, $IsCartAttr=1, $VId=0, $Type=0;
				if(!$Value) return false;
				if($ID=='check_general_attribute_name'){ //普通属性 or 描述属性
					$IsCartAttr=0;
					$Type=$('#add_general_attribute_form input[name=Type]:checked').val(); //0-1:普通属性 2:描述属性 (当不属于规格属性时候的才调用)
					$List=$This.parents('.rows').next().next();
					$Form.find('input[name=CateId]').val($CateId);
				}
				$.post('?', {"do_action":"products.products_check_attribute", 'Name':$Value, 'CateId':$CateId, 'Lang':$Lang, 'IsCartAttr':$IsCartAttr, 'Type':$Type}, function(data){
					$('.attribute_select_list').remove();
					if(data.ret==1){
						$Html='<ul class="attribute_select_list">';
						for(k in data.msg){
							$Html+='<li><a href="javascript:;" data-value="'+data.msg[k].AttrId+'">'+data.msg[k].Name+(data.msg[k].Category?' ('+data.msg[k].Category+')':'')+'</a></li>';
						}
						$Html+='</ul>';
						$This.after($Html);
						$('.attribute_select_list').show();
						if(data.msg[0]){ //有名称重复的属性，禁止添加
							$This.removeAttr('data-not-add');
							$Type==2 && $List.show();
						}else{
							$This.attr('data-not-add', 1);
							$Type==2 && $List.hide();
						}
					}
					//点击已有属性，进行加载相应的属性选项数据
					$('.attribute_select_list a').off().on('click', function(){
						$This=$(this);
						$Value=$This.attr('data-value');
						$ProId=$('#ProId').val();
						$MaxNum=parseInt($('#attr_max_number').val()); //获取新属性ID的最大值
						$Type==2 && $List.show();
						$This.parents('.box_attr').find('.check_attribute_name').removeAttr('data-not-add');
						if($.trim($Value)==''){ //添加选项
							if($IsCartAttr==0){ //普通属性
								$Form.find('input[name=AttrId]').val('');
							}else{ //规格属性
								$This.parents('.box_attr').attr('data-id', 'ADD:'+$MaxNum);
								$This.parents('.box_attr').find('.rows:eq(0) .tab_txt').each(function(){
									$(this).find('.check_attribute_name').attr('name', 'AttrTitle[ADD:'+$MaxNum+']['+$(this).attr('lang')+']');
								});
							}
							$('.attribute_select_list').remove();
							return false;
						}
						$.post('?', {"do_action":"products.products_check_attribute_option", 'ProId':$ProId, 'AttrId':$Value, 'IsCartAttr':$IsCartAttr}, function(result){
							$List.find('.attr_selected .select_list, .attr_not_yet .select_list').html('');
							$List.find('.box_attr_input .box_input').html('');
							if(result.ret==1){
								$Html='';
								if($IsCartAttr==1){ //规格属性
									for(k in result.msg.Name){ //属性名称
										$Name.find('.tab_txt_'+k+' .box_input').val(result.msg.Name[k]);
									}
									for(k in result.msg.Selected){
										$VId=result.msg.Selected[k].VId;
										products_obj.data_init.attr_data[$VId]=new Object;
										products_obj.data_init.attr_data[$VId]['Info']=result.msg.Selected[k].Info;
										products_obj.data_init.attr_data[$VId]['Data']=result.msg.Selected[k].Data;
										$Html+=products_obj.function_init.attr_option_button(result.msg.Selected[k].Value, result.msg.Selected[k].VId, $Value, 1);
									}
									if($Html){ //有内容
										$List.find('.placeholder').addClass('hide');
									}
									$List.find('.attr_selected .select_list').html($Html);
									$Html='';
									for(k in result.msg.Option){
										$VId=result.msg.Option[k].VId;
										products_obj.data_init.attr_data[$VId]=new Object;
										products_obj.data_init.attr_data[$VId]['Info']=result.msg.Option[k].Info;
										products_obj.data_init.attr_data[$VId]['Data']=result.msg.Option[k].Data;
										$Html+=products_obj.function_init.attr_option_button(result.msg.Option[k].Value, result.msg.Option[k].VId, $Value, 0);
									}
									if($Html){ //有内容
										$List.find('.attr_not_yet .select_list').html($Html);
									}else{ //没有内容就隐藏起来
										$List.find('.attr_not_yet .select_list').hide();
									}
									products_obj.function_init.attr_price_show(); //加载完才触发
								}else{ //普通属性
									if(result.msg.Type==2){ //描述属性
										$Form.find('.type_box input[name=Type][value=2]').parent().click();
										for(k in result.msg.Name){ //属性名称
											$Form.find('.rows[data-type=name] .tab_txt_'+k+' .box_input').val(result.msg.Name[k]);
										}
										for(k in result.msg.Option){ //向编辑器增加内容
											CKEDITOR.instances['Editor_'+k].setData((result.msg.Option[k] ? result.msg.Option[k].replace(/\\"/g, '"') : result.msg.Option[k]));
										}
									}else{ //普通属性
										$Form.find('.type_box input[name=Type][value="'+result.msg.Type+'"]').parent().click();
										for(k in result.msg.Name){ //属性名称
											$Form.find('.rows[data-type=name] .tab_txt_'+k+' .box_input').val(result.msg.Name[k]);
										}
										for(k in result.msg.Option){ //选项
											$Form.find('.rows[data-type=value] .tab_txt_'+k+' .box_input').val(result.msg.Option[k]);
										}
									}
								}
							}
							if($IsCartAttr==0){ //普通属性
								$Form.find('input[name=AttrId]').val($Value);
							}else{ //规格属性
								$This.parents('.box_attr').attr('data-id', $Value);
								$This.parents('.box_attr').find('.check_attribute_name').attr('name', 'AttrTitle['+$Value+']['+ueeshop_config.lang+']');
								$This.parents('.box_attr').find('input[name=IsColor\\[\\]]').val($Value); //颜色属性勾选按钮
							}
							$('.attribute_select_list').remove();
							products_obj.function_init.attr_load();
						}, 'json');
						return false;
					});
				}, 'json');
				return false;
			});
			
			$(document).off().on('click', function(e){
				if($('#check_general_attribute_name .attribute_select_list:visible').length){
					if($(e.target).parents('#check_general_attribute_name').size()==0){
						$('.attribute_select_list').remove();
					}
				}
				if($('.box_cart_attribute .attribute_select_list:visible').length){
					$('.attribute_select_list').remove();
				}
			});
		},
		
		//属性价格事件
		attr_ext_option:function(){
			//选项卡
			$('#attribute_ext_box .multi_tab_row .btn_multi_tab').off().on('click', function(){
				var $This=$(this),
					$OvId=$This.attr('data-oversea');
				if($('#AttrId_'+$OvId).length){
					$('#AttrId_'+$OvId).show().siblings('tbody').hide();
					$('#attribute_ext_box .btn_multi_tab[data-oversea='+$OvId+']').addClass('current').siblings().removeClass('current');
				}
			});
			
			//加价按钮
			$('#attribute_ext .switchery').off().on('click', function(){
				if($(this).hasClass('checked')){
					$(this).removeClass('checked').find('input').removeAttr('checked');
					if($(this).hasClass('btn_increase_all')){ //总舵
						$(this).parents('tbody').find('.switchery').removeClass('checked').find('input').removeAttr('checked');
						$('#attribute_tmp .btn_increase_all').removeClass('checked').find('input').removeAttr('checked');
						$('#attribute_tmp .contents .switchery').removeClass('checked').find('input').removeAttr('checked');
					}else{ //分舵
						$(this).parents('tbody').find('tr:gt(0) .switchery').not('.checked').length>0 && $(this).parents('tbody').find('.btn_increase_all').removeClass('checked').find('input').removeAttr('checked');
					}
				}else{
					$(this).addClass('checked').find('input').attr('checked', true);
					if($(this).hasClass('btn_increase_all')){ //总舵
						$(this).parents('tbody').find('.switchery').addClass('checked').find('input').attr('checked', true);
						$('#attribute_tmp .btn_increase_all').addClass('checked').find('input').attr('checked', true);
						$('#attribute_tmp .contents .switchery').addClass('checked').find('input').attr('checked', true);
					}else{ //分舵
						$(this).parents('tbody').find('tr:gt(0) .switchery').not('.checked').length==0 && $(this).parents('tbody').find('.btn_increase_all').addClass('checked').find('input').attr('checked', true);
					}
				}
				return false;
			});
			
			//判断加价全选按钮状态
			if($('#attribute_ext tbody tr:gt(0) .switchery').not('.checked').length==0){ //全选状态
				$('#attribute_ext tbody .btn_increase_all').addClass('checked').find('input').attr('checked', true);
			}
			
			//同步操作
			$('#attribute_ext_box .synchronize_btn').off().on('click', function(){
				var $num=parseInt($(this).attr('data-num')),
					$obj=$(this).parents('tr'),
					$value=$obj.nextAll('tr[id!=""]').find('td:eq('+($num+1)+') input').val();
				$obj.siblings('tr').find('td:eq('+($num+1)+') input').val($value);
			});
			
			//批量操作
			$('#attribute_ext_box .btn_batch').off().on('click', function(){
				var $Attr=$('.box_cart_attribute .box_attr[data-id!=Overseas]'), $Html='';
				$('#attribute_ext_box .batch_edit').html('').hide();
				$Html+=	'<em></em><i></i>';
				$Html+=	'<div class="rows clean">';
				$Html+=		'<label class="color_000">'+lang_obj.manage.products.price+'</label>';
				$Html+=		'<span class="input">';
								$Attr.each(function(){
									if($(this).find('.attr_current:checked').size()>0){
				$Html+=					'<p><input type="radio" name="BatchPrice" value="'+$(this).attr('data-id')+'" /> '+lang_obj.manage.products.batch_notes.replace('%txt%', $(this).find('.tab_txt_'+ueeshop_config.lang+' .box_input').val())+'</p>';
									}
								});
				$Html+=		'</span>';
				$Html+=	'</div>';
				$Html+=	'<div class="rows clean">';
				$Html+=		'<label class="color_000">'+lang_obj.manage.products.stock+'</label>';
				$Html+=		'<span class="input">';
								$Attr.each(function(){
									if($(this).find('.attr_current:checked').size()>0){
				$Html+=					'<p><input type="radio" name="BatchStock" value="'+$(this).attr('data-id')+'" /> '+lang_obj.manage.products.batch_notes.replace('%txt%', $(this).find('.tab_txt_'+ueeshop_config.lang+' .box_input').val())+'</p>';
									}
								});
				$Html+=		'</span>';
				$Html+=	'</div>';
				$Html+=	'<div class="button"><input type="button" class="btn btn_batch_submit" value="'+lang_obj.manage.global.save+'" /><input type="button" class="btn btn_batch_cancel" value="'+lang_obj.global.cancel+'" /></div>';
				$(this).next('.batch_edit').html($Html).show();
				
				$('.batch_edit .btn_batch_submit').off().on('click', function(){
					var obj=$(this).parents('.batch_edit'), data=new Object, input_data=new Object, $name, $value;
					input_data['Price']=3;
					input_data['Stock']=4;
					obj.find('.rows input:radio:checked').each(function(){
						$name=$(this).attr('name').replace('Batch', '');
						data[$name]=Object();
						data[$name]['ID']=$(this).val();
						data[$name]['No']=obj.parents('tr').attr('data-attr-id-'+$(this).val());
						data[$name]['Value']=obj.parents('tr').find('td:eq('+input_data[$name]+') input:text').val();
					});
					for(k in data){
						$value=k=='Price'?parseFloat(data[k]['Value']).toFixed(2):data[k]['Value'];
						$('#attribute_ext tr[data-attr-id-'+data[k]['ID']+'='+data[k]['No']+']').find('td:eq('+input_data[k]+') input:text').val($value);
					}
					$(this).parents('.batch_edit').html('').hide();
				});
				$('.batch_edit .btn_batch_cancel').off().on('click', function(){
					$(this).parents('.batch_edit').html('').hide();
				});
				$(document).off().on('click', function(e){
					if($('.batch_edit:visible').length){
						var $obj=$('.batch_edit:visible').parent();
						if(!(e.target==$obj[0] || $.contains($obj[0], e.target)) && $('.batch_edit:visible').length){
							$('.batch_edit').html('').hide();
						}
					}
				});
			});
		},
		
		//依据产品分类，显示产品属性选项
		attr_category_select:function(value){
			var $Status=0,
                $ProId=$('#ProId').val(),
				$Data={"do_action":"products.products_get_attr", "CateId":value, "ProId":$ProId, "IsCartAttr":0};
			$('#model_edit_form input[name=CateId]').val(value);
			$.post('?', $Data, function(data){ //普通属性
				if(data.ret==1){
					var $Html='', $Attr=new Object, $IsChecked=0;
					if(data.msg.Attr){
						$Attr=data.msg.Attr;
						for(k in $Attr){ //普通属性
							if($Attr[k]['Type']==1){
								//选项
								$Html+=	'<div class="box_attr clean" data-id="'+$Attr[k]['AttrId']+'"'+'>';
								$Html+=		'<div class="rows clean" data-id="'+$Attr[k]['AttrId']+'"'+'>';
								$Html+=			'<label>';
								$Html+=				'<strong>'+$Attr[k]['Name']+'</strong>';
								$Html+=				'<dl class="box_basic_more box_attr_basic_more">';
								$Html+=					'<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>';
								$Html+=					'<dd class="drop_down">';
								$Html+=						'<a href="javascript:;" class="item input_checkbox_box btn_option_edit"><span>'+lang_obj.manage.global.edit+'</span></a>';
								$Html+=						'<a href="javascript:;" class="item input_checkbox_box attr_delete"><span>'+lang_obj.global.del+'</span></a>';
								$Html+=					'</dd>';
								$Html+=				'</dl>';
								$Html+=			'</label>';
								$Html+=			'<div class="input">';
								$Html+=				'<div class="box_attr_list">';
								$Html+=					'<div class="attr_selected">';
								$Html+=						'<div class="select_list">';
															i=0;
															if($Attr[k]['Selected']){
																for(k2 in $Attr[k]['Selected'][ueeshop_config.lang]){
																	++i;
																	VId=$Attr[k]['Selected'][ueeshop_config.lang][k2]['VId'];
																	products_obj.data_init.attr_data[VId]=new Object;
																	products_obj.data_init.attr_data[VId]['Name']=new Object;
								$Html+=								products_obj.function_init.attr_option_button($Attr[k]['Selected'][ueeshop_config.lang][k2]['Name'], VId, $Attr[k]['AttrId'], 1);
																}
																for(lang in $Attr[k]['Selected']){ //选项名称多语言
																	for(k2 in $Attr[k]['Selected'][lang]){
																		VId=$Attr[k]['Selected'][lang][k2]['VId'];
																		products_obj.data_init.attr_data[VId]['Name'][lang]=$Attr[k]['Selected'][lang][k2]['Name'];
																	}
																}
															}
								$Html+=						'</div>';
								$Html+=						'<input type="text" class="box_input" name="_Attr" value="" size="30" maxlength="255" />';
								$Html+=						'<span class="placeholder'+(i>0?' hide':'')+'">'+lang_obj.manage.products.placeholder+'</span>';
								$Html+=					'</div>';
								$Html+=					'<div class="attr_not_yet">';
								$Html+=						'<div class="select_list">';
															if($Attr[k]['Option']){
																for(k2 in $Attr[k]['Option'][ueeshop_config.lang]){
																	VId=$Attr[k]['Option'][ueeshop_config.lang][k2]['VId'];
																	products_obj.data_init.attr_data[VId]=new Object;
																	products_obj.data_init.attr_data[VId]['Name']=new Object;
								$Html+=								products_obj.function_init.attr_option_button($Attr[k]['Option'][ueeshop_config.lang][k2]['Name'], VId, $Attr[k]['AttrId'], 0, 0);
																}
																for(lang in $Attr[k]['Option']){ //选项名称多语言
																	for(k2 in $Attr[k]['Option'][lang]){
																		VId=$Attr[k]['Option'][lang][k2]['VId'];
																		products_obj.data_init.attr_data[VId]['Name'][lang]=$Attr[k]['Option'][lang][k2]['Name'];
																	}
																}
															}
								$Html+=						'</div>';
								$Html+=						'<a href="javascript:;" class="select_all">'+lang_obj.global.select_all+'</a>';
								$Html+=					'</div>';
								$Html+=				'</div>';
								$Html+=			'</div>';
								$Html+=		'</div>';
								$Html+=		'<input type="hidden" name="AttrTitle['+$Attr[k].AttrId+']['+ueeshop_config.lang+']" value="'+$Attr[k].Name+'" />';
								$Html+=		'<input type="hidden" name="AttrCart['+$Attr[k].AttrId+']" value="0" class="attr_cart" />';
								$Html+=	'</div>';
							}else{
								//文本框
								$Html+=	'<div class="box_text clean">';
								$Html+=		'<div class="rows clean multi_lang" data-id="'+$Attr[k].AttrId+'">';
								$Html+=			'<label>';
								$Html+=				'<strong>'+$Attr[k].Name+'</strong>';
								$Html+=			'</label>';
								$Html+=			'<div class="input">';
								$Html+=				products_obj.function_init.attr_unit_form_edit($Attr[k].Option, 'text', 'AttrTxt['+$Attr[k].AttrId+']', 46, 255);
								$Html+=			'</div>';
								$Html+=			'<a href="javascript:;" class="attr_delete icon_delete_1"><i></i></a>';
								$Html+=		'</div>';
								$Html+=	'</div>';
							}
						}
					}
					$('.box_general_attribute').html($Html);
					$Html='';
					var $HeadHtml='';
					if(data.msg.Tab){
						$Tab=data.msg.Tab;
						for(k in data.msg.TabSort){ //描述属性
							$k=data.msg.TabSort[k];
							$HeadHtml+='<li data-id="'+$k+'"><span>'+$Tab[$k].Name+'</span><a href="javascript:;" class="attr_delete icon_delete_1"><i></i></a></li>';
							$Html+='<li data-id="'+$k+'">'+products_obj.function_init.attr_form_edit($Tab[$k].Option, 'editor', 'Tab_'+$k)+'</li>';
						}
					}
					$('.description_box .head ul li[data-id]').remove();
					$('.description_box .input ul li[data-id]').remove();
					$('.description_box .head ul').append($HeadHtml);
					$('.description_box .input ul').append($Html);
					products_obj.function_init.attr_load();
					products_obj.function_init.tab_load(); //加载产品描述属性事件
					$('.description_box .head li:eq(0)>span').click(); //默认勾选“详细介绍”
				}
                $Status+=1;
                if($Status>1){
                    $('.fixed_btn_submit .btn_submit').removeClass('btn_disabled');
                }
				return false;
			}, 'json');
			$Data['IsCartAttr']=1;
			$.post('?', $Data, function(data){ //规格属性
				if(data.ret==1){
					var $Html='', $Attr=new Object, $IsOverseas=0, $VId=0, $AttrCount=0, i=0,
						$IsOpenOverseas=parseInt($('#IsOpenOverseas').val());
					if(data.msg.Attr){
						$Attr=data.msg.Attr;
						for(k in $Attr){
							$IsOverseas=($Attr[k]['AttrId']=='Overseas'?1:0); //是否为海外仓选项
							if($IsOverseas==1){
								for(i in $Attr[k]['Option']){ //统计海外仓对象的数量
									$AttrCount+=1;
								}
							}else{
								$AttrCount=$Attr[k]['Option'].length;
							}
							$Html+=	'<div class="box_attr clean" data-id="'+$Attr[k]['AttrId']+'"'+($Attr[k]['AttrId']=='Overseas' && $IsOpenOverseas==0?' style="display:none;"':'')+'>';
							$Html+=		'<div class="rows clean">';
							$Html+=			'<label>';
							$Html+=				'<strong>'+$Attr[k]['Name']+'</strong>'+($Attr[k]['Color']==1?'<span>('+lang_obj.manage.products.master_pictures+')</span>':'');
												if($IsOverseas==0){ //颜色属性选项栏
							$Html+=					'<dl class="box_basic_more box_attr_basic_more">';
							$Html+=						'<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>';
							$Html+=						'<dd class="drop_down">';
							$Html+=							'<a href="javascript:;" class="item input_checkbox_box btn_option_edit"><span>'+lang_obj.manage.global.edit+'</span></a>';
							$Html+=							'<a href="javascript:;" class="item input_checkbox_box btn_associate_pic"><span>'+lang_obj.manage.products.pictures+'</span></a>';
							$Html+=							'<a href="javascript:;" class="item input_checkbox_box attr_delete"><span>'+lang_obj.global.del+'</span></a>';
							$Html+=						'</dd>';
							$Html+=					'</dl>';
												}
							$Html+=			'</label>';
							$Html+=			'<div class="input">';
							$Html+=				'<div class="box_attr_list">';
							$Html+=					'<div class="attr_selected">';
							$Html+=						'<div class="select_list">';
														i=0;
														if($Attr[k]['Selected']){
															for(k2 in $Attr[k]['Selected'][ueeshop_config.lang]){
																++i;
																VId=$Attr[k]['Selected'][ueeshop_config.lang][k2]['VId'];
																products_obj.data_init.attr_data[VId]=new Object;
																products_obj.data_init.attr_data[VId]['Info']=$Attr[k]['Selected'][ueeshop_config.lang][k2]['Info'];
																products_obj.data_init.attr_data[VId]['Data']=$Attr[k]['Selected'][ueeshop_config.lang][k2]['Data'];
																products_obj.data_init.attr_data[VId]['Name']=new Object;
							$Html+=								products_obj.function_init.attr_option_button($Attr[k]['Selected'][ueeshop_config.lang][k2]['Name'], VId, $Attr[k]['AttrId'], 1);
															}
															for(lang in $Attr[k]['Selected']){ //选项名称多语言
																for(k2 in $Attr[k]['Selected'][lang]){
																	VId=$Attr[k]['Selected'][lang][k2]['VId'];
																	products_obj.data_init.attr_data[VId]['Name'][lang]=$Attr[k]['Selected'][lang][k2]['Name'];
																}
															}
														}
							$Html+=						'</div>';
														if($IsOverseas==0){ //除了海外仓以外，都可以添加选项
							$Html+=							'<input type="text" class="box_input" name="_Attr" value="" size="30" maxlength="255" />';
														}
							$Html+=						'<span class="placeholder'+(i>0?' hide':'')+'">'+lang_obj.manage.products.placeholder+'</span>';
							$Html+=					'</div>';
							$Html+=					'<div class="attr_not_yet"'+($AttrCount==0?' style="display:none;"':'')+'>';
							$Html+=						'<div class="select_list">';
														if($Attr[k]['Option']){
															for(k2 in $Attr[k]['Option'][ueeshop_config.lang]){
																VId=$Attr[k]['Option'][ueeshop_config.lang][k2]['VId'];
																products_obj.data_init.attr_data[VId]=new Object;
																products_obj.data_init.attr_data[VId]['Info']=$Attr[k]['Option'][ueeshop_config.lang][k2]['Info'];
																products_obj.data_init.attr_data[VId]['Data']=$Attr[k]['Option'][ueeshop_config.lang][k2]['Data'];
																products_obj.data_init.attr_data[VId]['Name']=new Object;
							$Html+=								products_obj.function_init.attr_option_button($Attr[k]['Option'][ueeshop_config.lang][k2]['Name'], VId, $Attr[k]['AttrId'], 0, $IsOverseas);
															}
															for(lang in $Attr[k]['Option']){ //选项名称多语言
																for(k2 in $Attr[k]['Option'][lang]){
																	VId=$Attr[k]['Option'][lang][k2]['VId'];
																	products_obj.data_init.attr_data[VId]['Name'][lang]=$Attr[k]['Option'][lang][k2]['Name'];
																}
															}
														}
							$Html+=						'</div>';
							$Html+=						'<a href="javascript:;" class="select_all">'+lang_obj.global.select_all+'</a>';
							$Html+=					'</div>';
							$Html+=				'</div>';
							$Html+=			'</div>';
							$Html+=		'</div>';
							$Html+=		'<input type="hidden" name="AttrTitle['+$Attr[k].AttrId+']['+ueeshop_config.lang+']" value="'+$Attr[k].Name+'" />';
							$Html+=		'<input type="hidden" name="AttrCart['+$Attr[k].AttrId+']" value="1" class="attr_cart" />';
							$Html+=	'</div>';
						}
						products_obj.data_init.ext_attr_data=data.msg.Combinatin;
					}
					$('.box_cart_attribute').html($Html);
					products_obj.function_init.attr_load();
					products_obj.function_init.attr_price_show(); //加载完才触发
				}
                $Status+=1;
                if($Status>1){
                    $('.fixed_btn_submit .btn_submit').removeClass('btn_disabled');
                }
				return false;
			}, 'json');
		},
		
		//产品属性价格显示
		attr_price_show:function(){
			var OvId=(typeof(arguments[0])=='undefined' || arguments[0]==0)?0:arguments[0], //是否为海外仓选项
				IsCombination=parseInt($('input[name=IsCombination]:checked').val()), //是否组合属性
				checked_data=new Object,
				checked_data_len=0;
			//已选中属性选项
			$('.box_cart_attribute .rows').each(function(){
				var _This=$(this),
					_AttrId=$(this).parent().attr('data-id');
				_This.find('.attr_current:checked').each(function(){
					if(!checked_data[_AttrId]){ //还没生成数组
						checked_data[_AttrId]=new Array();
						checked_data[_AttrId][0]=$(this).val();
					}else{ //数组已存在
						checked_data[_AttrId][checked_data[_AttrId].length]=$(this).val();
					}
				});
			});
			//属性是否组合的关键点
			var key_ary=new Array(), attr_ary=ary_0=ary_1=new Array(), oversea_attr_ary=new Array(),
				oversea_attr_len=0;
			if(IsCombination==1){ //需要自动组合属性
				//自动组合可以搭配的组合
				for(k in checked_data){
					key_ary.push(k);
					checked_data_len+=1;
				}
				key_ary.sort(function(a,b){
					if(a.indexOf('Overseas')!=-1){
						a=99999999;
					}else if(a.indexOf('ADD:')!=-1){
						a=88888888;
					}
					if(b.indexOf('Overseas')!=-1){
						b=99999999;
					}else if(b.indexOf('ADD:')!=-1){
						b=88888888;
					}
					return a - b;
				});
				function CartAttr($arr, $oversea_attr, $num){
					var _arr=new Array();
					if($num==0){
						for(j in checked_data[key_ary[$num]]){
							if(key_ary[$num]=='Overseas'){ //海外仓
								$oversea_attr.push(checked_data[key_ary[$num]][j]);
							}else{
								$arr.push(checked_data[key_ary[$num]][j]);
							}
						}
					}else{
						for(i in $arr){
							if(key_ary[$num]=='Overseas'){ //海外仓
								_arr.push(String($arr[i])); //转换成字符串
							}else{
								for(j in checked_data[key_ary[$num]]){
									_arr.push($arr[i]+'_'+checked_data[key_ary[$num]][j]);
								}
							}
						}
						if(key_ary[$num]=='Overseas'){ //海外仓
							for(j in checked_data[key_ary[$num]]){
								$oversea_attr[checked_data[key_ary[$num]][j]]=_arr;
							}
						}
						$arr=_arr;
					}
					++$num;
					if($num<checked_data_len){
						CartAttr($arr, $oversea_attr, $num);
					}else{
						attr_ary=$arr;
						oversea_attr_ary=$oversea_attr;
					}
				}
				CartAttr(attr_ary, oversea_attr_ary, 0);
				//海外仓选项
				var html_oversea='';
				$('#attribute_ext_box .multi_tab_row.show').hide();
				if(!OvId){ //不是主动切换海外仓选项卡
					OvId=$('#attribute_ext_box .btn_multi_tab.current').attr('data-oversea'); //已有勾选选项
					if(!OvId){ //默认获取第一个
						OvId=$('#attribute_ext_box .btn_multi_tab:eq(0)').attr('data-oversea');
					}
					if(!OvId){ //默认
						OvId=1;
					}
				}
				for(i in oversea_attr_ary){ //统计海外仓对象的数量
					oversea_attr_len+=1;
				}
				if(oversea_attr_len>0 && attr_ary.length){ //海外仓选项 和 规格选项 都有 
					for(k in oversea_attr_ary){
						_k=parseInt(k.replace('Ov:', ''));
						html_oversea+='<a class="btn_multi_tab fl'+(OvId==_k?' current':'')+'" data-oversea="'+_k+'">'+products_obj.data_init.attr_data[k].Info.Name+'</a>';
					}
					$('#attribute_ext_box .multi_tab_row').html(html_oversea).show();
					$('#attribute_ext').addClass('tab_show');
					if($('#attribute_ext_box .btn_multi_tab').size()==1){ //海外仓选项仅有一个
						$('#attribute_ext_box .btn_multi_tab:eq(0)').addClass('current'); //默认选择第一个
						OvId=$('#attribute_ext_box .btn_multi_tab:eq(0)').attr('data-oversea');
					}
					if($('#attribute_ext_box .btn_multi_tab').size()>0 && $('#attribute_ext_box .btn_multi_tab.current').size()==0){ //丢失海外仓选项
						$('#attribute_ext_box .btn_multi_tab:eq(0)').addClass('current'); //默认选择第一个
						OvId=$('#attribute_ext_box .btn_multi_tab:eq(0)').attr('data-oversea');
					}
				}else{
					$('#attribute_ext_box .multi_tab_row').html('').hide();
				}
			}else{ //不需要组合属性
				for(k in checked_data){
					if(k=='Overseas'){ //海外仓
						oversea_attr_ary[k]=checked_data[k];
					}else{ //规格属性
						attr_ary[k]=checked_data[k];
					}
				}
				$('#attribute_ext_box .multi_tab_row').hide();
			}
			//生成表格内容
			$('#attribute_ext tbody').remove(); //清除所有表格内容
			var insert=products_obj.function_init.attr_price_row_show(attr_ary, oversea_attr_ary, checked_data);
			$('#attribute_ext .relation_box').append(insert).find('tr').addClass('group');
			frame_obj.check_amount($('#attribute_ext .relation_box'));
			if($('#AttrId_'+OvId).length){
				$('#AttrId_'+OvId).show();
			}else{
				$('#AttrId_1, #AttrId_0').show();
			}
			products_obj.function_init.attr_ext_option();
		},
		
		//tbody显示
		attr_price_row_show:function(attr_ary, oversea_attr_ary, checked_data){
			var html, _html, html_attr, ary_str, ary_merge, title, name, val_ary, IS, SKUV, PV, SV, WV,
				ary=new Object,
				object=new Object,
				html_tmp=$('#attribute_tmp .contents').html(),
				oversea_attr_len=0,
				OvId=1,
				IsCombination=($('input[name=IsCombination]').is(':checked')?1:0); //是否组合属性
			
			if(IsCombination==1){ //需要自动组合属性
				for(i in oversea_attr_ary){ //统计海外仓对象的数量
					oversea_attr_len+=1;
				}
				if(oversea_attr_len>0 && !attr_ary.length){ //仅有海外仓信息
					object['Ov:0']=oversea_attr_ary;
				}else if(oversea_attr_len==0 && attr_ary){ //没有海外仓信息
					object['Ov:1']=attr_ary;
				}else{
					object=oversea_attr_ary;
				}
				for(i in object){
					OvId=parseInt(i.replace('Ov:', ''));
					html+='<tbody id="AttrId_'+OvId+'">';
					
					var html_t=$('#attribute_tmp .column tbody').html();
					html+=html_t.replace('XXX',0).replace('Column', lang_obj.manage.products.group_attr);
						
					for(j in object[i]){
						//if(j>499) break;
						_html=html_tmp;
						html_attr='';
						name='';
						ary=object[i][j].split('_');
						ary.sort(function(a,b){ return a-b }); //重新排序，从小到大
						for(k in ary){
							name+=(k==0?'':' + ')+products_obj.data_init.attr_data[ary[k]].Info.Name.replace(/@8#/g, "'");
						}
						ary_str=ary.join('_');
						ary_merge='|'+ary.join('|')+'|';
						if(products_obj.data_init.ext_attr_data[ary_merge]){
							if(OvId>0){
								val_ary=products_obj.data_init.ext_attr_data[ary_merge][OvId];
							}else{
								val_ary=products_obj.data_init.ext_attr_data[ary_merge];
							}
						}
						if(OvId>0) ary_str+=('_'+i);
						for(k1 in checked_data){
							for(k2 in ary){
								if(global_obj.in_array(ary[k2], checked_data[k1])){
									html_attr+=' data-attr-id-'+k1+'='+ary[k2];
								}
							}
						}
						if(!val_ary) val_ary=['',0,0,0,0];
						SKUV= val_ary[0]?val_ary[0]:'';	//SKU
						IS	= val_ary[1]?val_ary[1]:0;	//加价
						PV	= val_ary[2]?val_ary[2]:0;	//价格
						SV	= val_ary[3]?val_ary[3]:0;	//库存
						WV	= val_ary[4]?val_ary[4]:0;	//重量
						if(IS==1){ //勾选
							_html=_html.replace('class="switchery"', 'class="switchery checked"').replace('type="checkbox"', 'type="checkbox" checked');
						}
						html+=_html.replace(/XXX/g, ary_str).replace('Name', name).replace('%SKUV%', SKUV).replace('%PV%', parseFloat(PV).toFixed(2)).replace('%SV%', SV).replace('%WV%', WV).replace('attr_txt=""', html_attr);
					}
					html+='</tbody>';
				}
			}else{ //不需要自动组合属性
				html+='<tbody id="AttrId_0">';
				var html_t=$('#attribute_tmp .column tbody').html();
				html+=html_t.replace('XXX',0).replace('Column', lang_obj.manage.products.group_attr);
				for(i in attr_ary){
					title=$('.box_cart_attribute .box_attr[data-id="'+i+'"] .box_input').val();
					$.trim(title)=='' && (title=$('.box_cart_attribute .box_attr[data-id="'+i+'"] label>strong').text());
					html+='<tr class="group_title"><td colspan="7">'+title+'</td></tr>';
					for(j in attr_ary[i]){
						_html=html_tmp;
						name=products_obj.data_init.attr_data[attr_ary[i][j]].Info.Name.replace(/@8#/g, "'");
						if(products_obj.data_init.ext_attr_data['|'+attr_ary[i][j]+'|']){
							val_ary=products_obj.data_init.ext_attr_data['|'+attr_ary[i][j]+'|'][1];
						}
						PV=val_ary?val_ary[2]:0; //价格
						html+=_html.replace(/XXX/g, attr_ary[i][j]).replace('Name', name).replace('%SKUV%', '').replace('%PV%', parseFloat(PV).toFixed(2));
					}
				}
				html+='</tbody>';
			}
			return html;
		}
	},
	//************************************************* 函数事件 End *************************************************
	
	//产品列表
	products_init:function(){
		frame_obj.del_init($('#products .r_con_table')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button').find('.del, .sold_in, .sold_out, .facebook, .bat_close')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'products.products_del_bat'); //批量删除
		//批量上架
		frame_obj.del_bat($('.list_menu_button .sold_in'), $('input[name=select]'), '', function(id_list){
			global_obj.win_alert(lang_obj.global.sold_in_confirm, function(){
				$.get('?', {do_action:'products.products_sold_bat', id:id_list, sold:0}, function(data){
					if(data.ret==1){
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
			return false;
		}, lang_obj.global.dat_select);
		//批量下架
		frame_obj.del_bat($('.list_menu_button .sold_out'), $('input[name=select]'), '', function(id_list){
			global_obj.win_alert(lang_obj.global.sold_out_confirm, function(){
				$.get('?', {do_action:'products.products_sold_bat', id:id_list, sold:1}, function(data){
					if(data.ret==1){
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
			return false;
		}, lang_obj.global.dat_select);
		//批量发布到Facebook店铺
		frame_obj.del_bat($('.list_menu_button .facebook_release'), $('input[name=select]'), '', function(id_list){
			global_obj.win_alert(lang_obj.global.facebook_release_confirm, function(){
				facebook_store_obj.products_upload(id_list);
			}, 'confirm');
			return false;
		}, lang_obj.global.dat_select);
		//批量从Facebook店铺删除
		frame_obj.del_bat($('.list_menu_button .facebook_delete'), $('input[name=select]'), '', function(id_list){
			global_obj.win_alert(lang_obj.global.facebook_delete_confirm, function(){
				facebook_store_obj.products_delete(id_list);
			}, 'confirm');
			return false;
		}, lang_obj.global.dat_select);
		frame_obj.select_all($('.list_menu_button input[name=custom_all]'), $('.list_menu_button input[class=custom_list][disabled!=disabled]')); //批量操作
		frame_obj.submit_form_init($('.list_menu_button .ico form'), './?m=products&a=products');
		$('#products .r_con_table .copy').click(function(){
			var $this=$(this);
			global_obj.win_alert(lang_obj.global.copy_confirm, function(){
				$.get($this.attr('href'), function(data){
					if(data.ret==1){
						window.location='./?m=products&a=products&d=edit&ProId='+data.msg;
					}
				}, 'json');
			}, 'confirm');
			return false;
		});

		$('.list_menu_button .bat_close').on('click', function(){
			var id_list='';
			$('input[name=select]').each(function(index, element) {
				id_list+=$(element).get(0).checked?$(element).val()+'-':'';
            });
			if(id_list){
				id_list=id_list.substring(0,id_list.length-1);
				window.location='./?m=products&a=products&d=batch_edit&proid_list='+id_list;
			}else{
				global_obj.win_alert(lang_obj.global.dat_select, function(){}, 'confirm');
				// window.location='./?m=products&a=products&d=batch_edit'
			}
		});
		$('#products .r_con_table .category_select').on('dblclick', function(){
			var $obj=$(this),
				$cateid=$obj.attr('cateid'),
				$ProId=$obj.parents('tr').find('td:eq(0) input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#category_select_hide').html(),
				$val;
			$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
			$cateid && $obj.find('select').val($cateid).focus();
			$obj.find('select').on('blur', function(){
				$val=$(this).val();
				if($val!=$cateid){
					$.post('?', 'do_action=products.products_edit_category&ProId='+$ProId+'&CateId='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.html(data.msg);
							$obj.attr('cateid', $val);
						}
					}, 'json');
				}else{
					$obj.html($obj.find('span').html());
				}
			});
		});
		$('#products .r_con_table .price_input>span').on('click', function(){
			var $obj=$(this),
				$price=$obj.attr('price'),
				$name=$obj.attr('class'),
				$ProId=$obj.parents('tr').find('td:eq(0) input').val(),
				pHtml, $val;
			if($obj.attr('data-status')!=1){
				$obj.attr('data-status', 1).addClass('cur');
				pHtml='<input name="'+$name+'" value="'+$price+'" type="text" class="box_input" size="5" maxlength="10" rel="amount" notnull />';
				$obj.find('span').html(pHtml);
				frame_obj.check_amount($obj);
				$obj.find('input').focus();
				$obj.find('input').on('blur', function(){
					$val=$(this).val();
					$.post('?', 'do_action=products.products_edit_price&ProId='+$ProId+'&Type='+$name+'&Price='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.attr('price', $val);
							$obj.find('input').remove();
							$obj.find('span').html('<em class="ajax_upload_btn">'+$val+'</em>');
	                        if(data.msg.Time){
	                            $obj.parents('tr').find('.time_data').html(data.msg.Time);
	                        }
						}
						$obj.attr('data-status', 0).removeClass('cur');
					}, 'json');
				});
			}
		});
		$('#products .r_con_table .myorder_select').on('click', function(){
			var $obj=$(this),
				$number=$obj.attr('data-num'),
				$ProId=$obj.parents('tr').find('td:eq(0) input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#myorder_select_hide').html(),
				$val;
			if($obj.attr('data-status')!=1){
				$obj.attr('data-status', 1);
				$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
				$number && $obj.find('select').val($number).focus();
				$obj.find('select').on('blur', function(){
					$val=$(this).val();
					if($val!=$number){
						$.post('?', 'do_action=products.products_edit_myorder&ProId='+$ProId+'&Number='+$(this).val(), function(data){
							if(data.ret==1){
								$obj.html('<em class="ajax_upload_btn">'+data.msg.MyOrder+'</em>');
								$obj.attr('data-num', $val);
	                            $obj.parents('tr').find('.time_data').html(data.msg.Time);
							}
						}, 'json');
					}else{
						$obj.html($obj.find('span').html());
					}
					$obj.attr('data-status', 0);
				});
			}
		});
		//勾选事件：默认执行
		$('.inside_table .r_con_table input:checkbox:not(.null)').each(function(){ //重新默认全部取消勾选
			if($(this).is(':checked')===true) $(this).prev().click();
		});
	},
    //供应商产品列表
    gysproducts_init:function(){
        frame_obj.del_init($('#products .r_con_table')); //删除提示
        frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button').find('.del, .sold_in, .sold_out, .facebook, .bat_close')); //批量操作
        frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'products.products_del_bat'); //批量删除
        //批量上架
        frame_obj.del_bat($('.list_menu_button .sold_in'), $('input[name=select]'), '', function(id_list){
            global_obj.win_alert(lang_obj.global.sold_in_confirm, function(){
                $.get('?', {do_action:'products.gysproducts_sold_bat', id:id_list, sold:1}, function(data){
                    if(data.ret==1){
                        window.location.reload();
                    }
                }, 'json');
            }, 'confirm');
            return false;
        }, lang_obj.global.dat_select);
        //批量下架
        frame_obj.del_bat($('.list_menu_button .sold_out'), $('input[name=select]'), '', function(id_list){
            global_obj.win_alert(lang_obj.global.sold_out_confirm, function(){
                $.get('?', {do_action:'products.gysproducts_sold_bat', id:id_list, sold:2}, function(data){
                    if(data.ret==1){
                        window.location.reload();
                    }
                }, 'json');
            }, 'confirm');
            return false;
        }, lang_obj.global.dat_select);
        //批量删除
        frame_obj.del_bat($('.list_menu_button .zhzdel'), $('input[name=select]'), '', function(id_list){
            global_obj.win_alert('准备删除吗', function(){
                $.get('?', {do_action:'products.gysproducts_del', id:id_list, sold:2}, function(data){
                    if(data.ret==1){
                        window.location.reload();
                    }
                }, 'json');
            }, 'confirm');
            return false;
        }, lang_obj.global.dat_select);
        //批量发布到Facebook店铺
        frame_obj.del_bat($('.list_menu_button .facebook_release'), $('input[name=select]'), '', function(id_list){
            global_obj.win_alert(lang_obj.global.facebook_release_confirm, function(){
                facebook_store_obj.products_upload(id_list);
            }, 'confirm');
            return false;
        }, lang_obj.global.dat_select);
        //批量从Facebook店铺删除
        frame_obj.del_bat($('.list_menu_button .facebook_delete'), $('input[name=select]'), '', function(id_list){
            global_obj.win_alert(lang_obj.global.facebook_delete_confirm, function(){
                facebook_store_obj.products_delete(id_list);
            }, 'confirm');
            return false;
        }, lang_obj.global.dat_select);
        frame_obj.select_all($('.list_menu_button input[name=custom_all]'), $('.list_menu_button input[class=custom_list][disabled!=disabled]')); //批量操作
        frame_obj.submit_form_init($('.list_menu_button .ico form'), './?m=products&a=products');
        $('#products .r_con_table .copy').click(function(){
            var $this=$(this);
            global_obj.win_alert(lang_obj.global.copy_confirm, function(){
                $.get($this.attr('href'), function(data){
                    if(data.ret==1){
                        window.location='./?m=products&a=products&d=edit&ProId='+data.msg;
                    }
                }, 'json');
            }, 'confirm');
            return false;
        });

        $('.list_menu_button .bat_close').on('click', function(){
            var id_list='';
            $('input[name=select]').each(function(index, element) {
                id_list+=$(element).get(0).checked?$(element).val()+'-':'';
            });
            if(id_list){
                id_list=id_list.substring(0,id_list.length-1);
                window.location='./?m=products&a=products&d=batch_edit&proid_list='+id_list;
            }else{
                global_obj.win_alert(lang_obj.global.dat_select, function(){}, 'confirm');
                // window.location='./?m=products&a=products&d=batch_edit'
            }
        });
        $('#products .r_con_table .category_select').on('dblclick', function(){
            var $obj=$(this),
                $cateid=$obj.attr('cateid'),
                $ProId=$obj.parents('tr').find('td:eq(0) input').val(),
                $mHtml=$obj.html(),
                $sHtml=$('#category_select_hide').html(),
                $val;
            $obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
            $cateid && $obj.find('select').val($cateid).focus();
            $obj.find('select').on('blur', function(){
                $val=$(this).val();
                if($val!=$cateid){
                    $.post('?', 'do_action=products.products_edit_category&ProId='+$ProId+'&CateId='+$(this).val(), function(data){
                        if(data.ret==1){
                            $obj.html(data.msg);
                            $obj.attr('cateid', $val);
                        }
                    }, 'json');
                }else{
                    $obj.html($obj.find('span').html());
                }
            });
        });
        $('#products .r_con_table .price_input>span').on('click', function(){
            var $obj=$(this),
                $price=$obj.attr('price'),
                $name=$obj.attr('class'),
                $ProId=$obj.parents('tr').find('td:eq(0) input').val(),
                pHtml, $val;
            if($obj.attr('data-status')!=1){
                $obj.attr('data-status', 1).addClass('cur');
                pHtml='<input name="'+$name+'" value="'+$price+'" type="text" class="box_input" size="5" maxlength="10" rel="amount" notnull />';
                $obj.find('span').html(pHtml);
                frame_obj.check_amount($obj);
                $obj.find('input').focus();
                $obj.find('input').on('blur', function(){
                    $val=$(this).val();
                    $.post('?', 'do_action=products.products_edit_price&ProId='+$ProId+'&Type='+$name+'&Price='+$(this).val(), function(data){
                        if(data.ret==1){
                            $obj.attr('price', $val);
                            $obj.find('input').remove();
                            $obj.find('span').html('<em class="ajax_upload_btn">'+$val+'</em>');
                            if(data.msg.Time){
                                $obj.parents('tr').find('.time_data').html(data.msg.Time);
                            }
                        }
                        $obj.attr('data-status', 0).removeClass('cur');
                    }, 'json');
                });
            }
        });
        $('#products .r_con_table .myorder_select').on('click', function(){
            var $obj=$(this),
                $number=$obj.attr('data-num'),
                $ProId=$obj.parents('tr').find('td:eq(0) input').val(),
                $mHtml=$obj.html(),
                $sHtml=$('#myorder_select_hide').html(),
                $val;
            if($obj.attr('data-status')!=1){
                $obj.attr('data-status', 1);
                $obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
                $number && $obj.find('select').val($number).focus();
                $obj.find('select').on('blur', function(){
                    $val=$(this).val();
                    if($val!=$number){
                        $.post('?', 'do_action=products.products_edit_myorder&ProId='+$ProId+'&Number='+$(this).val(), function(data){
                            if(data.ret==1){
                                $obj.html('<em class="ajax_upload_btn">'+data.msg.MyOrder+'</em>');
                                $obj.attr('data-num', $val);
                                $obj.parents('tr').find('.time_data').html(data.msg.Time);
                            }
                        }, 'json');
                    }else{
                        $obj.html($obj.find('span').html());
                    }
                    $obj.attr('data-status', 0);
                });
            }
        });
        //勾选事件：默认执行
        $('.inside_table .r_con_table input:checkbox:not(.null)').each(function(){ //重新默认全部取消勾选
            if($(this).is(':checked')===true) $(this).prev().click();
        });
    },
	//产品导出
	explode_init:function(){
		var $Form=$('#explode_edit_form');
		frame_obj.submit_form_init($Form, '', '', '', function(data){
			if(data.ret==2){
				$('#explode_progress').append(data.msg[1]);
				$Form.find('input[name=Number]').val(data.msg[0]);
				$Form.find('.btn_submit').removeAttr('disabled').click();
			}else if(data.ret==1){ //初始化，打包压缩包
				$Form.find('input:submit').removeAttr('disabled');
				$Form.find('input[name=Number]').val(0);
				window.location='./?do_action=products.products_explode_down&Status=ok';
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
			}
		});
	},
	
	//批量修改
	batch_edit_init:function(){
		if($('#batch_edit_form').size()){
			var $Form=$('#batch_edit_form');
			$Form.find('.type .btn_choice').click(function(){
				var $this=$(this);
				$this.addClass('current').siblings().removeClass('current');
				$this.children('input').attr('checked', true);
				$Form.find('.choice_box .rows').eq($this.index()).show().siblings().hide();
			});
			if($Form.attr('name')=='batch_watermark_form'){ //批量更新水印
				frame_obj.submit_form_init($Form, '', '', '', function(data){
					if(data.ret==2){
						$('#explode_progress').append(data.msg[1]);
						$Form.find('input[name=Number]').val(data.msg[0]);
						$Form.find('.btn_submit').removeAttr('disabled').click();
					}else if(data.ret==1){
						$('#explode_progress').append(data.msg);
					}else{
						global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
					}
				});
			}else{
				frame_obj.submit_form_init($Form);
			}
		}else{
			$('.r_con_form input[name=SoldOutTime\\[\\]]').daterangepicker({showDropdowns:true});
			frame_obj.switchery_checkbox(function(obj){
				if(obj.find('.SoldOutInput').length){
					obj.nextAll('#sold_out_div').css('display', '');
				}else if(obj.find('.IsSoldOutInput').length){
					obj.nextAll('.sold_in_time').css('display', '');
				}
			}, function(obj){
				if(obj.find('.SoldOutInput').length){
					obj.nextAll('#sold_out_div').css('display', 'none');
				}else if(obj.find('.IsSoldOutInput').length){
					obj.nextAll('.sold_in_time').css('display', 'none');
				}
			});
			$('.r_con_form .other .choice_btn').click(function(){
				var $this=$(this);
				if($this.children('input').is(':checked')){
					$this.removeClass('current');
					$this.children('input').attr('checked', false);
				}else{
					$this.addClass('current');
					$this.children('input').attr('checked', true);
				}
			});
			frame_obj.submit_form_init($('form[name=batch_form]'), '', '', '', function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
				$('form[name=batch_form] input:submit').attr('disabled', false);
			});
		}
	},
	
	//批量上传
	upload_init:function(){
		var $Form=$('#upload_edit_form');
		$Form.fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
			callback: function(filepath, count){
				$('#excel_path').val(filepath);
			}
		});
		$Form.fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
		
		frame_obj.submit_form_init($Form, '', function(){
			$('#form_container').hide();
			$('#progress_container').show();
			$('.center_container_1200').removeClass('center_container');
			return true;
		}, '', function(data){
			if(data.ret==2){
				//继续产品资料上传
				var $Html='', $Number=0;
				if($('#progress_container tbody tr').size()){
					$Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
				}
				if(data.msg.Data){
					for(k in data.msg.Data){
						$Html+='<tr class="'+(data.msg.Data[k].Status==0?'error':'success')+'" data-id="'+data.msg.Data[k].ProId+'" data-pic-status="0">\
									<td nowrap="nowrap">'+($Number+parseInt(k))+'</td>\
									<td nowrap="nowrap"><div class="name" title="'+data.msg.Data[k].Name+'">'+data.msg.Data[k].Name+'</div></td>\
									<td nowrap="nowrap">'+data.msg.Data[k].Number+'</td>\
									<td nowrap="nowrap" data-type="status">'+(data.msg.Data[k].Status==1?'<div class="loading"></div>':'')+'</td>\
									<td nowrap="nowrap" data-type="message">'+data.msg.Data[k].Tips+'</td>\
								</tr>';
					}
					$('#progress_container tbody').append($Html);
					$Form.find('input[name=Number]').val(data.msg.Num);
					$Form.find('input[name=Current]').val(data.msg.Cur);
					$Form.find('.btn_submit').removeAttr('disabled').click();
				}
			}else if(data.ret==1){
				//产品资料上传完成
				$('.btn_picture').click(); //开始图片上传
				$('#progress_loading').addClass('completed').html(data.msg);
			}else{
				global_obj.win_alert_auto_close(data.msg, 'fail', 2000, '8%');
				$('#form_container').show();
				$('#progress_container').hide();
				$('.center_container_1200').addClass('center_container');
			}
		});
		
		$('.btn_picture').on('click', function(){
			var $Obj=$('#progress_container tbody'),
				checkTime=60*1000; //检查时间上限
				checkFun=''; //储存检查程序代码
				$ProId=0;
			$Obj.find('tr.success[data-id>0][data-pic-status=0]:eq(0)').each(function(){
				$(this).find('td[data-type="message"]').html(lang_obj.manage.products.upload.uploading); //图片上传中
				$ProId=$(this).attr('data-id');
			});
			if($ProId){
				$.post('?do_action=products.upload_picture', {'ProId':$ProId}, function(data){
					clearTimeout(checkFun);
					if(data.ret==1){ //导入成功
						$Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 1).find('td[data-type="status"]>div').attr('class', 'completed');
						$Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.imported); //导入完成
					}else{ //导入失败
						$Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
						$Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
						$Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
					}
					if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
						$('.btn_picture').click();
					}else{
						$('#progress_loading').addClass('completed').html(lang_obj.manage.products.upload.imported_all).append('<a href="./?m=products&a=products" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
					}
				}, 'json');
				//超过1分钟后，没有任何返回结果，系统自动识别为执行失败
				checkFun=setTimeout(function(){
					$Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
					$Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
					$Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
					if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
						$('.btn_picture').click();
					}
				}, checkTime);
			}
		});
	},
	//分类批量上传zhz
    bcupload_init:function(){
        var $Form=$('#upload_edit_form');
        $Form.fileupload({
            url: '/manage/?do_action=action.file_upload_plugin&size=file',
            acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
            callback: function(filepath, count){
                $('#excel_path').val(filepath);
            }
        });
        $Form.fileupload(
            'option',
            'redirect',
            window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
        );

        frame_obj.submit_form_init($Form, '', function(){
            $('#form_container').hide();
            $('#progress_container').show();
            $('.center_container_1200').removeClass('center_container');
            return true;
        }, '', function(data){
            if(data.ret==2){
                //继续产品资料上传
				console.log(data);
                var $Html='', $Number=0;
                if($('#progress_container tbody tr').size()){
                    $Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
                }
                if(data.msg){
                    for(k in data.msg){
                        $Html+='<tr>\
									<td nowrap="nowrap">'+data.msg[k].data+'</td>\
									<td nowrap="nowrap"><div class="name">'+data.msg[k].s+'</div></td>\
								</tr>';
                    }
                    $('#progress_container tbody').append($Html);
                    // $Form.find('input[name=Number]').val(data.msg.Num);
                    // $Form.find('input[name=Current]').val(data.msg.Cur);
                    // $Form.find('.btn_submit').removeAttr('disabled').click();
                }
            }else if(data.ret==1){
                //产品资料上传完成
                $('.btn_picture').click(); //开始图片上传
                $('#progress_loading').addClass('completed').html(data.msg);
            }else{
                global_obj.win_alert_auto_close(data.msg, 'fail', 2000, '8%');
                $('#form_container').show();
                $('#progress_container').hide();
                $('.center_container_1200').addClass('center_container');
            }
        });

        $('.btn_picture').on('click', function(){
            var $Obj=$('#progress_container tbody'),
                checkTime=60*1000; //检查时间上限
            checkFun=''; //储存检查程序代码
            $ProId=0;
            $Obj.find('tr.success[data-id>0][data-pic-status=0]:eq(0)').each(function(){
                $(this).find('td[data-type="message"]').html(lang_obj.manage.products.upload.uploading); //图片上传中
                $ProId=$(this).attr('data-id');
            });
            if($ProId){
                $.post('?do_action=products.upload_picture', {'ProId':$ProId}, function(data){
                    clearTimeout(checkFun);
                    if(data.ret==1){ //导入成功
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 1).find('td[data-type="status"]>div').attr('class', 'completed');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.imported); //导入完成
                    }else{ //导入失败
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    }
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }else{
                        $('#progress_loading').addClass('completed').html(lang_obj.manage.products.upload.imported_all).append('<a href="./?m=products&a=products" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
                    }
                }, 'json');
                //超过1分钟后，没有任何返回结果，系统自动识别为执行失败
                checkFun=setTimeout(function(){
                    $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }
                }, checkTime);
            }
        });
    },
    //供应商产品批量上传zhz
    bpupload_init:function(){
        var $Form=$('#upload_edit_form');
        $Form.fileupload({
            url: '/manage/?do_action=action.file_upload_plugin&size=file',
            acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
            callback: function(filepath, count){
                $('#excel_path').val(filepath);
            }
        });
        $Form.fileupload(
            'option',
            'redirect',
            window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
        );

        frame_obj.submit_form_init($Form, '', function(){
            $('#form_container').hide();
            $('#progress_container').show();
            $('.center_container_1200').removeClass('center_container');
            return true;
        }, '', function(data){
            if(data.ret==2){
                //继续产品资料上传
                console.log(data);
                var $Html='', $Number=0;
                if($('#progress_container tbody tr').size()){
                    $Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
                }
                if(data.msg){
                    for(k in data.msg){
                        $Html+='<tr>\
									<td nowrap="nowrap">'+data.msg[k].data+'</td>\
									<td nowrap="nowrap"><div class="name">'+data.msg[k].s+'</div></td>\
								</tr>';
                    }
                    $('#progress_container tbody').append($Html);
                    // $Form.find('input[name=Number]').val(data.msg.Num);
                    // $Form.find('input[name=Current]').val(data.msg.Cur);
                    // $Form.find('.btn_submit').removeAttr('disabled').click();
                }
            }else if(data.ret==1){
                //产品资料上传完成
                $('.btn_picture').click(); //开始图片上传
                $('#progress_loading').addClass('completed').html(data.msg);
            }else{
                global_obj.win_alert_auto_close(data.msg, 'fail', 2000, '8%');
                $('#form_container').show();
                $('#progress_container').hide();
                $('.center_container_1200').addClass('center_container');
            }
        });

        $('.btn_picture').on('click', function(){
            var $Obj=$('#progress_container tbody'),
                checkTime=60*1000; //检查时间上限
            checkFun=''; //储存检查程序代码
            $ProId=0;
            $Obj.find('tr.success[data-id>0][data-pic-status=0]:eq(0)').each(function(){
                $(this).find('td[data-type="message"]').html(lang_obj.manage.products.upload.uploading); //图片上传中
                $ProId=$(this).attr('data-id');
            });
            if($ProId){
                $.post('?do_action=products.upload_picture', {'ProId':$ProId}, function(data){
                    clearTimeout(checkFun);
                    if(data.ret==1){ //导入成功
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 1).find('td[data-type="status"]>div').attr('class', 'completed');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.imported); //导入完成
                    }else{ //导入失败
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    }
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }else{
                        $('#progress_loading').addClass('completed').html(lang_obj.manage.products.upload.imported_all).append('<a href="./?m=products&a=products" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
                    }
                }, 'json');
                //超过1分钟后，没有任何返回结果，系统自动识别为执行失败
                checkFun=setTimeout(function(){
                    $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }
                }, checkTime);
            }
        });
    },
    //供应商批量上传zhz
    bupload_init:function(){
        var $Form=$('#upload_edit_form');
        $Form.fileupload({
            url: '/manage/?do_action=action.file_upload_plugin&size=file',
            acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
            callback: function(filepath, count){
                $('#excel_path').val(filepath);
            }
        });
        $Form.fileupload(
            'option',
            'redirect',
            window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
        );

        frame_obj.submit_form_init($Form, '', function(){
            $('#form_container').hide();
            $('#progress_container').show();
            $('.center_container_1200').removeClass('center_container');
            return true;
        }, '', function(data){
            if(data.ret==2){
                //继续产品资料上传
                console.log(data);
                var $Html='', $Number=0;
                if($('#progress_container tbody tr').size()){
                    $Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
                }
                if(data.msg){
                    for(k in data.msg){
                        $Html+='<tr>\
									<td nowrap="nowrap">'+data.msg[k].data+'</td>\
									<td nowrap="nowrap"><div class="name">'+data.msg[k].s+'</div></td>\
								</tr>';
                    }
                    $('#progress_container tbody').append($Html);
                    // $Form.find('input[name=Number]').val(data.msg.Num);
                    // $Form.find('input[name=Current]').val(data.msg.Cur);
                    // $Form.find('.btn_submit').removeAttr('disabled').click();
                }
            }else if(data.ret==1){
                //产品资料上传完成
                $('.btn_picture').click(); //开始图片上传
                $('#progress_loading').addClass('completed').html(data.msg);
            }else{
                global_obj.win_alert_auto_close(data.msg, 'fail', 2000, '8%');
                $('#form_container').show();
                $('#progress_container').hide();
                $('.center_container_1200').addClass('center_container');
            }
        });

        $('.btn_picture').on('click', function(){
            var $Obj=$('#progress_container tbody'),
                checkTime=60*1000; //检查时间上限
            checkFun=''; //储存检查程序代码
            $ProId=0;
            $Obj.find('tr.success[data-id>0][data-pic-status=0]:eq(0)').each(function(){
                $(this).find('td[data-type="message"]').html(lang_obj.manage.products.upload.uploading); //图片上传中
                $ProId=$(this).attr('data-id');
            });
            if($ProId){
                $.post('?do_action=products.upload_picture', {'ProId':$ProId}, function(data){
                    clearTimeout(checkFun);
                    if(data.ret==1){ //导入成功
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 1).find('td[data-type="status"]>div').attr('class', 'completed');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.imported); //导入完成
                    }else{ //导入失败
                        $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                        $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    }
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }else{
                        $('#progress_loading').addClass('completed').html(lang_obj.manage.products.upload.imported_all).append('<a href="./?m=products&a=products" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
                    }
                }, 'json');
                //超过1分钟后，没有任何返回结果，系统自动识别为执行失败
                checkFun=setTimeout(function(){
                    $Obj.find('tr[data-id='+$ProId+']').attr('data-pic-status', 2).addClass('error');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="status"]>div').removeAttr('class');
                    $Obj.find('tr[data-id='+$ProId+'] td[data-type="message"]').html(lang_obj.manage.products.upload.upload_failed); //图片导入失败
                    if($Obj.find('tr.success[data-id>0][data-pic-status=0]').length>0){ //继续导入其他产品图片
                        $('.btn_picture').click();
                    }
                }, checkTime);
            }
        });
    },
	//产品编辑详情
	products_edit_init:function(){
		//翻译器加载
		frame_obj.translation_init();
		
		//定时上架时间
		$('#edit_form input[name=SoldOutTime]').daterangepicker({showDropdowns:true});
		
		//产品分类选择
		$('#edit_form select[name=CateId]').on('change', function(){products_obj.function_init.attr_category_select($(this).val());});
		
		//多分类按钮
		$('#btn_expand').on('click', function(){
			var obj=$(this).nextAll('.expand_list');
			var category_sel=$('#edit_form select[name=CateId]').html();
			obj.append('<li><div class="box_select"><select name="ExtCateId[]" notnull>'+category_sel.replace(' selected','')+'</select></div><a class="close icon_delete_1" href="javascript:;"><i></i></a></li>');
			$('.expand_list .close').on('click', function(){
				$(this).parent().remove();
			});
		});
		$('.expand_list .close').on('click', function(){
			$(this).prev().remove();
			$(this).remove();
		});
		
		//图片上传
		frame_obj.mouse_click($('.multi_img .img .upload_btn, .pic_btn .edit'), 'pro', function($this){ //产品主图点击事件
			var $num=$this.parents('.img').attr('num');
			frame_obj.photo_choice_init('PicDetail .img[num;'+$num+']', 'products', ueeshop_config.prodImageCount, 'do_action=products.products_img_del&Model=products', 1, "frame_obj.upload_pro_img_init(1, '', 1);");
		});
		$('.multi_img input[name=PicPath\\[\\]]').each(function(){
			if($(this).attr('save')==1){
				$(this).parent().find('img').attr('src', $(this).attr('data-value')); //直接替换成缩略图
			}
		});
		frame_obj.dragsort($('.multi_img'), '', 'dl', '.video, .show_btn', '<dl class="img placeHolder"></dl>'); //图片拖动
		frame_obj.mouse_click($('.pic_btn .del'), 'proDel', function($this){ //产品主图删除点击事件
			var $obj=$this.parents('.img'),
				$num=parseInt($obj.attr('num')),
				$path=$obj.find('input[name=PicPath\\[\\]]').val();
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.ajax({
					url:'./?do_action=products.products_img_del&Model=products&Path='+$path+'&Index='+$num+'&ProId='+$('#ProId').val(),
					success:function(data){
						$obj.removeClass('isfile').removeClass('show_btn');
						if($('.multi_img dl.video').index()){
							$obj.parent().find('.video').before($obj);
						}else{
							$obj.parent().append($obj);
						}
						$obj.find('.preview_pic .upload_btn').show();
						$obj.find('.preview_pic a').remove();
						$obj.find('.preview_pic input:hidden').val('').attr('save', 0);
						frame_obj.upload_pro_img_init(1);
						var $i=0;
						$obj.parent().find('.img').each(function(){
							$(this).attr('num', $i);
							++$i;
						});
					}
				});
			}, 'confirm');
		});
		
		//视频事件
		frame_obj.fixed_right($('.multi_img .video .upload_btn'), '.fixed_video');
		
		//开启参数
		$('.box_basic_more').on('click', '.btn_open_unit', function(){
			if($(this).find('input').is(':checked')){
				$('#prod_info_unit').hide();
			}else{
				$('#prod_info_unit').show();
			}
		}).on('click', '.btn_open_sku', function(){
			if($(this).find('input').is(':checked')){
				$('#prod_info_sku').hide();
			}else{
				$('#prod_info_sku').show();
			}
		});
		
		//单位
		$('.unit_box .add_unit').click(function(){
			var $obj=$('.unit_box');
			if($obj.hasClass('show')){
				$obj.removeClass('show');
			}else{
				$obj.addClass('show');
			}
		});
		$('.unit_box .list .item').click(function(){
			$('input[name=Unit]').val($(this).find('span').text());
			$('.unit_box').removeClass('show');
		});
		$('.unit_box .list .item>em').click(function(){
			var o=$(this), key=o.parent().attr('data-key');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.post('?', {"do_action":"products.products_unit_del", "Key":key, "Unit":o.prev().text()}, function(data){
					json=eval('('+data+')');
					if(json.ret==1){
						o.parent().remove();
					}
					return false;
				});
			}, 'confirm');
			return false;
		});
		
		//勾选按钮
		frame_obj.switchery_checkbox(function(obj){
			if(obj.find('input[name=SoldOut]').length){
				//上下架
				$('#sold_out_div').css('display', 'none');
			}else if(obj.find('input[name=IsSoldOut]').length){
				//定时上架
				obj.nextAll('.sold_in_time').css('display', '');
			}else if(obj.find('input[name=IsPacking]').length){
				$('#packing_div').css('display', '');
			}else if(obj.find('input[name=IsCombination]').length){
				//显示多规格模式
				$('#attribute_ext thead').find('td:eq(1), td:eq(2), td:gt(3)').show();
				$('#attribute_tmp tbody').find('td:eq(1), td:eq(2), td:gt(3)').show();
				products_obj.function_init.attr_price_show();
				$('#attribute_ext_box').css('display', 'block');
				$('#attribute_unit_box, .box_combination').css('display', 'none');
				$('#attribute_unit_box input[name=Stock]').removeAttr('notnull');
			}else if(obj.find('input[name=IsOpenAttrPrice]').length){
				//开启属性价格
				$('#attribute_ext thead').find('td:eq(1), td:eq(2), td:gt(3)').hide();
				$('#attribute_tmp tbody').find('td:eq(1), td:eq(2), td:gt(3)').hide();
				products_obj.function_init.attr_price_show();
				$('.box_combination, #attribute_ext_box').css('display', 'block');
			}else if(obj.find('input[name=IsFreeShipping]').length){
				//免运费
				$('.box_volume_weight').css('display', 'none');
			}else if(obj.find('input[name=IsColor]').length){
				//设置为主图
				var $Form=$('#edit_picture_form');
				$Form.find('.edit_picture_list .item').each(function(){
					$(this).find('.img[num!=0]').show();
				});
			}
		}, function(obj){
			if(obj.find('input[name=SoldOut]').length){
				//上下架
				$('#sold_out_div').css('display', '');
			}else if(obj.find('input[name=IsSoldOut]').length){
				//定时上架
				obj.nextAll('.sold_in_time').css('display', 'none');
			}else if(obj.find('input[name=IsPacking]').length){
				$('#packing_div').css('display', 'none');
			}else if(obj.find('input[name=IsCombination]').length){
				//隐藏多规格模式
				if(!$('input[name=IsOpenAttrPrice]').attr('checked')){
					$('#attribute_ext_box').css('display', 'none');
				}else{
					$('#attribute_ext thead').find('td:eq(1), td:eq(2), td:gt(3)').hide();
					$('#attribute_tmp tbody').find('td:eq(1), td:eq(2), td:gt(3)').hide();
					products_obj.function_init.attr_price_show();
				}
				$('#attribute_unit_box, .box_combination').css('display', 'block');
				$('#attribute_unit_box input[name=Stock]').attr('notnull', 'notnull');
			}else if(obj.find('input[name=IsOpenAttrPrice]').length){
				//关闭属性价格
				$('#attribute_ext_box').css('display', 'none');
			}else if(obj.find('input[name=IsFreeShipping]').length){
				//免运费
				$('.box_volume_weight').css('display', 'inline-block');
			}else if(obj.find('input[name=IsColor]').length){
				//设置为主图
				var $Form=$('#edit_picture_form');
				$Form.find('.edit_picture_list .item').each(function(){
					$(this).find('.img[num!=0]').hide();
				});
			}
		});
		
		//属性价格开启
		if($('#IsCombination').val()==1){
			$('.open_attr_price .switchery').click();
		}
		
		//平台导流
		function hide_add_form(){ //添加按钮
			if(!$('.platform_list li.hide').length){
				$('.platform_default,.btn_platform').hide();
			}else{
				$('.platform_default,.btn_platform').show();
			}
		}
		$('.btn_platform').on('click', function(){
			var $obj=$('.platform_default');
			if($obj.is(':hidden')){ //优先显示出添加选项
				$obj.show();
				return false;
			}
			var $value=$('select[name=PlatformType]').val(),
				$SecVal='';
			if($value!=-1){
				if($('select[name=PlatformType] option:selected').attr('data-sec')){
					$SecVal=$('.PlatformTypeSec[data-type="'+$value+'"] select option:selected').val();
				}
				$('.platform_list>li[data-type=platform_'+($SecVal?$SecVal:$value)+']').removeClass('hide');
				$('.platform_list>li[data-type=platform_default] .lang_txt').each(function(key, value){
					$('.platform_list>li[data-type=platform_'+($SecVal?$SecVal:$value)+'] .lang_txt').eq(key).find('textarea').val($(this).find('textarea').val());
				});
				if($('select[name=PlatformType] option:selected').attr('data-sec')){
					$('select[name=PlatformTypeSec]>option[value='+$SecVal+']').addClass('hide').attr('disabled', true).removeAttr('selected');
					if($('.PlatformTypeSec[data-type="'+$value+'"] select option.hide').length==$('.PlatformTypeSec[data-type="'+$value+'"] select option').length){
						$('select[name=PlatformType]>option[value='+$value+']').addClass('hide').attr('disabled', true);
					}
				}else{
					$('select[name=PlatformType]>option[value='+$value+']').addClass('hide').attr('disabled', true);
				}
				$('select[name=PlatformType]').val('-1').change();
			}
			hide_add_form();
		});

		$('select[name=PlatformType]').change(function(){
			var $type=$(this).find('option:selected').val();
			$('.PlatformTypeSec').hide();
			$('.PlatformTypeSec[data-type="'+$type+'"]').show();
		});

		$('.platform_list .del').on('click', function(){
			var $value=$(this).attr('data-type'),
				$top=$(this).attr('data-top');
			$('.platform_list>li[data-type=platform_'+$value+']').addClass('hide').find('textarea').val('');
			$('select[name=PlatformType]>option[value='+($top?$top:$value)+']').removeClass('hide').removeAttr('disabled');
			if($top){
				$('select[name=PlatformTypeSec]>option[value='+$value+']').removeClass('hide').removeAttr('disabled');
			}
			hide_add_form();
		});
		hide_add_form();

		$(window).resize(function(){
			$('.platform_list>li:visible .lang_txt .unit_input').each(function(){
				var $this=$(this),
					$width=$(this).width(),
					$b_w=$this.find('b').outerWidth(true);
				$this.find('textarea').css('width',$width-$b_w-21+'px');
			});
		}).resize();

		//状态选项
		$('select[name=SoldOut]').change(function(){
			var $value=$(this).val();
			if($value==1){ //下架
				$('#sold_out_div').show();
				$('#arrival_notice_div').hide();
			}else if($value==2){ //脱销
				$('#sold_out_div').hide();
				$('#arrival_notice_div').show();
			}else{ //上架
				$('#sold_out_div, #arrival_notice_div').hide();
			}
		});
		
		$('.right_container .big_title').on('click', function(){
			var $This=$(this);
			if($This.find('i').size()){
				if($This.next().is(':hidden')){
					$This.find('i').addClass('current');
					$This.next().slideDown();
				}else{
					$This.find('i').removeClass('current');
					$This.next().slideUp();
				}
			}
			$(window).resize();
		});
		
		//批发价事件
		$('#add_wholesale').click(function(){//添加条件
			var $count=$('#wholesale_list').find('tr').size();
			if($count>4){ //不能超过5个
				global_obj.win_alert_auto_close(lang_obj.manage.products.price_count, 'fail', 1000, '8%');
				return false;
			}
			var $newrow=document.getElementById('wholesale_list').insertRow(-1),
				$Unit=$('#wholesale_list').attr('data-unit');
				$Html='';
			$newcell=$newrow.insertCell(-1);
			$Html+=	'<span class="d_list">';
			$Html+=		($count==0?'<span class="d_tit color_000">'+lang_obj.manage.products.qty+'</span>':'<span class="d_empty"></span>');
			$Html+=		'<span class="unit_input"><input name="Qty[]" value="0" type="text" class="box_input" maxlength="10" size="5">'+($Unit?'<b class="last">'+$Unit+'</b>':'')+'</span>';
			$Html+=	'</span>';
			$Html+=	'<span class="d_list money">';
			$Html+=		($count==0?'<span class="d_tit color_000">'+lang_obj.manage.products.price+'</span>':'<span class="d_empty"></span>');
			$Html+=		'<span class="unit_input"><b>'+ueeshop_config.currSymbol+'</b><input name="Price[]" value="0" type="text" class="box_input" maxlength="10" size="5"></span>';
			$Html+=	'</span>';
			$Html+=	'&nbsp;<a class="d_del fl icon_delete_1'+($count>0?' d_del_empty':'')+'" href="javascript:;"><i></i></a>';
			$Html+=	'<div class="clear"></div>';									
			$newcell.innerHTML=$Html;
		});
		$('body, html').on('click', '#wholesale_list .d_del', function(){
			$(this).parents('tr').remove();
		});
		
		//多语言事件
		$('#edit_form').on('focus', '.multi_lang .box_input, .multi_lang .box_textarea', function(){
			$('.multi_lang').each(function(){
				$(this).find('.lang_txt').not('[data-default="1"]').hide();
			});
			$(this).parents('.multi_lang').find('.lang_txt').show();
			if($(this).hasClass('box_textarea')){
				$(window).resize();
			}
		});
		$('html').click(function(e){
			if(!$(e.target).parents('.lang_txt').length){
				$('.multi_lang').each(function(){
					$(this).find('.lang_txt').not('[data-default="1"]').hide();
				});
			}
		});
		
		//******************************** SEO Start ********************************
		//SEO编辑
		$('#edit_seo_list').click(function(){
			var $This=$(this),
				$Seo=$('.seo_box'),
				$SeoInfo=$('.seo_info_box'),
				$PageUrl=$Seo.find('.box_textarea[name=PageUrl]'),
				$Ary=new Array('title', 'description', 'keyword', 'url'),
				$Obj='', $Val='', $i=0, $j=0;
			if($This.attr('data-save')==0){ //编辑
				$This.text(lang_obj.global.pack_up).attr('data-save', 1);
				$SeoInfo.hide();
				$Seo.show();
				if($.trim($PageUrl.val())==''){
					$PageUrl.val($PageUrl.attr('data-content')).css('color', '#aaa');
				}
			}else{ //收起
				$SeoInfo.show();
				$Seo.hide();
				$This.text(lang_obj.global.edit).attr('data-save', 0);
				for(i in $Ary){ //注入内容
					$Obj=$Seo.find('div[data-name='+$Ary[i]+']').find('input:eq(0), textarea:eq(0)');
					$Val=$.trim($Obj.val());
					if($Ary[i]=='url'){ //自定义地址
						if($Val==$Obj.attr('data-content')){
							$Obj.removeAttr('style');
							$Val='';
						}else{
							$Val=$Val.replace($Obj.attr('data-domain'), '').replace('.html', '');
							$Val=$Obj.attr('data-url').replace('%url%', $Val);
						}
					}
					if($Ary[i]=='keyword'){ //关键词
						$Val='';
						$Obj=$Seo.find('div[data-name='+$Ary[i]+'] .option_selected input[name=keysName\\[\\]]');
						$Obj.each(function(){
							$Val+=($j?',':'')+$(this).val();
							++$j;
						});
					}
					$SeoInfo.find('.'+$Ary[i]).text($Val);
					if($Val==''){
						$SeoInfo.find('.'+$Ary[i]).hide();
					}else{
						++$i;
						$SeoInfo.find('.'+$Ary[i]).show();
					}
				}
				if($i>0){
					$SeoInfo.find('.blank20').show();
				}else{
					$SeoInfo.find('.blank20').hide();
				}
			}
		});
		
		$('.seo_box .box_textarea[name=PageUrl]').on('focus', function(){
			if($.trim($(this).val())==$(this).attr('data-content')){
				$(this).val('').removeAttr('style');
			}
		}).on('blur', function(){
			if($.trim($(this).val())==''){
				$(this).val($(this).attr('data-content')).css('color', '#aaa');
			}
		});
		
		frame_obj.fixed_right($('#edit_keyword'), '.fixed_edit_keyword', function($this){
			//修改 关键词
			var $Form=$('#edit_keyword_form'),
				$List=$('.seo_box .keys_row .option_selected .btn_attr_choice'),
				$Html='', $i=0, $ID='',
				$ProId=$('#ProId').val();
			$Form.find('.edit_keyword_list').html(''); //清空内容
			
			$Html+=	'<div class="rows">';
			$Html+=		'<label>'+lang_obj.manage.global.name+'<div class="tab_box">'+products_obj.function_init.html_tab_button()+'</div></label>';
			$Html+=		'<div class="input">';
			for(k in ueeshop_config.language){
				$i=0;
				$Lang=ueeshop_config.language[k];
				$Html+=		'<div class="tab_txt tab_txt_'+$Lang+'" lang="'+$Lang+'"'+(ueeshop_config.lang==$Lang?'style="display:block;"':'')+'>';
				$List.each(function(){
					$Html+=		'<div class="item clean" data-id="'+$i+'">';
					$Html+=			'<input type="text" class="box_input fl" name="Name['+$i+']['+$Lang+']" value="" size="21" maxlength="255" autocomplete="off" notnull />';
					$Html+=			'<a href="javascript:;" class="attr_delete fl icon_delete_1"><i></i></a>';
					$Html+=		'</div>';
					++$i;
				});
				$Html+=		'</div>';
			}
			$Html+=		'</div>';
			$Html+=	'</div>';
			$Form.find('.edit_keyword_list').html($Html);
			if($Html){
				$Form.find('.edit_keyword_list, .box_button').show();
				$Form.find('.bg_no_table_data').hide();
			}else{
				$Form.find('.edit_keyword_list, .box_button').hide();
				$Form.find('.bg_no_table_data').show();
			}
			$.post('?', {'do_action':'products.products_seo_keywords', 'ProId':$ProId}, function(data){
				if(data.ret==1){
					for(lang in data.msg){ //属性名称
						for(k in data.msg[lang]){
							$Obj=$Form.find('.edit_keyword_list .tab_txt_'+lang+' .item:eq('+k+')');
							$Obj.find('.box_input').val(data.msg[lang][k]);
						}
					}
					//事件
					$Form.on('click', '.attr_delete', function(){
						$Form.find('.edit_keyword_list .item[data-id="'+$(this).parent().attr('data-id')+'"]').remove();
					});
				}
			}, 'json');
		});
		//******************************** SEO End ********************************
		
		
		//******************************** 属性事件 Start ********************************
		frame_obj.fixed_right($('.box_basic_more .add_attr'), '.fixed_general_attribute', function($this){
			//添加 普通属性 or 描述属性
			if(!$('#edit_form select[name=CateId]').val()){
				$('.fixed_general_attribute .close').click();
				global_obj.win_alert_auto_close(lang_obj.manage.products.category_tips, 'fail', 1000, '8%');
				return false;
			}
			var $Form=$('#add_general_attribute_form'),
				$IsEditor=($this.attr('id')=='add_editor_attribute'?1:0);
			$Form.find('.input_radio_box').off().on('click', function(){ //切换类型
				var $Index=$(this).find('input').val();
				if($Index==2){ //描述属性
					$Form.find('.rows[data-type=editor]').show();
					$Form.find('.screen_box').hide();
					$Form.find('.global_box').show();
				}else{ //普通属性 or 选项
					$Form.find('.rows[data-type=editor]').hide();
					$Form.find('.screen_box').show();
					$Form.find('.global_box').hide();
				}
			});
			if($IsEditor){ //描述属性
				$Form.find('.type_box').hide();
				$Form.find('.input_radio_box').eq(2).click();
			}else{ //普通属性
				$Form.find('.type_box').show();
				$Form.find('.input_radio_box').eq(0).click();
			}
			//清空内容
			$Form.find('.box_input').val('');
			$Form.find('input[name=AttrId]').val('');
			$Form.find('input[name=CateId]').val($('#edit_form select[name=CateId]').val());
			$Form.find('div[data-type=editor] textarea').each(function(){ //清空编辑器内容
				CKEDITOR.instances[$(this).attr('id')].setData('');
			});
		});
		frame_obj.fixed_right($('#edit_attribute, .box_basic_more .edit_attr, .box_basic_more .myorder_attr'), '.fixed_edit_attribute', function($this){
			//修改 规格属性 or 普通属性 or 描述属性
			if(!$('#edit_form select[name=CateId]').val()){
				$('.fixed_edit_attribute .close').click();
				global_obj.win_alert_auto_close(lang_obj.manage.products.category_tips, 'fail', 1000, '8%');
				return false;
			}
			var $Form=$('#edit_attribute_form'),
				$AttrList=$('.box_cart_attribute .box_attr[data-id!=Overseas]'),
				$EditorList=$('.description_box .head li:gt(0)'),
				$Html='', $Attr='0', $Editor='0', $Lang='en', $i=0,
				$IsCart=0, //判断是否为规格属性 0:普通属性 1:规格属性
				$Type=0, //类型 0:规格属性 1:普通属性 2:描述属性
				$IsMyOrder=0; //描述属性排序
				$ProId=$('#ProId').val(),
				$Title=lang_obj.manage.products.attr_edit;
                $Note=lang_obj.manage.products.notes.edit_attribute;
			if($this.attr('id')=='edit_general_attribute'){ //普通属性
				$Type=1;
				$AttrList=$('.box_general_attribute .rows');
			}else if($this.attr('id')=='edit_editor_attribute'){ //描述属性
				$Type=2;
			}else if($this.attr('id')=='myorder_editor_attribute'){ //描述属性(排序)
				$Type=2;
				$IsMyOrder=1;
				$Title=lang_obj.manage.products.tab_myorder;
                $Note=lang_obj.manage.products.notes.edit_tab_myorder;
			}
			$Type==0 && ($IsCart=1);
			$('.fixed_edit_attribute .top_title>strong').text($Title);
            $('.fixed_edit_attribute .edit_tips').text($Note);
			$Form.find('.edit_attr_list, .edit_editor_list').html(''); //清空内容
			$Form.find('input[name=CartAttr]').val($IsCart);
            $Form.find('input[name=CateId]').val($('#edit_form select[name=CateId]').val());
			if($Type<2){
				//规格属性 and 普通属性
				$Html+=	'<div class="rows">';
				$Html+=		'<label>'+lang_obj.manage.global.name+'<div class="tab_box">'+products_obj.function_init.html_tab_button()+'</div></label>';
				$Html+=		'<div class="input">';
				for(k in ueeshop_config.language){
					$i=0;
					$Lang=ueeshop_config.language[k];
					$Html+=		'<div class="tab_txt tab_txt_'+$Lang+'" lang="'+$Lang+'"'+(ueeshop_config.lang==$Lang?'style="display:block;"':'')+'>';
					$AttrList.each(function(){
						if($(this).attr('data-id').indexOf('ADD:')==-1){ //排除新添加的
							$Attr+=','+$(this).attr('data-id');
							$Html+=		'<div class="item clean" data-id="'+$(this).attr('data-id')+'">';
							$Html+=			'<span class="myorder"><span class="icon_myorder"></span></span>';
							$Html+=			'<input type="text" class="box_input fl" name="Name['+$(this).attr('data-id')+']['+$Lang+']" value="" size="21" maxlength="255" autocomplete="off" notnull />';
							$Html+=			'<div class="product_count fl">'+lang_obj.manage.products.pro_count.replace('%num%', '<span></span>')+'</div>';
							$Html+=			($IsCart==0 && k==0?'<span class="input_checkbox_box fl"><span class="input_checkbox"><input type="checkbox" name="IsScreen['+$(this).attr('data-id')+']" value="1"></span>'+lang_obj.manage.products.screen_item+'</span>':''),
							$Html+=			'<a href="javascript:;" class="attr_delete fl icon_delete_1"><i></i></a>';
							$Html+=			(k==0?'<input type="hidden" name="IsDelete['+$(this).attr('data-id')+']" value="0" />':'');
							$Html+=		'</div>';
							++$i;
						}
					});
					$Html+=		'</div>';
				}
				$Html+=		'</div>';
				$Html+=	'</div>';
				$Form.find('.edit_attr_list').html($Html);
				$Form.find('.edit_attr_list .tab_txt').dragsort({ //属性排序拖动
					dragSelector:'div',
					dragSelectorExclude:'input, a, div.product_count',
					placeHolderTemplate:'<div class="item placeHolder clean"></div>',
					scrollSpeed:5,
					dragEnd:function(){
						var data=$(this).parent().children('.item').map(function(){
							return $(this).attr('data-id');
						}).get();
						var obj=$(this).parent().siblings(),
							t='';
						obj.each(function(){
							for(k in data){
								t=$(this).find('.item[data-id='+data[k]+']');
								t.remove();
								$(this).find('.item:last-child').after('<div class="item clean" data-id="'+data[k]+'">'+t.html()+'</div>');
								$(this).find('.item[data-id='+data[k]+'] input:text').val(t.find('input:text').val());
							}
						});
					}
				});
			}else if($IsMyOrder==1){
				//描述属性(排序)
				$Html+=	'<div class="rows">';
				$Html+=		'<label>'+lang_obj.manage.global.name+'</label>';
				$Html+=		'<div class="input">';
				$EditorList.each(function(){
					$Editor+=','+$(this).attr('data-id');
					$Html+=		'<div class="item clean" data-id="'+$(this).attr('data-id')+'">';
					$Html+=			'<span class="myorder"><span class="icon_myorder"></span></span>';
					$Html+=			'<input type="text" class="box_input fl" value="'+$(this).find('span').text()+'" size="21" maxlength="255" autocomplete="off" readonly notnull />';
					$Html+=			'<div class="product_count fl">'+lang_obj.manage.products.pro_count.replace('%num%', '<span></span>')+'</div>';
					$Html+=			'<input type="hidden" name="MyOrder[]" value="'+$(this).attr('data-id')+'" />';
					$Html+=		'</div>';
					++$i;
				});
				$Html+=		'</div>';
				$Html+=	'</div>';
				$Form.find('.edit_editor_list').html($Html);
				$Form.find('.edit_editor_list .input').dragsort({ //属性排序拖动
					dragSelector:'div',
					dragSelectorExclude:'input, a, div.product_count',
					placeHolderTemplate:'<div class="item placeHolder clean"></div>',
					scrollSpeed:5,
					dragEnd:function(){
						var data=$(this).parent().children('.item').map(function(){
							return $(this).attr('data-id');
						}).get();
						var obj=$(this).parent().siblings(),
							t='';
						obj.each(function(){
							for(k in data){
								t=$(this).find('.item[data-id='+data[k]+']');
								t.remove();
								$(this).find('.item:last-child').after('<div class="item clean" data-id="'+data[k]+'">'+t.html()+'</div>');
								$(this).find('.item[data-id='+data[k]+'] input:text').val(t.find('input:text').val());
							}
						});
					}
				});
			}else{
				//描述属性
				$Html='';
				$EditorList.each(function(){
					$Editor+=','+$(this).attr('data-id');
					$Html+=	'<div class="rows clean" data-id="'+$(this).attr('data-id')+'">';
					$Html+=		'<label>'+lang_obj.manage.products.tab+'<div class="tab_box">'+products_obj.function_init.html_tab_button()+'</div></label>';
					$Html+=		'<a href="javascript:;" class="attr_delete fr icon_delete_1"><i></i></a>';
					$Html+=		'<div class="product_count fr">'+lang_obj.manage.products.pro_count.replace('%num%', '<span></span>')+'</div>';
					$Html+=		'<div class="input">';
									for(i in ueeshop_config.language){
										$lang=ueeshop_config.language[i];
										$field_name='Tab_Name['+$(this).attr('data-id')+']['+$lang+']';
					$Html+=				'<div class="tab_txt tab_txt_'+$lang+'" lang="'+$lang+'"'+(ueeshop_config.lang==$lang?' style="display:block;"':'')+'>';
					$Html+=					'<input type="text" name="'+$field_name+'" value="" class="box_input check_attribute_name" size="39" maxlength="255" />';
					$Html+=				'</div>';
									}
					$Html+=		'</div>';
					$Html+=		'<div class="blank9"></div>';
					$Html+=		'<div class="input">';
									for(i in ueeshop_config.language){
										$lang=ueeshop_config.language[i];
										$field_name='Tab_Txt['+$(this).attr('data-id')+']['+$lang+']';
					$Html+=				'<div class="tab_txt tab_txt_'+$lang+'" lang="'+$lang+'"'+(ueeshop_config.lang==$lang?' style="display:block;"':'')+'>';
					$Html+=					'<textarea id="'+$field_name+'" name="'+$field_name+'"></textarea>';
					$Html+=				'</div>';
									}
					$Html+=		'</div>';
					$Html+=		'<input type="hidden" name="IsTabDelete['+$(this).attr('data-id')+']" value="0" />';
					$Html+=		'<div class="blank15"></div>';
					$Html+=	'</div>';
				});
				$Form.find('.edit_editor_list').html($Html);
			}
			$.post('?', {'do_action':'products.products_check_attribute_pro_count', 'ProId':$ProId, 'Attr':$Attr, 'Editor':$Editor}, function(data){
				if(data.ret==1){
					if(data.msg.Attr){
						//普通属性（选项）
						for(lang in data.msg.Attr.Name){ //属性名称
							for(k in data.msg.Attr.Name[lang]){
								$Obj=$Form.find('.edit_attr_list .tab_txt_'+lang+' .item[data-id="'+k+'"]');
								$Obj.find('.box_input').val(data.msg.Attr.Name[lang][k]);
							}
						}
						for(k in data.msg.Attr.ProCount){
							var $Count=parseInt(data.msg.Attr.ProCount[k]),
								$Screen=parseInt(data.msg.Attr.Screen[k]),
								$Obj=$Form.find('.edit_attr_list .item[data-id="'+k+'"]');
							$Obj.find('.product_count>span').text(data.msg.Attr.ProCount[k]);
							if($Screen==1){ //产品筛选
								$Obj.find('.input_checkbox_box').addClass('checked').find('input').attr('checked', true);
							}
							if($Count>0){ //有关联数量，就不许删除
								$Obj.find('.attr_delete').hide();
							}else{
								$Obj.find('.attr_delete').show();
							}
						}
					}
					if(data.msg.Editor){
						//描述属性
						if($IsMyOrder==1){ //排序
							for(k in data.msg.Editor.ProCount){
								$Form.find('.edit_editor_list .item[data-id="'+k+'"] .product_count>span').text(data.msg.Editor.ProCount[k]);
							}
						}else{
							for(lang in data.msg.Editor.Name){ //属性名称
								for(k in data.msg.Editor.Name[lang]){
									$Obj=$Form.find('.edit_editor_list .rows[data-id="'+k+'"] .input:eq(0) .tab_txt_'+lang+'');
									$Obj.find('.box_input').val(data.msg.Editor.Name[lang][k]);
								}
							}
							for(lang in data.msg.Editor.Description){ //属性内容
								for(k in data.msg.Editor.Description[lang]){
									$Obj=$Form.find('.edit_editor_list .rows[data-id="'+k+'"] .input:eq(1) .tab_txt_'+lang+'');
									Txt=CKEDITOR.replace('Tab_Txt['+k+']['+lang+']', {"language":ueeshop_config.manage_language});
									Txt.setData((data.msg.Editor.Description[lang][k] ? data.msg.Editor.Description[lang][k].replace(/\\"/g, '"') : data.msg.Editor.Description[lang][k]));
								}
							}
							for(k in data.msg.Editor.ProCount){
								var $IsGlobal=parseInt(data.msg.Editor.IsGlobal[k]),
                                    $Count=parseInt(data.msg.Editor.ProCount[k]),
									$Obj=$Form.find('.edit_editor_list .rows[data-id="'+k+'"]');
                                if($IsGlobal==1){
                                    //所有产品可用
                                    $Obj.find('.product_count').text(lang_obj.manage.products.notes.all_products_used);
                                    $Obj.find('.attr_delete').show();
                                }else{
                                    //选项卡
                                    $Obj.find('.product_count>span').text(data.msg.Editor.ProCount[k]);
                                    if($Count>0){ //有关联数量，就不许删除
                                        $Obj.find('.attr_delete').hide();
                                    }else{
                                        $Obj.find('.attr_delete').show();
                                    }
                                }
							}
						}
					}
					//事件
					$Form.find('.input_checkbox_box').click(function(e){
						if($(e.target).attr('class')){
							if($(this).hasClass('checked')){
								$(this).removeClass('checked').find('input').removeAttr('checked');
							}else{
								$(this).addClass('checked').find('input').attr('checked', true);
							}
						}
						return false;
					});
					$Form.on('click', '.attr_delete', function(){
						if($Form.find('.edit_attr_list').length){ //普通属性（选项）
							$Form.find('.edit_attr_list .item[data-id="'+$(this).parent().attr('data-id')+'"]').hide().find('input[type=hidden]').val(1);
						}else{ //描述属性
							$Form.find('.edit_editor_list .rows[data-id="'+$(this).parent().attr('data-id')+'"]').hide().find('input[type=hidden]').val(1);
						}
					});
				}
			}, 'json');
		});
		products_obj.function_init.attr_load(); //加载产品属性
		window.setTimeout(function(){
			//延时加载
			products_obj.function_init.attr_category_select($('select[name=CateId]').val()); //默认加载
		}, 1000);
		$('#add_attribute').on('click', function(){
			//添加规格属性
			if(!$('#edit_form select[name=CateId]').val()){
				global_obj.win_alert_auto_close(lang_obj.manage.products.category_tips, 'fail', 1000, '8%');
				return false;
			}
			var $Html='',
				$MaxNum=parseInt($('#attr_max_number').val())+1; //获取新属性ID的最大值
			$Html+=	'<div class="box_attr clean" data-id="ADD:'+$MaxNum+'">';
			$Html+=		'<div class="rows clean">';
			$Html+=			'<label>'+lang_obj.manage.global.name+'</label>';
			$Html+=			'<div class="input">';
			$Html+=				'<input type="text" name="AttrTitle[ADD:'+$MaxNum+']['+ueeshop_config.lang+']" value="" class="box_input check_attribute_name" size="39" maxlength="255" autocomplete="off" notnull />';
			$Html+=				'<input type="hidden" name="AttrCart[ADD:'+$MaxNum+']" value="1" class="attr_cart" />';
			$Html+=			'</div>';
			$Html+=		'</div>';
			$Html+=		'<div class="rows clean">';
			$Html+=			'<label>';
			$Html+=				'<strong>'+lang_obj.global.option+'</strong>';
			$Html+=				'<dl class="box_basic_more box_attr_basic_more box_attr_add_basic_more">';
			$Html+=					'<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>';
			$Html+=					'<dd class="drop_down">';
			$Html+=						'<a href="javascript:;" class="item input_checkbox_box btn_option_edit"><span>'+lang_obj.manage.global.edit+'</span></a>';
			$Html+=						'<a href="javascript:;" class="item input_checkbox_box btn_associate_pic"><span>'+lang_obj.manage.products.pictures+'</span></a>';
			$Html+=						'<a href="javascript:;" class="item input_checkbox_box attr_delete"><span>'+lang_obj.global.del+'</span></a>';
			$Html+=					'</dd>';
			$Html+=				'</dl>';
			$Html+=			'</label>';
			$Html+=			'<div class="input">';
			$Html+=				'<div class="box_attr_list">';
			$Html+=					'<div class="attr_selected">';
			$Html+=						'<div class="select_list"></div>';
			$Html+=						'<input type="text" class="box_input" name="_Attr" value="" size="30" maxlength="255" />';
			$Html+=						'<span class="placeholder">'+lang_obj.manage.products.placeholder+'</span>';
			$Html+=					'</div>';
			$Html+=					'<div class="attr_not_yet">';
			$Html+=						'<div class="select_list"></div>';
			$Html+=						'<a href="javascript:;" class="select_all">'+lang_obj.global.select_all+'</a>';
			$Html+=					'</div>';
			$Html+=				'</div>';
			$Html+=			'</div>';
			$Html+=		'</div>';
			$Html+=	'</div>';
			$('.box_cart_attribute').append($Html);
			$('#attr_max_number').val($MaxNum);
			products_obj.function_init.attr_load();
		});
		products_obj.function_init.tab_load(); //加载产品描述属性事件
		$(document).click(function(e){
			//属性框去掉着色
			if($(e.target).attr('class')!='attr_selected selected_focus'){
				$('.attr_selected').removeClass('selected_focus').next('.attr_not_yet').slideUp();
			}
		});
		//******************************** 属性事件 End ********************************
		
		//详细介绍
		$('.description_box .head_btn').click(function(){
			var $this=$(this),
				$target=$('.description_box .head_hide>ul'),
				$targetLeft=Math.abs(parseFloat($target.css('margin-left'))),
				$objWidth=$('.description_box .head_hide').outerWidth(),
				$width=0, $left=0, $W, $R, $direction;
			if($this.hasClass('end')){ //终结
				return false;
			}
			if($this.hasClass('head_btn_left')){ //左按钮
				$direction='left';
			}else{ //右按钮
				$direction='right';
			}
			$('.description_box .head li').each(function(){
				$W=parseFloat($(this)[0].getBoundingClientRect().width.toFixed(3)); //精准到小数点后三位，以防偏差
				$R=parseInt($(this).css('margin-right'));
				$width+=$W+$R;
				$width=parseFloat($width.toFixed(3)); //精准到小数点后三位，以防偏差
				if($direction=='left'){ //左按钮
					if($left==0 && $width>=$objWidth+$targetLeft){
						$left=($width-$W-$R)-$objWidth;
					}
				}else{ //右按钮
					if($left==0 && $width>$objWidth+$targetLeft){
						$left=$width-$objWidth;
					}
				}
			});
			$left<0 && ($left=0);
			if($left==0){
				$('.description_box .head_btn_left').addClass('end');
			}else{
				$('.description_box .head_btn_left').removeClass('end');
			}
			if($width==$objWidth+$left){
				$('.description_box .head_btn_right').addClass('end');
			}else{
				$('.description_box .head_btn_right').removeClass('end');
			}
			$left>=0 && $target.css('margin-left', $left*-1);
		});
		
		//产品数据提交
		frame_obj.submit_form_init($('#edit_form'), './?m=products&a=products', function(){
			var $name='', $notattr=0, $notnull=0, $cur_null;
            if($('.fixed_btn_submit .btn_submit').hasClass('btn_disabled')){
                return false;
            }
			$('.btn_platform').click();
			$('#edit_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					// if($(this).is(':hidden')){
					// 	$('.r_con_wrap').animate({scrollTop:$(this).parents('.rows').position().top}, 500);
					// }else{
					// 	$('.r_con_wrap').animate({scrollTop:$(this).position().top}, 500);
					// }
					return false;
				}
			});
			$('#edit_form .check_attribute_name').each(function(){
				if($(this).attr('data-not-add')==1){
					$cur_null=$(this);
					++$notattr;
					return false;
				}
			});
			if($notattr){
				$cur_null.css('border', '1px red solid').focus();
				global_obj.win_alert_auto_close(lang_obj.manage.products.attr_null, 'await', 1000, '8%');
				return false;
			}
			if($('input[name=IsCombination]:checked').val()==1){// && $('input[name=IsOpenAttrPrice]:checked').val()==1
				$('#attribute_ext tr.group[id^=VId_]').each(function(){
					var $price=$(this).find('input[name^=AttrPrice]');
					if($(this).find('input[name^=AttrIsIncrease]:checked').val()!=1 && !($price.val()>0)){
						$cur_null=$(this);
						$notnull++;
						return false;
					}
				});
			}
			if($notnull){
				if($('.multi_tab_row>a').length>1 && $cur_null.parents('tbody').attr('id')){
					var $ovid=$cur_null.parents('tbody').attr('id').replace('AttrId_','');
					$('.multi_tab_row>a[data-oversea="'+$ovid+'"]').click();
				}
				$('.r_con_wrap').animate({scrollTop:($cur_null.position().top+$('#attribute_ext').parents('.global_container')[0].offsetTop)});
				$cur_null.find('input[name^=AttrPrice]').focus();
				global_obj.win_alert_auto_close(lang_obj.manage.products.price_null, 'await', 1000, '8%');
				return false;
			}
			return true;
		}, '', function(result){
			if(result.ret==1){
				if(result.msg.jump){ //保存产品资料
					window.location=result.msg.jump;
				}else{
					window.location.reload();
				}
			}else{
				$('#edit_form').find('input:submit').removeAttr('disabled');
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
		});
		
		//视频数据提交
		frame_obj.submit_form_init($('#video_form'), '', '', 0, function(result){
			if(result.ret==1){
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
				if(result.msg[0]){
					$('#VideoUpload').addClass('is_video');
				}else{
					$('#VideoUpload').removeClass('is_video');
				}
				if(result.msg[1]){
					$('#PicDetail').prepend($('#PicDetail').find('dl.video'));
				}else{
					$('#PicDetail').append($('#PicDetail').find('dl.video'));
				}
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		
		//属性数据提交
		frame_obj.submit_form_init($('#add_general_attribute_form'), '', function(){
			var $name='';
			$('#add_general_attribute_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var $Obj=$('.box_general_attribute .rows[data-id='+result.msg.AttrId+']'),
					$Html='';
				if($Obj.length){ //目标存在
					$Obj.find('.box_input').val(result.msg.Option);
				}else{ //目标不存在
					if(result.msg.Type==0){ //普通属性（文本框）
						$Html+=	'<div class="box_text clean">';
						$Html+=		'<div class="rows clean multi_lang" data-id="'+result.msg.AttrId+'">';
						$Html+=			'<label>';
						$Html+=				'<strong>'+result.msg.Name+'</strong>';
						$Html+=			'</label>';
						$Html+=			'<div class="input">';
						$Html+=				products_obj.function_init.attr_unit_form_edit(result.msg.Option, 'text', 'AttrTxt['+result.msg.AttrId+']', 46, 255);
						$Html+=			'</div>';
						$Html+=			'<a href="javascript:;" class="attr_delete icon_delete_1"><i></i></a>';
						$Html+=		'</div>';
						$Html+=	'</div>';
						$('.box_general_attribute').append($Html);
					}else if(result.msg.Type==1){ //普通属性（选项）
						$Html+=	'<div class="box_attr clean" data-id="'+result.msg.AttrId+'"'+'>';
						$Html+=		'<div class="rows clean" data-id="'+result.msg.AttrId+'"'+'>';
						$Html+=			'<label>';
						$Html+=				'<strong>'+result.msg.Name+'</strong>';
						$Html+=				'<dl class="box_basic_more box_attr_basic_more">';
						$Html+=					'<dt><a href="javascript:;" class="btn_basic_more"><i></i></a></dt>';
						$Html+=					'<dd class="drop_down">';
						$Html+=						'<a href="javascript:;" class="item input_checkbox_box btn_option_edit"><span>'+lang_obj.manage.global.edit+'</span></a>';
						$Html+=						'<a href="javascript:;" class="item input_checkbox_box attr_delete"><span>'+lang_obj.global.del+'</span></a>';
						$Html+=					'</dd>';
						$Html+=				'</dl>';
						$Html+=			'</label>';
						$Html+=			'<div class="input">';
						$Html+=				'<div class="box_attr_list">';
						$Html+=					'<div class="attr_selected">';
						$Html+=						'<div class="select_list"></div>';
						$Html+=						'<input type="text" class="box_input" name="_Attr" value="" size="30" maxlength="255" />';
						$Html+=						'<span class="placeholder">'+lang_obj.manage.products.placeholder+'</span>';
						$Html+=					'</div>';
						$Html+=					'<div class="attr_not_yet">';
						$Html+=						'<div class="select_list">';
													if(result.msg.Option){
														for(k2 in result.msg.Option[ueeshop_config.lang]){
															VId=result.msg.Option[ueeshop_config.lang][k2]['VId'];
						$Html+=								products_obj.function_init.attr_option_button(result.msg.Option[ueeshop_config.lang][k2]['Name'], VId, result.msg.AttrId, 0, 0);
														}
													}
						$Html+=						'</div>';
						$Html+=						'<a href="javascript:;" class="select_all">'+lang_obj.global.select_all+'</a>';
						$Html+=					'</div>';
						$Html+=				'</div>';
						$Html+=			'</div>';
						$Html+=		'</div>';
						$Html+=		'<input type="hidden" name="AttrTitle['+result.msg.AttrId+']['+ueeshop_config.lang+']" value="'+result.msg.Name+'" />';
						$Html+=		'<input type="hidden" name="AttrCart['+result.msg.AttrId+']" value="0" class="attr_cart" />';
						$Html+=	'</div>';
						$('.box_general_attribute').append($Html);
					}else{ //描述属性
						var $HeadHtml='<li data-id="'+result.msg.AttrId+'"><span>'+result.msg.Name+'</span><a href="javascript:;" class="attr_delete icon_delete_1"><i></i></a></li>';
						$Html+='<li data-id="'+result.msg.AttrId+'">'+products_obj.function_init.attr_form_edit(result.msg.Option, 'editor', 'Tab_'+result.msg.AttrId)+'</li>';
						$('.description_box .head ul').append($HeadHtml);
						$('.description_box .input ul').append($Html);
						products_obj.function_init.tab_load(); //加载产品描述属性事件
						$('.description_box .head_btn_right').removeClass('end');
					}
					products_obj.function_init.attr_load();
				}
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
				$('#fixed_right .btn_cancel').click();
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			return false;
		});
		
		//修改产品属性
		frame_obj.submit_form_init($('#edit_attribute_form'), '', function(){
			var $name='';
			$('#edit_attribute_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var lang=ueeshop_config.lang;
				if($('#edit_attribute_form input[name=CartAttr]').val()==1){
					//规格属性
					for(k in result.msg.Name){ //更改属性名称
						//for(lang in result.msg.Name[k]){
							$('.box_cart_attribute .box_attr[data-id="'+k+'"] .rows strong').html(result.msg.Name[k][lang]);
							$('.box_cart_attribute .box_attr[data-id="'+k+'"] input[name=AttrTitle\\['+k+'\\]\\['+lang+'\\]]').val(result.msg.Name[k][lang]);
						//}
					}
					for(k in result.msg.Delete){ //删除属性
						$('.box_cart_attribute .box_attr[data-id="'+result.msg.Delete[k]+'"] .attr_delete').click();
					}
				}else{
					//普通属性
					for(k in result.msg.Name){ //更改属性名称
						$('.box_general_attribute .rows[data-id="'+k+'"] label>strong').html(result.msg.Name[k][lang]);
					}
					for(k in result.msg.Delete){ //删除属性
						$('.box_general_attribute .rows[data-id="'+result.msg.Delete[k]+'"] .attr_delete').click();
					}
					for(k in result.msg.TabName){ //更改描述属性名称
						$('.description_box .head li[data-id="'+k+'"]>span').html(result.msg.TabName[k][lang]);
					}
					for(k in result.msg.TabDelete){ //删除描述属性
						$('.description_box .head li[data-id="'+result.msg.TabDelete[k]+'"] .attr_delete').click();
					}
				}
                if(result.msg.TabOrder){
                    for(k in result.msg.TabOrder){
                        $Num=parseInt(k)+1;
                        $DataId=$('.description_box .head_hide li:eq('+$Num+')').attr('data-id');
                        console.log($Num+'__'+$DataId+'__'+result.msg.TabOrder[k]);
                        if($DataId!=result.msg.TabOrder[k]){
                            $('.description_box .head_hide li[data-id="'+result.msg.TabOrder[k]+'"]').attr('data-id');
                            $CurLi=$('.description_box .head_hide li[data-id="'+result.msg.TabOrder[k]+'"]');
                            $('.description_box .head_hide li:eq('+k+')').after($CurLi);
                        }
                    }
                }
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
                $('#fixed_right .btn_cancel').click();
			} else {
                global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
            }
			return false;
		});
		
		//修改产品选项
		frame_obj.submit_form_init($('#edit_attribute_option_edit_form'), '', function(){
			var $name='';
			$('#edit_attribute_option_edit_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var $AttrId=result.msg.AttrId,
					$Obj=$('.box_attr[data-id="'+$AttrId+'"]'),
					$id=0, $target;
				for(k in result.msg.Option){
					for(lang in result.msg.Option[k]){
						$id=result.msg.VId[k];
						products_obj.data_init.attr_data[$id]=products_obj.data_init.attr_data[k];
						products_obj.data_init.attr_data[$id]['Name'][lang]=result.msg.Option[k][lang];
					}
				}
				$Obj.find('.box_attr_list>div').each(function(){
					$(this).find('.btn_attr_choice').each(function(){
						$target=$(this).find('input[name=AttrOption\\[\\]]');
						$id=$target.val();
						$target.prev().val(result.msg.VId[$id]); //AttrCurrent[]
						$target.val(result.msg.VId[$id]); //AttrOption[]
						$target.next().next().val(result.msg.Option[$id][ueeshop_config.lang]); //AttrName[]
						$target.prev().prev().html(result.msg.Option[$id][ueeshop_config.lang]); //选项名称
					});
				});
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		
		//编辑关联图片
		frame_obj.submit_form_init($('#edit_picture_form'), '', function(){
			var $name='';
			$('#edit_picture_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var $AttrDataId=result.msg.AttrDataId,
					$AttrId=result.msg.AttrId,
					$Obj=$('.box_cart_attribute .box_attr[data-id="'+$AttrDataId+'"]'),
					$id=0, $target,
					$IsNew=$Obj.find('.check_attribute_name').length;
				for(k in result.msg.VId){
					$id=result.msg.VId[k];
					products_obj.data_init.attr_data[$id]=products_obj.data_init.attr_data[k];
				}
				if($IsNew){
					var AttrName=$Obj.find('.check_attribute_name').val();
					$Obj.attr('data-id', $AttrId);
					$Obj.find('.check_attribute_name').parents('.rows').remove();
					$Obj.find('label>strong').text(AttrName);
					$Obj.append('<input type="hidden" name="AttrTitle['+$AttrId+']['+ueeshop_config.lang+']" value="'+AttrName+'"><input type="hidden" name="AttrCart['+$AttrId+']" value="1" class="attr_cart">');
				}
				if(result.msg.IsColor==1){
					!$Obj.find('label>span').length && $Obj.find('label>strong').after('<span>('+lang_obj.manage.products.master_pictures+')</span>');
				}else{
					$Obj.find('label>span').length && $Obj.find('label>span').remove();
				}
				$Obj.find('.box_attr_list .attr_selected .btn_attr_choice').each(function(){
					$target=$(this).find('input[name=AttrOption\\[\\]]');
					$id=$target.val();
					$target.prev().val(result.msg.VId[$id]); //AttrCurrent[]
					if($IsNew){
						$target.next().val($AttrId); //AttrParent[]
					}
					$target.val(result.msg.VId[$id]); //AttrOption[]
				});
				//products_obj.function_init.attr_price_show();
				$('#edit_picture_form .edit_picture_list .item .img.isfile').append('<div class="loading"></div>'); //图片附上加载icon
				setTimeout(function(){ //图片分批保存
					products_obj.function_init.associate_pic_ajax(result.msg.Data, result.msg.AttrId, result.msg.IsColor, 1);
				}, 2000);
			}else{
				$('#fixed_right .btn_cancel').click();
			}
			return false;
		});
		
		//修改关键词
		frame_obj.submit_form_init($('#edit_keyword_form'), '', function(){
			var $name='';
			$('#edit_keyword_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var $Obj=$('.keys_row'),
					$index=0;
				$Obj.find('.option_selected .btn_attr_choice').each(function(){
					$index=$(this).index();
					$(this).find('input').val(result.msg[ueeshop_config.lang][$index]);
					$(this).find('b').html(result.msg[ueeshop_config.lang][$index]);
				});
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
	},
	
	products_edit_synchronize:function(){ //产品属性同步按钮
		$('#edit_form .synchronize_btn').off().on('click', function(){
			var $num=parseInt($(this).attr('data-num')),
				$obj=$(this).parents('tr'),
				$value=$obj.nextAll('tr[id!=""]').find('td:eq('+($num+1)+') input').val();
			$obj.siblings('tr').find('td:eq('+($num+1)+') input').val($value);
		});
		/* 加价单选按钮 */
		$('#attribute_ext_box .switchery').off().on('click', function(){
			if($(this).hasClass('checked')){
				$(this).removeClass('checked').find('input').attr('checked', false);
				if($(this).hasClass('btn_increase_all')){ //总舵
					$(this).parents('tbody').find('.switchery').removeClass('checked').find('input').attr('checked', false);
					$('#attribute_tmp .btn_increase_all').removeClass('checked').find('input').attr('checked', false);
					$('#attribute_tmp .contents .switchery').removeClass('checked').find('input').attr('checked', false);
				}else{ //分舵
					$(this).parents('tbody').find('tr:gt(0) .switchery').not('.checked').length>0 && $(this).parents('tbody').find('.btn_increase_all').removeClass('checked').find('input').attr('checked', false);
				}
			}else{
				$(this).addClass('checked').find('input').attr('checked', true);
				if($(this).hasClass('btn_increase_all')){ //总舵
					$(this).parents('tbody').find('.switchery').addClass('checked').find('input').attr('checked', true);
					$('#attribute_tmp .btn_increase_all').addClass('checked').find('input').attr('checked', true);
					$('#attribute_tmp .contents .switchery').addClass('checked').find('input').attr('checked', true);
				}else{ //分舵
					$(this).parents('tbody').find('tr:gt(0) .switchery').not('.checked').length==0 && $(this).parents('tbody').find('.btn_increase_all').addClass('checked').find('input').attr('checked', true);
				}
			}
		});
		$('#attribute_ext table').each(function(){
			var obj=$(this).find('tbody');
			if(obj.find('tr:gt(0) .switchery').not('.checked').length==0){
				obj.find('.btn_increase_all').addClass('checked').find('input').attr('checked', true);
			}else{
				obj.find('.btn_increase_all').removeClass('checked').find('input').attr('checked', false);
			}
		});
		/* 单个属性 */
		/*if($('#AttrId_0').length==0){
			//!$('input[name=IsCombination]').is(':checked') && $('input[name=IsCombination]').parent().click();
			$('input[name=IsCombination]').parents('.box_combination').hide();
		}
		if($('#AttrId_0').length){
			$('input[name=IsCombination]').parents('.box_combination').show();
		}*/
	},
	
	category_init:function(){
		frame_obj.del_init($('#category .r_con_table')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'products.category_del_bat'); //批量删除
		frame_obj.dragsort($('#category .r_con_table tbody'), 'products.category_order'); //元素拖动
		
		//首页显示 or 下架
		frame_obj.switchery_checkbox(function(obj){
			var $CateId=obj.find('input').attr('data-id'),
				$Type=obj.find('input').attr('name');
			$.post('?', {'do_action':'products.category_change_type', 'Type':$Type, 'CateId':$CateId, 'IsUsed':1}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		}, function(obj){
			var $CateId=obj.find('input').attr('data-id'),
				$Type=obj.find('input').attr('name');
			$.post('?', {'do_action':'products.category_change_type', 'Type':$Type, 'CateId':$CateId, 'IsUsed':0}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		});
		
		//隶属栏目
		if($('select[name=UnderTheCateId]>option[value!=""]').size()==0){ //没法选择的时候，自动隐藏起来
			$('select[name=UnderTheCateId]').parents('.rows').hide();
		}
		
		//图片上传
		frame_obj.mouse_click($('.multi_img .upload_btn, .pic_btn .edit'), 'pro', function($this){ //产品主图点击事件
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		//$('#PicUpload, .upload_pic .edit').on('click', function(){frame_obj.photo_choice_init('PicUpload', 'form input[name=PicPath]', 'PicDetail', '', 1);});
		if($('.multi_img input[name=PicPath]').length){
			var $obj=$('.multi_img input[name=PicPath]');
			if($obj.attr('save')==1){
				$obj.parent().append(frame_obj.upload_img_detail($obj.val())).children('.upload_btn').hide();
				$obj.parent().next().find('.del, .zoom').attr('href', $obj.val());
			}
		}
		$('.pic_btn .del').on('click', function(){
			var $this=$(this);
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.ajax({
					url:'./?do_action=action.file_del&PicPath='+$this.attr('href'),
					success:function(){
						$('.multi_img .preview_pic').children('a').remove();
						$('.multi_img .preview_pic').children('.upload_btn').show();
						$('.pic_btn .del, .pic_btn .zoom').attr('href', '');
						$('.multi_img input[name=PicPath]').val('');
					}
				});
			}, 'confirm');
			return false;
		});
		
		//标题与标签
		$('#edit_form').on('click', '.open', function(){
			if($(this).hasClass('close')){
				$(this).removeClass('close').text(lang_obj.global.open);
				$('.seo_hide').slideUp(300);
			}else{
				$(this).addClass('close').text(lang_obj.global.pack_up);
				$('.seo_hide').slideDown(300);
			}
			return false;
		});
		frame_obj.submit_form_init($('#edit_form'), './?m=products&a=category');
	},
    cate_init:function(){
        frame_obj.del_init($('#category .r_con_table')); //删除提示

        //标题与标签
        $('#edit_form').on('click', '.open', function(){
            if($(this).hasClass('close')){
                $(this).removeClass('close').text(lang_obj.global.open);
                $('.seo_hide').slideUp(300);
            }else{
                $(this).addClass('close').text(lang_obj.global.pack_up);
                $('.seo_hide').slideDown(300);
            }
            return false;
        });
        /* logo */
        frame_obj.mouse_click($('#img .upload_btn, #img .pic_btn .edit'), 'img', function($this){ //点击上传图片
            frame_obj.photo_choice_init('img', '', 1);
        });
        frame_obj.submit_form_init($('#edit_form'), './?m=products&a=business_cate');
    },
    catelog_init:function(){
        frame_obj.del_init($('#category .r_con_table')); //删除提示

        //标题与标签
        $('#edit_form').on('click', '.open', function(){
            if($(this).hasClass('close')){
                $(this).removeClass('close').text(lang_obj.global.open);
                $('.seo_hide').slideUp(300);
            }else{
                $(this).addClass('close').text(lang_obj.global.pack_up);
                $('.seo_hide').slideDown(300);
            }
            return false;
        });
        frame_obj.submit_form_init($('#edit_form'), './?m=products&a=business_catelog');
    },
    logistics_init:function(){
        frame_obj.del_init($('#category .r_con_table')); //删除提示

        //标题与标签
        $('#edit_form').on('click', '.open', function(){
            if($(this).hasClass('close')){
                $(this).removeClass('close').text(lang_obj.global.open);
                $('.seo_hide').slideUp(300);
            }else{
                $(this).addClass('close').text(lang_obj.global.pack_up);
                $('.seo_hide').slideDown(300);
            }
            return false;
        });
        frame_obj.submit_form_init($('#edit_form'), './?m=products&a=logistics_line');
    },
	business_global:{
		del_action:'',
		order_action:'',
		init:function(){
			frame_obj.del_init($('#business .r_con_table')); //删除提示
			frame_obj.select_all($('#business .r_con_table input[name=select_all]'), $('#business .r_con_table input[name=select]'), $('.list_menu_button .del')); //批量操作
			frame_obj.del_bat($('.list_menu .del'), $('#business .r_con_table input[name=select]'), products_obj.business_global.del_action); //批量删除
			/* 批量排序 */
			frame_obj.del_bat($('.list_menu .order'), $('#business .r_con_table input[name=select]'), function(id_list){
				var $this=$(this),
					$checkbox,
					my_order_str='';
				$('#business .myorder select').each(function(index, element){
					$checkbox=$(element).parents('tr').find(':checkbox');
					if($checkbox.length && $checkbox.get(0).checked){;
						my_order_str+=$(element).val()+'-';
					}
				});
				global_obj.win_alert(lang_obj.global.my_order_confirm, function(){
					$.get('?', {do_action:products_obj.business_global.order_action, group_id:id_list, my_order_value:my_order_str}, function(data){
						if(data.ret==1){
							window.location.reload();
						}
					}, 'json');
				}, 'confirm');
				return false;
			}, lang_obj.global.dat_select);
		}
	},
	
	business_category_init:function(){
		products_obj.business_global.del_action='products.business_category_del_bat';
		products_obj.business_global.order_action='products.business_category_my_order';
		products_obj.business_global.init();
	},
	
	business_category_edit_init:function(){
		frame_obj.submit_form_init($('#category_edit_form'), './?m=products&a=business&d=category');
	},
	
	business_init:function(){
		products_obj.business_global.del_action='products.business_del_bat';
		products_obj.business_global.order_action='products.business_my_order';
		products_obj.business_global.init();
		$('#business .r_con_column .switchery').click(function(){
			var $this=$(this);
			$.get('?', 'do_action=products.business_uesd', function(data){
				if(data.ret==1){
					if($this.hasClass('checked')){
						$this.removeClass('checked');
					}else{
						$this.addClass('checked');
					}
				}else{
					global_obj.win_alert(lang_obj.global.set_error);
				}
			}, 'json');
		});
	},
	
	business_edit_init:function(){
		/* 资质证书上传 */
		frame_obj.mouse_click($('#ImgDetail .upload_btn, #ImgDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('ImgDetail', '', 1);
		});
		/* 合作凭证上传 */
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
        /* logo */
        frame_obj.mouse_click($('#LogoDetail .upload_btn, #LogoDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
            frame_obj.photo_choice_init('LogoDetail', '', 1);
        });
		/* 提交 */
		frame_obj.submit_form_init($('#business_edit_form'), './?m=products&a=business');
	},
	
	review_init:function(){
		frame_obj.del_init($('#review .r_con_table, .review_box')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'products.review_del_bat'); //批量删除
		$('.review_box .switchery').click(function(){
			var $this=$(this),
				$rid=$this.attr('data-id'),
				$review=0,
				$audit;
			if($this.hasClass('checked')){
				$audit=0;
				$this.removeClass('checked');
			}else{
				$audit=1;
				$this.addClass('checked');
			}
			if($this.hasClass('review')) $review=1;
			$.post('?do_action=products.review_reply_audit', {'RId':$rid,'Audit':$audit,'Review':$review},function(){}, 'json');
		});
		frame_obj.submit_form_init($('#review_edit_form'), './?m=products&a=review&d=reply&RId='+$('input[name=RId]').val());
	},
	
	aliexpress_sync_init:function(){
		var step_0=function(){
			var ajaxTimeout=$.ajax({
				url:'./?do_action=products.aliexpress_sync&step=0',
				type:'get',
				timeout:10000,
				dataType:'json',
				success:function(data){
					$('.sync').html($('.sync').html()+data.msg.msg);
					data.ret==1 && step_1();
				},
				complete:function(XMLHttpRequest,status){
					if(status=='timeout'){
						ajaxTimeout.abort();
						step_0();
					}
				}
			});
		}
		var step_1=function(){	//获取产品的基本资料
			var ajaxTimeout=$.ajax({
				url:'./?do_action=products.aliexpress_sync&step=1',
				type:'get',
				timeout:10000,
				dataType:'json',
				success:function(data){
					$('.sync').html($('.sync').html()+data.msg.msg).scrollTop(1000000);
					if(data.ret==1){
						data.msg.step==2?step_2():step_1();
					}
				},
				complete:function(XMLHttpRequest,status){
					if(status=='timeout'){
						ajaxTimeout.abort();
						step_1();
					}
				}
			});
		}
		var step_2=function(){	//获取产品的详细资料
			var ajaxTimeout=$.ajax({
				url:'./?do_action=products.aliexpress_sync&step=2',
				type:'get',
				timeout:30000,
				dataType:'json',
				success:function(data){
					$('.sync').html($('.sync').html()+data.msg.msg).scrollTop(1000000);
					data.ret==1 && step_2();
				},
				complete:function(XMLHttpRequest,status){
					if(status=='timeout'){
						ajaxTimeout.abort();
						step_2();
					}
				}
			});
		}
		$('.btn_ok').click(function(){
			$(this).prop('disabled', true);
			step_0();
		});
	}
}