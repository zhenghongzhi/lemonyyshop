/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var operation_obj={	
	page_global:{
		del_action:'',
		order_action:'',
		init:function(){
			frame_obj.del_init($('#page .r_con_table')); //删除提示
			frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
			frame_obj.del_bat($('.list_menu .del'), $('#page .r_con_table input[name=select]'), operation_obj.page_global.del_action); //批量删除
			/* 排序操作 */
			$('#page .r_con_table .myorder_select').on('click', function(){
				var $obj=$(this),
					$number=$obj.attr('data-num'),
					$Id=$obj.parents('tr').find('td:eq(0) input').val(),
					$mHtml=$obj.html(),
					$sHtml=$('#myorder_select_hide').html(),
					$val;
				if($obj.attr('data-status')!=1){
					$obj.attr('data-status', 1);
					$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
					$number && $obj.find('select').val($number).focus();
					$obj.find('select').on('blur', function(){
						$val=$(this).val();
						if($val!=$number){
							$.post('?', 'do_action='+operation_obj.page_global.order_action+'&Id='+$Id+'&Number='+$(this).val(), function(data){
								if(data.ret==1){
									$obj.html(data.msg);
									$obj.attr('data-num', $val);
								}
							}, 'json');
						}else{
							$obj.html($obj.find('span').html());
						}
						$obj.attr('data-status', 0);
					});
				}
			});
		}
	},
	
	page_init:function(){
		operation_obj.page_global.del_action='operation.page_del_bat';
		operation_obj.page_global.order_action='operation.page_edit_myorder';
		operation_obj.page_global.init();
	},
	
	page_edit_init:function(){
		$('#edit_form').on('click', '.open', function(){
			if($(this).hasClass('close')){
				$(this).removeClass('close').text(lang_obj.global.open);
				$('.seo_hide').slideUp(300);
			}else{
				$(this).addClass('close').text(lang_obj.global.pack_up);
				$('.seo_hide').slideDown(300);
			}
		});
		frame_obj.submit_form_init($('#edit_form'), './?m=operation&a=page');
	},
	
	page_category_init:function(){
		operation_obj.page_global.del_action='operation.page_category_del_bat';
		operation_obj.page_global.order_action='operation.page_category_edit_myorder';
		operation_obj.page_global.init();
		
		frame_obj.dragsort($('#page .r_con_table tbody'), 'operation.page_category_order', 'tr', 'a, td[data!=move_myorder]', '<tr class="placeHolder"></tr>'); //元素拖动
	},
	
	page_category_edit_init:function(){
		frame_obj.switchery_checkbox();
		frame_obj.submit_form_init($('#cate_edit_form'), './?m=operation&a=page&d=category');
	},
	
	billing_edit_init:function(){
		frame_obj.del_init($('#billing .config_table'));

		$('.billing_used .switchery').click(function(){
			var $bid=$(this).attr('data-bid'),
				$used;
			if($(this).hasClass('checked')){
				$(this).removeClass('checked');
				$used=0;
			}else{
				$(this).addClass('checked')
				$used=1;
			}
			$.post('?do_action=operation.billing_used',{'BId':$bid,'IsUsed':$used},function(){},'json');
		});

		$('.input_radio_box').click(function(){
			var pos_ary=$(this).attr('data-position'),
			 	$value=$(this).find('input').val(),
			 	$odevice=$('input[name=OldDevice]').val();
			if(pos_ary) pos_ary=$.evalJSON(pos_ary);
			if($value==1){
				$('.ad_position .item.mobile').show();
				$('.ad_position .item.pc').hide();
				$('.ad_position .item').removeClass('no_drop').each(function(){
					if(pos_ary && global_obj.in_array($(this).find('input').val(), pos_ary) && !$(this).hasClass('used')){
						$(this).addClass('no_drop').removeClass('cur').find('input').removeAttr('checked');
					}else if($odevice!=$value && $(this).hasClass('used') && $(this).find('input').val()==1){
						$(this).addClass('no_drop').removeClass('cur');
					}					
				});
			}else{
				$('.ad_position .item.mobile').hide();
				$('.ad_position .item.pc').show();
				$('.ad_position .item').removeClass('no_drop').each(function(){
					if(pos_ary && global_obj.in_array($(this).find('input').val(), pos_ary) && !$(this).hasClass('used')){
						$(this).addClass('no_drop').removeClass('cur').find('input').removeAttr('checked');
					}else if($odevice!=$value && $(this).hasClass('used') && $(this).find('input').val()==1){
						$(this).addClass('no_drop').removeClass('cur');
					}					
				});
			}
			setTimeout(function(){
				if($('input[name=BId]').val()>0){
					$('input[name=Position]:checked').parent().click();
				}else{
					$('input[name=Position]').not(':checked').parent(':visible').not('.no_drop').eq(0).click();
				}
			}, 100);
		});

		$('.ad_position .item').click(function(){
			var $value=$(this).find('input').val();
			if(!$(this).hasClass('no_drop')){
				$('.ad_position .item').removeClass('cur').find('input').removeAttr('checked');
				$(this).addClass('cur').find('input').attr('checked','checked');
				if($value==0 && $('input[name=Device]:checked').val()==0){
					$('#billing .pic_box').hide();
					$('#billing .desc_box').show();
				}else{
					$('#billing .pic_box').show();
					$('#billing .desc_box').hide();
				}
			}
		});

		$('input[name=Device]:checked').parent().click();

		/* 广告图图片上传 */
		frame_obj.mouse_click($('.multi_img .upload_btn, .pic_btn .edit'), 'ad', function($this){ 
			var $id=$this.parents('.multi_img').attr('id'),
				piccount=parseInt($this.attr('data-count'));
			frame_obj.photo_choice_init($id, 'ad', 1);
		});

		frame_obj.submit_form_init($('#billing_edit_form'), './?m=operation&a=billing');
	},
	
	//邮件发送
	email_send_init:function(){
		//图片上传
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		
		//选择邮件模板
		frame_obj.fixed_right($('.btn_tpl_choice'), '.fixed_template_choice');
		$('.fixed_template_choice .top_menu li>a').click(function(){ //选项卡切换
			var $Type=$(this).attr('data-type');
			$(this).addClass('current').parent().siblings().find('a').removeClass('current');
			$('.template_choice_list .list[data-type='+$Type+']').show().siblings().hide();
		});
		$('.fixed_template_choice .top_menu li:eq(0)>a').click(); //默认点击第一个
		$('.template_choice_list .item').click(function(){
			var $This=$(this),
				$Tpl=$This.attr('data-tpl'),
				$Form=$('#template_choice_form'),
				$Type=$This.parent().attr('data-type');
			if($This.hasClass('cur')){ //已勾选
				$This.removeClass('cur');
				$Form.find('input[name=Template]').val(''); //清除选择
				$Form.find('input[name=Type]').val(''); //清零
			}else{ //未勾选
				$This.addClass('cur').siblings().removeClass('cur');
				$Form.find('input[name=Template]').val($Tpl); //选择了那个模板
				$Form.find('input[name=Type]').val($Type); //清零
			}
		});
		$('.template_choice_list .item .del_mask').click(function(){
			var $Obj=$(this).parent().parent(),
				$Tpl=$Obj.attr('data-tpl');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.get('?do_action=operation.email_template_del&EId='+$Tpl, function(data){
					if(data.ret==1){
						$Obj.remove();
						global_obj.win_alert_auto_close(lang_obj.manage.global.del_success, '', 1000, '8%');
					}else{
						global_obj.win_alert_auto_close(lang_obj.manage.global.del_fail, 'fail', 1000, '8%');
					}
				}, 'json');
			}, 'confirm');
			return false;
		});
		
		//保存为邮件模板
		frame_obj.fixed_right($('.btn_tpl_save'), '.fixed_template_save', function(){
			var $Content=CKEDITOR.instances['Content'].getData();
			$('#template_save_form textarea[name=Content]').html($Content);
		});
		
		//发送邮件
		frame_obj.submit_form_init($('#edit_form'), './?m=operation&d=email');
		
		//数据提交
		if($('#template_save_form').length){ //保存为邮件模板
			var $Form=$('#template_save_form');
			$Form.submit(function(){ return false; });
			$Form.find('.btn_submit').click(function(){
				if(global_obj.check_form($Form.find('*[notnull]'), $Form.find('*[format]'), 1)){ return false; };
				$Content=$Form.find('textarea[name=Content]').html();
				$.post('?', $Form.serialize()+'&Content='+escape($Content), function(data){
					if(data.ret==1){
						global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
					}else{
						global_obj.win_alert_auto_close(lang_obj.manage.email.not_tit_or_desc, 'fail', 1000, '8%');
					}
					$('#fixed_right .btn_cancel').click();
					return false;
				}, 'json');
			});
		}
		frame_obj.submit_form_init($('#template_choice_form'), '', function(){ //选择邮件模板
			if($('#template_choice_form input[name=Template]').val()==''){
				global_obj.win_alert_auto_close(lang_obj.manage.email.not_model, 'fail', 1000, '8%');
				return false;
			}
		}, false, function(data){
			if(data.ret==1){
				CKEDITOR.instances['Content'].setData(data.msg);
				CKEDITOR.instances['Content'].updateElement();//更新数据
			}else{
				global_obj.win_alert_auto_close(lang_obj.manage.email.load_error, 'fail', 1000, '8%');
			}
			$('#template_choice_form .btn_submit').removeAttr('disabled');
			$('#fixed_right .btn_cancel').click();
			return false;
		});
	},
	
	config_init:function(){
		//多选事件
		$('.notice_menu .choice_btn').off().on('click', function(){
			var inputBox=$(this).children('input'),
				Num=inputBox.val(),
				checked;
			if(inputBox.is(':checked')){
				checked=false;
				$(this).removeClass('current');
			}else{
				checked=true;
				$(this).addClass('current');
			}
			inputBox.attr('checked', checked);
		});
		//提交
		frame_obj.submit_form_init($('#edit_form'), './?m=email&d=config');
	},
	
	newsletter_init:function(){
		frame_obj.del_init($('#newsletter .r_con_table'));
		
		$('#excel_format').on('click', function(){
			window.location='./?&do_action=operation.newsletter_explode';
		});
		
		//首页显示 or 下架
		frame_obj.switchery_checkbox(function(obj){
			var $NId=obj.find('input').attr('data-id'),
				$Type=obj.find('input').attr('name');
			$.post('?', {'do_action':'operation.newsletter_status', 'Type':$Type, 'NId':$NId, 'IsUsed':1}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		}, function(obj){
			var $NId=obj.find('input').attr('data-id'),
				$Type=obj.find('input').attr('name');
			$.post('?', {'do_action':'operation.newsletter_status', 'Type':$Type, 'NId':$NId, 'IsUsed':0}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		});
	},
	
	arrival_init:function(){
		frame_obj.del_init($('#arrival .r_con_table'));
	},
	
	system_init:function(){//系统邮件模板
		$('#sys_tpl_btn').click(function (){
			frame_obj.pop_form($('.sys_tpl_edit'));
		});
		frame_obj.switchery_checkbox();
		//下次更新删掉8-10
		/*$('#email_tpl_form .list .item .img').click(function(){	//模板选择
			var $obj=$(this).parent();
			if($obj.hasClass('cur')){
				$obj.removeClass('cur');
				$('#email_tpl_form input[name=template]').val('');//清除选择
			}else{
				$obj.addClass('cur').siblings().removeClass('cur');
				$('#email_tpl_form input[name=template]').val($obj.attr('template'));//选择了那个模板
			}
		});
		
		var close_template=function(){
			$('#email_tpl_form .list .item').removeClass('cur');//清零
			$('#email_tpl_form input[name=template]').val('');//清零
			frame_obj.pop_form($('.sys_tpl_edit'), 1);
			$('#div_mask').remove();
		}
		
		frame_obj.submit_form_init($('#email_tpl_form'), '', function(){
			if($('#email_tpl_form input[name=template]').val()==''){
				close_template();
				global_obj.win_alert(lang_obj.manage.email.not_model);
				return false;
			}
		}, false, function(data){
			if(data.ret==1){
				$.each(data.msg.lang, function (i, v){
					$('#edit_form input[name=Title_'+v+']').val(data.msg.Title[v]);
					CKEDITOR.instances['Content_'+v].setData(data.msg.Content[v]);
					CKEDITOR.instances['Content_'+v].updateElement();//更新数据
				});
				
				if(data.msg.IsUsed==1){
					$('#edit_form input[name=IsUsed]').attr('checked', true).parent('.switchery').addClass('checked');
				}else{
					$('#edit_form input[name=IsUsed]').attr('checked', false).parent('.switchery').removeClass('checked');
				}
				$('#edit_form input[name=template]').val(data.msg.template);
				$('.tpl_tips').hide(0);
				$('.r_con_form .rows .input .'+data.msg.template).show(0);
			}else{
				global_obj.win_alert(lang_obj.manage.email.load_error);
			}
			close_template();
			$('#email_tpl_form input[name=submit_button]').attr('disabled', false);
		});	*/
		//模板选择
		$('#template_select').change(function (){
			var template = $(this).val();
			var lang = $(this).attr('lang');
			var lang_ary = new Array();
			lang_ary = lang.split(",");
			if (template!=''){
				$('#template_select').attr('disabled', true);
				$.post('./', {do_action:'email.sys_get_tpl', template:template, lang:lang}, function (data){
					if(data.ret==1){
						$.each(data.msg.lang, function (i, v){
							$('#edit_form input[name=Title_'+v+']').val(data.msg.Title[v]);
							CKEDITOR.instances['Content_'+v].setData(data.msg.Content[v]);
							CKEDITOR.instances['Content_'+v].updateElement();//更新数据
						});
						if(data.msg.IsUsed==1){
							$('#edit_form input[name=IsUsed]').attr('checked', true).parent('.switchery').addClass('checked');
						}else{
							$('#edit_form input[name=IsUsed]').attr('checked', false).parent('.switchery').removeClass('checked');
						}
						$('#edit_form input[name=template]').val(data.msg.template);
						$('.tpl_tips').hide(0);
						$('.r_con_form .rows .input .'+data.msg.template).show(0);
						$('#template_select').attr('disabled', false);
					}else{
						global_obj.win_alert(lang_obj.manage.email.load_error);
						$('#template_select').attr('disabled', false);
					}
				}, 'json');
			}else{
				$('#edit_form input[name=IsUsed]').attr('checked', false).parent('.switchery').removeClass('checked');
				$('#edit_form input[name=template]').val('');
				$('.tpl_tips').hide(0);
				$.each(lang_ary, function (i, v){
					if ($('#Content_'+v).length){
						$('#edit_form input[name=Title_'+v+']').val('');
						CKEDITOR.instances['Content_'+v].setData('');
						CKEDITOR.instances['Content_'+v].updateElement();//更新数据
					}
				});
			}
		});
		
		//提交表单
		frame_obj.submit_form_init($('#edit_form'), '', '', '', function (data){
			if(data.ret==1){
				global_obj.win_alert(data.msg, function (){
					window.location.href='./?m=email&d=system';
				});
			}else{
				$('#edit_form').find('input:submit').attr('disabled', false);
				global_obj.win_alert(data.msg);
			}
		});
	},
	
	chat_set_init:function(){
		frame_obj.switchery_checkbox();
		
		/** 0 */
		$('#Bg3_0, .upload_Bg3_0 .edit').on('click', function(){frame_obj.photo_choice_init('DetailBg3_0', '', 1);});
		if($('form input[name=Bg3_0]').attr('save')==1){
			$('#DetailBg3_0').append(frame_obj.upload_img_detail($('form input[name=Bg3_0]').val())).children('.upload_btn').hide();
		}
		$('.upload_Bg3_0 .del').on('click', function(){
			$('#DetailBg3_0').children('a').remove();
			$('#DetailBg3_0').children('.upload_btn').show();
			$('#edit_form input[name=Bg3_0]').val('');
		});
		
		/** 1 */
		$('#Bg3_1, .upload_Bg3_1 .edit').on('click', function(){frame_obj.photo_choice_init('DetailBg3_1', '', 1);});
		if($('form input[name=Bg3_1]').attr('save')==1){
			$('#DetailBg3_1').append(frame_obj.upload_img_detail($('form input[name=Bg3_1]').val())).children('.upload_btn').hide();
		}
		$('.upload_Bg3_1 .del').on('click', function(){
			$('#DetailBg3_1').children('a').remove();
			$('#DetailBg3_1').children('.upload_btn').show();
			$('#edit_form input[name=Bg3_1]').val('');
		});
		
		/** 2 */
		$('#Bg4_0, .upload_Bg4_0 .edit').on('click', function(){frame_obj.photo_choice_init('DetailBg4_0', '', 1);});
		if($('form input[name=Bg4_0]').attr('save')==1){
			$('#DetailBg4_0').append(frame_obj.upload_img_detail($('form input[name=Bg4_0]').val())).children('.upload_btn').hide();
		}
		$('.upload_Bg4_0 .del').on('click', function(){
			$('#DetailBg4_0').children('a').remove();
			$('#DetailBg4_0').children('.upload_btn').show();
			$('#edit_form input[name=Bg4_0]').val('');
		});
		
		$('#chat .style_box input[name=Type]').click(function(){
			$Type=$(this).val();
			$.post('?', 'do_action=set.chat_style&Type='+$Type, function(data){
				if(data.ret==1){
				}else{
					global_obj.win_alert(lang_obj.global.set_error);
				}
			}, 'json');
		});
		
		$('.style_select').click(function(){
			var val = $(this).val();
			$('#bgcolor').show(0);
			$('#mulcolor').hide(0);
			$('#bg3pic').hide(0);
			$('#bg4pic').hide(0);
			if (val==1){
				$('#bgcolor').hide(0);
				$('#mulcolor').show(0);
			}else if (val==3){
				$('#mulcolor').show(0);
				$('#bg3pic').show(0);
			}else if (val==4){
				$('#bg4pic').show(0);
			}
			$('#window_style').val($(this).val());	
		});
		
		$('#chat .color').change(function (){
			var name = $(this).attr('name');
			$('.'+name).css('background-color', '#'+$(this).val()).attr('color', '#'+$(this).val());
			var hover = $('.hover'+name);
			hover.attr('hover-color', '#'+$(this).val());
		});
		
		$('#service_2 .Color').hover(function (){
			$(this).css('background-color', $(this).attr('hover-color'));
		}, function (){
			$(this).css('background-color', $(this).attr('color'));
		});
		frame_obj.submit_form_init($('#edit_form'), './?m=operation&a=chat');
	},
	
	chat_init:function(){
		frame_obj.del_init($('#chat .chat_box'));
		frame_obj.config_switchery($('.used .switchery'), 'set.config_switchery', 'data-config', 'config');//开启语言版
		frame_obj.submit_form_init($('#chat_edit_form'), './?m=operation&a=chat');
		
		//弹出框编辑
		var box_chat_edit = $('.box_chat_edit');
		var select_Type = $('select[name=Type]', box_chat_edit);//选择框
		var ubox = $('.ubox', box_chat_edit);//图片上传框
		var $data = box_chat_edit.attr('data-chat');//数据
		$data=$.evalJSON($data);
		$('#chat').on('click', '.chat_title .add', function (){//添加
			$('#Picture, .whatsapp_tips').hide(0);
			$('#PicDetail .preview_pic a').remove();
			$('#PicDetail .upload_btn').css('display', 'block');
			$('#PicDetail .img').removeClass('isfile').find('.preview_pic').children('.upload_btn').show(0);
			$('#PicDetail .zoom').attr('href', 'javascript:;');
			$('input[name=Name]', box_chat_edit).val('');
			$('input[name=CId]', box_chat_edit).val('');
			$('input[name=PicPath]', box_chat_edit).val('');
			$('input[name=Account]', box_chat_edit).val('');
			select_Type.find('option:selected').attr('selected', false);
			select_Type.find('option:eq(0)').attr('selected', true);
			select_Type.change()
			if ($data.add){//判断添加权限
				ubox.css('display', 'block');
			}else{
				ubox.css('display', 'none');
			}
		});
		
		$('#chat').on('click', '.chat_box .edit', function (){//修改
			var CId=$(this).attr('data-cid');
			var data=$data[CId];
			$('#Picture, .whatsapp_tips').hide(0);
			$('#PicDetail .preview_pic a').remove();
			$('#PicDetail .upload_btn').css('display', 'block');
			$('input[name=Name]', box_chat_edit).val(data.Name);
			$('input[name=PicPath]', box_chat_edit).val(data.PicPath);
			$('input[name=CId]', box_chat_edit).val(CId);
			$('input[name=Account]', box_chat_edit).val(data.Account);
			select_Type.find('option:selected').attr('selected', false);
			select_Type.find('option[value="'+data.Type+'"]').attr('selected', true);
			
			if (data.Type==4){
				$('#Picture').show(0);
				if (data.PicPath){
					$('#PicDetail .img').addClass('isfile').find('.preview_pic').append(frame_obj.upload_img_detail(data.PicPath)).children('.upload_btn').hide(0);
					$('#PicDetail .zoom').attr('href', data.PicPath);
				}else{
					$('#PicDetail .img').removeClass('isfile').find('.preview_pic').children('.upload_btn').show(0);
				}
			}
			if(data.Type==5) $('.whatsapp_tips').show(0);
			
			if ($data.edit){//判断添加权限
				ubox.css('display', 'block');
			}else{
				ubox.css('display', 'none');
			}
		});

		frame_obj.fixed_right($('.chat_box .edit, #chat .chat_title .add'), '.box_chat_edit');
		select_Type.change(function(){
			$val=$(this).val();	
			if($val==4){
				$('#Picture').show();
				$('.whatsapp_tips').hide();
			}else if($val==5){
				$('.whatsapp_tips').show();
				$('#Picture').hide();
			}else{
				$('.whatsapp_tips').hide();
				$('#Picture').hide();	
			}
		});
		/* 图片上传 */
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		
		// window.onload = function(){
		// 	$(window).resize(function(){
		// 		frame_obj.Waterfall('chat_box', '260', 3, 'chat_list'); // 瀑布流
		// 	}).resize();
		// }

		frame_obj.dragsort($('#chat .chat_box .list_box'), 'operation.chat_my_order', '.list .icon_myorder', '', '<div class="list cur"></div>', '.list'); //排序拖动
	},
	
	translate_init:function(){
		$('#translate').on('click', '.translate_used .switchery', function(){
			var $This=$(this),
				$Lang=$This.parents('tr').attr('data-lang'),
				$Status=0;
			if(!$This.hasClass('checked')){
				$Status=1;
				$This.addClass('checked');
			}else{
				$This.removeClass('checked');
			}
			$.post('?', {'do_action':'operation.translate_lang_set', 'Lang':$Lang, 'Status':$Status}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		});
	},
	
	/**************************** 博客 start ****************************/
	blog_global:{
		del_action:'',
		order_action:'',
		init:function(){
			frame_obj.del_init($('#blog .r_con_table')); //删除事件
			frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
			frame_obj.del_bat($('.list_menu .del'), $('.r_con_table input[name=select]'), operation_obj.blog_global.del_action); //批量删除
			/* 批量排序 */
			$('#blog .r_con_table .myorder_select').on('click', function(){
				var $obj=$(this),
					$number=$obj.attr('data-num'),
					$AId=$obj.parents('tr').find('td:eq(0) input').val(),
					$mHtml=$obj.html(),
					$sHtml=$('#myorder_select_hide').html(),
					$val;
				if($obj.attr('data-status')!=1){
					$obj.attr('data-status', 1);
					$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
					$number && $obj.find('select').val($number).focus();
					$obj.find('select').on('blur', function(){
						$val=$(this).val();
						if($val!=$number){
							$.post('?', 'do_action='+operation_obj.blog_global.order_action+'&Id='+$AId+'&Number='+$(this).val(), function(data){
								if(data.ret==1){
									$obj.html(data.msg);
									$obj.attr('data-num', $val);
								}
							}, 'json');
						}else{
							$obj.html($obj.find('span').html());
						}
						$obj.attr('data-status', 0);
					});
				}
			});
		}
	},
	
	blog_set_init:function(){
		/* 添加导航栏目 */
		$('#blog_edit_form').on('click', '.addNav', function (){
			var container=$('.blog_nav');
			var name_lang=container.attr('data-name');
			var link_lang=container.attr('data-link');
			var html='';
			html+='<div>';
			html+=	'<div class="unit_input"><b>'+name_lang+'</b><input type="text" name="name[]" class="box_input" value="" size="10" maxlength="30" /></div> &nbsp;&nbsp;';
			html+=	'<div class="unit_input"><b>'+link_lang+'</b><input type="text" name="link[]" class="box_input" value="" size="30" max="150" /></div>';
			html+=	'<a class="d_del icon_delete_1" href="javascript:;"><i></i></a>';
			html+=	'<div class="blank6"></div>';
			html+='</div>';
			container.append(html);
		});
		/* 移除导航栏目 */
		$('.blog_nav').on('click', 'div a', function (){
			$(this).parent().remove();
		});
		/* 广告图上传 */
		frame_obj.mouse_click($('#AdDetail .upload_btn, #AdDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('AdDetail', '', 1);
		});
		frame_obj.submit_form_init($('#blog_edit_form'), './?m=operation&a=blog&d=set');
	},
	
	blog_init:function(){
		operation_obj.blog_global.del_action='operation.blog_del_bat';
		operation_obj.blog_global.order_action='operation.blog_edit_myorder';
		operation_obj.blog_global.init();
	},
	
	blog_edit_init:function(){
		/* 图片上传 */
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){ //点击上传图片
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		$('#edit_form .choice_btn').click(function(){
			var $this=$(this);
			if($this.children('input').is(':checked')){
				$this.removeClass('current');
				$this.children('input').attr('checked', false);
			}else{
				$this.addClass('current');
				$this.children('input').attr('checked', true);
			}
		});
		$('#edit_form').on('click', '.open', function(){
			if($(this).hasClass('close')){
				$(this).removeClass('close').text(lang_obj.global.open);
				$('.seo_hide').slideUp(300);
			}else{
				$(this).addClass('close').text(lang_obj.global.pack_up);
				$('.seo_hide').slideDown(300);
			}
		});
		$('#edit_form input[name=AccTime]').daterangepicker({singleDatePicker:true});
		frame_obj.submit_form_init($('#edit_form'), './?m=operation&a=blog&d=blog');
	},
	
	blog_category_init:function(){
		operation_obj.blog_global.del_action='operation.blog_category_del_bat';
		operation_obj.blog_global.order_action='operation.blog_category_my_order';
		operation_obj.blog_global.init();
		
		frame_obj.dragsort($('#blog .r_con_table tbody'), 'operation.blog_category_order', 'tr', 'a, td[data!=move_myorder]', '<tr class="placeHolder"></tr>'); //元素拖动
	},
	
	blog_category_edit_init:function(){
		frame_obj.submit_form_init($('#blog_edit_form'), './?m=operation&a=blog&d=category');
	},
	
	blog_review_init:function(){
		operation_obj.blog_global.del_action='operation.blog_review_del_bat';
		operation_obj.blog_global.init();
	},
	
	blog_review_reply_init:function(){
		frame_obj.submit_form_init($('#blog_review_edit_form'), './?m=operation&a=blog&d=review');
	},
	/**************************** 博客 end ****************************/

	partner_init:function(){
		frame_obj.del_init($('#partner .r_con_table'));
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		// frame_obj.del_bat($('.r_nav .del'), $('#partner .r_con_table input[name=select]'), function(id_list){
		// 	var $this=$(this);
		// 	global_obj.win_alert(lang_obj.global.del_confirm, function(){
		// 		$.get('./?do_action=operation.partner_del_bat&group_pid='+id_list, function(data){
		// 			if(data.ret==1){
		// 				window.location='./?m=operation&a=partner';
		// 			}
		// 		}, 'json');
		// 	}, 'confirm');
		// 	return false;
		// });
		frame_obj.del_bat($('.list_menu .del'), $('#partner .r_con_table input[name=select]'), 'operation.partner_del_bat'); //批量删除
		$('.r_con_table').on('click', '.used_checkbox .switchery', function(){//启用或关闭合作伙伴
			var $this=$(this),
				$tr=$this.parents('tr');
			if(!$this.hasClass('checked')){
				var IsUsed=1;
				$this.addClass('checked');
			}else{
				var IsUsed=0;
				$this.removeClass('checked');
			}
			$.post('?', 'do_action=operation.partner_used&PId='+$tr.attr('pid')+'&IsUsed='+IsUsed, function(data){
				if(data.msg){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		});
	},
	
	partner_edit_init:function(){
		/* 友情连接图片上传 */
		frame_obj.mouse_click($('#PicDetail .upload_btn, #PicDetail .pic_btn .edit'), 'img', function($this){
			frame_obj.photo_choice_init('PicDetail', '', 1);
		});
		frame_obj.switchery_checkbox();
		frame_obj.submit_form_init($('#partners_edit_form'), './?m=operation&a=partner');
	},

	//邮件群发
	mailchimp_send_init:function(){
		//图片上传
		
		//选择邮件模板
		frame_obj.fixed_right($('.btn_tpl_choice'), '.fixed_template_choice');
		$('.fixed_template_choice .top_menu li>a').click(function(){ //选项卡切换
			var $Type=$(this).attr('data-type');
			$(this).addClass('current').parent().siblings().find('a').removeClass('current');
			$('.template_choice_list .list[data-type='+$Type+']').show().siblings().hide();
		});
		$('.fixed_template_choice .top_menu li:eq(0)>a').click(); //默认点击第一个
		$('.template_choice_list .item').click(function(){
			var $This=$(this),
				$Tpl=$This.attr('data-tpl'),
				$Form=$('#template_choice_form'),
				$Type=$This.parent().attr('data-type');
			if($This.hasClass('cur')){ //已勾选
				$This.removeClass('cur');
				$Form.find('input[name=Template]').val(''); //清除选择
				$Form.find('input[name=Type]').val(''); //清零
			}else{ //未勾选
				$This.addClass('cur').siblings().removeClass('cur');
				$Form.find('input[name=Template]').val($Tpl); //选择了那个模板
				$Form.find('input[name=Type]').val($Type); //清零
			}
		});
		$('.template_choice_list .item .del_mask').click(function(){
			var $Obj=$(this).parent().parent(),
				$Tpl=$Obj.attr('data-tpl');
			global_obj.win_alert(lang_obj.global.del_confirm, function(){
				$.get('?do_action=operation.email_template_del&EId='+$Tpl, function(data){
					if(data.ret==1){
						$Obj.remove();
						global_obj.win_alert_auto_close(lang_obj.manage.global.del_success, '', 1000, '8%');
					}else{
						global_obj.win_alert_auto_close(lang_obj.manage.global.del_fail, 'fail', 1000, '8%');
					}
				}, 'json');
			}, 'confirm');
			return false;
		});
		
		//发送邮件
		frame_obj.submit_form_init($('#edit_form'), './?m=operation&d=mailchimp');
		
		frame_obj.submit_form_init($('#template_choice_form'), '', function(){ //选择邮件模板
			if($('#template_choice_form input[name=Template]').val()==''){
				global_obj.win_alert_auto_close(lang_obj.manage.email.not_model, 'fail', 1000, '8%');
				return false;
			}
		}, false, function(data){
			if(data.ret==1){
				$('#TemplatesDetail').attr('src',data.msg.PicPath);
				$('#TemplatesId').attr('src',data.msg.Id);
			}else{
				global_obj.win_alert_auto_close(lang_obj.manage.email.load_error, 'fail', 1000, '8%');
			}
			$('#template_choice_form .btn_submit').removeAttr('disabled');
			$('#fixed_right .btn_cancel').click();
			return false;
		},'json');
	}
}