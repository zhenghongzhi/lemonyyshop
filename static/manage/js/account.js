/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var account_obj={
	login_init:function(){
		if(ueeshop_config.FunVersion>=10){
			$('#login').css('margin-top', (($(window).height()-450)/2)+'px');
		}else{
			if($(window).width()<1150){$('#login').css('width',($(window).width())-40+'px');}
			$('#login').css('margin-top', (($(window).height()-400)/2)+'px');
		}
		$('form').submit(function(){return false;});
		$('input:submit').click(function(){
			var flag=false;
			$('#UserName, #Password').each(function(){
				if($(this).val()==''){
					$(this).focus();
					flag=true;
					return false;
				}
			});
			if(flag){return;}
			$('#login .tips').html(lang_obj.manage.account.log_in);
			$(this).attr('disabled', true);
			$.post('?', $('form').serialize(), function(data){
				$('input:submit').attr('disabled', false);
				if(data.ret==1){
					$('#login .tips').html(lang_obj.manage.account.log_in_ok);
					if(data.msg.FirstEnterManage==1){ //第一次进入后台默认跳到切换风格页面
						window.top.location='./?m=view&a=visual&d=edit';
					}else{
						window.top.location='./';
					}
				}else{
					$('#login .tips').html(data.msg);
				};
			}, 'json');
		});
		//token验证
		var url=window.location.href;
		if(url.indexOf('token')!=-1){
			var token=url.substr(url.indexOf('token')+6);
			$.post('?','do_action=account.login&token='+token,function(data){
				if(data.ret==1){
					if(data.msg.FirstEnterManage==1){ //第一次进入后台默认跳到切换风格页面
						window.top.location='./?m=view&a=visual&d=edit';
					}else{
						window.top.location='./';
					}
				}else{
					global_obj.win_alert(data.msg);
				}
			},'json');
		}
	},
	
	index_init:function(){
		account_obj.home_condition(function(time_s){
			var lang=$('body').hasClass('en')?'en':'zh-cn',
				par='do_action=account.get_account_data&TimeS='+time_s+'&lang='+lang,
				Data=new Object;
			$.post('./', par, function(data){
				var $Obj=$('#account .home_container'),
					$Html='';
				
				//总销售量
				frame_obj.highcharts_init.areaspline('orders_sales_charts', data.msg.orders_sales_charts);
				$('.home_total_sales .big_number>span').html(data.msg.total.orders_sales);
				
				//销量 and 会话
				frame_obj.highcharts_init.areaspline('session_charts', data.msg.session_charts);
				$('.home_session .big_number_session p').html(data.msg.total.session);
                $('.home_session .big_number_visitors p').html(data.msg.total.uv);
				
				//广告 切换显示
				$('.home_container .home_ad .title>a').click(function(){
					var $Type=$(this).attr('data-type');
					$(this).addClass('current').siblings().removeClass('current');
					$('.home_'+$Type).show().siblings().hide();
				});
                
                //总订单数
				frame_obj.highcharts_init.areaspline('orders_count_charts', data.msg.orders_count_charts);
				$('.home_total_orders .big_number>span').html(data.msg.total.orders_count);
				//转化率
				$('.home_conversion .big_number>span').html((data.msg.conversion.ratio.total?data.msg.conversion.ratio.total:0) + '%');
				$('.home_conversion .ratio_session').text(data.msg.conversion.ratio.session);
				$('.home_conversion .ratio_addtocart').text(data.msg.conversion.ratio.addtocart);
				$('.home_conversion .ratio_operate_addtocart').text(data.msg.conversion.ratio.addtocart_ratio);
				$('.home_conversion .ratio_placeorder').text(data.msg.conversion.ratio.placeorder);
				$('.home_conversion .ratio_operate_placeorder').text(data.msg.conversion.ratio.placeorder_ratio);
				$('.home_conversion .ratio_complete').text(data.msg.conversion.ratio.complete);
				$('.home_conversion .ratio_operate_complete').text(data.msg.conversion.ratio.complete_ratio);
				
				//流量来源
				frame_obj.highcharts_init.pie_donut_center_title('referrer_charts', data.msg.referrer_charts);
				
				//社交平台
				$Html='';
				if(data.msg.conversion.share_platform.uv){
					$.each(data.msg.conversion.share_platform.uv, function(key, value){
						$Html+='<tr data-key="'+key+'">\
							<td nowrap="nowrap" class="picture"><i class="icon_share_platform icon_share_platform_'+key+'"></i></td>\
							<td nowrap="nowrap" class="country">'+key+'</td>\
							<td nowrap="nowrap" class="percent">'+data.msg.conversion.share_platform.total[key]+'</td>\
							<td nowrap="nowrap" class="count">'+value+'</td>\
						</tr>';
					});
				}
				if($Html){
					$('.home_share_platform .no_data').hide();
				}else{
					$('.home_share_platform .no_data').show();
				}
				$('.home_share_platform .table_report_list tbody').html($Html);
				
				//销售排行
				$Html='';
				if(data.msg.products.sales){
					$.each(data.msg.products.sales, function(key, value){
						/*$Html+='<tr data-key="'+key+'">\
							<td nowrap="nowrap" class="picture"><img src="'+value['picture']+'" /></td>\
							<td class="name"><a href="'+value['url']+'" target="_blank" class="pro_name">'+value['name']+'</a></td>\
							<td nowrap="nowrap" class="count">'+value['count']+'</td>\
						</tr>';*/
						$Html+='<div class="item">\
							<div class="picture"><img src="'+value['picture']+'" /></div>\
							<div class="name"><a href="'+value['url']+'" target="_blank" class="pro_name">'+value['name']+'</a></div>\
							<div class="count">'+value['count']+'</div>\
						</div>';
					});
				}
				if($Html){
					$('.home_sales .no_data').hide();
				}else{
					$('.home_sales .no_data').show();
				}
				//$('.home_sales .table_report_list tbody').html($Html);
				$('.home_sales .div_report_list').html($Html);
				
				//订单分布
				$Html='';
				if(data.msg.orders.country){
					$.each(data.msg.orders.country, function(key, value){
						$Html+='<tr data-key="'+key+'">\
							<td nowrap="nowrap" class="picture"><div class="icon_flag flag_'+value['acronym']+'"></div></td>\
							<td nowrap="nowrap" class="country">'+value['title']+'</td>\
							<td nowrap="nowrap" class="percent">'+value['percent']+'</td>\
							<td nowrap="nowrap" class="count">'+value['count']+'</td>\
						</tr>';
					});
				}
				if($Html){
					$('.home_orders_country .no_data').hide();
				}else{
					$('.home_orders_country .no_data').show();
				}
				$('.home_orders_country .table_report_list tbody').html($Html);
				
				//搜索排行
				$Html='';
				if(data.msg.products.search){
					$.each(data.msg.products.search, function(key, value){
						title=global_obj.htmlspecialchars(value['title']);
						$Html+='<div class="item" data-key="'+key+'">\
							<div class="name" title="'+title+'">'+title+'</div>\
							<div class="percent">'+value['percent']+'</div>\
							<div class="count">'+value['count']+'</div>\
						</div>';
					});
				}
				if($Html){
					$('.home_search .no_data').hide();
				}else{
					$('.home_search .no_data').show();
				}
				$('.home_search .div_report_list').html($Html);
				
				//来源网址
				$Html='';
				if(data.msg.referrer.uv){
					$.each(data.msg.referrer.uv, function(key, value){
						$Html+='<div class="item">\
							<div class="name" title="'+key+'">'+key+'</div>\
							<div class="percent">'+data.msg.referrer.total[key]+'</div>\
							<div class="count">'+value+'</div>\
						</div>';
					});
				}
				if($Html){
					$('.home_referrer_url .no_data').hide();
				}else{
					$('.home_referrer_url .no_data').show();
				}
				$('.home_referrer_url .div_report_list').html($Html);
				
				//访问终端
				if(data.msg.conversion.terminal){
					$('.home_terminal .terminal_list>li:eq(0) strong').text(data.msg.conversion.terminal.pc);
					$('.home_terminal .terminal_list>li:eq(1) strong').text(data.msg.conversion.terminal.mobile);
				}
                
                global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
			}, 'json');
		});
		/*
		account_obj.index_change();
		$(window).resize(function(){
			account_obj.index_change();
		});
		*/
	},
	
	welfare_init:function(){
		$('.welfare_menu a').click(function(){
			var _ind = $(this).index();
			$('.welfare_menu a').removeClass('cur').eq(_ind).addClass('cur');
			$('#welfare .welfare_box').hide().eq(_ind).show();
		});
		$('.welfare_box .category a').click(function(){
			var _this = $(this),
				_ind = _this.index();
			_this.parents('.welfare_box').find('.category a').removeClass('cur').eq(_ind).addClass('cur');
			_this.parents('.welfare_box').find('.category_box').hide().eq(_ind).show();
		});
		$('.resource .list a.more').click(function(){
			$('#alert_box').show();
			$('#alert_box .html').html($(this).parents('tr').find('td.html').html());
		});
		$('#alert_box .alert_box .close').click(function(){
			$('#alert_box').hide();
		});
		$('.video_list .list').click(function(){
				global_obj.div_mask();
				$('.video_box').show();
			if($(this).hasClass('no_play')){
				$('#video_play').html($('.no_play_tips').html());
			}else{
				var alid=$(this).attr('data-alid');
				$.get('?','do_action=account.video_play&alid='+alid,function(data){
					if(data){
						var player = new Aliplayer({
							id:'video_play',
							width:'100%',
							height:'100%',
							autoplay:true,
							//useH5Prism:true,
							useFlashPrism:true,
							preload:true,
							controlBarVisibility:'hover',
							vid:alid,
							playauth:data.msg
						});
					}else{
						global_obj.div_mask(1);
						$('.video_box').hide();
					}
				}, 'json');
			}
		});
		$('.video_box .cloce').click(function(){
			global_obj.div_mask(1);
			$('.video_box').hide();
			$('#video_play').html('');
		});
	},
	index_change:function(){
		if($(window).width()>1280 && $(window).width()<=1600){ //合并销量排行，隐藏订单分布
			$('.home_sales').removeClass('mr10').parent().removeAttr('class').next().hide();
		}else{ //订单分布，销量排行，显示
			$('.home_sales').addClass('mr10').parent().addClass('home_parent_box').next().show();
		}
	},
	
	home_condition:function(callback, not_limit){
		$('.home_choice_day').hover(function(){
			$('.home_choice_day>dd').show().stop(true).animate({'top':58, 'opacity':1}, 250);
		}, function(){
			$('.home_choice_day>dd').stop(true).animate({'top':48, 'opacity':0}, 250, function(){ $(this).hide(); });
		});
		
		$('.home_choice_day>dd>a').click(function(){
			$(this).addClass('current').siblings().removeClass('current');
			$('.home_choice_day>dt>strong').html($(this).html());
			
			global_obj.win_alert_auto_close(lang_obj.global.loading, 'loading', -1);
			account_obj.home_condition_callback(callback);
			//global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
		});
		
		global_obj.win_alert_auto_close(lang_obj.global.loading, 'loading', -1);
		account_obj.home_condition_callback(callback);
		//global_obj.win_alert_auto_close('', 'loading', 500, '', 0);
	},
	
	home_condition_callback:function(callback){
		var time_s=$('.home_choice_day a.current').attr('data-rel');
		callback(time_s);
	},

}