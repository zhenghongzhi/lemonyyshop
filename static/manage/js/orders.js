/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var orders_obj={
		
		inbox_init:function(){
			
			orders_obj.inbox_resize();
			$(window).resize(function(){
				orders_obj.inbox_resize();
			});
			
			$(window).load(function(){
				orders_obj.inbox_reload(0);
			});
//			alert(123);return;
//			$('.inbox_container .message_list').scroll(function(){
//				var $This=$(this),
//					$ViewHeight=$This.outerHeight(true), //可见高度  
//					$ContentHeight=$This.get(0).scrollHeight, //内容高度  
//					$ScrollHeight=$This.scrollTop(), //滚动高度
//					$Page=parseInt($This.attr('data-page')),
//					$Total=parseInt($This.attr('data-total-pages')),
//					$Count=parseInt($This.attr('data-count')),
//					$IsAnimate=parseInt($This.attr('data-animate'));
//				if(($ContentHeight-$ViewHeight<=$ScrollHeight) && $IsAnimate==0 && $Page<$Total){ 
//					$IsAnimate=1;
//					$This.attr({'data-page':$Page+1, 'data-animate':$IsAnimate});
//					orders_obj.inbox_reload(0);
//				}
//				if($Page>=$Total && $IsAnimate==0 && $ContentHeight-$ViewHeight==$ScrollHeight-1){
//					global_obj.win_alert_auto_close(lang_obj.manage.sales.lastpage, 'await', 2000, '8%', 0);
//				}
//			});
			
//			$('#search_form').on('mouseenter', '.search_btn', function(){
//				$('#search_form').addClass('show');
//			}).on('click', '.search_btn', function(){ //搜索按钮
//				alert(111);
//				return false;
//			}).on('submit', function(){
//				$('#search_form .search_btn').click();
//				return false;
//			});
//			
//			$('body').on('click', '#inbox .message_list>li', function(){ //左侧栏目
//				var $Obj=$('.inbox_right');
//				$(this).addClass('current').siblings().removeClass('current');
//				if($Obj.find('.unread_message').is(':visible')){
//					$Obj.find('.unread_message').hide();
//					$Obj.find('.message_title, .message_dialogue, .message_bottom').show();
//				}
//				$Obj.find('.message_dialogue_list').html(''); //清空对话栏
//				$Obj.find('.menu_products_list').attr('data-page', 0);
//				orders_obj.inbox_products_list();
//				return false;
//			}).on('click', '.products_menu', function(){
//				var $Obj=$('.inbox_right .menu_products_list');
//				if($Obj.is(':hidden')){
//					$('.inbox_right .menu_products_list').show();
//					$('.inbox_right .menu_products_list .list').css('height', $('.inbox_container').height()-$('.inbox_right .message_title').outerHeight()-$('.inbox_right .message_bottom').outerHeight()-1); //1:边框 10:内间距
//				}else{
//					$('.inbox_right .menu_products_list').hide();
//				}
//				return false;
//			})
			$('body').on('click', '.menu_products_list .item', function(){ //右侧栏目
				var $This=$(this),
					$Obj=$('.inbox_right'),
					$MId=$This.attr('data-id'),
					$UserId=$This.attr('data-user-id');
					$oid=$This.attr('data-oid');
				$This.addClass('current').siblings().removeClass('current');
				$Obj.find('.message_dialogue_list').html(''); //清空对话栏
				$.post('?', {'do_action':'orders.inbox_view', 'MId':$MId, 'UserId':$UserId,'OId':$oid}, function(data){
					if(data.ret==1){
						var $Html='', $Count=0;
//						$Obj.find('.message_title>h2').html(data.msg.Email);
						$Obj.find('.message_title .products_view .products_info img').attr('src', data.msg.PicPath);
						$Obj.find('.message_title .products_view .products_info .name>a').html(data.msg.Name).attr('title', data.msg.Name);
						if(data.msg.Url){
							$Obj.find('.message_title .products_view .products_info a').attr('href', data.msg.Url).attr('target', '_blank');
						}else{
							$Obj.find('.message_title .products_view .products_info a').attr('href', 'javascript:;').removeAttr('target');
						}
						
//						if(data.msg.Reply==null)
//							{
//							$(".inbox_container").hide();
//							$("#inbox").height(0);
//							$("#inbox_count").html("(0 条未读)");
//							}
						for(k in data.msg.Reply){
							if((k==0 && data.msg.Reply[k].Type==1) || (data.msg.Reply[k].UserId==0 && !data.msg.Reply[k].CusEmail)){ //回答
								$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
								$Html+=		'<div class="time color_aaa">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message color_000">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}else{ //追问
								$Html+=	'<div class="dialogue_box dialogue_box_left clean">';
								$Html+=		'<div class="time color_aaa">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message color_000">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}
						}
						$Obj.find('.menu_products_list').hide();
						$Obj.find('.message_dialogue_list').append($Html);
						if($Obj.find('.message_dialogue_list .dialogue_box').size()) $Obj.find('.message_dialogue').animate({scrollTop:$Obj.find('.message_dialogue_list .dialogue_box:last').offset().top}, 10); //自动滚动到最后一个消息
						$Obj.find('input[name=MId]').val(data.msg.MId);
						$This.find('i').remove();
						$This.siblings().find('i').each(function(){
							$Count+=parseInt($(this).text());
						});
						var id_obj;
						if($UserId>0){
							id_obj='data-user-id='+$UserId;
							$('.inbox_right .message_bottom .send_box').show();
							$('.inbox_right .message_bottom .mail_box').hide();
						}else{
							$('.inbox_right .message_bottom .mail_box a').attr('href', '?m=operation&a=email&Email='+data.msg.Email);
							$('.inbox_right .message_bottom .send_box').hide();
							$('.inbox_right .message_bottom .mail_box').show();
							$This.parent().find('.item').each(function(index){
								var id=$(this).data('id');
								if($('.inbox_left .message_list li[data-id='+id+']').size()){
									id_obj='data-id='+id;
									return false;
								}
							});
						}
						$('.inbox_left .message_list li['+id_obj+']>i').text($Count); //更新左侧栏目的未读数量
						if($Count==0){ //未读数量低于1
							$Obj.find('.products_view .products_menu>i').hide();
							$('.inbox_left .message_list li['+id_obj+']>i').hide();
						}
					}
				}, 'json');
				return false;
			}).on('click', '.menu_products_list .btn_more', function(){
				//加载更多
				orders_obj.inbox_products_list();
				return false;
			});
			$(document).click(function(){
				$('.inbox_right .menu_products_list').hide();
			});
	        
	        //直接回车发送消息
	        $('#message_inbox_form textarea[name=Message]').on('keyup', function(e){
				var $Key=window.event?e.keyCode:e.which;
				if($Key==13){ //回车键
					$('#message_inbox_form .btn_submit').click();
				}
	            return false;
	        });
			
			//站内信消息提交回复
			frame_obj.submit_form_init($('#message_inbox_form'), '', '', 0, function(data){
				if(data.ret==1){
					var $Obj=$('.inbox_right'),
						$Form=$('#message_inbox_form'),
						$Pic=$('#MsgPicDetail .img');
						$Html='';
					$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
					$Html+=		'<div class="time color_aaa">'+data.msg.Time+'</div>';
					$Html+=		'<div class="message color_000">'+data.msg.Content+'</div>';
					if(data.msg.PicPath){
					$Html+=		'<div class="picture"><a href="'+data.msg.PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.PicPath+'" /><span></span></a></div>';
					}
					$Html+=	'</div>';
					$Html+=	'<div class="clear"></div>';
					$Obj.find('.message_dialogue_list').append($Html);
					$Obj.find('.message_dialogue').animate({scrollTop:$('.inbox_right .message_dialogue_list').outerHeight()}, 500); //自动滚动到最后一个消息
					$Form.find('textarea[name=Message]').val('');
					$Pic.removeClass('isfile').removeClass('show_btn');
					$Pic.find('.preview_pic .upload_btn').show();
					$Pic.find('.preview_pic a').remove();
					$Pic.find('.preview_pic input:hidden').val('').attr('save', 0);
					global_obj.win_alert_auto_close(lang_obj.manage.email.send_success, '', 1000, '8%');
				}
				return false;
			});
			var $cl=$('.inbox_right');
			$cl.find('.menu_products_list .list .item:eq(0)').click();
		},
		inbox_resize:function(){
			var $iHeight=$('#inbox').outerHeight(),
				$navHeight=$('#inbox .inside_container').outerHeight();
			$('.inbox_container').css('height', $iHeight-$navHeight-40-2); //40:外间距 2:边框
			$('.inbox_left .message_list').css('height', $('.inbox_container').height()-$('.inbox_left .search').outerHeight()-1);
			$('.inbox_right .unread_message').css('height', $('.inbox_container').height());
			$('.inbox_right .message_dialogue').css('height', $('.inbox_container').height()-$('.inbox_right .message_title').outerHeight()-$('.inbox_right .message_bottom').outerHeight()-1-10); //1:边框 10:内间距
		},
		
		inbox_reload:function($Clear){
			var $Obj=$('.inbox_left .message_list'),
				$Page=parseInt($Obj.attr('data-page')),
				$Total=parseInt($Obj.attr('data-total-pages')),
				$Count=parseInt($Obj.attr('data-count')),
				$Input=$('#search_form .box_input'),
				$Search='';
			$Input.width()>0 && ($Search=$.trim($Input.val()));
			$Clear==1 && ($Page=1);
			$.post('?', {'do_action':'user.inbox_list', 'Page':$Page, 'Count':$Count, 'Keyword':$Search, 'IsClear':$Clear}, function(data){
				if(data.ret==1){
					var $Html='';
					for(k in data.msg.List){
						$Html+=	'<li data-id="'+data.msg.List[k].MId+'" data-user-id="'+data.msg.List[k].UserId+'">';
						if(data.msg.List[k].sum>0){
							$Html+=	'<i>'+data.msg.List[k].sum+'</i>';
						}
						$Html+=		'<div class="email color_555">'+data.msg.List[k].NewEmail+'</div>';
						$Html+=		'<div class="message color_aaa">'+data.msg.List[k].Content+'</div>';
						$Html+=	'</li>';
					}
					if($Clear==1){
						$('.inbox_left .message_list').html($Html);
						$Obj.scrollTop(0);
						$Obj.attr('data-page', 1);
						$Obj.attr('data-total-pages', data.msg.Total);
					}else{
						$('.inbox_left .message_list').append($Html);
					}
					$Obj.attr('data-animate', 0)
				}
			}, 'json');
		},
		
		inbox_products_list:function(){
			var $Obj=$('.inbox_right'),
				$Page=parseInt($Obj.find('.menu_products_list').attr('data-page')),
				$MId=$('.inbox_left .message_list li.current').attr('data-id'),
				$UserId=$('.inbox_left .message_list li.current').attr('data-user-id');
			$.post('?', {'do_action':'user.inbox_view_list', 'MId':$MId, 'UserId':$UserId, 'Page':$Page}, function(data){
				var $Html='';
				$Obj.find('.menu_products_list .more').remove();
				if(data.ret==1){
					$Obj.find('.message_title .products_view').show();
					if(data.msg.NoRead>0){
						$Obj.find('.message_title .products_menu>i').show();
					}else{
						$Obj.find('.message_title .products_menu>i').hide();
					}
					if(data.msg.Message){
						$Html+=	'<li class="item" data-id="'+data.msg.Message.MId+'" data-user-id="'+data.msg.Message.UserId+'">';
						if(data.msg.Message.Read>0){
						$Html+=		'<i>'+data.msg.Message.Read+'</i>';
						}
						$Html+=		'<div class="img"><img src="'+data.msg.Message.PicPath+'" /></div>';
						$Html+=		'<div class="name color_555">'+data.msg.Message.Name+'</div>';
						$Html+=	'</li>';
					}
					if(data.msg.Products){ //有产品信息
						for(k in data.msg.Products){
							$Html+=	'<li class="item" data-id="'+data.msg.Products[k].MId+'" data-user-id="'+data.msg.Products[k].UserId+'">';
							if(data.msg.Products[k].Read>0){
							$Html+=		'<i>'+data.msg.Products[k].Read+'</i>';
							}
							$Html+=		'<div class="img"><img src="'+data.msg.Products[k].PicPath+'" /></div>';
							$Html+=		'<div class="name color_555" title="'+data.msg.Products[k].Name+'">'+data.msg.Products[k].Name+'</div>';
							$Html+=	'</li>';
						}
						$Obj.find('.message_title .products_info').removeClass('products_info_msg');
						$Obj.find('.message_title .products_menu').show();
						$Obj.find('.message_title .products_view').show();
					}else{ //没有产品信息
						$Obj.find('.message_title .products_info').addClass('products_info_msg');
						$Obj.find('.message_title .products_menu').hide();
						$Obj.find('.message_title .products_view').hide();
					}
					if(data.msg.Over>0){
						$Html+=	'<li class="more"><a href="javascript:;" class="btn_global btn_more">'+lang_obj.manage.global.more+'</a></li>';
					}
					if($Page==0){ //第一次加载
						$Obj.find('.menu_products_list .list').html($Html);
						$Obj.find('.menu_products_list .list .item:eq(0)').click(); //默认点击第一个选项
					}else{
						$Obj.find('.menu_products_list .list').append($Html);
					}
					$Obj.find('.menu_products_list').attr('data-page', $Page+1);
				}
			}, 'json');
		},

		
	function_init:{
		select_country:function(CId, SId){
			var vselect='', str='';
			$.post('?', {'do_action':'orders.select_country', 'CId':CId}, function(data){
				if(data.ret==1){
					d=data.msg.contents;
					if(d==-1){
						$('#zoneId').css({'display':'none'}).find('select').attr('disabled', true);
						$('#state').css({'display':'table-row'}).find('input').removeAttr('disabled');
					}else{
						$('#zoneId').css({'display':'table-row'}).find('select').removeAttr('disabled');
						$('#state').css({'display':'none'}).find('input').attr('disabled', true);
						for(i=0; i<d.length; ++i){
							vselect+='<option value="'+d[i]['SId']+'">'+d[i]['States']+'</option>';
						}
						$('#zoneId select').html(vselect);
						SId>0 && $('#zoneId select').find('option[value='+SId+']').attr('selected', true);
					}
					$('#countryCode').text('+'+data.msg.code);
					$('#phoneSample span').text(data.msg.code);
					$('#shipping_address_form input[name=CountryCode]').val('+'+data.msg.code);
					if(data.msg.cid==30){
						$('#taxCode').css({'display':'table-row'}).find('select, input').removeAttr('disabled');
						$('#tariffCode').css({'display':'none'}).find('select, input').attr('disabled', true).parent().find('p.errorInfo').text('');
					}else if(data.msg.cid==211){
						$('#tariffCode').css({'display':'table-row'}).find('select, input').removeAttr('disabled');
						$('#taxCode').css({'display':'none'}).find('select, input').attr('disabled', true).parent().find('p.errorInfo').text('');
					}else{
						$('#taxCode').css({'display':'none'}).find('select, input').attr('disabled', true).parent().find('p.errorInfo').text('');
						$('#tariffCode').css({'display':'none'}).find('select, input').attr('disabled', true).parent().find('p.errorInfo').text('');
					}
				}
			}, 'json');
		},
		
		orders_inbox:function(){
			frame_obj.pop_up($('.r_con_table .operation .inbox, .orders_payment .orders_source .btn_message'), '.pop_orders_message', 1, function($This){
				var $Obj=$('.pop_orders_message'),
					$OId=$This.attr('data-id');
				$Obj.find('.message_dialogue_list').html(''); //清空对话栏
				$Obj.find('.message_dialogue').css('height', $Obj.outerHeight()-$Obj.find('.message_title').outerHeight()-$Obj.find('.message_bottom').outerHeight()-10);
				$.post('?', {'do_action':'action.message_orders_view', 'OId':$OId}, function(data){
					if(data.ret==1){
						var $Html='';
						$Obj.find('.message_title .title strong').html('No#'+data.msg.OId);
						$Obj.find('.message_title .email').html(data.msg.Email);
						for(k in data.msg.Reply){
							if((k==0 && data.msg.Reply[k].Type==1) || (k>0 && data.msg.Reply[k].UserId==0)){ //回答
								$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
								$Html+=		'<div class="time">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}else{ //追问
								$Html+=	'<div class="dialogue_box dialogue_box_left clean">';
								$Html+=		'<div class="time">'+data.msg.Reply[k].Time+'</div>';
								$Html+=		'<div class="message">'+data.msg.Reply[k].Content+'</div>';
								if(data.msg.Reply[k].PicPath){
								$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
								}
								$Html+=	'</div>';
								$Html+=	'<div class="clear"></div>';
							}
						}
						$Obj.find('.message_dialogue_list').append($Html);
						$Obj.find('.message_dialogue').animate({scrollTop:$Obj.find('.message_dialogue_list .dialogue_box:last').offset().top}, 10); //自动滚动到最后一个消息
						$Obj.find('input[name=MId]').val(data.msg.MId);
					}
				}, 'json');
			});
		}
	},
	
	orders_init:function(){
		orders_obj.function_init.orders_inbox(); //订单询盘事件
		frame_obj.del_init($('#orders .r_con_table')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del, .list_menu_button .change_status')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'orders.orders_del_bat'); //批量删除
		frame_obj.del_bat($('.list_menu_button .change_status'), $('input[name=select]'), 'orders.orders_status_bat', '', '', lang_obj.manage.orders.change_status); //批量删除
		$('#orders .r_con_table .sales_select').on('dblclick', function(){
			var $obj=$(this),
				$salesid=$obj.attr('data-id'),
				$OrderId=$obj.parents('tr').find('td:eq(0)>input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#sales_select_hide').html(),
				$val;
			$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
			$salesid && $obj.find('select').val($salesid).focus();
			$obj.find('select').on('blur', function(){
				$val=$(this).val();
				if($val && $val!=$salesid){
					$.post('?', 'do_action=orders.orders_edit_sales&OrderId='+$OrderId+'&SalesId='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.html(data.msg);
							$obj.attr('data-id', $val);
						}
					}, 'json');
				}else{
					$obj.html($obj.find('span').html());
				}
			});
		});
		
		// $('.list_menu_button .explode').click(function(){
		// 	if($('input[name=select]:checked').size()>0){ //有已勾选的订单
		// 		var $Data=$('input[name=select]:checked').map(function(){
		// 				return $(this).val();
		// 			}).get();
		// 		$.post('?', {'do_action':'orders.orders_export', 'Number':0, 'OrderId':$Data.join(',')}, function(data){
		// 			if(data.ret==2){
		// 				window.location='./?do_action=orders.orders_export_down&Status=ok';
		// 				global_obj.win_alert_auto_close(lang_obj.manage.global.export_success, '', 1000, '8%');
		// 			}
		// 			$('#fixed_right .btn_cancel').click();
		// 		}, 'json');
		// 		return false;
		// 	}
		// });
	},
	orders_request_init:function(){
		orders_obj.function_init.orders_inbox(); //订单询盘事件
		frame_obj.del_init($('#source_request .r_con_table')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del, .list_menu_button .change_status')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'orders.orders_request_del_bat'); //批量删除
		frame_obj.del_bat($('.list_menu_button .change_status'), $('input[name=select]'), 'orders.orders_request_status_bat', '', '', lang_obj.manage.orders.change_status); //批量修改完成
		$('#orders .r_con_table .sales_select').on('dblclick', function(){
			var $obj=$(this),
				$salesid=$obj.attr('data-id'),
				$OrderId=$obj.parents('tr').find('td:eq(0)>input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#sales_select_hide').html(),
				$val;
			$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
			$salesid && $obj.find('select').val($salesid).focus();
			$obj.find('select').on('blur', function(){
				$val=$(this).val();
				if($val && $val!=$salesid){
					$.post('?', 'do_action=orders.orders_edit_sales&OrderId='+$OrderId+'&SalesId='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.html(data.msg);
							$obj.attr('data-id', $val);
						}
					}, 'json');
				}else{
					$obj.html($obj.find('span').html());
				}
			});
		});
		
		// $('.list_menu_button .explode').click(function(){
		// 	if($('input[name=select]:checked').size()>0){ //有已勾选的订单
		// 		var $Data=$('input[name=select]:checked').map(function(){
		// 				return $(this).val();
		// 			}).get();
		// 		$.post('?', {'do_action':'orders.orders_export', 'Number':0, 'OrderId':$Data.join(',')}, function(data){
		// 			if(data.ret==2){
		// 				window.location='./?do_action=orders.orders_export_down&Status=ok';
		// 				global_obj.win_alert_auto_close(lang_obj.manage.global.export_success, '', 1000, '8%');
		// 			}
		// 			$('#fixed_right .btn_cancel').click();
		// 		}, 'json');
		// 		return false;
		// 	}
		// });
	},
	orders_export_init:function(){
		$('.tab_box .tab_box_btn').click(function(){
			var $Value=$(this).attr('data-value');
			$('#export_edit_form input[name=TimeType]').val($Value);
		});
		
		$('input[name=Time]').daterangepicker({
			showDropdowns:true,
			format:'YYYY-MM-DD HH:mm'
		});
		
		frame_obj.fixed_right($('.btn_option_set'), '.fixed_export_config');
		frame_obj.dragsort($('.fixed_export_config .export_menu'), '', 'li', 'a', '<li class="placeHolder"></li>'); //导出选项拖动
		
		//选项设置
		$('#export_config_form .btn_submit').click(function(){
			var $Form=$('#export_config_form'),
				$Data=$Form.find('.export_menu li').map(function(){
					return $(this).find('input').val();
				}).get();
			$.post('?', $Form.serialize()+'&sort_order='+$Data.join('|'), function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
				}
				$('#fixed_right .btn_cancel').click();
			}, 'json');
			return false;
		});
		
		if(ueeshop_config.FunVersion==10 && $('#export_edit_form .btn_submit').hasClass('cdx_limit')){
			$('#export_edit_form .btn_submit').click();
		}
		
		//导出
		$('#export_edit_form .btn_submit').click(function(){
			if(!$('.loading_bg').length){
				$('body').append('<div class="loading_bg" style="width:100%;height:100%;top:0;left:0;position:fixed;z-index:10001;background: rgba(0,0,0,0.4) url(/static/themes/default/images/global/loading.gif) no-repeat center center;"></div>');
			}
		});
		frame_obj.submit_form_init($('#export_edit_form'), '', '', '', function(data){
			var $Form=$('#export_edit_form');
			if(data.ret==2){
				$('#export_progress_export').append(data.msg[1]);
				$Form.find('input[name=Number]').val(data.msg[0]);
				$Form.find('.btn_submit').click();
			}else if(data.ret==1){ //初始化
				$Form.find('input[name=Number]').val(0);
				$Form.find('btn_submit').removeAttr('disabled');
				$('.loading_bg').remove();
				window.location='./?do_action=orders.orders_export_down&Status=ok';
			}else{
				global_obj.win_alert_auto_close(lang_obj.manage.orders.not_orders, 'fail', 1000, '8%');
				setTimeout(function(){
					$('.loading_bg').remove();
				}, 1000);
			}
		});
	},
	
	orders_view:function(){
		//订单询盘事件
		orders_obj.function_init.orders_inbox();
		
		//调整“订单详情”和“右侧”的高度对齐
		if($('.orders_info').length){
			var obj=$('.orders_info'),
				leftH=obj.parent().outerHeight(),
				rightH=obj.parent().next().outerHeight(),
				maxH=0;
			maxH=Math.max(leftH, rightH);
			obj.css('height', maxH-42); //30:内间距 10:右侧的间隔 2:边框
		}
		
		//修改订单价格信息
		frame_obj.fixed_right($('#edit_price_list'), '.fixed_orders_info');
		
		//更改订单状态
		frame_obj.fixed_right($('.orders_status .btn_orders_status[data-status=7]'), '.fixed_shipped');
		frame_obj.fixed_right($('.edit_btn_orders_status'), '.edit_form_orders_status');
		$('.edit_form_orders_status select[name=OrderStatus]').change(function(){
			var $this = $(this),
				$val = $this.val();
				if($val==7){
					$('.edit_form_orders_status .shipping_box').removeClass('hide');
					$('.edit_form_orders_status').find('input[name^="TrackingNumber"], input[name^="OvTrackingNumber"]').attr('notnull', 'notnull');
				}else{
					$('.edit_form_orders_status .shipping_box').addClass('hide');
					$('.edit_form_orders_status').find('input[name^="TrackingNumber"], input[name^="OvTrackingNumber"]').removeAttr('notnull');
				}
		});
		$('.orders_status .btn_orders_status').click(function(){
			var $Status=$(this).attr('data-status'),
				$OrderId=$('#OrderId').val();
			if($Status!=7){ //除了“已发货”
				$.post('./?do_action=orders.orders_mod_status', {'OrderId':$OrderId, 'OrderStatus':$Status}, function(data){
					if(data.ret==1){
						window.location.reload();
					}
				}, 'json');
			}
			return false;
		});
		$('#fixed_right .fixed_shipped input.shipping_time').daterangepicker({//发货时间插件加载
			singleDatePicker:true,
			showDropdowns:true,
			timePicker:false,
			format:'YYYY-MM-DD'
		});
		
		//退款
		$('.orders_status .btn_refund').click(function(){
			var $This=$(this),
				$OrderId=$('#OrderId').val();
			if($This.hasClass('btn_loading')) return false;
			global_obj.win_alert(lang_obj.manage.orders.refund_confirm, function(){
				$This.addClass('btn_loading');
				$.post('./?do_action=orders.orders_refund', {'OrderId':$OrderId}, function(result){
					var $Html='';
					$Html+=	'<div class="global_container orders_refund_box" style="opacity:0;">';
					$Html+=		'<strong class="refund_left">'+lang_obj.manage.orders.refund_status[result.Status]+'</strong>';
					$Html+=		'<span class="refund_right">';
					$Html+=			lang_obj.manage.orders.transaction+': '+result['Number']+'&nbsp;&nbsp;&nbsp;&nbsp;';
					$Html+=			lang_obj.manage.orders.refund_amount+': '+result.Amount+'&nbsp;&nbsp;&nbsp;&nbsp;';
					$Html+=			lang_obj.manage.orders.refund_time+': '+result.Time;
					$Html+=		'</span>';
					$Html+=	'</div>';
					$('.box_order_info .orders_number').after($Html);
					$('.box_order_info .orders_refund_box').animate({'opacity':1}, 1000);
					$This.removeClass('btn_loading');
					if(result.Status>0){
						$('.orders_status .btn_refund').fadeOut(1000);
					}
					global_obj.win_alert_auto_close(lang_obj.manage.orders.refund_status[result.Status], (result.Status==0?'fail':''), 2000, '20%');
                    setTimeout(function(){
                        window.location.reload();
                    }, 2500);
				}, 'json');
			}, 'confirm');
			return false;
		});
		
		//修改订单产品信息
		$('#edit_products_list').click(function(){
			var $This=$(this),
				$Form=$('#orders_products_form');
			if($This.attr('data-save')==0){ //准备修改
				$This.text(lang_obj.manage.global.save).attr('data-save', 1);
				$Form.find('.show_input').show().find('.box_input').removeAttr('disabled').attr('notnull', 'notnull');
				$Form.find('.show_txt').hide();
				$Form.find('.del').show();
			}else{ //准备保存
				if(global_obj.check_form($Form.find('*[notnull]'))){return false;};
				$.post('?', $Form.serialize(), function(data){
					if(data.ret==1){
						var $Index=0;
						$('#orders_products_list tbody tr').each(function(){
							$Index=$(this).index();
							if(data.msg.ProInfo[$Index]){
								$(this).find('td[data-type=price]').find('.show_txt').html(ueeshop_config.currency+data.msg.ProInfo[$Index].Price).next().find('.box_input').val(data.msg.ProInfo[$Index].Price);
								$(this).find('td[data-type=qty]').find('.show_txt').html(data.msg.ProInfo[$Index].Qty).next().find('.box_input').val(data.msg.ProInfo[$Index].Qty);
								$(this).find('td[data-type=price_rate]').find('.show_txt').html((data.msg.ProInfo[$Index].price_rate==null)?0:data.msg.ProInfo[$Index].price_rate);
								$(this).find('td[data-type=amount]>span').html(ueeshop_config.currency+data.msg.ProInfo[$Index].Amount);
								$(this).find('td[data-type=front_amount]>span').html(ueeshop_config.currency+data.msg.ProInfo[$Index].front_amount);
								if(data.msg.WebProInfo){
									//下单所相应的货币价格
									$(this).find('td[data-type=price]').find('.show_web').html(data.msg.WebProInfo[$Index].Price);
									$(this).find('td[data-type=amount]').find('.show_web').html(data.msg.WebProInfo[$Index].Amount);
								}
							}
						});
						$('#orders_front_product_price').html(ueeshop_config.currency+data.msg.front_ProductPrice);
						$('#orders_end_product_price').html(ueeshop_config.currency+(data.msg.TotalPrice-data.msg.front_ProductPrice).toFixed(2));
						$('#orders_total_price').html(ueeshop_config.currency+data.msg.TotalPrice);
						$('#orders_product_price').html(ueeshop_config.currency+data.msg.ProductPrice);
						$('#orders_products_list tbody tr:last').find('td:eq(1)').html(data.msg.TotalQty).next().next().find('span').html(ueeshop_config.currency+data.msg.front_ProductPrice).parent().next().find('span').html(ueeshop_config.currency+data.msg.ProductPrice);
						if(data.msg.WebProInfo){
							//下单所相应的货币价格
							$('#orders_total_price_web').html(data.msg.WebTotalPrice);
							$('#orders_product_price_web').html(data.msg.WebProductPrice);
							$('#orders_products_list tbody tr:last').find('td:eq(2) .show_web').html(data.msg.WebTotalPrice);
						}
						global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
					}else{
						global_obj.win_alert_auto_close(lang_obj.global.save_fail, 'fail', 1000, '8%');
					}
					$This.text(lang_obj.manage.global.edit).attr('data-save', 0);
					$Form.find('.show_input').hide().find('.box_input').removeAttr('notnull').attr('disabled', true);
					$Form.find('.show_txt').show();
					$Form.find('.del').hide();
				}, 'json');
			}
		});
		
		//删除订单产品
		$('#orders_products_list .del').click(function(){
			$(this).parent().parent().remove();
			if($('#orders_products_list tbody tr').size()<3){
				$('#orders_products_list .del').hide();
			}
		});
		
		//提交备注日志
		$('.form_remark_log .btn_save').click(function(){
			var $Form=$(this).parent().parent(),
				$Log=$Form.find('.box_input').val(),
				$OrderId=$('#OrderId').val();
			if($.trim($Log)==''){
				$Form.css('border-color', 'red');
				return false;
			}else{
				$Form.removeAttr('style');
			}
			$.post('./?do_action=orders.orders_remark_log', {'OrderId':$OrderId, 'Log':$Log}, function(data){
				if(data.ret==1){
					$Form.find('.box_input').val('');
					var $Html='';
					$Html+=	'<li>';
					$Html+=		'<div class="time">'+data.msg.AccTime+'</div>';
					$Html+=		'<div class="information">';
					$Html+=			'<i class="radio"></i>';
					$Html+=			'<div class="log">'+data.msg.Log+'</div>';
					$Html+=			'<div class="user">('+lang_obj.manage.global.admin+') '+data.msg.UserName+'</div>';
					$Html+=		'</div>';
					$Html+=	'</li>';
					$('.orders_remark_log .log_list').prepend($Html);
					global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
				}else{
					global_obj.win_alert_auto_close(lang_obj.global.save_fail, 'fail', 1000, '8%');
				}
			}, 'json');
			return false;
		});
		
		//修改配送地址
		frame_obj.fixed_right($('#edit_address'), '.fixed_shipping_address', function(){
			var $OrderId=$('#OrderId').val(),
				$Form=$('#shipping_address_form');
			$.post('?', {'do_action':'orders.get_shipping_address', 'OrderId':$OrderId}, function(data){
				if(data.ret==1){
					$Form.find('input[name=FirstName]').val(data.msg.FirstName);
					$Form.find('input[name=LastName]').val(data.msg.LastName);
					$Form.find('input[name=AddressLine1]').val(data.msg.AddressLine1);
					$Form.find('input[name=AddressLine2]').val(data.msg.AddressLine2);
					$Form.find('input[name=City]').val(data.msg.City);
					$Form.find('select[name=country_id]').find('option[value='+data.msg.CId+']').eq(0).attr('selected', true);
					if(data.msg.CId==30 || data.msg.CId==211){
						$Form.find('select[name=tax_code_type]').find('option[value='+data.msg.CodeOption+']').attr('selected', 'selected');
						$Form.find('input[name=tax_code_value]').attr('maxlength', (data.msg.CodeOption==1?11:14)).val(data.msg.TaxCode);
					}
					if(data.msg.SId>0){
						//$Form.find('select[name=Province]').find('option[value='+data.msg.SId+']').attr('selected', true);
					}else{
						$Form.find('input[name=State]').val(data.msg.State);
					}
					$Form.find('input[name=ZipCode]').val(data.msg.ZipCode);
					$Form.find('input[name=CountryCode]').val(data.msg.CountryCode);
					$Form.find('input[name=PhoneNumber]').val(data.msg.PhoneNumber);
					
					orders_obj.function_init.select_country(data.msg.CId, data.msg.SId);
					
					$Form.find('select[name=country_id]').off().on('change', function(){ //选择国家
						orders_obj.function_init.select_country($(this).val(), 0);
					});
				}
			}, 'json');
		});
		
		//修改配送信息
		frame_obj.fixed_right($('.orders_shipping .btn_shipping_edit'), '.fixed_shipping_info', function($This){
			var $ID=$This.parent().parent().attr('data-id'),
				$Form=$('#shipping_info_form'),
				$OrderId=$Form.find('input[name=OrderId]').val();
			$Form.find('input[name=OvId]').val($ID);
			$.post('./?do_action=orders.orders_shipping_method', {'OrderId':$OrderId}, function(data){
				if(data.ret==1){
					var $Html='', $Obj;
					if($ID==0){
						for(OvId in data.msg.info){
							$ID=OvId;
						}
						$Form.find('input[name=OvId]').val($ID); //重新定义
					};
					if(data.msg.info[$ID]){
						$Obj=data.msg.info[$ID];
						start=0;
						if(parseInt(data.msg.isOverseas)==1){
							SId=data.msg.orders_info.ShippingOvSId[$ID];
							isInsurance=data.msg.orders_info.ShippingOvInsurance[$ID];
						}else{
							SId=data.msg.orders_info.ShippingMethodSId;
							isInsurance=data.msg.orders_info.ShippingInsurance;
						}
						for(i=0; i<$Obj.length; i++){
							rowObj=$Obj[i];
							if(parseFloat(rowObj.ShippingPrice)<0) continue;
							selected='';
							start=i;
							if(SId==rowObj.SId){
								selected='selected';
								start=i;
							}
							$Html+='<option value="'+rowObj.SId+'" data-type="'+rowObj.type+'" data-insurance="'+rowObj.InsurancePrice+'"  '+selected+'>'+rowObj.Name.toUpperCase()+' - '+ueeshop_config.currency+rowObj.ShippingPrice+'</option>';
						}
						$Form.find('select[name=ShippingMethodSId]').html($Html);
						$Form.find('input[name=ShippingMethodType]').val($Obj[start].type);
						$Form.find('.shipping_method_insurance').text(ueeshop_config.currency+$Obj[start].InsurancePrice);
						if(isInsurance){
							$Form.find('.insurance_info .input_checkbox_box').addClass('checked').find('input').attr('checked', true);
						}else{
							$Form.find('.insurance_info .input_checkbox_box').removeClass('checked').find('input').removeAttr('checked');
						}
						if(data.msg.shipping_template==1){
							$Form.find('.insurance_info').hide();
						}
					}
					$('#fixed_right .fixed_shipping_info input.shipping_time').daterangepicker({
						singleDatePicker:true,
						showDropdowns:true,
						timePicker:false,
						format:'YYYY-MM-DD',
						startDate:$('#fixed_right .fixed_shipping_info input.shipping_time').val(),
						endDate:$('#fixed_right .fixed_shipping_info input.shipping_time').val()
					});
				}
			}, 'json');
		});
		
		//修改包裹信息
		frame_obj.fixed_right($('.orders_shipping .btn_track_edit'), '.fixed_track_info', function($This){
			var $Obj=$This.parent().parent(),
				$ID=$Obj.attr('data-id'),
				$Form=$('#track_info_form'),
				$OrderId=$Form.find('input[name=OrderId]').val();
			$Form.find('input[name=OvId]').val($This.attr('data-ovid'));
			$Form.find('input[name=WId]').val($This.attr('data-wid'));
			$Obj.find('.right_list .rows').each(function(){
				if($(this).index()==1){ //发货时间插件加载
					$('#fixed_right .fixed_track_info input.shipping_time').daterangepicker({
						singleDatePicker:true,
						showDropdowns:true,
						timePicker:false,
						format:'YYYY-MM-DD',
						startDate:$(this).find('span').text(),
						endDate:$(this).find('span').text()
					});
				}
				$Form.find('input, textarea').eq($(this).index()).val($(this).find('span').text());
			});
		});

		// 亚翔供应链
		frame_obj.fixed_right($('#asiafly_btn'), '.fixed_asiafly_list', function($This){
			$.post('./?do_action=orders.asiafly_get_products',{},function(result){
				if (result.ret == 0) {
					global_obj.win_alert_auto_close(result.msg, 'error', 2000, '8%');
				}else{
					var $providerHtml='<option value="-1">--请选择物流商--</option>', // 物流商
						$channelHtml='<option value="-1">--请选择渠道--</option>', // 渠道
						$productHtml='<option value="-1">--请选择物流产品--</option>'; // 物流产品
					for(i in result.msg){
						$providerHtml += '<option value="' + i + '">' + result.msg[i] + '</option>';
					}
					$('#asiaflyProvider').html($providerHtml);
					$('#asiaflyChannel').html($channelHtml);
					$('#asiaflyProduct').html($productHtml);
				}
			}, 'json');
		});

		// 改变物流方式，则
		$('.fixed_asiafly_list').on('change', '#asiaflyProvider', function(){
			var $ProviderCode = $(this).val();
			$.post('./?do_action=orders.get_channel_product',{"ProviderCode":$ProviderCode, "Type":"Provider"},function(result){
				var $channelHtml='<option value="-1">--请选择渠道--</option>', // 渠道
					$productHtml='<option value="-1">--请选择物流产品--</option>';
				for(i in result.msg){
					$channelHtml += '<option value="' + i + '">' + result.msg[i] + '</option>';
				}
				$('#asiaflyChannel').html($channelHtml);
				$('#asiaflyProduct').html($productHtml);
			}, 'json')
		}).on('change', '#asiaflyChannel', function(){
			var $ChannelCode = $(this).val();
			$.post('./?do_action=orders.get_channel_product',{"ChannelCode":$ChannelCode, "Type":"Channel"},function(result){
				var $productHtml='<option value="-1">--请选择物流产品--</option>'; // 物流产品
				for(i in result.msg.ProductName){
					$productHtml += '<option value="' + i + '" data-amount="' + result.msg.Amount[i] + '">' + result.msg.ProductName[i] + '</option>';
				}
				$('#asiaflyProduct').html($productHtml);
			}, 'json')
		}).on('change', '#asiaflyProduct', function(){
			var $ProductCode = $(this).val(),
				$ProductName = $(this).text();
				$Amount = $(this).text();
			$('#asiafly_use_product').find('input[name=ProductCode]').val($ProductCode);
			$('#asiafly_use_product').find('input[name=ProductName]').val($ProductName);
			$('#asiafly_use_product').find('input[name=Amount]').val($Amount);
		});


		// 亚翔供应链查询物流轨迹
		frame_obj.fixed_right($('#track_check'), '.fixed_asiafly_track', function($This){
			var BusinessNo = $This.attr('data-no');
			$.post('./?do_action=orders.asiafly_track_check', {"BusinessNo":BusinessNo},function(result){
				if (result.ret == 0) {
					global_obj.win_alert_auto_close(result.msg, 'error', 2000, '8%');
				}else{
					var $html='';
					for(i in result.msg){
						$html += '<li>';
						$html += 	'<div class="time">' + result.msg[i].TrackTime + '</div>';
						$html +=	'<div class="information">';
						$html +=		'<i class="radio"></i>';
						$html +=		'<div class="status">' + result.msg[i].TrackPosition + '</div>';
						$html +=		'<div class="log">' + result.msg[i].TrackContent + '</div>';
						$html +=	'</div>';
						$html +='</li>';
					}
					$('.fixed_asiafly_track').find('.log_list').html($html);
				}
			}, 'json');
		});

		// DHL物流标签打印
		frame_obj.fixed_right($('#get_label'), '.fixed_get_label');

		// DHL查询物流轨迹
		frame_obj.fixed_right($('#dhl_track_check'), '.fixed_dhl_track', function($This){
			var OrderId = $This.attr('data-no');
			global_obj.win_alert_auto_close(lang_obj.global.data_posting, 'loading', -1);
			$.post('./?do_action=orders.dhl_track_check', {"OrderId":OrderId},function(result){
				if (result.ret == 0) {
					global_obj.win_alert_auto_close(result.msg, 'error', 2000, '8%');
					$('#fixed_right .btn_cancel').click();
				}else{
					var $html='';
					for(i in result.msg){
						$html += '<li>';
						$html += 	'<div class="time">' + result.msg[i].TrackTime + '</div>';
						$html +=	'<div class="information">';
						$html +=		'<i class="radio"></i>';
						$html +=		'<div class="status">' + result.msg[i].TrackPosition + '</div>';
						$html +=		'<div class="log">' + result.msg[i].TrackContent + '</div>';
						$html +=	'</div>';
						$html +='</li>';
					}
					$('.fixed_dhl_track').find('.log_list').html($html);
				}
			}, 'json');
		});
		
		//Paypal地址
		frame_obj.switchery_checkbox(function(obj){
			var $OrderId=obj.find('input').attr('data-id');
			$.post('?', {'do_action':'orders.orders_mod_paypal_address', 'OrderId':$OrderId, 'IsUsed':1}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		}, function(obj){
			var $OrderId=obj.find('input').attr('data-id');
			$.post('?', {'do_action':'orders.orders_mod_paypal_address', 'OrderId':$OrderId, 'IsUsed':0}, function(data){
				if(data.ret==1){
					global_obj.win_alert_auto_close(data.msg, '', 1000, '8%');
				}
			}, 'json');
		});
		
		//收起 or 展开
		$('.orders_log .big_title').on('click', function(){
			var $This=$(this);
			if($This.find('i').size()){
				if($This.next().is(':hidden')){
					$This.find('i').addClass('current');
					$This.next().slideDown();
				}else{
					$This.find('i').removeClass('current');
					$This.next().slideUp();
				}
			}
		});
		
		//订单数据提交
		frame_obj.submit_form_init($('#orders_info_form'), '', function(){ //订单价格提交
			var $name='';
			$('#orders_info_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				var currency=ueeshop_config.currency;
				$('#orders_shipping_price').text(currency+parseFloat(result.msg.info.ShippingPrice).toFixed(2));
				$('#orders_shipping_insurance_price').text(currency+parseFloat(result.msg.info.ShippingInsurancePrice).toFixed(2));
				if(parseFloat(result.msg.info.DiscountPrice)>0){
					$('#orders_discount_price').text('-'+currency+parseFloat(result.msg.info.DiscountPrice).toFixed(2));
				}else{
					$('#orders_discount').text('-'+(100-parseFloat(result.msg.info.Discount))+'%');
				}
				$('#orders_user_discount').text('-'+(100-parseFloat(result.msg.info.UserDiscount))+'%');
				if(parseFloat(result.msg.info.CouponPrice)>0){
					$('#orders_coupon').text('-'+currency+parseFloat(result.msg.info.CouponPrice).toFixed(2));
				}else{
					$('#orders_coupon').text('-'+(100-parseFloat(result.msg.info.CouponDiscount)*100)+'%');
				}
				$('#orders_handing_fee').text(currency+parseFloat(result.msg.info.HandingFee).toFixed(2));
				$('#orders_total_price, #orders_info_total_price').text(currency+parseFloat(result.msg.info.TotalAmount).toFixed(2));
				$('.orders_shipping .shipping_price').text(currency+(parseFloat(result.msg.info.ShippingPrice)).toFixed(2));
				$('.orders_shipping .shipping_insurance').text(currency+(parseFloat(result.msg.info.ShippingInsurancePrice)).toFixed(2));
				if(result.msg.web){
					$('#orders_shipping_price_web').text(result.msg.web.ShippingPrice);
					$('#orders_shipping_insurance_price_web').text(result.msg.web.ShippingInsurancePrice);
					$('#orders_discount_price_web').text(result.msg.web.DiscountPrice);
					$('#orders_coupon_web').text(result.msg.web.CouponPrice);
					$('#orders_handing_fee_web').text(result.msg.web.HandingFee);
					$('#orders_total_price_web').text(result.msg.web.TotalAmount);
					$('.orders_shipping .shipping_price_web').text(result.msg.web.ShippingPrice);
					$('.orders_shipping .shipping_insurance_web').text(result.msg.web.ShippingInsurancePrice);
				}
				global_obj.win_alert_auto_close(result.msg.tips, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		frame_obj.submit_form_init($('#shipping_address_form'), '', function(){ //配送地址提交
			var $name='';
			$('#shipping_address_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				$('.orders_address .right_list .rows').each(function(){
					$(this).find('span').html(result.msg.info[$(this).find('span').attr('data-type')]);
				});
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.save_fail, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});

		frame_obj.submit_form_init($('#edit_orders_status_form'), '', function(){ //修改订单状态
			var $name='';
			$('#edit_orders_status_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				window.location.reload();
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});

		frame_obj.submit_form_init($('#shipping_info_form'), '', function(){ //配送信息提交
			var $name='';
			$('#shipping_info_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				if(result.msg.info){
					var $obj=$('.orders_shipping dt[data-id='+result.msg.info.OvId+']');
					$obj.find('.shipping_name').html(result.msg.info.Express).removeClass('error');
					$obj.find('.shipping_price').html(ueeshop_config.currency+result.msg.info.Price);
					if(result.msg.info.Insurance==1){
						$obj.find('.shipping_insurance').html(ueeshop_config.currency+result.msg.info.InsurancePrice);
					}else{
						$obj.find('.shipping_insurance').html(ueeshop_config.currency+'0.00');
					}
                    $obj.find('.right_list .rows').removeClass('hide');
					if(result.msg.info.TrackingNumber) $obj.find('.shipping_track').html(result.msg.info.TrackingNumber);
					if(result.msg.info.ShippingTime) $obj.find('.shipping_delivery_time').html(result.msg.info.ShippingTime);
					if(result.msg.info.Remarks) $obj.find('.shipping_remark').html(result.msg.info.Remarks);
					$('#orders_total_price').html(ueeshop_config.currency+result.msg.info.TotalAmount);
					$('#orders_shipping_price').html(ueeshop_config.currency+result.msg.info.TotalShippingPrice);
					$('#orders_shipping_insurance_price').html(ueeshop_config.currency+result.msg.info.TotalInsurancePrice);
					//下单所相应的货币价格
					$('#orders_total_price_web').html(result.msg.info.WebTotalAmount);
					$('#orders_shipping_price_web').html(result.msg.info.WebTotalShippingPrice);
					$('#orders_shipping_insurance_price_web').html(result.msg.info.WebTotalInsurancePrice);
				}
				global_obj.win_alert_auto_close(lang_obj.global.save_success, '', 1000, '8%');
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.save_fail, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		frame_obj.submit_form_init($('#shipped_form'), '', function(){ //配送信息提交
			var $name='';
			$('#shipped_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				window.location.reload();
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		frame_obj.submit_form_init($('#track_info_form'), '', function(){ //包裹信息提交
			var $name='';
			$('#track_info_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				window.location.reload();
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});

		// 亚翔供应链查询
		frame_obj.submit_form_init($('#asiafly_list_form'), '', function(){
			$('#asiafly_list_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==0){
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			} else {
				var $Form=$('#asiafly_choice_form'),$Html = '';
				for(k in result.msg){
					$Html+=	'<tr class="asiafly_row">';
					$Html+=		'<td>';
					$Html+=		'<div>' + result.msg[k].ProductFullName + '</div>';
					$Html+=		'<div>[ ' + result.msg[k].ProductCode + ' ]</div>';
					$Html+=		'</td>';
					$Html+=		'<td>￥' + result.msg[k].Price + '</td>';
					$Html+=		'<td>' + result.msg[k].UnitWeight + '</td>';
					$Html+=		'<td>￥' + result.msg[k].Amount + '</td>';
					$Html+=		'<td><a class="btn_global use_asiafly" data-code="' + result.msg[k].ProductCode + '" data-name="' + result.msg[k].ProductFullName + '" data-amount="' + result.msg[k].Amount + '">' + lang_obj.manage.app.asiafly.use + '</a></td>';
					$Html+=	'</tr>';
				}
				$Form.show().find('.r_con_table tbody').html($Html);
				$('#asiafly_choice_form').find('.asiafly_row').eq(0).click();
			}
			return false;
		});

		$('#asiafly_choice_form').on('click', '.use_asiafly', function(){
			var ProductCode = $(this).attr('data-code'),
				ProductName = $(this).attr('data-name'),
				Amount = $(this).attr('data-amount');
			$('#asiafly_choice_form').find('input[name=ProductCode]').val(ProductCode);
			$('#asiafly_choice_form').find('input[name=ProductName]').val(ProductName);
			$('#asiafly_choice_form').find('input[name=Amount]').val(Amount);
			$('#asiafly_choice_form').find('.btn_submit').click();
		});

		frame_obj.submit_form_init($('#asiafly_choice_form'), '', '', 0, function(result){
			if(result.ret==0){
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			} else {
				$('#asiafly_choice_form').find('.asiafly_row').eq(0).click();
				window.location='./?do_action=orders.asiafly_label_download&BusinessNo='+result.msg.BusinessNo+'&OrderId='+result.msg.OrderId;
				setTimeout(function(){
					window.location.reload();
				}, 3000);
			}
			return false;
		});

		// DHL获取标签提交
		frame_obj.submit_form_init($('#get_label_form'), '', function(){
			var $name='';
			global_obj.win_alert_auto_close(lang_obj.global.data_posting, 'loading', -1);
			$('#get_label_form *[notnull]').each(function(){
				if($.trim($(this).val())==''){
					return false;
				}
			});
			return true;
		}, 0, function(result){
			if(result.ret==1){
				window.location='./?do_action=orders.dhl_label_download&OrderId='+result.msg;
				setTimeout(function(){
					window.location.reload();
				}, 3000);
			}else{
				global_obj.win_alert_auto_close(result.msg, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
	},
	
	waybill_init:function(){
		$('#shipped_form input[name=ShippingTime]').daterangepicker({ //发货时间
			singleDatePicker:true,
			showDropdowns:true,
			timePicker:false,
			format:'YYYY-MM-DD'
		});
		
		frame_obj.fixed_right($('.btn_delivery'), '.fixed_shipped', function($This){ //发货
			var $Obj=$This.parents('.waybill_list'),
				$OvId=$Obj.attr('data-ovid'),
				$Number=$Obj.attr('data-number'),
				$OrderId=$Obj.attr('data-orderid'),
				$TrackingNumber=$This.attr('data-number'),
				$ShippingTime=$This.attr('data-time'),
				$Remarks=$This.attr('data-remarks'),
				$Form=$('#shipped_form');
			$Form.find('input[name=OrderId]').val($OrderId);
			$Form.find('input[name=Number]').val($Number);
			$Form.find('input[name=OvId]').val($OvId);
			if($TrackingNumber){
				$Form.find('input[name=TrackingNumber]').val($TrackingNumber);
				$Form.find('input[name=ShippingTime]').val($ShippingTime);
				$Form.find('textarea[name=Remarks]').val($Remarks);
			}else{
				$Form.find('input[name=TrackingNumber]').val('');
				$Form.find('input[name=ShippingTime]').val($Form.find('input[name=ShippingTime]').attr('default-value'));
				$Form.find('textarea[name=Remarks]').val('');
			}
		});
		
		frame_obj.fixed_right($('.btn_separate'), '.fixed_separate', function($This){ //拆单
			var $Obj=$This.parents('.waybill_list'),
				$OvId=$Obj.attr('data-ovid'),
				$Number=$Obj.attr('data-number'),
				$OrderId=$Obj.attr('data-orderid');
			$.post('./?do_action=orders.waybill_orders_products', {'OvId':$OvId, 'Number':$Number, 'OrderId':$OrderId}, function(data){
				if(data.ret==1){
					var $Form=$('#separate_form'),
						$Html='';
					for(k in data.msg){
						$Html+=	'<tr>';
						$Html+=		'<td>';
						$Html+=			'<dl>';
						$Html+=				'<dt><a href="javascript:;" target="_blank" class="pic_box"><img src="'+data.msg[k].PicPath+'" title="'+data.msg[k].Name+'" /><span></span></a></dt>';
						$Html+=				'<dd><h4>'+data.msg[k].Name+'</h4></dd>';
						$Html+=			'</dl>';
						$Html+=		'</td>';
						$Html+=		'<td class="last" data-num="'+data.msg[k].Qty+'"><input type="text" class="box_input" name="Waybill['+data.msg[k].LId+']" value="0" size="4" maxlength="8" data-max="'+data.msg[k].Qty+'" data-lid="'+data.msg[k].LId+'" /><span class="qty_num">/ '+data.msg[k].Qty+'</span></td>';
						$Html+=	'</tr>';
					}
					$Form.find('.r_con_table tbody').html($Html);
					$Form.find('input[name=OrderId]').val($OrderId);
					$Form.find('input[name=Number]').val($Number);
					$Form.find('input[name=OvId]').val($OvId);
				}
			}, 'json');
		});
		
		$('.btn_merge').click(function(){ //合单
			var $Obj=$(this).parents('.waybill_list'),
				$Number=$Obj.attr('data-number'),
				$OrderId=$Obj.attr('data-orderid');
			global_obj.win_alert(lang_obj.global.merge_confirm, function(){
				$.post('./?do_action=orders.waybill_separate', {'Number':$Number, 'OrderId':$OrderId}, function(data){
					if(data.ret==1){
						window.location.reload();
					}
				}, 'json');
			}, 'confirm');
		});
		
		//数据提交
		frame_obj.submit_form_init($('#separate_form'), '', function(){
			var $Input=0, $Num=0, $Error=0;
			$('#separate_form tbody tr').each(function(){
				$Input=parseInt($(this).find('.box_input').val());
				$Num=parseInt($(this).find('.last').attr('data-num'));
				isNaN($Input) && ($Error+=1);
				if($Input>$Num){ //填写的数量过大
					$(this).find('.box_input').css('border', '1px solid red');
					$Error+=1;
				}else{
					$(this).find('.box_input').removeAttr('style');
				}
			});
			if($Error>0){
				return false;
			}
			return true;
		}, 0, function(data){ //拆单
			if(data.ret==1){
				window.location.reload();
			}else{
				global_obj.win_alert_auto_close(lang_obj.manage.error.no_prod_data, 'fail', 1000, '8%');
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
		frame_obj.submit_form_init($('#shipped_form'), '', '', 0, function(data){ //发货
			if(data.ret==1){
				window.location.reload();
			}
			$('#fixed_right .btn_cancel').click();
			return false;
		});
	},
	
	orders_import_init:function(){
		var $Form=$('#import_delivery_form');
		$Form.fileupload({
			url: '/manage/?do_action=action.file_upload_plugin&size=file',
			acceptFileTypes: /^application\/(vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|csv|xlsx|xls)$/i, //csv xlsx xls
			callback: function(filepath, count){
				$('#excel_path').val(filepath);
				$('#file_path').text('文件已上传');
			}
		});
		$Form.fileupload(
			'option',
			'redirect',
			window.location.href.replace(/\/[^\/]*$/, '/cors/result.html?%s')
		);
		frame_obj.submit_form_init($Form, '', function(){
			$('.explode_box').hide();
			$('#progress_container').show();
			return true;
		}, '', function(data){
			if(data.ret==2){
				//继续产品资料上传
				var $Html='', $Number=0;
				if($('#progress_container tbody tr').size()){
					$Number=parseInt($('#progress_container tbody tr:last td:eq(0)').text());
				}
				if(data.msg[1]){
					for(k in data.msg[1]){
						$Html+='<tr class="'+(data.msg[1][k].Status==0?'error':'success')+'">\
									<td nowrap="nowrap">'+($Number+parseInt(k)+1)+'</td>\
									<td nowrap="nowrap"><div class="name" title="'+data.msg[1][k].OId+'">'+data.msg[1][k].OId+'</div></td>\
									<td nowrap="nowrap">'+data.msg[1][k].TN+'</td>\
									<td nowrap="nowrap" data-type="message">'+data.msg[1][k].Tips+'</td>\
								</tr>';
					}
					$('#progress_container tbody').append($Html);
					$Form.find('input[name=Number]').val(data.msg[0]);
					$Form.find('.btn_submit').removeAttr('disabled').click();
				}
			}else if(data.ret==1){
				$('#progress_loading').addClass('completed').html(data.msg).append('<a href="./?m=orders&a=import_delivery" class="btn_global btn_cancel">'+lang_obj.global['return']+'</a>'); //全部导入完成
			}else{
				global_obj.win_alert_auto_close(lang_obj.global.set_error, 'fail', 1000, '8%');
				$('.explode_box').show();
				$('#progress_container').hide();
			}
		});
	}
}