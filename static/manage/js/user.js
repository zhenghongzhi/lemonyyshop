/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var user_obj={
	user_global:{
		choice:function(){	//会员选择器
			var bindCheck=function(obj, type){
				var $this=obj.parent(),
					value=$this.children('input').val(),
					UserIdVal=$('#edit_form input[name=UserIdStr]').val();
				if(type==false || (!type && $this.hasClass('current'))){
					$this.removeClass('current');
					$this.children('input').attr('checked', false);
					if(global_obj.in_array(value, UserIdVal.split('|'))){
						$('#edit_form input[name=UserIdStr]').val(UserIdVal.replace('|'+value+'|', '|'));
					}
				}else{
					$this.addClass('current');
					$this.children('input').attr('checked', true);
					if(!global_obj.in_array(value, UserIdVal.split('|'))){
						$('#edit_form input[name=UserIdStr]').val(UserIdVal+value+'|');
					}
				}
			}
			var checkAll=function(){
				if($('.user_list input:checkbox').size()==$('.user_list input:checkbox:checked').size()){
					$('#all_user').attr('checked', true);
				}else{
					$('#all_user').attr('checked', false);
				}
			}
			var loadAction=function(type){
				var defaultType=parseInt($('#edit_form input[name=UserIdStr]').attr('data'));
				type==1 && $('#edit_form input[name=UserIdStr]').val()=='|' && $('#edit_form input[name=UserIdStr]').val('|');
				$('.user_list a, .user_list input:checkbox').off().on('click', function(){
					bindCheck($(this));
					checkAll();
				});
				$('.user_list a>span').off().on('click', function(){
					window.location=$(this).attr('data');
				});
				/* 多选事件 （检查触发点击当前已选择的会员等级下的会员） */
				$('.user_menu .choice_btn').off().on('click', function(){
					var inputBox=$(this).children('input'),
						Num=inputBox.val(),
						checked;
					if(inputBox.is(':checked')){
						checked=false;
						$(this).removeClass('current');
					}else{
						checked=true;
						$(this).addClass('current');
					}
					inputBox.attr('checked', checked);
					$('.user_list li[status='+Num+'] a').each(function(){
						bindCheck($(this), checked);
					});
					checkAll();
				});
				/* 单选事件 （全选） */
				$('.user_choice .all_btn').off().on('click', function(){
					var inputBox=$(this).children('input');
					if($(this).hasClass('current')){
						checked=false;
						$(this).removeClass('current');
					}else{
						checked=true;
						$(this).addClass('current');
					}
					inputBox.attr('checked', checked);
					$('.user_list li a').each(function(){
						bindCheck($(this), checked);
					});
				});
				/* 单选事件 （导出所有会员） */
				$('.user_choice .explode_all_btn').off().on('click', function(){
					var inputBox=$(this).children('input');
					if($(this).hasClass('current')){
						checked=false;
						$(this).removeClass('current');
					}else{
						checked=true;
						$(this).addClass('current');
					}
					inputBox.attr('checked', checked);
				});
				$('.clear_check').off().on('click', function(){	//清空所有勾选选项
					global_obj.win_alert(lang_obj.global.del_confirm, function(){
						$('.user_menu input.level_check, #all_user, .user_list input:checkbox').attr('checked', false);
						$('.user_list li').removeClass('current');
						$('#edit_form input[name=UserIdStr]').val('|');
					}, 'confirm');
				});
				$('.user_menu .choice_btn.current').each(function(){	//检查触发点击当前已选择的会员等级下的会员
					var Num=$(this).children('input').val();
					$('.user_list li[status='+Num+'] a').each(function(){
						bindCheck($(this), true);
					});
				});
				$('.user_list input:checkbox').each(function(){	//检查当前页所有会员是否已勾选
					var value=$(this).val(),
						UserIdVal=$('#edit_form input[name=UserIdStr]').val();
					if(global_obj.in_array(value, UserIdVal.split('|'))){
						$(this).attr('checked', true).parent().addClass('current');
					}
				});
				$('.r_con_wrap .turn_page').off().on('click', 'a', function(){
					$.ajax({
						type:'GET',
						url:$(this).attr('href'),
						async:false,
						success:function(data){
							$('#list_box').html($(data).find('#list_box').html());
							loadAction(0);
						}
					});
					return false;
				});
				checkAll();
			}
			//loadAction(1);
			var defaultType=parseInt($('#edit_form input[name=UserIdStr]').attr('data'))?0:1;
			loadAction(defaultType);
			$('#edit_form .sub_btn').off().on('click', function(){
				$.ajax({
					type:'GET',
					url:'?m=user&a=inbox&d=others_edit&Type=1&Name='+$('input[name=Name]').val(),
					async:false,
					success:function(data){
						$('#list_box').html($(data).find('#list_box').html());
						loadAction(1);
					}
				});
				return false;
			});
			/*
			$('.user_choice input:checkbox').attr('checked', false);
			*/
		}
	},
	
	user_init:function(){
		frame_obj.del_init($('#user .r_con_table'));
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button').find('.del, .edit')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'user.user_del_bat'); //批量删除
		frame_obj.del_bat($('.list_menu_button .bat_close'), $('input[name=select]'), '', function(id_list){ //批量修改
			window.location='./?m=user&a=user&d=batch_edit&userid_list='+id_list;
		}, lang_obj.global.dat_select);

		$('#user .r_con_table .status').click(function(){
			var $this=$(this),
				$href=$(this).attr('href');
				$.get($href, '', function(data){
					if(data.ret==1){
						$this.parents('tr').toggleClass('lock');
						$this.parent().find('a.status').toggle();
					}
				}, 'json');
			return false;
		});

		$('#user .r_con_table .sales_select').on('dblclick', function(){ //业务员修改
			var $obj=$(this),
				$salesid=$obj.attr('data-id'),
				$UserId=$obj.parents('tr').find('td:eq(0) input').val(),
				$mHtml=$obj.html(),
				$sHtml=$('#sales_select_hide').html(),
				$val;
			$obj.html($sHtml+'<span style="display:none;">'+$mHtml+'</span>');
			$salesid && $obj.find('select').val($salesid).focus();
			$obj.find('select').on('blur', function(){
				$val=$(this).val();
				if($val && $val!=$salesid){
					$.post('?', 'do_action=user.user_edit_sales&UserId='+$UserId+'&SalesId='+$(this).val(), function(data){
						if(data.ret==1){
							$obj.html(data.msg);
							$obj.attr('data-id', $val);
						}
					}, 'json');
				}else{
					$obj.html($obj.find('span').html());
				}
			});
		});
		frame_obj.fixed_right($('a.password_edit'), '.password_edit', function(_this){
			$('#password_edit input[name=UserId]').val(_this.data('userid'));
		});
	},
	
	user_add_init:function(){
		frame_obj.submit_form_init($('#user_edit_form'), './?m=user&a=user');
	},
	
	user_explode_init:function(){
		$('#explode_edit_form input[name=RegTime]').daterangepicker({showDropdowns:true});
		frame_obj.submit_form_init($('#explode_edit_form'), '', '', '', function(data){
			if(data.ret==2){
				$('#explode_progress').append(data.msg[1]);
				$('#explode_edit_form input[name=Number]').val(data.msg[0]);
				$('#explode_edit_form .btn_submit').click();
			}else if(data.ret==1){
				$('#explode_edit_form input[name=Number]').val(0);
				window.location='./?do_action=user.user_explode_down&Status=ok';
			}else{
				$('#explode_edit_form input[name=Number]').val(0);
				global_obj.win_alert_auto_close(data.msg, 'await', 1000, '8%');
				$('#explode_edit_form .btn_submit').removeAttr('disabled');
			}
		});
	},
	
	user_base_edit_init:function(){
		frame_obj.switchery_checkbox();
		frame_obj.submit_form_init($('#edit_form'));
		$('.address_list .more').click(function(){
			$(this).hide();
			$('.address_list .addr_box').show();
		});
        
        $('#edit_form input[name=Birthday]').daterangepicker({
            showDropdowns:true,
            singleDatePicker:true,
            timePicker:false,
            format:'MM/DD/YYYY'
        });
	},
	
	user_password_edit_init:function(){
		frame_obj.submit_form_init($('#password_edit'), '', '', '', function(data){
			if(data.ret==1){
				$('#password_edit input[type=password]').val('');
				$('#fixed_right .close').click();
			}else{
				global_obj.win_alert(data.msg, '', 'alert');
			}
		});
	},
	
	batch_edit_init:function(){
		frame_obj.switchery_checkbox();
		frame_obj.submit_form_init($('#batch_edit_form'), '','','',function(data){
			global_obj.win_alert(data.msg, function(){
				window.location.href='?m=user&a=user';
			});
		});
	},
	
	inbox_init:function(){
		user_obj.inbox_resize();
		$(window).resize(function(){
			user_obj.inbox_resize();
		});
		
		$(window).load(function(){
			user_obj.inbox_reload(0);
		});
		
		$('.inbox_container .message_list').scroll(function(){
			var $This=$(this),
				$ViewHeight=$This.outerHeight(true), //可见高度  
				$ContentHeight=$This.get(0).scrollHeight, //内容高度  
				$ScrollHeight=$This.scrollTop(), //滚动高度
				$Page=parseInt($This.attr('data-page')),
				$Total=parseInt($This.attr('data-total-pages')),
				$Count=parseInt($This.attr('data-count')),
				$IsAnimate=parseInt($This.attr('data-animate'));
			if(($ContentHeight-$ViewHeight<=$ScrollHeight) && $IsAnimate==0 && $Page<$Total){ 
				$IsAnimate=1;
				$This.attr({'data-page':$Page+1, 'data-animate':$IsAnimate});
				user_obj.inbox_reload(0);
			}
			if($Page>=$Total && $IsAnimate==0 && $ContentHeight-$ViewHeight==$ScrollHeight-1){
				global_obj.win_alert_auto_close(lang_obj.manage.sales.lastpage, 'await', 2000, '8%', 0);
			}
		});
		
		$('#search_form').on('mouseenter', '.search_btn', function(){
			$('#search_form').addClass('show');
		}).on('click', '.search_btn', function(){ //搜索按钮
			user_obj.inbox_reload(1);
			return false;
		}).on('submit', function(){
			$('#search_form .search_btn').click();
			return false;
		});
		
		$('body').on('click', '#inbox .message_list>li', function(){ //左侧栏目
			var $Obj=$('.inbox_right');
			$(this).addClass('current').siblings().removeClass('current');
			if($Obj.find('.unread_message').is(':visible')){
				$Obj.find('.unread_message').hide();
				$Obj.find('.message_title, .message_dialogue, .message_bottom').show();
			}
			$Obj.find('.message_dialogue_list').html(''); //清空对话栏
			$Obj.find('.menu_products_list').attr('data-page', 0);
			user_obj.inbox_products_list();
			return false;
		}).on('click', '.products_menu', function(){
			var $Obj=$('.inbox_right .menu_products_list');
			if($Obj.is(':hidden')){
				$('.inbox_right .menu_products_list').show();
				$('.inbox_right .menu_products_list .list').css('height', $('.inbox_container').height()-$('.inbox_right .message_title').outerHeight()-$('.inbox_right .message_bottom').outerHeight()-1); //1:边框 10:内间距
			}else{
				$('.inbox_right .menu_products_list').hide();
			}
			return false;
		}).on('click', '.menu_products_list .item', function(){ //右侧栏目
			var $This=$(this),
				$Obj=$('.inbox_right'),
				$MId=$This.attr('data-id'),
				$UserId=$This.attr('data-user-id');
			$This.addClass('current').siblings().removeClass('current');
			$Obj.find('.message_dialogue_list').html(''); //清空对话栏
			$.post('?', {'do_action':'user.inbox_view', 'MId':$MId, 'UserId':$UserId}, function(data){
				if(data.ret==1){
					var $Html='', $Count=0;
					$Obj.find('.message_title>h2').html(data.msg.Email);
					$Obj.find('.message_title .products_view .products_info img').attr('src', data.msg.PicPath);
					$Obj.find('.message_title .products_view .products_info .name>a').html(data.msg.Name).attr('title', data.msg.Name);
					if(data.msg.Url){
						$Obj.find('.message_title .products_view .products_info a').attr('href', data.msg.Url).attr('target', '_blank');
					}else{
						$Obj.find('.message_title .products_view .products_info a').attr('href', 'javascript:;').removeAttr('target');
					}
					for(k in data.msg.Reply){
						if((k==0 && data.msg.Reply[k].Type==1) || (data.msg.Reply[k].UserId==0 && !data.msg.Reply[k].CusEmail)){ //回答
							$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
							$Html+=		'<div class="time color_aaa">'+data.msg.Reply[k].Time+'</div>';
							$Html+=		'<div class="message color_000">'+data.msg.Reply[k].Content+'</div>';
							if(data.msg.Reply[k].PicPath){
							$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
							}
							$Html+=	'</div>';
							$Html+=	'<div class="clear"></div>';
						}else{ //追问
							$Html+=	'<div class="dialogue_box dialogue_box_left clean">';
							$Html+=		'<div class="time color_aaa">'+data.msg.Reply[k].Time+'</div>';
							$Html+=		'<div class="message color_000">'+data.msg.Reply[k].Content+'</div>';
							if(data.msg.Reply[k].PicPath){
							$Html+=		'<div class="picture"><a href="'+data.msg.Reply[k].PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.Reply[k].PicPath+'" /><span></span></a></div>';
							}
							$Html+=	'</div>';
							$Html+=	'<div class="clear"></div>';
						}
					}
					$Obj.find('.menu_products_list').hide();
					$Obj.find('.message_dialogue_list').append($Html);
					if($Obj.find('.message_dialogue_list .dialogue_box').size()) $Obj.find('.message_dialogue').animate({scrollTop:$Obj.find('.message_dialogue_list .dialogue_box:last').offset().top}, 10); //自动滚动到最后一个消息
					$Obj.find('input[name=MId]').val(data.msg.MId);
					$This.find('i').remove();
					$This.siblings().find('i').each(function(){
						$Count+=parseInt($(this).text());
					});
					var id_obj;
					if($UserId>0){
						id_obj='data-user-id='+$UserId;
						$('.inbox_right .message_bottom .send_box').show();
						$('.inbox_right .message_bottom .mail_box').hide();
					}else{
						$('.inbox_right .message_bottom .mail_box a').attr('href', '?m=operation&a=email&Email='+data.msg.Email);
						$('.inbox_right .message_bottom .send_box').hide();
						$('.inbox_right .message_bottom .mail_box').show();
						$This.parent().find('.item').each(function(index){
							var id=$(this).data('id');
							if($('.inbox_left .message_list li[data-id='+id+']').size()){
								id_obj='data-id='+id;
								return false;
							}
						});
					}
					$('.inbox_left .message_list li['+id_obj+']>i').text($Count); //更新左侧栏目的未读数量
					if($Count==0){ //未读数量低于1
						$Obj.find('.products_view .products_menu>i').hide();
						$('.inbox_left .message_list li['+id_obj+']>i').hide();
					}
				}
			}, 'json');
			return false;
		}).on('click', '.menu_products_list .btn_more', function(){
			//加载更多
			user_obj.inbox_products_list();
			return false;
		});
		$(document).click(function(){
			$('.inbox_right .menu_products_list').hide();
		});
        
        //直接回车发送消息
        $('#message_inbox_form textarea[name=Message]').on('keyup', function(e){
			var $Key=window.event?e.keyCode:e.which;
			if($Key==13){ //回车键
				$('#message_inbox_form .btn_submit').click();
			}
            return false;
        });
		
		//站内信消息提交回复
		frame_obj.submit_form_init($('#message_inbox_form'), '', '', 0, function(data){
			if(data.ret==1){
				var $Obj=$('.inbox_right'),
					$Form=$('#message_inbox_form'),
					$Pic=$('#MsgPicDetail .img');
					$Html='';
				$Html+=	'<div class="dialogue_box dialogue_box_right clean">';
				$Html+=		'<div class="time color_aaa">'+data.msg.Time+'</div>';
				$Html+=		'<div class="message color_000">'+data.msg.Content+'</div>';
				if(data.msg.PicPath){
				$Html+=		'<div class="picture"><a href="'+data.msg.PicPath+'" target="_blank" class="pic_box"><img src="'+data.msg.PicPath+'" /><span></span></a></div>';
				}
				$Html+=	'</div>';
				$Html+=	'<div class="clear"></div>';
				$Obj.find('.message_dialogue_list').append($Html);
				$Obj.find('.message_dialogue').animate({scrollTop:$('.inbox_right .message_dialogue_list').outerHeight()}, 500); //自动滚动到最后一个消息
				$Form.find('textarea[name=Message]').val('');
				$Pic.removeClass('isfile').removeClass('show_btn');
				$Pic.find('.preview_pic .upload_btn').show();
				$Pic.find('.preview_pic a').remove();
				$Pic.find('.preview_pic input:hidden').val('').attr('save', 0);
				global_obj.win_alert_auto_close(lang_obj.manage.email.send_success, '', 1000, '8%');
			}
			return false;
		});
	},
	
	inbox_resize:function(){
		var $iHeight=$('#inbox').outerHeight(),
			$navHeight=$('#inbox .inside_container').outerHeight();
		$('.inbox_container').css('height', $iHeight-$navHeight-40-2); //40:外间距 2:边框
		$('.inbox_left .message_list').css('height', $('.inbox_container').height()-$('.inbox_left .search').outerHeight()-1);
		$('.inbox_right .unread_message').css('height', $('.inbox_container').height());
		$('.inbox_right .message_dialogue').css('height', $('.inbox_container').height()-$('.inbox_right .message_title').outerHeight()-$('.inbox_right .message_bottom').outerHeight()-1-10); //1:边框 10:内间距
	},
	
	inbox_reload:function($Clear){
		var $Obj=$('.inbox_left .message_list'),
			$Page=parseInt($Obj.attr('data-page')),
			$Total=parseInt($Obj.attr('data-total-pages')),
			$Count=parseInt($Obj.attr('data-count')),
			$Input=$('#search_form .box_input'),
			$Search='';
		$Input.width()>0 && ($Search=$.trim($Input.val()));
		$Clear==1 && ($Page=1);
		$.post('?', {'do_action':'user.inbox_list', 'Page':$Page, 'Count':$Count, 'Keyword':$Search, 'IsClear':$Clear}, function(data){
			if(data.ret==1){
				var $Html='';
				for(k in data.msg.List){
					$Html+=	'<li data-id="'+data.msg.List[k].MId+'" data-user-id="'+data.msg.List[k].UserId+'">';
					if(data.msg.List[k].sum>0){
						$Html+=	'<i>'+data.msg.List[k].sum+'</i>';
					}
					$Html+=		'<div class="email color_555">'+data.msg.List[k].NewEmail+'</div>';
					$Html+=		'<div class="message color_aaa">'+data.msg.List[k].Content+'</div>';
					$Html+=	'</li>';
				}
				if($Clear==1){
					$('.inbox_left .message_list').html($Html);
					$Obj.scrollTop(0);
					$Obj.attr('data-page', 1);
					$Obj.attr('data-total-pages', data.msg.Total);
				}else{
					$('.inbox_left .message_list').append($Html);
				}
				$Obj.attr('data-animate', 0)
			}
		}, 'json');
	},
	
	inbox_products_list:function(){
		var $Obj=$('.inbox_right'),
			$Page=parseInt($Obj.find('.menu_products_list').attr('data-page')),
			$MId=$('.inbox_left .message_list li.current').attr('data-id'),
			$UserId=$('.inbox_left .message_list li.current').attr('data-user-id');
		$.post('?', {'do_action':'user.inbox_view_list', 'MId':$MId, 'UserId':$UserId, 'Page':$Page}, function(data){
			var $Html='';
			$Obj.find('.menu_products_list .more').remove();
			if(data.ret==1){
				$Obj.find('.message_title .products_view').show();
				if(data.msg.NoRead>0){
					$Obj.find('.message_title .products_menu>i').show();
				}else{
					$Obj.find('.message_title .products_menu>i').hide();
				}
				if(data.msg.Message){
					$Html+=	'<li class="item" data-id="'+data.msg.Message.MId+'" data-user-id="'+data.msg.Message.UserId+'">';
					if(data.msg.Message.Read>0){
					$Html+=		'<i>'+data.msg.Message.Read+'</i>';
					}
					$Html+=		'<div class="img"><img src="'+data.msg.Message.PicPath+'" /></div>';
					$Html+=		'<div class="name color_555">'+data.msg.Message.Name+'</div>';
					$Html+=	'</li>';
				}
				if(data.msg.Products){ //有产品信息
					for(k in data.msg.Products){
						$Html+=	'<li class="item" data-id="'+data.msg.Products[k].MId+'" data-user-id="'+data.msg.Products[k].UserId+'">';
						if(data.msg.Products[k].Read>0){
						$Html+=		'<i>'+data.msg.Products[k].Read+'</i>';
						}
						$Html+=		'<div class="img"><img src="'+data.msg.Products[k].PicPath+'" /></div>';
						$Html+=		'<div class="name color_555" title="'+data.msg.Products[k].Name+'">'+data.msg.Products[k].Name+'</div>';
						$Html+=	'</li>';
					}
					$Obj.find('.message_title .products_info').removeClass('products_info_msg');
					$Obj.find('.message_title .products_menu').show();
					$Obj.find('.message_title .products_view').show();
				}else{ //没有产品信息
					$Obj.find('.message_title .products_info').addClass('products_info_msg');
					$Obj.find('.message_title .products_menu').hide();
					$Obj.find('.message_title .products_view').hide();
				}
				if(data.msg.Over>0){
					$Html+=	'<li class="more"><a href="javascript:;" class="btn_global btn_more">'+lang_obj.manage.global.more+'</a></li>';
				}
				if($Page==0){ //第一次加载
					$Obj.find('.menu_products_list .list').html($Html);
					$Obj.find('.menu_products_list .list .item:eq(0)').click(); //默认点击第一个选项
				}else{
					$Obj.find('.menu_products_list .list').append($Html);
				}
				$Obj.find('.menu_products_list').attr('data-page', $Page+1);
			}
		}, 'json');
	},

	message_init:function(){
		frame_obj.del_init($('#message .r_con_table')); //删除提示
		frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
		frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'user.message_del_bat'); //批量删除
	},

	message_edit_init:function(){
		frame_obj.submit_form_init($('#message_edit_form'), './?m=user&a=message');
	},

    usermessage_init:function(){
        frame_obj.del_init($('#message .r_con_table')); //删除提示
        frame_obj.select_all($('input[name=select_all]'), $('input[name=select]'), $('.list_menu_button .del')); //批量操作
        frame_obj.del_bat($('.list_menu_button .del'), $('input[name=select]'), 'user.usermessage_del_bat'); //批量删除
    },
    usermessage_edit_init:function(){
        frame_obj.submit_form_init($('#message_edit_form'), './?m=user&a=usermessage');
    },
}