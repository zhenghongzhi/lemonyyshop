/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

if(!document.getElementByClassName){
    function hasClass(elem, cls){
      cls=cls || '';
      if(cls.replace(/\s/g, '').length == 0) return false; //当cls没有参数时，返回false
      var ret=new RegExp(' '+cls+' ').test(' '+elem.className+' ');
      return ret;
    }
    document.getElementByClassName=function(className,index){
        var nodes=document.getElementsByTagName("*");//获取页面里所有元素，因为他会匹配全页面元素，所以性能上有缺陷，但是可以约束他的搜索范围；
        var arr=[];//用来保存符合的className；
        for(var i=0; i<nodes.length; i++){
            if(hasClass(nodes[i],className)) arr.push(nodes[i]);
        }
        if(!index)index=0;
        return index==-1?arr:arr[index];
    };
    function addClass(elements, cName){
       if(!hasClass(elements, cName)){
          elements.className+=" "+cName;
       };
    }
    function removeClass(elements, cName){
       if(hasClass(elements, cName)){
          elements.className=elements.className.replace(new RegExp("(\\s|^)"+cName+"(\\s|$)"), " "); // replace方法是替换
       };
    }
}

function appendHTML(o, html){
    var divTemp=document.createElement("div"),
		nodes=null,
		fragment=document.createDocumentFragment();
    divTemp.innerHTML=html;
    nodes=divTemp.childNodes;
    for(var i=0, length=nodes.length; i<length; i+=1){
       fragment.appendChild(nodes[i].cloneNode(true));
    }
    o.appendChild(fragment);
    nodes=null;
    fragment=null;
};



var _ajax=function(){};
_ajax.prototype={
    request: function(method, url, callback, postVars){
        var xhr=this.createXhrObject()();
        xhr.onreadystatechange=function(){
            if(xhr.readyState!==4) return;
            (xhr.status===200) ? callback.success(xhr.responseText, xhr.responseXML) : callback.failure(xhr,status);
        };
        if(method!=="POST" && postVars){
            url+="?"+this.JSONStringify(postVars);
            postVars=null;
        }
        xhr.open(method, url, true);
        xhr.send(postVars);
    },
    createXhrObject: function(){
        var methods=[
            function(){ return new XMLHttpRequest(); },
            function(){ return new ActiveXObject("Msxml2.XMLHTTP"); },
            function(){ return new ActiveXObject("Microsoft.XMLHTTP"); }
        ],
        i=0,
        len=methods.length,obj;
        for(; i<len; i++){
            try{
                methods[i];
            }catch(e){
                continue;
            }
            this.createXhrObject=methods[i];
            return methods[i];
        }
        throw new Error("ajax created failure");
    },
    JSONStringify: function(obj){
        return JSON.stringify(obj).replace(/"|{|}/g, "").replace(/b:b/g, "=").replace(/b,b/g, "&");
    }
};


var ver_code={
    _obj:null,
    _ver_code:null,
    _img:null,
    _img_loaded:false,
    _is_draw_bg:false,
    _is_moving:false,
    _block_start_x:0,
    _block_start_y:0,
    _doing:false,
    _mark_w:50,
    _mark_h:50,
    _mark_offset:0,
    _img_w:240,
    _img_h:150,
    _result:false,
    _err_c:0,
    _onsuccess:null,
    _bind:function(elm, evType, fn){
        //event.preventDefault();
        if(elm.addEventListener){
            elm.addEventListener(evType, fn);//DOM2.0
            return true;
        }else if(elm.attachEvent){
            var r=elm.attachEvent(evType, fn);//IE5+
            return r;
        }
    },
    _block_start_move:function(e){
        if(ver_code._doing || !ver_code._img_loaded){
            return;
        }
        e.preventDefault();
        var theEvent=window.event || e;
        if(theEvent.touches){
            theEvent=theEvent.touches[0];
        }
        var obj=document.getElementByClassName('slide_block_text');
        obj.style.display="none";
        ver_code._draw_bg();
        ver_code._block_start_x=theEvent.clientX;
        ver_code._block_start_y=theEvent.clientY;
        ver_code._doing=true;
        ver_code._is_moving=true;
		$('.slide_block').addClass('move');
    },
    _block_on_move:function(e){
        if(!ver_code._doing){
			return true;
		}
        if(!ver_code._is_moving){
			return true;
		}
        e.preventDefault();
        var theEvent=window.event || e;
        if(theEvent.touches){
            theEvent=theEvent.touches[0];
        }
        ver_code._is_moving=true;
        var offset=theEvent.clientX - ver_code._block_start_x;
        if(offset<0){
            offset=0;
        }
        var max_off=ver_code._img_w - ver_code._mark_w;
        if(offset>max_off){
            offset=max_off;
        }
        var obj=document.getElementByClassName('slide_block');

        obj.style.cssText="transform: translate("+offset+"px, 0px)";
        ver_code._mark_offset=offset/max_off*(ver_code._img_w-ver_code._mark_w);
        ver_code._draw_bg();
        ver_code._draw_mark();
    },
    _block_on_end:function(e){
        if(!ver_code._doing){
			return true;
		}
        e.preventDefault();
        var theEvent=window.event || e;
        if(theEvent.touches){
            theEvent=theEvent.touches[0];
        }
        ver_code._is_moving=false;
        ver_code._send_result();
		$('.slide_block').removeClass('move');
    },
    _send_result:function(){
        var haddle={success:ver_code._send_result_success, failure:ver_code._send_result_failure};
        ver_code._result=false;
		
		var sessionID=global_obj.getCookie('session_id');
		var sessID=global_obj.getCookie('PHPSESSID');
		var timestamp=Date.parse(new Date());
		var code=global_obj.base64_encode(sessID+'_'+timestamp);
		
        var re=new _ajax();
        re.request('get', ver_code._currentUrl()+'check.php?vr='+ver_code._mark_offset+'&gt='+sessionID+'&challenge='+code, haddle);
    },
    _send_result_success:function(responseText, responseXML){
        ver_code._doing=false;
        if(responseText=='ok'){
            ver_code._ver_code.innerHTML='Success';
			$(ver_code._ver_code).addClass('ver_code_success');
            ver_code._showmsg('Success', 1);
            ver_code._result=true;
            document.getElementByClassName('hgroup').style.display="block";
            setTimeout(ver_code.hide, 3000);
			$('.slide_block').attr('class', 'slide_block success');
            if(ver_code._onsuccess){
                ver_code._onsuccess();
            }
        }else{
            var obj=document.getElementById('ver_code_div');
            addClass(obj, 'dd');
            setTimeout(function(){
                removeClass(obj, 'dd');
            }, 200);
            ver_code._result=false;
            ver_code._showmsg('Fail');
            ver_code._err_c++;
			$('.slide_block').attr('class', 'slide_block error');
            if(ver_code._err_c>5){
                ver_code.refresh();
            }
        }
    },
    _send_result_failure:function(xhr, status){

    },
    _draw_fullbg:function(){
        var canvas_bg=document.getElementByClassName('ver_code_canvas_bg');
        var ctx_bg=canvas_bg.getContext('2d');
        ctx_bg.drawImage(ver_code._img, 0, ver_code._img_h*2, ver_code._img_w, ver_code._img_h, 0, 0, ver_code._img_w, ver_code._img_h);
    },
    _draw_bg:function(){
        if(ver_code._is_draw_bg){
            return;
        }
        ver_code._is_draw_bg=true;
        var canvas_bg=document.getElementByClassName('ver_code_canvas_bg');
        var ctx_bg=canvas_bg.getContext('2d');
        ctx_bg.drawImage(ver_code._img, 0, 0, ver_code._img_w, ver_code._img_h, 0, 0, ver_code._img_w, ver_code._img_h);
    },
    _draw_mark:function(){
        var canvas_mark=document.getElementByClassName('ver_code_canvas_mark');
        var ctx_mark=canvas_mark.getContext('2d');
        //清理画布
        ctx_mark.clearRect(0, 0, canvas_mark.width,canvas_mark.height);
        ctx_mark.drawImage(ver_code._img, 0, ver_code._img_h, ver_code._mark_w, ver_code._img_h, ver_code._mark_offset, 0, ver_code._mark_w, ver_code._img_h);
        var imageData=ctx_mark.getImageData(0, 0, ver_code._img_w, ver_code._img_h);
		// 获取画布的像素信息
		// 是一个一维数组，包含以 RGBA 顺序的数据，数据使用  0 至 255（包含）的整数表示
		// 如：图片由两个像素构成，一个像素是白色，一个像素是黑色，那么 data 为
		// [255,255,255,255,0,0,0,255]
		// 这个一维数组可以看成是两个像素中RBGA通道的数组的集合即:
		// [R,G,B,A].concat([R,G,B,A])
        var data=imageData.data;
        var x=ver_code._img_h,y=ver_code._img_w;
        for(var j=0; j<x; j++){
            var ii=1, k1=-1;
            for(var k=0; k<y && k>=0 && k>k1;){
              // 得到 RGBA 通道的值
                var i=(j*y+k)*4;
                k+=ii;
                var r=data[i],
					g=data[i+1],
					b=data[i+2];
                // 我们从最下面那张颜色生成器中可以看到在图片的右上角区域，有一小块在
                // 肉眼的观察下基本都是白色的，所以我在这里把 RGB 值都在 245 以上的
                // 的定义为白色
                // 大家也可以自己定义的更精确，或者更宽泛一些
                if(r+g+b<200) data[i+3]=0;
                else{
                    var arr_pix=[1,-5];
                    var arr_op=[250,0];
                    for(var i=1; i<arr_pix[0]-arr_pix[1]; i++){
                        var iiii=arr_pix[0]-1*i;
                        var op=parseInt(arr_op[0]-(arr_op[0]-arr_op[1])/(arr_pix[0]-arr_pix[1])*i);
                        var iii=(j*y+k+iiii*ii)*4;
                        data[iii+3]=op;
                    }
                    if(ii==-1){
                        break;
                    }
                    k1=k;
                    k=y-1;
                    ii=-1;
                };
            }
        }
        ctx_mark.putImageData(imageData, 0, 0);
    },
    _reset:function(){
        ver_code._mark_offset=0;
        ver_code._draw_bg();
        ver_code._draw_mark();
        var obj=document.getElementByClassName('slide_block');
        obj.style.cssText="transform: translate(0px, 0px)";
		$('.slide_block').attr('class', 'slide_block');
    },
    show:function(){
		$(ver_code._ver_code).html(lang_obj.verification.ver_click).removeClass('ver_code_success');
        var obj=document.getElementByClassName('hgroup');
        if(obj){
            obj.style.display="none";
        }
        ver_code.refresh();
        ver_code._ver_code=this;
        document.getElementById('ver_code_div').style.display="block";
    },
    hide:function(){
        document.getElementById('ver_code_div').style.display="none";
		$('.slide_block').attr('class', 'slide_block');
		global_obj.div_mask(1); //遮罩层消失
		return false;
    },
    _showmsg:function(msg, status){
        if(!status){
            status=0;
            var obj=document.getElementByClassName('ver_code_msg_error');
        }else{
            var obj=document.getElementByClassName('ver_code_msg_ok');
        }
        obj.innerHTML=msg;
        var setOpacity=function(ele, opacity){
            if(ele.style.opacity != undefined){
                ///兼容FF和GG和新版本IE
                ele.style.opacity=opacity / 100;
            }else{
                ///兼容老版本ie
                ele.style.filter="alpha(opacity="+opacity+")";
            }
        };
        function fadeout(ele, opacity, speed){
            if(ele){
                var v=ele.style.filter.replace("alpha(opacity=", "").replace(")", "") || ele.style.opacity || 100;
                v < 1 && (v=v * 100);
                var count=speed / 1000;
                var avg=(100 - opacity) / count;
                var timer=null;
                timer=setInterval(function(){
                    if(v-avg>opacity){
                        v-=avg;
                        setOpacity(ele, v);
                    }else{
                        setOpacity(ele, 0);
                        if(status==0){
                            ver_code._reset();
                        }
                        clearInterval(timer);
                    }
                }, 100);
            }
        }
        function fadein(ele, opacity, speed){
            if(ele){
                var v=ele.style.filter.replace("alpha(opacity=", "").replace(")", "") || ele.style.opacity;
                v<1 && (v=v*100);
                var count=speed/1000;
                var avg=count<2 ? (opacity/count) : (opacity/count-1);
                var timer=null;
                timer=setInterval(function(){
                    if(v<opacity){
                        v+=avg;
                        setOpacity(ele, v);
                    }else{
                        clearInterval(timer);
                        setTimeout(function(){fadeout(obj, 0, 6000);},1000);
                    }
                }, 100);
            }
        }

        fadein(obj, 80, 4000);
    },
    _html:function(){
        var html='<div class="ver_code_div" id="ver_code_div"><div class="loading"></div><canvas class="ver_code_canvas_bg"></canvas><canvas class="ver_code_canvas_mark"></canvas><div class="hgroup"></div><div class="ver_code_msg_error"></div><div class="ver_code_msg_ok"></div><div class="slide"><div class="slide_block"></div><div class="slide_block_text">'+lang_obj.verification.ver_slide+'</div></div><div class="tools"><div class="ver_code_close"></div><div class="ver_code_refresh"></div><div class="ver_code_tips"></div></div></div>';
        var bo=document.getElementsByTagName('body');
        appendHTML(bo[0],html);
    },
    _currentUrl:function(){
        var list=document.getElementsByTagName('script');
        for(var i in list){
            var d=list[i];
            if(d.src.indexOf('verification_code')!==-1){ //js文件名一定要带这个字符
				if(d.src.indexOf('cdn')!==-1){ //带有CDN
					var _arr=d.src.split('verification_code');
					d.src=ueeshop_config.domain+'/static/js/plugin/verification/verification_code'+_arr[1];
				}
                var arr=d.src.split('verification_code');
                return arr[0];
            }
        }
    },
    refresh:function(){
        var isSupportWebp=!![].map && document.createElement('canvas').toDataURL('image/webp').indexOf('data:image/webp')==0;
        var _this=this;
        ver_code._err_c=0;
        ver_code._is_draw_bg=false;
        ver_code._result=false;
        ver_code._img_loaded=false;
		
		global_obj.div_mask(); //遮罩层开启
        var obj=document.getElementByClassName('ver_code_canvas_bg');
        obj.style.display="none";
        obj=document.getElementByClassName('ver_code_canvas_mark');
        obj.style.display="none";
		
		var sessionID=global_obj.getCookie('session_id');
		var sessID=global_obj.getCookie('PHPSESSID');
		var timestamp=Date.parse(new Date());
		var code=global_obj.base64_encode(sessID+'_'+timestamp);
		
        ver_code._img=new Image();
        var img_url=ver_code._currentUrl()+"verification.php?gt="+sessionID+"&challenge="+code;
        if(!isSupportWebp){//浏览器不支持webp
            img_url+="&nowebp=1";
        }
        ver_code._img.src=img_url;
        ver_code._img.onload=function(){
            ver_code._draw_fullbg();
            var canvas_mark=document.getElementByClassName('ver_code_canvas_mark');
            var ctx_mark=canvas_mark.getContext('2d');
            ctx_mark.clearRect(0, 0, canvas_mark.width, canvas_mark.height); //清理画布
            ver_code._img_loaded=true;
            obj=document.getElementByClassName('ver_code_canvas_bg');
            obj.style.display="";
            obj=document.getElementByClassName('ver_code_canvas_mark');
            obj.style.display="";
        };
        //alert("Hong Kong ForHarvest Technology and Culture Development Co. Limited".length);
        obj=document.getElementByClassName('slide_block');
        obj.style.cssText="transform: translate(0px, 0px)";
        obj=document.getElementByClassName('slide_block_text');
        obj.style.display="block";
    },
    init:function(){
        var _this=this;
        if(!ver_code._img){
            ver_code._html();
            var obj=document.getElementByClassName('slide_block');
			var close_obj=document.getElementByClassName('ver_code_close');
			var refresh_obj=document.getElementByClassName('ver_code_refresh');
			if(ueeshop_config.IsMobile==1){ //移动端
				ver_code._bind(obj, 'touchstart', _this._block_start_move);
				ver_code._bind(document, 'touchmove', _this._block_on_move);
				ver_code._bind(document, 'touchend', _this._block_on_end);
				ver_code._bind(close_obj, 'touchstart', _this.hide);
				ver_code._bind(refresh_obj, 'touchstart', _this.refresh);
			}else{ //PC端
				ver_code._bind(obj, 'mousedown', _this._block_start_move);
				ver_code._bind(document, 'mousemove', _this._block_on_move);
				ver_code._bind(document, 'mouseup', _this._block_on_end);
				ver_code._bind(close_obj, 'click', _this.hide);
				ver_code._bind(refresh_obj, 'click', _this.refresh);
			}

			var objs=document.getElementByClassName('ver_code', -1);
			for(var i in objs){
				var o=objs[i];
				o.innerHTML=lang_obj.verification.ver_click;
				o.classList.add('completed');
				if(ueeshop_config.IsMobile==1){ //移动端
					ver_code._bind(o, 'touchstart', _this.show);
				}else{ //PC端
					ver_code._bind(o, 'click', _this.show);
				}
			}
		}
	},
	result:function(){
		return ver_code._result;
	},
	onsuccess:function(fn){
		ver_code._onsuccess=fn;
	}
};
var $VER=ver_code;
var _old_onload=window.onload;
$(window).load(function(){
	$.post('/?do_action=action.clear_verification&t='+Math.random());
	if(typeof _old_onload=='function'){
		_old_onload();
	}
	ver_code.init();	
});