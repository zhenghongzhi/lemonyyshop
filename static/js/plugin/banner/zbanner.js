function zbanner_init(){
	var obj=$('#banner_edit');
	var type=obj.data('type');  //Banner插件类型,类型种类看ly200类
	var slidetype=obj.data('slidetype');  //滚动类型
	var mask_width=obj.data('width');  //建议图片尺寸宽度
	var mask_height=obj.data('height');  //建议图片尺寸高度
	var banner_tab=0;
	var banner_auto=0;
	var banner_size=obj.find('.banner_list').size();
	var init=function(){
		obj.children('.banner_loading').show();
		if(slidetype==2){
			obj.find('.banner_box').width(banner_size*100+'%').find('.banner_list').css('float', 'left');
		}else if(slidetype==1){
			obj.find('.banner_box').height(banner_size*100+'%').find('.banner_list').height((100/banner_size).toFixed(2)+'%');
		}else{
			obj.find('.banner_list').css({'position':'absolute', 'top':0, 'left':0}).hide().eq(banner_tab).show();
		}
		if(banner_size<=1) obj.find('.banner_tab, .banner_prev, .banner_next').hide();
		if(type==2){
			//obj.height(mask_height);
			obj.height('100%').parent().height('100%');
		}else if(type==1){
			obj.height(parseInt(mask_height*obj.width()/mask_width));
		}
		obj.find('.banner_list').each(function(index){
			var _this=$(this);
			var image = new Image();
			image_url=_this.find('img').attr('src');
			image.src = image_url;
			image.onload = function(){
				var height=image.height;  //图片高度
				_this.width(obj.width());  //前台图片框宽度
				var rate=obj.width()/mask_width;
				var param=_this.data('data');
				param=JSON.stringify(param);
				param=JSON.parse(param);
				if(rate<1 && type!=2){  //等比例缩放
					if(param['title']['font-size']) param['title']['font-size']=parseInt(param['title']['font-size'])*rate+'px';
					if(param['brief']['font-size']) param['brief']['font-size']=parseInt(param['brief']['font-size'])*rate+'px';
				}
				var left=param['banner']['left'];
				var top=param['banner']['top'];
				_this.find('.banner_position').css({'left':left, 'top':top});
				if(type==2){  //计算左定位偏移差
					_this.find('.banner_position').css({width:mask_width, height:image.height, 'left':'50%', 'margin-left':-mask_width/2});
				}else if(type==1){
					_this.find('.banner_position').css({'width':image.width>obj.width()?obj.width():image.width*rate, 'height':image.width>obj.width()?obj.width()*image.height/image.width:image.height*rate});
				}else{
					height=parseInt(obj.width()*image.height/image.width);
					if(image.width<obj.width()) height=image.height;  //图片小于容器宽度
					height=height+1;  //兼容像素小数点导致左右有白边
					obj.height(height).parent().parent().height(height);
					_this.find('.banner_position').css({width:obj.width(), height:height});
				}
				_this.find('.banner_title').css(param['title']);
				_this.find('.banner_brief').css(param['brief']);
				if(banner_size-index==1) obj.children('.banner_loading').fadeOut();
				if(_this.find('a').size()) _this.find('div').css('cursor', 'pointer'); //有链接,子元素增加a效果
			}
		});
	}
	var auto_play=function(){
		if(banner_size<=1){  //只有一张图或者后台时,不执行切换
			clearInterval(banner_auto);
			return;	
		}
		obj.find('.banner_next').trigger('click');
	}
	var turn_banner=function(turn){
		if(slidetype==2){
			obj.find('.banner_box').animate({'left':'-'+banner_tab*100+'%'});
		}else if(slidetype==1){
			obj.find('.banner_box').animate({'top':'-'+banner_tab*100+'%'});
		}else{
			obj.find('.banner_list').stop(true, true).eq(banner_tab).fadeIn().siblings().fadeOut();
		}
		obj.find('.banner_tab a').stop(true, true).eq(banner_tab).addClass('on').siblings().removeClass('on');
	}
	init();
	banner_auto=setInterval(auto_play, 6000);
	$(window).resize(function(){init();});
	obj.find('.banner_tab').off().on('mouseenter', 'a', function(){
		banner_tab=$(this).index();
		turn_banner();
	});
	obj.off('mouseenter').on('mouseenter', function(){
		clearInterval(banner_auto);
	});
	obj.off('mouseleave').on('mouseleave', function(){
		banner_auto=setInterval(auto_play, 6000);
	});
	obj.find('.banner_prev').off().on('click', function(){
		banner_tab = banner_tab > 0 ? --banner_tab : banner_size-1;
		turn_banner();
	});
	obj.find('.banner_next').off().on('click', function(){
		banner_tab = banner_tab < banner_size-1 ? ++banner_tab : 0;
		turn_banner();	
	});
}