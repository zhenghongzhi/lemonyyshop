/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

var facebook_store_obj={
	fae_data:{},
	products_upload:function(ProId){ //上传产品
		facebook_store_obj.products_process(ProId, 0);
	},
	products_delete:function(ProId){ //删除产品		
		facebook_store_obj.products_process(ProId, 1);
	},
	products_process:function(ProId, isDelete){
		//处理进度
		var $Data=ProId.split('-'),
			$Obj;
		for(k in $Data){
			$Obj=$('#products .r_con_table tr[data-id="'+$Data[k]+'"]');
			$Obj.attr('data-status', 0);
			$Obj.find('.info .clean').append('<div class="loading fl"></div><a href="javascript:;" class="btn_facebook_process hide">同步</a>');
		}
		$('#products .list_menu_button>li>a.facebook').parent().addClass('loading');
		$('.facebook_process .btn_close').off().on('click', function(){
			$('.facebook_process').slideUp(250, function(){
				$('.facebook_process').remove();
			});
		});
		$('.btn_facebook_process').off().on('click', function(){
			var $Obj=$(this).parents('tr'),
				$ProId=$Obj.attr('data-id');
			if($Obj.attr('data-status')>0) return false;
			$.post('?', {'do_action':'plugins.facebook_products_process', 'ProId':$ProId, 'Type':(isDelete==1?'Delete':'Upload')}, function(result){
				if(result.ret==1){
					if(result.msg.Type==1){ //创建
						$Obj.find('.info .clean').append('<div class="other_box fl" data-name="facebook" style="opacity:0;">'+lang_obj.manage.app.facebook_ads_extension.store+'</div>');
						$Obj.find('.info .clean .other_box[data-name="facebook"]').animate({'opacity':1}, 1000);
					}else if(result.msg.Type==2){ //更新
						//留空
					}else{ //删除
						$Obj.find('.info .clean .other_box[data-name="facebook"]').animate({'opacity':0}, 1000, function(){
							$(this).remove();
						});
					}
					$Obj.attr('data-status', 1);
					$Obj.find('.loading, .btn_facebook_process').remove();
					facebook_store_obj.products_upload_complete();
				}else{
					global_obj.win_alert(JSON.stringify(result.msg));
				}
			}, 'json');
		});
		$('#products .r_con_table tr[data-status=0]:eq(0) .btn_facebook_process').click(); //默认执行一次
	},
	products_upload_complete:function(){
		if($('#products .r_con_table tr[data-status=0]:eq(0)').length){
			$('#products .r_con_table tr[data-status=0]:eq(0) .btn_facebook_process').click();
		}else{
			global_obj.win_alert_auto_close('处理完成！', '', 2000);
			$('#products .list_menu_button>li>a.facebook').parent().removeClass('loading');
			$('#products .r_con_table tr').removeAttr('data-status');
		}
	}
}