/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
	// The response object is returned with a status field that lets the
	// app know the current login status of the person.
	// Full docs on the response object can be found in the documentation
	// for FB.getLoginStatus().
	if (response.status === 'connected') {
		// Logged into your app and Facebook.
		LoginWithFacebook();
	} else {
		console.log('Please log into this app.');
		// The person is not logged into your app or we are unable to tell.
		// alert('Please log into this app.');
	}
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
	FB.getLoginStatus(function(response) {
	  statusChangeCallback(response);
	});
}

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function LoginWithFacebook(){
	//console.log('Welcome!  Fetching your information.... ');
	FB.api('/me', {fields: 'id,email,first_name,last_name,gender,age_range,picture'}, function(response){
		//console.log('Successful login for: ' + response.name);
		var url='&id='+response['id']+'&email='+response['email']+'&first_name='+response['first_name']+'&last_name='+response['last_name']+'&gender='+response['gender'];
		
		$.get('/?do_action=user.user_oauth&Type=Facebook', url, function(data){
			global_obj.div_mask(1);
			global_obj.data_posting(false);
			if(data.ret==1){
				//window.location=data.msg[0];
				if(typeof data.msg==='string'){
					window.location=data.msg;
				}else{
					window.location=data.msg[0];
				}
			}else{
				$.get(data.msg[0]+'?module=1', '', function(data){
					$('body').prepend(data);
					$('body').find('#binding_module').css({left:$(window).width()/2-220});
					global_obj.div_mask();
					user_obj.user_login_binding();
				});
			}
		}, 'json');
	});
}