/*
Powered by ueeshop.com		http://www.ueeshop.com
广州联雅网络科技有限公司		020-83226791
*/
$(function(){
	var $instagram = $('#instagram_button'),
		$client_id = $instagram.attr('client_id'),
		$client_secret = $instagram.attr('client_secret'),
		$redirect_uri = $instagram.attr('redirect_uri'),
		$login = $instagram.attr('login'),
		$code = $instagram.attr('code');
	$instagram.click(function(){
		location.href='https://api.instagram.com/oauth/authorize/?client_id='+$client_id+'&redirect_uri='+$redirect_uri+'&response_type=code';
	});

	if($login && $code){
		global_obj.div_mask();
		global_obj.data_posting(true, 'Login with Instagram');
		$.post('https://api.instagram.com/oauth/access_token',{'client_id':$client_id,'client_secret':$client_secret,'grant_type':'authorization_code','redirect_uri':$redirect_uri,'code':$code},function(data){
			global_obj.div_mask(1);
			global_obj.data_posting(false);
			if(data.user.id){
				$.get('/?do_action=user.user_oauth',{'Type':'Instagram','id':data.user.id,'full_name':data.user.full_name},function(data2){
					if(data2.ret==1){
						location.href=data2.msg;
					}else{
						$.get(data2.msg[0]+'?module=1', '', function(data3){
							$('body').prepend(data3);
							$('body').find('#binding_module').css({left:$(window).width()/2-220});
							global_obj.div_mask();
							user_obj.user_login_binding();
						});
					}
				},'json');
			}else{
				location.href='/';
			}
		},'json');
		setTimeout(function(){
			global_obj.div_mask(1);
			global_obj.data_posting(false);
			location='/';
		},30000);
	}
});