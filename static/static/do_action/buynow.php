<?php
/*
 * Powered by ueeshop.com   http://www.ueeshop.com
 * 广州联雅网络科技有限公司   020-83226791
 * Note: 单独的购物流程
 */

class buynow_module{

	public static function check_low_consumption($BackType=0, $Price=0){ //1:返回数组, 0:返回JSON格式
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$w=$c['where']['cart'];
		if($p_CId){
			$p_CId=str::ary_format(@str_replace('.', ',', $p_CId), 2);
			$w.=" and CId in({$p_CId})";
		}
		if($_GET['t'] && $p_CId=='0'){//来自产品详细页的传入，不用检查
			// ly200::e_json(array(), 1);
			if($p_Attr){//产品属性
				$Attr=str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
				ksort($Attr);
			}
			$proInfo=db::get_one('products', "ProId='$p_ProId'");
			$StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;	//起订量
			$p_Qty<$StartFrom && $p_Qty=$StartFrom;	//小于起订量
			$CurPrice=cart::products_add_to_cart_price($proInfo, $p_Qty);
			$PropertyPrice=0;
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'ProId'			=> $p_ProId,
				'Price'			=> $CurPrice,
				'Attr'			=> $Attr,
				'IsCombination'	=> $proInfo['IsCombination'],
				'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice']
			));
			if($AttrData){
				$CurPrice		= $AttrData['Price'];		//产品单价
				$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			}
			//产品数据
			if($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime']<$c['time'] && $c['time']<$proInfo['EndTime']){
				$CurPrice=$CurPrice*($proInfo['PromotionDiscount']/100);
			}
			$total_price=cart::iconv_price(($CurPrice+$PropertyPrice),2)*$p_Qty;
		}
		if($Price>0){//有传递产品总价格数据
			$ProductPrice=$Price;
		}else{
			$cartInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', $w, "p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr", 's.CId desc');
			$ProductPrice=0;
			foreach($cartInfo as $val){
				$ProductPrice+=($val['Price']+$val['PropertyPrice'])*$val['Qty']*($val['Discount']<100?$val['Discount']/100:1);
			}
		}
		$DisData=cart::discount_contrast($ProductPrice);//“会员优惠”和“全场满减”之间的优惠对比
		$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
		$Discount		= $DisData['Discount'];			//全场满减的折扣
		$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
		//最低消费设置
		$difference=0;
		$ret=1;
		$result=array();
		$low_price=cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0);
		$_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)-$DiscountPrice;//订单折扣后的总价
		if($_GET['t'] && $p_CId=='0') $_total_price = $total_price;
		if((int)$c['config']['global']['LowConsumption'] && cart::iconv_price($_total_price, 2, '', 0)<$low_price){
			$ret=0;
			$difference=$low_price-cart::iconv_price($_total_price, 2, '', 0);
			$result=array('s_total'=>cart::iconv_price($_total_price, 2, '', 0), 'low_price'=>$low_price, 'difference'=>$difference);
		}
		if($BackType==1){
			return $result;
		}else{
			ly200::e_json($result, $ret);
		}
	}

	/********************************** 单独购物流程 start **********************************/
	public static function addtocart(){	//add product to shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_back=(int)$p_back;
		$p_IsBuyNow=(int)$p_IsBuyNow;//是否为立即购买
		$products_type=(int)$p_products_type;//产品类型，0：普通产品，1：团购，2：秒杀，3:组合购买，4:组合促销
		if($p_id){	//注册后自动返回立即添加购物车的属性数据
			$id_ary=array();
			foreach((array)$p_id as $k=>$v){
				$key=($k=='Overseas'?$k:(int)$k);
				$value=($k=='Overseas'?('Ov:'.(int)str_replace('Ov:','',$v)):(int)$v);
				$id_ary[$key]=$value;
			}
			$p_Attr=str::json_data($id_ary);
		}
		$IsStock=(int)$c['config']['products_show']['Config']['stock'];
		//初始化
		if(!@is_array($p_ProId)){
			$_KeyId=array($p_ProId);
			$_Qty=array($p_Qty);
			$_Attr=array($p_Attr);
		}else{
			$_KeyId=$p_ProId;
			$_Qty=$p_Qty;
			$_Attr=$p_Attr;
		}
		for($m=0; $m<count($_KeyId); $m++){
			//初始化
			$Data=cart::product_type(array(
				'Type'	=> $products_type,
				'KeyId'	=> $_KeyId[$m],
				'Qty'	=> $_Qty[$m],
				'Attr'	=> $_Attr[$m]
			));
			if($Data===false) continue;
			$BuyType	= $Data['BuyType'];	//产品类型
			$KeyId		= $Data['KeyId'];	//主ID
			$ProId		= $Data['ProId'];	//产品ID
			$StartFrom	= $Data['StartFrom'];//产品目前的起订量
			$Price		= $Data['Price'];	//产品目前的单价
			$Qty		= $Data['Qty'];		//购买的数量
			$Discount	= $Data['Discount'];//折扣
			$Attr		= $Data['Attr'];	//产品属性
			$Weight		= $Data['Weight'];	//产品目前的重量
			$Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
			$SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
			$seckill_row= $Data['SeckRow'];	//秒杀数据
			$OvId		= 1;//发货地ID
			$in_cart	= true;
			$sAttr		= str::json_data(str::str_code(str::json_data($_Attr[$m], 'decode'), 'stripslashes'));
			$cart_where	= "{$c['where']['cart']} and ProId='{$ProId}' and BuyType='{$BuyType}' and KeyId='{$KeyId}'".($_Attr[$m]?" and Attr='{$sAttr}'":'');
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 1,//获取产品属性名称
				'BuyType'		=> $BuyType,
				'ProId'			=> $ProId,
				'Price'			=> $Price,
				'Attr'			=> $Attr,
				'IsCombination'	=> $Data['ProdRow']['IsCombination'],
				'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
				'SKU'			=> $SKU,
				'Weight'		=> $Weight
			));
			if($AttrData===false) continue;
			$Property		= $AttrData['Property'];	//产品属性名称
			$Price			= $AttrData['Price'];		//产品单价
			$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
			$OvId			= $AttrData['OvId'];		//发货地ID
			$ColorId		= $AttrData['ColorId'];		//颜色ID
			$Weight			= $AttrData['Weight'];		//产品重量
			$SKU			= $AttrData['SKU'];			//产品SKU
			$Attr			= $AttrData['Attr'];		//产品属性数据
			//没有库存
			if(!(int)$Data['ProdRow']['Stock']){
				if($IsStock || (!$IsStock && count($combinatin_ary)==0)){//关闭无限库存 or 开启无限库存，没有属性
					if($p_back){ //返回JSON格式
						ly200::e_json($c['lang_pack']['cart']['error']['additem_stock'], 2);
					}else{ //跳转执行
						js::location(ly200::get_url($Data['ProdRow'], 'products'), $c['lang_pack']['cart']['error']['additem_stock']);
					}
				}
			}
			$cart_where.=" and OvId='{$OvId}'";//记入发货地的条件
			//更新？插入？
			if(db::get_row_count('shopping_cart', $cart_where)){
				//!db::get_row_count('shopping_cart', "$cart_where and Property='$Property'") && $in_cart=false;
				$in_cart=true;
			}else{
				$in_cart=false;
			}
			if($p_excheckout==1) $in_cart=false;//产品详细页的快捷支付，单独创建
			//库存检测
			$NowQty=0;
			if($in_cart==true){
				$NowQty=(int)db::get_value('shopping_cart', $cart_where, 'Qty');
				$Qty=($Qty+$NowQty);//更新后数量
			}
			$Qty=cart::check_product_stock(array(
				'IsStock'	=> $IsStock,
				'BuyType'	=> $BuyType,
				'Qty'		=> $Qty,
				'ProdRow'	=> $Data['ProdRow'],
				'Combinatin'=> $combinatin_ary,
				'Seckill'	=> $seckill_row
			));
			if($Qty===false) continue;
			if($BuyType==0){//普通产品，计算价格，开始判断批发价和促销价
				// if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
				}else{ //属性类型是“单价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
				}
			}
			//产品图片
			$PicPath=$Data['ProdRow']['PicPath_0'];
			//颜色属性产品图片
			if($ColorId){
				$Path=db::get_value('products_color', "ProId='{$ProId}' and VId='{$ColorId}'", 'PicPath_0');
				@is_file($c['root_path'].$Path) && $PicPath=$Path;
			}
			if($in_cart==false){
				db::insert('shopping_cart', array(
						'UserId'		=>	0,
						'SessionId'		=>	$c['session_id'],
						'ProId'			=>	$ProId,
						'BuyType'		=>	$BuyType,
						'KeyId'			=>	$KeyId,
						'SKU'			=>	addslashes($SKU),
						'PicPath'		=>	$PicPath,
						'StartFrom'		=>	$StartFrom,
						'Weight'		=>	(float)$Weight,
						'Volume'		=>	$Volume,
						'Price'			=>	$Price,
						'Qty'			=>	$Qty,
						'Property'		=>	addslashes($Property),
						'PropertyPrice'	=>	$PropertyPrice,
						'Attr'			=>	$sAttr,
						'OvId'			=>	$OvId,
						'Discount'		=>	$Discount,
						'Language'		=>	substr($c['lang'], 1),
						'AddTime'		=>	$c['time']
					)
				);
			}else{
				db::update('shopping_cart', $cart_where, array(
						'Price'			=>	$Price,
						'Qty'			=>	$Qty,
						'PropertyPrice'	=>	$PropertyPrice,
					)
				);
			}
			$CId=(int)db::get_value('shopping_cart', $cart_where, 'CId', 'CId desc');
			db::delete('shopping_cart', "{$c['where']['cart']} and CId!='$CId'");//删除当前以外的产品
		}
		//立即购买
		ly200::e_json(array('location'=>'/order_cart.html?Data='.base64_encode("CId={$CId}"), 'CId'=>$CId, 'ProId'=>$ProId, 'item_price'=>cart::iconv_price((($Price+$PropertyPrice)*100/$Discount)*($Qty-$NowQty), 2, '', 0)), 1);
	}
	
	public static function buynow_placeorder(){	//下订单 place an order
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		//付款方式
		$PId=(int)$p_order_payment_method_pid;
		(!$PId || $PId==-1) && ly200::e_json('', -3);
		$payment_row=str::str_code(db::get_one('payment', 'Method="CashOnDelivery"'));
		//发货地址、账单地址
		$data_user=array('UserId'=>0, 'Email'=>$p_Email);
		!$data_user['Email'] && ly200::e_json('', -1);
		$tax_ary=$bill_tax_ary=array();
		$address_country_row=db::get_one('country', "CId='$p_country_id'", 'Country, Code');
		$StateName=db::get_value('country_states', "CId='$p_country_id' and SId='{$p_Province}'", 'States');
		$address_row=array(
			'FirstName'		=>	str_replace(array('\\', '\\\\'), '', $p_FirstName),
			'LastName'		=>	str_replace(array('\\', '\\\\'), '', $p_LastName),
			'AddressLine1'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine1),
			'AddressLine2'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine2),
			'CountryCode'	=>	($address_country_row['Code']?$address_country_row['Code']:(int)$p_CountryCode),
			'PhoneNumber'	=>	$p_PhoneNumber,
			'City'			=>	str_replace(array('\\', '\\\\'), '', $p_City),
			'SId'			=>	(int)$p_Province,
			'State'			=>	str_replace(array('\\', '\\\\'), '', $p_State),
			'StateName'		=>	$StateName,
			'CId'			=>	(int)$p_country_id,
			'Country'		=>	$address_country_row['Country'],
			'ZipCode'		=>	$p_ZipCode,
			'CodeOption'	=>	$p_CodeOption,
			'TaxCode'		=>	$p_TaxCode,
		);
		$bill_row=$address_row;
		$tax_ary=$bill_tax_ary=user::get_tax_info($address_row);
		//检查产品的信息是否正常
		$cart_where='';
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$cart_where=" and CId in({$in_where})";
		}
		$cart_row=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.'.$c['where']['cart'].$cart_where, "s.CId, s.ProId, s.BuyType, s.KeyId, s.PicPath, s.Price, s.Property, s.PropertyPrice, s.Discount, s.Qty, s.Weight as CartWeight, s.Volume, s.OvId, s.Remark, s.Language, s.SKU as CartSKU, s.Attr as CartAttr, p.IsFreeShipping", 's.CId desc');
		!count($cart_row) && ly200::e_json('', -4);
		$IsStock=(int)$c['config']['products_show']['Config']['stock'];
		$stock_error='0';
		$IsFreeShipping=$ProductPrice=$totalWeight=$totalVolume=$s_totalWeight=$s_totalVolume=0;
		$pro_info_ary=array();//统计产品重量体积，用于运费计算
		foreach($cart_row as $key=>$val){
			//产品类型的处理
			$Data=cart::product_type(array(
				'Type'	=> $val['BuyType'],
				'KeyId'	=> $val['KeyId'],
				'Qty'	=> $val['Qty'],
				'Attr'	=> $val['CartAttr']
			));
			if($Data===false) $stock_error.=",{$val['CId']}";
			$BuyType	= $Data['BuyType'];	//产品类型
			$KeyId		= $Data['KeyId'];	//主ID
			$ProId		= $Data['ProId'];	//产品ID
			$StartFrom	= $Data['StartFrom'];//产品目前的起订量
			$Price		= $Data['Price'];	//产品目前的单价
			$Qty		= $Data['Qty'];		//购买的数量
			$Discount	= $Data['Discount'];//折扣
			$Attr		= $Data['Attr'];	//产品属性
			$Weight		= $Data['Weight'];	//产品目前的重量
			$Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
			$SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
			$seckill_row= $Data['SeckRow'];	//秒杀数据
			$OvId		= 1;//发货地ID
			$Qty<1 && $Qty=1;
			$cart_row[$key]['ProId']=$ProId;//记录产品ID
			$cart_row[$key]['Name']=$Data['ProdRow']['Name'.$c['lang']];//记录产品名称
			if($BuyType!=4){//“组合促销”除外
				!$Data['ProdRow']['Number'] && $stock_error.=",{$val['CId']}";//既不是组合促销，产品也同时不存在
				if(($IsStock && ($Data['ProdRow']['Stock']<$Qty || $Data['ProdRow']['Stock']<$Data['ProdRow']['MOQ'] || $Data['ProdRow']['Stock']<1)) || $Data['ProdRow']['SoldOut'] || ($Data['ProdRow']['IsSoldOut'] && ($Data['ProdRow']['SStartTime']>$c['time'] || $c['time']>$Data['ProdRow']['SEndTime'])) || in_array($Data['ProdRow']['CateId'], $c['procate_soldout'])){//产品库存量不足（包括产品下架）
					$stock_error.=",{$val['CId']}";
				}
			}
			//产品属性的处理
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'BuyType'		=> $BuyType,
				'ProId'			=> $ProId,
				'Price'			=> $Price,
				'Attr'			=> $Attr,
				'IsCombination'	=> $Data['ProdRow']['IsCombination'],
				'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
				'SKU'			=> $Data['ProdRow']['SKU'],
				'Weight'		=> $Weight
			));
			if($AttrData===false) $stock_error.=",{$val['CId']}";
			$Price			= $AttrData['Price'];		//产品单价
			$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
			$OvId			= $AttrData['OvId'];		//发货地ID
			$ColorId		= $AttrData['ColorId'];		//颜色ID
			$Weight			= $AttrData['Weight'];		//产品重量
			$SKU			= $AttrData['SKU'];			//产品SKU
			$Attr			= $AttrData['Attr'];		//产品属性数据
			if((int)$IsStock && (int)$Data['ProdRow']['IsCombination'] && $combinatin_ary && (!$combinatin_ary[1] || $Qty>$combinatin_ary[1])){//产品属性库存量不足
				$stock_error.=",{$val['CId']}";//没有无限库存、开启组合属性
			}
			if((int)$IsStock==0 && (int)$Data['ProdRow']['IsCombination']==0 && $combinatin_ary && $combinatin_ary[1]>0 && $Qty>$combinatin_ary[1]){//产品属性库存量不足
				$stock_error.=",{$val['CId']}";//开启无限库存、不是组合属性、属性库存不是0
			}
			//普通产品，重新计算价格，开始判断批发价和促销价
			if($BuyType==0){
				// if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || (int)$Data['ProdRow']['IsCombination']==0 || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
				}else{ //没有属性
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
				}
			}
			//更新产品信息
			$update_data=array(
				'Weight'		=>	(float)($Weight?$Weight:$val['CartWeight']),
				'Volume'		=>	(float)sprintf('%01.3f', ($Volume?$Volume:$val['Volume'])),
				'Price'			=>	(float)sprintf('%01.2f', ($Price?$Price:$val['Price'])),
				'Qty'			=>	$Qty,
				'PropertyPrice'	=>	(float)sprintf('%01.2f', ($PropertyPrice?$PropertyPrice:$val['PropertyPrice'])),
				'Discount'		=>	(int)($Discount?$Discount:$val['Discount'])
			);
			if($BuyType==0){//检查产品混批功能
				$result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, 1, ($p_order_cid?1:0));//检查产品混批功能
				$result && $update_data['Price']=$result;
			}
			db::update('shopping_cart', "CId='{$val['CId']}'", $update_data);//更新购物车产品信息
			if($BuyType==0){//检查产品混批功能
				$result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, $val['CId'], ($p_order_cid?1:0));//检查产品混批功能
				$result && $update_data['Price']=$result;
			}
			//整理产品的信息数据
			$cart_row[$key]=array_merge($cart_row[$key], $update_data);
			$item_price=($update_data['Price']+$update_data['PropertyPrice'])*($update_data['Discount']<100?$update_data['Discount']/100:1)*$Qty;
			$ProductPrice+=$item_price;
			$totalVolume+=($update_data['Volume']*$Qty);
			if(!$pro_info_ary[$OvId]){
				$pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
			}
			$pro_info_ary[$OvId]['tWeight']+=($update_data['Weight']*$Qty);
			$pro_info_ary[$OvId]['tVolume']+=($update_data['Volume']*$Qty);
			$pro_info_ary[$OvId]['tQty']+=$Qty;
			$pro_info_ary[$OvId]['Price']+=$item_price;
			if((int)$val['IsFreeShipping']==1){//免运费
				$pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
			}else{
				$pro_info_ary[$OvId]['Weight']+=($update_data['Weight']*$Qty);
				$pro_info_ary[$OvId]['Volume']+=($update_data['Volume']*$Qty);
				$pro_info_ary[$OvId]['Qty']+=$Qty;
			}
			if(${'p_Remark_'.$val['ProId'].'_'.$val['CId']}){//记录备注
				$cart_row[$key]['Remark']=${'p_Remark_'.$val['ProId'].'_'.$val['CId']};
			}
		}
		$stock_error!='0' && ly200::e_json($stock_error, -6);//产品库存量不足
		//产品包装重量
		$weight_where='';
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$weight_where=" and c.CId in({$in_where})";
		}
		$cartProAry=cart::cart_product_weight($weight_where, 1);
		foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['tWeight']+=$v2;
			}
		}
		foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['Weight']+=$v2;
			}
		}
		//总重量
		$totalWeight=cart::cart_product_weight($weight_where, 2);
		
		//“会员优惠”和“全场满减”之间的优惠对比
		$DisData=cart::discount_contrast($ProductPrice);
		$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
		$Discount		= $DisData['Discount'];			//全场满减的折扣
		$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
		//最低消费设置
		$_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)*(1-$CouponDiscount)-$CouponPrice-$DiscountPrice;//订单折扣后的总价
		if((int)$c['config']['global']['LowConsumption'] && cart::iconv_price($_total_price, 2, '', 0)<cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0)){
			ly200::e_json(cart::iconv_price($c['config']['global']['LowPrice'], 0, '', 0), -5);
		}
		//确认这订单能够正常下单，才进行更新操作的优惠券信息
		if($coupon_data){
			db::update('sales_coupon', "CouponNumber='{$CouponCode}'", $coupon_data);
		}
		//游客，自动获取上次最新下单的业务员数据（相同邮箱地址）
		$SalesId=0;
		if((int)$data_user['UserId']==0){
			$SalesId=(int)db::get_value('user', "Email='{$data_user['Email']}' and SalesId>0", 'SalesId', 'UserId desc');
		}
		//分销订单
		if($data_user['UserId']==0 && $_SESSION['DIST']){
			$source_row=db::get_one('user', "UserId='{$_SESSION['DIST']['UserId']}'");//分销上级的会员数据
			$DISTInfo=$source_row['DISTUId'].$source_row['UserId'].',';
		}
		//创建订单号
		while(1){
			$OId=date('ymdHis', $c['time']).mt_rand(10,99);
			if(!db::get_row_count('orders', "OId='$OId'")){ break; }
		}
		$order_data=array(
			/*******************订单基本信息*******************/
			'OId'					=>	$OId,
			'UserId'				=>	$data_user['UserId'],
			'Source'				=>	ly200::is_mobile_client(0)?1:0,
			'RefererId'				=>	(int)$_COOKIE['REFERER'],
			'Email'					=>	$data_user['Email'],
			'SalesId'				=>	$SalesId,
			'Discount'				=>	$Discount,
			'DiscountPrice'			=>	$DiscountPrice,
			'UserDiscount'			=>	$UserDiscount,
			'ProductPrice'			=>	(float)substr(sprintf('%01.3f', $ProductPrice), 0, -1),//$ProductPrice,
			'Currency'				=>	$_SESSION['Currency']['Currency'],//当前货币
			'ManageCurrency'		=>	$_SESSION['ManageCurrency']['Currency'],//当前后台货币
			'Rate'					=>	$_SESSION['Currency']['Rate'],//当前货币的汇率
			'TotalWeight'			=>	$totalWeight,
			'TotalVolume'			=>	$totalVolume,
			'OrderTime'				=>	$c['time'],
			'UpdateTime'			=>	$c['time'],
			'Note'					=>	$p_Note,
			'DISTInfo'				=>	$DISTInfo,
			'IP'					=>	ly200::get_ip(),
			/*******************优惠券信息*******************/
			'CouponCode'			=>	$CouponCode,
			'CouponPrice'			=>	$CouponPrice,
			'CouponDiscount'		=>	$CouponDiscount,
			/*******************收货地址*******************/
			'ShippingFirstName'		=>	addslashes($address_row['FirstName']),
			'ShippingLastName'		=>	addslashes($address_row['LastName']),
			'ShippingAddressLine1'	=>	addslashes($address_row['AddressLine1']),
			'ShippingAddressLine2'	=>	addslashes($address_row['AddressLine2']),
			'ShippingCountryCode'	=>	'+'.$address_row['CountryCode'],
			'ShippingPhoneNumber'	=>	$address_row['PhoneNumber'],
			'ShippingCity'			=>	addslashes($address_row['City']),
			'ShippingState'			=>	addslashes($address_row['StateName']?$address_row['StateName']:$address_row['State']),
			'ShippingSId'			=>	$address_row['SId'],
			'ShippingCountry'		=>	addslashes($address_row['Country']),
			'ShippingCId'			=>	$address_row['CId'],
			'ShippingZipCode'		=>	$address_row['ZipCode'],
			'ShippingCodeOption'	=>	$tax_ary['CodeOption'],
			'ShippingCodeOptionId'	=>	$tax_ary['CodeOptionId'],
			'ShippingTaxCode'		=>	$tax_ary['TaxCode'],
			/*******************账单地址*******************/
			'BillFirstName'			=>	addslashes($bill_row['FirstName']),
			'BillLastName'			=>	addslashes($bill_row['LastName']),
			'BillAddressLine1'		=>	addslashes($bill_row['AddressLine1']),
			'BillAddressLine2'		=>	addslashes($bill_row['AddressLine2']),
			'BillCountryCode'		=>	'+'.$bill_row['CountryCode'],
			'BillPhoneNumber'		=>	$bill_row['PhoneNumber'],
			'BillCity'				=>	addslashes($bill_row['City']),
			'BillState'				=>	addslashes($bill_row['StateName']?$bill_row['StateName']:$bill_row['State']),
			'BillSId'				=>	$bill_row['SId'],
			'BillCountry'			=>	addslashes($bill_row['Country']),
			'BillCId'				=>	$bill_row['CId'],
			'BillZipCode'			=>	$bill_row['ZipCode'],
			'BillCodeOption'		=>	$bill_tax_ary['CodeOption'],
			'BillCodeOptionId'		=>	$bill_tax_ary['CodeOptionId'],
			'BillTaxCode'			=>	$bill_tax_ary['TaxCode'],
			/*******************发货方式*******************/
			//'ShippingExpress'		=>	addslashes($shipping_ary['ShippingExpress']?$shipping_ary['ShippingExpress']:''),
			//'ShippingMethodSId'		=>	addslashes($shipping_ary['ShippingMethodSId']?$shipping_ary['ShippingMethodSId']:''),
			//'ShippingMethodType'	=>	addslashes($shipping_ary['ShippingMethodType']?$shipping_ary['ShippingMethodType']:''),
			//'ShippingInsurance'		=>	addslashes($shipping_ary['ShippingInsurance']?$shipping_ary['ShippingInsurance']:''),
			//'ShippingPrice'			=>	$shipping_ary['ShippingPrice'],
			//'ShippingInsurancePrice'=>	$shipping_ary['ShippingInsurancePrice'],
			//'ShippingOvExpress'		=>	addslashes($shipping_ary['ShippingOvExpress']?$shipping_ary['ShippingOvExpress']:''),
			//'ShippingOvSId'			=>	addslashes($shipping_ary['ShippingOvMethodSId']?$shipping_ary['ShippingOvMethodSId']:''),
			//'ShippingOvType'		=>	addslashes($shipping_ary['ShippingOvMethodType']?$shipping_ary['ShippingOvMethodType']:''),
			//'ShippingOvInsurance'	=>	addslashes($shipping_ary['ShippingOvInsurance']?$shipping_ary['ShippingOvInsurance']:''),
			//'ShippingOvPrice'		=>	addslashes($shipping_ary['ShippingOvPrice']?$shipping_ary['ShippingOvPrice']:''),
			//'ShippingOvInsurancePrice'=>addslashes($shipping_ary['ShippingOvInsurancePrice']?$shipping_ary['ShippingOvInsurancePrice']:''),
			/*******************付款方式*******************/
			'PId'					=>	$PId,
			'PaymentMethod'			=>	$payment_row['Name'.$c['lang']],//db::get_value('payment', "IsUsed=1 and PId='$PId'", "Name{$c['lang']}"),
			'PayAdditionalFee'		=>	$payment_row['AdditionalFee'],
			'PayAdditionalAffix'	=>	$payment_row['AffixPrice']
		);
		//生成订单
		db::insert('orders', $order_data);
		$OrderId=db::get_insert_id();
		//删除购物车相关的信息
		$_SESSION['Cart']['Coupon']='';
		$where=$c['where']['cart'];
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$where.=" and CId in({$in_where})";
		}
		db::delete('shopping_cart', $where);
		unset($tax_ary, $shipping_ary, $coupon_row, $_SESSION['Cart']['Coupon']);
		//购物车产品的数据转移
		$i=1;
		$insert_sql='';
		$order_pic_dir=$c['orders']['path'].date('ym', $c['time'])."/{$OId}/";
		!is_dir($c['root_path'].$order_pic_dir) && file::mk_dir($order_pic_dir);
		foreach($cart_row as $v){
			$ext_name=file::get_ext_name($v['PicPath']);
			$ImgPath=$order_pic_dir.str::rand_code().'.'.$ext_name;
			@copy($c['root_path'].$v['PicPath'].'.240x240.'.$ext_name, $c['root_path'].$ImgPath);
			$Name=str::str_code($v['Name'], 'addslashes');
			$SKU=str::str_code(($v['CartSKU']?$v['CartSKU']:$v['SKU']), 'addslashes');
			$Property=str::str_code($v['Property'], 'addslashes');
			$Remark=str::str_code($v['Remark'], 'addslashes');
			$insert_sql.=($i%100==1)?"insert into `orders_products_list` (OrderId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Price, Qty, Property, PropertyPrice, OvId, Discount, Remark, Language, AccTime) VALUES":',';
			$insert_sql.="('$OrderId', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$ImgPath}', '{$v['StartFrom']}', '{$v['Weight']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}', {$v['Discount']}, '{$Remark}', '{$v['Language']}', '{$c['time']}')";
			if($i++%100==0){
				db::query($insert_sql);
				$insert_sql='';
			}
		}
		$insert_sql!='' && db::query($insert_sql);
		unset($address_row, $data_oth);
		orders::orders_log((int)$data_user['UserId'], $data_user['UserId']?addslashes($_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']):'System', $OrderId, 1, "Place an Order: ".$OId);
		if((int)$c['config']['global']['LessStock']==0){//下单减库存
			$orders_row=db::get_one('orders', "OrderId='$OrderId'");
			orders::orders_products_update(1, $orders_row);
		}
		if((int)$c['config']['global']['CheckoutEmail']){//下单后邮件通知
			$ToAry=array($data_user['Email']);
			include($c['static_path'].'/inc/mail/order_create.php');
			$c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
			$c['config']['global']['OrdersSmsStatus'][1] && orders::orders_sms($OId);
		}
		$p_ReturnType=(int)$p_ReturnType;//数据反馈方式
		if($p_ReturnType==1){
			return array('OId'=>$OId, 'Method'=>$payment_row['Method']);
		}else{
			ly200::e_json(array('OId'=>$OId, 'Method'=>$payment_row['Method']), 1);
		}
	}
	
	public static function goods_detail_pic(){
		global $c;
		//产品详细页主图 小图的横向显示
		$ProId=(int)$_GET['ProId'];
		$ColorId=(int)$_GET['ColorId'];
		$row=str::str_code(db::get_one('products_color', "ProId='$ProId' and VId='$ColorId' and VId>0 and PicPath_0!=''"));
		$pro_row=str::str_code(db::get_one('products', "ProId='$ProId'"));
		if(!$row || !is_file($c['root_path'].$row['PicPath_0'])) $row=$pro_row;
		?>
		<div class="goods_pic">
			<ul class="clean">
				<?php
				for($i=0; $i<10; ++$i){
					$pic=$row['PicPath_'.$i];
					if(!is_file($c['root_path'].$pic)) continue;
				?>
					<li class="fl"><img src="<?=ly200::get_size_img($pic, '500x500');?>"></li>
				<?php }?>
				<?php if($pro_row['VideoUrl']){?>
					<li class="fl video_container"><img src="<?=ly200::get_size_img($row['PicPath_0'], '500x500');?>"></li>
				<?php } ?>
			</ul>
			<div class="trigger clean">
				<?php
				for($i=0; $i<10; $i++){
					$pic=$row['PicPath_'.$i];
					if(!is_file($c['root_path'].$pic)) continue;
				?>
					<div class="item<?=$i==0?' FontBgColor':' off';?>"><?=$i;?></div>
				<?php }?>
				<?php if($pro_row['VideoUrl']){?>
					<div class="item off"></div>
				<?php } ?>
			</div>
		</div>
		<?php if($pro_row['VideoUrl']){?>
			<div class="pro_video"><a href="javascript:;" rel="nofollow" class="close"></a><?=html::video_show($pro_row['VideoUrl']);?></div>
		<?php } ?>
		<!-- 抛物线的div -->
		<div class="big_pic" style="display:none;"><img src="<?=ly200::get_size_img($row['PicPath_0'], '240x240');?>" class="normal" alt="<?=$pro_row['Name'.$c['Lang']];?>"></div>
	<?php
	}
	/********************************** 单独购物流程 end **********************************/
}