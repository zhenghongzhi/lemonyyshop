<?php
/*
 * Powered by ueeshop.com   http://www.ueeshop.com
 * 广州联雅网络科技有限公司   020-83226791
 * Note: 购物车操作事件
 */

class cart_module{
	public static function additem(){	//add product to shopping cart
	    print_r($_POST);die;
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_back=(int)$p_back;
		$p_IsBuyNow=(int)$p_IsBuyNow;//是否为立即购买
		$products_type=(int)$p_products_type;//产品类型，0：普通产品，1：团购，2：秒杀，3:组合购买，4:组合促销
		//$p_id && $p_Attr=str::json_data(str::str_code($p_id, 'stripslashes'));//注册后自动返回立即添加购物车的属性数据
		/*
		$p_id=str::str_code($p_id, 'stripslashes');
		$p_Attr=$p_id?addslashes(str::json_data($p_id)):$p_Attr; //注册后自动返回立即添加购物车的属性数据
		*/
		if($p_id){	//注册后自动返回立即添加购物车的属性数据
			$id_ary=array();
			foreach((array)$p_id as $k=>$v){
				$key=($k=='Overseas'?$k:(int)$k);
				$value=($k=='Overseas'?('Ov:'.(int)str_replace('Ov:','',$v)):(int)$v);
				$id_ary[$key]=$value;
            }
            $p_Attr=str::json_data($id_ary);
        }
		//初始化
		if($products_type==1){//团购
			$_KeyId=array($p_TId);
			$_Attr=array($p_Attr);
		}else{
			if($products_type==2){//秒杀
				$_KeyId=array($p_SId);
				$_Qty=array($p_Qty);
				$_Attr=array($p_Attr);
			}elseif($products_type==3){//组合购买
				$_KeyId=explode(',', $p_ProId);
				//$mobile = ly200::is_mobile_client(0) ? 1 : 0;
				if ($mobile) {
					$_Qty[0]=$p_Qty; //主产品数量
					$_Attr[0]=$p_Attr;	//主产品属性
				}
				if($p_ExtAttr){
					$package_data=str::json_data(htmlspecialchars_decode(stripslashes($p_ExtAttr)), 'decode');
				}else{
					$p_PId=(int)$p_PId;
					$PackageData=db::get_value('sales_package', "PId='$p_PId'", 'PackageData');
					$package_data=str::json_data($PackageData, 'decode');
				}
				foreach($_KeyId as $k=>$v){
					//if($k==0 && $mobile) continue;
					$_Attr[$k]='';
					if($package_data[$v]){
						$pachage_attr=array();
						foreach((array)$package_data[$v] as $key=>$value){
							$key!='Overseas' && $key=(int)$key;
							$pachage_attr[$key]=($key=='Overseas'?('Ov:'.(int)str_replace('Ov:','',$value)):(int)$value);
						}
						//$_Attr[$k]=addslashes(str::json_data($pachage_attr));
                        $_Attr[$k]=str::json_data($pachage_attr);
					}
					//$_Attr[$k]=$package_data[$v]?addslashes(str::json_data($package_data[$v])):'';
				}
			}elseif($products_type==4){//组合促销
				$_KeyId = array($p_PId);
                $Attr = str::json_data(htmlspecialchars_decode(stripslashes($p_Attr)), 'decode');
                $_Attr[0] = str::json_data($Attr); //主产品属性
			}else{//普通产品
				if(!@is_array($p_ProId)){
					$_KeyId=array($p_ProId);
					$_Qty=array($p_Qty);
					$_Attr=array($p_Attr);
				}else{
					$_KeyId=$p_ProId;
					$_Qty=$p_Qty;
					$_Attr=$p_Attr;
				}
			}
		}
		for($m=0; $m<count($_KeyId); $m++){
			//初始化
			$Data=cart::product_type(array(
				'Type'	=> $products_type,
				'KeyId'	=> $_KeyId[$m],
				'Qty'	=> $_Qty[$m],
				'Attr'	=> $_Attr[$m]
			));
			if($Data===false) continue;
			$BuyType	= $Data['BuyType'];	//产品类型
			$KeyId		= $Data['KeyId'];	//主ID
			$ProId		= $Data['ProId'];	//产品ID
			$StartFrom	= $Data['StartFrom'];//产品目前的起订量
			$Price		= $Data['Price'];	//产品目前的单价
			$Qty		= $Data['Qty'];		//购买的数量
			$Discount	= $Data['Discount'];//折扣
			$Attr		= $Data['Attr'];	//产品属性
			$Weight		= $Data['Weight'];	//产品目前的重量
			$Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
			$SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
			$seckill_row= $Data['SeckRow'];	//秒杀数据
			$tuan_row	= $Data['TuanRow'];	//团购数据
			$IsStock	= (int)$Data['ProdRow']['SoldStatus'];//售卖状态
			$OvId		= 1;//发货地ID
			$in_cart	= true;
			//$sAttr		= str::json_data(str::str_code(str::json_data($_Attr[$m], 'decode'), 'stripslashes'));
			$sAttr		= str::json_data(str::str_code(str::json_data(str::str_code($_Attr[$m], 'stripslashes'), 'decode'), 'stripslashes'));
			$cart_where	= "{$c['where']['cart']} and ProId='{$ProId}' and BuyType='{$BuyType}' and KeyId='{$KeyId}'".($_Attr[$m]?" and Attr='{$sAttr}'":'');
			//产品属性
			if($BuyType!=4){
				$AttrData=cart::get_product_attribute(array(
					'Type'			=> 1,//获取产品属性名称
					'BuyType'		=> $BuyType,
					'ProId'			=> $ProId,
					'Price'			=> $Price,
					'Attr'			=> $Attr,
					'IsCombination'	=> $Data['ProdRow']['IsCombination'],
					'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
					'SKU'			=> $SKU,
					'Weight'		=> $Weight
				));
				if($AttrData===false) continue;
				$Property		= $AttrData['Property'];	//产品属性名称
				$Price			= $AttrData['Price'];		//产品单价
				$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
				$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
				$OvId			= $AttrData['OvId'];		//发货地ID
				$ColorId		= $AttrData['ColorId'];		//颜色ID
				$Weight			= $AttrData['Weight'];		//产品重量
				$SKU			= $AttrData['SKU'];			//产品SKU
				$Attr			= $AttrData['Attr'];		//产品属性数据
			}
			//没有库存
			if(!(int)$Data['ProdRow']['Stock']){
				if($IsStock!=1 && count($combinatin_ary)==0){//库存为0，(不允许购买 or 下架)，没有属性
					if($p_back){ //返回JSON格式
						ly200::e_json($c['lang_pack']['cart']['error']['additem_stock'], 2);
					}else{ //跳转执行
						js::location(ly200::get_url($Data['ProdRow'], 'products'), $c['lang_pack']['cart']['error']['additem_stock']);
					}
				}
			}
			$cart_where.=" and OvId='{$OvId}'";//记入发货地的条件
			$OldCId=0;
			//更新？插入？
			if(db::get_row_count('shopping_cart', $cart_where)){
				$in_cart=true;
				$OldCId=(int)db::get_value('shopping_cart', $cart_where, 'CId');
			}else{
				$in_cart=false;
			}
			if($p_excheckout==1) $in_cart=false;//产品详细页的快捷支付，单独创建
			//库存检测
			$NowQty=0;
			if($in_cart==true){
				$NowQty=(int)db::get_value('shopping_cart', $cart_where, 'Qty');
				$Qty=($Qty+$NowQty);//更新后数量
			}
			$Qty=cart::check_product_stock(array(
				'CId'		=> $OldCId,
				'IsStock'	=> $IsStock,
				'BuyType'	=> $BuyType,
				'Qty'		=> $Qty,
				'ProdRow'	=> $Data['ProdRow'],
				'Combinatin'=> $combinatin_ary,
				'Seckill'	=> $seckill_row,
				'Tuan'		=> $tuan_row
			));
			if($Qty===false) continue;
			if($BuyType==0){//普通产品，计算价格，开始判断批发价和促销价
				// if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
				}else{ //属性类型是“单价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
				}
			}
			//产品图片
			$PicPath=$Data['ProdRow']['PicPath_0'];
			//颜色属性产品图片
			if($ColorId){
				$Path=db::get_value('products_color', "ProId='{$ProId}' and VId='{$ColorId}'", 'PicPath_0');
				@is_file($c['root_path'].$Path) && $PicPath=$Path;
			}
			if($p_excheckout==1){//产品详细页的快捷支付
				$low_ary=self::check_low_consumption(1, ($Price+$PropertyPrice)*$Discount/100*$Qty);
				if($low_ary){//未达到最低消费金额
					ly200::e_json(array('qty'=>$Qty, 'price'=>($Price+$PropertyPrice)*$Discount/100, 'low_price'=>$low_ary['low_price'], 'difference'=>$low_ary['difference']), 2);
				}
			}
			if($in_cart==false){
				db::insert('shopping_cart', array(
						'UserId'		=>	(int)$_SESSION['User']['UserId'],
						'SessionId'		=>	$c['session_id'],
						'ProId'			=>	$ProId,
						'BuyType'		=>	$BuyType,
						'KeyId'			=>	$KeyId,
						'SKU'			=>	addslashes($SKU),
						'PicPath'		=>	$PicPath,
						'StartFrom'		=>	$StartFrom,
						'Weight'		=>	(float)$Weight,
						'Volume'		=>	$Volume,
						'Price'			=>	$Price,
						'Qty'			=>	$Qty,
						'Property'		=>	addslashes($Property),
						'PropertyPrice'	=>	$PropertyPrice,
						'Attr'			=>	$sAttr,
						'OvId'			=>	$OvId,
						'Discount'		=>	$Discount,
						'Language'		=>	substr($c['lang'], 1),
						'AddTime'		=>	$c['time']
					)
				);
			}else{
				db::update('shopping_cart', $cart_where, array(
						'Price'			=>	$Price,
						'Qty'			=>	$Qty,
						'PropertyPrice'	=>	$PropertyPrice,
					)
				);
			}
			if(!$p_excheckout && $BuyType==0){//检查产品混批功能
				cart::update_cart_wholesale_price($ProId, $Data['ProdRow']);
			}
			if($p_IsBuyNow) $CId=(int)db::get_value('shopping_cart', $cart_where, 'CId', 'CId desc');
		}
		$p_ReturnType=(int)$p_ReturnType;//数据反馈方式
		if($p_ReturnType==1){//新版Paypal checkout返回
			$CId=(int)db::get_value('shopping_cart', $cart_where, 'CId', 'CId desc');
			$total_price=(float)cart::cart_total_price('', 1);
			$low_ary=self::check_low_consumption(1);
			return array('CId'=>$CId, 'price'=>($low_ary['s_total']?$low_ary['s_total']:$total_price), 'low_price'=>$low_ary['low_price']);
			exit;
		}
		if($p_back){
			if($p_IsBuyNow){
				//立即购买
				if($CId){
                    ly200::e_json(array('location'=>'/cart/buynow.html?Data='.base64_encode("CId={$CId}"), 'CId'=>$CId, 'ProId'=>$ProId, 'item_price'=>cart::iconv_price((($Price+$PropertyPrice)*100/$Discount)*($Qty-$NowQty), 2, '', 0)), 1);
                }
                ly200::e_json($c['lang_pack']['cart']['error']['additem_stock'], 2);
			}else{
				//添加购物车
				if((int)$_SESSION['User']['UserId']){	//已登录
					$UserId=$_SESSION['User']['UserId'];
					$user_where="UserId='{$UserId}'";
				}else{	//未登录
					$user_where="SessionId='{$c['session_id']}'";
				}
				$total_quantity=(int)db::get_row_count('shopping_cart', $user_where, 'CId');
				$default_total_price=(float)cart::cart_total_price('', 1, 0);
				$total_price=(float)cart::cart_total_price('', 1);
				$DiscountData=cart::check_list_discounts($default_total_price, $total_price, 1);//检查购物车列表的总价格变动后，优惠情况的变动
				$low_ary=self::check_low_consumption(1);
				ly200::e_json(array('qty'=>$total_quantity, 'price'=>($low_ary['s_total']?$low_ary['s_total']:$total_price), 'low_price'=>$low_ary['low_price'], 'difference'=>$low_ary['difference'], 'total_price'=>$total_price, 'item_price'=>cart::iconv_price((($Price+$PropertyPrice)*100/$Discount)*($Qty-$NowQty), 2, '', 0),'FullCondition'=>$DiscountData['fullcondition']), 1);
			}
		}else{
			js::location('/cart/', '', '.top');
		}
	}
	
	public static function select(){	//select item product of shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($p_CId=='0'){
			ly200::e_json(array('total_count'=>0, 'total_price'=>0, 'cutprice'=>0), 1);
		}else{
			$p_CId=str::ary_format($p_CId, 2);
			$cart_row=db::get_all('shopping_cart', "{$c['where']['cart']} and CId in({$p_CId})");
			$total_price=$total_count=0;
			foreach($cart_row as $k=>$v){
				$total_price+=(float)cart::iconv_price(($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1)*$v['Qty'], 2, '', 0);
				$total_count+=$v['Qty'];
			}
			$iconv_total_price=(float)cart::cart_total_price(($p_CId?" and c.CId in({$p_CId})":''), 1);
			$DiscountData=cart::check_list_discounts($total_price, $iconv_total_price);//检查购物车列表的总价格变动后，优惠情况的变动
			ly200::e_json(array('total_count'=>$total_count, 'total_price'=>$iconv_total_price, 'cutprice'=>$DiscountData['cutprice']), 1);
		}
	}
	
	public static function modify(){	//modify item quantity of shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
		$IsBuyNow=(int)$_GET['BuyNow'];
		$p_Qty=(int)$p_Qty;
		$p_CId=(int)$p_CId;
		$p_ProId=(int)$p_ProId;
		$cart_row=db::get_one('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'");
		$products_row=str::str_code(db::get_one('products', "ProId='{$p_ProId}'"));
		$IsStock=(int)$products_row['SoldStatus'];
		$AttrQty=0;
		$OvId=1;
		//产品属性
		if($cart_row['Attr']){
			$ext_ary=array();
			$attr_ary=@str::json_data(str::attr_decode($cart_row['Attr']), 'decode');
			foreach((array)$attr_ary as $k=>$v){
				if($k=='Overseas'){ //发货地
					$OvId=(int)str_replace('Ov:', '', $v);
					!$OvId && $OvId=1;//丢失发货地，自动默认China
				}else{
					$ext_ary[]=(int)$v;
				}
			}
			sort($ext_ary); //从小到大排序
			$Combination='|'.implode('|', $ext_ary).'|';
			$row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$p_ProId}' and Combination='{$Combination}' and OvId='{$OvId}'"));
			if($row){
				$combinatin_ary=array($row['Price'], $row['Stock'], $row['Weight'], $row['SKU'], $row['IsIncrease']);
				$AttrQty=(int)$combinatin_ary[1];
			}
		}
		if($cart_row['BuyType']==1){//团购产品
			$tuan_row=str::str_code(db::get_one('sales_tuan', "TId='{$cart_row['KeyId']}' and BuyerCount<TotalCount and {$c['time']} between StartTime and EndTime"));
		}
		if($cart_row['BuyType']==2){//秒杀产品
			$seckill_row=str::str_code(db::get_one('sales_seckill', "SId='{$cart_row['KeyId']}' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
		}
		//检查库存
		$Qty=cart::check_product_stock(array(
			'CId'		=> $p_CId,
			'IsStock'	=> $IsStock,
			'BuyType'	=> $cart_row['BuyType'],
			'Qty'		=> $p_Qty,
			'ProdRow'	=> $products_row,
			'Combinatin'=> $combinatin_ary,
			'Seckill'	=> $seckill_row,
			'Tuan'		=> $tuan_row
		));
		if($Qty===false) ly200::e_json('', 0);
		$Price=(int)$cart_row['BuyType']?$cart_row['Price']:cart::products_add_to_cart_price($products_row, $Qty);
		if($cart_row['BuyType']==0){//普通产品，计算价格，开始判断批发价和促销价
			// if($products_row['IsOpenAttrPrice']==0 || !$combinatin_ary || (int)$products_row['IsCombination']==0 || ($combinatin_ary && (int)$combinatin_ary[4]==1)){//没有属性，或者，属性类型是“加价”
			if((!$products_row['IsOpenAttrPrice'] && !$products_row['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $products_row['IsOpenAttrPrice']==1 && !$products_row['IsCombination']) || ($combinatin_ary && $products_row['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				$Price=cart::products_add_to_cart_price($products_row, $Qty);
			}else{//属性类型是“单价”
				$Price=cart::products_add_to_cart_price($products_row, $Qty, (float)$combinatin_ary[0]);
			}
		}
		$IsWholesale=0;//是不是混批产品
		db::update('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'", array(
				'Qty'	=>	$Qty,
				'Price'	=>	$Price
			)
		);
		if($cart_row['BuyType']==0){//检查产品混批功能
			$result=(float)cart::update_cart_wholesale_price($p_ProId, $products_row, $p_CId);//检查产品混批功能
			if($result){
				$Price=$result;
				$IsWholesale=1;
			}
			cart::update_cart_wholesale_price($p_ProId, $products_row);//检查产品混批功能
		}
		$Price=sprintf('%01.2f', $Price);
		//统计产品的改动单价计算
		$PriceAry=$AmountAry=array();
		$pro_list_row=db::get_all('shopping_cart', "{$c['where']['cart']} and ProId='{$p_ProId}'".($IsWholesale==0?" and CId='{$p_CId}'":''), 'CId, Price, PropertyPrice, Discount, Qty');
		foreach($pro_list_row as $k=>$v){
			$_p=cart::iconv_price(($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1), 2, '', 0);
			$PriceAry[$v['CId']]=$_p;
			$AmountAry[$v['CId']]=$_p*$v['Qty'];
		}
		$p_CIdAry=str::ary_format($p_CIdAry, 2);
		$tWhere=$c['where']['cart'].($IsBuyNow==1?" and CId='{$p_CId}'":'').($p_CIdAry?" and CId in({$p_CIdAry})":'');
		$total_weight=(float)db::get_sum('shopping_cart', $tWhere." and ProId='{$p_ProId}'", 'Weight*Qty');
		if(!$p_CIdAry){//一个列表产品都没有勾选
			$total_count=$total_price=$iconv_total_price=0;
		}else{//有勾选产品
			$total_count=(int)db::get_sum('shopping_cart', $tWhere, 'Qty');
			$total_price=(float)db::get_sum('shopping_cart', $tWhere, '(Price+PropertyPrice)*Discount/100*Qty');
			$iconv_total_price=(float)cart::cart_total_price(($IsBuyNow==1?" and c.CId='{$p_CId}'":'').($p_CIdAry?" and c.CId in({$p_CIdAry})":''), 1);
		}
		$cutArr=str::json_data(db::get_value('config', 'GroupId="cart" and Variable="discount"', 'Value'), 'decode');
		$DiscountData=cart::check_list_discounts($total_price, $iconv_total_price,1);//检查购物车列表的总价格变动后，优惠情况的变动
		ly200::e_json(array('qty'=>$Qty, 'price'=>$PriceAry, 'amount'=>$AmountAry, 'total_count'=>$total_count, 'total_price'=>$iconv_total_price, 'total_weight'=>$total_weight, 'cutprice'=>$DiscountData['cutprice'], 'IsCoupon'=>$DiscountData['IsCoupon'], 'FullCondition'=>$DiscountData['fullcondition']), 1);
	}

	public static function modify_remark(){	//modify item remark of shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_CId=(int)$p_CId;
		$p_ProId=(int)$p_ProId;
		db::update('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'", array('Remark'=>$p_Remark));
		ly200::e_json('', 1);
	}
	
	public static function modify_attribute(){	//modify item attribute of shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$data=array();
		$result=1;
		$IsBuyNow=(int)$_GET['BuyNow'];
		$p_CId=(int)$p_CId;
		$p_ProId=(int)$p_ProId;
		$cart_row=db::get_one('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}'");
		$products_row=str::str_code(db::get_one('products', "SoldOut!=1 and ProId='{$p_ProId}'"));
		$IsStock=(int)$products_row['SoldStatus'];
		$Price=$cart_row['Price'];//购物车的价格
		$Qty=$cart_row['Qty'];//购物车的数量
		$Volume=$cart_row['Volume'];//购物车的体积
		$Weight=$products_row['Weight'];//产品默认重量
		if((int)$products_row['IsVolumeWeight']){//体积重
			$VolumeWeight=($Volume*1000000)/5000;//先把立方米转成立方厘米，再除以5000
			$VolumeWeight>$Weight && $Weight=$VolumeWeight;
		}
		if(count($p_Attr)){ //产品属性执行
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 1,//获取产品属性名称
				'BuyType'		=> $cart_row['BuyType'],
				'ProId'			=> $p_ProId,
				'Price'			=> $Price,
				'Attr'			=> $p_Attr,
				'IsCombination'	=> $products_row['IsCombination'],
				'IsAttrPrice'	=> $products_row['IsOpenAttrPrice'],
				'SKU'			=> $products_row['SKU'],
				'Weight'		=> $Weight
			));
			if($AttrData===false) ly200::e_json('', 0);
			$Property		= $AttrData['Property'];	//产品属性名称
			$Price			= $AttrData['Price'];		//产品单价
			$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
			$OvId			= $AttrData['OvId'];		//发货地ID
			$ColorId		= $AttrData['ColorId'];		//颜色ID
			$Weight			= $AttrData['Weight'];		//产品重量
			$SKU			= $AttrData['SKU'];			//产品SKU
			$Attr			= $AttrData['Attr'];		//产品属性数据
			if($cart_row['BuyType']==1){//团购产品
				$tuan_row=str::str_code(db::get_one('sales_tuan', "TId='{$cart_row['KeyId']}' and BuyerCount<TotalCount and {$c['time']} between StartTime and EndTime"));
			}
			if($cart_row['BuyType']==2){//秒杀产品
				$seckill_row=str::str_code(db::get_one('sales_seckill', "SId='{$cart_row['KeyId']}' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
			}
			//检查库存
			$Qty=cart::check_product_stock(array(
				'CId'		=> $p_CId,
				'IsStock'	=> $IsStock,
				'BuyType'	=> $cart_row['BuyType'],
				'Qty'		=> $Qty,
				'ProdRow'	=> $products_row,
				'Combinatin'=> $combinatin_ary,
				'Seckill'	=> $seckill_row,
				'Tuan'		=> $tuan_row
			));
			if($Qty===false) ly200::e_json('', 0);
			//价格
			if($cart_row['BuyType']==0){ //普通产品，重新计算价格，开始判断批发价和促销价
				// if($products_row['IsOpenAttrPrice']==0 || !$combinatin_ary || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				if((!$products_row['IsOpenAttrPrice'] && !$products_row['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $products_row['IsOpenAttrPrice']==1 && !$products_row['IsCombination']) || ($combinatin_ary && $products_row['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$Price=cart::products_add_to_cart_price($products_row, $Qty);
				}else{ //属性类型是“单价”
					$Price=cart::products_add_to_cart_price($products_row, $Qty, (float)$combinatin_ary[0]);
				}
			}
			//数据
			$PropertyData=addslashes($Property);
			$Attr=str_replace('_', '', str::json_data(str::str_code($Attr, 'stripslashes')));
			$data=array(
				'Price'			=>	(float)$Price,
				'Qty'			=>	$Qty,
				'Weight'		=>	(float)$Weight,
				'Property'		=>	$PropertyData,
				'PropertyPrice'	=>	$PropertyPrice,
				'SKU'			=>	$SKU,
				'Attr'			=>	$Attr,
				'OvId'			=>	$OvId
			);
			//颜色属性产品图片
			if($ColorId){
				$Path=db::get_value('products_color', "ProId='{$p_ProId}' and VId='{$ColorId}'", 'PicPath_0');
				@is_file($c['root_path'].$Path) && $data['PicPath']=$Path;
			}
			//更新
			db::update('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'", $data);
			$IsWholesale=0;//是不是混批产品
			if($cart_row['BuyType']==0){//检查产品混批功能
				$wholesale_price=(float)cart::update_cart_wholesale_price($p_ProId, $products_row, $p_CId);//检查产品混批功能
				if($wholesale_price){
					$Price=$wholesale_price;
					$IsWholesale=1;
				}
			}
			if($data['PicPath']) $data['PicPath']=ly200::get_size_img($Path, '240x240');
			//返回数据
			$data['Property']=$Property;//还原到格式化之前的数据
			//统计产品的改动单价计算
			$data['itemprice']=$data['amount']=array();
			$pro_list_row=db::get_all('shopping_cart', "{$c['where']['cart']} and ProId='{$p_ProId}'".($IsWholesale==0?" and CId='{$p_CId}'":''), 'CId, Price, PropertyPrice, Discount, Qty');
			foreach($pro_list_row as $k=>$v){
				$_p=cart::iconv_price(($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1), 2, '', 0);
				$data['itemprice'][$v['CId']]=$_p;
				$data['amount'][$v['CId']]=$_p*$v['Qty'];
			}
			$data['qty']=$Qty;
			$p_CIdAry=str::ary_format($p_CIdAry, 2);
			$tWhere=$c['where']['cart'].($IsBuyNow==1?" and CId='{$p_CId}'":'').($p_CIdAry?" and CId in({$p_CIdAry})":'');
			$data['total_weight']=(float)db::get_sum('shopping_cart', $tWhere." and ProId='{$p_ProId}'", 'Weight*Qty');
			if(!$p_CIdAry){//一个列表产品都没有勾选
				$data['total_count']=$data['total_price']=$data['iconv_total_price']=0;
			}else{//有勾选产品
				$data['total_count']=(int)db::get_sum('shopping_cart', $tWhere, 'Qty');
                $data['total_price']=(float)db::get_sum('shopping_cart', $tWhere, '(Price+PropertyPrice)*Discount/100*Qty');
				$data['iconv_total_price']=(float)cart::cart_total_price(($IsBuyNow==1?" and c.CId='{$p_CId}'":'').($p_CIdAry?" and c.CId in({$p_CIdAry})":''), 1);
			}
			$DiscountData=cart::check_list_discounts($data['total_price'], $data['iconv_total_price']);//检查购物车列表的总价格变动后，优惠情况的变动
			$data['cutprice']=$DiscountData['cutprice'];
			$data['IsCoupon']=$DiscountData['IsCoupon'];
            $data['iconv_total_price']=(float)cart::currency_float_price($data['iconv_total_price'], $_SESSION['Currency']['Currency']);
		}
		if($p_Remark!=''){
			db::update('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'", array('Remark'=>$p_Remark));
			$data['Remark']=$p_Remark;
			!count($p_Attr) && $result=2;
		}
		$data=array_merge($data, array('CId'=>$p_CId, 'ProId'=>$p_ProId));
		ly200::e_json($data, $result);
	}

	public static function remove(){	//remove item from shopping cart
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'p');
		$p_CId=(int)$p_CId;
		$ProId=(int)db::get_value('shopping_cart', "{$c['where']['cart']} and CId='$p_CId'", 'ProId');
		db::delete('shopping_cart', "{$c['where']['cart']} and CId='$p_CId'");
		$ProId && cart::update_cart_wholesale_price($ProId);//检查产品混批功能
		js::back();
	}

	public static function bacth_remove(){	//remove item from shopping cart
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		if($p_cid_list){
			$p_cid_list=str::ary_format($p_cid_list, 2);
			$rows=db::get_all('shopping_cart', "{$c['where']['cart']} and CId in($p_cid_list)", 'ProId');
			db::delete('shopping_cart', "{$c['where']['cart']} and CId in($p_cid_list)");
			$proid_ary=array();
			foreach($rows as $v){//检查产品混批功能
				if($v['ProId'] && (!count($proid_ary) || !@in_array($v['ProId'], $proid_ary))){
					cart::update_cart_wholesale_price($v['ProId']);
					$proid_ary[]=$v['ProId'];
				}
			}
			ly200::e_json('', 1);
		}
		ly200::e_json('');
	}
	
	public static function checkout_submit(){
		global $c;
		foreach((array)$_POST as $k=>$v){
			if((int)$k && $v){
				db::update('shopping_cart', "{$c['where']['cart']} and CId='{$k}'", array('Remark'=>addslashes($v)));
			}
		}
		ly200::e_json('', 1);
	}
	
	public static function check_low_consumption($BackType=0, $Price=0){ //1:返回数组, 0:返回JSON格式
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$w=$c['where']['cart'];
		if($p_CId){
			$p_CId=str::ary_format(@str_replace('.', ',', $p_CId), 2);
			$w.=" and CId in({$p_CId})";
		}
		if($_GET['t'] && $p_CId=='0'){//来自产品详细页的传入，不用检查
			// ly200::e_json(array(), 1);
			if($p_Attr){//产品属性
				$Attr=str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
				ksort($Attr);
			}
			$proInfo=db::get_one('products', "ProId='$p_ProId'");
			$StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;	//起订量
			$p_Qty<$StartFrom && $p_Qty=$StartFrom;	//小于起订量
			$CurPrice=cart::products_add_to_cart_price($proInfo, $p_Qty);
			$PropertyPrice=0;
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'ProId'			=> $p_ProId,
				'Price'			=> $CurPrice,
				'Attr'			=> $Attr,
				'IsCombination'	=> $proInfo['IsCombination'],
				'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice']
			));
			if($AttrData){
				$CurPrice		= $AttrData['Price'];		//产品单价
				$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			}
			//产品数据
			if($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime']<$c['time'] && $c['time']<$proInfo['EndTime']){
				$CurPrice=$CurPrice*($proInfo['PromotionDiscount']/100);
			}
			$total_price=($CurPrice+$PropertyPrice)*$p_Qty;
		}
		if($Price>0){//有传递产品总价格数据
			$ProductPrice=$Price;
		}else{
			$cartInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', $w, 'p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr', 's.CId desc');
			$ProductPrice=0;
			foreach($cartInfo as $val){
				$ProductPrice+=($val['Price']+$val['PropertyPrice'])*$val['Qty']*($val['Discount']<100?$val['Discount']/100:1);
			}
		}
		$DisData=cart::discount_contrast($ProductPrice);//“会员优惠”和“全场满减”之间的优惠对比
		$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
		$Discount		= $DisData['Discount'];			//全场满减的折扣
		$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
		//最低消费设置
		$difference=0;
		$ret=1;
		$result=array();
		$low_price=$c['config']['global']['LowPrice'];
		$_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)-$DiscountPrice;//订单折扣后的总价
		if($_GET['t'] && $p_CId=='0') $_total_price = $total_price;
		if((int)$c['config']['global']['LowConsumption'] && $_total_price<$low_price){
			$ret=0;
			$difference=$low_price-$_total_price;
			$result=array('s_total'=>cart::iconv_price($_total_price, 2, '', 0), 'low_price'=>cart::iconv_price($low_price, 2, '', 0), 'difference'=>cart::iconv_price($difference, 2, '', 0));
		}
		if($BackType==1){
            return $result;
		}else{
			ly200::e_json($result, $ret);
		}
	}

	public static function get_excheckout_country(){//get excheckout country
		global $c;
		@extract($_GET, EXTR_PREFIX_ALL, 'g');
		$products_ary=array();//产品数据
		if($g_Type=='shipping_cost'){//产品详细页
			$g_ProId=(int)$g_ProId;
			$g_Qty=(int)$g_Qty;
			$g_SId=(int)$g_SId;
			if($g_Attr){//产品属性
				$Attr=str::json_data(stripslashes($g_Attr), 'decode');
				ksort($Attr);
			}
			$proInfo=db::get_one('products', "ProId='$g_ProId'");
			$StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;//起订量
			$g_Qty<$StartFrom && $g_Qty=$StartFrom;//小于起订量
			$CurPrice=cart::products_add_to_cart_price($proInfo, $g_Qty);
			$Discount=100;//默认100% OFF折扣
			$IconvPropertyPrice=0;
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'BuyType'		=> $g_proType,
				'ProId'			=> $g_ProId,
				'Price'			=> $CurPrice,
				'Attr'			=> $Attr,
				'IsCombination'	=> $proInfo['IsCombination'],
				'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice'],
			));
			$CurPrice		= $AttrData['Price'];		//产品单价
			$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
			$OvId			= $AttrData['OvId'];		//发货地ID
			$Attr			= $AttrData['Attr'];		//产品属性数据
			//产品数据
			if($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime']<$c['time'] && $c['time']<$proInfo['EndTime']){
				$Discount=$proInfo['PromotionDiscount'];
			}
			if($g_proType==0){//普通产品，计算价格，开始判断批发价和促销价
				// if($proInfo['IsOpenAttrPrice']==0 || !$combinatin_ary || ($combinatin_ary && (int)$combinatin_ary[4]==1)){//没有属性，或者，属性类型是“加价”
				if((!$proInfo['IsOpenAttrPrice'] && !$proInfo['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $proInfo['IsOpenAttrPrice']==1 && !$proInfo['IsCombination']) || ($combinatin_ary && $proInfo['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$CurPrice=cart::products_add_to_cart_price($proInfo, $g_Qty);
				}else{//属性类型是“单价”
					$CurPrice=cart::products_add_to_cart_price($proInfo, $g_Qty, (float)$combinatin_ary[0]);
				}
			}
			if($g_proType==2){//秒杀产品
				$seckill_row=str::str_code(db::get_one('sales_seckill', "SId='$g_SId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
				if($seckill_row){
					$CurPrice=$seckill_row['Price'];
					$Discount=100;
				}
			}
			$PicPath=ly200::get_size_img($proInfo['PicPath_0'], '240x240');
			if($ColorId){
				$Path=db::get_value('products_color', "ProId='{$g_ProId}' and VId='{$AttrData['ColorId']}'", 'PicPath_0');
				@is_file($c['root_path'].$Path) && $PicPath=ly200::get_size_img($Path, '240x240');
			}
			$ProductsPrice=($CurPrice+$PropertyPrice)*$Discount/100*$g_Qty;
			$IconvPropertyPrice=cart::iconv_price(($CurPrice+$PropertyPrice)*$Discount/100, 2, '', 0)*$g_Qty;
			$OvId_where="a.OvId='$OvId'";
			$products_ary[]=array(
				'Name'	=>	$proInfo['Name'.$c['lang']],
				'Price'	=>	($CurPrice+$PropertyPrice)*$Discount/100,
				'Qty'	=>	$g_Qty,
				'CId'	=>	$OvId,
				'OvId'	=>	$OvId,
				'PicPath'	=>	$PicPath,
			);
		}else{//购物车
			$where='';
			if($g_CId){
				$g_CId=str::ary_format($g_CId, 2);
				$where.=" and CId in({$g_CId})";
			}
			$ProductsPrice=db::get_sum('shopping_cart', $c['where']['cart'].$where, '(Price+PropertyPrice)*Discount/100*Qty');//购物车产品总价
			$IconvPropertyPrice=cart::cart_total_price($where, 1);
			$cart_row=db::get_all('shopping_cart c left join products p on c.ProId=p.ProId', $c['where']['cart'].$where, "c.OvId, p.Name{$c['lang']}, c.PicPath, c.Price, c.PropertyPrice, c.Discount, c.Qty, c.CId", 'c.CId desc');
			$OvId_where='a.OvId in(-1';
			foreach($cart_row as $v){
				$products_ary[]=array(
					'Name'	=>	$v['Name'.$c['lang']],
					'Price'	=>	($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1),
					'Qty'	=>	$v['Qty'],
					'CId'	=>	$v['CId'],
					'OvId'	=>	$v['OvId'],
					'PicPath'	=>	ly200::get_size_img($v['PicPath'], '240x240'),
				);
				$OvId_where.=",{$v['OvId']}";
			}
			$OvId_where.=')';
		}
		//生成临时订单号
		while(1){
			$OId=date('ymdHis', $c['time']).mt_rand(10,99);
			if(!db::get_row_count('orders', "OId='$OId'")){
				break;
			}
		}
		//相关数据
		$hot_country_len=0;//记录热门国家的总数
		$country_data=$country_ary=$hot_country_data=array();
		$data=db::get_all('country', 'IsUsed=1', 'CId,Country,Acronym,FlagPath,IsDefault,CountryData,IsHot', 'IsHot desc, Country asc');
		$shipping_country_row=db::get_all('shipping_area a left join shipping_country c on a.AId=c.AId', $OvId_where.' Group By c.CId', 'c.CId');
		foreach($shipping_country_row as $v){ $country_ary[]=$v['CId']; }
		foreach($data as $k=>$v){//所有快递方式都没有的国家，给过滤掉
			if(in_array($v['CId'], $country_ary)){
				$country_data[]=$v;
				if($v['IsHot']){
					$hot_country_data[]=$v;
					$hot_country_len+=1;
				}
			}
		}
		if($hot_country_len<10) $hot_country_data=array();//热门国家总数在10个以上才显示 
		$DisData=cart::discount_contrast($ProductsPrice);//“会员优惠”和“全场满减”之间的优惠对比
		$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
		$Discount		= $DisData['Discount'];			//全场满减的折扣
		$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
		$total			= $ProductsPrice;//*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100);
		//优惠券优惠
		$coupon=$_SESSION['Cart']['Coupon'];
		$couponData=array('coupon'=>'', 'cutprice'=>0);
		if($coupon!='' || $g_Type=='shipping_cost'){
			$info=self::get_coupon_info($coupon, cart::iconv_price($ProductsPrice, 2, '', 0), (int)$_SESSION['User']['UserId'], $g_CId, $g_ProId);
			if($info['status']==1){
				$coupon_total=$total;
				if($info['pro_price']){
					$coupon_total=$info['pro_price'];//*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100);
				}
				$_SESSION['Cart']['Coupon']=$info['coupon'];
				$cutPrice=$info['type']==1?$info['cutprice']:($coupon_total*(100-$info['discount'])/100);//CouponType: [0, 打折] [1, 减价格]
				$info['cutprice']=cart::iconv_price($cutPrice, 2, '', 0);
				$info['end']=@date('m/d/Y', $info['end']);
				$couponData=$info;
			}
		}
		//海外仓
		if($g_Type=='shipping_cost'){//产品详细页海外仓情况
			$oversea_id_ary[$OvId]=$c['config']['Overseas'][$OvId]['Name'.$c['lang']];
		}else{//购物车海外仓情况
			$oversea_id_ary=array();
			foreach($cart_row as $v){
				!$oversea_id_ary[$v['OvId']] && $oversea_id_ary[$v['OvId']]=$c['config']['Overseas'][$v['OvId']]['Name'.$c['lang']];
			}
			ksort($oversea_id_ary); //排列正序
		}
		//检查产品的货币小数点和汇率
		foreach($products_ary as $k=>$v){
			$v['Price']=cart::iconv_price($v['Price'], 2, '', 0);
			$v['Price']=cart::currency_float_price($v['Price'], $_SESSION['Currency']['Currency']);//检查是否需要取整
			$products_ary[$k]['Price']=$v['Price'];
		}
		if($Discount || $UserDiscount){
			$total=$ProductsPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100);
		}
		$DiscountPrice==0 && $DiscountPrice=$ProductsPrice-$total;
		$DiscountPrice=cart::iconv_price($DiscountPrice, 2, '', 0);
		$IsCreditCard=(int)db::get_value('payment', 'IsUsed=1 and Method="Excheckout"', 'IsCreditCard');//是否开启信用卡支付
		$AdditionalFee=db::get_value('payment', 'IsUsed=1 and Method="Excheckout"', 'AdditionalFee');//手续费
		$IconvPropertyPrice=cart::currency_float_price($IconvPropertyPrice, $_SESSION['Currency']['Currency']);//检查是否需要取整
		$DiscountPrice=cart::currency_float_price($DiscountPrice, $_SESSION['Currency']['Currency']);//检查是否需要取整
		$result=array(
			'country'			=>	$country_data,
			'hot_country'		=>	$hot_country_data,
			'v'					=>	(int)$c['FunVersion'],
			'coupon'			=>	$couponData,
			'oversea'			=>	$oversea_id_ary,
			'CartProductPrice'	=>	$IconvPropertyPrice,
			'DiscountPrice'		=>	$DiscountPrice,
			'IsCreditCard'		=>	$IsCreditCard,
			'AdditionalFee'		=>	$AdditionalFee,
			'Item'				=>	$products_ary,
			'NewFunVersion'		=>	$c['NewFunVersion'],
			'OId'				=>	$OId,
			'shipping_template'	=>	@in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0,
		);
		ly200::e_json($result, 1);
	}

	public static function set_no_login_address(){//非会员购物地址设置
		$data=array();
		$info='';
		foreach($_POST as $k=>$v){
			if($k=='edit_address_id') continue;
			$k=='tax_code_type' && $k='CodeOption';
			$k=='tax_code_value' && $k='TaxCode';
			if($k=='country_id'){
				$k='CId';
				$v=(int)$v;
				$data['Country']=db::get_value('country', "CId='{$v}'", 'Country');
			}
			if($k=='Province'){
				$v=(int)$v;
				$CId=(int)$_POST['country_id'];
				$data['StateName']=db::get_value('country_states', "CId='{$CId}' and SId='{$v}'", 'States');
			}
			$data[$k]=$v;
			$info.="&{$k}=$v";
		}
        $_SESSION['Cart']['ShippingAddress']=$data;
        $_SESSION['Cart']['ShippingAddress']['SId']=$data['Province'];
        $data['SId'] && $_SESSION['Cart']['ShippingAddress']['SId']=$data['SId'];
        $_SESSION['Cart']['ShippingAddress']['country_id']=$data['CId'];
        $_SESSION['Cart']['ShippingAddress']['tax_code_type']=$data['CodeOption'];
        $_SESSION['Cart']['ShippingAddress']['tax_code_value']=$data['TaxCode'];
        $_SESSION['Cart']['ShippingAddress']['typeAddr']=1;
		ly200::e_json(array('info'=>$info, 'v'=>$data), 1);
	}
	public static function get_shipping_methods_by_proId($CId,$ProId,$AId)
	{
	    $_POST=array( 'CId' =>'223',
	        'AId' => '25',
	        'ProId' => "420",
	    );
	    $info=cart_module::get_shipping_methods();
//	    file_put_contents("t1.txt",var_export($info,true),FILE_APPEND);
	    return $info;
	 
	}
	public static function get_shipping_methods(){	//get shipping methods
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
//		file_put_contents("t.txt",var_export($_POST,true),FILE_APPEND);
		$p_CId=(int)$p_CId;
		$p_AId=(int)$p_AId;
		$p_ProId=(int)$p_ProId;
		$p_Qty=(int)$p_Qty;
		$info=array();
		$IsFreeShipping=$ProductPrice=0;
		$shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0;
		$provincial_freight=@in_array('provincial_freight', (array)$c['plugins']['Used']) ? 1 : 0; //开启省份运费
		$pro_info_ary=array();
		if($p_Type=='order'){
			$order_row=db::get_one('orders', "OId='{$p_OId}'");
			$sProdInfo=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, p.IsFreeShipping, p.TId');
			foreach($sProdInfo as $v){
				$price=($v['Price']+$v['PropertyPrice'])*$v['Qty']*($v['Discount']<100?$v['Discount']/100:1);
				$ProductPrice+=$price;
				if($shipping_template){
					if(!$pro_info_ary[$v['CId']]){
						$pro_info_ary[$v['CId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$v['OvId']);
					}
					$pro_info_ary[$v['CId']]['tWeight']=($v['Weight']*$v['Qty']);
					$pro_info_ary[$v['CId']]['tVolume']=($v['Volume']*$v['Qty']);
					$pro_info_ary[$v['CId']]['tQty']=$v['Qty'];
					$pro_info_ary[$v['CId']]['Price']=$price;
					$pro_info_ary[$v['CId']]['TId']=$v['TId'];
					if((int)$v['IsFreeShipping']==1){//免运费
						$pro_info_ary[$v['CId']]['IsFreeShipping']=1; //其中有免运费
					}else{
						$pro_info_ary[$v['CId']]['Weight']=($v['Weight']*$v['Qty']);
						$pro_info_ary[$v['CId']]['Volume']=($v['Volume']*$v['Qty']);
						$pro_info_ary[$v['CId']]['Qty']=$v['Qty'];
					}
				}else{
					if(!$pro_info_ary[$v['OvId']]){
						$pro_info_ary[$v['OvId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
					}
					$pro_info_ary[$v['OvId']]['tWeight']+=($v['Weight']*$v['Qty']);
					$pro_info_ary[$v['OvId']]['tVolume']+=($v['Volume']*$v['Qty']);
					$pro_info_ary[$v['OvId']]['tQty']+=$v['Qty'];
					$pro_info_ary[$v['OvId']]['Price']+=$price;
					if((int)$v['IsFreeShipping']==1){//免运费
						$pro_info_ary[$v['OvId']]['IsFreeShipping']=1; //其中有免运费
					}else{
						$pro_info_ary[$v['OvId']]['Weight']+=($v['Weight']*$v['Qty']);
						$pro_info_ary[$v['OvId']]['Volume']+=($v['Volume']*$v['Qty']);
						$pro_info_ary[$v['OvId']]['Qty']+=$v['Qty'];
					}
				}
			}
		}elseif($p_Type=='shipping_cost'){
			if($p_Attr){//产品属性
				$Attr=str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
				ksort($Attr);
			}
			$proInfo=db::get_one('products', "ProId='$p_ProId'");
			$Weight=$proInfo['Weight'];//产品默认重量
			$volume_ary=$proInfo['Cubage']?@explode(',', $proInfo['Cubage']):array(0,0,0);
			$Volume=$volume_ary[0]*$volume_ary[1]*$volume_ary[2];
			$Volume=sprintf('%.f', $Volume);//防止数额太大，程序自动转成科学计数法
			if((int)$proInfo['IsVolumeWeight']){//体积重
				$VolumeWeight=($Volume*1000000)/5000;//先把立方米转成立方厘米，再除以5000
				$VolumeWeight>$Weight && $Weight=$VolumeWeight;
			}
			$StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;	//起订量
			$p_Qty<$StartFrom && $p_Qty=$StartFrom;	//小于起订量
			$CurPrice=cart::products_add_to_cart_price($proInfo, $p_Qty);
			$PropertyPrice=0;
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'ProId'			=> $p_ProId,
				'Price'			=> $CurPrice,
				'Attr'			=> $Attr,
				'IsCombination'	=> $proInfo['IsCombination'],
				'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice'],
				'Weight'		=> $Weight
			));
			$CurPrice		= $AttrData['Price'];		//产品单价
			$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
			$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
			$OvId			= $AttrData['OvId'];		//发货地ID
			$Weight			= $AttrData['Weight'];		//产品重量
			$Attr			= $AttrData['Attr'];		//产品属性数据
			//秒杀产品
			if($p_proType==2){
				$seckill_row=str::str_code(db::get_one('sales_seckill', "SId='$p_SId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
				if($seckill_row){
					$CurPrice=$seckill_row['Price'];
				}
			}
			//产品数据
			if($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime']<$c['time'] && $c['time']<$proInfo['EndTime']){
				$CurPrice=$CurPrice*($proInfo['PromotionDiscount']/100);
			}
			$total_price=($CurPrice+$PropertyPrice)*$p_Qty;
			$IsFreeShipping=(int)$proInfo['IsFreeShipping'];
			$total_volume=$Volume*$p_Qty;
			$total_weight=$Weight*$p_Qty;
			$s_total_weight=$total_weight;
			$s_total_volume=$total_volume;
			//产品的海外仓数据
			$pro_info_ary[$OvId]['Weight']=$pro_info_ary[$OvId]['tWeight']=$total_weight;
			$pro_info_ary[$OvId]['Volume']=$pro_info_ary[$OvId]['tVolume']=$total_volume;
			$pro_info_ary[$OvId]['Price']=$total_price;
			$pro_info_ary[$OvId]['Qty']=$p_Qty;
			$pro_info_ary[$OvId]['TId']=(int)$proInfo['TId'];
			$ProductPrice+=$total_price;
			if($IsFreeShipping==1){
				$pro_info_ary[$OvId]['Weight']=$pro_info_ary[$OvId]['Volume']=0;
			}
		}else{
			$CIdStr='';
			if($p_order_cid){
				$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
				$CIdStr=" and CId in({$in_where})";
			}
//			file_put_contents("t.txt","p_order_cid".var_export($p_order_cid,true).PHP_EOL.PHP_EOL,FILE_APPEND);
			$cartInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.'.$c['where']['cart'].$CIdStr, 'p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr', 's.CId desc');
			$IsFreeShipping=0;
			$total_weight=$total_volume=0;
//			file_put_contents("t.txt","cartInfo".var_export($cartInfo,true).PHP_EOL.PHP_EOL,FILE_APPEND);
			foreach($cartInfo as $val){
				$CartId=(int)$val['CId'];
				$OvId=1;
				$CartAttr=str::json_data(stripslashes($val['CartAttr']), 'decode');
				if(count($CartAttr)){ //include attribute values
					foreach((array)$CartAttr as $k=>$v){
						if($k=='Overseas'){ //发货地
							$OvId=str_replace('Ov:', '', $v);
							!$OvId && $OvId=1;//丢失发货地，自动默认China
						}
					}
				}
				$price=($val['Price']+$val['PropertyPrice'])*$val['Qty']*($val['Discount']<100?$val['Discount']/100:1);
				if($shipping_template){
					if(!$pro_info_ary[$CartId]){
						$pro_info_ary[$CartId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$OvId);
					}
					$pro_info_ary[$CartId]['tWeight']=($val['CartWeight']*$val['Qty']);
					$pro_info_ary[$CartId]['tVolume']=($val['Volume']*$val['Qty']);
					$pro_info_ary[$CartId]['tQty']=$val['Qty'];
					$pro_info_ary[$CartId]['Price']=$price;
					$pro_info_ary[$CartId]['TId']=(int)$val['TId'];
					if((int)$val['IsFreeShipping']==1){//免运费
						$pro_info_ary[$CartId]['IsFreeShipping']=1; //其中有免运费
					}else{
						$pro_info_ary[$CartId]['Weight']=($val['CartWeight']*$val['Qty']);
						$pro_info_ary[$CartId]['Volume']=($val['Volume']*$val['Qty']);
						$pro_info_ary[$CartId]['Qty']=$val['Qty'];
					}
				}else{
					if(!$pro_info_ary[$OvId]){
						$pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
					}
					$pro_info_ary[$OvId]['tWeight']+=($val['CartWeight']*$val['Qty']);
					$pro_info_ary[$OvId]['tVolume']+=($val['Volume']*$val['Qty']);
					$pro_info_ary[$OvId]['tQty']+=$val['Qty'];
					$pro_info_ary[$OvId]['Price']+=$price;
					if((int)$val['IsFreeShipping']==1){//免运费
						$pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
					}else{
						$pro_info_ary[$OvId]['Weight']+=($val['CartWeight']*$val['Qty']);
						$pro_info_ary[$OvId]['Volume']+=($val['Volume']*$val['Qty']);
						$pro_info_ary[$OvId]['Qty']+=$val['Qty'];
					}
				}
				$total_weight+=($val['CartWeight']*$val['Qty']);
				$total_volume+=($val['Volume']*$val['Qty']);
				$ProductPrice+=$price;
			}
			/*
			//产品包装重量  //没有多少客户用 4.0先注释
			$cartProAry=cart::cart_product_weight($CIdStr, 1);
			foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
				foreach((array)$v as $k2=>$v2){//$k2是ProId
					$pro_info_ary[$k]['tWeight']+=$v2;
				}
			}
			foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
				foreach((array)$v as $k2=>$v2){//$k2是ProId
					$pro_info_ary[$k]['Weight']+=$v2;
				}
			}*/
			$total_weight=$pro_info_ary[$OvId]['Weight'];
			$total_volume=$pro_info_ary[$OvId]['tVolume'];
		}
//		file_put_contents("t.txt",'pro_info_ary'.var_export($pro_info_ary,true),FILE_APPEND);
		ksort($pro_info_ary); //排列正序
		$shipping_cfg=db::get_one('shipping_config', 'Id="1"');
		$weight=@ceil($total_weight);
		if($_SESSION['User']['UserId']){			
			if($p_AId){
				$address_aid=$p_AId;
			}else{
				$address_aid=(int)$_SESSION['Cart']['ShippingAddressAId'];
			}
			if($address_aid>0) $StatesSId=(int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
		}else{			
			$StatesSId=(int)$_SESSION['Cart']['ShippingAddress']['Province'];
		}
		$StatesSId=$p_StatesSId?(int)$p_StatesSId:$StatesSId;
		$provincial_freight && $p_CId==226 && $StatesSId && $area_where=" and states like '%|{$StatesSId}|%'";
		$IsInsurance=str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
		$row=db::get_all('shipping_area a left join shipping s on a.SId=s.SId', "a.AId in(select AId from shipping_country where CId='{$p_CId}' {$area_where})", 's.Express, s.Logo, s.IsWeightArea, s.WeightArea, s.ExtWeightArea, s.VolumeArea, s.IsUsed, s.IsAPI, s.FirstWeight, s.ExtWeight, s.StartWeight, s.MinWeight, s.MaxWeight, s.MinVolume, s.MaxVolume, s.FirstMinQty, s.FirstMaxQty, s.ExtQty, s.WeightType, s.TId, a.*', 'if(s.MyOrder>0, s.MyOrder, 100000) asc, a.SId asc, a.AId asc');
		//添加获取清关费费率 所有id
		$qg_rows=array();
		foreach ((array)$row as $value) {
		    $r=array();
		    $r["SId"]=$value["SId"];
		    $r["AffixPrice_rate"]=$value["AffixPrice_rate"]?$value["AffixPrice_rate"]:0;
		    $qg_rows[]=$r;
		}
		//-------------------------------------
		$shipping_row=db::get_all('shipping', '1', 'SId, TId');
		$row_ary=$shipping_tid_ary=array();
		foreach((array)$row as $v){
			!$row_ary[$v['SId']] && $row_ary[$v['SId']]=array('info'=>$v, 'overseas'=>array());
			$row_ary[$v['SId']]['overseas'][$v['OvId']]=$v;
		}
		$shipping_template && $DefaultTId=db::get_value('shipping_template', 'IsDefault=1', 'TId');
		foreach((array)$shipping_row as $v){
			if($shipping_template) $shipping_tid_ary[$v['TId']][]=$v['SId'];
		}
		unset($row);
		foreach((array)$row_ary as $key=>$val){
			$row=$val['info'];
			$isOvId=0;
			foreach((array)$pro_info_ary as $k=>$v){ 
				if($shipping_template && $p_Type!='shipping_cost'){
					$val['overseas'][$v['OvId']] && $isOvId+=1;
				}else{
					$val['overseas'][$k] && $isOvId+=1;
				}
			}//循环产品数据
			if($isOvId==0 && $pro_info_ary['1']){
				$info[1][]=array('SId'=>'', 'Name'=>'', 'Brief'=>'', 'IsAPI'=>'', 'type'=>'', 'ShippingPrice'=>'-1');
				continue;
			}
			/************************************** 新增折后之后计算运费 开始**********************************/
			$_total_price=orders::orders_discount_total_price($ProductPrice, $_SESSION['Cart']['Coupon'], $_SESSION['User']['UserId'], $p_order_cid, $p_ProId);
			$pro_info_ary=orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
			/************************************** 新增折后之后计算运费 结束**********************************/
			//循环产品数据 Start
			foreach((array)$pro_info_ary as $k=>$v){
				$overseas=$val['overseas'][$k];
				$shipping_template && $p_Type!='shipping_cost' && $overseas=$val['overseas'][$v['OvId']];
				if($shipping_template && (!in_array($key, (array)$shipping_tid_ary[$v['TId']]) || ($DefaultTId && !$shipping_tid_ary[$v['TId']] && !in_array($key, (array)$shipping_tid_ary[$DefaultTId])))) continue;
				$open=0;//默认不通过
				if(in_array($row['IsWeightArea'], array(0,1,2)) && ((float)$row['MaxWeight']?($v['tWeight']>=$row['MinWeight'] && $v['tWeight']<=$row['MaxWeight']):($v['tWeight']>=$row['MinWeight']))){//重量限制
					$open=1;
				}elseif($row['IsWeightArea']==4 && ($v['tWeight']>=$row['MinWeight'] || $v['tVolume']>=$row['MinVolume'])){//重量限制+体积限制
					$open=1;
				}elseif($row['IsWeightArea']==3){//按数量计算，直接不限制
					$open=1;
				}
				if($overseas && (int)$row['IsUsed']==1 && $open==1){
					$sv=array(
						'SId'		=>	$row['SId'],
						'Name'		=>	$overseas['Express'],
						'Logo'		=>	($overseas['Logo'] && is_file($c['root_path'].$overseas['Logo']))?$overseas['Logo']:'',
						'Brief'		=>	$overseas['Brief'],
						'IsAPI'		=>	$overseas['IsAPI'],
						'type'		=>	'',
						'weight'	=>	$v['Weight']
					);
					if($IsFreeShipping || ($v['IsFreeShipping']==1 && $v['Weight']==0) || ((int)$c['config']['products_show']['Config']['freeshipping'] && $v['Weight']==0) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']>0 && $v['Price']>=$overseas['FreeShippingPrice']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingWeight']>0 && $v['Weight']<$overseas['FreeShippingWeight']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']==0 && $overseas['FreeShippingWeight']==0)){
						$shipping_price=0;
					}else{
						$shipping_price=0;
						if($overseas['IsWeightArea']==1 || ($overseas['IsWeightArea']==2 && $v['Weight']>=$overseas['StartWeight'])){
							//重量区间 重量混合
							$WeightArea=str::json_data($overseas['WeightArea'], 'decode');
							$WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
							$areaCount=count($WeightArea)-1;
							foreach((array)$WeightArea as $k2=>$v2){
								if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
									if($overseas['WeightType']==1){//按每KG计算
										$shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
									}else{//按整价计算
										$shipping_price=$WeightAreaPrice[$k2];
									}
									break;
								}
							}
							//$v['Weight']>$WeightArea[$areaCount] && $shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
						}elseif($overseas['IsWeightArea']==3){
							//按数量
							$shipping_price=$overseas['FirstQtyPrice'];//先收取首重费用
							$ExtQtyValue=$v['Qty']>$overseas['FirstMaxQty']?$v['Qty']-$overseas['FirstMaxQty']:0;//超出的数量
							if($ExtQtyValue){//续重
								$shipping_price+=(float)(@ceil($ExtQtyValue/$overseas['ExtQty'])*$overseas['ExtQtyPrice']);
							}
						}elseif($overseas['IsWeightArea']==4){
							//重量体积混合计算
							$weight_shipping_price=$volume_shipping_price=0;
							if($v['Weight']>=$overseas['MinWeight']){//重量
								$WeightArea=str::json_data($overseas['WeightArea'], 'decode');
								$WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
								$areaCount=count($WeightArea)-1;
								foreach((array)$WeightArea as $k2=>$v2){
									if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
										if($overseas['WeightType']==1){//按每KG计算
											$weight_shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
										}else{//按整价计算
											$weight_shipping_price=$WeightAreaPrice[$k2];
										}
										break;
									}
								}
								$overseas['WeightType']==1 && $v['Weight']>$WeightArea[$areaCount] && $weight_shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
							}
							if($v['Volume']>=$overseas['MinVolume']){//体积
								$VolumeArea=str::json_data($overseas['VolumeArea'], 'decode');
								$VolumeAreaPrice=str::json_data($overseas['VolumeAreaPrice'], 'decode');
								$areaCount=count($VolumeArea)-1;
								foreach((array)$VolumeArea as $k2=>$v2){
									if($k2<=$areaCount && (($VolumeArea[$k2+1] && $v['Volume']<$VolumeArea[$k2+1]) || (!$VolumeArea[$k2+1] && $v['Volume']>=$v2))){
										$volume_shipping_price=$VolumeAreaPrice[$k2]*$v['Volume'];
										break;
									}
								}
								$v['Volume']>$VolumeArea[$areaCount] && $volume_shipping_price=$VolumeAreaPrice[$areaCount]*$v['Volume'];
							}
							$shipping_price=max($weight_shipping_price, $volume_shipping_price);
						}else{
							//首重续重
							$ExtWeightArea=str::json_data($overseas['ExtWeightArea'], 'decode');
							$ExtWeightAreaPrice=str::json_data($overseas['ExtWeightAreaPrice'], 'decode');
							$areaCount=count($ExtWeightArea)-1;
							$ExtWeightValue=$v['Weight']>$overseas['FirstWeight']?$v['Weight']-$overseas['FirstWeight']:0;//超出的重量
							if($areaCount>0){
								$shipping_price=$overseas['FirstPrice'];//先收取首重费用
								foreach((array)$ExtWeightArea as $k2=>$v2){
									if($v['Weight']>$v2 && $ExtWeightArea[$k2+1]){
										$ext=$v['Weight']>$ExtWeightArea[$k2+1]?($ExtWeightArea[$k2+1]-$v2):($v['Weight']-$v2);
										$shipping_price+=(float)(@ceil(sprintf('%01.4f', $ext/$overseas['ExtWeight']))*$ExtWeightAreaPrice[$k2]);
									}elseif($v['Weight']>$v2 && !$ExtWeightArea[$k2+1]){//达到以上费用
										$ext=$v['Weight']-$v2;
										$shipping_price+=(float)(@ceil(sprintf('%01.4f', $ext/$overseas['ExtWeight']))*$ExtWeightAreaPrice[$k2]);
									}
								}
							}else{
								$ExtWeightCount = sprintf('%01.4f', $ExtWeightValue/$overseas['ExtWeight']);
								$ExtWeightCountdecimal = floor($ExtWeightCount);
								if(strstr($ExtWeightCount,'.')!=false && ($ExtWeightCount-$ExtWeightCountdecimal)>0){
									$ExtWeightCount = @ceil($ExtWeightCount);
								}
								$shipping_price=(float)($ExtWeightCount*$ExtWeightAreaPrice[0]+$overseas['FirstPrice']);
							}
						}
						if($overseas['AffixPrice']){//附加费用
							$shipping_price+=$overseas['AffixPrice'];
						}
					}
					$sv['ShippingPrice']=cart::iconv_price($shipping_price, 2, '', 0);
					$sv['InsurancePrice']=cart::get_insurance_price_by_price($v['Price']);
					$info[$k][]=$sv;
				}
			}
			//循环产品数据 End
		}

		if($info){
//		    file_put_contents("t.txt",var_export($info,true),FILE_APPEND);
			$info_ary=array();
			foreach((array)$info as $k=>$v){
				$sort_ary=$price_ary=array();
				foreach((array)$v as $k2=>$v2){ //
					$price_ary[$v2['ShippingPrice']][]=$k2;
				}
				ksort($price_ary);
				foreach((array)$price_ary as $k2=>$v2){
					foreach((array)$v2 as $k3=>$v3){
						$info_ary[$k][]=$info[$k][$v3];
					}
				}
			}
			//添加清关费到返回值
			foreach ((array)$info_ary as $k=>&$v) {
			    foreach ((array)$v as $k2=>&$v2) { //
			        foreach ($qg_rows as $q_k=>$q_v)
			        {
			            if ($v2["SId"]==$q_v['SId'])
			            {
			                $info_ary[$k][$k2]["AffixPrice_rate"]=$q_v["AffixPrice_rate"];
			                break;
			            }
			        }
			    }
			}
			//-------------
			ly200::e_json(array('Symbol'=>cart::iconv_price(0, 1), 'info'=>$info_ary, 'IsInsurance'=>$IsInsurance, 'total_weight'=>$total_weight, 'shipping_template'=>(int)$shipping_template), 1);
		}else{
			ly200::e_json('', 0);
		}
	}
	public static function get_shipping_methods_v2(){	//get shipping methods and filer 
	    global $c;
	    @extract($_POST, EXTR_PREFIX_ALL, 'p');
	    $p_Type="shipping_cost";
	    //		file_put_contents("t.txt",var_export($_POST,true),FILE_APPEND);Type=shipping_cost
	    $p_CId=(int)$p_CId;
	    $p_AId=(int)$p_AId;
	    $p_ProId=(int)$p_ProId;
	    $p_Qty=(int)$p_Qty;
	    $info=array();
	    $IsFreeShipping=$ProductPrice=0;
	    $shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used']) ? 1 : 0;
	    $provincial_freight=@in_array('provincial_freight', (array)$c['plugins']['Used']) ? 1 : 0; //开启省份运费
	    $pro_info_ary=array();
	    if($p_Type=='order'){
	        $order_row=db::get_one('orders', "OId='{$p_OId}'");
	        $sProdInfo=db::get_all('orders_products_list o left join products p on o.ProId=p.ProId', "o.OrderId='{$order_row['OrderId']}'", 'o.*, p.IsFreeShipping, p.TId');
	        foreach($sProdInfo as $v){
	            $price=($v['Price']+$v['PropertyPrice'])*$v['Qty']*($v['Discount']<100?$v['Discount']/100:1);
	            $ProductPrice+=$price;
	            if($shipping_template){
	                if(!$pro_info_ary[$v['CId']]){
	                    $pro_info_ary[$v['CId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$v['OvId']);
	                }
	                $pro_info_ary[$v['CId']]['tWeight']=($v['Weight']*$v['Qty']);
	                $pro_info_ary[$v['CId']]['tVolume']=($v['Volume']*$v['Qty']);
	                $pro_info_ary[$v['CId']]['tQty']=$v['Qty'];
	                $pro_info_ary[$v['CId']]['Price']=$price;
	                $pro_info_ary[$v['CId']]['TId']=$v['TId'];
	                if((int)$v['IsFreeShipping']==1){//免运费
	                    $pro_info_ary[$v['CId']]['IsFreeShipping']=1; //其中有免运费
	                }else{
	                    $pro_info_ary[$v['CId']]['Weight']=($v['Weight']*$v['Qty']);
	                    $pro_info_ary[$v['CId']]['Volume']=($v['Volume']*$v['Qty']);
	                    $pro_info_ary[$v['CId']]['Qty']=$v['Qty'];
	                }
	            }else{
	                if(!$pro_info_ary[$v['OvId']]){
	                    $pro_info_ary[$v['OvId']]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
	                }
	                $pro_info_ary[$v['OvId']]['tWeight']+=($v['Weight']*$v['Qty']);
	                $pro_info_ary[$v['OvId']]['tVolume']+=($v['Volume']*$v['Qty']);
	                $pro_info_ary[$v['OvId']]['tQty']+=$v['Qty'];
	                $pro_info_ary[$v['OvId']]['Price']+=$price;
	                if((int)$v['IsFreeShipping']==1){//免运费
	                    $pro_info_ary[$v['OvId']]['IsFreeShipping']=1; //其中有免运费
	                }else{
	                    $pro_info_ary[$v['OvId']]['Weight']+=($v['Weight']*$v['Qty']);
	                    $pro_info_ary[$v['OvId']]['Volume']+=($v['Volume']*$v['Qty']);
	                    $pro_info_ary[$v['OvId']]['Qty']+=$v['Qty'];
	                }
	            }
	        }
	    }elseif($p_Type=='shipping_cost'){
	        if($p_Attr){//产品属性
	            $Attr=str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
	            ksort($Attr);
	        }
	        $proInfo=db::get_one('products', "ProId='$p_ProId'");
	        $Weight=$proInfo['Weight'];//产品默认重量
	        $volume_ary=$proInfo['Cubage']?@explode(',', $proInfo['Cubage']):array(0,0,0);
	        $Volume=$volume_ary[0]*$volume_ary[1]*$volume_ary[2];
	        $Volume=sprintf('%.f', $Volume);//防止数额太大，程序自动转成科学计数法
	        if((int)$proInfo['IsVolumeWeight']){//体积重
	            $VolumeWeight=($Volume*1000000)/5000;//先把立方米转成立方厘米，再除以5000
	            $VolumeWeight>$Weight && $Weight=$VolumeWeight;
	        }
	        $StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;	//起订量
	        $p_Qty<$StartFrom && $p_Qty=$StartFrom;	//小于起订量
	        $CurPrice=cart::products_add_to_cart_price($proInfo, $p_Qty);
	        $PropertyPrice=0;
	        //产品属性
	        $AttrData=cart::get_product_attribute(array(
	            'Type'			=> 0,//不用获取产品属性名称
	            'ProId'			=> $p_ProId,
	            'Price'			=> $CurPrice,
	            'Attr'			=> $Attr,
	            'IsCombination'	=> $proInfo['IsCombination'],
	            'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice'],
	            'Weight'		=> $Weight
	        ));
	        $CurPrice		= $AttrData['Price'];		//产品单价
	        $PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
	        $combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
	        $OvId			= $AttrData['OvId'];		//发货地ID
	        $Weight			= $AttrData['Weight'];		//产品重量
	        $Attr			= $AttrData['Attr'];		//产品属性数据
	        //秒杀产品
	        if($p_proType==2){
	            $seckill_row=str::str_code(db::get_one('sales_seckill', "SId='$p_SId' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
	            if($seckill_row){
	                $CurPrice=$seckill_row['Price'];
	            }
	        }
	        //产品数据
	        if($proInfo['IsPromotion'] && $proInfo['PromotionType'] && $proInfo['StartTime']<$c['time'] && $c['time']<$proInfo['EndTime']){
	            $CurPrice=$CurPrice*($proInfo['PromotionDiscount']/100);
	        }
	        $total_price=($CurPrice+$PropertyPrice)*$p_Qty;
	        $IsFreeShipping=(int)$proInfo['IsFreeShipping'];
	        $total_volume=$Volume*$p_Qty;
	        $total_weight=$Weight*$p_Qty;
	        $s_total_weight=$total_weight;
	        $s_total_volume=$total_volume;
	        //产品的海外仓数据
	        $pro_info_ary[$OvId]['Weight']=$pro_info_ary[$OvId]['tWeight']=$total_weight;
	        $pro_info_ary[$OvId]['Volume']=$pro_info_ary[$OvId]['tVolume']=$total_volume;
	        $pro_info_ary[$OvId]['Price']=$total_price;
	        $pro_info_ary[$OvId]['Qty']=$p_Qty;
	        $pro_info_ary[$OvId]['TId']=(int)$proInfo['TId'];
	        $ProductPrice+=$total_price;
	        if($IsFreeShipping==1){
	            $pro_info_ary[$OvId]['Weight']=$pro_info_ary[$OvId]['Volume']=0;
	        }
	    }else{
	        $CIdStr='';
	        if($p_order_cid){
	            $in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
	            $CIdStr=" and CId in({$in_where})";
	        }
	        //			file_put_contents("t.txt","p_order_cid".var_export($p_order_cid,true).PHP_EOL.PHP_EOL,FILE_APPEND);
	        $cartInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.'.$c['where']['cart'].$CIdStr, 'p.*,s.CId,s.BuyType,s.Price,s.PropertyPrice,s.Qty,s.Discount,s.Weight as CartWeight,s.Volume,s.Attr as CartAttr', 's.CId desc');
	        $IsFreeShipping=0;
	        $total_weight=$total_volume=0;
	        //			file_put_contents("t.txt","cartInfo".var_export($cartInfo,true).PHP_EOL.PHP_EOL,FILE_APPEND);
	        foreach($cartInfo as $val){
	            $CartId=(int)$val['CId'];
	            $OvId=1;
	            $CartAttr=str::json_data(stripslashes($val['CartAttr']), 'decode');
	            if(count($CartAttr)){ //include attribute values
	                foreach((array)$CartAttr as $k=>$v){
	                    if($k=='Overseas'){ //发货地
	                        $OvId=str_replace('Ov:', '', $v);
	                        !$OvId && $OvId=1;//丢失发货地，自动默认China
	                    }
	                }
	            }
	            $price=($val['Price']+$val['PropertyPrice'])*$val['Qty']*($val['Discount']<100?$val['Discount']/100:1);
	            if($shipping_template){
	                if(!$pro_info_ary[$CartId]){
	                    $pro_info_ary[$CartId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$OvId);
	                }
	                $pro_info_ary[$CartId]['tWeight']=($val['CartWeight']*$val['Qty']);
	                $pro_info_ary[$CartId]['tVolume']=($val['Volume']*$val['Qty']);
	                $pro_info_ary[$CartId]['tQty']=$val['Qty'];
	                $pro_info_ary[$CartId]['Price']=$price;
	                $pro_info_ary[$CartId]['TId']=(int)$val['TId'];
	                if((int)$val['IsFreeShipping']==1){//免运费
	                    $pro_info_ary[$CartId]['IsFreeShipping']=1; //其中有免运费
	                }else{
	                    $pro_info_ary[$CartId]['Weight']=($val['CartWeight']*$val['Qty']);
	                    $pro_info_ary[$CartId]['Volume']=($val['Volume']*$val['Qty']);
	                    $pro_info_ary[$CartId]['Qty']=$val['Qty'];
	                }
	            }else{
	                if(!$pro_info_ary[$OvId]){
	                    $pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
	                }
	                $pro_info_ary[$OvId]['tWeight']+=($val['CartWeight']*$val['Qty']);
	                $pro_info_ary[$OvId]['tVolume']+=($val['Volume']*$val['Qty']);
	                $pro_info_ary[$OvId]['tQty']+=$val['Qty'];
	                $pro_info_ary[$OvId]['Price']+=$price;
	                if((int)$val['IsFreeShipping']==1){//免运费
	                    $pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
	                }else{
	                    $pro_info_ary[$OvId]['Weight']+=($val['CartWeight']*$val['Qty']);
	                    $pro_info_ary[$OvId]['Volume']+=($val['Volume']*$val['Qty']);
	                    $pro_info_ary[$OvId]['Qty']+=$val['Qty'];
	                }
	            }
	            $total_weight+=($val['CartWeight']*$val['Qty']);
	            $total_volume+=($val['Volume']*$val['Qty']);
	            $ProductPrice+=$price;
	        }
	        /*
	         //产品包装重量  //没有多少客户用 4.0先注释
	         $cartProAry=cart::cart_product_weight($CIdStr, 1);
	         foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
	         foreach((array)$v as $k2=>$v2){//$k2是ProId
	         $pro_info_ary[$k]['tWeight']+=$v2;
	         }
	         }
	         foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
	         foreach((array)$v as $k2=>$v2){//$k2是ProId
	         $pro_info_ary[$k]['Weight']+=$v2;
	         }
	         }*/
	        $total_weight=$pro_info_ary[$OvId]['Weight'];
	        $total_volume=$pro_info_ary[$OvId]['tVolume'];
	    }
	    //		file_put_contents("t.txt",'pro_info_ary'.var_export($pro_info_ary,true),FILE_APPEND);
	    ksort($pro_info_ary); //排列正序
	    $shipping_cfg=db::get_one('shipping_config', 'Id="1"');
	    $weight=@ceil($total_weight);
	    if($_SESSION['User']['UserId']){
	        if($p_AId){
	            $address_aid=$p_AId;
	        }else{
	            $address_aid=(int)$_SESSION['Cart']['ShippingAddressAId'];
	        }
	        if($address_aid>0) $StatesSId=(int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
	    }else{
	        $StatesSId=(int)$_SESSION['Cart']['ShippingAddress']['Province'];
	    }
	    $StatesSId=$p_StatesSId?(int)$p_StatesSId:$StatesSId;
	    $provincial_freight && $p_CId==226 && $StatesSId && $area_where=" and states like '%|{$StatesSId}|%'";
	    $IsInsurance=str::str_code(db::get_value('shipping_config', '1', 'IsInsurance'));
	    $row=db::get_all('shipping_area a left join shipping s on a.SId=s.SId', "a.AId in(select AId from shipping_country where CId='{$p_CId}' {$area_where})", 's.Express, s.Logo, s.IsWeightArea, s.WeightArea, s.ExtWeightArea, s.VolumeArea, s.IsUsed, s.IsAPI, s.FirstWeight, s.ExtWeight, s.StartWeight, s.MinWeight, s.MaxWeight, s.MinVolume, s.MaxVolume, s.FirstMinQty, s.FirstMaxQty, s.ExtQty, s.WeightType, s.TId, a.*', 'if(s.MyOrder>0, s.MyOrder, 100000) asc, a.SId asc, a.AId asc');
	    //添加获取清关费费率 所有id
	    $qg_rows=array();
	    foreach ((array)$row as $value) {
	        $r=array();
	        $r["SId"]=$value["SId"];
	        $r["AffixPrice_rate"]=$value["AffixPrice_rate"]?$value["AffixPrice_rate"]:0;
	        $qg_rows[]=$r;
	    }
	    //-------------------------------------
	    $shipping_row=db::get_all('shipping', '1', 'SId, TId');
	    $row_ary=$shipping_tid_ary=array();
	    foreach((array)$row as $v){
	        !$row_ary[$v['SId']] && $row_ary[$v['SId']]=array('info'=>$v, 'overseas'=>array());
	        $row_ary[$v['SId']]['overseas'][$v['OvId']]=$v;
	    }
	    $shipping_template && $DefaultTId=db::get_value('shipping_template', 'IsDefault=1', 'TId');
	    foreach((array)$shipping_row as $v){
	        if($shipping_template) $shipping_tid_ary[$v['TId']][]=$v['SId'];
	    }
	    unset($row);
	    foreach((array)$row_ary as $key=>$val){
	        $row=$val['info'];
	        $isOvId=0;
	        foreach((array)$pro_info_ary as $k=>$v){
	            if($shipping_template && $p_Type!='shipping_cost'){
	                $val['overseas'][$v['OvId']] && $isOvId+=1;
	            }else{
	                $val['overseas'][$k] && $isOvId+=1;
	            }
	        }//循环产品数据
	        if($isOvId==0 && $pro_info_ary['1']){
	            $info[1][]=array('SId'=>'', 'Name'=>'', 'Brief'=>'', 'IsAPI'=>'', 'type'=>'', 'ShippingPrice'=>'-1');
	            continue;
	        }
	        /************************************** 新增折后之后计算运费 开始**********************************/
	        $_total_price=orders::orders_discount_total_price($ProductPrice, $_SESSION['Cart']['Coupon'], $_SESSION['User']['UserId'], $p_order_cid, $p_ProId);
	        $pro_info_ary=orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
	        /************************************** 新增折后之后计算运费 结束**********************************/
	        //循环产品数据 Start
	        foreach((array)$pro_info_ary as $k=>$v){
	            $overseas=$val['overseas'][$k];
	            $shipping_template && $p_Type!='shipping_cost' && $overseas=$val['overseas'][$v['OvId']];
	            if($shipping_template && (!in_array($key, (array)$shipping_tid_ary[$v['TId']]) || ($DefaultTId && !$shipping_tid_ary[$v['TId']] && !in_array($key, (array)$shipping_tid_ary[$DefaultTId])))) continue;
	            $open=0;//默认不通过
	            if(in_array($row['IsWeightArea'], array(0,1,2)) && ((float)$row['MaxWeight']?($v['tWeight']>=$row['MinWeight'] && $v['tWeight']<=$row['MaxWeight']):($v['tWeight']>=$row['MinWeight']))){//重量限制
	                $open=1;
	            }elseif($row['IsWeightArea']==4 && ($v['tWeight']>=$row['MinWeight'] || $v['tVolume']>=$row['MinVolume'])){//重量限制+体积限制
	                $open=1;
	            }elseif($row['IsWeightArea']==3){//按数量计算，直接不限制
	                $open=1;
	            }
	            if($overseas && (int)$row['IsUsed']==1 && $open==1){
	                $sv=array(
	                    'SId'		=>	$row['SId'],
	                    'Name'		=>	$overseas['Express'],
	                    'Logo'		=>	($overseas['Logo'] && is_file($c['root_path'].$overseas['Logo']))?$overseas['Logo']:'',
	                    'Brief'		=>	$overseas['Brief'],
	                    'IsAPI'		=>	$overseas['IsAPI'],
	                    'type'		=>	'',
	                    'weight'	=>	$v['Weight']
	                );
	                if($IsFreeShipping || ($v['IsFreeShipping']==1 && $v['Weight']==0) || ((int)$c['config']['products_show']['Config']['freeshipping'] && $v['Weight']==0) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']>0 && $v['Price']>=$overseas['FreeShippingPrice']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingWeight']>0 && $v['Weight']<$overseas['FreeShippingWeight']) || ($overseas['IsFreeShipping']==1 && $overseas['FreeShippingPrice']==0 && $overseas['FreeShippingWeight']==0)){
	                    $shipping_price=0;
	                }else{
	                    $shipping_price=0;
	                    if($overseas['IsWeightArea']==1 || ($overseas['IsWeightArea']==2 && $v['Weight']>=$overseas['StartWeight'])){
	                        //重量区间 重量混合
	                        $WeightArea=str::json_data($overseas['WeightArea'], 'decode');
	                        $WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
	                        $areaCount=count($WeightArea)-1;
	                        foreach((array)$WeightArea as $k2=>$v2){
	                            if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
	                                if($overseas['WeightType']==1){//按每KG计算
	                                    $shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
	                                }else{//按整价计算
	                                    $shipping_price=$WeightAreaPrice[$k2];
	                                }
	                                break;
	                            }
	                        }
	                        //$v['Weight']>$WeightArea[$areaCount] && $shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
	                    }elseif($overseas['IsWeightArea']==3){
	                        //按数量
	                        $shipping_price=$overseas['FirstQtyPrice'];//先收取首重费用
	                        $ExtQtyValue=$v['Qty']>$overseas['FirstMaxQty']?$v['Qty']-$overseas['FirstMaxQty']:0;//超出的数量
	                        if($ExtQtyValue){//续重
	                            $shipping_price+=(float)(@ceil($ExtQtyValue/$overseas['ExtQty'])*$overseas['ExtQtyPrice']);
	                        }
	                    }elseif($overseas['IsWeightArea']==4){
	                        //重量体积混合计算
	                        $weight_shipping_price=$volume_shipping_price=0;
	                        if($v['Weight']>=$overseas['MinWeight']){//重量
	                            $WeightArea=str::json_data($overseas['WeightArea'], 'decode');
	                            $WeightAreaPrice=str::json_data($overseas['WeightAreaPrice'], 'decode');
	                            $areaCount=count($WeightArea)-1;
	                            foreach((array)$WeightArea as $k2=>$v2){
	                                if($k2<=$areaCount && (($WeightArea[$k2+1] && $v['Weight']<$WeightArea[$k2+1]) || (!$WeightArea[$k2+1] && $v['Weight']>=$v2))){
	                                    if($overseas['WeightType']==1){//按每KG计算
	                                        $weight_shipping_price=$WeightAreaPrice[$k2]*$v['Weight'];
	                                    }else{//按整价计算
	                                        $weight_shipping_price=$WeightAreaPrice[$k2];
	                                    }
	                                    break;
	                                }
	                            }
	                            $overseas['WeightType']==1 && $v['Weight']>$WeightArea[$areaCount] && $weight_shipping_price=$WeightAreaPrice[$areaCount]*$v['Weight'];
	                        }
	                        if($v['Volume']>=$overseas['MinVolume']){//体积
	                            $VolumeArea=str::json_data($overseas['VolumeArea'], 'decode');
	                            $VolumeAreaPrice=str::json_data($overseas['VolumeAreaPrice'], 'decode');
	                            $areaCount=count($VolumeArea)-1;
	                            foreach((array)$VolumeArea as $k2=>$v2){
	                                if($k2<=$areaCount && (($VolumeArea[$k2+1] && $v['Volume']<$VolumeArea[$k2+1]) || (!$VolumeArea[$k2+1] && $v['Volume']>=$v2))){
	                                    $volume_shipping_price=$VolumeAreaPrice[$k2]*$v['Volume'];
	                                    break;
	                                }
	                            }
	                            $v['Volume']>$VolumeArea[$areaCount] && $volume_shipping_price=$VolumeAreaPrice[$areaCount]*$v['Volume'];
	                        }
	                        $shipping_price=max($weight_shipping_price, $volume_shipping_price);
	                    }else{
	                        //首重续重
	                        $ExtWeightArea=str::json_data($overseas['ExtWeightArea'], 'decode');
	                        $ExtWeightAreaPrice=str::json_data($overseas['ExtWeightAreaPrice'], 'decode');
	                        $areaCount=count($ExtWeightArea)-1;
	                        $ExtWeightValue=$v['Weight']>$overseas['FirstWeight']?$v['Weight']-$overseas['FirstWeight']:0;//超出的重量
	                        if($areaCount>0){
	                            $shipping_price=$overseas['FirstPrice'];//先收取首重费用
	                            foreach((array)$ExtWeightArea as $k2=>$v2){
	                                if($v['Weight']>$v2 && $ExtWeightArea[$k2+1]){
	                                    $ext=$v['Weight']>$ExtWeightArea[$k2+1]?($ExtWeightArea[$k2+1]-$v2):($v['Weight']-$v2);
	                                    $shipping_price+=(float)(@ceil(sprintf('%01.4f', $ext/$overseas['ExtWeight']))*$ExtWeightAreaPrice[$k2]);
	                                }elseif($v['Weight']>$v2 && !$ExtWeightArea[$k2+1]){//达到以上费用
	                                    $ext=$v['Weight']-$v2;
	                                    $shipping_price+=(float)(@ceil(sprintf('%01.4f', $ext/$overseas['ExtWeight']))*$ExtWeightAreaPrice[$k2]);
	                                }
	                            }
	                        }else{
	                            $ExtWeightCount = sprintf('%01.4f', $ExtWeightValue/$overseas['ExtWeight']);
	                            $ExtWeightCountdecimal = floor($ExtWeightCount);
	                            if(strstr($ExtWeightCount,'.')!=false && ($ExtWeightCount-$ExtWeightCountdecimal)>0){
	                                $ExtWeightCount = @ceil($ExtWeightCount);
	                            }
	                            $shipping_price=(float)($ExtWeightCount*$ExtWeightAreaPrice[0]+$overseas['FirstPrice']);
	                        }
	                    }
	                    if($overseas['AffixPrice']){//附加费用
	                        $shipping_price+=$overseas['AffixPrice'];
	                    }
	                }
	                $sv['ShippingPrice']=cart::iconv_price($shipping_price, 2, '', 0);
	                $sv['InsurancePrice']=cart::get_insurance_price_by_price($v['Price']);
	                $info[$k][]=$sv;
	            }
	        }
	        //循环产品数据 End
	    }
	    if($info){
	        //		    file_put_contents("t.txt",var_export($info,true),FILE_APPEND);
	        $info_ary=array();
	        foreach((array)$info as $k=>$v){
	            $sort_ary=$price_ary=array();
	            foreach((array)$v as $k2=>$v2){ //
	                $price_ary[$v2['ShippingPrice']][]=$k2;
	            }
	            ksort($price_ary);
	            foreach((array)$price_ary as $k2=>$v2){
	                foreach((array)$v2 as $k3=>$v3){
	                    $info_ary[$k][]=$info[$k][$v3];
	                }
	            }
	        }
	        //添加清关费到返回值
	        foreach ((array)$info_ary as $k=>&$v) {
	            foreach ((array)$v as $k2=>&$v2) { //
	                foreach ($qg_rows as $q_k=>$q_v)
	                {
	                    if ($v2["SId"]==$q_v['SId'])
	                    {
	                        $info_ary[$k][$k2]["AffixPrice_rate"]=$q_v["AffixPrice_rate"];
	                        break;
	                    }
	                }
	            }
	        }
	        
	      
	        
	        if ($p_ProId)
	        {
	            if (isset($info_ary[1]))
	            {
	                $new_shiping_arr = array();
	                //$_get_only=array();
	                //获取运输方式
	                //     file_put_contents("t1.txt", "o_ary=>" . var_export($o_ary, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
	                $p_s_all = db::get_all("product_shipping", "products_id='" . $p_ProId . "'");
	                //     file_put_contents("t1.txt", "p_s_all=>" . var_export($p_s_all, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
	                foreach ($info_ary[1] as $k=>$oir) {
	                    $exist=false;
	                    foreach ($p_s_all as $ps_item) {
	                        if ($oir["SId"] == $ps_item["shipping_id"]) {
	                            $new_shiping_arr[]=$oir;
	                            break;
	                        }
	                    }
	                }
	                $info_ary[1]=$new_shiping_arr;
	            }
	        }
	        //-------------
	        ly200::e_json(array('Symbol'=>cart::iconv_price(0, 1), 'info'=>$info_ary, 'IsInsurance'=>$IsInsurance, 'total_weight'=>$total_weight, 'shipping_template'=>(int)$shipping_template), 1);
	    }else{
	        ly200::e_json('', 0);
	    }
	}
	public static function ajax_get_api_info(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_IsAPI=(int)$p_IsAPI;
		$api_row=db::get_one('shipping_api', "AId='$p_IsAPI'");
		if($api_row){
			$c['plugin']=new plugin('api');
			$ApiName=strtolower($api_row['Name']);
			$ApiName=='4px' && $ApiName='_4px';
			$Attribute=str::json_data($api_row['Attribute'], 'decode');
			!count($Attribute) && ly200::e_json('');//没有插件参数
		}else{
			ly200::e_json('');//没有插件数据
		}
		if($ApiName=='4px'){//4PX旧接口已失效
			ly200::e_json('');
		}
		$ShippingCharge=-1;//快递运费
		$CId=(int)$p_CId;
		$AId=(int)$p_AId;
		if($AId>0){
			//收货地址
			$address_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.{$c['where']['cart']} and a.AId='$AId' and a.IsBillingAddress=0", 'a.*,c.Country,c.Acronym,s.States as StateName'));
		}else{
			$address_row=array(
				'Acronym'	=>	db::get_value('country', "CId='{$CId}'", 'Acronym'),
				'ZipCode'	=>	'',
			);
		}
		$p_OvId=(int)$p_OvId;
		$p_cCId=(int)$p_cCId;
		$p_ProId=(int)$p_ProId;
		$p_Qty=(int)$p_Qty;
		$info=array();
		$IsFreeShipping=0;
		$pro_info_ary=array();
		if($p_Type=='shipping_cost'){
			if($p_Attr){//产品属性
				$Attr=str::str_code(str::json_data(stripslashes($p_Attr), 'decode'), 'addslashes');
				ksort($Attr);
			}
			$proInfo=db::get_one('products', "ProId='$p_ProId'");
			$Weight=$proInfo['Weight'];//产品默认重量
			$volume_ary=$proInfo['Cubage']?@explode(',', $proInfo['Cubage']):array(0,0,0);
			$Volume=$volume_ary[0]*$volume_ary[1]*$volume_ary[2];
			$Volume=sprintf('%.f', $Volume);//防止数额太大，程序自动转成科学计数法
			if((int)$proInfo['IsVolumeWeight']){//体积重
				$VolumeWeight=($Volume*1000000)/5000;//先把立方米转成立方厘米，再除以5000
				$VolumeWeight>$Weight && $Weight=$VolumeWeight;
			}
			$StartFrom=(int)$proInfo['MOQ']>0?(int)$proInfo['MOQ']:1;//起订量
			$p_Qty<$StartFrom && $p_Qty=$StartFrom;//小于起订量
			//产品属性
			$AttrData=cart::get_product_attribute(array(
				'Type'			=> 0,//不用获取产品属性名称
				'ProId'			=> $p_ProId,
				'Attr'			=> $Attr,
				'IsCombination'	=> $proInfo['IsCombination'],
				'IsAttrPrice'	=> $proInfo['IsOpenAttrPrice'],
				'Weight'		=> $Weight
			));
			$Weight=$AttrData['Weight'];//产品重量
			$IsFreeShipping=(int)$proInfo['IsFreeShipping'];
			$total_volume=$Volume*$p_Qty;
			$total_weight=$Weight*$p_Qty;
			if($IsFreeShipping==1){
				$total_weight=$total_volume=0;
			}
		}else{
			$CIdStr=' and p.IsFreeShipping=0';
			if($p_order_cid){
				$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
				$CIdStr=" and CId in({$in_where})";
			}
			$cartInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', $c['where']['cart'].$CIdStr, 's.Weight, s.Volume, s.Qty, s.OvId', 's.CId desc');
			$IsFreeShipping=0;
			$total_weight=$total_volume=0;
			foreach($cartInfo as $val){
				$OvId=$val['OvId'];
				if(!$pro_info_ary[$OvId]){
					$pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'IsFreeShipping'=>0);
				}
				//$pro_info_ary[$OvId]['Weight']+=($val['Weight']*$val['Qty']);
				//$pro_info_ary[$OvId]['Volume']+=($val['Volume']*$val['Qty']);
				if((int)$val['IsFreeShipping']==1){//免运费
					$pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
				}else{
					$pro_info_ary[$OvId]['Weight']+=($val['Weight']*$val['Qty']);
					$pro_info_ary[$OvId]['Volume']+=($val['Volume']*$val['Qty']);
				}
			}
			/*
			//产品包装重量 //没有多少客户用 4.0先注释
			$cartProAry=cart::cart_product_weight($CIdStr, 1);
			foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
				foreach((array)$v as $k2=>$v2){//$k2是ProId
					$pro_info_ary[$k]['Weight']+=$v2;
				}
			}*/
			$total_weight=$pro_info_ary[$p_OvId]['Weight'];
			$total_volume=$pro_info_ary[$p_OvId]['Volume'];
		}
		(float)$total_weight<=0 && ly200::e_json('');
		if($p_IsAPI>0 && $api_row['Name']=='DHL' && $AId){//需要会员的收货地址
			//DHL插件
			$txt='
			<?xml version="1.0" encoding="utf-8"?>
			<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" schemaVersion="2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
				<GetQuote>
					<Request>
						<ServiceHeader>
							<MessageTime>'.date('Y-m-d', $c['time']+80000).'T'.date('H:i:s', $c['time']).'+00:00</MessageTime>
							<MessageReference>1234567890123456789012345678901</MessageReference>
							<SiteID>'.$Attribute['Siteid'].'</SiteID>
							<Password>'.$Attribute['Password'].'</Password>
						</ServiceHeader>
						<MetaData>
							<SoftwareName>3PV</SoftwareName>
							<SoftwareVersion>1.0</SoftwareVersion>
						</MetaData>
					</Request>
					<From>
						<CountryCode>'.$Attribute['CountryCode'].'</CountryCode>
						<Postalcode>'.$Attribute['Postalcode'].'</Postalcode>
					</From>
					<BkgDetails>
						<PaymentCountryCode>'.$Attribute['CountryCode'].'</PaymentCountryCode>
						<Date>'.date('Y-m-d', $c['time']).'</Date>
						<ReadyTime>PT10H21M</ReadyTime>
						<ReadyTimeGMTOffset>+00:00</ReadyTimeGMTOffset>
						<DimensionUnit>CM</DimensionUnit>
						<WeightUnit>KG</WeightUnit>
						<Pieces>
							<Piece>
								<PieceID>1</PieceID>
								<Weight>'.$total_weight.'</Weight>
							</Piece>
						</Pieces> 
						<IsDutiable>N</IsDutiable>
						<NetworkTypeCode>AL</NetworkTypeCode>	
						<InsuredValue>0.000</InsuredValue>
						<InsuredCurrency>USD</InsuredCurrency>
					</BkgDetails>
					<To>
						<CountryCode>'.$address_row['Acronym'].'</CountryCode>
						<Postalcode>'.$address_row['ZipCode'].'</Postalcode>
					</To>
					<Dutiable>
						<DeclaredCurrency>USD</DeclaredCurrency>
						<DeclaredValue>9.0</DeclaredValue>
					</Dutiable>
				</GetQuote>
			</p:DCTRequest>
			';
			$url='http://xmlpi-ea.dhl.com/XMLShippingServlet';
			//$url='https://xmlpitest-ea.dhl.com/XMLShippingServlet';
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $txt);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$result=curl_exec($ch);
			curl_close($ch);
			//解析返回的xml参数
			$payXml=@simplexml_load_string($result);
			$len=count($payXml->GetQuoteResponse->BkgDetails->QtdShp);
			for($i=0; $i<$len; ++$i){
				$object=$payXml->GetQuoteResponse->BkgDetails->QtdShp[$i];
				if($object->ProductShortName=='EXPRESS WORLDWIDE'){
					$WeightCharge=(float)$object->WeightCharge;//包含税的重量价格
					$WeightChargeTax=(float)$object->WeightChargeTax;//重量报价税
					$ShippingCharge=$WeightCharge+$WeightChargeTax;
					$CurrencyCode=(string)$object->CurrencyCode;//返回价格的货币代码
					$ExchangeRate=(float)$object->ExchangeRate;//美元与收费货币代码之间的汇率
				}
			}
			if($ExchangeRate){//汇率处理 返回货币->美元
				$ShippingCharge=sprintf('%01.2f', $ShippingCharge*$ExchangeRate);
			}
		}else{
			//其他插件
			/*
			if($c['plugin']->trigger($ApiName, '__config', 'charge_calculate')=='enable'){//API插件是否存在
				$api_data=array(
					'Weight'	=>	$total_weight,
					'Acronym'	=>	$address_row['Acronym'],
					'PostCode'	=>	$address_row['ZipCode'],
					'account'	=>	$Attribute
				);
				$result=$c['plugin']->trigger($ApiName, 'charge_calculate', $api_data);//调用API插件
				$result=str::json_data($result, 'decode');
				if($result['ack']=='Success'){//返回成功
					$ShippingCharge=(float)$result['totalAmount'];
				}
			}
			//汇率处理 人民币=>后台默认货币
			if($result['currencyCode']=='RMB'){
				$ExchangeRate=db::get_value('currency', "Currency='CNY'", 'ExchangeRate');
				$ShippingCharge=cart::currency_price($ShippingCharge, $ExchangeRate, $_SESSION['ManageCurrency']['ExchangeRate']);
			}
			*/
		}
		$return=array(
			'OvId'	=>	$p_OvId,
			'AId'	=>	$p_AId,
			'Name'	=>	$p_Name,
			'IsAPI'	=>	$p_IsAPI,
			'Price'	=>	$ShippingCharge
		);
		if($ShippingCharge>=0){
			ly200::e_json($return, 1);
		}else{
			ly200::e_json($return, 0);
		}
	}

	public static function ajax_get_coupon_info(){	//获取优惠券信息	get coupon info
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$coupon=@trim($p_coupon);
		$price=(float)$p_price;
		$p_userprice=(float)$p_userprice; //会员折扣的优惠金额
		$p_order_discount_price=(float)$p_order_discount_price; //全场满减的优惠金额
		$w=$c['where']['cart'];
		if($p_order_cid){//购物车产品ID
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$w.=" and CId in({$in_where})";
		}
		if($p_CId){//购物车产品ID
			$in_where=str::ary_format($p_CId, 2);
			$w.=" and CId in({$in_where})";
			$p_order_cid=$p_CId;
		}
		$ProId=0;
		if($_POST['Type']=='shipping_cost'){//来自产品详细页
			$ProId=(int)$_POST['ProId'];
		}
		if($p_jsonData!=''){
			$product_row=str::json_data(str::str_code($p_jsonData, 'stripslashes'), 'decode');
			$ProductsPrice=($product_row['Price']+$product_row['PropertyPrice'])*$product_row['Qty'];
		}else{
			$ProductsPrice=db::get_sum('shopping_cart', $w, "(Price+PropertyPrice)*Qty");	//购物车产品总价
		}
		$ProductsPrice=cart::iconv_price($ProductsPrice, 2, '', 0);
		$exchange=1;
		if($price && $ProductsPrice!=$price){
			$exchange=0;
			$ProductsPrice=$price;
		}
		$info=self::get_coupon_info($coupon, $ProductsPrice, (int)$_SESSION['User']['UserId'], $p_order_cid, $ProId);
		if($info['status']==1){
			$info['pro_price'] && $ProductsPrice=$info['pro_price'];
			$_SESSION['Cart']['Coupon']=$info['coupon'];
			$cutPrice=$info['type']==1?cart::iconv_price($info['cutprice'], 2, '', 0):($ProductsPrice*(100-$info['discount'])/100);//CouponType: [0, 打折] [1, 减价格]
			$info['cutprice']=$cutPrice;//cart::iconv_price($cutPrice, 2, '', 0);
			$info['end']=@date('m/d/Y', $info['end']);
		}else{
			unset($_SESSION['Cart']['Coupon']);
		}
		ly200::e_json($info, $info['status']==1?1:0);
	}

	public static function get_coupon_info($coupon, $price, $UserId=0, $p_order_cid, $ProId=0){//获取优惠券信息 get coupon info
		//优惠券编码、用户ID
		global $c;
		$coupon_row=db::get_one('sales_coupon', "CouponNumber='{$coupon}'");
		$UserId=(int)$_SESSION['User']['UserId'];
		$cou_rela_row=db::get_one('sales_coupon_relation', "CId='{$coupon_row['CId']}' and UserId='{$UserId}'");
		if($coupon_row){
			if($cou_rela_row){
				$BeUseTimes=db::get_row_count('sales_coupon_log', "CId='{$coupon_row['CId']}' and UserId='{$UserId}'");
			}else{
				$BeUseTimes=db::get_row_count('sales_coupon_log', "CId='{$coupon_row['CId']}'");
			}
		}
		$data=array('coupon'=>$coupon);
		if(!$coupon_row){
			$data['status']=0;	//未开始
		}elseif($c['time']<$coupon_row['StartTime']){
			$data['status']=-1;	//未开始
		}elseif($c['time']>$coupon_row['EndTime']){
			$data['status']=-2;	//已结束
		}elseif($coupon_row['UseNum'] && $BeUseTimes>=$coupon_row['UseNum']){
			$data['status']=-3;	//已无可使用次数
		}else{
			$data=array(
				'status'	=>	1,
				'cid'		=>	$coupon_row['CId'],
				'coupon'	=>	$coupon_row['CouponNumber'],
				'type'		=>	$coupon_row['CouponType'],
				'discount'	=>	$coupon_row['Discount'],
				'cutprice'	=>	$coupon_row['Money'],
				'end'		=>	$coupon_row['EndTime'],
			);
		}
		$cou_rela_row['IsExpired'] && $data['status']=-2;
		//限制条件 和 应用范围
		$pro_price=$price;
		$CateIdAry=$ProIdAry=$TagIdAry=array();
		if(($coupon_row['UserId'] || $coupon_row['LevelId']) && $data['status']==1){ //会员
			if(!db::get_row_count('sales_coupon_relation', "CId='{$coupon_row['CId']}' and UserId='{$UserId}'")) $data['status']=-5;
		}	
		if($coupon_row['CateId']){ //产品分类
			$CateAry=explode('|', substr($coupon_row['CateId'], 1, -1));
			foreach((array)$CateAry as $v){
				$CateIdAry[]=$v;
				$UId=category::get_UId_by_CateId($v);
				$category_row=db::get_all('products_category', "UId like '{$UId}%'", 'CateId');
				foreach($category_row as $k2=>$v2){ $CateIdAry[]=$v2['CateId']; }
			}
		}
		if($coupon_row['ProId']){ //产品
			$ProIdAry=explode('|', substr($coupon_row['ProId'], 1, -1));
		}
		if($coupon_row['TagId']){ //产品标签
			$TagIdAry=explode('|', substr($coupon_row['TagId'], 1, -1));
		}
		if($data['status']==1 && ($CateIdAry || $ProIdAry || $TagIdAry)){
			$count=0;
			$pro_price=0;
			if($ProId){//来自产品详细页
				$pro_row=db::get_one('products', "ProId='$ProId'", "ProId, CateId, Tags");
				$proInfo=array($pro_row);
				$tags=array();
				$pro_row['Tags'] && $tags=explode('|', substr($pro_row['Tags'], 1, -1)); //产品标签
				if(($CateIdAry && (in_array($pro_row['CateId'], $CateIdAry) || in_array('-1', $CateIdAry))) || ($ProIdAry && (in_array($pro_row['ProId'], $ProIdAry) || in_array('-1', $ProIdAry))) || ($TagIdAry && count(array_intersect($TagIdAry, $tags))>0)){ //允许使用此优惠券
					$pro_price+=$price;
				}else{
					$count=1;
				}
			}else{
				$w=$c['where']['cart'];
				if($p_order_cid){//购物车产品ID
					$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
					$w.=" and s.CId in({$in_where})";
				}
				$proInfo=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', $w, 's.Price, s.PropertyPrice, s.Qty, s.Discount, p.ProId, p.CateId, p.Tags');
				foreach($proInfo as $v){
					$tags=array();
					$v['Tags'] && $tags=explode('|', substr($v['Tags'], 1, -1)); //产品标签
					if(($CateIdAry && (in_array($v['CateId'], $CateIdAry) || in_array('-1', $CateIdAry))) || (($ProIdAry && (in_array($v['ProId'], $ProIdAry) || in_array('-1', $ProIdAry)) || in_array('-1', $ProIdAry))) || (($TagIdAry && count(array_intersect($TagIdAry, $tags))>0) || in_array('-1', $TagIdAry))){ //允许使用此优惠券
						$pro_price+=($v['Price']+$v['PropertyPrice'])*($v['Discount']/100)*$v['Qty'];
					}else{
						$count+=1;
					}
				}
			}
			if(count($proInfo)==$count){
				$data['status']=-6;	//没有一个产品能使用此优惠券
			}
		}
		if($pro_price<(float)cart::iconv_price($coupon_row['UseCondition'], 2, '', 0)){
			$data['status']=-4;	//产品总额未达到使用条件
			$data['balance']=(float)cart::iconv_price($coupon_row['UseCondition'], 2, '', 0);
		}
		$data['pro_price']=$pro_price; //购物车产品总价
		
		return $data;
	}

	public static function remove_coupon(){//清除优惠券
		unset($_SESSION['Cart']['Coupon']);
	}
	
	public static function placeorder(){	//下订单 place an order
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		//初始化
//		file_put_contents("t2.txt","_POST=>".var_export($_POST,true).PHP_EOL.PHP_EOL,FILE_APPEND);
		if(!(int)$c['config']['global']['TouristsShopping']){
			$user=user::check_login('', 1);
		}

		$shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used'])?1:0;
		//发货方式
		$Oversea=explode(',', $p_order_shipping_oversea);
		$SId=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_method_sid), 'decode');//发货方式
		$sInsurance=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_insurance), 'decode');//运费保险
		$sType=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_method_type), 'decode');//海运或空运
		foreach($Oversea as $v){//丢失海外仓
			!$SId[$v] && !$shipping_template && ly200::e_json('', -2);
		}
		$SId_ary=array();
		foreach($SId as $k=>$v){
			((!$v && $sType[$k]=='') || $v==-1) && ly200::e_json('', -2);
			$SId_ary[]=$k;
		}
		
		//付款方式
		$PId=(int)$p_order_payment_method_pid;
		(!$PId || $PId==-1) && ly200::e_json('', -3);
		$payment_row=str::str_code(db::get_one('payment', "IsUsed=1 and PId='$PId'"));
		//发货地址、账单地址
		$typeAddr=(int)$p_typeAddr;//是否非会员购物【0，否；1，是】
		if($typeAddr==1){
			$data_user=array('UserId'=>0, 'Email'=>$p_Email);
			!$data_user['Email'] && ly200::e_json('', -1);
			$tax_ary=$bill_tax_ary=array();
            $CountryCId = $p_CId ? $p_CId : $p_country_id;
			$address_country_row=db::get_one('country', "CId='$CountryCId'", 'Country, Code');
			$StateName=db::get_value('country_states', "CId='$CountryCId' and SId='{$p_Province}'", 'States');
			$address_row=array(
				'FirstName'		=>	str_replace(array('\\', '\\\\'), '', $p_FirstName),
				'LastName'		=>	str_replace(array('\\', '\\\\'), '', $p_LastName),
				'AddressLine1'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine1),
				'AddressLine2'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine2),
				'CountryCode'	=>	($address_country_row['Code']?$address_country_row['Code']:(int)$p_CountryCode),
				'PhoneNumber'	=>	$p_PhoneNumber,
				'City'			=>	str_replace(array('\\', '\\\\'), '', $p_City),
				'SId'			=>	(int)$p_Province,
				'State'			=>	str_replace(array('\\', '\\\\'), '', $p_State),
				'StateName'		=>	$StateName,
				'CId'			=>	(int)$CountryCId,
				'Country'		=>	$address_country_row['Country'],
				'ZipCode'		=>	$p_ZipCode,
				'CodeOption'	=>	$p_CodeOption,
				'TaxCode'		=>	$p_TaxCode,
			);
			$bill_row=$address_row;
			$tax_ary=$bill_tax_ary=user::get_tax_info($address_row);
		}else{
			$data_user=array('UserId'=>$_SESSION['User']['UserId'], 'Email'=>$_SESSION['User']['Email']);
			$AId=(int)$p_order_shipping_address_aid;//收货地址ID
			(!$AId || $AId==-1 || !$data_user['Email']) && ly200::e_json('', -1);
			//收货地址
			$tax_ary=array();
			$address_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.{$c['where']['cart']} and a.AId='$AId' and a.IsBillingAddress=0", 'a.*, c.Country, s.States as StateName'));
			(!$address_row['FirstName'] || !$address_row['LastName'] || !$address_row['AddressLine1'] || !$address_row['City'] || !$address_row['CId'] || !$address_row['ZipCode'] || !$address_row['PhoneNumber']) && ly200::e_json('', -1);
			$tax_ary=user::get_tax_info($address_row);
			//账单地址
			$bill_tax_ary=array();
			$bill_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.{$c['where']['cart']} and a.IsBillingAddress=1", 'a.*, c.Country, s.States as StateName'));
			$bill_tax_ary=user::get_tax_info($bill_row);
		}
		//检查产品的信息是否正常
		$cart_where='';
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$cart_where=" and CId in({$in_where})";
		}
		//$cart_row=db::get_all('shopping_cart', $c['where']['cart'].$cart_where, "CId, BuyType, KeyId, PicPath, Price, Property, PropertyPrice, Discount, Qty, Weight as CartWeight, Volume, OvId, Remark, Language, SKU as CartSKU, Attr as CartAttr", 'CId desc');
		$cart_row=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.'.$c['where']['cart'].$cart_where, 's.CId, s.ProId, s.BuyType, s.KeyId, s.PicPath, s.Price, s.Property, s.PropertyPrice, s.Discount, s.Qty, s.Weight as CartWeight, s.Volume, s.OvId, s.Remark, s.Language, s.SKU as CartSKU, s.Attr as CartAttr, p.IsFreeShipping, p.Wholesale, p.SoldStatus', 's.CId desc');
		!count($cart_row) && ly200::e_json('', -4);
		$stock_error='0';
		$IsFreeShipping=$ProductPrice=$totalWeight=$totalVolume=$s_totalWeight=$s_totalVolume=0;
		$pro_info_ary=array();//统计产品重量体积，用于运费计算
		foreach($cart_row as $key=>$val){
			//产品类型的处理
			if($shipping_template && !in_array($val['CId'], (array)$SId_ary)) ly200::e_json(array('cid'=>$val['CId']), -2);
			$Data=cart::product_type(array(
				'Type'	=> $val['BuyType'],
				'KeyId'	=> $val['KeyId'],
				'Qty'	=> $val['Qty'],
				'Attr'	=> $val['CartAttr']
			));
			if($Data===false) $stock_error.=",{$val['CId']}";
			$BuyType	= $Data['BuyType'];	//产品类型
			$KeyId		= $Data['KeyId'];	//主ID
			$ProId		= $Data['ProId'];	//产品ID
			$StartFrom	= $Data['StartFrom'];//产品目前的起订量
			$Price		= $Data['Price'];	//产品目前的单价
			$Qty		= $Data['Qty'];		//购买的数量
			$Discount	= $Data['Discount'];//折扣
			$Attr		= $Data['Attr'];	//产品属性
			$Weight		= $Data['Weight'];	//产品目前的重量
			$Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
			$SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
			$IsStock	= (int)$Data['ProdRow']['SoldStatus'];
			$seckill_row= $Data['SeckRow'];	//秒杀数据
			$OvId		= 1;//发货地ID
			$Qty<1 && $Qty=1;
			$cart_row[$key]['ProId']=$ProId;//记录产品ID
			$cart_row[$key]['Name']=$Data['ProdRow']['Name'.$c['lang']];//记录产品名称
			if($BuyType!=4){//“组合促销”除外
				!$Data['ProdRow']['Number'] && $stock_error.=",{$val['CId']}";//既不是组合促销，产品也同时不存在
				if(($IsStock!=1 && ($Data['ProdRow']['Stock']<$Qty || $Data['ProdRow']['Stock']<$Data['ProdRow']['MOQ'] || $Data['ProdRow']['Stock']<1)) || ($Data['ProdRow']['SoldOut']==1 && $Data['ProdRow']['IsSoldOut']==0) || ($Data['ProdRow']['SoldOut']==1 && $Data['ProdRow']['IsSoldOut']==1 && ($Data['ProdRow']['SStartTime']>$c['time'] || $c['time']>$Data['ProdRow']['SEndTime'])) || in_array($Data['ProdRow']['CateId'], $c['procate_soldout'])){//产品库存量不足（包括产品下架）
					$stock_error.=",{$val['CId']}";
				}
			}
			//产品属性的处理
			if($BuyType!=4){
				$AttrData=cart::get_product_attribute(array(
					'Type'			=> 0,//不用获取产品属性名称
					'BuyType'		=> $BuyType,
					'ProId'			=> $ProId,
					'Price'			=> $Price,
					'Attr'			=> $Attr,
					'IsCombination'	=> $Data['ProdRow']['IsCombination'],
					'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
					'SKU'			=> $Data['ProdRow']['SKU'],
					'Weight'		=> $Weight
				));
				if($AttrData===false) $stock_error.=",{$val['CId']}";
				$Price			= $AttrData['Price'];		//产品单价
				$PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
				$combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
				$OvId			= $AttrData['OvId'];		//发货地ID
				$ColorId		= $AttrData['ColorId'];		//颜色ID
				$Weight			= $AttrData['Weight'];		//产品重量
				$SKU			= $AttrData['SKU'];			//产品SKU
				$Attr			= $AttrData['Attr'];		//产品属性数据
			}
			if((int)$IsStock!=1 && (int)$Data['ProdRow']['IsCombination'] && $combinatin_ary && (!$combinatin_ary[1] || $Qty>$combinatin_ary[1])){//产品属性库存量不足
				$stock_error.=",{$val['CId']}";//没有无限库存、开启组合属性
			}
			if((int)$IsStock==1 && (int)$Data['ProdRow']['IsCombination']==0 && $combinatin_ary && $combinatin_ary[1]>0 && $Qty>$combinatin_ary[1]){//产品属性库存量不足
				$stock_error.=",{$val['CId']}";//开启无限库存、不是组合属性、属性库存不是0
			}
			//普通产品，重新计算价格，开始判断批发价和促销价
			if($BuyType==0){
				// if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || (int)$Data['ProdRow']['IsCombination']==0 || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
				if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
				}else{ //没有属性
					$Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
				}
			}
			//更新产品信息
			$update_data=array(
				'Weight'		=>	(float)($Weight?$Weight:$val['CartWeight']),
				'Volume'		=>	(float)sprintf('%01.3f', ($Volume?$Volume:$val['Volume'])),
				'Price'			=>	(float)sprintf('%01.2f', ($Price?$Price:$val['Price'])),
				'Qty'			=>	$Qty,
				'PropertyPrice'	=>	(float)sprintf('%01.2f', ($PropertyPrice?$PropertyPrice:$val['PropertyPrice'])),
				'Discount'		=>	(int)($Discount?$Discount:$val['Discount'])
			);
			if($BuyType==0){//检查产品混批功能
				$result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, $val['CId'], ($p_order_cid?1:0));//检查产品混批功能
				$result && $update_data['Price']=$result;
			}
			db::update('shopping_cart', "CId='{$val['CId']}'", $update_data);//更新购物车产品信息
			if($BuyType==0){//检查产品混批功能
				$result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, $val['CId'], ($p_order_cid?1:0));//检查产品混批功能
				$result && $update_data['Price']=$result;
			}
			//整理产品的信息数据
			$cart_row[$key]=array_merge($cart_row[$key], $update_data);
			$item_price=($update_data['Price']+$update_data['PropertyPrice'])*($update_data['Discount']<100?$update_data['Discount']/100:1)*$Qty;
			$ProductPrice+=$item_price;
			$totalVolume+=($update_data['Volume']*$Qty);
			if($shipping_template){
				$CartId=(int)$val['CId'];
				if(!$pro_info_ary[$CartId]){
					$pro_info_ary[$CartId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$OvId);
				}
				$pro_info_ary[$CartId]['tWeight']=($update_data['Weight']*$Qty);
				$pro_info_ary[$CartId]['tVolume']=($update_data['Volume']*$Qty);
				$pro_info_ary[$CartId]['tQty']=$Qty;
				$pro_info_ary[$CartId]['Price']=$item_price;
				if((int)$val['IsFreeShipping']==1){//免运费
					$pro_info_ary[$CartId]['IsFreeShipping']=1; //其中有免运费
				}else{
					$pro_info_ary[$CartId]['Weight']=($update_data['Weight']*$Qty);
					$pro_info_ary[$CartId]['Volume']=($update_data['Volume']*$Qty);
					$pro_info_ary[$CartId]['Qty']=$Qty;
				}
			}else{
				if(!$pro_info_ary[$OvId]){
					$pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
				}
				$pro_info_ary[$OvId]['tWeight']+=($update_data['Weight']*$Qty);
				$pro_info_ary[$OvId]['tVolume']+=($update_data['Volume']*$Qty);
				$pro_info_ary[$OvId]['tQty']+=$Qty;
				$pro_info_ary[$OvId]['Price']+=$item_price;
				if((int)$val['IsFreeShipping']==1){//免运费
					$pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
				}else{
					$pro_info_ary[$OvId]['Weight']+=($update_data['Weight']*$Qty);
					$pro_info_ary[$OvId]['Volume']+=($update_data['Volume']*$Qty);
					$pro_info_ary[$OvId]['Qty']+=$Qty;
				}
			}
			if(${'p_Remark_'.$val['ProId'].'_'.$val['CId']}){//记录备注
				$cart_row[$key]['Remark']=${'p_Remark_'.$val['ProId'].'_'.$val['CId']};
			}
		}
		$stock_error!='0' && ly200::e_json($stock_error, -6);//产品库存量不足
		/*
		//产品包装重量 用的人不多暂时注释
		$weight_where='';
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$weight_where=" and c.CId in({$in_where})";
		}
		$cartProAry=cart::cart_product_weight($weight_where, 1);
		foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['tWeight']+=$v2;
			}
		}
		foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
			foreach((array)$v as $k2=>$v2){//$k2是ProId
				$pro_info_ary[$k]['Weight']+=$v2;
			}
		}
		*/
		
		//优惠券处理
		$CouponCode='';
		$IsUseCoupon=$CouponPrice=$CouponDiscount=0;
		if($p_order_coupon_code){
			$coupon_row=self::get_coupon_info($p_order_coupon_code, cart::iconv_price($ProductPrice, 2, '', 0), $data_user['UserId'], $p_order_cid);
			if($coupon_row['status']==1){
				$CouponCode=addslashes($coupon_row['coupon']);
				if($coupon_row['type']==1){//CouponType: [0, 打折] [1, 减价格]
					$CouponPrice=$coupon_row['cutprice'];
				}else{
					if($coupon_row['pro_price']){
						$CouponPrice=($coupon_row['pro_price']*(100-$coupon_row['discount'])/100);
						$CouponPrice=cart::currency_price($CouponPrice, $_SESSION['Currency']['ExchangeRate'], $_SESSION['ManageCurrency']['ExchangeRate']);//换成后台默认的货币
					}else{
						$CouponDiscount=(100-$coupon_row['discount'])/100;
					}
				}
				$IsUseCoupon=1;
			}
		}
		//“会员优惠”和“全场满减”之间的优惠对比
		$DisData=cart::discount_contrast($ProductPrice);
		$DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
		$Discount		= $DisData['Discount'];			//全场满减的折扣
		$UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
		//最低消费设置
		$_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)*(1-$CouponDiscount)-$CouponPrice-$DiscountPrice;//订单折扣后的总价
		if((int)$c['config']['global']['LowConsumption'] && cart::iconv_price($_total_price, 2, '', 0)<cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0)){
			ly200::e_json(cart::iconv_price($c['config']['global']['LowPrice'], 0, '', 0), -5);
		}
		/****************************2019/04/18 计算折扣后的产品价格数据 开始******************************/
		$pro_info_ary=orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
		/****************************2019/04/18 计算折扣后的产品价格数据 结束******************************/
		//计算快递运费
		if($_SESSION['User']['UserId']){ //获取省份ID
			if($AId){
				$address_aid=$AId;
			}else{
				$address_aid=(int)$_SESSION['Cart']['ShippingAddressAId'];
			}
			if($address_aid>0) $StatesSId=(int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
		}else{
			if($p_Province){
				$StatesSId=(int)$p_Province;
			}else{
				$StatesSId=(int)$_SESSION['Cart']['ShippingAddress']['Province'];
			}
		}
		$shipping_ary=orders::orders_shipping_method($SId, $address_row['CId'], $sType, $sInsurance, $pro_info_ary, $p_order_shipping_api, $StatesSId);
		!$shipping_ary && ly200::e_json('', -2);
		//总重量
		$totalWeight=cart::cart_product_weight($cart_where, 2);
		//游客，自动获取上次最新下单的业务员数据（相同邮箱地址）
		$SalesId=0;
		if((int)$data_user['UserId']==0){
			$SalesId=(int)db::get_value('user', "Email='{$data_user['Email']}' and SalesId>0", 'SalesId', 'UserId desc');
		}
		//分销订单
		if($_SESSION['DIST']){
			$source_row=db::get_one('user', "UserId='{$_SESSION['DIST']['UserId']}'");//分销上级的会员数据
			$DISTInfo=$source_row['DISTUId'].$source_row['UserId'].',';
		}

		//创建订单号
		while(1){
			$OId=date('ymdHis', $c['time']).mt_rand(10,99);
			if(!db::get_row_count('orders', "OId='$OId'")){ break; }
		}
		//添加总运费
		$total_shipping_price = 0;
        $total_qingugan = 0;
		if ($p_order_shipping_info)
		{
		    $_arr=@str::json_data(str_replace(array('\\'), '', $p_order_shipping_info), 'decode');
		    // 			    $_arr=json_decode($p_order_shipping_info,true);
		    foreach ($_arr as $_item)
		    {
		        $total_shipping_price+=$_item["price"];
		        $total_shipping_price+=$_item["qingguan"];
                $total_qingugan += $_item['qingguan'];
		    }
		}

		if ($p_order_affix_price != $total_qingugan){
            //
        }
		$order_data=array(
			/*******************订单基本信息*******************/
			'OId'					=>	$OId,
			'UserId'				=>	$data_user['UserId'],
			'Source'				=>	ly200::is_mobile_client(0)?1:0,
			'RefererId'				=>	(int)$_COOKIE['REFERER'],
			'Email'					=>	$data_user['Email'],
			'SalesId'				=>	$SalesId,
			'Discount'				=>	$Discount,
			'DiscountPrice'			=>	$DiscountPrice,
			'UserDiscount'			=>	$UserDiscount,
			'ProductPrice'			=>	(float)substr(sprintf('%01.3f', $ProductPrice), 0, -1),//$ProductPrice,
			'Currency'				=>	$_SESSION['Currency']['Currency'],//当前货币
			'ManageCurrency'		=>	$_SESSION['ManageCurrency']['Currency'],//当前后台货币
			'Rate'					=>	$_SESSION['Currency']['Rate'],//当前货币的汇率
			'TotalWeight'			=>	$totalWeight,
			'TotalVolume'			=>	$totalVolume,
			'OrderTime'				=>	$c['time'],
			'UpdateTime'			=>	$c['time'],
			'Note'					=>	$p_Note,
			'DISTInfo'				=>	$DISTInfo,
			'IP'					=>	ly200::get_ip(),
			/*******************优惠券信息*******************/
			'CouponCode'			=>	$CouponCode,
			'CouponPrice'			=>	$CouponPrice,
			'CouponDiscount'		=>	$CouponDiscount,
			/*******************收货地址*******************/
			'ShippingFirstName'		=>	addslashes($address_row['FirstName']),
			'ShippingLastName'		=>	addslashes($address_row['LastName']),
			'ShippingAddressLine1'	=>	addslashes($address_row['AddressLine1']),
			'ShippingAddressLine2'	=>	addslashes($address_row['AddressLine2']),
			'ShippingCountryCode'	=>	'+'.$address_row['CountryCode'],
			'ShippingPhoneNumber'	=>	$address_row['PhoneNumber'],
			'ShippingCity'			=>	addslashes($address_row['City']),
			'ShippingState'			=>	addslashes($address_row['StateName']?$address_row['StateName']:$address_row['State']),
			'ShippingSId'			=>	$address_row['SId'],
			'ShippingCountry'		=>	addslashes($address_row['Country']),
			'ShippingCId'			=>	$address_row['CId'],
			'ShippingZipCode'		=>	$address_row['ZipCode'],
			'ShippingCodeOption'	=>	$tax_ary['CodeOption'],
			'ShippingCodeOptionId'	=>	$tax_ary['CodeOptionId'],
			'ShippingTaxCode'		=>	$tax_ary['TaxCode'],
			/*******************账单地址*******************/
			'BillFirstName'			=>	addslashes($bill_row['FirstName']),
			'BillLastName'			=>	addslashes($bill_row['LastName']),
			'BillAddressLine1'		=>	addslashes($bill_row['AddressLine1']),
			'BillAddressLine2'		=>	addslashes($bill_row['AddressLine2']),
			'BillCountryCode'		=>	'+'.$bill_row['CountryCode'],
			'BillPhoneNumber'		=>	$bill_row['PhoneNumber'],
			'BillCity'				=>	addslashes($bill_row['City']),
			'BillState'				=>	addslashes($bill_row['StateName']?$bill_row['StateName']:$bill_row['State']),
			'BillSId'				=>	$bill_row['SId'],
			'BillCountry'			=>	addslashes($bill_row['Country']),
			'BillCId'				=>	$bill_row['CId'],
			'BillZipCode'			=>	$bill_row['ZipCode'],
			'BillCodeOption'		=>	$bill_tax_ary['CodeOption'],
			'BillCodeOptionId'		=>	$bill_tax_ary['CodeOptionId'],
			'BillTaxCode'			=>	$bill_tax_ary['TaxCode'],
			/*******************发货方式*******************/
			'ShippingExpress'		=>	addslashes($shipping_ary['ShippingExpress']?$shipping_ary['ShippingExpress']:''),
			'ShippingMethodSId'		=>	addslashes($shipping_ary['ShippingMethodSId']?$shipping_ary['ShippingMethodSId']:''),
			'ShippingMethodType'	=>	addslashes($shipping_ary['ShippingMethodType']?$shipping_ary['ShippingMethodType']:''),
			'ShippingInsurance'		=>	addslashes($shipping_ary['ShippingInsurance']?$shipping_ary['ShippingInsurance']:''),
		    'ShippingPrice'			=>	(float)substr(sprintf('%01.3f',$total_shipping_price), 0, -1),//$total_shipping_price,//$shipping_ary['ShippingPrice'],
			'ShippingInsurancePrice'=>	$shipping_ary['ShippingInsurancePrice'],
			'ShippingOvExpress'		=>	addslashes($shipping_ary['ShippingOvExpress']?$shipping_ary['ShippingOvExpress']:''),
			'ShippingOvSId'			=>	addslashes($shipping_ary['ShippingOvMethodSId']?$shipping_ary['ShippingOvMethodSId']:''),
			'ShippingOvType'		=>	addslashes($shipping_ary['ShippingOvMethodType']?$shipping_ary['ShippingOvMethodType']:''),
			'ShippingOvInsurance'	=>	addslashes($shipping_ary['ShippingOvInsurance']?$shipping_ary['ShippingOvInsurance']:''),
			'ShippingOvPrice'		=>	addslashes($shipping_ary['ShippingOvPrice']?$shipping_ary['ShippingOvPrice']:''),
			'ShippingOvInsurancePrice'=>addslashes($shipping_ary['ShippingOvInsurancePrice']?$shipping_ary['ShippingOvInsurancePrice']:''),
			'shipping_template'		=>	(int)$shipping_template,
			/*******************付款方式*******************/
			'PId'					=>	$PId,
			'PaymentMethod'			=>	$payment_row['Name'.$c['lang']],//db::get_value('payment', "IsUsed=1 and PId='$PId'", "Name{$c['lang']}"),
			'PayAdditionalFee'		=>	$payment_row['AdditionalFee'],
			'PayAdditionalAffix'	=>	$payment_row['AffixPrice'],
			/**********************add by jay********************/
			'DepositPrice'	        =>	$p_order_deposit_price,
			'QingguanPrice'	        =>	$total_qingugan
		);
		//生成订单
		db::insert('orders', $order_data);
		$OrderId=db::get_insert_id();
		//删除购物车相关的信息
		
		//确认这订单能够正常下单，才进行更新操作的优惠券信息
		$coupon_row=db::get_one('sales_coupon', "CouponNumber='{$coupon_row['coupon']}'");
		$coupon_row && $IsUseCoupon && user::sales_coupon_log($coupon_row, $OrderId);
		
		$_SESSION['Cart']['Coupon']='';
		
		unset($tax_ary, $shipping_ary, $coupon_row, $_SESSION['Cart']['Coupon']);
		//购物车产品的数据转移
		$i=1;
		$insert_sql='';
		$order_pic_dir=$c['orders']['path'].date('ym', $c['time'])."/{$OId}/";
		!is_dir($c['root_path'].$order_pic_dir) && file::mk_dir($order_pic_dir);
		foreach($cart_row as $v){
			$ext_name=file::get_ext_name($v['PicPath']);
			$ImgPath=$order_pic_dir.str::rand_code().'.'.$ext_name;
			if(count($cart_row)<200){
				@copy($c['root_path'].$v['PicPath'].'.240x240.'.$ext_name, $c['root_path'].$ImgPath);
			}
			$Name=str::str_code($v['Name'], 'addslashes');
			$SKU=str::str_code(($v['CartSKU']?$v['CartSKU']:$v['SKU']), 'addslashes');
			$Property=str::str_code($v['Property'], 'addslashes');
			$Remark=str::str_code($v['Remark'], 'addslashes');
			//新增 order_shipping_info list 开始
			$ShippingMethodSId=0;
			$ShippingPrice=0;
			$QingguanPrice=0;
			if ($p_order_shipping_info)
			{
                $_arr=@str::json_data(str_replace(array('\\'), '', $p_order_shipping_info), 'decode');
// 			    $_arr=json_decode($p_order_shipping_info,true);
//			    file_put_contents("t2.txt","_arr=>".var_export($_arr,true).PHP_EOL.PHP_EOL,FILE_APPEND);
			    foreach ($_arr as $_item)
			    {
			        if ($_item["proid"]==$v['ProId'])
			        {
			            $ShippingMethodSId=$_item["sid"];
			            $ShippingPrice=$_item["price"];
			            $QingguanPrice=$_item["qingguan"];
			            break;
			        }
			    }
			}
			
			
			$insert_sql.=($i%100==1)?'insert into `orders_products_list` (OrderId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Price, Qty, Property, PropertyPrice, OvId, CId, Discount, Remark, Language, AccTime,ShippingMethodSId,ShippingPrice,QingguanPrice) VALUES':',';
			$insert_sql.="('$OrderId', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$ImgPath}','{$v['StartFrom']}', '{$v['Weight']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}','{$v['CId']}', {$v['Discount']}, '{$Remark}', '{$v['Language']}', '{$c['time']}', '{$ShippingMethodSId}', '{$ShippingPrice}', '{$QingguanPrice}')";
			////新增 order_shipping_info list 结束
			
			if($i++%100==0){
				db::query($insert_sql);
				$insert_sql='';
			}
		}
		$insert_sql!='' && db::query($insert_sql);
		//确保数据转移成功才删除购物车
		$where=$c['where']['cart'];
		if($p_order_cid){
			$in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
			$where.=" and CId in({$in_where})";
		}
		db::delete('shopping_cart', $where);

		//没登录会员的情况下单，利用邮箱判断是否为会员，是则分配到该会员，不是则创建临时会员，类似下单后自动注册会员
		orders::assign_member($order_data);
		/*
		//下单后自动注册会员
		if((int)$c['config']['global']['AutoRegister'] && !db::get_row_count('user', "Email='{$data_user['Email']}'")){
			//随机生成密码
			$PasswordStr=str::rand_code();
			$Password=ly200::password($PasswordStr);
			$time=$c['time'];
			$ip=ly200::get_ip();
			$data=array(
				'Language'		=>	'en',
				'FirstName'		=>	addslashes($address_row['FirstName']),
				'LastName'		=>	addslashes($address_row['LastName']),
				'Email'			=>	$data_user['Email'],
				'Password'		=>	$Password,
				'RegTime'		=>	$time,
				'RegIp'			=>	$ip,
				'LastLoginTime'	=>	$time,
				'LastLoginIp'	=>	$ip,
				'LoginTimes'	=>	1,
				'Status'		=>	1,
				'IsLocked'		=>	0
			);
			db::insert('user', $data);
			$UserId=db::get_insert_id();
			$data_oth=array(
				'UserId'		=>	$UserId,
				'FirstName'		=>	addslashes($address_row['FirstName']),
				'LastName'		=>	addslashes($address_row['LastName']),
				'AddressLine1'	=>	addslashes($address_row['AddressLine1']),
				'City'			=>	addslashes($address_row['City']),
				'State'			=>	addslashes($address_row['StateName']?$address_row['StateName']:$address_row['State']),
				'SId'			=>	$address_row['SId'],
				'CId'			=>	$address_row['CId'],
				'CodeOption'	=>	$tax_ary['CodeOption'],
				'TaxCode'		=>	$tax_ary['TaxCode'],
				'ZipCode'		=>	$address_row['ZipCode'],
				'CountryCode'	=>	$address_row['CountryCode'],
				'PhoneNumber'	=>	$address_row['PhoneNumber'],
				'AccTime'		=>	$time
			);
			db::insert('user_address_book', $data_oth);//Shipping Address
			$data_oth['IsBillingAddress']=1;
			db::insert('user_address_book', $data_oth);//Billing Address
			$_SESSION['User']=$data;
			$_SESSION['User']['UserId']=$UserId;
			db::update('orders', "OrderId='$OrderId'", array('UserId'=>$UserId));
			//更新会员等级
			$LId=(int)db::get_value('user_level', 'IsUsed=1 and FullPrice<=0', 'LId');
			if(!$_SESSION['User']['IsLocked'] && $LId){//没有固定会员等级
				db::update('user', "UserId='$UserId'", array('Level'=>$LId));
				$_SESSION['User']['Level']=$LId;
			}
			user::operation_log($UserId, '会员注册');
			if((int)$c['config']['email']['notice']['create_account']){ //邮件通知开关【会员注册】
				include($c['static_path'].'/inc/mail/create_account.php');
				ly200::sendmail($data_user['Email'], $mail_title, $mail_contents);
			}
			unset($_SESSION['Cart']['ShippingAddress']);
		}
		*/
		
		unset($address_row, $data_oth);
		orders::orders_log((int)$data_user['UserId'], $data_user['UserId']?addslashes($_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']):'System', $OrderId, 1, "Place an Order: ".$OId);
		if((int)$c['config']['global']['LessStock']==0){//下单减库存
			$orders_row=db::get_one('orders', "OrderId='$OrderId'");
			orders::orders_products_update(1, $orders_row);
		}
		if((int)db::get_value('system_email_tpl', 'Template="order_create"', 'IsUsed')){//下单后邮件通知
			$ToAry=array($data_user['Email']);
			include($c['static_path'].'/inc/mail/order_create.php');
			$c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
			$c['config']['global']['OrdersSmsStatus'][1] && orders::orders_sms($OId);
		}
		$p_ReturnType=(int)$p_ReturnType;//数据反馈方式
		if($p_ReturnType==1){
			return array('OId'=>$OId, 'Method'=>$payment_row['Method']);
		}else{
			ly200::e_json(array('OId'=>$OId, 'Method'=>$payment_row['Method']), 1);
		}
	}
	
	public static function payment_ready(){//付款前的例行检查
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$order_row=db::get_one('orders', "OId='$p_OId'");
		//订单不存在
		!$order_row && ly200::e_json('', -1);
		//订单无产品
		if(!(int)db::get_row_count('orders_products_list', "OrderId='{$order_row['OrderId']}'")){
			db::delete('orders', "OrderId='{$order_row['OrderId']}'");
			ly200::e_json('', -1);
		}
		$order_row['UserId'] && $user=db::get_one('user',"UserId='{$order_row['UserId']}'");
		//会员被删除或者找不到会员
		$order_row['UserId'] && !$user && ly200::e_json('', -1);
		//准会员
		if($user['IsRegistered']==1){
			/**************************** 跟“orders::assign_member”函数有冲突 Start ****************************/
			//会员订单，未登录不允许付款
			//((int)$order_row['UserId'] && !(int)$_SESSION['User']['UserId']) && ly200::e_json('', -1);
			//当前会员非订单会员
			//((int)$order_row['UserId'] && (int)$order_row['UserId']!=(int)$_SESSION['User']['UserId']) && y200::e_json('', -1);
			/**************************** 跟“orders::assign_member”函数有冲突 End ****************************/
			//会员订单发货后状态在会员中心查询
			((int)$_SESSION['User']['UserId'] && (int)$order_row['OrderStatus']>4) && ly200::e_json('', -1);
		}
		//订单总金额低于等于0
		$total_price=sprintf('%01.2f', orders::orders_price($order_row, 1));
		if($total_price<=0 && (int)$order_row['OrderStatus']<4){
			if((int)$order_row['OrderStatus']==1){//更改为“待确认”状态
				$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
				$Log='Update order status from '.$c['orders']['status'][1].' to '.$c['orders']['status'][2];
				db::update('orders', "OId='$p_OId'", array('OrderStatus'=>2, 'UpdateTime'=>$c['time']));
				orders::orders_log($order_row['UserId'], $UserName, $order_row['OrderId'], 2, $Log);
			}
			ly200::e_json('', 0);
		}
		//付款跳转
		$payment_row=db::get_one('payment', "PId='{$order_row['PId']}'");
		if($payment_row['IsOnline']==1 && (int)$order_row['OrderStatus']<4){//支付方式为在线付款，并且状态为未付款
			ly200::e_json('', 1);
		}else{//线下付款
			ly200::e_json('', 2);
		}
	}

	public static function offline_payment(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$orders_row=db::get_one('orders', "OId='$p_OId'");
		!$orders_row && ly200::e_json('', -1);
		($orders_row['OrderStatus']!=1 && $orders_row['OrderStatus']!=3) && ly200::e_json('', -2);
		$payment_row=db::get_one('payment', "PId='{$orders_row['PId']}'");
		(!(int)$payment_row['IsUsed'] || (int)$payment_row['IsOnline'] || $p_PaymentMethod!=$payment_row['Method']) && ly200::e_json('', -3);
		(!$p_FirstName || !$p_LastName || !$p_MTCNNumber) && ly200::e_json('', -1);
		$data=array(
			'OrderId'		=>	$orders_row['OrderId'],
			'FirstName'		=>	$p_FirstName,
			'LastName'		=>	$p_LastName,
			'SentMoney'		=>	(float)$p_SentMoney,
			'MTCNNumber'	=>	(int)$p_MTCNNumber,
			'Currency'		=>	$p_Currency,
			'Country'		=>	db::get_value('country', "CId='{$p_Country}' and IsUsed=1", 'Country'),
			'Contents'		=>	$p_Contents,
			'AccTime'		=>	$c['time']
		);
		db::update('orders', "OrderId='{$orders_row['OrderId']}'", array('OrderStatus'=>2, 'UpdateTime'=>$c['time']));
		db::insert('orders_payment_info', $data);
		$orders_row['OrderStatus'] = 2;
		$UserId=(int)($_SESSION['User']['UserId']?$_SESSION['User']['UserId']:$orders_row['UserId']);
		$UserName=(int)$_SESSION['User']['UserId']?$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']:'Tourist';
		$log="Payment by ".$payment_row['Name'.$c['lang']]."(#{$p_MTCNNumber}: {$p_Currency} {$p_SentMoney})";
		orders::orders_log($UserId, $UserName, $orders_row['OrderId'], 2, $log);
		if((int)db::get_value('system_email_tpl', 'Template="order_payment"', 'IsUsed')){//邮件通知开关
			$OId=$orders_row['OId'];
			$ToAry=array(0=>$orders_row['Email']);
			include($c['static_path'].'/inc/mail/order_change.php');
			$c['config']['global']['AdminEmail'] && $ToAry[1]=$c['config']['global']['AdminEmail'];
			ly200::sendmail($ToAry, $mail_title, $mail_contents);
		}
		ly200::e_json($data, 1);
	}
	
	/********************************** Paypal普通支付处理(New) start **********************************/
	public static function paypal_payment_create_log(){
		global $c;
		include("{$c['root_path']}/plugins/payment/paypal/REST/Create.php");
	}
	
	public static function paypal_payment_execute_log(){
		global $c;
		include("{$c['root_path']}/plugins/payment/paypal/REST/Execute.php");
	}
	/********************************** Paypal普通支付处理(New) end **********************************/
	
	/********************************** 快捷支付处理(信用卡支付) start **********************************/
	public static function paypal_checkout_payment_log(){
		global $c;
		include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditPayment.php");
	}
	
	public static function paypal_checkout_create_log(){
		global $c;
		include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditCreate.php");
	}
	
	public static function paypal_checkout_complete_log(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditCheckout.php");
		ly200::e_json('', 1);
	}
	
	public static function paypal_checkout_change_log(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditChange.php");
		ly200::e_json('', 1);
	}
	
	public static function paypal_checkout_cancel_log(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		include("{$c['root_path']}/plugins/payment/paypal_excheckout/credit/CreditCancel.php");
		ly200::e_json('', 1);
	}
	/********************************** 快捷支付处理(信用卡支付) end **********************************/
	
	/********************************** 订单处理 start **********************************/
	public static function get_payment_methods()
    {
        //get payment methods
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$orders_row = db::get_one('orders', "OId='{$p_OId}'");
		!$orders_row && ly200::e_json('', 0);
        $total_price = orders::orders_price($orders_row);
		$payment_row = db::get_all('payment', 'IsUsed=1 and PId!=2', '*', $c['my_order'] . 'IsOnline desc,PId asc');
		
		$info = array();
		foreach ($payment_row as $v) {
			if ($v['MaxPrice'] > 0 ? ($total_price < $v['MinPrice'] || $total_price > $v['MaxPrice']) : ($total_price < $v['MinPrice'])) continue;
			$data = array(
				'PId'			=>	$v['PId'],
				'LogoPath'		=>	$v['LogoPath'],
				'Name'			=>	$v['Name' . $c['lang']],
				'Method'		=>	$v['Method'],
				'AdditionalFee'	=>	$v['AdditionalFee'],//手续费，百分比
				'AffixPrice'	=>	cart::iconv_price($v['AffixPrice'], 2, $orders_row['Currency'], 0),//附加费用
				'Description'	=>	$v['Description' . $c['lang']]
			);
			$info[] = $data;
		}
		if (count($info)) {
			$IsCreditCard = (int)db::get_value('payment', 'IsUsed=1 and Method="Paypal"', 'IsCreditCard');//是否开启信用卡支付
            $result_ary = array(
                'total_price'       =>  cart::iconv_price($total_price, 1, $orders_row['Currency']) . $total_price,
                'currency_symbols'  =>  cart::iconv_price($total_price, 1, $orders_row['Currency']),
                'currency'          =>  $orders_row['Currency'],
                'info'              =>  $info,
                'NewFunVersion'     =>  $c['NewFunVersion'],
                'IsCreditCard'      =>  $IsCreditCard,
                'fee_price'         =>  orders::orders_price($orders_row, 1) - $total_price,
                'PId'               =>  $orders_row['PId'],
                //add by jay start
                'DepositPrice'      =>  $orders_row['DepositPrice']
                //add by jay end
            );
			ly200::e_json($result_ary, 1);
		} else {
			ly200::e_json('', 0);
		}
	}
	
	public static function orders_payment_update()
    {
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_PId = (int)$p_PId;
		$orders_row = db::get_one('orders', "OId='{$p_OId}'");
		$payment_row = db::get_one('payment', "PId='{$p_PId}'");
		$data = array(
            'PId'			=>  $p_PId,
            'PaymentMethod'	=>  $payment_row['Name' . $c['lang']],
            'UpdateTime'	=>  $c['time']
        );
		if ($orders_row['PId'] != $p_PId) {
			$data['PayAdditionalFee'] = $payment_row['AdditionalFee'];
			$data['PayAdditionalAffix'] = $payment_row['AffixPrice'];
		}
		if ($orders_row && $payment_row) {
			db::update('orders', "OId='{$p_OId}'", $data);
			ly200::e_json('', 1);
		}
		ly200::e_json('', 0);
	}
	
	public static function orders_payment_update_loaction(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$p_PId=(int)$p_PId;
		$orders_row=db::get_one('orders', "OId='{$p_OId}'");
		$payment_row=db::get_one('payment', "PId='{$p_PId}'");
		$data = array(
					'PId'				=>$p_PId,
					'PaymentMethod'		=>$payment_row['Name'.$c['lang']],
					'UpdateTime'		=>$c['time']
				);
		if($orders_row['PId']!=$p_PId){
			$data['PayAdditionalFee'] = $payment_row['AdditionalFee'];
			$data['PayAdditionalAffix'] = $payment_row['AffixPrice'];
		}
		if($orders_row && $payment_row){
			db::update('orders', "OId='{$p_OId}'", $data);
		}
		js::loaction("/cart/complete/{$p_OId}.html");
	}
	
	public static function orders_create_account(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$orders_row=db::get_one('orders', "OId='{$p_OId}'");
		$Email=$orders_row['Email'];
		$p_Password=trim($p_Password);
		if(!$orders_row || empty($Email) || empty($p_Password)){
			ly200::e_json('', 0);
		}
		if(!db::get_row_count('user', "Email='{$Email}'")){
			$Password=ly200::password($p_Password);
			$time=$c['time'];
			$ip=ly200::get_ip();
			$data=array(
				'Language'		=>	'en',
				'FirstName'		=>	addslashes($orders_row['ShippingFirstName']),
				'LastName'		=>	addslashes($orders_row['ShippingLastName']),
				'Email'			=>	$Email,
				'Password'		=>	$Password,
				'RegTime'		=>	$time,
				'RegIp'			=>	$ip,
				'LastLoginTime'	=>	$time,
				'LastLoginIp'	=>	$ip,
				'LoginTimes'	=>	1,
				'Status'		=>	1,
				'IsLocked'		=>	0
			);
			db::insert('user', $data);
			$UserId=db::get_insert_id();
			$data_oth=array(
				'UserId'		=>	$UserId,
				'FirstName'		=>	addslashes($orders_row['ShippingFirstName']),
				'LastName'		=>	addslashes($orders_row['ShippingLastName']),
				'AddressLine1'	=>	addslashes($orders_row['ShippingAddressLine1']),
				'City'			=>	addslashes($orders_row['ShippingCity']),
				'State'			=>	addslashes($orders_row['ShippingState']),
				'SId'			=>	$orders_row['ShippingSId'],
				'CId'			=>	$orders_row['ShippingCId'],
				'CodeOption'	=>	$orders_row['CodeOption'],
				'TaxCode'		=>	$orders_row['ShippingTaxCode'],
				'ZipCode'		=>	$orders_row['ShippingZipCode'],
				'CountryCode'	=>	$orders_row['ShippingCountryCode'],
				'PhoneNumber'	=>	$orders_row['ShippingPhoneNumber'],
				'AccTime'		=>	$time
			);
			db::insert('user_address_book', $data_oth);//Shipping Address
			$data_oth['IsBillingAddress']=1;
			db::insert('user_address_book', $data_oth);//Billing Address
			$_SESSION['User']=$data;
			$_SESSION['User']['UserId']=$UserId;
			if($coupon_row=db::get_one('sales_coupon',"CouponWay=2 and ({$c['time']} < EndTime and {$c['time']} > StartTime)",'*')){
				user::get_user_coupons($coupon_row); //会员注册送优惠券
			}
			db::update('orders', "OrderId='{$orders_row['OrderId']}'", array('UserId'=>$UserId, 'UpdateTime'=>$c['time']));
			//更新会员等级
			$LId=(int)db::get_value('user_level', 'IsUsed=1 and FullPrice<=0', 'LId');
			if(!$_SESSION['User']['IsLocked'] && $LId){//没有固定会员等级
				db::update('user', "UserId='$UserId'", array('Level'=>$LId));
				$_SESSION['User']['Level']=$LId;
			}
			// $PasswordStr=$p_Password;
			user::operation_log($UserId, '会员注册');
			if((int)db::get_value('system_email_tpl', 'Template="create_account"', 'IsUsed')){ //邮件通知开关【会员注册】
				include($c['static_path'].'/inc/mail/create_account.php');
				ly200::sendmail($Email, $mail_title, $mail_contents);
			}
			ly200::e_json('', 1);
		}
		ly200::e_json('', 0);
	}
	
	public static function orders_cancel_confirm(){
		global $c;
		@extract($_POST, EXTR_PREFIX_ALL, 'p');
		$orders_row=db::get_one('orders', "OId='{$p_OId}'");
		if(!$orders_row || empty($Email) || empty($p_Password)){
			ly200::e_json('', 0);
		}
	}
	/********************************** 订单处理 end **********************************/

    /********************************** 多规加入购物车 start **********************************/
    public static function zhzadditem(){	//add product to shopping cart
//        print_r($_POST);die;
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        $p_back=(int)$p_back;
        $p_IsBuyNow=(int)$p_IsBuyNow;//是否为立即购买
        $products_type=(int)$p_products_type;//产品类型，0：普通产品，1：团购，2：秒杀，3:组合购买，4:组合促销
        //$p_id && $p_Attr=str::json_data(str::str_code($p_id, 'stripslashes'));//注册后自动返回立即添加购物车的属性数据
        /*
        $p_id=str::str_code($p_id, 'stripslashes');
        $p_Attr=$p_id?addslashes(str::json_data($p_id)):$p_Attr; //注册后自动返回立即添加购物车的属性数据
        */
        if($p_zhzid){	//注册后自动返回立即添加购物车的属性数据
            foreach((array)$p_zhzid as $k=>$v){
                $zhzid_ary=array();
                foreach ($v as $k1=>$v1) {
                    $zhzid_ary[$k1] = $v1;
                    $zhzid_ary['Overseas'] = $p_id['Overseas'];
                    $p_Attr1[$k]=str::json_data($zhzid_ary);
                }
            }
        }

        //初始化
        if($products_type==1){//团购
            $_KeyId=array($p_TId);
            $_Attr=array($p_Attr);
        }else{
            if($products_type==2){//秒杀
                $_KeyId=array($p_SId);
                $_Qty=array($p_Qty);
                $_Attr=array($p_Attr);
            }elseif($products_type==3){//组合购买
                $_KeyId=explode(',', $p_ProId);
                //$mobile = ly200::is_mobile_client(0) ? 1 : 0;
                if ($mobile) {
                    $_Qty[0]=$p_Qty; //主产品数量
                    $_Attr[0]=$p_Attr;	//主产品属性
                }
                if($p_ExtAttr){
                    $package_data=str::json_data(htmlspecialchars_decode(stripslashes($p_ExtAttr)), 'decode');
                }else{
                    $p_PId=(int)$p_PId;
                    $PackageData=db::get_value('sales_package', "PId='$p_PId'", 'PackageData');
                    $package_data=str::json_data($PackageData, 'decode');
                }
                foreach($_KeyId as $k=>$v){
                    //if($k==0 && $mobile) continue;
                    $_Attr[$k]='';
                    if($package_data[$v]){
                        $pachage_attr=array();
                        foreach((array)$package_data[$v] as $key=>$value){
                            $key!='Overseas' && $key=(int)$key;
                            $pachage_attr[$key]=($key=='Overseas'?('Ov:'.(int)str_replace('Ov:','',$value)):(int)$value);
                        }
                        //$_Attr[$k]=addslashes(str::json_data($pachage_attr));
                        $_Attr[$k]=str::json_data($pachage_attr);
                    }
                    //$_Attr[$k]=$package_data[$v]?addslashes(str::json_data($package_data[$v])):'';
                }
            }elseif($products_type==4){//组合促销
                $_KeyId = array($p_PId);
                $Attr = str::json_data(htmlspecialchars_decode(stripslashes($p_Attr)), 'decode');
                $_Attr[0] = str::json_data($Attr); //主产品属性
            }else{//普通产品
                if(!@is_array($p_ProId)){
                    $_KeyId=array($p_ProId);
                    $_Qty=array($p_Qty);
                    $_Attr=array($p_Attr1);
                }else{
                    $_KeyId=$p_ProId;
                    $_Qty=$p_Qty2;
                    $_Attr=$p_Attr1;
                }
            }
        }

        for($m=0; $m<count($p_Qty); $m++){
            if ($p_Qty[$m] == 0){
                continue;
            }
            //初始化
            $Data=cart::zhzproduct_type(array(
                'Type'	=> $products_type,
                'KeyId'	=> $p_ProId,
                'Qty'	=> $_Qty[0][$m],
                'Attr'	=> $_Attr[0][$m]
            ));
            if($Data===false) continue;
            $BuyType	= $Data['BuyType'];	//产品类型
            $KeyId		= $Data['KeyId'];	//主ID
            $ProId		= $Data['ProId'];	//产品ID
            $StartFrom	= $Data['StartFrom'];//产品目前的起订量
            $Price		= $Data['Price'];	//产品目前的单价
            $Qty		= $Data['Qty'];		//购买的数量
            $Discount	= $Data['Discount'];//折扣
            $Attr		= $Data['Attr'];	//产品属性
            $Weight		= $Data['Weight'];	//产品目前的重量
            $Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
            $SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
            $seckill_row= $Data['SeckRow'];	//秒杀数据
            $tuan_row	= $Data['TuanRow'];	//团购数据
            $IsStock	= (int)$Data['ProdRow']['SoldStatus'];//售卖状态
            $OvId		= 1;//发货地ID
            $in_cart	= true;
            //$sAttr		= str::json_data(str::str_code(str::json_data($_Attr[$m], 'decode'), 'stripslashes'));
            $sAttr		= str::json_data(str::str_code(str::json_data(str::str_code($_Attr[0][$m], 'stripslashes'), 'decode'), 'stripslashes'));
//            $cart_where	= "{$c['where']['cart']} and ProId='{$ProId}' and BuyType='{$BuyType}' and KeyId='{$KeyId}'".($_Attr[$m]?" and Attr='{$sAttr}'":'');
            $cart_where	= "{$c['where']['cart']} and ProId='{$ProId}' and BuyType='{$BuyType}' and KeyId='{$KeyId}'".($_Attr[0][$m]?" and Attr='{$sAttr}'":'');
            //产品属性
            if($BuyType!=4){
                $AttrData=cart::get_product_attribute(array(
                    'Type'			=> 1,//获取产品属性名称
                    'BuyType'		=> $BuyType,
                    'ProId'			=> $ProId,
                    'Price'			=> $Price,
                    'Attr'			=> $Attr,
                    'IsCombination'	=> $Data['ProdRow']['IsCombination'],
                    'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
                    'SKU'			=> $SKU,
                    'Weight'		=> $Weight
                ));
                if($AttrData===false) continue;
                $Property		= $AttrData['Property'];	//产品属性名称
                $Price			= $AttrData['Price'];		//产品单价
                $PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
                $combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
                $OvId			= $AttrData['OvId'];		//发货地ID
                $ColorId		= $AttrData['ColorId'];		//颜色ID
                $Weight			= $AttrData['Weight'];		//产品重量
                $SKU			= $AttrData['SKU'];			//产品SKU
                $Attr			= $AttrData['Attr'];		//产品属性数据
            }
            //没有库存
            if(!(int)$Data['ProdRow']['Stock']){
                if($IsStock!=1 && count($combinatin_ary)==0){//库存为0，(不允许购买 or 下架)，没有属性
                    if($p_back){ //返回JSON格式
                        ly200::e_json($c['lang_pack']['cart']['error']['additem_stock'], 2);
                    }else{ //跳转执行
                        js::location(ly200::get_url($Data['ProdRow'], 'products'), $c['lang_pack']['cart']['error']['additem_stock']);
                    }
                }
            }
            $cart_where.=" and OvId='{$OvId}'";//记入发货地的条件
            $OldCId=0;

            //更新？插入？
            if(db::get_row_count('shopping_cart', $cart_where)){
                $in_cart=true;
                $OldCId=(int)db::get_value('shopping_cart', $cart_where, 'CId');
            }else{
                $in_cart=false;
            }
            if($p_excheckout==1) $in_cart=false;//产品详细页的快捷支付，单独创建
            //库存检测
            $NowQty=0;
            if($in_cart==true){
                $NowQty=(int)db::get_value('shopping_cart', $cart_where, 'Qty');
                $Qty=($Qty+$NowQty);//更新后数量
            }
            $Qty=cart::zhzcheck_product_stock(array(
                'CId'		=> $OldCId,
                'IsStock'	=> $IsStock,
                'BuyType'	=> $BuyType,
                'Qty'		=> $Qty,
                'ProdRow'	=> $Data['ProdRow'],
                'Combinatin'=> $combinatin_ary,
                'Seckill'	=> $seckill_row,
                'Tuan'		=> $tuan_row
            ));
            if($Qty===false) continue;
            if($BuyType==0){//普通产品，计算价格，开始判断批发价和促销价
                // if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
                if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
                    $Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
                }else{ //属性类型是“单价”
                    $Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
                }
            }
            //产品图片
            $PicPath=$Data['ProdRow']['PicPath_0'];
            //颜色属性产品图片
            if($ColorId){
                $Path=db::get_value('products_color', "ProId='{$ProId}' and VId='{$ColorId}'", 'PicPath_0');
                @is_file($c['root_path'].$Path) && $PicPath=$Path;
            }
            if($p_excheckout==1){//产品详细页的快捷支付
                $low_ary=self::check_low_consumption(1, ($Price+$PropertyPrice)*$Discount/100*$Qty);
                if($low_ary){//未达到最低消费金额
                    ly200::e_json(array('qty'=>$Qty, 'price'=>($Price+$PropertyPrice)*$Discount/100, 'low_price'=>$low_ary['low_price'], 'difference'=>$low_ary['difference']), 2);
                }
            }

            $cartId[0] = 0;
            if($in_cart==false){
                db::insert('shopping_cart', array(
                        'UserId'		=>	(int)$_SESSION['User']['UserId'],
                        'SessionId'		=>	$c['session_id'],
                        'ProId'			=>	$ProId,
                        'BuyType'		=>	$BuyType,
                        'KeyId'			=>	$KeyId,
                        'SKU'			=>	addslashes($SKU),
                        'PicPath'		=>	$PicPath,
                        'StartFrom'		=>	$StartFrom,
                        'Weight'		=>	(float)$Weight,
                        'Volume'		=>	$Volume,
                        'Price'			=>	$Price,
                        'Qty'			=>	$Qty,
                        'Property'		=>	addslashes($Property),
                        'PropertyPrice'	=>	$PropertyPrice,
                        'Attr'			=>	$sAttr,
                        'OvId'			=>	$OvId,
                        'Discount'		=>	$Discount,
                        'Language'		=>	substr($c['lang'], 1),
                        'AddTime'		=>	$c['time']
                    )
                );
                $cartId[$m+1] = db::get_insert_id();
            }else{
                db::update('shopping_cart', $cart_where, array(
                        'Price'			=>	$Price,
                        'Qty'			=>	$Qty,
                        'PropertyPrice'	=>	$PropertyPrice,
                    )
                );

                $cart = db::get_one('shopping_cart',$cart_where,'CId');

                $cartId[$m+1] = $cart['CId'];
            }
            if(!$p_excheckout && $BuyType==0){//检查产品混批功能
                cart::update_cart_wholesale_price($ProId, $Data['ProdRow']);
            }
            if($p_IsBuyNow) $CId=(int)db::get_value('shopping_cart', $cart_where, 'CId', 'CId desc');
        }
        $p_ReturnType=(int)$p_ReturnType;//数据反馈方式
        if($p_ReturnType==1){//新版Paypal checkout返回
            $CId=(int)db::get_value('shopping_cart', $cart_where, 'CId', 'CId desc');
            $total_price=(float)cart::cart_total_price('', 1);
            $low_ary=self::check_low_consumption(1);
            return array('CId'=>$CId, 'price'=>($low_ary['s_total']?$low_ary['s_total']:$total_price), 'low_price'=>$low_ary['low_price']);
            exit;
        }
        if($p_back){
            if($p_IsBuyNow){
                //立即购买
                if($CId){
//                    ly200::e_json(array('location'=>'/cart/buynow.html?Data='.base64_encode("CId={$CId}"), 'CId'=>$CId, 'ProId'=>$ProId, 'item_price'=>cart::iconv_price((($Price+$PropertyPrice)*100/$Discount)*($Qty-$NowQty), 2, '', 0)), 1);
                    ly200::e_json(array('location'=>'/cart/checkout.html?CId='.implode('.',$cartId)),1);
                }
                ly200::e_json($c['lang_pack']['cart']['error']['additem_stock'], 2);
            }else{
                //添加购物车
                if((int)$_SESSION['User']['UserId']){	//已登录
                    $UserId=$_SESSION['User']['UserId'];
                    $user_where="UserId='{$UserId}'";
                }else{	//未登录
                    $user_where="SessionId='{$c['session_id']}'";
                }
                $total_quantity=(int)db::get_row_count('shopping_cart', $user_where, 'CId');
                $default_total_price=(float)cart::cart_total_price('', 1, 0);
                $total_price=(float)cart::cart_total_price('', 1);
                $DiscountData=cart::check_list_discounts($default_total_price, $total_price, 1);//检查购物车列表的总价格变动后，优惠情况的变动
                $low_ary=self::check_low_consumption(1);
                ly200::e_json(array('qty'=>$total_quantity, 'price'=>($low_ary['s_total']?$low_ary['s_total']:$total_price), 'low_price'=>$low_ary['low_price'], 'difference'=>$low_ary['difference'], 'total_price'=>$total_price, 'item_price'=>cart::iconv_price((($Price+$PropertyPrice)*100/$Discount)*($Qty-$NowQty), 2, '', 0),'FullCondition'=>$DiscountData['fullcondition']), 1);
            }
        }else{
            js::location('/cart/', '', '.top');
        }
    }
    public static function zhzmodify(){	//modify item quantity of shopping cart
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        //初始化
        $IsBuyNow=(int)$_GET['BuyNow'];
        $p_Qty=(int)$p_Qty;
        $p_CId=(int)$p_CId;
        $p_ProId=(int)$p_ProId;
        $cart_row=db::get_one('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'");
        $products_row=str::str_code(db::get_one('products', "ProId='{$p_ProId}'"));
        $IsStock=(int)$products_row['SoldStatus'];
        $AttrQty=0;
        $OvId=1;
        //产品属性
        if($cart_row['Attr']){
            $ext_ary=array();
            $attr_ary=@str::json_data(str::attr_decode($cart_row['Attr']), 'decode');
            foreach((array)$attr_ary as $k=>$v){
                if($k=='Overseas'){ //发货地
                    $OvId=(int)str_replace('Ov:', '', $v);
                    !$OvId && $OvId=1;//丢失发货地，自动默认China
                }else{
                    $ext_ary[]=(int)$v;
                }
            }
            sort($ext_ary); //从小到大排序
            $Combination='|'.implode('|', $ext_ary).'|';
            $row=str::str_code(db::get_one('products_selected_attribute_combination', "ProId='{$p_ProId}' and Combination='{$Combination}' and OvId='{$OvId}'"));
            if($row){
                $combinatin_ary=array($row['Price'], $row['Stock'], $row['Weight'], $row['SKU'], $row['IsIncrease']);
                $AttrQty=(int)$combinatin_ary[1];
            }
        }
        if($cart_row['BuyType']==1){//团购产品
            $tuan_row=str::str_code(db::get_one('sales_tuan', "TId='{$cart_row['KeyId']}' and BuyerCount<TotalCount and {$c['time']} between StartTime and EndTime"));
        }
        if($cart_row['BuyType']==2){//秒杀产品
            $seckill_row=str::str_code(db::get_one('sales_seckill', "SId='{$cart_row['KeyId']}' and RemainderQty>0 and {$c['time']} between StartTime and EndTime"));
        }
        //检查库存
        $Qty=cart::zhzcheck_product_stock(array(
            'CId'		=> $p_CId,
            'IsStock'	=> $IsStock,
            'BuyType'	=> $cart_row['BuyType'],
            'Qty'		=> $p_Qty,
            'ProdRow'	=> $products_row,
            'Combinatin'=> $combinatin_ary,
            'Seckill'	=> $seckill_row,
            'Tuan'		=> $tuan_row,
            'CartId'    => $p_CIdAry
        ));
        if($Qty===false) ly200::e_json('', 0);
        $Price=(int)$cart_row['BuyType']?$cart_row['Price']:cart::products_add_to_cart_price($products_row, $Qty);
        if($cart_row['BuyType']==0){//普通产品，计算价格，开始判断批发价和促销价
            // if($products_row['IsOpenAttrPrice']==0 || !$combinatin_ary || (int)$products_row['IsCombination']==0 || ($combinatin_ary && (int)$combinatin_ary[4]==1)){//没有属性，或者，属性类型是“加价”
            if((!$products_row['IsOpenAttrPrice'] && !$products_row['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $products_row['IsOpenAttrPrice']==1 && !$products_row['IsCombination']) || ($combinatin_ary && $products_row['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
                $Price=cart::products_add_to_cart_price($products_row, $Qty);
            }else{//属性类型是“单价”
                $Price=cart::products_add_to_cart_price($products_row, $Qty, (float)$combinatin_ary[0]);
            }
        }
        $IsWholesale=0;//是不是混批产品
        db::update('shopping_cart', "{$c['where']['cart']} and CId='{$p_CId}' and ProId='{$p_ProId}'", array(
                'Qty'	=>	$Qty,
                'Price'	=>	$Price
            )
        );
        if($cart_row['BuyType']==0){//检查产品混批功能
            $result=(float)cart::update_cart_wholesale_price($p_ProId, $products_row, $p_CId);//检查产品混批功能
            if($result){
                $Price=$result;
                $IsWholesale=1;
            }
            cart::update_cart_wholesale_price($p_ProId, $products_row);//检查产品混批功能
        }
        $Price=sprintf('%01.2f', $Price);
        //统计产品的改动单价计算
        $PriceAry=$AmountAry=array();
        $pro_list_row=db::get_all('shopping_cart', "{$c['where']['cart']} and ProId='{$p_ProId}'".($IsWholesale==0?" and CId='{$p_CId}'":''), 'CId, Price, PropertyPrice, Discount, Qty');
        foreach($pro_list_row as $k=>$v){
            $_p=cart::iconv_price(($v['Price']+$v['PropertyPrice'])*($v['Discount']<100?$v['Discount']/100:1), 2, '', 0);
            $PriceAry[$v['CId']]=$_p;
            $AmountAry[$v['CId']]=$_p*$v['Qty'];
        }
        $p_CIdAry=str::ary_format($p_CIdAry, 2);
        $tWhere=$c['where']['cart'].($IsBuyNow==1?" and CId='{$p_CId}'":'').($p_CIdAry?" and CId in({$p_CIdAry})":'');
        $total_weight=(float)db::get_sum('shopping_cart', $tWhere." and ProId='{$p_ProId}'", 'Weight*Qty');
        if(!$p_CIdAry){//一个列表产品都没有勾选
            $total_count=$total_price=$iconv_total_price=0;
        }else{//有勾选产品
            $total_count=(int)db::get_sum('shopping_cart', $tWhere, 'Qty');
            $total_price=(float)db::get_sum('shopping_cart', $tWhere, '(Price+PropertyPrice)*Discount/100*Qty');
            $iconv_total_price=(float)cart::cart_total_price(($IsBuyNow==1?" and c.CId='{$p_CId}'":'').($p_CIdAry?" and c.CId in({$p_CIdAry})":''), 1);
        }
        $cutArr=str::json_data(db::get_value('config', 'GroupId="cart" and Variable="discount"', 'Value'), 'decode');
        $DiscountData=cart::check_list_discounts($total_price, $iconv_total_price,1);//检查购物车列表的总价格变动后，优惠情况的变动
        ly200::e_json(array('qty'=>$Qty, 'price'=>$PriceAry, 'amount'=>$AmountAry, 'total_count'=>$total_count, 'total_price'=>$iconv_total_price, 'total_weight'=>$total_weight, 'cutprice'=>$DiscountData['cutprice'], 'IsCoupon'=>$DiscountData['IsCoupon'], 'FullCondition'=>$DiscountData['fullcondition']), 1);
    }

    public static function zhzplaceorder(){	//下订单 place an order
        global $c;
        @extract($_POST, EXTR_PREFIX_ALL, 'p');
        //初始化
//		file_put_contents("t2.txt","_POST=>".var_export($_POST,true).PHP_EOL.PHP_EOL,FILE_APPEND);
        if(!(int)$c['config']['global']['TouristsShopping']){
            $user=user::check_login('', 1);
        }

        $shipping_template=@in_array('shipping_template', (array)$c['plugins']['Used'])?1:0;
        //发货方式
        $Oversea=explode(',', $p_order_shipping_oversea);
        $SId=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_method_sid), 'decode');//发货方式
        $sInsurance=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_insurance), 'decode');//运费保险
        $sType=@str::json_data(str_replace(array('OvId_', '\\'), '', $p_order_shipping_method_type), 'decode');//海运或空运
        foreach($Oversea as $v){//丢失海外仓
            !$SId[$v] && !$shipping_template && ly200::e_json('', -2);
        }
        $SId_ary=array();
        foreach($SId as $k=>$v){
            ((!$v && $sType[$k]=='') || $v==-1) && ly200::e_json('', -2);
            $SId_ary[]=$k;
        }

        //付款方式
        $PId=(int)$p_order_payment_method_pid;
        (!$PId || $PId==-1) && ly200::e_json('', -3);
        $payment_row=str::str_code(db::get_one('payment', "IsUsed=1 and PId='$PId'"));
        //发货地址、账单地址
        $typeAddr=(int)$p_typeAddr;//是否非会员购物【0，否；1，是】
        if($typeAddr==1){
            $data_user=array('UserId'=>0, 'Email'=>$p_Email);
            !$data_user['Email'] && ly200::e_json('', -1);
            $tax_ary=$bill_tax_ary=array();
            $CountryCId = $p_CId ? $p_CId : $p_country_id;
            $address_country_row=db::get_one('country', "CId='$CountryCId'", 'Country, Code');
            $StateName=db::get_value('country_states', "CId='$CountryCId' and SId='{$p_Province}'", 'States');
            $address_row=array(
                'FirstName'		=>	str_replace(array('\\', '\\\\'), '', $p_FirstName),
                'LastName'		=>	str_replace(array('\\', '\\\\'), '', $p_LastName),
                'AddressLine1'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine1),
                'AddressLine2'	=>	str_replace(array('\\', '\\\\'), '', $p_AddressLine2),
                'CountryCode'	=>	($address_country_row['Code']?$address_country_row['Code']:(int)$p_CountryCode),
                'PhoneNumber'	=>	$p_PhoneNumber,
                'City'			=>	str_replace(array('\\', '\\\\'), '', $p_City),
                'SId'			=>	(int)$p_Province,
                'State'			=>	str_replace(array('\\', '\\\\'), '', $p_State),
                'StateName'		=>	$StateName,
                'CId'			=>	(int)$CountryCId,
                'Country'		=>	$address_country_row['Country'],
                'ZipCode'		=>	$p_ZipCode,
                'CodeOption'	=>	$p_CodeOption,
                'TaxCode'		=>	$p_TaxCode,
            );
            $bill_row=$address_row;
            $tax_ary=$bill_tax_ary=user::get_tax_info($address_row);
        }else{
            $data_user=array('UserId'=>$_SESSION['User']['UserId'], 'Email'=>$_SESSION['User']['Email']);
            $AId=(int)$p_order_shipping_address_aid;//收货地址ID
            (!$AId || $AId==-1 || !$data_user['Email']) && ly200::e_json('', -1);
            //收货地址
            $tax_ary=array();
            $address_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.{$c['where']['cart']} and a.AId='$AId' and a.IsBillingAddress=0", 'a.*, c.Country, s.States as StateName'));
            (!$address_row['FirstName'] || !$address_row['LastName'] || !$address_row['AddressLine1'] || !$address_row['City'] || !$address_row['CId'] || !$address_row['ZipCode'] || !$address_row['PhoneNumber']) && ly200::e_json('', -1);
            $tax_ary=user::get_tax_info($address_row);
            //账单地址
            $bill_tax_ary=array();
            $bill_row=str::str_code(db::get_one('user_address_book a left join country c on a.CId=c.CId left join country_states s on a.SId=s.SId', "a.{$c['where']['cart']} and a.IsBillingAddress=1", 'a.*, c.Country, s.States as StateName'));
            $bill_tax_ary=user::get_tax_info($bill_row);
        }
        //检查产品的信息是否正常
        $cart_where='';
        if($p_order_cid){
            $in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
            $cart_where=" and CId in({$in_where})";
        }
        //$cart_row=db::get_all('shopping_cart', $c['where']['cart'].$cart_where, "CId, BuyType, KeyId, PicPath, Price, Property, PropertyPrice, Discount, Qty, Weight as CartWeight, Volume, OvId, Remark, Language, SKU as CartSKU, Attr as CartAttr", 'CId desc');
        $cart_row=db::get_all('shopping_cart s left join products p on s.ProId=p.ProId', 's.'.$c['where']['cart'].$cart_where, 's.CId, s.ProId, s.BuyType, s.KeyId, s.PicPath, s.Price, s.Property, s.PropertyPrice, s.Discount, s.Qty, s.Weight as CartWeight, s.Volume, s.OvId, s.Remark, s.Language, s.SKU as CartSKU, s.Attr as CartAttr, p.IsFreeShipping, p.Wholesale, p.SoldStatus', 's.CId desc');
        !count($cart_row) && ly200::e_json('', -4);
        $stock_error='0';
        $IsFreeShipping=$ProductPrice=$totalWeight=$totalVolume=$s_totalWeight=$s_totalVolume=0;
        $pro_info_ary=array();//统计产品重量体积，用于运费计算
        foreach($cart_row as $key=>$val){
            //产品类型的处理
            if($shipping_template && !in_array($val['CId'], (array)$SId_ary)) ly200::e_json(array('cid'=>$val['CId']), -2);
            $Data=cart::zhzproduct_type(array(
                'Type'	=> $val['BuyType'],
                'KeyId'	=> $val['KeyId'],
                'Qty'	=> $val['Qty'],
                'Attr'	=> $val['CartAttr']
            ));
            if($Data===false) $stock_error.=",{$val['CId']}";
            $BuyType	= $Data['BuyType'];	//产品类型
            $KeyId		= $Data['KeyId'];	//主ID
            $ProId		= $Data['ProId'];	//产品ID
            $StartFrom	= $Data['StartFrom'];//产品目前的起订量
            $Price		= $Data['Price'];	//产品目前的单价
            $Qty		= $Data['Qty'];		//购买的数量
            $Discount	= $Data['Discount'];//折扣
            $Attr		= $Data['Attr'];	//产品属性
            $Weight		= $Data['Weight'];	//产品目前的重量
            $Volume		= $Data['ProdRow']['Volume'];//产品目前的体积
            $SKU		= $Data['ProdRow']['SKU'];//产品默认SKU
            $IsStock	= (int)$Data['ProdRow']['SoldStatus'];
            $seckill_row= $Data['SeckRow'];	//秒杀数据
            $OvId		= 1;//发货地ID
            $Qty<1 && $Qty=1;
            $cart_row[$key]['ProId']=$ProId;//记录产品ID
            $cart_row[$key]['Name']=$Data['ProdRow']['Name'.$c['lang']];//记录产品名称
            if($BuyType!=4){//“组合促销”除外
                !$Data['ProdRow']['Number'] && $stock_error.=",{$val['CId']}";//既不是组合促销，产品也同时不存在
                if(($IsStock!=1 && ($Data['ProdRow']['Stock']<$Qty || $Data['ProdRow']['Stock']<$Data['ProdRow']['MOQ'] || $Data['ProdRow']['Stock']<1)) || ($Data['ProdRow']['SoldOut']==1 && $Data['ProdRow']['IsSoldOut']==0) || ($Data['ProdRow']['SoldOut']==1 && $Data['ProdRow']['IsSoldOut']==1 && ($Data['ProdRow']['SStartTime']>$c['time'] || $c['time']>$Data['ProdRow']['SEndTime'])) || in_array($Data['ProdRow']['CateId'], $c['procate_soldout'])){//产品库存量不足（包括产品下架）
                    $stock_error.=",{$val['CId']}";
                }
            }
            //产品属性的处理
            if($BuyType!=4){
                $AttrData=cart::get_product_attribute(array(
                    'Type'			=> 0,//不用获取产品属性名称
                    'BuyType'		=> $BuyType,
                    'ProId'			=> $ProId,
                    'Price'			=> $Price,
                    'Attr'			=> $Attr,
                    'IsCombination'	=> $Data['ProdRow']['IsCombination'],
                    'IsAttrPrice'	=> $Data['ProdRow']['IsOpenAttrPrice'],
                    'SKU'			=> $Data['ProdRow']['SKU'],
                    'Weight'		=> $Weight
                ));
                if($AttrData===false) $stock_error.=",{$val['CId']}";
                $Price			= $AttrData['Price'];		//产品单价
                $PropertyPrice	= $AttrData['PropertyPrice'];//产品属性价格
                $combinatin_ary	= $AttrData['Combinatin'];	//产品属性的数据
                $OvId			= $AttrData['OvId'];		//发货地ID
                $ColorId		= $AttrData['ColorId'];		//颜色ID
                $Weight			= $AttrData['Weight'];		//产品重量
                $SKU			= $AttrData['SKU'];			//产品SKU
                $Attr			= $AttrData['Attr'];		//产品属性数据
            }
            if((int)$IsStock!=1 && (int)$Data['ProdRow']['IsCombination'] && $combinatin_ary && (!$combinatin_ary[1] || $Qty>$combinatin_ary[1])){//产品属性库存量不足
                $stock_error.=",{$val['CId']}";//没有无限库存、开启组合属性
            }
            if((int)$IsStock==1 && (int)$Data['ProdRow']['IsCombination']==0 && $combinatin_ary && $combinatin_ary[1]>0 && $Qty>$combinatin_ary[1]){//产品属性库存量不足
                $stock_error.=",{$val['CId']}";//开启无限库存、不是组合属性、属性库存不是0
            }
            //普通产品，重新计算价格，开始判断批发价和促销价
            if($BuyType==0){
                // if($Data['ProdRow']['IsOpenAttrPrice']==0 || !$combinatin_ary || (int)$Data['ProdRow']['IsCombination']==0 || ($combinatin_ary && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
                if((!$Data['ProdRow']['IsOpenAttrPrice'] && !$Data['ProdRow']['IsCombination']) || !$combinatin_ary || ($combinatin_ary && $Data['ProdRow']['IsOpenAttrPrice']==1 && !$Data['ProdRow']['IsCombination']) || ($combinatin_ary && $Data['ProdRow']['IsCombination']==1 && (int)$combinatin_ary[4]==1)){ //没有属性，或者，属性类型是“加价”
                    $Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty);
                }else{ //没有属性
                    $Price=cart::products_add_to_cart_price($Data['ProdRow'], $Qty, (float)$combinatin_ary[0]);
                }
            }
            //更新产品信息
            $update_data=array(
                'Weight'		=>	(float)($Weight?$Weight:$val['CartWeight']),
                'Volume'		=>	(float)sprintf('%01.3f', ($Volume?$Volume:$val['Volume'])),
                'Price'			=>	(float)sprintf('%01.2f', ($Price?$Price:$val['Price'])),
                'Qty'			=>	$Qty,
                'PropertyPrice'	=>	(float)sprintf('%01.2f', ($PropertyPrice?$PropertyPrice:$val['PropertyPrice'])),
                'Discount'		=>	(int)($Discount?$Discount:$val['Discount'])
            );
            if($BuyType==0){//检查产品混批功能
                $result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, $val['CId'], ($p_order_cid?1:0));//检查产品混批功能
                $result && $update_data['Price']=$result;
            }
            db::update('shopping_cart', "CId='{$val['CId']}'", $update_data);//更新购物车产品信息
            if($BuyType==0){//检查产品混批功能
                $result=(float)cart::update_cart_wholesale_price($val['ProId'], $val, $val['CId'], ($p_order_cid?1:0));//检查产品混批功能
                $result && $update_data['Price']=$result;
            }
            //整理产品的信息数据
            $cart_row[$key]=array_merge($cart_row[$key], $update_data);
            $item_price=($update_data['Price']+$update_data['PropertyPrice'])*($update_data['Discount']<100?$update_data['Discount']/100:1)*$Qty;
            $ProductPrice+=$item_price;
            $totalVolume+=($update_data['Volume']*$Qty);
            if($shipping_template){
                $CartId=(int)$val['CId'];
                if(!$pro_info_ary[$CartId]){
                    $pro_info_ary[$CartId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0, 'OvId'=>$OvId);
                }
                $pro_info_ary[$CartId]['tWeight']=($update_data['Weight']*$Qty);
                $pro_info_ary[$CartId]['tVolume']=($update_data['Volume']*$Qty);
                $pro_info_ary[$CartId]['tQty']=$Qty;
                $pro_info_ary[$CartId]['Price']=$item_price;
                if((int)$val['IsFreeShipping']==1){//免运费
                    $pro_info_ary[$CartId]['IsFreeShipping']=1; //其中有免运费
                }else{
                    $pro_info_ary[$CartId]['Weight']=($update_data['Weight']*$Qty);
                    $pro_info_ary[$CartId]['Volume']=($update_data['Volume']*$Qty);
                    $pro_info_ary[$CartId]['Qty']=$Qty;
                }
            }else{
                if(!$pro_info_ary[$OvId]){
                    $pro_info_ary[$OvId]=array('Weight'=>0, 'Volume'=>0, 'tWeight'=>0, 'tVolume'=>0, 'Price'=>0, 'IsFreeShipping'=>0);
                }
                $pro_info_ary[$OvId]['tWeight']+=($update_data['Weight']*$Qty);
                $pro_info_ary[$OvId]['tVolume']+=($update_data['Volume']*$Qty);
                $pro_info_ary[$OvId]['tQty']+=$Qty;
                $pro_info_ary[$OvId]['Price']+=$item_price;
                if((int)$val['IsFreeShipping']==1){//免运费
                    $pro_info_ary[$OvId]['IsFreeShipping']=1; //其中有免运费
                }else{
                    $pro_info_ary[$OvId]['Weight']+=($update_data['Weight']*$Qty);
                    $pro_info_ary[$OvId]['Volume']+=($update_data['Volume']*$Qty);
                    $pro_info_ary[$OvId]['Qty']+=$Qty;
                }
            }
            if(${'p_Remark_'.$val['ProId'].'_'.$val['CId']}){//记录备注
                $cart_row[$key]['Remark']=${'p_Remark_'.$val['ProId'].'_'.$val['CId']};
            }
        }
        $stock_error!='0' && ly200::e_json($stock_error, -6);//产品库存量不足
        /*
        //产品包装重量 用的人不多暂时注释
        $weight_where='';
        if($p_order_cid){
            $in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
            $weight_where=" and c.CId in({$in_where})";
        }
        $cartProAry=cart::cart_product_weight($weight_where, 1);
        foreach((array)$cartProAry['tWeight'] as $k=>$v){//$k是OvId
            foreach((array)$v as $k2=>$v2){//$k2是ProId
                $pro_info_ary[$k]['tWeight']+=$v2;
            }
        }
        foreach((array)$cartProAry['Weight'] as $k=>$v){//$k是OvId
            foreach((array)$v as $k2=>$v2){//$k2是ProId
                $pro_info_ary[$k]['Weight']+=$v2;
            }
        }
        */

        //优惠券处理
        $CouponCode='';
        $IsUseCoupon=$CouponPrice=$CouponDiscount=0;
        if($p_order_coupon_code){
            $coupon_row=self::get_coupon_info($p_order_coupon_code, cart::iconv_price($ProductPrice, 2, '', 0), $data_user['UserId'], $p_order_cid);
            if($coupon_row['status']==1){
                $CouponCode=addslashes($coupon_row['coupon']);
                if($coupon_row['type']==1){//CouponType: [0, 打折] [1, 减价格]
                    $CouponPrice=$coupon_row['cutprice'];
                }else{
                    if($coupon_row['pro_price']){
                        $CouponPrice=($coupon_row['pro_price']*(100-$coupon_row['discount'])/100);
                        $CouponPrice=cart::currency_price($CouponPrice, $_SESSION['Currency']['ExchangeRate'], $_SESSION['ManageCurrency']['ExchangeRate']);//换成后台默认的货币
                    }else{
                        $CouponDiscount=(100-$coupon_row['discount'])/100;
                    }
                }
                $IsUseCoupon=1;
            }
        }
        //“会员优惠”和“全场满减”之间的优惠对比
        $DisData=cart::discount_contrast($ProductPrice);
        $DiscountPrice	= $DisData['DiscountPrice'];	//全场满减的抵现金
        $Discount		= $DisData['Discount'];			//全场满减的折扣
        $UserDiscount	= $DisData['UserDiscount'];		//会员优惠的折扣
        //最低消费设置
        $_total_price=$ProductPrice*((100-$Discount)/100)*((($UserDiscount>0 && $UserDiscount<100)?$UserDiscount:100)/100)*(1-$CouponDiscount)-$CouponPrice-$DiscountPrice;//订单折扣后的总价
        if((int)$c['config']['global']['LowConsumption'] && cart::iconv_price($_total_price, 2, '', 0)<cart::iconv_price($c['config']['global']['LowPrice'], 2, '', 0)){
            ly200::e_json(cart::iconv_price($c['config']['global']['LowPrice'], 0, '', 0), -5);
        }
        /****************************2019/04/18 计算折扣后的产品价格数据 开始******************************/
        $pro_info_ary=orders::orders_discount_update_pro_info($pro_info_ary, $_total_price);
        /****************************2019/04/18 计算折扣后的产品价格数据 结束******************************/
        //计算快递运费
        if($_SESSION['User']['UserId']){ //获取省份ID
            if($AId){
                $address_aid=$AId;
            }else{
                $address_aid=(int)$_SESSION['Cart']['ShippingAddressAId'];
            }
            if($address_aid>0) $StatesSId=(int)db::get_value('user_address_book', "AId={$address_aid}", 'SId');
        }else{
            if($p_Province){
                $StatesSId=(int)$p_Province;
            }else{
                $StatesSId=(int)$_SESSION['Cart']['ShippingAddress']['Province'];
            }
        }
        $shipping_ary=orders::orders_shipping_method($SId, $address_row['CId'], $sType, $sInsurance, $pro_info_ary, $p_order_shipping_api, $StatesSId);
        !$shipping_ary && ly200::e_json('', -2);
        //总重量
        $totalWeight=cart::cart_product_weight($cart_where, 2);
        //游客，自动获取上次最新下单的业务员数据（相同邮箱地址）
        $SalesId=0;
        if((int)$data_user['UserId']==0){
            $SalesId=(int)db::get_value('user', "Email='{$data_user['Email']}' and SalesId>0", 'SalesId', 'UserId desc');
        }
        //分销订单
        if($_SESSION['DIST']){
            $source_row=db::get_one('user', "UserId='{$_SESSION['DIST']['UserId']}'");//分销上级的会员数据
            $DISTInfo=$source_row['DISTUId'].$source_row['UserId'].',';
        }

        //创建订单号
        while(1){
            $OId=date('ymdHis', $c['time']).mt_rand(10,99);
            if(!db::get_row_count('orders', "OId='$OId'")){ break; }
        }
        //添加总运费
        $total_shipping_price = 0;
        $total_qingugan = 0;
        if ($p_order_shipping_info)
        {
            $_arr=@str::json_data(str_replace(array('\\'), '', $p_order_shipping_info), 'decode');
            // 			    $_arr=json_decode($p_order_shipping_info,true);
            foreach ($_arr as $_item)
            {
                $total_shipping_price+=$_item["price"];
                $total_shipping_price+=$_item["qingguan"];
                $total_qingugan += $_item['qingguan'];
            }
        }

        if ($p_order_affix_price != $total_qingugan){
            //
        }
        $order_data=array(
            /*******************订单基本信息*******************/
            'OId'					=>	$OId,
            'UserId'				=>	$data_user['UserId'],
            'Source'				=>	ly200::is_mobile_client(0)?1:0,
            'RefererId'				=>	(int)$_COOKIE['REFERER'],
            'Email'					=>	$data_user['Email'],
            'SalesId'				=>	$SalesId,
            'Discount'				=>	$Discount,
            'DiscountPrice'			=>	$DiscountPrice,
            'UserDiscount'			=>	$UserDiscount,
            'ProductPrice'			=>	(float)substr(sprintf('%01.3f', $ProductPrice), 0, -1),//$ProductPrice,
            'Currency'				=>	$_SESSION['Currency']['Currency'],//当前货币
            'ManageCurrency'		=>	$_SESSION['ManageCurrency']['Currency'],//当前后台货币
            'Rate'					=>	$_SESSION['Currency']['Rate'],//当前货币的汇率
            'TotalWeight'			=>	$totalWeight,
            'TotalVolume'			=>	$totalVolume,
            'OrderTime'				=>	$c['time'],
            'UpdateTime'			=>	$c['time'],
            'Note'					=>	$p_Note,
            'DISTInfo'				=>	$DISTInfo,
            'IP'					=>	ly200::get_ip(),
            /*******************优惠券信息*******************/
            'CouponCode'			=>	$CouponCode,
            'CouponPrice'			=>	$CouponPrice,
            'CouponDiscount'		=>	$CouponDiscount,
            /*******************收货地址*******************/
            'ShippingFirstName'		=>	addslashes($address_row['FirstName']),
            'ShippingLastName'		=>	addslashes($address_row['LastName']),
            'ShippingAddressLine1'	=>	addslashes($address_row['AddressLine1']),
            'ShippingAddressLine2'	=>	addslashes($address_row['AddressLine2']),
            'ShippingCountryCode'	=>	'+'.$address_row['CountryCode'],
            'ShippingPhoneNumber'	=>	$address_row['PhoneNumber'],
            'ShippingCity'			=>	addslashes($address_row['City']),
            'ShippingState'			=>	addslashes($address_row['StateName']?$address_row['StateName']:$address_row['State']),
            'ShippingSId'			=>	$address_row['SId'],
            'ShippingCountry'		=>	addslashes($address_row['Country']),
            'ShippingCId'			=>	$address_row['CId'],
            'ShippingZipCode'		=>	$address_row['ZipCode'],
            'ShippingCodeOption'	=>	$tax_ary['CodeOption'],
            'ShippingCodeOptionId'	=>	$tax_ary['CodeOptionId'],
            'ShippingTaxCode'		=>	$tax_ary['TaxCode'],
            /*******************账单地址*******************/
            'BillFirstName'			=>	addslashes($bill_row['FirstName']),
            'BillLastName'			=>	addslashes($bill_row['LastName']),
            'BillAddressLine1'		=>	addslashes($bill_row['AddressLine1']),
            'BillAddressLine2'		=>	addslashes($bill_row['AddressLine2']),
            'BillCountryCode'		=>	'+'.$bill_row['CountryCode'],
            'BillPhoneNumber'		=>	$bill_row['PhoneNumber'],
            'BillCity'				=>	addslashes($bill_row['City']),
            'BillState'				=>	addslashes($bill_row['StateName']?$bill_row['StateName']:$bill_row['State']),
            'BillSId'				=>	$bill_row['SId'],
            'BillCountry'			=>	addslashes($bill_row['Country']),
            'BillCId'				=>	$bill_row['CId'],
            'BillZipCode'			=>	$bill_row['ZipCode'],
            'BillCodeOption'		=>	$bill_tax_ary['CodeOption'],
            'BillCodeOptionId'		=>	$bill_tax_ary['CodeOptionId'],
            'BillTaxCode'			=>	$bill_tax_ary['TaxCode'],
            /*******************发货方式*******************/
            'ShippingExpress'		=>	addslashes($shipping_ary['ShippingExpress']?$shipping_ary['ShippingExpress']:''),
            'ShippingMethodSId'		=>	addslashes($shipping_ary['ShippingMethodSId']?$shipping_ary['ShippingMethodSId']:''),
            'ShippingMethodType'	=>	addslashes($shipping_ary['ShippingMethodType']?$shipping_ary['ShippingMethodType']:''),
            'ShippingInsurance'		=>	addslashes($shipping_ary['ShippingInsurance']?$shipping_ary['ShippingInsurance']:''),
            'ShippingPrice'			=>	(float)substr(sprintf('%01.3f',$total_shipping_price), 0, -1),//$total_shipping_price,//$shipping_ary['ShippingPrice'],
            'ShippingInsurancePrice'=>	$shipping_ary['ShippingInsurancePrice'],
            'ShippingOvExpress'		=>	addslashes($shipping_ary['ShippingOvExpress']?$shipping_ary['ShippingOvExpress']:''),
            'ShippingOvSId'			=>	addslashes($shipping_ary['ShippingOvMethodSId']?$shipping_ary['ShippingOvMethodSId']:''),
            'ShippingOvType'		=>	addslashes($shipping_ary['ShippingOvMethodType']?$shipping_ary['ShippingOvMethodType']:''),
            'ShippingOvInsurance'	=>	addslashes($shipping_ary['ShippingOvInsurance']?$shipping_ary['ShippingOvInsurance']:''),
            'ShippingOvPrice'		=>	addslashes($shipping_ary['ShippingOvPrice']?$shipping_ary['ShippingOvPrice']:''),
            'ShippingOvInsurancePrice'=>addslashes($shipping_ary['ShippingOvInsurancePrice']?$shipping_ary['ShippingOvInsurancePrice']:''),
            'shipping_template'		=>	(int)$shipping_template,
            /*******************付款方式*******************/
            'PId'					=>	$PId,
            'PaymentMethod'			=>	$payment_row['Name'.$c['lang']],//db::get_value('payment', "IsUsed=1 and PId='$PId'", "Name{$c['lang']}"),
            'PayAdditionalFee'		=>	$payment_row['AdditionalFee'],
            'PayAdditionalAffix'	=>	$payment_row['AffixPrice'],
            /**********************add by jay********************/
            'DepositPrice'	        =>	$p_order_deposit_price,
            'QingguanPrice'	        =>	$total_qingugan
        );
        //生成订单
        db::insert('orders', $order_data);
        $OrderId=db::get_insert_id();
        //删除购物车相关的信息

        //确认这订单能够正常下单，才进行更新操作的优惠券信息
        $coupon_row=db::get_one('sales_coupon', "CouponNumber='{$coupon_row['coupon']}'");
        $coupon_row && $IsUseCoupon && user::sales_coupon_log($coupon_row, $OrderId);

        $_SESSION['Cart']['Coupon']='';

        unset($tax_ary, $shipping_ary, $coupon_row, $_SESSION['Cart']['Coupon']);
        //购物车产品的数据转移
        $i=1;
        $insert_sql='';
        $order_pic_dir=$c['orders']['path'].date('ym', $c['time'])."/{$OId}/";
        !is_dir($c['root_path'].$order_pic_dir) && file::mk_dir($order_pic_dir);
        foreach($cart_row as $v){
            $ext_name=file::get_ext_name($v['PicPath']);
            $ImgPath=$order_pic_dir.str::rand_code().'.'.$ext_name;
            if(count($cart_row)<200){
                @copy($c['root_path'].$v['PicPath'].'.240x240.'.$ext_name, $c['root_path'].$ImgPath);
            }
            $Name=str::str_code($v['Name'], 'addslashes');
            $SKU=str::str_code(($v['CartSKU']?$v['CartSKU']:$v['SKU']), 'addslashes');
            $Property=str::str_code($v['Property'], 'addslashes');
            $Remark=str::str_code($v['Remark'], 'addslashes');
            //新增 order_shipping_info list 开始
            $ShippingMethodSId=0;
            $ShippingPrice=0;
            $QingguanPrice=0;
            if ($p_order_shipping_info)
            {
                $_arr=@str::json_data(str_replace(array('\\'), '', $p_order_shipping_info), 'decode');
// 			    $_arr=json_decode($p_order_shipping_info,true);
//			    file_put_contents("t2.txt","_arr=>".var_export($_arr,true).PHP_EOL.PHP_EOL,FILE_APPEND);
                foreach ($_arr as $_item)
                {
                    if ($_item["proid"]==$v['ProId'])
                    {
                        $ShippingMethodSId=$_item["sid"];
                        $ShippingPrice=$_item["price"];
                        $QingguanPrice=$_item["qingguan"];
                        break;
                    }
                }
            }


            $insert_sql.=($i%100==1)?'insert into `orders_products_list` (OrderId, ProId, BuyType, KeyId, Name, SKU, PicPath, StartFrom, Weight, Price, Qty, Property, PropertyPrice, OvId, CId, Discount, Remark, Language, AccTime,ShippingMethodSId,ShippingPrice,QingguanPrice) VALUES':',';
            $insert_sql.="('$OrderId', '{$v['ProId']}', '{$v['BuyType']}', '{$v['KeyId']}', '{$Name}', '{$SKU}', '{$ImgPath}','{$v['StartFrom']}', '{$v['Weight']}', '{$v['Price']}', '{$v['Qty']}', '{$Property}', '{$v['PropertyPrice']}', '{$v['OvId']}','{$v['CId']}', {$v['Discount']}, '{$Remark}', '{$v['Language']}', '{$c['time']}', '{$ShippingMethodSId}', '{$ShippingPrice}', '{$QingguanPrice}')";
            ////新增 order_shipping_info list 结束

            if($i++%100==0){
                db::query($insert_sql);
                $insert_sql='';
            }
        }
        $insert_sql!='' && db::query($insert_sql);
        //确保数据转移成功才删除购物车
        $where=$c['where']['cart'];
        if($p_order_cid){
            $in_where=str::ary_format(@str_replace('.', ',', $p_order_cid), 2);
            $where.=" and CId in({$in_where})";
        }
        db::delete('shopping_cart', $where);

        //没登录会员的情况下单，利用邮箱判断是否为会员，是则分配到该会员，不是则创建临时会员，类似下单后自动注册会员
        orders::assign_member($order_data);
        /*
        //下单后自动注册会员
        if((int)$c['config']['global']['AutoRegister'] && !db::get_row_count('user', "Email='{$data_user['Email']}'")){
            //随机生成密码
            $PasswordStr=str::rand_code();
            $Password=ly200::password($PasswordStr);
            $time=$c['time'];
            $ip=ly200::get_ip();
            $data=array(
                'Language'		=>	'en',
                'FirstName'		=>	addslashes($address_row['FirstName']),
                'LastName'		=>	addslashes($address_row['LastName']),
                'Email'			=>	$data_user['Email'],
                'Password'		=>	$Password,
                'RegTime'		=>	$time,
                'RegIp'			=>	$ip,
                'LastLoginTime'	=>	$time,
                'LastLoginIp'	=>	$ip,
                'LoginTimes'	=>	1,
                'Status'		=>	1,
                'IsLocked'		=>	0
            );
            db::insert('user', $data);
            $UserId=db::get_insert_id();
            $data_oth=array(
                'UserId'		=>	$UserId,
                'FirstName'		=>	addslashes($address_row['FirstName']),
                'LastName'		=>	addslashes($address_row['LastName']),
                'AddressLine1'	=>	addslashes($address_row['AddressLine1']),
                'City'			=>	addslashes($address_row['City']),
                'State'			=>	addslashes($address_row['StateName']?$address_row['StateName']:$address_row['State']),
                'SId'			=>	$address_row['SId'],
                'CId'			=>	$address_row['CId'],
                'CodeOption'	=>	$tax_ary['CodeOption'],
                'TaxCode'		=>	$tax_ary['TaxCode'],
                'ZipCode'		=>	$address_row['ZipCode'],
                'CountryCode'	=>	$address_row['CountryCode'],
                'PhoneNumber'	=>	$address_row['PhoneNumber'],
                'AccTime'		=>	$time
            );
            db::insert('user_address_book', $data_oth);//Shipping Address
            $data_oth['IsBillingAddress']=1;
            db::insert('user_address_book', $data_oth);//Billing Address
            $_SESSION['User']=$data;
            $_SESSION['User']['UserId']=$UserId;
            db::update('orders', "OrderId='$OrderId'", array('UserId'=>$UserId));
            //更新会员等级
            $LId=(int)db::get_value('user_level', 'IsUsed=1 and FullPrice<=0', 'LId');
            if(!$_SESSION['User']['IsLocked'] && $LId){//没有固定会员等级
                db::update('user', "UserId='$UserId'", array('Level'=>$LId));
                $_SESSION['User']['Level']=$LId;
            }
            user::operation_log($UserId, '会员注册');
            if((int)$c['config']['email']['notice']['create_account']){ //邮件通知开关【会员注册】
                include($c['static_path'].'/inc/mail/create_account.php');
                ly200::sendmail($data_user['Email'], $mail_title, $mail_contents);
            }
            unset($_SESSION['Cart']['ShippingAddress']);
        }
        */

        unset($address_row, $data_oth);
        orders::orders_log((int)$data_user['UserId'], $data_user['UserId']?addslashes($_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName']):'System', $OrderId, 1, "Place an Order: ".$OId);
        if((int)$c['config']['global']['LessStock']==0){//下单减库存
            $orders_row=db::get_one('orders', "OrderId='$OrderId'");
            orders::orders_products_update(1, $orders_row);
        }
        if((int)db::get_value('system_email_tpl', 'Template="order_create"', 'IsUsed')){//下单后邮件通知
            $ToAry=array($data_user['Email']);
            include($c['static_path'].'/inc/mail/order_create.php');
            $c['config']['global']['AdminEmail'] && $ToAry[]=$c['config']['global']['AdminEmail'];
            ly200::sendmail($ToAry, $mail_title, $mail_contents);
            $c['config']['global']['OrdersSmsStatus'][1] && orders::orders_sms($OId);
        }
        $p_ReturnType=(int)$p_ReturnType;//数据反馈方式
        if($p_ReturnType==1){
            return array('OId'=>$OId, 'Method'=>$payment_row['Method']);
        }else{
            ly200::e_json(array('OId'=>$OId, 'Method'=>$payment_row['Method']), 1);
        }
    }

    public static function zhzcheck(){
        global $c;
        @extract($_GET, EXTR_PREFIX_ALL, 'p');

        $cart = db::get_all("shopping_cart","CId in ({$p_CId}) group by ProId",'*,sum(Qty) as Qtyall');
        foreach ($cart as $item) {
            $products_row=str::str_code(db::get_one('products', "ProId='{$item['ProId']}'"));
            if ($item['Qtyall'] < $products_row['MOQ']){
                ly200::e_json('', 0);
                break;
            }
        }
        ly200::e_json('',1);
    }
    /********************************** 多规加入购物车 end   **********************************/
}
?>