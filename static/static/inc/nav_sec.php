<?php
$down_count=count($allcate_ary[$nav['UId']]);
foreach((array)$allcate_ary[$nav['UId']] as $k1=>$v1){
    $down_count+=count($allcate_ary[$nav['UId'].$v1['CateId'].',']);
}
if(($nav['Nav']==3 && $nav['UId'] && count($allcate_ary[$nav['UId']])) || ($nav['Nav']==1 && db::get_row_count('article',"CateId='{$nav['Page']}'"))){//只有产品和单页有下拉
    $nav_img_count = 0;
    $img_lang = substr($c['lang'], 1);
    for($i=0;$i<4;$i++){
        if(!@is_file($c['root_path'].$v['PicPath_'.$i][$img_lang])) continue;
        $nav_img_count++;
    }
    ?>
    <div class="nav_sec <?=$nav_img_count==0 ? 'small_nav_sec' : ''; ?>">
        <div class="top"></div>
        <div class="nav_sec_box">
            <div class="<?=$nav_img_count==1 ? 'small' : ''; ?>">
                <div id="nav_sec_<?=$k; ?>" class="nav_list">
                    <?php if($nav['Nav']==3){  
                        foreach((array)$allcate_ary[$nav['UId']] as $k1 =>$v1){
                        ?>
                        <dl class="nav_sec_item">
                            <dt>
                                <a class="nav_sec_a" href="<?=ly200::get_url($v1, 'products_category');?>" title="<?=$v1['Category'.$c['lang']];?>">
                                    <?=$v1['Category'.$c['lang']];?>
                                    <?=count($allcate_ary[$nav['UId'].$v1['CateId'].',']) ? '<em></em>' : ''; ?>
                                </a>
                            </dt>
                            <?php if(count($allcate_ary[$nav['UId'].$v1['CateId'].','])){?>
                            <dd class="nav_thd_list">
                                <?php
                                foreach((array)$allcate_ary[$nav['UId'].$v1['CateId'].','] as $k2=>$v2){
                                    $nn=$v2['Category'.$c['lang']];
                                    if($nav_img_count && $k2>4){
                                    ?>
                                        <?php if($k2==5){ ?>
                                            <div class="nav_thd_item more_box">
                                                <a class="more" href="<?=ly200::get_url($v1, 'products_category');?>" title="<?=$c['lang_pack']['more']; ?> >>"><?=$c['lang_pack']['more']; ?> (<?=count($allcate_ary[$nav['UId'].$v1['CateId'].','])-5; ?>)</a>
                                                <div class="nav_four_item">
                                        <?php } ?>
                                                <a href="<?=ly200::get_url($v2, 'products_category');?>" title="<?=$nn;?>"><?=$nn;?></a>
                                        <?php if((count($allcate_ary[$nav['UId'].$v1['CateId'].','])-1)==$k2){ ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php }else{ ?>
                                        <div class="nav_thd_item">
                                            <a href="<?=ly200::get_url($v2, 'products_category');?>" title="<?=$nn;?>">
                                                <?=$nn;?>
                                                <?=count($allcate_ary[$nav['UId'].$v1['CateId'].','.$v2['CateId'].',']) ? '<em></em>' : ''; ?>
                                            </a>
                                            <?php if(count($allcate_ary[$nav['UId'].$v1['CateId'].','.$v2['CateId'].','])){ ?>
                                                <div class="nav_four_item">
                                                    <?php
                                                    foreach((array)$allcate_ary[$nav['UId'].$v1['CateId'].','.$v2['CateId'].','] as $k3=>$v3){
                                                        $nnn=$v3['Category'.$c['lang']];
                                                        ?>
                                                        <a href="<?=ly200::get_url($v3, 'products_category');?>" title="<?=$nnn;?>"><?=$nnn;?></a>
                                                    <?php }?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php }?>
                            </dd>
                            <?php } ?>
                        </dl>
                    <?php } 
                    }else{ 
                        $article_nav_row=db::get_all('article', "CateId='{$nav['Page']}'", "AId,Title{$c['lang']},PageUrl,Url", $c['my_order'].'AId desc');
                        foreach((array)$article_nav_row as $v1){
                        ?>
                        <dl class="nav_sec_item">
                            <dt>
                                <a class="nav_sec_a" href="<?=ly200::get_url($v1, 'article');?>" title="<?=$v1['Title'.$c['lang']];?>">
                                    <?=$v1['Title'.$c['lang']];?>
                                </a>
                            </dt>
                        </dl>
                    <?php }
                    } ?>
                    <div class="clear"></div>
                </div>
                <div class="nav_img <?=$nav_img_count==1 ? 'small' : ''; ?>">
                    <?php 
                        for($i=0;$i<4;$i++){
                            if(!@is_file($c['root_path'].$v['PicPath_'.$i][$img_lang])) continue;
                        ?>
                        <div class="imgl">
                            <?php
                                $img_str='';
                                $v['Url_'.$i][$img_lang] && $img_str.="<a href='{$v['Url_'.$i][$img_lang]}' title='{$v['ImgName_'.$i][$img_lang]}{$v['Brief_'.$i][$img_lang]}'>";
                                $img_str.="<img src='{$v['PicPath_'.$i][$img_lang]}' alt='{$v['ImgName_'.$i][$img_lang]}' />";
                                $v['Url_'.$i][$img_lang] && $img_str.="</a>";
                                echo $img_str;
                            ?>
                        </div>
                    <?php } ?>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php }?>