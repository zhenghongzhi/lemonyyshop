<?php !isset($c) && exit();?>
<?php
$fontCss="/static/themes/{$c['theme']}/css/font.css";
if(is_file($c['root_path'].$fontCss)){
	echo '<link href="'.$fontCss.'" rel="stylesheet" type="text/css">';
}
echo ly200::load_static(
	'/static/css/visual.css',
	'/static/css/global.css',
	'/static/themes/default/css/global.css',
	'/static/themes/default/css/user.css',
	"/static/themes/{$c['theme']}/css/style.css",
	'/static/js/jquery-1.7.2.min.js',
	'/static/js/lang/'.substr($c['lang'], 1).'.js',
	'/static/js/global.js',
	'/static/themes/default/js/global.js',
	'/static/themes/default/js/user.js',
	"/static/themes/{$c['theme']}/js/main.js"
);
development::load_link_file('/static/themes/default/js/development/user.js');
if($c['FunVersion']>=10){
	echo ly200::load_static('/static/css/cdx_global.css');
}
$MainConfig=str::json_data(db::get_value('config_module', "Themes='{$c['theme']}'", 'StyleData'), 'decode');
$MainColor=$MainConfig['FontColor'];
if(is_file($c['root_path']."/static/themes/{$c['theme']}/css/skin_demo.css")){
	$file_css=$c['root_path']."/static/themes/{$c['theme']}/css/skin.css";
	$time_css=@filemtime($file_css);
	$c['cache_timeout']=120;
	$set_cache_css=$c['time']-$time_css-$c['cache_timeout'];
	if($set_cache_css>0){
		$CustomCssFile=str_replace('{$MainColor}', $MainColor, ly200::curl(ly200::get_domain()."/static/themes/{$c['theme']}/css/skin_demo.css"));
		file::write_file("/static/themes/{$c['theme']}/css/", 'skin.css', $CustomCssFile);
	}
}
$CustomCss="/static/themes/{$c['theme']}/css/skin.css";
if(is_file($c['root_path'].$CustomCss)){
	echo '<link href="'.$CustomCss.'" rel="stylesheet" type="text/css">';
}

if(in_array('facebook_pixel', $c['plugins']['Used'])){
	//Advanced Matching in Pixel
	if((int)$_SESSION['User']['UserId']){//会员个人信息
		$fae_address_row=str::str_code(db::get_all('user_address_book', "UserId='{$_SESSION['User']['UserId']}' and IsBillingAddress=0", '*', 'AccTime desc, AId desc'));
	}elseif($_SESSION['Cart']['ShippingAddress']){ //非会员个人信息
		$address_ary=$_SESSION['Cart']['ShippingAddress'];
		$fae_address_row[0]=$address_ary;
		unset($address_ary);
	}
	$pii='{';
	$_SESSION['User']['Email'] && $pii.="em: '{$_SESSION['User']['Email']}',";//Email
	$fae_address_row[0]['PhoneNumber'] && $pii.=($pii!='{'?'':'')."ph: '{$fae_address_row[0]['PhoneNumber']}',";//Phone Number
	$fae_address_row[0]['FirstName'] && $pii.="fn: '{$fae_address_row[0]['FirstName']}'";//First Name
	$pii.='}';
?>
	<!-- Facebook Pixel Code -->
	<script type="text/javascript">
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '<?=$c['config']['Platform']['Facebook']['Pixel']['Data']['PixelID'];?>', <?=$pii;?>, {'agent':'plueeshop'});
	fbq('track', "PageView");
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?=$c['config']['Platform']['Facebook']['Pixel']['Data']['PixelID'];?>&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
<?php
}

if(in_array('google_pixel', $c['plugins']['Used'])){
    //GA代码
?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?=$c['config']['Platform']['Google']['Pixel']['Data']['TrackingID']?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '<?=$c['config']['Platform']['Google']['Pixel']['Data']['TrackingID']?>');
    </script>
<?php
}

if(in_array('paypal_marketing_solution', $c['plugins']['Used'])){
    //PPMS Partner Activate
	$container_data=str::json_data(str::str_code(db::get_value('config', 'GroupId="paypal" and Variable="MarketingSolution"', 'Value'), 'htmlspecialchars_decode'), 'decode');
	if($container_data['ID']){
?>
        <!-- PayPal BEGIN -->
        <script type="text/javascript">
        ;(function(a,t,o,m,s){a[m]=a[m]||[];a[m].push({t:new Date().getTime(),event:'snippetRun'});var f=t.getElementsByTagName(o)[0],e=t.createElement(o),d=m!=='paypalDDL'?'&m='+m:'';e.async=!0;e.src='https://www.paypal.com/tagmanager/pptm.js?id='+s+d;f.parentNode.insertBefore(e,f);})(window,document,'script','paypalDDL','<?=$container_data['ID'];?>');
        </script>
        <!-- PayPal END -->
<?php
	}
}?>