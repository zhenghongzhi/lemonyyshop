<?php !isset($c) && exit();?>
<?php
if((int)$_SESSION['User']['UserId']){
	//已登录
	// if($c['FunVersion']>=1){
		//会员站内信未读数量统计
		$msg_where="(UserId like '%|{$_SESSION['User']['UserId']}|%' or UserId='-1') and Type=1";	//兼容3.0系统
		$msg_where.=" or (UserId='{$_SESSION['User']['UserId']}' and Module='others' and IsReply=1)";
		$user_msg_row=str::str_code(db::get_all('user_message', $msg_where, '*', 'MId desc'));
		$user_msg_len=0;
		foreach((array)$user_msg_row as $k=>$v){
			$is_read=0;
			if($v['UserId']==$_SESSION['User']['UserId']){
				$user_msg_len+=1;
			}else{	//兼容3.0系统,3.0系统UserId有多个
				if($v['IsRead']){
					$userid_ary=@array_flip(explode('|', $v['UserId']));
					$isread_ary=@explode('|', $v['IsRead']);
					$is_read=$isread_ary[$userid_ary[$_SESSION['User']['UserId']]];
					!$is_read && $user_msg_len+=1;
				}
			}
		}
		$user_prod_msg=db::get_row_count('user_message',"UserId='{$_SESSION['User']['UserId']}' and Module='products' and IsReply=1");
		$user_order_msg=db::get_row_count('user_message',"UserId='{$_SESSION['User']['UserId']}' and Module='orders' and IsReply=1");
		$user_rfq_msg=db::get_row_count('request_message',"UserId='{$_SESSION['User']['UserId']}' and Module='purchase' and IsReply=1");
	// }
	$_UserName=substr($c['lang'], 1)=='jp'?$_SESSION['User']['LastName'].' '.$_SESSION['User']['FirstName']:$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName'];
?>
	<div class="FontColor fl"><?=$c['lang_pack']['welcome'];?>&nbsp;</div>
	<dl class="fl">
		<dt><a rel="nofollow" href="/account/" class="FontColor"><span><?=($_SESSION['User']['FirstName'] || $_SESSION['User']['LastName'])?$_UserName:$_SESSION['User']['Email'];?></span><?=($user_msg_len || $user_prod_msg || $user_order_msg)?'<b></b>':'';?></a></dt>
		<dd class="user">
			<a rel="nofollow" href="/account/rfq/"><?=$c['lang_pack']['my_rfq'];?><?=$user_rfq_msg?'<b class="inbox_tips FontBgColor">'.$user_rfq_msg.'</b>':'';?></a>
			<a rel="nofollow" href="/account/balance/"><?=$c['lang_pack']['my_balance'];?></a>
			<a rel="nofollow" href="/account/orders/"><?=$c['lang_pack']['my_orders'];?><?=$user_order_msg?'<b class="inbox_tips FontBgColor">'.$user_order_msg.'</b>':'';?></a>
            <a rel="nofollow" href="/account/pruchaseorder/"><?= $c['lang_pack']['user']['zhz_pruchaseorders']; ?></a>
            <a rel="nofollow" href="/account/favorite/"><?=$c['lang_pack']['my_fav'];?></a>
			<a rel="nofollow" href="/account/coupon/"><?=$c['lang_pack']['my_coupon'];?></a>
        	<a rel="nofollow" href="/account/inbox/"><?=$c['lang_pack']['my_inbox'];?><?=$user_msg_len?'<b class="inbox_tips FontBgColor">'.$user_msg_len.'</b>':'';?></a>
			<?php if(in_array('product_inbox', $c['plugins']['Used'])){//APP应用开启产品咨询?>
        		<a rel="nofollow" href="/account/inbox/?Pro=1"><?=$c['lang_pack']['my_qa'];?><?=$user_prod_msg?'<b class="inbox_tips FontBgColor">'.$user_prod_msg.'</b>':'';?></a>
			<?php }?>
			<a rel="nofollow" href="/account/logout.html"><?=$c['lang_pack']['sign_out'];?></a>
		</dd>
	</dl>
<?php
}else{
	//未登录
	$sign_count=0;
	foreach($c['config']['Platform'] as $k=>$v){
		//$v['SignIn']['IsUsed']==1 && $sign_count+=1;
		in_array(strtolower($k).'_login', $c['plugins']['Used'])==1 && $sign_count+=1;
	}
	?>
		<dl>

            <dt<?=($sign_count>0)?'':' class="not_dd"';?>><a rel="nofollow" href="javascript:;" class="SignInButton FontColor"><?=$c['lang_pack']['sign_in'];?></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a rel="nofollow" href="/account/sign-up.html" class="FontColor"><?=$c['lang_pack']['join_free'];?></a></dt>
			<?php if($sign_count>0){?>
			<dd class="login">
				<?php
				$facebook_data=$c['config']['Platform']['Facebook']['SignIn'];
				if(in_array('facebook_login', $c['plugins']['Used']) && $facebook_data['Data']['appId']){
					echo ly200::load_static('/static/js/oauth/facebook.js');
				?>
					<fb:login-button scope="public_profile,email" onlogin="checkLoginState();" class="fb-login-button fb-bg-login-button" data-size="medium" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false" data-width="184px" data-height="32px"></fb:login-button>
				<?php
				}
				$twitter_data=$c['config']['Platform']['Twitter']['SignIn'];
				if(in_array('twitter_login', $c['plugins']['Used']) && $twitter_data['Data']['CONSUMER_KEY'] && $twitter_data['Data']['CONSUMER_SECRET']){
					echo ly200::load_static('/static/js/oauth/twitter.js');
				?>
					<span class="twitter_button" id="twitter_btn" key="<?=base64_encode(~$twitter_data['Data']['CONSUMER_KEY']);?>" secret="<?=base64_encode(~$twitter_data['Data']['CONSUMER_SECRET']);?>" callback="<?=urlencode(ly200::get_domain().$c['config']['Platform']['Twitter']['ReturnUrl']);?>">
                    	<span class="icon"></span>
                    	<span class="text">Log In with Twitter</span>
                    </span>
				<?php
				}
				$google_data=$c['config']['Platform']['Google']['SignIn'];
				if(in_array('google_login', $c['plugins']['Used']) && $google_data['Data']['clientid']){
					echo '<meta name="google-signin-scope" content="profile email"><meta name="google-signin-client_id" content="'.$google_data['Data']['clientid'].'"><script src="https://apis.google.com/js/platform.js" async defer></script>';
                    echo ly200::load_static('/static/js/oauth/google.js');
				?>
					<div class="google_button">
						<div class="g-signin2" id="google_btn" data-onsuccess="GoogleSignIn" data-theme="dark" data-login-status="">&nbsp;</div>
						<span class="icon"></span>
						<span class="button_text">Log In with Google</span>
					</div>
				<?php
				}
				$paypal_data=$c['config']['Platform']['Paypal']['SignIn'];
				if(in_array('paypal_login', $c['plugins']['Used']) && $paypal_data['Data']['client_id']){
					echo ly200::load_static('/static/js/oauth/paypal/api.js');
					$_domain=!$paypal_data['Data']['domain']?ly200::get_domain():$paypal_data['Data']['domain'];
				?>
					<div id="paypalLogin" appid="<?=$paypal_data['Data']['client_id'];?>" u="<?=htmlspecialchars_decode(rtrim($_domain, '/').$c['config']['Platform']['Paypal']['ReturnUrl']);?>" scopes="<?=(int)db::get_row_count('user', 'PaypalId like "%@%"', 'UserId'); ?>"></div>
				<?php
                }
				$vk_data=$c['config']['Platform']['VK']['SignIn'];
				if(in_array('vk_login', $c['plugins']['Used']) && $vk_data['Data']['apiId']){
					echo ly200::load_static('/static/js/oauth/vk.js');
				?>
					<div id="vk_button" class="vk_button" apiid="<?=$vk_data['Data']['apiId']?>">
						<span class="icon"></span>
						<span class="button_text">Log In with VK</span>
					</div>
                <?php
				}
				/*
				$instagram_data=$c['config']['Platform']['Instagram']['SignIn'];
				if(in_array('instagram_login', $c['plugins']['Used']) && $instagram_data['Data']['client_id'] && $instagram_data['Data']['client_secret']){
					echo ly200::load_static('/static/js/oauth/instagram.js');
				?>
					<div id="instagram_button" class="instagram_button" client_id="<?=$instagram_data['Data']['client_id']?>" client_secret="<?=$instagram_data['Data']['client_secret']?>" redirect_uri="<?=(rtrim(ly200::get_domain(),'/').'/?login_with_instagram=1');?>" login="<?=(int)$_GET['login_with_instagram'] ? 1 : 0; ?>" code="<?=trim($_GET['code']); ?>">
						<span class="icon"></span>
						<span class="button_text">Log In with Instagram</span>
					</div>
                <?php }
				*/?>
			</dd>
		<?php }?>
	</dl>
	<script type="text/javascript">
	$(document).ready(function(){
		user_obj.sign_in_init();
		<?php
		//默认打开登录框
		if((int)$c['config']['global']['UserLogin'] && !$_SESSION['User']['UserLoginTime']){
			echo "$('.SignInButton').click();";
			$_SESSION['User']['UserLoginTime']=$c['time'];
		}?>
	});
	</script>
<?php }?>