<?php !isset($c) && exit();?>
<?php 
$chat_row=db::get_all('chat', '1', '*', $c['my_order'].'CId asc');
foreach((array)$chat_row as $k=>$v){
    $type_row_ary[$v['Type']][]=$v;
}
?>
<div id="chat_window">
    <div class="chat_box <?=$c['config']['global']['IsFloatChat'] ? 'cur' : ''; ?>">
        <div class="box"> 
            <?php foreach((array)$type_row_ary as $k => $v){ 
                ?>
                <?php if(count($v)>1 || $k==4){ ?>
                <div class="chat_item chat_<?=strtolower($c['chat']['type'][$k]); ?>" >
                    <div class="abs">
                        <div class="more_box">
                            <?php 
                            foreach((array)$v as $v1){ 
                                $link = $k==4 ? 'javascript:;' : sprintf($c['chat']['link'][$v1['Type']],$v1['Account']);
                                ?>
                                <a href="<?=$link; ?>" target="_blank" class="item">
                                    <?=$v1['Name'];?>
                                    <?php if($k==4){ ?>
                                        <div class="relimg"><img src="<?=$v1['PicPath']?>" alt=""></div>
                                    <?php } ?>
                                </a>
                            <?php } ?>
                        </div>
                    </div>                
                </div>
                <?php }else{ ?>
                    <a href="<?=sprintf($c['chat']['link'][$v[0]['Type']],$v[0]['Account']); ?>" title="<?=$v[0]['Name']; ?>" target="_blank" class="chat_item chat_<?=strtolower($c['chat']['type'][$k]); ?>"></a>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="menu">
            <?php if($chat_row){ ?>
            <a href="javascript:;" class="more"></a>
            <?php } ?>
            <a href="javascript:;" id="go_top" class="top"></a>
        </div>
    </div>
</div>
<?php
//交换链接

if($c['plugin_app']->trigger('swap_chain', '__config', 'get_urls_list')=='enable'){
	$c['plugin_app']->trigger('swap_chain', 'get_urls_list');
}
if(in_array('facebook_messenger', $c['plugins']['Used'])){
	$messenger_url='https://m.me/'.$c['config']['Platform']['Facebook']['PageId']['Data']['page_id'];
	$messenger_data=str::str_code(db::get_value('config', 'GroupId="facebook" and Variable="facebook_messenger"', 'Value'));
	$messenger_data=str::json_data(htmlspecialchars_decode($messenger_data), 'decode');
	// attribution="fbe_ueeshop"
	if($messenger_data){ //新
		echo '<div class="fb-customerchat" page_id="'.$messenger_data['page_id'].'" theme_color="'.$messenger_data['customization']['themeColorCode'].'" logged_in_greeting="'.$messenger_data['customization']['greetingTextCode'].'" logged_out_greeting="'.$messenger_data['customization']['greetingTextCode'].'"></div>';
	}else{ //旧
		echo '<div id="facebook-messenger" class="message_us"><a href="'.$messenger_url.'" target="_blank"><img src="/static/ico/message-us-blue.png" alt="Message Us" /></a></div>';
	}
}?>
<?php
//加载JavaScript版 Facebook SDK (Facebook登录 | Facebook Messenger)
$facebook_sign_data=$c['config']['Platform']['Facebook']['SignIn'];
$facebook_page_data=$c['config']['Platform']['Facebook']['PageId'];
if(($facebook_sign_data['Data']['appId'] && in_array('facebook_login', $c['plugins']['Used']) || ($facebook_page_data['Data']['page_id'] && in_array('facebook_messenger', $c['plugins']['Used'])))){
    $AppId=$facebook_page_data['Data']['page_id'];  
    if($facebook_sign_data['Data']['appId'] && in_array('facebook_login', $c['plugins']['Used'])){
        //Facebook登录 最优先
        $AppId=$facebook_sign_data['Data']['appId'];
    }
    
    switch(substr($c['lang'], 1)){
        case 'de': $fb_lang='de_DE'; break;
        case 'es': $fb_lang='es_ES'; break;
        case 'fr': $fb_lang='fr_FR'; break;
        case 'jp': $fb_lang='ja_JP'; break;
        case 'pt': $fb_lang='pt_PT'; break;
        case 'ru': $fb_lang='ru_RU'; break;
        case 'zh_tw': $fb_lang='zh_HK'; break;
        default: $fb_lang='en_US'; break;
    }
?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/<?=$fb_lang;?>/sdk/xfbml.customerchat.js#xfbml=1&version=<?=$messenger_data['facebook_jssdk_version']?$messenger_data['facebook_jssdk_version']:'v3.2';?>"></script>
    <script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId           : '<?=$AppId;?>',
            autoLogAppEvents: true,
            cookie          : true,
            xfbml           : true,
            version         : '<?=$messenger_data['facebook_jssdk_version']?$messenger_data['facebook_jssdk_version']:'v3.2';?>'
        });

    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/<?=$fb_lang;?>/sdk/xfbml.customerchat.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
<?php }?>
