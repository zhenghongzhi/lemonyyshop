<?php !isset($c) && exit();?>
<?php
if((int)$_SESSION['User']['UserId']){
	//已登录
	// if($c['FunVersion']>=1){
		//会员站内信未读数量统计
		$msg_where="(UserId like '%|{$_SESSION['User']['UserId']}|%' or UserId='-1') and Type=1";	//兼容3.0系统
		$msg_where.=" or (UserId='{$_SESSION['User']['UserId']}' and Module='others' and IsReply=1)";
		$user_msg_row=str::str_code(db::get_all('user_message', $msg_where, '*', 'MId desc'));
		$user_msg_len=0;
		foreach((array)$user_msg_row as $k=>$v){
			$is_read=0;
			if($v['UserId']==$_SESSION['User']['UserId']){
				$user_msg_len+=1;
			}else{	//兼容3.0系统,3.0系统UserId有多个
				if($v['IsRead']){
					$userid_ary=@array_flip(explode('|', $v['UserId']));
					$isread_ary=@explode('|', $v['IsRead']);
					$is_read=$isread_ary[$userid_ary[$_SESSION['User']['UserId']]];
					!$is_read && $user_msg_len+=1;
				}
			}
		}
		$user_prod_msg=db::get_row_count('user_message',"UserId='{$_SESSION['User']['UserId']}' and Module='products' and IsReply=1");
		$user_order_msg=db::get_row_count('user_message',"UserId='{$_SESSION['User']['UserId']}' and Module='orders' and IsReply=1");
	// }
	$_UserName=substr($c['lang'], 1)=='jp'?$_SESSION['User']['LastName'].' '.$_SESSION['User']['FirstName']:$_SESSION['User']['FirstName'].' '.$_SESSION['User']['LastName'];
?>
<div class="global_account_sec">
	<div class="AccountButton_sec"><?=($_SESSION['User']['FirstName'] || $_SESSION['User']['LastName'])?$_UserName:$_SESSION['User']['Email'];?></div>
    <div class="account_container_sec">
    	<div class="account_box_sec">
            <div class="rows"><a rel="nofollow" href="/account/rfq/"><?=$c['lang_pack']['my_rfq'];?></a></div>
            <div class="rows"><a rel="nofollow" href="/account/balance/"><?=$c['lang_pack']['my_balance'];?></a></div>
            <div class="rows"><a rel="nofollow" href="/account/orders/"><?=$c['lang_pack']['my_orders'];?><?php if($user_order_msg){?><b class="FontBgColor"><?=$user_order_msg?></b><?php }?></a></div>
            <div class="rows"><a rel="nofollow" href="/account/favorite/"><?=$c['lang_pack']['my_fav'];?></a></div>
            <div class="rows"><a rel="nofollow" href="/account/coupon/"><?=$c['lang_pack']['my_coupon'];?></a></div>
        	<div class="rows"><a rel="nofollow" href="/account/inbox/"><?=$c['lang_pack']['my_inbox'];?><?php if($user_msg_len){?><b class="FontBgColor"><?=$user_msg_len?></b><?php }?></a></div>
			<?php if(in_array('product_inbox', $c['plugins']['Used'])){//APP应用开启产品咨询?>
				<div class="rows"><a rel="nofollow" href="/account/inbox/?Pro=1"><?=$c['lang_pack']['my_qa'];?><?php if($user_prod_msg){?><b class="FontBgColor"><?=$user_prod_msg?></b><?php }?></a></div>
			<?php }?>
            <div class="btn"><a class="FontBorderColor FontColor" rel="nofollow" href="/account/logout.html"><?=$c['lang_pack']['sign_out'];?></a></div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var timer;
	$('.global_account_sec').hover(
		function(){
			clearTimeout(timer);
			$(this).find('.AccountButton_sec').addClass('cur');
			$(this).find('.account_container_sec').fadeIn();
		},
		function(){
			var _this=$(this);
			timer=setTimeout(function(){
				_this.find('.AccountButton_sec').removeClass('cur');
				_this.find('.account_container_sec').fadeOut();
			},500);
		}
	);
});
</script>
<?php
}else{
	//未登录
	$sign_count=0;
	foreach($c['config']['Platform'] as $k=>$v){
		//$v['SignIn']['IsUsed']==1 && $sign_count+=1;
		in_array(strtolower($k).'_login', $c['plugins']['Used'])==1 && $sign_count+=1;
	}
?>
<div class="global_login_sec">
	<div class="SignInButton_sec"></div>
    <div class="signin_box_sec global_signin_module">
    	<div class="signin_container">
            <form class="signin_form" name="signin_form" method="post" autocomplete="off">
                <input name="Email" class="g_s_txt" type="text" autocomplete="off" maxlength="100" placeholder="<?=$c['lang_pack']['emailAddress'];?>" format="Email" notnull />
                <div class="blank20"></div>
                <input name="Password" class="g_s_txt" type="text" onfocus="this.type='password'" autocomplete="new-password" placeholder="<?=$c['lang_pack']['user']['password'];?>" notnull />
                <button class="signin FontBgColor" type="submit"><?=$c['lang_pack']['user']['signIn'];?></button>
                <input type="hidden" name="do_action" value="user.login">
            </form>
            <div id="error_login_box" class="error_login_box"></div>
            <?php if($sign_count>0){?>
                <h4><?=$c['lang_pack']['user']['signInWith'];?>:</h4>
                <ul>
                    <?php
					$facebook_data=$c['config']['Platform']['Facebook']['SignIn'];
					if(in_array('facebook_login', $c['plugins']['Used']) && $facebook_data['Data']['appId']){
                        echo ly200::load_static('/static/js/oauth/facebook.js');
                    ?>
                        <li><fb:login-button scope="public_profile,email" onlogin="checkLoginState();" class="fb-login-button" data-size="medium" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="false" data-width="200px"></fb:login-button></li>
                    <?php
                    }
					$twitter_data=$c['config']['Platform']['Twitter']['SignIn'];
					if(in_array('twitter_login', $c['plugins']['Used']) && $twitter_data['Data']['CONSUMER_KEY'] && $twitter_data['Data']['CONSUMER_SECRET']){
                        echo ly200::load_static('/static/js/oauth/twitter.js');
                    ?>
                        <li id="twitter_btn" key="<?=base64_encode(~$twitter_data['Data']['CONSUMER_KEY']);?>" secret="<?=base64_encode(~$twitter_data['Data']['CONSUMER_SECRET']);?>" callback="<?=urlencode(ly200::get_domain().$c['config']['Platform']['Twitter']['ReturnUrl']);?>"></li>
                    <?php
                    }
					$google_data=$c['config']['Platform']['Google']['SignIn'];
					if(in_array('google_login', $c['plugins']['Used']) && $google_data['Data']['clientid']){
						echo '<meta name="google-signin-scope" content="profile email"><meta name="google-signin-client_id" content="'.$google_data['Data']['clientid'].'"><script src="https://apis.google.com/js/platform.js" async defer></script>';
                    	echo ly200::load_static('/static/js/oauth/google.js');
                    ?>
                        <li id="google_login"><div class="g-signin2" id="google_btn" data-onsuccess="GoogleSignIn" data-theme="dark" data-login-status="">&nbsp;</div></li>
                    <?php
                    }
					$paypal_data=$c['config']['Platform']['Paypal']['SignIn'];
					if(in_array('paypal_login', $c['plugins']['Used']) && $paypal_data['Data']['client_id']){
                        echo ly200::load_static('/static/js/oauth/paypal/api.js');
                        $_domain=!$paypal_data['Data']['domain']?ly200::get_domain():$paypal_data['Data']['domain'];
                    ?>
                        <li id="paypalLogin" appid="<?=$paypal_data['Data']['client_id'];?>" u="<?=htmlspecialchars_decode(rtrim($_domain, '/').$c['config']['Platform']['Paypal']['ReturnUrl']);?>" scopes="<?=(int)db::get_row_count('user', 'PaypalId like "%@%"', 'UserId'); ?>"></li>
                    <?php
					}
					$vk_data=$c['config']['Platform']['VK']['SignIn'];
					if(in_array('vk_login', $c['plugins']['Used']) && $vk_data['Data']['apiId']){
						echo ly200::load_static('/static/js/oauth/vk.js');
					?>    
                    	<li id="vk_button" apiid="<?=$vk_data['Data']['apiId']?>"></li>
                    <?php
					}
					/*
					$instagram_data=$c['config']['Platform']['Instagram']['SignIn'];
					if(in_array('instagram_login', $c['plugins']['Used']) && $instagram_data['Data']['client_id'] && $instagram_data['Data']['client_secret']){
						echo ly200::load_static('/static/js/oauth/instagram.js');
					?>    
                    	<li id="instagram_button" client_id="<?=$instagram_data['Data']['client_id']?>" client_secret="<?=$instagram_data['Data']['client_secret']?>" redirect_uri="<?=(rtrim(ly200::get_domain(),'/').'/?login_with_instagram=1');?>" login="<?=(int)$_GET['login_with_instagram'] ? 1 : 0; ?>" code="<?=trim($_GET['code']); ?>"></li>
                    <?php }
					*/?>
                </ul>
            <?php }?>
            <div class="blank20"></div>
            <a href="/account/sign-up.html" class="signup FontBorderColor FontColor"><?=$c['lang_pack']['join_free'];?></a>
            <a href="/account/forgot.html" class="forgot"><?=$c['lang_pack']['user']['forgotPWD'];?></a>
            <div class="clear"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	user_obj.sign_in_init();
	<?php
	//默认打开登录框
	if((int)$c['config']['global']['UserLogin'] && !$_SESSION['User']['UserLoginTime']){
		echo "user_obj.set_form_sign_in('', '', 1);";
		$_SESSION['User']['UserLoginTime']=$c['time'];
	}?>
	var timer;
	$('.global_login_sec').hover(
		function(){
			clearTimeout(timer);
			$(this).find('.SignInButton_sec').addClass('cur');
			$(this).find('.signin_box_sec').fadeIn();
		},
		function(){
			var _this=$(this);
			timer=setTimeout(function(){
				_this.find('.SignInButton_sec').removeClass('cur');
				_this.find('.signin_box_sec').fadeOut();
			},500);
		}
	);
});
</script>
<?php }?>
