<?php !isset($c) && exit();?>
<?php
$saveId=(int)$CateId?$CateId:(int)$SecCateId;
if(!file::check_cache("catalog-{$saveId}.html")){
    ob_start();
    
    if(!$allcate_ary){
        $c_where="IsSoldOut=0";
        // if($saveId) $c_where.=" and UId like ',{$saveId},%'";
        // else $c_where.=" and UId='0,'";
        $allcate_row=str::str_code(db::get_all('products_category', $c_where, '*',  $c['my_order'].'CateId asc'));
        $allcate_ary=array();
        foreach((array)$allcate_row as $k=>$v){
            $allcate_ary[$v['UId']][]=$v;
        }
    }
    if($CateId){
        $sec_count=db::get_row_count('products_category', "UId like '{$category_row['UId']}{$CateId},'");
        $row=$sec_count ? ($category_row?$category_row:$SecCategory_row) : $SecCategory_row;
        echo html::website_h1('h1', $category_row['Category'.$c['lang']], 'class="category_h1"');
    }
?>
    <div class="themes_bor new_side_category <?=$CateId && !$sec_count && $category_row['Dept']==1 ? 'hide' : ''; ?>">
        <?php
        if($CateId){
        ?>
            <div class="cate_title"><?=$row['Category'.$c['lang']];?></div>
            <dl>
                <?php
                $all_uid=$row['UId'].($sec_count ? ($CateId?$CateId:$SecCateId) : $SecCateId).',';
                foreach((array)$allcate_ary[$all_uid] as $v){
                    $vCateId=$v['CateId'];
                ?>
                <dd class="first">
                    <a class="catalog_<?=$vCateId;?>" href="<?=ly200::get_url($v, 'products_category');?>"><h2><?=$v['Category'.$c['lang']];?></h2></a>
                    <?php if((int)$v['SubCateCount']){?>
                    <dl class="catalog_<?=$vCateId;?> hide">
                        <?php
                        foreach((array)$allcate_ary["{$all_uid}{$vCateId},"] as $v2){
                            $v2CateId=$v2['CateId'];
                        ?>
                            <dd><a class="catalog_<?=$v2CateId?>" href="<?=ly200::get_url($v2, 'products_category');?>"><h3><?=$v2['Category'.$c['lang']];?></h3></a></dd>
                        <?php }?>
                    </dl>
                    <?php }?>
                </dd>
                <?php }?>
            </dl>
        <?php
        }else{
        ?>
            <?=html::website_h1('h1', $c['lang_pack']['related_category'], 'class="cate_title"'); ?>
            <dl>
                <?php foreach((array)$allcate_ary['0,'] as $v){?>
                    <dd class="first"><a href="<?=ly200::get_url($v, 'products_category');?>"><h2><?=$v['Category'.$c['lang']];?></h2></a></dd>
                <?php }?>
            </dl>
        <?php }?>
    </div>
<?php 
    $cache_contents=ob_get_contents();
    ob_end_clean();
    file::write_file(ly200::get_cache_path($c['theme'], 0), "catalog-{$saveId}.html", $cache_contents);
}
include(ly200::get_cache_path($c['theme'])."catalog-{$saveId}.html");
?>
<?php if($CateId){?>
<script type="text/javascript">
$('dl.catalog_<?=$CateId;?>, dl.catalog_<?=(int)$SecCateId;?>').addClass('show').removeClass('hide');
$('a.catalog_<?=$CateId;?>, a.catalog_<?=(int)$SecCateId;?>').addClass('current');
</script>
<?php }?>