<?php !isset($c) && exit();?>
<?php
if(!file::check_cache('global_popular_search.html')){
	ob_start();
?>
<div class="themes_bor new_sidebar">
    <h2 class="b_title FontColor"><?=$c['lang_pack']['global_popular_search'];?></h2>
    <div class="pop_search">
        <?php
        $search_row=str::str_code(db::get_all('global_popular_search', '1=1', '*', 'SId desc'));
		foreach ($search_row as $k=>$v){?><?=$k?', ':'';?><a href="<?=$v['Url']?$v['Url']:"/search/?Keyword={$v['Name'.$c['lang']]}";?>"><?=$v['Name'.$c['lang']];?></a><?php }?>
    </div>
</div>
<?php 
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'global_popular_search.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'global_popular_search.html');
?>
