<?php !isset($c) && exit();?>
<?php
if(!file::check_cache('global_seckill.html')){
	ob_start();
	$seckill_row=str::str_code(db::get_one('sales_seckill s left join products p on s.ProId=p.ProId', "s.StartTime<={$c['time']} and {$c['time']}<s.EndTime", "s.*, p.Name{$c['lang']}, p.PicPath_0, Price_0", 's.SId desc'));
	if($c['FunVersion']==2 && $seckill_row){
			$name=$seckill_row["Name{$c['lang']}"];
			$url=ly200::get_url($seckill_row, 'seckill');
			$img=ly200::get_size_img($seckill_row['PicPath_0'], '240x240');
?>
	<div class="themes_bor new_seckill_box new_sidebar_to">
		<h2 class="seckill_title b_title FontColor"><?=$c['lang_pack']['flashSales'];?></h2>
		<div class="seckill_main b_main">
			<div class="item">
				<div class="img pic_box"><a href="<?=$url?>" title="<?=$name;?>"><img src="<?=$img;?>" alt="<?=$name;?>" /></a><span></span></div>
				<div class="name"><a href="<?=$url?>" title="<?=$name;?>"><?=$name;?></a></div>
				<div class="days clean" endTime="<?=date('Y/m/d H:i:s', $seckill_row['EndTime']);?>">
					<div class="i fl"><div class="pink d">00</div><?=$c['lang_pack']['day'];?></div>
					<div class="i fl"><div class="h">00</div><?=$c['lang_pack']['hours'];?></div>
					<div class="i fl"><div class="m">00</div><?=$c['lang_pack']['mins'];?></div>
					<div class="i fl"><div class="s">00</div><?=$c['lang_pack']['secs'];?></div>
				</div>
			</div>
		</div>
	</div>
<?php
	}
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'global_seckill.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'global_seckill.html');
?>
<script type="text/javascript">
$(document).ready(function(){
	//秒杀时间倒计时
	$(".new_seckill_box .item .days").each(function(){
		var obj=$(this),
			time=new Date();
		obj.genTimer({
			beginTime: ueeshop_config.date,
			targetTime: obj.attr("endTime"),
			callbackOnlyDatas:1,
			callback: function(e){
				obj.find('.d').html(e.dates>99?99:e.dates);
				obj.find('.h').html(e.hours);
				obj.find('.m').html(e.minutes);
				obj.find('.s').html(e.seconds);
			}
		});
	});
});
</script>