<?php !isset($c) && exit();?>
<?php
if(!file::check_cache('global_newsletter.html')){
	ob_start();
?>
<div class="themes_bor new_newsletter">
    <div class="title"><?=$c['lang_pack']['newsletter'];?></div>
    <div class="desc"><?=ucfirst($c['lang_pack']['newsletterWTips']);?></div>
    <form name="newsletter">
        <input type="text" class="themes_bor input" name="Email" format="Email" placeholder="<?=$c['lang_pack']['emailAddress']; ?>"  notnull />
        <button><?=$c['lang_pack']['newsletter_btn']; ?></button>
    </form>
</div>
<?php 
	$cache_contents=ob_get_contents();
	ob_end_clean();
	file::write_file(ly200::get_cache_path($c['theme'], 0), 'global_newsletter.html', $cache_contents);
}
include(ly200::get_cache_path($c['theme']).'global_newsletter.html');
?>