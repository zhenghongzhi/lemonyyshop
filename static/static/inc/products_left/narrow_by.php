<?php !isset($c) && exit();?>
<div class="themes_bor new_narrow_by">
    <?php
    $all_count=0;
    foreach((array)$attr_ary as $v){
    ?>
		<dl>
			<dt class="cur"><strong><?=$v['Name'.$c['lang']];?></strong><em></em></dt>
			<dd>
				<?php
				$i=0;
				foreach((array)$all_value_ary[$v['AttrId']] as $k2=>$v2){

					$v['Type']==0 && $k2='s'.$k2;
					$url=ly200::get_narrow_url($no_narrow_url, $Narrow_ary, $k2, $v['Type']);
					$num=ly200::get_narrow_pro_count($Narrow_ary, $k2, $CateId, $v['Type']);
					if(!$num) continue;
					$all_count++;
				?>
					<a rel="nofollow" href="<?=$url;?>"<?=$i>5?' style="display:none;"':'';?> hidefocus="true"<?=in_array($k2, $Narrow_ary)?' class="current"':'';?>>
						<em class="ns_icon_checkbox"></em>
						<span><?=$v2['Value'.$c['lang']];?><i>(<?=$num;?>)</i></span>
					</a>
				<?php
					++$i;
				}?>
				<?php if($i>6){?>
					<a rel="nofollow" href="javascript:;" class="view_more"><?=$c['lang_pack']['mobile']['view_all'];?></a>
				<?php }?>
			</dd>
		</dl>
    <?php }?>
</div>
<?php if(!$all_count){ ?>
	<script>$('.new_narrow_by').remove();</script>
<?php } ?>