<?php !isset($c) && exit();?>
<?php
/*
 * Powered by ueeshop.com		http://www.ueeshop.com
 * 广州联雅网络科技有限公司		020-83226791
 */
js::jump_301();	//301跳转

//网站基本设置
$config_row = db::get_all('config', 'GroupId="global" or GroupId="translate" or GroupId="chat" or (GroupId="products_show" and Variable="Config") or (GroupId="products_show" and Variable="tab") or (GroupId="products_show" and Variable="item") or (GroupId="content_show" and Variable="Config") or (GroupId="email" and Variable="notice") or GroupId="paypal" or GroupId="print"');
foreach ($config_row as $v) {
	if (in_array("{$v['GroupId']}|{$v['Variable']}",
        array('content_show|Config', 'products_show|Config', 'global|OrdersSmsStatus', 'global|SearchTips', 'global|CopyRight', 'global|CloseWeb',
                'global|Notice', 'global|ArrivalInfo', 'global|IndexContent', 'global|HeaderContent', 'global|TopMenu', 'global|ContactMenu',
                'global|ShareMenu', 'global|LanguageFlag', 'global|LanguageCurrency', 'translate|TranLangs', 'email|notice'))) {
		$c['config'][$v['GroupId']][$v['Variable']] = str::json_data(htmlspecialchars_decode($v['Value']), 'decode');
	} elseif ("{$v['GroupId']}|{$v['Variable']}" == 'global|Language') {
		$c['config'][$v['GroupId']][$v['Variable']] = explode(',', $v['Value']);
	} else {
		$c['config'][$v['GroupId']][$v['Variable']] = $v['Value'];
	}
}

//平台授权
$platform_row = db::get_all('sign_in', '1');
foreach ($platform_row as $v) {
	$c['config']['Platform'][$v['Title']] = str::json_data(htmlspecialchars_decode($v['Data']), 'decode');
	$c['config']['Platform'][$v['Title']]['ReturnUrl'] = $v['ReturnUrl']; //第三方登录返回地址
}
$c['config']['products_show']['Config']['price'] = 1;
$c['config']['products_show']['Config']['review'] = 1;
$c['config']['products_show']['Config']['favorite'] = 1;

//设置语言版
$c['lang_oth'] = str_replace('-', '_', array_shift(explode('.', $_SERVER['HTTP_HOST'])));
$c['lang'] = '_' . (@in_array($c['lang_oth'], $c['config']['global']['Language']) ? $c['lang_oth'] : $c['config']['global']['LanguageDefault']);

//发货地
$overseas_row = str::str_code(db::get_all('shipping_overseas', '1', '*', $c['my_order'].'OvId asc'));
foreach ($overseas_row as $v) {
	if((int)$c['config']['global']['Overseas'] == 0 && $v['OvId'] != 1) continue; //关闭海外仓功能，不是China的都踢走
	$v['Name' . $c['lang']] == '' && $v['Name' . $c['lang']] = $v['Name_en'];
	$c['config']['Overseas'][$v['OvId']] = $v;
}

// 判断官网登陆点击前台
ly200::official_website_landing();

//网站关闭
if (ly200::lock_close_website()) {
	echo $c['config']['global']['CloseWeb']['CloseWeb' . $c['lang']];
	exit;
}

//浏览器语言跳转
if (!$_SESSION['jump_lang']) {
	js::jump_lang();
}

//前台风格设置
if (!ly200::is_mobile_client(1) && $_GET['m'] != 'buynow' && $_GET['client'] != 'mobile') {
    //网页版设置	//buynow 手机版的单独购物流程//手机版编辑模式
	$themes_row = db::get_one('config_module', 'IsDefault=1', 'Themes, Themes_Type'); //网站当前风格
	$c['theme'] = $themes_row['Themes'];
	$c['themes_type'] = $themes_row['Themes_Type'];
	!$c['theme'] && $c['theme'] = 'default';
	$c['theme_path'] = $c['root_path'] . "/static/themes/{$c['theme']}/"; //当前风格的物理路径
	if ($c['theme'] != 'default' && !@is_dir($c['theme_path']) && !@is_file($c['theme_path'] . 'index.php')) {//当前风格文件不存在
		$c['theme'] = 'default';
		$c['theme_path'] = $c['root_path']."/static/themes/{$c['theme']}/"; //当前风格的物理路径
	}
	if (@is_file($c['theme_path'].'inc/themes_set.php')) {
        //载入风格配置文件
		include($c['theme_path'].'inc/themes_set.php');
		//载入模板数据
		$c['web_pack'] = array();
		$web_global=db::get_all('web_visual', "Themes='global' and Pages='search'");
		$web_global_data = db::get_all('web_visual', "Themes='{$c['theme']}' and Pages='global'");  //模板全局数据
		if($_GET['m'] == 'index') $web_index_data = db::get_all('web_visual', "Themes='{$c['theme']}' and (Pages='index' or Pages='banner')");  //首页数据
		$web_data = array_merge((array)$web_global_data, (array)$web_index_data, (array)$web_global);
		foreach ((array)$web_data as $v) {
			$web_data_lang = str::json_data($v['Data'], 'decode');
			$web_data_lang = $web_data_lang[substr($c['lang'], 1)];
			$c['web_pack'][$v['Pages']][$v['Position']] = $web_data_lang;
			$c['web_pack'][$v['Pages']][$v['Position']]['Layout'] = $v['Layout'];
		}
	}
} else {
    //手机版设置
	$c['theme_path'] = $c['root_path'] . $c['mobile']['tpl_dir']; //手机版当前风格的物理路径
	$mobile_config = array();
	$set_row = db::get_all('config', 'GroupId="mobile"');
	foreach ($set_row as $v) {
		$mobile_config[$v['Variable']] = $v['Value'];
	}
	$c['mobile'] = array_merge($c['mobile'], array(
			'theme_path'=>	$c['root_path'] . $c['mobile']['tpl_dir'], //手机版物理路径
			'HeadIcon'	=>	$mobile_config['HeadIcon'], //图标 0 白色 1 黑色
			'HeadFixed'	=>	$mobile_config['HeadFixed'], //固定头部
			'LogoPath'	=>	@is_file($c['root_path'] . $mobile_config['LogoPath']) ? $mobile_config['LogoPath'] : $c['config']['global']['LogoPath'], //Logo
			'HomeTpl'	=>	@is_file("{$c['theme_path']}index/{$mobile_config['HomeTpl']}/template.php")?$mobile_config['HomeTpl']:'06', //首页模板
			'ListTpl'	=>	'00'
		)
	);
	if ((int)$_GET['ueeshop_store'] == 1) {
        //进入店铺模式
		$c['mobile']['HomeTpl'] = $c['mobile']['ListTpl'] = 'store';
	}
	//载入模板数据
	$c['web_pack'] = array();
	$web_global_data = db::get_all('web_visual', "Themes='m{$c['mobile']['HomeTpl']}' and Pages='mglobal'");  //全局数据
	if($_GET['m'] == 'index') $web_index_data = db::get_all('web_visual', "Themes='m{$c['mobile']['HomeTpl']}' and (Pages='mindex' or Pages='mbanner')");  //首页数据
	$web_data = array_merge((array)$web_global_data, (array)$web_index_data);
	foreach ((array)$web_data as $v) {
		$web_data_lang = str::json_data($v['Data'], 'decode');
		$web_data_lang = $web_data_lang[substr($c['lang'], 1)];
		$c['web_pack'][$v['Pages']][$v['Position']] = $web_data_lang;
		$c['web_pack'][$v['Pages']][$v['Position']]['Layout'] = $v['Layout'];
	}
}
$c['old_coupon_acctime'] = (int)db::get_value('config', 'GroupId="delete_data" and Variable="old_coupon_acctime"', 'Value'); //小于这个时间创建的优惠券是旧版优惠券
$c['static_path'] = $c['root_path'] . '/static/static/'; //风格入口的物理路径
$c['default_path'] = $c['root_path'] . '/static/themes/default/'; //默认风格的物理路径
$c['lang_pack'] = include("{$c['static_path']}/lang/" . substr($c['lang'], 1) . '.php'); //加载语言包
$c['holiday_theme'] = '/static/themes/default/holiday/';
$gender_ary = array();
foreach ($c['gender'] as $k => $v) {
	$gender_ary[$k] = $c['lang_pack'][$v];
}
$c['gender'] = $gender_ary;

ly200::check_user_id(); //检测保持登录会员ID

//查询条件
$c['where'] = array(
	'user_id'		=>	"user_id='{$_SESSION['User']['UserId']}'",
	'user'			=>	(int)$_SESSION['User']['UserId'] ? "UserId='{$_SESSION['User']['UserId']}'" : "SessionId='{$c['session_id']}'",
	'cart'			=>	(int)$_SESSION['User']['UserId'] ? "UserId='{$_SESSION['User']['UserId']}'" : "SessionId='{$c['session_id']}'",
	'products'		=>	" and (((SoldStatus!=2 and Stock>=0) or (SoldStatus=2 and Stock>0)) and (SoldOut=0 or SoldOut=2 or (SoldOut=1 and IsSoldOut=1 and {$c['time']} between SStartTime and SEndTime)))",//产品上下架显示条件
	'products_ext'	=>	array(
							1	=>	' and IsNew=1',
							2	=>	' and IsHot=1',
							3	=>	' and IsBestDeals=1',
							4	=>	" and (IsPromotion=1 and {$c['time']} between StartTime and EndTime)"
						)
);
$c['cache_timeout'] = 3600*2; //更新缓存文件间隔(s)
if($_SESSION['Manage']['UserId'] > 0 || $_SESSION['Manage']['UserName'] || $_SESSION['Manage']['UserId'] == '-1'){//后台管理员上线
	$c['cache_timeout'] = 10;
}

$_SESSION['User']['UserId'] && !$_SESSION['sales_coupon_relation'] && user::sales_coupon_relation($_SESSION['User']); //会员登录自动绑定优惠券

$c['cart'] = array('timeout' => 86400 * 7);	//删除超过N天，非会员所加入的购物车产品(天)

//货币和汇率
$c['shopping_cart'] = db::get_one('shopping_cart', $c['where']['user'], 'count(CId) as TotalQty, sum((Price+PropertyPrice)*Discount/100*Qty) as TotalPrice');
$c['currency'] = array();
$manage_default = 0;
$currency_row = db::get_all('currency', 'IsUsed="1"', '*', 'IsDefault desc, CId asc');
foreach ((array)$currency_row as $k => $v) {
	$c['currency'][] = $v;
	$c['currency']['Symbol'][] = $v['Symbol'];
	$v['ManageDefault'] == 1 && $manage_default = $k;
}
if (!isset($_SESSION['Currency']) || !$_SESSION['Currency']) {
	if (@!in_array($_SESSION['Currency']['Symbol'], $c['currency']['Symbol'])) {
		$_SESSION['ManageCurrency'] = $c['currency'][$manage_default]; //后台默认货币
	}
	//不同语言打开网站时对应不同默认货币
	if ($_currency_id = $c['config']['global']['LanguageCurrency'][substr($c['lang'], 1)]) {
		$currency_row = db::get_one('currency', "IsUsed=1 and CId='{$_currency_id}'");
		if ($currency_row) {
			$_SESSION['Currency'] = $currency_row;
		} else {
            //相应货币没有开启
			$_SESSION['Currency'] = $currency_result ? $currency_result : $c['currency'][0];
		}
	} else {
        //还原默认货币
		$_SESSION['Currency'] = $currency_result ? $currency_result : $c['currency'][0];
	}
}

//App
$c['plugin_app'] = new plugin('app'); //插件类(app插件)
if ($c['plugin_app']->trigger('distribution', '__config', 'DIST_get_check') == 'enable') {
    //分销APP是否存在
	$c['plugin_app']->trigger('distribution', 'DIST_get_check'); //检查分销识别码
}
if ($c['plugin_app']->trigger('swap_chain', '__config', 'update_urls_list') == 'enable') {
    //交换链接
	$c['plugin_app']->trigger('swap_chain', 'update_urls_list');
}
$c['plugins']['Used'] = array();
$app_used_row = db::get_all('plugins', 'Category="app" and IsUsed=1 and IsInstall=1'); //已使用的APP
foreach ($app_used_row as $k => $v) {
	if (!in_array($v['ClassName'], $c['un_used_ary'][$c['FunVersion']])) {
		$c['plugins']['Used'][] = $v['ClassName'];
	}
}
if ($_GET['client'] == 'website' || $_GET['client'] == 'mobile'){
    //可视化后台编辑不加载插件
	$disabled_ary = array('facebook_messenger', 'facebook_login', 'facebook_pixel', 'facebook_ads_extension', 'facebook_store', 'google_login', 'vk_login', 'paypal_marketing_solution');
	foreach ((array)$c['plugins']['Used'] as $k => $v){
		if (in_array($v, $disabled_ary)) unset($c['plugins']['Used'][$k]);
	}
}

//程序处理
$do_action = isset($_POST['do_action']) ? $_POST['do_action'] : $_GET['do_action'];
if ($do_action) {
    development::web_do_action($do_action);
	$_ = @explode('.', $do_action);
	$do_action_file = "{$c['static_path']}/do_action/{$_[0]}.php";
	if (@is_file($do_action_file)) {
		include($do_action_file);
		if (method_exists($_[0] . '_module', $_[1])) {
			eval("{$_[0]}_module::{$_[1]}();");
			exit;
		}
	}
}

//收藏列表
$_SESSION['User']['UserId'] && $user_favorite_row=db::get_all('user_favorite',"UserId='{$_SESSION['User']['UserId']}'",'ProId');
$user_favorite_ary=array();
foreach((array)$user_favorite_row as $v){
	$user_favorite_ary[]=$v['ProId'];
}

//开启“仅会员浏览”功能，非会员自动跳转会员登录页
($c['config']['global']['UserView'] == 1 && !(int)$_SESSION['User']['UserId'] && !substr_count($_SERVER['REQUEST_URI'], '/account/')) && js::location('/account/sign-up.html');

//默认产品列表排序 1:Name 2:Price 3:New 4:Best Sellers
$c['products_sort'] = array(
	'1d'	=>	'IsBestDeals desc,',
	'2d'	=>	'Sales desc,',
	'3d'	=>	'FavoriteCount desc,',
	'4d'	=>	'IsNew desc,',
	'5a'	=>	'LowestPrice asc,',
	'5d'	=>	'LowestPrice desc,'
);

$c['powered_by'] = ly200::powered_by((int)$c['HideSupport']); //技术支持
$c['config']['global']['IsVgcart'] = @stripos($_SERVER['HTTP_HOST'], 'vgcart');
if ($c['config']['global']['IsVgcart']) {
	$c['config']['global']['CopyRight']['CopyRight' . $c['lang']] = '';
}
if ($_COOKIE['REFERER'] == '') {
    //COOKIE记录来源ID
	$referrerId = str::referrer_filter($_SERVER['HTTP_REFERER']);
	$_COOKIE['REFERER'] = $referrerId;
	setcookie('REFERER', $referrerId);
}

//检查CDN是否正常
if ((int)$c['config']['global']['IsCdn']) {
	$c['cdn_available'] = 0;
	$result_cdn = ly200::curl('http:' . $c['cdn'] . 'test.png');
	$result_cdn && $c['cdn_available'] = 1;
}

if (!$_GET['m'] && !$_GET['a'] && !$_POST['a'] && !$_POST['d'] && !$_GET['d'] && !$_GET['do_action'] && !$_POST['do_action']) {
	//链接不存在，解决软404问题
	@header('HTTP/1.1 404');
	exit;
}

//加载内容
$m = $_GET['m'];
$a = $_GET['a'] ? $_GET['a'] : ($_POST['a'] ? $_POST['a'] : 'index');
$d = $_GET['d'] ? $_GET['d'] : $_POST['d'];
ob_start();
if ((ly200::is_mobile_client(0) || $m == 'buynow') && !in_array($m, array('blog', 'holiday'))) {
    //是否为手机版
    //holiday 节日显示PC版有错误  buynow 手机版的单独购物流程
	$file = (in_array($m, array('user', 'cart', 'products', 'tuan', 'seckill', 'gateway', 'buynow'))) ? "$m/index.php" : "$m.php";
	$m == 'picture' && $file = "products/detail/$a.php"; //产品详细页的主图加载
	if ($m != 'gateway' && (ly200::lock_china_ip() || ly200::lock_china_browser())) {
        //屏蔽国内IP跳转到屏蔽页面
		include($c['theme_path'] . 'shield.php');
	} else {
		if ($m == 'gateway' && @is_file("{$c['default_path']}{$file}")) {
            //支付接口返回路径
			include("{$c['default_path']}{$file}");
		} elseif (@is_file("{$c['theme_path']}{$file}")) {
			include("{$c['theme_path']}{$file}");
		} else {//页面不存在跳转到404
			include("{$c['theme_path']}404.php");
		}
	}
} else {
	$shield = 1;
	if ($m != 'gateway' && (ly200::lock_china_ip() || ly200::lock_china_browser())) {
        //屏蔽国内IP跳转到屏蔽页面
		include($c['default_path'] . 'shield.php');
	} else {
		$c['config']['translate']['IsTranslate'] = 1; //后台没有了开关 这里默认开启
		if ($c['config']['translate']['IsTranslate'] == 1) {
            //设置Google翻译部分
			foreach ($c['config']['translate']['TranLangs'] as $k => $v) {
				if (@in_array($v, $c['config']['global']['Language'])) {
					unset($c['config']['translate']['TranLangs'][$k]);
				} elseif ($v == 'zh-ch' && @in_array('cn', $c['config']['global']['Language'])) {
					unset($c['config']['translate']['TranLangs'][$k]);
				} elseif ($v == 'zh-tw' && @in_array('zh_tw', $c['config']['global']['Language'])) {
					unset($c['config']['translate']['TranLangs'][$k]);
				} elseif ($v == 'ja' && @in_array('jp', $c['config']['global']['Language'])) {
					unset($c['config']['translate']['TranLangs'][$k]);
				}
			}
		}
		$file = (in_array($m, array('user', 'cart', 'products', 'holiday', 'blog', 'seckill', 'tuan', 'gateway'))) ? "$m/index.php" : "$m.php";
		if ($m == 'cart' && (int)db::get_value('config', 'GroupId="cart" and Variable="IsOldCart"', 'value') == 1) {
            //新旧购物车文件的切换
			$file = "{$m}_old/index.php";
		}
		if (@is_file("{$c['theme_path']}{$file}")) {
            //判断当前风格文件是否存在
			include("{$c['theme_path']}{$file}");
		} elseif (@is_file("{$c['default_path']}{$file}")) {
            //否则加载默认文件
			include("{$c['default_path']}{$file}");
		} else {
            //页面不存在跳转到404
			include($c['default_path'] . '404.php');
		}
	}
}
$html = ob_get_contents();
ob_end_clean();
ly200::load_cdn_contents($html);
